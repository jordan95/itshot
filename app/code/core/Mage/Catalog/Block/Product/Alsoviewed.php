<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Product View block
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @module     Catalog
 */
class Mage_Catalog_Block_Product_Alsoviewed extends Mage_Catalog_Block_Product_Abstract
{

    const ATTRIBUTE_ID_STATUS = 96;
    const ATTRIBUTE_ID_GENDER = 2051;
    const ATTRIBUTE_ID_META_KEYWORD = 83;

    public function getProductIdsByMetaKeyword()
    {
        if (Mage::registry('product_ids_meta_keyword') !== null) {
            return Mage::registry('product_ids_meta_keyword');
        }
        $_product = $this->getProduct();
        $meta_keyword = $_product->getData('meta_keyword');
        $words = $this->getWordsByLength($meta_keyword);
        $productIds = array();
        if (count($words) > 0) {
            $sql = 'SELECT DISTINCT (icpet.entity_id) FROM ' . $this->getTableName('catalog_product_entity_text') . ' AS icpet';
            $sql .= ' JOIN ' . $this->getTableName('catalog_product_entity_int') . ' AS icpei ON icpei.entity_id = icpet.entity_id';
            $sql .= ' JOIN ' . $this->getTableName('cataloginventory_stock_item') . ' AS stock_items ON icpei.entity_id = stock_items.product_id';
            $sql .= ' WHERE icpei.attribute_id = ' . self::ATTRIBUTE_ID_STATUS . ' AND icpei.value = 1'; //check status
            $sql .= ' AND stock_items.is_in_stock = 1';
            $sql .= ' AND icpet.attribute_id = ' . self::ATTRIBUTE_ID_META_KEYWORD . ' AND (';
            $countWords = count($words);
            for ($i = 0; $i < $countWords; $i++) {
                $word = $this->quoteString("%$words[$i]%");
                if ($i === 0) {
                    $sql .= ' icpet.value LIKE ' . $word;
                } else {
                    $sql .= ' OR icpet.value LIKE ' . $word;
                }
            }
            $sql .= ' )';
            $sql .= ' AND icpet.entity_id <>' . $_product->getId();
            $productIds = $this->queryIds($sql);
        }
        Mage::register('product_ids_meta_keyword', $productIds);
        $this->showDebug(__FUNCTION__);
        return $productIds;
    }

    public function getProductIdsByCategoryIds()
    {
        $categoryIds = $this->getAllCategoryIds();
        $productIds = array();
        if (count($categoryIds) > 0) {
            $sql = 'SELECT DISTINCT(a.product_id) FROM ' . $this->getTableName('catalog_category_product') . ' AS a';
            $sql .= ' JOIN ' . $this->getTableName('catalog_product_entity_int') . ' AS icpei ON icpei.entity_id = a.product_id';
            $sql .= ' JOIN ' . $this->getTableName('cataloginventory_stock_item') . ' AS stock_items ON a.product_id = stock_items.product_id';
            $sql .= ' WHERE icpei.attribute_id=' . self::ATTRIBUTE_ID_STATUS . ' AND icpei.value=1'; //check status
            $sql .= ' AND a.category_id IN(' . join(',', $categoryIds) . ')';
            $excludeIds = $this->getProductIdsByMetaKeyword();
            if (count($excludeIds) > 0) {
                $sql .= ' AND a.product_id NOT IN(' . join(',', $excludeIds) . ')';
            }
            $sql .= ' AND stock_items.is_in_stock = 1';
            $productIds = $this->queryIds($sql);
        }
        $this->showDebug(__FUNCTION__);
        return $productIds;
    }

    public function getAlsoViewedProductIds()
    {
        if (Mage::registry('also_viewed_product_ids')) {
            return Mage::registry('also_viewed_product_ids');
        }
        $productIds = array_merge($this->getProductIdsByMetaKeyword(), $this->getProductIdsByCategoryIds());
        Mage::register('also_viewed_product_ids', $productIds);
        return $productIds;
    }

    public function getProductCollection($includeIds, $limit = 3)
    {
        $_product = $this->getProduct();
        $special_price = $_product->getSpecialPrice();
        $max_price = $special_price * 1.4;
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addFieldToFilter('entity_id', array('in' => $includeIds));
        $collection->addAttributeToFilter('special_price', array('from' => $special_price, 'to' => $max_price));
        if ($_product->getData('c2c_gender_for_filter')) {
            $collection->addAttributeToFilter('c2c_gender_for_filter', $_product->getData('c2c_gender_for_filter'));
        }
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collection->addAttributeToSelect($attributes);
        $collection->getSelect()->limit($limit);
        $products = array();
        $c1 = null;
        if ($collection->count() < $limit) {
            $limit_1 = $limit - $collection->count();
            $c1 = $this->getProductPriceNearest($includeIds, $limit_1);
        }

        if ($collection->count() > 0) {
            foreach ($collection as $p) {
                $products[] = $p;
            }
        }
        if ($c1 && count($c1) > 0) {
            foreach ($c1 as $p1) {
                $products[] = $p1;
            }
        }
        $this->showDebug(__FUNCTION__);

        return $products;
    }

    public function getProductPriceNearest($includeIds, $limit = 3)
    {
        $products = array();
        $c1 = $this->getProductMaxPriceNearest($includeIds, $limit);
        $c2 = null;
        if ($c1->count() < $limit) {
            $limit_1 = $limit - $c1->count();
            $c2 = $this->getProductMinPriceNearest($includeIds, $limit_1);
        }

        if ($c1->count() > 0) {
            foreach ($c1 as $p1) {
                $products[] = $p1;
            }
        }
        if ($c2 && $c2->count() > 0) {
            foreach ($c2 as $p2) {
                $products[] = $p2;
            }
        }
        return $products;
    }

    public function getProductMaxPriceNearest($includeIds, $limit = 3)
    {
        $_product = $this->getProduct();
        $special_price = $_product->getSpecialPrice();
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->addFieldToFilter('entity_id', array('in' => $includeIds))
                ->addAttributeToSort('price', 'ASC')
                ->addAttributeToFilter('price', array('gteq' => $special_price))
                ->addAttributeToFilter('c2c_actual_stock_available', 1);
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collection->addAttributeToSelect($attributes);
        $collection->getSelect()->limit($limit);
        $this->showDebug(__FUNCTION__);
        return $collection;
    }

    public function getProductMinPriceNearest($includeIds, $limit = 3)
    {
        $_product = $this->getProduct();
        $special_price = $_product->getSpecialPrice();
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->addFieldToFilter('entity_id', array('in' => $includeIds))
                ->addAttributeToSort('price', 'DESC')
                ->addAttributeToFilter('price', array('lteq' => $special_price))
                ->addAttributeToFilter('c2c_actual_stock_available', 1);
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collection->addAttributeToSelect($attributes);
        $collection->getSelect()->limit($limit);
        $this->showDebug(__FUNCTION__);
        return $collection;
    }

    public function getAlsoViewedProduct($limit = 3)
    {
        $products = array();
        $c1 = $this->getProductCollection($this->getProductIdsByMetaKeyword(), $limit);
        $c2 = array();
        if (count($c1) < $limit) {
            $limit_1 = $limit - count($c1);
            $c2 = $this->getProductCollection($this->getProductIdsByCategoryIds(), $limit_1);
        }
        if (count($c1) > 0) {
            foreach ($c1 as $p1) {
                $products[] = $p1;
            }
        }
        if (count($c2) > 0) {
            foreach ($c2 as $p2) {
                $products[] = $p2;
            }
        }
        return $products;
    }

    public function getAllCategoryIds($product = null)
    {
        if (!$product) {
            $product = $this->getProduct();
        }

        return $this->_filterCategoryIds($product->getCategoryIds());
    }

    public function _filterCategoryIds(array $ids)
    {
        $filteredCategoryIds = array();
        foreach ($ids as $categoryId) {

            if ($categoryId == Mage::app()->getStore()->getRootCategoryId()) {
                continue;
            }
            $_category = Mage::getModel('catalog/category')->load($categoryId);

            if ($_category->getIsAnchor()) {
                $categoryChildren = $_category->getAllChildren(true);

                $key = array_search($categoryId, $categoryChildren);
                if ($key == false) {
                    unset($categoryChildren[$key]);
                }

                $intersect = array_intersect($ids, $categoryChildren);
                if (count($intersect)) {
                    continue;
                }
            }
            $filteredCategoryIds[] = $categoryId;
        }
        return $filteredCategoryIds;
    }

    public function getWordsByLength($subject, $length = 2)
    {
        $words = array();
        $words_arr = explode(',', $subject);
        foreach ($words_arr as $w) {
            if (str_word_count($w) > $length) {
                $words[] = trim($w);
            }
        }
        return $words;
    }

    public function getTableName($table)
    {
        return $this->getResource()->getTableName($table);
    }

    public function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    public function getConnection()
    {
        return $this->getResource()->getConnection('core_read');
    }

    public function queryIds($sql)
    {
        return $this->getConnection()->fetchCol($sql);
    }

    public function showDebug($message)
    {
        if (isset($_GET['debug_related']) && $_GET['debug_related'] == 'opentechiz') {
            echo $message. '=>';
        }
    }

    public function quoteString($subject)
    {
        return $this->getConnection()->quote($subject);
    }

}
