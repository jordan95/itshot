<?php

/**
 * Description of SitemapEnhancedPlus
 * @package   CueBlocks_SitemapEnhancedPlus
 * @company   CueBlocks - http://www.cueblocks.com/
 */
class CueBlocks_SitemapEnhancedPlus_Model_Processor_Catalog_Product_Image extends CueBlocks_SitemapEnhancedPlus_Model_Processor_Catalog_Product_Abstract
{

    const CUSTOM_LICENSE_URL = 1;

    protected $_sourceModel = 'sitemapEnhancedPlus/processor_catalog_product_image';
    protected $_counterLabel = 'image';
    protected $_configKey = 'image';

    protected function _getProcessCollection($addFirst = false)
    {
        $imgXml = '';

        $queryCollection = $this->getQueryModel()->getCollection();
        // echo "<pre>";print_r($queryCollection);
        while ($row = $queryCollection->fetch()) {
            $imgXml .= $this->_processRow($row);
        }
        unset($queryCollection);

        return $imgXml;
    }

    public function _getQueryModelConfig($usePagination = false)
    {
        $config = parent::_getQueryModelConfig($usePagination);

        $imageConfig = $this->getConfig()->getData();
        $config->addData($imageConfig);
        return $config;
    }

    protected function _processRow($row)
    {
        $imgXml = '';
        $title = '';
        $caption = '';
       
         /**
         * EXCLUDE VALUE SHOULD NOT BE USED:
         * ALSO IF AN IMAGE IS EXCLUDED IT IS SHOWN
         * IF ASSIGNED TO SOMETHING
         */
//            $img_enabled = true;

//            if ($imageRow['disabled'] === '1') {
//                $img_enabled = false;
//            } elseif ($imageRow['disabled'] === null && $imageRow['disabled_default'] === '1') {
//                $img_enabled = false;
//            }

//            if ($img_enabled) {
         //Code added by Ankush as denis ask for customization.
         $productname     = $this->getImageNameWithLabel($row['entity_id'],$row['label']);
         $mainImageLabel  = $this->checkMainImageLabel($row['entity_id']); 
        
        $location = sprintf('<image:loc>%s</image:loc>', $this->_getUrl($row));
        if ($row['label']) {
            $title = sprintf('<image:title>%s</image:title>', htmlspecialchars($productname));
        }
         //Code added by Ankush as denis ask for customization.
        if (($row['short_description'] !='') && ($mainImageLabel == $row['label'])) {			
             $caption = sprintf('<image:caption>%s</image:caption>', htmlspecialchars($row['short_description']));
		   
        }
        $license = $this->_getImageLicense();

        $imgXml .= sprintf('<image:image>%s %s %s %s</image:image>', $location, $title, $caption, $license);
        $this->_increaseLinkCounter();

        return $imgXml;
    }

    protected function _getUrl($row)
    {
        $url = '';
        $url = htmlspecialchars($this->getMediaUrl() . 'catalog/product' . $row['path']);

        return $url;
    }

    protected function getMediaUrl()
    {
        return Mage::app()
            ->getStore($this->getSitemap()->getStoreId())
            ->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, false);
    }

    protected function _getImageLicense()
    {
        $license = '';
        $licenseTypeUrl = $this->getConfig()->getLicenseType();

        if ((bool)$licenseTypeUrl) {
            if ($licenseTypeUrl == self::CUSTOM_LICENSE_URL) {
                $licenseTypeUrl = $this->getConfig()->getLicenseTypeCustom();
            }
            // check that at list we have a content for the license
            if ($licenseTypeUrl) {
                $license = sprintf('<image:license>%s</image:license>', $licenseTypeUrl);
            }
        }

        return $license;
    }
    
public function getImageNameWithLabel($productId,$imagelabel)
 {
        //create connection object
		$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();		
        $imageSortOrder = array(
		//'Main' =>main,
		'Main White' => 'mainwh',
		'Main Yellow'=> 'mainye',
		'Main Rose' => 'mainro',
		'Main Black' => 'mainbl',
		'Main Two Tone' => 'maintt',
		'Main White Yellow Rose' => 'mainwyr',
		'Main White Rose' => 'mainwr',
		'Main White Yellow' => 'mainwy',
		'White' => 'wh',
		'Yellow' => 'ye',
		'Rose' => 'ro',
		'Black' => 'bl', 
		'Two Tone' => 'tt', 
		'White Yellow Rose' => 'wyr', 
		'White Rose' => 'wr', 
		'White Yellow' => 'wy', 
		'Back' => 'back',
		'Back White' => 'backwh',
		'Back Yellow' => 'backye',
		'Back Rose' => 'backro',
		'Back Black' => 'backbl',
		'Back Two Tone' => 'backtt', 
		'Back White Yellow Rose' => 'backwyr', 
		'Back White Rose' => 'backwr', 
		'Back White Yellow' => 'backwy', 
		'Body' => 'bod',
		'Body White' => 'bodwh',
		'Body Yellow' => 'bodye',
		'Body Rose' => 'bodro',
		'Body Black' => 'bodbl',
		'Body Two Tone' => 'bodtt',
		'Body White Yellow Rose' => 'bodwyr',
		'Body White Rose' => 'bodwr',
		'Body White Yellow' => 'bodwy',
		'Box' =>   'box',
		'Box White' => 'boxwh',
		'Box Yellow' => 'boxye',
		'Box Rose' => 'boxro',
		'Box Black' => 'boxbl',
		'Box Two Tone' => 'boxtt',
		'Box White Yellow Rose' => 'boxwyr',
		'Box White Rose' => 'boxwr',
		'Box White Yellow' => 'boxwy',
		'Ruler' => 'ruler',
		'Ruler White' => 'rulerwh',
		'Ruler Yellow' => 'rulerye',
		'Ruler Rose' => 'rulerro',
		'Ruler Black' => 'rulerbl',
		'Ruler Two Tone' => 'rulertt',
		'Ruler White Yellow Rose' => 'rulerwry',
		'Ruler White Yellow' => 'rulerwy',
		'Ruler White Rose' => 'rulerwr',
		 'aa' => 54,
		 'ab' => 55,
		 'ac' => 56,
		 'ad' => 57,
		 'Clasp' => 'clasp',
		 'Clasp Yellow' => 'claspye',
		 'Clasp Rose' => 'claspro',
		 'Body White' => 'bodwh',
		 'Chain' => 'chain',
		 'white Yellow' => 'whye',
		 'white Rose' => 'whro',
		 'Main White Yellow' => 'mainwhye',
		 'Main White Rose' => 'mainwhro',
		 'Back White Rose' => 'backwhro',
		 'Back White Yellow' => 'backwhye',
		 'Body White Rose' => 'bodwhro',
		 'Body White Yellow' => 'bodwhye',
		 'Box White Rose' => 'boxwhro',
		 'Box White Yellow' => 'boxwhye',
		 'Chain Rose' => 'chainro',
		 'Ruler White Rose' => 'rulerwhro',
		 'Chain White' => 'chainwh',
		 'Chain Yellow' => 'chainye',
		 'Ruler White Yellow' => 'rulerwhye'		 
		 );
		
		$nameSql = "SELECT value FROM ".$tablePrefix."catalog_product_entity_varchar WHERE attribute_id=71 AND entity_id=".$productId;
		$sqlNameRes = $conn->fetchRow($nameSql);
		if($sqlNameRes["value"]!="")
		{
			$productName = $sqlNameRes["value"];
		}		
        //$labelsmall = array_search($position, $imageSortOrder);
        $labelsmall = array_search($imagelabel, $imageSortOrder);        
		$label = ucwords($labelsmall);
        
        $sqlId = "SELECT opt.option_id FROM ".$tablePrefix."catalog_product_option as opt INNER JOIN ".$tablePrefix."catalog_product_option_title as tit on tit.option_id= opt.option_id WHERE opt.product_id= '".$productId."' AND tit.title LIKE '%color%'";				
		$optionRes = $conn->fetchRow($sqlId);		
        $optionId = $optionRes["option_id"]; 
		
		if($optionId>0) 
		{  //in white gold
			//if($label == 'Main' || $label == 'Back' || $label == 'Bod' || $label == 'Body' || $label == 'Box' || $label == 'Two Tone')
			if($label == 'Back' || $label == 'Body' || $label == 'Box' || $label == 'Two Tone')
			{
				$finaleLabel = $label.' Image'; 
			}
			else 
		    {
			  if($imagelabel != 'main')
		      {
			   $finaleLabel = 'in '.$label.' Gold Image'; 
		      }
		      else
		      {
			     $finaleLabel = ''; 
			  }
			}
			 
			
		  $labelname	= $productName.' '.$finaleLabel;
	    }
	    else
	    { 
		   if($imagelabel == 'main')
		   {	
		    $labelname	= $productName;	
	       }
	       else
	       {
			   $labelname	= $productName.' '.$label.' Image';	
		   }
	       
	       
	       
		}
        return $labelname;
 }
    
function checkMainImageLabel($productId)
  { 
        //global $conn;
        $conn = Mage::getSingleton('core/resource')->getConnection('core_write');
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();		
        $selectMainValueMain= "select value from ".$tablePrefix."catalog_product_entity_varchar where attribute_id = 85 and entity_id='".$productId."' ";
		$selectMainValueMainRes = $conn->fetchRow($selectMainValueMain);
		$mainImage  = $selectMainValueMainRes['value'];	    
		if($mainImage !='')
		{		
			 $imageNameSql = "SELECT * FROM `".$tablePrefix."catalog_product_entity_media_gallery` AS mgallery, ".$tablePrefix."catalog_product_entity_media_gallery_value AS mgalleryvalue WHERE entity_id =  '".$productId."' AND value='".$mainImage."' AND mgallery.value_id = mgalleryvalue.value_id AND store_id = 0";		
			$selectMainImageNameRes = $conn->fetchRow($imageNameSql);
			$mainImageDisabled      = $selectMainImageNameRes['disabled'];	
			$label                  = $selectMainImageNameRes['label'];			
			  
		}	    
       return $label; 
  }    
}
