<?php


if ('true' == (string) Mage::getConfig()->getNode('modules/Medialounge_Checkout/active')) {

    class Signifyd_Connect_Model_Authnet_Pure extends Medialounge_Checkout_Model_Authorisednet_Paygate {
        
    }

} else {

    class Signifyd_Connect_Model_Authnet_Pure extends Mage_Paygate_Model_Authorizenet {
        
    }

}

class Signifyd_Connect_Model_Authnet extends Signifyd_Connect_Model_Authnet_Pure
{
    protected function _registercard(varien_object $response, mage_sales_model_order_payment $payment)
    {
        Mage::log(">: ".$response->getTransactionId(), null, 'signifyd_connect.log');
        $card = parent::_registercard($response,$payment);
        $card->setCcAvsResultCode($response->getAvsResultCode());
        $card->setCcResponseCode($response->getCardCodeResponseCode());
        $payment->setCcAvsStatus($response->getAvsResultCode());
        $payment->setCcCidStatus($response->getCardCodeResponseCode());
        $payment->setCcTransId($response->getTransactionId());
        $payment->getCcTransId();
        return $card;
    }
}
