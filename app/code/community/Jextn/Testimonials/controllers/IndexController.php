<?php
class Jextn_Testimonials_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    		
		$this->loadLayout();
		////set Page Title, added by Mahipal S Adhikari on 12-Jun-2013
		$this->getLayout()->getBlock('head')->setTitle(Mage::helper('testimonials')->getTestimonialsTitle());
		$this->getLayout()->getBlock('head')->setKeywords("itshot.com reviews,  itshot.com customer reviews, itshot.com testimonials, itshot.com ratings and reviews");
		$this->getLayout()->getBlock('head')->setDescription("ItsHot.com Reviews - Read customer reviews, ratings and testimonials about our products services and share your experience at ItsHot.com");
		$this->renderLayout();
    }
}
