<?php

class Jextn_Testimonials_Block_Adminhtml_Testimonials_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('testimonials_form', array('legend'=>Mage::helper('testimonials')->__('Item information')));
     
      $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('testimonials')->__('Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));
	  $fieldset->addField('email', 'text', array(
          'label'     => Mage::helper('testimonials')->__('Email'),
          'required'  => false,
          'name'      => 'email',
      ));
	$fieldset->addField('url', 'text', array(
          'label'     => Mage::helper('testimonials')->__('URL'),
          'required'  => false,
          'name'      => 'url',
      ));
      		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('testimonials')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('testimonials')->__('Approved'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('testimonials')->__('Pending'),
              ),
          ),
      ));
       $fieldset->addField('rating', 'select', array(
          'label'     => Mage::helper('testimonials')->__('Rating'),
          'name'      => 'rating',
          'required'  => true,
          'values'    => array(
          array(
                  'value'     => '',
                  'label'     => Mage::helper('testimonials')->__('Select Rating'),
              ),

              array(
                  'value'     => 1,
                  'label'     => Mage::helper('testimonials')->__('1'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('testimonials')->__('2'),
              ),
              array(
                  'value'     => 3,
                  'label'     => Mage::helper('testimonials')->__('3'),
              ),
              array(
                  'value'     => 4,
                  'label'     => Mage::helper('testimonials')->__('4'),
              ),
              array(
                  'value'     => 5,
                  'label'     => Mage::helper('testimonials')->__('5'),
              ),
          ),
      ));
    /* $fieldset->addField('sidebar', 'select', array(
          'label'     => Mage::helper('testimonials')->__('Side bar display'),
          'name'      => 'sidebar',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('testimonials')->__('yes'),
              ),

              array(
                  'value'     => 0,
                  'label'     => Mage::helper('testimonials')->__('No'),
              ),
          ),
      ));     
       */
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('testimonials')->__('Testimonial Content'),
          'title'     => Mage::helper('testimonials')->__('Testimonial Content'),
          'style'     => 'width:700px; height:200px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));

      if ( Mage::getSingleton('adminhtml/session')->getTestimonialsData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getTestimonialsData());
          Mage::getSingleton('adminhtml/session')->setTestimonialsData(null);
      } elseif ( Mage::registry('testimonials_data') ) {
          $form->setValues(Mage::registry('testimonials_data')->getData());
      }
      return parent::_prepareForm();
  }
}
