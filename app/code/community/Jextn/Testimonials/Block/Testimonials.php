<?php
class Jextn_Testimonials_Block_Testimonials extends Mage_Core_Block_Template
{
	public $_itemPerPage = 20;
	public $_pageFrame = 10;
	public $_curPage = 1;
	public function __construct() {
		parent::__construct();

		$collection = Mage::getModel('testimonials/testimonials')->getCollection()
							->addIsActiveFilter()
							->setOrder('created_time','desc');
		$this->setTestimonials($collection);
	}

	public function _prepareLayout()
    {
		//Added By Ankush Garg on 17 Nov 2014 for canonical URL
		if($this->getRequest()->getParam('p')) {
			$TPage = $this->getRequest()->getParam('p');
		}
		else {
			$TPage = 1;
		}
		$this->getLayout()->getBlock('head')->addLinkRel('canonical',$this->getPagerUrl());
		if($TPage >= 2) {
			$this->getLayout()->getBlock('head')->addLinkRel('prev',$this->getPagerUrl().'?p='.($TPage-1));
		}
		if($TPage != ceil(count($this->getTestimonials())/$this->_itemPerPage)) {
			$this->getLayout()->getBlock('head')->addLinkRel('next',$this->getPagerUrl().'?p='.($TPage+1));
		}
		//End
		$pager = $this->getLayout()->createBlock('page/html_pager', 'testimonials.pager');
		$pager->setAvailableLimit(array(1=>1,10=>10,20=>20));
		$pager->setCollection($this->getTestimonials());
		$this->setChild('pager', $pager);
		$this->getTestimonials()->load();
		return $this;
    }
    public function getPagerHtml1()
    {
        return $this->getChildHtml('pager');
    }

    public function getTestimonials()
     {
		//$this->getLayout()->getBlock('head')->setTitle(Mage::helper('testimonials')->getTestimonialsTitle());
		$collection = Mage::getModel('testimonials/testimonials')->getCollection()
							->addIsActiveFilter()
							->setOrder('created_time','desc');
        return $collection;
    }

	public function getSidebarTestimonials()
     {
		$collection = Mage::getModel('testimonials/testimonials')->getCollection()
							//->addSidebarFilter()
							->addIsActiveFilter()
							->setOrder('created_time','desc');
        return $collection;
    }

	public function getFormAction()
	{
		return $this->getUrl('testimonials/submit/post', array('_secure' => true));
	}

	public function getCollection($collection = 'null')
	{
		if($collection != 'null'){
			$page = $this->getRequest()->getParam('p');
			if($page) $this->_curPage = $page;

			$collection->setCurPage($this->_curPage);
			$collection->setPageSize($this->_itemPerPage);
			return $collection;
		}
	}

	public function getPagerHtml($collection = 'null')
	{
		$html = false;
		if($collection == 'null') return;
		if(count($collection) > $this->_itemPerPage)
		{
			$curPage = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
			$pager = (int)(count($collection) / $this->_itemPerPage);
			$count = (count($collection) % $this->_itemPerPage == 0) ? $pager : $pager + 1 ;
			$url = $this->getPagerUrl();
			$start = 1;
			$end = $this->_pageFrame;

			if($curPage>1)
			{
				$html .='<div class="sorter"><a href="'.$url.'?p='.($curPage - 1).'" class="previous i-previous"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="15" viewBox="0 0 98.384 59.337">
                        <path d="M98.384 10.257L88.126 0 49.19 38.936 10.257 0 0 10.257l49.08 49.08.11-.11.112.11"></path>
                    </svg> <span>Previous</span></a></div>';
			}

			$html .= '<div class="pages"><strong>Page:&nbsp;</strong>';
        	$html .= '<ol>';
			if(isset($curPage) && $curPage != 1){
				$start = $curPage - 1;
				$end = $start + $this->_pageFrame;
			}else{
				$end = $start + $this->_pageFrame;
			}
			if($end > $count){
				$start = $count - ($this->_pageFrame-1);
			}else{
				$count = $end-1;
			}

			for($i = $start; $i<=$count; $i++)
			{
				if($i >= 1){
					if($curPage){
						$html .= ($curPage == $i) ? '<li class="current">'. $i .'</li>' : '<li><a href="'.$url.'?p='.$i.'">'. $i .'</a></li>';
					}else{
						$html .= ($i == 1) ? '<li class="current">'. $i .'</li>' : '<li><a href="'.$url.'?p='.$i.'">'. $i .'</a></li>';
					}
				}

			}

			$html .= '</ol></div>';

			if($end<=$pager)
			{
				$html .='<div class="pager"><a href="'.$url.'?p='.($curPage + 1).'" class="next i-next"><span>Next</span> <svg xmlns="http://www.w3.org/2000/svg" width="9" height="15" viewBox="0 0 98.384 59.337">
                    <path d="M98.384 10.257L88.126 0 49.19 38.936 10.257 0 0 10.257l49.08 49.08.11-.11.112.11"></path>
                </svg></a></div>';
			}
		}

		return $html;
	}

	public function getPagerUrl()                            // You need to change this function as per your url.
	{
		$cur_url = mage::helper('core/url')->getCurrentUrl();
		$new_url = preg_replace('/\?p=.*/', '', $cur_url);

		return $new_url;
	}

}
