<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_PointOfSales_Helper_CashRegister extends Mage_Core_Helper_Abstract
{

    public function setCashInitAmount($amount, $method) {

        $pcr = $this->getCashRegisterModel();
        $pcr->setpcr_amount($amount);
        $pcr->setpcr_method($method);
        $pcr->save();
    }

    public function getCashRegisterModel() {

        $model = $this->getCashRegisterOfTheDay();

        if($model !== null)
            return $model;

        $userId = Mage::helper('PointOfSales/User')->getCurrentUser()->getId();
        $storeId = Mage::helper('PointOfSales/User')->getStoreId();

        $model = Mage::getModel('PointOfSales/CashRegister');
        $model->setpcr_date(Mage::getModel('core/date')->date('Y-m-d'));
        $model->setpcr_user_id($userId);
        $model->setpcr_store_id($storeId);

        return $model;
    }

    public function getCashRegisterOfTheDay() {

        $curDate = Mage::getModel('core/date')->date('Y-m-d');
        $userId = Mage::helper('PointOfSales/User')->getCurrentUser()->getId();
        $storeId = Mage::helper('PointOfSales/User')->getStoreId();

       return $this->getCashRegister($curDate, $storeId, $userId);
    }

    public function getCashRegister($date, $storeId, $userId) {

        $to = $date.' 23:59:59';
        $from = $date.' 00:00:00';

        $collection = Mage::getModel('PointOfSales/CashRegister')->getCollection()
            ->addFieldToFilter('pcr_date', array('gteq' => $from))
            ->addFieldToFilter('pcr_date', array('lteq' => $to))
            ->addFieldToFilter('pcr_user_id', array('eq' => $userId))
            ->addFieldToFilter('pcr_store_id', array('eq' => $storeId));

        if(count($collection) > 0)
            return $collection->getFirstItem();

        return null;
    }

}
