<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_PointOfSales_Helper_User extends Mage_Core_Helper_Abstract
{
    private $_user = null;
    private $_store = null;
	private $_currency = null;
	private $_defaultRegion = null;

    public function getCurrentUser()
    {

        if($this->_user !== null)
            return $this->_user;
        
        $userId = Mage::app()->getRequest()->getPost('user_id');

        if(empty($userId))
            $userId = Mage::app()->getRequest()->getParam('user_id');

        if(empty($userId))
            $userId = Mage::getModel('core/session')->getData('user_id');

        if(empty($userId))
        {
            $user = Mage::getSingleton('admin/session');
            $userId = $user->getUser()->getUserId();
        }

        $user = Mage::getModel('admin/user')->load($userId);
        if(!$user->getId())
            throw New Exception ("Unable to load user !");

        Mage::getModel('core/session')->setData('user_id', $userId);

        $this->_user = $user;
        return $this->_user;

            
    }

    public function getUsername()
    {
        return $this->getCurrentUser()->getName();
    }

    public function getUsernameForUserId($userId)
    {
        $users = $this->getUserOptions();
        if(isset($users[$userId]))
            return $users[$userId];

        return '';
    }

    public function getUserList()
    {
        $users = Mage::getModel('admin/user')->getCollection()->setOrder('username','ASC');
        $return = '';
        $return .= '<div class="list-group" id="user_id">';
        foreach($users as $user)
        {
            $url = Mage::helper('adminhtml')->getUrl('adminhtml/PointOfSales_PointOfSales/StepOne', array('user_id'=>$user->getId(), 'store_id'=>$this->getCurrentStoreId()));
            $return .= '<a href="'.$url.'" data-id="'.$user->getId().'" class="list-group-item'.($this->getCurrentUser()->getId() == $user->getId() ? ' active' : '').'">'.$user->getName().'</a>';
        }
        $return .= '</div>';

        return $return;
    }

    public function getUserOptions()
    {
        $users = Mage::getModel('admin/user')->getCollection()->setOrder('username','ASC');
        $return = array();
        foreach($users as $user)
        {
            $return[$user->getId()] = $user->getName();
        }
        return $return;
    }






    public function getCurrentStoreId()
    {

        if($this->_store !== null)
            return $this->_store;

        $storeId = Mage::app()->getRequest()->getPost('store_id');

        if(empty($storeId))
            $storeId = Mage::app()->getRequest()->getParam('store_id');

        if(empty($storeId))
            $storeId = Mage::getModel('core/session')->getData('store_id');

        if(empty($storeId))
        {
            $store = Mage::getSingleton('admin/session');
            $storeId = $store->getUser()->getstore_id();
        }

        if(empty($storeId))
            throw New Exception ("Unable to load store !  Please make sure that you configured the current user POS details in system > permission");

        Mage::getModel('core/session')->setData('store_id', $storeId);

        $this->_store = $storeId;
        return $this->_store;


    }

    public function getStoreList(){
        $users = Mage::getModel('admin/user')->getCollection(); //->setOrder('name','ASC');
        $return = '';
        $return .= '<div class="list-group" id="store_id">';

        foreach (Mage::app()->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $url = Mage::helper('adminhtml')->getUrl('adminhtml/PointOfSales_PointOfSales/StepOne', array('user_id'=>$this->getCurrentUser()->getId(), 'store_id'=>$store->getId()));
                    $return .= '<a href="'.$url.'" data-id="'.$store->getId().'" class="list-group-item'.($this->getCurrentStoreId() == $store->getId() ? ' active' : '').'">'.$website->getName().' > '.$group->getName().' > '.$store->getName().'</a>';
                }
            }
        }

        $return .= '</div>';

        return $return;
    }



	/**
	 * Return currency associated to user
	 *
	 */
	public function getCurrency()
    {
        if ($this->_currency == null)
        {
            //$currencyCode = $this->getCurrentUser()->getcurrency();
            $currencyCode = $this->getStore()->getCurrentCurrencyCode();
            $this->_currency = Mage::getModel('directory/currency')->load($currencyCode);
        }
        return $this->_currency;
    }

	/**
	 * return default shipping method
	 *
	 * @return unknown
	 */
	public function getDefaultShippingMethod()
	{
		return $this->getCurrentUser()->getdefault_shipping_method();
	}

	/**
	 * return default payment method
	 *
	 * @return unknown
	 */
	public function getDefaultPaymentMethod()
	{
		return $this->getCurrentUser()->getdefault_payment_method();
	}

	/**
	 * return customer group
	 *
	 * @return unknown
	 */
	public function getCustomerGroup()
	{
		return $this->getCurrentUser()->getcustomer_group();
	}

	/**
	 * return default country
	 *
	 * @return unknown
	 */
	public function getDefaultCountryId()
	{
		return $this->getCurrentUser()->getcountry_id();
	}

	public function getDefaultCountry()
	{
		return mage::getModel('directory/country')->load($this->getDefaultCountryId());
	}

	public function getDefaultCity()
	{
		return $this->getCurrentUser()->getcity();
	}

	public function getDefaultZip()
	{
		return $this->getCurrentUser()->getpostcode();
	}

	/**
	 * return default region
	 *
	 */
	public function getDefaultRegion()
	{
		if ($this->_defaultRegion == null)
		{
			$regionCode = $this->getCurrentUser()->getregion();

			$country = $this->getDefaultCountry();
			foreach ($country->getRegions() as $item)
			{
				if ($item->getcode() == $regionCode)
				{
					$this->_defaultRegion = $item;
					return $this->_defaultRegion;
				}
			}
		}

		return $this->_defaultRegion;
	}

	/**
	 * return store id
	 *
	 * @return unknown
	 */
	public function getStoreId()
	{
		return $this->getCurrentStoreId();
	}
	
	/**
	 * Return store
	 *
	 * @return unknown
	 */
	public function getStore()
	{
		return mage::getModel('core/store')->load($this->getStoreId());
	}
	
	/**
	 * Return website id
	 *
	 */
	public function getWebsiteId()
	{
		return $this->getStore()->getWebsiteId();
	}
	
	/**
	 * Return product price for POS incl tax
	 *
	 */
	public function getPosProductPriceInclTax($product, $price)
	{
		//init vars
		$helper = mage::helper('tax');
		$customerGroup = mage::getModel('customer/group')->load($this->getCustomerGroup());
		$CustomerTaxClass = $customerGroup->gettax_class_id();
		$store = $this->getStoreId();
		
		//create fake addresses
		$Address = $this->getFakeAddress();
		$priceInclTax = $helper->getPrice($product, $price, true, $Address, $Address, $CustomerTaxClass, $store);
		
		return $priceInclTax;
	}
		
	/**
	 * Return product price for POS
	 *
	 */
	public function getPriceExclTaxFromPriceInclTax($product, $priceInclTax)
	{
		//init vars
		$helper = mage::helper('tax');
		$customerGroup = mage::getModel('customer/group')->load($this->getCustomerGroup());
		$CustomerTaxClass = $customerGroup->gettax_class_id();
		$storeId = $this->getStoreId();
		$store = mage::getModel('core/store')->load($storeId);
		$taxClassId = $product->getTaxClassId();
		        
		//create fake addresses
		$Address = $this->getFakeAddress();
		
		//retrieve tax percent
		$request = Mage::getSingleton('tax/calculation')->getRateRequest($Address, $Address, $CustomerTaxClass, $store);
        $percent = Mage::getSingleton('tax/calculation')->getRate($request->setProductClassId($taxClassId));
        	
        //compute price excl tax from tax rate
        $priceExclTax = $priceInclTax * 100 / (100 + $percent);
        $priceExclTax = number_format($priceExclTax, 4, '.', '');
        
		return $priceExclTax;
	}
	
	/**
	 * Return shipping tax rate
	 *
	 */
	public function getShippingTaxRate()
	{
		$store = mage::helper('PointOfSales/User')->getStore();
		$shippingTaxClassId = mage::getStoreConfig('tax/classes/shipping_tax_class');
		$Address = $this->getFakeAddress();
		$customerGroup = mage::getModel('customer/group')->load($this->getCustomerGroup());
		$CustomerTaxClass = $customerGroup->gettax_class_id();
		$request = Mage::getSingleton('tax/calculation')->getRateRequest($Address, $Address, $CustomerTaxClass, $store);
        $percent = Mage::getSingleton('tax/calculation')->getRate($request->setProductClassId($shippingTaxClassId));
		
		return $percent;
	}
	 
	 
	/**
	 * create a fake address matching to user settings
	 *
	 * @return unknown
	 */
	public function getFakeAddress()
	{
		$session = Mage::getSingleton('core/session');
		$country = $session->getData('customer_country');

		if (empty($country))
			$country = $this->getDefaultCountryId();

		$address = Mage::getModel('sales/order_address');
		$address->setId(null);
		$address->setcountry_id($country);
		$address->setcity($this->getDefaultCity());
		$address->setpostcode($this->getDefaultZip());
		
		if ($this->getDefaultRegion())
			$address->setregion($this->getDefaultRegion()->getId());
		
		return $address;
		
		
	}
	
}
