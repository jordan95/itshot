<?php

class MDN_PointOfSales_Helper_Search extends Mage_Core_Helper_Abstract {

    public function searchProducts($queryString, $limit = 10)
    {
        $filters = $this->getFilters($queryString);

        $storeId = Mage::helper('PointOfSales/User')->getCurrentStoreId();

        $collection = Mage::getModel('catalog/product')
            ->setStoreId($storeId)
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('type_id', array('nin' => array('bundle')))
            ->addAttributeToFilter($filters, null, 'left');

        if(!Mage::getStoreConfig('pointofsales/configuration/enable_configurable_search', $storeId)) {
            $collection->addAttributeToFilter('type_id', array('neq' => 'configurable'));
        }


        $stockIds = $this->getEligibleStockIds();

        $showOutOfStockProduct = Mage::getStoreConfig('pointofsales/configuration/enable_outofstock_product_search', $storeId);

        $collection->joinField('qty',
                        'cataloginventory/stock_item',
                        'qty',
                        'product_id=entity_id',
                        '{{table}}.stock_id in ('.implode(',',$stockIds).') ' . ($showOutOfStockProduct ? '' : 'AND is_in_stock = 1'),
                        'inner');

        $collection->getSelect()
            ->group('entity_id');

        $collection->getSelect()->limit($limit);

        return $collection;
    }

    protected function getFilters($queryString)
    {
        $queryString = '%' .trim($queryString). '%';

        $filters = array(
            array('attribute' => 'sku', 'like' => $queryString),
            array('attribute' => 'name', 'like' => $queryString)
        );

        if (Mage::getStoreConfig('pointofsales/barcode_scanner/enable')) {
            if (Mage::getStoreConfig('pointofsales/barcode_scanner/barcode_attribute')) {
                $filters[] = array('attribute' => Mage::getStoreConfig('pointofsales/barcode_scanner/barcode_attribute'), 'like' => $queryString);
            } elseif (Mage::helper('PointOfSales/Stock')->erpIsInstalled()) {
                $barcodeModel = Mage::getModel('AdvancedStock/ProductBarcode')
                    ->getCollection()
                    ->addFieldToFilter('ppb_barcode', array('like' => $queryString))
                    ->getFirstItem();

                if (!empty($barcodeModel)) {
                    $productId = $barcodeModel->getppb_product_id();
                    $filters[] = array('attribute' => 'entity_id', 'eq' => $productId);
                }
            }
        }

        return $filters;
    }

    protected function getEligibleStockIds()
    {
        $stockIds = array();

        if (!Mage::helper('PointOfSales/Stock')->erpIsInstalled()) {
            $stockIds[] = 1;
        }else {
            $websiteId = Mage::helper('PointOfSales/User')->getWebsiteId();
            if ($websiteId > 0) {
                $collectionAssignments = Mage::getModel('AdvancedStock/Assignment')
                    ->getCollection()
                    ->addFieldToFilter('csa_website_id', $websiteId)
                    ->addFieldToFilter('csa_assignment', MDN_AdvancedStock_Model_Assignment::_assignmentSales);
                foreach ($collectionAssignments as $assignItem) {
                    $stockIds[] = $assignItem->getcsa_stock_id();
                }
            }
        }

        return $stockIds;
    }

}