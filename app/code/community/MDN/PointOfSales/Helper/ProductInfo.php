<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_PointOfSales_Helper_ProductInfo extends Mage_Core_Helper_Abstract
{
    public function getInfoForPopup($productId)
    {
        $product = mage::getModel('catalog/product')->load($productId);

        //create block & return html
        $layout = Mage::getSingleton('core/layout');
        $html = $layout->createBlock('PointOfSales/OrderCreation_ProductInfo')
            ->setTemplate('PointOfSales/OrderCreation/ProductInfo.phtml')
            ->setProduct($product)
            ->toHtml();

        return $html;
    }

    public function cleanTxt($txt)
    {
        return addslashes(str_replace('"', ' ', $txt));
    }

    public function cleanTxtForJson($txt)
    {
        return str_replace('"', ' ', $txt);
    }

    public function getProductToAdd($product, $productNameSuffix = '')
    {

        try {
            if (($product == null) || (!$product->getId()))
                throw new Exception($this->__('Cant find product'));

            //define product information
            $storeId = mage::helper('PointOfSales/User')->getCurrentStoreId();
            $price = $this->getProductPrice($product);
            $taxRate = $this->getTaxRate($product);

            //define prices
            if (Mage::getStoreConfig('tax/calculation/price_includes_tax', $storeId) == 1) {
                $priceInclTax = $price;
                $priceExclTax = $priceInclTax / (1 + ($taxRate / 100));
            } else {
                $priceExclTax = $price;
                $priceInclTax = $price * (1 + ($taxRate / 100));
            }

            //format prices
            $priceExclTax = number_format($priceExclTax, 2, '.', '');
            $taxRate = number_format($taxRate, 4, '.', '');
            $priceInclTax = number_format($priceInclTax, 2, '.', '');
            $productName = $this->cleanTxtForJson($product->getname()) . $productNameSuffix;

            // convert price
            $priceExclTax = Mage::helper('PointOfSales/Currency')->convert($priceExclTax);
            $priceInclTax = Mage::helper('PointOfSales/Currency')->convert($priceInclTax);

            $log = 'Tax rate : '.$taxRate.' , Price Excluding Tax : '.$priceExclTax.' , Price Including tax : '.$priceInclTax;
            Mage::helper('PointOfSales')->addLog($log);


            //return product information
            $result['product_information'] = array();
            $result['product_information']['product_name'] = htmlentities(utf8_decode($productName), ENT_QUOTES);
            $result['product_information']['product_id'] = $product->getId();
            $result['product_information']['skin_url'] = Mage::getDesign()->getSkinUrl('images/OrderWizardCreation/');
            $result['product_information']['price_excl_tax'] = $priceExclTax;
            $result['product_information']['price_incl_tax'] = $priceInclTax;
            $result['product_information']['tax_rate'] = $taxRate;
            $result['product_information']['currency_symbol'] = mage::helper('PointOfSales/User')->getCurrency()->getData('currency_code');

            $result['product_information']['product_sku'] = $product->getSku();
            $result['product_information']['product_type'] = $product->gettype_id();
            $result['product_information']['price_includes_tax'] = Mage::getStoreConfig('tax/calculation/price_includes_tax');

            //additionnal for autocomplete
            $websiteId = mage::helper('PointOfSales/User')->getWebsiteId();
            $result['product_information']['stock'] = mage::helper('PointOfSales/Stock')->getAvailableQuantityForSale($product,$websiteId)."";

            $shelfLocation = '';
            $productStock = mage::getResourceModel('AdvancedStock/MassStockEditor_Collection')
                    ->addFieldToFilter('sku',$product->getSku())
                    ->getFirstItem();
            if($productStock->getId()){
                $shelfLocation = $productStock->getShelfLocation();
            }
            $result['product_information']['shelf_location'] = $shelfLocation;
            try{

                $parentProduct = $this->getConfigurableProduct($product);
                if($parentProduct === null)
                    $result['product_information']['small_image'] = (string) mage::helper('catalog/image')->init($product, 'small_image')->resize(48, 48);
                else{
                    $result['product_information']['small_image'] = (string) mage::helper('catalog/image')->init($parentProduct, 'small_image')->resize(48, 48);
                }
            }
            catch(Exception $e)
            {
                $result['product_information']['small_image'] = '';
            }

            $result['error'] = 0;
            $result['message'] = $this->__('%s added', $productName);
        } catch (Exception $ex) {
            $result['error'] = 1;
            $result['message'] = $ex->getMessage();
        }

        return $result;
    }

    public function getTaxRate($product) {

        $session = Mage::getSingleton('core/session');
        $groupId = $session->getData('customer_group_id');
        $customerId = $session->getData('customer_id');
        $store = mage::helper('PointOfSales/User')->getStore();

        $customerTaxClass = false;

        if($groupId) {
            $customerGroup = Mage::getModel('customer/group')->load($groupId);
            if($customerGroup)
                $customerTaxClass = $customerGroup->getTaxClassId();
        }

        //define tax rate
        /** @var Mage_Tax_Model_Calculation $helper */
        $helper = Mage::getSingleton('tax/calculation');

        if(empty($customerId)) {
            $shippingAddress = mage::helper('PointOfSales/User')->getFakeAddress();
            $billingAddress = $shippingAddress;
        } else {
            /** @var Mage_Customer_Model_Customer $customer */
            $customer = Mage::getModel('Customer/Customer')->load($customerId);
            $billingAddress = $customer->getDefaultBillingAddress();
            $shippingAddress = $customer->getDefaultShippingAddress();
            $helper->setCustomer($customer);
        }

        $request = $helper->getRateRequest($shippingAddress, $billingAddress, $customerTaxClass, $store);
        $request->setProductClassId($product->getTaxClassId());
        $taxRate = $helper->getRate($request);

        $log = 'Add Product to cart : '.$product->getSku(). ' (';
        $log .= 'store='.$store->getId().' group='.$groupId.' customerTaxClass='.$customerTaxClass.' productTaxClass='.$product->getTaxClassId().')';
        Mage::helper('PointOfSales')->addLog($log);

        return $taxRate;
    }

    /**
     * handle price calculation based on product and the customer
     * @param $product Mage_Catalog_Model_Product the product
     */
    public function getProductPrice($product) {

        $storeId = mage::helper('PointOfSales/User')->getCurrentStoreId();
        $session = Mage::getSingleton('core/session');
        $groupId = $session->getData('customer_group_id');

        $product->setCustomerGroupId($groupId);
        $product->setStoreId($storeId);
        /** @var Mage_Catalog_Model_Product_Type_Price $cptp */
        $cptp = Mage::getSingleton('Catalog/Product_Type_Price');
        $price = $cptp->getBasePrice($product, 1);

        return $price;
    }

    private function getParentProduct($product) {

        $type = $product->getTypeID();

        if($type != "simple" && $type != "virtual")
            return null;

        $parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')->getParentIdsByChild($product->getId());

        if(count($parentIds) > 0)
            return Mage::getModel('catalog/product')->load($parentIds[0]);

        return null;
    }

    public function getGroupPrice($product, $groupId) {

        $groupPrices = $product->getData('group_price');

        if (is_null($groupPrices)) {
            $attribute = $product->getResource()->getAttribute('group_price');
            if ($attribute) {
                $attribute->getBackend()->afterLoad($product);
                $groupPrices = $product->getData('group_price');
            }
        }

        if (is_null($groupPrices) || !is_array($groupPrices)) {
            return $product->getPrice();
        }

        $matchedPrice = $product->getPrice();
        foreach ($groupPrices as $groupPrice) {
            if ($groupPrice['cust_group'] == $groupId && $groupPrice['website_price'] < $matchedPrice) {
                $matchedPrice = $groupPrice['website_price'];
                break;
            }
        }

        return $matchedPrice;
    }

    public function getProductAttributeLabelsAsText($product)
    {
        $configurableProduct = $this->getConfigurableProduct($product);
        
        if (!$configurableProduct)
            return;
        
        if ($configurableProduct->gettype_id() != 'configurable')
            return '';
        
        $attributes = $configurableProduct->getTypeInstance(true)->getConfigurableAttributesAsArray($configurableProduct);

        $productNameSuffix = array();

        foreach ($attributes as $attribute) {
            $productValue = $product->getData($attribute['attribute_code']);

            foreach ($attribute['values'] as $option) {
                if ($option['value_index'] == $productValue) {
                    $productNameSuffix[] = $attribute['label'] . ': ' . $option['default_label'];
                    break;
                }
            }

        }

        return ' [' . implode(', ', $productNameSuffix) . ']';
    }

    public function getConfigurableProduct($product)
    {
        $parentIdArray = $this->getProductParentIds($product);
        foreach ($parentIdArray as $parentId) {
            $parent = mage::getModel('catalog/product')->load($parentId);
            return $parent;
        }
        return null;
    }

    public function getProductParentIds($product)
    {
        $productId = null;
        if (is_object($product))
            $productId = $product->getId();
        else
            $productId = $product;
        $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($productId);
        return $parentIds;
    }
}
