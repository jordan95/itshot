<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_PointOfSales_Helper_Customer extends Mage_Core_Helper_Abstract {

    /**
     * Method that create and return customer
     * If customer email already used, only return customer
     *
     * @param unknown_type $data
     */
    public function createCustomer($data) {
        //try to load by customer by email
        $customer = Mage::getModel('customer/customer')
                        ->setWebsiteId(mage::helper('PointOfSales/User')->getWebsiteId())
                        ->loadByEmail($data['customer_email']);

        //create customer if doesn't exist
        if (!$customer->getId()) {
            $customer = mage::getModel('customer/customer')
                            ->setfirstname($data['customer_firstname'])
                            ->setlastname($data['customer_lastname'])
                            ->setemail($data['customer_email'])
                            ->setstore_id(mage::helper('PointOfSales/User')->getStoreId())
                            ->setgroup_id($data['group'])
                            ->setwebsite_id(mage::helper('PointOfSales/User')->getWebsiteId())
                            ->save();
            
            $this->createAddress($data,$customer->getId());

        } else {
            throw new Exception($this->__('Customer email already used !'));
        }

        return $customer;
    }

    /**
     * get all the regions of a specific country or of the current user
     * @param $name string id of the select
     * @param $countryCode string the selected country code
     * @return string html select with all the regions
     */
    public function getRegionAsCombo($name, $countryCode = null) {

        $regionCode = null;

        if($countryCode == null)
            $country = mage::helper('PointOfSales/User')->getDefaultCountry();
        else
            $country = Mage::getModel('directory/country')->loadByCode($countryCode);

        //if the country selected is the same than the current user
        //select the same region than him by default
        if($country->getCountryId() == mage::helper('PointOfSales/User')->getDefaultCountry()->getCountryId())
            $regionCode = Mage::getSingleton('admin/session')->getUser()->getregion();

        $regions = $country->getRegions();

        $html = '<select name="'.$name.'" id="'.$name.'" class="form-control">';

        foreach($regions as $region) {
            $selected = '';
            if ($regionCode != null && $region->getcode() == $regionCode)
                $selected = ' selected ';
            $html .= '<option value="' . $region->getId() . '" ' . $selected . '>' . $region->getName() . '</option>';
        }
        $html .= '</select>';

        return $html;
    }

    public function setCustomerInfo($country = null, $groupId = null, $customerId = null ) {

        if(empty($country) || $country == -1)
            $country = Mage::helper('PointOfSales/User')->getDefaultCountry()->getcountry_code();

        if(empty($groupId) || $groupId == -1)
            $groupId = Mage::helper('PointOfSales/User')->getCustomerGroup();

        $session = Mage::getSingleton('core/session');
        $session->setData('customer_country', $country);
        $session->setData('customer_group_id', $groupId);
        $session->setData('customer_id', $customerId > 0 ? $customerId : null);
    }

    /**
     * Add customer to newsletter subscription
     */
    public function newsletterSubscription($id){

        $customer = Mage::getModel('customer/customer')->load($id);

        $newsletter = Mage::getModel('newsletter/subscriber')->getCollection()
                ->addFieldToFilter('subscriber_email', $customer->getemail());

        if($newsletter->count() == 0){

            $newsletter = Mage::getModel('newsletter/subscriber')
                            ->setcustomer_id($customer->getid())
                            ->setstore_id(mage::helper('PointOfSales/User')->getStoreId())
                            ->setsubscriber_email($customer->getemail())
                            ->setsubscriber_status(1)
                            ->save();

        } else {
            $newsletter = $newsletter->getFirstItem();
            $newsletter->setsubscriber_status(1)->save();
        }

    }

    public function newsletterUnsubscription($id){

        $customer = Mage::getModel('customer/customer')->load($id);

        $newsletter = Mage::getModel('newsletter/subscriber')->getCollection()
            ->addFieldToFilter('subscriber_email', $customer->getemail())
            ->getFirstItem();

        if($newsletter && $newsletter->getId() > 0){
            $newsletter->setsubscriber_status(3)->save();
        }

    }

    /**
     * Create customer address
     * @param <type> $data
     */
    public function createAddress($data, $customerId)
    {
        
        $regionName = mage::getModel('directory/region')->load($data['region'])->getName();

        //create address
        $model = mage::getModel('customer/address')
                    ->setCustomerId($customerId)
                    ->setfirstname($data['customer_firstname'])
                    ->setlastname($data['customer_lastname'])
                    ->setcountry_id($data['country'])
                    ->setregion($regionName)
                    ->setregionid($data['region'])
                    ->setpostcode($data['zip'])
                    ->setcity($data['city'])
                    ->setstreet($data['address'])
                    ->settelephone($data['phone'])
                    //->setfax($data['fax'])
                    ->setcompany($data['customer_company'])
                    ->setIsDefaultShipping()
                    ->setIsDefaultBilling()
                    ->save();

        $customer = mage::getModel('customer/customer')->load($customerId);
        $customer->setDefaultBilling($model->getId());
        $customer->setDefaultShipping($model->getId());
        $customer->save();

    }

    public function searchCustomer($term) {

        $websiteId = Mage::helper('PointOfSales/User')->getWebsiteId();
        $collection = mage::getModel('customer/customer')->getCollection()
            ->addAttributeToFilter(array(
                array('attribute' => 'firstname', 'like' => "%$term%"),
                array('attribute' => 'lastname', 'like' => "%$term%"),
                array('attribute' => 'email', 'like' => "%$term%"),
            ));
        $collection->getSelect()->orwhere("CONCAT(at_firstname.value,' ',at_lastname.value) LIKE '%$term%'");
		$collection->getSelect()->orwhere("CONCAT(at_lastname.value,' ',at_firstname.value) LIKE '%$term%'");
        if(mage::getStoreConfig('pointofsales/configuration/customer_filter_website', mage::helper('PointOfSales/User')->getCurrentStoreId()))
            $collection->addAttributeToFilter('website_id', array('eq' => $websiteId));

        $return = array();
        foreach ($collection as $customer) {
            $return[] = array(
				'id' => $customer->getId(),
				'email' => $customer->getEmail(),
				'firstname' => $customer->getFirstname(),
				'lastname' => $customer->getLastname(),
			);
        }
        return $return;
    }
	public function searchCustomerByID($customerId){
		$tmp = $this->getCustomerInfo($customerId);
		if(!empty($tmp)){
			$newsletter = Mage::getModel('newsletter/subscriber')->getCollection()
				->addFieldToFilter('subscriber_email', $tmp['email'])
				->getFirstItem();

			if($newsletter && $newsletter->getId() > 0) {

				$status = $newsletter->getsubscriber_status();
				if($status === '1')
					$tmp['newsletter'] = '1';
				else
					$tmp['newsletter'] = '0';
			}
		}
		return $tmp;
	}
    public function getCustomerInfo($customerId) {

        $customer = mage::getModel('customer/customer')->load($customerId);

        $infos = array();
        $infos['id'] = $customer->getId();
        $infos['email'] = $customer->getData('email');
        $infos['firstname'] = $customer->getData('firstname');
        $infos['lastname'] = $customer->getData('lastname');

        $address = Mage::getModel('customer/address')->load($customer->getDefaultBilling());
        $infos['street'] = $address->getstreet();
        $infos['city'] = $address->getcity()?$address->getcity():Mage::helper('PointOfSales/User')->getDefaultCity();
        $infos['zip'] = $address->getpostcode()?$address->getpostcode():Mage::helper('PointOfSales/User')->getDefaultZip();
        $infos['country_id'] = $address->getcountry_id()?$address->getcountry_id():Mage::helper('PointOfSales/User')->getDefaultCountryId();
        $infos['region_id'] = $address->getregion_id()?$address->getregion_id():Mage::helper('PointOfSales/User')->getDefaultRegion()->getRegionId();
        $infos['phone'] = $address->gettelephone();
        $infos['group'] = $customer->getGroupId();
        if (Mage::helper('PointOfSales')->checkRewardPointModule()):
            $infos['pos_reward_points'] = $customer->storeCreditBalance();
            $infos['pos_reward_credits'] = Mage::helper('core')->currency(Mage::helper("posrewardpoints")->creditsToMoney($infos['pos_reward_points']), true, false);
        endif;

        return $infos;
    }

    public function updateInformation($data) {
        $customerId = $data['customer_id'];

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = mage::getModel('customer/customer')->load($customerId);

        if(!$customer || $customer->getId() <= 0)
            return;

        /** @var Mage_Customer_Model_Address $address */
        $address = $customer->getDefaultBillingAddress();

        if(!$address || $address->getId() <= 0) {
            $address = Mage::getModel('customer/address')
                ->setCustomerId($customerId)
                ->setIsDefaultBilling(true);
        }

        $fields = array(
            'setfirstname' => 'customer_firstname',
            'setlastname' => 'customer_lastname',
            'setcountry_id' => 'country',
            'setpostcode' => 'zip',
            'setcity' => 'city',
            'setstreet' => 'address',
            'settelephone' => 'phone',
            'setcompany' => 'customer_company',
            'setregionid' => 'region'
        );

        foreach ($fields as $method => $field) {
            if(!isset($data[$field]))
                continue;

            $address->$method($data[$field]);
        }

        $address->save();

        $customer->setemail($data['customer_email']);
        $customer->setfirstname($data['customer_firstname']);
        $customer->setlastname($data['customer_lastname']);
        $customer->setData('group_id', $data['group']);
        $customer->save();

        if (!isset($data['newsletter'])) {
            $this->newsletterUnsubscription($customerId);
        }
        else {
            if($data['newsletter']) {
                $this->newsletterSubscription($customerId);
            } else {
                $this->newsletterUnsubscription($customerId);
            }
        }

    }
    
}
