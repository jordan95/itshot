<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_PointOfSales_Helper_Payment extends Mage_Core_Helper_Abstract
{
    public function getMultiplePaymentsAsText($order, $separator = '<br />')
    {
        $collection = Mage::getModel('PaymentTracker/Payment')
            ->getCollection()
            ->addFieldToFilter('ptp_order_id',$order->getId());

        $return = '';
        foreach($collection as $payment)
        {
            $return .= $payment->getptp_method().': '.$order->formatPrice($payment->getptp_amount()).$separator;
        }

        return $return;
    }

    public function getMultiplePayments($order)
    {
        return Mage::getModel('PaymentTracker/Payment')
            ->getCollection()
            ->addFieldToFilter('ptp_order_id',$order->getId());
    }
}
