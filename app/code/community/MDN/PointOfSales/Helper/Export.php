<?php

class MDN_PointOfSales_Helper_Export extends Mage_Core_Helper_Abstract {

    const kFormatCsv = 'csv';
    const kFormatPdf = 'pdf';
    const kFormatExcel = 'excel';

    public function getPayments($from, $to, $storeIds)
    {
        $to .= ' 23:59:59';
        $from .= ' 00:00:00';

        $collection = Mage::getModel('PaymentTracker/Payment')
                            ->getCollection()
                            ->addFieldToFilter('ptp_date', array('gteq' => $from))
                            ->addFieldToFilter('ptp_date', array('lteq' => $to))
                            ->join('sales/order', 'entity_id = ptp_order_id')
                            ->addFieldToFilter('store_id', array('in' => $storeIds));


        return $collection;
    }

    public function getContent($from, $to, $storeIds, $format)
    {
        $payments = $this->getPayments($from, $to, $storeIds);
        $cashRegisterInit = null;

        if($from == $to)
            $cashRegisterInit = Mage::helper('PointOfSales/CashRegister')->getCashRegister(
                $from,
                $storeIds[0],
                Mage::helper('PointOfSales/User')->getCurrentUser()->getId()
            );

        switch($format)
        {
            case self::kFormatPdf:
                return Mage::getModel('PointOfSales/Pdf_SalesReport')->getPdf(array($payments), $cashRegisterInit)->render();
                break;
            case self::kFormatCsv:
            default:
                return $this->buildCsv($payments, $cashRegisterInit);
                break;
        }
    }

    public function buildCsv($payments, $cashRegisterInit)
    {
        $separator = ',';
        $newLine = "\r\n";

        $header = array('Date', 'Order', 'Customer', 'Payment Method', 'Value', 'Currency');
        $content = implode($separator, $header).$newLine;

        if ($cashRegisterInit !== null) {
            $fields = array();

            $fields[] = $cashRegisterInit->getpcr_date();
            $fields[] = '-';
            $fields[] = $this->__('Cash Register at start');
            $fields[] = $cashRegisterInit->getpcr_method();
            $fields[] = $cashRegisterInit->getpcr_amount();
            $fields[] = Mage::helper('PointOfSales/User')->getStore()->getCurrentCurrencyCode();

            $content .= implode($separator, $fields) . $newLine;
        }

        foreach($payments as $payment)
        {
            $fields = array();

            $fields[] = $payment->getptp_date();
            $fields[] = $payment->getincrement_id();
            $fields[] = $payment->getcustomer_firstname().' '.$payment->getcustomer_lastname();
            $fields[] = $payment->getptp_method();
            $fields[] = number_format($payment->getptp_amount(), 2);
            $fields[] = $payment->getorder_currency_code();

            $content .= implode($separator, $fields).$newLine;
        }

        return $content;
    }

}
