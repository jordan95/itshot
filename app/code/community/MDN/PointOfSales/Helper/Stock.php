<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_PointOfSales_Helper_Stock extends Mage_Core_Helper_Abstract
{

    public function getAvailableQuantityForSale($product, $websiteId = 0)
    {
        if (!$websiteId)
            $websiteId = $this->getWebsiteId();

        if ($this->erpIsInstalled())
        {
            return Mage::helper('AdvancedStock/Website')->getAvailableQtyForSale($websiteId, $product->getentity_id());
        }
        else
        {
            $stockItem = mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getentity_id());
            if ($stockItem) {
                if ($stockItem->getManageStock()) {
                    return (int)$stockItem->getQty();
                }
            }
        }

        return "";
    }

    public function getWebsiteId()
    {
        return mage::helper('PointOfSales/User')->getWebsiteId();
    }

    public function erpIsInstalled()
    {
        return (Mage::getStoreConfig('advancedstock/erp/is_installed') == 1);
    }
}
