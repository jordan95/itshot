<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class MDN_PointOfSales_Helper_Order extends MDN_PaymentTracker_Helper_Order {

    public function getTotalPaid($order)
    {
        $tableName = Mage::getConfig()->getTablePrefix().'payment_tracker_payment';
        $sql = 'select SUM(ptp_amount) from '.$tableName.' where ptp_order_id = '.$order->getId(). ' AND (ptp_amount > 0 OR ptp_is_money_back = 1)';
        $value = mage::getResourceModel('sales/order_item_collection')->getConnection()->fetchOne($sql);
        if (!$value)
            $value = 0;
        return $value;
    }

}