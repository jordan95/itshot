<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *refund
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author     : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_PointOfSales_Helper_Creditmemo extends Mage_Core_Helper_Abstract
{
    public function CreateCreditmemo($object)
    {

        if (!$this->hasProducts($object)) {
            Mage::throwException($this->__('Can not do credit memo without product'));
        }
        $groupedProducts    = $this->getGroupedProductsByInvoices($object);
        $createdCreditmemos = array();
        $isOnline           = false;
        if ($object['refund_online']) {
            $isOnline = true;
        }

        foreach ($groupedProducts as $invoiceId => $data) {
            $creditmemo = $this->createInvoiceCreditmemo($invoiceId, $data, $object, $isOnline);
            $createdCreditmemos[] = $creditmemo;
        }
        return $createdCreditmemos;
    }
    /**
     * Main function, create credit memo using data
     *
     * @param      $invoiceId
     * @param      $rmaData
     * @param bool $isOnline
     *
     * @return
     * @internal param \unknown_type $object
     */
    protected function createInvoiceCreditmemo($invoiceId, $rmaData, $object, $isOnline = false)
    {
        $invoice = mage::getModel('sales/order_invoice')->load($invoiceId);
        if (!$invoice->getId()) {
            Mage::throwException($this->__('Unable to load sales invoice (%s)', $invoiceId));
        }
        $order = $invoice->getOrder();
        if (!$order->canCreditmemo()) {
            Mage::throwException($this->__('Can not do credit memo for order'));
        }
        //init credit memo
        $convertor        = Mage::getModel('sales/convert_order');
        $creditmemo       = $convertor->toCreditmemo($order)->setInvoice($invoice);
        $adjustmentRefund = 0;
        // add items
        if (isset($rmaData['items'])) {
            foreach ($rmaData['items'] as $itemData) {
                $orderItem = $itemData['item'];
                $orderItem  = $order->getItemById($orderItem->getitem_id());
                $qty       = $itemData['qty'];
                if ($qty == 0) {
                    if ($orderItem->isDummy()) {
                        if ($orderItem->getParentItem() && ($qty > 0)) {
                            /* 
                             * this part of code will never run, bacaouse of conditions: $qty==0 && $qty > 0 !!! 
                             */
                            $parentItemNewQty  = $this->getQtyForOrderItemId($object, $orderItem->getParentItem()->getId());
                            $parentItemOrigQty = $orderItem->getParentItem()->getQtyOrdered();
                            $itemOrigQty       = $orderItem->getQtyOrdered() / $parentItemOrigQty;
                            $qty               = $itemOrigQty * $parentItemNewQty;
                        }
                    }
                }
                $item = $convertor->itemToCreditmemoItem($orderItem);
                $item->setQty($qty);
                //customize price if partial refund
                $price = $this->getPriceForOrderItemId($object, $orderItem->getId());
                if ($price > 0) {
                    $adjustmentRefund += $item->getPrice() - $price;
                }

                $creditmemo->addItem($item);
            }
        }

        $rmaData['refund'] += $adjustmentRefund;
        //refund shipping fees
        if (isset($rmaData['refund_shipping_amount'])) {
            if ($order->getshipping_tax_amount() > 0)
                $shippingTaxCoef = $order->getshipping_amount() / $order->getshipping_tax_amount();
            else
                $shippingTaxCoef = 0;
            $refundAmountInclTax = $rmaData['refund_shipping_amount'];
            if ($shippingTaxCoef > 0 )
                $refundAmountTax = $refundAmountInclTax / $shippingTaxCoef;
            else
                $refundAmountTax = 0;
            $refundAmountExclTax = $refundAmountInclTax - $refundAmountTax;
            $creditmemo->setShippingAmount($refundAmountInclTax);
            $creditmemo->setBaseShippingAmount($refundAmountInclTax);
        } else {
            $creditmemo->setBaseShippingAmount(0.00);
        }
        // print_r($creditmemo->getInvoice()->getData());
        // die('dasdasd');
        //manage adjustement
        $creditmemo->setAdjustmentPositive($rmaData['fee']);
        $creditmemo->setAdjustmentNegative($rmaData['refund']);


        $creditmemo->setRefundRequested(true);
        $creditmemo->setOfflineRequested(!$isOnline);
        $creditmemo->collectTotals();
        $creditmemo->register();

        $transactionSave = Mage::getModel('core/resource_transaction')
            ->addObject($creditmemo)
            ->addObject($creditmemo->getOrder())
            ->addObject($creditmemo->getInvoice());
        $transactionSave->save();

        $this->createStockMovements($creditmemo);

        return $creditmemo;
    }

    protected function createStockMovements($creditmemo)
    {
        if(!mage::helper('PointOfSales/Stock')->erpIsInstalled())
            return;

        $storeId = mage::helper('PointOfSales/User')->getCurrentStoreId();
        $websiteId = Mage::getModel('core/store')->load($storeId)->getWebsiteId();
        $targetWarehouseId = mage::helper('AdvancedStock/Warehouse')->getWarehouseForAssignment($websiteId, MDN_AdvancedStock_Model_Assignment::_assignmentSales)->getId();

        foreach($creditmemo->getAllItems() as $item)
        {
            $productId = $item->getproduct_id();
            $sourceWarehouseId = null;
            $qty = $item->getqty();
            $additionalData = array('sm_type' => 'creditmemo');
            mage::getModel('AdvancedStock/StockMovement')->createStockMovement($productId, $sourceWarehouseId, $targetWarehouseId, $qty, 'Creditmemo #' . $creditmemo->getIncrementId(), $additionalData);
        }
    }

    /**
     * Return products gropued by invoices
     *
     * @param unknown_type $object
     *
     * @return array
     */
    protected function getGroupedProductsByInvoices($object)
    {
        $order         = Mage::getModel('sales/order')->load($object['order_id']);
        $orderInvoices = $order->getInvoiceCollection();
        $result = array();
        foreach ($order->getAllItems() as $orderItem) {
            if (!$orderItem->isDummy() && !$orderItem->getQtyToRefund()) {
                continue;
            }
            $qty = $this->getQtyForOrderItemId($object, $orderItem->getId());
            if ($qty == 0) {
                continue;
            }
            $remainingQty = $qty;
            foreach ($orderInvoices as $invoice) {
                if (!$remainingQty) {
                    continue;
                }
                // refund shipping fees  for first invoice
                if (isset($object['refund_shipping_fees']) && $object['refund_shipping_fees']) {
                    $result[$invoice->getId()]['refund_shipping_amount'] = $object['refund_shipping_amount'];
                    unset($object['refund_shipping_fees']); // dont refund shipping fore next invoices
                }
                $invoiceQtyRefundLimit = $this->getInvoiceQtysRefundLimits($orderItem, $invoice);
                if ($invoiceQtyRefundLimit) {
                    $invoiceRefundQty                     = min($invoiceQtyRefundLimit, $remainingQty);
                    $result[$invoice->getId()]['items'][] = array(
                        'item' => $orderItem,
                        'qty'  => $invoiceRefundQty
                    );
                    $result[$invoice->getId()]['refund']  = $object['refund'];
                    $result[$invoice->getId()]['fee']     = $object['fee'];
                    $remainingQty -= max($invoiceRefundQty, 0);
                }
            }
            if ($remainingQty) {
                Mage::throwException($this->__("Cannot refund qty %s of %s", $qty, $orderItem->getName()));
            }
        }
        return $result;
    }
    /**
     * Return qty refund limit for given invoice
     *
     * @param order_item
     * @param invoice
     *
     * @return qty
     */
    protected function getInvoiceQtysRefundLimits($orderItem, $invoice)
    {
        $invoiceItem = $this->getInvoiceItemForOrderItem($orderItem, $invoice);
        if (!$invoiceItem) {
            return 0;
        }
        $invoiceQtyRefunded = $this->getInvoiceQtysRefunded($orderItem, $invoice);
        return $invoiceItem->getQty() - $invoiceQtyRefunded;
    }
    /**
     * Return invice_item for given order_item
     *
     * @param order_item
     * @param invoice
     *
     * @return invoice_item or false
     */
    protected function getInvoiceItemForOrderItem($orderItem, $invoice)
    {
        foreach ($invoice->getAllItems() as $invoiceItem) {
            if ($invoiceItem->getOrderItemId() == $orderItem->getId()) {
                return $invoiceItem;
            }
        }
        return false;
    }
    /**
     * Return already refunded item for given invoice
     *
     * @param order_item
     * @param invoice
     *
     * @return qty
     */
    protected function getInvoiceQtysRefunded($orderItem, $invoice)
    {
        $qtyRefunded = 0;
        foreach ($invoice->getOrder()->getCreditmemosCollection() as $createdCreditmemo) {
            if ($createdCreditmemo->getState() != Mage_Sales_Model_Order_Creditmemo::STATE_CANCELED
                && $createdCreditmemo->getInvoiceId() == $invoice->getId()
            ) {
                foreach ($createdCreditmemo->getAllItems() as $createdCreditmemoItem) {
                    $orderItemId = $createdCreditmemoItem->getOrderItem()->getId();
                    if ($orderItem->getId() == $orderItemId) {
                        $qtyRefunded += $createdCreditmemoItem->getQty();
                    }
                }
            }
        }
        return $qtyRefunded;
    }
    /**
     * Check if credit memo request contains products
     *
     * @param unknown_type $object
     *
     * @return unknown
     */
    protected function hasProducts($object)
    {
        $retour = false;
        foreach ($object['products'] as $item) {
            if ($item['qty'] > 0)
                $retour = true;
        }
        return $retour;
    }
    /**
     * Return qty to refund for productid
     *
     * @param unknown_type $object
     * @param              $productId
     *
     * @return int
     */
    protected function getQtyForOrderItemId($object, $orderItemId)
    {
        $retour   = 0;
        $products = $object['products'];
        foreach ($products as $key => $value) {
            if ($value['order_item_id'] == $orderItemId)
                $retour = $value['qty'];
        }
        return $retour;
    }
    /**
     * Return qty to refund for productid
     *
     * @param unknown_type $object
     * @param              $productId
     *
     * @return int
     */
    protected function getPriceForOrderItemId($object, $orderItemId)
    {
        $retour   = 0;
        $products = $object['products'];
        foreach ($products as $key => $value) {
            if ($value['order_item_id'] == $orderItemId)
                $retour = $value['price'];
        }
        return $retour;
    }
}