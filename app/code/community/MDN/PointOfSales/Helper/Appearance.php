<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_PointOfSales_Helper_Appearance extends Mage_Core_Helper_Abstract
{
    public function getButtonColor() {
        return $this->getConfig('button_color');
    }

    public function getCreateOrderColor() {
        return $this->getConfig('create_order_color');
    }

    public function getHeaderColor() {
        return $this->getConfig('header_color');
    }

    public function getNavbarColor() {
        return $this->getConfig('navbar_color');
    }

    private function getConfig($configName) {
        $storeId = Mage::helper('PointOfSales/User')->getCurrentStoreId();
        return Mage::getStoreConfig('pointofsales/appearance/'.$configName, $storeId);
    }


}
