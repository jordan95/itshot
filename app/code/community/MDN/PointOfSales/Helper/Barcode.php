<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_PointOfSales_Helper_Barcode extends Mage_Core_Helper_Abstract
{
    public function getBarcodeForProduct($product)
    {
        if(mage::helper('PointOfSales/Stock')->erpIsInstalled())
            return mage::helper('AdvancedStock/Product_Barcode')->getBarcodeForProduct($product);

        $barcodeAttributeCode = mage::getStoreConfig('pointofsales/barcode_scanner/barcode_attribute');
        if ($barcodeAttributeCode == '')
                throw new Exception($this->__('Barcode attribute not set in Sytem > Configuration > Point of sales'));

        return $product->getData($barcodeAttributeCode);
        
    }

    public function getProductFromBarcode($barcode)
    {
        $storeId = Mage::helper('PointOfSales/User')->getCurrentStoreId();
        $barcodeAttributeCode = Mage::getStoreConfig('pointofsales/barcode_scanner/barcode_attribute', $storeId);

        if( empty($barcodeAttributeCode) && Mage::helper('PointOfSales/Stock')->erpIsInstalled()) {
            return Mage::helper('AdvancedStock/Product_Barcode')->getProductFromBarcode($barcode);
        }

        if (empty($barcodeAttributeCode)) {
            throw new Exception($this->__('Barcode attribute not set in Sytem > Configuration > Point of sales'));
        }

        $product = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter($barcodeAttributeCode, $barcode)
            ->getFirstItem();

        return $product;
    }
}
