<?php

class MDN_PointOfSales_Helper_PosDisplay extends Mage_Core_Helper_Abstract
{

    public function getCode($storeId = "", $userId = "")
    {
        if (!$storeId)
            $storeId = Mage::helper('PointOfSales/User')->getCurrentStoreId();
        if (!$userId)
            $userId = Mage::helper('PointOfSales/User')->getCurrentUser()->getId();

        $code = "";

        $code .= chr(65 + $storeId);
        $code .= chr(83 - $userId);
        $code .= chr(65 + $userId * 2);
        $code .= chr(72 + $userId - $storeId);

        return $code;
    }

    public function getUpdateUrl()
    {
        $code = $this->getCode();
        $url = Mage::getBaseUrl();
        $t = parse_url($url);
        return $t['scheme']."://".$t['host']."/pos_display/update.php?code=".$code;
    }

    public function getUrl()
    {
        $code = $this->getCode();
        $url = Mage::getBaseUrl();
        $t = parse_url($url);
        return $t['scheme']."://".$t['host']."/pos_display/index.php?code=".$code;
    }

}