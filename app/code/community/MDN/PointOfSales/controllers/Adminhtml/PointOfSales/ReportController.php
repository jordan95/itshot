<?php

require_once Mage::getModuleDir('controllers', 'MDN_SmartReport').DS.'Adminhtml'.DS.'SmartReport'.DS.'ReportsController.php';

class MDN_PointOfSales_Adminhtml_PointOfSales_ReportController extends MDN_SmartReport_Adminhtml_SmartReport_ReportsController
{

    public function MainAction()
    {
        $this->loadLayout();

        $this->getLayout()->getBlock('root')->setTemplate('PointOfSales/Report/Container.phtml');

        $block = $this->getLayout()->createBlock('PointOfSales/Report_Tabs');

        $this->getLayout()->getBlock('content')->append($block);

        $blockJq = $this->getLayout()->createBlock('core/template')->setTemplate('SmartReport/Jquery.phtml');
        $this->getLayout()->getBlock('content')->append($blockJq);
        $this->getLayout()->getBlock('head')->addCss('lib/prototype/windows/themes/magento.css');
        $this->getLayout()->getBlock('head')->addJs('mdn/SmartReport/Main.js');
        $this->getLayout()->getBlock('head')->addItem('js_css', 'prototype/windows/themes/default.css');
        $this->getLayout()->getBlock('head')->addItem('js_css', 'prototype/windows/themes/magento.css');


        $this->renderLayout();
    }

    public function printZreportReceiptAction()
    {
        //create pdf and download
        $pdf = Mage::getModel('PointOfSales/Pdf_ZreportReceipt')->initFromRegistry()->getPdf(array());

        $name = 'zreport_'.date('Ymd').'.pdf';
        $this->_prepareDownloadResponse($name, $pdf->render(), 'application/pdf');
    }

    public function printZreportTodayReceiptAction()
    {
        $pdf = Mage::getModel('PointOfSales/Pdf_ZreportReceipt');

        $pdf->_from = date('Y-m-d')." 00:00:00";
        $pdf->_to = date('Y-m-d')." 23:59:59";
        $pdf->_storeIds = mage::helper('PointOfSales/User')->getStoreId();

        $pdf = $pdf->getPdf(array());
        $name = 'zreport_'.date('Ymd').'.pdf';
        $this->_prepareDownloadResponse($name, $pdf->render(), 'application/pdf');
    }

    public function printZreportA4Action()
    {
        //create pdf and download
        $pdf = Mage::getModel('PointOfSales/Pdf_ZreportA4')->getPdf(array());

        $name = 'zreport_'.date('Ymd').'.pdf';
        $this->_prepareDownloadResponse($name, $pdf->render(), 'application/pdf');
    }

    protected function _isAllowed()
    {
        return true;
    }

}