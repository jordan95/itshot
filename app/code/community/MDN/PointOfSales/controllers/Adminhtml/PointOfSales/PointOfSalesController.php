<?php

class MDN_PointOfSales_Adminhtml_PointOfSales_PointOfSalesController extends Mage_Adminhtml_Controller_Action
{

    private $_OrderCreationData = null;

    /**
     * First step to create an order
     *
     */
    public function StepOneAction()
    {
        try {
            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setTitle($this->__('POS'));
            $this->renderLayout();
            Mage::helper('PointOfSales/Customer')->setCustomerInfo();
        } catch (Exception $ex) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('An error occured : %s', $ex->getMessage()));
            $this->_redirect('adminhtml/dashboard');
        }
    }

    /*
     * Return Billing adresse for existing customer
     *
     */

    public function customerBillingAdressAction()
    {

        $customer_id = $this->getRequest()->getPost('customer_id');

        $customer = Mage::getModel('customer/customer')->load($customer_id);
        $customer_adresse = $customer->getDefaultBillingAddress();

        //retourne
        $response = array(
            'street' => ($customer_adresse->getstreet()),
            'city' => $this->__($customer_adresse->getcity()),
            'zip' => ($customer_adresse->getpostcode()),
            'country_id' => ($customer_adresse->getcountry_id()),
            'phone' => ($customer_adresse->gettelephone()),
            'fax' => ($customer_adresse->getfax())
        );


        $response = Zend_Json::encode($response);
        $this->getResponse()->setBody($response);
    }

    /**
     * Return payment form depending of the payment method
     *
     */
    public function regionAsComboAction()
    {
        try {
            $countryCode = $this->getRequest()->getParam('country_code');
            $html = Mage::helper('PointOfSales/Customer')->getRegionAsCombo('region', $countryCode);
        } catch (Exception $ex) {
            $html = $this->__('An error occured : %s', $ex->getMessage());
        }

        //return html
        $this->getResponse()->setBody($html);
    }


    /**
     * Return customer grid to search existing customer
     *
     */
    public function customerGridAction()
    {
        $this->loadLayout();
        $Block = $this->getLayout()->createBlock('PointOfSales/OrderCreation_CustomerGrid');
        $this->getResponse()->setBody($Block->toHtml());
    }

    /**
     * Return products grid
     *
     */
    public function productsGridAction()
    {
        $this->loadLayout();
        $Block = $this->getLayout()->createBlock('PointOfSales/OrderCreation_ProductsGrid');
        $this->getResponse()->setBody($Block->toHtml());
    }

    /**
     * order view modal
     *
     */
    public function OrderViewAction()
    {
        $this->loadLayout();
        $this->getLayout()->removeOutputBlock('root')->addOutputBlock('content');
        $this->renderLayout();
        return;
    }

    /**
     * Print invoice
     *
     */
    public function printInvoiceAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $order = mage::getModel('sales/order')->load($orderId);
        if ($order->getId()) {
            $this->printInvoice($order);
        } else
            die('Unable to load order');
    }

    /**
     * Print shipment
     *
     */
    public function printShipmentAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $order = mage::getModel('sales/order')->load($orderId);
        if ($order->getId()) {
            $this->printShipment($order);
        } else
            die('Unable to load order');
    }

    public function printOrderAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $order = mage::getModel('sales/order')->load($orderId);
        if ($order->getId()) {
            $this->printOrder($order);
        } else
            die('Unable to load order');
    }

    /*     * ******************************************************************************************************************************
     * ********************************************************************************************************************************
     * ** Shipping & Payment method
     * ********************************************************************************************************************************
     * ******************************************************************************************************************************* */

    /**
     * Return payment form depending of the payment method
     *
     */
    public function PaymentFormAction()
    {
        $html = '';

        try {
            $paymentMethodCode = $this->getRequest()->getParam('payment_method');
            $paymentMethod = '';
            $formBlockType = $paymentMethod->getFormBlockType();
            $formBlock = $this->getLayout()->createBlock($formBlockType);

            //return form in json
            $html = $formBlock->toHtml();
        } catch (Exception $ex) {
            $html = $this->__('An error occured : %s', $ex->getMessage());
        }

        //return html
        $this->getResponse()->setBody($html);
    }

    /**
     * Return shipping methods
     *
     */
    public function ShippingMethodAction()
    {
        //init vars
        $error = false;
        $message = '';
        $shippingRates = '';

        try {

            //load and init datas & create order
            $this->loadData();
            $this->initData();

            $shippingRates = mage::helper('PointOfSales')->getShippingRates($this->_OrderCreationData);
        } catch (Exception $e) {
            $error = true;
            $message = $this->__('An error occured : %s', $e->getMessage());
        }

        //return ajax result
        $response = array();
        $response['error'] = $error;
        $response['message'] = $message;
        $response['shippingRates'] = $shippingRates;
        $response = Zend_Json::encode($response);
        $this->getResponse()->setBody($response);
    }

    public function SetCustomerInfoAction()
    {
        //init vars
        $error = false;
        $message = '';
        $customer = null;

        try {

            $customerCountry = $this->getRequest()->getPost('customer_country');
            $customerGroupId = $this->getRequest()->getPost('customer_group');
            $customerMode = $this->getRequest()->getPost('customer_mode');
            $customerId = $this->getRequest()->getPost('customer_id');

            $this->initData();

            Mage::helper('PointOfSales/Customer')->setCustomerInfo($customerCountry, $customerGroupId, $customerId);

            switch($customerMode) {
                case "new" :
                    $customer = Mage::helper('PointOfSales/Customer')->createCustomer($this->_OrderCreationData);
                    $message = $customer->getId();
                    break;

                case "existing" :
                    $customer = Mage::getModel('Customer/Customer')->load($customerId);
                    Mage::helper('PointOfSales/Customer')->updateInformation($this->getRequest()->getPost());
                    break;

            }

            $session = Mage::getSingleton('core/session');
            $session->setData('customer_id', $customer ? $customer->getId() : null);

        } catch (Exception $e) {
            $error = true;
            $message = $this->__('An error occured : %s', $e->getMessage());
        }

        //return ajax result
        $response['error'] = $error;
        $response['message'] = $message;
        $response = Zend_Json::encode($response);
        $this->getResponse()->setBody($response);
    }

    /* *******************************************************************************************************************************
     * ********************************************************************************************************************************
     * ** ORDER CREATION
     * ********************************************************************************************************************************
     * ******************************************************************************************************************************* */

    /**
     * Create order and return result with ajax
     *
     */
    public function CreateOrderAction()
    {
        //init vars
        $error = false;
        $message = '';
        $stacktrace = '';

        try {

            //load and init datas & create order
            $this->loadData();

            $this->initData();

            $this->checkStocks();
            if (Mage::helper('PointOfSales')->checkRewardPointModule()):
                $this->_OrderCreationData["pos_credits_applied"] = $this->getRequest()->getPost('pos_credits_applied')=="on"?1:0;
                $this->_OrderCreationData["pos_credits_credits_used"] = $this->getRequest()->getPost('pos_credits_credits_used');
            endif;
            //If new customer selected, create customer
            if ($this->_OrderCreationData['customer_mode'] == 'new') {
                $customer = mage::helper('PointOfSales/Customer')->createCustomer($this->_OrderCreationData);
                $address = mage::helper('PointOfSales/Customer')->createAddress($this->_OrderCreationData, $customer->getId());

                $this->_OrderCreationData['customer_id'] = $customer->getId();
            } else {
                Mage::helper('PointOfSales/Customer')->updateInformation($this->_OrderCreationData);
            }

            //newsletter subscription
            if ($this->getRequest()->getPost('newsletter')) {
                Mage::helper('PointOfSales/Customer')->newsletterSubscription($this->_OrderCreationData['customer_id']);
            }
            $order = mage::helper('PointOfSales')->createOrder($this->_OrderCreationData);

            mage::helper('PointOfSales')->createPayments($order, $this->_OrderCreationData);

            //create order / shipment
            if ($this->_OrderCreationData['is_paid']) {
                $invoice = mage::helper('PointOfSales')->createInvoice($order, $this->_OrderCreationData['invoice_comments']);
            }
            mage::helper('PointOfSales')->createShipment($order, $this->_OrderCreationData);

            /* Printing is now handled at the client level with PDF download

            //print invoice (is required)
            if (mage::getStoreConfig('pointofsales/configuration/print_invoice')) {
                $this->printInvoice($order);
            }

            //print shipment (is required)
            if (mage::getStoreConfig('pointofsales/configuration/print_shipment')) {
                $this->printShipment($order);
            }

            if (Mage::getStoreConfig('pointofsales/configuration/print_receipt')) {
                $this->printReceipt($order);
            }
            */

            // notify guest customer if congiguration allow it
            if (Mage::getStoreConfig('pointofsales/notification/enable_new_order_email')) {
                $order->sendNewOrderEmail();
            }

            //return url
            //$url = $this->getUrl('PointOfSales/PointOfSales/Confirm', array('order_id' => $order->getId()));
            $order->setData('order_view_url', $this->getUrl('adminhtml/PointOfSales_PointOfSales/OrderView', array('order_id' => $order->getId(), 'from' => 'created', 'print_receipt' => Mage::getStoreConfig('pointofsales/configuration/print_receipt'), 'print_invoice' => Mage::getStoreConfig('pointofsales/configuration/print_invoice'))));
            $message = $order->getData();
        } catch (Exception $e) {
            $error = true;
            $exceptionText = $e->getMessage();

            //improve error msg
            $quoteSession = Mage::getSingleton('adminhtml/session_quote');
            $messages = $quoteSession->getMessages(true);
            if ($messages->count() > 0)
                $exceptionText = $messages->getLastAddedMessage()->toString();

            if ($exceptionText == '')
                $exceptionText = $this->__('Undefined exception');

            $stacktrace = $e->getTraceAsString();

            $message = $this->__($exceptionText);
        }

        //return ajax result
        $response = array();
        $response['error'] = $error;
        $response['message'] = $message;
        $response['stacktrace'] = $stacktrace;
        $response = Zend_Json::encode($response);
        $this->getResponse()->setBody($response);
    }

    /**
     * Check that all products can be purchased (check stock levels)
     */
    protected function checkStocks()
    {
        /** @var MDN_PointOfSales_Helper_Stock $helper */
        $helper = Mage::helper('PointOfSales/Stock');
        $websiteId = $helper->getWebsiteId();

        foreach ($this->_OrderCreationData['products'] as $productData) {
            $productId = $productData['product_id'];
            $qty = $productData['qty'];
            $product = Mage::getModel('catalog/product')->load($productId);
            $qtyAvailable = $helper->getAvailableQuantityForSale($product, $websiteId);

            if($qtyAvailable < $qty)
                throw new Exception($this->__('The requested quantity for %s is not available.', $product->getName()));
        }
    }

    /**
     * load datas
     *
     */
    private function loadData()
    {
        $dataRaw = $this->getRequest()->getPost('rawdata');
        $this->_OrderCreationData = mage::helper('PointOfSales/Serialization')->unserializeObject($dataRaw);
    }

    /**
     * init with post data
     *
     */
    private function initData()
    {
        //throw new Exception(print_r($this->getRequest()->getPost(), true));

        //add products
        if ($this->getRequest()->getPost('product_ids') != '') {
            $this->_OrderCreationData['products'] = array();
            $products = explode(';', $this->getRequest()->getPost('product_ids'));

            foreach ($products as $item) {
                $t = explode('-', $item);
                if (count($t) == 6) {
                    $id = $t[0];
                    $product = mage::getModel('catalog/product')->load($id);
                    $qty = $t[1];
                    $priceExclTax = $t[2];
                    $priceInclTax = $t[3];
                    $discount = $t[4];
                    $isShipped = $t[5];
                    $newProduct = array('product_id' => $id,
                        'qty' => $qty,
                        'priceExclTax' => $priceExclTax,
                        'priceInclTax' => $priceInclTax,
                        'discount' => $discount,
                        'shipped' => $isShipped);
                    $this->_OrderCreationData['products'][] = $newProduct;
                }
            }
        }


        $this->_OrderCreationData['total_discount'] = $this->getRequest()->getPost('input_total_discount');
        $this->_OrderCreationData['pos_user_id'] = $this->getRequest()->getPost('user_id');
        $this->_OrderCreationData['store_id'] = $this->getRequest()->getPost('store_id');

        //payment & shipping methods
        $this->_OrderCreationData['payment_method'] = $this->getRequest()->getPost('paymentmethod');
        $this->_OrderCreationData['shipping_method'] = $this->getRequest()->getPost('shippingmethod');

        //throw new Exception(print_r($this->getRequest()->getPost(), true));

        $this->_OrderCreationData['payments'] = array();
        if ($this->getRequest()->getPost('payments') != '') {
            $this->_OrderCreationData['payments'] = json_decode($this->getRequest()->getPost('payments'), true);
        }


        if ($this->getRequest()->getPost('is_paid') != '')
            $this->_OrderCreationData['is_paid'] = $this->getRequest()->getPost('is_paid');

        if ($this->getRequest()->getPost('is_shipped') != '')
            $this->_OrderCreationData['is_shipped'] = $this->getRequest()->getPost('is_shipped');

        //customer
        $this->_OrderCreationData['customer_mode'] = $this->getRequest()->getPost('customer_mode');

        $this->_OrderCreationData['comments'] = $this->getRequest()->getPost('comments');
        $this->_OrderCreationData['invoice_comments'] = $this->getRequest()->getPost('invoice_comments');

        //define information depending of customer mode
        $emptyString = mage::getStoreConfig('pointofsales/configuration/empty_string');
        switch ($this->_OrderCreationData['customer_mode']) {
            case 'guest':

                $this->_OrderCreationData['customer_firstname'] = $emptyString;
                $this->_OrderCreationData['customer_lastname'] = $emptyString;
                $this->_OrderCreationData['customer_email'] = Mage::getStoreConfig('pointofsales/notification/guest_account_email');

                //address
                $this->_OrderCreationData['customer_company'] = $emptyString;
                $this->_OrderCreationData['address'] = $emptyString;

                $defaultCity = mage::helper('PointOfSales/User')->getDefaultCity();
                if (!$defaultCity)
                    $this->_OrderCreationData['city'] = $emptyString;
                else
                    $this->_OrderCreationData['city'] = $defaultCity;

                $defaultZip = mage::helper('PointOfSales/User')->getDefaultZip();
                if (!$defaultZip)
                    $this->_OrderCreationData['zip'] = $emptyString;
                else
                    $this->_OrderCreationData['zip'] = $defaultZip;

                $this->_OrderCreationData['country'] = $this->getRequest()->getPost('country');
                $this->_OrderCreationData['region'] = $this->getRequest()->getPost('region');
                $this->_OrderCreationData['mobile'] = $emptyString;
                $this->_OrderCreationData['phone'] = $emptyString;
                $this->_OrderCreationData['fax'] = $emptyString;
                $this->_OrderCreationData['customer_phone'] = $emptyString;

                break;
            default: //new, existing

                $this->_OrderCreationData['customer_id'] = $this->getRequest()->getPost('customer_id');
                $this->_OrderCreationData['customer_firstname'] = $this->getRequest()->getPost('customer_firstname');
                $this->_OrderCreationData['customer_lastname'] = $this->getRequest()->getPost('customer_lastname');
                $this->_OrderCreationData['customer_email'] = $this->getRequest()->getPost('customer_email');

                //address
                $this->_OrderCreationData['customer_company'] = $this->getRequest()->getPost('customer_company');
                $this->_OrderCreationData['address'] = $this->getRequest()->getPost('address');
                $this->_OrderCreationData['city'] = $this->getRequest()->getPost('city');
                $this->_OrderCreationData['zip'] = $this->getRequest()->getPost('zip');
                $this->_OrderCreationData['country'] = $this->getRequest()->getPost('country');
                $this->_OrderCreationData['phone'] = $this->getRequest()->getPost('phone');
                //$this->_OrderCreationData['fax'] = $this->getRequest()->getPost('fax');
                //$this->_OrderCreationData['customer_phone'] = $this->getRequest()->getPost('customer_phone');
                $this->_OrderCreationData['region'] = $this->getRequest()->getPost('region');
                $this->_OrderCreationData['group'] = $this->getRequest()->getPost('group');

                //fill required address information with empty string if not set
                $fields = array('address', 'city', 'zip', 'phone');
                foreach ($fields as $field) {
                    if ($this->_OrderCreationData[$field] == '')
                        $this->_OrderCreationData[$field] = $emptyString;
                }

                break;
        }
    }

    /**
     * Print invoce using magento client computer
     *
     * @param unknown_type $order
     */
    private function printInvoice($order)
    {
        //collect invoice
        $invoice = null;
        $order = mage::getModel('sales/order')->load($order->getId());
        foreach ($order->getInvoiceCollection() as $item) {
            $invoice = $item;
        }

        //print
        if ($invoice != null) {
            $invoicePdfModel = Mage::getModel('sales/order_pdf_invoice');
            $pdf = $invoicePdfModel->getPdf(array($invoice));
            if ($pdf != null && Mage::helper('PointOfSales/CheckClientComputer')->isInstalled()) {
                $fileName = 'invoice_' . $invoice->getincrement_id() . '.pdf';
                mage::helper('ClientComputer')->printDocument($pdf->render(), $fileName, 'Point of sales  - print invoice #' . $invoice->getincrement_id());
            }
        }
    }

    private function printReceipt($order)
    {

        $order = mage::getModel('sales/order')->load($order->getId());

        $pdf = Mage::getModel('PointOfSales/Pdf_Receipt')->getPdf(array($order));
        if ($pdf != null && Mage::helper('PointOfSales/CheckClientComputer')->isInstalled()) {
            $name = 'Receipt_' . $order->getincrement_id() . '.pdf';
            mage::helper('ClientComputer')->printDocument($pdf->render(), $name, 'Point Of Sales : print receipt');
        }
    }

    /**
     * Print shipment using magento client computer
     *
     * @param unknown_type $order
     */
    private function printShipment($order)
    {
        //collect shipment
        $shipments = $order->getShipmentsCollection();
        $shipment = null;
        foreach ($shipments as $item) {
            $shipment = $item;
        }

        //print
        if ($shipment != null) {
            $ShipmentPdfModel = Mage::getModel('sales/order_pdf_shipment');
            $pdf = $ShipmentPdfModel->getPdf(array($shipment));
            if ($pdf != null && Mage::helper('PointOfSales/CheckClientComputer')->isInstalled()) {
                $fileName = 'shipment_' . $shipment->getincrement_id() . '.pdf';
                mage::helper('ClientComputer')->printDocument($pdf->render(), $fileName, 'Point of sales  - print shipment #' . $shipment->getincrement_id());
            }
        }
    }

    /**
     * Print order using magento client computer
     *
     * @param unknown_type $order
     */
    private function printOrder($order)
    {
        //print
        if ($order != null && Mage::helper('PointOfSales/CheckClientComputer')->isInstalled()) {
            $pdf = Mage::getModel('PointOfSales/Pdf_Order')->getPdf(array($order));
            $fileName = mage::helper('PointOfSales')->__('order_') . $order->getincrement_id() . '.pdf';
            $fileName = str_replace('/', '-', $fileName);
            mage::helper('ClientComputer')->printDocument($pdf->render(), $fileName, 'Point of sales  - print order #' . $order->getincrement_id());
        }
    }

    /**
     * Print shipment using magento client computer
     *
     * @param unknown_type $order
     */
    public function downloadOrderAction()
    {
        //load order
        $orderId = $this->getRequest()->getParam('order_id');
        $order = mage::getModel('sales/order')->load($orderId);

        //create pdf and download
        $pdf = Mage::getModel('PointOfSales/Pdf_Order')->getPdf(array($order));
        $name = mage::helper('PointOfSales')->__('order_') . $order->getincrement_id() . 'pdf';
        $name = str_replace('/', '-', $name);
        $this->_prepareDownloadResponse($name, $pdf->render(), 'application/pdf');
    }

    public function downloadReceiptAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($orderId);

        $pdf = Mage::getModel('PointOfSales/Pdf_Receipt')->getPdf(array($order));
        $name = 'Receipt_' . $order->getincrement_id() . '.pdf';
        $this->_prepareDownloadResponse($name, $pdf->render(), 'application/pdf');
    }

    public function downloadInvoiceAction()
    {
        $invoice = null;
        $order = mage::getModel('sales/order')->load($order->getId());
        foreach ($order->getInvoiceCollection() as $item) {
            $invoice = $item;
        }
        if ($invoice)
        {

        }
    }

    public function downloadReceiptReturnAction()
    {
        $creditMemoId = $this->getRequest()->getParam('creditmemo_id');
        $creditMemo = Mage::getModel('sales/order_creditmemo')->load($creditMemoId);

        $pdf = Mage::getModel('PointOfSales/Pdf_ReceiptReturn')->getPdf(array($creditMemo));
        $name = 'Credit_note_' . $creditMemo->getincrement_id() . '.pdf';
        $this->_prepareDownloadResponse($name, $pdf->render(), 'application/pdf');
    }

    /**
     * Add product from barcode
     *
     */
    public function AddProductFromBarcodeAction()
    {
        //init vars
        $barcode = $this->getRequest()->getParam('barcode');
        $barcode = str_replace('slash', '/', $barcode);
        $barcode = urldecode($barcode);

        $result = array();
        $result['error'] = 0;
        $result['message'] = '';
        $result['product_information'] = null;

        try {
            $product = mage::helper('PointOfSales/Barcode')->getProductFromBarcode($barcode);
            if (($product == null) || (!$product->getId()))
                throw new Exception($this->__('Cant find product with barcode = %s', $barcode));

            if ($product->gettype_id() == 'configurable') {
                //return in json
                $result = array('configurable' => 1, 'product_id' => $product->getId());
                $response = Zend_Json::encode($result);
                $this->getResponse()->setBody($response);
                return;
            }


            $result = mage::helper('PointOfSales/ProductInfo')->getProductToAdd($product);

        } catch (Exception $ex) {
            $result['error'] = 1;
            $result['message'] = $ex->getMessage();
        }

        //return in json
        $response = Zend_Json::encode($result);
        $this->getResponse()->setBody($response);
    }

    public function cleanTxt($txt)
    {
        return addslashes(str_replace('"', ' ', $txt));
    }

    /**
     * Return change calculator form
     */
    public function ChangeCalculatorAction()
    {
        $block = $this->getLayout()->createBlock('core/template');
        $block->setTemplate('PointOfSales/ChangeCalculator.phtml');
        $this->getResponse()->setBody($block->toHtml());
    }

    public function ShowConfigurableProductOptionsAction()
    {
        $productId = $this->getRequest()->getParam('product_id');
        $block = $this->getLayout()->createBlock('PointOfSales/OrderCreation_ConfigurableProductOptions');
        $block->setTemplate('PointOfSales/OrderCreation/ConfigurableProductOptions.phtml');
        $block->setProduct($productId);
        $this->getResponse()->setBody($block->toHtml());
    }

    public function AddConfigurableProductAction()
    {
        $post = $this->getRequest()->getPost();

        $configurableProduct = mage::getModel('catalog/product')->load($post['product_id']);

        $attributes = array();
        foreach ($post as $attributeCode => $value) {
            if (in_array($attributeCode, array('product_id', 'form_key')))
                continue;

            $attributes[$attributeCode] = $value;
        }


        $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $configurableProduct);
        foreach ($childProducts as $child) {
            $match = true;
            foreach ($attributes as $attributeCode => $value) {
                if ($child->getData($attributeCode) != $value) {
                    $match = false;
                    break;
                }
            }

            if ($match) {
                $productNameSuffix = mage::helper('PointOfSales/ProductInfo')->getProductAttributeLabelsAsText($child);
                $productToAdd = mage::helper('PointOfSales/ProductInfo')->getProductToAdd($child, $productNameSuffix);
                break;
            }
        }

        if (!$match) {
            $productToAdd = mage::helper('PointOfSales/ProductInfo')->getProductToAdd(null);
        }

        $this->getResponse()->setBody(json_encode($productToAdd));
    }


    public function SearchProductsAction()
    {
        $term = $this->getRequest()->getParam('term');
        $return = array();

        if ($term) {
            $collection = mage::helper('PointOfSales/Search')->searchProducts($term);

            foreach ($collection as $product) {
                $suffix = mage::helper('PointOfSales/ProductInfo')->getProductAttributeLabelsAsText($product);
                $result = mage::helper('PointOfSales/ProductInfo')->getProductToAdd($product, $suffix);
                $return[] = $result['product_information'];
            }
        }
        $this->getResponse()->setBody(json_encode($return));
    }

    public function SearchCustomersAction()
    {

        $term = $this->getRequest()->getParam('term');
        $customerid = $this->getRequest()->getParam('customerid');
		$return = array();
		if($term){
			$return = Mage::helper('PointOfSales/Customer')->searchCustomer($term);		
		}elseif($customerid){
			$return = Mage::helper('PointOfSales/Customer')->searchCustomerByID($customerid);
		}

        $this->getResponse()->setBody(json_encode($return));
    }

    public function addProductAction()
    {
        $storeId = $this->getRequest()->getParam('store_id');

        $blockSearch = $this->getLayout()->createBlock('PointOfSales/Product_Addproduct');
        $blockSearch->setStoreId($storeId);
        $blockSearch->setTemplate('PointOfSales/Product/AddProduct.phtml');
        
        $this->getResponse()->setBody($blockSearch->toHtml());
    }

    public function SalesListWithSearchAction()
    {
        $storeId = $this->getRequest()->getParam('store_id');
        $term = $this->getRequest()->getParam('term');
        $screenHeight = $this->getRequest()->getParam('screen_height');
        $page = $this->getRequest()->getParam('page');

        $blockSearch = $this->getLayout()->createBlock('PointOfSales/OrderCreation_SalesSearch');
        $blockSearch->setStoreId($storeId);

        $blockSearch->setTemplate('PointOfSales/OrderCreation/SalesSearch.phtml');

        $blockList = $this->getLayout()->createBlock('PointOfSales/OrderCreation_SalesList');
        $blockList->setStoreId($storeId);
        $blockList->setScreenHeight($screenHeight);
        $blockList->setCurrentPage((empty($page) ? 1 : $page));
        $blockList->setSearchTerm(empty($term) ? null : $term);
        $blockList->setTemplate('PointOfSales/OrderCreation/SalesList.phtml');

        $this->getResponse()->setBody($blockSearch->toHtml() . '<div id="orderList">' . $blockList->toHtml() . '</div>');
    }

    public function SalesListAction()
    {
        $storeId = $this->getRequest()->getParam('store_id');
        $term = $this->getRequest()->getParam('term');
        $screenHeight = $this->getRequest()->getParam('screen_height');
        $page = $this->getRequest()->getParam('page');

        $blockList = $this->getLayout()->createBlock('PointOfSales/OrderCreation_SalesList');
        $blockList->setStoreId($storeId);
        $blockList->setScreenHeight($screenHeight);
        $blockList->setCurrentPage((empty($page) ? 1 : $page));
        $blockList->setSearchTerm(empty($term) ? null : $term);
        $blockList->setTemplate('PointOfSales/OrderCreation/SalesList.phtml');

        $this->getResponse()->setBody($blockList->toHtml());
    }

    public function addProductPostAction()
    {
        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $storeId = $this->getRequest()->getParam('store_id');
        $websiteId = Mage::getModel('core/store')->load($storeId)->getWebsiteId();
        parse_str($this->getRequest()->getPost('product'), $productData);
        $product = Mage::getModel('catalog/product');
        $taxId = Mage::getStoreConfig('tax/classes/shipping_tax_class', $storeId);
        $store = Mage::getModel('core/store')->load($storeId);
        $currencySymbol = Mage::app()->getLocale()->currency($store->getCurrentCurrencyCode())->getSymbol();
        try{
            $product
                ->setStoreId($storeId) 
                ->setWebsiteIds($websiteId)
                ->setAttributeSetId(Mage::getModel('catalog/product')->getDefaultAttributeSetId()) 
                ->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
                ->setCreatedAt(strtotime('now'))
                ->setSku($productData['sku']) 
                ->setName($productData['name'])
                ->setWeight(0.0000)
                ->setStatus(1)
                ->setTaxClassId($taxId)
                ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE) //VISIBILITY_BOTH
                ->setPrice($productData['price']?$productData['price']:0.00)
                ->setBarcode($productData['barcode']?$productData['barcode']:"")
                ->setDescription($productData['description'])
                ->setStockData(array(
                                   'use_config_manage_stock' => 0,
                                   'manage_stock'=>1, 
                                   'is_in_stock' => 1,
                                   'qty' => $productData['qty']
                               )
                );
            $product->save();
            $_priceExcludingTax = Mage::helper('tax')->getPrice($product, $product->getFinalPrice(), false );
            $_priceIncludingTax = Mage::helper('tax')->getPrice($product, $product->getFinalPrice());
            $taxRate = mage::helper('PointOfSales/ProductInfo')->getTaxRate($product);
            $taxRate = number_format($taxRate, 4, '.', '');
            $body = json_encode(array(
                                "status"            =>"success",
                                "msg"               =>$this->__("Product has been created successfully."),
                                "productid"         =>$product->getId(),
                                "sku"               =>$product->getSku(),
                                "name"              =>$product->getName(),
                                "price"             =>Mage::helper('core')->currency($product->getPrice()),
                                "currencySymbol"    =>$currencySymbol,
                                "qty"               => $productData['qty'],
                                "price_excl_tax"    =>$_priceExcludingTax,
                                "price_incl_tax"    =>$_priceIncludingTax,
                                "tax"               =>$taxRate));
            $this->getResponse()->setBody($body);
        }catch(Exception $e){
            if ($e->getCode() == 23000) {
                $this->getResponse()->setBody(json_encode(array("status"=>"error","msg"=>$this->__("This sku already in use."))));
            }
            Mage::log($e->getMessage());
        }
    }

    public function ReturnViewAction()
    {
        try {


            $this->loadLayout();
            $this->getLayout()->removeOutputBlock('root')->addOutputBlock('content');
            $this->renderLayout();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function cashRegisterInitAction()
    {
        try {
            $this->loadLayout();
            $this->getLayout()->removeOutputBlock('root')->addOutputBlock('content');
            $this->renderLayout();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function setCashInitAmountAction()
    {
        try {
            $method = $this->getRequest()->getPost('method');
            $amount = $this->getRequest()->getPost('amount');

            Mage::helper('PointOfSales/CashRegister')->setCashInitAmount($amount, $method);

            $return['error'] = 0;
            $return['message'] = '';

        } catch (Exception $e) {
            $return['error'] = 1;
            $return['message'] = $e->getMessage();
        }

        $this->getResponse()->setBody(json_encode($return));
    }

    public function CreateCreditMemoAction()
    {
        try {
            $orderId = $this->getRequest()->getPost('orderId');
            $paymentMethod = $this->getRequest()->getPost('paymentMethod');


            $order = mage::getModel('sales/order')->load($orderId);
            if (!$order || !$order->getId())
                throw new Exception("Order #$orderId doen't exists !");

            $products = $this->getRequest()->getPost('products');

            $object = array();
            $object['order_id'] = $orderId;
            $object['products'] = array();
            foreach ($products as $product) {
                $object['products'][] = array(
                    'order_item_id' => $product['orderItemId'],
                    'qty' => $product['qty'],
                    'price' => $product['price']
                );
            }

            if (empty($object['products']))
                throw new Exception("There is no products to return");

            $object['refund'] = 0;
            $object['fee'] = 0;
            $object['refund_online'] = 0;

            $creditMemos = mage::helper('PointOfSales/Creditmemo')->CreateCreditmemo($object);
            $creditMemo = current($creditMemos);

            $incrementId = $order->getIncrementId();
            $comment = "Return for order #".$incrementId.", credit memo #".$creditMemo->getincrement_id();
            Mage::getModel('PaymentTracker/Payment')->addPayment(
                $paymentMethod,
                -$creditMemo->getgrand_total(),
                Mage::getModel('core/date')->date('Y-m-d H:i:s'),
                $comment,
                $orderId
            );

            $return['creditmemoUrl'] = mage::helper('adminhtml')->getUrl('adminhtml/sales_creditmemo/view', array('creditmemo_id' => $creditMemo->getId()));
            $return['error'] = 0;
            $return['order_id'] = $orderId;
            $return['creditmemo_id'] = $creditMemo->getId();


        } catch (Exception $e) {
            $return['error'] = 1;
            $return['message'] = $e->getMessage();
        }

        $this->getResponse()->setBody(json_encode($return));
    }

    public function ExportAction()
    {
        $from = $this->getRequest()->getParam('from');
        $to = $this->getRequest()->getParam('to');
        $storeId = $this->getRequest()->getParam('store');
        $format = $this->getRequest()->getParam('format');

        $content = Mage::helper('PointOfSales/Export')->getContent($from, $to, array($storeId), $format);
        $fileName = 'pos_export_' . $from . '_' . $to;
        $contentType = 'text/csv';
        switch ($format) {
            case MDN_PointOfSales_Helper_Export::kFormatCsv:
                $fileName .= '.csv';
                break;
            case MDN_PointOfSales_Helper_Export::kFormatPdf:
                $fileName .= '.pdf';
                $contentType = 'application/pdf';
                break;
        }

        return $this->_prepareDownloadResponse($fileName, $content, $contentType);

    }


    protected function _isAllowed()
    {
        return true;
    }

    public function ProductViewAction()
    {
        try {
            $productId = $this->getRequest()->getParam('product_id');
            $product = mage::getModel('catalog/product')->load($productId);
            if (!$product || !$product->getId())
                throw new Exception("product #$productId doesn't exists");

            $blockList = $this->getLayout()->createBlock('PointOfSales/OrderCreation_ProductView');
            $blockList->setProduct($product);
            $blockList->setTemplate('PointOfSales/OrderCreation/ProductView.phtml');

            $this->getResponse()->setBody($blockList->toHtml());

        } catch (Exception $e) {
            $this->getResponse()->setBody($e->getMessage());
            return;
        }
    }

    public function updateShortcutsAction()
    {
        try {

            $return['shortcuts'] = Mage::helper('PointOfSales/Shortcuts')->getShortcutsAsHtml();

            $return['error'] = 0;
            $return['message'] = '';

        } catch (Exception $e) {
            $return['error'] = 1;
            $return['message'] = $e->getMessage();
        }

        $this->getResponse()->setBody(json_encode($return));
    }
    public function rewardToMoneyAction()
    {
        if(Mage::helper('PointOfSales')->checkRewardPointModule())
        {
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $points = $this->getRequest()->getParam("points");
            $money = $this->getRequest()->getParam("money");
            $customerId = $this->getRequest()->getParam("customerId");
            $maxPoints = Mage::helper("posrewardpoints")->storeCreditBalance($customerId);
            if($maxPoints < $points)
            {
                $this->getResponse()->setBody(json_encode(array("status"=>"error","msg"=>$this->__('Please enter valid reward points. Not greater than: '.$maxPoints))));
                return;
            }
            $points = Mage::helper("posrewardpoints")->moneyToCeedits($money, $maxPoints);
            if($points>$maxPoints)
            {
                $money = Mage::helper("posrewardpoints")->creditsToMoney($maxPoints);
                $points = $maxPoints;
            }
            if(!$points){
                $this->getResponse()->setBody(json_encode(array("status"=>"error","msg"=>$this->__('Points are not sufficient to convert in money'))));
            }
            else
            {
                $this->getResponse()->setBody(json_encode(array("status"=>"success","points"=>$points,"money"=>number_format(($money*(-1)), '2', '.', ','))));
            }
        }
    }

}