<?php

class MDN_PointOfSales_Model_Mysql4_Zreport extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('PointOfSales/CashRegister', 'pcr_id');
    }

    public function getTotalSales($settings)
    {
        $select = $this->_getReadAdapter()
            ->select()
            ->reset()
            ->from(Mage::getSingleton('core/resource')->getTableName('sales_flat_order'), array(
                    new Zend_Db_Expr('SUM(base_subtotal) + SUM(base_shipping_amount) as sub_total')
                )
            )
            ->where('store_id in ('.$settings['store_id'].')')
            ->where('created_at >= "'.$settings['from'].'"')
            ->where('created_at <= "'.$settings['to'].'"')
        ;

        $result = $this->_getReadAdapter()->fetchOne($select);
        return $result;
    }

    public function getTotalReturn($settings)
    {
        $select = $this->_getReadAdapter()
            ->select()
            ->from(Mage::getSingleton('core/resource')->getTableName('sales_flat_order'), array(
                    new Zend_Db_Expr('SUM(subtotal_refunded) as refunded')
                )
            )
            ->where('store_id in ('.$settings['store_id'].')')
            ->where('created_at >= "'.$settings['from'].'"')
            ->where('created_at <= "'.$settings['to'].'"')
        ;

        $result = $this->_getReadAdapter()->fetchOne($select);
        return $result;
    }

    public function getTotalTax($settings)
    {
        $select = $this->_getReadAdapter()
            ->select()
            ->from(Mage::getSingleton('core/resource')->getTableName('sales_flat_order'), array(
                    new Zend_Db_Expr('SUM(base_tax_amount) as taxes')
                )
            )
            ->where('store_id in ('.$settings['store_id'].')')
            ->where('created_at >= "'.$settings['from'].'"')
            ->where('created_at <= "'.$settings['to'].'"')
        ;

        $result = $this->_getReadAdapter()->fetchOne($select);
        return $result;
    }

    public function getTaxAmountPerPercent($settings)
    {
        $select = $this->_getReadAdapter()
            ->select()
            ->from(Mage::getSingleton('core/resource')->getTableName('sales_flat_order_item'), array('tax_percent', new Zend_Db_Expr('SUM(base_tax_amount) as total')))
            ->where('store_id in ('.$settings['store_id'].')')
            ->where('created_at >= "'.$settings['from'].'"')
            ->where('created_at <= "'.$settings['to'].'"')
            ->where('tax_percent > 0')
            ->group('tax_percent')
        ;

        $result = $this->_getReadAdapter()->fetchAll($select);
        return $result;
    }

    public function getPaymentsPerMethods($settings)
    {
        $select = $this->_getReadAdapter()
            ->select()
            ->from(Mage::getSingleton('core/resource')->getTableName('payment_tracker_payment'), array('ptp_method', new Zend_Db_Expr('SUM(ptp_amount) as total')))
            ->join(Mage::getSingleton('core/resource')->getTableName('sales_flat_order'), 'entity_id = ptp_order_id', array(''))
            ->where('store_id in ('.$settings['store_id'].')')
            ->where('ptp_date>= "'.$settings['from'].'"')
            ->where('ptp_date <= "'.$settings['to'].'"')
            ->group('ptp_method')
        ;

        $result = $this->_getReadAdapter()->fetchAll($select);
        return $result;
    }

    public function getTransactionsPerMethods($settings)
    {
        $select = $this->_getReadAdapter()
            ->select()
            ->from(Mage::getSingleton('core/resource')->getTableName('payment_tracker_payment'), array('ptp_method', new Zend_Db_Expr('count(*) as total')))
            ->join(Mage::getSingleton('core/resource')->getTableName('sales_flat_order'), 'entity_id = ptp_order_id', array(''))
            ->where('store_id in ('.$settings['store_id'].')')
            ->where('ptp_date >= "'.$settings['from'].'"')
            ->where('ptp_date <= "'.$settings['to'].'"')
            ->group('ptp_method')
        ;

        $result = $this->_getReadAdapter()->fetchAll($select);
        return $result;
    }
    public function getSales($settings)
    {
        $select = $this->_getReadAdapter()
            ->select()
            ->from(array('o' => Mage::getSingleton('core/resource')->getTableName('sales_flat_order')), array('increment_id','grand_total'))
            ->joinLeft(Mage::getSingleton('core/resource')->getTableName('payment_tracker_payment'), 'entity_id = ptp_order_id', array('ptp_method', 'ptp_amount'))
            ->where('store_id in ('.$settings['store_id'].')')
            ->where('o.created_at >= "'.$settings['from'].'"')
            ->where('o.created_at <= "'.$settings['to'].'"')
            ->order('entity_id asc')
        ;

        $result = $this->_getReadAdapter()->fetchAll($select);
        return $result;
    }
}
