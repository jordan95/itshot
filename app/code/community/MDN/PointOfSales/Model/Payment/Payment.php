<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class MDN_PointOfSales_Model_Payment_Payment extends MDN_PaymentTracker_Model_Payment {

    public function addPayment($method, $amount, $date, $comments, $orderId, $isMoneyBack = 0)
    {
        parent::addPayment($method, $amount, $date, $comments, $orderId);
        $this->setptp_is_money_back($isMoneyBack);
        $this->save();

        Mage::helper('PaymentTracker/Order')->updateOrderPaidInformation($orderId);
    }

}