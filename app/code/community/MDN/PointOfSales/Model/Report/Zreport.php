<?php


class MDN_PointOfSales_Model_Report_Zreport  extends Mage_Core_Model_Abstract
{

    public function getReport($from, $to, $storeIds)
    {
        $content = array();

        $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
        $now = Mage::helper('core')->formatDate(date('Y-m-d', $currentTimestamp), 'medium', false);
        $resourceModel = Mage::getResourceModel('PointOfSales/Zreport');

        $settings = array();
        $settings['from'] = $from;
        $settings['to'] = $to;
        $settings['register'] = 'XXX';
        $settings['store_id'] = $storeIds;

        $section = array();
        $section[] = array('title' => 'Z-Report');
        $section[] = array('title' => 'Report date', 'value' => $now);
        //$section[] = array('title' => 'Register', 'value' => $settings['register']);
        $section[] = array('title' => 'Start date', 'value' => $settings['from']);
        $section[] = array('title' => 'Closing date', 'value' => $settings['to']);
        $section[] = array('title' => 'Stores', 'value' => $this->getStoreDescription($storeIds));
        $content[] = $section;

        $section = array();
        $section[] = array('title' => 'Main figures');
        $openingTransaction = $this->getOpeningTransaction($from, $storeIds);
        $section[] = array('title' => 'Opening total', 'value' => number_format($openingTransaction->getpcr_amount(), 2, '.', ' '));
        $itemSales = $resourceModel->getTotalSales($settings);
        $section[] = array('title' => 'Sales (excl tax)', 'value' => number_format($itemSales, 2, '.', ' '));
        $taxes = $resourceModel->getTotalTax($settings);
        $section[] = array('title' => 'Sales Tax', 'value' => number_format($taxes, 2, '.', ' '));
        $total = $itemSales + $taxes;   //do not include opening total here
        $section[] = array('title' => 'Sales Total', 'value' => number_format($total, 2, '.', ' '));
        $totalReturn = $resourceModel->getTotalReturn($settings);
        $section[] = array('title' => 'Item Returns (Incl tax)', 'value' => number_format($totalReturn, 2, '.', ' '));

        $content[] = $section;

        //taxes
        $section = array();
        $section[] = array('title' => 'Tax collected', 'value' => '');
        foreach($resourceModel->getTaxAmountPerPercent($settings) as $item)
        {
            $section[] = array('title' => number_format($item['tax_percent'], 2, '.', ' ').'%', 'value' => number_format($item['total'], 2, '.', ' '));
        }
        $content[] = $section;

        //payment methods totals
        $section = array();
        $section[] = array('title' => 'Total per payment method', 'value' => '');
        foreach($resourceModel->getPaymentsPerMethods($settings) as $item)
        {
            $value = $item['total'];
            if ($openingTransaction->getpcr_method() && $item['ptp_method'] == $openingTransaction->getpcr_method())
                $value += $openingTransaction->getpcr_amount();
            $section[] = array('title' => $item['ptp_method'], 'value' => $value);
        }
        $content[] = $section;

        //payment methods count
        $section = array();
        $section[] = array('title' => 'Transactions count per payment method', 'value' => '');
        foreach($resourceModel->getTransactionsPerMethods($settings) as $item)
        {
            $section[] = array('title' => $item['ptp_method'], 'value' => $item['total']);
        }
        $content[] = $section;

        //list of sales
        $section = array();
        $section[] = array('title' => 'Sales', 'value' => '');
        foreach($resourceModel->getSales($settings) as $item)
        {
            $section[] = array('title' => $item['increment_id'].' : '.$item['ptp_method'], 'value' => number_format($item['ptp_amount'] ? $item['ptp_amount'] : $item['grand_total'], 2, '.', ' '));
        }
        $content[] = $section;

        /*
        $section = [];
        $section[] = ['title' => 'Cash drawer expected', 'value' => '##'];
        $content[] = $section;
        */

        return $content;
    }

    public function getOpeningTransaction($from, $storeId)
    {
        $t = explode(',', $storeId);
        if (count($t) > 1)
            $storeId = -1;
        $from = date("Y-m-d", strtotime($from));
        $item = Mage::getModel('PointOfSales/CashRegister')
                        ->getCollection()
                        ->addFieldToFilter('pcr_date', $from)
                        ->addFieldToFilter('pcr_store_id', $storeId)
                        ->getFirstItem();
        return $item;
    }

    public function getStoreDescription($storeIds)
    {
        $desc = array();

        $storeIds  = explode(',', $storeIds);
        foreach($storeIds as $storeId)
        {
            $store = Mage::getModel('core/store')->load($storeId);
            $desc[] = $store->getname();
        }

        return implode(",", $desc);
    }


}