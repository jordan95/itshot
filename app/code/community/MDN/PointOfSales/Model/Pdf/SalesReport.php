<?php

class MDN_PointOfSales_Model_Pdf_SalesReport extends MDN_PointOfSales_Model_Pdf_Helper {

    const kLeftDate = 15;
    const kLeftOrder = 100;
    const kLeftCustomer = 200;
    const kLeftMethod = 400;
    const kLeftAmount = 490;

    public function getPdf($collection = array(), $cashRegisterInit = null) {
        $this->_beforeGetPdf();
        $this->_initRenderer('invoice');

        $this->pdf = new Zend_Pdf();
        $style = new Zend_Pdf_Style();
        $style->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD), 10);

        //Add new page
        $settings = array();
        $settings['title'] = mage::helper('PointOfSales')->__('Payment Report');
        $storeId = mage::helper('PointOfSales/User')->getStoreId();
        $settings['store_id'] = $storeId;
        $page = $this->NewPage($settings);

        $totals = array();
        $attributes = array();

        //add table header
        $this->drawTableHeader($page);

        //add cash register initialization if any
        if ($cashRegisterInit !== null) {
            $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);

            $page->drawText($cashRegisterInit->getpcr_date(), self::kLeftDate, $this->y, 'UTF-8');
            $page->drawText('-', self::kLeftOrder, $this->y, 'UTF-8');
            $page->drawText(Mage::helper('PointOfSales')->__('Cash Register at start'), self::kLeftCustomer, $this->y, 'UTF-8');
            $page->drawText($cashRegisterInit->getpcr_method(), self::kLeftMethod, $this->y, 'UTF-8');
            $page->drawText($cashRegisterInit->getpcr_amount(), self::kLeftAmount, $this->y, 'UTF-8');

            //separation line
            $page->setLineWidth(0.5);
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.1));
            $page->drawLine(10, $this->y - 7, $this->_BLOC_ENTETE_LARGEUR, $this->y - 5);

            $this->y -= $this->_ITEM_HEIGHT;

            if (!isset($totals[$cashRegisterInit->getpcr_method()]))
                $totals[$cashRegisterInit->getpcr_method()] = 0;
            $totals[$cashRegisterInit->getpcr_method()] += $cashRegisterInit->getpcr_amount();
        }


        //print order collection
        $collection = $collection[0];

        foreach ($collection as $item) {

            //print order information
            $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->drawText($item->getptp_date(), self::kLeftDate, $this->y, 'UTF-8');
            $page->drawText($item->getincrement_id(), self::kLeftOrder, $this->y, 'UTF-8');
            $page->drawText($item->getcustomer_firstname().' '.$item->getcustomer_lastname(), self::kLeftCustomer, $this->y, 'UTF-8');
            $page->drawText($item->getptp_method(), self::kLeftMethod, $this->y, 'UTF-8');
            $page->drawText($item->getptp_amount(), self::kLeftAmount, $this->y, 'UTF-8');

            //separation line
            $page->setLineWidth(0.5);
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.1));
            $page->drawLine(10, $this->y - 7, $this->_BLOC_ENTETE_LARGEUR, $this->y - 5);

            $this->y -= $this->_ITEM_HEIGHT;

            if (!isset($totals[$item->getptp_method()]))
                $totals[$item->getptp_method()] = 0;
            $totals[$item->getptp_method()] += $item->getptp_amount();

            //new page
            if ($this->y < ($this->_BLOC_FOOTER_HAUTEUR + 40)) {
                $this->drawFooter($page, $storeId);
                $page = $this->NewPage($settings);
                $this->drawTableHeader($page);
            }
        }

        //draw totals
        $this->drawTotals($totals, $page, $attributes);
        $this->drawFooter($page, $storeId);
        $this->AddPagination($this->pdf);
        $this->_afterGetPdf();

        return $this->pdf;
    }

    /**
     * Draw table headers
     *
     * @param unknown_type $page
     */
    public function drawTableHeader(&$page) {
        $this->y -= 15;
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);

        $page->drawText(mage::helper('PointOfSales')->__('Date'), self::kLeftDate, $this->y, 'UTF-8');
        $page->drawText(mage::helper('PointOfSales')->__('Order'), self::kLeftOrder, $this->y, 'UTF-8');
        $page->drawText(mage::helper('PointOfSales')->__('Customer'), self::kLeftCustomer, $this->y, 'UTF-8');
        $page->drawText(mage::helper('PointOfSales')->__('Method'), self::kLeftMethod, $this->y, 'UTF-8');
        $page->drawText(mage::helper('PointOfSales')->__('Amount'), self::kLeftAmount, $this->y, 'UTF-8');

        $this->y -= 8;
        $page->drawLine(10, $this->y, $this->_BLOC_ENTETE_LARGEUR, $this->y);

        $this->y -= 15;
    }

    /**
     * Draw totals per payment method
     *
     */
    public function drawTotals($totals, &$page, $attributes) {

        // add line
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 14);
        $page->drawLine(10, $this->y, $this->_BLOC_ENTETE_LARGEUR, $this->y);
        $this->y -= 25;

        // display total by payment method
        foreach ($totals as $key => $value) {
            $page->drawText(mage::helper('PointOfSales')->__('Total %s', $key), 285, $this->y, 'UTF-8');
            $page->drawText($this->FormatPrice($value), 500, $this->y, 'UTF-8');
            $this->y -= 25;
        }

        //display grand total
        $grandTotal = 0;
        foreach ($totals as $key => $value) {
            $grandTotal += $value;
        }
        $page->drawText(mage::helper('PointOfSales')->__('Total'), 285, $this->y, 'UTF-8');
        $page->drawText($this->FormatPrice($grandTotal), 500, $this->y, 'UTF-8');

        $this->y -= 15;
        $page->drawLine(10, $this->y, $this->_BLOC_ENTETE_LARGEUR, $this->y);
        $this->y -= 15;
    }

    /**
     * format price
     *
     * @param unknown_type $price
     * @return unknown
     */
    public function FormatPrice($price) {
        $currency = mage::helper('PointOfSales/User')->getCurrency();
        return $currency->format($price, array(), false);
    }

    /**
     * Format date
     *
     * return only date like YYYY-MM-DD
     *
     * @param string $date
     */
    public function formatDate($date) {

        $tmp = explode(" ", $date);
        return $tmp[0];
    }

}
