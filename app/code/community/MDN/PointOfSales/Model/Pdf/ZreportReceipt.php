<?php

class MDN_PointOfSales_Model_Pdf_ZreportReceipt  extends MDN_PointOfSales_Model_Pdf_Receipt {

    const kSectionMargin = 20;
    const kItemMargin = 10;

    public $_from;
    public $_to;
    public $_storeIds;

    public function initFromRegistry()
    {
        $variables = Mage::helper('SmartReport')->getVariables();
        $this->_from = $variables['date_from'];
        $this->_to = $variables['date_to'];
        $this->_storeIds = $variables['sm_store'];

        return $this;
    }

    public function getPdf($creditMemos = array()) {

        $this->_beforeGetPdf();

        $data = Mage::getModel('PointOfSales/Report_Zreport')->getReport($this->_from, $this->_to, $this->_storeIds);

        // init
        $storeId = 0;
        $this->initSizes(Mage::getStoreConfig('pointofsales/receipt/width', $storeId), Mage::getStoreConfig('pointofsales/receipt/unit', $storeId), null);
        $this->height = $this->calculateHeight($data, $storeId);

        if ($this->pdf == null)
            $this->pdf = new Zend_Pdf();

        $style = new Zend_Pdf_Style();
        $style->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD), $this->_fontSize);

        // create new page
        $settings['title'] = Mage::getStoreConfig('pointofsales/receipt/header', $storeId);
        $settings['store_id'] = $storeId;
        $page = $this->NewPage($settings);

        foreach($data as $sections)
        {
            foreach ($sections as $section)
            {
                if ($section['value'] == '')
                {
                    $this->y -= self::kSectionMargin;
                    $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD), 10);
                    $page->drawText(Mage::helper('PointOfSales')->__($section['title']), 5 + $this->margin_left, $this->y, 'UTF-8');
                    $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), $this->_fontSize);
                    $this->y -= self::kItemMargin;
                }
                else
                {
                    $page->drawText(Mage::helper('PointOfSales')->__($section['title']), 5 + $this->margin_left, $this->y, 'UTF-8');
                    $this->drawTextInBlock($page, $section['value'], 5, $this->y, $this->width - 10, 50, 'r');
                    $this->y -= self::kItemMargin;
                }
            }
        }

        // add footer
        $this->drawFooter($page);
        $this->_afterGetPdf();

        return $this->pdf;
    }

    public function calculateHeight($data, $storeId)
    {
        $height = 0;

        //header size
        $headerText = Mage::getStoreConfig('pointofsales/receipt/header', $storeId);
        $height += count(explode("\n", $headerText)) + 50;

        foreach($data as $sections)
        {
            foreach ($sections as $section)
            {
                if ($section['value'] == '')
                {
                    $height += self::kSectionMargin;
                    $height += self::kItemMargin;
                }
                else
                {
                    $height += self::kItemMargin;
                }
            }
        }


        //footer size
        $footerText = Mage::getStoreConfig('pointofsales/receipt/footer', $storeId);
        $height += count(explode("\n", $footerText)) + 50;



        $height += 40;

        return $height;
    }

}
