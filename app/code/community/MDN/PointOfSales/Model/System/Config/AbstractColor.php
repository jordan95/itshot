<?php

/**
 * Magento Fianet Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Gr
 * @package    Gr_Fianet
 * @author     Nicolas Fabre <nicolas.fabre@groupereflect.net>
 * @copyright  Copyright (c) 2008 Nicolas Fabre
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_PointOfSales_Model_System_Config_AbstractColor extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    protected $colors = array(
        'primary' => 'Dark Blue',
        'success' => 'Green',
        'info' => 'Light Blue',
        'warning' => 'Orange',
        'danger' => 'Red',
        'default' => 'White'
    );

    protected $style = array(
        'Dark Blue' => '#337ab7',
        'Green' => '#5cb85c',
        'Light Blue' => '#5bc0de',
        'Orange' => '#f0ad4e' ,
        'Red' => '#d9534f',
        'White' => '#ffffff'
    );

    public function getAllOptions() {
        if (!$this->_options) {
            $colors = $this->colors;
            foreach ($colors as $code => $color) {
                $options[] = array(
                    'value' => $code,
                    'label' => $color,
                    'style' => 'background-color: '.$this->style[$color]
                );
            }
            $this->_options = $options;
        }
        return $this->_options;
    }

    public function toOptionArray() {
        return $this->getAllOptions();
    }

}