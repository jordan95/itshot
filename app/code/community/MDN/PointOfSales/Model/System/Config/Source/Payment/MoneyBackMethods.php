<?php

class MDN_PointOfSales_Model_System_Config_Source_Payment_MoneyBackMethods extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    protected $_options;

    public function toOptionArray() {

        if (!$this->_options) {
            $this->getAllOptions();
        }
        return $this->_options;
    }

    public function getAllOptions() {
        if (!$this->_options) {

            $methods = explode("\n", Mage::getStoreConfig('payment_tracker/general/custom_payment_method'));

            $this->_options = array(
                array(
                    'value' => '',
                    'label' => ''
                )
            );
            foreach($methods as $method)
            {
                $method = trim($method);
                if($method) {
                    $this->_options[] = array(
                        'value' => $method,
                        'label' => $method
                    );
                }
            }

        }
        return $this->_options;
    }

}