<?php

/**
 * Magento Fianet Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Gr
 * @package    Gr_Fianet
 * @author     Nicolas Fabre <nicolas.fabre@groupereflect.net>
 * @copyright  Copyright (c) 2008 Nicolas Fabre
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_PointOfSales_Model_System_Config_NavbarColor extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    /**
     * List of flat colors
     *
     *  '#1abc9c' => 'Turquoise',
     *  '#2ecc71' => 'Emerald',
     *  '#3498db' => 'Peter River',
     *  '#9b59b6' => 'Amethyst',
     *  '#16a085' => 'Green Sea',
     *  '#27ae60' => 'Nephritis',
     *  '#2980b9' => 'Belize Hole',
     *  '#8e44ad' => 'Wisteria',
     *  '#f1c40f' => 'Sun Flower',
     *  '#e67e22' => 'Carrot',
     *  '#e74c3c' => 'Alizarin',
     *  '#ecf0f1' => 'Clouds',
     *  '#95a5a6' => 'Concrete',
     *  '#f39c12' => 'Orange',
     *  '#d35400' => 'Pumpkin',
     *  '#c0392b' => 'Pomegranate',
     *  '#bdc3c7' => 'Silver',
     *  '#7f8c8d' => 'Abestos',
     *
     */

    private $colors = array(
        '#337ab7' => 'Dark Blue',
        '#5cb85c' => 'Green',
        '#5bc0de' => 'Light Blue',
        '#f0ad4e' => 'Orange',
        '#d9534f' => 'Red',
        '#e6e9ed' => 'Light Grey',
        '#ffffff' => 'White',
    );

    public function getAllOptions() {
        if (!$this->_options) {
            $colors = $this->colors;
            foreach ($colors as $code => $color) {
                $options[] = array(
                    'value' => $code,
                    'label' => $color,
                    'style' => 'background-color : '.$code,
                );
            }
            $this->_options = $options;
        }
        return $this->_options;
    }

    public function toOptionArray() {
        return $this->getAllOptions();
    }

}