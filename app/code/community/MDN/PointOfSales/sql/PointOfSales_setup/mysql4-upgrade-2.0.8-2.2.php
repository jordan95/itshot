<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$installer=$this;
$installer->startSetup();

$prefix = Mage::getConfig()->getTablePrefix();
$paymentTrackerTableName = $prefix . "payment_tracker_payment";
$cashRegisterTableName = $prefix . "pos_cash_register";

$installer->run("
    ALTER TABLE  {$paymentTrackerTableName}
    ADD  `ptp_is_money_back` smallint NOT NULL DEFAULT  '0';
");

$installer->run("
-- DROP TABLE IF EXISTS {$cashRegisterTableName};
    CREATE TABLE {$cashRegisterTableName}
    (
        pcr_id int(10) unsigned NOT NULL auto_increment,
        pcr_date date default NULL,
        pcr_user_id INT(10) unsigned,
        pcr_store_id INT(10) unsigned,
        pcr_method VARCHAR(30),
        pcr_amount FLOAT,
        PRIMARY KEY  (`pcr_id`)
    )
");




$installer->endSetup();