<?php
class MDN_PointOfSales_Block_Product_Addproduct extends Mage_Core_Block_Template
{

    public function setStoreId($storeId)
    {
        $this->storeId = $storeId;
        return $this;
    }

    public function getStoreId()
    {
        return $this->storeId;
    }

}