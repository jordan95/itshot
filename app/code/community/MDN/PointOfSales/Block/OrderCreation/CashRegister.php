<?php

class MDN_PointOfSales_Block_OrderCreation_CashRegister extends MDN_PointOfSales_Block_OrderCreation_StepOne
{

    public function getInitCashPaymentMethodAsCombo()
    {
        $selectedPayment = $this->getCashInitPaymentMethod();
        $html = '<select name="init-cash-payment-method" id="init-cash-payment-method" style="width: 130px">';
        $paymentMethods = array_merge(array(''=>''), $this->getPaymentMethods());
        foreach ($paymentMethods as $paymentId => $paymentLabel) {
            $html .= '<option value="'.$paymentId.'" '.($selectedPayment == $paymentId ? 'selected' : '').'>'.$paymentLabel.'</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public function getCashInitAmount() {

        $pcr = Mage::helper('PointOfSales/CashRegister')->getCashRegisterModel();

        return $pcr->getpcr_amount();
    }

    public function getCashInitPaymentMethod() {

        $pcr = Mage::helper('PointOfSales/CashRegister')->getCashRegisterModel();

        return $pcr->getpcr_method();
    }

}