<?php

class MDN_PointOfSales_Block_OrderCreation_ProductView extends Mage_Core_Block_Template
{
    private $_product = null;

    public function setProduct($product)
    {
        $this->_product = $product;
        return $this;
    }

    public function getProduct()
    {
        return $this->_product;
    }


    public function getProductImageUrl(){

        try{
            $parentProduct = $this->getConfigurableProduct($this->getProduct());
            if($parentProduct === null)
                return (string) mage::helper('catalog/image')->init($this->getProduct(), 'image');
            else{
                return (string) mage::helper('catalog/image')->init($parentProduct, 'image');
            }
        }
        catch(Exception $e)
        {
            return '';
        }
    }

}