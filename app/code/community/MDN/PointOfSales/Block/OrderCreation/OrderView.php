<?php

class MDN_PointOfSales_Block_OrderCreation_OrderView extends Mage_Core_Block_Template
{
	private $_order = null;


    public function getFrom()
    {
        return $this->getRequest()->getParam('from');
    }

	/**
	 * Return order
	 *
	 * @return unknown
	 */
	public function getOrder()
	{
		if ($this->_order == null)
		{
			$orderId = $this->getRequest()->getParam('order_id');
			$this->_order = mage::getModel('sales/order')->load($orderId);
			Mage::register('current_order', $this->_order);
		}
		return $this->_order;
	}
	
	/**
	 * 
	 *
	 */
	public function getDownloadInvoiceLink()
	{
		$invoices = $this->getOrder()->getInvoiceCollection();
		$retour = '';
		foreach ($invoices as $invoice)
		{
			$retour = Mage::helper('adminhtml')->getUrl('adminhtml/sales_order_invoice/print', array('invoice_id' => $invoice->getId()));
		}
		return  $retour;
	}

    public function getDownloadCreditmemoLink()
    {
        $cms = $this->getOrder()->getCreditmemosCollection();
        $retour = '';
        foreach ($cms as $cm)
        {
            $retour = Mage::helper('adminhtml')->getUrl('adminhtml/sales_order_creditmemo/print', array('creditmemo_id' => $cm->getId()));
        }
        return  $retour;
    }
	
	/**
	 * 
	 *
	 */
	public function getDownloadShipmentLink()
	{
		$shipments = $this->getOrder()->getShipmentsCollection();
		$retour = '';
		foreach ($shipments as $shipment)
		{
			$retour = Mage::helper('adminhtml')->getUrl('adminhtml/sales_order_shipment/print', array('invoice_id' => $shipment->getId()));
		}
		return  $retour;
	}

        /**
         * 
         */
        public function getDownloadOrderUrl()
        {
                return Mage::helper('adminhtml')->getUrl('adminhtml/PointOfSales_PointOfSales/downloadOrder', array('order_id' => $this->getOrder()->getId()));

        }

        public function getDownloadReceiptUrl(){
            return Mage::helper('adminhtml')->getUrl('adminhtml/PointOfSales_PointOfSales/downloadReceipt', array('order_id' => $this->getOrder()->getId()));
        }

		public function getDownloadInvoiceUrl(){
			return Mage::helper('adminhtml')->getUrl('adminhtml/PointOfSales_PointOfSales/downloadInvoice', array('order_id' => $this->getOrder()->getId()));
		}

	/**
	 * Enter description here...
	 *
	 */
	public function getPrintInvoiceLink()
	{
		return Mage::helper('adminhtml')->getUrl('adminhtml/PointOfSales_PointOfSales/printInvoice', array('order_id' => $this->getOrder()->getId()));
	}
		
	/**
	 * Enter description here...
	 *
	 */
	public function getPrintShipmentLink()
	{
		return Mage::helper('adminhtml')->getUrl('adminhtml/PointOfSales_PointOfSales/printShipment', array('order_id' => $this->getOrder()->getId()));
	}

        public function getPrintOrderLink()
        {
            return Mage::helper('adminhtml')->getUrl('adminhtml/PointOfSales_PointOfSales/printOrder', array('order_id' => $this->getOrder()->getId()));
        }
	
	/**
	 * 
	 *
	 */
	public function getDisplayOrderLink()
	{
		return Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view', array('order_id' => $this->getOrder()->getId()));
	}

        public function getNewOrderUrl()
        {
            return Mage::helper('adminhtml')->getUrl('adminhtml/PointOfSales_PointOfSales/StepOne');
        }
	
	/**
	 * 
	 *
	 * @return unknown
	 */
	public function getOrderId()
	{
		return $this->getOrder()->getincrement_id();
	}
	
	public function getOrderInfoData()
    {
        return array(
            'no_use_order_link' => true,
        );
    }

    /**
     * return currency to use
     *
     * @return unknown
     */
    public function getCurrency() {
        return mage::helper('PointOfSales/User')->getCurrency();
    }
	
	public function getOrderTotal()
	{
		return $this->getOrder()->getgrand_total();
	}

	public function autoReceiptDownload()
	{
		return ($this->getRequest()->getParam('print_receipt') == 1);
	}

	public function autoInvoiceDownload()
	{
		return ($this->getRequest()->getParam('print_invoice') == 1);
	}


}