<?php
class MDN_PointOfSales_Block_OrderCreation_ConfigurableProductOptions extends Mage_Core_Block_Template
{
    protected $product = null;
    protected $configurableOptions = null;

    public function setProduct($productId)
    {
        $this->product = mage::getModel('catalog/product')->load($productId);
        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function getConfigurableOptions()
    {
        if($this->configurableOptions !== null)
            return $this->configurableOptions;

        // Collect options applicable to the configurable product
        $productAttributeOptions = $this->getProduct()->getTypeInstance(true)->getConfigurableAttributesAsArray($this->getProduct());

        //die("<pre>".print_r($productAttributeOptions, true));
        $attributeOptions = array();
        foreach ($productAttributeOptions as $productAttribute) {
            $attributeOptions[$productAttribute['attribute_code']] = array(
                'label' => $productAttribute['label']
            );
            foreach ($productAttribute['values'] as $attribute) {
                $attributeOptions[$productAttribute['attribute_code']]['options'][$attribute['value_index']] = $attribute['store_label'];
            }
        }

        $this->configurableOptions = $attributeOptions;
        return $this->configurableOptions;
    }

    public function getChildren()
    {
        return Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $this->getProduct());
        foreach ($childProducts as $child) {
            $match = true;
            foreach ($attributes as $attributeCode => $value) {
                if ($child->getData($attributeCode) != $value) {
                    $match = false;
                    break;
                }
            }

            if ($match) {
                $productNameSuffix = mage::helper('PointOfSales/ProductInfo')->getProductAttributeLabelsAsText($child);
                $productToAdd = mage::helper('PointOfSales/ProductInfo')->getProductToAdd($child, $productNameSuffix);
                break;
            }
        }
    }

    public function getProductAttributeLabel($product, $attributeCode)
    {
        $attributes = $this->getProduct()->getTypeInstance(true)->getConfigurableAttributesAsArray($this->getProduct());

        foreach ($attributes as $attribute) {

            if($attribute['attribute_code'] != $attributeCode)
                continue;

            $productValue = $product->getData($attribute['attribute_code']);

            foreach ($attribute['values'] as $option) {
                if ($option['value_index'] == $productValue) {
                    return $option['default_label'];
                }
            }

        }

        return '-';
    }

    public function getImage($product)
    {
        try{
            $imageUrl = (string)mage::helper('catalog/image')->init($product, 'small_image')->resize(48, 48);
            if(strlen($imageUrl) < 10)
                throw new Exception ('No image for child product');

            return $imageUrl;
        }
        catch(Exception $e)
        {
            return (string)mage::helper('catalog/image')->init($this->getProduct(), 'small_image')->resize(48, 48);
        }

    }


}