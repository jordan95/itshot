<?php

class MDN_PointOfSales_Block_OrderCreation_ReturnView extends MDN_PointOfSales_Block_OrderCreation_OrderView
{

    public function getReturnableProducts()
    {

        $items = array();

        foreach ($this->getOrder()->getAllItems() as $item) {
            if($item->getData('qty_invoiced') == 0)
                continue;

            if($item->getData('qty_refunded') == $item->getData('qty_invoiced'))
                continue;

            $items[] = $item;
        }

        return $items;
    }

    public function getSmallImage($item)
    {
        try{
            $product = mage::getModel('catalog/product')->load($item->getproduct_id());
            $parentProduct = $this->getConfigurableProduct($product);
            if($parentProduct === null)
                return (string) mage::helper('catalog/image')->init($product, 'small_image')->resize(30, 30);
            else{
                return (string) mage::helper('catalog/image')->init($parentProduct, 'small_image')->resize(30, 30);
            }
        }
        catch(Exception $e)
        {
            return null;
        }
    }

    public function getReturnedQtyAsCombo($item)
    {
        $html = '';
        $html .= '<select class="returned-qty" data-order-item-id="'.$item->getId().'" data-price="'.$item->getprice().'">';
        for($i = 0; $i <= ($item->getData('qty_invoiced') - $item->getData('qty_refunded')); $i++)
            $html .= '<option value="'.$i.'">'.$i.'</option>';
        $html .= '</select>';
        return $html;
    }

    public function getPaymentMethodCombo()
    {
        $html = '<select name="return_payment_method" id="return_payment_method" style="width: 130px">';
        $paymentMethods = $this->getPaymentMethods();
        foreach ($paymentMethods as $paymentId => $paymentLabel) {
            $html .= '<option value="'.$paymentId.'">'.$paymentLabel.'</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public function getPaymentMethods() {
        $methods = array();

        $tmp = explode("\n", Mage::getStoreConfig('payment_tracker/general/custom_payment_method'));

        foreach($tmp as $method)
        {
            $method = trim($method);
            if($method)
                $methods[$method] = $method;
        }


        return $methods;
    }

}