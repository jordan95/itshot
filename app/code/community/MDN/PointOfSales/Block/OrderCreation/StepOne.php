<?php

class MDN_PointOfSales_Block_OrderCreation_StepOne extends Mage_Core_Block_Template {

    /**
     * return data serialized
     *
     * @return unknown
     */
    public function getRaw() {
        return mage::helper('PointOfSales/Serialization')->serializeObject(null);
    }

    /**
     * return payment methods
     *
     */
    public function getPaymentMethods() {
            $methods = array();

            $tmp = explode("\n", Mage::getStoreConfig('payment_tracker/general/custom_payment_method'));

            foreach($tmp as $method)
            {
                $method = trim($method);
                if($method)
                    $methods[$method] = $method;
            }


            return $methods;
    }

    /**
     * return shipping methods
     *
     */
    public function getShippingMethods() {
        $carriers = Mage::getStoreConfig('carriers', 0);
        $retour = array();
        foreach ($carriers as $item) {
            $instance = mage::getModel($item['model']);
            $instance->setmodel($item['model']);
            $retour[] = $instance;
        }
        return $retour;
    }

    /**
     * Return country as combo
     *
     * @param string $name
     * @return string
     */
    public function getCountryCombo($name) {

        $countries = Mage::getResourceModel('directory/country_collection')
            ->loadData()
            ->toOptionArray(false);

        $defaultCountry = mage::helper('PointOfSales/User')->getDefaultCountry();
        $defaultCountryId = $defaultCountry->getcountry_id();

        $html = '<select name="'.$name.'" id="'.$name.'" class="form-control">';

        foreach($countries as $country) {
            $selected = '';
            if ($country['value'] == $defaultCountryId) {
                $selected = 'selected';
            }
            $html .= '<option value="' . $country['value'] . '" ' . $selected . ' >' . $country['label'] . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    /**
     * Return combobox with regions for default country
     * @param <type> $name
     * @param <type> $value
     */
    public function getRegionCombo($name, $country = null) {

        return Mage::helper('PointOfSales/Customer')->getRegionAsCombo($name, $country);
    }

    /**
     * @param string $name
     * @return string customer groups as combo
     */
    public function getCustomerGroupCombo($name) {
        $groups = Mage::helper('customer')->getGroups();

        $html = '<select name="'.$name.'" id="'.$name.'" class="form-control">';


        foreach($groups as $group)
        {
            $selected = '';
            if ($group['value'] == $group) {
                $selected = 'selected';
            }
            $html .= '<option value="'.$group->getId().'" >'.$group->getCustomerGroupCode().'</option>';
        }

        $html .= '</select>';

        return $html;
    }

    /**
     * return currency to use
     *
     * @return unknown
     */
    public function getCurrency() {
        return mage::helper('PointOfSales/User')->getCurrency();
    }

    public function getCreateShipmentCheckedValue() {
        if (mage::getStoreConfig('pointofsales/configuration/default_shipment_tick'))
            return ' checked ';
        else
            return '';
    }

    public function getCreateInvoiceCheckedValue() {
        if (mage::getStoreConfig('pointofsales/configuration/default_invoice_tick'))
            return ' checked ';
        else
            return '';
    }

    public function getPaymentMethodCombo($i)
    {
        $html = '<select name="payment[MultiplePayment][method]['.$i.']" id="payment-'.$i.'-method" style="width: 130px">';
        $paymentMethods = array_merge(array(''=>''), $this->getPaymentMethods());
        foreach ($paymentMethods as $paymentId => $paymentLabel) {
            $html .= '<option value="'.$paymentId.'">'.$paymentLabel.'</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public function getPaymentOptionsContent(){
        $block = $this->getLayout()->createBlock('core/template');
        $block->setTemplate('PointOfSales/ChangeCalculator.phtml');
        return $block->toHtml();
    }

    public function getStoreLogoUrl(){

        $image = Mage::getStoreConfig('pointofsales/appearance/logo', Mage::helper('PointOfSales/User')->getCurrentStoreId());

        if ($image) {
            $url = Mage::getStoreConfig('system/filesystem/media') . '/upload/image/' . $image;
            $url = str_replace('{{root_dir}}', substr(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB), 0, -1), $url);
            return $url;
        }

        $storeId = mage::helper('PointOfSales/User')->getCurrentStoreId();
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
        $logoSrc = Mage::getStoreConfig('design/header/logo_src', $storeId);
        if(empty($logoSrc))
        {
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            return null;
        }

        $imgSrc = $this->getSkinUrl($logoSrc);
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        return $imgSrc;
    }

    public function getBackgroundImageUrl() {

        $url = "";
        $image = Mage::getStoreConfig(
            'pointofsales/appearance/background',
            Mage::helper('PointOfSales/User')->getCurrentStoreId()
        );

        if ($image) {
            $url = Mage::getStoreConfig('system/filesystem/media') . '/upload/image/' . $image;
            $url = str_replace(
                '{{root_dir}}',
                substr(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB), 0, -1),
                $url
            );
        }

        return $url;
    }

    public function getReportViewUrl() {
        return Mage::helper('adminhtml')->getUrl('adminhtml/PointOfSales_Report/Main');
    }

    public function getStoreAsCombo(){
        $return = '';
        $return .= '<select id="export_store">';

        foreach (Mage::app()->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $return .= '<option value="'.$store->getId().'" '.(Mage::helper('PointOfSales/User')->getCurrentStoreId() == $store->getId() ? 'selected' : '').'>'.$store->getName().'</option>';
                }
            }
        }

        $return .= '</select>';

        return $return;
    }

    public function getFullStoreTree($glue) {

        $store = Mage::helper('PointOfSales/User')->getStore();
        $storename = $store->getName();
        $group = $store->getGroup();
        $groupname = $group->getName();
        $website = $group->getWebsite();
        $websitename = $website->getName();

        return $websitename.$glue.$groupname.$glue.$storename;
    }


    private function actionAsArray($href, $dataAction, $icon, $label) {

        return array(
            'href' => $href,
            'data-action' => $dataAction,
            'icon' => $icon,
            'label' => $label
        );
    }

    public function getCashInitAction() {

        return $this->actionAsArray(
            '#',
            'show-cash-init-modal',
            'fa fa-money',
            $this->__('Cash register init')
        );
    }

    public function getZReportAction()
    {
        return $this->actionAsArray(
            Mage::helper("adminhtml")->getUrl("adminhtml/PointOfSales_Report/printZreportTodayReceipt"),
            '',
            'fa fa-print',
            $this->__('Print Z-Report')
        );
    }

    public function getAddProductAction() {

        return $this->actionAsArray(
            '#',
            'show-add-product-modal',
            'fa fa-plus',
             $this->__('Create product')
        );
    }

    public function getOrderListAction() {

        return $this->actionAsArray(
            '#',
            'show-order-list-modal',
            'fa fa-search',
             $this->__('Search orders')
        );
    }

    public function getBackToMagentoAction() {

        return $this->actionAsArray(
            Mage::helper("adminhtml")->getUrl("adminhtml/index/index"),
            '',
            'fa fa-arrow-circle-left',
             $this->__('Back to Magento')
        );
    }

    public function getClearCartAction() {

        return $this->actionAsArray(
            Mage::helper("adminhtml")->getUrl("adminhtml/PointOfSales_PointOfSales/StepOne"),
            '',
            'fa fa-close',
             $this->__('Clear cart')
        );
    }

    public function getExportPaymentAction() {

        return $this->actionAsArray(
            '#',
            'show-export-modal',
            'fa fa-download',
            $this->__('Export payments')
        );

    }

    public function getChangeUserAction() {

        return $this->actionAsArray(
            '#',
            'show-user-modal',
            'fa fa-exchange',
             $this->__('Change user')
        );
    }

    public function getChangeStoreAction() {

        return $this->actionAsArray(
            '#',
            'show-store-modal',
            'fa fa-user',
             $this->__('Change store')
        );
    }

    public function getGlobalConfAction() {

        return $this->actionAsArray(
            Mage::helper('adminhtml')->getUrl('adminhtml/system_config/edit', array('section' => 'pointofsales')),
            '',
            'fa fa-sliders',
             $this->__('Global configuration')
        );
    }

    public function getReportAction() {

        return $this->actionAsArray(
            '#',
            'show-statistics',
            'fa fa-bar-chart',
            $this->__('Statistics')
        );
    }

    public function getUserSettingsAction() {

        return $this->actionAsArray(
            Mage::helper('adminhtml')->getUrl('adminhtml/permissions_user/edit',
                array('user_id' => Mage::helper('PointOfSales/User')->getCurrentUser()->getId())
            ),
            '',
            'fa fa-sliders',
            $this->__('User settings')
        );
    }

    public function getAvailableActions() {

        $actions = array();

        $actions[] = $this->getBackToMagentoAction();
        $actions[] = $this->getClearCartAction();
        $actions[] = $this->getCashInitAction();
        $actions[] = $this->getOrderListAction();
        $actions[] = $this->getExportPaymentAction();

        if (Mage::getSingleton('admin/session')->isAllowed('admin/sales/pointofsales/createproduct')) {
            $actions[] = $this->getAddProductAction();
        }
        if(mage::getStoreConfig('pointofsales/configuration/allow_user_change', mage::helper('PointOfSales/User')->getCurrentStoreId()))
            $actions[] = $this->getChangeUserAction();

        if(mage::getStoreConfig('pointofsales/configuration/allow_store_change', mage::helper('PointOfSales/User')->getCurrentStoreId()))
            $actions[] = $this->getChangeStoreAction();

        if (Mage::getSingleton('admin/session')->isAllowed('admin/system/config/pointofsales'))
            $actions[] = $this->getGlobalConfAction();

        if (Mage::getSingleton('admin/session')->isAllowed('admin/system/acl/users'))
            $actions[] = $this->getUserSettingsAction();

        if (Mage::getSingleton('admin/session')->isAllowed('admin/smartreport/sales/pointofsales'))
            $actions[] = $this->getReportAction();

        $actions[] = $this->getZReportAction();

        return $actions;
    }

    public function getActionsAsMosaic() {

        $html = '';
        $i = 0;
        $actions = $this->getAvailableActions();

        foreach($actions as $action) {
            if ($i % 4 == 0) {
                if ($i != 0) {
                    $html .= "</ul>";
                }
                $html .= "<ul class='list-inline row text-center'>";
            }
            $html .= '<li class="col-sm-3 col-md-3 col-lg-3">';
            $html .= '<a ';
            $html .= 'href="'.$action['href'].'" ';
            $html .= 'data-action="'.$action['data-action'].'" ';
            $html .= 'class="btn btn-default btn-action-menu" style="vertical-align: middle">';
            $html .= '<i class="'.$action['icon'].' fa-4x" style="padding-top: 25px"></i><br>';
            $html .= $this->__($action['label']).' </a></li>';

            $i++;
        }
        if($i > 0)
            $html .= "</ul>";

        return $html;
    }
}