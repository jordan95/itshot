<?php
class MDN_PointOfSales_Block_OrderCreation_SalesList extends Mage_Core_Block_Template
{

    const kLimit = 6;

    private $storeId = null;
    private $searchTerm = null;
    private $users = null;
    private $pageCount = null;
    private $page = null;

    public function setStoreId($storeId)
    {
        $this->storeId = $storeId;
        return $this;
    }

    public function getStoreId()
    {
        return $this->storeId;
    }

    public function setSearchTerm($searchTerm)
    {
        $this->searchTerm = $searchTerm;
        return $this;
    }

    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    public function setScreenHeight($screenHeight)
    {
        $this->screenHeight = $screenHeight;
        return $this;
    }

    public function getScreenHeight()
    {
        return $this->screenHeight;
    }

    public function setCurrentPage($page)
    {
        $this->page = $page;
        return $this;
    }

    public function getCurrentPage()
    {
        return $this->page;
    }

    public function getLimit()
    {
        if(($screenHeight = $this->getScreenHeight()) < 728)
            return self::kLimit;

        if($screenHeight >=728 && $screenHeight <= 800)
            return 6;

        return 12;
    }



    public function getOrders($withPagination = true)
    {
        if (Mage::helper('PointOfSales/MagentoVersionCompatibility')->ordersUseEavModel()) {
            $collection = Mage::getResourceModel('sales/order_collection')
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('state', array('nin' => array('canceled')))
                ->joinAttribute('billing_firstname', 'order_address/firstname', 'billing_address_id', null, 'left')
                ->joinAttribute('billing_lastname', 'order_address/lastname', 'billing_address_id', null, 'left')
                ->joinAttribute('shipping_firstname', 'order_address/firstname', 'shipping_address_id', null, 'left')
                ->joinAttribute('shipping_lastname', 'order_address/lastname', 'shipping_address_id', null, 'left')
                ->addExpressionAttributeToSelect('billing_name',
                    'CONCAT({{billing_firstname}}, " ", {{billing_lastname}})',
                    array('billing_firstname', 'billing_lastname'));
        } else {
            $collection = mage::getModel('sales/order')
                ->getCollection()
                ->addAttributeToFilter('state', array('nin' => array('canceled')))
                ->join('sales/order_address', '`sales/order_address`.entity_id=billing_address_id', array('billing_name' => "concat(firstname, ' ', lastname)"));
        }

        if (Mage::getStoreConfig('pointofsales/configuration/sales_filter_per_store'))
            $collection->addFieldToFilter('store_id', $this->getStoreId());

        $collection = $this->addSearchFilter($collection);
        $collection->setOrder('created_at', 'DESC');

        if($withPagination)
        {
            $collection->setPageSize($this->getLimit());
            $collection->setCurPage($this->getCurrentPage());
        }

        //die($collection->getSelect());

        return $collection;
    }

    public function getPageCount()
    {
        if($this->pageCount !== null)
            return $this->pageCount;

        $size = $this->getOrders()->getSize();
        $this->pageCount = intval($size / $this->getLimit());
        return $this->pageCount;
    }

    public function addSearchFilter($collection)
    {
        $term = $this->getSearchTerm();
        if(empty($term))
            return $collection;
        $term = str_replace('"', "", $term);
        $term = str_replace("'", "", $term);

        $connexion = mage::getResourceModel('sales/order_item_collection')->getConnection();
        $sql = 'select entity_id from '.Mage::getConfig()->getTablePrefix().'sales_flat_order_grid where increment_id like "%'.$term.'%" or billing_name like "%'.$term.'%"';
        $orderIds = $connexion->fetchCol($sql);

        if (count($orderIds)> 0)
            $collection->addFieldToFilter('entity_id', $orderIds);
        else
            $collection->addFieldToFilter('entity_id', array(-1));

        return $collection;
    }


    public function renderProducts($row)
    {
        $product_list = "";
        $count = 0;
        $max = 4;

        foreach($row->getItemsCollection() as $item)
        {
            if ($count < $max)
            {
                $qte = (int)$item->getqty_ordered();
                $product_list .= $qte."x ".$item->getname().'<br>';
            }
            else
            {
                if ($count == $max)
                    $product_list .= '[...]';
            }
            $count++;
        }

        return $product_list;
    }

    public function renderPayments($row)
    {
        $html = '?';
        try
        {
            if ($row->getPayment())
            {

                if($row->getPayment()->getMethodInstance()->getcode() == 'MultiplePayment')
                    $html = mage::helper('PointOfSales/Payment')->getMultiplePaymentsAsText($row);
                else
                    $html = $row->getPayment()->getMethodInstance()->gettitle();
            }

        }
        catch(Exception $ex)
        {
            $html = $ex->getMessage();
        }
        return $html;
    }

    public function renderUser($row)
    {
        if($this->users === null)
            $this->users = mage::helper('PointOfSales/User')->getUserOptions();

        if(isset($this->users[$row['pos_user_id']]))
            return $this->users[$row['pos_user_id']];

        return '';
    }

}