<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boostmyshop (http://www.boostmyshop.com)
 * @author : Guillauem SARRAZIN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_PointOfSales_Block_System_Config_Version extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $version = $this->getVersionFromCoreResource('PointOfSales');
        $html = '<p><b>Point Of Sales : '.$version.'</b></p>';

        return $html;
    }

    /**
     * Get the version installed in the table core_resource
     * The version is relative with the install success of the script presents in the /sql folder of each module
     * @param string $modName
     * @return string
     */
    protected function getVersionFromCoreResource($modName){
       $postfix = '_setup';
       $tablePrefix = Mage::getConfig()->getTablePrefix();
       $sql = "select version from ".$tablePrefix."core_resource where code='".$modName.$postfix."'";
       $version = mage::getResourceModel('sales/order_item_collection')->getConnection()->fetchOne($sql);
       return $version;
    }   
    
}