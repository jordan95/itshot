<?php


class MDN_PointOfSales_Block_Report_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    protected $_dummyBlock = null;

    public function __construct() {
        parent::__construct();
        $this->setId('pos_report_tabs');
        $this->setDestElementId('tabs_container');
        $this->setTitle(Mage::helper('customer')->__('Statistics'));
        $this->setTemplate('PointOfSales/Report/Tabs.phtml');
    }

    protected function _beforeToHtml() {

        $reports = array(  '1299999999' => 'Sales details', '15974854' => 'Payments', '155555648521' =>  'Sales Total', '124842' => 'Sales per user', '1597532845' => 'Tax');
        foreach($reports as $reportId => $reportTitle)
        {
            $block = $this->getLayout()->createBlock('PointOfSales/Report_Type')->setTemplate('SmartReport/Report/Type.phtml');
            $block->setReportId($reportId);
            $this->addTab('tab_'.$reportId, array(
                'label' => Mage::helper('PointOfSales')->__($reportTitle),
                'content' => $block->toHtml()
            ));
        }

        //add Z Report tab
        $content = $this->getLayout()->createBlock('PointOfSales/Report_Zreport')->setTemplate('PointOfSales/Report/Zreport.phtml')->toHtml();
        $this->addTab('tab_z', array(
            'label' => Mage::helper('PointOfSales')->__('Z-Report'),
            'content' => $content
        ));


        return parent::_beforeToHtml();
    }

    public function getDummyBlock()
    {
        if (!$this->_dummyBlock)
        {
            $this->_dummyBlock = $this->getLayout()->createBlock('PointOfSales/Report_Type');
        }
        return $this->_dummyBlock;
    }

}
