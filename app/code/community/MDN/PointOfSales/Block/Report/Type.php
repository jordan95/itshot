<?php


class MDN_PointOfSales_Block_Report_Type extends MDN_SmartReport_Block_Report_Type {

    public function getReports()
    {
        $reports = Mage::getModel('SmartReport/Report')->getReportById($this->getReportId());
        return array($reports);
    }

    public function getTitle()
    {

    }


    public function getDisableHeader()
    {
        return true;
    }

}
