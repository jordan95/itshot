<?php

class MDN_PointOfSales_Block_Report_Zreport extends Mage_Core_Block_Template
{

    public function getReportData()
    {
        $variables = Mage::helper('SmartReport')->getVariables();
        $from = $variables['date_from'];
        $to = $variables['date_to'];
        $storeIds = $variables['sm_store'];

        return Mage::getModel('PointOfSales/Report_Zreport')->getReport($from, $to, $storeIds);
    }

    public function getPrintReceiptUrl()
    {
        return Mage::helper('adminhtml')->getUrl('*/*/printZreportReceipt');
    }

    public function getPrintA4Url()
    {
        return Mage::helper('adminhtml')->getUrl('*/*/printZreportA4');
    }

}
