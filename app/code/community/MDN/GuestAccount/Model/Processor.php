<?php

class MDN_GuestAccount_Model_Processor {

    public function process($data)
    {
        //check that email doesnt exist
        if ($this->emailExists($data['email'], $data['website_id']))
            throw new Exception('This email is already used for an account');
        
        //check that an order with this email & order number exists
        $order = $this->findOrder($data['email'], $data['order_id']);
        if (!$order)
            throw new Exception('Unable to find and order matching to this information');
        
        //set customer
        $customer = Mage::getModel('customer/customer')->setId(null);
        foreach($data as $key => $value)
        {
            $customer[$key] = $value;
        }
        $customer->setPassword($data['password']);
        $customer->setConfirmation($data['confirmation']);
        $customer->validate();
        
        //create customer
        $customer->save();
        Mage::dispatchEvent('customer_register_success',
            array('account_controller' => $this, 'customer' => $customer)
        );

        //send email
        $customer->sendNewAccountEmail(
            'registered',
            '',
            $data['store_id']
        );
        
        //associate order to customer
        $this->associateOrderToCustomer($customer, $order);
        
        return $customer;
    }
    
    /**
     *
     * @param type $customer
     * @param type $order 
     */
    public function associateOrderToCustomer($customer, $order)
    {
        $oldCustomerName = $order->getCustomerName();
        
        //edit order
        $order->setcustomer_id($customer->getId())->setcustomer_is_guest(0);
        
        //change customer name & email in order
        $order->setcustomer_email($customer->getEmail());
        $order->setcustomer_firstname($customer->getFirstname());   
        $order->setcustomer_lastname($customer->getLastname());   
        $order->setcustomer_middlename($customer->getMiddlename());   
                
        //add comment in order
        $msg = Mage::helper('GuestAccount')->__('Customer changed from "%s" to "%s"', $oldCustomerName, $customer->getName());
        $order->addStatusHistoryComment($msg);
         
        $order->save();
        
        //edit sales flat order grid (not necessary, handled by magento events)
        //$sfo = Mage::getModel('sales/order_grid')->load($order->getId());
        //$sfo->setcustomer_id($customer->getId())->save();

        return $order;
    }
 
    /**
     * Check if email is already used for an account
     * @param type $email 
     */
    protected function emailExists($email, $websiteId)
    {
        $collection = Mage::getModel('customer/customer')
                        ->getCollection()
                        ->addFieldToFilter('email', $email);
        if (Mage::getModel('customer/customer')->getSharingConfig()->isWebsiteScope())
            $collection->addFieldToFilter('website_id', $websiteId);
        return ($collection->getSize() > 0);
    }
    
    /**
     *
     * @param type $email
     * @param type $orderId 
     */
    protected function findOrder($email, $orderId)
    {
        $order = Mage::getModel('sales/order')
                    ->getCollection()
                    ->addFieldToFilter('increment_id', $orderId)
                    ->addFieldToFilter('customer_email', $email)
                    ->getFirstItem();
        if (!$order->getId())
            return null;
        else
            return $order;
                    
    }
    
}