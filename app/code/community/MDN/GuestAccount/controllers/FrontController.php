<?php

class MDN_GuestAccount_FrontController extends Mage_Core_Controller_Front_Action {

    /**
     * Display form to create account
     */
    public function AccountFormAction() {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    /**
     * Submit account form 
     */
    public function SubmitAction() {

        //set data
        $data = $this->getRequest()->getPost();
        $data['website_id'] = Mage::app()->getStore()->getWebsiteId();
        $data['store_id'] = Mage::app()->getStore()->getId();

        $session = Mage::getSingleton('customer/session');

        try {

            $customer = Mage::getModel('GuestAccount/Processor')->process($data);
            $session->setCustomerAsLoggedIn($customer);        
            
            //redirect customer
            $session->addSuccess($this->__('Thanks for registering !'));
            $this->_redirect('customer/account/index', array('_secure'=>true));
        } catch (Exception $ex) {
            $session->setCustomerFormData($data);
            $session->addError($this->__($ex->getMessage()));
            $this->_redirect('*/*/AccountForm');
        }
    }
    
    
}