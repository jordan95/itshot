<?php

class MDN_GuestAccount_AdminController extends Mage_Adminhtml_Controller_Action {

    /**
     * Reutrn customers grid
     *
     */
    public function CustomerGridAction() {
        $this->loadLayout();
        $orderId = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($orderId);
        Mage::register('current_order', $order);
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('GuestAccount/Adminhtml_Sales_Order_View_Tab_GuestAccount')->toHtml()
        );
    }
    
    /**
     * Change customer for an order 
     */
    public function ChangeOrderCustomerAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $customerId = $this->getRequest()->getParam('customer_id');
        
        $order = Mage::getModel('sales/order')->load($orderId);
        $customer = Mage::getModel('customer/customer')->load($customerId);
        
        Mage::getModel('GuestAccount/Processor')->associateOrderToCustomer($customer, $order);
        
        //confirm & redirect
        Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Customer changed for order'));
        $this->_redirect('adminhtml/sales_order/view', array('order_id' => $orderId));
        
    }

}