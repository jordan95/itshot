<?php

class MDN_GuestAccount_Block_Adminhtml_Widget_Grid_Column_Renderer_SelectCustomer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $order = Mage::registry('current_order');
        
        $url = $this->getUrl('GuestAccount/Admin/ChangeOrderCustomer', array('customer_id' => $row->getId(), 'order_id' => $order->getId()));
        return '<a href="'.$url.'">'.$this->__('Select').'</a>';
    }

}