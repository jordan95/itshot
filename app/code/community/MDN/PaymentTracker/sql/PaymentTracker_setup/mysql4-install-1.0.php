<?php

$installer = $this;

$installer->startSetup();

$installer->run("

	CREATE TABLE  {$this->getTable('payment_tracker_payment')} (
	`ptp_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`ptp_order_id` INT NOT NULL ,
	`ptp_date` DATE NOT NULL ,
	`ptp_method` VARCHAR( 100 ) NOT NULL ,
	`ptp_amount` DECIMAL( 10, 2 ) NOT NULL ,
        ptp_comments varchar(255) NULL,
	INDEX (  `ptp_order_id` )
	) ENGINE = INNODB ;


");

$installer->endSetup();
