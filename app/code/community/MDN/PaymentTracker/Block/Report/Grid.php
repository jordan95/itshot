<?php

class MDN_PaymentTracker_Block_Report_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('PaymentReportGrid');
        $this->_parentTemplate = $this->getTemplate();
        $this->setEmptyText(Mage::helper('PaymentTracker')->__('No Items'));
        $this->setDefaultSort('created_at', 'desc');
    }

    protected function _prepareCollection() {

        
        $collection = Mage::getModel('sales/order')
                        ->getCollection()
                        ->addFieldToFilter(array('total_due', 'total_due'), array(array('gt' => 0)	, array('null' => true)))
                        ->addFieldToFilter('state', array('in' => array('new', 'pending', 'processing', 'complete', 'holded')))
                        ->join('sales/order_address', '`sales/order_address`.entity_id=billing_address_id', array('billing_name' => "concat(firstname, ' ', lastname)"));
						
        $dontConsiderOrdersBefore = Mage::getStoreConfig('payment_tracker/general/dont_consider_before');
        if ($dontConsiderOrdersBefore)
            $collection->addFieldToFilter('created_at', array('gt' => $dontConsiderOrdersBefore));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('created_at', array(
            'header' => Mage::helper('PaymentTracker')->__('Date'),
            'index' => 'created_at',
            'type' => 'datetime'
        ));

        $this->addColumn('increment_id', array(
            'header' => Mage::helper('PaymentTracker')->__('Order #'),
            'index' => 'increment_id'
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('PaymentTracker')->__('Bill to'),
            'index' => 'billing_name',
            'filter_index' => "concat(firstname, ' ', lastname)"
        ));

        $this->addColumn('grand_total', array(
            'header' => Mage::helper('PaymentTracker')->__('Grand total'),
            'index' => 'grand_total',
            'type' => 'currency',
            'currency' => 'base_currency_code'
        ));

        $this->addColumn('total_paid', array(
            'header' => Mage::helper('PaymentTracker')->__('Total paid'),
            'index' => 'total_paid',
            'type' => 'currency',
            'currency' => 'base_currency_code'
        ));

        $this->addColumn('total_due', array(
            'header' => Mage::helper('PaymentTracker')->__('Total due'),
            'index' => 'total_due',
            'type' => 'currency',
            'currency' => 'base_currency_code'
        ));


        $this->addColumn('view_order', array(
            'header' => Mage::helper('PaymentTracker')->__('View'),
            'renderer' => 'MDN_PaymentTracker_Block_Widget_Grid_Column_Renderer_ViewOrder',
            'filter' => false,
            'is_system' => true

        ));

        $this->addExportType('*/*/exportPaymentPendingGridCsv', Mage::helper('PaymentTracker')->__('CSV'));
        $this->addExportType('*/*/exportPaymentPendingGridExcel', Mage::helper('PaymentTracker')->__('XML Excel'));

        return parent::_prepareColumns();
    }

    public function getGridParentHtml() {
        $templateName = Mage::getDesign()->getTemplateFilename($this->_parentTemplate, array('_relative' => true));
        return $this->fetchView($templateName);
    }

    public function getRowUrl($row) {
        return $this->getUrl('adminhtml/sales_order/view', array('order_id' => $row->getId()));
    }

}
