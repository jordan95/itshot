<?php

class MDN_PaymentTracker_Block_Report_All extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('AllPaymentsGrid');
        $this->_parentTemplate = $this->getTemplate();
        $this->setEmptyText(Mage::helper('PaymentTracker')->__('No Items'));
        $this->setDefaultSort('ptp_date', 'desc');
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('PaymentTracker/Payment')
                        ->getCollection()
                        ->join('sales/order', 'entity_id=ptp_order_id')
                        ->join('sales/order_address', '`sales/order_address`.entity_id=billing_address_id', array('billing_name' => "concat(firstname, ' ', lastname)"));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('increment_id', array(
            'header' => Mage::helper('PaymentTracker')->__('Order #'),
            'index' => 'increment_id'
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('PaymentTracker')->__('Bill to'),
            'index' => 'billing_name',
            'filter_index' => "concat(firstname, ' ', lastname)"
        ));

        $this->addColumn('ptp_date', array(
            'header' => Mage::helper('PaymentTracker')->__('Payment Date'),
            'index' => 'ptp_date',
            'type' => 'date'
        ));

        $this->addColumn('ptp_method', array(
            'header' => Mage::helper('PaymentTracker')->__('Method'),
            'index' => 'ptp_method'
        ));

        $this->addColumn('ptp_amount', array(
            'header' => Mage::helper('PaymentTracker')->__('Amount'),
            'index' => 'ptp_amount',
            'type' => 'number'
        ));


        $this->addColumn('ptp_comments', array(
            'header' => Mage::helper('PaymentTracker')->__('Comments'),
            'index' => 'ptp_comments'
        ));


        $this->addExportType('*/*/exportPaymentAllGridCsv', Mage::helper('PaymentTracker')->__('CSV'));
        $this->addExportType('*/*/exportPaymentAllGridExcel', Mage::helper('PaymentTracker')->__('XML Excel'));

        return parent::_prepareColumns();
    }

    public function getGridParentHtml() {
        $templateName = Mage::getDesign()->getTemplateFilename($this->_parentTemplate, array('_relative' => true));
        return $this->fetchView($templateName);
    }

    public function getRowUrl($row) {
        return $this->getUrl('adminhtml/sales_order/view', array('order_id' => $row->getentity_id()));
    }

}
