<?php

class MDN_PaymentTracker_Block_Widget_Grid_Column_Renderer_Payment_Action
	extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $payment)
    {
        $html = '';

        $html .= '<a href="#" onclick="objPaymentTracker.deletePayment('.$payment->getId().')">'.$this->__('delete').'</a>';

        return $html;
    }

}