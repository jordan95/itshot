<?php

class MDN_PaymentTracker_Block_Widget_Grid_Column_Renderer_ViewOrder
	extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $order)
    {
        $html = '';

        $html .= '<a href="'.$this->getUrl('adminhtml/sales_order/view', array('order_id' => $order->getId())).'">'.$this->__('View').'</a>';

        return $html;
    }

}