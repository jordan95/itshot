<?php

class MDN_PaymentTracker_Block_Sales_Order_View_Tab_Payments extends Mage_Adminhtml_Block_Sales_Order_Abstract implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('PaymentTracker/PaymentTab.phtml');
    }

    /**
     * Retrieve order model instance
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder() {
        return Mage::registry('current_order');
    }

    public function getPaymentsBlock()
    {
        $block = $this->getLayout()->createBlock('PaymentTracker/Sales_Order_View_Tab_Payments_Grid');
        $block->setOrder($this->getOrder());
        return $block->toHtml();
    }

    public function getAddPaymentUrl()
    {
        return Mage::helper('adminhtml')->getUrl('adminhtml/PaymentTracker_Order/AddPayment');
    }

    public function getUpdateTabUrl()
    {
        return Mage::helper('adminhtml')->getUrl('adminhtml/PaymentTracker_Order/UpdatePaymentTab', array('order_id' => $this->getOrder()->getId()));
    }

    public function getDeletePaymentUrl()
    {
        return Mage::helper('adminhtml')->getUrl('adminhtml/PaymentTracker_Order/DeletePayment');
    }

    public function getTotalPaid()
    {
        return $this->getOrder()->formatPrice(Mage::helper('PaymentTracker/Order')->getTotalPaid($this->getOrder()));
    }

    public function getPaymentStatus()
    {
        return Mage::helper('PaymentTracker/Order')->getPaymentStatus($this->getOrder());
    }

    public function getTotalDue($format = true)
    {
        $value = Mage::helper('PaymentTracker/Order')->getTotalDue($this->getOrder());
        if ($format)
            $value = $this->getOrder()->formatPrice($value);
        return $value;
    }

    public function canAddPayment()
    {
        return ($this->getTotalDue(false) > 0);
    }

    /**
     * Return payment method
     */
    public function getPaymentMethods()
    {
        return Mage::helper('PaymentTracker')->getPaymentMethods();
    }

    /**
     * ######################## TAB settings #################################
     */
    public function getTabLabel() {
        return Mage::helper('PaymentTracker')->__('Payment records');
    }

    public function getTabTitle() {
        return Mage::helper('PaymentTracker')->__('Payment records');
    }

    public function canShowTab()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/sales/order/payments');
    }

    public function isHidden() {
        return false;
    }

}
