<?php

class MDN_PaymentTracker_Block_Sales_Order_View_Tab_Payments_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('PaymentsGrid');
        $this->_parentTemplate = $this->getTemplate();
        $this->setEmptyText(Mage::helper('PaymentTracker')->__('No Items'));
        $this->setUseAjax(true);
        $this->setRowClickCallback(false);
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('PaymentTracker/Payment')
        	->getCollection()
                ->addFieldToFilter('ptp_order_id', $this->getOrder()->getId());


        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('ptp_date', array(
            'header'=> Mage::helper('PaymentTracker')->__('Date'),
            'index' => 'ptp_date',
            'type' => 'date'
        ));

        $this->addColumn('ptp_method', array(
            'header'=> Mage::helper('PaymentTracker')->__('Method'),
            'index' => 'ptp_method'
        ));

        $this->addColumn('ptp_amount', array(
            'header'=> Mage::helper('PaymentTracker')->__('Amount'),
            'index' => 'ptp_amount',
            'type' => 'price'
        ));


        $this->addColumn('ptp_comments', array(
            'header'=> Mage::helper('PaymentTracker')->__('Comments'),
            'index' => 'ptp_comments'
        ));

        $this->addColumn('action', array(
            'header'=> Mage::helper('PaymentTracker')->__('Action'),
            'renderer' => 'MDN_PaymentTracker_Block_Widget_Grid_Column_Renderer_Payment_Action',
            'filter' => false,
            'align' => 'center'
        ));

        return parent::_prepareColumns();
    }

    public function getGridParentHtml()
    {
        $templateName = Mage::getDesign()->getTemplateFilename($this->_parentTemplate, array('_relative'=>true));
        return $this->fetchView($templateName);
    }

    public function getGridUrl() {
        return $this->getUrl('adminhtml/PaymentTracker_Order/PaymentsGridAjax', array('_current' => true, 'order_id' => $this->getOrder()->getId()));
    }

}
