<?php

class MDN_PaymentTracker_Block_Payment_MultiplePayment_Info extends Mage_Payment_Block_Info {

    protected $_order = null;

    protected function _construct() {
        parent::_construct();
        $this->setTemplate('PaymentTracker/MultiplePayment/Info.phtml');
    }

    public function getOrder()
    {
        if ($this->_order == null)
        {
            $orderId = $this->getInfo()->getparent_id();
            $this->_order = Mage::getModel('sales/order')->load($orderId);
        }
        return $this->_order;
    }

    public function getPayments()
    {
        if ($this->getOrder()) {
            $collection = Mage::getModel('PaymentTracker/Payment')
                ->getCollection()
                ->addFieldToFilter('ptp_order_id', $this->getOrder()->getId());
            return $collection;
        }
        else
            return array();
    }

    public function formatAmount($value)
    {
        return $this->getOrder()->formatPrice($value);
    }

    public function getTotalDue()
    {
        return Mage::helper('PaymentTracker/Order')->getTotalDue($this->getOrder());
    }

    public function getTotalDueColor()
    {
        if ($this->getTotalDue() > 0)
            return 'red';
        else
            return 'green';
    }
}