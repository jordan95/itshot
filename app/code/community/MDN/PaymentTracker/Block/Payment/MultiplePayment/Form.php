<?php

class MDN_PaymentTracker_Block_Payment_MultiplePayment_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('PaymentTracker/MultiplePayment/form.phtml');
    }

    public function getMethodsDropDownMenu($i)
    {
        $html = '<select name="payment['.$this->getMethodCode().'][method]['.$i.']">';
        $html .= '<option></option>';
        foreach(Mage::helper('PaymentTracker')->getPaymentMethods() as $k => $v)
        {
            $html .= '<option value="'.$v.'">'.$v.'</option>';
        }

        $html .= '</select>';

        return $html;
    }

}