<?php

class MDN_PaymentTracker_Block_System_Config_Button_Initialize extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $url = $this->getUrl('adminhtml/PaymentTracker_Admin/Initialize');

        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setType('button')
                    ->setClass('scalable')
                    ->setLabel('Initialize')
                    ->setOnClick("setLocation('$url')")
                    ->toHtml();

        return $html;
    }
}