<?php

class MDN_PaymentTracker_Adminhtml_PaymentTracker_AdminController extends Mage_Adminhtml_Controller_Action {

    public function InitializeAction()
    {
        Mage::helper('PaymentTracker/Initialize')->process();

        Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Initialization done'));
        $this->_redirect('adminhtml/system_config/edit', array('section' => 'payment_tracker'));

    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/system/config/payment_tracker');
    }

}