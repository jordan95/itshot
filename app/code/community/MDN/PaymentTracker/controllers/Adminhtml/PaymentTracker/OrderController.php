<?php

class MDN_PaymentTracker_Adminhtml_PaymentTracker_OrderController extends Mage_Adminhtml_Controller_Action {

    /**
     * Add a payment
     */
    public function AddPaymentAction() {
        //init var
        $response = array();
        $response['error'] = false;
        $response['message'] = '';
        $data = $this->getRequest()->getPost();

        try {
            //Add payment
            $payment = Mage::getModel('PaymentTracker/Payment')->addPayment($data['method'],
                            $data['amount'],
                            $data['date'],
                            $data['comments'],
                            $data['order_id']);
        } catch (Exception $ex) {
            $response['error'] = true;
            $response['message'] = $ex->getMessage();
        }

        //return response
        $response = Zend_Json::encode($response);
        $this->getResponse()->setBody($response);
    }

    /**
     * Return payment grid in ajax
     */
    public function PaymentsGridAjaxAction() {
        $orderId = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($orderId);
        $this->loadLayout();
        $block = $this->getLayout()->createBlock('PaymentTracker/Sales_Order_View_Tab_Payments_Grid');
        $block->setOrder($order);
        $this->getResponse()->setBody($block->toHtml());
    }

    public function UpdatePaymentTabAction() {
        $orderId = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($orderId);
        Mage::register('current_order', $order);
        $this->loadLayout();
        $block = $this->getLayout()->createBlock('PaymentTracker/Sales_Order_View_Tab_Payments');
        $this->getResponse()->setBody($block->toHtml());
    }

    public function DeletePaymentAction() {

        $ptpId = $this->getRequest()->getParam('ptp_id');
        $payment = Mage::getModel('PaymentTracker/Payment')->load($ptpId);
        $payment->delete();
        
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/sales/order/payments');
    }

}
