<?php

class MDN_PaymentTracker_Adminhtml_PaymentTracker_ReportController extends Mage_Adminhtml_Controller_Action {

    public function PendingAction()
    {
        $this->loadLayout();
        $layout = $this->getLayout();
        $block = $layout->createBlock('PaymentTracker/Report_Grid')->setTemplate('PaymentTracker/Report/Grid.phtml');
        $this->loadLayout()->_addContent($block);
        $this->renderLayout();
    }

    public function AllAction()
    {
        $this->loadLayout();
        $layout = $this->getLayout();
        $block = $layout->createBlock('PaymentTracker/Report_All')->setTemplate('PaymentTracker/Report/All.phtml');
        $this->loadLayout()->_addContent($block);
        $this->renderLayout();
    }

    public function exportPaymentAllGridCsvAction()
    {
        $fileName = 'all_payments'.date('Y_M_D_H_i_s').'.csv';
        $content = $this->getLayout()->createBlock('PaymentTracker/Report_All')
            ->getCsv();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportPaymentAllGridExcelAction()
    {
        $fileName = 'all_payments'.date('Y_M_D_H_i_s').'.xls';
        $content = $this->getLayout()->createBlock('PaymentTracker/Report_All')
            ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportPaymentPendingGridCsvAction()
    {
        $fileName = 'pending_payments'.date('Y_M_D_H_i_s').'.csv';
        $content = $this->getLayout()->createBlock('PaymentTracker/Report_Grid')
            ->getCsv();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportPaymentPendingGridExcelAction()
    {
        $fileName = 'pending_payments'.date('Y_M_D_H_i_s').'.xls';
        $content = $this->getLayout()->createBlock('PaymentTracker/Report_Grid')
            ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/report/salesroot');
    }

}
