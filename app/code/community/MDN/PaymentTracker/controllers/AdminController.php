<?php

class MDN_PaymentTracker_AdminController extends Mage_Adminhtml_Controller_Action {

    public function InitializeAction()
    {
        Mage::helper('PaymentTracker/Initialize')->process();

        Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Initialization done'));
        $this->_redirect('adminhtml/system_config/edit', array('section' => 'payment_tracker'));

    }

}