<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_PaymentTracker_Model_Observer {

    public function sales_order_invoice_pay(Varien_Event_Observer $observer) {

        try
        {

            //init var
            $paymentMethod = null;
            $order = $observer->getEvent()->getInvoice()->getOrder();
            if ($order->getPayment())
                $paymentMethod = $order->getPayment()->getmethod();
            else {
                Mage::helper('PaymentTracker')->log('Order #'.$order->getIncrementId()).' has no payment method';
                return false;
            }

			//if we already have some payment for this order, skip
			if (Mage::helper('PaymentTracker/Order')->getTotalPaid($order) > 0) {
                Mage::helper('PaymentTracker')->log('Order #'.$order->getIncrementId()).' has total paid > 0, dont add payment record';
                return false;
            }

            //create payment
            if ($paymentMethod != 'MultiplePayment')
            {
                Mage::helper('PaymentTracker')->log('Create payment record for Order #'.$order->getIncrementId());
                Mage::helper('PaymentTracker/Order')->createPayment($order);
            }

        }
        catch(Exception $ex)
        {
            Mage::helper('PaymentTracker')->log('Error for Order #'.$order->getIncrementId().' : '.$ex->getMessage());
        }
    }

    public function sales_order_save_after(Varien_Event_Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        if ($order->getPayment()) {
            $paymentMethod = $order->getPayment()->getmethod();
            if ($paymentMethod == 'MultiplePayment')
            {
                if ($order->gettotal_paid() > $order->getgrand_total())
                {
                    Mage::helper('PaymentTracker/Order')->updateOrderPaidInformation($order->getId());
                }
            }
        }

    }

}
