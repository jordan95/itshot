<?php

class MDN_PaymentTracker_Model_Payment_MultiplePayment extends Mage_Payment_Model_Method_Abstract
{
    protected $_code  = 'MultiplePayment';
    protected $_formBlockType = 'PaymentTracker/Payment_MultiplePayment_Form';
    protected $_infoBlockType = 'PaymentTracker/Payment_MultiplePayment_Info';

    protected $_canUseCheckout = false;

    /**
     *
     * @return type
     */
    public function getInformation() {
        return $this->getConfigData('information');
    }

    /**
     *
     * @return type
     */
    public function getAddress() {
        return $this->getConfigData('address');
    }

    /**
     *
     * @return type
     */
    public function getTitle()
    {
        return $this->getConfigData('title');
    }

    /**
     *
     * @return type
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * Save payments
     *
     * @param $data
     * @return $this
     */
    public function assignData($data)
    {
        if (isset($data['MultiplePayment']))
        {
            $details = $data['MultiplePayment'];
            $this->getInfoInstance()->setAdditionalData(serialize($details));
        }

        return $this;
    }

    /**
     * Create payment records based on additional datas
     */
    public function validate()
    {
        $paymentInfo = $this->getInfoInstance();
        if ($paymentInfo instanceof Mage_Sales_Model_Order_Payment) {
            $orderId = $paymentInfo->getOrder()->getId();
            $rows = unserialize($paymentInfo->getAdditionalData());
            if (is_array($rows['method']))
            {
                foreach($rows['method'] as $k => $v)
                {
                    if ($v && $rows['amount'][$k])
                    {
                        $payment = Mage::getModel('PaymentTracker/Payment')->addPayment($v,
                            $rows['amount'][$k],
                            date('Y-m-d'),
                            '',
                            $orderId);
                    }
                }
            }
        }
    }


}