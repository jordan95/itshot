<?php


class MDN_PaymentTracker_Model_Payment_Status extends Mage_Core_Model_Abstract {

    const kFull = 'full';
    const kPartial = 'partial';
    const kMissing = 'missing';

    /**
     * Return payment statuses
     * @return <type>
     */
    public function getStatuses()
    {
        $statuses = array();

        $statuses[self::kfull] = Mage::helper('PaymentTracker')->__('Full');
        $statuses[self::kPartial] = Mage::helper('PaymentTracker')->__('Partial');
        $statuses[self::kMissing] = Mage::helper('PaymentTracker')->__('Missing');

        return $statuses;
    }
}