<?php


class MDN_PaymentTracker_Model_Payment extends Mage_Core_Model_Abstract {

    /**
     * Constructor
     */
    public function _construct() {
        parent::_construct();
        $this->_init('PaymentTracker/Payment');
    }

    /**
     * Add a new payment
     * @param <type> $method
     * @param <type> $amount
     * @param <type> $date
     * @param <type> $comments
     * @param <type> $orderId
     */
    public function addPayment($method, $amount, $date, $comments, $orderId)
    {
        $this->setptp_order_id($orderId);
        $this->setptp_date($date);
        $this->setptp_method($method);
        $this->setptp_amount($amount);
        $this->setptp_comments($comments);
        $this->save();

        Mage::helper('PaymentTracker/Order')->updateOrderPaidInformation($orderId);
        
        return $this;
    }


    /**
     *
     */
    protected function _afterDelete() {
        parent::_afterDelete();

        $orderId = $this->getptp_order_id();

        Mage::helper('PaymentTracker/Order')->updateOrderPaidInformation($orderId);
    }


}