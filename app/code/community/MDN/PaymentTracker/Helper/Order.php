<?php


class MDN_PaymentTracker_Helper_Order extends Mage_Core_Helper_Abstract {

    /**
     * Return total paid for order
     * @param <type> $orderId
     */
    public function getTotalPaid($order)
    {
        $tableName = Mage::getConfig()->getTablePrefix().'payment_tracker_payment';
        $sql = 'select SUM(ptp_amount) from '.$tableName.' where ptp_order_id = '.$order->getId();
        $value = mage::getResourceModel('sales/order_item_collection')->getConnection()->fetchOne($sql);
        if (!$value)
            $value = 0;
        return $value;
    }

    public function getTotalDue($order)
    {
        $orderTotal = $order->getgrand_total();
        $totalPaid = $this->getTotalPaid($order);
        $missing = $orderTotal - $totalPaid;
        return $missing;
    }

    /**
     * Return order status
     * @param <type> $orderId
     */
    public function getPaymentStatus($order)
    {
        $orderTotal = $order->getgrand_total();
        $missing = $this->getTotalDue($order);
        if ($missing == 0)
            return MDN_PaymentTracker_Model_Payment_Status::kFull;
        if ($missing == $orderTotal)
            return MDN_PaymentTracker_Model_Payment_Status::kMissing;
        return MDN_PaymentTracker_Model_Payment_Status::kPartial;
    }

    /**
     * Create payment for order
     * @param <type> $order 
     */
    public function createPayment($order)
    {
        $paymentMethod = $order->getPayment()->getmethod();
        $payment = Mage::getModel('PaymentTracker/Payment')->addPayment($paymentMethod,
                                                                        $order->getGrandTotal(),
                                                                        date('Y-m-d'),
                                                                        '',
                                                                        $order->getId());
        return $payment;
    }   

    /**
     * Update order totals (paid & due)
     * @param <type> $orderId
     */
    public function updateOrderPaidInformation($orderId)
    {
        //Get datas
        $order = Mage::getModel('sales/order')->load($orderId);
        $totalPaid = Mage::helper('PaymentTracker/Order')->getTotalPaid($order);
        $totalDue = Mage::helper('PaymentTracker/Order')->getTotalDue($order);

//        if($totalPaid == $order->gettotal_paid())
//            return;

        //update
        $order->setbase_total_paid($totalPaid)
            ->settotal_paid($totalPaid)
            ->setbase_total_due($totalDue)
            ->settotal_due($totalDue)
            ->save();

        return $order;
    }

}