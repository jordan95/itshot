<?php


class MDN_PaymentTracker_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * Return payment methods
     * @return <type>
     */
    public function getPaymentMethods()
    {
        $methods = array();

        $customMethods = explode("\n", Mage::getStoreConfig('payment_tracker/general/custom_payment_method'));
        foreach($customMethods as $elt)
        {
            if ($elt)
                $methods[$elt] = $elt;
        }

        $paymentmethods = Mage::getModel('Payment/config')->getAllMethods();
        foreach($paymentmethods as $paymentmethod)
        {
            $methods[$paymentmethod->getId()] = $paymentmethod->getTitle();
        }

        return $methods;
    }

    /**
     * Return true if we have to create the payment when invoice is created
     * Depending of the method passer in parameter
     * use system > configuration > payment tracker settings
     *
     * @param <type> $paymentMethod
     */
    public function autoCreatePaymentForMethod($paymentMethod)
    {
       $allowedMethods = explode(',', Mage::getStoreConfig('payment_tracker/general/payment_method'));       
       return in_array($paymentMethod, $allowedMethods);
    }

    public function log($msg)
    {
        Mage::log($msg, null, 'payment_tracker.log');
    }

}

?>