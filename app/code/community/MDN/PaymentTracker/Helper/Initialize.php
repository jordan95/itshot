<?php

class MDN_PaymentTracker_Helper_Initialize extends Mage_Core_Helper_Abstract {

    /**
     * Apply initialization
     */
    public function process() {
        //delete current payments
        $collection = Mage::getModel('PaymentTracker/Payment')
                        ->getCollection();
        foreach ($collection as $item) {
            $item->delete();
        }

        //get all order having specified payment methods and invoiced
        $allowedMethods = explode(',', Mage::getStoreConfig('payment_tracker/general/payment_method'));
        $collection = Mage::getModel('sales/order')
                        ->getCollection()
                        ->addFieldToFilter('total_invoiced', array('gt' => 0))
                        ->addFieldToFilter('method', array('in' => $allowedMethods))
                        ->join('sales/order_payment', 'main_table.entity_id = parent_id');

        //process orders
        foreach ($collection as $order) {
            $amount = $order->getgrand_total();
            if (!$order->getPayment())
                continue;
            $method = $order->getPayment()->getMethod();
            $date = $order->getcreated_at();

            Mage::getModel('PaymentTracker/Payment')->addPayment($method,
                    $amount,
                    $date,
                    'Initialized with payment tracker',
                    $order->getparent_id());
        }
    }

}