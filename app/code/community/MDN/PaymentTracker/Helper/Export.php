<?php


class MDN_PaymentTracker_Helper_Export extends Mage_Core_Helper_Abstract {

    const kLineReturn = "\r\n";

    /**
     * Return csv content
     * @param <type> $from
     * @param <type> $to
     */
    public function getCsv($from, $to)
    {
        $collection = Mage::getModel('PaymentTracker/Payment')
                        ->getCollection()
                        ->addFieldToFilter('ptp_date', array('gteq' => $from.' 00:00:00'))
                        ->addFieldToFilter('ptp_date', array('lteq' => $to.' 23:59:59'))
                        ->join('sales/order', 'ptp_order_id = entity_id');

        $csv = '';
        $pattern = 'payment_date;payment_method;payment_amount;payment_comments;order;customer;order_date;order_total;invoice'.self::kLineReturn;
        $csv .= $pattern;

        foreach($collection as $item)
        {
            $line = $pattern;
            $invoice = Mage::getModel('sales/order_invoice')->load($item->getptp_order_id(), 'order_id');

            $line = str_replace('payment_date', $item->getptp_date(), $line);
            $line = str_replace('payment_method', $item->getptp_method(), $line);
            $line = str_replace('payment_amount', $item->getptp_amount(), $line);
            $line = str_replace('payment_comments', $item->getptp_comments(), $line);
            $line = str_replace('customer', ($item->getcustomer_firstname().' '.$item->getcustomer_lastname()), $line);
            $line = str_replace('order_date', $item->getcreated_at(), $line);
            $line = str_replace('order_total', $item->getgrand_total(), $line);
            $line = str_replace('invoice', $invoice->getincrement_id(), $line);
            $line = str_replace('order', $item->getincrement_id(), $line);

            $csv .= $line;
        }

        return $csv;
        
    }

}