<?php

/*
 * @category Vivacity_Canonical
 * @package    Canonical
 * @author      Vivacity Infotech Pvt.Ltd.
 * @license    http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL v3.0)

 * 
 * Class for handling product conical url
 *  
 */

class Vivacity_Canonical_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getProductCanonicalUrl() {

        if (Mage::registry('product')) {
            $product = Mage::registry('product');
            $productId = $product->getId();
            $_resource = Mage::getSingleton('catalog/product')->getResource();
            $valueOpt = $_resource->getAttributeRawValue($productId, 'procanonicalurl', Mage::app()->getStore());
            $valueOpt = trim($valueOpt);
            if (isset($valueOpt) && !empty($valueOpt)) {
                $conUrl = $valueOpt;
            } else {
                $conUrl = $product->getProductUrl();
            }
            return $conUrl;
        }
    }

    public function getCatCanonicalUrl() {

        if (Mage::registry('current_category')) {
            $cat = Mage::registry('current_category');
            $catId = $cat->getId();
            $_resource = Mage::getSingleton('catalog/category')->getResource();
            $valueOpt = $_resource->getAttributeRawValue($catId, 'catcanonicalurl', Mage::app()->getStore());
            $valueOpt = trim($valueOpt);
            if (isset($valueOpt) && !empty($valueOpt)) {
                $conUrl = $valueOpt;
            } else {
                $conUrl = $cat->getUrl();
            }
            return $conUrl;
        }
    }

    public function getCMSCanonicalUrl() {

        if (Mage::app()->getFrontController()->getRequest()->getRouteName() == 'cms') {
            $cms = Mage::getBlockSingleton('cms/page');
            $ID = $cms->getPage()->getIdentifier();

            $CmsUrl = Mage::getModel('cms/page')->load($ID, 'identifier')->getCanonicalUrlValue();

            if (isset($CmsUrl) && !empty($CmsUrl)) {
                $conUrl = $CmsUrl;
            } else {
                $cmsPageUrl = Mage::getUrl() .Mage::getSingleton('cms/page')->getIdentifier();
                $conUrl = $cmsPageUrl;
            }
            return $conUrl;
        }
    }

}
