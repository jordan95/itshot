<?php

class Vivacity_Canonical_Model_Canurl {

    public function cmsField($canurl) {
        $model = Mage::registry('cms_page');

        $form = $canurl->getForm();

        $fieldset = $form->addFieldset('vivacity_customvalue_fieldset', array('legend' => Mage::helper('cms')->__('Canonical Url Panel'), 'class' => 'fieldset-wide'));
        //add new field
        $fieldset->addField('canonical_url_value', 'text', array(
            'name' => 'canonical_url_value',
            'label' => Mage::helper('cms')->__('Canonical Url'),
            'title' => Mage::helper('cms')->__('Canonical Url'),
            'disabled' => false,
            'note' => 'Full Page URL',
            //set field value
            'value' => $model->getCanonicalUrlValue()
        ));
    }

}
