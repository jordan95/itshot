<?php

$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
//$setup->removeAttribute('catalog_product', 'procanonicalurl');
//$setup->removeAttribute('catalog_category', 'catcanonicalurl');

$installer->addAttribute('catalog_product', 'procanonicalurl', array(
    'label' => 'Product Canonical Url',
    'input' => 'text',
    'type' => 'text',
    'note' => 'Full Page URL',
    'class' => '',
    'global' => true,
    'visible' => true,
    'required' => false,
    'user_defined' => false,
    'default' => '',
    'apply_to' => 'simple,configurable,bundle,grouped',
    'visible_on_front' => true,
    'is_configurable' => false,
    'wysiwyg_enabled' => false,
    'used_in_product_listing' => true,
    'is_html_allowed_on_front' => true,
    'group' => 'Meta Information',
    'sort_order' => '74',
));
$installer->addAttribute('catalog_category', 'catcanonicalurl', array(
    'label' => 'Category Canonical Url',
    'input' => 'text',
    'type' => 'text',
    'class' => '',
    'note' => 'Full Page URL',
    'global' => true,
    'visible' => true,
    'required' => false,
    'user_defined' => false,
    'default' => '',
    'apply_to' => 'simple,configurable,bundle,grouped',
    'visible_on_front' => true,
    'is_configurable' => false,
    'wysiwyg_enabled' => false,
    'used_in_categories_listing' => true,
    'is_html_allowed_on_front' => true,
    'group' => 'General Information',
    'sort_order' => '74',
));
$installer->run("ALTER TABLE {$this->getTable('cms_page')} ADD `canonical_url_value` varchar( 250 ) NOT NULL DEFAULT '';");

$installer->endSetup();
