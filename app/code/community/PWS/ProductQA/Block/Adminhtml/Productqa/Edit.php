<?php
class PWS_ProductQA_Block_Adminhtml_Productqa_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'pws_productqa';
        $this->_controller = 'adminhtml_productqa';

        $this->_updateButton('save', 'label', Mage::helper('pws_productqa')->__('Save Answer'));
        $this->_updateButton('delete', 'label', Mage::helper('pws_productqa')->__('Delete Question'));

    }

    public function getHeaderText()
    {
        if( Mage::registry('productqa') && Mage::registry('productqa')->getId() ) {
            return Mage::helper('pws_productqa')->__("Edit Answer", $this->htmlEscape(Mage::registry('productqa')->getTitle()));
        } else {
            return Mage::helper('pws_productqa')->__('New Question');
        }
    }
}
