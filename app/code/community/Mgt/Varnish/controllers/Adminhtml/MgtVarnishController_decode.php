<?php /** * MGT-Commerce GmbH * http://www.mgt-commerce.com * * NOTICE OF LICENSE * * This source file is subject to the Open Software License (OSL 3.0) * that is bundled with this package in the file LICENSE.txt. * It is also available through the world-wide-web at this URL: * http://opensource.org/licenses/osl-3.0.php * If you did not receive a copy of the license and are unable to * obtain it through the world-wide-web, please send an email * to info@mgt-commerce.com so we can send you a copy immediately. * * @category    Mgt * @package     Mgt_Varnish * @author      Stefan Wieczorek <stefan.wieczorek@mgt-commerce.com> * @copyright   Copyright (c) 2012 (http://www.mgt-commerce.com) * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0) */
class Mgt_Varnish_Adminhtml_MgtVarnishController extends Mage_Adminhtml_Controller_Action
{
    public function purgeAction()
    {
        $adminSession = $this->_getAdminSession();
        $helper = Mage::helper('mgt_varnish');
        try {
            $isEnabled = $helper->isEnabled();
            if (true === $isEnabled) {
                $domains = $helper->getStoreDomainList($this->getRequest()->getParam('stores', 0));
                $varnish = $this->_getVarnishModel();
                $varnish->purge($domains);
                $adminSession->addSuccess($helper->__('Varnish has been purged.'));
            }
        } catch (Mage_Core_Exception $e) {
            $adminSession->addError($e->getMessage());
        } catch (Exception $e) {
            $adminSession->addException($e, $helper->__('An error occurred while clearing the Varnish cache.'));
        }
        $this->_redirect('*/cache/index');
    }

    public function singlePurgeAction()
    {
        $adminSession = $this->_getAdminSession();
        $helper = Mage::helper('mgt_varnish');
        try {
            $isEnabled = $helper->isEnabled();
            if (true === $isEnabled) {
                $singlePurgeUrl = $this->getRequest()->getParam('single_purge_url', false);
                if (!$singlePurgeUrl) {
                    throw new Mage_Core_Exception($helper->__('Invalid URL "%s".', $singlePurgeUrl));
                }
                $domainList = $helper->getStoreDomainList();
                extract(parse_url($singlePurgeUrl));
                if (!isset($host)) {
                    throw new Mage_Core_Exception($helper->__('Invalid URL "%s".', $singlePurgeUrl));
                }
                if (!in_array($host, explode('|', $domainList))) {
                    throw new Mage_Core_Exception($helper->__('Invalid domain "%s".', $host));
                }
                $uri = '';
                if (isset($path)) {
                    $uri .= $path;
                }
                if (isset($query)) {
                    $uri .= '\?';
                    $uri .= $query;
                }
                if (isset($fragment)) {
                    $uri .= '#';
                    $uri .= $fragment;
                }
                $varnish = $this->_getVarnishModel();
                $varnish->purge($host, sprintf('^%s$', $uri));
                $adminSession->addSuccess($helper->__('The URL\'s "%s" cache has been purged.', $singlePurgeUrl));
            }
        } catch (Mage_Core_Exception $e) {
            $adminSession->addError($e->getMessage());
        } catch (Exception $e) {
            $adminSession->addException($e, $helper->__('An error occurred while clearing Varnish.'));
        }
        $this->_redirect('*/cache/index');
    }

    protected function _getVarnishModel()
    {
        return Mage::getModel('mgt_varnish/varnish');
    }

    protected function _getAdminSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }

    protected function _isAllowed()
    {
        return true;
    }
}