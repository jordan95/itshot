<?php
/**
 * MGT-Commerce GmbH
 * http://www.mgt-commerce.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@mgt-commerce.com so we can send you a copy immediately.
 *
 * @category    Mgt
 * @package     Mgt_Varnish
 * @author      Stefan Wieczorek <stefan.wieczorek@mgt-commerce.com>
 * @copyright   Copyright (c) 2017 (http://www.mgt-commerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Mgt_Varnish_Model_License {
    const MODULE_NAME = 'Mgt_Varnish';
    static protected $_hasValidLicense = array();
    public function hasValidLicense($currentHost) {
        return true;
    }
    protected function _isHostValid($currentHost, array $domains) {
        $isHostValid = false;
        foreach ($domains as $domain) {
            if (strstr($currentHost, $domain)) {
                $isHostValid = true;
                break;
            }
        }
        return $isHostValid;
    }
}
