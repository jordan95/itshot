<?php
/**
 * MGT-Commerce GmbH
 * http://www.mgt-commerce.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@mgt-commerce.com so we can send you a copy immediately.
 *
 * @category    Mgt
 * @package     Mgt_Varnish
 * @author      Stefan Wieczorek <stefan.wieczorek@mgt-commerce.com>
 * @copyright   Copyright (c) 2012 (http://www.mgt-commerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Mgt_Varnish_Model_Observer {
    const XML_PATH_MGT_VARNISH_ENABLED = 'mgt_varnish/mgt_varnish/enabled';
    const SET_CACHE_HEADER_FLAG = 'MGT_VARNISH_CONTROL_HEADERS_SET';

    //Observer for event `http_response_send_before`
    //control NO_CACHE_COOKIE
    //unset cookie when has quote or session so must run after action processing
    //set cookie when no quote or session 
    public function varnish(Varien_Event_Observer $observer) {
        $isEnabled = $this->_isEnabled();
        $hasValidLicense = true;
        $varnish = $this->_getVarnishModel();
        if (Mage::app()->getRequest()->isXmlHttpRequest()) {
            return $this;
        }
        if (true === $isEnabled && true === $hasValidLicense) {
            if (!Mage::registry(self::SET_CACHE_HEADER_FLAG)) {
                $varnish->setCacheControlHeaders();
                Mage::register(self::SET_CACHE_HEADER_FLAG, true);
            }
        } elseif (true === $isEnabled && false === $hasValidLicense) {
            $varnish->setNoCacheHeader();
        } elseif (false === $isEnabled && true === $hasValidLicense) {
            $varnish->setNoCacheHeader();
        }
        return $this;
    }
    protected function _hasValidLicense() {
        $license = Mage::getModel('mgt_varnish/license');
        $currentHost = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
        $hasValidLicense = $license->hasValidLicense($currentHost);
        return $hasValidLicense;
    }
    public function disableVarnish() {
        $isEnabled = $this->_isEnabled();
        $isAdminStore = Mage::app()->getStore()->isAdmin();
        if ((true === $isEnabled) || (true === $isAdminStore)) {
            $varnish = $this->_getVarnishModel();
            $varnish->setNoCacheHeader();
        }
    }
    public function checkIfDisabled(Varien_Event_Observer $observer) {
        $configData = $observer->getConfigData();
        if (isset($configData) && $configData->getPath() == self::XML_PATH_MGT_VARNISH_ENABLED) {
            $isDisabled = $configData->getValue() == 0 ? true : false;
            if (true === $isDisabled) {
                $varnish = $this->_getVarnishModel();
                $domains = Mage::helper('mgt_varnish')->getStoreDomainList();
                $varnish->purge($domains);
            }
        }
    }

    //Observer for event `http_response_send_before`
    //keep NO_CACHE_COOKIE updated on every request
    public function sanitizeCacheControlHeader() {
        $varnish = $this->_getVarnishModel();
        $varnish->sanitizeCacheControlHeader();
    }
    public function disablePermanent() {
        $isEnabled = $this->_isEnabled();
        if (true === $isEnabled) {
            $varnish = $this->_getVarnishModel();
            $varnish->setNoCacheCookie();
        }
        return $this;
    }
    protected function _isEnabled() {
        return Mage::helper('mgt_varnish')->isEnabled();
    }
    protected function _getVarnishModel() {
        return Mage::getModel('mgt_varnish/varnish');
    }
    public function purgeCatalogProductByStock(Varien_Event_Observer $observer) {
        $isEnabled = $this->_isEnabled();
        $stockItem = $observer->getEvent()->getItem();
        if (true === $isEnabled && ($productId = $stockItem->getProductId())) {
            try {
                $product = $stockItem->getProduct();
                if (!$product) {
                    $product = Mage::getModel('catalog/product');
                    $product->setId($productId);
                }
                if (isset($product) && !Mage::registry('mgt_varnish_catalog_product_purged_' . $product->getId())) {
                    $purger = Mage::getModel('mgt_varnish/purge_catalog_product');
                    $purger->purge($product, true, true);
                    Mage::register('mgt_varnish_catalog_product_purged_' . $product->getId(), true);
                }
            }
            catch(Exception $e) {
                Mage::helper('mgt_varnish')->debug('Error on save product purging: ' . $e->getMessage());
            }
        }
        return $this;
    }
    public function purgeCatalogProduct(Varien_Event_Observer $observer) {
        $isEnabled = $this->_isEnabled();
        $product = $observer->getEvent()->getProduct();
        if (true === $isEnabled && (isset($product) && $product->getId())) {
            try {
                if (!Mage::registry('mgt_varnish_catalog_product_purged_' . $product->getId())) {
                    $purger = Mage::getModel('mgt_varnish/purge_catalog_product');
                    $purger->purge($product, true, true);
                    Mage::register('mgt_varnish_catalog_product_purged_' . $product->getId(), true);
                }
            }
            catch(Exception $e) {
                Mage::helper('mgt_varnish')->debug('Error on save product purging: ' . $e->getMessage());
            }
        }
        return $this;
    }
    public function purgeCatalogCategory(Varien_Event_Observer $observer) {
        $isEnabled = $this->_isEnabled();
        if (true === $isEnabled && ($category = $observer->getEvent()->getCategory())) {
            try {
                if (!Mage::registry('mgt_varnish_catalog_category_purged_' . $category->getId())) {
                    $purger = Mage::getModel('mgt_varnish/purge_catalog_category');
                    $purger->purge($category);
                    Mage::register('mgt_varnish_catalog_category_purged_' . $category->getId(), true);
                }
            }
            catch(Exception $e) {
                Mage::helper('mgt_varnish')->debug('Error on save category purging: ' . $e->getMessage());
            }
        }
        return $this;
    }
    public function purgeCmsPage(Varien_Event_Observer $observer) {
        $isEnabled = $this->_isEnabled();
        $page = $observer->getEvent()->getObject();
        if (true === $isEnabled && (isset($page) && $page->getId())) {
            try {
                if (!Mage::registry('mgt_varnish_cms_page_purged_' . $page->getId())) {
                    $purger = Mage::getModel('mgt_varnish/purge_cms_page');
                    $purger->purge($page);
                    Mage::register('mgt_varnish_cms_page_purged_' . $page->getId(), true);
                }
            }
            catch(Exception $e) {
                Mage::helper('mgt_varnish')->debug('Error on save cms page purging: ' . $e->getMessage());
            }
        }
        return $this;
    }
    public function injectEsi(Varien_Event_Observer $observer) {
        $request = Mage::app()->getRequest();
        $moduleName = $request->getModuleName();
        $esiDataParam = $request->getParam('data', null);
        $block = $observer->getBlock();
        $helper = Mage::helper('mgt_varnish');
        $esiHelper = Mage::helper('mgt_varnish/esi');
        $canUseEsi = $esiHelper->canUseEsi();
        $isAjaxRequest = $request->isXmlHttpRequest();
        $uencParameter = $request->getParam('uenc');
        if (!$esiDataParam && true === $canUseEsi && ($block instanceof Mage_Core_Block_Template) && (false === $isAjaxRequest) && !$uencParameter) {
            $isEsiBlockLoggingEnabled = $esiHelper->isEsiBlockLoggingEnabled();
            if (true === $isEsiBlockLoggingEnabled) {
                $esiHelper->logBlockName($block);
            }
            $esiBlocks = $esiHelper->getEsiBlocks();
            $blockName = $block->getNameInLayout();
            if ($esiBlocks && $blockName && in_array($blockName, $esiBlocks)) {
                $esiData = $esiHelper->getEsiData($block);
                $frozenData = $helper->freeze($esiData);
                $urlOptions = array('data' => $frozenData, '_nosid' => 1);
                $esiUrl = Mage::getUrl('mgtvarnish/esi/render', $urlOptions);
                $esiUrl = str_replace('https', 'http', $esiUrl);
                $isEsiUrlLoggingEnabled = $esiHelper->isEsiUrlLoggingEnabled();
                if (true === $isEsiUrlLoggingEnabled) {
                    $esiHelper->logEsiUrl($block, $esiUrl);
                }
                $block->setEsiUrl($esiUrl);
                foreach (array('lifetime', 'tags', 'key') as $key) {
                    $block->unsetData(sprintf('cache_%s', $key));
                }
                $block->setTemplate('mgt_varnish/esi.phtml');
            }
        }
    }
}

