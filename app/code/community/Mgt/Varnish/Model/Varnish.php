<?php 
/**
 * MGT-Commerce GmbH
 * http://www.mgt-commerce.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@mgt-commerce.com so we can send you a copy immediately.
 *
 * @category    Mgt
 * @package     Mgt_Varnish
 * @author      Stefan Wieczorek <stefan.wieczorek@mgt-commerce.com>
 * @copyright   Copyright (c) 2012 (http://www.mgt-commerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Mgt_Varnish_Model_Varnish {
    const REGISTRY_MGT_VARNISH_CONTROL_HEADERS_SET_FLAG = 'mgt_varnish_control_headers_set_flag';
    const XML_PATH_MGT_VARNISH_EXCLUDED_PARAMS = 'mgt_varnish/mgt_varnish/excluded_params';
    const XML_PATH_MGT_VARNISH_EXCLUDED_ROUTES = 'mgt_varnish/mgt_varnish/excluded_routes';
    const XML_PATH_MGT_VARNISH_EXCLUDED_URLS = 'mgt_varnish/mgt_varnish/excluded_urls';
    const XML_PATH_MGT_VARNISH_ROUTES_CACHE_LIFETIME = 'mgt_varnish/mgt_varnish/routes_cache_lifetime';
    const XML_PATH_MGT_VARNISH_DEFAULT_CACHE_LIFETIME = 'mgt_varnish/mgt_varnish/default_cache_lifetime';
    const XML_PATH_MGT_VARNISH_SERVERS = 'mgt_varnish/mgt_varnish/servers';
    const XML_PATH_MGT_VARNISH_PORT = 'mgt_varnish/mgt_varnish/server_port';
    const VARNISH_HEADER_REGEX = 'X-Purge-Regex';
    const VARNISH_HEADER_HOST = 'X-Purge-Host';
    const VARNISH_HEADER_CONTENT_TYPE = 'X-Purge-Content-Type';
    const MGT_VARNISH_DEBUG_HEADER = 'X-Cache-Debug: 1';
    const NO_CACHE_COOKIE = 'MGT_NO_CACHE';
    static public $request;
    static public $response;
    public function setCacheControlHeaders() {
        if (Mage::registry(self::REGISTRY_MGT_VARNISH_CONTROL_HEADERS_SET_FLAG)) {
            return $this;
        } else {
            Mage::register(self::REGISTRY_MGT_VARNISH_CONTROL_HEADERS_SET_FLAG, 1);
        }
        $request = self::getRequest();
        $response = self::getResponse();
        $isDebug = Mage::helper('mgt_varnish')->isDebug();
        if (true === $isDebug) {
            $this->addDebugHeader();
        }
        $this->_checkNoCacheCookie();

        $excludedParams = trim(Mage::getStoreConfig(self::XML_PATH_MGT_VARNISH_EXCLUDED_PARAMS));
        if ($excludedParams) {
            $excludedParams = explode(',', $excludedParams);
            foreach ($excludedParams as $param) {
                if ($request->getParam(trim($param))) {
                    $this->setNoCacheHeader();
                    return $this->setNoCacheCookie();
                }
            }
        }
        $requestString = ltrim($request->getRequestString(), '/');
        $excludedUrls = explode("
", trim(Mage::getStoreConfig(self::XML_PATH_MGT_VARNISH_EXCLUDED_URLS)));
        if ($excludedUrls) {
            foreach ($excludedUrls as $excludedUrl) {
                $excludedUrl = trim($excludedUrl);
                if ($excludedUrl && $excludedUrl == $requestString) {
                    return $this->setNoCacheHeader();
                }
            }
        }
        $httpResponseCode = $response->getHttpResponseCode();
        if ($request->isPost() || $request->getParam('no_cache') || !in_array($httpResponseCode, array(200, 301, 404))) {
            return $this->setNoCacheHeader();
        }
        $fullActionName = sprintf('%s_%s_%s', $request->getRequestedRouteName(), $request->getRequestedControllerName(), $request->getRequestedActionName());
        $excludedRoutes = explode("
", trim(Mage::getStoreConfig(self::XML_PATH_MGT_VARNISH_EXCLUDED_ROUTES)));
        foreach ($excludedRoutes as $route) {
            $route = trim($route);
            if (!empty($route) && strpos($fullActionName, $route) === 0) {
                return $this->setNoCacheHeader();
            }
        }
        $regexp = null;
        $value = null;
        $routesCacheLifetime = unserialize(Mage::getStoreConfig(self::XML_PATH_MGT_VARNISH_ROUTES_CACHE_LIFETIME));
        if (is_array($routesCacheLifetime)) {
            foreach ($routesCacheLifetime as $routeCacheLifetime) {
                extract($routeCacheLifetime, EXTR_OVERWRITE);
                $regexp = trim($regexp);
                if (!empty($regexp) && strpos($fullActionName, $regexp) === 0) {
                    break;
                }
                $value = null;
            }
        }
        if (!isset($value)) {
            $value = Mage::getStoreConfig(self::XML_PATH_MGT_VARNISH_DEFAULT_CACHE_LIFETIME);
        }
        $value = (int)$value;
        $this->setCacheLifetimeHeader($value);
        return $this;
    }
    protected function _checkNoCacheCookie() {
        $helper = Mage::helper('mgt_varnish');
        $esiHelper = Mage::helper('mgt_varnish/esi');
        $canUseEsi = $esiHelper->canUseEsi();
        $isCustomerLoggedIn = $helper->isCustomerLoggedIn();
        $hasCompareItems = $helper->hasCompareItems();
        if (true === $canUseEsi) {
            $hasQuoteItems = false;
            $hasCompareItems = false;
            $isCustomerLoggedIn = false;
        } else {
            $hasQuoteItems = $helper->quoteHasItems();
        }
        $hasMessageSession = $this->hasMessageSession();
        if ((true === $hasQuoteItems) || (true === $isCustomerLoggedIn) || (true === $hasCompareItems) || (true === $hasMessageSession)) {
            $this->setNoCacheCookie();
        } else {
            $this->removeNoCacheCookie();
        }
    }

    protected function hasMessageSession(){
        $sessions = ["core/session", "customer/session", "checkout/session"];
        foreach ($sessions as $session) {
            $sessInstance = Mage::getSingleton($session);
            if($sessInstance->getMessages() && $sessInstance->getMessages()->getItems()){
                return true;
            }
        }
        return false;
    }

    public function purge($domains, $urlRegEx = '.*', $contentType = '.*') {
        $helper = Mage::helper('mgt_varnish');
        try {
            $headers = array(self::VARNISH_HEADER_HOST => '^(' . str_replace('.', '.', $domains) . ')$', self::VARNISH_HEADER_REGEX => (empty($urlRegEx) ? '.*' : $urlRegEx), self::VARNISH_HEADER_CONTENT_TYPE => (empty($contentType) ? '.*' : $contentType));
            $this->_purgeCacheServers($headers);
            $helper->debug('Purged Varnish items with parameters ' . var_export($headers, true));
        }
        catch(Exception $e) {
            $helper->debug('Error during purging: ' . $e->getMessage());
            return false;
        }
        return true;
    }
    protected function _purgeCacheServers(array $headers) {
        $servers = $this->_getVarnishServers();
        if (!$servers) {
            return;
        }
        foreach ($servers as $server) {
            $uri = 'http://' . $server;
            if ($port = trim(Mage::getStoreConfig(self::XML_PATH_MGT_VARNISH_PORT))) {
                $uri.= ':' . $port;
            }
            $uri.= '/';
            try {
                $client = new Zend_Http_Client();
                $client->setUri($uri)->setHeaders($headers)->setConfig(array('timeout' => 10));
                $response = $client->request('PURGE');
                if ($response->getStatus() != '200') {
                    throw new Exception('Return status ' . $response->getStatus());
                }
            }
            catch(Exception $e) {
                Mage::helper('mgt_varnish')->debug('Purging on server ' . $server . ' failed (' . $e->getMessage() . ').');
            }
        }
    }
    protected function _getVarnishServers() {
        return explode(';', Mage::getStoreConfig(self::XML_PATH_MGT_VARNISH_SERVERS));
    }
    public function addDebugHeader() {
        $header = explode(':', self::MGT_VARNISH_DEBUG_HEADER, 2);
        $response = self::getResponse();
        $response->setHeader($header[0], $header[1], true);
        return $this;
    }
    public static function sanitizeCacheControlHeader() {
        $cookie = self::getCookie();
        if ($cookie->get(self::NO_CACHE_COOKIE)) {
            self::setNoCacheHeader();
        }
    }
    public static function setNoCacheHeader() {
        return self::setCacheLifetimeHeader(0);
    }
    static public function setCacheLifetimeHeader($seconds) {
        $maxAge = 's-maxage=' . (($seconds < 0) ? 0 : $seconds);
        $cacheControlValue = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, ' . $maxAge;
        $response = self::getResponse();
        $headers = $response->getHeaders();
        foreach ($headers as $key => $header) {
            if ('Cache-Control' == $header['name'] && !empty($header['value'])) {
                if (strpos($header['value'], 'age=') !== false) {
                    $cacheControlValue = preg_replace('/(s-)?max[-]?age=[0-9]+/', $maxAge, $header['value']);
                } else {
                    $cacheControlValue.= $header['value'] . ', ' . $maxAge;
                }
            }
        }
        $response->setHeader('X-Cache-Lifetime', $seconds, true);
        $response->setHeader('Cache-Control', $cacheControlValue, true);
        $response->setHeader('Expires', 'Mon, 31 Mar 2010 10:00:00 GMT', true);
        $response->setHeader('Pragma', 'no-cache', true);
    }
    public function setNoCacheCookie($renewOnly = false) {
        $cookie = self::getCookie();
        if ($cookie->get(self::NO_CACHE_COOKIE)) {
            $cookie->renew(self::NO_CACHE_COOKIE);
        } elseif (!$renewOnly) {
            $cookie->set(self::NO_CACHE_COOKIE, 1);
        }
        return $this;
    }
    public function removeNoCacheCookie() {
        $cookie = self::getCookie();
        if ($cookie->get(self::NO_CACHE_COOKIE)) {
            $cookie->delete(self::NO_CACHE_COOKIE);
        }
    }
    static public function getCookie() {
        return Mage::getSingleton('core/cookie');
    }
    static public function getResponse() {
        if (!self::$response) {
            self::$response = Mage::app()->getResponse();
        }
        return self::$response;
    }
    static public function getRequest() {
        if (!self::$request) {
            self::$request = Mage::app()->getRequest();
        }
        return self::$request;
    }
}
