<?php

class TBT_RewardsReferral_Model_Observer_Sales_Order_Edit_Tab extends TBT_Rewards_Model_Transfer_Edit_Tab_Observer {

    /**
     * Add Tab
     * 
     * @param TBT_Rewards_Model_Transfer $transfer
     * @param TBT_Rewards_Block_Manage_Transfer_Edit_Tabs $block
     */
    protected function _addTab($transfer, $block) 
    {
        // if the user is allowed to see sales order information
        if ( ! Mage::getSingleton( 'admin/session' )->isAllowed( 'sales/order/actions/view' ) ) {
            return $this;
        }
        
        //  Add the transfer reference tab
        $this->_addOrderRefTab($transfer, $block);
        
        return $this;
    }

    /**
     * Add Order Referral Tab
     * 
     * @param TBT_Rewards_Model_Transfer $transfer
     * @param TBT_Rewards_Block_Manage_Transfer_Edit_Tabs $block
     */
    protected function _addOrderRefTab($transfer, $block) {
        
        $order_id = $transfer->getReferenceId();
        
        // Set the reference info in the registry so that it is shown
        $reg_transfer = Mage::registry( 'transfer_data' );
        if ( $reg_transfer ) {
            Mage::unregister( 'transfer_data' );
        } else {
            $reg_transfer = $transfer;
        }
        $reg_transfer->setData( 'order_id', $order_id );
        Mage::register( 'transfer_data', $reg_transfer );
        
        $reference_order_block = $block->getLayout()->createBlock( 'rewards/manage_transfer_edit_tab_grid_orders' );
        
        $reference_order_block->setOrderId( $order_id );
        
        return $this;
    }

}