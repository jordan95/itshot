<?php

class TBT_Rewards_Model_Tag_Edit_Tabs extends TBT_Rewards_Model_Transfer_Edit_Tab_Observer 
{
	protected function _addTab($transfer, $block) 
        {
		if ($transfer->getReasonId() == Mage::helper('rewards/transfer_reason')->getReasonId('tag')) {
			$block->addTab ( 'tags_section', array ('label' => Mage::helper ( 'rewards' )->__ ( 'Reference Product Tag' ), 
				'title' => Mage::helper ( 'rewards' )->__ ( 'Reference Product Tag' ), 
				'content' => $block->getLayout ()->createBlock ( 'rewards/manage_transfer_edit_tab_grid_tags' )->toHtml () ) );
		}
		
		return $this;
	}
}
