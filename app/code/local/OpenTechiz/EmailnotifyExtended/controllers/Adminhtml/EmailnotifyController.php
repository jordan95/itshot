<?php
require_once(Mage::getModuleDir('controllers', 'MSA_Emailnotify') . DS . 'Adminhtml' . DS . 'EmailnotifyController.php');
class OpenTechiz_EmailnotifyExtended_Adminhtml_EmailnotifyController extends MSA_Emailnotify_Adminhtml_EmailnotifyController {

	const BLOCK_SEND_EMAIL   = 1;
	const UNBLOCK_SEND_EMAIL   = 2;

	protected function _getEmailnotify($email){
		$collection = Mage::getModel("emailnotify/emailnotify")->getCollection()
									->addFieldToFilter("email_address",$email)
									->addFieldToFilter("status",1);
		$emailnotify = $collection->getFirstItem();
		if ($emailnotify->getId()) {
			return $emailnotify;
		}else{
			return Mage::getModel("emailnotify/emailnotify");
		}
	}

	public function blockSendEmailAction(){
		$order_id = Mage::app()->getRequest()->getParam('order_id');
    	$order = $this->getOrderData($order_id);
    	if (!empty($order->getCustomerEmail())) {
    		$this->updateStatusReviewEmail($order->getCustomerEmail(),self::BLOCK_SEND_EMAIL);
    	}
    	
        $this->_redirect('*/sales_order/view',array('order_id'=>$order_id));
	}

	public function unblockSendEmailAction(){
		$order_id = Mage::app()->getRequest()->getParam('order_id');
    	$order = $this->getOrderData($order_id);
    	if (!empty($order->getCustomerEmail())) {
    		$this->updateStatusReviewEmail($order->getCustomerEmail(),self::UNBLOCK_SEND_EMAIL);
    	}
        $this->_redirect('*/sales_order/view',array('order_id'=>$order_id));
	}

	public function getOrderData($order_id){
		return Mage::getSingleton('sales/order')->load($order_id);
	}

	protected function _getDescriptionBlock($emailnotify){
		if (!$emailnotify->getId()) {
			$description = OpenTechiz_EmailnotifyExtended_Helper_data::PRODUCT_REVIEW_EMAIL;
			return $description;
		}else{
			$list = explode(',',$emailnotify->getEmailDescription());
		}
		
		// Block send Email type
		if(in_array(OpenTechiz_EmailnotifyExtended_Helper_data::BLOCK_ALL_EMAIL,$list)){
			return $list;
		}else{
			if(!in_array(OpenTechiz_EmailnotifyExtended_Helper_data::PRODUCT_REVIEW_EMAIL,$list)){
				$list[] = OpenTechiz_EmailnotifyExtended_Helper_data::PRODUCT_REVIEW_EMAIL;
			}
		}
		$description = implode($list, ',');
		return $description;
	}

	protected function _getDescriptionUnBlock($emailnotify){
		$list = explode(',',$emailnotify->getEmailDescription());
		// // Unblock send email type
		if(in_array(OpenTechiz_EmailnotifyExtended_Helper_data::BLOCK_ALL_EMAIL,$list)){
			$allow_description = array(
				OpenTechiz_EmailnotifyExtended_Helper_data::PAST_CUSTOMER_COUPON,
				OpenTechiz_EmailnotifyExtended_Helper_data::NEWSLETTER,
				OpenTechiz_EmailnotifyExtended_Helper_data::REWARDS_EXPIRY,
			);

		}else{
			$key_review_email = array_search(OpenTechiz_EmailnotifyExtended_Helper_data::PRODUCT_REVIEW_EMAIL, $list);
			if ($key_review_email !== false) {
				unset($list[$key_review_email]);
				$allow_description = $list;
			}
		}
		
		$description = implode($allow_description, ',');
		return $description;
	}



	public function updateStatusReviewEmail($email,$type){
		$emailnotify = $this->_getEmailnotify($email);
		$data = array();

		if (!$emailnotify->getId()) {
			$emailnotify->setData('email_address',$email);
			$emailnotify->setData('status',1);
			$emailnotify->setData('created_time',Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s'));
		}

		if ($type == self::BLOCK_SEND_EMAIL) {
			$description = $this->_getDescriptionBlock($emailnotify);
		}

		if ($type == self::UNBLOCK_SEND_EMAIL) {
			$description = $this->_getDescriptionUnBlock($emailnotify);
		}

		$emailnotify->setData('email_description',$description);
		$emailnotify->setData('update_time',Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s'));
		try {
			$emailnotify->save();
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('emailnotify')->__('The email address have been blocked from notification'));
		} catch (Exception $e) {
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		}

		return $this;
	}
}
