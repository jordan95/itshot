<?php
class OpenTechiz_EmailnotifyExtended_Helper_data extends Mage_Core_Helper_Abstract
{
    const PAST_CUSTOMER_COUPON   = 14;
	const PRODUCT_REVIEW_EMAIL   = 20;
	const NEWSLETTER   = 22;
	const REWARDS_EXPIRY   = 'rewards_expire_email_custom_template';
	const BLOCK_ALL_EMAIL   = 'all';
}