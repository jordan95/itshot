<?php
class OpenTechiz_EmailnotifyExtended_Model_Emailnotify extends MSA_Emailnotify_Model_Emailnotify {

	public function checkReviewEmailNotify($email,$order_date='') {
		$found = false;

		$objModel = Mage::getModel("emailnotify/emailnotify");
		$emailData = $objModel->getCollection()->addFieldToFilter("email_address",$email)->addFieldToFilter("status",1);
		$dataArr = $emailData->getData();
		if(count($dataArr)>0) {
			foreach($dataArr as $dataVal) {
				$tname_array = explode(',',$dataVal["email_description"]);
				if(in_array('20',$tname_array)) {
					$found = true;
				}
			}
		}
		return $found;
	}
}
