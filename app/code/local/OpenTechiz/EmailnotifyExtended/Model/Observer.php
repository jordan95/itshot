<?php
class OpenTechiz_EmailnotifyExtended_Model_Observer
{
    public function addCustomerReturnToEmailList(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();
        $varien_object = $event->getEventData();
        $customer = $varien_object->getCustomer();

        $list = Mage::getModel('emailnotify/emailnotify')->getCollection()->addFieldToFilter('email_address', $customer->getEmail())->getFirstItem();
        if($list->getId()){
            
            $emailDescription = $list->getEmailDescription();
            if (!$emailDescription) {
                $emailDescription = "20";
            } else {
                if (strpos($emailDescription, '20') === false) {
                    $emailDescription .= ",20";
                }
            }
            $list->setEmailDescription($emailDescription)
                ->setStatus(1)
                ->setUpdateTime(Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s'))
                ->save();
        }else{
            $emailModel = Mage::getModel('emailnotify/emailnotify');
            $data = [];
            $data['email_address'] = $customer->getEmail();
            $data['email_description'] = '20';
            $data['status'] = 1;
            $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');

            $emailModel->setData($data)->save();
        }
    }

    public function addButtonOrderView($event){
        $block = $event->getBlock();
        if (!Mage::getSingleton('admin/session')->isAllowed('admin/customer/emailnotify')) {
            return $this;
        }

        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
            $params = Mage::app()->getRequest()->getParams();
            $orderId = $params['order_id'];
            $order = Mage::getSingleton('sales/order')->load($orderId);

            $check_email_sending_status = Mage::getModel("emailnotify/emailnotify")->checkReviewEmailNotify($order->getCustomerEmail(), $order->getCreatedAt());

            $is_allow_block = false;
            $collection = Mage::getModel("emailnotify/emailnotify")->getCollection()
                                        ->addFieldToFilter("email_address",$order->getCustomerEmail())
                                        ->addFieldToFilter("status",1);
            $emailnotify = $collection->getFirstItem();
            if ($emailnotify->getId()) {
                $list = explode(',',$emailnotify->getEmailDescription());
                if (in_array(OpenTechiz_EmailnotifyExtended_Helper_data::PRODUCT_REVIEW_EMAIL, $list) || 
                    in_array(OpenTechiz_EmailnotifyExtended_Helper_data::BLOCK_ALL_EMAIL, $list)) {
                    $is_allow_block = true;
                }
            }

            if ($is_allow_block) {
                $block->addButton('unblock_sending_email', array(
                    'label' => Mage::helper('opentechiz_salesextend')->__('Unblock Sending Email'),
                    'onclick' => 'setLocation(\'' . Mage::getUrl('*/emailnotify/unblockSendEmail/', array('order_id' => $orderId)) . '\')'
                ), 1, 5);
            }else{
                $block->addButton('block_sending_email', array(
                    'label' => Mage::helper('opentechiz_salesextend')->__('Block Sending Email'),
                    'onclick' => 'setLocation(\'' . Mage::getUrl('*/emailnotify/blockSendEmail/', array('order_id' => $orderId)) . '\')'
                ), 1, 5);
            }
            
        }
    }
}