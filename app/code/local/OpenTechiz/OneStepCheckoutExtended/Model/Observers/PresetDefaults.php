<?php
use GeoIp2\Database\Reader as Reader;

class OpenTechiz_OneStepCheckoutExtended_Model_Observers_PresetDefaults extends Idev_OneStepCheckout_Model_Observers_PresetDefaults {

      /**
     * If you have aquired a quote from cart and you are having saved addresses then you can get wrong shipping methods
     *
     * @param Varien_Event_Observer $observer
     */
    public function compareDefaultsFromCart(Varien_Event_Observer $observer) {
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        if(is_object($quote)){

            //extract the data from quote
            $currentBilling = $this->hasDataSet($quote->getBillingAddress());
            $currentShipping = $this->hasDataSet($quote->getShippingAddress());

            $sameAsBilling = $quote->getShippingAddress()->getSameAsBilling();
            $difference = array();

            if($sameAsBilling){
                if(Mage::getSingleton('customer/session')->isLoggedIn()){
                    $selectedAddress = $quote->getBillingAddress()->getCustomerAddressId();
                    if($selectedAddress){
                        $currentShippingOriginal = $this->hasDataSet($quote->getCustomer()->getAddressById($selectedAddress));
                        $difference = array_diff($currentShippingOriginal, $currentShipping);
                    } else {
                        $currentPrimaryBilling = $this->hasDataSet($quote->getCustomer()->getPrimaryBillingAddress());
                        $difference  = array_diff($currentPrimaryBilling, $currentBilling);
                        if(empty($difference)){
                            $difference  = array_diff($currentPrimaryBilling, $currentShipping);
                        }
                    }
                } else {
                    $difference = array_diff($currentBilling, $currentShipping);
                }

                if(!empty($difference)){
                    $quote->getBillingAddress()->addData($difference)->implodeStreetAddress();
                    $quote->getShippingAddress()->addData($difference)->implodeStreetAddress()->setCollectShippingRates(true);
                }
            }

            $quote->getShippingAddress()->setCollectShippingRates(true);
        }

        return $this;
    }

}
