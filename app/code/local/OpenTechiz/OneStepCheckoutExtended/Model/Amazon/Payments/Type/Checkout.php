<?php

class OpenTechiz_OneStepCheckoutExtended_Model_Amazon_Payments_Type_Checkout extends Amazon_Payments_Model_Type_Checkout
{

    public function getCheckoutMethod()
    {
        $customerHelper = Mage::helper('customer');
        if (!$customerHelper->isLoggedIn()) {
            $registration_mode = Mage::getStoreConfig('onestepcheckout/registration/registration_mode');
            if($registration_mode == 'auto_generate_account' || $registration_mode == 'require_registration' || $registration_mode == 'allow_guest'){
                return self::METHOD_GUEST;
            }
        }
        return parent::getCheckoutMethod();
    }

}
