<?php

class OpenTechiz_OneStepCheckoutExtended_Model_Observer
{

    public function changeCheckoutMethodForPaypalExpress(Varien_Event_Observer $observer)
    {
        $customerHelper = Mage::helper('customer');
        if ($customerHelper->isLoggedIn()) {
            return;
        }
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $registration_mode = Mage::getStoreConfig('onestepcheckout/registration/registration_mode');
        if ($registration_mode == 'auto_generate_account' || $registration_mode == 'require_registration' || $registration_mode == 'allow_guest') {
            $quote->setCheckoutMethod(Mage_Checkout_Model_Type_Onepage::METHOD_GUEST);
            $billAddress = $quote->getBillingAddress();
            $quote->setCustomerFirstname($billAddress->getFirstname())
                    ->setCustomerLastname($billAddress->getLastname());
        }
    }

    public function setGroundShippingAsDefault(Varien_Event_Observer $observer)
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        /* @var $shippingAddr Mage_Sales_Model_Quote_Address */
        $shippingAddr = $quote->getShippingAddress();
        $shippingMethod = $shippingAddr->getShippingMethod();
        if (!$shippingMethod && $quote->getId()){
            $carriers = Mage::getModel('checkout/cart_shipping_api')->getShippingMethodsList($quote->getId(),Mage::app()->getStore()->getId());
            foreach ($carriers as $item){
                if(($item["method_title"] == "Ground Shipping" && $shippingAddr->getCountryId() == "US") || ($item["method_title"] == "International Expedited Air" && $shippingAddr->getCountryId() != "US")){
                   $shippingAddr->setShippingMethod($item["code"])->setCollectShippingRates(false)->collectShippingRates()->save();
                   $quote->setShippingMethod($item["code"])->collectTotals()->save();
                }
            }
        }
    }

    public function exchangeCustomerToOrder(Varien_Event_Observer $observer)
    {
        $customerHelper = Mage::helper('customer');
        if ($customerHelper->isLoggedIn()) {
            return;
        }
        $lastOrderId = Mage::getSingleton('checkout/session')
                ->getLastRealOrderId();
        $order = Mage::getModel('sales/order')
                ->loadByIncrementId($lastOrderId);
        if ($order->getCustomerId()) {
            $order->setCustomerIsGuest(false);
        }
        $order->save();
    }

}
