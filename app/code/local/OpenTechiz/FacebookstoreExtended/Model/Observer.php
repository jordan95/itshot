<?php

class OpenTechiz_FacebookstoreExtended_Model_Observer
{

    public function redirectProductFromFbstoretoWebsite(Varien_Event_Observer $observer)
    {
        $productId = (int) Mage::app()->getRequest()->getParam('id');
        $product = Mage::getModel('catalog/product')->load($productId);
        if (!$product->getId()) {
            $url = Mage::getBaseUrl();
        } else {
            $url = $product->getProductUrl();
        }

        header("HTTP/1.1 301 Moved Permanently");
        header("Location: {$url}");
        exit();
    }

}
