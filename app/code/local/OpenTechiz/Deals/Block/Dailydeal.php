<?php

class OpenTechiz_Deals_Block_Dailydeal extends Mage_Catalog_Block_Product_List
{
    private $dealProducts;

    /**
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    protected function _getProductCollection()
    {
        if ($this->_productCollection === null) {
            $dealProducts = $this->getDealProducts();
            $productIds = [];
            /** @var OpenTechiz_Deals_Model_Deal $dealProduct */
            foreach ($dealProducts as $dealProduct) {
                $productIds[] = $dealProduct->getProductId();
            }
            /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
            $collection = Mage::getModel('catalog/product')->getCollection();
            $collection->setStoreId($this->getStoreId())
                ->addFieldToFilter('entity_id', array('in' => $productIds))
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addMinimalPrice()
                ->addTaxPercents()
                ->addStoreFilter();
								
            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInSiteFilterToCollection($collection);
            $this->_productCollection = $collection;
        											
		}
        return $this->_productCollection;
    }

    /**
     * @return OpenTechiz_Deals_Model_Deal[]
     */
    public function getDealProducts()
    {
        if ($this->dealProducts === null) {
            /** @var OpenTechiz_Deals_Model_Resource_Deal_Collection $dealProductsCollection */
            $dealProductsCollection = Mage::helper('opentechiz_deals')->getDealProducts(OpenTechiz_Deals_Model_Deal::TYPE_DAILY);
            Mage::helper('opentechiz_deals')->addEndDateFilter($dealProductsCollection);
            $this->dealProducts = $dealProductsCollection->getItems();
        }

        return $this->dealProducts;
    }

    /**
     * @param int $productId
     * @return false|OpenTechiz_Deals_Model_Deal
     */
    public function getDealByProductId($productId)
    {
        foreach ($this->getDealProducts() as $dealProduct) {
            if ($dealProduct->getProductId() == $productId) {
                return $dealProduct;
            }
        }

        return false;
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        if ($this->hasData('store_id')) {
            return $this->_getData('store_id');
        }
        return Mage::app()->getStore()->getId();
    }
}
