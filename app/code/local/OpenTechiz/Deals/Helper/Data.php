<?php

class OpenTechiz_Deals_Helper_Data extends Mage_Core_Helper_Abstract
{
    const GENDER_ATTRIBUTE_CODE = 'c2c_gender_for_filter';
    const GENDER_MEN = "Men's";
    const GENDER_WOMEN = "Women's";
    const GENDER_UNISEX = 'Unisex';

    private $genderAttribute;
    private $clearanceCategoryId;

    /**
     * @param string $dealType
     * @return OpenTechiz_Deals_Model_Resource_Deal_Collection
     */
    public function getDealProducts($dealType)
    {
        /** @var OpenTechiz_Deals_Model_Resource_Deal_Collection $dealProducts */
        $dealProducts = Mage::getModel('opentechiz_deals/deal')
            ->getCollection()
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('type', $dealType);

        return $dealProducts;
    }

    /**
     * @param OpenTechiz_Deals_Model_Resource_Deal_Collection $collection
     */
    public function addEndDateFilter($collection)
    {
        $collection->addFieldToFilter('end_date', ['gt' => date('Y-m-d H:i:s', Mage::app()->getLocale()->date()->getTimestamp())]);

        return $collection;
    }

    /**
     * @param $dealType
     * @return Mage_Catalog_Model_Product[]
     */
    public function getProductsForDealUpdate($dealType)
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addAttributeToSelect('special_price')
            ->addFieldToFilter('visibility', [
                'in' => Mage::getSingleton('catalog/product_visibility')->getVisibleInSiteIds()
            ])
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('created_at', ['lt' => date('Y-m-d H:i:s', strtotime('-6 months'))]);
        $collection->joinTable(
            ['oii' => $collection->getResource()->getTable('opentechiz_inventory/instock')],
            'product_id=entity_id',
            [
                'qty' => 'qty',
                'product_id' => 'product_id',
            ],
            ['qty' => ['gt' => 0]]
        );

        $collection->groupByAttribute('entity_id');
        $collection->getSelect()->orderRand();

        $dealProducts = Mage::getModel('opentechiz_deals/deal')->getCollection()
            ->addFieldToSelect('product_id');
        $updatedAtDate = date('Y-m-d H:i:s', strtotime('-6 months'));

        $orderUpdatedAtDate = date('Y-m-d H:i:s', strtotime('-12 months'));
        $select = $collection->getResource()->getReadConnection()->select();
        $select->from(
            ['soi' => $collection->getResource()->getTable('sales/order_item')],
            ['product_id']
        )->join(
            ['so' => $collection->getResource()->getTable('sales/order')],
            "so.entity_id=soi.order_id AND so.updated_at > '$orderUpdatedAtDate' AND so.state IN ('processing', 'complete')",
            []
        )->distinct();

        $productIdsToExclude = $collection->getResource()->getReadConnection()->fetchCol($select);

        $dealProducts->getSelect()->where("(type = $dealType AND updated_at > '$updatedAtDate') OR is_active = 1");
        $dealProductIds = $dealProducts->getColumnValues('product_id');

        $productIdsToExclude = array_unique(array_merge($productIdsToExclude, $dealProductIds));

        $this->addCategoriesFilter($collection, $productIdsToExclude);

        $products = [];
        $unloadedCount = 0;
        foreach ($this->getGenderArrayByDealType($dealType) as $optionId => $size) {
            if ($unloadedCount) {
                $size += $unloadedCount;
                $unloadedCount = 0;
            }
            $collection->addFieldToFilter(OpenTechiz_Deals_Helper_Data::GENDER_ATTRIBUTE_CODE, $optionId);
            $collection->setPageSize($size);
            $items = $collection->getItems();
            $loadedIds = [];
            foreach ($items as $item) {
                $loadedIds[] = $item->getId();
            }
            $select = $collection->getSelect();
            $where = $select->getPart(Zend_Db_Select::WHERE);
            foreach ($where as $key => $condition) {
                if (strpos($condition, OpenTechiz_Deals_Helper_Data::GENDER_ATTRIBUTE_CODE)) {
                    unset($where[$key]);
                    continue;
                }
                if ($loadedIds && strpos($condition, OpenTechiz_Deals_Helper_Data::GENDER_ATTRIBUTE_CODE)) {
                    unset($where[$key]);
                }
            }
            $select->setPart(Zend_Db_Select::WHERE, $where);
            $products = array_merge($products, $items);
            if ($dealType != OpenTechiz_Deals_Model_Deal::TYPE_DAILY && count($items) < $size) {
                $size -= count($items);
                $collection->clear();

                $collection->setPageSize($size);
                $items = $collection->getItems();
                $products = array_merge($products, $items);
                if (count($items) < $size) {
                    $unloadedCount = $size - count($items);
                }
            }
            $collection->clear();
        }

        if ($dealType == OpenTechiz_Deals_Model_Deal::TYPE_DAILY && count($products) && count($products) < 3) {
            $products = array_slice($products, 0, 1);
        }

        return $products;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        $discount = (float) Mage::getStoreConfig('opentechiz_deals/general/discount');
        return $discount ? $discount : 1;
    }

    /**
     * @return int|false
     */
    public function getClearanceCategoryId()
    {
        if ($this->clearanceCategoryId === null) {
            $categoryId = (int)Mage::getStoreConfig('opentechiz_deals/general/clearance_category');
            if (!$categoryId) {
                $category = Mage::getModel('catalog/category')->getCollection()
                    ->addFieldToFilter('name', 'Clearance Sale')
                    ->getFirstItem();
                $categoryId = $category->getId();
            }
            $this->clearanceCategoryId = $categoryId ? $categoryId : false;
        }

        return $this->clearanceCategoryId;
    }

    /**
     * @param int $dealType
     * @return array
     */
    private function getGenderArrayByDealType($dealType)
    {
        switch ($dealType) {
            case OpenTechiz_Deals_Model_Deal::TYPE_DAILY:
                return $this->getGenderArray(1, 1, 1);
            case OpenTechiz_Deals_Model_Deal::TYPE_WEEKLY:
                return $this->getGenderArray(3, 2, 2);
            case OpenTechiz_Deals_Model_Deal::TYPE_CLEARANCE:
                return $this->getGenderArray(10, 5, 5);
            default: return [];
        }
    }

    /**
     * @param int $men
     * @param int $women
     * @param int $unisex
     * @return array
     */
    private function getGenderArray($men = 0, $women = 0, $unisex = 0)
    {
        $result = [];
        if (!$this->getGenderAttribute()) {
            return $result;
        }
        if ($men && ($optionId = $this->getGenderOptionId(self::GENDER_MEN))) {
            $result[$optionId] = $men;
        }

        if ($women && ($optionId = $this->getGenderOptionId(self::GENDER_WOMEN))) {
            $result[$optionId] = $women;
        }

        if ($unisex && ($optionId = $this->getGenderOptionId(self::GENDER_UNISEX))) {
            $result[$optionId] = $unisex;
        }

        return $result;
    }

    /**
     * @param string $optionValue
     * @return string|null
     * @throws Mage_Core_Exception
     */
    private function getGenderOptionId($optionValue)
    {
        return $this->getGenderAttribute()->getSource()->getOptionId($optionValue);
    }

    /**
     * @return false|Mage_Catalog_Model_Resource_Eav_Attribute
     */
    private function getGenderAttribute()
    {
        if ($this->genderAttribute === null) {
            $this->genderAttribute = Mage::getResourceModel('catalog/product')
                ->getAttribute(self::GENDER_ATTRIBUTE_CODE);
        }

        return $this->genderAttribute;
    }

    /**
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param array $productIdsToExclude
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    private function addCategoriesFilter($collection, $productIdsToExclude)
    {
        $resource = $collection->getResource();
        $read = $resource->getReadConnection();
        $select = $read->select();
        $where = [];
        $excludeCategoryPaths = $this->getExcludedCategoryPaths();
        foreach ($excludeCategoryPaths as $categoryPath) {
            $where[] = "(cce.path = '" . $categoryPath . "' OR cce.path LIKE '" . $categoryPath ."/%')";
        }
        if ($where) {
            $select->from(['ccp' => $resource->getTable('catalog/category_product')], 'product_id')
                ->joinLeft(
                    ['cce' => $collection->getResource()->getTable('catalog/category')],
                    'cce.entity_id = ccp.category_id',
                    []
                )
                ->where(implode(' OR ', $where))
                ->distinct();
            $productIds = $read->fetchCol($select);
            if ($productIds) {
                $productIdsToExclude = array_merge($productIdsToExclude, $productIds);
            }
        }

        if ($productIdsToExclude) {
            $collection->addFieldToFilter('entity_id', ['nin' => array_unique($productIdsToExclude)]);
        }

        return $collection;
    }

    /**
     * @return array
     */
    private function getExcludedCategoryPaths()
    {
        $excludeCategoryIds = explode(',', Mage::getStoreConfig('opentechiz_deals/general/exclude_categories'));
        if (!$excludeCategoryIds) {
            return [];
        }
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $select = $read->select();
        $select->from($resource->getTableName('catalog/category'), 'path')
            ->where('entity_id IN (?)', $excludeCategoryIds);
        return $read->fetchCol($select);
    }

}
