<?php

class OpenTechiz_Deals_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->addLinkRel('canonical', Mage::helper('core/url')->getCurrentUrl());
		$this->renderLayout();
	}
}
