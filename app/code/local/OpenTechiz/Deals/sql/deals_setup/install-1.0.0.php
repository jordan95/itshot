<?php

/* @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

if (!$installer->tableExists($installer->getTable('opentechiz_deals/deal'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('opentechiz_deals/deal'))
        ->addColumn('deal_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'Deal Id')
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false,
        ), 'Product id')
        ->addColumn('type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
        ), 'Deal Type')
        ->addColumn('last_price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(), 'Last Price')
        ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default' => '0',
        ), 'Is Active')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Update Time')
        ->addIndex(
            $installer->getIdxName('opentechiz_deals/deal', array('product_id')),
            array('product_id')
        )
        ->addIndex(
            $installer->getIdxName('opentechiz_deals/deal', array('product_id', 'type')),
            array('product_id', 'type'),
            array('type' => 'unique')
        )
        ->addIndex(
            $installer->getIdxName('opentechiz_deals/deal', array('updated_at')),
            array('updated_at')
        )
        ->addForeignKey(
            $installer->getFkName(
                'opentechiz_deals/deal',
                'product_id',
                'catalog/product',
                'entity_id'
            ),
            'product_id',
            $installer->getTable('catalog/product'),
            'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE,
            Varien_Db_Ddl_Table::ACTION_CASCADE
        );

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();
