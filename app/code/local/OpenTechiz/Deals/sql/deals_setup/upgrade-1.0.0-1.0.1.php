<?php

/* @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

try {
    $connection = $installer->getConnection();
    $tableName = $installer->getTable('opentechiz_deals/deal');
    $connection->addColumn(
        $tableName,
        'end_date',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
            'comment' => 'End Date',
        )
    );
}catch (Exception $e){

}

$installer->endSetup();
