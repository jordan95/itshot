<?php

/**
 * @method OpenTechiz_Deals_Model_Resource_Deal _getResource()
 * @method OpenTechiz_Deals_Model_Resource_Deal getResource()
 * @method OpenTechiz_Deals_Model_Resource_Deal_Collection getCollection()
 *
 * @method int getProductId()
 * @method $this setProductId(int $value)
 * @method int getType()
 * @method $this setType(int $value)
 * @method float getLastPrice()
 * @method $this setLastPrice(float $value)
 * @method int getIsActive()
 * @method $this setIsActive(int $value)
 * @method int getUpdatedAt()
 * @method $this setUpdatedAt(int $value)
 * @method int getEndDate()
 * @method $this setEndDate(int $value)
 */
class OpenTechiz_Deals_Model_Deal extends Mage_Core_Model_Abstract
{
    const TYPE_DAILY = 1;
    const TYPE_WEEKLY = 2;
    const TYPE_CLEARANCE = 3;

    protected static $_availableTypes = [
        self::TYPE_DAILY,
        self::TYPE_WEEKLY,
        self::TYPE_CLEARANCE,
    ];

    protected function _construct()
    {
        $this->_init('opentechiz_deals/deal');
    }

    /**
     * @return $this
     */
    protected function _beforeSave()
    {
        $this->setUpdatedAt(Mage::app()->getLocale()->date()->getTimestamp());
        if (!in_array($this->getType(), self::$_availableTypes)) {
            Mage::throwException(Mage::helper('opentechiz_deals')->__('Wrong deal type.'));
        }
        if ($this->getType() == self::TYPE_DAILY && !$this->getId()) {
            $this->setEndDate(date('Y-m-d H:00:00', strtotime('+1 day', $this->getUpdatedAt())));
        }

        return parent::_beforeSave();
    }

    /**
     * @return string
     */
    public function getCloseTime($now = 0)
    {
        $updatedAt = strtotime(date('Y-m-d H:00:00', Mage::getModel('core/date')->timestamp($this->getUpdatedAt())));
        switch ($this->getType()) {
            case self::TYPE_DAILY:
                if ($this->getEndDate()) {
                    return date('Y-m-d H:00:00', Mage::getModel('core/date')->timestamp($this->getEndDate()));
                }
                $date = date('Y-m-d H:00:00', strtotime('+1 day', $updatedAt));
                if ($now > strtotime($date)) {
                    $time = date('H:00:00', strtotime($date));
                    $date = date('Y-m-d', strtotime('+1 day', $now)) . ' ' . $time;
                }
                return $date;
            case self::TYPE_WEEKLY:
                return date('Y-m-d H:00:00', strtotime('+1 week', $updatedAt));
            case self::TYPE_CLEARANCE:
                return date('Y-m-d H:00:00', strtotime('+1 month', $updatedAt));
            default: return '';
        }
    }
}
