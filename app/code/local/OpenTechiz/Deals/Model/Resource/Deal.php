<?php

class OpenTechiz_Deals_Model_Resource_Deal extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_deals/deal', 'deal_id');
    }
}
