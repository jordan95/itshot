<?php

class OpenTechiz_Deals_Model_Resource_Deal_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    function _construct()
    {
        $this->_init('opentechiz_deals/deal');
    }
}
