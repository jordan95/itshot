<?php

class OpenTechiz_Deals_Model_Cron
{
    /**
     * @var OpenTechiz_Deals_Helper_Data
     */
    private $helper;

    public function __construct()
    {
        $this->helper = Mage::helper('opentechiz_deals');
    }

    public function updateDailyDeals()
    {
        $this->updateDeals(OpenTechiz_Deals_Model_Deal::TYPE_DAILY);
        if (!$this->warmCache(['daily-deals/index_index'])) {
            Mage::helper("opentechiz_fpccrawler")->purgeVarnish(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'daily-deals');
        }

        return $this;
    }

    public function updateWeeklyDeals()
    {
        $this->updateDeals(OpenTechiz_Deals_Model_Deal::TYPE_WEEKLY);
        if (!$this->warmCache(['cms/index_index'])) {
            Mage::helper("opentechiz_fpccrawler")->purgeVarnish(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB));
        }

        return $this;
    }

    public function updateClearanceDeals()
    {
        $this->updateDeals(OpenTechiz_Deals_Model_Deal::TYPE_CLEARANCE);

        return $this;
    }

    /**
     * @param int $dealType
     */
    private function updateDeals($dealType)
    {
        $dealProducts = $this->helper->getDealProducts($dealType);

        $clearanceCategoryId = $this->helper->getClearanceCategoryId();
        $isClearance = $dealType == OpenTechiz_Deals_Model_Deal::TYPE_CLEARANCE && $clearanceCategoryId;

        /** @var OpenTechiz_Deals_Model_Deal $dealProduct */
        foreach ($dealProducts as $dealProduct) {
            $dealProduct->setIsActive(0);
            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product')->load($dealProduct->getProductId());
            if (!$product || !$product->getId()) {
                $dealProduct->save();
                continue;
            }
            $product->setSpecialPrice($dealProduct->getLastPrice());
            if ($isClearance) {
                $categoryIds = $product->getCategoryIds();
                foreach ($categoryIds as $key => $categoryId) {
                    if ($categoryId == $clearanceCategoryId) {
                        unset($categoryIds[$key]);
                        $product->setCategoryIds($categoryIds);
                        break;
                    }
                }
            }
            $product->save();
            $dealProduct->save();
        }

        $discount = $this->helper->getDiscount();

        foreach ($this->helper->getProductsForDealUpdate($dealType) as $productForUpdate) {
            $product = Mage::getModel('catalog/product')->load($productForUpdate->getId());
            if (!$product || !$product->getId()) {
                continue;
            }
            $dealProduct = Mage::getModel('opentechiz_deals/deal')
                ->getCollection()
                ->addFieldToFilter('product_id', $product->getId())
                ->addFieldToFilter('type', $dealType)
                ->getFirstItem();
            if (!$dealProduct || !$dealProduct->getId()) {
                $dealProduct = Mage::getModel('opentechiz_deals/deal');
                $dealProduct->setProductId($product->getId())
                    ->setType($dealType);
            }
            $dealProduct->setLastPrice($product->getSpecialPrice());

            if ($isClearance) {
                $categoryIds = $product->getCategoryIds();
                if (!in_array($clearanceCategoryId, $categoryIds)) {
                    $categoryIds[] = $clearanceCategoryId;
                    $product->setCategoryIds($categoryIds);
                }
            }
            $newPrice = ceil($product->getSpecialPrice() - $product->getSpecialPrice() * $discount / 100);
            $product->setSpecialPrice($newPrice);
            $product->save();
            $dealProduct->setIsActive(1)->save();
        }
    }

    /**
     * @param array $types
     * @return bool
     */
    private function warmCache($types)
    {
        if (!$types || !Mage::helper('core')->isModuleEnabled('Mirasvit_Fpc') || !Mage::app()->useCache('fpc')) {
            return false;
        }
        if (is_string($types)) {
            $types = [$types];
        }
        $result = $this->warmCacheByModel($types, 'fpccrawler/crawler_url');
        $result = $result || $this->warmCacheByModel($types, 'fpccrawler/crawlerlogged_url');

        return $result;
    }

    /**
     * @param array $types
     * @param string $model
     * @return false
     * @throws Mage_Core_Exception
     */
    private function warmCacheByModel($types, $model)
    {
        $fpcCrawlerCollection = Mage::getModel($model)
            ->getCollection()
            ->addFieldToFilter('sort_by_page_type', ['in' => $types]);
        if ($fpcCrawlerCollection->count() == 0) {
            return false;
        }
        /* @var OpenTechiz_FpcCrawler_Model_Crawler_Url $urlCrawler */
        foreach ($fpcCrawlerCollection as $urlCrawler) {
            try {
                $urlCrawler->warmCache();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }

        return true;
    }
}
