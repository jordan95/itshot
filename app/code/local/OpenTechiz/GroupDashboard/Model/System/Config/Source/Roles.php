<?php
class Opentechiz_GroupDashboard_Model_System_Config_Source_Roles
{
    public function toOptionArray()
    {
        $roleCollection = Mage::getModel('admin/roles')->getCollection()->addFieldToFilter('parent_id', 0);
        $result = [];
        foreach ($roleCollection as $role){
            array_push($result, array('value' => $role->getRoleId(), 'label' => $role->getRoleName()));
        }
        return $result;
    }
}