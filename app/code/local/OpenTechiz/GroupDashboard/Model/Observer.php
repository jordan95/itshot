<?php
class OpenTechiz_GroupDashboard_Model_Observer
{
    public function afterOutput(Varien_Event_Observer $obj)
    {
        $block = $obj->getEvent ()->getBlock ();
        $transport = $obj->getEvent ()->getTransport ();

        if(empty($transport)) {
            return $this;
        }

        if (!Mage::getStoreConfigFlag('opentechiz_dashboard/general/enable')) {
            return $this;
        }

        if (Mage::getStoreConfigFlag('advanced/modules_disable_output/TBT_Rewards')) {
            $blockBefore = 'Mage_Adminhtml_Block_Dashboard_Sales';
        }else {
            $blockBefore = 'TBT_Rewards_Block_Manage_Dashboard_Widget';
        }
        if ($block instanceof $blockBefore) {
            $html = $transport->getHtml ();
            $groupDashboardHtml = $block->getParentBlock()->getChildHtml('opentechiz_dashboard_widget');
            if (!empty($groupDashboardHtml))
                $html .= $groupDashboardHtml;
            $transport->setHtml($html);
        }

        return $this;
    }

    public function checkPermissionDashboard($observer){
        $block = $observer->getEvent()->getBlock();
        $currentUser = Mage::getSingleton('admin/session')->getUser();
        if($currentUser && $currentUser->getUserId()) {
            $adminUserId = $currentUser->getUserId();
            $role_data = Mage::getModel('admin/user')->load($adminUserId)->getRole()->getData();
            $currentRoleId = $role_data['role_id'];

            $isManagement = false;
            $isUser = false;
            if ($this->_isAdminAllowed()) {
                $isManagement = true;
            }
            if ($this->_isUserAllowed()) {
                $isUser = true;
            }

            if (!$isManagement) {
                if ($block instanceof Mage_Adminhtml_Block_Dashboard) {
                    $block->setTemplate('opentechiz/dashboard/group/management_widget.phtml');

                }
            }
        }
        return $this;
    }

    public function checkRemoveNotiMessage($observer){
        $block = $observer->getEvent()->getBlock();
        if(!$this->_isMessageAllowed()) {
            if ($block->getNameInLayout() == 'notifications') {
                $block->unsetChildren();
            }
        }
    }

    protected function _isMessageAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('admin/system_messages');
    }

    protected function _isAdminAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('admin/dashboard/management_grid');
    }

    protected function _isUserAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('admin/dashboard/user_grid');
    }
}