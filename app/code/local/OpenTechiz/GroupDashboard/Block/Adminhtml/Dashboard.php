<?php

class OpenTechiz_GroupDashboard_Block_Adminhtml_Dashboard extends Mage_Adminhtml_Block_Dashboard
{

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('opentechiz/dashboard/index.phtml');

    }
     protected function _prepareLayout()
    {
        $this->setChild('lastOrders',
                $this->getLayout()->createBlock('adminhtml/dashboard_orders_grid')
        );

        $this->setChild('totals',
                $this->getLayout()->createBlock('adminhtml/dashboard_totals')
        );

        $this->setChild('sales',
                $this->getLayout()->createBlock('adminhtml/dashboard_sales')
        );

        if (Mage::getStoreConfig(self::XML_PATH_ENABLE_CHARTS)) {
            $block = $this->getLayout()->createBlock('adminhtml/dashboard_diagrams');
        } else {
            $block = $this->getLayout()->createBlock('adminhtml/template')
                ->setTemplate('dashboard/graph/disabled.phtml')
                ->setConfigUrl($this->getUrl('adminhtml/system_config/edit', array('section'=>'admin')));
        }
        $this->setChild('diagrams', $block);

        $this->setChild('grids',
                $this->getLayout()->createBlock('adminhtml/dashboard_grids')
        );
    }

    protected function _isAdminAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('admin/dashboard/management_grid');
    }

    protected function _isUserAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('admin/dashboard/user_grid');
    }

    public function getOrderData($stage, $hasStatus, $notHasStatus = null, $orderType = null){
        $results = [];
        $dates = $this->getDate(-10000, 0);
        $collection = Mage::getModel('sales/order')->getCollection()
            ->join(array('log' => 'opentechiz_reportExtend/log_orderStatus'), 'log.id = main_table.last_history_id', '*')
            ->addFieldToFilter('log.updated_at', array(
                'from' => $dates['from'],
                'to' => $dates['to']
        ));
        if($stage !== 5){
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterActiveOrder($collection);
        }else{
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterCompleteOrder($collection);
        }
        if($stage !== null) $collection->addFieldToFilter('order_stage', $stage);
        if($hasStatus !== null) $collection->addFieldToFilter('internal_status', $hasStatus);
        if($notHasStatus != null) $collection->addFieldToFilter('internal_status', array('neq' => $notHasStatus));
        if($orderType !== null){
            if($orderType == 2){
                $collection->addFieldToFilter('main_table.order_stage', array('neq' => OpenTechiz_SalesExtend_Helper_Data::SHIPPED));
            }
            if($orderType == 0){
                $collection->addFieldToFilter('main_table.order_type', array('in' => array(0,3)));
            }else{
                $collection->addFieldToFilter('main_table.order_type', $orderType);
            }
        }

        $total = $collection->getSize();

        //over 7days
        $dates = $this->getDate(-10000, -7);
        $collection = Mage::getModel('sales/order')->getCollection()
            ->join(array('log' => 'opentechiz_reportExtend/log_orderStatus'), 'log.id = main_table.last_history_id', '*')
            ->addFieldToFilter('log.updated_at', array(
                'from' => $dates['from'],
                'to' => $dates['to']
        ));
        if($stage !== 5){
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterActiveOrder($collection);
        }else{
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterCompleteOrder($collection);
        }
        if($stage !== null) $collection->addFieldToFilter('order_stage', $stage);
        if($hasStatus !== null) $collection->addFieldToFilter('internal_status', $hasStatus);
        if($notHasStatus != null) $collection->addFieldToFilter('internal_status', array('neq' => $notHasStatus));
        if($orderType !== null){
            if($orderType == 2){
                $collection->addFieldToFilter('main_table.order_stage', array('neq' => OpenTechiz_SalesExtend_Helper_Data::SHIPPED));
            }
            if($orderType == 0){
                $collection->addFieldToFilter('main_table.order_type', array('in' => array(0,3)));
            }else{
                $collection->addFieldToFilter('main_table.order_type', $orderType);
            }
        }

        $over10days = $collection->getSize();

        //5-10days
        $dates = $this->getDate(-7, -2);
        $collection = Mage::getModel('sales/order')->getCollection()
            ->join(array('log' => 'opentechiz_reportExtend/log_orderStatus'), 'log.id = main_table.last_history_id', '*')
            ->addFieldToFilter('log.updated_at', array(
                'from' => $dates['from'],
                'to' => $dates['to']
        ));
        if($stage !== 5){
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterActiveOrder($collection);
        }else{
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterCompleteOrder($collection);
        }
        if($stage !== null) $collection->addFieldToFilter('order_stage', $stage);
        if($hasStatus !== null) $collection->addFieldToFilter('internal_status', $hasStatus);
        if($notHasStatus != null) $collection->addFieldToFilter('internal_status', array('neq' => $notHasStatus));
        if($orderType !== null){
            if($orderType == 2){
                $collection->addFieldToFilter('main_table.order_stage', array('neq' => OpenTechiz_SalesExtend_Helper_Data::SHIPPED));
            }
            if($orderType == 0){
                $collection->addFieldToFilter('main_table.order_type', array('in' => array(0,3)));
            }else{
                $collection->addFieldToFilter('main_table.order_type', $orderType);
            }
        }

        $from5to10days = $collection->getSize();


        //under 5days
        $dates = $this->getDate(-2, 0);
        $collection = Mage::getModel('sales/order')->getCollection()
            ->join(array('log' => 'opentechiz_reportExtend/log_orderStatus'), 'log.id = main_table.last_history_id', '*')
            ->addFieldToFilter('log.updated_at', array(
                'from' => $dates['from'],
                'to' => $dates['to']
        ));
        if($stage !== 5){
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterActiveOrder($collection);
        }else{
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterCompleteOrder($collection);
        }
        if($stage !== null) $collection->addFieldToFilter('order_stage', $stage);
        if($hasStatus !== null) $collection->addFieldToFilter('internal_status', $hasStatus);
        if($notHasStatus != null) $collection->addFieldToFilter('internal_status', array('neq' => $notHasStatus));
        if($orderType !== null){
            if($orderType == 2){
                $collection->addFieldToFilter('main_table.order_stage', array('neq' => OpenTechiz_SalesExtend_Helper_Data::SHIPPED));
            }
            if($orderType == 0){
                $collection->addFieldToFilter('main_table.order_type', array('in' => array(0,3)));
            }else{
                $collection->addFieldToFilter('main_table.order_type', $orderType);
            }
        }

        $under5days = $collection->getSize();


        $results['total'] = $total;
        $results['over10days'] = $over10days;
        $results['from5to10days'] = $from5to10days;
        $results['under5days'] = $under5days;

        return $results;
    }

    public function getDate($from, $to){
        $currentDate = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $time =  strtotime($currentDate);

        $from = (int)$from*24; //to hour
        $to = (int)$to*24; //to hour

        $toDate = date('Y-m-d H:i:s', strtotime($to." hour", $time));
        $fromDate = date('Y-m-d H:i:s', strtotime($from." hour", $time));

        $result= [];
        $result['from'] = $fromDate;
        $result['to'] = $toDate;

        return $result;
    }

    public function getUserLink($stage, $hasStatus, $orderType = null,$status = "0"){
        $filter = '';
        if($stage != null || $stage == 0) $filter .= 'order_stage='.$stage;
        if($hasStatus != null) {
            if($filter != ''){
                $filter .= '&';
            }
            $filter .= 'internal_status='.$hasStatus;
        }
        if($orderType != null) {
            if($filter != ''){
                $filter .= '&';
            }
            $filter .= 'order_type='.$orderType;
        }
        if($status !=null){
            if($filter != ''){
                $filter .= '&';
            }
            $filter .= 'state='.$status;
        }
        $filter = base64_encode($filter);
        $url = Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/index', array('filter'=>$filter));
        return $url;
    }
}
