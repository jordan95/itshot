<?php

class OpenTechiz_Fpc_Model_Container_Phtmlblock extends Mirasvit_Fpc_Model_Container_Phtmlblock
{
    public function applyToContent(&$content, $withoutBlockUpdate = false)
    {
        if (!$this->_definition['replacer_tag_begin']
            || !$this->_definition['template']
            || ($this->_definition['replacer_tag_begin']
                && strpos($content, $this->_definition['replacer_tag_begin']) === false)) {
            return true;
        }

        $startTime = microtime(true);
        $definitionHash = $this->_definition['block'].'_'.$this->_definition['template'];
        Mage::helper('fpc/debug')->startTimer('FPC_BLOCK_'.$definitionHash);

        $pattern = '/'.preg_quote($this->_definition['replacer_tag_begin'], '/').'(.*?)'.preg_quote($this->_definition['replacer_tag_end'], '/').'/ims';
        $html = $this->_renderBlock();

        if ($html !== false) {
            ini_set('pcre.backtrack_limit', 100000000);
            if (preg_match($pattern, $html, $matches)) {
                $html = $matches[0];
            }
            Mage::helper('fpc/debug')->appendDebugInformationToBlock($html, $this, 0, $startTime);
            $content = preg_replace($pattern, str_replace('$', '\\$', $html), $content, 1);
            Mage::helper('fpc/debug')->stopTimer('FPC_BLOCK_'.$definitionHash);

            return true;
        }
        Mage::helper('fpc/debug')->stopTimer('FPC_BLOCK_'.$definitionHash);

        return false;
    }
}
