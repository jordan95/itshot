<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Full Page Cache
 * @version   1.0.38
 * @build     681
 * @copyright Copyright (C) 2017 Mirasvit (http://mirasvit.com/)
 */



class OpenTechiz_Fpc_Model_Container_Base  extends Mirasvit_Fpc_Model_Container_Abstract
{

    protected function getWishlist()
    {
        if (self::$_wishlist === null) {
            Mage::helper('fpc/debug')->startTimer('FPC_DEPENDENCES_' . $this->_prepareDependenceName(__FUNCTION__));
            
            $items = Mage::helper('medialounge_theme')->getFavoritesItems();
            $_wishlistQty = count($items->getItems());

            if ($_wishlistQty >0) {
                    self::$_wishlist .= $_wishlistQty;
            }
            Mage::helper('fpc/debug')->stopTimer('FPC_DEPENDENCES_' . $this->_prepareDependenceName(__FUNCTION__));
        }

        return self::$_wishlist;
    }
}
