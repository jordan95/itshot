<?php

class OpenTechiz_Fpc_Model_Config extends Mirasvit_Fpc_Model_Config
{

    public function getContainers()
    {
        if ($this->_containers === null) {
            $this->_containers = array();
            foreach ($this->getNode('containers')->children() as $container) {
                $containerName = (string) $container->name;
                $containerBlock = (string) $container->block;
                $containerBlockId = isset($container->block_id) ? (string) $container->block_id : false;
                $containerTemplate = isset($container->template) ? (string) $container->template : false;

                // ignore reports/product_viewed if it is not category
                /*if ($containerBlock == self::REPORT_BLOCK
                    && Mage::helper('fpc')->getFullActionCode() != 'catalog/category_view') {
                    continue;
                }*/

                $additionalData = false;
                if ((string) $container->container == self::PHTML_BLOCK_CLASS) {
                    $additionalData = (array) $container->additional_data;
                }

                $containerData = array(
                    'container' => (string) $container->container,
                    'block' => $containerBlock,
                    'cache_lifetime' => (int) $container->cache_lifetime,
                    'name' => (string) $container->name,
                    'depends' => (string) $container->depends,
                    'in_register' => isset($container->in_register) ? (string) $container->in_register : false,
                    'in_session' => isset($container->in_session) ? ((trim($container->in_session) !== 'true') ? intval($container->in_session) : true) : false,
                    'in_app' => isset($container->in_app) ? intval($container->in_app) : intval($container->in_app) + 1,
                    'block_id' => $containerBlockId,
                    'replacer_tag_begin' => isset($container->replacer_tag_begin) ? (string) $container->replacer_tag_begin : false,
                    'replacer_tag_end' => isset($container->replacer_tag_end) ? (string) $container->replacer_tag_end : false,
                    'template' => $containerTemplate,
                    'set_id' => isset($container->set_id) ? (int) $container->set_id : false, // for Mirasvit_Fpc_Model_Container_PhtmlblockAm
                    'additional_data' => $additionalData, //additional data for Mirasvit_Fpc_Model_Container_Phtmlblock
                    'disabled' => (isset($container->disabled)
                        && (trim($container->disabled) == 'true'
                            || $container->disabled == 1)) ? true : false, //disabled blocks from cache.xml
                );

                if ($containerData['disabled']) {
                    continue;
                }

                if ($containerTemplate) {
                    //fix for 2 or more amfinder blocks
                    $setId = (isset($containerData['set_id']) && $containerData['set_id'])
                        ? '+'.$containerData['set_id'] : '';
                    $this->_containers[$containerBlock][$containerTemplate.$setId] = $containerData;
                } elseif ($containerBlock == 'cms/block' && $containerBlockId) {
                    $this->_containers[$containerBlock][$containerBlockId] = $containerData;
                } elseif (!empty($containerName)) {
                    $this->_containers[$containerBlock][$containerName] = $containerData;
                } else {
                    $this->_containers[$containerBlock] = $containerData;
                }
            }
        }

        return $this->_containers;
    }
}
