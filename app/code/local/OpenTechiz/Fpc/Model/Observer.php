<?php

class OpenTechiz_Fpc_Model_Observer
{

    public function catalog_product_save_after(\Varien_Event_Observer $observer)
    {
        if (Mage::registry(__FUNCTION__)) {
            return;
        }
        /* @var $product Mage_Catalog_Model_Product */
        $product = $observer->getProduct();
        if ($product->getStatus() != Mage_Catalog_Model_Product_Status::STATUS_DISABLED) {
            return;
        }
        $productUrl = $this->getProductUrl($product);
        if ($productUrl) {
            $this->clean_up_fpc_crawler_url($productUrl);
        }
        Mage::register(__FUNCTION__, true);
    }

    public function catalog_product_delete_before(\Varien_Event_Observer $observer)
    {
        if (Mage::registry(__FUNCTION__)) {
            return;
        }
        /* @var $product Mage_Catalog_Model_Product */
        $product = $observer->getProduct();
        $productUrl = $this->getProductUrl($product);
        if ($productUrl) {
            $this->clean_up_fpc_crawler_url($productUrl);
        }
        Mage::register(__FUNCTION__, true);
    }

    public function getProductUrl(Mage_Catalog_Model_Product $product)
    {
        $urlRewrite = $product->getUrlModel()->getUrlRewrite()->load("product/{$product->getId()}", 'id_path');
        if (!$urlRewrite || !$urlRewrite->getId() || !$urlRewrite->getRequestPath()) {
            return false;
        }
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $urlRewrite->getRequestPath();
    }

    public function clean_up_fpc_crawler_url($url)
    {
        $crawlerloggedCollection = Mage::getModel('fpccrawler/crawlerlogged_url')->getCollection();
        $crawlerloggedCollection->addFieldToFilter('url', array('in' => array(
                $url,
                $url . '/'
        )));
        $crawlerloggedCollection->addFieldToFilter('sort_by_page_type', 'catalog/product_view');
        if ($crawlerloggedCollection->count() > 0) {
            foreach ($crawlerloggedCollection as $curl) {
                $curl->delete();
            }
        }
        $crawlerCollection = Mage::getModel('fpccrawler/crawler_url')->getCollection();
        $crawlerCollection->addFieldToFilter('url', array('in' => array(
                $url,
                $url . '/'
        )));
        $crawlerCollection->addFieldToFilter('sort_by_page_type', 'catalog/product_view');
        if ($crawlerCollection->count() > 0) {
            foreach ($crawlerCollection as $curl) {
                $curl->delete();
            }
        }
    }

}
