<?php


class OpenTechiz_Fpc_Helper_Processor_Requestcacheid extends Mirasvit_Fpc_Helper_Processor_Requestcacheid
{
    protected function _getRequestId()
    {
        if ($customerId = $this->getLoggedCustomerId()) {
            $customerGroupId = $this->_getCustomerGroupId($customerId); //for logged in user
        } else {
            $customerGroupId = 0;//Tung changed : Todo : why in my session, I see customer id from other customers??
        }

        // Use the same cache for all groups
        $customerGroupId = Mage::helper('fpc/useSameCache')->getFpcCustomerGroup($customerGroupId);

        $currentCurrencyCode = $this->getCurrentCurrencyCode();

        $url = Mage::helper('fpc')->getNormalizedUrl();

        $dependencies = array(
            $url,
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('layout'),
            Mage::app()->getStore()->getCode(),
            Mage::app()->getLocale()->getLocaleCode(),
            $currentCurrencyCode,
            $customerGroupId,
            intval(Mage::app()->getRequest()->isXmlHttpRequest()),
            Mage::app()->getStore()->isCurrentlySecure(),
            Mage::getSingleton('core/design_package')->getTheme('frontend'),
            Mage::getSingleton('core/design_package')->getPackageName(),
        );

        $action = Mage::helper('fpc')->getFullActionCode();

        switch ($action) {
            case 'catalog/category_view':
            case 'splash/page_view':
                $data = Mage::getSingleton('catalog/session')->getData();
                $paramsMap = array(
                    'display_mode' => 'mode',
                    'limit_page' => 'limit',
                    'sort_order' => 'order',
                    'sort_direction' => 'dir',
                );
                foreach ($paramsMap as $sessionParam => $queryParam) {
                    if (isset($data[$sessionParam])) {
                        $dependencies[] = $queryParam.'_'.$data[$sessionParam];
                    }
                }
                break;
        }

        foreach ($this->getConfig()->getUserAgentSegmentation() as $segment) {
            if ($segment['useragent_regexp']
                && preg_match($segment['useragent_regexp'], Mage::helper('core/http')->getHttpUserAgent())) {
                $dependencies[] = $segment['cache_group'];
            }
        }

        if ($this->_requestHelper->isCrawler()) {
            $dependencies = array_merge($dependencies, $this->getWarmerParams($url));
        }

        if (Mage::helper('mstcore')->isModuleInstalled('AW_Mobile2')
            && Mage::helper('aw_mobile2')->isCanShowMobileVersion()
        ) {
            $dependencies[] = 'awMobileGroup';
        }

        if (Mage::helper('mstcore')->isModuleInstalled('Mediarocks_RetinaImages')
            && Mage::getStoreConfig('retinaimages/module/enabled')) {
            $retinaValue = Mage::getModel('core/cookie')->get('device_pixel_ratio');
            $dependencies[] = (!$retinaValue) ? false : $retinaValue;
        }

        if ($deviceType = Mage::helper('fpc/mobile')->getMobileDeviceType()) {
            $dependencies[] = $deviceType;
        }

        if ($this->_custom && in_array('getRequestIdDependencies', $this->_custom)) {
            $dependencies[] = Mage::helper('fpc/customDependence')->getRequestIdDependencies();
        }

        if ($action == 'catalog/product_view'
            && $this->getConfig()->getUpdateStockMethod() == Mirasvit_Fpc_Model_Config::UPDATE_STOCK_METHOD_FRONTEND) {
            $dependencies[] = Mage::helper('fpc/processor_stock')->getProductStock();
        }

        //Pimgento_Product compatibility
        if ($action == 'catalog/product_view'
            && Mage::helper('fpc/processor_frontupdate')->isFrontUpdateEnabled()
            && ($productListingHash = Mage::helper('fpc/processor_frontupdate')->getProductListingHash())) {
            $dependencies[] = $productListingHash;
        }

        $dependencies[] = Mage::helper('custom')->isCookieVersionExisting();

        $requestId = strtolower(implode('/', $dependencies));

        if ($this->getConfig()->isDebugLogEnabled()) {
            Mage::log('Request ID (url from cache): '.$requestId, null, Mirasvit_Fpc_Model_Config::DEBUG_LOG);
        }

        return $requestId;
    }
   
}
