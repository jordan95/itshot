<?php

class OpenTechiz_Fpc_Helper_Response extends Mirasvit_Fpc_Helper_Response
{

    public function updateIgnoredUrlParams(&$content)
    {
        $debug = Mage::helper('fpc/debug');
        $debug->startTimer('FPC_UPDATE_IGNORED_URL_PARAMS');
        preg_match_all('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $content, $match); //get all page urls
        $ignoredUrlParams = Mage::getSingleton('fpc/config')->getIgnoredUrlParams();
        $urlsWithIgnoredUrlParams = array();
        $urlsForReplace = array();
        if (isset($match[0]) && $match[0] && $ignoredUrlParams && count($match[0]) < 2000) {
            foreach ($match[0] as $url) {
                if (strpos($url, '?q=') !== false) {  //check with q param will not replace url
                    $urlsWithIgnoredUrlParams[] = $url;
                    $urlsForReplace[] = $url;
                } else {
                    if(strpos($url, Mage::getBaseUrl()) === FALSE || strpos($url, "?") === FALSE){
                        continue;
                    }
                    $q = substr($url, strpos($url, "?"), strlen($url));
                    $tempIgnore = array();
                    foreach ($ignoredUrlParams as $ig) {
                        $tempIgnore[] = $ig . '=';
                    }
                    if ($q && preg_match('/' . implode('|', $tempIgnore) . '/mis', $q)) {
                        $urlsWithIgnoredUrlParams[] = $url;
                        $urlsForReplace[] = $this->_customPrepareUrl($url, $ignoredUrlParams);
                    }
                }
            }
        }
        if ($urlsWithIgnoredUrlParams && $urlsForReplace) {
            $content = str_replace($urlsWithIgnoredUrlParams, $urlsForReplace, $content);
        }
        $debug->stopTimer('FPC_UPDATE_IGNORED_URL_PARAMS');
    }

    protected function _customPrepareUrl($url, $ignoredUrlParams)
    {
        $url = str_replace('&amp;', '&', $url);
        $urlParsed = explode('?', $url);
        if (isset($urlParsed[1]) && ($urlParsedParams = explode('&', $urlParsed[1]))) {
            foreach ($urlParsedParams as $key => $param) {
                $preparedParam = trim(strtok($param, '='));
                if ($preparedParam) {
                    foreach ($ignoredUrlParams as $_ignoreParam) {
                        if ($_ignoreParam == $preparedParam) {
                            unset($urlParsedParams[$key]);
                        }
                    }
                }
            }
            if ($urlParsedParams) {
                $url = $urlParsed[0] . '?' . implode('&', $urlParsedParams);
            } else {
                $url = $urlParsed[0];
            }
        }
        return $url;
    }

}
