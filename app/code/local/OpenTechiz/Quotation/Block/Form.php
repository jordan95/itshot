<?php

class OpenTechiz_Quotation_Block_Form extends Mage_Core_Block_Template
{
    public function getCurrentProduct(){
        $product = Mage::registry('product');
        if(!$product || !$product->getId()){
            $params = $this->getRequest()->getParams();
            if(!isset($params['id'])){
                return null;
            }else{
                $productId = $params['id'];
                $product = Mage::getModel('catalog/product')->load($productId);
            }
        }

        return $product;
    }
}