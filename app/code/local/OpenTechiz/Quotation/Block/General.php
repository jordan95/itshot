<?php

class OpenTechiz_Quotation_Block_General extends Mage_Core_Block_Template
{
    public function _toHtml(){

        if(!Mage::getStoreConfig('opentechiz_quotation/general/enable')) return null;
        return parent::_toHtml();
    }
}