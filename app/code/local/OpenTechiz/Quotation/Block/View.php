<?php

class OpenTechiz_Quotation_Block_View extends Mage_Core_Block_Template
{
    protected function _getAllQuotation($customerId)
    {
        $model = Mage::getModel('opentechiz_quotation/quotations');
        $items = $model->getCollection()
            ->addFieldToFilter('customer_id', array('eq' => $customerId))
            ->setOrder('quote_id', 'DESC');
        return $items;
    }

    protected function _getProductName($productId)
    {
        $model = Mage::getModel('catalog/product')->load($productId);
        $productName = $model->getName();
        return $productName;
    }

    protected function _getCustomerEmail()
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $custEmail = $customer->getEmail();//get customer email
        }
        return $custEmail;
    }

    public function _getOptionValue($optionId, $option)
    {
        $alloyData = Mage::getModel('opentechiz_material/gold');
        $stoneData = Mage::getModel('opentechiz_material/stone');
        if ($option->getOptionType() == 'stone') {
            echo '<td class="stone-'.$optionId.' option-value">'. $stoneData->load($optionId)->getStoneType() .' ' . $stoneData->load($optionId)->getQuality() . '-' . $stoneData->load($optionId)->getShape() . '-' . $stoneData->load($optionId)->getDiameter() . 'mm-' . $stoneData->load($optionId)->getWeight() . 'ct</td>';
        } elseif ($option->getOptionType() == 'gold') {
            echo '<td class="gold-'. $optionId .' option-value">'. $alloyData->load($optionId)->getGoldType() .' '. $alloyData->load($optionId)->getGoldColor() .'</td>';
        } else {
            echo '<td class="gold-'. $optionId .' option-value">'. $optionId .'</td>';
        }
    }

    protected function _getQuoteProductImage($quoteProductId) {
        $model = Mage::getModel('opentechiz_quotation/product');
        $quoteProductImage = $model->load($quoteProductId)->getQpImage();
        return $quoteProductImage;
    }
}