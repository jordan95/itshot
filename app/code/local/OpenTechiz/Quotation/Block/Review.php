<?php
class OpenTechiz_Quotation_Block_Review extends Mage_Review_Block_Helper
{
    protected $_availableTemplates = array(
        'default' => 'opentechiz/quotation/review/summary.phtml',
        'short'   => 'opentechiz/quotation/review/summary_short.phtml'
    );
}