<?php

class OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set grid params
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('quotationGrid');
        $this->setDefaultSort('quote_id');
        $this->setNoFilterMassactionColumn(true);
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_quotation/quotations')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('quote_id', array(
            'header' => Mage::helper('opentechiz_quotation')->__('Quotation #'),
            'width' => '50',
            'index' => 'quote_id',
            'align' => 'center'
        ));

        $this->addColumn('image', array(
            'header'    => Mage::helper('opentechiz_production')->__('Gallery Image'),
            'index'     => 'image',
            'width'     => '125px',
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Renderer_Image'
        ));

        $this->addColumn('product_id', array(
            'header' => Mage::helper('opentechiz_quotation')->__('Product ID'),
            'index' => 'product_id',
            'width' => '50',
            'align' => 'center'
        ));

        $this->addColumn('url_key', array(
            'header' => Mage::helper('opentechiz_quotation')->__('Url key'),
            'index' => 'url_key',
            'width' => '200',
        ));

        $this->addColumn('description', array(
            'header' => Mage::helper('opentechiz_quotation')->__('Description'),
            'index' => 'description',
        ));

        $this->addColumn('customer_name', array(
            'header' => Mage::helper('opentechiz_quotation')->__('Customer Name'),
            'index' => array('first_name', 'last_name'),
            'type' => 'concat',
            'separator' => ' ',
            'filter_index' => new Zend_Db_Expr("CONCAT(first_name, ' ', last_name)"),
            'width' => '140px',
        ));

        $this->addColumn('customer_email', array(
            'header' => Mage::helper('opentechiz_quotation')->__('Customer Email'),
            'index' => 'email',
            'width' => '200',
        ));

        $this->addColumn('customer_phone', array(
            'header' => Mage::helper('opentechiz_quotation')->__('Customer Phone'),
            'width' => '100',
            'index' => 'phone',
        ));

        $this->addColumn('request_time', array(
            'header' => Mage::helper('opentechiz_quotation')->__('Request time'),
            'index' => 'request_time',
            'type' => 'datetime',
            'width' => '150',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('opentechiz_quotation')->__('Status'),
            'width' => '120',
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::helper('opentechiz_quotation')->getAllStatusQuotation(),
            'align' => 'center',
        ));

        $this->addColumn('action',
            array(
                'header' => Mage::helper('opentechiz_quotation')->__('Action'),
                'width' => '80',
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
                'renderer' => 'OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Renderer_Action',
            ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('quote_id');
        $this->getMassactionBlock()->setFormFieldName('quote');
        $this->getMassactionBlock()->setUseSelectAll(false);
        $status = Mage::helper('opentechiz_quotation')->getAllStatusQuotation();

        // MassDelete
        $this->getMassactionBlock()->addItem('delete_quote', array(
            'label' => Mage::helper('opentechiz_quotation')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('opentechiz_quotation')->__('Are you sure?')
        ));

        // Change status
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('opentechiz_quotation')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('opentechiz_quotation')->__('Status'),
                    'values' => $status
                )
            )
        ));

        return $this;
    }
}
