<?php

class OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_sales_quotations';
        $this->_blockGroup = 'opentechiz_quotation';
        $this->_removeButton('reset');
        $this->_removeButton('delete');
        $this->_updateButton('save', 'label', Mage::helper('opentechiz_quotation')->__('Save Quotations'));
        $this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('quote_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'quote_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'quote_content');
			}
		";

        $quoteId = $this->getRequest()->getParam('id');
        $quote = Mage::getModel('opentechiz_quotation/quotations')->load($quoteId);

        $this->_addButton('send_email', array(
            'label' => $this->__('Send Email'),
            'onclick' => 'setLocation(\'' . $this->getSendEmailUrl($quoteId) . '\')'
        ));
        if($quote->getStatus() != 2) {
            $this->_addButton('Deny', array(
                'label' => $this->__('Deny and send Email'),
                'onclick' => 'setLocation(\'' . $this->getDeniedUrl($quoteId) . '\')'
            ));
        }
        if($quote->getStatus() != 1) {
            $this->_addButton('Approve', array(
                'label' => $this->__('Approve and send Email'),
                'onclick' => 'setLocation(\'' . $this->getApprovedUrl($quoteId) . '\')'
            ));
        }
    }

    public function getSendEmailUrl($id){
        return $this->getUrl('*/sales_quotations/sendEmail', array('id' => $id));
    }

    public function getDeniedUrl($id)
    {
        return $this->getUrl('*/sales_quotations/denyAndSendEmail', array('id' => $id));
    }

    public function getApprovedUrl($id)
    {
        return $this->getUrl('*/sales_quotations/approveAndSendEmail', array('id' => $id));
    }

    protected function _prepareLayout()
    {
//        $this->_addButton('new_product', array(
//            'label'   => Mage::helper('opentechiz_quotation')->__('New Product'),
//            'onclick' => "setLocation('{$this->getUrl('*/catalog_product/new')}')",
//            'class'   => 'add'
//        ),-1);

        return parent::_prepareLayout();
    }

    public function getHeaderText()
    {
        if (Mage::registry('current_quote') && Mage::registry('current_quote')->getId()) {
            return Mage::helper('opentechiz_quotation')->__("Detail for #%s", $this->htmlEscape(Mage::registry('current_quote')->getId()));
        }
    }
}