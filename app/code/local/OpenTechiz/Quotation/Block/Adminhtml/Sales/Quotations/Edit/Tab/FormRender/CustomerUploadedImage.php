<?php
class OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Edit_Tab_FormRender_CustomerUploadedImage extends Mage_Adminhtml_Block_Abstract implements Varien_Data_Form_Element_Renderer_Interface
{

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $quote = Mage::registry('current_quote');
        if($quote->getImage()) {
            $html = '<tr>' .
                '<td class="label"><label>Customer Uploaded Image:</label></td>' .
                '<td class="value">';
            $image = $quote->getImage();
            $html .= '<img style="margin-left: 5px" src="' . Mage::getBaseUrl("media") . "/upload/" .  $image . '" width="120px" height="120px"/>';
            $html .= '</td>';

            return $html;
        }
        else return '';
    }
}