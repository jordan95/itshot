<?php

class OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        if ($row->getdata('image') == "") {
            return "";
        } else {
            /* You can also use some resizer here... */
            return "<img src='" . Mage::getBaseUrl("media") . "/upload/" . $row->getdata('image') . "' width='120px' height='auto'/>";
        }
    }
}