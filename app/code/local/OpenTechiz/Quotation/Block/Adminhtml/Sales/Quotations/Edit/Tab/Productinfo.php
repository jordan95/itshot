<?php

class OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Edit_Tab_Productinfo extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
        $quoteId = $this->getRequest()->getParam('id');
        $product = Mage::getModel('opentechiz_quotation/product')->load($quoteId, 'quote_id');
        Mage::register('current_quote_product', $product);
        $this->setData('quote_product', $product);
    }

    public function getQuote()
    {
        if (!$this->hasData('current_quote')) {
            $this->setData('current_quote', Mage::registry('current_quote'));
        }
        return $this->getData('current_quote');
    }

    public function getQuotationProduct(){
        return $this->getData('quote_product');
    }

    protected function _prepareForm()
    {
        $quotationProduct = Mage::getModel('opentechiz_quotation/product')->getQuotationProduct();
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('product_info_form', array('legend'=>Mage::helper('opentechiz_quotation')->__('Quotation Product')));
        $fieldset->addType('image', 'OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Edit_Tab_FormRender_Image');

        $fieldset->addField('quote_id', 'hidden', array(
            'label' => 'Quote Id:',
            'name' => 'quote_id',
            'value' => $this->getQuote()->getId()
        ));

        $fieldset->addField('qp_name', 'text', array(
            'label' => 'Product Name:',
            'value' => $this->getQuotationProduct()->getQpName(),
            'name' => 'qp_name',
            'required'  => true,
            'after_element_html' => '<br><small>Replace all space with "-". Dont use any other special characters.</small> <br> <small>The name should be unique.</small>'
        ));

        $fieldset->addField('qp_price', 'text', array(
            'label' => 'Price:',
            'name' => 'qp_price',
            'required'  => true,
            'value' => $this->getQuotationProduct()->getQpPrice(),
        ));

        $thumbnail = $fieldset->addField('thumbnail', 'text', array(
            'name'      => 'thumbnail',

        ));

        $thumbnail->setRenderer($this->getLayout()->createBlock('opentechiz_quotation/adminhtml_sales_quotations_edit_tab_formRender_imgThumbnail'));

        $fieldset->addField('image', 'image', array(
            'name'      => 'image[]',
            'multiple'  => 'multiple',
            'label'     => '',
            'title'     => '',
            'after_element_html' => '<br><small>Uploads will override current (if any) images</small>'
        ));

        $fieldset->addField('note', 'textarea', array(
            'label' => 'Note:',
            'name' => 'note',
            'value' => $this->getQuotationProduct()->getNote(),
        ));

        $field = $fieldset->addField('options', 'text', array(
            'name'      => 'options',
        ));

        $field->setRenderer($this->getLayout()->createBlock('opentechiz_quotation/adminhtml_sales_quotations_edit_tab_formRender_multiOptions'));

        $fieldset->addField('url', 'link', array(
            'label' => 'Quotation Product Url:',
            'value' => $quotationProduct->getProductUrl(). str_replace(' ', '', $this->getQuotationProduct()->getQpName()),
            'href'  => $quotationProduct->getProductUrl(). str_replace(' ', '', $this->getQuotationProduct()->getQpName()),
        ));

        return parent::_prepareForm();
    }
}
