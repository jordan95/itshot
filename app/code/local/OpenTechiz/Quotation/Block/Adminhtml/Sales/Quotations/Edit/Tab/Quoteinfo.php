<?php

class OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Edit_Tab_Quoteinfo extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getQuote()
    {
        if (!$this->hasData('current_quote')) {
            $this->setData('current_quote', Mage::registry('current_quote'));
        }
        return $this->getData('current_quote');
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $general = $form->addFieldset('general', array('legend' => Mage::helper('opentechiz_quotation')->__('General')));

        $general->addField('customer_name', 'label', array(
            'label' => 'Customer Name:',
            'value' => $this->getQuote()->getFirstName() .' '. $this->getQuote()->getLastName()
        ));

        $general->addField('email', 'label', array(
            'label' => 'Customer Email:',
            'value' => $this->getQuote()->getEmail()
        ));

        $general->addField('phone', 'label', array(
            'label' => 'Customer Phone:',
            'value' => $this->getQuote()->getPhone()
        ));

        $productLink = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $this->getQuote()->getUrlKey();
        $general->addField('product_url', 'link', array(
            'label' => 'Product URL:',
            'value' => $productLink,
            'href' => $productLink
        ));

        $cusomerSupport = $form->addFieldset('customer_support', array('legend' => Mage::helper('opentechiz_quotation')->__('Customer Support')));

        $cusomerSupport->addField('description', 'textarea', array(
            'label' => 'Description:',
            'value' => $this->getQuote()->getDescription(),
            'name' => 'description'
        ));
        $cusomerSupport->addField('price_rage_from', 'text', array(
            'label' => 'Price From:',
            'value' => $this->getQuote()->getPriceRangeFrom(),
            'name' => 'price_rage_from'
        ));
        $cusomerSupport->addField('price_rage_to', 'text', array(
            'label' => 'Price To:',
            'value' => $this->getQuote()->getPriceRangeTo(),
            'name' => 'price_rage_to'
        ));
        $cusomerSupport->addField('size', 'text', array(
            'label' => 'Size:',
            'value' => $this->getQuote()->getSize(),
            'name' => 'size'
        ));
        $cusomerSupport->addField('metal', 'text', array(
            'label' => 'Metal:',
            'value' => $this->getQuote()->getMetal(),
            'name' => 'metal'
        ));
        $cusomerSupport->addField('stone', 'text', array(
            'label' => 'Stone:',
            'value' => $this->getQuote()->getStone(),
            'name' => 'stone'
        ));

        $thumbnail = $cusomerSupport->addField('thumbnail', 'text', array(
            'name'      => 'thumbnail',
        ));

        $thumbnail->setRenderer($this->getLayout()->createBlock('opentechiz_quotation/adminhtml_sales_quotations_edit_tab_formRender_customerUploadedImage'));

        return parent::_prepareForm();
    }
}
