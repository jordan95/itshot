<?php

class OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_sales_quotations';
        $this->_blockGroup = 'opentechiz_quotation';
        $this->_headerText = Mage::helper('opentechiz_quotation')->__('Quotations');
        parent::__construct();

        // Remove the Add button as by default it is usually visible
        $this->_removeButton('add');
    }
}