<?php

class OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Edit_Tab_FormRender_MultiOptions extends Mage_Adminhtml_Block_Abstract implements Varien_Data_Form_Element_Renderer_Interface
{

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $product = Mage::registry('current_quote_product');

        $alloyData = OpenTechiz_Quotation_Helper_Data::getAlloyOptions();
        $stoneData = OpenTechiz_Quotation_Helper_Data::getStoneOptions();

        if ($product->getId()) {
            $optionModel = Mage::getModel('opentechiz_quotation/productOption');
            $optionCollection = $optionModel->getCollection()->addFieldToFilter('quote_product_id', $product->getId());
            $html = '<tr>' .
                '<td class="label"><label>Options:</label></td>' .
                '<td class="value">';

            $html .= '<input name="productId" type="hidden" value="' . $product->getId() . '"/>';
            $html .= '<input id="deleted" name="deleted" type="hidden" value=""/>';

            $html .= '<table id="options-table">
                  <tr>
                    <th>Type</th>
                    <th>Option Name</th>
                    <th>Value</th>
                    <th>Quantity</th>
                  </tr>';
            $count = 1;

            foreach ($optionCollection as $option) {
                $html .= '<tr id="row' . $count . '">
                    <input type="hidden" name="id' . $count . '" value="' . $option->getId() . '"/>
                    <td><select name="type' . $count . '" required id="type' . $count . '" onchange="changeType(' . $count . ')">
                            <option value="">Choose option type</option>
                            <option value="stone" ' . $this->isSelected('stone', $option) . '>Stone</option>
                            <option value="gold" ' . $this->isSelected('gold', $option) . '>Gold</option>
                            <option value="other" ' . $this->isSelected('other', $option) . '>Other</option>
                        </select>
                    </td>';
                if ($option->getOptionType() == 'stone') {
                    $html .= '<td>
                                <select name="name' . $count . '" required id="name' . $count . '">
                                    ';
                    foreach (OpenTechiz_Quotation_Helper_Data:: OPTION_NAME_STONE as $key => $value) {
                        $html .= '<option value="' . $value . '" ' . $this->isSelected($value, $option) . '>' . $value . '</option>';
                    }

                    $html .= '  </select>
                              </td>';
                } elseif ($option->getOptionType() == 'gold') {
                    $html .= '<td>
                                <select name="name' . $count . '" required id="name' . $count . '">
                                    ';
                    foreach (OpenTechiz_Quotation_Helper_Data:: OPTION_NAME_ALLOY as $key => $value) {
                        $html .= '<option value="' . $value . '" ' . $this->isSelected($value, $option) . '>' . $value . '</option>';
                    }
                    $html .= '  </select>
                              </td>';
                } else {
                    $html .= '<td><input name="name' . $count . '" value="' . $option->getOptionName() . '"/></td>';
                }

                if ($option->getOptionType() == 'stone') {
                    $html .= '<td>
                                <select name="value' . $count . '" required id="value' . $count . '">
                                    ';

                    foreach ($stoneData as $stone) {
                        $html .= '<option value="' . $stone->getId() . '" ' . $this->isSelected($stone->getId(), $option) . '> ' . $stone->getStoneType() . ' ' . $stone->getQuality() . '-' . $stone->getShape() . '-' . $stone->getDiameter() . 'mm-' . $stone->getWeight() . 'ct </option>';
                    }

                    $html .= '  </select>
                              </td>';
                } elseif ($option->getOptionType() == 'gold') {
                    $html .= '<td>
                                <select name="value' . $count . '" required id="value' . $count . '">
                                    ';
                    foreach ($alloyData as $key => $value) {
                        $html .= '<option value="' . $key . '" ' . $this->isSelected($key, $option) . '>' . $value . '</option>';
                    }
                    $html .= '</select>
                              </td>';
                } else {
                    $html .= '<td><input name="value' . $count . '" value="' . $option->getOptionValue() . '"/></td>';
                }

                $html .= '<td><input name="qty' . $count . '" value="' . $option->getQty() . '"/></td>
                    <input name="sort' . $count . '" type="hidden" value="' . $option->getOptionSortOrder() . '"/>
                    <td><button onclick="deleteRow(' . $count . ',' . $option->getId() . ')" type="button">Delete</button></td>
                  </tr>';
                $count++;
            }

        } else {
            $html = '<tr>' .
                '<td class="label"><label>Options (optional):</label></td>' .
                '<td class="value">';
            $count = 0;

            $html .= '<table id="options-table">
                  <tr>
                    <th>Type</th>
                    <th>Option Name</th>
                    <th>Value</th>
                    <th>Quantity</th>
                    <th></th>
                  </tr>';
        }
        $html .= '</table></td></tr>';

        $html .= '<tr><td class="label"></td><td class="value"><table style="border: none"><tr><td style="width: 53px; float: right; border: none"><button onclick="addRow()" style="float: right; width: 100%" type="button"> Add</button></td></tr></table></td></tr>';

        $html .= '<style>
                    table {
                      font-family: arial, sans-serif;
                      border-collapse: collapse;
                      width: 100%;
                    }

                    td, th {
                      border: 1px solid #dddddd;
                      text-align: left;
                      padding: 8px;
                    }
                </style>
                
                <script>
                var stoneData = [];
                var alloyData = [];
                 ';
        foreach ($stoneData as $stone) {
            $html .= 'stoneData[' . $stone->getId() . '] = "' . $stone->getStoneType() . ' ' . $stone->getQuality() . '-' . $stone->getShape() . '-' . $stone->getDiameter() . 'mm-' . $stone->getWeight() . 'ct";';
        }
        foreach ($alloyData as $key => $value) {
            $html .= 'alloyData[' . $key . '] = "' . $value . '";';
        }


        $html .= '</script>';

        $html .= '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script>
                jQuery.noConflict();
                var count = ' . $count . ';
                var deleted = [];
                
                
                 function addRow() {
                   var row = "row"+count;
                   var name = "name"+count;
                   var value = "value"+count;
                   var type = "type"+count;
                   var qty = "qty"+count;
                   var sort = "sort"+count;
                   var deleteBtn = "<button onclick=deleteRow("+count+") type=button> Delete</button>";
                   
                   var html = "<tr id="+row+"> " +
                   "<td><select name="+type+" id="+type+" onchange=changeType("+count+") required>"+
                            "<option value=>Choose option type</option>"+
                            "<option value=stone>Stone</option>"+
                            "<option value=gold>Gold</option>"+
                            "<option value=other>Other</option>"+
                        "</select>"+
                    "</td>"+
                    "<td><input name="+name+" id="+name+" /></td>" +
                    "<td><input name="+value+" id="+value+" /></td>" +
                    "<td><input name="+qty+" /></td>" +
                    "<input name="+sort+" type=hidden />" +
                    "<td>"+deleteBtn+"</td>" +
                    "</tr>";
                   
                   jQuery("#options-table").append(html);
                   count ++;
                }
                
                function changeType(count) {
                    var value = document.getElementById("type"+count).value;
                    if(value === "stone"){
                        var namehtml = "<select name=name"+count+" id=name"+count+" required>"+
                            "<option value=\'Center Stone\'>Center Stone</option>"+
                            "<option value=\'Stone 2\'>Stone 2</option>"+
                            "<option value=\'Stone 3\'>Stone 3</option>"+
                            "<option value=\'Stone 4\'>Stone 4</option>"+
                        "</select>";
                        var valuehtml = "<select name=value"+count+" id=value"+count+" required>";
                        for (var key in stoneData){
                            if (stoneData.hasOwnProperty(key)) {
                                 valuehtml += "<option value="+key+">"+stoneData[key]+"</option>"
                            }
                        }
                        valuehtml += "</select>";
                        document.getElementById("name"+count).outerHTML = namehtml;
                        document.getElementById("value"+count).outerHTML = valuehtml;
                    }else if(value === "gold"){
                        var namehtml = "<select name=name"+count+" id=name"+count+" required>"+
                            "<option value=Metal selected>Metal</option>"+
                        "</select>";
                        var valuehtml = "<select name=value"+count+" id=value"+count+" required>";
                        for (var key in alloyData){
                            if (alloyData.hasOwnProperty(key)) {
                                valuehtml += "<option value="+key+">"+alloyData[key]+"</option>"
                            }
                        }
                        valuehtml += "</select>";
                        document.getElementById("name"+count).outerHTML = namehtml;
                        document.getElementById("value"+count).outerHTML = valuehtml;
                    } else{
                        var namehtml = "<input name=name"+count+" id=name"+count+" />";
                        var valuehtml = "<input name=value"+count+" id=value"+count+" />";
                        
                        document.getElementById("name"+count).outerHTML = namehtml;
                        document.getElementById("value"+count).outerHTML = valuehtml;
                    }
                }
                
                function deleteRow(id, optionId = "") {
                    document.getElementById("row"+id).outerHTML = "";
                    if(optionId !== ""){
                        deleted.push(optionId);
                        document.getElementById("deleted").value = deleted;
                        console.log(deleted);
                    }
                }
                </script>';
        return $html;
    }

    public function isSelected($value, $option)
    {
        if ($value == $option->getOptionType() || $value == $option->getOptionName() || $value == $option->getOptionValue()) {
            return 'selected';
        } else return '';
    }
}