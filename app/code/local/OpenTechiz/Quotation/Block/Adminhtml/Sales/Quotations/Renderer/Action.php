<?php

class OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Renderer_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        if ($row->getId())
            return sprintf('
				<a href="%s" title="Detail">%s</a><br><a href="%s" title="Delete">%s</a>',
                $this->getUrl('*/*/detail', array('_current' => true, 'id' => $row->getId())),
                'Detail',
                $this->getUrl('*/*/delete', array('_current' => true, 'id' => $row->getId())),
                'Delete'
            );
        else
            return sprintf('%s', 'Edit');
    }

}