<?php
class OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Edit_Tab_FormRender_ImgThumbnail extends Mage_Adminhtml_Block_Abstract implements Varien_Data_Form_Element_Renderer_Interface
{

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $product = Mage::registry('current_quote_product');
        $html = '<tr>' .
            '<td class="label"><label>Design Images:</label></td>' .
            '<td class="value">';
        $images = $product->getQpImage();
        if($images != ''){
            $imagesArray = explode(',', $images);
            foreach ($imagesArray as $image){
                $html .= '<img style="margin-left: 5px" src="'.$image.'" width="120px" height="120px"/>';
            }
        }
        $html.='</td>';
        return $html;
    }
}