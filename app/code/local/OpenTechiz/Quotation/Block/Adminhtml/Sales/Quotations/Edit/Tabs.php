<?php

class OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('quote_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('opentechiz_quotation')->__('Detail Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('opentechiz_quotation')->__('Quotation Information'),
            'title'     => Mage::helper('opentechiz_quotation')->__('Quotation Information'),
            'content'   => $this->getLayout()->createBlock('opentechiz_quotation/adminhtml_sales_quotations_edit_tab_quoteinfo')->toHtml(),
            'active'    => Mage::registry('current_quote')->getId() ? false : true
        ));

        $this->addTab('product_info', array(
            'label'     => Mage::helper('opentechiz_quotation')->__('Quotation Product'),
            'title'     => Mage::helper('opentechiz_quotation')->__('Quotation Product'),
            'content'   => $this->getLayout()->createBlock('opentechiz_quotation/adminhtml_sales_quotations_edit_tab_productinfo')->toHtml(),
        ));

        $this->addTab('quotation_comments', array(
            'label'     => Mage::helper('opentechiz_quotation')->__('Quotation Comments'),
            'title'     => Mage::helper('opentechiz_quotation')->__('Quotation Comments'),
            'content'   => $this->getLayout()->createBlock('opentechiz_quotation/adminhtml_sales_quotations_edit_tab_quotecomments')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }


    public function getQuote()
    {
        if (!$this->hasData('current_quote')) {
            $this->setData('current_quote', Mage::registry('current_quote'));
        }
        return $this->getData('current_quote');
    }
}