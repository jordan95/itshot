<?php

class OpenTechiz_Quotation_Block_Adminhtml_Sales_Quotations_Edit_Tab_Quotecomments extends Mage_Adminhtml_Block_Widget_Form
{
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('opentechiz/quotation/comment.phtml');
    }
}