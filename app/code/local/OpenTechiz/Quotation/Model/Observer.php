<?php

class OpenTechiz_Quotation_Model_Observer
{
    public function modifyPrice(Varien_Event_Observer $obs)
    {
        $check_product_quotation = Mage::app()->getRequest()->getParam('check_product_quotation');
        if(isset($check_product_quotation) && $check_product_quotation =='1'){
                // Get the quote item
                $item = $obs->getQuoteItem();
                // Ensure we have the parent item, if it has one
                $item = ($item->getParentItem() ? $item->getParentItem() : $item);
                // Load the custom price
                $price = $this->_getPriceByItem($item);
                // Set the custom price
                $item->setCustomPrice($price);
                $item->setOriginalCustomPrice($price);
                // Enable super mode on the product.
                $item->getProduct()->setIsSuperMode(true);
        }

    }

    protected function _getPriceByItem(Mage_Sales_Model_Quote_Item $item)
    {
        $sku = $item->getSku();
        if (strpos($item->getSku(), 'quotation') !== false) {

            $quotationProductId = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[1];

            $price = Mage::getModel('opentechiz_quotation/product')->load($quotationProductId)->getQpPrice();
        } else {
            $price = $item->getPrice();
        }
        return $price;
    }

    public function changeOrderItemNameAndSku(Varien_Event_Observer $obs)
    {
        $order = $obs->getEvent()->getOrder();

        $orderItem = $order->getAllItems();
        foreach ($orderItem as $item) {
            if ($item->getSku() == 'quotation') {
                $option = unserialize($item->getData('product_options'));
                Mage::log($option);
                $quoteProductId = $option['info_buyRequest']['options']['quotation_product_id'];
                $item->setSku($item->getSku() . OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . $quoteProductId);
            }
        }
    }

    public function sendNotificationToEmail(Varien_Event_Observer $observer)
    {
        $quoteData = $observer->getEvent()->getQuoteData();
        $user = $quoteData['user'];
        $quotationId    = $quoteData['quotation_id'];

        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        $recipientEmail = $this->_getRecipientEmail($user, $quotationId);

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('quotation_noty_message');
        $emailTemplate->setTemplateSubject('Quotation Notification Message');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $admin = true;

        if ($this->_isAdminUser($user)) {
            $admin = false;
        }

        $var = [
            'quotation_id'  => $quotationId,
            'admin'         => $admin,
            'customer_name' => $this->_getCustomerName($user, $quotationId)
        ];

        $processedTemplate = $emailTemplate->getProcessedTemplate($var);

        $emailTemplate->send($recipientEmail, $var);
    }

    protected function _getRecipientEmail($user, $quotationId)
    {
        $quote = Mage::getModel('opentechiz_quotation/quotations');
        if ($this->_isAdminUser($user)) {
            return $quote->load($quotationId)->getEmail();
        } else {
            return Mage::getStoreConfig('contacts/email/recipient_email');
        }
    }

    protected function _isAdminUser($user)
    {
        $flag = false;
        $userAdmin = Mage::getModel('admin/user')->getCollection()->addFieldToFilter('username', $user);
        if ($userAdmin->getData()) {
            $flag = true;
        }
        return $flag;
    }

    protected function _getCustomerName($user, $quotationId)
    {
        $quote = Mage::getModel('opentechiz_quotation/quotations')->load($quotationId);
        if ($this->_isAdminUser($user)) {
            $customerName = $quote->getFirstName();
            return $customerName;
        }
    }

    public function addRouteQuotationToRecaptcha(Varien_Event_Observer $observer) {
        $routes = $observer->getRoutes();
        $routes->add('quotation_index_saveQuotation', Mage::helper('studioforty9_recaptcha')->__('Quotation Form'));
    }
}