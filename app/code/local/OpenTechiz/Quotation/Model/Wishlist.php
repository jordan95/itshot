<?php
class OpenTechiz_Quotation_Model_Wishlist extends OpenTechiz_Wishlist_Model_Wishlist
{
    public function saveItemForLater($quote_item_id, $quotationSku = null)
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            throw new Mage_Core_Exception($this->__('Please login'), 4);
        }
        $quote_item = $this->_getCart()->getQuote()->getItemById($quote_item_id);
        if (!$quote_item || !$quote_item->getId()) {
            throw new Mage_Core_Exception('Quote item is not exist');
        }

        $list_id = $this->getListId();
        $options = array();
        $product = $quote_item->getProduct();
        $custom_options = Mage::helper('catalog/product_configuration')->getCustomOptions($quote_item);
        foreach ($custom_options as $opt) {
            if (isset($opt['option_type']) && $opt['option_type'] == 'drop_down') {
                $vals = $product->getOptionById($opt['option_id'])->getValues();
                foreach ($vals as $val) {
                    if ($val->getData('title') == $opt['value']) {
                        $options[$opt['option_id']] = $val->getData('option_type_id');
                    }
                }
            } else {
                $options[$opt['option_id']] = $opt['value'];
            }
        }
        if($quotationSku){
            $quotationProductId = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $quotationSku)[1];
            $options['quotation_product_id'] = $quotationProductId;
        }

        $item = Mage::getModel('amlist/item')
            ->setProductId($quote_item->getProductId())
            ->setBuyRequest(serialize(array(
                'product' => $quote_item->getProductId(),
                'options' => $options,
            )))
            ->setListId($list_id)
            ->setQty($quote_item->getQty());
        $id = $item->findDuplicate();
        if ($id) {
            $item->setId($id);
        }
        $item->save();
        $this->_getCart()->removeItem($quote_item_id)
            ->save();
    }
}