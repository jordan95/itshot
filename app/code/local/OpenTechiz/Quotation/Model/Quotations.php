<?php

class OpenTechiz_Quotation_Model_Quotations extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_quotation/quotations');
    }

    public function generateIncrementId() {
        // prefix
        $new = date('mdY') . 'QUOTE';


        //find next with a limit off 3000 per day
        for ($i = 1; $i < 3000; $i++) {

            // define next number
            $current = $new . $i;

            // check if exist
            $collection = $this->getCollection()->addFieldToFilter('increment_id', $current);

            // if not return current
            if (sizeof($collection) == 0){
                return $current;
            }
        }
        return $new;
    }
}