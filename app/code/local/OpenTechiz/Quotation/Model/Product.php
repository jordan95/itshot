<?php

class OpenTechiz_Quotation_Model_Product extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_quotation/product');
    }

    public function getQuotationProduct()
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product');
        $productId = $product->getIdBySku(OpenTechiz_Quotation_Helper_Data::QUOTATION_PRODUCT_SKU);
        if ($productId) {
            return $product->load($productId);
        }

        $websites = Mage::getModel('core/website')->getCollection()->toArray(array('website_id'));
        $websitesIds = array();
        if (!empty($websites['items'])) {
            foreach ($websites['items'] as $website) {
                $websitesIds[] = $website['website_id'];
            }
        } else {
            $websitesIds[] = 1;
        }

        try {
            $product
                ->setStoreId(0)
                ->setWebsiteIds($websitesIds)
                ->setAttributeSetId(Mage::getModel('catalog/product')->getDefaultAttributeSetId())
                ->setTypeId('quotation')
                ->setCreatedAt(strtotime('now'))
                ->setUpdatedAt(strtotime('now'))
                ->setSku(OpenTechiz_Quotation_Helper_Data::QUOTATION_PRODUCT_SKU)
                ->setName('Quotation Product')
                ->setUrlKey('quotation-product')
                ->setWeight(0)
                ->setStatus(1)
                ->setTaxClassId(0)
                ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH)
                ->setPrice(0)
                ->setCost(0)
                ->setDescription('Quotation Product')
                ->setShortDescription('Quotation Product')
                ->setStockData(array(
                        'use_config_manage_stock' => 0,
                        'manage_stock' => 0,
                        'min_sale_qty' => 1,
                        'max_sale_qty' => 1,
                        'is_in_stock' => 1,
                        'qty' => 999
                    )
                );
            $product->save();
        } catch (Exception $e) {
            Mage::log($e->getMessage());
            throw $e;
        }

        return $product;
    }

    public function getProductBaseUrl(){
        $baseUrl = Mage::getBaseUrl();
        $baseUrl = trim($baseUrl, '/');
        return substr($baseUrl, 0, strrpos( $baseUrl, '/'));;
    }

    public function getQuoteProductOption($quoteProductId){
        return Mage::getModel('opentechiz_quotation/productOption')->getCollection()->addFieldToFilter('quote_product_id', $quoteProductId);
    }

    public function getStoneName($stoneId){
        $stone =  Mage::getModel('opentechiz_material/stone')->load($stoneId);
        return strtolower($stone->getStoneType(). ' '. $stone->getQuality());
    }

    public function getGoldName($goldId){
        $gold =  Mage::getModel('opentechiz_material/gold')->load($goldId);
        return strtolower($gold->getGoldType().' '.$gold->getGoldColor());
    }
}