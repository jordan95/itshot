<?php

class OpenTechiz_Quotation_Model_QuotationSave extends Mage_Core_Model_Abstract
{
    public function saveOptions($params, $productId){
        $quoteProductOptionModel = Mage::getModel('opentechiz_quotation/productOption');
        $counter = [];
        foreach ($params as $key => $value){
            if (strpos($key, 'sort') !== false) {
                array_push($counter, trim($key, 'sort'));
            }
        }
        $options = '';

        foreach($counter as $number){
            $data = [];
            $data['quote_product_id'] = $productId;
            $data['option_name'] = $params['name'.$number];
            $data['option_value'] = $params['value'.$number];
            $data['option_type'] = $params['type'.$number];
            $data['qty'] = $params['qty'.$number];
            $data['option_sort_order'] = $params['sort'.$number];
            
            if(array_key_exists('id'.$number, $params)){
                $productOption = $quoteProductOptionModel->addData($data)->setId($params['id'.$number])->save();
            } else $productOption = $quoteProductOptionModel->setData($data)->save();

            $options .= $productOption->getId() . ',';
        }

        if(isset($params['deleted'])) {
            foreach (explode(',', $params['deleted']) as $deleted) {
                $option = $quoteProductOptionModel->load((int)$deleted);
                if ((int)$deleted > 0 && $option->getId()) $option->delete();
            }
        }
        return trim($options, ',');
    }
}