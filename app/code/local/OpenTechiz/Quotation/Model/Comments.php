<?php
class OpenTechiz_Quotation_Model_Comments extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_quotation/comments');
    }
}