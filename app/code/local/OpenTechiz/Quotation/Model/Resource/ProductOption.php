<?php

class OpenTechiz_Quotation_Model_Resource_ProductOption extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_quotation/productOption', 'quote_product_option_id');
    }
}