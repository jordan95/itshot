<?php

class OpenTechiz_Quotation_Model_Resource_Comments extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_quotation/comments', 'id');
    }
}