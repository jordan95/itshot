<?php

class OpenTechiz_Quotation_Model_Resource_ProductOption_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_quotation/productOption');
    }
}