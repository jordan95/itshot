<?php

class OpenTechiz_Quotation_ViewController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        // check if customer is login
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->loadLayout()
                ->renderLayout();
        } else
            $this->_redirect('customer/account/login');
    }

    public function detailAction()
    {
        $model = Mage::getModel('opentechiz_quotation/quotations');
        $quoteId = $this->getRequest()->getParam('id');
        $allQuoteIds = $model->getCollection()->getAllIds();
        if (in_array($quoteId, $allQuoteIds)) {
            $this->loadLayout()
                ->renderLayout();
        } else {
            $this->_forward('defaultNoRoute');
        }
    }

    public function saveCommentAction()
    {
        $data = $this->getRequest()->getPost();

        /* Session errors */
        $session = Mage::getSingleton('core/session');


        try {
            if ($data['message'] != '') {
                $model = Mage::getModel('opentechiz_quotation/comments');
                $model->setData($data);
                $model->setCreatedAt(Mage::helper('opentechiz_quotation')->_getCurrentTime());
                $model->save();
                $session->addSuccess('Send sucessfully!');

                Mage::dispatchEvent('quotation_view_saveComment_after', array('quote_data' => $data));
            } else {
                $session->addError('Send comment failed');
            }
        } catch (Exception $e) {
            $session->addError($e->getMessage());
            return false;
        }

        $this->_redirectReferer();
    }
}