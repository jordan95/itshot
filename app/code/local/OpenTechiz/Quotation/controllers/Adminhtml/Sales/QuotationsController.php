<?php

class OpenTechiz_Quotation_Adminhtml_Sales_QuotationsController extends Mage_Adminhtml_Controller_Action
{
    protected function _initQuote($idFieldName = 'id')
    {
        $quoteId = (int)$this->getRequest()->getParam($idFieldName);
        $quote = Mage::getModel('opentechiz_quotation/quotations');

        if ($quoteId) {
            $quote->load($quoteId);
        }

        Mage::register('current_quote', $quote);
        return $this;
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/quotations')
            ->_title($this->__('Sales'))
            ->_title($this->__('Quotations'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function deleteAction()
    {
        $this->_initQuote();
        $quote = Mage::registry('current_quote');
        if ($quote->getId()) {
            try {
                $quoteId = $quote->getId();
                $quote->load($quoteId);
                $quote->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('The item has been deleted.'));

                $admin = Mage::getSingleton('admin/session')->getUser();
                if($admin->getId()){
                    Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Delete Quote #'.$quoteId));
                }

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/sales_quotations');
    }

    public function detailAction()
    {
        $this->_initQuote();
        $quote = Mage::registry('current_quote');
        $quoteId = $quote->getId();
        if ($quoteId || $quoteId == $this->getRequest()->getParam('id')) {

            $this->_initAction();
            $this->_title($this->__('Detail'));
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('opentechiz_quotation/adminhtml_sales_quotations_edit'))
                ->_addLeft($this->getLayout()
                    ->createBlock('opentechiz_quotation/adminhtml_sales_quotations_edit_tabs')
                );
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_material')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function denyAndSendEmailAction()
    {
        $quoteId = $this->getRequest()->getParam('id');
        $quote = Mage::getModel('opentechiz_quotation/quotations')->load($quoteId)->setStatus(2)->save();
        $productId = $quote->getProductId();
        $quotationProduct = Mage::getModel('opentechiz_quotation/product')->load($quoteId, 'quote_id');
        $productUrl = $quotationProduct->getProductBaseUrl() .'/quotation-product/'. str_replace(' ', '', $quotationProduct->getQpName());

        $productName = Mage::getModel('catalog/product')->load($productId)->getName();

        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = $quote->getEmail();
        $name = $quote->getFirstName();

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('deny_quotation_template');
        $emailTemplate->setTemplateSubject('Itshot.com: Quotation Status Notification');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $html = '';
        $productUrl = '';

        $emailTemplateVariables = array();
        $emailTemplateVariables['url'] = Mage::getUrl('customer/account/login');
        $emailTemplateVariables['product_name'] = $productName;
        $emailTemplateVariables['name'] = $quote->getFirstName();
        $emailTemplateVariables['product_url'] = $productUrl;
        $emailTemplateVariables['option_html'] = $html;
        $emailTemplateVariables['logo_url'] = OpenTechiz_Quotation_Helper_Data::EMAIL_LOGO;

        $emailTemplate->send($to, $name, $emailTemplateVariables);

        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('opentechiz_material')->__('Item denied'));
        $this->_redirect('*/*/detail/id/'.$quoteId);
    }

    public function sendEmailAction(){
        $quoteId = $this->getRequest()->getParam('id');
        $quote = Mage::getModel('opentechiz_quotation/quotations')->load($quoteId);
        $productId = $quote->getProductId();
        $quotationProduct = Mage::getModel('opentechiz_quotation/product')->load($quoteId, 'quote_id');
        $productUrl = $quotationProduct->getProductBaseUrl() .'/quotation-product/'. str_replace(' ', '', $quotationProduct->getQpName());

        $productName = Mage::getModel('catalog/product')->load($productId)->getName();

        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = $quote->getEmail();
        $name = $quote->getFirstName();

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('email_quotation_template');
        $emailTemplate->setTemplateSubject('Itshot.com: Quotation Status Notification');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $html = '<table><tbody>';
        $optionCollection = Mage::getModel('opentechiz_quotation/productOption')->getCollection()->addFieldToFilter('quote_product_id', $quotationProduct->getId());
        foreach ($optionCollection as $option){
            $html .= '<tr>';
            $html .= '<td class="label">'.$option->getOptionName().'</td>';
            if($option->getOptionType() == 'stone'){
                $stone = Mage::getModel('opentechiz_material/stone')->load($option->getOptionValue());
                $html .= '<td class="value">' . ucfirst($stone->getStoneType());
                if($stone->getQuality()){
                    $html .= ' - '.strtoupper($stone->getQuality());
                }
                $html .= '</td>';
            }elseif($option->getOptionType() == 'gold'){
                $gold = Mage::getModel('opentechiz_material/gold')->load($option->getOptionValue());
                $html .= '<td class="value">' . ucfirst($gold->getGoldType()).' '.ucfirst($gold->getGoldColor()) .'</td>';
            }else {
                $html .= '<td class="value">' . $option->getOptionValue().'</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</tbody></table>';

        $productUrl = '<p>Your Customized product can be viewed and purchased <a href="'.$productUrl.'">here</a></p>';

        $emailTemplateVariables = array();
        $emailTemplateVariables['url'] = Mage::getUrl('customer/account/login');
        $emailTemplateVariables['product_name'] = $productName;
        $emailTemplateVariables['name'] = $quote->getFirstName();
        $emailTemplateVariables['product_url'] = $productUrl;
        $emailTemplateVariables['option_html'] = $html;
        $emailTemplateVariables['logo_url'] = OpenTechiz_Quotation_Helper_Data::EMAIL_LOGO;

        $emailTemplate->send($to, $name, $emailTemplateVariables);

        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('opentechiz_material')->__('Email Sent'));
        $this->_redirect('*/*/detail/id/'.$quoteId);
    }

    public function approveAndSendEmailAction()
    {
        $quoteId = $this->getRequest()->getParam('id');
        $quote = Mage::getModel('opentechiz_quotation/quotations')->load($quoteId)->setStatus(1)->save();
        $productId = $quote->getProductId();
        $quotationProduct = Mage::getModel('opentechiz_quotation/product')->load($quoteId, 'quote_id');
        $productUrl = $quotationProduct->getProductBaseUrl() .'/quotation-product/'. str_replace(' ', '', $quotationProduct->getQpName());

        $productName = Mage::getModel('catalog/product')->load($productId)->getName();

        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = $quote->getEmail();
        $name = $quote->getFirstName();

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('approve_quotation_template');
        $emailTemplate->setTemplateSubject('Itshot.com: Quotation Status Notification');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $html = '<table><tbody>';
        $optionCollection = Mage::getModel('opentechiz_quotation/productOption')->getCollection()->addFieldToFilter('quote_product_id', $quotationProduct->getId());
        foreach ($optionCollection as $option){
            $html .= '<tr>';
            $html .= '<td class="label">'.$option->getOptionName().'</td>';
            if($option->getOptionType() == 'stone'){
                $stone = Mage::getModel('opentechiz_material/stone')->load($option->getOptionValue());
                $html .= '<td class="value">' . ucfirst($stone->getStoneType());
                if($stone->getQuality()){
                    $html .= ' - '.strtoupper($stone->getQuality());
                }
                $html .= '</td>';
            }elseif($option->getOptionType() == 'gold'){
                $gold = Mage::getModel('opentechiz_material/gold')->load($option->getOptionValue());
                $html .= '<td class="value">' . ucfirst($gold->getGoldType()).' '.ucfirst($gold->getGoldColor()) .'</td>';
            }else {
                $html .= '<td class="value">' . $option->getOptionValue().'</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</tbody></table>';

        $productUrl = '<p>Your Customized product can be viewed and purchased <a href="'.$productUrl.'">here</a></p>';

        $emailTemplateVariables = array();
        $emailTemplateVariables['url'] = Mage::getUrl('customer/account/login');
        $emailTemplateVariables['product_name'] = $productName;
        $emailTemplateVariables['name'] = $quote->getFirstName();
        $emailTemplateVariables['product_url'] = $productUrl;
        $emailTemplateVariables['option_html'] = $html;
        $emailTemplateVariables['logo_url'] = OpenTechiz_Quotation_Helper_Data::EMAIL_LOGO;

            $emailTemplate->send($to, $name, $emailTemplateVariables);

        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('opentechiz_material')->__('Item approved'));
        $this->_redirect('*/*/detail/id/'.$quoteId);
    }

    public function saveAction()
    {

        $quoteProductModel = Mage::getModel('opentechiz_quotation/product');
        $quoteModel = Mage::getModel('opentechiz_quotation/quotations');
        $data = [];
        $data['image'] = '';
        $path = Mage::getBaseDir('media') . '/quotation/';
        if (!file_exists($path)) {
            mkdir($path, 777, true);
        }

        for($i = 0; $i < count($_FILES['image']['name']); $i ++) {
            $data[$i]['image'] = [];
            $data[$i]['image']['name'] = $_FILES['image']['name'][$i];
            $data[$i]['image']['type'] = $_FILES['image']['type'][$i];
            $data[$i]['image']['tmp_name'] = $_FILES['image']['tmp_name'][$i];
            $data[$i]['image']['error'] = $_FILES['image']['error'][$i];
            $data[$i]['image']['size'] = $_FILES['image']['size'][$i];
        }
        for($j = 0; $j < $i ; $j++){
            $_FILES = $data[$j];
            if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']) ){
                try {
                    $uploader = new Varien_File_Uploader('image');

                    if(is_array($_FILES['image']['name'])) {
                        foreach ($_FILES['image']['name'] as $image) {
                            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                            $uploader->setAllowRenameFiles(true);
                            $uploader->setFilesDispersion(false);

                            $uploader->save($path, $image);

                            $data['image'] .= $path.$image.',';
                        }
                    } else{
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(false);
                        $uploader->save($path, $_FILES['image']['name']);
                        $data['image'] .= Mage::getBaseUrl('media').DS.'quotation/'.$_FILES['image']['name'].',';
                    }
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            }
        }
        if(isset($data['image'])) {
            $data['qp_image'] = trim($data['image'], ',');
        }
        $params = $this->getRequest()->getParams();

        $allQuoteProducts = Mage::getModel('opentechiz_quotation/product')->getCollection();
        foreach ($allQuoteProducts as $quotationProduct){
            if($quotationProduct->getQpName() == $params['qp_name'] && $quotationProduct->getQuoteProductId() != $params['productId']){
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_quotation')->__('The product name is not unique'));
                $this->_redirect('*/*/detail/id/'.$params['productId']);
                return;
            }
        }

        $data['qp_name'] = $params['qp_name'];
        $data['qp_price'] = $params['qp_price'];
        $data['note'] = $params['note'];
        $data['quote_id'] = $params['id'];
        $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $description = $params['description'];

        if(array_key_exists('productId', $params)){
            if($data['image'] == '') {
                $data['qp_image'] = $quoteProductModel->load($params['productId'])->getQpImage();
            }
            $quoteProduct = $quoteProductModel->addData($data)->setId($params['productId'])->save();
        } else
            $quoteProduct = $quoteProductModel->setData($data)->save();

        $productId = $quoteProduct->getId();

        if (!$params) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Something gone wrong.'));
        } else {
            try {
                $options = Mage::getSingleton('opentechiz_quotation/quotationSave')->saveOptions($params, $productId);
                $data['options'] = $options;
                $quoteProductModel->load($productId)->addData($data)->setId($productId)->save();
                
                $quote = $quoteModel->load($params['id']);
                if($quote->getId()){
                    $quote->setDescription($description)->save();
                }

                $admin = Mage::getSingleton('admin/session')->getUser();
                if($admin->getId()){
                    Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Save quote product #'.$productId));
                }

                $this->_getSession()->addSuccess(
                    $this->__('Data was successfully saved.')
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/detail/id/'.$params['id']);
    }

    public function massStatusAction()
    {
        $quoteIds = $this->getRequest()->getParam('quote');

        if (!is_array($quoteIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s).'));
        } else {
            try {
                foreach ($quoteIds as $quoteId) {
                    $model = Mage::getSingleton('opentechiz_quotation/quotations')
                        ->load($quoteId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true);
                    $model->save();
                    $admin = Mage::getSingleton('admin/session')->getUser();
                    if($admin->getId()){
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'MassAction Save quote #'.$quoteId.' status ='.$this->getRequest()->getParam('status')));
                    }
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated.', count($quoteIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massDeleteAction()
    {
        $quoteIds = $this->getRequest()->getParam('quote');
        if (!is_array($quoteIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                $quote = Mage::getModel('opentechiz_quotation/quotations');
                foreach ($quoteIds as $quoteId) {
                    $quote->load($quoteId)
                        ->delete();
                    $admin = Mage::getSingleton('admin/session')->getUser();
                    if($admin->getId()){
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'MassAction delete quote #'.$quoteId));
                    }
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted.', count($quoteIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function saveCommentsAction()
    {
        $data = $this->getRequest()->getPost();
        try {
            if (isset($data['message']) || $data['message'] != '') {
                $model = Mage::getModel('opentechiz_quotation/comments');
                $model->setData($data);
                $model->setCreatedAt(Mage::helper('opentechiz_quotation')->_getCurrentTime());
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess('Send sucessfully!');

                Mage::dispatchEvent('quotation_view_saveComment_after', array('quote_data' => $data));
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Send comment failed');
            }
        }
        catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirectReferer();
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/sales/quotations');
    }
}