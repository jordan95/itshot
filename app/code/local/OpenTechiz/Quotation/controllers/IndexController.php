<?php

class OpenTechiz_Quotation_IndexController extends Mage_Core_Controller_Front_Action
{
    public function saveQuotationAction()
    {
        $data = $this->getRequest()->getPost();
        $model = Mage::getModel('opentechiz_quotation/quotations');

        if ($data)
        {
            // get increment_id
            $incrementId = $model->generateIncrementId();

            // check login and get customer_id
            if(Mage::getSingleton('customer/session')->isLoggedIn()) {
                $customerData = Mage::getSingleton('customer/session')->getCustomer();
                $customerId =  $customerData->getId();
                $model->setCustomerId($customerId);

                // call to _getCustomerAddress function
                $customerAddress = $this->_getCustomerAddress($customerId);

                foreach ($customerAddress as $value)
                {
                    $region = $value['region'];
                    $address = $value['street'].', '.$value['city'].', '.$region.', '.$value['country_id'];
                    $postcode = $value['postcode'];

                    // set customer_id and address to model
                    $model->setAddress($address)
                        ->setRegion($region)
                        ->setPostcode($postcode);
                }
            }

            // get product_id and load product
            $productId = (int) $data['product_id'];
            $product = Mage::getModel('catalog/product')->load($productId);

            // get url_key
            $urlKey = $product->getUrlKey();

            // get file upload data
            if(isset($_FILES['images']['name']) and (file_exists($_FILES['images']['tmp_name']))) {
                $_logFile = $_FILES['images']['name'].'.log';
                try {
                    /* Starting upload */
                    $uploader = new Varien_File_Uploader('images');

                    /* Any extention would work */
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                    $uploader->setAllowRenameFiles(false);

                    /* Set the file upload mode
                         false -> get the file directly in the specified folder
                         true -> get the file in the product like folders
                        	(file.jpg will go in something like /media/f/i/file.jpg) */
                    $uploader->setFilesDispersion(false);

                    /* We set media/upload as the upload dir */
                    $path = Mage::getBaseDir('media') . DS ."upload". DS ;
                    $uploader->save($path, str_replace(" ","_",$_FILES['images']['name']));

                    $model->setImage(str_replace(" ","_",$_FILES['images']['name']));
                }catch(Exception $e) {
                    Mage::log($e->getMessage(), null, $_logFile, true);
                }
            }
            // set data to model
            $model->setIncrementId($incrementId)
                ->setProductId($productId)
                ->setUrlKey($urlKey)
                ->setFirstName($data['first_name'])
                ->setLastName($data['last_name'])
                ->setEmail($data['email'])
                ->setPhone($data['phone'])
                ->setDescription($data['description'])
                ->setCreatedAt(Mage::helper('opentechiz_quotation')->_getCurrentTime())
                ->setRequestTime(Mage::helper('opentechiz_quotation')->_getCurrentTime())
                ->setPriceRangeFrom($data['price_range_from'])
                ->setPriceRangeTo($data['price_range_to'])
                ->setSize($data['size'])
                ->setMetal($data['metal'])
                ->setStone($data['stone']);

            /* Session errors */
            $session = Mage::getSingleton('core/session');

            // save data
            try{
                $model->save();
                $session->addSuccess('Submit sucessfully!');
            }
            catch(Exception $e)
            {
                $session->addError($e->getMessage());
                return false;
            }
            $this->_redirectReferer();
        }
    }

    protected function _getCustomerAddress($customerId)
    {
        $customer = Mage::getModel('customer/customer')->load($customerId);
        $customerAddress = array();
        foreach ($customer->getAddresses() as $address)
        {
            $customerAddress[] = $address->toArray();
        }

        return $customerAddress;
    }
}