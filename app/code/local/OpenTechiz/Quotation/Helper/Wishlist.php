<?php

class OpenTechiz_Quotation_Helper_Wishlist extends OpenTechiz_Wishlist_Helper_Data
{
    public function getSaveForLaterUrl($item_id, $quoteProductSku = null)
    {
        if($quoteProductSku){
            return Mage::getUrl('ot_wishlist/index/saveForLater', array(
                'id' => $item_id,
                'quote_product_sku' => $quoteProductSku,
                'form_key' => Mage::getSingleton('core/session')->getFormKey(),
                Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => Mage::helper('core/url')->getEncodedUrl()
            ));
        }else {
            return Mage::getUrl('ot_wishlist/index/saveForLater', array(
                'id' => $item_id,
                'form_key' => Mage::getSingleton('core/session')->getFormKey(),
                Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => Mage::helper('core/url')->getEncodedUrl()
            ));
        }
    }
}