<?php

class OpenTechiz_Quotation_Helper_Data extends Mage_Core_Helper_Abstract
{
    const STT_PENDING = 0;
    const STT_PROCESSING = 3;
    const STT_WAITFORDESIGNER = 4;
    const STT_WAITFORPRODUCTION = 5;
    const STT_READY = 6;
    const STT_ORDERED = 8;
    const STT_APPROVED = 1;
    const STT_DENIED = 2;
    const STT_CLOSE = 7;
    const EMAIL_LOGO = 'https://media.itshot.com/email/logo/default/logo_2.png';
    const QUOTATION_PRODUCT_SKU = 'quotation';

    /**
     * Note: Process request status Quotation:
     * Pending -> Processing -> Waiting for designer -> Waiting for production -> Ready to send -> Dennied -> Approved -> Closed
     */

    public function getAllStatusQuotation()
    {
        $status = array(
            self::STT_PENDING => $this->__('Pending'),
            self::STT_PROCESSING => $this->__('Processing'),
            self::STT_WAITFORDESIGNER => $this->__('Waiting for designer'),
            self::STT_WAITFORPRODUCTION => $this->__('Waiting for production'),
            self::STT_READY => $this->__('Ready to order'),
            self::STT_ORDERED => $this->__('Ordered'),
            self::STT_APPROVED => $this->__('Approved'),
            self::STT_DENIED => $this->__('Denied'),
            self::STT_CLOSE => $this->__('Closed')
        );
        return $status;
    }

    public function getStoneOptions()
    {
        return Mage::getModel('opentechiz_material/stone')->getCollection()->setOrder('stone_type', 'ASC');
    }

    public function getAlloyOptions()
    {
        $alloyData = Mage::getModel('opentechiz_material/gold')->getCollection();
        $data = [];
        foreach ($alloyData as $alloy) {
            $alloyName = strtoupper($alloy->getGoldType()) . ' ' . $alloy->getGoldColor();

            $data[$alloy->getId()] = $alloyName;
        }
        return $data;
    }

    const OPTION_NAME_STONE = [
        0 => 'Center Stone', 1 => 'Stone 2', 2 => 'Stone 3', 3 => 'Stone 4'
    ];

    const OPTION_NAME_ALLOY = [4 => 'Metal'];

    public function _getCurrentTime()
    {
        return Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
    }
}