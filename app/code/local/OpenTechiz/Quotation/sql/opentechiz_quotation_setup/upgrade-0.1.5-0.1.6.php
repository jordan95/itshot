<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('quotation_comments')}
    CHANGE `created_at` `created_at`
    DATETIME
    NOT NULL
    COMMENT 'Created at';
");

$installer->endSetup();