<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('quotation_quotes')} 
    ADD `product_id`
    INT(11)
    NOT NULL
    COMMENT 'Product ID'
    AFTER `customer_id`;
    
    ALTER TABLE {$this->getTable('quotation_quotes')}
    CHANGE `customer_id` `customer_id`
    INT(11)
    NULL
    COMMENT 'Customer ID';
    
    ALTER TABLE {$this->getTable('quotation_quotes')}
    CHANGE `increment_id` `increment_id`
    VARCHAR(25)
    NOT NULL
    COMMENT 'Increment ID';
    
    ALTER TABLE {$this->getTable('quotation_quotes')}
    CHANGE `address` `address`
    TEXT
    NULL
    COMMENT 'Address';
    
    ALTER TABLE {$this->getTable('quotation_quotes')}
    CHANGE `region` `region`
    TEXT
    NULL
    COMMENT 'Region';
    
    ALTER TABLE {$this->getTable('quotation_quotes')}
    CHANGE `postcode` `postcode`
    TEXT
    NULL
    COMMENT 'Postcode';
");

$installer->endSetup();