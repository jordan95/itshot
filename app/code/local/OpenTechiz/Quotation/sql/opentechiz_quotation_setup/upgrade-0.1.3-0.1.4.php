<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('quotation_product_option')}
    ADD `option_type`
    VARCHAR(30)
    NULL
    COMMENT 'Option Type'
    AFTER `option_name`;
");

$installer->endSetup();