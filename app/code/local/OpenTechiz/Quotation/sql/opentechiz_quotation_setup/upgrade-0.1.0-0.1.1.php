<?php

$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('quotation_product_option')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('quotation_product_option'))
        ->addColumn('quote_product_option_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'ID')
        ->addColumn('quote_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'quote product ID')
        ->addColumn('option_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'name')
        ->addColumn('option_value', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'value')
        ->addColumn('qty', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
        ), 'qty')
        ->addColumn('option_sort_order', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'sort order');
    $installer->getConnection()->createTable($table);
}

$installer->endSetup();