<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('quotation_quotes')} 
    ADD `price_range_from`
    INT(11)
    NOT NULL
    COMMENT 'quote price range from';
    
    ALTER TABLE {$this->getTable('quotation_quotes')} 
    ADD `price_range_to`
    INT(11)
    NOT NULL
    COMMENT 'quote price range to';
    
    ALTER TABLE {$this->getTable('quotation_quotes')} 
    ADD `size`
    FLOAT
    NOT NULL
    COMMENT 'size';
    
    ALTER TABLE {$this->getTable('quotation_quotes')} 
    ADD `metal`
    VARCHAR (100)
    NOT NULL
    COMMENT 'preferred metal';
    
    ALTER TABLE {$this->getTable('quotation_quotes')} 
    ADD `stone`
    VARCHAR (100)
    NOT NULL
    COMMENT 'preferred stone';

");

$installer->endSetup();