<?php
$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('quotation_comments')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('quotation_comments'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'ID')

        ->addColumn('quotation_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
            'nullable' => false,
        ), 'Quotation ID')

        ->addColumn('user', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'User')

        ->addColumn('message', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Message')

        ->addColumn('when', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
            'nullable' => false,
        ), 'When');
    $installer->getConnection()->createTable($table);
}

$installer->endSetup();