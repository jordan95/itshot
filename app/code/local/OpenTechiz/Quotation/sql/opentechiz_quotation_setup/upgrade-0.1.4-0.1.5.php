<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('quotation_quotes')}
    CHANGE `description` `description`
    VARCHAR(255)
    CHARACTER SET utf8 COLLATE utf8_general_ci
    NULL
    COMMENT 'Description';
");

$installer->endSetup();