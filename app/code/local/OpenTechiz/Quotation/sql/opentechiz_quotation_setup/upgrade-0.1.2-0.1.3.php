<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    
    ALTER TABLE {$this->getTable('quotation_comments')}
    CHANGE `when` `created_at`
    TIMESTAMP
    NOT NULL
    DEFAULT
    CURRENT_TIMESTAMP
    COMMENT 'Created at';
");

$installer->endSetup();