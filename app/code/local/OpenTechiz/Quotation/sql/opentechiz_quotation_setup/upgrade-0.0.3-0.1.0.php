<?php

$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('quotation_product')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('quotation_product'))
        ->addColumn('quote_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'ID')
        ->addColumn('quote_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'Increment ID')
        ->addColumn('qp_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'Customer ID')
        ->addColumn('qp_price', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
            'nullable' => false,
        ), 'Url Key')
        ->addColumn('qp_image', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
        ), 'First Name')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'Created at')
        ->addColumn('options', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => true,
        ), 'Request time')
        ->addColumn('note', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => true,
        ), 'Request time');
    $installer->getConnection()->createTable($table);
}

$installer->endSetup();