<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('quotation_quotes')}
    ADD `image`
    TEXT
    NULL
    COMMENT 'Image Path'
    AFTER `description`;
");

$installer->endSetup();