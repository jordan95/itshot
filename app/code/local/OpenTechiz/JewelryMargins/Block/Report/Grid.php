<?php

class OpenTechiz_JewelryMargins_Block_Report_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * @var OpenTechiz_JewelryMargins_Helper_Data
     */
    private $helper;

    public function __construct()
    {
        parent::__construct();
        $this->helper = $this->helper('opentechiz_jewerlymargins');
        $this->setId('jewelryMarginsBlockGrid');
        $this->setDefaultSort('sku');
        $this->setDefaultDir('ASC');
        $additionalJavaScript = <<<JS
varienGridMassaction.prototype.setUseAjax(true);
varienGridMassaction.prototype.apply = function() {
    if(varienStringArray.count(this.checkedString) == 0) {
            alert(this.errorText);
            return;
        }

    var item = this.getSelectedItem();
    if(!item) {
        this.validator.validate();
        return;
    }
    this.currentItem = item;
    var fieldName = (item.field ? item.field : this.formFieldName);
    var fieldsHtml = '';

    if(this.currentItem.confirm && !window.confirm(this.currentItem.confirm)) {
        return;
    }

    this.formHiddens.update('');
    
    var ids = this.checkedString.split(',');
    var prices = [];
    ids.forEach(function (value, index) {
        var input = $$('input[value="' + value + '"].massaction-checkbox');
        if (input.length) {
            prices[index] = value + '|' + input[0].up().up().down('.editable').down('input').value;
        }
    });
    
    new Insertion.Bottom(this.formHiddens, this.fieldTemplate.evaluate({name: fieldName, value: this.checkedString}));
    new Insertion.Bottom(this.formHiddens, this.fieldTemplate.evaluate({name: 'massaction_prepare_key', value: fieldName}));
    new Insertion.Bottom(this.formHiddens, this.fieldTemplate.evaluate({name: 'prices', value: prices.join(',')}));

    if(!this.validator.validate()) {
        return;
    }

    if(this.useAjax && item.url) {
        new Ajax.Request(item.url, {
            'method': 'post',
            'parameters': this.form.serialize(true),
            'onComplete': this.onMassactionComplete.bind(this)
        });
    } else if(item.url) {
        this.form.action = item.url;
        this.form.submit();
    }
};

function refreshGrid(grid, gridMassAction, transport) {
    gridMassAction.unselectAll();
    var content = '<table><thead><tr><th>SKU</th><th>New Price</th></tr></thead><tbody>';
    transport.responseJSON.forEach(function(row) {
        var input = $$('input[value="' + row.productId + '"].massaction-checkbox');
        if (input.length) {
            input[0].up().up().down('.current-price').innerText = row.formattedPrice;
        }
        content += '<tr><td style="padding-right: 15px;">' + row.sku + '</td><td>' + row.price + '</td></tr>';
    });
    content += '</tbody></table>';
    Dialog.info(content, {
        draggable: true,
        resizable: true,
        closable: true,
        className: 'magento',
        windowClassName: 'popup-window',
        title: 'Updated Products',
        width: 500,
        zIndex: 1000,
        recenterAuto: false,
        hideEffect: Element.hide,
        showEffect: Element.show,
        id: 'updated_products_popup'
    });
}
JS;
        $this->setAdditionalJavaScript($additionalJavaScript);
    }

    protected function _prepareCollection()
    {
        /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getModel('catalog/product')->getCollection();
        $multiplier = $this->helper->getPriceMultiplier();
        $newPriceExpr = new Zend_Db_Expr('ROUND(`oii`.`last_cost` * ' . $multiplier . ', 2)');
        $collection->addAttributeToSelect('name')
            ->addAttributeToSelect('thumbnail')
            ->addAttributeToSelect('special_price')
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('special_price', ['lt' => $newPriceExpr]);

        $collection->joinTable(
            ['oii' => $collection->getResource()->getTable('opentechiz_inventory/instock')],
            'product_id=entity_id',
            [
                'last_cost' => 'last_cost',
                'last_cost_date' => 'last_cost_date',
                'instock_sku' => 'sku',
                'product_id' => 'product_id',
                'new_price' => $newPriceExpr,
            ],
            ['last_cost' => ['gt' => 0]]
        );

        $collection->joinTable(
            ['soi' => $collection->getResource()->getTable('sales/order_item')],
            'sku=instock_sku',
            ['order_id' => 'order_id'],
            []
        );

        $collection->joinTable(
            ['so' => $collection->getResource()->getTable('sales/order')],
            'entity_id=order_id',
            ['order_created_at' => 'MAX(so.created_at)'],
            []
        );

        $collection->groupByAttribute('product_id');
        $collection->groupByAttribute('entity_id');

        $this->addCategoriesFilter($collection);

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', [
            'header'=> Mage::helper('catalog')->__('ID'),
            'width' => '50px',
            'type'  => 'number',
            'index' => 'entity_id',
        ]);

        $this->addColumn('thumbnail', [
            'header'    => Mage::helper('catalog')->__('Image'),
            'sortable'  => false,
            'filter'    => false,
            'width'     => '110px',
            'renderer'  => 'OpenTechiz_JewelryMargins_Block_Report_Renderer_Image'
        ]);

        $this->addColumn('sku', [
            'header'    => Mage::helper('catalog')->__('Sku'),
            'align'     => 'left',
            'index'     => 'sku',
        ]);

        $this->addColumn('name', [
            'header'    => Mage::helper('catalog')->__('Name'),
            'align'     => 'left',
            'index'     => 'name',
        ]);

        $store = Mage::app()->getStore(0);
        $this->addColumn('last_cost', [
            'header'=> Mage::helper('opentechiz_jewerlymargins')->__('Last Cost'),
            'type'  => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
            'index' => 'last_cost',
        ]);

        $this->addColumn('last_cost_date', [
            'header'=> Mage::helper('opentechiz_jewerlymargins')->__('Last Cost Date'),
            'type'  => 'date',
            'index' => 'last_cost_date',
        ]);

        $this->addColumn('special_price', [
            'header'=> Mage::helper('opentechiz_jewerlymargins')->__('Current Selling Price'),
            'type'  => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
            'column_css_class' => 'current-price',
            'index' => 'special_price',
        ]);

        $this->addColumn('new_price', [
            'header'=> Mage::helper('opentechiz_jewerlymargins')->__('New Selling Price'),
            'type'  => 'input',
            'currency_code' => $store->getBaseCurrency()->getCode(),
            'index' => 'new_price',
            'editable' => true,
        ]);

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('product');
        $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->setUseAjax(true);
        $this->getMassactionBlock()->addItem('save', array(
            'label'=> Mage::helper('catalog')->__('Save New Prices'),
            'url' => $this->getUrl('*/*/massSave'),
            'complete' => 'refreshGrid',
            'selected' => true
        ));

        return $this;
    }

    /**
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    private function addCategoriesFilter($collection)
    {
        $resource = $collection->getResource();
        $read = $resource->getReadConnection();
        $select = $read->select();
        $where = [];
        $excludeCategoryPaths = $this->helper->getExcludedCategoryPaths();
        foreach ($excludeCategoryPaths as $categoryPath) {
            $where[] = "(cce.path = '" . $categoryPath . "' OR cce.path LIKE '" . $categoryPath ."/%')";
        }
        if ($where) {
            $select->from(['ccp' => $resource->getTable('catalog/category_product')], 'product_id')
                ->joinLeft(
                    ['cce' => $collection->getResource()->getTable('catalog/category')],
                    'cce.entity_id = ccp.category_id',
                    []
                )
                ->where(implode(' OR ', $where))
                ->distinct();
            $productIds = $read->fetchCol($select);
            if ($productIds) {
                $collection->addFieldToFilter('entity_id', ['nin' => $productIds]);
            }
        }

        return $collection;
    }
}
