<?php

class OpenTechiz_JewelryMargins_Block_Report_Index extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'report';
        $this->_blockGroup = 'opentechiz_jewerlymargins';
        $this->_headerText = Mage::helper('opentechiz_jewerlymargins')->__('Jewelry Margins Report');
        parent::__construct();
    }

    protected function _addButton($id, $data, $level = 0, $sortOrder = 0, $area = 'header')
    {
        return $this;
    }
}
