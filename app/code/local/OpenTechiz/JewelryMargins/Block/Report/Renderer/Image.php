<?php

class OpenTechiz_JewelryMargins_Block_Report_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $image = $row->getData('thumbnail');
        if (!$image) {
            return '';
        }
        $url = Mage::helper('catalog/image')->init(Mage::getModel('catalog/product'), 'thumbnail', $image)
            ->resize(120, 120)->__toString();
        return '<img src="' . $url . '" width="100px">';
    }
}
