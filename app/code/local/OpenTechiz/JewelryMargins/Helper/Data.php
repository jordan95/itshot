<?php

class OpenTechiz_JewelryMargins_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @return array
     */
    public function getExcludedCategoryPaths()
    {
        $excludeCategoryIds = explode(',', Mage::getStoreConfig('opentechiz_jewerlymargins/general/exclude_categories'));
        if (!$excludeCategoryIds) {
            return [];
        }
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $select = $read->select();
        $select->from($resource->getTableName('catalog/category'), 'path')
            ->where('entity_id IN (?)', $excludeCategoryIds);
        return $read->fetchCol($select);
    }

    /**
     * @return float
     */
    public function getPriceMultiplier()
    {
        $multiplier = (float) Mage::getStoreConfig('opentechiz_jewerlymargins/general/multiplier');
        return $multiplier ? $multiplier : 1;
    }
}
