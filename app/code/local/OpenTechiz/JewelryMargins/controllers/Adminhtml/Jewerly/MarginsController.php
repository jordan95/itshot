<?php

class OpenTechiz_JewelryMargins_Adminhtml_Jewerly_MarginsController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('catalog/jewerly_margins')
            ->_addBreadcrumb(Mage::helper('catalog')->__('Catalog'), Mage::helper('opentechiz_jewerlymargins')->__('Jewelry Margins Report'));

        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__('Jewelry Margins Report'));

        $this->_initAction();
        $this->renderLayout();
    }

    public function massSaveAction()
    {
        $productIds = $this->getRequest()->getParam('product');
        $isAjax = $this->getRequest()->isAjax();
        $updatedProducts = [];
        if (!is_array($productIds)) {
            if (!$isAjax) {
                $this->_getSession()->addError($this->__('Please select product(s).'));
            }
        } else {
            if (!empty($productIds)) {
                try {
                    $prices = explode(',', $this->getRequest()->getParam('prices'));
                    $parsedPrices = [];
                    foreach ($prices as $price) {
                        $parsedPrice = explode('|', $price);
                        if (isset($parsedPrice[1])) {
                            $parsedPrices[$parsedPrice[0]] = $parsedPrice[1];
                        }
                    }
                    foreach ($productIds as $productId) {
                        $product = Mage::getSingleton('catalog/product')->load($productId);
                        if (isset($parsedPrices[$productId])) {
                            $product->setSpecialPrice($parsedPrices[$productId]);
                            $product->save();
                            $updatedProducts[$productId] = [
                                'sku' => $product->getSku(),
                                'price' => $product->getSpecialPrice(),
                            ];
                        }
                    }
                    if (!$isAjax) {
                        $this->_getSession()->addSuccess(
                            $this->__('Total of %d record(s) have been updated.', count($productIds))
                        );
                    }
                } catch (Exception $e) {
                    if (!$isAjax) {
                        $this->_getSession()->addError($e->getMessage());
                    }
                }
            }
        }
        if ($this->getRequest()->isAjax()) {
            $columnBlock = $this->getLayout()->createBlock('adminhtml/widget_grid_column');
            $columnBlock->setIndex('special_price');
            $store = Mage::app()->getStore(0);
            $columnBlock->setCurrencyCode($store->getBaseCurrency()->getCode());
            $priceBlock = $this->getLayout()->createBlock('adminhtml/widget_grid_column_renderer_price');
            $priceBlock->setColumn($columnBlock);
            $result = [];
            $row = new Varien_Object();
            foreach ($updatedProducts as $productId => $data) {
                $row->setData('special_price', $data['price']);
                $result[] = [
                    'productId' => $productId,
                    'sku' => $data['sku'],
                    'price' => $data['price'],
                    'formattedPrice' => $priceBlock->render($row),
                ];
            }
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } else {
            $this->_redirect('*/*/index');
        }
    }
}
