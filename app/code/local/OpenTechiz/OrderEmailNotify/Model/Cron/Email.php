<?php
class OpenTechiz_OrderEmailNotify_Model_Cron_Email
{
    public function orderDailyByUser(){
      
        if (!$to = Mage::getStoreConfig('order_email_notification/order_was_process_by/recipient_email')) {
            return;
        }
        $to = explode(',', $to);
        $today = Mage::helper('opentechiz_orderemailnotify')->getDateToday();

        $collection = $this->getCollectionData($today);
        $orders = $collection->getItems();
        if (!$orders) {
            return;
        }
      
        $this->sendEmail($orders,$to,"Daily");
        
    }
    public function orderWeeklyByUser(){
      
        if (!$to = Mage::getStoreConfig('order_email_notification/order_was_process_by/recipient_email')) {
            return;
        }
        $to = explode(',', $to);
        $weekly_date = Mage::helper('opentechiz_orderemailnotify')->getWeeklyDate();

        $collection = $this->getCollectionData($weekly_date);
        $orders = $collection->getItems();
        if (!$orders) {
            return;
        }
        $this->sendEmail($orders,$to,"Weekly");
    }
    public function orderMonthlyByUser(){

        if (!$to = Mage::getStoreConfig('order_email_notification/order_was_process_by/recipient_email')) {
            return;
        }
        $to = explode(',', $to);
        $monthly_date = Mage::helper('opentechiz_orderemailnotify')->getMonthlyDate();

        $collection = $this->getCollectionData($monthly_date);
        $orders = $collection->getItems();
        if (!$orders) {
            return;
        }
        $this->sendEmail($orders,$to,"Monthly");

    }
    public function getCollectionData($from){

        $current_date = Mage::helper('opentechiz_orderemailnotify')->getCurrentDate();
        $collection = Mage::getModel('sales/order')->getCollection()
            ->join(array('log' => 'opentechiz_reportExtend/log_orderStatus'), 'log.id = main_table.last_history_id')
            ->addFieldToFilter('log.updated_at',array("gteq"=>$from))
            ->addFieldToFilter('main_table.order_stage',array("eq"=>OpenTechiz_SalesExtend_Helper_Data::SHIPPED))
            ->addFieldToFilter('main_table.state',array("nin"=>array("canceled","holded")));
        $collection->getSelect()->reset(Zend_Db_Select::COLUMNS);
        $collection->getSelect()
                ->columns(array('COUNT(entity_id) AS sum_order','processed_by_user','GROUP_CONCAT(entity_id) as list_order'))
                ->group('processed_by_user');
        $collection->getSelect()->order('sum_order DESC');
        return $collection;
    }
    public function sendEmail($orders,$to,$phase){
        try{
            $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
            $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
            $emailTemplate = Mage::getModel('core/email_template');
            $emailTemplate->loadDefault('ordershippedbynotify');
            $emailTemplate->setTemplateSubject('Total Order was Processed By '.$phase.'('.Mage::helper('core')->formatDate().')');
            $emailTemplate->setSenderName($senderName);
            $emailTemplate->setSenderEmail($senderEmail);
            $emailTemplateVariables = ['orders' => $orders,'phase' => $phase];

            Mage::helper('opentechiz_orderemailnotify')->sendEmail($to,$emailTemplate,$emailTemplateVariables);
        }
        catch (Exception $e) {
            Mage::log($e->getMessage(),null,'error-email-notification.log',true);
        }
    }

}