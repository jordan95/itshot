<?php
class OpenTechiz_OrderEmailNotify_Model_Cron
{
	const SHIPPING_FEDEX_2_DAY_AIR = "matrixrate_matrixrate_11";
	const SHIPPING_FEDEX_NEXT_DAY_AIR = "matrixrate_matrixrate_10";
    public function possiblyLateOrders(){
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        if (!$to = Mage::getStoreConfig('order_email_notification/receive_email_list/possibly_late_order')) {
            return;
        }
        $to = explode(',', $to);
        $days = (int) Mage::getStoreConfig('order_email_notification/receive_email_list/day');

        $list_stage = [OpenTechiz_SalesExtend_Helper_Data::ORDER_VERIFICATION,OpenTechiz_SalesExtend_Helper_Data::ORDER_PROCESSING,OpenTechiz_SalesExtend_Helper_Data::BILLING,OpenTechiz_SalesExtend_Helper_Data::PREPSHIPMENT];
        //$collection = $this->getListOrder(-$days);
        try {
            foreach($list_stage as $stage){
                $re_collection = '';
                $order_stage = '';
                $re_collection = $this->getListOrder(-$days);
                $re_collection->getSelect()->where('state not in (?)', (array)OpenTechiz_SalesExtend_Helper_Data::DEACTIVE_ORDER_STATE);
                $re_collection->getSelect()->where('order_stage = ?', $stage);
                $re_collection->getSelect()->where('internal_status != ?', OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS_WAITING_ON_CUSTOMER);
                if($stage == OpenTechiz_SalesExtend_Helper_Data::ORDER_PROCESSING){
                    $re_collection->getSelect()->where('internal_status != ?', OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS_FIRST_PAYMENT_COMPLETE);
                }
                if($stage == OpenTechiz_SalesExtend_Helper_Data::BILLING){
                    $re_collection->getSelect()->where('internal_status != ?', OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS_WAITING_ON_FINAL_PAYMENT);
                }
                $re_collection->getSelect()->order('main_table.created_at ASC');
                $orders = $re_collection->getItems();
                if (!$orders) {
                    continue;
                }
                if(isset(OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE[$stage])){
                    $order_stage = OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE[$stage];
                }
                $emailTemplate = Mage::getModel('core/email_template');
                $emailTemplate->loadDefault('possiblylateordernotify');
                $emailTemplate->setTemplateSubject('Order in '.$order_stage.', Possibly Late Orders - Review Needed');

                $emailTemplate->setSenderName($senderName);
                $emailTemplate->setSenderEmail($senderEmail);

                $emailTemplateVariables = ['orders' => $orders,'stage' => $order_stage];

                $this->sendEmail($to,$emailTemplate,$emailTemplateVariables);
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(),null,'orderoverdays.log',true);
        }


        
        
        
    }
    public function possiblyLateOrdersMoreDays(){
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
        $stage ="Fulfillment";
        if (!$to = Mage::getStoreConfig('order_email_notification/receive_email_list/possibly_late_order_more_days')) {
            return;
        }
        $to = explode(',', $to);
        $days = (int) Mage::getStoreConfig('order_email_notification/receive_email_list/more_days');
        try {
            $collection = $this->getListOrder(-$days);
            $collection->getSelect()->where('state not in (?)', (array)OpenTechiz_SalesExtend_Helper_Data::DEACTIVE_ORDER_STATE);
            $collection->getSelect()->where('order_stage = ?', OpenTechiz_SalesExtend_Helper_Data::FULFILLMENT);
            $collection->getSelect()->where('internal_status != ?', OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS_MANUFACTURING);
            $collection->getSelect()->where('internal_status != ?', OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS_WAITING_ON_CUSTOMER);
            $collection->getSelect()->where('internal_status != ?', OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS_WAITING_ON_VENDOR);
            $collection->getSelect()->order('main_table.created_at ASC');
            
            $orders = $collection->getItems();
            
            if (!$orders) {
                return;
            }
            $emailTemplate = Mage::getModel('core/email_template');
            $emailTemplate->loadDefault('possiblylateordernotify');
            $emailTemplate->setTemplateSubject('Order in Fulfillment, Possibly Late Orders - Review Needed');

            $emailTemplate->setSenderName($senderName);
            $emailTemplate->setSenderEmail($senderEmail);

            $emailTemplateVariables = ['orders' => $orders,'stage' => $stage];

            $this->sendEmail($to,$emailTemplate,$emailTemplateVariables);
        }catch (Exception $e) {
            Mage::log($e->getMessage(),null,'orderoverdays.log',true);
        }
    }
    public function lateOrders(){
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        if (!$to = Mage::getStoreConfig('order_email_notification/receive_email_list/late_order')) {
            return;
        }
        $to = explode(',', $to);
        $days = (int) Mage::getStoreConfig('order_email_notification/receive_email_list/late_day');
        try {
            /** @var Mage_Sales_Model_Resource_Order_Collection $collection */
            $collection = Mage::getModel('sales/order')->getCollection();
            $dateModel = Mage::getModel('core/date');
            $fromDate = $dateModel->gmtTimestamp() - 60 * 60 * 24 * $days;
            $collection->addFieldToFilter('shipping_method',array('in'=>[self::SHIPPING_FEDEX_2_DAY_AIR,self::SHIPPING_FEDEX_NEXT_DAY_AIR]) )->addFieldToFilter('state',array('nin'=>OpenTechiz_SalesExtend_Helper_Data::DEACTIVE_ORDER_STATE) )
                ->addFieldToFilter('created_at', ['lteq' => $dateModel->gmtDate(null, $fromDate)])
                ->addFieldToFilter('order_stage',array('neq'=>OpenTechiz_SalesExtend_Helper_Data::SHIPPED) )
                ->addFieldToFilter('internal_status',array('nin'=>[OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS_FIRST_PAYMENT_COMPLETE,OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS_WAITING_ON_FINAL_PAYMENT,OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS_WAITING_ON_CUSTOMER]) ); 
            $collection->getSelect()->order('main_table.created_at ASC');
            $orders = $collection->getItems();
            if (!$orders) {
                return;
            }
            $emailTemplate = Mage::getModel('core/email_template');
            $emailTemplate->loadDefault('lateordernotify');
            $emailTemplate->setTemplateSubject('Urgent Orders - Next Day Air Late Orders');

            $emailTemplate->setSenderName($senderName);
            $emailTemplate->setSenderEmail($senderEmail);

            $emailTemplateVariables = ['orders' => $orders];

            $this->sendEmail($to,$emailTemplate,$emailTemplateVariables);
        }catch (Exception $e) {
            Mage::log($e->getMessage(),null,'orderoverdays.log',true);
        }
        
    }
    protected function sendEmail($to,$emailTemplate,$emailTemplateVariables){
        try {
            foreach ($to as $recipientEmail) {
                if ($recipientEmail) {
                    $emailTemplate->send(trim($recipientEmail), null, $emailTemplateVariables);
                }
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(),null,'orderoverdays.log',true);
        }
    }
    protected function getListOrder($day){
        $dates = Mage::helper("opentechiz_orderemailnotify")->getDate(-10000,$day);
        $collection = Mage::getModel('sales/order')->getCollection()
            ->join(array('log' => 'opentechiz_reportExtend/log_orderStatus'), 'log.id = main_table.last_history_id', 'log.updated_at as changed_at')
            ->addFieldToFilter('log.updated_at', array(
                'from' => $dates['from'],
                'to' => $dates['to']
        ));
        $collection->getSelect()->reset(Zend_Db_Select::COLUMNS)->columns(array('entity_id','increment_id','created_at','shipping_method','log.updated_at as changed_at','shipping_description','order_stage','internal_status','order_type','processed_by_user'));
        return $collection;
    }
}