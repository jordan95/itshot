<?php
class OpenTechiz_OrderEmailNotify_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getDifferenceDays($created_at=''){
		$created_at_time = strtotime($created_at);
        $now_time = time();
        $day = (int)(($now_time - $created_at_time)/86400);
        return $day;
	}
	public function getDate($from, $to){
        $currentDate = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $time =  strtotime($currentDate);

        $from = (int)$from*24; //to hour
        $to = (int)$to*24; //to hour

        $toDate = date('Y-m-d H:i:s', strtotime($to." hour", $time));
        $fromDate = date('Y-m-d H:i:s', strtotime($from." hour", $time));

        $result= [];
        $result['from'] = $fromDate;
        $result['to'] = $toDate;

        return $result;
    }
    public function getDateToday(){
        
        $today = Mage::getModel('core/date')->date('Y-m-d');
        $today =  $today ." 04:00:00";
        
        return $today;
    }
    public function getWeeklyDate(){
        
        $weekly_date = Mage::getModel('core/date')->date('Y-m-d', strtotime(" -7 days"));
        $weekly_date =  $weekly_date ." 04:00:00";
        return $weekly_date;
    }
    public function getMonthlyDate(){
        
        $current_year = Mage::getModel('core/date')->date('Y');
        $current_month = Mage::getModel('core/date')->date('m');
        $day_of_month = cal_days_in_month(CAL_GREGORIAN,$current_month,$current_year);
        $monthly_date = Mage::getModel('core/date')->date('Y-m-d', strtotime(" -".$day_of_month ." days"))." 04:00:00";
        return $monthly_date;
    }
    public function getCurrentDate(){
        
        $currentdate = date('Y-m-d H:i:s');
        
        return $currentdate;
    }
    public function sendEmail($to,$emailTemplate,$emailTemplateVariables){
        foreach ($to as $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send(trim($recipientEmail), null, $emailTemplateVariables);
            }
        }
    }
    public function getLinkOrder($order){
        $list_order = $order->getListOrder();
        $list_order = explode(",",$list_order);
        $link ='';
        foreach($list_order as $order_id){

            $order_model = Mage::getModel("sales/order")->load($order_id);
            $url = Mage::getModel("core/url")->getUrl("adminhtml/sales_order/view", array("order_id" => $order_model->getId()));
            $link .= "<a href=\"{$url}\" target=\"_blank\">#{$order_model->getIncrementId()} </a> ".", ";
        }

        return  substr($link, 0, -2);
    }
}
