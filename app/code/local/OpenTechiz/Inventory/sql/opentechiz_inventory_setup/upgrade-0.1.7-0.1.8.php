<?php
$installer = $this;
$installer->startSetup();

try {
    $installer->run("
            ALTER TABLE {$this->getTable('opentechiz_inventory/instock')} 
            ADD `flat_need_to_order`
            SMALLINT 
            NULL
            DEFAULT 0;
        ");
    $installer->run("
            ALTER TABLE {$this->getTable('opentechiz_inventory/instock')} 
            ADD `flat_qty_in_open_po`
            INT(11)
            NULL
            DEFAULT 0;
        ");
    $installer->run("
            ALTER TABLE {$this->getTable('opentechiz_inventory/instock')} 
            ADD `flat_qty_in_manufacture`
            INT(11) 
            NULL
            DEFAULT 0;
        ");
}catch (Exception $e){

}

$installer->endSetup();