<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('opentechiz_inventory/inventory_instockRequest')} 
    ADD `return_id`
     INT(11)
    NOT NULL
    DEFAULT '0';
");

$installer->endSetup();