<?php

$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('opentechiz_inventory/inventory_instockRequest'))
    ->addColumn('request_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Id')
    ->addColumn('order_item_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'order item Id')
    ->addColumn('instock_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    ), 'instock product id')
    ->addColumn('qty',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false
    ), 'Qty')
    ->addColumn('status',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        'default'   => 0
    ), 'Status');
$installer->getConnection()->createTable($table);

$installer->endSetup();