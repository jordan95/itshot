<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_inventory/inventory_instockRequest')} 
    CHANGE `return_id` `return_id`
     INT(11)
    NULL
    COMMENT 'return id';
");
$installer->endSetup();
