<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE tsht_product_history_log
    ADD `product_name`
    TEXT
    NULL
");

$installer->endSetup();
