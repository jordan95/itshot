<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('inventory_item')} 
    ADD `cost`
    FLOAT
    NOT NULL;
");

$installer->endSetup();