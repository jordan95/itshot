<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('opentechiz_inventory/certificate');
$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
if ($connection->isTableExists($tableName)) {
    $connection->dropTable($tableName);
}

if(!$installer->tableExists('opentechiz_inventory/certificate')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('opentechiz_inventory/certificate'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'Id')
        ->addColumn('cert_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Cert Id')
        ->addColumn('barcode', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'barcode');
    $installer->getConnection()->createTable($table);
}


$installer->run("
    DROP TABLE IF EXISTS tsht_stock_update_log;
    CREATE TABLE tsht_stock_update_log (
      `id`   INT(11)  NOT NULL auto_increment,
      `updated_product_id`   INT(11)        NULL,
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$installer->endSetup();