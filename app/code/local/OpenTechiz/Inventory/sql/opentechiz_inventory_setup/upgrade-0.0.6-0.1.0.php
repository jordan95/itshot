<?php

$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('opentechiz_inventory/certificate')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('opentechiz_inventory/certificate'))
        ->addColumn('cert_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'Cert Id')
        ->addColumn('barcode', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'barcode');
    $installer->getConnection()->createTable($table);
}


$installer->run("
    ALTER TABLE {$this->getTable('opentechiz_inventory/inventory_instockRequest')} 
    ADD `barcodes`
    varchar(255)
    NOT NULL
    DEFAULT '';
");

$installer->endSetup();