<?php

$installer = $this;
$installer->startSetup();

try {
    $installer->run("
        ALTER TABLE {$this->getTable('opentechiz_inventory/instock')} 
        ADD `last_po_id`
        INT(11) 
        NULL
        DEFAULT 0;
    ");

}catch (Exception $e){

}

$installer->endSetup();