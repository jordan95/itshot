<?php

$installer = $this;
$installer->startSetup();

try {
    $installer->run("
        ALTER TABLE {$this->getTable('opentechiz_inventory/instock')} 
        ADD `last_cost`
        double 
        NOT NULL
        DEFAULT 0;
    ");

    $installer->run("
        ALTER TABLE {$this->getTable('opentechiz_inventory/instock')} 
        ADD `last_cost_date`
        varchar(255)
        NOT NULL
        DEFAULT '';
    ");
}catch (Exception $e){

}

$installer->endSetup();