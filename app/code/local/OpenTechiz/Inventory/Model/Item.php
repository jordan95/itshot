<?php

class OpenTechiz_Inventory_Model_Item extends Mage_Core_Model_Abstract
{
    const goldQuality = ['10k', '14k'];
    const STATE_IN_STOCK = 35;
    const STATE_RESERVED = 25;
    const STATE_STOCK_WAITING = 30;
    protected function _construct()
    {
        $this->_init('opentechiz_inventory/item');
    }

    protected function _beforeSave(){
        parent::_beforeSave();
        if(Mage::registry('inventory_item_before_save')){
            Mage::unregister('inventory_item_before_save');
        }

        if(!isset($this->getData()['item_id'])){
            $itemData = $this->getData();
        }else{
            $item = Mage::getModel('opentechiz_inventory/item')->load($this->getData()['item_id']);
            $itemData = $item->getData();
        }
        Mage::register('inventory_item_before_save', $itemData);
    }

    protected function _afterSave(){
        parent::_afterSave();
        $itemBeforeSave = Mage::registry('inventory_item_before_save');
        $itemAfterSave = $this;
        if(!isset($itemBeforeSave['item_id'])
            && $itemAfterSave->getState() == self::STATE_IN_STOCK){ //item is new
            $qty = 1;
            $onHold = 0;
        }else{
            if($itemBeforeSave['state'] == self::STATE_IN_STOCK
                && $itemAfterSave->getState() == self::STATE_RESERVED){  //reserve item
                $qty = 0;
                $onHold = 1;
            }elseif($itemBeforeSave['state'] == self::STATE_RESERVED
                && $itemAfterSave->getState() == self::STATE_IN_STOCK){  //un-reserve item
                $qty = 0;
                $onHold = -1;
            }elseif($itemBeforeSave['state'] == self::STATE_RESERVED
                && $itemAfterSave->getState() != self::STATE_IN_STOCK
                && $itemAfterSave->getState() != self::STATE_RESERVED){  //reserved to other state
                $qty = -1;
                $onHold = -1;
            }elseif($itemBeforeSave['state'] == self::STATE_IN_STOCK
                && $itemAfterSave->getState() != self::STATE_IN_STOCK){  //remove from stock
                $qty = -1;
                $onHold = 0;
            }elseif($itemBeforeSave['state'] != self::STATE_IN_STOCK
                && $itemAfterSave->getState() == self::STATE_IN_STOCK){  //go back to stock
                $qty = 1;
                $onHold = 0;
            }
        }

        //remove item links when item go back to stock
        if($itemBeforeSave['state'] != self::STATE_IN_STOCK
            && $itemAfterSave->getState() == self::STATE_IN_STOCK){
            $itemId = $itemAfterSave->getId();
            $linkCollection = Mage::getModel('opentechiz_inventory/order')
                ->getCollection()
                ->addFieldToFilter('item_id', $itemId);
            foreach ($linkCollection as $link){
                //add qty returned
                if($itemBeforeSave['state'] == self::STATE_STOCK_WAITING) {
                    $orderItemId = $link->getOrderItemId();
                    $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);
                    $data = [];
                    $data['qty_returned'] = $orderItem->getQtyReturned() + 1;
                    $orderItem->addData($data)->save();
                }

                $link->delete();
            }
        }

        if(isset($onHold) && isset($qty)){
            $instock = Mage::getModel('opentechiz_inventory/instock')->load($itemAfterSave->getSku(), 'sku');
            if($instock && $instock->getId()){
                $instock->setQty($instock->getQty() + $qty)
                    ->setOnHold($instock->getOnHold() + $onHold)
                    ->save();
            }else{
                $data = [];
                $data['sku'] = $itemAfterSave->getSku();
                //get product id
                $productSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $data['sku'])[0];
                $productId = Mage::getModel('catalog/product')->loadByAttribute('sku', $productSku)->getId();
                $data['product_id'] = $productId;
                $data['qty'] = $qty;
                //get group sku

                $groupSku = Mage::helper('opentechiz_inventory')->getGroupSKU($data['sku']);
                $data['group_sku'] = $groupSku;

                Mage::getModel('opentechiz_inventory/instock')->setData($data)->save();
            }

            if($qty < 0 && $instock->getQty() <= $instock->getReorderPoint()){
                $fullSku = strtolower($instock->getSku());
                $productInstock = Mage::getModel('opentechiz_inventory/instock')->load($fullSku, 'sku');
                $groupSku = $productInstock->getGroupSku();
                $flagBecomeZero = true;

                if(isset($skuWithColor)) {
                    //check if all qty is = 0
                    $Collection = Mage::getModel('opentechiz_inventory/instock')->getCollection()
                        ->addFieldToFilter('group_sku', array('like' => $groupSku));
                    foreach ($Collection as $variant) {
                        if ($variant->getQty() > $variant->getReorderPoint()) {
                            $flagBecomeZero = false;
                        }
                    }
                }
                //dispatch event
                if ($flagBecomeZero) {
                    Mage::dispatchEvent('stock_become_zero', array(
                        'stock_id' => $instock->getId(),
                    ));
                }
            }
        }
    }

    public function generateBarcode($id){
        $date = Mage::getModel('core/date')->gmtDate('Y-m-d');
        $date = str_replace('-', '', $date);
        $date = substr($date, 2);
        while(strlen($id)<7) $id = '0'.$id;
        if(strlen($id) >= 7) $id = substr($id, -7);
        return $date.$id;
    }

    public function createItemWithProductionRequest($requestData, $productId, $sku){
        $product = Mage::getModel('catalog/product')->load($productId);
        $itemData = [];
        $itemData['sku'] = $sku;
        $itemData['name'] = $product->getName();
        $itemData['image'] = $requestData['image'];
        $itemData['type'] = 0;
        $itemData['options'] = $requestData['option'];
        $itemData['note'] = $requestData['note'];
        $itemData['created_at'] = $requestData['created_at'];
        $itemData['updated_at'] = $itemData['created_at'];
        $itemData['cost'] = $this->getCurrentStoneCost($requestData['option'], $product);
        $itemData['state'] = 0;
        $this->setData($itemData)->save();
        $this->addData(['barcode' => $this->generateBarcode($this->getId())])->setId($this->getId())->save();

        return $this->getBarcode();
    }

    public function imageUrlFromOption($productData, Mage_Catalog_Model_Product $product){
        $sku = $product->getSku();
        if(Mage::helper('personalizedproduct')->isPersonalizedProduct($product)){
            foreach($productData['options'] as $option){
                $option['value'] = strtolower($option['value']);
                if($option['option_type'] == 'stone') {
                    $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.str_replace(' ', '',$option['value']);
                }
            }
            foreach($productData['options'] as $option){
                $option['value'] = strtolower($option['value']);
                if($option['option_type'] == 'gold') {
                    $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . str_replace(' ', '',$option['value']);
                }
            }
            $sku.=OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.'1';
            $product->setSku($sku);
        }

        return Mage::helper('catalog/image')->init($product, 'thumbnail');
    }

    public function getCurrentStoneCost($options, $product){
        $options = unserialize($options);
        
        $stoneOptionModel = Mage::getModel('personalizedproduct/price_stone');
        $purchaseOrderStoneModel = Mage::getModel('opentechiz_purchase/purchasestone');

        if(isset($options['options'])) $optionArray = $options['options'];
        else $optionArray = [];
        $totalPrice = 0;

        foreach($optionArray as $option){
            if($option['option_type'] == 'stone'){
                $canGetDataFromProductOption = false;
                foreach ($product->getOptions() as $productOption) {
                    if($productOption->getId() == $option['option_id']){
                        $value = $productOption->getValueById($option['option_value']);
                        $stoneSku = $value->getSku();
                        $canGetDataFromProductOption = true;
                        break;
                    }
                }

                if(!$canGetDataFromProductOption) {
                    $stoneSku = strtolower($option['value']);
                }

                $quality = '';
                if (strpos($stoneSku, 'diamond') !== false) {
                    $type = str_replace('diamond', '', $stoneSku);
                    if(in_array($type, OpenTechiz_PersonalizedProduct_Helper_Data::QUALITY)){
                        $quality = $type;
                    }
                }

                $query  = 'stone.stone_id = main_table.stone_id and main_table.option_id = "'.$option['option_id'].'" and stone.stone_type = "'.$stoneSku.'"';
                if($quality != ''){
                    $query .= 'and stone.quality = "'.$quality.'"';
                }

                $collection = $stoneOptionModel->getCollection()->join(array('stone'=> 'opentechiz_material/stone'), $query, '*');

                foreach ($collection as $requiredStone) {
                    $stoneId = $requiredStone->getStoneId();
                    $lastPurchaseOrder = $purchaseOrderStoneModel->getCollection()->addFieldToFilter('stone_id', $stoneId)
                        ->setOrder('po_stone_id', 'desc')->getFirstItem();
                    $totalPrice += (float)($lastPurchaseOrder->getPrice()*$requiredStone->getQuantity());
                }
            }
        }
        return $totalPrice;
    }

    public function getCurrentAlloyCost($castingCode, $goldWeight){
        $goldCasting = Mage::getModel('opentechiz_production/goldcasting')->load($castingCode, 'gold_casting_code');
        $goldBarSerial = $goldCasting->getData('gold_bar_serial_no');
        $goldBarModel = Mage::getModel('opentechiz_material/goldbar');

        $joinData = $goldBarModel->getCollection()->join(array('po'=> 'opentechiz_purchase/purchase'),'main_table.po_increment_id = po.po_increment_id and main_table.serial_no = "'. $goldBarSerial.'"')->getFirstItem();
        $poID = $joinData->getData('po_id');
        $goldId = $joinData->getData('gold_id');

        $goldPrice = Mage::getModel('opentechiz_purchase/purchasegold')->getCollection()->addFieldToFilter('po_id', $poID)
            ->addFieldToFilter('gold_id', $goldId)->getFirstItem()->getPrice();

        return $goldPrice*$goldWeight;
    }
}