<?php
class OpenTechiz_Inventory_Model_Observer
{
    public function updateItem(Varien_Event_Observer $observer)
    {
        $shipment = $observer->getEvent()->getShipment();
        foreach ($shipment->getAllItems() as $shipmentItem) {
                $order_item_id = $shipmentItem->getOrderItem()->getId();
                $order_item = Mage::getModel('sales/order_item')->load( $order_item_id);
               
                if($order_item->getData('qty_shipped')>=$order_item->getData('qty_ordered')){
                    $item_orders = Mage::getModel('opentechiz_inventory/order')->getCollection()->AddFieldtoFilter('order_item_id',$order_item_id);
                  
                    foreach ($item_orders as $item_order) {
                        # code...
                        $item = Mage::getModel('opentechiz_inventory/item')->load($item_order->getItemId());
                        $item->setState('15')->save();
                    }
                }
        }
    }

    public function productSaveUpdateInventory(Varien_Event_Observer $observer){
        $job = new OpenTechiz_Inventory_Model_Job_ProductSaveUpdateInventory();
        $job->setProductId($observer->getProduct()->getId());
        $job->defer();
    }

    public function cleanUpStockAfterProductDelete($observer){
        $eventProduct = $observer->getEvent()->getProduct();
        $stockCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()
            ->addFieldToFilter('product_id', $eventProduct->getId());
        foreach ($stockCollection as $stock){
            $qty = $stock->getQty();
            $sku = $stock->getSku();
            $date = Mage::getModel('core/date')->date('Y-m-d');

            //log if item have qty
            if($qty > 0){
                Mage::log($date.': Deleted '.$qty.' items with sku '.$sku .' after deleted product '.$eventProduct->getId(),
                    null, 'product_delete_cleanup_stock.log');
            }

            //delete items
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $tableName = Mage::getSingleton('core/resource')->getTableName('inventory_item');

            $where = array('sku = ?' => $sku, 'state = ?' => OpenTechiz_Inventory_Model_Item::STATE_IN_STOCK);
            $write->delete($tableName, $where);

            //delete stock
            $stock->delete();
        }
    }

    public function validateStockBeforeProductDelete($observer){
        $eventProduct = $observer->getEvent()->getProduct();
        $productId = $eventProduct->getId();

        $stockCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()
            ->addFieldToFilter('product_id', $productId);

        $haveStock = false;
        foreach ($stockCollection as $stock){
            if($stock->getQty() > 0){
                $haveStock = true;
                break;
            }
        }

        if($haveStock){
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('opentechiz_inventory')
                    ->__('Product have QTY in stock, can only delete product with no item in stock.')
            );

            $params = Mage::app()->getRequest()->getParams();
            if(isset($params['id'])){
                $url = Mage::getUrl('*/*/edit', array('id' => $params['id']));
            }else{
                $url = Mage::getUrl('*/*');
            }

            Mage::app()->getResponse()->setRedirect($url);
            Mage::app()->getResponse()->sendHeaders();
            exit;
        }
    }
}