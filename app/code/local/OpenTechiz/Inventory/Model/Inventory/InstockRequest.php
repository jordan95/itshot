<?php

class OpenTechiz_Inventory_Model_Inventory_InstockRequest extends Mage_Core_Model_Abstract
{
    const goldQuality = ['10k', '14k'];
    const INTERNAL_STATUS_ORDER = 3;
    protected function _construct()
    {
        $this->_init('opentechiz_inventory/inventory_instockRequest');
    }

    public function takeItem($id){
        $itemModel = Mage::getModel('opentechiz_inventory/item');
        $request = $this->load($id);
        $instockProduct = Mage::getModel('opentechiz_inventory/instock')->load($request->getInstockProductId());
        $qty = $request->getQty();
        $error = '';

        $needChangeStock = true;
        if($needChangeStock) {
            if ((int)$instockProduct->getQty() >= (int)$qty && (int)$qty > 0) {
                $orderItemId = $request->getOrderItemId();
                $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);
                $order = $orderItem->getOrder();
                $type = 12;
                $test =$order->getInternalStatus();
                $params = Mage::app()->getRequest()->getParam('internal_status');
               
                if(isset($params) && $params == self::INTERNAL_STATUS_ORDER){ // when change Internal Status to Engraving/Sizing
                    $type = 11;
                   
                }
                if((Mage::app()->getRequest()->getControllerName()=="instockRequest" && Mage::app()->getRequest()->getActionName()=="massStatus")){
                   $type = 11;
                }
                 

               
                if ($this->getReturnId()) {
                    $return = Mage::getModel('opentechiz_return/return')->load($this->getReturnId());
                    $item = $itemModel->getCollection()->AddFieldtoFilter('barcode', $return->getNewItemBarcode())->getFirstItem();
                    $this->createResizeAndEngraveforReturn($id, $item->getId(), $return);
                } else {
                    $barcode = $request->getBarcodes();
                    if($barcode){
                        $item = $itemModel->load($barcode, 'barcode');
                        $itemId = $item->getId();
                    }else {
                        $orderItemId = $request->getOrderItemId();
                        $itemId = Mage::getModel('opentechiz_inventory/order')->load($orderItemId, 'order_item_id')->getItemId();
                        $item = $itemModel->load($itemId);
                    }
                    
                    $item->addData(['state' => 1])->save();
                    
                    //add log release reserved item
                    Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                        'qty' => '-' . $qty,
                        'on_hold' => '-' . $qty,
                        'type' => $type,
                        'stock_id' => $instockProduct->getId(),
                        'qty_before_movement' => $instockProduct->getQty(),
                        'on_hold_before_movement' => $instockProduct->getOnHold(),
                        'sku' => $instockProduct->getSku(),
                        'order_id' =>$order->getId(),
                        'instock_request_id' =>$request->getId()
                    ));
                    $this->createResizeAndEngrave($id, $itemId);
                }

                //delete process request
                
                $processRequest = Mage::getModel('opentechiz_production/process_request')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItemId)
                    ->addFieldToFilter('status', 2)->getFirstItem();
                if ($processRequest && $processRequest->getId()) {
                    $processRequest->delete();
                }

                //check for production request and delete link if not needed
                
                $qtyOrdered = $orderItem->getQtyOrdered() - $orderItem->getQtyRefunded() - $orderItem->getQtyCanceled();
                $linkCollection = Mage::getModel('opentechiz_inventory/order')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItemId)
                    ->join(array('item' => 'opentechiz_inventory/item'), 'main_table.item_id = item.item_id', '*');
                $itemCreatedCount = $linkCollection->getSize();
                while($itemCreatedCount > $qtyOrdered){
                    $linkCollection = Mage::getModel('opentechiz_inventory/order')->getCollection()
                        ->addFieldToFilter('order_item_id', $orderItemId)
                        ->join(array('item' => 'opentechiz_inventory/item'), 'main_table.item_id = item.item_id', '*')
                        ->addFieldToFilter('state', 0);
                    if(count($linkCollection) == 0) break;
                    $linkedItem = $linkCollection->getFirstItem();
                    //remove production request from production request
                    $processRequest = Mage::getModel('opentechiz_production/product')->load($linkedItem->getBarcode(), 'barcode');
                    $processRequest->setOrderItemId(null)->save();
                    //delete item-order link
                    $item = Mage::getModel('opentechiz_inventory/item')->load($linkedItem->getBarcode(), 'barcode');
                    $linkCollection = Mage::getModel('opentechiz_inventory/order')->getCollection()->addFieldToFilter('item_id', $item->getId())
                        ->addFieldToFilter('order_item_id', $orderItemId);
                    foreach ($linkCollection as $link){
                        $link->delete();
                    }
                    $itemCreatedCount--;
                }

            } else {
                $error = Mage::helper('opentechiz_production')->__('Quantity in stock is not sufficient');
            }
        }

        return $error;
    }

    public function createResizeAndEngraveforReturn($id, $itemId , $return){
        
        $item = Mage::getModel('opentechiz_inventory/item')->load($itemId);
        $barcode = $item->getBarcode();
        $orderItemId = $this->load($id)->getOrderItemId();
        $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);

        $admin = Mage::getSingleton('admin/session')->getUser();
        $isUser = false;
        if ($admin && $admin->getId()){//check if admin is logged in
            $isUser = true;
        }

        $skuparts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $item->getSku());
        $size = $skuparts[count($skuparts)-1];

        $hasEngravingOption = false;
        $hasEngraving = false;

        if($return->getNewOption()){
            $data_options = $return->getNewOption();
        }else{
            $data_options = $orderItem->getData('product_options');
            $data_sku = $orderItem->getSku();
        }
        $options = unserialize($data_options);

        foreach($options['options'] as $option){
            if(strpos(strtolower($option['label']),OpenTechiz_PersonalizedProduct_Helper_Data::ENGRAVING_KEYWORD) !== false){
                $hasEngravingOption = true;
                break;
            }
        }
        if($hasEngravingOption) {
            $hasEngraving = true;
            foreach ($options['options'] as $option) {
                if (strpos(strtolower($option['value']), OpenTechiz_PersonalizedProduct_Helper_Data::DONT_ENGRAVE) !== false) {
                    $hasEngraving = false;
                    break;
                }
            }
        }

        $neededResize = true;
        if(is_numeric($size)&&$hasEngraving==false){
            $ar_options = array_reverse($options['options']);
            foreach($ar_options as $option){
                if($option['option_value']) {
                    $optionvalue = Mage::getModel('catalog/product_option_value')->load($option['option_value']);

                    if(strpos(strtolower($option['label']), OpenTechiz_PersonalizedProduct_Helper_Data::SIZE) !== false && is_numeric($optionvalue->getData('sku'))){
                        $return_item_size = $optionvalue->getData('sku');
                        break;
                    }

                }
            }
            if(!isset($return_item_size)){
                $orderedSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR,$data_sku );
                $return_item_size = $orderedSku[count($orderedSku)-1];
            }

            if($size == $return_item_size){
                $neededResize = false;
            }
        }

        if($neededResize || $hasEngraving) {
            $requestModel = Mage::getModel('opentechiz_production/product');

            $data = [];
            $data['barcode'] = $barcode;
            $data['image'] = $item->getImage();
            $data['production_type'] = 5;
            $data['order_item_id'] = $orderItemId;
            $data['option'] = $data_options;
            $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['status'] = 38;
            $data['item_state'] = 4;

            $requestModel->setData($data)->save();
            $return->setStatus(61)->save();
        }else{
            $item->setState(10)->save();

            $returnModel = Mage::getModel('opentechiz_return/return');
            $return = $returnModel->load($orderItemId, 'order_item_id');
            if($return && $return->getReturnFeeOrderId()){ //if return order
                $order = Mage::getModel('sales/order')->loadByIncrementId($return->getReturnFeeOrderId());
                if(Mage::helper('opentechiz_salesextend')->checkReservedItems($order)) {
                    Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 3, 6, null, $isUser);
                }
            }
        }
    }

    public function createResizeAndEngrave($id, $itemId){
        $item = Mage::getModel('opentechiz_inventory/item')->load($itemId);
        $barcode = $item->getBarcode();
        $orderItemId = $this->load($id)->getOrderItemId();
        $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);

        $admin = Mage::getSingleton('admin/session')->getUser();
        $isUser = false;
        if ($admin && $admin->getId()){//check if admin is logged in
            $isUser = true;
        }

        $options = unserialize($orderItem->getData('product_options'));
        if(is_array($options) && array_key_exists('options', $options)) {
            $hasEngravingOption = false;
            $hasEngraving = false;

            foreach($options['options'] as $option){
                if(strpos(strtolower($option['label']),OpenTechiz_PersonalizedProduct_Helper_Data::ENGRAVING_KEYWORD) !== false){
                    $hasEngravingOption = true;
                    break;
                }
            }

            if($hasEngravingOption) {
                $hasEngraving = true;
                foreach ($options['options'] as $option) {
                    if (strtolower($option['value']) == OpenTechiz_PersonalizedProduct_Helper_Data::DONT_ENGRAVE) {
                        $hasEngraving = false;
                        break;
                    }
                }
            }
            
            $neededResize = Mage::helper('opentechiz_inventory')->itemNeedResize($item->getSku(), $orderItem->getSku());

            if ($neededResize || $hasEngraving) {
                $requestModel = Mage::getModel('opentechiz_production/product');
                $status = 38;
                if (!$neededResize) $status = 40;

                $data = [];
                $data['barcode'] = $barcode;
                $data['image'] = $item->getImage();
                $data['production_type'] = 5;
                $data['order_item_id'] = $orderItemId;
                $data['option'] = $orderItem->getData('product_options');
                $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                $data['status'] = $status;
                $data['item_state'] = 4;

                $requestModel->setData($data)->save();
            } else {
                $item->setState(10)->save();

                //set status to item ready
                $order = Mage::getModel('sales/order')->load($orderItem->getOrderId());
                if(Mage::helper('opentechiz_salesextend')->checkReservedItems($order)) {
                    $totalPaid = Mage::helper('opentechiz_salesextend')->getTotalPaymentRecordForOrder($order);
                    $orderTotal = round($order->getBaseGrandTotal(), 4);
                    $totalPaid = round($totalPaid, 4);
                    if ($totalPaid < $orderTotal && $order->getOrderType() == 1) {
                        Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 3, 26, null, $isUser);
                    }else {
                        Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 3, 6, null, $isUser);
                    }
                }
            }
        }else{
            $item->setState(10)->save();

            //set status to item ready
            $order = Mage::getModel('sales/order')->load($orderItem->getOrderId());
            if(Mage::helper('opentechiz_salesextend')->checkReservedItems($order)) {
                $totalPaid = Mage::helper('opentechiz_salesextend')->getTotalPaymentRecordForOrder($order);
                $orderTotal = round($order->getBaseGrandTotal(), 4);
                $totalPaid = round($totalPaid, 4);
                if ($totalPaid < $orderTotal && $order->getOrderType() == 1) {
                    Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 3, 26, null, $isUser);
                }else {
                    Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 3, 6, null, $isUser);
                }
            }
        }
    }

    public function releaseItem($id){
        $itemModel = Mage::getModel('opentechiz_inventory/item');
        $instockProduct = Mage::getModel('opentechiz_inventory/instock')->load($this->load($id)->getInstockProductId());
        $qty = $this->load($id)->getQty();
        Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
            'qty' => 0,
            'on_hold' => '-'.$qty,
            'type' => 0,
            'stock_id' => $instockProduct->getId(),
            'qty_before_movement' => $instockProduct->getQty(),
            'on_hold_before_movement' => $instockProduct->getOnHold(),
            'sku' => $instockProduct->getSku()

        ));

        $collection = $this->getCollection()->join(array('order' => 'opentechiz_inventory/order'),
            'order.order_item_id = main_table.order_item_id AND request_id  = '.$id,
            array('*'));
        foreach ($collection->getData() as $item) {
            $itemId = $item['item_id'];
            $item = $itemModel->load($itemId);
            $item->addData(['state' => 35])->setId($itemId)->save();

            //delete order-item
            Mage::getModel('opentechiz_inventory/order')->deleteOrderItemBind($item->getId(), $this->load($id)->getOrderItemId());
        }
    }
}