<?php

class OpenTechiz_Inventory_Model_Certificate extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('opentechiz_inventory/certificate');
    }
}