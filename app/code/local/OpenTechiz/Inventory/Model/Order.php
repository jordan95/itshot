<?php

class OpenTechiz_Inventory_Model_Order extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('opentechiz_inventory/order');
    }

    public function deleteOrderItemBind($itemId, $orderItemId){
        $collection = $this->getCollection()->addFieldToFilter('order_item_id', $orderItemId)
            ->addFieldToFilter('item_id', $itemId);
        foreach ($collection as $item){
            $item->delete();
        }
    }
}