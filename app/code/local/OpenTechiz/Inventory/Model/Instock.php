<?php

class OpenTechiz_Inventory_Model_Instock extends Mage_Core_Model_Abstract
{
    protected $_eventPrefix = 'opentechiz_inventory_instock';

    protected function _construct()
    {
        $this->_init('opentechiz_inventory/instock');
    }

    public function addStock($productSku){
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productSku);
        if(!$product->getId()){
            return false;
        }else {
            $html = '';

            $html .= '<input type="hidden" name="product_id" value="' . $product->getId() . '"/>';
            $product = Mage::getModel('catalog/product')->load($product->getId());

            foreach ($product->getOptions() as $option) {
                $html .= '<tr>';

                if (strpos(strtolower($option->getTitle()), 'engraving') === false) {
                    $html .= '<td class="label"><strong>Select ' . $option->getTitle() . ':</strong></td>';
                    $html .= '<td class="value">';
                    $html .= '<select required name="' . $option->getSortOrder() . '">';
                    $html .= '<option value="">-- Select Option --</option>';
                    foreach ($option->getValues() as $value) {
                        $html .= '<option value="' . $value->getSku() . '">' . $value->getSku() . '</option>';
                    }
                    $html .= '</select>';
                    $html .= '</td>';
                }

                $html .= '</tr>';
            }

            $html .= '<tr>';
            $html .= '<td class="label"><strong>Quantity:</strong></td>';
            $html .= '<td class="value"><input required name="qty" class="text-field input-text" /></td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td class="label"><strong>Item Cost (Each):</strong></td>';
            $html .= '<td class="value"><input required name="cost" class="text-field input-text" /></td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td class="label"><strong>Note:</strong></td>';
            $html .= '<td class="value"><textarea name="note"></textarea></td>';
            $html .= '</tr>';
        }
        return $html;
    }

    public function refreshFlatData(){
        //save flag to store date last updated
        $coreFlagCode = 'inventory_instock_refresh_date';
        $flag = Mage::getModel('core/flag', array('flag_code' => $coreFlagCode))->loadSelf();

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        //populate qty in manufacture
        $query = "update tsht_inventory_instock_product join 
            ( SELECT count(*) as countManufacture, tsht_inventory_item.sku as sku 
            FROM tsht_request_product join tsht_inventory_item
            on tsht_request_product.barcode = tsht_inventory_item.barcode and tsht_inventory_item.state = 0 group by sku) 
            as countTable on tsht_inventory_instock_product.sku = countTable.sku 
            set flat_qty_in_manufacture = countTable.countManufacture";
        $writeConnection->query($query);

        //populate qty in open PO
        $query = "update tsht_inventory_instock_product join 
            (SELECT sku, sum(qty - delivered_qty) as qty_not_delivered FROM `tsht_purchase_order_item` 
            join tsht_purchase_order on tsht_purchase_order.po_id = tsht_purchase_order_item.po_id 
            where delivered_qty < qty GROUP BY sku) as countTable 
            on tsht_inventory_instock_product.sku = countTable.sku 
            set flat_qty_in_open_po = countTable.qty_not_delivered";
        $writeConnection->query($query);

        //populate need to order status
        $query = "update tsht_inventory_instock_product join 
            (SELECT sku, count(*) as countOrder FROM `tsht_sales_flat_order_item` join tsht_sales_flat_order 
            on tsht_sales_flat_order.entity_id = tsht_sales_flat_order_item.order_id 
            where tsht_sales_flat_order.internal_status IN (3, 21) GROUP BY sku) as countTable 
            on tsht_inventory_instock_product.sku = countTable.sku 
            set flat_need_to_order = countTable.countOrder";
        $writeConnection->query($query);
        $query = "update tsht_inventory_instock_product 
            set flat_need_to_order = 1
            where tsht_inventory_instock_product.flat_need_to_order > 1";
        $writeConnection->query($query);

        $flagData = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $flag->setFlagData($flagData)->save();
    }
}