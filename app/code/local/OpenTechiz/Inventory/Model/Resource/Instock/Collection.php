<?php

class OpenTechiz_Inventory_Model_Resource_Instock_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_inventory/instock');
    }

    protected function getQuerySummary()
    {
        $this->_renderFilters();
        $select = clone $this->getSelect();

        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);
        $select->reset(Zend_Db_Select::COLUMNS);
        return $select;
    }

    public function getQtySummary($sku, $tagSku)
    {
        $qty_summary = array();
        $qty = 0;
        if ($sku || $tagSku) {
            $isSku = $this->getProduct($sku);
            $where = "main_table.sku like '%" . $sku . "%'";
            if (!$isSku) {
                $isSku = $this->getProduct($tagSku);
                $where = "main_table.tag_sku like '%" . $tagSku . "%'";
            }
            if ($isSku) {
                $collection = Mage::getModel('opentechiz_inventory/instock')->getCollection();
                $collection->getSelect()
                    ->where($where)
                    ->columns('SUM(main_table.qty) as total_qty')->group('main_table.group_sku');

                if ($collection->count() > 0) {
                    foreach ($collection as $item) {
                        $sku = '';
                        if ($item->getTotalQty() > 0) {
                            if ($item->getGroupSku() == '') {
                                $sku = $item->getSku();
                            } else {
                                $sku = $item->getGroupSku();
                            }
                            $qty_summary[] = $sku . ' x ' . $item->getTotalQty();
                        }
                    }
                }
                if (!empty($qty_summary)) {
                    $qty = implode(", ", $qty_summary);
                }
            }

        }
        return $qty;
    }

    public function getQtyOpenPurchase($sku, $tagSku)
    {
        $purchase = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/purchase');
        $order_item = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/orderitem');
        $qty_summary = array();
        $qty = 0;
        if ($sku || $tagSku) {
            $isSku = $this->getProduct($sku);
            $where = "main_table.sku like '%" . $sku . "%'";
            if (!$isSku) {
                $isSku = $this->getProduct($tagSku);
                $where = "main_table.tag_sku like '%" . $tagSku . "%'";
            }
            if ($isSku) {
                $collection = Mage::getModel('opentechiz_inventory/instock')->getCollection();
                $collection->getSelect()
                    ->join(array('po_item' => $order_item), 'main_table.sku = po_item.sku', 'delivered_qty')
                    ->join(array('purchase_order' => $purchase), "po_item.po_id = purchase_order.po_id", array('*'))
                    ->where("purchase_order.status NOT IN(3,4,5) AND " . $where . " AND po_item.qty > po_item.delivered_qty");
                $collection->getSelect()->columns('SUM(po_item.qty) as qty_po,SUM(po_item.delivered_qty) as delivered_qty_po')->group('main_table.group_sku');
                if ($collection->count() > 0) {
                    foreach ($collection as $item) {
                        $undelivery_qty = $item->getQtyPo() - $item->getDeliveredQtyPo();
                        if ($undelivery_qty > 0) {
                            $sku = '';
                            if ($item->getGroupSku() == '') {
                                $sku = $item->getSku();
                            } else {
                                $sku = $item->getGroupSku();
                            }
                            $qty_summary[] = $sku . ' x ' . $undelivery_qty;
                        }
                    }
                }
                if (!empty($qty_summary)) {
                    $qty = implode(", ", $qty_summary);
                }
            }
        }
        return $qty;
    }

    public function getQtyManufacturing($sku, $tagSku)
    {
        $sales_order_item = Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $sales_order = Mage::getSingleton('core/resource')->getTableName('sales/order');
        $qty_summary = array();
        $qty = 0;
        if ($sku || $tagSku) {
            $isSku = $this->getProduct($sku);
            $where = "main_table.sku like '%" . $sku . "%'";
            if (!$isSku) {
                $isSku = $this->getProduct($tagSku);
                $where = "main_table.tag_sku like '%" . $tagSku . "%'";
            }
            if ($isSku) {
                $collection = Mage::getModel('opentechiz_inventory/instock')->getCollection();
                $collection->getSelect()
                    ->join(array('sales_order_item' => $sales_order_item), "main_table.sku = sales_order_item.sku", array('sales_order_item.qty_ordered'))
                    ->join(array('sales_order' => $sales_order), "sales_order.entity_id = sales_order_item.order_id", array('sales_order_item.order_id'))
                    ->where("sales_order.order_stage = 2 AND sales_order.internal_status =2 AND sales_order.state NOT IN('canceled','complete','holded','closed') AND " . $where)
                    ->columns('SUM(sales_order_item.qty_ordered) as total_qty')
                    ->group('main_table.group_sku');

                if ($collection->count() > 0) {
                    foreach ($collection as $item) {
                        $sku = '';
                        if ($item->getTotalQty() > 0) {
                            if ($item->getGroupSku() == '') {
                                $sku = $item->getSku();
                            } else {
                                $sku = $item->getGroupSku();
                            }
                            $qty_summary[] = $sku . ' x ' . (int)$item->getTotalQty();
                        }
                    }
                }
                if (!empty($qty_summary)) {
                    $qty = implode(", ", $qty_summary);
                }
            }
        }
        return $qty;
    }

    public function getProduct($sku)
    {
        $tagSku = $sku;
        $sku = explode('_', $sku);
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku[0]);
        if (!$product) {
            $tagSku = explode('/', $tagSku);
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $tagSku[0]);
        }
        if ($product) {
            return true;
        } else {
            return false;
        }
    }
}