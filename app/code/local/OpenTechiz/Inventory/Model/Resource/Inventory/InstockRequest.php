<?php

class OpenTechiz_Inventory_Model_Resource_Inventory_InstockRequest extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_inventory/inventory_instockRequest', 'request_id');
    }
}