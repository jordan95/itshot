<?php

class OpenTechiz_Inventory_Model_Resource_Order extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_inventory/order', 'id');
    }
}