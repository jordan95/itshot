<?php

class OpenTechiz_Inventory_Model_Resource_StockMovement_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_inventory/stockMovement');
    }
    public function getTotals()
    {
        $this->_renderFilters();
        $select = clone $this->getSelect();
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->columns(array(
            'sum_delivered_qty' => new Zend_Db_Expr("SUM(purchase_item.delivered_qty)"),
            'sum_price_paid' => new Zend_Db_Expr("SUM(purchase_item.price)"),
            'count_price_paid' => new Zend_Db_Expr("COUNT(purchase_item.price)"),
            'sum_qty_ordered' => new Zend_Db_Expr("SUM(sales_order_item.qty_ordered)"),
            'sum_item_price' => new Zend_Db_Expr("SUM(sales_order_item.price)"),
            'count_item_price' => new Zend_Db_Expr("COUNT(sales_order_item.price)"),
        ));
        return $this->getResource()->getReadConnection()->fetchRow($select);
    }
    public function addGroupBySkuFilter()
    {
        $this->getSelect()->group('instock.sku');
        return $this;
    }
    public function getTotalsGroupType()
    {
        $da = Mage::app()->getRequest()->getParams();
        
        $this->_renderFilters();
        $select = clone $this->getSelect();

        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->columns(array(
            'qty' => new Zend_Db_Expr("SUM(main_table.qty)"),
            'type' => new Zend_Db_Expr("main_table.type"),
        ));
        $select->group('main_table.type'); 
        return $this->getResource()->getReadConnection()->fetchAll($select);
    }
    public function getLastCost($sku)
    {
        $table_purchase =  Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/purchase');
        $table_purchase_supplier =  Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/supplier');
        $data_item = array();
        if ($sku !=false) {
            $is_sku = $this->getProduct($sku);
            if($is_sku){

                $collection = Mage::getModel('opentechiz_inventory/instock')->getCollection();
                $collection->getSelect()
                            ->joinLeft(array('purchase_order' => $table_purchase), "purchase_order.po_id = main_table.last_po_id", array('purchase_order.supplier_id'))
                            ->joinLeft(array('supplier' => $table_purchase_supplier), "supplier.supplier_id = purchase_order.supplier_id", array('supplier.name'))
                            ->columns(array('last_cost','last_cost_date'))
                            ->where("(sku = '".$sku."' OR sku like '".$sku."\_%' ) AND last_cost_date !=''" )
                            ->order(array('last_cost_date desc'))->limit(1);
                if ($collection->count() > 0){
                    foreach($collection as $item){
                        if(!is_null($item->getLastCost())){
                            $data_item['last_cost'] = Mage::helper('core')->currency($item->getLastCost(), true, false);
                        }
                        if(!empty($item->getLastCostDate())){
                            $data_item['last_cost_date'] = $this->getConvertDate($item->getLastCostDate());
                        }else{
                            $data_item['lowest_cost_date'] = '';
                        }
                        $data_item['supplier_name'] = $item->getName();
                    }
                }
            }
        }
        
        return $data_item;
    }
    public function getLowestCost($sku)
    {
        $table_purchase_supplier =  Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/supplier');
        $table_purchase =  Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/purchase');
        $lowest_cost = 100000000;
        $data_item = array();
        if ($sku !=false) {
            $is_sku = $this->getProduct($sku);
            if($is_sku){
                $collection = Mage::getModel('opentechiz_purchase/orderitem')->getCollection();
                $collection->getSelect()
                            ->joinLeft(array('purchase_order' => $table_purchase), "main_table.po_id = purchase_order.po_id", array('purchase_order.created_at'))
                            ->joinLeft(array('supplier' => $table_purchase_supplier), "supplier.supplier_id = purchase_order.supplier_id", array('supplier.name'))
                            ->reset(Zend_Db_Select::COLUMNS)
                            ->columns(array('price as lowest_cost','purchase_order.created_at as lowest_cost_date','supplier.name'))
                            ->where("sku = '".$sku."' OR sku like '".$sku."\_%'"); 
                if ($collection->count() > 0){
                    foreach($collection as $item){
                        if(!is_null($item->getLowestCost())){
                            if($lowest_cost > $item->getLowestCost()){
                                $lowest_cost = $item->getLowestCost();
                                $data_item['lowest_cost'] = Mage::helper('core')->currency($item->getLowestCost(), true, false);
                                
                                if(!empty($item->getLowestCostDate())){
                                    $data_item['lowest_cost_date'] = $this->getConvertLowestDate($item->getLowestCostDate());
                                }else{
                                    $data_item['lowest_cost_date'] = '';
                                }
                                $data_item['supplier_name'] = $item->getName();
                            }
                        }
                    }
                }
            }
        }
        
        return $data_item;
    }
    public function getConvertDate($date)
    {
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) { // check correct format date
            return '';
        } 
        $date .= " 10:00:00";
        $createdtime = new Zend_Date(strtotime($date));
        $convertDate  = Mage::getModel('core/date')->date('m/d/Y', $createdtime);
        return $convertDate;
    }
    public function formatPrice($price)
    {
        Mage::helper('core')->currency($price, true, false);
    }
    public function getProduct($sku){
        $sku = explode('_', $sku);
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku',$sku[0]);
        if($product){
            return true;
        }else{
            return false;
        }
    }
    public function getConvertLowestDate($date)
    {
        $createdtime = new Zend_Date(strtotime($date));
        $convertDate  = Mage::getModel('core/date')->date('m/d/Y', $createdtime);
        return $convertDate;
    }
    public function getQtySummary($sku){

        $qty_summary = array();
        $qty = 0;
        
        if ($sku !=false) {
            $is_sku = $this->getProduct($sku);
            if($is_sku){

                $collection = Mage::getModel('opentechiz_inventory/instock')->getCollection();
                $collection->getSelect()
                            ->where("main_table.sku like "."'%".$sku."%'")
                            ->columns('SUM(main_table.qty) as total_qty')->group('main_table.group_sku'); 
             
                if ($collection->count() > 0){
                    foreach($collection as $item){
                        $sku = '';

                        if($item->getTotalQty() > 0){
                            if($item->getGroupSku() ==''){
                                $sku = $item->getSku();
                            }else{
                                $sku = $item->getGroupSku();
                            }
                            $qty_summary[] = $sku .' x '.$item->getTotalQty();
                        }
                       
                    }
                }
                if(!empty($qty_summary)){
                    $qty = implode(", ",$qty_summary); 
                }
            }
            
        }
        return $qty;
    }
    public function getQtyOpenPurchase($sku){
        // $where = $select->getSelect()->getPart(Zend_Db_Select::WHERE);
        $purchase = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/purchase');
        $order_item = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/orderitem');
        $qty_summary = array();
        $qty = 0;
        if ($sku !=false) {

            $is_sku = $this->getProduct($sku);
            if($is_sku){
                $collection = Mage::getModel('opentechiz_inventory/instock')->getCollection();
                $collection->getSelect()
                            
                            ->join(array('po_item' => $order_item), 'main_table.sku = po_item.sku', 'delivered_qty')
                            ->join(array('purchase_order' => $purchase), "po_item.po_id = purchase_order.po_id", array('*'))
                            ->where("purchase_order.status NOT IN(3,4,5) AND main_table.sku like "."'%".$sku."%' AND po_item.qty > po_item.delivered_qty");
                $collection->getSelect()->columns('SUM(po_item.qty) as qty_po,SUM(po_item.delivered_qty) as delivered_qty_po')->group('main_table.group_sku'); 
                if ($collection->count() > 0){
                    foreach($collection  as $item){
                        $undelivery_qty = $item->getQtyPo() -  $item->getDeliveredQtyPo();
                        if($undelivery_qty >0){
                            $sku = '';
                            if($item->getGroupSku() ==''){
                                $sku = $item->getSku();
                            }else{
                                $sku = $item->getGroupSku();
                            }
                            $qty_summary[] = $sku.' x '.$undelivery_qty;
                        }
                    }
                }
                if(!empty($qty_summary)){
                    $qty = implode(", ",$qty_summary); 
                }
            }
        }
        return $qty;
    }
    public function getQtyManufacturing($sku){

        $sales_order_item = Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $sales_order = Mage::getSingleton('core/resource')->getTableName('sales/order');
        $qty = 0;
        if ($sku !=false) {
            $is_sku = $this->getProduct($sku);
            if($is_sku){
                $collection = Mage::getModel('opentechiz_inventory/instock')->getCollection();
                $collection->getSelect()
                            ->join(array('sales_order_item' => $sales_order_item), "main_table.sku = sales_order_item.sku", array('sales_order_item.qty_ordered'))
                            ->join(array('sales_order' => $sales_order), "sales_order.entity_id = sales_order_item.order_id", array('sales_order_item.order_id'))
                            ->where("sales_order.order_stage = 2 AND sales_order.internal_status =2 AND sales_order.state NOT IN('canceled','complete','holded','closed') AND main_table.sku like "."'%".$sku."%'")
                            ->columns('SUM(sales_order_item.qty_ordered) as total_qty')
                            ->group('main_table.group_sku');

                if ($collection->count() > 0){
                    foreach($collection as $item){
                        $sku = '';

                        if($item->getTotalQty() > 0){
                            if($item->getGroupSku() ==''){
                                $sku = $item->getSku();
                            }else{
                                $sku = $item->getGroupSku();
                            }
                            $qty_summary[] = $sku .' x '.(int)$item->getTotalQty();
                        }
                       
                    }
                }
                if(!empty($qty_summary)){
                    $qty = implode(", ",$qty_summary); 
                }
            }
        }
        return $qty;
    }
}