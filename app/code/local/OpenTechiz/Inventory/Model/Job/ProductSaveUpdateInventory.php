<?php

class OpenTechiz_Inventory_Model_Job_ProductSaveUpdateInventory implements Made_Queue_Model_Job_Interface
{
    protected $optionNameContain = ['color', 'colour', 'quality', 'size', 'length', 'letter'];

    protected $_productId;

    public function defer($queue = Made_Queue_Model_Job::DEFAULT_QUEUE,
                          $numRetries = 1)
    {
        $job = Mage::getModel('queue/job');
        $job->setHandler($this);
        $job->setNumRetries($numRetries);
        $job->setQueue($queue);
        $job->save()
            ->enqueue();
    }

    public function perform()
    {
        $product = Mage::getModel('catalog/product')->load($this->getProductId());
        $instockModel = Mage::getModel('opentechiz_inventory/instock');
        $sku = $product->getSku();
        $defaultDate = '2000-01-01 00:00:00';
        $maxCostDate = $defaultDate;
        $cost = 0;
        $maxReorder = 0;
        $data = [];
        $data['product_id'] = $product->getId();

        $options = Mage::getModel('catalog/product_option')->getProductOptionCollection($product);
        if(count($options) == 0){
            if (Mage::getModel('opentechiz_inventory/instock')->getCollection()
                    ->addFieldToFilter('sku', $sku)->getSize() > 0) {
                return;
            }
            $data['sku'] = $sku;
            $data['group_sku'] = $sku;
            $instockModel->setData($data)->save();
            $instockModel->unsetData();
        }else {
            $main = array();
            foreach ($options as $option) {
                $values = $option->getValues();
                $sub = array();
                foreach ($values as $value) {
                    if (!$value->getSku()){   //if no sku, check if value need sku, if so, add sku for value
                        $needUpdate = false;
                        foreach ($this->optionNameContain as $title){
                            if(stripos($option->getTitle(), $title) !== false){
                                $needUpdate = true;break;
                            }
                        }
                        if($needUpdate){
                            $valueSKu = $value->getTitle();
                            $valueSKu = strtolower($valueSKu);
                            $valueSKu = str_replace('(custom)', '', $valueSKu);
                            $valueSKu = str_replace(' ', '', $valueSKu);
                            $value->setSku($valueSKu)->save();
                        }else{
                            continue;
                        }
                    }
                    array_push($sub, $value->getSku());
                }
                if (count($sub) > 0) {
                    array_push($main, $sub);
                }
            }
            if (count($main) == 1) {
                foreach ($this->combinations($main) as $option) {
                    $fullSku = $sku . OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . $option;
                    if (Mage::getModel('opentechiz_inventory/instock')->getCollection()
                            ->addFieldToFilter('sku', $fullSku)->getSize() > 0) {
                        continue;
                    }
                    $data['sku'] = $fullSku;
                    //set group sku
                    $groupSku = Mage::helper('opentechiz_inventory')->getGroupSKU($fullSku);
                    $data['group_sku'] = $groupSku;
                    //get cost data from other variant
                    $otherVariantCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()
                        ->addFieldToFilter('sku', array('like' => $sku.'%'));
                    if(count($otherVariantCollection) > 0){
                        foreach ($otherVariantCollection as $otherVariant){
                            if($otherVariant->getReorderPoint() > $maxReorder) $maxReorder = $otherVariant->getReorderPoint();
                            if(strtotime($otherVariant->getLastCostDate()) > strtotime($otherVariant->getLastCostDate($maxCostDate))){
                                $maxCostDate = $otherVariant->getLastCostDate();
                                $cost = $otherVariant->getLastCost();
                            }
                        }
                    }

                    $data['reorder_point'] = $maxReorder;
                    if($maxCostDate == $defaultDate) $maxCostDate = '';
                    $data['last_cost_date'] = $maxCostDate;
                    $data['last_cost'] = $cost;
                    

                    $instockModel->setData($data)->save();
                    $instockModel->unsetData();
                }
            } elseif (count($main) > 1) {
                foreach ($this->combinations($main) as $option) {
                    $fullSku = $sku;
                    foreach ($option as $value) {
                        $fullSku = $fullSku . OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . $value;
                    }
                    if (Mage::getModel('opentechiz_inventory/instock')->getCollection()
                            ->addFieldToFilter('sku', $fullSku)->getSize() > 0) {
                        continue;
                    }
                    $data['sku'] = $fullSku;
                    //set group sku
                    $groupSku = Mage::helper('opentechiz_inventory')->getGroupSKU($fullSku);
                    $data['group_sku'] = $groupSku;
                    //get cost data from other variant
                    $otherVariantCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()
                        ->addFieldToFilter('sku', array('like' => $sku.'%'));
                    if(count($otherVariantCollection) > 0){
                        foreach ($otherVariantCollection as $otherVariant){
                            if($otherVariant->getReorderPoint() > $maxReorder) $maxReorder = $otherVariant->getReorderPoint();
                            if(strtotime($otherVariant->getLastCostDate()) > strtotime($otherVariant->getLastCostDate($maxCostDate))){
                                $maxCostDate = $otherVariant->getLastCostDate();
                                $cost = $otherVariant->getLastCost();
                            }
                        }
                    }
                    $data['reorder_point'] = $maxReorder;
                    if($maxCostDate == $defaultDate) $maxCostDate = '';
                    $data['last_cost_date'] = $maxCostDate;
                    $data['last_cost'] = $cost;

                    $instockModel->setData($data)->save();
                    $instockModel->unsetData();
                }
            }
        }
    }

    public function getProductId()
    {
        return $this->_productId;
    }

    public function setProductId($productId)
    {
        $this->_productId = $productId;
    }

    function combinations($arrays, $i = 0)
    {
        if (!isset($arrays[$i])) {
            return array();
        }
        if ($i == count($arrays) - 1) {
            return $arrays[$i];
        }

        $tmp = $this->combinations($arrays, $i + 1);

        $result = array();

        foreach ($arrays[$i] as $v) {
            foreach ($tmp as $t) {
                $result[] = is_array($t) ?
                    array_merge(array($v), $t) :
                    array($v, $t);
            }
        }

        return $result;
    }

    public function serialize() {
        return serialize($this->_productId);
    }

    public function unserialize($item) {
        $this->_productId = unserialize($item);
    }
}