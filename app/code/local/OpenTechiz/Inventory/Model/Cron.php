<?php

class OpenTechiz_Inventory_Model_Cron
{
    public function sendReorderNotyEmail()
    {
        $coreFlagCode = 'reorder_point_report';
        $flag = Mage::getModel('core/flag', array('flag_code' => $coreFlagCode))->loadSelf();

        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/general/reorder_point'));

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('inventory_noty_reorder_tpl');
        $emailTemplate->setTemplateSubject('Inventory ReOrder Report ('.Mage::helper('core')->formatDate().')');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();

        foreach ($to as  $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send($recipientEmail, $emailTemplateVariables);
            }
        }

        $flagData = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $flag->setFlagData($flagData)->save();
    }

    public function refreshFlatData(){
        Mage::getModel('opentechiz_inventory/instock')->refreshFlatData();
    }
}