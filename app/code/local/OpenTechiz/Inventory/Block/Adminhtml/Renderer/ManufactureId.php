<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_ManufactureId extends OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $manufactureIds = $row->getManufactureId();
        return $this->_getLink($manufactureIds);
    }

    /**
     * @param array $poIds
     * @return string
     */
    protected function _getLink(array $manufactureIds)
    {
        $link = array();
        foreach ($manufactureIds as $manufactureId) {
            $filter = 'personalized_id='.$manufactureId;
            $filter = base64_encode($filter);
            $url = Mage::helper('adminhtml')->getUrl('adminhtml/request_product/index', array('filter'=>$filter));
            $link[] = '<a href="' . $url . '">' . $manufactureId . '</a>';
        }
        return implode(', ', $link);
    }
}