<?php
class OpenTechiz_Inventory_Block_Adminhtml_Renderer_LastPOnote extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $po_ids =  $row->getData('purchase_order_id');
        $po_ids = explode(',', $po_ids);
        $po = Mage::getModel('opentechiz_purchase/purchase')->getCollection()->addFieldToFilter('po_increment_id', array('in' => $po_ids))
            ->setOrder('po_id', 'DESC')->getFirstItem();
        return $po->getNote();
    }
}