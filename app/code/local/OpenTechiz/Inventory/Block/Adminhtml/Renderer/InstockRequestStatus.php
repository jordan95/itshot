<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_InstockRequestStatus
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $status = $row->getData($this->getColumn()->getIndex());

        if ($status == 0) {
            $color  = 'black';
            $bg     = 'yellow';
            $value  = 'Pending';
        }
        if ($status == 1) {
            $color  = 'white';
            $bg     = 'green';
            $value  = 'Stock Taken';
        } if ($status == 2) {
            $color  = 'white';
            $bg     = 'red';
            $value  = 'Released';
        }
        return '<div style="color:' . $color . ';font-weight:bold;background:' . $bg . ';border-radius:8px;width:100%; text-align: center">' . $value . '</div>';
    }
}