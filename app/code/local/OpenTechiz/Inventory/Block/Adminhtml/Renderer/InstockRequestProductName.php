<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_InstockRequestProductName
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $instockProductId =  $row->getData('instock_product_id');
        $model = Mage::getModel('opentechiz_inventory/instock');
        $instockItem = $model->load($instockProductId);
        $productId =  $instockItem->getProductId();
        return Mage::getModel('catalog/product')->load($productId)->getName();
    }
}