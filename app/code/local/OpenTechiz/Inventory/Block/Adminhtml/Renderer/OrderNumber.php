<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_OrderNumber extends OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $sku =  $row->getData($this->getColumn()->getIndex());
        $orderStatesToNotDisplay = "('holded', 'closed', 'canceled', 'complete')";
            $collection = Mage::getModel('sales/order_item')->getCollection()->join(array('order' => 'sales/order'),
                'main_table.order_id = order.entity_id AND main_table.sku = "' . addslashes($sku) . '" AND order.order_stage IN (0,1,2,3,4)
            AND order.state NOT IN ' . $orderStatesToNotDisplay, '*');


        $orderIds = [];
        foreach ($collection as $orderItem){
            $countItemReleased = Mage::getModel('opentechiz_inventory/order')->getCollection()
                ->join(array('item' => 'opentechiz_inventory/item'),
                'main_table.item_id = item.item_id AND main_table.order_item_id = "'.$orderItem->getId().'" AND item.state = 10', '*')
                ->getSize();
            $orderIds[$orderItem->getOrderId()] = $countItemReleased;
        }
        $html = '';
        $orders = [];

        if(count($orderIds)) {
            $orderCollection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('entity_id', array('in' => array_keys($orderIds)));
            foreach ($orderCollection as $order) {
                array_push($orders, $order);
                $internalStatusArray = Mage::helper('opentechiz_salesextend')->getStatusArrayByType($order->getOrderType());
                $orderStageArray = OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE;
                $html .= '<a href="' . Mage::helper("adminhtml")->getUrl("adminhtml/sales_order/view", array('order_id' => $order->getId())) . '">' . $order->getIncrementId() . '</a>';
                $html .= ' (Released: '.$orderIds[$order->getId()].')<br>';
                $html .= '('.$orderStageArray[$order->getOrderStage()].' > '.$internalStatusArray[$order->getInternalStatus()].')<br>';
                $date = explode(' ', $this->formatDate($order->getCreatedAt(), 'short', true))[0];
                $html .= ' ('.OpenTechiz_SalesExtend_Helper_Data::ORDER_TYPE[$order->getOrderType()].' - '.$date.')<br>';
            }
        }
        $row->setOrders($orders);
        return $html;
    }
}
