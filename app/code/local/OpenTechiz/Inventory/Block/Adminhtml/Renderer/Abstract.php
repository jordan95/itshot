<?php
class OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function renderHeader()
    {
        if (false !== $this->getColumn()->getGrid()->getSortable() && false !== $this->getColumn()->getSortable()) {
            $className = 'not-sort';
            $dir = strtolower($this->getColumn()->getDir());
            $nDir= ($dir=='desc') ? 'asc' : 'desc';
            if ($this->getColumn()->getDir()) {
                $className = 'sort-arrow-' . $dir;
            }
            $out = '<a href="#" name="' . $this->getColumn()->getId() . '" title="' . $nDir
                . '" class="' . $className . '"><span class="sort-title">'
                . $this->escapeHtml($this->getColumn()->getHeader()) . '</span></a>';
        } else {
            $out = $this->escapeHtml($this->getColumn()->getHeader());
        }
        return $out;
    }
}