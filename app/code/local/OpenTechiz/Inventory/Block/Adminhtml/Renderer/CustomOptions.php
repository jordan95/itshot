<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_CustomOptions extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        if(strpos($row->getData('sku'), 'quotation') === false) {
        $value =  $row->getData($this->getColumn()->getIndex());
        $value = unserialize($value);

        $sku = $row->getData('sku');
        $productName =  $row->getData('name');
        $optionHtml = '';

        if($value && array_key_exists('options', $value)) {
            foreach ($value['options'] as $option) {
                $optionHtml .= '<dt>' . $option['label'] . '</dt>';
                $optionHtml .= '<dd>' . $option['value'] . '</dd>';
            }
        }

        $html = '<div class="item-container">
                    <div class="item-text">
                        <h5 class="title"><span>'.$productName.'</span></h5>
                        <div><strong>SKU:</strong>'.$sku.'</div>
                        <dl class="item-options">
                            '.$optionHtml.'
                        </dl>
                    </div>
                </div>';
        } else{
            $quotationProductId = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $row->getSku())[1];
            $optionHtml = '';

            $optionCollection = Mage::getModel('opentechiz_quotation/productOption')->getCollection()->addFieldToFilter('quote_product_id', $quotationProductId);
            $productName = Mage::getModel('opentechiz_quotation/product')->load($quotationProductId)->getQpName();

            foreach ($optionCollection as $option){
                if($option->getOptionType() == 'stone') {
                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
                    $optionHtml .= '<dd>' . Mage::getModel('opentechiz_quotation/product')->getStoneName($option->getOptionValue()) . '</dd>';
                } elseif ($option->getOptionType() == 'gold'){
                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
                    $optionHtml .= '<dd>' . Mage::getModel('opentechiz_quotation/product')->getGoldName($option->getOptionValue()) . '</dd>';
                } else{
                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
                    $optionHtml .= '<dd>' . $option->getOptionValue() . '</dd>';
                }
            }
            $html = '<div class="item-container">
                    <div class="item-text">
                        <h5 class="title"><span>Quotation Product - ' . $productName . '</span></h5>
                        <div><strong>SKU:</strong>' . $row->getSku() . '</div>
                        <dl class="item-options">
                            ' . $optionHtml . '
                        </dl>
                    </div>
                </div>';
        }
        return $html;
    }
}