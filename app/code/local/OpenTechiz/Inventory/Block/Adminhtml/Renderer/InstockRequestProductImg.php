<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_InstockRequestProductImg
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $instockProductId = $row->getData('instock_product_id');
        $model = Mage::getModel('opentechiz_inventory/instock')->load($instockProductId);
        $sku = $model->getSku();
        $product = Mage::getModel('catalog/product')->load($model->getProductId());

        if(Mage::helper('personalizedproduct')->isPersonalizedProduct($product)){
            $product->setSku($sku);
            $imageUrl = $this->helper('catalog/image')->init($product, 'thumbnail')->resize(120, 120);

            $html = '<img src="'.$imageUrl.'" width="120px" height="120px">';

        } else $html = '<img src="'.Mage::helper('opentechiz_salesextend')->getImageByColor($product, $sku).'" width="120px" height="120px">';

        return $html;
    }
}