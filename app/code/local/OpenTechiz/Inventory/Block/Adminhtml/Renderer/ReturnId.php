<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_ReturnId extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row) {
        $returnId = $row->getReturnId();
        return '<a href="' . Mage::helper('adminhtml')->getUrl('adminhtml/sales_returns/edit/', array('order_id' => $returnId)) . '">' . $returnId . '</a>';
    }
}