<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_InstockStatusDisplay extends OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $qty =  $row->getData($this->getColumn()->getIndex());
        $reorderPoint = $row->getData('reorder_point');
        $onHold = $row->getData('on_hold');

        if($qty-$onHold <= $reorderPoint ) {
            $color = 'red';
            $text = 'Reorder';
        }
        else {
            $color  = 'green';
            $text = 'OK';
        }

        $html = '<div style="color:white; font-weight:bold; background:' . $color . '; border-radius:8px; width:100%;
         text-align: center">' . $text . '</div>';
        return $html;
    }
}