<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_ProductHistoryLink extends OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $sku = $row->getData($this->getColumn()->getIndex());
        $url = Mage::getModel('core/url')->getUrl("adminhtml/stockmovement/index", array("filter" => base64_encode(http_build_query(array(
                'sku_variant' => $sku,
                'timestamp' => array(
                    'from' => date('m/d/Y', strtotime('-2 year')),
                    'locale' => 'en_US'
                )
        )))));
        return '<a href="' . $url . '" target="_blank">' . $sku . '</a>';
    }

}
