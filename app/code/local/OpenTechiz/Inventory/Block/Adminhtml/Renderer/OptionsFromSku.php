<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_OptionsFromSku extends OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $sku =  $row->getData($this->getColumn()->getIndex());
        $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
        if(count($parts) > 1){
            $productId =  $row->getData('product_id');
            $product = Mage::getModel('catalog/product')->load($productId);
            $html = '<div class="item-container">
                        <div class="item-text">
                            <dl class="item-options">';
            $i = 1;
            foreach ($product->getOptions() as $option){
                if(isset($parts[$i])) {
                    if (strpos($option->getTitle(), 'Engraving') === false) {
                        $html .= '<dt>' . $option->getTitle() . '</dt>';
                        if (strpos($option->getTitle(), 'Stone') !== false) {
                            $hasQuality = false;
                            if (strpos($parts[$i], 'diamond') !== false) {
                                $quality = str_replace('diamond', '', $parts[$i]);
                                $hasQuality = array_key_exists($quality, OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_QUALITY);
                            }
                            $html .= '<dd>' . OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_TYPE[$parts[$i]];
                            if ($hasQuality) {
                                $html .= ' ' . OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_QUALITY[$quality];
                            }
                            $html .= '</dd>';
                        } elseif ($option->getTitle() == 'Metal' || $option->getTitle() == 'Alloy') {
                            $html .= '<dd>' . OpenTechiz_Material_Helper_Data::GOLD_NAME_FRONT_END[$parts[$i]] . '</dd>';
                        } else {
                            $html .= '<dd>' . $parts[$i] . '</dd>';
                        }

                        $i++;
                    }
                }
            }

            $html .='        </dl>
                        </div>
                    </div>';
        } else $html = 'This product has no option';

        return $html;
    }
}