<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_OrderId extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $orderId =  $row->getData($this->getColumn()->getIndex());
        $orderIncrementId = $row->getData('increment_id');
        $html = '<a href="'.Mage::getUrl('adminhtml/sales_order/view', array('order_id' => $orderId)).'">'.$orderIncrementId.'</a>';
        return $html;
    }
}