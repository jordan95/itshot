<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_InstockDelivery extends OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $sku = $row->getData('sku');
        $availablePO = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()
            ->addFieldToFilter('sku', $sku)
            ->addFieldToFilter('delivered_qty', array('eq' => new Zend_Db_Expr('qty')))
            ->join(array('po' => 'opentechiz_purchase/purchase'), 'main_table.po_id = po.po_id', '*')
            ->setOrder('po.delivery_date', 'DESC');
        $lastPO = $availablePO->getFirstItem();
        $lastPOData = '';
        if($lastPO && $lastPO->getId()){
            $lastPOData .= '<a href="' . Mage::helper('adminhtml')->getUrl('adminhtml/purchase_product/edit/', array('id' => $lastPO->getPoId())) . '">' . $lastPO->getPoIncrementId() . '</a>';
            $qty = ' (Delivered '.$lastPO->getDeliveredQty().')';
            $lastPOData .= $qty;
        }

        return $lastPOData;
    }
}