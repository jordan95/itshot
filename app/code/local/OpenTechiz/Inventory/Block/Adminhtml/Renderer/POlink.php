<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_POlink extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $po_id = $row->getData('po_id');
        $po_increment = $row->getData('po_increment_id');
        $html = '<a href="'.Mage::getUrl('adminhtml/purchase_product/edit', array('id' => $po_id)).'">'.$po_increment.'</a>';

        echo $html;
    }
}