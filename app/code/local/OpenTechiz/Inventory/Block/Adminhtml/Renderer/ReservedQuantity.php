<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_ReservedQuantity extends OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $hold =  $row->getData($this->getColumn()->getIndex());
        $sku = $row->getData('sku');
        $filter = '';
        $filter .= 'sku='.$sku;
        $filter .= '&state=25';
        $filter = base64_encode($filter);
        $url = Mage::helper('adminhtml')->getUrl('adminhtml/inventory_item/index', array('filter'=>$filter));
        return '<a href="'.$url.'">'.$hold.'</a>';
    }
}