<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_ManufactureQuantity extends OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $sku =  $row->getData('sku');
        $results = Mage::helper('opentechiz_production')->queryAllProduction($sku);
        $numberOfManufacturing = $row->getData('flat_qty_in_manufacture');
        $personalizedIds = [];
        foreach ($results as $result){
            array_push($personalizedIds, $result['personalized_id']);
        }
        $row->setManufactureId($personalizedIds);
        $html = $numberOfManufacturing. ' item(s) in Manufacturing <br>';

        if($row->getAllProductStatus()){
            $row->setAllProductStatus($row->getAllProductStatus().$html);
        }else {
            $row->setAllProductStatus($html);
        }
        echo $numberOfManufacturing;
    }
}