<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_AllProductStatus extends OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $arrayStatusToExclude = [
            OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL,
            OpenTechiz_Purchase_Helper_Data::PO_STATUS_CLOSED,
            OpenTechiz_Purchase_Helper_Data::PO_STATUS_DRAFTED,
        ];
        $steps = OpenTechiz_Production_Helper_Data::PROCESS_LIST;
        $countRepair = [];
        foreach ($steps as $key => $value){
            $countRepair[$key] = 0;
        }

        $sku = $row->getData('sku');

        $poIncrementIdString = $row->getPurchaseOrderId();
        $poIncrementIds = explode(',', $poIncrementIdString);

        $collection = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()
            ->join(array('po' => 'opentechiz_purchase/purchase'), 'main_table.po_id = po.po_id', '*')
            ->addFieldToFilter('po_increment_id', array('in' => $poIncrementIds))
            ->addFieldToFilter('status', array('nin' => $arrayStatusToExclude))
            ->addFieldToFilter('sku', $row->getSku());
        $total = 0;
        foreach ($collection as $item){
            $total += ($item->getQty() - $item->getDeliveredQty());
        }

        $html = $total. ' item(s) in Order from Vendor <br>';
        $html .= $row->getAllProductStatus();
        $html .= $row->getData('on_hold').' item(s) in Stock Requested <br>';
        $itemInRepairCollection = Mage::getModel('opentechiz_inventory/item')->getCollection()
            ->addFieldToFilter('sku', $sku)->addFieldToFilter('state', 1);
        $repairBarcodes = [];
        foreach ($itemInRepairCollection as $repair){
            array_push($repairBarcodes, $repair->getBarcode());
        }

        $itemInRepairCollection = Mage::getModel('opentechiz_production/product')->getCollection()
            ->addFieldToFilter('barcode', array('in' => $repairBarcodes));

        foreach ($itemInRepairCollection as $repair){
            $currentStep = $repair->getStatus();
            $countRepair[$currentStep] = $countRepair[$currentStep] + 1;
        }

        $numberOfItemInRepair = count($itemInRepairCollection);
        $html .= $numberOfItemInRepair.' item(s) in Repair <br>';
        foreach ($countRepair as $key => $value) {
            if($value > 0)
                $html .= '<span style="white-space: pre;">    '.OpenTechiz_Production_Helper_Data::PROCESS_LIST[$key].': '.$value.'</span><br>';
        }

        echo $html;
    }
}