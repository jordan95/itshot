<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_PurchaseOrderId extends OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $arrayStatusToExclude = [
            OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL,
            OpenTechiz_Purchase_Helper_Data::PO_STATUS_CLOSED,
            OpenTechiz_Purchase_Helper_Data::PO_STATUS_DRAFTED,
        ];
        $sku = $row->getData('sku');
        $link = '';
        $availablePO = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()
            ->addFieldToFilter('sku', $sku)
            ->addFieldToFilter('qty', array ('gt' => new Zend_Db_Expr('delivered_qty') ))
            ->join(array('po' => 'opentechiz_purchase/purchase'), 'main_table.po_id = po.po_id', '*')
            ->addFieldToFilter('status', array('nin' => $arrayStatusToExclude))
            ->join(array('sup' => 'opentechiz_purchase/supplier'), 'po.supplier_id = sup.supplier_id', '*')
            ->setOrder('po.po_id', 'DESC');
        foreach ($availablePO as $po){
            $explode = str_split( $po->getName(), 1);
            $first3Letter = '';
            $count = 0;
            foreach ($explode as $char){
                if($char != ' '){
                    $count++;
                }
                if($count > 3) break;
                $first3Letter .= $char;
            }

            $link .= '<a href="' . Mage::helper('adminhtml')->getUrl('adminhtml/purchase_product/edit/', array('id' => $po->getPoId())) . '">' . $po->getPoIncrementId() . '</a>';
            $qty = '('.$po->getDeliveredQty().'/'.$po->getQty().') '.$first3Letter.',';
            $link .= ' '.$qty;


            $date = Mage::helper('core')->formatDate($po->getCreatedAt());
            $link .= '<br>';
            $link .= ' Ordered: '.$date.',';
            $date = Mage::helper('core')->formatDate($po->getDeliveryDate());
            $link .= '<br>';
            $link .= ' Delivery: '.$date;
            $link .= '<br>';
        }
        return $link;
    }
}