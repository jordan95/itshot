<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_InstockQuantity extends OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $qty =  $row->getData($this->getColumn()->getIndex());
        $sku = $row->getData('sku');
        $filter = '';
        $filter .= 'sku='.$sku;
        $filter .= '&state=35';

        $reorderPoint = $row->getData('reorder_point');
        $onHold = $row->getData('on_hold');

        if($qty-$onHold <= $reorderPoint ) $color = 'red';
        else $color  = 'green';

        $filter = base64_encode($filter);
        $url = Mage::helper('adminhtml')->getUrl('adminhtml/inventory_item/index', array('filter'=>$filter));
        return '<a href="'.$url.'"><span style ="color: '.$color.'">'.$qty.'</span></a>';
    }
}