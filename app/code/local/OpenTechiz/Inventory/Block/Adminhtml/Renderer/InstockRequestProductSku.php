<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_InstockRequestProductSku extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $instockProductId =  $row->getData('instock_product_id');
        $model = Mage::getModel('opentechiz_inventory/instock')->load($instockProductId);

        return $model->getSku();
    }
}