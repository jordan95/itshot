<?php
class OpenTechiz_Inventory_Block_Adminhtml_Renderer_Inline extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Input
{
    public function render(Varien_Object $row)
    {
        $html  = '<input type="text" name="reorder_point" id="input'. $row->getId() .'" value="'.$row->getReorderPoint().'" class="input-text ">';
        $html .= '<img src="'.Mage::getBaseUrl('skin').'adminhtml/default/default/opentechiz/image/tick.png" onclick="changeReorderPoint('. $row->getId() .')" width=15px height=15px style=" margin-top:5px">';

        $html .= '<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
                  <script type="text/javascript">jQuery.noConflict();</script>
                  <script>
                        function changeReorderPoint(id){
                            var stock_id = id;
                            var rop = document.getElementById("input"+id).value;
                
                            jQuery.ajax({
                                url: "'. Mage::helper('adminhtml')->getUrl('adminhtml/instockProduct/inlineEdit') .'",
                                type: \'post\',
                                dataType: \'json\',
                                data: { stock_id: stock_id, rop: rop },
                                beforeSend:function() {
                                    Element.show(\'loading-mask\');
                                },
                                complete:function() {
                                    Element.hide(\'loading-mask\');
                                },
                                success: function(transport){
                                    var data = transport;
                                    if(data[\'error\'] == true){
                                        alert("Something went wrong");
                                    } else {
                                        alert("Updated");
                                    }
                                }
                            })
                        }
                </script>';
        return $html;
    }

    public function renderHeader()
    {
        if (false !== $this->getColumn()->getGrid()->getSortable() && false !== $this->getColumn()->getSortable()) {
            $className = 'not-sort';
            $dir = strtolower($this->getColumn()->getDir());
            $nDir= ($dir=='desc') ? 'asc' : 'desc';
            if ($this->getColumn()->getDir()) {
                $className = 'sort-arrow-' . $dir;
            }
            $out = '<a href="#" name="' . $this->getColumn()->getId() . '" title="' . $nDir
                . '" class="' . $className . '"><span class="sort-title">'
                . $this->escapeHtml($this->getColumn()->getHeader()) . '</span></a>';
        } else {
            $out = $this->escapeHtml($this->getColumn()->getHeader());
        }
        return $out;
    }
}