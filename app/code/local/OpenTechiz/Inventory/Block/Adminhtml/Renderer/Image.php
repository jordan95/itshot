<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $sku = $row->getData('sku');
        if(strpos($sku, 'quotation') === false) {
            $value = $row->getData($this->getColumn()->getIndex());
            if(strpos($value, 'media/personalized') === false) {
                $value = Mage::helper('opentechiz_salesextend')->getImageByColor(null, $sku, null);
            }
        } else {
            $quotationProductId = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $row->getSku())[1];
            $productImages = Mage::getModel('opentechiz_quotation/product')->load($quotationProductId)->getQpImage();

            $value = explode(',',$productImages)[0];
        }
        return '<img src="' . $value . '" width="120px" height="120px">';
    }
}