<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_ProductName extends OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $productId =  $row->getData($this->getColumn()->getIndex());
        return Mage::getModel('catalog/product')->load($productId)->getName();
    }
}