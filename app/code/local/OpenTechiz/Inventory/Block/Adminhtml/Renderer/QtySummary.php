<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_QtySummary extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $sumQty =  $row->getData('sum_qty');
        $groupSku =  $row->getData('group_sku');

        if(!$groupSku) return '';
        
        $html = '';
        $html .= $groupSku.' x '.$sumQty.'<br>';

        return $html;
    }
}