<?php

class OpenTechiz_Inventory_Block_Adminhtml_Renderer_POQuantity extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $poIncrementIdString = $row->getPurchaseOrderId();
        $poIncrementIds = explode(',', $poIncrementIdString);

        $collection = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()
            ->join(array('po' => 'opentechiz_purchase/purchase'), 'main_table.po_id = po.po_id', '*')
            ->addFieldToFilter('po_increment_id', array('in' => $poIncrementIds))
            ->addFieldToFilter('sku', $row->getSku());
        $total = 0;
        foreach ($collection as $item){
            $total += ($item->getQty() - $item->getDeliveredQty());
        }
        if($row->getAllProductStatus()){
            $row->setAllProductStatus($row->getAllProductStatus().$total. ' item(s) in Order from Vendor <br>');
        }else {
            $row->setAllProductStatus($total. ' item(s) in Order from Vendor <br>');
        }
        echo $total;
    }
}