<?php

class OpenTechiz_Inventory_Block_Adminhtml_InstockProduct extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected  $_controller = 'adminhtml_instockProduct';
    protected $_blockGroup = 'opentechiz_inventory';
    protected $_headerText = 'Inventory Management';

    public function __construct()
    {
//        $this->_addButtonLabel = Mage::helper('opentechiz_production')->__('Add items to Stock');
        parent::__construct();
        $this->_removeButton('add');
    }
}