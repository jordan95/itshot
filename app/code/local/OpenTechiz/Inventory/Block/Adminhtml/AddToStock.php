<?php

class OpenTechiz_Inventory_Block_Adminhtml_AddToStock extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_instockProduct';
    protected $_blockGroup = 'opentechiz_inventory';
    protected $_headerText = 'Add Item to Stock';

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Add items to stock');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function getFormAction(){
        return Mage::helper("adminhtml")->getUrl('adminhtml/instockProduct/addStock/');
    }

    public function getAjaxAction(){
        return $this->getUrl('adminhtml/instockProduct/ajaxGetProduct/');
    }
}