<?php

class OpenTechiz_Inventory_Block_Adminhtml_StockMovement extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_stockMovement';
        $this->_blockGroup = 'opentechiz_inventory';
        $this->_headerText = Mage::helper('opentechiz_inventory')->__('Stock Movement');
        parent::__construct();
       
        $this->_removeButton('add');
    }
}