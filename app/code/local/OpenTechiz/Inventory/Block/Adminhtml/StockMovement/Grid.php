<?php

class OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $_countTotals = true;
    protected $_showfull = null;
    public function __construct()
    {
        parent::__construct();
        $this->setId('stockmovementGrid');
        $this->setDefaultSort('timestamp');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $locale = Mage::app()->getLocale();
        $transactionDateFromFilter = $locale->date(null, null, null, false);
        $transactionDateFromFilter->setHour(00);
        $transactionDateFromFilter->setMinute(00);
        $transactionDateFromFilter->setSecond(00);
        $transactionDateFromFilter->set(date('m/d/Y', strtotime('-2 year')));

        $this->setDefaultFilter(array(
            'timestamp' => array(
                'orig_from' => $transactionDateFromFilter,
                'from' => $transactionDateFromFilter
            )
        ));
        $params = $this->getRequest()->getParams();
        if(isset($params['mode'])){
            $this->_showfull =trim($params['mode']);
        }

    }
    public function getTemplate()
    {
        return "opentechiz/stockmovement/grid.phtml";
    }
    protected function _prepareCollection()
    {

        /* @var $collection OpenTechiz_Purchase_Model_Resource_ProductHistory_Collection */
        $collection = Mage::getModel('opentechiz_inventory/stockMovement')->getCollection()->distinct(true);
        $purchase_item =  Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/orderitem');
        $table_purchase =  Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/purchase');
        $table_supplier =  Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/supplier');
        $sales_order =  Mage::getSingleton('core/resource')->getTableName('sales/order');
        $sales_order_item =  Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $inventory =  Mage::getSingleton('core/resource')->getTableName('opentechiz_inventory/instock');
        $catalog =  Mage::getSingleton('core/resource')->getTableName('catalog/product');
        $table_return = Mage::getSingleton('core/resource')->getTableName('opentechiz_return/return');
        $collection->getSelect()->reset(Zend_Db_Select::COLUMNS)->columns('stock_movement_id')->columns('timestamp')->columns('sku_variant')->columns('order_id')->columns('qty_before_movement')->columns('on_hold_before_movement')->columns('main_table.qty as qty_movement')->columns('on_hold')->columns('return_id')->columns('user_id')->columns('type')->columns('inventory_item_id')->columns('production_request_id')->columns('product_process_request_id')->columns('instock_request_id')->columns('description')->columns('from_sku')->columns('to_sku')->columns('main_table.product_name as product_name_log')->columns('po_return_id')
                    ->joinLeft(array('purchase_item' => $purchase_item), "purchase_item.po_id = main_table.po_order_id AND purchase_item.sku = main_table.sku_variant", array('po_id','delivered_qty','price'))
                    ->joinLeft(array('purchase_order' => $table_purchase), "purchase_item.po_id = purchase_order.po_id", array('po_increment_id'))
                    ->joinLeft(array('purchase_supplier' => $table_supplier), "purchase_order.supplier_id = purchase_supplier.supplier_id", array('purchase_supplier.name'))
                    ->joinLeft(array('sales_order' => $sales_order), "sales_order.entity_id = main_table.order_id", array('sales_order.increment_id','CONCAT(customer_firstname," ",customer_lastname) AS customer_name','order_stage','internal_status'))
                    ->joinLeft(array('sales_order_item' => $sales_order_item), "sales_order.entity_id = sales_order_item.order_id AND main_table.sku_variant = sales_order_item.sku", array('order_price'=>'sales_order_item.price'))
                    ->joinLeft(array('inventory' => $inventory), "inventory.id = main_table.stock_id", array('product_id'=>'inventory.product_id','sku_inventory'=>'inventory.sku','last_cost'=>'inventory.last_cost','last_cost_date'=>'inventory.last_cost_date'))
                    ->joinLeft(array('catalog' => $catalog), "inventory.product_id = catalog.entity_id", array('product_sku'=>'catalog.sku'))
                    ->joinLeft(array('catalog_varchar' => 'tsht_catalog_product_entity_varchar'), "catalog.entity_id = catalog_varchar.entity_id AND attribute_id = 71 AND catalog_varchar.store_id = 0", array('product_name'=>'catalog_varchar.value'))
                    ->joinLeft(array('return' => $table_return), "return.return_id = main_table.return_id", array('return_increment_id'=>'return.increment_id'));
        if($this->_showfull !=="full"){
             $collection->getSelect()->where("main_table.qty > 0 OR main_table.qty < 0");
        }      
        $select = $collection->getSelect();
        $filter = $this->getParam($this->getVarNameFilter(), null);
        if (is_null($filter) && $this->_showfull !=="full") {
            $select->where("false");
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }
    public function getEmptyText()
    {
        $filter = $this->getParam($this->getVarNameFilter(), null);
        if (is_null($filter)) {
            $text = " Please enter at least 1 filter and click Search to view results.";
        } else {
            $text = parent::getEmptyText();
        }
        return $text;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('stock_movement_id', array(
            'header' => Mage::helper('opentechiz_production')->__('ID'),
            'index' => 'stock_movement_id',

            'width'=>'20'
        ));
        $this->addColumn('timestamp', array(
            'header' => Mage::helper('opentechiz_production')->__('Transaction Date'),
            'index' => 'timestamp',
            'format'    => 'F',
            'type' => 'datetime'
        ));
        $this->addColumn('sku_variant', array(
            'header' => Mage::helper('opentechiz_production')->__('Sku'),
            'index' => 'sku_variant',
            'filter_index' => 'sku_variant',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_Sku',
        ));
        $this->addColumn('product_name', array(
            'header' => Mage::helper('opentechiz_production')->__('Product Name'),
            'index' => 'product_name',
            'filter_index' => 'catalog_varchar.value',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_ProductName'
        ));
         $this->addColumn('product_option', array(
            'header' => Mage::helper('opentechiz_production')->__('Product Options'),
            'index' => 'product_option',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_Option',
            'sortable' => false,
            'filter'    => false
        ));
        $this->addColumn('qty_before_movement', array(
            'header' => Mage::helper('opentechiz_production')->__('Qty +/-'),
            'index' => 'qty_before_movement',
            'filter_index' => 'qty_before_movement',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_Qty',
            'sortable' => false,
            'filter'    => false,
            'width'=> "100"
        ));
        $this->addColumn('on_hold', array(
            'header' => Mage::helper('opentechiz_production')->__('On-hold +/-'),
            'index' => 'on_hold',
            'filter_index' => 'on_hold',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_OnholdQty',
            'sortable' => false,
            'filter'    => false,
            'width'=> "100"
        ));
        $this->addColumn('description', array(
            'header' => Mage::helper('opentechiz_production')->__('Note'),
            'index' => 'description',
            'sortable' => false,
            'filter'    => false,
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_Note',
        ));

        $this->addColumn('po_increment_id', array(
            'header' => Mage::helper('opentechiz_production')->__('PO #'),
            'index' => 'po_increment_id',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_Purchase',
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('opentechiz_production')->__('Vendor'),
            'index' => 'name',  
            'filter_index' => 'purchase_supplier.name'
        ));
        $this->addColumn('delivered_qty', array(
            'header' => Mage::helper('opentechiz_production')->__('Qty Purchased'),
            'index' => 'delivered_qty',
            'filter_index' => 'purchase_item.delivered_qty'
        ));
        
        $this->addColumn('price', array(
            'header' => Mage::helper('opentechiz_production')->__('Price Paid'),
            'index' => 'price',  
            'filter_index' => 'purchase_item.price',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_FormatPrice'
        ));
        $this->addColumn('return_id', array(
            'header' => Mage::helper('opentechiz_production')->__('Return Number'),
            'index' => 'return_increment_id',  
            'filter_index' => 'return.increment_id',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_Return',
        ));
        $this->addColumn('customer_name', array(
            'header' => Mage::helper('opentechiz_production')->__('Customer Name'),
            'index' => 'customer_name',  
            'filter_index' => 'customer_name',
            'sortable' => false,
            'filter'    => false,
            'width'  =>'130'
        ));
        $this->addColumn('increment_id', array(
            'header' => Mage::helper('opentechiz_production')->__('Order Number'),
            'width'  =>'100',
            'index' => 'increment_id',  
            'filter_index' => 'sales_order.increment_id',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_Order',
        ));
        $this->addColumn('order_price', array(
            'header' => Mage::helper('opentechiz_production')->__('Order Item Price'),
            'index' => 'order_price',
            'filter_index' => 'sales_order_item.price',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_FormatPrice',
        ));
        $this->addColumn('user_id', array(
            'header' => Mage::helper('opentechiz_production')->__('User Name'),
            'index' => 'user_id',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_User',
            'sortable' => false,
            'filter'    => false
        ));
        return parent::_prepareColumns();
    }
    public function getTotals()
    {
        $totals = new Varien_Object();
        if (is_null(Mage::registry('product_history_totals'))) {
            Mage::register('product_history_totals', $this->getCollection()->getTotals());
        }

        $data = Mage::registry('product_history_totals');
        $fields = array(
            "qty_before_movement" => '',
            "delivered_qty" => $data['sum_delivered_qty'],
            "sum_price_paid" => $data['sum_price_paid'],
            "order_price" => $data['sum_item_price'],
        );
        //First column in the grid
        $fields['name'] = 'Totals';
        $totals->setData($fields);
        return $totals;
    }
    public function getTotalsGroupType()
    {
        $totals = new Varien_Object();
        $sum_qty = 0;
        $data = $this->getCollection()->getTotalsGroupType();
        if (is_array($data) || is_object($data)){
            foreach($data as $k => $val){
                if((int)$val['type'] == 11 || (int)$val['type'] == 12){
                    $sum_qty += $val['qty'];
                }
                if((int)$val['type'] == 5 || (int)$val['type'] == 9 || (int)$val['type'] == 14 || (int)$val['type'] == 17 || (int)$val['type'] == 18 ||(int)$val['type'] == 19 ||(int)$val['type'] == 21 ||(int)$val['type'] == 22){
                    $sum_qty += $val['qty'];
                }
            }
        }
        if($sum_qty < 0){
            $sum_qty = abs($sum_qty);
        }else{
            $sum_qty = 0;
        }
        $fields = array(
            "qty_sold" => $sum_qty
        );
        $totals->setData($fields);
        return $totals;
    }
    protected function _setCollectionOrder($column)
    {
        $collection = $this->getCollection();
        if ($collection) {
            $columnIndex = $column->getFilterIndex() ?
                $column->getFilterIndex() : $column->getIndex();
            $collection->setOrder($columnIndex, strtoupper($column->getDir()));
            if($columnIndex !=="stock_movement_id"){
                $collection->setOrder('stock_movement_id','DESC');
            }
            
        }
        return $this;
    }
    protected function getLastCost()
    {
        $sku = $this->getSkuFilter();
        $data = $this->getCollection()->getLastCost($sku);
        return $data;
    }
    protected function getLowestCost()
    {   $sku = $this->getSkuFilter();
        $data = $this->getCollection()->getLowestCost($sku);
        return $data;
    }
    public function getSkuFilter(){
        $filter   = $this->getParam($this->getVarNameFilter(), null);
        $data = $this->helper('adminhtml')->prepareFilterString($filter);
        if(isset($data['sku_variant'])){
            return $data['sku_variant'];
        }
        return false;
    }
    public function getQtySummary(){
        $sku = $this->getSkuFilter();
        $data = $this->getCollection()->getQtySummary($sku);
        return $data;
    }
    public function getQtyOpenPurchase(){
        $sku = $this->getSkuFilter();
        $data = $this->getCollection()->getQtyOpenPurchase($sku);
        return $data;
    }
    public function getQtyManufacturing(){
        $sku = $this->getSkuFilter();
        $data = $this->getCollection()->getQtyManufacturing($sku);
        return $data;
    }
}