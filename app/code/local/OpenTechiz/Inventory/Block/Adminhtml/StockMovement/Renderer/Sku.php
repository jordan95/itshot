<?php

class OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_Sku extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $url = Mage::getModel("core/url")->getUrl("adminhtml/instockProduct/index", array(
            "filter" => base64_encode(http_build_query(array(
                                'sku' => $row['sku_variant']
        )))));
        return "<a href=\"{$url}\" target='_blank'>{$row['sku_variant']}</a>";
    }

}
