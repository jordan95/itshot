<?php

class OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_ProductName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $product_name = $row->getData('product_name_log');
        $value = $row->getData($this->getColumn()->getIndex());
        if($product_name !=''){
            return $product_name;
        }else{
            if($value ==''){
                $sku_variant = $row->getData('sku_variant');
                $sku = explode("_",$sku_variant);
                $product  = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku[0]);
                if($product){
                    return $product->getName();
                }
                return '';
                
            }else{
                return $value;
            }
            
        }
    }

}
