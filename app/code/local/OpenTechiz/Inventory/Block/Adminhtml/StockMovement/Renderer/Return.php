<?php

class OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_Return extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        $return_id = $row->getData('return_id');
        if ($value) {
            $url = Mage::getModel("core/url")->getUrl("adminhtml/sales_returns/edit", array("id" => $return_id));
            return "<a href=\"{$url}\" target=\"_blank\">{$value}</a>";
        }
        return "";
    }

}
