<?php

class OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_Order extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $order_id = $row->getData('order_id');
        $value = $row->getData($this->getColumn()->getIndex());
        $internal_status ='';
        $order_stage ='';
        
        if ($value) {
            if(!is_null($row->getData('order_stage'))){
                if(isset(OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE[$row->getData('order_stage')])){
                    $order_stage = OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE[$row->getData('order_stage')];
                }
            }
            if(!is_null($row->getData('internal_status'))){
                if(isset(OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS[$row->getData('internal_status')])){
                    $internal_status = OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS[$row->getData('internal_status')];
                }
            }
            
            $url = Mage::getModel("core/url")->getUrl("adminhtml/sales_order/view", array("order_id" => $order_id));
            return "<a href=\"{$url}\" target=\"_blank\">#{$value} </a> <p>{$order_stage} &#8594; {$internal_status}</p>";
        }
        return "";
    }

}
