<?php

class OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_Qty extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        $qty_before =  $row->getData('qty_before_movement');
        $qty_moved =  $row->getData('qty_movement');
        $qty_after = 0;
        if($value!==""){
            if ($qty_moved > 0) {

                $qty_after =$qty_before + $qty_moved;
                if($qty_before==""){
                    return "<span style='color:green'> +".$qty_moved."</span>"; 
                }else{
                    return "<span><span style='color:green'> +".$qty_moved."</span> (".$qty_before." &#8680; ".$qty_after.")"."</span>"; 
                }
                
            }elseif($qty_moved < 0){
                $qty_after =$qty_before + $qty_moved;
                if($qty_before==""){
                    return "<span style='color:red'>".$qty_moved."</span>"; 
                }else{
                    return "<span><span style='color:red'>".$qty_moved."</span> (".$qty_before." &#8680; ".$qty_after.")"."</span>"; 
                }
                
            }else{
                $qty_moved = 0;
                $qty_after = $qty_before;
                if($qty_before==""){
                    return "<span>".$qty_moved."</span>"; 
                }else{
                    return "<span><span>".$qty_moved."</span> (".$qty_before." &#8680; ".$qty_after.")"."</span>"; 
                }
                
            }
        }
        return '';

    }

}
