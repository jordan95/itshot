<?php

class OpenTechiz_Inventory_Block_Adminhtml_StockMovement_Renderer_Note extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $data =  $row->getData();
        $note = $row->getData('description');
        if(empty($note)){
        	$message = Mage::helper("opentechiz_inventory")->getMessage($data);
        	return $message;
        }elseif(!empty($note)){
        	return $note;
        }
        

    }

}
