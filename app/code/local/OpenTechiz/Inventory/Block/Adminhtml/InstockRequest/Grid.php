<?php

class OpenTechiz_Inventory_Block_Adminhtml_InstockRequest_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('requestGrid');
        $this->setDefaultSort('request_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);

    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_inventory/inventory_instockRequest')->getCollection()
            ->join(array('instock' => 'opentechiz_inventory/instock'), 'main_table.instock_product_id = instock.id',
                array('sku' => 'instock.sku'));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('request_id', array(
            'header' => Mage::helper('opentechiz_inventory')->__('ID #'),
            'index' => 'request_id',
            'width' => '80',
            'align' => 'center'
        ));
        $this->addColumn('image', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Thumbnail'),
            'index' => 'instock_product_id',
            'width' => '120',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Image'
        ));
        $this->addColumn('order_item_id', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Order Item #'),
            'index' => 'order_item_id',
            'width' => '100',
            'renderer' => 'OpenTechiz_Production_Block_Adminhtml_Renderer_OrderItemId'
        ));
         $this->addColumn('return_id', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Return #'),
            'index' => 'return_id',
            'width' => '100',
             'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_ReturnId'
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Name'),
            'index' => 'instock_product_id',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_InstockRequestProductName'
        ));
        $this->addColumn('sku', array(
            'header' => Mage::helper('opentechiz_inventory')->__('SKU'),
            'index' => 'sku',
//            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_InstockRequestProductSku'
        ));
        $this->addColumn('barcode', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Requested Barcode'),
            'index' => 'barcodes',
            'width' => '200'
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Status'),
            'align' => 'left',
            'width' => '100px',
            'index' => 'status',
            'type' => 'options',
            'options' => OpenTechiz_Inventory_Helper_Data::INSTOCK_REQUEST_STATUS,
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_InstockRequestStatus'
        ));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('request_id');
        $this->getMassactionBlock()->setFormFieldName('instock_request_status');
        $this->getMassactionBlock()->setUseSelectAll(false);

        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('opentechiz_inventory')->__('Take Items/Remove Reservations'),
            'url' => $this->getUrl('*/*/massStatus'),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('opentechiz_inventory')->__('Status'),
                    'values' => [1 => 'Take Items From Stock']
                )
            )
        ));

        return $this;
    }
}