<?php

class OpenTechiz_Inventory_Block_Adminhtml_InstockRequest extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_instockRequest';
        $this->_blockGroup = 'opentechiz_inventory';
        $this->_headerText = Mage::helper('opentechiz_inventory')->__('Stock Requests');
        parent::__construct();
        $this->_removeButton('add');
    }
}