<?php

class OpenTechiz_Inventory_Block_Adminhtml_InstockProduct_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('instockGrid');
        $this->setDefaultSort('qty');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_inventory/instock')->getCollection();
        $purchase = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/purchase');
        $order_item = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/orderitem');
        $productionRequest = Mage::getSingleton('core/resource')->getTableName('opentechiz_production/product');
        $table = new Zend_Db_Expr("(SELECT GROUP_CONCAT(po.po_increment_id) as 'purchase_order_id' ,GROUP_CONCAT(order_item.`delivered_qty`,'/',order_item.`qty`) as delivery_qty,max(po.delivery_date) as delivery_date ,sku as order_sku,order_item.qty as qty_po,order_item.delivered_qty as delivered_qty_po FROM " . $order_item . " as order_item join " . $purchase . "  as po on po.po_id = order_item.po_id and po.status NOT IN(1,3,4) GROUP BY sku)");
        $manu_table = new Zend_Db_Expr("(SELECT sku as sku2, GROUP_CONCAT(`personalized_id`) as manufacture_id FROM " . $productionRequest . " JOIN tsht_inventory_item on tsht_inventory_item.barcode = tsht_request_product.barcode and state = 0 GROUP BY sku2)");
        $summaryQtyTable = new Zend_Db_expr("(SELECT group_sku, sum(qty) as sum_qty FROM `tsht_inventory_instock_product` GROUP BY group_sku)");
        $collection->getSelect()
            ->joinLeft(array('purchase_order' => $table), "main_table.sku = purchase_order.order_sku", array('*'))
            ->joinLeft(array('manufacture_product' => $manu_table),
                'main_table.sku = manufacture_product.sku2',
                array('*'))
            ->joinLeft(array('qty' => $summaryQtyTable), 'main_table.group_sku = qty.group_sku', array('sum_qty', 'group_sku' => 'main_table.group_sku'));
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('opentechiz_inventory')->__('ID #'),
            'index' => 'id',
            'width' => '35px',
            'align' => 'center'
        ));
        $this->addColumn('gallery_image', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Gallery Image'),
            'index' => 'id',
            'width' => '35px',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_ImageFromSku'
        ));
        $this->addColumn('product_id', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Product Name'),
            'type' => 'text',
            'index' => 'product_id',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_ProductName',
            'filter_condition_callback' => array($this, '_productNameFilter')
//            'filter' => false
        ));
        $this->addColumn('options', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Options Information'),
            'index' => 'sku',
            'width' => '300',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_OptionsFromSku',
            'filter' => false
        ));
        $this->addColumn('tag_sku', array(
            'header' => Mage::helper('opentechiz_inventory')->__('TagSKU'),
            'index' => 'tag_sku',
            'filter_index' => 'main_table.tag_sku',
            'width' => '100',
        ));
        $this->addColumn('sku', array(
            'header' => Mage::helper('opentechiz_inventory')->__('SKU'),
            'index' => 'sku',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_ProductHistoryLink',
            'filter_index' => 'main_table.sku',
            'width' => '100',
        ));
        $this->addColumn('group_sku', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Group SKU'),
            'index' => 'group_sku',
            'filter_index' => 'main_table.group_sku',
            'width' => '100',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract'
        ));
        $this->addColumn('qty', array(
            'header' => Mage::helper('opentechiz_inventory')->__('On-hand QTY'),
            'index' => 'qty',
            'width' => '10',
            'filter' => false,
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_InstockQuantity'
        ));
        $this->addColumn('on_hold', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Reserved'),
            'index' => 'on_hold',
            'filter' => false,
            'width' => '50',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_ReservedQuantity'
        ));
        $this->addColumn('order_number', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Order #').str_repeat('&nbsp;', 30),
            'index' => 'sku',
            'filter' => false,
            'sortable' => false,
            'width' => '300px',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_OrderNumber'
        ));
        $this->addColumn('need_to_order', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Need to Order'),
//            'filter' => false,
//            'sortable' => false,
            'sort_index' => 'flat_need_to_order',
            'index' => 'flat_need_to_order',
            'width' => '350px',
            'type'      => 'options',
            'options'   => array(
                0 => 'No',
                1 => 'Yes'
            ),
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_Abstract'
//            'filter_condition_callback' => array($this, '_needToOrderFilter')
        ));
        $this->addColumn('purchase_order_id', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Open Purchase Order #'),
            'index' => 'purchase_order_id',
            'filter' => false,
            'sort_index' => 'flat_qty_in_open_po',
            'width' => '300',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_PurchaseOrderId'
        ));
        $this->addColumn('delivery_date', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Last Delivered PO'),
            'index' => 'delivery_date',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_InstockDelivery',
            'width' => '198',
            'filter' => false,
            'sortable' => false,
        ));
        $this->addColumn('manu_qty', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Manufacturing QTY'),
            'width' => '50',
            'filter' => false,
            'index' => 'flat_qty_in_manufacture',
            'sort_index' => 'flat_qty_in_manufacture',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_ManufactureQuantity'
        ));
        $this->addColumn('manufacture_id', array(
            'header'    => Mage::helper('opentechiz_inventory')->__('Manufacture #'),
            'width'     => '100',
            'filter' => false,
            'sortable' => false,
            'index'     => 'manufacture_id',
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_ManufactureId'
        ));
        $this->addColumn('all_product_status', array(
            'header'    => Mage::helper('opentechiz_inventory')->__('Items Status'),
            'width'     => '300',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_AllProductStatus'
        ));

        if(Mage::getSingleton('admin/session')->isAllowed('admin/erp/inventory/instock/inventory_reorder')) {
            $this->addColumn('reorder_point', array(
                'header' => Mage::helper('opentechiz_inventory')->__('Reorder Point'),
                'index' => 'reorder_point',
                'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_Inline',
                'width' => '50'
            ));
        }else{
            $this->addColumn('reorder_point', array(
                'header' => Mage::helper('opentechiz_inventory')->__('Reorder Point'),
                'index' => 'reorder_point',
                'width' => '50'
            ));
        }


        $this->addColumn('status', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Status'),
            'index' => 'qty',
            'width' => '75',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_InstockStatusDisplay'
        ));

        if(Mage::getSingleton('admin/session')->isAllowed('admin/erp/inventory/instock/inventory_action')) {
            $this->addColumn('action',
                array(
                    'header' => Mage::helper('opentechiz_inventory')->__('Action'),
                    'width' => '50',
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('opentechiz_inventory')->__('Edit Stock'),
                            'url' => array('base' => '*/*/editStock'),
                            'field' => 'id',
                            'data-column' => 'action',
                        ),
                        array(
                            'caption' => Mage::helper('opentechiz_inventory')->__('Print Appraisal'),
                            'url' => array('base' => '*/*/printAppraisal'),
                            'field' => 'id',
                            'data-column' => 'action',
                        ),
                        array(
                            'caption' => Mage::helper('opentechiz_inventory')->__('Confirm Reservation'),
                            'url' => array('base' => '*/*/confirmReservation'),
                            'field' => 'id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'is_system' => true,
                    'align' => 'center',
                ));
        }
        return parent::_prepareColumns();
    }

    protected function _needToOrderFilter($collection, $column)
    {
        $needToOrder = $column->getFilter()->getValue();

        $orderItemCollection = Mage::getModel('sales/order_item')->getCollection()->join(
            array('order' => 'sales/order'),
            'order.entity_id = main_table.order_id AND order.internal_status IN (3, 21)',
            '*'
        );

        $stockProductSkuNeedOrder = [];
        foreach ($orderItemCollection as $orderItem){
            array_push($stockProductSkuNeedOrder, $orderItem->getSku());
        }

        if($needToOrder) {
            $collection->addFieldToFilter('main_table.sku', array('in' => $stockProductSkuNeedOrder));
        }else{
            $collection->addFieldToFilter('main_table.sku', array('nin' => $stockProductSkuNeedOrder));
        }

        return $this;
    }
    public function getTemplate()
    {
        return "opentechiz/inventory/grid.phtml";
    }
    public function getQtySummary(){
        $sku = $this->getSkuFilter();
        $tagSku = $this->getTagSkuFilter();
        return $this->getCollection()->getQtySummary($sku, $tagSku);
    }
    public function getQtyOpenPurchase(){
        $sku = $this->getSkuFilter();
        $tagSku = $this->getTagSkuFilter();
        return $this->getCollection()->getQtyOpenPurchase($sku, $tagSku);
    }
    public function getQtyManufacturing(){
        $sku = $this->getSkuFilter();
        $tagSku = $this->getTagSkuFilter();
        return $this->getCollection()->getQtyManufacturing($sku, $tagSku);
    }
    public function getProductNote(){
        $sku = $this->getSkuFilter();
        $baseSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
        if (!$baseSku) {
            $tagSku = $this->getTagSkuFilter();
            $baseSku = explode('/', $tagSku)[0];
        }
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $baseSku);
        return $product ? $product->getProductNote() : '';
    }
    public function getQtyInOpenOrder()
    {
        $sku = $this->getSkuFilter();
        $baseSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
        $tagSku = false;
        if (!$baseSku) {
            $tagSku = $this->getTagSkuFilter();
            $baseSku = explode('/', $tagSku)[0];
        }
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $baseSku);
        if ($product && $product->getId()) {
            $orderItems = Mage::getModel('sales/order_item')->getCollection()
                ->join(array('order' => 'sales/order'), 'main_table.order_id = order.entity_id')
                ->join(array('instock' => 'opentechiz_inventory/instock'), 'main_table.sku = instock.sku')
                ->addFieldToFilter('order_stage', array('in' => array(0, 1, 2, 3, 4)));
            if ($tagSku) {
                $orderItems->addFieldToFilter('instock.tag_sku', array('like' => '%' . $tagSku . '%'));
            } else {
                $orderItems->addFieldToFilter('instock.sku', array('like' => '%' . $sku . '%'));
            }

            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterActiveOrder($orderItems);

            $qtyArray = [];
            foreach ($orderItems as $orderItem) {
                $sku = $orderItem->getSku();
                $qty = $orderItem->getQtyOrdered() - $orderItem->getQtyCanceled() - $orderItem->getQtyRefunded();
                if (!isset($qtyArray[$sku])) {
                    $qtyArray[$sku] = $qty;
                } else {
                    $qtyArray[$sku] += $qty;
                }
            }

            $string = '';
            foreach ($qtyArray as $sku => $qty) {
                $string .= $sku . ' x ' . $qty . ', ';
            }
            $string = trim($string, ', ');
            return $string;
        } else {
            return '';
        }
    }

    protected function  _productNameFilter($collection, $column){
        $value = $column->getFilter()->getValue();
        $collectionProduct = Mage::getModel('catalog/product')->getCollection();
        $collectionProduct->addAttributeToFilter('name', array('like' => '%' .$value. '%'));

        $product_ids = [];
        foreach ($collectionProduct as $product) {
            $product_ids[] = $product->getId();
        }

        $collection->addFieldToFilter('main_table.product_id', array('in' => $product_ids));
        return $this;
    }

    public function getSkuFilter(){
        $filter   = $this->getParam($this->getVarNameFilter(), null);
        $data = $this->helper('adminhtml')->prepareFilterString($filter);
        if(isset($data['sku'])){
            return $data['sku'];
        }
        if(isset($data['group_sku'])){
            $sku = explode('_', $data['group_sku']);
            return $sku[0];
        }
        return false;
    }

    public function getTagSkuFilter() {
        $filter = $this->getParam($this->getVarNameFilter());
        $data = $this->helper('adminhtml')->prepareFilterString($filter);
        if (isset($data['tag_sku'])) {
            return $data['tag_sku'];
        }
        return false;
    }

    public function getLastRefreshDataDate(){
        $coreFlagCode = 'inventory_instock_refresh_date';
        $flag = Mage::getModel('core/flag', array('flag_code' => $coreFlagCode))->loadSelf();

        if($flag && $flag->getFlagData()){
            $last = $flag->getFlagData();
        }else{
            $last = Mage::getModel('core/date')->gmtDate('Y-m-d').' 00:00:00';
        }

        return Mage::helper('core')->formatDate($last, 'medium', true);
    }
}