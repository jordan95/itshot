<?php

class OpenTechiz_Inventory_Block_Adminhtml_InstockProduct_Edit extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_inventory_productEdit';
    protected $_blockGroup = 'opentechiz_inventory';
    protected $_headerText = 'Edit Stock';
    protected $_currentStock = null;

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Edit Stock');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function _toHtml()
    {
        $this->setTemplate('opentechiz/inventory/stock_edit.phtml');

        return parent::_toHtml();
    }

    public function getFormAction(){
        return $this->getUrl('adminhtml/instockProduct/editStockSave');
    }

    public function getCurrentStockEntity(){
        if($this->_currentStock == null) {
            $params = $this->getRequest()->getParams();
            $stockId = $params['id'];
            return Mage::getModel('opentechiz_inventory/instock')->load($stockId);
        }else{
            return $this->_currentStock;
        }
    }

    public function getHelper(){
        return Mage::helper('opentechiz_inventory');
    }
    
}