<?php

class OpenTechiz_Inventory_Block_Adminhtml_Inventory_Item_Certificate extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'opentechiz_purchase';
        $this->_controller = 'adminhtml_product';
        parent::__construct();
         $this->_updateButton('save', 'label', Mage::helper('opentechiz_purchase')->__('Print Appraisal'));
        
        $this->removeButton('reset');

        $this->removeButton('add');
        $this->removeButton('delete');
        $this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('supplier_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'supplier_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'supplier_content');
			}

			
		";
        $this->_updateButton('save', 'onclick', 'var retailPrice = document.getElementById(\'retail_price\').value;
            if(retailPrice.substring(0, 1) == \'$\'){
            retailPrice = retailPrice.substr(1);
            }
            if(!retailPrice || Number(retailPrice) == 0)
            {
            if (!window.confirm(\''. Mage::helper('salesrule')->__('Do you want to remove Retail Value?') .' \')) 
            {
            return false;
            }else{
            editForm.submit();
            }
            }else{
            editForm.submit();
            }');
    }

    public function getHeaderText()
    {
        if( Mage::registry('item') && Mage::registry('item')->getId()) 
            return Mage::helper('opentechiz_inventory')->__("Item %s", Mage::registry('item')->getId());
       
    }

    public function getSaveUrl()
    {
        return $this->getUrl('adminhtml/inventory_item/print');
    }
}