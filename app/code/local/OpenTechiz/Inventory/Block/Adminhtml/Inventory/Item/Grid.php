<?php

class OpenTechiz_Inventory_Block_Adminhtml_Inventory_Item_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('itemGrid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setNoFilterMassactionColumn(true);
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_inventory/item')->getCollection();
        $collection->getSelect()
            ->joinLeft( array('po_item'=> 'tsht_purchase_purchase_order_item'), 'po_item.item_id = main_table.item_id', array('item_id' => 'main_table.item_id'))
            ->joinLeft( array('po'=> 'tsht_purchase_order'), 'po_item.po_id = po.po_id', array('po_increment_id', 'po_id' => 'po.po_id'))
            ->joinLeft( array('inv_order'=> 'tsht_inventory_order_item'), 'inv_order.item_id = main_table.item_id', 'order_item_id')
            ->joinLeft( array('order_item'=> 'tsht_sales_flat_order_item'), 'order_item.item_id = order_item_id', array('order_id', 'sku' => 'main_table.sku'))
            ->joinLeft( array('order'=> 'tsht_sales_flat_order'), 'order_item.order_id = order.entity_id', array('increment_id', 'state' => 'main_table.state'))
        ;
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('item_id', array(
            'header' => Mage::helper('opentechiz_material')->__('ID'),
            'width' => '50',
            'index' => 'item_id',
            'sort_index' => 'item_id',
            'filter_index' => 'main_table.item_id',
            'align' => 'center',
            'column_css_class'=>'no-display',
            'header_css_class'=>'no-display'
        ));
        $this->addColumn('image', array(
            'header'    => Mage::helper('opentechiz_production')->__('Gallery Image'),
            'index'     => 'image',
            'width'     => '125px',
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_Image'
        ));
        $this->addColumn('options', array(
            'header'    => Mage::helper('opentechiz_production')->__('Options Information'),
            'index'     => 'options',
            'renderer'  => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_CustomOptions'
        ));
        $this->addColumn('name', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Product Name'),
            'type'      => 'text',
            'index'     =>  'name',
            'sort_index'=> 'main_table.name',
            'filter_index'=> 'main_table.name',
        ));
        $this->addColumn('sku', array(
            'header'    => Mage::helper('opentechiz_production')->__('SKU'),
            'index'     => 'sku',
            'sort_index'=> 'main_table.sku',
            'filter_index' => 'main_table.sku',
            'width'     =>  '100',
        ));
        $this->addColumn('barcode', array(
            'header'    => Mage::helper('opentechiz_production')->__('Barcode #'),
            'index'     => 'barcode',
            'width'     =>  '50'
        ));
        $this->addColumn('order_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Order #'),
            'index'     => 'order_id',
            'width'     =>  '50',
            'sort_index'=> 'increment_id',
            'filter_index'=> 'increment_id',
            'renderer'  => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_OrderId'
        ));
        $this->addColumn('po_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Purchase Order #'),
            'index'     => 'po_increment_id',
            'width'     =>  '50',
            'sort_index'=> 'po_increment_id',
            'filter_index'=> 'po_increment_id',
            'renderer'  => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_POlink'
        ));
        $this->addColumn('created_at', array(
            'header'    => Mage::helper('opentechiz_production')->__('Created At'),
            'index'     => 'created_at',
            'type'      => 'datetime',
            'sort_index'=> 'main_table.created_at',
            'filter_index' => 'main_table.created_at',
            'width'     => '100'
        ));
        $this->addColumn('updated_at', array(
            'header'    => Mage::helper('opentechiz_production')->__('Updated At'),
            'index'     => 'updated_at',
            'type'      => 'datetime',
            'sort_index'=> 'main_table.updated_at',
            'filter_index' => 'main_table.updated_at',
            'width'     => '100'
        ));
        $this->addColumn('type', array(
            'header'    => Mage::helper('opentechiz_production')->__('Type'),
            'width'     => '50',
            'index'     => 'type',
            'type'      => 'options',
            'sort_index'=> 'main_table.type',
            'filter_index' => 'main_table.type',
            'options'   => OpenTechiz_Inventory_Helper_Data::ITEM_TYPE
        ));
        $this->addColumn('note', array(
            'header'    => Mage::helper('opentechiz_production')->__('Note'),
            'index'     => 'note',
            'width'     =>  '200',
            'filter_index'  => 'main_table.note'
        ));
        $this->addColumn('state', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Item State'),
            'index'     =>  'state',
            'type'      => 'options',
            'sort_index'=> 'main_table.state',
            'filter_index' => 'main_table.state',
            'options'   => OpenTechiz_Inventory_Helper_Data::ITEM_STATE
        ));
//        $this->addColumn('cost', array(
//            'header'    =>  Mage::helper('opentechiz_production')->__('Production/Purchase Cost'),
//            'index'     =>  'cost'
//        ));

        $this->addColumn('action',
            array(
                'header' => Mage::helper('opentechiz_material')->__('Action'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('opentechiz_material')->__('Print Appraisal'),
                        'url' => array('base' => '*/*/certifacate'),
                        'field' => 'id',
                        'data-column' => 'action',
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
                'align' => 'center',
            ));
        return parent::_prepareColumns();
    }

    /**
     * @return $this|Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('item_id');
        $this->getMassactionBlock()->setFormFieldName('item_id');
        $this->getMassactionBlock()->setUseSelectAll(false);
        $status = OpenTechiz_Inventory_Helper_Data::ITEM_SHIP_STATE ;

        // Change status
        $this->getMassactionBlock()->addItem('state', array(
            'label'=> Mage::helper('opentechiz_inventory')->__('Change state'),
            'url'  => $this->getUrl('*/*/massState', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'state',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('opentechiz_material')->__('State'),
                    'values' => $status
                )
            )
        ));

        // edit items
        $this->getMassactionBlock()->addItem('edit_items', array(
            'label'=> Mage::helper('opentechiz_inventory')->__('Edit Items'),
            'url'  => $this->getUrl('adminhtml/inventory_itemEdit/index', array('_current'=>true))
        ));

        return $this;
    }


}