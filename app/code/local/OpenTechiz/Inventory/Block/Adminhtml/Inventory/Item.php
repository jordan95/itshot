<?php

class OpenTechiz_Inventory_Block_Adminhtml_Inventory_Item extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected $_controller = '';
    protected $_blockGroup = '';
    protected $_headerText = '';

    public function __construct()
    {
        $this->_controller = 'adminhtml_inventory_item';
        $this->_blockGroup = 'opentechiz_inventory';
        $this->_headerText = Mage::helper('opentechiz_inventory')->__('All Items');
        parent::__construct();
        $this->_removeButton('add');
    }
}