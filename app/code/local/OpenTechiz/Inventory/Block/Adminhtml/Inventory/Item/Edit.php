<?php

class OpenTechiz_Inventory_Block_Adminhtml_Inventory_Item_Edit extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_inventory_itemEdit';
    protected $_blockGroup = 'opentechiz_inventory';
    protected $_headerText = 'Add Item to Stock';

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Edit Items');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function _toHtml()
    {
        $this->setTemplate('opentechiz/inventory/items_edit.phtml');

        return parent::_toHtml();
    }

    public function getFormAction(){
        return $this->getUrl('adminhtml/inventory_itemEdit/submitEditForm');
    }

    public function getAjaxAction(){
        return $this->getUrl('adminhtml/inventory_itemEdit/ajaxGetProductOption/');
    }
}