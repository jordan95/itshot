<?php

class OpenTechiz_Inventory_Block_Adminhtml_Inventory_Item_Certificate_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('certificate_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('opentechiz_purchase')->__('Appraisal'));
    }

    protected function _beforeToHtml()
    {
      
        $this->addTab('general', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Item Infomation'),
            'title'     => Mage::helper('opentechiz_purchase')->__('Item Infomation'),
            'content'   => $this->getLayout()->createBlock('opentechiz_inventory/adminhtml_inventory_item_certificate_tab_form')->toHtml(),
        ));
       
        return parent::_beforeToHtml();
    }
}