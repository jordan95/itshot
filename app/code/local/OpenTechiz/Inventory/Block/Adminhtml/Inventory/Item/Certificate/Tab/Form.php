<?php

class OpenTechiz_Inventory_Block_Adminhtml_Inventory_Item_Certificate_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $item = Mage::registry('item');
        if($item){
            $fieldset = $form->addFieldset('certificate_form', array('legend'=>Mage::helper('opentechiz_inventory')->__('Certificate form')));
            $order_item = Mage::getModel('opentechiz_inventory/order')->getCollection()->addFieldtoFilter('item_id',$item->getId())->getFirstItem();
          
            $orderitem = Mage::getModel('sales/order_item')->load($order_item->getData('order_item_id'));
            $order_id = $orderitem->getOrderId();
            if($orderitem&&$orderitem->getProductId()){
                $product = Mage::getModel('catalog/product')->load($orderitem->getProductId());
            }
            else{
                $productSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $item->getSku())[0];
                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productSku);
            }
           
            $order = Mage::getModel('sales/order')->load($order_id);
            $customer_id = $order->getCustomerId();

            $customer = Mage::getModel('customer/customer')->load($customer_id);
            $customer_address = $customer->getPrimaryBillingAddress();
            $productData = unserialize( $item->getOptions());
            $imageName = $product->getSku();
            
            $is_personalizeditem = 0;
            if(strpos($item->getData('sku'), 'quotation') === false) {
                if(Mage::helper('personalizedproduct')->isPersonalizedProduct($product)){
                    $product->setSku($item->getSku());
                    $is_personalizeditem = 1;
                }
                $url = $this->helper('catalog/image')->init($product, 'thumbnail')->resize(120, 120);
            } else {
                $quotationProductId = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $item->getSku())[1];
                $productImages = Mage::getModel('opentechiz_quotation/product')->load($quotationProductId)->getQpImage();

                $url = explode(',',$productImages)[0];
            }
            $image = $url;
           
            /*$fieldset->addField('address', 'textarea', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Address'),
                'name'      => 'address',
                'value'     => 
            ));*/
            $fieldset->addField('id', 'hidden', array(
                'name'      => 'id',
                'value'     => $item->getId()
            ));
            $fieldset->addField('customer_name', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Customer Name'),
                'name'      => 'customer_name',
                'value'     => $customer->getName() 
            ));
             $fieldset->addField('street', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Street'),
                'name'      => 'street',
                'value'     => $customer_address['street'] 
            ));
            $fieldset->addField('city', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('City'),
                'name'      => 'city',
                'value'     => $customer_address['city'] 
            ));
            $fieldset->addField('region', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Region'),
                'name'      => 'region',
                'value'     => $customer_address['region'] 
            ));
            $fieldset->addField('postcode', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Postcode'),
                'name'      => 'postcode',
                'value'     => $customer_address['postcode'] 
            ));
            
            $fieldset->addField('country', 'select', 
                         array(
                    'name'  => 'country',
                    'label'     => 'Country',
                    'value'     => $customer_address['country_id'] ,
                    'values'    => Mage::getModel('adminhtml/system_config_source_country')->toOptionArray(),

            ));

            $fieldset->addField('item_code', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Item code'),
                'name'      => 'item_code',
                'value'     => $item->getSku()
            ));

            $fieldset->addField('item_name', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Item Name'),
                'name'      => 'item_name',
                'value'     => $item->getName() ? $item->getName() :$product->getName()
            ));

            $fieldset->addField('item_image', 'hidden', array(
                'name'      => 'item_image',
                'value'     => $image
            ));

            $fieldset->addField('is_personalizeditem', 'hidden', array(
                'name'      => 'is_personalizeditem',
                'value'     => $is_personalizeditem
            ));

            $fieldset->addField('uploadimage', 'file', array(
                'label'     => 'Item image',
                'required'  => false,
                'name'      => 'uploadimage',
            ));
            if((strip_tags( Mage::helper('opentechiz_inventory')->getItemOption(unserialize($item->getOptions()),$item->getSku())) !="no option") && (strip_tags( Mage::helper('opentechiz_inventory')->getItemOption(unserialize($item->getOptions()),$item->getSku())) !="")){
                $fieldset->addField('item_gold', 'label', array(
                     'label'     => Mage::helper('opentechiz_purchase')->__('Option'),
                    'value'     => strip_tags( Mage::helper('opentechiz_inventory')->getItemOption(unserialize($item->getOptions()),$item->getSku()))
                ));
            }

             $fieldset->addField('item_option', 'hidden', array(
                'name'      => 'item_option',
                'value'     =>  Mage::helper('opentechiz_inventory')->getItemOption(unserialize($item->getOptions()),$item->getSku())
            ));

           /* $fieldset->addField('item_image', 'image', array(
                'name'      => 'item_image',
                'label'     => 'Item Image:',
                'title'     => 'Item Image',
                'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Inventory_Item_Certificate_Renderer_Image',
                'value'     => $image
            ));*/

           if($product->getAttributeText('c2c_met_filter')) {
               $qualityOption = '';
               $itemOptions = unserialize($item->getOptions());
               if(isset($itemOptions['options'])){
                   foreach ($itemOptions['options'] as $option){
                       if(isset($option['label']) && $option['label'] == 'Gold Quality'){
                           $qualityOption = $option['value'];
                       }
                   }
               }
               
               if($qualityOption != ''){
                   $valueQuality = $qualityOption;

               }else {
                   if(is_array($product->getAttributeText('c2c_met_filter'))){
                       $valueQuality =  $product->getAttributeText('c2c_met_filter')[0];
                   }else {
                       $valueQuality = $product->getAttributeText('c2c_met_filter');
                   }
               }
               $fieldset->addField('metal', 'text', array(
                   'label' => Mage::helper('opentechiz_purchase')->__('Metal'),
                   'name' => 'metal',
                   'value' => $valueQuality
               ));
           }


            $fieldset->addField('retail_price', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Retail Value'),
                'name'      => 'retail_price',
                'value'     => Mage::helper('core')->currency($product->getPrice(), true, false)
            ));

            
        }else{
            $item = Mage::registry('item_instock');
            $fieldset = $form->addFieldset('certificate_form', array('legend'=>Mage::helper('opentechiz_inventory')->__('Certificate form')));

            $productSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $item->getSku())[0];
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productSku);            
            $is_personalizeditem = 0;
            if(strpos($item->getData('sku'), 'quotation') === false) {
                if(Mage::helper('personalizedproduct')->isPersonalizedProduct($product)){
                    $product->setSku($item->getSku());
                    $is_personalizeditem = 1;
                }
                $url = $this->helper('catalog/image')->init($product, 'thumbnail')->resize(120, 120);
            } else {
                $quotationProductId = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $item->getSku())[1];
                $productImages = Mage::getModel('opentechiz_quotation/product')->load($quotationProductId)->getQpImage();

                $url = explode(',',$productImages)[0];
            }
            $image = $url;
           
            /*$fieldset->addField('address', 'textarea', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Address'),
                'name'      => 'address',
                'value'     => 
            ));*/
            $fieldset->addField('id', 'hidden', array(
                'name'      => 'id',
                'value'     => ''
            ));
            $fieldset->addField('customer_name', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Customer Name'),
                'name'      => 'customer_name',
                'value'     => '' 
            ));
             $fieldset->addField('street', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Street'),
                'name'      => 'street',
                'value'     => ''
            ));
            $fieldset->addField('city', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('City'),
                'name'      => 'city',
                'value'     => ''
            ));
            $fieldset->addField('region', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Region'),
                'name'      => 'region',
                'value'     => ''
            ));
            $fieldset->addField('postcode', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Postcode'),
                'name'      => 'postcode',
                'value'     => ''
            ));
            
            $fieldset->addField('country', 'select', 
                         array(
                    'name'  => 'country',
                    'label'     => 'Country',
                    'value'     => '' ,
                    'values'    => Mage::getModel('adminhtml/system_config_source_country')->toOptionArray(),

            ));

            $fieldset->addField('item_code', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Item code'),
                'name'      => 'item_code',
                'value'     => $item->getSku()
            ));

            $fieldset->addField('item_name', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Item Name'),
                'name'      => 'item_name',
                'value'     => $product->getName()
            ));

            $fieldset->addField('item_image', 'hidden', array(
                'name'      => 'item_image',
                'value'     => $image
            ));

            $fieldset->addField('is_personalizeditem', 'hidden', array(
                'name'      => 'is_personalizeditem',
                'value'     => $is_personalizeditem
            ));

            $fieldset->addField('uploadimage', 'file', array(
                'label'     => 'Item image',
                'required'  => false,
                'name'      => 'uploadimage',
            ));
            if((strip_tags( Mage::helper('opentechiz_inventory')->getItemOption(unserialize($item->getOptions()),$item->getSku())) !="no option") && (strip_tags( Mage::helper('opentechiz_inventory')->getItemOption(unserialize($item->getOptions()),$item->getSku())) !="")){
                $fieldset->addField('item_gold', 'label', array(
                     'label'     => Mage::helper('opentechiz_purchase')->__('Option'),
                    'value'     => strip_tags( Mage::helper('opentechiz_inventory')->getItemOption(unserialize($item->getOptions()),$item->getSku()))
                ));
            }
            

             $fieldset->addField('item_option', 'hidden', array(
                'name'      => 'item_option',
                'value'     =>  Mage::helper('opentechiz_inventory')->getItemOption(unserialize($item->getOptions()),$item->getSku())
            ));


           /* $fieldset->addField('item_image', 'image', array(
                'name'      => 'item_image',
                'label'     => 'Item Image:',
                'title'     => 'Item Image',
                'renderer' => 'OpenTechiz_Inventory_Block_Adminhtml_Inventory_Item_Certificate_Renderer_Image',
                'value'     => $image
            ));*/

           if($product->getAttributeText('c2c_met_filter')) {

               if(is_array($product->getAttributeText('c2c_met_filter'))){
                   $valueQuality =  $product->getAttributeText('c2c_met_filter')[0];
               }else {
                   $valueQuality = $product->getAttributeText('c2c_met_filter');
               }

               $fieldset->addField('metal', 'text', array(
                   'label' => Mage::helper('opentechiz_purchase')->__('Metal'),
                   'name' => 'metal',
                   'value' => $valueQuality
               ));
           }

            $fieldset->addField('retail_price', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Retail Value'),
                'name'      => 'retail_price',
                'value'     => Mage::helper('core')->currency($product->getPrice(), true, false)
            ));
        }
        $attributeSetId = $product->getAttributeSetId();
        $attributes =Mage::getModel('catalog/product_attribute_api')->items($attributeSetId);
        $list_attribute_set = array_column($attributes, 'code');
        if(in_array('c2c_setting_type',$list_attribute_set)){
            $fieldset->addField('c2c_setting_type', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Setting Type'),
                'name'      => 'c2c_setting_type',
                 'value'     => $product->getData('c2c_setting_type')
            ));
        }
        if(in_array('c2c_average_clarity_for_filter',$list_attribute_set)){
             $fieldset->addField('c2c_average_clarity_for_filter', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Average Clarity'),
                'name'      => 'c2c_average_clarity_for_filter',
                'value'     => $product->getAttributeText('c2c_average_clarity_for_filter')
            ));
        }
        if(in_array('c2c_average_color',$list_attribute_set)){
            $fieldset->addField('c2c_average_color', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Average Color'),
                'name'      => 'c2c_average_color',
                 'value'     => $product->getData('c2c_average_color')
            ));
        }
        if(in_array('c2c_carat_weight',$list_attribute_set)){
            $fieldset->addField('c2c_carat_weight', 'text', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Approx Carat Weight'),
                'name'      => 'c2c_carat_weight',
                 'value'     => !is_null($product->getData('c2c_carat_weight')) ?number_format((float)$product->getData('c2c_carat_weight'), 2, '.', '')."ct" :''
            ));
        }
        $fieldset->addField('logo', 'select', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Background Logo'),
            'name'      => 'logo',
            'options'     => $this->getLogoList(),
            'required'  => true,
        ));
        $fieldset->addField('note', 'textarea', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Note'),
            'name'      => 'note',
            'value'     => '',
        ));

        return parent::_prepareForm();
    }

    public function getLogoList(){
        $result = [];
        $result['empty'] = 'Empty';
        $result['itshot'] = 'ItsHot';
        $result['luxurman'] = 'LUXURMAN';

        return $result;
    }
}
