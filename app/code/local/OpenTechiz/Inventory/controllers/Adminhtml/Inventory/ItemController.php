<?php


class OpenTechiz_Inventory_Adminhtml_Inventory_ItemController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Inventory'))->_title($this->__('All Items'));
        $this->loadLayout()
            ->_setActiveMenu('erp');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_inventory/adminhtml_inventory_item'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction(){
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_inventory/adminhtml_inventory_item_grid')->toHtml()
        );
    }
    public function certifacateAction(){
       try {
        if ($this->getRequest()->getParam('id')||$this->getRequest()->getParam('code')) {
             if($this->getRequest()->getParam('id'))
                $item_id = $this->getRequest()->getParam('id');
             else{
                  $barcode = $this->getRequest()->getParam('code');
                  $item_id = substr($barcode,6);
                }
            $item_model = Mage::getModel('opentechiz_inventory/item')->load($item_id);

            Mage::register('item',$item_model);
            $this->_title($this->__('Inventory'))->_title($this->__('All Items'));
        $this->loadLayout()
            ->_setActiveMenu('erp');
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('opentechiz_inventory/adminhtml_inventory_item_certificate'))
                ->_addLeft($this->getLayout()->createBlock('opentechiz_inventory/adminhtml_inventory_item_certificate_tabs'));

            $this->renderLayout();
        }else{
            Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('adminhtml')->__(
                            'Missing Item Id . Please check.'
                        )
                    );
            $this->_redirect('*/*/');
        }
        } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                if(isset($barcode))
                    $this->_redirect('*/request_product/');
                else
                    $this->_redirect('*/*/');
            }
    }
    public function saveAction(){
        if ($this->getRequest()->getParam('id')) {

            $id = $this->getRequest()->getParam('id');
            $data = $this->getRequest()->getParams();

            try {
                $data['server_image'] = 0;
                if(isset($_FILES['uploadimage']['name']) and (file_exists($_FILES['uploadimage']['tmp_name']))) {
                    $uploader = new Varien_File_Uploader('uploadimage');
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));

                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);

                    $path = Mage::getBaseDir('media') . DS .'upload'. DS.'item_image'.DS ;
                    $destFile = $path . $_FILES['uploadimage']['name'];
                    $filename = $uploader->getNewFileName($destFile);
                    $uploader->save($path, $filename);

                    $data['item_image'] = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'upload'. DS.'item_image'.DS.$filename;

                }
                if($data['is_personalizeditem']==1)
                {
                    $imageName_ar = explode('/', $data['item_image']);
                    $imageName = array_pop($imageName_ar);

                    $_GET['path'] = $imageName;
                    ob_start(); // begin collecting output
                    include(Mage::getBaseDir() . DS . 'image_render_for_pdf.php');
                    $data['item_image'] =  ob_get_contents();
                    ob_end_clean();
                }

                //save certificate - item link
                $item = Mage::getModel('opentechiz_inventory/item')->load($id);
                if($item && $item->getBarcode()){
                    $cert = Mage::getModel('opentechiz_inventory/certificate');
                    $certNumber = Mage::helper('opentechiz_inventory')->getCertNumber();
                    $cert->setCertId($certNumber)
                        ->setBarcode($item->getBarcode())->save();

                    $resource = Mage::getSingleton('core/resource');
                    $write = $resource->getConnection('core_write');
                    $table = $resource->getTableName('opentechiz_inventory/certificate');
                    $write->query("UPDATE {$table} SET cert_id = '{$certNumber}' WHERE id = {$cert->getId()}");
                    $data['cert_number'] = $certNumber;
                }

                require_once(Mage::getBaseDir('lib') . DS . 'mpdf/vendor/autoload.php');
                $block = $this->getLayout()->createBlock('core/template');
                $block->setItemId($id);
                $block->setItemData($data);
                $block->setTemplate('opentechiz/production/itempdf.phtml');

                $mpdf = new \Mpdf\Mpdf(array('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0));
                $mpdf->showImageErrors = true;
                $mpdf->SetDisplayMode('fullpage');
                $mpdf->AddPage('', // L - landscape, P - portrait
                    '', '', '', '',
                    5, // margin_left
                    5, // margin right
                   10, // margin top
                   20, // margin bottom
                    0, // margin header
                    0); // margin footer
                // $mpdf->list_indent_first_level = 0;
                $mpdf->WriteHTML($block->toHtml());
                $mpdf->Output('certificate.pdf','D');
                exit;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                if(Mage::helper('opentechiz_inventory')->isAllowPermission()){
                    $this->_redirect('*/instockProduct/index/');
                }else{
                    $this->_redirect('*/*/');
                }
            }
        }
        else{
            $this->_getSession()->addError($this->__('Missing item id . Please check again'));
            if(Mage::helper('opentechiz_inventory')->isAllowPermission()){
                $this->_redirect('*/instockProduct/index/');
            }else{
                $this->_redirect('*/*/');
            }

        }
    }
       /**
     * MassStatus action
     */
    public function massStateAction()
    {
        $item_id = $this->getRequest()->getParam('item_id');

        if (!is_array($item_id)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s).'));
        } else {
            try {

                $admin = Mage::getSingleton('admin/session')->getUser();
                foreach ($item_id as $id) {
                    $item =  Mage::getModel('opentechiz_inventory/item')->load($id);
                    $check_state = $item->getState();
                    $test = $item->getId();
                    $item->setState($this->getRequest()->getParam('state'))
                        ->setIsMassupdate(true);
                     //log stock
                    if($check_state == 35){
                        $stock = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', $item->getSku())->getFirstItem();
                        if($stock->getId()){
                            Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                                'qty' => '-1',
                                'on_hold' => '0',
                                'type' => 10,
                                'stock_id' => $stock->getId(),
                                'qty_before_movement' => $stock->getQty(),
                                'on_hold_before_movement' => $stock->getOnHold(),
                                'sku' => $stock->getSku(),
                                'item_inventory_id' =>$item->getId()
                            ));
                        }

                    }

                    $item->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated.', count($item_id))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        if($action =="certifacate" || $action =="save"){
            $aclResource = 'admin/erp/inventory/instock';
        }
        if(Mage::getSingleton('admin/session')->isAllowed('admin/erp/inventory/item')){
            $aclResource ='admin/erp/inventory/item';
        }
        return Mage::getSingleton('admin/session')->isAllowed($aclResource);
    }
}
