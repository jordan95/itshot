<?php


class OpenTechiz_Inventory_Adminhtml_Inventory_ItemEditController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Inventory'))->_title($this->__('Items Edit'));
        $this->loadLayout()
            ->_setActiveMenu('erp');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_inventory/adminhtml_inventory_item_edit'));
        return $this;
    }

    public function indexAction()
    {
        $flag = true;
        $error = '';
        $po_id = 0;
        $sku = '';

        $item_ids = $this->getRequest()->getParam('item_id');

        if (!is_array($item_ids)) {
            $error = Mage::helper('adminhtml')->__('Please select item(s).');
        }

        foreach ($item_ids as $item_id){
            $POitem = Mage::getModel('opentechiz_purchase/poitem')->load($item_id, 'item_id');
            if(!$POitem->getId()){
                $flag = false;
                $error = Mage::helper('adminhtml')->__('You have selected Item(s) not in any Purchase Order');
            }

            if($po_id == 0){
                $po_id = $POitem->getPoId();
            } else{
                if($POitem->getPoId() != $po_id){
                    $flag = false;
                    $error = Mage::helper('adminhtml')->__('You have selected Items from different Purchase Orders');
                }
            }

            $item = Mage::getModel('opentechiz_inventory/item')->load($item_id);
            if($sku == ''){
                $sku = $item->getSku();
            } else{
                if($item->getSku() != $sku){
                    $flag = false;
                    $error = Mage::helper('adminhtml')->__('You have selected Items that have different SKUs');
                }
            }
            if($item->getState() != 35){
                $flag = false;
                $error = Mage::helper('adminhtml')->__('You can\'t edit item(s) not in stock');
            }
        }


        if($flag == true) {$this->_initAction()->renderLayout();}
        else{
            Mage::getSingleton('adminhtml/session')->addError($error);
            $this->_redirect('adminhtml/inventory_item/index');
        }

    }

    public function ajaxGetProductOptionAction()
    {
        $productId = $this->getRequest()->getParam('product_id');
        $product = Mage::getModel('catalog/product')->load($productId);

        $html = '<input type="hidden" name="product_id" value="'.$productId.'">
                 <table class="form-list" cellspacing="0"><tbody>';
        foreach ($product->getOptions() as $option){
            $html .= '<tr><td class="label"> '.$option->getTitle().' </td>';
            if($option->getType() == 'stone' || $option->getType() == 'gold' || $option->getType() == 'drop_down' || $option->getType() == 'radio'){
                $html .= '<td class="value"> <select name="'.$option->getId().'">';
                foreach ($option->getValues() as $value){
                    if($value->getSku()) $html .= '<option value="'.$value->getId().'">'.$value->getSku().'</option>';
                    else $html .= '<option value="'.$value->getId().'">'.$value->getTitle().'</option>';
                }
                $html .= '</select> </td>';
            }else{
                $html .= '<td class="value">';
                $html .= '<input name="'.$option->getId().'" />';
                $html .= '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '  
                    <tr>
                        <td class="label"><label >Cost for each item</label></td>
                        <td class="value">
                            <input id="cost" name="cost" class="input-text" required/>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><label for="note">Note</label></td>
                        <td class="value">
                            <textarea id="note" name="note" rows="2" cols="15" class=" textarea"></textarea>
                        </td>
                    </tr>
                    </tbody>
                    </table>';

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($html));
    }

    public function submitEditFormAction(){
        $params = $this->getRequest()->getParams();
        //change item's data
        $product = Mage::getModel('catalog/product')->load($params['product_id']);
        ////get name
        $name = $product->getName();
        ////get sku
        $skuParts = [];
        foreach ($params as $key => $value) {
            foreach ($product->getOptions() as $option) {
                if ($option->getId() == $key) {
                    $optionValue = $option->getValueById($value);
                    if($optionValue){
                        $skuParts[$key] = $optionValue->getData('sku');
                    }
                }
            }
        }
        $skuParts = implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $skuParts);
        $sku = $product->getSku().OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.$skuParts;
        $sku= trim($sku, OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR);
        ////get image
        $product->setSku($sku);
        $imageUrl = Mage::helper('catalog/image')->init($product, 'thumbnail')->__toString();
        ////get options
        $options = [];
        $options['info_buyRequest'] = [];
        $options['info_buyRequest']['product'] = $params['product_id'];
        $options['options'] = [];
        foreach ($params as $key => $value) {
            foreach ($product->getOptions() as $option) {
                if ($option->getId() == $key) {
                    $optionData = [];
                    $optionData['label'] = $option->getTitle();
                    $optionData['option_id'] = $option->getId();
                    $optionData['option_type'] = $option->getType();

                    $optionValue = $option->getValueById($value);
                    if($optionValue){
                        $optionData['value'] = $optionValue->getTitle();
                        $optionData['print_value'] = $optionValue->getTitle();
                        $optionData['option_value'] = $optionValue->getId();
                    }else{
                        $optionData['value'] = $value;
                        $optionData['print_value'] = $value;
                        $optionData['option_value'] = $value;
                    }
                    array_push($options['options'], $optionData);
                }
            }
        }
        ////get updated time
        $updated_at = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        ////change data
        $oldSku = '';
        $qty = 0;
        foreach (explode(', ', $params['barcode']) as $barcode){
            $qty++;
            $barcode = trim($barcode);
            $item = Mage::getModel('opentechiz_inventory/item')->load($barcode, 'barcode');
            if($oldSku == '') $oldSku = $item->getSku();
            $data = [];
            $data['sku'] = $sku;
            $data['name'] = $name;
            $data['image'] = $imageUrl;
            $data['options'] = serialize($options);
            $data['note'] = $params['note'];
            $data['cost'] = $params['cost'];
            $data['updated_at'] = $updated_at;
            $item->addData($data)->setId($item->getId())->save();
        }

        //find and change qty in stock
        ////find old sku
        $itemNeedChanged = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', $oldSku)->getFirstItem();
        ////reduce qty
        if($itemNeedChanged->getId()){
            //log stock
            Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                'qty' => '-'.(int)$qty,
                'on_hold' => '0',
                'type' => 6,
                'stock_id' => $itemNeedChanged->getId(),
                'qty_before_movement' => $itemNeedChanged->getQty(),
                'on_hold_before_movement' => $itemNeedChanged->getOnHold(),
                'sku' => $itemNeedChanged->getSku(),
                'from_sku' => $itemNeedChanged->getSku(),
                'to_sku' => $sku
            ));
            $itemNeedChanged->setQty((int)$itemNeedChanged->getQty() - (int)$qty)->save();
        }
        ////find new sku
        $itemToChangeTo = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', $sku)->getFirstItem();
        ////exist -> inc qty
        if($itemToChangeTo->getId()){
            //log stock
            Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                'qty' => (int)$qty,
                'on_hold' => '0',
                'type' => 16,
                'stock_id' => $itemToChangeTo->getId(),
                'qty_before_movement' => $itemToChangeTo->getQty(),
                'on_hold_before_movement' => $itemToChangeTo->getOnHold(),
                'sku' => $itemToChangeTo->getSku(),
                'from_sku' => $itemNeedChanged->getSku(),
                'to_sku' => $itemToChangeTo->getSku()
            ));
            $itemToChangeTo->setQty((int)$itemToChangeTo->getQty() + (int)$qty)->save();
        }
        ////else create new
        else{
            $stockData = [];
            $stockData['product_id'] = $params['product_id'];
            $stockData['sku'] = $sku;
            $stockData['qty'] = $qty;
            $stockData['reorder_point'] = 0;
            $stockData['on_hold'] = 0;
            Mage::getModel('opentechiz_inventory/instock')->setData($stockData)->save();
        }

        //find and change data in PO
        $POitemModel = Mage::getModel('opentechiz_purchase/orderitem');

        if($item->getId()){
            $item_id = $item->getId();
            $po_id = Mage::getModel('opentechiz_purchase/poitem')->getCollection()->addFieldToFilter('item_id', $item_id)->getFirstItem()->getPoId();

            $old_po_item = $POitemModel->getCollection()->addFieldToFilter('po_id', $po_id)->addFieldToFilter('sku', $oldSku)->getFirstItem();
            if($old_po_item->getId()) $old_po_item->setQty($old_po_item->getQty() - $qty)->setDeliveredQty($old_po_item->getDeliveredQty() - $qty)->preventAfterSave()->save();

            $new_po_item = $POitemModel->getCollection()->addFieldToFilter('po_id', $po_id)->addFieldToFilter('sku', $sku)->getFirstItem();
            if($new_po_item->getId()) $new_po_item->setQty($new_po_item->getQty() + $qty)->setDeliveredQty($new_po_item->getDeliveredQty() + $qty)->preventAfterSave()->save();
            else{
                $poData = [];
                $poData['product_id'] = $params['product_id'];
                $poData['po_id'] = $po_id;
                $poData['sku'] = $sku;
                $poData['option'] = $item->getData('options');
                $poData['qty'] = $qty;
                $poData['delivered_qty'] = $qty;
                $poData['price'] = $params['cost'];
                $POitemModel->setData($poData)->preventAfterSave()->save();
            }
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Updated.'));
        $this->_redirect('adminhtml/inventory_item/index');
    }
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/inventory/item');
    }
}