<?php

class OpenTechiz_Inventory_Adminhtml_InstockRequestController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Inventory'))->_title($this->__('Stock Requests'));
        $this->loadLayout()
            ->_setActiveMenu('erp');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_inventory/adminhtml_instockRequest'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_inventory/adminhtml_instockRequest_grid')->toHtml()
        );
    }

    public function massStatusAction()
    {
        $requestIds = $this->getRequest()->getParam('instock_request_status');
        if (!$requestIds) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                $k=0;
                foreach ($requestIds as $requestId) {
                    $request = Mage::getSingleton('opentechiz_inventory/inventory_instockRequest')->load($requestId);
                    if($request->getStatus()!=0){
                        $k++;
                        continue;
                    }

//                    $instockProduct = Mage::getModel('opentechiz_inventory/instock')->load($request->getInstockProductId());
//                    if($instockProduct->getQty()-$request->getQty() < 0){
//                        $k++;
//                        continue;
//                    }

                    $request->setStatus($this->getRequest()->getParam('status'))
                    ->setIsMassupdate(true)
                    ->save();
                       
                    $admin = Mage::getSingleton('admin/session')->getUser();
                    if($this->getRequest()->getParam('status') == 1){
                        $error = $request->takeItem($requestId);
                        if($error != ''){
                            $this->_getSession()->addError($error);
                            $this->_redirect('*/*/index');
                        }
                        if($admin->getId()){
                            Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Accept instock request #'.$requestId));
                        }
                    } else {
                        $request->releaseItem($requestId);
                        //change request to process status back to "no"
                        Mage::getModel('sales/order_item')->load($request->getOrderItemId())->setIsRequestedToProcess(0)->setRemainingQty((int)$request->getQty())->save();
                        
                        if($admin->getId()){
                            Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Decline instock request #'.$requestId));
                           if($request->getReturnId()){
                                $return = Mage::getModel('opentechiz_return/return')->load($request->getReturnId());  
                                if($return->getId()){
                                    $return->setStatus(52);
                                    $return->save();
                                }
                            }  
                            
                        }
                    }
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated , %d cannot be changed status', count($requestIds)-$k,$k)
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/inventory/stockrequest');
    }
}