<?php


class OpenTechiz_Inventory_Adminhtml_InstockProductController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Inventory'))->_title($this->__('Inventory Management'));
        $this->loadLayout()
            ->_setActiveMenu('erp');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_inventory/adminhtml_instockProduct'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function newAction(){
        $this->loadLayout()->_setActiveMenu('erp');
        $block = $this->getLayout()->createBlock('opentechiz_inventory/adminhtml_addToStock');
        $block->setTemplate('opentechiz/inventory/add_stock_form.phtml');
        $this->getLayout()->getBlock('content')->append($block);
        $this->renderLayout();
    }

    public function gridAction(){
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_inventory/adminhtml_instockProduct_Grid')->toHtml()
        );
    }

    public function addStockAction(){
        $params = $this->getRequest()->getParams();
        $stockModel = Mage::getModel('opentechiz_inventory/instock');
        $itemModel = Mage::getModel('opentechiz_inventory/item');

        //get item sku
        $sku = $params['product_sku'];
        for($i = 0; $i < 10; $i++){
            if(array_key_exists($i, $params)){
                $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.$params[$i];
            }
        }

        //log stock
        $stock = $stockModel->load($sku, 'sku');
        $id = $stock->getId();
        Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
            'qty' => (int)$params['qty'],
            'on_hold' => 0,
            'description'=>"Add To Stock manually (".$params['note']. ")",
            'type' => 7,
            'stock_id' => $id,
            'qty_before_movement' => $stock->getQty(),
            'on_hold_before_movement' => $stock->getOnHold(),
            'sku' => $stock->getSku()
        ));

        //add items
        $product = Mage::getModel('catalog/product')->load($params['product_id']);
        $name = $product->getName();
        $product->setSku($sku);
        $image = Mage::helper('catalog/image')->init($product, 'image');

        $itemOption = [];
        $itemOption['info_buyRequest']=[];
        $itemOption['info_buyRequest']['product'] = $product->getId();

        $itemOption['options'] = [];

        foreach($params as $key=>$value){
            $data = [];
            if (strpos($key, 'product_option') !== false) {
                $parts = explode('_', $key);

                foreach($product->getOptions() as $option) {
                    if($option->getId() == $parts[2]) {

                        $data['label'] = $option->getTitle();
                        foreach ($option->getValues() as $optionValue) {
                            if ($optionValue->getSku() == $value) {
                                $data['value'] = $optionValue->getTitle();
                                $data['option_id'] = $option->getId();
                                $data['option_value'] = $optionValue->getId();
                                $data['option_type'] = $option->getType();
                            } else {
                                $data['value'] = $value;
                                $data['option_id'] = $option->getId();
                                $data['option_value'] = $optionValue->getId();
                                $data['option_type'] = $option->getType();
                            }
                        }

                    }
                }

                array_push($itemOption['options'], $data);
            }
        }

        for ($i = 0; $i < (int)$params['qty']; $i++){
            $data = [];
            $data['sku'] = $sku;
            $data['name'] = $name;
            $data['image'] = $image;
            $data['type'] = 2;
            $data['note'] = $params['note'];
            $data['state'] = 35;
            $data['cost'] = $params['cost'];
            $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['updated_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['options'] = serialize($itemOption);

            $itemModel->setData($data)->save();
            $barcode = $itemModel->generateBarcode($itemModel->getId());
            $itemModel->setBarcode($barcode)->save();
        }

        $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Data has been saved.'));
        $this->_redirect('*/*');
    }

    public function ajaxGetProductAction(){
        $productSku = $this->getRequest()->getParam('product_sku');
        $productSku = str_replace(' ', '', $productSku);
        $logicModel = Mage::getModel('opentechiz_inventory/instock');

        $html = $logicModel->addStock($productSku);

        if(!$html){
            $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('The product does not exist'));
            $this->_redirect('*/*/new');
        }else {
            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            return $this->getResponse()->setBody(json_encode($html));
        }
    }

    public function inlineEditAction(){
        $params = $this->getRequest()->getParams();
        $stockId = $params['stock_id'];
        $value = (int)$params['rop'];
        $data['reorder_point'] = $value;

        $stock = Mage::getModel('opentechiz_inventory/instock')->load((int)$stockId);
        try{
            $stock->addData($data)->save();
            $response=array('error'=>false);
        } catch (Exception $e) {
            $response=array('items'=>$e->getMessage(),'error'=>true);
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/inventory/instock');
    }

    public function editStockAction(){
        $params = $this->getRequest()->getParams();
        if(!isset($params['id'])){
            $this->_getSession()->addError(Mage::helper('adminhtml')->__('Missing Id Param.'));
            $this->_redirect('*/*/index');
            return;
        }
        $this->_title($this->__('Inventory'))->_title($this->__('Inventory Management - Edit'));
        $this->loadLayout()
            ->_setActiveMenu('erp');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_inventory/adminhtml_instockProduct_Edit'));
        $this->renderLayout();
    }

    public function editStockSaveAction(){
        $params = $this->getRequest()->getParams();
        $stock = Mage::getModel('opentechiz_inventory/instock')->load($params['stock_id']);
        $currentQty = $stock->getQty();
        $actionName = '';
        if(isset($params['amount']) && $params['amount'] > $currentQty){
            $actionName = 'add';
        }elseif(isset($params['amount']) && $params['amount'] < $currentQty){
            $actionName = 'remove';
        }
        $changedQty = abs($currentQty - $params['amount']);
        if($changedQty > 100){
            $this->_getSession()->addError(Mage::helper('adminhtml')->__('Cannot add/remove over 100 items at once.'));
            $this->_redirect('*/*/index');
            return;
        }
        //add
        if($actionName == 'add'){
            //create items, add note
            $sku = $stock->getSku();
            $skuArray = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
            $originalSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $originalSku);
            $product = Mage::getModel('catalog/product')->load($product->getId());
            $product->setSku($sku);
            $imageUrl = Mage::helper('catalog/image')->init($product, 'thumbnail')->__toString();

            $options = [];
            $options['info_buyRequest'] = [];
            $options['info_buyRequest']['product'] = $product->getId();
            $options['options'] = [];
            $optionIds = [];
            foreach($product->getOptions() as $option){
                array_push($optionIds, $option->getId());
            }
            foreach($product->getOptions() as $option){
                $optionData = [];
                $optionData['label'] = $option->getTitle();
                $optionData['option_id'] = $option->getId();
                $optionData['option_type'] = $option->getType();
                foreach ($option->getValues() as $value){
                    $samePosition = ((array_search($value->getSku(), $skuArray) - 1) == array_search($option->getId(), $optionIds));
                    if(in_array($value->getSku(), $skuArray) && $samePosition){
                        $optionData['value'] = $value->getTitle();
                        $optionData['print_value'] = $value->getTitle();
                        $optionData['option_value'] = $value->getId();
                    }
                }
                if(isset($optionData['value'])) {
                    array_push($options['options'], $optionData);
                }
            }
            $updated_at = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            for ($i = 0; $i < (int)$changedQty; $i++){
                $item = Mage::getModel('opentechiz_inventory/item');
                $data = [];
                $data['sku'] = $sku;
                $data['name'] = $product->getName();
                $data['image'] = $imageUrl;
                $data['options'] = serialize($options);
                $data['note'] = OpenTechiz_Inventory_Helper_Data::CHANGE_STOCK_REASON[$params['reason']].'. ';
                if(isset($params['note']) && $params['note'] != ''){
                    $data['note'] .= $params['note'];
                }
                $data['cost'] = Mage::helper('opentechiz_inventory')->getDefaultItemCost($product);
                $data['updated_at'] = $updated_at;
                $data['created_at'] = $updated_at;
                $data['state'] = 35;
                $data['type'] = 2;
                $item->setData($data)->save();
                $barcode = $item->generateBarcode($item->getId());
                $item->setBarcode($barcode)->save();
            }
            //log stock
            Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                'qty' => (int)$changedQty,
                'on_hold' => 0,
                'description'=>"Add To Stock manually (".$params['note']. ")",
                'type' => 7,
                'stock_id' => $stock->getId(),
                'qty_before_movement' => $stock->getQty(),
                'on_hold_before_movement' => $stock->getOnHold(),
                'sku' => $stock->getSku()
            ));
        }
        //remove
        elseif($actionName == 'remove'){
            //check qty from stock
            $stock = Mage::getModel('opentechiz_inventory/instock')->load($params['stock_id']);
            if($stock->getQty() - $changedQty < 0){
                $this->_getSession()->addError(Mage::helper('adminhtml')->__('Requested remove qty > qty in stock.'));
                $this->_redirect('*/*/index');
                return;
            }
            if($stock->getQty() - $changedQty < $stock->getOnHold()){
                $this->_getSession()->addError(Mage::helper('adminhtml')->__('Updated Qty is smaller than reserved Qty.'));
                $this->_redirect('*/*/index');
                return;
            }
            //change items instock to lost, add note
            $sku = $stock->getSku();
            $itemCollection = Mage::getModel('opentechiz_inventory/item')->getCollection()->addFieldToFilter('sku', $sku)
                ->addFieldToFilter('state', 35);
            $i = 0;
            foreach ($itemCollection as $item){
                if($i == $changedQty) break;
                $note = OpenTechiz_Inventory_Helper_Data::CHANGE_STOCK_REASON[$params['reason']].'. ';
                if(isset($params['note']) && $params['note'] != ''){
                    $note .= $params['note'];
                }
                $item->setNote($note)
                    ->setState(50)->save();
                $i++;
            }
            //log stock
            Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                'qty' => '-'.$changedQty,
                'on_hold' => 0,
                'description'=>"Remove From Stock manually (".$params['note']. ")",
                'type' => 8,
                'stock_id' => $stock->getId(),
                'qty_before_movement' => $stock->getQty(),
                'on_hold_before_movement' => $stock->getOnHold(),
                'sku' => $stock->getSku()
            ));
        }
        $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Saved successfully.'));
        $this->_redirect('*/*/index');
    }

    public function printAppraisalAction(){
        $params = $this->getRequest()->getParams();
        $item = Mage::getModel('opentechiz_inventory/instock')->load($params['id']);
        $sku = $item->getSku();
        try {

            if ($item->getId()) {
                Mage::register('item_instock',$item);
                $this->_title($this->__('Inventory'))->_title($this->__('All Items'));
            $this->loadLayout()
                ->_setActiveMenu('erp');
                $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
                $this->_addContent($this->getLayout()->createBlock('opentechiz_inventory/adminhtml_inventory_item_certificate'))
                    ->_addLeft($this->getLayout()->createBlock('opentechiz_inventory/adminhtml_inventory_item_certificate_tabs'));

                $this->renderLayout();
            }else{
                Mage::getSingleton('adminhtml/session')->addError(
                            Mage::helper('adminhtml')->__(
                                'Missing Item Id . Please check.'
                            )
                        );
                $this->_redirect('*/*/');
            }

        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            $this->_redirect('*/*/');
        }
    }

    public function confirmReservationAction(){
        $params = $this->getRequest()->getParams();
        $instockRequestCollection = Mage::getModel('opentechiz_inventory/inventory_instockRequest')
            ->getCollection()->addFieldToFilter('instock_product_id', $params['id'])
            ->addFieldToFilter('status', 0)->setOrder('request_id', 'ASC');
        $model = Mage::getModel('opentechiz_inventory/inventory_instockRequest');
        $orderItemIds = [];

        foreach ($instockRequestCollection as $instockRequest){
            $error = $model->takeItem($instockRequest->getId());
            if($error != ''){
                $this->_getSession()->addError($error);
                $this->_redirect('*/*/index');
                return;
                break;
            }
            $instockRequest->setStatus(1)->save();
            if(!in_array($instockRequest->getOrderItemId(), $orderItemIds)) {
                array_push($orderItemIds, $instockRequest->getOrderItemId());
            }
        }


        if(count($orderItemIds)) {
            $orderIds = [];
            $orderItemCollection = Mage::getModel('sales/order_item')->getCollection()->addFieldToFilter('item_id', array('in' => $orderItemIds));
            foreach ($orderItemCollection as $orderItem){
                $orderId = $orderItem->getOrderId();
                if(!in_array($orderId, $orderIds)){
                    array_push($orderIds, $orderId);
                }
            }
            $orderString = '';
            $orderCollection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('entity_id', array('in' => $orderIds));
            foreach ($orderCollection as $order){
                $orderString .= ', '. $order->getIncrementId();
            }
            $orderString = trim($orderString, ', ');
            $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Order(s) '.$orderString. ' have been updated.'));
            $this->_redirect('*/*/index');
        }else{
            $this->_getSession()->addError(Mage::helper('adminhtml')->__('No item was reserved for order.'));
            $this->_redirect('*/*/index');
        }
    }

    public function saveAction(){
        if ($this->getRequest()->getParam('id')) {

            $id = $this->getRequest()->getParam('id');
            $data = $this->getRequest()->getParams();

            try {
                $data['server_image'] = 0;
                if(isset($_FILES['uploadimage']['name']) and (file_exists($_FILES['uploadimage']['tmp_name']))) {
                    $uploader = new Varien_File_Uploader('uploadimage');
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));

                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);

                    $path = Mage::getBaseDir('media') . DS .'upload'. DS.'item_image'.DS ;
                    $destFile = $path . $_FILES['uploadimage']['name'];
                    $filename = $uploader->getNewFileName($destFile);
                    $uploader->save($path, $filename);

                    $data['item_image'] = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'upload'. DS.'item_image'.DS.$filename;
                    $data['server_image'] = 1;
                }
                if($data['is_personalizeditem']==1)
                {
                    $imageName_ar = explode('/', $data['item_image']);
                    $imageName = array_pop($imageName_ar);

                    $_GET['path'] = $imageName;
                    ob_start(); // begin collecting output
                    include(Mage::getBaseDir() . DS . 'image_render_for_pdf.php');
                    $data['item_image'] =  ob_get_contents();
                    ob_end_clean();
                }
                 //save certificate - item link

                $cert = Mage::getModel('opentechiz_inventory/certificate');
                $certNumber = Mage::helper('opentechiz_inventory')->getCertNumber();
                $cert->setCertId($certNumber)->save();
                $data['cert_number'] = $certNumber;

                require_once(Mage::getBaseDir('lib') . DS . 'mpdf/vendor/autoload.php');
                $block = $this->getLayout()->createBlock('core/template');
                $block->setItemId($id);
                $block->setItemData($data);
                $block->setTemplate('opentechiz/production/itempdf.phtml');

                $mpdf = new \Mpdf\Mpdf(array('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0));
                $mpdf->showImageErrors = true;
                $mpdf->SetDisplayMode('fullpage');
                $mpdf->AddPage('', // L - landscape, P - portrait
                    '', '', '', '',
                    5, // margin_left
                    5, // margin right
                   10, // margin top
                   20, // margin bottom
                    0, // margin header
                    0); // margin footer
                // $mpdf->list_indent_first_level = 0;
                $mpdf->WriteHTML($block->toHtml());
                $mpdf->Output('certificate.pdf','D');
                exit;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                    $this->_redirect('*/*/');
            }
        }
        else{
            $this->_getSession()->addError($this->__('Missing item id . Please check again'));
            $this->_redirect('*/*/');

        }
    }

    public function refreshFlatDataAction(){
        Mage::getModel('opentechiz_inventory/instock')->refreshFlatData();

        $this->_getSession()->addSuccess($this->__('Data Refreshed'));
        $this->_redirect('*/*/');
    }
}
