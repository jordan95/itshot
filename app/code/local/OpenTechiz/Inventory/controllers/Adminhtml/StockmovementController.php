<?php

class OpenTechiz_Inventory_Adminhtml_StockmovementController extends Mage_Adminhtml_Controller_Action
{
      protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp')
            ->_title($this->__('Stock Movement'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_inventory/adminhtml_stockMovement'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_inventory/adminhtml_stockMovement_grid')->toHtml()
        );
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/inventory/stockmovement');
    }
}