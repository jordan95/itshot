<?php

class OpenTechiz_Inventory_Helper_Data extends Mage_Core_Helper_Abstract
{
    const ITEM_TYPE = [
        0 => 'Manufactured',
        1 => 'Purchase Order',
        2 => 'Added manually'
    ];

    const CHANGE_STOCK_ACTION = [
        0 => 'Add To Stock',
        1 => 'Remove From Stock'
    ];

    const CHANGE_STOCK_REASON = [
        0 => 'Added to Stock manually',
        1 => 'Removed from Stock manually'
    ];

    const ITEM_STATE = [
        0 => 'In production',
        1 => 'In Repair',
        5 => 'Production finished',
        6 => 'Repair finished',
        10 => 'Ship out waiting',
        15 => 'Shipped out',
        20 => 'Shipped to customer',
        21 => 'Returning from customer',
        25 => 'Reserved',
        26 => 'Reserved after Production',
        30 => 'Waiting to stock',
        35 => 'In Stock',
        40 => 'Canceled',
        45 => 'Broken',
        50 => 'Lost',
    ];
    const CONVERT_SIZE = [
        '1/4' => '.25',
        '1/2' => '.5',
        '3/4' => '.75',
    ];

    const RESERVED_STATE = [10,25,26,1];//added state In repair to allow bypass production finish step
    const RELEASED_STATE = [10,15,20,21,30];
    const SHIPPED_STATE = [15,20];

    const ITEM_SHIP_STATE = [
        20 => 'Shipped to customer',
        21 => 'Returning from customer',
        30 => 'Waiting to stock',
    ];
    const INSTOCK_REQUEST_STATUS = [
        0 => 'Pending',
        1 => 'Approved',
        2 => 'Declined'
    ];
    const STARTING_CERT_NUMBER = 'H20001';
    const CERT_PREFIX_CODE = 'H';
    const STOCK_PRODUCT_MOVEMENT_TYPE = [
        3 => 'Purchase Order',
        4 => 'Manufacture',
        5 => 'Return',
        6 => 'Convert from Item',
        7 => 'Add Stock Manually',
        8 => 'Remove Stock Manually',
        9 => 'Cancel Order',
        10 => 'Change State Item',
        11 => 'Taken Item',
        12 => 'Reserved Item',
        13 => 'Product Process Requests',
        14 => 'Change State Order',
        15 => 'InStock Waiting Items',
        16 => 'Convert to Item',
        21 => 'Populate Data Shipping',
        22 => 'Populate Data Return'
    ];

    public function getItemOption($option, $sku = '')
    {
        if (strpos($sku, 'quotation') === false) {
            if (!isset($option['options']))
                return 'no option';
            $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
            for ($i = 1; $i < count($parts) - 2; $i++) {
                switch ($i) {
                    case 1:
                        $key = 'Center Stone';
                        break;
                    case 2:
                        $key = 'Stone 2';
                        break;
                    case 3:
                        $key = 'Stone 3';
                        break;
                    case 4:
                        $key = 'Stone 4';
                        break;
                }
                $optionData[$key] = $parts[$i];
            }
            $html = '<dl class="item-options">';
            foreach ($option['options'] as $value) {
                if (in_array($value['option_type'], array('gold', 'stone')) || $value['label'] == "Gold Color") {
                    if ($value['option_type'] == 'stone') {
                        $value['value'] = $optionData[$value['label']];
                        $stoneSku = strtolower(str_replace(' ', '', $value['value']));
                        $quality = '';
                        if (strpos($stoneSku, 'diamond') !== false) {
                            $quality = str_replace('diamond', '', $stoneSku);
                            if (in_array($quality, OpenTechiz_PersonalizedProduct_Helper_Data::QUALITY)) {
                                $value['value'] = 'diamond' . '-' . $quality;
                            }
                        }
                    }
                }
                $html .= '<dd> ' . $value['label'] . ' : ' . $value['value'] . ' </dd> ';
            }
            $html .= '</dl>';
        } else {
            $quotationProductId = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[1];
            $optionHtml = '';

            $optionCollection = Mage::getModel('opentechiz_quotation/productOption')->getCollection()->addFieldToFilter('quote_product_id', $quotationProductId);
            $productName = Mage::getModel('opentechiz_quotation/product')->load($quotationProductId)->getQpName();

            foreach ($optionCollection as $option) {
                if ($option->getOptionType() == 'stone') {
                    $optionHtml .= '<dd>' . $option->getOptionName() . ' : ' . Mage::getModel('opentechiz_quotation/product')->getStoneName($option->getOptionValue()) . '</dd>';
                } elseif ($option->getOptionType() == 'gold') {
                    $optionHtml .= '<dd>' . $option->getOptionName() . ' : ' . Mage::getModel('opentechiz_quotation/product')->getGoldName($option->getOptionValue()) . '</dd>';
                }
            }
            $html = '<div class="item-container">
                    <div class="item-text">

                        <dl class="item-options">
                            ' . $optionHtml . '
                        </dl>
                    </div>
                </div>';
        }
        return $html;
    }

    public function barcode($filepath="",$text="0",$size="20",$orientation="horizontal",$code_type="code128") {
        $code_string = "";
        // Translate the $text into barcode the correct $code_type
        if ( strtolower($code_type) == "code128" ) {
            $chksum = 104;
            // Must not change order of array elements as the checksum depends on the array's key to validate final code
            $code_array = array(" "=>"212222","!"=>"222122","\""=>"222221","#"=>"121223","$"=>"121322","%"=>"131222","&"=>"122213","'"=>"122312","("=>"132212",")"=>"221213","*"=>"221312","+"=>"231212",","=>"112232","-"=>"122132","."=>"122231","/"=>"113222","0"=>"123122","1"=>"123221","2"=>"223211","3"=>"221132","4"=>"221231","5"=>"213212","6"=>"223112","7"=>"312131","8"=>"311222","9"=>"321122",":"=>"321221",";"=>"312212","<"=>"322112","="=>"322211",">"=>"212123","?"=>"212321","@"=>"232121","A"=>"111323","B"=>"131123","C"=>"131321","D"=>"112313","E"=>"132113","F"=>"132311","G"=>"211313","H"=>"231113","I"=>"231311","J"=>"112133","K"=>"112331","L"=>"132131","M"=>"113123","N"=>"113321","O"=>"133121","P"=>"313121","Q"=>"211331","R"=>"231131","S"=>"213113","T"=>"213311","U"=>"213131","V"=>"311123","W"=>"311321","X"=>"331121","Y"=>"312113","Z"=>"312311","["=>"332111","\\"=>"314111","]"=>"221411","^"=>"431111","_"=>"111224","\`"=>"111422","a"=>"121124","b"=>"121421","c"=>"141122","d"=>"141221","e"=>"112214","f"=>"112412","g"=>"122114","h"=>"122411","i"=>"142112","j"=>"142211","k"=>"241211","l"=>"221114","m"=>"413111","n"=>"241112","o"=>"134111","p"=>"111242","q"=>"121142","r"=>"121241","s"=>"114212","t"=>"124112","u"=>"124211","v"=>"411212","w"=>"421112","x"=>"421211","y"=>"212141","z"=>"214121","{"=>"412121","|"=>"111143","}"=>"111341","~"=>"131141","DEL"=>"114113","FNC 3"=>"114311","FNC 2"=>"411113","SHIFT"=>"411311","CODE C"=>"113141","FNC 4"=>"114131","CODE A"=>"311141","FNC 1"=>"411131","Start A"=>"211412","Start B"=>"211214","Start C"=>"211232","Stop"=>"2331112");
            $code_keys = array_keys($code_array);
            $code_values = array_flip($code_keys);
            for ( $X = 1; $X <= strlen($text); $X++ ) {
                $activeKey = substr( $text, ($X-1), 1);
                $code_string .= $code_array[$activeKey];
                $chksum=($chksum + ($code_values[$activeKey] * $X));
            }
            $code_string .= $code_array[$code_keys[($chksum - (intval($chksum / 103) * 103))]];
            $code_string = "211214" . $code_string . "2331112";
        } elseif ( strtolower($code_type) == "code39" ) {
            $code_array = array("0"=>"111221211","1"=>"211211112","2"=>"112211112","3"=>"212211111","4"=>"111221112","5"=>"211221111","6"=>"112221111","7"=>"111211212","8"=>"211211211","9"=>"112211211","A"=>"211112112","B"=>"112112112","C"=>"212112111","D"=>"111122112","E"=>"211122111","F"=>"112122111","G"=>"111112212","H"=>"211112211","I"=>"112112211","J"=>"111122211","K"=>"211111122","L"=>"112111122","M"=>"212111121","N"=>"111121122","O"=>"211121121","P"=>"112121121","Q"=>"111111222","R"=>"211111221","S"=>"112111221","T"=>"111121221","U"=>"221111112","V"=>"122111112","W"=>"222111111","X"=>"121121112","Y"=>"221121111","Z"=>"122121111","-"=>"121111212","."=>"221111211"," "=>"122111211","$"=>"121212111","/"=>"121211121","+"=>"121112121","%"=>"111212121","*"=>"121121211");
            // Convert to uppercase
            $upper_text = strtoupper($text);
            for ( $X = 1; $X<=strlen($upper_text); $X++ ) {
                $code_string .= $code_array[substr( $upper_text, ($X-1), 1)] . "1";
            }
            $code_string = "1211212111" . $code_string . "121121211";
        } elseif ( strtolower($code_type) == "code25" ) {
            $code_array1 = array("1","2","3","4","5","6","7","8","9","0");
            $code_array2 = array("3-1-1-1-3","1-3-1-1-3","3-3-1-1-1","1-1-3-1-3","3-1-3-1-1","1-3-3-1-1","1-1-1-3-3","3-1-1-3-1","1-3-1-3-1","1-1-3-3-1");
            for ( $X = 1; $X <= strlen($text); $X++ ) {
                for ( $Y = 0; $Y < count($code_array1); $Y++ ) {
                    if ( substr($text, ($X-1), 1) == $code_array1[$Y] )
                        $temp[$X] = $code_array2[$Y];
                }
            }
            for ( $X=1; $X<=strlen($text); $X+=2 ) {
                if ( isset($temp[$X]) && isset($temp[($X + 1)]) ) {
                    $temp1 = explode( "-", $temp[$X] );
                    $temp2 = explode( "-", $temp[($X + 1)] );
                    for ( $Y = 0; $Y < count($temp1); $Y++ )
                        $code_string .= $temp1[$Y] . $temp2[$Y];
                }
            }
            $code_string = "1111" . $code_string . "311";
        } elseif ( strtolower($code_type) == "codabar" ) {
            $code_array1 = array("1","2","3","4","5","6","7","8","9","0","-","$",":","/",".","+","A","B","C","D");
            $code_array2 = array("1111221","1112112","2211111","1121121","2111121","1211112","1211211","1221111","2112111","1111122","1112211","1122111","2111212","2121112","2121211","1121212","1122121","1212112","1112122","1112221");
            // Convert to uppercase
            $upper_text = strtoupper($text);
            for ( $X = 1; $X<=strlen($upper_text); $X++ ) {
                for ( $Y = 0; $Y<count($code_array1); $Y++ ) {
                    if ( substr($upper_text, ($X-1), 1) == $code_array1[$Y] )
                        $code_string .= $code_array2[$Y] . "1";
                }
            }
            $code_string = "11221211" . $code_string . "1122121";
        }
        // Pad the edges of the barcode
        $code_length = 20;
        for ( $i=1; $i <= strlen($code_string); $i++ )
            $code_length = $code_length + (integer)(substr($code_string,($i-1),1));
        if ( strtolower($orientation) == "horizontal" ) {
            $img_width = $code_length;
            $img_height = $size;
        } else {
            $img_width = $size;
            $img_height = $code_length;
        }
        $image = imagecreate($img_width, $img_height + 20);
        $black = imagecolorallocate ($image, 0, 0, 0);
        $white = imagecolorallocate ($image, 255, 255, 255);
        imagefill( $image, 0, 0, $white );
        $location = 10;
        for ( $position = 1 ; $position <= strlen($code_string); $position++ ) {
            $cur_size = $location + ( substr($code_string, ($position-1), 1) );
            if ( strtolower($orientation) == "horizontal" )
                imagefilledrectangle( $image, $location, 0, $cur_size, $img_height, ($position % 2 == 0 ? $white : $black) );
            else
                imagefilledrectangle( $image, 0, $location, $img_width, $cur_size, ($position % 2 == 0 ? $white : $black) );
            $location = $cur_size;
        }

        imagestring($image, 3, 35, $img_height, $text, $black);
        imagecolortransparent($image,$white);



        // Draw barcode to the screen or save in a file
        if($filepath=="") {
            header ('Content-type: image/png');
            imagepng($image);
            imagedestroy($image);
        } else {
            imagepng($image,$filepath);
            imagedestroy($image);
        }
    }

    public function getCertNumber(){
        $collection = Mage::getModel('opentechiz_inventory/certificate')->getCollection()
            ->setOrder('id', 'DESC');
        if(count($collection) == 0){
            return self::STARTING_CERT_NUMBER;
        }else{
            $lastestCode = $collection->getFirstItem()->getCertId();
            $lastestCode = str_replace(self::CERT_PREFIX_CODE, '', $lastestCode);
            $newCode = (int)$lastestCode + 1;
            $newCode = self::CERT_PREFIX_CODE . (string)$newCode;
            return $newCode;
        }
    }

    public function getDefaultItemCost($product){
        $cost = 0;

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        //get from last PO
        $sku = $product->getSku();

        $query = "SELECT * FROM `tsht_purchase_order` inner join tsht_purchase_order_item
            on tsht_purchase_order_item.po_id = tsht_purchase_order.po_id
            where sku like '".$sku."%"."'
            ORDER BY `tsht_purchase_order`.`created_at` DESC limit 1";
        $results = $readConnection->fetchAll($query);

        if(count($results) > 0){
            $Po_item = $results[0];
            $cost = $Po_item['price'];
        }

        if($cost == 0){
            //get from supplier product data
            $Po_product = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()
                ->addFieldToFilter('product_id', $product->getId());
            //get the highest cost
            $priceArray = [];
            foreach ($Po_product as $sProduct){
                array_push($priceArray, $sProduct->getLastCost());
            }
            if(count($priceArray)) {
                $cost = max($priceArray);
            }else{
                $cost = 0;
            }
        }
        return $cost;
    }

    public function isAllowPermission(){
        if(Mage::getSingleton('admin/session')->isAllowed('admin/erp/inventory/instock') && !Mage::getSingleton('admin/session')->isAllowed('admin/erp/inventory/item')){
                return true;
        }
        return false;
    }

    public function getGroupSKU($sku){
        $skuParts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
        foreach ($skuParts as $key => $value){
            if($key == 0) continue;
            $value = str_replace('in.', '', $value);
            foreach (self::CONVERT_SIZE as $string => $size){
                $value = str_replace($string, $size, $value);
            }
            if(is_numeric($value)) unset($skuParts[$key]);
        }
        return implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $skuParts);
    }

    public function updateLastCostFromPO($sku, $po_id){
        $instock = Mage::getModel('opentechiz_inventory/instock')->load($sku, 'sku');
        //set group sku for new item
        if(!$instock->getGroupSku()){
            $groupSku = $this->getGroupSKU($sku);
            $instock->setGroupSku($groupSku)->save();
        }

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $po = Mage::getModel('opentechiz_purchase/purchase')->load($po_id);
        $poItem = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()
            ->addFieldToFilter('po_id', $po_id)
            ->addFieldToFilter('sku', $sku)->getFirstItem();

        if($poItem && $poItem->getId()){
            $cost = (double)$poItem->getPrice();
            $date = explode(' ', $po->getUpdatedAt())[0];
            $query = "update `tsht_inventory_instock_product`
                      set last_cost = ".$cost.", last_cost_date = '".$date."' where group_sku = '".$instock->getGroupSku()."'";
            $writeConnection->query($query);
            //update last cost,last cost date to tsht_purchase_product_supplier
            $product_supplier = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()->addFieldToFilter('product_id', $poItem->getProductId())
                                ->addFieldToFilter('sup_id', $po->getSupplierId())->getFirstItem();
            $data['last_cost'] = $cost;
            $data['last_cost_date'] = $po->getUpdatedAt();
            $data['last_po_id'] = $po->getPoId();
            $product_supplier->addData($data)->save(); 
        }
    }

    public function getLastCost($sku){
        $sku = $this->getGroupSKU($sku);
        $instock = Mage::getModel('opentechiz_inventory/instock')->load($sku, 'group_sku');
        return $instock->getLastCost();
    }

    public function getLastCostDate($sku){
        $sku = $this->getGroupSKU($sku);
        $instock = Mage::getModel('opentechiz_inventory/instock')->load($sku, 'group_sku');
        return $instock->getLastCostDate();
    }
    public function imageUrlFromOption($productData, Mage_Catalog_Model_Product $product)
    {
        $sku = $product->getSku();
        if (Mage::helper('personalizedproduct')->isPersonalizedProduct($product)) {
            foreach ($productData['options'] as $option) {
                $option['value'] = strtolower($option['value']);
                if ($option['option_type'] == 'stone') {
                    $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . str_replace(' ', '', $option['value']);
                }
            }
            foreach ($productData['options'] as $option) {
                $option['value'] = strtolower($option['value']);
                if ($option['option_type'] == 'gold') {
                    $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . str_replace(' ', '', $option['value']);
                }
            }
            $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . '1';
            $product->setSku($sku);
        }

        return Mage::helper('catalog/image')->init($product, 'thumbnail');
    }

    public function getCurrentStoneCost($options, $product)
    {
        $stoneOptionModel = Mage::getModel('personalizedproduct/price_stone');
        $purchaseOrderStoneModel = Mage::getModel('opentechiz_purchase/purchasestone');

        if (isset($options['options']))
            $optionArray = $options['options'];
        else
            $optionArray = [];
        $totalPrice = 0;

        foreach ($optionArray as $option) {
            if ($option['option_type'] == 'stone') {
                $canGetDataFromProductOption = false;
                foreach ($product->getOptions() as $productOption) {
                    if ($productOption->getId() == $option['option_id']) {
                        $value = $productOption->getValueById($option['option_value']);
                        $stoneSku = $value->getSku();
                        $canGetDataFromProductOption = true;
                        break;
                    }
                }

                if (!$canGetDataFromProductOption) {
                    $stoneSku = strtolower($option['value']);
                }

                $quality = '';
                if (strpos($stoneSku, 'diamond') !== false) {
                    $type = str_replace('diamond', '', $stoneSku);
                    if (in_array($type, OpenTechiz_PersonalizedProduct_Helper_Data::QUALITY)) {
                        $quality = $type;
                    }
                }

                $query = 'stone.stone_id = main_table.stone_id and main_table.option_id = "' . $option['option_id'] . '" and stone.stone_type = "' . $stoneSku . '"';
                if ($quality != '') {
                    $query .= 'and stone.quality = "' . $quality . '"';
                }

                $collection = $stoneOptionModel->getCollection()->join(array('stone' => 'opentechiz_material/stone'), $query, '*');

                foreach ($collection as $requiredStone) {
                    $stoneId = $requiredStone->getStoneId();
                    $lastPurchaseOrder = $purchaseOrderStoneModel->getCollection()->addFieldToFilter('stone_id', $stoneId)
                                    ->setOrder('po_stone_id', 'desc')->getFirstItem();
                    $totalPrice += (float) ($lastPurchaseOrder->getPrice() * $requiredStone->getQuantity());
                }
            }
        }
        return $totalPrice;
    }

    public function generateBarcode($id)
    {
        $date = Mage::getModel('core/date')->gmtDate('Y-m-d');
        $date = str_replace('-', '', $date);
        $date = substr($date, 2);
        while (strlen($id) < 7)
            $id = '0' . $id;
        if (strlen($id) >= 7)
            $id = substr($id, -7);
        return $date . $id;
    }

    public function getMessage($data){
        $type =0;
        $link_order ="";
        $link_po_order ="";
        $link_return ="";
        $link_from_sku ="";
        $link_to_sku ="";
        $link_product_request ="";
        $link_item ="";
        $link_po_return_id ="";
        
        if(isset($data["type"])){
            $type = (int)$data["type"];
        }
        //can understand type by const STOCK_PRODUCT_MOVEMENT_TYPE
        if(isset( $data['order_id'])){
            $url = Mage::getModel("core/url")->getUrl("adminhtml/sales_order/view", array("order_id" => $data['order_id']));
            $link_order = "<a href=\"{$url}\" target=\"_blank\"> #{$data['increment_id']}</a>";
        }
        if(isset($data['po_id'])){
            $url = Mage::getModel("core/url")->getUrl("adminhtml/purchase_product/edit/", array("id" => $data['po_id']));
            $link_po_order = "<a href=\"{$url}\" target=\"_blank\"> #{$data['po_increment_id']}</a>";
        }
        if(isset($data['return_id'])){
            $url = Mage::getModel("core/url")->getUrl("adminhtml/sales_returns/edit/", array("id" => $data['return_id']));
            $link_return = "<a href=\"{$url}\" target=\"_blank\"> #{$data['return_increment_id']}</a>";
        }
        if(isset($data['from_sku'])){
            $url = Mage::getModel('core/url')->getUrl("adminhtml/instockProduct/index", array("filter" => base64_encode(http_build_query(array(
                'sku' => $data['from_sku'],
            )))));
            $link_from_sku = "<a href=\"{$url}\" target=\"_blank\"> {$data['from_sku']}</a>";
        }
        if(isset($data['to_sku'])){
            $url = Mage::getModel('core/url')->getUrl("adminhtml/instockProduct/index", array("filter" => base64_encode(http_build_query(array(
                'sku' => $data['to_sku'],
            )))));
            $link_to_sku = "<a href=\"{$url}\" target=\"_blank\"> {$data['to_sku']}</a>";
        }
        if(isset($data['production_request_id'])){
            $url = Mage::getModel('core/url')->getUrl("adminhtml/request_product/index/", array("filter" => base64_encode(http_build_query(array(
                'personalized_id' => $data['production_request_id'],
            )))));
            $link_product_request = "<a href=\"{$url}\" target=\"_blank\"> #{$data['production_request_id']}</a>";
        }
        if(isset($data['inventory_item_id']) && !empty($data['inventory_item_id'])){
            $url = Mage::getModel('core/url')->getUrl("adminhtml/inventory_item/index/", array("filter" => base64_encode(http_build_query(array(
                'item_id' => $data['inventory_item_id'],
                'sku' => $data['sku_variant']
            )))));
            $link_item = "<a href=\"{$url}\" target=\"_blank\"> #{$data['inventory_item_id']}</a>";
        }
        if(isset($data['po_return_id'])){
            $url = Mage::getModel("core/url")->getUrl("adminhtml/purchase_return/edit/", array("id" => $data['po_id']));
            $link_po_return_id = "<a href=\"{$url}\" target=\"_blank\"> {$data['po_return_id']}</a>";
        }
        switch ($type) {
            case 3:
            case 19:
                 $message = "Added from purchase order {$link_po_order}";
                 return $message;
                 break;
            case 4:
                 $message = "Added from production request {$link_product_request}";
                 return $message;
                 break;
            case 5:
                 if(!$data['return_increment_id'] || !$data['increment_id']){
                     $message = "Added to stock from repair process";
                 }else {
                     $message = "Added to stock from return {$link_return} on order {$link_order}";
                 }
                 return $message;
                 break;
            case 6:
                 $message = "Converted from item SKU {$link_from_sku} to item SKU {$link_to_sku}";
                 return $message;
                 break;
            case 7:
                 $message = "Add to stock manually";
                 return $message;
                 break;
            case 8:
                 $message = "Remove from stock manually";
                 return $message;
                 break;
            case 9:
                 $message = "Added to stock from cancel order {$link_order}";
                 return $message;
                 break;
            case 10:
                 $message = "Change state from all item page {$link_item}";
                 return $message;
                 break;
            case 11:
                 $message = "Item taken for order {$link_order}";
                 return $message;
                 break;
            case 12:
                 $message = "Release reserved item for order {$link_order}";
                 return $message;
                 break;
            case 13:
                 $message = "Locked item for order {$link_order}";
                 return $message;
                 break;
            case 14:
                 $message = "Added to stock when change billing state to fulfillment from order {$link_order}";
                 return $message;
                 break;
            case 15:
                 $message = "Added to stock from instock waiting items";
                 return $message;
                 break;
            case 16:
                 $message = "Converted from item SKU {$link_from_sku} to item SKU {$link_to_sku}";
                 return $message;
                 break;
            case 17:
                 $message = "Added to stock from creditmemo on order {$link_order} when not shipped";
                 return $message;
                 break;
            case 18:
                 $message = "Added to stock when change fulfillment state to Order Processing from order {$link_order}";
                 return $message;
                 break;
            case 20:
                 $message = "Item shipped for order {$link_order}";
                 return $message;
                 break;
            case 21:
                 $message = "Added to stock when update payment record to zero from {$link_order}";
                 return $message;
                 break;
            case 22:
                 $message = "Reduce stock after purchase return {$link_po_return_id}";
                 return $message;
                 break;
            default:
                 $message = "";
                 break;
         }
    }

    public function convertSizeString($string){
        foreach (self::CONVERT_SIZE as $key => $value){
            $string = str_replace($key, $value, $string);
        }
        return $string;
    }

    public function itemNeedResize($itemSku, $requestedSku){
        //item sku
        $skuparts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $itemSku);
        foreach ($skuparts as $skuKey => $skuValue){
            $skuValue = str_replace('in.', '', $skuValue);
            $skuValue = $this->convertSizeString($skuValue);
            $skuparts[$skuKey] = $skuValue;
        }
        //requested sku
        $orderedSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $requestedSku);
        foreach ($orderedSku as $skuKey => $skuValue){
            $skuValue = str_replace('in.', '', $skuValue);
            $skuValue = $this->convertSizeString($skuValue);
            $orderedSku[$skuKey] = $skuValue;
        }

        //in case sku in inventory changed
        if(count($skuparts) != count($orderedSku)){
            return true;
        }

        //requested size(s)
        $requestSizes = [];
        while(count($orderedSku) > 1 && is_numeric($orderedSku[count($orderedSku) - 1])) {
            $requestSizes[count($orderedSku) - 1] = $orderedSku[count($orderedSku) - 1];
            unset($orderedSku[count($orderedSku) - 1]);
        }

        $neededResize = true;

        if(count($requestSizes)) {
            foreach ($requestSizes as $pos => $requestSize){
                $size = $skuparts[$pos];
                if(!is_numeric($size) || $size <= 0){
                    $neededResize = false;
                }
                if ($size == $requestSize) {
                    $neededResize = false;
                }
            }
        }else{
            $neededResize = false;
        }

        return $neededResize;
    }

    public function getAvailableSize($requestedSku, $getClosest = false){
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');

        //get number part as size
        $originalParts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $requestedSku);
        $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $requestedSku);
        foreach ($parts as $skuKey => $skuValue){
            $skuValue = str_replace('in.', '', $skuValue);
            $skuValue = $this->convertSizeString($skuValue);
            $parts[$skuKey] = $skuValue;
        }
        //get requested size(s) from requested SKU
        $requestSizes = [];
        while(count($parts) > 1 && is_numeric($parts[count($parts) - 1])) {
            $requestSizes[count($parts) - 1] = $parts[count($parts) - 1];
            unset($parts[count($parts) - 1]);
        }
        //glue array to get group sku
        $orderSku = implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $parts);
        if(count($requestSizes)){
            $orderSku .= '\_%';
        }
        //get all items with available qty
        $result = $conn->fetchAll("SELECT * FROM tsht_inventory_instock_product where sku like '" . $orderSku . "' and qty > 0 and on_hold < qty");

        //if need closest and exist exact sku, return
        if($getClosest) {
            $sameSize = array();
            foreach ($result as $key => $value) {
                if ($value['sku'] == $requestedSku) {
                    $sameSize[$key] = $value;
                    return $sameSize;
                }
            }
        }

        $sizeFinal = array();
        if(count($originalParts) > 1 && count($requestSizes) > 0) { //skip if sku no need to check size
            foreach ($requestSizes as $pos => $requestSize) { //loop requested sizes to get closest for each
                $sizeDiff = 2.0;
                foreach ($result as $key => $value) {
                    //get number part as size
                    $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $value['sku']);
                    foreach ($parts as $skuKey => $skuValue) {
                        $skuValue = str_replace('in.', '', $skuValue);
                        $skuValue = $this->convertSizeString($skuValue);
                        $parts[$skuKey] = $skuValue;
                    }
                    //continue if item doesn't have correct sku format
                    if (!isset($parts[$pos])) {
                        unset($result[$key]);
                        continue;
                    }

                    //continue if a size doesn't match with final size array
                    if ($getClosest) {
                        $continue = false;
                        foreach ($sizeFinal as $skuKey => $skuValue) {
                            if ($parts[$skuKey] != $skuValue) {
                                $continue = true;
                                break;
                            }
                        }
                        if($continue) continue;
                    }

                    $size = $parts[$pos];

                    if (abs($requestSize - $size) > $sizeDiff) { //if size bigger than allowed size diff
                        unset($result[$key]);  //remove if size cannot be re-sized
                    } elseif ($getClosest) {
                        $sizeDiff = abs($requestSize - $size); //reduce size diff every time size get closer to requested size
                        $sizeFinal[$pos] = $size;  //add closest size to final size array
                    }
                }
            }
        }

        return $result;
    }

    public function countReleasedItem($item_id){
        $qty_released = 0;
        $itemOrders = Mage::getModel('opentechiz_inventory/order')->getCollection()->addFieldToFilter('order_item_id', $item_id);
        if(count($itemOrders)){
            $itemIds = [];
            foreach ($itemOrders as $itemOrder){
                array_push($itemIds, $itemOrder->getItemId());
            }
            $qty_released = Mage::getModel('opentechiz_inventory/item')->getCollection()
                ->addFieldToFilter('item_id', array('in' => $itemIds))
                ->getSize();
        }

        return $qty_released;
    }
    public function checkItemInstock($product_id){
        
        $collection = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('product_id',array('eq'=>$product_id))->addFieldToFilter('qty',array('gt'=>0));

        return $collection->count();
    }
}
