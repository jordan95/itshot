<?php

class OpenTechiz_SalesExtend_Adminhtml_ItemController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales')
            ->_title($this->__('Order Item Grid'));
        return $this;
    }


    public function indexAction() {
        $this->_initAction()
            ->renderLayout();
    }


    /**
     * MassRequestToProcess action
     */

    public function massRequestToProcessAction()
    {
        $Ids = $this->getRequest()->getParam('item');

        if(!is_array($Ids)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s) to process'));
        } else {
            $flag = true;

            foreach ($Ids as $orId) {
                $orderItem = Mage::getSingleton('sales/order_item')->load($orId);
                $order = Mage::getSingleton('sales/order')->load($orderItem->getOrderId());

                if($order->getOrderType() == 1 && !Mage::helper('opentechiz_salesextend')->orderHasStatus(15, $order)){
                    Mage::getSingleton('adminhtml/session')->addError($this->__('This layaway order is not paid enough to be able to process'));
                    $flag = false;
                    break;
                }

                if ($orderItem->getIsRequestedToProcess() == 1 && $orderItem->getRemainingQty() == 0) {
                    Mage::getSingleton('adminhtml/session')->addError($this->__('There are record(s) that are already requested to process and have no remaining quantity'));
                    $flag = false;
                    break;
                }
//                elseif (Mage::getSingleton('sales/order')->load($orderItem->getOrderId())->getOrderApproved() != 1) {
//                    Mage::getSingleton('adminhtml/session')->addError($this->__('Order needs to be approved to request item(s) processing'));
//                    $flag = false;
//                    break;
//                }
                $costCheck = Mage::helper('opentechiz_salesextend')->checkProfitOrder($order);
                if ($costCheck != '') {
                    Mage::getSingleton('adminhtml/session')->addError($this->__('There are record(s) that has insufficient profit compare to Last Cost: '.$costCheck));
                    $flag = false;
                    break;
                }
            }

            try {
                if($flag == true) {
                    $model = Mage::getModel('opentechiz_salesextend/item');
                    foreach ($Ids as $orId) {
                        $model->requestToProcess($orId);

                        $orderItem = Mage::getSingleton('sales/order_item')->load($orId);
                        $orderItem->setIsRequestedToProcess(1)->save();
                        $admin = Mage::getSingleton('admin/session')->getUser();
                        if($admin->getId()){
                            Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'MassAction request to process order item #'.$orId));
                        }
                    }

                    Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Total of %d record(s) were updated.', count($Ids)));

                    //update order stage, internal status

                    $orderId = Mage::getSingleton('sales/order_item')->load($Ids[0])->getOrderId();
                    $order = Mage::getModel('sales/order')->load($orderId);
                    if($order->getOrderType() == 2){
                        $status = 17;
                    }else{
                        $status = 1;
                    }
                    if($order && $order->getId()){
                        Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 2, $status);
                    }
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        if(count($Ids)>0) {
            if(isset($order) && $order->getId()){
                $orderId = $order->getId();
            }
            if(!isset($orderId)) $orderId = Mage::getSingleton('sales/order_item')->load($Ids[0])->getOrderId();
            $this->_redirect('adminhtml/sales_order/view/order_id/' . $orderId);
        } else{
            $this->_redirect('adminhtml/sales_order/index');
        }
    }

    public function massAddNonPersonalizedStockRequestAction(){
        $Ids = $this->getRequest()->getParam('item');
        $model = Mage::getModel('opentechiz_salesextend/item');

        if(!is_array($Ids)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select checkbox'));
        } else {
            $flag = true;
            $selected = count($Ids);
            foreach ($Ids as $orId) {
                $order = Mage::getSingleton('sales/order_item')->load($orId);
                $productId = $order->getProductId();
                $product = Mage::getModel('catalog/product')->load($productId);

                if ($order->getIsRequestedToProcess() == 1) {
                    Mage::getSingleton('adminhtml/session')->addError($this->__('There are record(s) that are already requested to process'));
                    $flag = false;
                    continue;
                }
                if(Mage::helper('personalizedproduct')->isPersonalizedProduct($product)) {
                    Mage::getSingleton('adminhtml/session')->addError($this->__('Some selected record(s) is personalized product. Please use "Request To Process" action for those item(s)'));
                    $selected = $selected - 1;
                    continue;
                }
                if ($model->hasEnoughInstock($orId, $order->getQtyOrdered())) {
                    $model->addStockRequest($orId, $order->getQtyOrdered());
                } else{
                    $flag = false;
                    $selected = $selected - 1;
                }
            }
            if($flag == true && $selected > 0){
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Total of %d record(s) were updated.', $selected));
            } else{
                Mage::getSingleton('adminhtml/session')->addError($this->__('Number of item(s) in stock is not enough for some request(s). Please check in stock again'));
            }
        }
        $this->_redirect('*/*/index');
    }

    public function skipCostCheckAction(){
        $params = $this->getRequest()->getParams('item');
        $orderId = $params['order_id'];
        $action = $params['action'];
        $order = Mage::getModel('sales/order')->load($orderId);
        if($order && $order->getId()){
            foreach ($order->getAllItems() as $item){
                $item->setSkipCostCheck($action)->save();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Updated.'));
        }else{
            Mage::getSingleton('adminhtml/session')->addError($this->__('Order not found.'));
        }
        
        $this->_redirect('adminhtml/sales_order/view/order_id/' . $orderId);
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/sales/order/actions/skip_cost_check');
    }
}