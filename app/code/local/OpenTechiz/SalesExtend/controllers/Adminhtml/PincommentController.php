<?php
include_once('Mage/Adminhtml/controllers/Sales/OrderController.php');
class OpenTechiz_SalesExtend_Adminhtml_PincommentController extends Mage_Adminhtml_Sales_OrderController
{
    public function updateAction(){
        try {
            $response = false;
            $order_id = $this->getRequest()->getParam('order_id');
            $id = $this->getRequest()->getParam('id');
            $comment_data = Mage::getModel('sales/order_status_history')->getCollection()->addFieldToFilter("parent_id",$order_id);

            foreach($comment_data->getData() as $k => $val){
                if($val['has_pin_comment']){
                    $comment = Mage::getModel('sales/order_status_history')->load($val['entity_id']);
                    $comment->setHasPinComment(0)->save();
                }
                if($val['entity_id']  == $id){
                    $comment = Mage::getModel('sales/order_status_history')->load($val['entity_id']);
                    $comment->setHasPinComment(1)->save();
                }
                
            }
            $this->_initOrder();
            $this->loadLayout('empty');
            $this->renderLayout();

        } catch (Mage_Core_Exception $e) {
            $response = array(
                'error'     => true,
                'message'   => $e->getMessage(),
            );
        } catch (Exception $e) {
            $response = array(
                'error'     => true,
                'message'   => $this->getMwHelper()->__('Failed to pin comment.')
            );
        }

        if (is_array($response)) {
            $response = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setBody($response);
        }
    }
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions');
    }
}