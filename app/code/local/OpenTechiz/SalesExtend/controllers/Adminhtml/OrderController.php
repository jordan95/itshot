<?php

class OpenTechiz_SalesExtend_Adminhtml_OrderController extends Mage_Adminhtml_Controller_Action
{
    public function changeShipByAction(){
        $params = $this->getRequest()->getParams();
        $date = $params['date'];
        $orderId = $params['order_id'];

        $date = strtotime($date);
        $date = date('Y-m-d', $date);

        $order = Mage::getSingleton('sales/order')->load($orderId);

        try{
            $order->setShipBy($date)->save();
            $response=array('error'=>false);
            Mage::helper('opentechiz_salesextend')->updateOrderGrid($order, 'ship_by');
        } catch (Exception $e) {
            $response=array('items'=>$e->getMessage(),'error'=>true);
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function changeDeliverByAction(){
        $params = $this->getRequest()->getParams();
        $date = $params['date'];
        $orderId = $params['order_id'];

        $date = strtotime($date);
        $date = date('Y-m-d', $date);

        $order = Mage::getSingleton('sales/order')->load($orderId);

        try{
            $order->setDeliverBy($date)->save();
            $response=array('error'=>false);
            Mage::helper('opentechiz_salesextend')->updateOrderGrid($order, 'deliver_by');
        } catch (Exception $e) {
            $response=array('items'=>$e->getMessage(),'error'=>true);
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function changeShippingMethodAction(){
        $params = $this->getRequest()->getParams();
        $orderId = $this->getRequest()->getParam('order_id', false);

        if(empty($params['shipping']) || !$orderId){
           $this->_getSession()->addError("Please select one Actual Shipping Information");
           return $this->_redirect('adminhtml/sales_order_shipment/new', array('order_id'=> $orderId));
        }
        $shipping = $params['shipping']['shipping_method'];
        $order = Mage::getSingleton('sales/order')->load($orderId);

        try{
            $order->setData('shipping_method',$shipping);
            $order->save();
        }  catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        $this->_redirect('adminhtml/sales_order_shipment/new/', array('order_id'=> $order->getId()));
    }

    public function changeOrderManagementDataAction(){
        $params = $this->getRequest()->getParams();
        $order = $this->getOrder();
        try{
            //register to update pin comment.
            Mage::register('register_before_order_stage', $order->getOrderStage());
          
            $from_stage = $order->getOrderStage();
            //start check if can not change stage alert to user.
            $prevent_change_Stage ='';
            $prevent_change_Stage = Mage::helper('opentechiz_salesextend')->PreventChangeOrderStage($order, $params['order_stage']);
            if($prevent_change_Stage !=''){
                Mage::getSingleton('adminhtml/session')->addError($prevent_change_Stage);
                $this->_redirect('*/sales_order/view/order_id/'.$order->getId());
                return;
            }
            //end check if can not change stage alert to user.
            if(isset($params['source'])) {
                $order->setData('source', $params['source']);
            }

            if(isset($params['ship_by'])) {
                $shipbyDate = $params['ship_by'];
                $shipbyDate = strtotime($shipbyDate);
                $shipbyDate = date('Y-m-d', $shipbyDate);
                $order->setShipBy($shipbyDate);
            }

            if(isset($params['deliver_by'])) {
                $deliverbyDate = $params['deliver_by'];
                $deliverbyDate = strtotime($deliverbyDate);
                $deliverbyDate = date('Y-m-d', $deliverbyDate);
                $order->setDeliverBy($deliverbyDate);
            }
            // update confirm address customer task #2003
            Mage::dispatchEvent('opentechiz_update_confirm_address_customer_before_save', array(
                'order'=>$order
            ));
            if(isset($params['salesperson']) || isset($params['fulfilledby'])) {
                Mage::dispatchEvent('opentechiz_update_user_create_and_fulfilled_before_save', array(
                    'order'=>$order
                ));
            }
            
            $order->save();

            //avoid exception
            if(!isset($params['verification'])) $params['verification'] = null;
            if(!isset($params['order_stage'])) $params['order_stage'] = null;
            if(!isset($params['internal_status'])) $params['internal_status'] = null;
            if($order->getOrderStage() == $params['order_stage']) {
                Mage::helper('opentechiz_salesextend')->updateTermAndVerification($order, null, $params['verification'], true);
                //get order
                if($order->getOrderType() == 1 && $order->getInternalStatus() == 15
                    && !in_array($params['verification'], OpenTechiz_SalesExtend_Helper_Data::VERIFICATION_STATUS_NOT_ALLOWED_TO_MOVE_TO_PROCESSING)){
                    Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, null, 25);
                    //prevent change stage status back
                    if($params['order_stage'] == $order->getOrderStage()) $params['order_stage'] = null;
                    if($params['internal_status'] == 15 && $order->getInternalStatus() == 25) $params['internal_status'] = null;
                }
            }
            //check if customer is fraudster
            $customer_id = $order->getCustomerId();
            $customer = Mage::getModel('customer/customer')->load($customer_id);
            $isFraudster = $customer->getIsFraudster();
            if($isFraudster && !$order->getManagementApproved()
                && $params['order_stage'] == 2 && $order->getOrderStage() == 1){
                $error = 'Possible Fraud order, please contact Management for approval.';
            }else {
                if(isset($params['order_stage'])) {
                    $error = Mage::helper('opentechiz_salesextend')->changeOrderStage($order, $params['order_stage']);
                }else{
                    $error = '';
                }
            }
            if($error != ''){
                Mage::getSingleton('adminhtml/session')->addError($error);
            }elseif($order->getInternalStatus() != $params['internal_status']) {
                $selectedStatus = $params['internal_status'];
                if($selectedStatus == 2){
                    $requestCollection = Mage::getModel('opentechiz_production/process_request')->getCollection()
                        ->addFieldToFilter('order_id', $order->getId())->addFieldToFilter('status', 0);
                    if(count($requestCollection) > 0) {
                        $canManufacture = true;
                        foreach ($order->getAllItems() as $item) {
                            if (!Mage::helper('personalizedproduct')->canProduceProduct($item->getProductId())) {
                                $canManufacture = false;
                            }
                        }
                        if ($canManufacture == true) {
                            foreach ($requestCollection as $processRequest){
                                Mage::getModel('opentechiz_production/process_request')->createProductionRequest($processRequest->getOrderItemId());
                                $processRequest->setStatus(1)->save();
                            }
                        }else{
                            $selectedStatus = 1;
                            Mage::getSingleton('adminhtml/session')->addError('Some Item(s) cannot be placed in production. Please process manually.');
                        }
                    }
                }
                //if select engrave/resize status
                if($selectedStatus == 3 && !Mage::helper('opentechiz_salesextend')->orderHasStatus(3, $order)){
                    $items = $order->getAllItems();
                    //delete all existing stock request
                    foreach ($items as $item){
                        $requestCollection = Mage::getModel('opentechiz_inventory/inventory_instockRequest')->getCollection()
                            ->addFieldToFilter('order_item_id', $item->getId());
                        if(count($requestCollection) > 0){
                            foreach ($requestCollection as $request){
                                //release stock
                                Mage::getModel('opentechiz_inventory/inventory_instockRequest')->releaseItem($request->getId());
                                $request->delete();
                            }
                        }
                    }
                    //add stock request
                    foreach ($items as $item) {
                        $productId = $item->getProductId();
                        if(!$productId || !Mage::getModel('catalog/product')->load($productId) || $item->getProductType() == 'virtual'){
                            continue;
                        }
                        $totalQty = (int)$item->getQtyOrdered() - $item->getQtyRefunded() - $item->getQtyCanceled();
                        for ($i = 0; $i < $totalQty; $i++){
                            $create = Mage::getModel('opentechiz_production/process_request')->createStockProductRequest($item->getId(),$order->getId());
                            if (!$create) {
                                return 'Something\'s wrong when creating stock request.';
                                break;
                            }
                        }
                    }
                    //accept said stock request
                    foreach ($items as $item) {
                        $productId = $item->getProductId();
                        if(!$productId || !Mage::getModel('catalog/product')->load($productId) || $item->getProductType() == 'virtual'){
                            continue;
                        }
                        $requestCollection = Mage::getModel('opentechiz_inventory/inventory_instockRequest')->getCollection()
                            ->addFieldToFilter('order_item_id', $item->getId());
                        foreach ($requestCollection as $request){
                            $error = Mage::getModel('opentechiz_inventory/inventory_instockRequest')->takeItem($request->getId());
                            if($error != ''){
                                return $error;
                                break;
                            }
                            $request->setStatus(1)->save();
                        }
                    }
                    foreach ($items as $item) {
                        $productId = $item->getProductId();
                        if(!$productId || !Mage::getModel('catalog/product')->load($productId) || $item->getProductType() == 'virtual'){
                            continue;
                        }
                        //set requested to process true
                        $item->setIsRequestedToProcess(1)->save();
                        //delete all process request
                        $processRequests = Mage::getModel('opentechiz_production/process_request')->getCollection()
                            ->addFieldToFilter('order_item_id', $item->getId());
                        if (count($processRequests) > 0) {
                            foreach ($processRequests as $processRequest) {
                                $processRequest->delete();
                            }
                        }
                    }
                }
                //check for verification status before move to process layaway
                if($selectedStatus == 25 && $order->getOrderType() == 1){
                    if(in_array($order->getVerification(), OpenTechiz_SalesExtend_Helper_Data::VERIFICATION_STATUS_NOT_ALLOWED_TO_MOVE_TO_PROCESSING)){
                        Mage::getSingleton('core/session')->addError(Mage::helper('core')->__('Order not verified. To proceed it must be approved by Management.'));
                        $this->_redirect('*/*/*', array('order_id' => $order->getId()));
                        return;
                    }
                }
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, null, $selectedStatus, true, true);

            }
            

        }  catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        $this->_redirect('*/sales_order/view/order_id/'.$order->getId());
    }

    public function getOrder(){
        if(Mage::registry('current_order')){
            return Mage::registry('current_order');
        }else{
            $params = $this->getRequest()->getParams();
            $orderId = $params['order_id'];
            $order = Mage::getSingleton('sales/order')->load($orderId);
            Mage::register('current_order', $order);
            return $order;
        }
    }

    public function changeStageAction(){
        $params = $this->getRequest()->getParams();
        $stage = $params['stage'];
        $orderId = $params['order_id'];

        $order = Mage::getSingleton('sales/order')->load($orderId);

        try{
            $order->setOrderStage($stage)->save();
            $response=array('error'=>false);
            Mage::helper('opentechiz_salesextend')->updateOrderGrid($order, 'order_stage');
        } catch (Exception $e) {
            $response=array('items'=>$e->getMessage(),'error'=>true);
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function changeInternalStatusAction(){
        $params = $this->getRequest()->getParams();
        $status = $params['status'];
        $orderId = $params['order_id'];
        $order = Mage::getSingleton('sales/order')->load($orderId);
        try{
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $table = $resource->getTableName('sales/order');
            $query = "UPDATE {$table} SET internal_status = '".$status."' WHERE entity_id = '" .$orderId."'";
            $writeConnection->query($query);
            Mage::helper('opentechiz_salesextend')->updateOrderGrid($order, 'internal_status');

            //if is waiting for customer, change order verification status to under verification
            if((OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS[$status] == 'Waiting on Customer'
                || OpenTechiz_SalesExtend_Helper_Data::LAYAWAY_ORDER_INTERNAL_STATUS[$status] == 'Waiting on Customer')
                && $order->getVerification() == 0){
                $query = "UPDATE {$table} SET verification = 8 WHERE entity_id = '" .$orderId."'";
                $writeConnection->query($query);
                $query = "UPDATE tsht_sales_flat_order_grid SET verification = 8 WHERE entity_id = '" .$orderId."'";
                $writeConnection->query($query);
            }
            $response=array('error'=>false);
        } catch (Exception $e) {
            $response=array('items'=>$e->getMessage(),'error'=>true);
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function managementApproveAction(){
        $params = $this->getRequest()->getParams();
        $orderId = $params['order_id'];
        $order = Mage::getSingleton('sales/order')->load($orderId);
        try{
            //change order stage to fulfilment
            Mage::helper('opentechiz_salesextend')->changeOrderStage($order, 2);
            //save order to be management approved
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $table = $resource->getTableName('sales/order');
            $query = "UPDATE {$table} SET management_approved = 1 WHERE entity_id = '" .$orderId."'";
            $writeConnection->query($query);
            //get user name
            $admin = Mage::getSingleton('admin/session')->getUser();
            $adminName = $admin->getFirstname().' '.$admin->getLastname();
            //add order comment
            $order->addStatusHistoryComment('Customer is marked as a Fraudster, this order was approved for Fulfillment by '.$adminName);
            $order->save();
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        $this->_redirect('*/sales_order/view/order_id/'.$orderId);
    }

    public function createShowroomPickupAction(){
        $params = $this->getRequest()->getParams();
        $orderId = (int)$params['order_id'];
        $order = Mage::getSingleton('sales/order')->load($orderId);

        $itemQty = array();
        foreach ($order->getAllItems() as $orderItem) {
            if ($orderItem->getQtyToShip() && !$orderItem->getIsVirtual()) {
                $itemQty[$orderItem->getId()] = $orderItem->getQtyToShip();
            }
        }

        //set shipping method to store pickup
        $order->setData('shipping_method','storepickup_storepickup');
        $order->save();

        Mage::getModel('sales/service_order', $order)
            ->prepareShipment($itemQty);
        $shipment = new Mage_Sales_Model_Order_Shipment_Api();
        $shipment->create($order->getIncrementId(), $itemQty, 'Shipment created through ShipMailInvoice', true, true);

        Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 5, 28, true, true);

        foreach ($order->getAllItems() as $orderItem) {
            foreach ($itemQty as $itemId => $qtyShipped){
                if($orderItem->getId() == $itemId){
                    $orderItem->setQtyShipped($qtyShipped)->save();
                }
            }
        }

        $this->_redirect('*/sales_order/view/order_id/'.$orderId);
    }

    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            case 'changeordermanagementdata':
                $aclResource = 'sales/mageworx_ordersedit';
                break;
            default:
                $aclResource = 'sales/order/actions';
                break;
        }

        return Mage::getSingleton('admin/session')->isAllowed($aclResource);
    }
}