<?php

class OpenTechiz_SalesExtend_Adminhtml_FraudsterController extends Mage_Adminhtml_Controller_Action
{
    public function massFraudAction(){
        $customersIds = $this->getRequest()->getParam('customer');
        $status = $this->getRequest()->getParam('status');
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        if(!is_array($customersIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select customer(s).'));

        } else {
            try {
                foreach ($customersIds as $customerId) {
//                    $customer = Mage::getModel('customer/customer')->load($customerId);
//                    $customer->setIsFraudster($status);
//                    $customer->save();
                    $query = "UPDATE tsht_customer_entity SET is_fraudster = '".$status."' WHERE entity_id = '" .$customerId."'";
                    $writeConnection->query($query);
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d record(s) were updated.', count($customersIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('adminhtml/customer/index');
    }

    public function markFraudAction(){
        $customerId = $this->getRequest()->getParam('customer_id');
//        $customer = Mage::getModel('customer/customer')->load($customerId);
//        $customer->setIsFraudster(1);
//        $customer->save();

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query = "UPDATE tsht_customer_entity SET is_fraudster = '1' WHERE entity_id = '" .$customerId."'";
        $writeConnection->query($query);

        $this->_redirect('adminhtml/customer/edit', array('id'=>$customerId));
    }

    public function unmarkFraudAction(){
        $customerId = $this->getRequest()->getParam('customer_id');
//        $customer = Mage::getModel('customer/customer')->load($customerId);
//        $customer->setIsFraudster(0);
//        $customer->save();

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query = "UPDATE tsht_customer_entity SET is_fraudster = '0' WHERE entity_id = '" .$customerId."'";
        $writeConnection->query($query);

        $this->_redirect('adminhtml/customer/edit', array('id'=>$customerId));
    }

    public function manualApproveCCAction(){
        $orderId = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($orderId);
        if($order->getId()) {
            $admin_user_session = Mage::getSingleton('admin/session');
            $adminuserId = $admin_user_session->getUser()->getUserId();
            $role = Mage::getModel('admin/user')->load($adminuserId)->getRole();
            if($role->getId() == 1) { //user is admin
                Mage::helper('opentechiz_salesextend')->updateTermAndVerification($order, 1, 3);
            }else {    //user is staff
                Mage::helper('opentechiz_salesextend')->updateTermAndVerification($order, 1, 6);
            }
            if(in_array($order->getOrderType(), array(0, 3))) {
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 1, 0);
            }
        }

        $this->_redirect('*/sales_order/view/order_id/'.$order->getId());
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('customer/fraudster');
    }
}