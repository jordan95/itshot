<?php
include_once('Mage/Adminhtml/controllers/Sales/OrderController.php');
class OpenTechiz_SalesExtend_Adminhtml_CommentController extends Mage_Adminhtml_Sales_OrderController
{
    public function updateformAction(){

        try {
            $response = false;
            $order_id = $this->getRequest()->getParam('order_id');
            $order = Mage::getModel('sales/order')->load($order_id);
            $response = array(
                'comment'     => $order->getOnestepcheckoutCustomercomment(),
            );
            $response = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setBody($response);

        } catch (Mage_Core_Exception $e) {
            $response = array(
                'error'     => true,
                'message'   => $e->getMessage(),
            );
        } catch (Exception $e) {
            $response = array(
                'error'     => true,
                'message'   => $this->getMwHelper()->__('Failed update comment.')
            );
        }

        if (is_array($response)) {
            $response = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setBody($response);
        }
    }
    public function updateAction(){
        try {
            $response = false;
            $order_id = $this->getRequest()->getParam('order_id');
            $comment = $this->getRequest()->getParam("comment");
            $order = Mage::getModel('sales/order')->load($order_id);
            if (Mage::getSingleton('admin/session')->getUser()->getUserId()) {
                $adminUserId = Mage::getSingleton('admin/session')->getUser()->getUserId();
            }else{
                $adminUserId = 0;
            }
            if($order->getId()){
                $order->setOnestepcheckoutCustomercomment($comment);
                $order->setOnestepcheckoutCustomercommentAt(now());
                $order->setOnestepcheckoutCustomercommentBy($adminUserId);
                $order->save();
            }

            $response = array(
                'comment_update'     => $order->getOnestepcheckoutCustomercomment(),
                'comment_update_at'  => Mage::helper('core')->formatDate($order->getOnestepcheckoutCustomercommentAt(), 'medium', true),
                'comment_update_by'  => Mage::getSingleton('admin/session')->getUser()->getName(),
            );
            $response = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setBody($response);

        } catch (Mage_Core_Exception $e) {
            $response = array(
                'error'     => true,
                'message'   => $e->getMessage(),
            );
        } catch (Exception $e) {
            $response = array(
                'error'     => true,
                'message'   => $this->getMwHelper()->__('Failed update comment.')
            );
        }

        if (is_array($response)) {
            $response = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setBody($response);
        }
    }
    public function cancelAction(){
        try {
            $response = false;
            $order_id = $this->getRequest()->getParam('order_id');
            $order = Mage::getModel('sales/order')->load($order_id);
            $user = Mage::getModel('admin/user')->load($order->getOnestepcheckoutCustomercommentBy());
            $comment_by = $user->getName();
            $response = array(
                'comment'        => $order->getOnestepcheckoutCustomercomment(),
                'comment_at'     => Mage::helper('core')->formatDate($order->getOnestepcheckoutCustomercommentAt(), 'medium', true),
                'comment_by'     => $comment_by,
            );
            $response = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setBody($response);

        } catch (Mage_Core_Exception $e) {
            $response = array(
                'error'     => true,
                'message'   => $e->getMessage(),
            );
        } catch (Exception $e) {
            $response = array(
                'error'     => true,
                'message'   => $this->getMwHelper()->__('Failed update comment.')
            );
        }

        if (is_array($response)) {
            $response = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setBody($response);
        }
    }
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/customer_comment');
    }
}