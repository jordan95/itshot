<?php

class OpenTechiz_SalesExtend_Adminhtml_ApproveController extends Mage_Adminhtml_Controller_Action
{

//    Approve multi orders
    public function approvedsAction()
    {
        $orderIds = $this->getRequest()->getParam('order_ids');

        if (!$orderIds) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                $admin = Mage::getSingleton('admin/session')->getUser();
                $k = 0;
                foreach ($orderIds as $orderId) {
                    $orderObject = Mage::getModel('sales/order')->load($orderId);
                    $invoiceIds = $orderObject->getInvoiceCollection();
                    if(count($invoiceIds) == 0){
                        $k ++;
                        continue;
                    }

                    Mage::getSingleton('sales/order')
                        ->load($orderId)
                        ->setData('order_approved', $this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();

                    if($admin->getId()){
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'MassAction approve order #'.$orderId));
                    }
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($orderIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/sales_order/index');
    }


    // Approve single order
    public function approvedAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $order = Mage::getSingleton('sales/order')->load($orderId);

        $approvable = true;
        $data = new Varien_Object();
        Mage::dispatchEvent('opentechiz_order_approve_before', array('order' => $order, 'object' => $data));

        $error = $data->getError();
        if($error != '') $approvable = false;
        if (!$order) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                if($approvable) {
                    $order->setData('order_approved', 1)
                        ->setIsMassupdate(true)
                        ->save();
                    $admin = Mage::getSingleton('admin/session')->getUser();
                    if ($admin->getId()) {
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'MassAction approve order #' . $orderId));
                    }

                    $this->_getSession()->addSuccess(
                        $this->__('Order was successfully updated')
                    );
                } else{
                    $this->_getSession()->addError(
                        $this->__('Order cannot be approved, these Item(s) has insufficient profit compare to Last Cost: ' .$error)
                    );
                }
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/sales_order/view/order_id/'.$order->getId());
    }

    public function changeSourceAction(){
        $orderIds = $this->getRequest()->getParam('order_ids');

        if (!$orderIds) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                $admin = Mage::getSingleton('admin/session')->getUser();
                foreach ($orderIds as $orderId) {
                    Mage::getSingleton('sales/order')
                        ->load($orderId)
                        ->setSource((int)$this->getRequest()->getParam('status'))
                        ->save();
                            
                    if($admin->getId()){
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'MassAction change source order #'.$orderId));
                    }
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($orderIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/sales_order/index');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions');
    }
}