<?php

require_once(Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Sales' . DS . 'Order' . DS . 'CreateController.php');

class OpenTechiz_SalesExtend_Adminhtml_Sales_Order_CreateController extends Mage_Adminhtml_Sales_Order_CreateController
{
    /**
     * Start order create action
     */
    public function startAction()
    {
        $this->_getSession()->clear();
        $this->_redirect('*/*', array('customer_id' => $this->getRequest()->getParam('customer_id'),
            'type' => $this->getRequest()->getParam('type')));
    }

    /**
     * Index page
     */
    public function indexAction()
    {
        $this->_title($this->__('Sales'))->_title($this->__('Orders'))->_title($this->__('New Order'));
        $this->_initSession();
        $this->loadLayout();

        $this->_setActiveMenu('sales/order')
            ->renderLayout();
    }

    /**
     * Loading page block
     */
    public function loadBlockAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();

        if(isset($params['type']) && $params['type'] == OpenTechiz_SalesExtend_Helper_Data::ORDER_TYPE_LAYAWAY) {
            if(isset($params['order']['payment_method'])) {
                if ($params['order']['payment_method'] != 'authorizenet') {
                    if (Mage::getSingleton('core/session')->getAllowPartialPayment() != 0)
                    Mage::getSingleton('core/session')->setAllowPartialPayment(0);
                } else {
                    if (Mage::getSingleton('core/session')->getAllowPartialPayment() != 1)
                    Mage::getSingleton('core/session')->setAllowPartialPayment(1);
                }
            }
        }else{
            Mage::getSingleton('core/session')->setAllowPartialPayment(0);
        }

        try {
            $this->_initSession()
                ->_processData();
        }
        catch (Mage_Core_Exception $e){
            $this->_reloadQuote();
            $this->_getSession()->addError($e->getMessage());
        }
        catch (Exception $e){
            $this->_reloadQuote();
            $this->_getSession()->addException($e, $e->getMessage());
        }


        $asJson= $request->getParam('json');
        $block = $request->getParam('block');

        $update = $this->getLayout()->getUpdate();
        if ($asJson) {
            $update->addHandle('adminhtml_sales_order_create_load_block_json');
        } else {
            $update->addHandle('adminhtml_sales_order_create_load_block_plain');
        }

        if ($block) {
            $blocks = explode(',', $block);
            if ($asJson && !in_array('message', $blocks)) {
                $blocks[] = 'message';
            }

            foreach ($blocks as $block) {
                $update->addHandle('adminhtml_sales_order_create_load_block_' . $block);
            }
        }
        $this->loadLayoutUpdates()->generateLayoutXml()->generateLayoutBlocks();
        $result = $this->getLayout()->getBlock('content')->toHtml();
        if ($request->getParam('as_js_varname')) {
            Mage::getSingleton('adminhtml/session')->setUpdateResult($result);
            $this->_redirect('*/*/showUpdateResult');
        } else {
            $this->getResponse()->setBody($result);
        }

    }

    public function reorderAction()
    {
        $this->_getSession()->clear();
        $orderId = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($orderId);
        $orderType = $order->getOrderType();
        $orderType = strtolower(OpenTechiz_SalesExtend_Helper_Data::ORDER_TYPE[$orderType]);
        if (!Mage::helper('sales/reorder')->canReorder($order)) {
            return $this->_forward('noRoute');
        }

        if ($order->getId()) {
            $order->setReordered(true);
            $this->_getSession()->setUseOldShippingMethod(true);
            $this->_getOrderCreateModel()->initFromOrder($order);

            $this->_redirect('*/*', array('type' => $orderType));
        }
        else {
            $this->_redirect('*/sales_order/');
        }
    }


    /**
     * Saving quote and create order
     *
     * @throws Mage_Core_Exception
     */
    public function saveAction()
    {
        $orderData = $this->getRequest()->getPost('order');
        $orderType = $this->getRequest()->getPost('order_type');
        $paymentData = $this->getRequest()->getPost('payment');

        if(!$orderType) $orderType = OpenTechiz_SalesExtend_Helper_Data::ORDER_TYPE_STANDARD;

        $isLayaway = ($orderType == OpenTechiz_SalesExtend_Helper_Data::ORDER_TYPE_LAYAWAY);
        if($isLayaway) {
            Mage::register('admin_order_is_layaway', true);
            if (isset($paymentData['method']) && $paymentData['method'] != 'authorizenet') {
                if (Mage::getSingleton('core/session')->getAllowPartialPayment() != 0)
                    Mage::getSingleton('core/session')->setAllowPartialPayment(0);
            } else {
                if (Mage::getSingleton('core/session')->getAllowPartialPayment() != 1)
                    Mage::getSingleton('core/session')->setAllowPartialPayment(1);
            }
        }else{
            Mage::register('admin_order_is_layaway', false);
            Mage::getSingleton('core/session')->setAllowPartialPayment(0);
        }
        try {
            if (
                array_key_exists('reserved_order_id', $orderData['comment'])
                && Mage::helper('adminhtml/sales')->hasTags($orderData['comment']['reserved_order_id'])
            ) {
                Mage::throwException($this->__('Invalid order data.'));
            }

            $this->_processActionData('save');
            if ($paymentData) {
                $paymentData['checks'] = Mage_Payment_Model_Method_Abstract::CHECK_USE_INTERNAL
                    | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
                    | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
                    | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX
                    | Mage_Payment_Model_Method_Abstract::CHECK_ZERO_TOTAL;
                $this->_getOrderCreateModel()->setPaymentData($paymentData);
                $this->_getOrderCreateModel()->getQuote()->getPayment()->addData($paymentData);
            }

            $order = $this->_getOrderCreateModel()
                ->setIsValidate(true)
                ->importPostData($this->getRequest()->getPost('order'))
                ->createOrder();

            //set type
            if($isLayaway){
                $order->setOrderType(1)->setOrderStage(1)->setVerification(9)->save();
            }
            // identify order was created by user
            Mage::dispatchEvent('opentechiz_create_order_by_user', array(
                'order'=>$order
            ));
            $this->_getSession()->clear();
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The order has been created.'));
            if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
                $this->_redirect('*/sales_order/view', array('order_id' => $order->getId()));
            } else {
                $this->_redirect('*/sales_order/index');
            }
        } catch (Mage_Payment_Model_Info_Exception $e) {
            $this->_getOrderCreateModel()->saveQuote();
            $message = $e->getMessage();
            if( !empty($message) ) {
                $this->_getSession()->addError($message);
            }
            $this->_redirect('*/*/', array('type' => $orderType));
        } catch (Mage_Core_Exception $e){
            $message = $e->getMessage();
            if( !empty($message) ) {
                $this->_getSession()->addError($message);
            }
            $this->_redirect('*/*/', array('type' => $orderType));
        }
        catch (Exception $e){
            $this->_getSession()->addException($e, $this->__('Order saving error: %s', $e->getMessage()));
            $this->_redirect('*/*/', array('type' => $orderType));
        }
    }

    /**
     * Cancel order create
     */
    public function cancelAction()
    {
        if ($orderId = $this->_getSession()->getReordered()) {
            $this->_getSession()->clear();
            $this->_redirect('*/sales_order/view', array(
                'order_id'=>$orderId
            ));
        } else {
            $this->_getSession()->clear();
            $this->_redirect('*/*', array('type' => $this->getRequest()->getParam('type')));
        }

    }

    //========= code from TBT_rewards
    /**
     * Acl check for admin
     *
     * @return bool
     * @see Mage_Adminhtml_Sales_Order_CreateController::_isAllowed()
     */
    protected function _isAllowed()
    {
        return parent::_isAllowed();
    }

    /**
     * Process Data
     * @return \TBT_Rewards_Adminhtml_Sales_Order_CreateController
     */
    protected function _processData()
    {
        parent::_processData();

        Mage::dispatchEvent('adminhtml_sales_order_create_process_data_complete', array());

        return $this;
    }
    //========= code from TBT_rewards

    public function addToSessionAction(){
        $params = $this->getRequest()->getParams();

        $session = Mage::getSingleton('core/session');
        foreach ($params as $key => $value){
            $session->setData($key, $value);
        }
    }
}