<?php

class OpenTechiz_SalesExtend_Adminhtml_Order_PaymentController extends Mage_Adminhtml_Controller_action {

    public function changeFormat($date) {
        $date = explode('/', $date);
        return $date[2] . '-' . $date[1] . '-' . $date[0];
    }

    public function savepaymentAction() {
        if(Mage::registry("recollect_installment") === null) {
            Mage::register('recollect_installment', true);
        }
        if ($data = $this->getRequest()->getPost()) {
            $creditMemoId = $data['credit_memo_id'];
            $creditMemo = Mage::getModel('sales/order_creditmemo')->load($creditMemoId);
            for ($i = 1; $i <= $data['hd_num_row']; $i++) {
                if ($data['hd_total_' . $i] > 0 || $data['hd_total_' . $i] > 0.00) {
                    $received_date = $data['hd_date_recei_' . $i];
                    if ($received_date == '') {
                        $received_date = Mage::getModel('core/date')->date('Y-m-d');
                    } else {
                        $received_date = $this->changeFormat($received_date);
                    }
                    $order = $creditMemo->getOrder();
                    $customer_id = Mage::getModel('sales/order')->load($creditMemo->getOrder_id())->getCustomer_id();
                    $customer_row = Mage::getModel('customer/customer')->load($customer_id);
                    $data_arr = array(
                        'increment_id' => 0,
                        'invoice_id' => 0,
                        'payment_id' => $data['hd_payment_' . $i],
                        'number' => $data['hd_number_' . $i],
                        'total' => (-1) * $data['hd_total_' . $i],
                        'billing_name' => $order->getBillingAddress()->getName(),
                        'email' => $customer_row->getEmail(),
                        'received_date' => $received_date,
                        'credit_memo_id' => $creditMemoId,
                        'order_id' => $order->getId(),
                        'type'   => 'creditmemo'
                    );
                    $model = Mage::getModel('cod/paymentgrid')->setData($data_arr);

                    if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                        $model->setCreatedTime(now())
                            ->setUpdateTime(now());
                    } else {
                        $model->setUpdateTime(now());
                    }

                    $model->save();
                }
            }
            $creditMemo->save();
            $this->_redirect('adminhtml/sales_order_creditmemo/view', array('creditmemo_id' => $this->getRequest()->getParam('creditmemo_id')));
            return;
        }
    }

    public function updatepaymentAction() {
        if(Mage::registry("recollect_installment") === null) {
            Mage::register('recollect_installment', true);
        }
        if ($data = $this->getRequest()->getPost()) {
            $creditMemoId = $data['credit_memo_id'];
            $creditMemo = Mage::getModel('sales/order_creditmemo')->load($creditMemoId);
            for ($i = 1; $i <= $data['hd_num_row_update']; $i++) {
                if ($data['hd_status_' . $i] == 'true' || $data['hd_total_' . $i] == 0) {
                    $item = Mage::getModel('cod/paymentgrid')->load($data['hd_paymentid_' . $i], 'payment_grid_id');
                    $item->delete();
                } else {
                    $received_date = $data['hd_date_recei_' . $i];
                    if ($received_date == '') {
                        $received_date = Mage::getModel('core/date')->date('Y-m-d');
                    } else {
                        $received_date =  date("Y-m-d", strtotime($received_date));
                    }
                    $data_update['number'] = $data['hd_number_' . $i];
                    $data_update['total'] = (-1) * $data['hd_total_' . $i];
                    $data_update['received_date'] = $received_date;
                    $model = Mage::getModel('cod/paymentgrid')->load($data['hd_paymentid_' . $i], 'payment_grid_id');
                    if ($model->getId()) {
                        $model->addData($data_update);
                        if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                            $model->setCreatedTime(now())
                                ->setUpdateTime(now());
                        } else {
                            $model->setUpdateTime(now());
                        }
                        $model->setTotal($data_update['total']);
                        $model->save();
                    }
                }
            }
            $creditMemo->save();
            $this->_redirect('adminhtml/sales_order_creditmemo/view', array('creditmemo_id' => $this->getRequest()->getParam('creditmemo_id')));
            return;
        }
    }

    public function ajaxupdatefeeAction()
    {
//        $cmemoid = $this->getRequest()->getParam('creditmemoid');
//        $creditMemo = Mage::getModel('sales/order_creditmemo')->load($cmemoid);
//        $state = $invoice_row->getState();
//        $grand_total = $invoice_row->getGrand_total();
//        if ($state == 1) {
//            $data['fee_amount'] = $grand_total;
//            $data['base_fee_amount'] = $grand_total;
//        } else {
//            $data['paid_fee_amount'] = $grand_total;
//            $data['paid_base_fee_amount'] = $grand_total;
//        }
//
//
//        $invoice_row->addData($data);
//        $invoice_row->save();

//        echo Mage::helper('core')->currency(round($grand_total, 2)) . '?' . $state;
//        echo '0';
    }

    public function UpdateCreditMemoStatusNew($cId, $total = null){

    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/creditmemo');
    }
}