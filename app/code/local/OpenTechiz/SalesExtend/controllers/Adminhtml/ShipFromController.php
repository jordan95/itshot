<?php

class OpenTechiz_SalesExtend_Adminhtml_ShipFromController extends Mage_Adminhtml_Controller_Action
{
    protected function _initShipFrom($idFieldName = 'id')
    {
        $id = (int)$this->getRequest()->getParam($idFieldName);
        $shipFrom = Mage::getModel('opentechiz_salesextend/shipFrom');

        if ($id) {
            $shipFrom->load($id);
            if (!$shipFrom->getId()) {
                Mage::throwException(Mage::helper('opentechiz_salesextend')->__('No address found.'));
            }
        }

        Mage::register('shipFrom', $shipFrom);
        return $shipFrom;
    }


    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales')
            ->_title($this->__('Ship From Address'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_salesextend/adminhtml_shipFrom'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();

        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_salesextend/adminhtml_shipFrom_grid')->toHtml()
        );
    }

    /**
     * Edit action
     */
    public function editAction()
    {
        $shipFrom = $this->_initShipFrom();

        $this->loadLayout();

        $this->_setActiveMenu('sales');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Edit Ship From Address'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_salesextend/adminhtml_shipFrom_edit/'));

        $this->renderLayout();

    }
    /**
     * New product action
     */
    public function createAction()
    {
        $this->_forward('edit');
    }
    /**
     * step one action
     */
    public function newAction()
    {
        $this->loadLayout();

        $this->_setActiveMenu('sales');
        $this->getLayout()->getBlock('head')->setTitle($this->__('New Ship From Address'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_salesextend/adminhtml_shipFrom_new/'));

        $this->renderLayout();
    }


    public function saveAction()
    {
        $params = $this->getRequest()->getParams();
        $address = $this->_initShipFrom();
        
        $address->setData($params)->save();
        Mage::getSingleton('adminhtml/session')->addSuccess('Saved');
        $this->_redirect('*/*/index');
    }

    public function deleteAction() {
        if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $address = $this->_initShipFrom();
                $address->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/ship_from');
    }
}