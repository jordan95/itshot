<?php

class OpenTechiz_SalesExtend_Adminhtml_CheckCostAtShippingController extends Mage_Adminhtml_Controller_Action
{
    public function checkCostAtShippingAction(){
        $params = $this->getRequest()->getParams('item');
        $orderId = $params['order_id'];
        $action = $params['action'];
        $order = Mage::getModel('sales/order')->load($orderId);
        if($order && $order->getId()){
            foreach ($order->getAllItems() as $item){
                $item->setSkipCostCheck($action)
                    ->setCheckCostAtShipping($action)
                    ->save();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Cost will be checked at shipping.'));
        }else{
            Mage::getSingleton('adminhtml/session')->addError($this->__('Order not found.'));
        }

        $this->_redirect('adminhtml/sales_order/view/order_id/' . $orderId);
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/sales/order/actions/check_cost_at_shipping');
    }
}