<?php
class OpenTechiz_SalesExtend_Model_Cron
{
    public function deleteSavedCcNumber(){
        $currentDate = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $time =  strtotime($currentDate);
        $toDate = date('Y-m-d H:i:s', strtotime("-72 hour", $time));
        $fromDate = date('Y-m-d H:i:s', strtotime("-408 hour", $time));

        $paymentCollection = Mage::getModel('sales/quote_payment')->getCollection();
        $paymentCollection->addFieldToFilter('method', 'authorizenet');
        $paymentCollection->addFieldToFilter('created_at', array(
            'from' => $fromDate,
            'to' => $toDate
        ));

        $quoteIds = [];
        foreach ($paymentCollection as $payment) {
            array_push($quoteIds, $payment->getQuoteId());
        }
        $orderCollection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('quote_id', array('in' => $quoteIds));
        
        $quoteIds = '(';
        $orderIds = '(';
        foreach ($orderCollection as $order) {
            $quoteId = $order->getQuoteId();
            if($order && $order->getId()){
                //delete all cc info for order not wait for customer > 3 days
                if($order->getInternalStatus() == 7) {
                    $quoteIds .= $quoteId . ',';
                    $orderIds .= $order->getId() . ',';
                }
                //delete if wait for customer > 15 days
                elseif($order->getInternalStatus() == 14 &&
                    $order->getUpdatedAt() < date('Y-m-d H:i:s', strtotime("-360 hour", $time))){
                    $quoteIds .= $quoteId . ',';
                    $orderIds .= $order->getId() . ',';
                }
                //delete if order status = canceled or on hold
                elseif($order->getState() == Mage_Sales_Model_Order::STATE_CANCELED ||
                    $order->getState() == Mage_Sales_Model_Order::STATE_HOLDED){
                    $quoteIds .= $quoteId . ',';
                    $orderIds .= $order->getId() . ',';
                }
            }
        }
        $quoteIds = trim($quoteIds, ',');
        $quoteIds .= ')';

        $orderIds = trim($orderIds, ',');
        $orderIds .= ')';

        //clean up in main database
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $table = $resource->getTableName('sales/quote_payment');
        $query = "UPDATE {$table} SET cc_number_enc = null, cc_cid_enc = null WHERE quote_id in  ".$quoteIds;
        $writeConnection->query($query);

        $table = $resource->getTableName('sales/order_payment');
        $query = "UPDATE {$table} SET cc_number_enc = null WHERE parent_id in  ".$orderIds;
        $writeConnection->query($query);

        //clean up in second database
        $conn = Mage::helper('medialounge_checkout/payment')->getSecondConnection('write');
        $conn->update('tsht_quote_payment', [
            'cc_number_enc' => '',
            'cc_cid_enc' => '',
        ], ['quote_id IN '. $quoteIds]);
    }

    public function cancelOrderAfterWaitForCustomerTooLong(){
        $currentDate = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $time =  strtotime($currentDate);
        $toDate = date('Y-m-d H:i:s', strtotime("-672 hour", $time));
        $fromDate = date('Y-m-d H:i:s', strtotime(0));

        $orderCollection = Mage::getModel('sales/order')->getCollection();
        $orderCollection->addFieldToFilter('internal_status', array('eq' => 14));
        $orderCollection->addFieldToFilter('updated_at', array(
            'from' => $fromDate,
            'to' => $toDate
        ));
        Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterActiveOrder($orderCollection);

        foreach ($orderCollection as $order) {
            if ($order->canCancel()) {
                try {
                    $order->addStatusHistoryComment('Order canceled by cron job.', false)
                        ->setIsCustomerNotified(false);
                    $order->cancel()->save();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }else{
                try {
                    $order->addStatusHistoryComment('Order canceled by cron job.', false)
                        ->setIsCustomerNotified(false);
                    $order->setState('canceled')->setStatus('canceled')->save();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }

            //unlink items
            $orderItems = $order->getAllItems();
            foreach ($orderItems as $orderItem){
                try {
                    $links = Mage::getModel('opentechiz_inventory/order')->getCollection()
                        ->addFieldToFilter('order_item_id', $orderItem->getId());
                    foreach ($links as $link){
                        $item = Mage::getModel('opentechiz_inventory/item')->load($link->getItemId());
                        if($item) {
                            $item->setState(35)->save();  //in stock
                        }
                        $link->delete();
                    }
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
        }
    }

    public function expireLayawayOrderAndSendMail(){
        $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
        $now = new Zend_Date($currentTimestamp);
        $date = new DateTime($now->addDay(-7)->get('YYYY-MM-dd HH:mm:ss'));

        $installments = Mage::getModel("partialpayment/installment")->getCollection()
            ->addFieldToFilter('installment_due_date', $date->format('Y-m-d'))
            ->addFieldToFilter('installment_status', 'Remaining');
        $partialpaymentIds = [];
        foreach ($installments as $installment){
            array_push($partialpaymentIds, $installment->getPartialPaymentId());
        }
        if(count($partialpaymentIds) > 0){
            $partialpaymentCollection = Mage::getModel("partialpayment/partialpayment")->getCollection()
                ->addFieldToFilter('partial_payment_id', array('in' => $partialpaymentIds));
            foreach ($partialpaymentCollection as $partialPayment){
                //update order status
                $order = Mage::getModel('sales/order')->load($partialPayment->getOrderId());
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 1, 13);

                //send mail to customer
                $email = $order->getCustomerEmail();
                $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
                $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

                $emailTemplate = Mage::getModel('core/email_template');

                $emailTemplate->loadDefault('order_layaway_expired_tpl');
                $emailTemplate->setTemplateSubject(Mage::app()->getStore()->getName() . ': Layaway Expired Notification');

                $emailTemplate->setSenderName($senderName);
                $emailTemplate->setSenderEmail($senderEmail);
                $emailTemplateVariables = array();
                $emailTemplateVariables['order_id'] = $order->getIncrementId();

                $emailTemplate->send($email, 'Admin', $emailTemplateVariables);
            }
        }
    }

    public function calculateTotalReturned(){
        
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $update_total_returned = "Update tsht_sales_flat_order_grid as A
                                    INNER JOIN 
                                    (Select B.entity_id,sum(C.qty_returned) as total_returned FROM tsht_sales_flat_order_grid as B join tsht_sales_flat_order_item as C on B.entity_id = C.order_id  group by B.entity_id) D
                                    on A.entity_id = D.entity_id
                                    set total_qty_returned = D.total_returned";
        try {
            $writeConnection->query($update_total_returned);
        } catch (Exception $e) {
            Mage::logException($e);
        }
        
    }

    public function checkPaypalPendingChangeStatus(){
        $currentDate = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $dateToCompare = date('Y-m-d H:i:s', strtotime("-20 day", strtotime($currentDate)));
        $orderCollection = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('verification', 12)->addFieldToFilter('created_at', array(
                'to' => $dateToCompare
            ));

        foreach ($orderCollection as $order){
            Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 0, 29);
        }
    }

    public function sendMailNotiCashOrder(){
        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/general/cash_order'));

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('cash_order_tpl');
        $emailTemplate->setTemplateSubject('Check/Cash Order Email ('.Mage::helper('core')->formatDate().')');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();

        foreach ($to as $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send($recipientEmail, $emailTemplateVariables);
            }
        }
    }

    public function cancelOrderNoPayment(){
        //f there are no payments cancel those older than 3 Weeks
        $orderStatusToExclude = ['canceled', 'closed', 'holded', 'complete', 'layawayorder_canceled'];

        $orderCollection = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('order_stage', array('eq' => 0)) //order verification
            ->addFieldToFilter('internal_status', array('eq' => 14)) //waiting on customer
            ->addFieldToFilter('status', array('nin' => $orderStatusToExclude))
            ->addFieldToFilter('state', array('nin' => $orderStatusToExclude))
            ->addFieldToFilter('created_at', array('to' => date('Y-m-d H:i:s', strtotime('-21 day', time()))))
            ->addFieldToFilter('base_total_paid', array('or'=> array(
                0 => array('eq' => 0),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left');

        foreach ($orderCollection as $order){
            $order = Mage::getModel('sales/order')->load($order->getId());
            if ($order->canCancel()) {
                try {
                    //cancel coupon code before cancel order
                    if ($code = $order->getCouponCode()) {
                        $order->setCouponCode(null)->save();
                    }

                    //cancel order
                    $order->addStatusHistoryComment('Order canceled by cron job, no payment in 3 weeks.', false)
                        ->setIsCustomerNotified(false);
                    $order->cancel()->save();
                } catch (Exception $e) {
                    Mage::log($order->getId(), null, 'exception.log');
                    Mage::logException($e);
                }
            }
        }
    }

    public function sendMailNotiWaitingOnVendor(){
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        if (!$to = Mage::getStoreConfig('opentechiz_salesextend/waiting_on_vendor/email')) {
            return;
        }
        $to = explode(',', $to);
        $days = (int) Mage::getStoreConfig('opentechiz_salesextend/waiting_on_vendor/days');

        /** @var Mage_Sales_Model_Resource_Order_Collection $collection */
        $collection = Mage::getModel('sales/order')->getCollection();
        $dateModel = Mage::getModel('core/date');
        $fromDate = $dateModel->gmtTimestamp() - 60 * 60 * 24 * $days;
        $collection->addFieldToFilter('internal_status', 4)
            ->addFieldToFilter('state', ['nin' => [
                Mage_Sales_Model_Order::STATE_COMPLETE,
                Mage_Sales_Model_Order::STATE_CANCELED,
                Mage_Sales_Model_Order::STATE_HOLDED,
                Mage_Sales_Model_Order::STATE_CLOSED,
            ]]);
        $collection->addFieldToFilter('base_total_paid', ['gteq' => new Zend_Db_Expr('`base_grand_total`')])
            ->addFieldToFilter('created_at', ['lt' => $dateModel->gmtDate(null, $fromDate)]);
        $orders = $collection->getItems();
        if (!$orders) {
            return;
        }

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('waiting_on_vendor_orders');
        $emailTemplate->setTemplateSubject('Orders may be stock, may need to check with Vendor');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = ['orders' => $orders];

        foreach ($to as $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send(trim($recipientEmail), null, $emailTemplateVariables);
            }
        }
    }
}