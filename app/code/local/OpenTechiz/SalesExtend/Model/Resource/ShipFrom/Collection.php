<?php

class OpenTechiz_SalesExtend_Model_Resource_ShipFrom_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_salesextend/shipFrom');
    }
}