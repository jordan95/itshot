<?php

class OpenTechiz_SalesExtend_Model_Resource_Authorizednet_Data extends Medialounge_Checkout_Model_Resource_Authorisednet_Data
{
    public function addPaymentQuoteRecord($quoteId, $request, $result)
    {
        /** @var \Medialounge_Checkout_Model_ExpiryDateParser $expiryDateParser */
        $expiryDateParser = Mage::getModel('medialounge_checkout/expiryDateParser', $request->getXExpDate());
        $connection = Mage::helper('medialounge_checkout/payment')->getSecondConnection('write');

        if ($recordId = $this->isQuotePaymentStartedRecreated($quoteId)) {
            $connection->update(Mage::getModel('core/resource')->getTableName('medialounge_checkout/quote_payment'), [
                'anet_trans_method' => $result->getMethod(),
                'cc_trans_id' => $result->getTransactionId(),
                'cc_status' => $result->getResponseCode(),
                'cc_type' => ($result->getCardType()=='MasterCard')?'MC':'VI',
                'cc_status_description' => $result->getResponseReasonText(),
                'cc_exp_month' => $expiryDateParser->getExpMonth(),
                'cc_exp_year' => $expiryDateParser->getExpYear(),
                'cc_avs_status' => $result->getAvsResultCode(),
                'cc_cid_status' => $result->getCardCodeResponseCode(),
                'cc_phnumber' => $request->getXCcBankNumber(),
                'cc_number_enc' => Mage::helper('core')->encrypt($request->getXCardNum()),
                'cc_cid_enc' => Mage::helper('core')->encrypt($request->getXCardCode()),
            ], ['quote_id=?' => $quoteId]);
        } else {
            $connection->insert(Mage::getModel('core/resource')->getTableName('medialounge_checkout/quote_payment'), [
                'quote_id' => $quoteId,
                'anet_trans_method' => $result->getMethod(),
                'cc_trans_id' => $result->getTransactionId(),
                'cc_status' => $result->getResponseCode(),
                'cc_type' => ($result->getCardType()=='MasterCard')?'MC':'VI',
                'cc_status_description' => $result->getResponseReasonText(),
                'cc_exp_month' => $expiryDateParser->getExpMonth(),
                'cc_exp_year' => $expiryDateParser->getExpYear(),
                'cc_avs_status' => $result->getAvsResultCode(),
                'cc_cid_status' => $result->getCardCodeResponseCode(),
                'cc_phnumber' => $request->getXCcBankNumber(),
                'cc_number_enc' => Mage::helper('core')->encrypt($request->getXCardNum()),
                'cc_cid_enc' => Mage::helper('core')->encrypt($request->getXCardCode()),
            ]);
        }
    }

    public function isQuotePaymentStartedRecreated($quoteId){
        $connection = Mage::helper('medialounge_checkout/payment')->getSecondConnection('read');
        $select = $connection->select();
        $select->from(Mage::getModel('core/resource')->getTableName('medialounge_checkout/quote_payment'));
        $select->reset(Varien_Db_Select::COLUMNS)->columns(['quote_id']);
        $select->where('quote_id=?', $quoteId);
        $recordId = $connection->fetchOne($select);

        return $recordId;
    }
}