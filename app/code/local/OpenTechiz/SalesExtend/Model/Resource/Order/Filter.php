<?php
class OpenTechiz_SalesExtend_Model_Resource_Order_Filter
{
    //param: collection joined with sales/order with sales/order.state as state
    public function filterActiveOrder($collection){
        $collection->getSelect()->where('state not in (?)', (array)OpenTechiz_SalesExtend_Helper_Data::DEACTIVE_ORDER_STATE);
        return $collection;
    }
 	public function filterCancelCloseOrder($collection){
        $collection->getSelect()->where('state in (?)', (array)OpenTechiz_SalesExtend_Helper_Data::CANCEL_CLOSE_ORDER_STATE);
        return $collection;
    }
    public function filterNotCancelCloseOrder($collection){
        $collection->getSelect()->where('state not in (?)', (array)OpenTechiz_SalesExtend_Helper_Data::CANCEL_CLOSE_ORDER_STATE);
        return $collection;
    }
    public function filterHoldOrder($collection){
        $collection->getSelect()->where("state ='holded'");
        return $collection;
    }
    public function filterCompleteOrder($collection){
        $collection->getSelect()->where("state ='complete'");
        return $collection;
    }
}