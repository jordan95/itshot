<?php

class OpenTechiz_SalesExtend_Model_Observer
{
    private $_isProcessed = false;
    private $_refund = 0;
    private $_base_refund = 0;

    public function registerOrderDataBeforeSave($observer){
        if(Mage::registry('order_before_save_data')){
            return;
        }
        $order = $observer->getEvent()->getOrder();
        Mage::register('order_before_save_data', $order->getOrigData());
    }

    public function autoCreateInvoiceOrderGrandtotalZero($observer){
        if(Mage::registry('flag_auto_create_invoice')) return;
        $order = $observer->getEvent()->getOrder();
        $stagebefore = (int)$observer->getEvent()->getStageBefore();
        //#3034 if move to billing stage
        //#3308 Unable to Complete order (update for all order have base grand total = 0)
        if(isset($stagebefore) && $stagebefore < 3 && $order->getOrderStage() == 3){
            //if order total = 0, auto create invoice
            if((int)$order->getBaseGrandTotal() == 0 && $order->canInvoice()){
                try {
                    Mage::register('flag_auto_create_invoice', true);
                    $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                    $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                    $invoice->register();
                    Mage::getModel('core/resource_transaction')
                        ->addObject($invoice)
                        ->addObject($invoice->getOrder())
                        ->save();
                }
                catch (Mage_Core_Exception $e) {

                }
            }
        }
    }

    public function checkOrderProfit(Varien_Event_Observer $observer){
        $order = $observer->getEvent()->getOrder();
        $skusHasErrorCost = Mage::helper('opentechiz_salesextend')->checkProfitOrder($order);
        $data = $observer->getEvent()->getObject();
        $data->setError($skusHasErrorCost);
    }

    public function checkValidCreditCardOrder(Varien_Event_Observer $observer){
        $order = $observer->getEvent()->getOrder();
        $paritalpayment = Mage::getModel('partialpayment/partialpayment')->load($order->getId(), 'order_id');
        if ($paritalpayment->getId()) {
            $order->setTotalPaid($order->getPaidAmount())
                ->setBaseTotalPaid($order->getBasePaidAmount())
                ->setTotalDue($order->getRemainingAmount())
                ->setBaseTotalDue($order->getBaseRemainingAmount());
        } else {
            $order->setRemainingAmount($order->getTotalDue())
                ->setBaseRemainingAmount($order->getBaseTotalDue());
        }

        if($this->_isProcessed == false) {
            $stage = null;
            $status = null;
            $term = null;
            $verification = null;

            $customer_id = $order->getCustomerId();
            $customer = Mage::getModel('customer/customer')->load($customer_id);

            //check for authorise error, if state is manual, not change stage and status
            if (Mage::registry('cc_bank_number')) {
                $order->setData('payment_bank_number', Mage::registry('cc_bank_number'));
            }

            $orderValidationData = Mage::helper('opentechiz_salesextend/orderValidation')->validateCreditCardOrder($order);

            if(!$orderValidationData['is_error_with_cc']) {
                if ($orderValidationData['is_layaway_or_repair']) {
                    $verification = 9; // Not Applicable
                } else {
                    if ($orderValidationData['is_paid_with_cc']) {
                        if ($orderValidationData['is_customer_fraudster']) {
                            //send email
                            $emailList = Mage::getStoreConfig('opentechiz_salesextend/general/fraud_email');
                            $emailList = explode(',', $emailList);
                            $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
                            $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

                            $emailTemplate = Mage::getModel('core/email_template');

                            $emailTemplate->loadDefault('order_customer_fraud_tpl');
                            $emailTemplate->setTemplateSubject(Mage::app()->getStore()->getName() . ': Fraud Notification');

                            $emailTemplate->setSenderName($senderName);
                            $emailTemplate->setSenderEmail($senderEmail);
                            $emailTemplateVariables = array();
                            $emailTemplateVariables['order_id'] = $order->getIncrementId();
                            $emailTemplateVariables['customer_id'] = $customer_id;
                            $emailTemplateVariables['customer_name'] = $customer->getName();

                            foreach ($emailList as $recipientEmail) {
                                $emailTemplate->send($recipientEmail, 'Admin', $emailTemplateVariables);
                            }

                            //set verification = fraudster & set term = contact customer
                            $verification = 8; // Contact Customer
                            $term = 2;

                            //change internal status
                            $status = 14;
                            Mage::helper('opentechiz_customernotes')->addFrausterOrderNote($customer, $order);

                        } else {
                            //first order > 5 month
                            if ($orderValidationData['is_customer_trusted']) {
                                $verification = 1; // Auto Approved
                                $term = 1;
                                $stage = 1;
                            }

                            //check stock
                            if (!Mage::helper('opentechiz_salesextend')->isInstock($order)) {
                                $term = 3;
                            }
                        }
                    }
                }

                //layaway doesnt have order processing stage
                if ($orderValidationData['is_layaway_or_repair']) {
                    $stage = 1;
                    $verification = 9; // Not Applicable
                }

                $this->_isProcessed = true;
                Mage::helper('opentechiz_salesextend')->updateTermAndVerification($order, $term, $verification);
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, $stage, $status);
            }
        }
    }

    public function isPaidWithCreditCard($order){
        $payment = $order->getPayment();
        if(!$payment) {
            return false;
        }
        $isCreditCard = false;
        if($payment->getMethod() == 'authorizenet'){
            $isCreditCard = true;
        }
        return $isCreditCard;
    }

    public function saveCcNumber($observer){
        $order = $observer->getEvent()->getOrder();
        $payment = $order->getPayment();
        $params = Mage::app()->getRequest()->getParams();
        if(isset($params['payment']) && isset($params['payment']['cc_cid'])) $cc_cid = $params['payment']['cc_cid'];
        if($order->getStatusLabel() != 'Processing (Manual CC)') $cc_cid = '';
        //save cc number
        if($this->isPaidWithCreditCard($order) && $payment->getCcNumber() && is_numeric($payment->getCcNumber())) {
            //save cc number
            $conn = Mage::helper('medialounge_checkout/payment')->getSecondConnection('write');
            $arrayData = [
                'cc_type'    => $payment->getCcType(),
                'cc_number_enc' => Mage::getModel('payment/info')->encrypt($payment->getCcNumber()),
                'cc_last4'   => substr($payment->getCcNumber(), -4),
            ];
            if(isset($cc_cid) && $cc_cid){
                $arrayData['cc_cid_enc'] = Mage::getModel('payment/info')->encrypt($cc_cid);
            }
            $conn->update('tsht_quote_payment', $arrayData , ['quote_id=?' => $order->getQuoteId()]);

            //change last 4 digit in origin table
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $table = $resource->getTableName('sales/quote_payment');
            $table2 = $resource->getTableName('sales/order_payment');
            $query = "UPDATE {$table} SET cc_last4 = ".substr($payment->getCcNumber(), -4)." WHERE quote_id = ".$order->getQuoteId();
            $writeConnection->query($query);
            $query = "UPDATE {$table2} SET cc_last4 = ".substr($payment->getCcNumber(), -4)." WHERE parent_id = ".$order->getId();
            $writeConnection->query($query);
        }
    }

    public function changeOrderDataAfterSignifydCaseSaved(Varien_Event_Observer $observer){
        $case = $observer->getCase();

        $incrementId = $case->getOrderIncrement();
        Mage::log('change order data on Signifyd case save order #'.$incrementId, null,
            OpenTechiz_SalesExtend_Helper_OrderValidation::ORDER_VALIDATION_LOG_FILE);
        $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
        if(!$order->getId()) {
            return $this;
        }

        //code to save order
        $guarantee = str_replace(' ', '', $case->getGuarantee());
        Mage::log('Guarantee: '.$guarantee, null,
            OpenTechiz_SalesExtend_Helper_OrderValidation::ORDER_VALIDATION_LOG_FILE);
        if(!empty($order->getIncrementId())){
            if ($guarantee == 'DECLINED' || $guarantee == 'CANCELED') {
                if($case->getNotifySignifydDeclined()) {
                    return $this;
                }
                //send email

                $customer_id = $order->getCustomerId();
                $customer = Mage::getModel('customer/customer')->load($customer_id);

                $emailList = Mage::getStoreConfig('opentechiz_salesextend/general/fraud_email');
                $emailList = explode(',', $emailList);
                $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
                $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

                $emailTemplate = Mage::getModel('core/email_template');

                $emailTemplate->loadDefault('order_signifyd_denied_tpl');
                $emailTemplate->setTemplateSubject('['.$order->getIncrementId() . '] was declined by Sign');

                $emailTemplate->setSenderName($senderName);
                $emailTemplate->setSenderEmail($senderEmail);
                $emailTemplateVariables = array();
                $emailTemplateVariables['order_id'] = $order->getIncrementId();
                $emailTemplateVariables['customer_id'] = $customer_id;
                $emailTemplateVariables['customer_name'] = $customer->getName();

                foreach ($emailList as $recipientEmail) {
                    $emailTemplate->send($recipientEmail, 'Admin', $emailTemplateVariables);
                }
                $case->setNotifySignifydDeclined(1);

                $status = 31;
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, null, $status);
            } elseif ($guarantee == 'APPROVED') {
                $term = 1;
                $verification = 2; // Guaranteed
                $stage = 1;
                $status = 0;
                Mage::helper('opentechiz_salesextend')->updateTermAndVerification($order, $term, $verification);
                Mage::log('AutoCharge order '.$order->getIncrementId().' after Signifyd approved', null, 'auto_charge_cc.log');
                Mage::helper('opentechiz_salesextend')->changeOrderStage($order, $stage);
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, null, $status);
            }
        }
    }

    public function getSalesOrderViewInfo(Varien_Event_Observer $observer) {
        $block = $observer->getBlock();

        // layout name should be same as used in app/design/adminhtml/default/default/layout/mymodule.xml
        if (($block->getNameInLayout() == 'order_info') && ($child = $block->getChild('opentechiz_salesextend.order.info.custom.shiprates'))) {
            $transport = $observer->getTransport();
            if ($transport) {
                $html = $transport->getHtml();
                $html .= $child->toHtml();
                $transport->setHtml($html);
            }
        }
        if (($block->getNameInLayout() == 'order_info') && ($child = $block->getChild('opentechiz_salesextend.order_source'))) {
            $transport = $observer->getTransport();
            if ($transport) {
                $html = $transport->getHtml();
                $html .= $child->toHtml();
                $transport->setHtml($html);
            }
        }
    }

    public function invoiceParentOrderAfterInvoiceSurchargeOrder(Varien_Event_Observer $observer){
        $surcharge  = $observer->getEvent()->getSurcharge();

        if($surcharge->getId()){
            $parentOrder = Mage::getModel('sales/order')->load($surcharge->getParentOrderId());
            $itemsArray = [];  //item to create invoice

            foreach($parentOrder->getAllItems() as $item) {
                $item_id = $item->getItemId();   //order_item_id
                $qty = $item->getQtyOrdered() - $item->getQtyInvoiced();   //qty ordered for that item
                $itemsArray[$item_id] = $qty;
            }

            //create invoice
            if($parentOrder->canInvoice()) {
                Mage::getModel('sales/order_invoice_api')
                    ->create($parentOrder->getIncrementId(), $itemsArray ,'invoice_for_surcharge' ,1,1);
            }

            //reduce parent order total (partially paid on surcharge order)
            $surchargePaidAmount = $surcharge->getBaseTotal() - $surcharge->getBaseTotalDue();
            $parentOrder->setGrandTotal($parentOrder->getGrandTotal() - $surchargePaidAmount)
                ->setBaseGrandTotal($parentOrder->getBaseGrandTotal() - $surchargePaidAmount)
                ->setTotalDue($parentOrder->getTotalDue() - $surchargePaidAmount)
                ->setBaseTotalDue($parentOrder->getBaseTotalDue() - $surchargePaidAmount)
                ->save();

            foreach($parentOrder->getAllItems() as $item) {
                $orderItem = Mage::getModel('sales/order_item')->load($item->getId());
                $orderItem->setQtyInvoiced($orderItem->getQtyOrdered())->save();
            }
        }
    }

    public function applyNewOrderDueAmountToPartialPayment(Varien_Event_Observer $observer){
        //if is partial payment, change the remaining unpaid installments
        $order = $observer->getOrder();
        $origOrder = $observer->getOrigOrder();
//        $order->setPaidAmount($origOrder->getPaidAmount())
//            ->setRemainingAmount($order->getGrandTotal() - $origOrder->getPaidAmount())->save();
        $isPartialPayment = false;
        $partialpaymentOrder = Mage::getModel('partialpayment/partialpayment')->load($order->getId(), 'order_id');

        if($partialpaymentOrder->getId() && $origOrder->getGrandTotal() != $order->getGrandTotal()) {
            $isPartialPayment = true;
            $partialpaymentOrder->setTotalAmount($order->getGrandTotal())
                ->setPaidAmount($order->getPaidAmount())
                ->setRemainingAmount($order->getRemainingAmount())->save();
        }

        if($isPartialPayment){
            $partialPaymentId = Mage::getModel('partialpayment/partialpayment')->load($order->getId(), 'order_id')->getId();
            $remainingInstallment = Mage::getModel('partialpayment/installment')->getCollection()
                ->addFieldToFilter('partial_payment_id', $partialPaymentId)
                ->addFieldToFIlter('installment_status', 'Remaining');
            $installmentAmount = ($order->getGrandTotal() - $order->getPaidAmount()) / count($remainingInstallment);
            foreach ($remainingInstallment as $installment){
                $installment->setInstallmentAmount($installmentAmount)->save();
            }
        }
    }

    public function sendToSignifydAfterEditPayment(Varien_Event_Observer $observer){
        $order = $observer->getOrder();
        $changes = $observer->getChanges();
        if(!isset($changes['payment']) || $changes['payment']['method'] != "authorizenet") {
            return;
        }
        if(in_array($order->getOrderType(), array(1, 2))) {
            return;
        }
        //do not send if payment have error
        $canSend = Mage::helper('opentechiz_salesextend/orderValidation')->checkIfSendToSignifyd($order);
        if(!$canSend){
            return;
        }

        //run send to signifyd
        try {
            $orderIds = array($order->getId());
            if(is_string($orderIds))
            {
                $orderIds = array_map('intval', explode(',', $orderIds));
            }
            if (!is_array($orderIds)) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')
                    ->__('Please select order(s)'));
            } else {
                // Reference T2395
                $user = "Unknown";
                try {
                    $user = Mage::getSingleton('admin/session')->getUser()->getUsername();
                } catch (Exception $ex) {
                    Mage::helper('signifyd_connect')->logError($ex->__toString());
                }
                Mage::helper('signifyd_connect')->logRequest("Bulk send initiated by: $user");

                $collection = Mage::getModel('sales/order')->getCollection()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('entity_id', array('in' => $orderIds));

                foreach ($collection as $order) {
                    if(Mage::registry('signifyd_action')) Mage::unregister('signifyd_action');
                    $result = Mage::helper('signifyd_connect')->buildAndSendOrderToSignifyd($order, /*forceSend*/ true);
                    if($result == "sent") {
                        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')
                            ->__('Successfully sent order ' . $order->getIncrementId() . '.'));
                    } else if ($result == "exists") {
                        Mage::getSingleton('adminhtml/session')->addWarning(Mage::helper('adminhtml')
                            ->__('Order ' . $order->getIncrementId() . ' has already been sent to Signifyd.'));
                    } else if ($result == "nodata") {
                        Mage::helper('signifyd_connect')->logRequest("Request/Update not sent because there is no data");
                    } else {
                        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')
                            ->__('Order ' . $order->getIncrementId() . ' failed to send to Signifyd. See log for details.'));
                    }
                }
                if (Mage::getStoreConfig('signifyd_connect/log/request')) {
                    Mage::helper('signifyd_connect')->logRequest("Bulk send complete");
                }
            }
        } catch(Exception $ex) {
            Mage::helper('signifyd_connect')->logError($ex->__toString());
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')
                ->__('Failed to send to Signifyd. See log for details'));
        }
    }

    public function checkMultipaymentAmount(Varien_Event_Observer $observer){
        if(Mage::registry('multipaymentInvoiceUpdated')) return;
        Mage::register('multipaymentInvoiceUpdated', true);

        $invoice = $observer->getInvoice();
        $order = $invoice->getOrder();
        if($order->getPayment()->getMethod() == 'MultiplePayment'){
            $collection = Mage::getModel('PaymentTracker/Payment')
                ->getCollection()
                ->addFieldToFilter('ptp_order_id', $order->getId());
            $orderTotal = $order->getGrandTotal();
            $totalPaid = 0;
            foreach ($collection as $payment){
                $totalPaid += $payment->getptp_amount();
            }

            //create payment record
            //search if payment method already exist
            $paymentMethodCollection = Mage::getModel('cod/payment')->getCollection()->addFieldToFilter('name', 'Multiple Payment');
            if (count($paymentMethodCollection) > 0) {
                $paymentMethod = $paymentMethodCollection->getFirstItem();
            } else {
                $data = [];
                $data['name'] = 'Multiple Payment';
                $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                $paymentMethod = Mage::getModel('cod/payment')->setData($data)->save();
            }
            $paymentMethodId = $paymentMethod->getId();

            $invoice->setPaidAmount($totalPaid)->setRemainingAmount($orderTotal - $totalPaid)
                ->setBasePaidAmount($totalPaid)->setBaseRemainingAmount($orderTotal - $totalPaid);

            if($orderTotal <= $totalPaid){
                $invoice->setState(2)->save();
            } else{
                $invoice->setState(1)->save();
            }
            $this->addPaymentRecordToInvoice($order, $invoice, $paymentMethodId);
        }
    }

    public function checkMultipaymentAmountOrder(Varien_Event_Observer $observer)
    {
        $order = $observer->getOrder();
        $methodName = $order->getPayment()->getMethod();
        if ($methodName == 'MultiplePayment') {
            $collection = Mage::getModel('PaymentTracker/Payment')
                ->getCollection()
                ->addFieldToFilter('ptp_order_id', $order->getId());
            $orderTotal = $order->getGrandTotal();
            $totalPaid = 0;
            foreach ($collection as $payment) {
                $totalPaid += $payment->getptp_amount();
            }
            $invoice = $observer->getInvoice();
            if ($orderTotal <= $totalPaid) {
                //create payment record
                //search if payment method already exist
                $paymentMethodCollection = Mage::getModel('cod/payment')->getCollection()->addFieldToFilter('name', $methodName);
                if (count($paymentMethodCollection) > 0) {
                    $paymentMethod = $paymentMethodCollection->getFirstItem();
                } else {
                    $data = [];
                    $data['name'] = $methodName;
                    $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                    $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                    $paymentMethod = Mage::getModel('cod/payment')->setData($data)->save();
                }
                $paymentMethodId = $paymentMethod->getId();

                //create/update invoice

                if ($invoice) {
                    $paid = $invoice->getGrandTotal();
                    $invoice->setState(2)->setPaidAmount($paid)->setRemainingAmount(0)
                        ->setBasePaidAmount($paid)->setBaseRemainingAmount(0);
                    $this->addPaymentRecordToInvoice($order, $invoice, $paymentMethodId);
                    $invoice->save();
                }
            } elseif ($totalPaid > 0 && $invoice) {
                $paymentMethodCollection = Mage::getModel('cod/payment')->getCollection()->addFieldToFilter('name', $methodName);
                if (count($paymentMethodCollection) > 0) {
                    $paymentMethod = $paymentMethodCollection->getFirstItem();
                } else {
                    $data = [];
                    $data['name'] = $methodName;
                    $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                    $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                    $paymentMethod = Mage::getModel('cod/payment')->setData($data)->save();
                }
                $paymentMethodId = $paymentMethod->getId();
                $invoice->setPaidAmount($totalPaid);
                $this->addPaymentRecordToInvoice($order, $invoice, $paymentMethodId);
            }
        }
    }

    public function addPaymentRecordToInvoice($order, $invoice, $paymentMethodId){
        //add payment record to invoice
        $paymentRecordCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->addFieldToFilter('invoice_id', $invoice->getId())
            ->addFieldToFilter('payment_id', $paymentMethodId);

        if (count($paymentRecordCollection) > 0) {
            $paymentRecord = $paymentRecordCollection->getFirstItem();
            $data = [];
            $data['total'] = $invoice->getGrandTotal();
            if ($invoice->getPaidAmount() > 0) {
                $data['total'] = $invoice->getPaidAmount();
            }
            $paymentRecord->addData($data)->save();
        } else {
            $data = [];
            $data['invoice_id'] = $invoice->getId();
            $data['order_id'] = $order->getId();
            $data['increment_id'] = $invoice->getIncrementId();
            $data['payment_id'] = $paymentMethodId;
            $data['total'] = $invoice->getPaidAmount();
            $data['billing_name'] = $order->getBillingAddress()->getName();
            $data['email'] = $order->getBillingAddress()->getEmail();
            $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['received_date'] = Mage::getModel('core/date')->gmtDate('Y-m-d');

            $paymentRecord = Mage::getModel('cod/paymentgrid');
            $paymentRecord->setData($data)->save();
        }
    }

    public function checkOrderType($observer){
        if(Mage::registry('checkOrderType')) return;
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $order = $observer->getEvent()->getOrder();
        $orderId = $order->getId();
        $collection = Mage::getModel('partialpayment/partialpayment')->getCollection()->addFieldToFilter('order_id', $orderId);

        //order created in admin
        $params = Mage::app()->getRequest()->getParams();
        if(!empty($params['order_type'])){
            $allOrderTypes = OpenTechiz_SalesExtend_Helper_Data::ORDER_TYPE;
            $allOrderTypes = array_map('strtolower', $allOrderTypes);
            $type = array_flip($allOrderTypes)[$params['order_type']];
            $order->setOrderType($type);
        }

        if(count($collection) > 0){
            Mage::register('checkOrderType', true);
            $order->setOrderType(1);
            $order->save();
            Mage::getModel('sales/order')->getResource()->updateGridRecords($orderId);

            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $table = $resource->getTableName('sales/order_grid');
            $query = "UPDATE {$table} SET order_type = '1' WHERE entity_id = '" .$orderId."'";
            $writeConnection->query($query);
        }

        $isRepairSurcharge = false;
        $orderType = 0;
        foreach ($order->getAllItems() as $item){
            $desctiption = $item->getDescription();
            if(stripos($desctiption, '#') !== false){
                $returnIncrementId = substr($desctiption, strpos($desctiption, '#')+1, 30);
                $return = Mage::getModel('opentechiz_return/return')->load($returnIncrementId, 'increment_id');
                if($return && $return->getId()){
                    $isRepairSurcharge = true;
                    $solution = $return->getSolution();
                    $orderType = OpenTechiz_Return_Helper_Data::SOLUTION_TO_ORDER_TYPE[$solution];
                    break;
                }
            }
        }
        if($isRepairSurcharge){
            if(!Mage::registry('checkOrderType')) Mage::register('checkOrderType', true);
            if($orderType == 2) {
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 1, 0);
            }elseif($orderType == 3){
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 0, 0);
            }
            $order->setOrderType($orderType);
            $order->save();
            Mage::getModel('sales/order')->getResource()->updateGridRecords($orderId);

            $table = $resource->getTableName('sales/order_grid');
            $query = "UPDATE {$table} SET order_type = '".$orderType."' WHERE entity_id = '" .$orderId."'";
            $writeConnection->query($query);
        }

        //log order type
        $statusLog = Mage::getModel('opentechiz_reportExtend/log_orderStatus')->load($order->getIncrementId(), 'order_id');
        if($statusLog && $statusLog->getId()){
            if($order->getOrderType() != $statusLog->getOrderType()) $statusLog->setOrderType($order->getOrderType())->save();
        }else{ //save default stage and status when order created
            $data = [];
            $data['user_id'] = 0;
            $data['order_id'] = $order->getIncrementId();
            $data['order_type'] = $order->getOrderType();
            $data['old_statuses'] = 'N/A';
            if(in_array($order->getOrderType(), array(0,3))) {
                $data['new_statuses'] = '0,0,9';
            }else{
                $data['new_statuses'] = '1,0,9';
            }

            if($data['old_statuses'] == $data['new_statuses']) return;

            $history = Mage::getModel('opentechiz_reportExtend/log_orderStatus')->setData($data)->save();

            //save last history id
            $query = "UPDATE tsht_sales_flat_order SET last_history_id = '".$history->getId()."' WHERE entity_id = '" .$orderId."'";
            $writeConnection->query($query);
        }
    }

    public function createPaymentRecordAfterPaymentTransactionSave($observer){
        if(Mage::registry('import_data_flag') || Mage::registry('skip_create_payment_record_after_payment_transaction_save')){
            return;
        }
        $transaction = $observer->getEvent()->getOrderPaymentTransaction();

        //only add payment record for new transaction
        $transactionData = $transaction->getData();
        if(!isset($transactionData['is_new']) || !$transactionData['is_new']){
            return;
        }

        $transactionType = $transaction->getTxnType();
        if($transactionType == 'capture' || $transactionType == 'refund'){
            $this->addPaymentRecordForTransaction($transaction, $transactionType);
        }
    }

    public function createInvoiceAndCaptureAfterPaymentTransactionSave($observer){
        if(Mage::registry('import_data_flag') || Mage::registry('skip_create_payment_record_after_payment_transaction_save')){
            return;
        }
        $transaction = $observer->getEvent()->getOrderPaymentTransaction();

        //only add payment record for new transaction
        $transactionData = $transaction->getData();
        if(!isset($transactionData['is_new']) || !$transactionData['is_new']){
            return;
        }

        $order = Mage::getModel('sales/order')->load($transaction->getData('order_id'));

        if(Mage::helper('opentechiz_salesextend/orderValidation')->checkIfCanAutoChargeCCOnOrderCreate($order)){
            Mage::log('AutoCharge on order create (transaction saved)', null, 'auto_charge_cc.log');
            Mage::helper('opentechiz_salesextend')->tryInvoiceAndCaptureOrder($order);
        }
    }

    public function addPaymentRecordForTransaction($transaction, $type)
    {
        $order = $transaction->getOrder();
        $payment = Mage::getModel('sales/order_payment')->load($transaction->getPaymentId());
        $methodName = $payment->getMethod();
        //avoid multiple records created by multiple save.
        if (count(Mage::getModel('cod/paymentgrid')->getCollection()
                ->addFieldToFilter('transaction_id', $transaction->getId())) > 0) {
            return;
        }

        $isPartialPayment = false;
        $partialpaymentOrder = Mage::getModel('partialpayment/partialpayment')->load($order->getId(), 'order_id');

        if ($partialpaymentOrder->getId()) {
            $isPartialPayment = true;
            $installment = Mage::getModel('partialpayment/installment')->load($transaction->getTxnId(), 'transaction_id');
            if ($installment && $installment->getInstallmentAmount()) {
                $methodName = $installment->getPaymentMethod();
            }
        }

        $invoiceId = 0;
        $invoiceIncrementId = 0;
        $invoiceCollection = $order->getInvoiceCollection();
        if (count($invoiceCollection)) {
            $invoice = $invoiceCollection
                ->addAttributeToSort('created_at', 'DSC')
                ->setPage(1, 1)
                ->getFirstItem();
            $invoiceId = $invoice->getId();
            $invoiceIncrementId = $invoice->getIncrementId();
        }

        //get payment method, create if not exist
        $methodId = Mage::helper('opentechiz_salesextend/rewrite_adminhtml_paymentRecord')->getPaymentMethodId($methodName);
        if (is_numeric($methodId)) {
            $paymentMethodId = $methodId;
        } else {
            $paymentMethodCollection = Mage::getModel('cod/payment')->getCollection()
                ->addFieldToFilter('name', $methodName);
            if (count($paymentMethodCollection) > 0) {
                $paymentMethod = $paymentMethodCollection->getFirstItem();
            } else {
                $data = [];
                $data['name'] = $methodName;
                $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                $paymentMethod = Mage::getModel('cod/payment')->setData($data)->save();
            }
            $paymentMethodId = $paymentMethod->getId();
        }
        //save payment record
        $data = [];
        $data['payment_id'] = $paymentMethodId;
        if (trim($transaction->getTxnId()) !== "P") {
            if ($type == 'capture') {
                $data['type'] = 'invoice';
                $data['invoice_id'] = $invoiceId;
                if ($isPartialPayment) {
                    if (Mage::registry('add_installment_amount_payment_grid')) {
                        $data['total'] = Mage::registry('add_installment_amount_payment_grid');
                    } else {
                        $data['total'] = $payment->getBaseAmountPaidOnline();
                    }
                } else {
                    $data['total'] = $payment->getBaseAmountPaid();
                }
                $data['increment_id'] = $invoiceIncrementId;
                $data['number'] = OpenTechiz_Payment_Helper_Data::ONLINE_CAPTURE_PAYMENT_NOTE;
            } elseif ($type == 'refund') {
                $data['type'] = 'creditmemo';
                $data['total'] = -1 * abs($payment->getAmountRefunded());
                $creditMemo = $order->getCreditmemosCollection()->getFirstItem();
                if ($creditMemo && $creditMemo->getId()) {
                    $data['credit_memo_id'] = $creditMemo->getId();
                } else {
                    $creditmemoCollection = Mage::getModel('sales/order_creditmemo')->getCollection()
                        ->addFieldToFilter('order_id', $order->getId());
                    if (count($creditmemoCollection)) {
                        $creditMemo = $creditmemoCollection->getLastItem();
                        $data['credit_memo_id'] = $creditMemo->getId();
                    }
                }
                $data['number'] = OpenTechiz_Payment_Helper_Data::ONLINE_REFUNDED_PAYMENT_NOTE;
            }
            $data['order_id'] = $order->getId();
            $data['user_id'] = Mage::helper('opentechiz_payment')->getUserId();
            $data['billing_name'] = $order->getBillingAddress()->getName();
            $data['email'] = $order->getBillingAddress()->getEmail();
            $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['received_date'] = Mage::getModel('core/date')->gmtDate('Y-m-d');
            if (!is_null(Mage::registry("payment_record_data"))) {
                $data = Mage::registry("payment_record_data");
            }
            $data['transaction_id'] = $transaction->getId();
            $paymentRecord = Mage::getModel('cod/paymentgrid');
            $paymentRecord->setData($data)->save();
        }
    }

    public function invoiceSaveAfterChangeState(Varien_Event_Observer $observer){
        if(Mage::registry('import_data_flag')){
            return;
        }

        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        if($invoice->getState() == 3) {
            return;
        }
        //invoice have total = 0
        if($invoice->getData('base_grand_total') == 0){
            if($invoice->getState() == 1) { // invoice is pending
                $invoice->setState(2)->save();
            }
            return;
        }
        $paymentRecordCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->addFieldToFilter('invoice_id', $invoice->getId());
        $totalPaid = 0;
        if(count($paymentRecordCollection) > 0) {
            foreach ($paymentRecordCollection as $paymentRecord) {
                $totalPaid += $paymentRecord->getTotal();
            }
            if ((round($invoice->getGrandTotal(), 2) <= round($totalPaid, 2)) && ($invoice->getState() != 2)) {
                $invoice->setState(2);
                $invoice->setPaidAmount($totalPaid);
                $invoice->setBasePaidAmount($totalPaid);
                $invoice->setRemainingAmount(0);
                $invoice->setBaseRemainingAmount(0);
                $invoice->save();
            } elseif((round($invoice->getGrandTotal(), 2) > round($totalPaid, 2)) && ($invoice->getState() != 1)) {
                $invoice->setState(1);
                $invoice->save();
            }
        }else{
            if($invoice->getState() != 1) {
                $invoice->setState(1);
                $invoice->save();
            }
        }
    }

    public function creditmemoSaveAfterChangeState(Varien_Event_Observer $observer){
        if(Mage::registry('import_data_flag')){
            return;
        }

        if(Mage::registry('creditmemoUpdated')) return;
        Mage::register('creditmemoUpdated', true);

        $creditMemo = $observer->getEvent()->getCreditmemo();
        $order = $creditMemo->getOrder();
        if($creditMemo->getState() == 3) {
            return;
        }
        $paymentRecordCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->addFieldToFilter('credit_memo_id', $creditMemo->getId());
        $totalPaid = 0;
        //refund to credit
        //add store credit payment method
        if($creditMemo->getCreditTotalRefunded() > 0) {
            $paymentMethod = Mage::helper('opentechiz_salesextend')->getPaymentMethodForStoreCredit();
            $paymentRecordCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                ->addFieldToFilter('credit_memo_id', $creditMemo->getId())
                ->addFieldToFilter('payment_id', $paymentMethod->getPaymentId());
            if(count($paymentRecordCollection) == 0){

                $data = [];
                $data['credit_memo_id'] = $creditMemo->getId();
                $data['order_id'] = $order->getId();
                $data['payment_id'] = $paymentMethod->getPaymentId();
                $data['total'] = -1 * $creditMemo->getCreditTotalRefunded();
                $data['billing_name'] = $order->getBillingAddress()->getName();
                $data['email'] = $order->getBillingAddress()->getEmail();
                $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                $data['received_date'] = Mage::getModel('core/date')->gmtDate('Y-m-d');
                $data['type'] = 'creditmemo';

                $newPaymentRecord = Mage::getModel('cod/paymentgrid');
                $newPaymentRecord->setData($data)->save();
            }
        }

        //change state
        $paymentRecordCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->addFieldToFilter('credit_memo_id', $creditMemo->getId());
        $grandTotal = round($creditMemo->getGrandTotal() + $creditMemo->getCreditTotalRefunded(), 2);
        foreach ($paymentRecordCollection as $paymentRecord) {
            $totalPaid += (-1)*$paymentRecord->getTotal();
        }
        if (($grandTotal <= $totalPaid) && ($creditMemo->getState() != 2)) {
            $creditMemo->setState(2);
            $creditMemo->save();
        } elseif(($grandTotal > $totalPaid) && ($creditMemo->getState() != 1)) {
            $creditMemo->setState(1);
            $creditMemo->save();
        }
        //add log when credit memo
        $creditMemo = Mage::getModel('sales/order_creditmemo')->load($creditMemo->getId());
        if($order->getOrderStage() !=5){
            foreach($creditMemo->getAllItems() as $item){
                if(Mage::registry('list_item_released_qty')){
                    $check_qty_released = Mage::registry('list_item_released_qty');
                    if(array_key_exists($item->getOrderItemId(),$check_qty_released)){
                        if($check_qty_released[$item->getOrderItemId()] > 0){
                            $stock = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', $item->getSku())->getFirstItem();
                            Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                                'qty' => (int)$item->getQty(),
                                'on_hold' => 0,
                                'type' => 17,
                                'stock_id' => $stock->getId(),
                                'qty_before_movement' => $stock->getQty(),
                                'on_hold_before_movement' => $stock->getOnHold(),
                                'sku' => $stock->getSku(),
                                'order_id' => $order->getId()
                            ));
                        }
                    }

                }
            }
        }


    }

    public function creditmemoSaveBeforeCheckForPartialRefund(Varien_Event_Observer $observer){
        $creditMemo = $observer->getEvent()->getCreditmemo();
        if($creditMemo->getState() != 3){
            return;
        }
        $refundCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->addFieldToFilter('credit_memo_id', $creditMemo->getId());
        $totalRefunded = 0;
        foreach ($refundCollection as $refundRecord){
            $totalRefunded += $refundRecord->getTotal();
        }
        if($totalRefunded != 0) {
            Mage::getSingleton('adminhtml/session')->addError("Cannot cancel CreditMemo, exist partial refunded payment.");
            Mage::app()->getResponse()
                ->setRedirect(Mage::getUrl('adminhtml/sales_order_creditmemo/view', array('creditmemo_id' => $creditMemo->getId())))
                ->sendResponse();
            exit;
        }
    }

    public function invoiceSaveBeforeCheckForPartialRefund(Varien_Event_Observer $observer){
        $invoice = $observer->getEvent()->getInvoice();
        if($invoice->getState() != 3){
            return;
        }
        $canCancel = true;
        foreach ($invoice->getAllItems() as $item) {
            $orderItem = $item->getOrderItem();
            if($orderItem->getQtyShipped() > 0){
                $canCancel = false;
                break;
            }
        }
        if(!$canCancel) {
            Mage::getSingleton('adminhtml/session')->addError("Cannot cancel the Invoice because the item has shipped.");
            Mage::app()->getResponse()
                ->setRedirect(Mage::getUrl('adminhtml/sales_order_invoice/view', array('invoice_id' => $invoice->getId())))
                ->sendResponse();
            exit;
        }
        $recordCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->addFieldToFilter('invoice_id', $invoice->getId());
        $totalPayment = 0;
        foreach ($recordCollection as $record){
            $totalPayment += $record->getTotal();
        }
        if($totalPayment > 0) {
            Mage::getSingleton('adminhtml/session')->addError("Cannot cancel Invoice, exist partial refunded payment.");
            Mage::app()->getResponse()
                ->setRedirect(Mage::getUrl('adminhtml/sales_order_invoice/view', array('invoice_id' => $invoice->getId())))
                ->sendResponse();
            exit;
        }
    }

    public function addPaymentRecordToInvoiceAfterInvoiceSave($observer){
        if(Mage::registry('import_data_flag')){
            return;
        }

        $invoice = $observer->getInvoice();
        $order = $invoice->getOrder();

        $paymentRecordCollection = Mage::getModel('cod/paymentgrid')->getCollection()->addFieldToFilter('invoice_id', 0)
            ->addFieldToFilter('order_id', $order->getId())->addFieldToFilter('credit_memo_id', 0);
        if(count($paymentRecordCollection)){
            foreach ($paymentRecordCollection as $paymentRecord){
                $paymentRecord->setInvoiceId($invoice->getId())->setIncrementId($invoice->getIncrementId())->save();
            }
        }
    }

    public function addPaymentRecordToCreditMemoAfterSave($observer){
        if(Mage::registry('import_data_flag')){
            return;
        }

        $creditMemo = $observer->getEvent()->getCreditmemo();
        $order = $creditMemo->getOrder();

        $paymentRecordCollection = Mage::getModel('cod/paymentgrid')->getCollection()->addFieldToFilter('invoice_id', 0)
            ->addFieldToFilter('order_id', $order->getId())->addFieldToFilter('credit_memo_id', 0);
        if(count($paymentRecordCollection)){
            foreach ($paymentRecordCollection as $paymentRecord){
                $paymentRecord->setCreditMemoId($creditMemo->getId())->save();
            }
        }
    }

    public function paymentRecordSaveChangeInvoiceAndCreditMemoStatus($observer){
        $paymentgrid = $observer->getEvent()->getPaymentgrid();

        if($paymentgrid->getCreditMemoId()){
            $creditMemo = Mage::getModel("sales/order_creditmemo")->load($paymentgrid->getData('credit_memo_id'));
            if (!$creditMemo || !$creditMemo->getId()) return;
            if ($creditMemo->getState() == 3) return;
            $total = 0;
            foreach (Mage::getModel('cod/paymentgrid')->getCollection()
                         ->addFieldToFilter('credit_memo_id', $creditMemo->getId()) as $record) {
                $total += (-1)*$record->getTotal();
            }
            $creditMemoGrandTotal = round($creditMemo->getCreditTotalRefunded() + $creditMemo->getGrandTotal(), 2);
            if ($total < $creditMemoGrandTotal && $creditMemo->getState() != 1) {
                $creditMemo->setState(1);
            } elseif ($creditMemoGrandTotal == $total && $creditMemo->getState() != 2) {
                $creditMemo->setState(2);
            } elseif ($creditMemoGrandTotal < $total && !Mage::registry('return_exchange_order_create')){
                Mage::getSingleton('adminhtml/session')->addError("Total refunded is higher than Credit Memo grand total.");
                Mage::app()->getResponse()
                    ->setRedirect(Mage::getUrl('adminhtml/sales_order_creditmemo/view', array('creditmemo_id' => $creditMemo->getId())))
                    ->sendResponse();
                exit;
            }
            $creditMemo->save();
        }else {
            $invoice = Mage::getModel("sales/order_invoice")->load($paymentgrid->getData('invoice_id'));
            if (!$invoice || !$invoice->getId()) return;
            if ($invoice->getState() == 3) return;
            $total = 0;
            foreach (Mage::getModel('cod/paymentgrid')->getCollection()
                         ->addFieldToFilter('invoice_id', $invoice->getId()) as $record) {
                $total += $record->getTotal();
            }
            if ($total < $invoice->getGrandTotal() && $invoice->getState() != 1) {
                $invoice->setState(1);
            } elseif ($invoice->getGrandTotal() <= $total && $invoice->getState() != 2) {
                $invoice->setState(2);
            }
            $invoice->save();
        }
    }

    public function paymentRecordDeleteChangeInvoiceAndCreditMemoStatus($observer){
        $paymentgrid = $observer->getEvent()->getPaymentgrid();
        if($paymentgrid->getCreditMemoId()){
            $creditMemo = Mage::getModel("sales/order_creditmemo")->load($paymentgrid->getData('credit_memo_id'));
            if (!$creditMemo || !$creditMemo->getId()) return;
            if ($creditMemo->getState() == 3) return;
            $deletedPayment = abs($paymentgrid->getTotal());
            $total = 0;
            foreach (Mage::getModel('cod/paymentgrid')->getCollection()
                         ->addFieldToFilter('credit_memo_id', $creditMemo->getId()) as $record) {
                $total += (-1)*$record->getTotal();
            }
            $total -= $deletedPayment;
            $creditMemoGrandTotal = $creditMemo->getCreditTotalRefunded() + $creditMemo->getGrandTotal();
            if ($total < $creditMemoGrandTotal && $creditMemo->getState() != 1) {
                $creditMemo->setState(1);
            } elseif ($creditMemoGrandTotal == $total && $creditMemo->getState() != 2) {
                $creditMemo->setState(2);
            }
            $creditMemo->save();
        }else {
            $invoice = Mage::getModel("sales/order_invoice")->load($paymentgrid->getData('invoice_id'));
            if (!$invoice || !$invoice->getId()) return;
            if ($invoice->getState() == 3) return;
            $deletedPayment = $paymentgrid->getTotal();
            $total = 0;
            foreach (Mage::getModel('cod/paymentgrid')->getCollection()
                         ->addFieldToFilter('invoice_id', $invoice->getId()) as $record) {
                $total += $record->getTotal();
            }
            $total -= $deletedPayment;
            if ($total < $invoice->getGrandTotal() && $invoice->getState() != 1) {
                $invoice->setState(1);
            } elseif ($invoice->getGrandTotal() == $total && $invoice->getState() != 2) {
                $invoice->setState(2);
            }
            $invoice->save();
        }
    }

    public function MWOrderEditChangeAuthorizedAmount($observer){
        $order = $observer->getEvent()->getOrder();
        $origOrder = $observer->getEvent()->getOrigOrder();

        if(count($order->getInvoiceCollection()) == 0) {
            if ($origOrder->getGrandTotal() != $order->getGrandTotal()) {
                $diff = (double)$order->getGrandTotal() - (double)$origOrder->getGrandTotal();
                $collection = Mage::getModel('sales/order_payment_transaction')->getCollection()
                    ->setOrderFilter($order)->addFieldToFilter('txn_type', 'authorization');
                foreach ($collection as $transaction){
                    $children = Mage::getModel('sales/order_payment_transaction')->getCollection()
                        ->addFieldToFilter('parent_txn_id', $transaction->getTxnId());
                    if(count($children) == 0){
                        $currentAuthorizeTransaction = $transaction;
                        break;
                    }
                }
                if(isset($currentAuthorizeTransaction)){
                    $payment = Mage::getModel('sales/order_payment')->load($currentAuthorizeTransaction->getPaymentId());
                    $payment->setbase_amount_authorized($payment->getbase_amount_authorized() + $diff)
                        ->setamount_authorized($payment->getamount_authorized() + $diff)
                        ->setbase_amount_ordered($payment->getbase_amount_ordered() + $diff)
                        ->setamount_ordered($payment->getamount_ordered() + $diff)
                        ->save();
                }
            }
        }
    }

    public function removeItemReservedAndProductionAfterOrderEdit($observer){
        $order = $observer->getEvent()->getOrder();
        //skip for repair order (#2472)
        if($order->getOrderType() == 2) {
            //update return's return fee amount
            $orderitems = $order->getAllItems();
            $returnFee = 0;
            foreach ($orderitems as $item){
                if($item->getSku() == OpenTechiz_Return_Helper_Data::RETURN_FEE_ITEM_SKU){
                    $returnFee = $item->getBaseRowTotal();
                }
            }
            $return = Mage::getModel('opentechiz_return/return')->getCollection()
                ->addFieldToFilter('return_fee_order_id', $order->getIncrementId())
                ->getFirstItem();
            $return->setReturnFee($returnFee)->save();

            //return
            return;
        }

        $origOrder = $observer->getEvent()->getOrigOrder();
        $changes = $observer->getEvent()->getChanges();
        if($origOrder->getOrderStage() < 2) { //#1633 prevent going up to fulfillment from lower stage
            return;
        }
        if(array_key_exists('quote_items', $changes)) {
            $newOrderItems = [];
            $orderItemIdToCheckReserved = [];
            foreach ($order->getAllItems() as $item) {
                $newOrderItems[$item->getId()] = $item->getSku();
                $orderItemIdToCheckReserved[$item->getId()] = $item->getQtyOrdered() - $item->getQtyRefunded() - $item->getQtyCanceled();
            }
            foreach ($origOrder->getAllItems() as $item) {
                if (!array_key_exists($item->getId(), $newOrderItems)) {
                    $orderItemIdToCheckReserved[$item->getId()] = 0;
                }
            }
            foreach ($orderItemIdToCheckReserved as $orderItemId => $qty) {
                $linkCollection = Mage::getModel('opentechiz_inventory/order')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItemId);
                foreach ($linkCollection as $link) {
                    $item = Mage::getModel('opentechiz_inventory/item')->load($link->getItemId());
                    if (in_array($item->getState(), OpenTechiz_Inventory_Helper_Data::RESERVED_STATE)) {
                        //put item back to stock
                        $item->setState(35)->save();
                    }
                    $link->delete();
                }
            }
            $orderItems = $order->getAllItems();
            //delete old process request
            //will be deleted after order item is deleted, in observer order_item_delete_remove_process_request
            foreach ($orderItems as $orderItem) {
                //delete instock request
                $instockRequestCollection = Mage::getModel('opentechiz_inventory/inventory_instockRequest')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItem->getId());
                foreach ($instockRequestCollection as $instockRequest){
                    $instockRequest->delete();
                }

                //remove order item id from production request
                $productionRequestCollection = Mage::getModel('opentechiz_production/product')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItem->getId());
                foreach ($productionRequestCollection as $productionRequest){
                    $productionRequest->setOrderItemId(null)->save();
                }
            }
            //remove status items ready
            $orderStatuses = $order->getInternalStatuses();
            $orderStatuses = explode(',', $orderStatuses);
            foreach ($orderStatuses as $key => $value){
                if($value == 6) unset($orderStatuses[$key]);
                if($value == 2) unset($orderStatuses[$key]);
            }
            $orderStatuses = implode(',', $orderStatuses);
            $order->setInternalStatuses($orderStatuses)->save();
            //add new process request
            foreach ($orderItems as $orderItem) {
                $orderItem = Mage::getModel('sales/order_item')->load($orderItem->getId());
                $productId = $orderItem->getProductId();
                if(!$productId || !Mage::getModel('catalog/product')->load($productId) || $orderItem->getProductType() == 'virtual'){
                    continue;
                }
                $itemModel = Mage::getModel('opentechiz_salesextend/item');
                $itemModel->requestToProcess($orderItem->getId());
                $orderItem->setIsRequestedToProcess(1)->save();
            }
            //send email to notify
            $oldStatus = $order->getInternalStatus();
            if($oldStatus == 4 || $oldStatus == 2) { //Waiting on Vendor, Item(s) Manufacturing
                $reason = 'Order status change on Order Edit';
                Mage::helper('opentechiz_salesextend/email')->emailNotiOrderStatusChange($order, $oldStatus, 1, $reason);
            }

            //remove status resize/engrave from passed list
            Mage::helper('opentechiz_salesextend')->removeStatusFromOrder(3, $order);

            Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 2, 1);
        }
    }

    public function addOrderCommentAfterChangeStage($observer){
        $order = $observer->getEvent()->getOrder();
        if($order->getOrderType() == 1){
            $totalPaid = Mage::helper('cod')->_getTotalPaidPayment($order->getId());
            $totalPaid = round($totalPaid, 4);
            $orderTotal = round($order->getBaseGrandTotal(), 4);
            if($totalPaid < $orderTotal && $order->getOrderStage() == 3){
                $hasComment = Mage::getModel('sales/order_status_history')->getCollection()->addFieldToFilter('comment', 'Items are ready and awaiting final payment')
                    ->addFieldToFilter('parent_id', $order->getId())
                    ->getSize();
                if($hasComment == 0) {
                    $order->addStatusHistoryComment('Items are ready and awaiting final payment');
                    $order->save();
                }
            }elseif($totalPaid >= $orderTotal && $order->getOrderStage() == 3){
                $hasComment = Mage::getModel('sales/order_status_history')->getCollection()->addFieldToFilter('comment', 'Items are ready and all installments are paid')
                    ->addFieldToFilter('parent_id', $order->getId())
                    ->getSize();
                if($hasComment == 0) {
                    $order->addStatusHistoryComment('Items are ready and all installments are paid');
                    $order->save();
                }
            }
        }
    }

    public function checkShipmentTrackingAndUpdateStatus(Varien_Event_Observer $observer){
        $shipment = $observer->getEvent()->getShipment();
        $order = $shipment->getOrder();
        $trackingCollection = Mage::getModel('sales/order_shipment_track')->getCollection()->addFieldToFilter('order_id', $order->getId());
        if(count($trackingCollection) > 0) {
            Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 5, 12);
        }else{
            Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 5, 22);
        }
        $check_create_shipment = $shipment->getOrigData();
        if(is_null($check_create_shipment)){
            $itemsCollection = $shipment->getItemsCollection();
            foreach ($itemsCollection as $shipItem){
                $qty = $shipItem->getQty();
                $stock = Mage::getModel('opentechiz_inventory/instock')->load($shipItem->getSku(), 'sku');
                if($stock->getId()){
                    //log stock
                    Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                        'qty' => 0,
                        'on_hold' => 0,
                        'type' => 20,
                        'stock_id' => $stock->getId(),
                        'qty_before_movement' => $stock->getQty(),
                        'on_hold_before_movement' => $stock->getOnHold(),
                        'sku' => $stock->getSku(),
                        'order_id' => $order->getId()
                    ));
                }


            }
        }


    }

    public function checkOrderInfoBeforeShip(Varien_Event_Observer $observer){
        if(Mage::registry("skipCheckOrderInfoBeforeShip")) {
            return;
        }
        $reservedItemState = OpenTechiz_Inventory_Helper_Data::RESERVED_STATE;
        $shipment = $observer->getEvent()->getShipment();
        //logic only apply for shipment create
        //prevent apply logic to POS
        if(!$shipment->isObjectNew() || (stripos(Mage::app()->getRequest()->getControllerName(), 'pointofsales') !== false)){
            return;
        }
        $order = $shipment->getOrder();
        $skus = Mage::helper('opentechiz_salesextend')->checkProfitOrder($order);
        $orderType = $order->getOrderType();
        if($skus != '' && $orderType != 2){
            Mage::getSingleton('adminhtml/session')->addError("These item(s) has insufficient profit compare to Last Cost: '.$skus.'\nPlease check the order again. You can skip cost check in order items view.");
            Mage::app()->getResponse()
                ->setRedirect(Mage::getUrl('adminhtml/sales_order/view', array('order_id' => $order->getId())))
                ->sendResponse();
            exit;
        }

        //check for number of reserved items match
        $itemsCollection = $shipment->getItemsCollection();
        foreach ($itemsCollection as $shipItem){
            $qty = $shipItem->getQty();
            $orderItemId = $shipItem->getOrderItemId();
            //check for virtual
            $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);
            if($orderItem->getProductType() == 'virtual'){
                continue;
            }
            $reservedOrderItem = Mage::getModel('opentechiz_inventory/order')->getCollection()
                ->addFieldToFilter('order_item_id', $orderItemId)
                ->join(array('item' => 'opentechiz_inventory/item'), 'item.item_id = main_table.item_Id', '*')
                ->addFieldToFilter('state', array('in' => $reservedItemState));
            $qtyShipped = $orderItem->getQtyShipped();
            $qtyReserved = count($reservedOrderItem);
            $qtyNotYetShipped = $qtyReserved - $qtyShipped;
            if($qtyNotYetShipped < $qty){
                Mage::getSingleton('adminhtml/session')->addError("Shipping ".$qty." item ".$shipItem->getSku()." but ".count($reservedOrderItem)." reserved.");
                Mage::app()->getResponse()
                    ->setRedirect(Mage::getUrl('adminhtml/sales_order/view', array('order_id' => $order->getId())))
                    ->sendResponse();
                exit;
            }
        }
    }

    public function changeOrderItemLinkFromReturnOrderToOriginalOrder($observer){
        $shipment = $observer->getEvent()->getShipment();
        $order = $shipment->getOrder();
        if($order->getOrderType() == 2){
            $returnId = '';
            foreach ($order->getAllItems() as $orderItem){
                $description = $orderItem->getDescription();
                $returnIncrement = explode(' ', $description);
                $returnId = $returnIncrement[count($returnIncrement) - 1];
                if(strpos($returnId, '#') !== false){
                    break;
                }
            }
            $returnIncrement = trim($returnId, '#');
            $return = Mage::getModel('opentechiz_return/return')->load($returnIncrement, 'increment_id');
            if($return && $return->getId()) {
                $itemBarcode = $return->getItemBarcode();

                $item = Mage::getModel('opentechiz_inventory/item')->load($itemBarcode, 'barcode');
                $item->setState(15)->save(); //shipped to customer
                $itemId = $item->getId();
                //delete old order-item link
                $oldOrderLink = Mage::getModel('opentechiz_inventory/order')->getCollection()
                    ->addFieldToFilter('item_id', $itemId)
                    ->getFirstItem();
                if ($oldOrderLink && $oldOrderLink->getId()) {
                    $oldOrderLink->setOrderItemId($return->getOrderItemId())->save();
                }
            }
        }
    }

    public function creditmemoCheckItemReleased(Varien_Event_Observer $observer){
        $creditMemo = $observer->getEvent()->getCreditmemo();
        $order = $creditMemo->getOrder();
        $allItem = $order->getAllItems();
        $list_qty_released = array();
        foreach($allItem as $item){
            $qty_release = Mage::helper('opentechiz_inventory')->countReleasedItem($item->getId());
            $list_qty_released[$item->getId()] = $qty_release;
        }
        if(!Mage::registry('list_item_released_qty')){
            Mage::register('list_item_released_qty', $list_qty_released);
        }
    }

    public function addCheckPaymentRecordToChangeStatus($observer){
        $order = $observer->getEvent()->getOrder();
        if($order->getOrderType() == 1){
            $paidAmount = Mage::helper('opentechiz_salesextend')->getTotalPaymentRecordForOrder($order);
            $paidAmount = round($paidAmount, 4);
            $orderTotal = round($order->getBaseGrandTotal(), 4);
            if(($paidAmount >= $orderTotal/2) && !Mage::helper('opentechiz_salesextend')->orderHasStatus(15, $order)){
                if($order->getOrderStage() < 2) { //if order not yet move to fulfilment
                    Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 1, 15);
                }else{ //else only add to status list
                    Mage::helper('opentechiz_salesextend')->addStatusToOrder(15, $order);
                }
            }
            if($paidAmount >= $orderTotal){
                if($order->getOrderStage() == 3 && $order->getInternalStatus() == 26) {
                    Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 3, 6);
                }
            }
        }
    }

    public function addCcCidToQuotePayment($observer){
        if(Mage::getSingleton('adminhtml/session_quote') && Mage::getSingleton('adminhtml/session_quote')->getQuote()){
            $quotePayment = $observer->getEvent()->getPayment();
            $params = Mage::app()->getRequest()->getParams();
            if(isset($params['payment']['cc_cid']) && $params['payment']['cc_cid']){
                $cc_cid = $params['payment']['cc_cid'];
                $quotePayment->setData('cc_cid_enc', Mage::getModel('payment/info')->encrypt($cc_cid));
            }
        }
    }

    public function addTrackNotifyEmail(Varien_Event_Observer $observer)
    {
        /* @var $shipment Mage_Sales_Model_Order_Shipment */
        $shipment = $observer->getShipment();
        if (!$shipment) {
            return;
        }
        $order = $shipment->getOrder();
        if (!$order) {
            return;
        }
        /* @var $tracks Mage_Sales_Model_Resource_Order_Shipment_Track_Collection */
        $tracks = $shipment->getTracksCollection();
        if (!empty($tracks)) {
            foreach ($tracks as $track) {
                if ($track->getNotifyEmail()) {
                    continue;
                }
                $this->sendNotifyTracking($track, $order, $shipment);
            }
        }
    }

    public function sendNotifyTracking($track, $order, $shipment)
    {
        $storeId = $order->getStore()->getId();
        if ($order->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $order->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId);
            $customerName = $order->getCustomerName();
        }
        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($order->getCustomerEmail(), $customerName);
        $mailer->addEmailInfo($emailInfo);
        $mailer->setSender(Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_UPDATE_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $trackingNumber = $track->getTrackNumber();
        if ($trackUrl = $this->getTrackUrl($track)) {
            $trackingNumber = sprintf('<a href="%s">%s</a>', $trackUrl, $trackingNumber);
        }
        try {
            // Set all required params and send emails
            $mailer->setTemplateParams(array(
                'order' => $order,
                'shipment' => $shipment,
                'comment' => 'Shipped on ' . date("m/d/Y") . ' via ' . $track->getTitle() . ', Tracking number ' . $trackingNumber,
                'billing' => $order->getBillingAddress()
            ));
            $mailer->send();
            $track->setNotifyEmail(true);
            $track->save();
        } catch (Exception $exc) {
        }
    }

    /**
     * @param Mage_Sales_Model_Order_Shipment_Track $track
     * @return false|string
     */
    private function getTrackUrl($track)
    {
        $trackNumber = $track->getTrackNumber();
        $carrierCode = $track->getCarrierCode();
        if ($carrierCode == 'custom') {
            $carrierCode = $this->parseCustomCarrierByTitle($track->getTitle());
        }
        switch ($carrierCode) {
            case 'fedex':
                return sprintf('https://www.fedex.com/fedextrack/?trknbr=%s', $trackNumber);
            case 'usps':
                return sprintf('https://tools.usps.com/go/TrackConfirmAction.action?tLabels=%s', $trackNumber);
            case 'ups':
                return sprintf('https://www.ups.com/track?loc=en_US&tracknum=%s', $trackNumber);
        }

        return false;
    }

    /**
     * @param string $title
     * @return false|string
     */
    private function parseCustomCarrierByTitle($title)
    {
        foreach (['fedex', 'usps', 'ups'] as $carrier) {
            if (stripos($title, $carrier) !== false) {
                return $carrier;
            }
        }

        return false;
    }
}
