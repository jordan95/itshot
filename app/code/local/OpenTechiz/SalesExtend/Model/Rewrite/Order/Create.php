<?php
class OpenTechiz_SalesExtend_Model_Rewrite_Order_Create extends Mage_Adminhtml_Model_Sales_Order_Create
{
    /**
     * Create new order
     *
     * @return Mage_Sales_Model_Order
     */
    public function createOrder()
    {
        $this->_prepareCustomer();
        $this->_validate();
        $quote = $this->getQuote();
        $this->_prepareQuoteItems();

        $service = Mage::getModel('sales/service_quote', $quote);
        /** @var Mage_Sales_Model_Order $oldOrder */
        $oldOrder = $this->getSession()->getOrder();
        if ($oldOrder->getId()) {
            $originalId = $oldOrder->getOriginalIncrementId();
            if (!$originalId) {
                $originalId = $oldOrder->getIncrementId();
            }
            $orderData = array(
                'original_increment_id'     => $originalId,
                'relation_parent_id'        => $oldOrder->getId(),
                'relation_parent_real_id'   => $oldOrder->getIncrementId(),
                'edit_increment'            => $oldOrder->getEditIncrement()+1,
                'increment_id'              => $originalId.'-'.($oldOrder->getEditIncrement()+1)
            );
            $quote->setReservedOrderId($orderData['increment_id']);
            $service->setOrderData($orderData);

            $oldOrder->cancel();
        }
        $orderData = array();
        if (!empty($_POST['order']['increment_id'])) {
            $orderExisted = Mage::getModel('sales/order')->load($_POST['order']['increment_id'], 'increment_id');
            if ($orderExisted->getId()) {
                Mage::throwException("Increment ID is duplicated.");
            }
            $orderData["increment_id"] = trim($_POST['order']['increment_id']);
        }
        if (!empty($_POST['order']['source'])) {
             $orderData["source"] = trim($_POST['order']['source']);
        }
        if(!empty($orderData)) {
            if(!empty($orderData['increment_id'])) {
                $quote->setReservedOrderId($orderData['increment_id']);
            }
            $service->setOrderData($orderData);
        }
        /** @var Mage_Sales_Model_Order $order */
        $order = $service->submit();
        $customer = $quote->getCustomer();
        if ((!$customer->getId() || !$customer->isInStore($this->getSession()->getStore()))
            && !$quote->getCustomerIsGuest()
        ) {
            $customer->setCreatedAt($order->getCreatedAt());
            $customer
                ->save()
                ->sendNewAccountEmail('registered', '', $quote->getStoreId());;
        }
        $orderChanged = false;
        if ($oldOrder->getId()) {
            $oldOrder->setRelationChildId($order->getId());
            $oldOrder->setRelationChildRealId($order->getIncrementId());
            $oldOrder->save();
            $orderChanged = true;
        }
        if ($this->getSendConfirmation()) {
            $order->queueNewOrderEmail();
        }
        Mage::dispatchEvent('checkout_submit_all_after', array('order' => $order, 'quote' => $quote));

        return $order;
    }
}