<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Shipping
 * @copyright  Copyright (c) 2006-2018 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class OpenTechiz_SalesExtend_Model_Rewrite_Shipment_Config extends Mage_Shipping_Model_Config
{
    /**
     * Retrieve carrier model instance by carrier code
     *
     * @param   string $carrierCode
     * @param   mixed $store
     * @return  Mage_Usa_Model_Shipping_Carrier_Abstract
     */
    public function getCarrierInstance($carrierCode, $store = null)
    {
        if(in_array($carrierCode, OpenTechiz_SalesExtend_Helper_Data::ACTUAL_SHIPPING_METHOD_CODES)
            && Mage::app()->getRequest()->getControllerName() != 'sales_order_shipment' && Mage::app()->getRequest()->getControllerName() != 'sales_order' && Mage::app()->getRequest()->getControllerName() != 'sales_shipment' && Mage::app()->getRequest()->getControllerName() != 'invoiceandshipment') return false;
        $carrierConfig =  Mage::getStoreConfig('carriers/'.$carrierCode, $store);
        if (!empty($carrierConfig)) {
            return $this->_getCarrier($carrierCode, $carrierConfig, $store);
        }
        return false;
    }
}