<?php
class OpenTechiz_SalesExtend_Model_Rewrite_Shipment_Shipping extends Mage_Shipping_Model_Shipping
{
    protected $_availabilityConfigField = 'active';

    /**
     * Get carrier by its code
     *
     * @param string $carrierCode
     * @param null|int $storeId
     * @return bool|Mage_Core_Model_Abstract
     */
    public function getCarrierByCode($carrierCode, $storeId = null)
    {
        if(in_array($carrierCode, OpenTechiz_SalesExtend_Helper_Data::ACTUAL_SHIPPING_METHOD_CODES)
            && Mage::app()->getRequest()->getControllerName() != 'sales_order_shipment') return false;
        if (!Mage::getStoreConfigFlag('carriers/'.$carrierCode.'/'.$this->_availabilityConfigField, $storeId)) {
            return false;
        }
        $className = Mage::getStoreConfig('carriers/'.$carrierCode.'/model', $storeId);
        if (!$className) {
            return false;
        }
        $obj = Mage::getModel($className);
        if ($storeId) {
            $obj->setStore($storeId);
        }
        return $obj;
    }
}