<?php
class OpenTechiz_SalesExtend_Model_Model_Rewrite_Shipment_Fedex extends Mage_Usa_Model_Shipping_Carrier_Fedex
{
    /**
     * Collect and get rates
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result|bool|null
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag($this->_activeFlag) || (Mage::app()->getRequest()->getControllerName() != 'sales_order_shipment') || !Mage::app()->getStore()->isAdmin()) {
            return false;
        }
        $this->setRequest($request);

        $this->_getQuotes();

        $this->_updateFreeMethodQuote($request);

        return $this->getResult();
    }
}