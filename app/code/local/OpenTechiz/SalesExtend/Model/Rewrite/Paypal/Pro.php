<?php

class OpenTechiz_SalesExtend_Model_Rewrite_Paypal_Pro extends Mage_Paypal_Model_Pro
{
    /**
     *
     * @param Mage_Sales_Model_Order_Payment $payment
     * @return bool
     */

    public function importPaymentInfo(Varien_Object $from, Mage_Payment_Model_Info $to)
    {
        // update PayPal-specific payment information in the payment object
//        $keys = ['notify_url', 'inv_num', 'protection_eligibility'];
//        $flag = false;
//        foreach ($keys as $key){
////            Mage::log($key);
//            if(array_key_exists($key, $from->getData())){
////                Mage::log('true');
//                $flag = true;
//            }
//        }
//        Mage::log($from->getData());
//        Mage::log('flag - '.$flag);
//        if($flag == false){
//            $from->setData('payment_status', 'completed');
//            $from->setData('pending_reason', '');
//        }

        $this->getInfo()->importToPayment($from, $to);
        /**
         * Detect payment review and/or frauds
         * PayPal pro API returns fraud results only in the payment call response
         */
        if ($from->getDataUsingMethod(Mage_Paypal_Model_Info::IS_FRAUD)) {
            $to->setIsTransactionPending(true);
            $to->setIsFraudDetected(true);
        } elseif ($this->getInfo()->isPaymentReviewRequired($to)) {
            $to->setIsTransactionPending(true);
        }

        // give generic info about transaction state
        if ($this->getInfo()->isPaymentSuccessful($to)) {
            $to->setIsTransactionApproved(true);
        } elseif ($this->getInfo()->isPaymentFailed($to)) {
            $to->setIsTransactionDenied(true);
        }

        return $this;
    }

    public function isPaymentComplete(Mage_Payment_Model_Info $payment){
        $paymentStatus = $payment->getAdditionalInformation(Mage_Paypal_Model_Info::PAYMENT_STATUS_GLOBAL);
        if ($paymentStatus == Mage_Paypal_Model_Info::PAYMENTSTATUS_COMPLETED) {
            return true;
        }else return false;
    }

    public function UpdateInvoiceStatusNew($invoiceId, $invoice_payment_total) {
        $invoice_row = Mage::getModel('sales/order_invoice')->load($invoiceId);
        $invoice_grid_row = Mage::getModel('cod/invoicegrid')->load($invoiceId);

        // $this->UpdateInvoiceTotals($invoiceId, $invoice_paymen_total);

        $data_status['paid_base_fee_amount'] = $invoice_payment_total;
        $data_status['paid_fee_amount'] = $invoice_payment_total;
        $grand_total = $invoice_row->getGrand_total();
        $totaldue = $grand_total - $invoice_payment_total;
        $data_status['fee_amount'] = $totaldue;
        $data_status['base_fee_amount'] = $totaldue;
        $invoice_row->addData($data_status);

        $total_pending = $grand_total - $invoice_payment_total;
        $data['cod_total_received'] = $invoice_payment_total;
        $data['cod_total_pending'] = $total_pending;

        $payment_invoice_rows = Mage::getModel('cod/paymentgrid')->getCollection()
            ->addfieldtofilter('invoice_id', array('eq' => $invoiceId));
        $total = 0;
        if ($payment_invoice_rows->count() > 0) {
            foreach ($payment_invoice_rows as $item) {
                $total += $item->getTotal();
            }
        }
        if (abs($grand_total - $total) < 0.00001) {
            $data['state'] = 2;
            $data_status['state'] = 2;
        } else {
            $data['state'] = 1;
            $data_status['state'] = 1;
        }
        $invoice_grid_row->addData($data);
        $invoice_grid_row->save();
        $invoice_row->addData($data_status);
        $invoice_row->save();

//        $this->UpdateOrderTotals($invoice_row->getOrder_id());
    }

}