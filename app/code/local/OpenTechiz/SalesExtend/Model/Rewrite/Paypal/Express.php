<?php
class OpenTechiz_SalesExtend_Model_Rewrite_Paypal_Express extends Milople_Partialpayment_Model_Express
{
    protected function _placeOrder(Mage_Sales_Model_Order_Payment $payment, $amount)
    {
        $order = $payment->getOrder();
        foreach ($order->getAllItems() as $salesItem){
            $salesItem->setbase_row_total_incl_tax($salesItem->getrow_total_incl_tax())
                ->setbase_row_total($salesItem->getrow_total())->setbase_row_invoiced($salesItem->getrow_invoiced())
                ->setbase_tax_amount($salesItem->gettax_amount())->setbase_discount_amount($salesItem->getdiscount_amount())
                ->setbase_tax_invoiced($salesItem->gettax_invoiced())->setbase_discount_invoiced($salesItem->getdiscount_invoiced());
        }

        // prepare api call
        $token = $payment->getAdditionalInformation(Mage_Paypal_Model_Express_Checkout::PAYMENT_INFO_TRANSPORT_TOKEN);
        if($order->getBasePaidAmount() > 0)
            $amount = $order->getBasePaidAmount();
        $api = $this->_pro->getApi()
            ->setToken($token)
            ->setPayerId($payment->
            getAdditionalInformation(Mage_Paypal_Model_Express_Checkout::PAYMENT_INFO_TRANSPORT_PAYER_ID))
            ->setAmount($amount)
            ->setPaymentAction($this->_pro->getConfig()->paymentAction)
            ->setNotifyUrl(Mage::getUrl('paypal/ipn/'))
            ->setInvNum($order->getIncrementId())
            ->setCurrencyCode($order->getBaseCurrencyCode())
            ->setPaypalCart(Mage::getModel('paypal/cart', array($order)))
            ->setIsLineItemsEnabled($this->_pro->getConfig()->lineItemsEnabled);

        // call api and get details from it
        $api->callDoExpressCheckoutPayment();

        $this->_importToPayment($api, $payment);
        return $this;
    }
    
    public function order(Varien_Object $payment, $amount)
    {
        $paypalTransactionData = Mage::getSingleton('checkout/session')->getPaypalTransactionData();
        if (!is_array($paypalTransactionData)) {
            $this->_placeOrder($payment, $amount);
        } else {
            $this->_importToPayment($this->_pro->getApi()->setData($paypalTransactionData), $payment);
        }

        $payment->setAdditionalInformation($this->_isOrderPaymentActionKey, true);

        if ($payment->getIsFraudDetected()) {
            return $this;
        }

        $order = $payment->getOrder();
        $orderTransactionId = $payment->getTransactionId();

        $api = $this->_callDoAuthorize($amount, $payment, $orderTransactionId);

        $state  = Mage_Sales_Model_Order::STATE_PROCESSING;
        $status = true;

        $formatedPrice = $order->getBaseCurrency()->formatTxt($amount);
        if ($payment->getIsTransactionPending()) {
            $message = Mage::helper('paypal')->__('Ordering amount of %s is pending approval on gateway.', $formatedPrice);
            $state = Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW;
        } else {
            $message = Mage::helper('paypal')->__('Ordered amount of %s.', $formatedPrice);
        }

        $payment->addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_ORDER, null, false, $message);

        $this->_pro->importPaymentInfo($api, $payment);

        if ($payment->getIsTransactionPending()) {
            $message = Mage::helper('paypal')->__('Authorizing amount of %s is pending approval on gateway.', $formatedPrice);
            $state = Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW;
            if ($payment->getIsFraudDetected()) {
                $status = Mage_Sales_Model_Order::STATUS_FRAUD;
            }
        } else {
            $message = Mage::helper('paypal')->__('Authorized amount of %s.', $formatedPrice);
        }

        $payment->resetTransactionAdditionalInfo();

        $payment->setTransactionId($api->getTransactionId());
        $payment->setParentTransactionId($orderTransactionId);

        $transaction = $payment->addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH, null, false,
            $message
        );

        if($api->getPaymentStatus() == 'completed'){
            $state = Mage_Sales_Model_Order::STATE_PROCESSING;
        }

        $order->setState($state, $status);

        $payment->setSkipOrderProcessing(true);
        return $this;
    }
}