<?php
class OpenTechiz_SalesExtend_Model_Rewrite_Paypal_Nvp extends Milople_Partialpayment_Model_Api_Nvp
{
    public function call($methodName, array $request)
    {
        $request = $this->_addMethodToRequest($methodName, $request);
        $eachCallRequest = $this->_prepareEachCallRequest($methodName);
        if ($this->getUseCertAuthentication()) {
            if ($key = array_search('SIGNATURE', $eachCallRequest)) {
                unset($eachCallRequest[$key]);
            }
        }
        $request = $this->_exportToRequest($eachCallRequest, $request);
        $debugData = array('url' => $this->getApiEndpoint(), $methodName => $request);
        foreach ($request as $key => $value){
            if(strpos($key, 'L_') !== false){
                unset($request[$key]);
            }
        }
        try {
            $http = new Varien_Http_Adapter_Curl();
            $config = array(
                'timeout'    => 60,
                'verifypeer' => $this->_config->verifyPeer
            );

            if ($this->getUseProxy()) {
                $config['proxy'] = $this->getProxyHost(). ':' . $this->getProxyPort();
            }
            if ($this->getUseCertAuthentication()) {
                $config['ssl_cert'] = $this->getApiCertificate();
            }
            $http->setConfig($config);
            $http->write(
                Zend_Http_Client::POST,
                $this->getApiEndpoint(),
                '1.1',
                $this->_headers,
                $this->_buildQuery($request)
            );
            $response = $http->read();
        } catch (Exception $e) {
            $debugData['http_error'] = array('error' => $e->getMessage(), 'code' => $e->getCode());
            $this->_debug($debugData);
            throw $e;
        }


        $response = preg_split('/^\r?$/m', $response, 2);
        $response = trim($response[1]);
        $response = $this->_deformatNVP($response);
        $debugData['response'] = $response;
        $this->_debug($debugData);
        $response = $this->_postProcessResponse($response);

        // handle transport error
        if ($http->getErrno()) {
            Mage::logException(new Exception(
                sprintf('PayPal NVP CURL connection error #%s: %s', $http->getErrno(), $http->getError())
            ));
            $http->close();

            Mage::throwException(Mage::helper('paypal')->__('Unable to communicate with the PayPal gateway.'));
        }

        // cUrl resource must be closed after checking it for errors
        $http->close();

        if (!$this->_validateResponse($methodName, $response)) {
            Mage::logException(new Exception(
                Mage::helper('paypal')->__("PayPal response hasn't required fields.")
            ));
            Mage::throwException(Mage::helper('paypal')->__('There was an error processing your order. Please contact us or try again later.'));
        }

        $this->_callErrors = array();
        if ($this->_isCallSuccessful($response)) {
            if ($this->_rawResponseNeeded) {
                $this->setRawSuccessResponseData($response);
            }
            return $response;
        }
        $this->_handleCallErrors($response);
        return $response;
    }
    public function callSetExpressCheckout()//Pay downpayment with paypal exress
    {
        $calculationModel = Mage::getModel('partialpayment/calculation');
        foreach($calculationModel->getQuote()->getAllAddresses() as $address)
        {
            if($address->getAddressType() == $calculationModel->getShippingOrBilling())
            {
                if($address->getPaidAmount() > 0 && $address->getRemainingAmount() > 0)
                {
                    $storeId = Mage::app()->getStore()->getStoreId();
                    $allow_ba_signup = Mage::getStoreConfig('payment/paypal_express/allow_ba_signup', $storeId);
                    if ($allow_ba_signup == Mage_Paypal_Model_Config::EC_BA_SIGNUP_AUTO){
                        $this->setBillingType($this->getBillingAgreementType());
                    }
                    
                }
            }
        }
        
        $this->_prepareExpressCheckoutCallRequest($this->_setExpressCheckoutRequest);
        $request = $this->_exportToRequest($this->_setExpressCheckoutRequest);
        
        $this->_exportLineItems($request);
        //$calculationModel = Mage::getModel('partialpayment/calculation');
        foreach($calculationModel->getQuote()->getAllAddresses() as $address)
        {
            if($address->getAddressType() == $calculationModel->getShippingOrBilling())
            {
                if($address->getPaidAmount() > 0 && $address->getRemainingAmount() > 0)
                {
                    $partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
                    $request['AMT'] = $partialpaymentHelper->setNumberFormat($address->getPaidAmount());
                    $request['ITEMAMT'] = $partialpaymentHelper->setNumberFormat($address->getPaidAmount());
                    if($partialpaymentHelper->isShippingAndTaxOnDownPayment())
                    {
                        $request['AMT'] += $request['TAXAMT'];
                        $request['AMT'] += $request['SHIPPINGAMT'];
                    }
                    else
                    {
                        $request['TAXAMT'] = 0;
                        $request['SHIPPINGAMT'] = 0;
                    }
                }
            }
        }
        
        // import/suppress shipping address, if any
        $options = $this->getShippingOptions();
        if ($this->getAddress()) {
            $request = $this->_importAddresses($request);
            $request['ADDROVERRIDE'] = 1;
        } elseif ($options && (count($options) <= 10)) { // doesn't support more than 10 shipping options
            $request['CALLBACK'] = $this->getShippingOptionsCallbackUrl();
            $request['CALLBACKTIMEOUT'] = 6; // max value
            $request['MAXAMT'] = $request['AMT'] + 999.00; // it is impossible to calculate max amount
            $this->_exportShippingOptions($request);
        }

        // add recurring profiles information
        $i = 0;
        foreach ($this->_recurringPaymentProfiles as $profile) {
            $request["L_BILLINGTYPE{$i}"] = 'RecurringPayments';
            $request["L_BILLINGAGREEMENTDESCRIPTION{$i}"] = $profile->getScheduleDescription();
            $i++;
        }
        $response = $this->call(self::SET_EXPRESS_CHECKOUT, $request);
        $this->_importFromResponse($this->_setExpressCheckoutResponse, $response);
    }
}