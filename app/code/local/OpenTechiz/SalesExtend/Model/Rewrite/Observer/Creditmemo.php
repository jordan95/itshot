<?php

class OpenTechiz_SalesExtend_Model_Rewrite_Observer_Creditmemo extends Mirasvit_Credit_Model_Observer_Creditmemo
{
    public function onSaveAfter($observer)
    {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order = $creditmemo->getOrder();

        if ($creditmemo->getAutomaticallyCreated()) {
            if (Mage::helper('credit')->isAutoRefundEnabled()) {
                $creditmemo->setCreditRefundFlag(true)
                    ->setCreditTotalRefunded($creditmemo->getCreditAmount())
                    ->setBaseCreditTotalRefunded($creditmemo->getBaseCreditAmount());
            } else {
                return $this;
            }
        }

        if ($creditmemo->getCreditRefundFlag() && $creditmemo->getBaseCreditTotalRefunded()) {
            $creditReturnMax = floatval($creditmemo->getCreditReturnMax());

            if (round($creditmemo->getCreditTotalRefunded(), 2) > round($creditReturnMax, 2)) {
                Mage::throwException(Mage::helper('credit')->__('Store credit amount cannot exceed order amount.'));
            }

            $order->setBaseCreditTotalRefunded(
                $order->getBaseCreditTotalRefunded() + $creditmemo->getBaseCreditTotalRefunded()
            );
            $order->setCreditTotalRefunded(
                $order->getCreditTotalRefunded() + $creditmemo->getCreditTotalRefunded()
            );

            if ($order->getCustomerIsGuest()) {
                $customer = Mage::getModel("customer/customer");
                $customer->setWebsiteId($order->getStore()->getWebsiteId());
                $customer->loadByEmail($order->getCustomerEmail());
                if (!($customerId = $customer->getId())) {
                    Mage::throwException(Mage::helper('credit')->__('Store credit can not be given to guests.'));
                    return $this;
                }
            } else {
                $customerId = $order->getCustomerId();
            }
            if(Mage::registry('MirasvitCreditOnSaveAfter')) return $this;
            Mage::register('MirasvitCreditOnSaveAfter', true);
            
            $balance = Mage::getModel('credit/balance')->loadByCustomer($customerId);
            $balance->addTransaction(
                $creditmemo->getBaseCreditTotalRefunded(),
                Mirasvit_Credit_Model_Transaction::ACTION_REFUNDED,
                array('order' => $order, 'creditmemo' => $creditmemo)
            );
        }

        return $this;
    }
}
