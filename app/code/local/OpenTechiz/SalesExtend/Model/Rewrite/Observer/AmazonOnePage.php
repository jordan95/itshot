<?php

class OpenTechiz_SalesExtend_Model_Rewrite_Observer_AmazonOnePage extends Amazon_Payments_Model_Observer_Onepage
{
    /**
     * Event: custom_quote_process
     *
     * Clear address if user switches from Amazon Checkout to third-party checkout
     */
    public function clearShippingAddress(Varien_Event_Observer $observer)
    {
        $_helper = Mage::helper('amazon_payments/data');
        $session = $observer->getEvent()->getCheckoutSession();

        if(get_class(Mage::app()->getFrontController()->getAction()) == 'Signifyd_Connect_ConnectController'){
            return;
        }

        $action = Mage::app()->getFrontController()->getAction()->getFullActionName();
        $action_reset = array('opc_index_index');

        if (in_array($action, $action_reset) && $session && $session->getCheckoutState() == 'begin' && $session->getAmazonAddressId() && $session->getQuoteId() && $this->_quote === null) {

            $quote = $this->_quote = Mage::getModel('sales/quote')->setStoreId(Mage::app()->getStore()->getId())->load($session->getQuoteId());
            $address = $quote->getShippingAddress();

            if ($address->getId() == $session->getAmazonAddressId()) {

                $reset = array(
                    'firstname'   => '',
                    'lastname'    => '',
                    'street'      => '',
                    'city'        => '',
                    'region_id'   => '',
                    'postcode'    => '',
                    'country_id'  => '',
                    'telephone'   => '',
                );

                $address->setData($reset);
                $quote->setShippingAddress($address);
                $quote->setBillingAddress($address);

                $quote->collectTotals()->save();

                $session->unsAmazonAddressId();
            }

        }
    }
}