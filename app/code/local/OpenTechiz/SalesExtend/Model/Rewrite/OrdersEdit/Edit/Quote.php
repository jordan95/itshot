<?php

class OpenTechiz_SalesExtend_Model_Rewrite_OrdersEdit_Edit_Quote extends MageWorx_OrdersEdit_Model_Edit_Quote
{
    public function applyDataToQuote(Mage_Sales_Model_Quote $quote, array $data)
    {

        //prepare quote item from order item
        $this->_prepareQuoteItem($quote);

        foreach ($data as $key => $value) {
            if ($key == 'shipping_address') {
                $this->setAddress($quote, $value, 'shipping');
            } elseif ($key == 'billing_address') {
                $this->setAddress($quote, $value, 'billing');
            } elseif ($key == 'payment') {
                $this->setPayment($quote, $value);
            } elseif ($key == 'shipping') {
                $this->setShipping($quote, $value);
            } elseif ($key == 'quote_items') {
                $this->updateItems($quote, $value);
            } elseif ($key == 'product_to_add' && !empty($value)) {
                $this->addNewItems($quote, $value);
            } elseif ($key == 'coupon_code') {
                $this->setCouponCode($quote, $value);
            }elseif($key == "redemption_uses"){
                $quote->setPointsSpending($value);
            }
        }

        // Clear quote from canceled items
        $this->clearQuote($quote);

        // If multifees enabled
        $this->collectMultifees();

        $quote->setTotalsCollectedFlag(false)
                ->setSkipGiftCardsTotalCollected(true)
                ->setSkipRequestInstockStone(true)
                ->collectTotals();

        Mage::dispatchEvent('mwoe_apply_data_to_quote_collect_totals_after', array(
            'quote' => $quote,
            'new_data' => $data
        ));

        $this->saveTemporaryItems($quote, 1, true);

        if (isset($data['coupon_code'])) {
            $valid = $this->validateCouponCode($quote, $data['coupon_code']);
            $valid ? $quote->getShippingAddress()->setCouponCode($data['coupon_code']) : null;
        }

        return $quote;
    }

    public function addNewItems(Mage_Sales_Model_Quote $quote, $data)
    {
        foreach ($data as $productId => $params) {

            if(isset($params['product_id'])) {
                $product = Mage::getModel('catalog/product')->setStoreId($quote->getStoreId())->load($params['product_id']);
            } else {
                $product = Mage::getModel('catalog/product')->setStoreId($quote->getStoreId())->load($productId);
            }
            if (!$product || !$product->getId()) {
                continue;
            }

            if (!isset($params['product'])) {
                $params['product'] = $product->getId();
            }

            $productParams = new Varien_Object($params);
            if ($this->isItemWithParamsExist($quote, $product, $productParams)) {
                continue;
            }

            $quoteItem = $quote->addProduct($product, $productParams);
            $quoteItem->setData('ordersedit_is_temporary', 1)->save();
            if (!is_object($quoteItem)) {
                Mage::throwException($quoteItem);
            }
        }

        if (Mage::registry('ordersedit_order')) {
            Mage::helper('mageworx_ordersedit/edit')->addPendingChanges(Mage::registry('ordersedit_order')->getId(), array(
                    'product_to_add' => array()
                )
            );
        }

        return $this;
    }
    
    protected function _prepareQuoteItem(Mage_Sales_Model_Quote $quote){
        $quoteItems = $quote->getAllItems();

        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach ($quoteItems as $quoteItem) {
            $orderItem = $this->getOrderItemByQuoteItem($quoteItem);
            if($orderItem && $orderItem->getId()){
                $buyRequest = $orderItem->getBuyRequest();
                if(!$buyRequest){
                    continue;
                }
                $productId = $orderItem->getProduct()->getId();
                $product = Mage::getModel('catalog/product')
                    ->setStoreId($quote->getStoreId())
                    ->load($productId);

                $productCandidates = $product->getTypeInstance(true)
                    ->prepareForCartAdvanced($buyRequest, $product);
                if (is_array($productCandidates) || is_object($productCandidates))
                {
                    foreach ($productCandidates as $productCandidate) {
                        $quoteItem->setOptions($productCandidate->getCustomOptions())
                            ->setProduct($productCandidate);
                    }
                }

                if ($additionalOptions = $orderItem->getProductOptionByCode('additional_options')) {
                    $quoteItem->addOption(new Varien_Object(
                        array(
                            'product' => $quoteItem->getProduct(),
                            'code' => 'additional_options',
                            'value' => serialize($additionalOptions)
                        )
                    ));
                }
            }else{
                if(!$quoteItem->getData('ordersedit_is_temporary')){
                    $quote->removeItem($quoteItem->getId());
                }
            }
        }
        return $quote;
    }
    
    public function setPayment(Mage_Sales_Model_Quote $quote, $data)
    {
        /* @var $payment Mage_Sales_Model_Quote_Payment */
        $payment = $quote->getPayment();
        /* @var $method Mage_Paygate_Model_Authorizenet */
        $method = $payment->getMethodInstance();
        if($method instanceof Mage_Paygate_Model_Authorizenet &&  $method->getCardsStorage()->getCardsCount() > 0) {
            $cards = $method->getCardsStorage()->flushCards();
        }
        if (isset($data['cc_number']) && !isset($data['cc_number_enc'])) {
            $data['cc_number_enc'] = $payment->encrypt($data['cc_number']);
            unset($data['cc_number']);
            $payment->setCcNumberEnc($data['cc_number_enc']);
        }
        $payment->addData($data);

        return $this;
    }
}
