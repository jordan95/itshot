<?php

if(Mage::helper('core')->isModuleEnabled('Medialounge_Core')){
    class OpenTechiz_SalesExtend_Model_Currency_Store_Pure extends Medialounge_Core_Model_Core_Store{

    }
}else {
    class OpenTechiz_SalesExtend_Model_Currency_Store_Pure extends Mage_Core_Model_Store{

    }
}

class OpenTechiz_SalesExtend_Model_Currency_Store extends OpenTechiz_SalesExtend_Model_Currency_Store_Pure
{
    private $_currency = false;
    /**
     * Convert price from default currency to current currency
     *
     * @param   double $price
     * @param   boolean $format             Format price to currency format
     * @param   boolean $includeContainer   Enclose into <span class="price"><span>
     * @return  double
     */
    public function convertPrice($price, $format = false, $includeContainer = true)
    {
        if ($this->getCurrentCurrency() && $this->getBaseCurrency()) {
            if($this->isDisplayBaseCurrency()){
                $value = $this->getBaseCurrency()->convert($price, $this->getBaseCurrentCurrency());
            }else {
                $value = $this->getBaseCurrency()->convert($price, $this->getCurrentCurrency());
            }
        } else {
            $value = $price;
        }

        if ($this->getCurrentCurrency() && $format) {
            $value = $this->formatPrice($value, $includeContainer);
        }
        return $value;
    }

    /**zg
     * Format price with currency filter (taking rate into consideration)
     *
     * @param   double $price
     * @param   bool $includeContainer
     * @return  string
     */
    public function formatPrice($price, $includeContainer = true)
    {
        if ($this->getBaseCurrency()) {
            if($this->isDisplayBaseCurrency()) {
                return $this->getBaseCurrency()->format($price, array(), $includeContainer);
            }
        }
        if ($this->getCurrentCurrency()) {
            return $this->getCurrentCurrency()->format($price, array(), $includeContainer);
        }
        return $price;
    }

    //route and controller to use base currency
    public function isDisplayBaseCurrency()
    {
        return Mage::app()->getRequest()->getControllerName() == 'cart'
            || Mage::app()->getRequest()->getControllerName() == 'checkout'
            || Mage::app()->getRequest()->getRouteName() == 'onestepcheckout'
            || Mage::app()->getRequest()->getRouteName() == 'checkout'
            || Mage::app()->getRequest()->getRouteName() == 'paypal'
            || Mage::app()->getRequest()->getRouteName() == 'rewards';
    }

    public function getCurrentCurrencyCode()
    {
        //use USD entirely in admin
        if(Mage::app()->getRequest()->getRouteName() == 'adminhtml'){
            return $this->getDefaultCurrencyCode();
        }

        if ($this->_currency) {//because in the case of varnish, we skip currency from session so we need this to get curreny within request scope
            return $this->_currency;
        }

        // try to get currently set code among allowed
        if( Mage::getStoreConfig('mgt_varnish/mgt_varnish/enabled') ) {
            $code = Mage::app()->getCookie()->get('currency');
        } else {
            $code = $this->_getSession()->getCurrencyCode();
        }

        if (empty($code)) {
            $code = $this->getDefaultCurrencyCode();
        }
        if (in_array($code, $this->getAvailableCurrencyCodes(true))) {
            return $code;
        }

        // take first one of allowed codes
        $codes = array_values($this->getAvailableCurrencyCodes(true));
        if (empty($codes)) {
            // return default code, if no codes specified at all
            return $this->getDefaultCurrencyCode();
        }
        return array_shift($codes);
    }
}