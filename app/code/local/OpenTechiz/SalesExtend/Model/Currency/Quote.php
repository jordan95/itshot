<?php

if (Mage::helper('core')->isModuleEnabled('Idev_OneStepCheckout')) {
    class OpenTechiz_SalesExtend_Model_Currency_Quote_Pure extends Idev_OneStepCheckout_Model_Sales_Quote{

    }
} else {
    class OpenTechiz_SalesExtend_Model_Currency_Quote_Pure extends Mage_Sales_Model_Quote{

    }
}

class OpenTechiz_SalesExtend_Model_Currency_Quote extends OpenTechiz_SalesExtend_Model_Currency_Quote_Pure
{
    protected $_customer;

    /**
     * Prepare data before save
     *
     * @return $this
     */
    protected function _beforeSave()
    {
        /**
         * Currency logic
         *
         * global - currency which is set for default in backend
         * base - currency which is set for current website. all attributes that
         *      have 'base_' prefix saved in this currency
         * store - all the time it was currency of website and all attributes
         *      with 'base_' were saved in this currency. From now on it is
         *      deprecated and will be duplication of base currency code.
         * quote/order - currency which was selected by customer or configured by
         *      admin for current store. currency in which customer sees
         *      price thought all checkout.
         *
         * Rates:
         *      store_to_base & store_to_quote/store_to_order - are deprecated
         *      base_to_global & base_to_quote/base_to_order - must be used instead
         */

        $globalCurrencyCode  = Mage::app()->getBaseCurrencyCode();
        $baseCurrency = $this->getStore()->getBaseCurrency();

        if ($this->hasForcedCurrency()){
            $quoteCurrency = $this->getForcedCurrency();
        } else {
            $quoteCurrency = $this->getStore()->getBaseCurrency();
        }

        $this->setGlobalCurrencyCode($globalCurrencyCode);
        $this->setBaseCurrencyCode($baseCurrency->getCode());
        $this->setStoreCurrencyCode($baseCurrency->getCode());
        $this->setQuoteCurrencyCode($quoteCurrency->getCode());

        //deprecated, read above
        $this->setStoreToBaseRate($baseCurrency->getRate($globalCurrencyCode));
        $this->setStoreToQuoteRate($baseCurrency->getRate($quoteCurrency));

        $this->setBaseToGlobalRate($baseCurrency->getRate($globalCurrencyCode));
        $this->setBaseToQuoteRate($baseCurrency->getRate($quoteCurrency));

        if (!$this->hasChangedFlag() || $this->getChangedFlag() == true) {
            $this->setIsChanged(1);
        } else {
            $this->setIsChanged(0);
        }

        $this->setstore_to_quote_rate(1)->setquote_currency_code('USD')
            ->setbase_to_quote_rate(1)->setbase_currency_code('USD')
            ->setGrandTotal($this->getbase_grand_total())
            ->setsubtotal($this->getbase_subtotal())
            ->setsubtotal_with_discount($this->getsubtotal_with_discount());

        $billingAddress = $this->getBillingAddress();
        $shippingAddress = $this->getShippingAddress();
        if($billingAddress && $billingAddress->getId()){
            $billingAddress->setsubtotal($billingAddress->getbase_subtotal())
                ->setshipping_amount($billingAddress->getbase_shipping_amount())
                ->setdiscount_amount($billingAddress->getbase_discount_amount())
                ->setgrand_total($billingAddress->getbase_grand_total())
                ->setsubtotal_incl_tax($billingAddress->getbase_subtotal_incl_tax())
                ->setshipping_incl_tax($billingAddress->getbase_shipping_incl_tax())
            ;
        }
        if($shippingAddress && $shippingAddress->getId()){
            $shippingAddress->setsubtotal($shippingAddress->getbase_subtotal())
                ->setshipping_amount($shippingAddress->getbase_shipping_amount())
                ->setdiscount_amount($shippingAddress->getbase_discount_amount())
                ->setgrand_total($shippingAddress->getbase_grand_total())
                ->setsubtotal_incl_tax($shippingAddress->getbase_subtotal_incl_tax())
                ->setshipping_incl_tax($shippingAddress->getbase_shipping_incl_tax())
            ;
        }

        if ($this->_customer) {
            $this->setCustomerId($this->_customer->getId());
        }
        foreach ($this->getAllItems() as $item){
            $item->setrow_total_with_discount(number_format($item->getbase_row_total() - $item->getbase_discount_amount(), 4))
                ->setrow_total($item->getBaseRowTotal())->setprice_incl_tax($item->getbase_price_incl_tax());
        }

        Mage_Core_Model_Abstract::_beforeSave();
    }
}