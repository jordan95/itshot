<?php

class OpenTechiz_SalesExtend_Model_ShipFrom extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_salesextend/shipFrom');
    }
}