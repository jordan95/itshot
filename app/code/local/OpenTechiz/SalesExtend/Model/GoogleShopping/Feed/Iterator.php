<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Advanced Product Feeds
 * @version   1.1.18
 * @build     769
 * @copyright Copyright (C) 2017 Mirasvit (http://mirasvit.com/)
 */



/**
 * @method int getSize() - returns size of currently iterated collection
 */
class OpenTechiz_SalesExtend_Model_GoogleShopping_Feed_Iterator extends Mirasvit_FeedExport_Model_Feed_Generator_Action_Iterator
{
    public function process()
    {
        // To solve conflict with 'Aitoc_Aitquantitymanager'
        Mage::register('aitoc_order_create_store_id', $this->getFeed()->getStoreId(), true);

        $iteratorModel = $this->getIteratorModel();
        if ($iteratorModel->init() === false) {
            $this->finish();

            return;
        }

        $collection = $iteratorModel->getCollection();
        // Set size of current collection
        $this->setData('size', $collection->getConnection()->fetchOne($collection->getSelectCountSql()));
        $idx = intval($this->getValue('idx'));
        $add = intval($this->getValue('add'));
        $limit = $this->getLimit($this->getSize());

        if ($idx == 0) {
            $this->start();
            $iteratorModel->start();
        }

        //get personalized product ids
        $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
        $attributeSetModel->load('Personalized Product', 'attribute_set_name');
        $personalized_attribute_set = $attributeSetModel->getId();
        $productCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter($personalized_attribute_set, 'attribute_set_id');
        $personalizedProductIds = [];
        foreach ($productCollection as $personalizedProduct){
            array_push($personalizedProductIds, $personalizedProduct->getId());
        }

        $result = array();
        $rows = $this->fetchCollection($iteratorModel->getCollection(), $limit, $idx);
        Varien_Profiler::start('feedexport:iterator_'.$this->getType().':process');
        for ($counter = 1, $rowSize = count($rows); $counter <= $rowSize; ++$counter) {
            $isTimeout = Mage::helper('feedexport')->getState()->isTimeout();
//            if(!preg_match('/^[09]/', $rows[$counter - 1]['sku'])) {
//                continue;
//            }
            if(in_array($rows[$counter - 1]['entity_id'], $personalizedProductIds)) {
                $product = Mage::getModel('catalog/product')->load($rows[$counter - 1]['entity_id']);

                $options = [];
                $options[0] = [];
                $options[1] = [];
                $options[2] = [];
                $options[3] = [];
                $options[4] = [];

                foreach($product->getOptions() as $option){
                    if($option->getTitle() == 'Center Stone'){
                        foreach ($option->getValues() as $value){
                            array_push($options[0], $value->getSku());
                        }
                    }
                    if($option->getTitle() == 'Stone 2'){
                        foreach ($option->getValues() as $value){
                            array_push($options[1], $value->getSku());
                        }
                    }
                    if($option->getTitle() == 'Stone 3'){
                        foreach ($option->getValues() as $value){
                            array_push($options[2], $value->getSku());
                        }
                    }
                    if($option->getTitle() == 'Stone 4'){
                        foreach ($option->getValues() as $value){
                            array_push($options[3], $value->getSku());
                        }
                    }
                    if($option->getTitle() == 'Metal'){
                        foreach ($option->getValues() as $value){
                            array_push($options[4], $value->getSku());
                        }
                    }
                }

                foreach ($options as $key => $value){
                    if($value == []) unset($options[$key]);
                }

                $newOption = [];
                foreach ($options as $key => $value){
                    array_push($newOption, $value);
                }

                $combinations = $this->combinations($newOption);
//                var_dump($combinations);exit;
                $limit = 0;
                foreach ($combinations as $combination){
                    $sku = $product->getSku();
                    foreach ($combination as $option){
                        $sku .=OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.$option;
                    }
                    $sku.=OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.'1';
                    $rows[$counter - 1]['sku'] = $sku;
                    $rows[$counter - 1]['isPersonalized'] = true;

                    $skuParts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
                    $metal = $skuParts[count($skuParts)-2];
                    $stones = [];
                    for($i=1; $i<count($skuParts)-2; $i++){
                        array_push($stones, $skuParts[$i]);
                    }
                    $personalizedPrice = 0;
                    foreach ($product->getOptions() as $option){
                        if($option->getType() == 'stone'){
                            $stonePrice = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStonePrice($option->getId(), $product->getId());
                            $stoneQualityPrice = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStoneWithQualityPrice($option->getId(), $product->getId());
                            foreach ($stoneQualityPrice as $key => $value){
                                $stonePrice['diamond'.$key] = $stoneQualityPrice[$key];
                            }
                            if($stonePrice['diamond'] !== null) unset($stonePrice['diamond']);
                            if($stonePrice['quality'] !== null) unset($stonePrice['quality']);
                            $personalizedPrice += $stonePrice[$stones[$option->getSortOrder()]];
                        }
                        if($option->getType() == 'gold'){
                            $goldPrice = Mage::helper('personalizedproduct/personalizedPriceHelper')->getAlloyPrice($product->getId());
                            $personalizedPrice += $goldPrice[$metal];
                        }
                    }
                    $personalizedPrice += $product->getAdditionalFee();

                    $rows[$counter - 1]['personalizedPrice'] = $personalizedPrice;

                    Varien_Profiler::start('feedexport:iterator:callback');
                    $callbackResult = $iteratorModel->callback($rows[$counter - 1]);
                    Varien_Profiler::stop('feedexport:iterator:callback');

                    if ($callbackResult !== null) {
                        $result[] = $callbackResult;
                        ++$add;
                    }

                    if ($counter >= $limit || $counter >= $rowSize || $isTimeout) {
                        $this->setValue('idx', $idx + $counter)
                            ->setValue('size', $this->getSize())
                            ->setValue('add', $add);
                    }

                    if ($isTimeout) {
                        break;
                    }
                    $limit++;
                    if($limit == 100){
                        break;
                    }
                }
            }else {
                Varien_Profiler::start('feedexport:iterator:callback');
                $callbackResult = $iteratorModel->callback($rows[$counter - 1]);
                Varien_Profiler::stop('feedexport:iterator:callback');
                
                if ($callbackResult !== null) {
                    $result[] = $callbackResult;
                    ++$add;
                }

                if ($counter >= $limit || $counter >= $rowSize || $isTimeout) {
                    $this->setValue('idx', $idx + $counter)
                        ->setValue('size', $this->getSize())
                        ->setValue('add', $add);
                }

                if ($isTimeout) {
                    break;
                }
            }
        }

        $iteratorModel->save($result);
        Varien_Profiler::stop('feedexport:iterator_'.$this->getType().':process');

        if ($idx >= $this->getSize()) {
            $iteratorModel->finish();
            $this->finish();
            $this->setIteratorType($this->getKey());
        }
    }

    public function combinations($arrays, $i = 0) {
        if (!isset($arrays[$i])) {
            return array();
        }
        if ($i == count($arrays) - 1) {
            return $arrays[$i];
        }

        // get combinations from subsequent arrays
        $tmp = $this->combinations($arrays, $i + 1);

        $result = array();

        // concat each array from tmp with each element from $arrays[$i]
        foreach ($arrays[$i] as $v) {
            foreach ($tmp as $t) {
                $result[] = is_array($t) ?
                    array_merge(array($v), $t) :
                    array($v, $t);
            }
        }

        return $result;
    }

    /**
     * Get limit number for size of products per iteration.
     *
     * @param int $size
     *
     * @return int
     */
    private function getLimit($size)
    {
        $limit = Mage::getSingleton('feedexport/config')->getPageSize();
        if (!$limit) {
            $limit = intval($size / 100);
            if ($limit < 100) {
                $limit = 100;
            }
        }

        return $limit;
    }
}
