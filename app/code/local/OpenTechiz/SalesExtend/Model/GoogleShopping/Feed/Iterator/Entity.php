<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Advanced Product Feeds
 * @version   1.1.18
 * @build     769
 * @copyright Copyright (C) 2017 Mirasvit (http://mirasvit.com/)
 */



class OpenTechiz_SalesExtend_Model_GoogleShopping_Feed_Iterator_Entity extends Mirasvit_FeedExport_Model_Feed_Generator_Action_Iterator_Entity
{

    public function callback($row)
    {
        $this->_patternModel = Mage::getSingleton('feedexport/feed_generator_pattern');
        $this->_patternModel->setFeed($this->getFeed());

        if ($this->_type == 'review') {
            $model = Mage::getModel('review/review')->load($row['review_id']);
        } else {
            $model = Mage::getModel('catalog/'.$this->_type)->load($row['entity_id']);
            $model->setStoreId($this->getFeed()->getStoreId());
            $model->setSku($row['sku']);
            if(array_key_exists('isPersonalized', $row)){
                if($row['isPersonalized']) $model->setPrice($row['personalizedPrice']);
            }
        }
        $result = $this->_patternModel->getPatternValue($this->_format['entity'][$this->_type], $this->_type, $model);
        return $result;
    }
}
