<?php
class OpenTechiz_SalesExtend_Model_Observer_EditOrderNotify
{
    protected $_comment = 'Some of the item(s) on the order may not have been delivered. 
    If you need to split the order you can do it at this time. 
    If not, please update the state back to Waiting on Vendor. ';

    public function addCommentAfterEditOrderFromWaitingForVendor($observer){
        $origOrder = $observer->getEvent()->getOrigOrder();
        $order = $observer->getEvent()->getOrder();
        $changes = $observer->getEvent()->getChanges();

        $itemRemoved = [];
        if (isset($changes['quote_items'])){
            foreach ($changes['quote_items'] as $key => $value){
                if(isset($value['action']) && $value['action'] == 'remove'){
                    $orderItem = Mage::getModel('sales/order_item')->load($key, 'quote_item_id');
                    array_push($itemRemoved, $orderItem->getId());
                }

            }
        }

        if($origOrder->getInternalStatus() == 4) { //waiting for vendor
            //check if new item is in any PO and not yet delivered, if yes add comment
            $items = $order->getAllItems();
            foreach ($items as $item){
                //skip checking removed item
                if(in_array($item->getId(), $itemRemoved)){
                    continue;
                }

                $sku = $item->getSku();
                $purchasingItems = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()
                    ->addFieldToFilter('sku', $sku)
                    ->addFieldToFilter('qty', array('gt' => new Zend_Db_Expr('delivered_qty')));
                if(count($purchasingItems)){
                    $order->addStatusHistoryComment($this->_comment);
                    $order->save();
                }
            }
        }
    }
}