<?php
class OpenTechiz_SalesExtend_Model_Observer_OrderState
{
    function checkCanceledAmountBeforeChangeOrderState($observer)
    {
        $origData = $observer->getEvent()->getDataObject()->getOrigData();
        $newData = $observer->getEvent()->getDataObject()->getData();

        //prevent reverse canceled order if total canceled = total
        if(isset($newData['base_total_canceled']) && isset($newData['base_grand_total'])) {
            $totalCanceled = round((float)$newData['base_total_canceled'], 2);
            $total = round((float)$newData['base_grand_total'], 2);

            if ($origData['state'] == 'canceled' && $newData['state'] != 'canceled' && $totalCanceled == $total) {
                Mage::getSingleton('adminhtml/session')->addError("Order is canceled, cannot be reversed when total canceled = grand total.");
                Mage::app()->getResponse()
                    ->setRedirect(Mage::getUrl('adminhtml/sales_order/view', array('order_id' => $origData['entity_id'])))
                    ->sendResponse();
                exit;
            }
        }
    }
}