<?php
class  OpenTechiz_SalesExtend_Model_Observer_RepairOrderProcess
{
    public function orderCancelAfter($observer){
        $order = $observer->getEvent()->getOrder();
        if($order->getOrderType() == OpenTechiz_SalesExtend_Helper_Data::ORDER_TYPE_REPAIR_CODE) {
            $incrementId = $order->getIncrementId();

            $return = Mage::getModel('opentechiz_return/return')->load($incrementId, 'return_fee_order_id');
            //close return
            if ($return && $return->getId()) {
                //add comment to description
                $string = $return->getDescription();
                $string .= ' 
(Repair order Cancelled)';

                $return->setStatus(OpenTechiz_Return_Helper_Data::DENIED)
                    ->setDescription($string)
                    ->save();
                //return item to original order
                $barcode = $return->getItemBarcode();
                $originalOrderItem = $return->getOrderItemId();

                $item = Mage::getModel('opentechiz_inventory/item')->load($barcode, 'barcode');
                $item->setState(15)->save();

                $link = Mage::getModel('opentechiz_inventory/order')->load($item->getId(), 'item_id');
                $linkedOrderItem = $link->getOrderItemId();
                if ($linkedOrderItem && $linkedOrderItem != $originalOrderItem) { //if is not linked to original, link it to original
                    $link->setOrderItemId($originalOrderItem)->save();
                }

                //remove repair process
                $request = Mage::getModel('opentechiz_production/product')->load($barcode, 'barcode');
                if($request && $request->getId()){
                    $request->delete();
                }
            } else {
                Mage::log('return for order' . $incrementId . ' not found', null, 'return_process.log');
            }
        }
    }
}