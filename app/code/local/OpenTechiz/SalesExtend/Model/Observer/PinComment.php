<?php
class OpenTechiz_SalesExtend_Model_Observer_PinComment
{
    public function pinCommentAfterChangeState($observer){
        $order = $observer->getEvent()->getOrder();
        $order_stage = $order->getOrderStage();
        $order_status = $order->getStatus();
        $order_id = $order->getId();
        $request = Mage::app()->getRequest()->getParams();

        if(Mage::registry('register_before_order_stage')=="0"){
            if(isset($request['order_stage']) && $request['order_stage'] ==1){
                try {
                    $all_item_instock = Mage::helper('opentechiz_salesextend')->IsAllInstock($order->getAllItems());
                    if($all_item_instock == 1 && $order_stage == 1){
                        $comment = Mage::getModel('sales/order_status_history')->getCollection()->addFieldToFilter("parent_id",$order_id)->addFieldToFilter("comment",array("eq"=>"All_In_Stock"))->addFieldToFilter("has_pin_comment",array("eq"=>1));
                        if($comment->count() <= 0){
                            $history_model = Mage::getModel('sales/order_status_history');
                            $history_model->setParentId($order_id)->setIsCustomerNotified(0)->setIsVisibleOnFront(0)->setComment("All_In_Stock")->setStatus($order_status)->setEntityName("order")->setHasPinComment(1)->save();

                            $comment_collection = Mage::getModel('sales/order_status_history')->getCollection()->addFieldToFilter("parent_id",$order_id);
                            if($comment_collection->count() > 0){
                                foreach($comment_collection as $comment){
                                    if($comment->getHasPinComment()){
                                        $comment->setHasPinComment(0)->save();
                                    }
                                    if($comment->getId() == $history_model->getId()){
                                        $comment->setHasPinComment(1)->save();
                                    }
                                    
                                }
                            }
                        }
                    }
                      
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
        }
        
        	
        	
    }
}