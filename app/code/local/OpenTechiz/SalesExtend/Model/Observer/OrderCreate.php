<?php
class OpenTechiz_SalesExtend_Model_Observer_OrderCreate
{
    const ORDER_SESSION_DATA_KEYS = ['increment', 'source', 'cc_cid'];

    public function clearOrderDataInSession($observer){
        $session = Mage::getSingleton('core/session');
        foreach (self::ORDER_SESSION_DATA_KEYS as $key){
            $session->setData($key, '');
        }
    }
}