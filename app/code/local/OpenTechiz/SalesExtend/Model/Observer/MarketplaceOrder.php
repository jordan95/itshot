<?php
class OpenTechiz_SalesExtend_Model_Observer_MarketplaceOrder{
    public function addInvoiceAndPaymentRecord($observer){
        if(Mage::registry('addInvoiceAndPaymentRecordForMarketplaceOrder')) return;
        Mage::register('addInvoiceAndPaymentRecordForMarketplaceOrder', true);
        $order = $observer->getEvent()->getOrder();
        $existInvoice = Mage::getModel('sales/order_invoice')->load($order->getId(), 'order_id');
        if($order->getSource()
            && in_array($order->getSource(), OpenTechiz_SalesExtend_Helper_Data::MARKETPLACE_ORDER_SOURCE)
            && (!$existInvoice || !$existInvoice->getId())) {
            //auto create invoice
            $itemsArray = [];  //item to create invoice
            foreach($order->getAllItems() as $item) {
                $item_id = $item->getItemId();
                $qty = $item->getQtyOrdered() - $item->getQtyInvoiced() - $item->getQtyCanceled() - $item->getQtyRefunded();
                $itemsArray[$item_id] = $qty;
            }
            //create invoice
            if($order->canInvoice()) {
                $invoice_id = Mage::getModel('sales/order_invoice_api')
                    ->create($order->getIncrementId(), $itemsArray ,'invoice_marketplace_order' ,1,1);
                $invoice = Mage::getModel('sales/order_invoice')->load($invoice_id, 'increment_id');
                //set qty invoiced
                $_items = $order->getAllItems();
                foreach ($_items as $_item){
                    $_item->setQtyInvoiced($itemsArray[$_item->getId()])->save();
                }

                //create payment record
                $paymentMethod = $order->getPayment()->getMethod();
                $paymentMethodId = Mage::helper('opentechiz_salesextend/rewrite_adminhtml_paymentRecord')->getPaymentMethodId($paymentMethod);

                $data = [];
                $data['invoice_id'] = $invoice->getId();
                $data['increment_id'] = $invoice->getIncrementId();
                $data['payment_id'] = $paymentMethodId;
                $data['total'] = $order->getBaseGrandTotal();
                $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                $data['order_id'] = $order->getId();
                $data['type'] = 'invoice';
                $data['number'] = 'Auto create for marketplace order'; //note
                $data['received_date'] = Mage::getModel('core/date')->gmtDate('Y-m-d');
                $data['email'] = $order->getCustomerEmail();
                $data['billing_name'] = $order->getCustomerFirstname().' '.$order->getCustomerLastname();

                Mage::getModel('cod/paymentgrid')->setData($data)->save();
            }
        }
    }

    public function checkOrderSourceAndMoveToPrepShip($observer){
        if(Mage::registry('checkOrderSourceAndMoveToPrepShipForMarketplace')) return;
        Mage::register('checkOrderSourceAndMoveToPrepShipForMarketplace', true);
        $order = $observer->getEvent()->getOrder();
        
        if($order->getOrderStage() == 3 && in_array($order->getSource(), OpenTechiz_SalesExtend_Helper_Data::MARKETPLACE_ORDER_SOURCE)){
            $totalPaymentRecord = Mage::helper('opentechiz_salesextend')->getTotalPaymentRecordForOrder($order);
            $orderTotal = round($order->getBaseGrandTotal(), 4);
            $totalPaymentRecord = round($totalPaymentRecord, 4);
            if($totalPaymentRecord >= $orderTotal){
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 4, 7, true, false);
            }
        }
    }
}