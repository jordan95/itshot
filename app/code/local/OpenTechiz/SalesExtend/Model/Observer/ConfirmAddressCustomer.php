<?php

class OpenTechiz_SalesExtend_Model_Observer_ConfirmAddressCustomer
{
	public function UpdateConfirmAddressCustomer($observer){

        $order = $observer->getEvent()->getOrder();
        $params = Mage::app()->getRequest()->getParams();
        if(isset($params['confirm_customer'])) {
            $order->setConfirmAddressCustomer($params['confirm_customer']);
        }else{
            $order->setConfirmAddressCustomer(0);
        }


	}
}