<?php
class OpenTechiz_SalesExtend_Model_Observer_PaymentRecord
{
    public function getDataForPaymentAfterSave($observer){
        $payment = $observer->getEvent()->getPaymentgrid();
        Mage::unregister('opentechiz_payment_save_before');
        Mage::register('opentechiz_payment_save_before', $payment->getOrigData());

        //get order total paid before save
        if($payment->getInvoiceId()) {
            $paymentMethodCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                ->addFieldToFilter('invoice_id', $payment->getInvoiceId());
            $total = 0;
            foreach ($paymentMethodCollection as $record) {
                $total += $record->getTotal();
            }
            $totalPaidBeforeSave = round($total, 4);

            Mage::unregister('opentechiz_order_total_paid_before');
            Mage::register('opentechiz_order_total_paid_before', $totalPaidBeforeSave);
        }
    }

    public function paymentRecordSaveChangeOrderStatus($observer){
        $payment = $observer->getEvent()->getPaymentgrid();
        //save order id if no order id
        if(!$payment->getOrderId()){
            if($payment->getType() == 'invoice'){
                $invoiceId = $payment->getInvoiceId();
                $orderId = Mage::getModel('sales/order_invoice')->load($invoiceId)->getOrderId();
            }else{
                $creditMemoId = $payment->getCreditMemoId();
                $orderId = Mage::getModel('sales/order_creditmemo')->load($creditMemoId)->getOrderId();
            }
            $payment->setOrderId($orderId)->save();
        }
        //get order
        $order = Mage::getModel('sales/order')->load($payment->getOrderId());
        if(!$order || !$order->getId()){
            return;
        }
        $totalPaid = Mage::helper('opentechiz_salesextend')->getTotalPaymentRecordForOrder($order);
        $totalPaid = round($totalPaid, 4);
        $orderTotal = round($order->getBaseGrandTotal(), 4);
        //change order stage for layaway

        if($totalPaid && $orderTotal && ($order->getOrderType() == 1)) {  //for layaway
            $orderStage = $order->getOrderStage();
            $changeStage = false;
            if ($totalPaid >= $orderTotal / 2) {
                if ($orderStage < 2 && !Mage::helper('opentechiz_salesextend')->orderHasStatus(15, $order)) { //if order not yet move to fulfilment
                    Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 1, 15);
                } else { //else only add to status list
                    Mage::helper('opentechiz_salesextend')->addStatusToOrder(15, $order);
                }
                $changeStage = true;
            }

            if ($totalPaid >= $orderTotal) {
                //change to item ready if order is paid at billing
                if ($order->getOrderStage() == 3 && $order->getInternalStatus() == 26) {
                    Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 3, 6);
                }
                //if order have status items ready and invoice paid, add ship prep to status list
                Mage::helper('opentechiz_salesextend')->addStatusToOrder(7, $order);
                $changeStage = true;
            } elseif ($order->getOrderStage() == 4) {
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 3, 6);
            } elseif ($order->getOrderStage() == 3 && $order->getInternalStatus() != 26) {
                //change to item ready if order changed to not fully paid at billing
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 3, 26);
            }

            if (!$changeStage) {
                if ($orderStage < 2) {
                    Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 1, 5);
                } else {
                    Mage::helper('opentechiz_salesextend')->addStatusToOrder(5, $order);
                }
            }
        }elseif($order->getOrderType() == 1 && $totalPaid == 0){ //in case paypal pending, no payment record
            Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 1, 0);
        }elseif($totalPaid && $orderTotal){ //for other order
            if($totalPaid >= $orderTotal){
                //if order have status items ready and invoice paid, add ship prep to status list
                Mage::helper('opentechiz_salesextend')->addStatusToOrder(7, $order);

            }elseif($order->getOrderStage() == 4){
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 3, 6);
            }
        }

        //add comment to order
        if(!Mage::registry('prevent_create_order_comment')) {
            if (isset($payment->getData()['is_new']) && $payment->getData()['is_new']) {
                if ($payment->getTotal() > 0) {
                    $message = 'A payment of $'.number_format(abs($payment->getTotal()), 2, '.', '')
                        . ' was applied to order #' . $order->getIncrementId();
                } else {
                    $message = 'A refund of $' . number_format(abs($payment->getTotal()), 2, '.', '')
                        . ' was applied to order #' . $order->getIncrementId();
                }
                $order->addStatusHistoryComment($message);
                $order->save();
            } else {
                $beforeSaveData = Mage::registry('opentechiz_payment_save_before');
                $totalBefore = $beforeSaveData['total'];
                $adjustment = $payment->getTotal() - $totalBefore;

                //don't add comment on delete (module save before delete)
                $postData = Mage::app()->getRequest()->getPost();
                if (isset($postData['hd_num_row_update'])) {
                    for ($i = 1; $i <= $postData['hd_num_row_update']; $i++) {
                        if (!isset($postData['hd_status_' . $i]) || !isset($postData['hd_total_' . $i])
                            || !isset($postData['hd_paymentid_' . $i])) continue; //continue in case warning

                        if ($postData['hd_status_' . $i] == 'true' || $postData['hd_total_' . $i] == 0
                            && $postData['hd_paymentid_' . $i] == $payment->getId()) {
                            return;
                        }
                    }
                }

                if ($adjustment == 0) return; //don't comment if no adjustment
                if ($payment->getTotal() > 0) {
                    $string = '$' . number_format($adjustment, 2, '.', '');
                    if($adjustment < 0){
                        $string = '-$' . number_format(abs($adjustment), 2, '.', '');
                    }
                    $message = 'A adjustment of '.$string
                        . ' was applied to invoice of order #' . $order->getIncrementId();
                } else {
                    $adjustment = abs($adjustment);
                    $string = '$' . number_format(abs($adjustment), 2, '.', '');
                    if($adjustment > 0){
                        $string = '-$' . number_format(abs($adjustment), 2, '.', '');
                    }
                    $message = 'An adjustment of '.$string
                        . ' was applied to creditmemo of order #' . $order->getIncrementId();
                }
                $order->addStatusHistoryComment($message);
                $order->save();
            }
        }
    }

    public function changeOrderToVerificationWhenPaymentRecordBackToZero($observer){
        $payment = $observer->getEvent()->getPaymentgrid();

        //get order
        $order = Mage::getModel('sales/order')->load($payment->getOrderId());

        if(!$order || !$order->getId()){
            return;
        }

        $paymentMethodCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->addFieldToFilter('invoice_id', $payment->getInvoiceId());
        $total = 0;
        foreach ($paymentMethodCollection as $record) {
            $total += $record->getTotal();
        }
        $totalPaid = round($total, 4);
        $beforeSaveTotalPaid = round(Mage::registry('opentechiz_order_total_paid_before'), 4);

        //if total paid = 0 and order is not shipped and is standard or exchange
        if($totalPaid == 0 && $totalPaid != $beforeSaveTotalPaid
            && !in_array($order->getOrderStage(), [0,5]) && in_array($order->getOrderType(), array(0,3))){

            $allItems = $order->getAllItems();
            foreach ($allItems as $orderItem){
                // add movement log
                $stock = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', $orderItem->getSku())->getFirstItem();
                $qty = Mage::helper("opentechiz_inventory")->countReleasedItem($orderItem->getId());
                if($qty > 0){
                    Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                        'qty' => $qty,
                        'on_hold' => '0',
                        'type' => 21,
                        'stock_id' => $stock->getId(),
                        'qty_before_movement' => $stock->getQty(),
                        'on_hold_before_movement' => $stock->getOnHold(),
                        'sku' => $stock->getSku(),
                        'order_id'=>$order->getId()
                    ));
                }
                //return items back to stock, delete all order item link
                $links = Mage::getModel('opentechiz_inventory/order')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItem->getId());
                foreach ($links as $link){
                    $item = Mage::getModel('opentechiz_inventory/item')->load($link->getItemId());
                    $item->setState(35)->save();
                    $link->delete();
                }

                //delete all process request
                $processRequests = Mage::getModel('opentechiz_production/process_request')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItem->getId());
                foreach ($processRequests as $processRequest){
                    $processRequest->delete();
                }

                //delete all stock request
                $stockRequests = Mage::getModel('opentechiz_inventory/inventory_instockRequest')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItem->getId());
                foreach ($stockRequests as $stockRequest){
                    $stockRequest->delete();
                }
            }
            Mage::register('changeOrderToVerificationWhenPaymentRecordBackToZero', true);
        }
    }

    public function checkOrderPaymentAndMoveToProcessing($observer){
        $payment = $observer->getEvent()->getPaymentgrid();
        //save order id if no order id
        if(!$payment->getOrderId()){
            if($payment->getType() == 'invoice'){
                $invoiceId = $payment->getInvoiceId();
                $orderId = Mage::getModel('sales/order_invoice')->load($invoiceId)->getOrderId();
            }else{
                $creditMemoId = $payment->getCreditMemoId();
                $orderId = Mage::getModel('sales/order_creditmemo')->load($creditMemoId)->getOrderId();
            }
            $payment->setOrderId($orderId)->save();
        }
        $order = Mage::getModel('sales/order')->load($payment->getOrderId());
        $_helper = Mage::helper('opentechiz_salesextend');
        $is_order_paid = $_helper->isOrderPaid($order);
        if($order->getOrderStage() == 0 && $order->getOrderType() == 0 && $is_order_paid) { //standard order, stage verification
            $_helper->updateOrderVerification($order, true);
            if(!Mage::registry('changeOrderToVerificationWhenPaymentRecordBackToZero')) { //prevent loop changeOrderStageToVerification
                $_helper->updateStageAndStatus($order, 1, null);
            }
        }
    }

    public function changeOrderStageToVerification($observer){
        if(!Mage::registry('changeOrderToVerificationWhenPaymentRecordBackToZero')){
            return;
        }
        if(Mage::registry('changeOrderStageToVerification')){
            return;
        }
        Mage::register('changeOrderStageToVerification', true);

        $order = $observer->getEvent()->getOrder();
        if($order->getOrderStage() != 0){
            if(in_array($order->getOrderType(), array(1,2))){
                $orderStage = 1;
            }else{
                $orderStage = 0;
            }
            Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, $orderStage, 0, true);
            Mage::helper('opentechiz_salesextend')->updateTermAndVerification($order, null, 0);
        }
    }
}
