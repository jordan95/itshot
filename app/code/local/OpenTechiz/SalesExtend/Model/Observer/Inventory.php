<?php
class OpenTechiz_SalesExtend_Model_Observer_Inventory
{
    public function orderItemChangeBefore($observer){
        $orderItem = $observer->getEvent()->getItem();
        $orderItem = Mage::getModel('sales/order_item')->load($orderItem->getId());
        if(!Mage::registry('order_item_data_before') && !empty($orderItem->getData())){
            Mage::register('order_item_data_before', $orderItem->getData());
        }
    }

    public function returnItemToStockOnOrderCancel($observer){
        if(Mage::registry('return_order_create')) return; //prevent logic conflict when create order from return
        $orderItem = $observer->getEvent()->getItem();
        $orderItemBefore = Mage::registry('order_item_data_before');
        Mage::unregister('order_item_data_before');
        if(!isset($orderItemBefore['qty_canceled']) || !isset($orderItemBefore['qty_refunded'])) return;
        $dataChanged = [];
        $dataChanged['qty_canceled'] = $orderItem->getData('qty_canceled') - $orderItemBefore['qty_canceled'];
        $dataChanged['qty_refunded'] = $orderItem->getData('qty_refunded') - $orderItemBefore['qty_refunded'];
        if($dataChanged['qty_canceled'] == 0
            && $dataChanged['qty_refunded'] == 0) {
            return;
        }
        $this->returnItemToStock($orderItem);
    }

    public function returnItemToStockOnOrderItemDelete($observer){
        $orderItem = $observer->getEvent()->getItem();
        $this->returnItemToStock($orderItem, 0);
    }

    public function returnItemToStock($orderItem, $newItemQty = null){
        $itemLinkCollection = Mage::getModel('opentechiz_inventory/order')->getCollection()
            ->join(array('item' => 'opentechiz_inventory/item'), 'main_table.item_id = item.item_id', '*')
            ->addFieldToFilter('order_item_id', $orderItem->getId());
        if($newItemQty === null) {
            $newItemQty = $orderItem->getQtyOrdered() - $orderItem->getQtyCanceled()
                - $orderItem->getQtyRefunded();
        }
        //get all item linked
        $qtyOver = count($itemLinkCollection) - $newItemQty;
        if($qtyOver > 0) {//if item linked > new qty
            //unlink in production
            foreach ($itemLinkCollection as $item){
                if($qtyOver == 0) break;
                $productionRequest = Mage::getModel('opentechiz_production/product')->getCollection()
                    ->addFieldToFilter('barcode', $item->getBarcode())
                    ->setOrder('personalized_id', 'DESC')
                    ->getFirstItem(); //get last production request
                //#1625
                if($productionRequest && $productionRequest->getId()
                    && $item->getState() == 0){ //currently in production, unlink order
                    $productionRequest->setOrderItemId(null)->save();
                    $qtyOver--;
                }elseif($productionRequest && $productionRequest->getId()
                    && $item->getState() == 1){
                    $productionRequest->delete();
                    $item = Mage::getModel('opentechiz_inventory/item')->load($item->getItemId());
                    $item->setState(35)->save(); //return to stock if in repair
                    $qtyOver--;
                }
            }
        }

        if($qtyOver > 0) {//if item linked > new qty
            //reset collection
            $itemLinkCollection = Mage::getModel('opentechiz_inventory/order')->getCollection()
                ->join(array('item' => 'opentechiz_inventory/item'), 'main_table.item_id = item.item_id', '*')
                ->addFieldToFilter('order_item_id', $orderItem->getId());
            //item in reserved -> back to stock
            foreach ($itemLinkCollection as $item){
                if($qtyOver == 0) break;
                if(in_array($item->getState(), OpenTechiz_Inventory_Helper_Data::RESERVED_STATE)){
                    $item = Mage::getModel('opentechiz_inventory/item')->load($item->getItemId());
                    $item->setState(35)->save();  //back to In Stock
                    //delete instock request
                    $instockRequest = Mage::getModel('opentechiz_inventory/inventory_instockRequest')
                        ->load($item->getBarcode(), 'barcodes');
                    $instockRequest->delete();
                    $qtyOver--;
                }
            }
        }

        if($qtyOver > 0) {//if item linked > new qty
            //reset collection
            $itemLinkCollection = Mage::getModel('opentechiz_inventory/order')->getCollection()
                ->join(array('item' => 'opentechiz_inventory/item'), 'main_table.item_id = item.item_id', '*')
                ->addFieldToFilter('order_item_id', $orderItem->getId());
            //item shipped -> instock waiting
            foreach ($itemLinkCollection as $item) {
                if($qtyOver == 0) break;
                if(in_array($item->getState(), OpenTechiz_Inventory_Helper_Data::SHIPPED_STATE)) {
                    $item = Mage::getModel('opentechiz_inventory/item')->load($item->getItemId());
                    //create return refund
                    //check if existing return
                    $existingReturnCollection = Mage::getModel('opentechiz_return/return')->getCollection()
                        ->addFieldToFilter('order_item_id', $orderItem->getId())
                        ->addFieldToFilter('item_barcode', $item->getBarcode());

                    if(count($existingReturnCollection) > 0){
                        $qtyOver--;
                        continue;  //skip if return already exist
                    }

                    //create return
                    $order = $orderItem->getOrder();
                    $creditmemoItem = Mage::getModel('sales/order_creditmemo_item')->load($orderItem->getId(), "order_item_id");
                    $shippingAddress = $order->getBillingAddress();
                    $data = [];
                    $data['customer_id'] = $order->getCustomerId();
                    $data['order_item_id'] = $orderItem->getId();
                    $data['return_type'] = OpenTechiz_Return_Helper_Data::WITHDRAWAL;
                    $data['first_name'] = $shippingAddress->getData('firstname');
                    $data['last_name'] = $shippingAddress->getData('lastname');
                    $data['email'] = $shippingAddress->getData('email');
                    $data['phone'] = $shippingAddress->getData('telephone');
                    $data['street'] = $shippingAddress->getData('street');
                    $data['city'] = $shippingAddress->getData('city');
                    $data['region'] = $shippingAddress->getData('region');
                    $data['postcode'] = $shippingAddress->getData('postcode');
                    $data['item_barcode'] = $item->getBarcode();
                    $data['reason'] = OpenTechiz_Return_Helper_Data::REFUNDAFTERSHIPPED;
                    $data['solution'] = OpenTechiz_Return_Helper_Data::REFUND;
                    if(!$creditmemoItem->getQty()){
                        $data['refund_amount'] = 0;
                    }else {
                        $data['refund_amount'] = $creditmemoItem->getBaseRowTotal() / $creditmemoItem->getQty();
                    }
                    $return = Mage::getModel('opentechiz_return/return')->setData($data)->save();
                    $return->setIncrementId($return->generatedIncrementId($return->getId()))->save();
                    $qtyOver--;
                    
                    $item->setState(30)->save(); //instock waiting
                }
            }
        }
    }
}