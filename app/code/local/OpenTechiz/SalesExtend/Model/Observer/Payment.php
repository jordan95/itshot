<?php
class OpenTechiz_SalesExtend_Model_Observer_Payment
{
    public function changeStatusWhenPaypalReview($observer){
        $payment = $observer->getEvent()->getPayment();
        $order = mage::getModel('sales/order')->load($payment->getParentId());

        $method = $payment->getMethod();
        if(stripos($method, 'paypal') !== false && in_array($order->getOrderType(), array(0,3))){ #1764 Process Improvements: Order Verification stage
            if(!is_array($payment->getAdditionalInformation())) {
                $additionalInfo = unserialize($payment->getAdditionalInformation());
            }else{
                $additionalInfo = $payment->getAdditionalInformation();
            }

            if(isset($additionalInfo[Mage_Paypal_Model_Info::PAYMENT_STATUS_GLOBAL])) {
                $status = $additionalInfo[Mage_Paypal_Model_Info::PAYMENT_STATUS_GLOBAL];
            }else{
                $status = '';
            }

            if(($payment->getIsTransactionPending() || $status == Mage_Paypal_Model_Info::PAYMENTSTATUS_PENDING) && $order->getVerification() != 12 && $order->getOrderStage() == 0){
                Mage::helper('opentechiz_salesextend')->updateTermAndVerification($order, null, 12);
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 0, 30);
            }elseif((stripos($status, Mage_Paypal_Model_Info::PAYMENTSTATUS_DENIED) !== false) && !Mage::helper('opentechiz_salesextend')->orderHasStatus(29, $order)){
                Mage::helper('opentechiz_salesextend')->updateTermAndVerification($order, null, 11);
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 0, 29);
            }elseif($status == Mage_Paypal_Model_Info::PAYMENTSTATUS_COMPLETED && $order->getOrderStage() === 0){ //when transaction updated to completed
                //move order to processing
                Mage::helper('opentechiz_salesextend')->updateTermAndVerification($order, null, 2); // change verification to Guaranteed
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 1, 0);

                //add payment record for transaction
                $transactionId = $payment->getLastTransId();
                $transaction = Mage::getModel('sales/order_payment_transaction')->load($transactionId, 'txn_id');
                $transactionType = $transaction->getTxnType();
                if($transactionType){
                    Mage::getModel('opentechiz_salesextend/observer')->addPaymentRecordForTransaction($transaction, $transactionType);
                }
            }
        }
    }

    public function changeStatusForOrderPaidWithRewardPoint($observer){
        if(Mage::registry('changeStatusForOrderPaidWithRewardPoint')) return;
        else Mage::register('changeStatusForOrderPaidWithRewardPoint', true);
        $order = $observer->getEvent()->getOrder();

        $totalPaid = Mage::helper('opentechiz_salesextend')->getTotalPaymentRecordForOrder($order);

        if($order->getOrderType() == OpenTechiz_SalesExtend_Helper_Data::ORDER_TYPE_LAYAWAY_CODE) {
            $totalReduced = 0;

            //change order status for reward point spent
            $dataRewardArray = $order->getData('rewards_cart_discount_map');
            if ($dataRewardArray) {
                if (!is_array($dataRewardArray)) {
                    $dataRewardArray = json_decode($dataRewardArray, true);
                }
                if (!is_array($dataRewardArray)) { //avoid error
                    return;
                }
                foreach ($dataRewardArray as $key => $dataReward) {
                    if (isset($dataReward['base_discount_amount']) && $dataReward['base_discount_amount']) {
                        $amountPaidWithPoint = round($dataReward['base_discount_amount'], 4);

                        $totalReduced += $amountPaidWithPoint;
                    }
                }
            }

            //change order status for store credit spent
            $totalCreditSpent = $order->getData('base_credit_amount');
            $totalReduced += $totalCreditSpent;


            //compare to total to change status
            if (!$totalPaid && $totalReduced > 0) { //no change status when have payment record, logic in another observer
                $orderSubTotal = round($order->getBaseSubtotal(), 4);

                $internalStatus = 0;

                if ($totalReduced >= $orderSubTotal / 2) {
                    $internalStatus = 15;
                }elseif ($totalReduced > 0) {
                    $internalStatus = 5;
                }

                if ($totalReduced >= $orderSubTotal) {
                    Mage::helper('opentechiz_salesextend')->addStatusToOrder(7, $order);
                }

                if($internalStatus && !Mage::helper('opentechiz_salesextend')->orderHasStatus($internalStatus, $order)) {
                    Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, null, $internalStatus);
                }
            }
        }
    }
}
