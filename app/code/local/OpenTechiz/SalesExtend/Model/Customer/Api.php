<?php

class OpenTechiz_SalesExtend_Model_Customer_Api extends Mage_Customer_Model_Customer_Api
{

    protected $_mapAttributes = array(
        'customer_id' => 'entity_id',
        'is_fraudster' => 'is_fraudster',
    );

    public function cart($customerId)
    {
        $customer = Mage::getModel('customer/customer')->load($customerId);
        if (!$customer->getId()) {
            return;
        }
        $quote = Mage::getModel('sales/quote')
                ->setStoreId($customer->getStoreId())
                ->loadByCustomer($customer);
        if (!$quote->getId()) {
            return;
        }
        return Mage::getModel('checkout/cart_api')->info($quote->getId());
    }

}
