<?php

class OpenTechiz_SalesExtend_Model_OrderSource extends TBT_Rewards_Model_Helper_Layout_Action_Append
{
    /**
     * Parent Block Instance
     * @var mixed|Mage_Core_Block_Abstract
     */
    private $_parentBlock;

    /**
     * Before Html that will be appended to parent block
     * @var string
     */
    private $_beforeBlockHtml;

    /**
     * After Html that will be appended to parent block
     * @var string
     */
    private $_afterBlockHtml;

    /**
     * Config path with boolean value to append or not html to parent block
     * @var string
     */
    private $_ifConfig;

    /**
     * Main Constructor
     * @param Mage_Core_Block_Abstract $parentBlock
     * @param string $ifConfig
     */
    public function __construct($parentBlock = null, $ifConfig = null)
    {
        $this->_parentBlock = $parentBlock;

        $this->_ifConfig = $ifConfig;
    }

    /**
     * Setter for parent block
     * @param Mage_Core_Block_Abstract $parentBlock
     * @return \TBT_Rewards_Model_Helper_Layout_Action_Append
     */
    public function setParentBlock($parentBlock)
    {
        $this->_parentBlock = $parentBlock;

        return $this;
    }

    /**
     * Setter for config path used to decide if the html will be appended
     * to parent block
     * @param string $ifConfig
     * @return \TBT_Rewards_Model_Helper_Layout_Action_Append
     */
    public function setIfConfig($ifConfig)
    {
        $this->_ifConfig = $ifConfig;

        return $this;
    }

    /**
     * Add block content or instance to be added as html before or after parent block
     * @param string|Mage_Core_Block_Abstract $block
     * @param string $position {'before', 'after'}
     * @return \TBT_Rewards_Model_Helper_Layout_Action_Append
     */
    public function add($block, $position = 'before')
    {
        if ($block instanceof Mage_Core_Block_Abstract) {
            $block = $block->toHtml();
        }

        if ($position === 'after') {
            $this->_afterBlockHtml .= $block;
        } else {
            $this->_beforeBlockHtml .= $block;
        }

        return $this;
    }

    /**
     * Appends the html from all defined block to parent block and after clears
     * this instance properties
     * @return \TBT_Rewards_Model_Helper_Layout_Action_Append
     */
    public function append()
    {
        if (!($this->_parentBlock instanceof Mage_Core_Block_Abstract)) {
            return $this->_clearData();
        }

        if (!is_null($this->_ifConfig) && !Mage::getStoreConfigFlag($this->_ifConfig)) {
            return $this->_clearData();
        }

        if (is_null($this->_afterBlockHtml) && is_null($this->_beforeBlockHtml)) {
            return $this->_clearData();
        }

        return $this->_clearData();
    }

    /**
     * Clears properties of this instance to make sure that we do not override
     * anything when this class has multiple usages
     * @return \TBT_Rewards_Model_Helper_Layout_Action_Append
     */
    private function _clearData()
    {
        $this->_parentBlock = null;
        $this->_ifConfig = null;
        $this->_beforeBlockHtml = null;
        $this->_afterBlockHtml = null;

        return $this;
    }
}