<?php

class OpenTechiz_SalesExtend_Model_Item extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_salesextend/item');
    }

    public function requestToProcess($orderItemId){

        $item = Mage::getModel('sales/order_item')->load($orderItemId);
        $requestModel = Mage::getModel('opentechiz_production/process_request');
        $orderId = $item->getOrderId();
        $qty = $item->getQtyOrdered() - $item->getQtyRefunded() - $item->getQtyCanceled();
        for($i = 0; $i < (int)$qty; $i++){
            $data=[];
            $data['order_item_id'] = $orderItemId;
            $data['order_id'] = $orderId;
            
            $requestModel->setData($data)->save();
        }
    }

    public function requestToProcessQuotation($order){

        $quoteProductModel = Mage::getModel('opentechiz_quotation/product');
        $quoteProductOptionModel = Mage::getModel('opentechiz_quotation/productOption');
        $requestModel = Mage::getModel('opentechiz_production/product');
        $itemModel = Mage::getModel('opentechiz_inventory/item');

        $data = [];
        $productData = unserialize($order->getData('product_options'));
        $quoteProductId = $productData['info_buyRequest']['options']['quotation_product_id'];
        $quoteProduct = $quoteProductModel->load($quoteProductId);

        $quoteProductImageArray = explode(',', $quoteProduct->getQpImage());
        if(count($quoteProductImageArray)>0) {
            $data['image'] = $quoteProductImageArray[0];
        } else $data['image'] = '';
        $data['order_item_id'] = $order->getId();
        $data['production_type'] = OpenTechiz_Production_Helper_Data::OPPENTECHIZ_PRODUCTION_PRODUCTION_TYPE[0];
        $data['option'] = $order->getData('product_options');
        $data['note'] = '';
        $data['item_state'] = 0;
        $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $data['status'] = OpenTechiz_Production_Helper_Data::PROCESSES[0];

        $orderData = [];
        $orderData['order_item_id'] = $order->getId();

        //calculate cost
        $optionCollection = Mage::getModel('opentechiz_quotation/productOption')->getCollection()->addFieldToFilter('quote_product_id', $quoteProductId);
        $cost = 0.00;
        foreach ($optionCollection as $option){
            if($option->getOptionType() == 'stone'){
                $stoneId = $option->getOptionValue();
                $qty = $option->getQty();
                $price = Mage::getModel('opentechiz_material/stone')->load($stoneId)->getPrice() * $qty;
                $cost += $price;
            } else if($option->getOptionType() == 'gold'){
                $goldId = $option->getOptionValue();
                $qty = $option->getQty();
                $price = Mage::getModel('opentechiz_material/stone')->load($goldId)->getPrice() * $qty;
                $cost += $price;
            }
        }
        $idArray = [];
        for($i = 0; $i < $order->getQtyOrdered(); $i ++) {
            //save
            $requestModel->setData($data)->save();
            //save item
            $itemData = [];
            $itemData['sku'] = $order->getSku();
            $itemData['name'] = $order->getName();
            $itemData['image'] = $data['image'];
            $itemData['type'] = 0;
            $itemData['options'] = $data['option'];
            $itemData['note'] = '';
            $itemData['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $itemData['updated_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $itemData['state'] = 0;
            $itemData['cost'] = $cost;
            $itemModel->setData($itemData)->save();

            $item_id = $itemModel->getId();
            $orderData['item_id'] = $item_id;

            Mage::getModel('opentechiz_inventory/order')->setData($orderData)->save();

            $itemData['barcode'] = $itemModel->generateBarcode($item_id);
            $itemModel->addData($itemData)->setId($item_id)->save();
            //add barcode
            $id = $requestModel->getId();
            $data['barcode'] = $itemData['barcode'];
            $requestModel->addData($data)->setId($id)->save();

            array_push($idArray, $id);

            $stoneOptionCollection = $quoteProductOptionModel->getCollection()
                ->addFieldToFilter('quote_product_id', $quoteProduct->getId())
                ->addFieldToFilter('option_type', 'stone');
            Mage::getModel('opentechiz_material/stoneRequest')->addStoneRequestQuotation($id, $stoneOptionCollection);

            Mage::helper('opentechiz_production/email')->sendEmailProduction('start', $idArray);
        }
        $order->setIsRequestedToProcess(1)->save();
    }
    
    public function requestToProcessReturn($orderId, $qty = 1,$return_id,$orderSku){
        $isInstock = false;
        $model = Mage::getSingleton('sales/order_item');
        $requestModel = Mage::getModel('opentechiz_production/product');
        $itemModel = Mage::getModel('opentechiz_inventory/item');
        
        $instockRequestModel = Mage::getModel('opentechiz_inventory/inventory_instockRequest');

        $model = $model->load($orderId);
       
        //check if available in stock
        $result = Mage::helper('opentechiz_inventory')->getAvailableSize($orderSku, true);
        if(!empty($result)){
            $isInstock = true;
            $data = $result[max(array_keys($result))];
            $missing = 0;
            //create instock request
            $instockRequestData = [];
            $instockRequestData['order_item_id'] = $orderId;
            $instockRequestData['instock_product_id'] = $data['id'];
            $itemLeft = $data['qty'] - $data['on_hold'];
            $instockRequestData['return_id'] = $return_id;
            if($itemLeft >= 1) $instockRequestData['qty'] = 1;
           
            $instockRequestModel->setData($instockRequestData)->save();

            $admin = Mage::getSingleton('admin/session')->getUser();
            if($admin->getId()){
                Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                    'user_id' => $admin->getId(),
                    'description' => 'Add instock request for order item #'.$orderId.' return '.$return_id,
                    'qty' => '0',
                    'on_hold' => '+'.$instockRequestData['qty'],
                    'type' => 0,
                    'stock_id' => $data['id']
                ));
            }

            //change state in item 
            $item = $itemModel->getCollection()->addFieldToFilter('sku', $data['sku'])->addFieldToFilter('state', 35)->getFirstItem();
            $item->addData([
                    'state' => 25,
                    'updated_at' => Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s')
                ])
                    ->setId($item->getId())
                    ->save();

            
            return $item->getBarcode();
        } 
       

        return $isInstock;
    }

    public function addStockRequest($orderId, $qty){
        $orderItem = Mage::getModel('sales/order_item')->load($orderId);
        $instockRequestModel = Mage::getModel('opentechiz_inventory/inventory_instockRequest');

        $orderSku = $orderItem->getSku();

        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $result = $conn->fetchAll("SELECT * FROM tsht_inventory_instock_product where sku = '" . $orderSku . "'");

        $instockProductData = $result[0];
        $data = [];
        $data['order_item_id'] = $orderId;
        $data['instock_product_id'] = $instockProductData['id'];
        $data['qty']  = $qty;

        $instockRequestModel->setData($data)->save();

        $orderItem->setIsRequestedToProcess(1);
        $orderItem->save();
    }

    public function hasEnoughInstock($orderId, $qty){
        $orderItem = Mage::getModel('sales/order_item')->load($orderId);

        $orderSku = $orderItem->getSku();

        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $result = $conn->fetchAll("SELECT * FROM tsht_inventory_instock_product where sku = '" . $orderSku . "' and qty >= " .(int)$qty);

        if(count($result) > 0){
            return true;
        } else return false;
    }
}