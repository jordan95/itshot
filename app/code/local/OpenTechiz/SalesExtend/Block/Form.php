<?php

class OpenTechiz_SalesExtend_Block_Form extends Mage_Payment_Block_Form_Cc
{
    /**
     * Retrieve field value data from payment info object
     *
     * @param   string $field
     * @return  mixed
     */
    public function getInfoData($field)
    {
        if($this->getMethod()->getCode() == 'authorizenet' && $field == 'cc_number'){
            $params = Mage::app()->getRequest()->getParams();
            if(array_key_exists('order_id', $params)){
                $order = Mage::getModel('sales/order')->load($params['order_id']);
                $conn = Mage::helper('medialounge_checkout/payment')->getSecondConnection('read');
                $query = 'SELECT * FROM ' . Mage::getModel('core/resource')->getTableName('medialounge_checkout/quote_payment') .' WHERE quote_id = '.$order->getQuoteId();
                $results = $conn->fetchAll($query);
                if(count($results) > 0) {
                    $info = $this->getMethod()->getInfoInstance();
                    return $this->escapeHtml($info->decrypt($results[0]['cc_number_enc']));
                }
            }
        } elseif($this->getMethod()->getCode() == 'authorizenet' && $field == 'cc_cid'){
            $params = Mage::app()->getRequest()->getParams();
            if(array_key_exists('order_id', $params)){
                $order = Mage::getModel('sales/order')->load($params['order_id']);
                $conn = Mage::helper('medialounge_checkout/payment')->getSecondConnection('read');
                $query = 'SELECT * FROM ' . Mage::getModel('core/resource')->getTableName('medialounge_checkout/quote_payment') .' WHERE quote_id = '.$order->getQuoteId();
                $results = $conn->fetchAll($query);
                if(count($results) > 0) {
                    $info = $this->getMethod()->getInfoInstance();
                    return $this->escapeHtml($info->decrypt($results[0]['cc_cid_enc']));
                }
            }
        } elseif($field == 'cc_type'){
            if(!$this->getMethod()->getInfoInstance()->getData($field)){ //if type is null
                //get data from additional info
                $info = $this->getMethod()->getInfoInstance()->getData('additional_information');
                if(isset($info['authorize_cards'])){
                    foreach ($info['authorize_cards'] as $dataArray){
                        if(isset($dataArray['cc_type'])) return $this->escapeHtml($dataArray['cc_type']);
                    }
                }
            }
        }
        return $this->escapeHtml($this->getMethod()->getInfoInstance()->getData($field));
    }
}