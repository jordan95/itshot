<?php
class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    protected function _prepareColumns()
    {
        $orderSources = OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE;
        asort($orderSources);
        $orderTypes = OpenTechiz_SalesExtend_Helper_Data::ORDER_TYPE;
        asort($orderTypes);
        $orderStages = OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE;
        asort($orderStages);
        $verifications = OpenTechiz_SalesExtend_Helper_Data::VERIFICATION;
        asort($verifications);
        $paymentMethods = Mage::helper('opentechiz_salesextend')->getAllPaymentMethod();
        asort($paymentMethods);
        $internalStatuses = Mage::helper('opentechiz_salesextend')->getAllInternalStatus();
        asort($internalStatuses);
        $listUser = Mage::helper('opentechiz_userprocessorder')->getAllUser();
        asort($listUser);
        $statusOptions = [
            '0' => 'Active',
            '1' => 'Cancelled/Closed',
            '2' => 'On Hold',
            '3' => 'Complete',
            '4' => 'Item(s) Returned'
        ];
        asort($statusOptions);

        $this->addColumnAfter('source', array(
            'header'    => $this->__('Order Source'),
            'width'     => '100',
            'type'      => 'options',
            'index' => 'source',
            'filter_index'=>'main_table.source',
            'options'   => $orderSources
        ), 'real_order_id');
        $this->addColumnAfter('order_type', array(
            'header'    => $this->__('Order Type'),
            'width'     => '100',
            'type'      => 'options',
            'index' => 'order_type',
            'filter_index'=>'main_table.order_type',
            'options'   => $orderTypes
        ), 'source');
        $this->addColumnAfter('paymentmethod', array(
            'header'    => $this->__('Payment Method'),
            'width'     => '100',
            'type'      => 'options',
            'sortable' => false,
            'options'   => $paymentMethods,
            'renderer'  => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_PaymentMethod',
            'filter_condition_callback' => array($this, '_paymentMethodFilter')
        ),'grand_total');
        $this->addColumnAfter('shipping_description', array(
            'header'    => $this->__('Shipping Method'),
            'width'     => '100',
            'type'      => 'text',
            'index' => 'shipping_description'
        ),'paymentmethod');
        $this->addColumnAfter('customer_email', array(
            'header'    => $this->__('Customer Email'),
            'width'     => '100',
            'type'      => 'text',
            'index' => 'customer_email'
        ),'shipping_description');
        $this->addColumnAfter('is_fraudster', array(
            'header'    => $this->__('Fraudster'),
            'width'     => '30',
            'type'      => 'options',
            'index' => 'is_fraudster',
            'sortable' => false,
            'options'   => ['0' => 'No','1' => 'Yes'],
            'renderer'  => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Fraudster',
            'filter_condition_callback' => array($this, '_fraudsterFilter')
        ),'billing_name');
        $this->addColumnAfter('base_grand_total_custom', array(
            'header' => Mage::helper('sales')->__('Order Total'),
            'index' => 'base_grand_total',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
        ),'is_fraudster');
        $this->addColumnAfter('customer_phone', array(
            'header'    => $this->__('Customer Phone'),
            'width'     => '100',
            'type'      => 'text',
            'index' => 'telephone'
        ),'customer_email');
        $this->addColumnAfter('ship_by', array(
            'header'    => $this->__('Ship By'),
            'width'     => '100',
            'type'      => 'date',
            'index'     => 'ship_by',
            'filter'    => false,
        ),'customer_phone');
        $this->addColumnAfter('deliver_by', array(
            'header'    => $this->__('Deliver By'),
            'width'     => '100px',
            'type'      => 'date',
            'index'     => 'deliver_by',
            'filter'    => false,
        ),'ship_by');
        $this->addColumnAfter('order_stage', array(
            'header'    => $this->__('Order Stage'),
            'width'     => '100',
            'type'      => 'options',
            'index'     => 'order_stage',
            'filter_index'=>'main_table.order_stage',
            'options'   => $orderStages,
        ),'deliver_by');
        $this->addColumnAfter('internal_status', array(
            'header'    => $this->__('Internal Status'),
            'width'     => '100px',
            'type'      => 'options',
            'options'   => $internalStatuses,
            'index'     => 'internal_status',
            'renderer'  => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_InternalStatus',
            'filter_condition_callback' => array($this, '_internalStatusFilter')
        ),'order_stage');
        $this->addColumnAfter('verification', array(
            'header'    => $this->__('Order Verification'),
            'width'     => '100px',
            'type'      => 'options',
            'index'     => 'verification',
            'filter_index' => 'main_table.verification',
            'options'   => $verifications,
            'renderer'  => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Verification'
        ),'internal_status');
        $this->addColumnAfter('last_changed', array(
            'header'    => $this->__('Changed On'),
            'width'     => '100px',
            'type'      => 'datetime',
            'filter'    => false,
            'index'     => 'last_changed',
            'filter_index' => 'change_history.updated_at',
        ),'verification');
    
        $this->addColumnAfter('returns', array(
            'header'    => $this->__('RAN'),
            'sortable'  => false,
            'filter_condition_callback' => array($this, '_returnsFilter'),
            'renderer'  => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Returns'
        ),'last_changed');
        $this->addColumnAfter('state', array(
            'header'    => $this->__('Status'),
            'width'     => '100',
            'type'      => 'options',
            'index' => 'state',
            'options'   => $statusOptions,
            'filter_condition_callback' => array($this, '_stateOrderFilter'),
            'renderer'  => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Status'
        ),'returns');
         $this->addColumnAfter('comment', array(
            'header'    => $this->__('Comment'),
            'width'     => '100',
            'type'      => 'text',
            'index' => 'comment',
            'renderer'  => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Comment',
            'filter_condition_callback' => array($this, '_commentFilter')
        ),'state');
        $this->addColumnAfter('order_number_id', array(
            'header' => Mage::helper('sales')->__('Order #'),
            'width'  => '100px',
            'type'   => 'text',
            'index'  => 'increment_id',
            'escape' => true,
            'renderer'  => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_OrderNumber'
        ), 'real_order_id');
        $this->addColumnAfter('username', array(
            'header'    => $this->__('Processed By'),
            'width'     => '100',
            'type'      => 'options',
            'sortable' => false,
            'options'   => $listUser,
            'index' => 'username',
            'filter_condition_callback' => array($this, '_userFilter')
        ),'comment');
        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $this->addColumn('action_left',
                array(
                    'header'    => Mage::helper('sales')->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'     => 'getId',
                    'actions'   => array(
                        array(
                            'caption' => Mage::helper('sales')->__('View'),
                            'url'     => array('base'=>'*/sales_order/view'),
                            'field'   => 'order_id',
                            'data-column' => 'action',
                            'target' => '_blank'
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => true,
            ));
        }
        $this->removeColumn('payment_bank_number');
        parent::_prepareColumns();
        $this->removeColumn('status');
        $this->removeColumn('guarantee');
        $this->removeColumn('score');
        $this->removeColumn('shipping_name');
        $this->removeColumn('real_order_id');
        $this->removeColumn('action');
        $this->removeColumn('grand_total');
        $this->removeColumn('base_grand_total');
        
        
        return $this;
    }
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('order_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

         $this->getMassactionBlock()->addItem('invoice_pdfshipments_order', array(
             'label'=> Mage::helper('sales')->__('Print Invoice And Packingslips'),
             'url'  => $this->getUrl('*/invoiceandshipment/printpdf'),
        ));

        $this->getMassactionBlock()->addItem('source_edit', array(
            'label'=> Mage::helper('opentechiz_salesextend')->__('Change order source'),
            'url'  => $this->getUrl('*/approve/changeSource', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('opentechiz_salesextend')->__('Select'),
                    'values' => OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE
                )
            )
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/cancel')) {
            $this->getMassactionBlock()->addItem('cancel_order', array(
                 'label'=> Mage::helper('sales')->__('Cancel'),
                 'url'  => $this->getUrl('*/sales_order/massCancel'),
            ));
        }

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/hold')) {
            $this->getMassactionBlock()->addItem('hold_order', array(
                 'label'=> Mage::helper('sales')->__('Hold'),
                 'url'  => $this->getUrl('*/sales_order/massHold'),
            ));
        }

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/unhold')) {
            $this->getMassactionBlock()->addItem('unhold_order', array(
                 'label'=> Mage::helper('sales')->__('Unhold'),
                 'url'  => $this->getUrl('*/sales_order/massUnhold'),
            ));
        }

        $this->getMassactionBlock()->addItem('pdfinvoices_order', array(
             'label'=> Mage::helper('sales')->__('Print Invoices'),
             'url'  => $this->getUrl('*/sales_order/pdfinvoices'),
        ));
        $this->getMassactionBlock()->addItem('pdfshipments_order', array(
             'label'=> Mage::helper('sales')->__('Print Packingslips'),
             'url'  => $this->getUrl('*/sales_order/pdfshipments'),
        ));
        $this->getMassactionBlock()->addItem('pdfcreditmemos_order', array(
             'label'=> Mage::helper('sales')->__('Print Credit Memos'),
             'url'  => $this->getUrl('*/sales_order/pdfcreditmemos'),
        ));

        $this->getMassactionBlock()->addItem('pdfdocs_order', array(
             'label'=> Mage::helper('sales')->__('Print All'),
             'url'  => $this->getUrl('*/sales_order/pdfdocs'),
        ));

        $this->getMassactionBlock()->addItem('print_shipping_label', array(
             'label'=> Mage::helper('sales')->__('Print Shipping Labels'),
             'url'  => $this->getUrl('*/sales_order_shipment/massPrintShippingLabel'),
        ));
        
        return $this;
    }

    protected function _prepareCollection()
    {

        $collection = Mage::getResourceModel($this->_getCollectionClass())
            ->join(
                'sales/order',
                '`sales/order`.entity_id = `main_table`.entity_id',
                array('customer_email'  => 'customer_email','state'  => 'state','shipping_description'  => 'shipping_description','processed_by_user' =>'processed_by_user')
            )
            ->join(
                'sales/order_address',
                '`sales/order_address`.parent_id = `main_table`.entity_id AND `sales/order_address`.address_type = "billing" ',
                array('telephone'  => 'telephone')
            )->addFilterToMap('increment_id', 'main_table.increment_id')->addFilterToMap('created_at', 'main_table.created_at')->addFilterToMap('store_id', 'main_table.store_id')->addFilterToMap('billing_name', 'main_table.billing_name')->addFilterToMap('shipping_name', 'main_table.shipping_name')->addFilterToMap('base_grand_total', 'main_table.base_grand_total')->addFilterToMap('grand_total', 'main_table.grand_total')->addFilterToMap('status', 'main_table.status')->addFilterToMap('internal_status', 'main_table.internal_status')
        ;
        $collection->getSelect()->joinLeft(array('customer_en'=>'tsht_customer_entity'), '`sales/order`.customer_id = customer_en.entity_id AND `sales/order`.store_id = `customer_en`.store_id', 'customer_en.is_fraudster')


            ->joinLeft(array('history'=>'tsht_sales_flat_order_status_history'), '`sales/order`.entity_id = history.parent_id AND history.has_pin_comment = 1', 'history.comment')
            ->joinLeft(array('change_history'=>'tsht_order_status_change_log'), '`sales/order`.last_history_id = change_history.id', array('last_changed' => 'change_history.updated_at'))
            ->joinLeft(array('order_item'=>'tsht_sales_flat_order_item'), '`sales/order`.entity_id = order_item.order_id', array('sum_return_qty' => 'sum(order_item.qty_returned)'))
            ->joinLeft(
                ['return' => $collection->getTable('opentechiz_return/return')],
                '`return`.`order_item_id` = `order_item`.`item_id`',
                []
            )
            ->joinLeft(array('user'=>'tsht_admin_user'), '`sales/order`.processed_by_user = user.user_id', array('username' => 'username'));
        $collection->getSelect()->group("entity_id");
        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }

    // function that takes care of the filter
    protected function _internalStatusFilter($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        $internalStatus = OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS;
        $layawayInternalStatus = OpenTechiz_SalesExtend_Helper_Data::LAYAWAY_ORDER_INTERNAL_STATUS;
        $repairInternalStatus = OpenTechiz_SalesExtend_Helper_Data::REPAIR_ORDER_INTERNAL_STATUS;

        $types = '(';
        $searchedKey = '(';
        foreach ($internalStatus as $key => $name){
            if(strpos(strtolower($name), strtolower($value)) !== false){
                $searchedKey .= $key.',';
                $types .= '0,3,';
            }
        }
        foreach ($layawayInternalStatus as $key => $name){
            if(strpos(strtolower($name), strtolower($value)) !== false){
                $searchedKey .= $key.',';
                $types .= '1,';
            }
        }
        foreach ($repairInternalStatus as $key => $name){
            if(strpos(strtolower($name), strtolower($value)) !== false){
                $searchedKey .= $key.',';
                $types .= '2,';
            }
        }

        $types = trim($types, ',');
        $types .= ')';
        $searchedKey = trim($searchedKey, ',');
        $searchedKey .= ')';

        if($types != '()' && $searchedKey != '()') {
            $whereString = 'main_table.internal_status in ' . $searchedKey . ' and main_table.order_type in ' . $types;
            if(strtolower($value) == 'new'){  //exclude not processed status
                $whereString .= ' and not (main_table.internal_status = 0 and main_table.order_stage = 1 and main_table.order_type = 1)';
            }
            $collection->getSelect()->where($whereString);
        }else{
            $collection->getSelect()->where('main_table.entity_id = 0');
        }
        return $this;
    }
    protected function _fraudsterFilter($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        $collection->getSelect()->where('customer_en.is_fraudster = '.$value);
        return $this;
    }
    protected function _stateOrderFilter($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        if($value =="0"){
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterActiveOrder($collection);
        }elseif($value =="1"){
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterCancelCloseOrder($collection);
        }elseif($value =="2"){
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterHoldOrder($collection);
        }
        elseif($value =="3"){
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterCompleteOrder($collection);
            $collection->getSelect()->where("`sales/order`.state ='complete'");
        }elseif($value =="4"){
            $collection->getSelect()->where("`main_table`.total_qty_returned > 0");
        }
        return $this;
    }
    protected function _paymentMethodFilter($collection, $column)
    {
       
        $value = $column->getFilter()->getValue();

        $collection->getSelect()->join(array('payment'=>'tsht_sales_flat_order_payment'), '`sales/order`.entity_id = payment.parent_id AND payment.method = '.'\''.$value.'\'' , 'payment.method');
        return $this;
    }
    protected function _commentFilter($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        $collection->getSelect()->where('history.comment like \'%'.$value.'%\'');
        return $this;
    }
    protected function _userFilter($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        $collection->getSelect()->where('user.user_id ='. $value);
        return $this;
    }
    protected function _returnsFilter($collection, $column)
    {
        $value = trim($column->getFilter()->getValue());
        $collection->getSelect()->where("return.increment_id LIKE ?", "%$value%");
        return $this;
    }
    public function __construct()
    {
        parent::__construct();
        $this->setSaveParametersInSession(false);
    }
    public function getParam($paramName, $default=null)
    {
        $session = Mage::getSingleton('adminhtml/session');
        $sessionParamName = $this->getId().$paramName;
        $this->_saveParametersInSession  = false; //set is false in order to don't save filter
        if ($this->getRequest()->has($paramName)) {
            $param = $this->getRequest()->getParam($paramName);
            if ($this->_saveParametersInSession) {
                $session->setData($sessionParamName, $param);
            }
            return $param;
        }
        elseif ($this->_saveParametersInSession && ($param = $session->getData($sessionParamName)))
        {
            return $param;
        }

        return $default;
    }
}