<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_View_Creditmemos
    extends Mage_Adminhtml_Block_Sales_Order_View_Tab_Creditmemos
{
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass())
            ->addFieldToSelect('entity_id')
            ->addFieldToSelect('created_at')
            ->addFieldToSelect('increment_id')
            ->addFieldToSelect('order_currency_code')
            ->addFieldToSelect('store_currency_code')
            ->addFieldToSelect('base_currency_code')
            ->addFieldToSelect('state')
            ->addFieldToSelect('grand_total')
            ->addFieldToSelect('base_grand_total')
            ->addFieldToSelect('billing_name')
            ->join(array('creditmemo' => 'sales/creditmemo'),
                'creditmemo.entity_id = main_table.entity_id and main_table.order_id = '.$this->getOrder()->getId(),
                '*')
        ;
        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        $this->addColumn('increment_id', array(
            'header' => Mage::helper('sales')->__('Credit Memo #'),
            'width' => '120px',
            'index' => 'increment_id',
            'filter_index' =>'creditmemo.increment_id',
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Created At'),
            'index' => 'created_at',
            'filter_index' =>'creditmemo.created_at',
            'type' => 'datetime',
        ));

        $this->addColumn('state', array(
            'header'    => Mage::helper('sales')->__('Status'),
            'index'     => 'state',
            'filter_index' =>'creditmemo.state',
            'type'      => 'options',
            'options'   => Mage::getModel('sales/order_creditmemo')->getStates(),
        ));

        $this->addColumn('base_grand_total', array(
            'header'    => Mage::helper('customer')->__('Refunded'),
            'index'     => 'base_grand_total',
            'filter_index' =>'creditmemo.base_grand_total',
            'type'      => 'currency',
            'currency'  => 'base_currency_code',
        ));

        $this->addColumn('base_credit_total_refunded', array(
            'header'    => Mage::helper('customer')->__('Refunded Store Credit'),
            'index'     => 'base_credit_total_refunded',
            'type'      => 'currency',
            'currency'  => 'base_currency_code',
        ));
        return $this;
        
    }
}