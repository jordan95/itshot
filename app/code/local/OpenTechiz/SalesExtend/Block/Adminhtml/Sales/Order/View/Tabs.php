<?php
class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_View_Tabs extends Mage_Adminhtml_Block_Sales_Order_View_Tabs
{
    const OTHER_ORDERS_TAB_NAME = 'Other Orders';
    const EXCLUDED_STATUSES = array('shipped_complete', 'complete', 'closed', 'canceled', 'layawayorder_canceled');

    protected function _beforeToHtml()
    {
        $order = $this->getOrder();
        $excludedStatus = self::EXCLUDED_STATUSES;
        if($order->getCustomerId()) {
            $orderCount = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('customer_id', $order->getCustomerId())
                ->addFieldToFilter('status', array('nin' => $excludedStatus))
                ->addFieldToFilter('entity_id', array('neq' => $order->getId()))
                ->getSize();
            $orderCount= '<b style="color: red">'.$orderCount.'</b>';
            $this->addTab('orders', array(
                'label' => self::OTHER_ORDERS_TAB_NAME. ' (' . $orderCount . ')',
                'class' => 'ajax',
                'url' => $this->getUrl('adminhtml/customer/orders', array('_current' => true, 'id' => $order->getCustomerId())),
            ));
        }

        if($order->getOrderType() != 2) { //disable thia tab if this is repair and exchange order
            $this->addTab('order_item', array(
                'label' => Mage::helper('opentechiz_salesextend')->__('Actual Reserved Stock Items'),
                'title' => Mage::helper('opentechiz_salesextend')->__('Actual Reserved Stock Items'),
                'content' => $this->getLayout()->createBlock('opentechiz_salesextend/adminhtml_sales_order_view_actualOrderItem')->toHtml(),
                'active' => false
            ));
        }
        $this->addTab('order_status_history', array(
            'label' => Mage::helper('opentechiz_salesextend')->__('Stage & Status Change History'),
            'title' => Mage::helper('opentechiz_salesextend')->__('Stage & Status Change History'),
            'content' => $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_logOrderStatus_grid')->toHtml(),
            'active' => false
        ));
        $this->addTab('actual_payment_record', array(
            'label' => Mage::helper('opentechiz_salesextend')->__('Actual payment records'),
            'title' => Mage::helper('opentechiz_salesextend')->__('Actual payment records'),
            'content' => $this->getLayout()->createBlock('opentechiz_salesextend/adminhtml_sales_order_view_payment_grid')->toHtml(),
            'active' => false
        ));
        if($this->getOrder()->getSource() == 0){
            $this->removeTab('payment_tracker');
        }

        return parent::_beforeToHtml();
    }

    public function getTabLabel($tab)
    {
        if ($tab instanceof Mage_Adminhtml_Block_Widget_Tab_Interface) {
            if(strpos($tab->getTabLabel(), self::OTHER_ORDERS_TAB_NAME) !== false){
                return $tab->getTabLabel();
            }else {
                return $this->escapeHtml($tab->getTabLabel());
            }
        }
        if(strpos($tab->getLabel(), self::OTHER_ORDERS_TAB_NAME) !== false){
            return $tab->getLabel();
        }else {
            return $this->escapeHtml($tab->getLabel());
        }
    }
}