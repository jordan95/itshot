<?php

if (Mage::helper('core')->isModuleEnabled('MageWorx_OrdersEdit')) {

    class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_View_Pure extends MageWorx_OrdersEdit_Block_Adminhtml_Sales_Order_View
    {
        
    }

} else {

    class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_View_Pure extends Mage_Adminhtml_Block_Sales_Order_View
    {
        
    }

}

class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_View extends OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_View_Pure {

    const ORDER_STAGE_TO_DISPLAY_COST_CHECK_BUTTON = [1, 4];
    const ORDER_TYPE_TO_DISPLAY_COST_CHECK_BUTTON = [0, 1, 3];

    public function getOrder(){
        if(Mage::registry('current_order')){
            return Mage::registry('current_order');
        }else{
            $params = $this->getRequest()->getParams();
            $orderId = $params['order_id'];
            $order = Mage::getSingleton('sales/order')->load($orderId);
            Mage::register('current_order', $order);
            return $order;
        }
    }

    public function getOrderSignifydCase(){
        $incrementId = $this->getOrder()->getIncrementId();
        $case = Mage::getModel('signifyd_connect/case')->load($incrementId, 'order_increment');
        return $case;
    }

    public function getApproveUrl($order)
    {
        return $this->getUrl('*/approve/approved', array('order_ids' => $order));
    }

    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/' . $action);
    }

    public function _beforeToHtml()
    {
        parent::_beforeToHtml();
        $order = $this->getOrder();
        $orderId = $order->getId();

        if ($order->getOrderType() == 1) {
            $this->_removeButton('order_cancel');
        }

        if (Mage::getSingleton('adminhtml/session')->getBlockEditOrder() && !$order->getSurchargeId()) {
            $this->_removeButton('order_invoice');
        }

        //#2359 remove cancel and cm button on repair order
        if($order->getOrderType() == 2){
            //enable cancel button for repair #3283
//            $action = 'cancel';
//            if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/' . $action) && $order->canCancel()) {
//                $this->removeButton('order_cancel');
//            }
            $action = 'creditmemo';
            if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/' . $action) && $order->canCreditmemo()) {
                $this->removeButton('order_creditmemo');
            }
            $action = 'reorder';
            if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/' . $action)) {
                $this->removeButton('order_reorder');
            }
        }

        //check if order is skipping cost check
        $skippedCostCheck = false;
        foreach ($order->getAllItems() as $item){
            if($item->getSkipCostCheck() == 1){
                $skippedCostCheck = true;
            }
        }

        $checkCostAtShip = false;
        foreach ($order->getAllItems() as $item){
            if($item->getCheckCostAtShipping() == 1){
                $checkCostAtShip = true;
            }
        }

        if($order->getOrderStage() == 4) {
            $this->_addButton('showwroom_pickup', array(
                'label' => Mage::helper('opentechiz_salesextend')->__('Showroom Pickup'),
                'onclick' => 'if(window.confirm(\'Confirm ShowRoom Pickup\'))
                                    {
                                    setLocation(\'' . Mage::getUrl('*/order/createShowroomPickup/', array('order_id' => $orderId)) . '\')
                                    }',
            ), 1, 1);
        }

        $itemsNeedCostCheck = Mage::helper('opentechiz_salesextend')->checkProfitOrder($order); //not show button if item cost is ok #3123
        if($itemsNeedCostCheck && !$checkCostAtShip && in_array($order->getOrderStage(), self::ORDER_STAGE_TO_DISPLAY_COST_CHECK_BUTTON)
            && in_array($order->getOrderType(), self::ORDER_TYPE_TO_DISPLAY_COST_CHECK_BUTTON)) {
            if ($skippedCostCheck) {
                if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/skip_cost_check')) {
                    $this->_addButton('enableCostCheck', array(
                        'label' => Mage::helper('opentechiz_salesextend')->__('Enable items cost check'),
                        'onclick' => 'setLocation(\'' . Mage::getUrl('*/item/SkipCostCheck/', array('order_id' => $orderId, 'action' => 0)) . '\')',
                    ), 1, 11);
                }
            } else {
                if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/skip_cost_check')) {
                    $this->_addButton('skipCostCheck', array(
                        'label' => Mage::helper('opentechiz_salesextend')->__('Skip items cost check'),
                        'onclick' => 'setLocation(\'' . Mage::getUrl('*/item/SkipCostCheck/', array('order_id' => $orderId, 'action' => 1)) . '\')',
                    ), 1, 11);
                }
                if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/check_cost_at_shipping')
                        && $order->getOrderStage() == 1) { //only show in processing
                    $this->_addButton('checkCostAtShipping', array(
                        'label' => Mage::helper('opentechiz_salesextend')->__('Check Cost at shipping'),
                        'onclick' => 'setLocation(\'' . Mage::getUrl('*/checkCostAtShipping/checkCostAtShipping/', array('order_id' => $orderId, 'action' => 1)) . '\')',
                    ), 1, 12);
                }
            }
        }

        if($order->getOrderStage() == 5 && Mage::getSingleton('admin/session')->isAllowed('sales/return')) {
            $this->_addButton('return_btn', array(
                'label' => Mage::helper('opentechiz_salesextend')->__('Add Rma'),
                'onclick' => 'setLocation(\'' . Mage::getUrl('*/sales_returns/create/', array('order_id' => $orderId)) . '\')',
            ), 1, 3);
        }

        $customer_id = $order->getCustomerId();
        $customer = Mage::getModel('customer/customer')->load($customer_id);
        $isFraudster = $customer->getIsFraudster();
        $allowManagementApproval = Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/management_approve');

        if($isFraudster && $allowManagementApproval && $order->getOrderStage() == 1
            && !$order->getManagementApproved()){
            $this->_addButton('management_approve', array(
                'label' => Mage::helper('opentechiz_salesextend')->__('Management Approve'),
                'onclick' => 'if(window.confirm(\'Are you sure you want to approve this order?\'))
                                    {
                                        setLocation(\'' . Mage::getUrl('*/order/managementApprove/', array('order_id' => $orderId)) . '\')
                                    }'
            ), 1, 4);
        }
    }
}