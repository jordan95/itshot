<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_Edit_Form_Items_Grid_Renderer_Price extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Price
{
    /**
     * Render minimal price for downloadable products
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        if ($row->getTypeId() == 'downloadable') {
            $row->setPrice($row->getPrice());
        }
        if($row->getSpecialPrice()==NULL){
            $row->setSpecialPrice($row->getPrice());
        }
        return parent::render($row);
    }

}
