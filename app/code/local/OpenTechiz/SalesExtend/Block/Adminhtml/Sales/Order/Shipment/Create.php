<?php
class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_Shipment_Create extends Mage_Adminhtml_Block_Sales_Items_Abstract
{
    /**
     * Retrieve invoice order
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return $this->getShipment()->getOrder();
    }

    /**
     * Retrieve source
     *
     * @return Mage_Sales_Model_Order_Invoice
     */
    public function getSource()
    {
        return $this->getShipment();
    }

    /**
     * Retrieve shipment model instance
     *
     * @return Mage_Sales_Model_Order_Shipment
     */
    public function getShipment()
    {
        return Mage::registry('current_shipment');
    }

    /**
     * Prepare child blocks
     */
    protected function _beforeToHtml()
    {
        $skus = Mage::helper('opentechiz_salesextend')->checkProfitOrder($this->getOrder());
        $orderType = $this->getOrder()->getOrderType();
        if($skus != '' && $orderType != 2){
            $this->setChild(
                'submit_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')->setData(array(
                    'label'     => Mage::helper('sales')->__('Submit Shipment'),
                    'class'     => 'save submit-button',
                    'onclick'   => 'alert(\'These item(s) has insufficient profit compare to Last Cost: '.$skus.'\nPlease check the order again. You can skip cost check in order items view\')',
                ))
            );
        }elseif ($this->getOrder()->getOrderStage() < 4){
            $this->setChild(
                'submit_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')->setData(array(
                    'label'     => Mage::helper('sales')->__('Submit Shipment'),
                    'class'     => 'save submit-button',
                    'onclick'   => 'alert(\'This order is not yet in Prep Shipment stage. \nCannot create shipment\')',
                ))
            );
        }else {
            $this->setChild(
                'submit_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')->setData(array(
                    'label'     => Mage::helper('sales')->__('Submit Shipment'),
                    'class'     => 'save submit-button',
                    'onclick'   => 'submitShipment(this);',
                ))
            );
        }
        return parent::_beforeToHtml();
    }

    /**
     * Format given price
     *
     * @param float $price
     * @return string
     */
    public function formatPrice($price)
    {
        return $this->getShipment()->getOrder()->formatPrice($price);
    }

    /**
     * Retrieve HTML of update button
     *
     * @return string
     */
    public function getUpdateButtonHtml()
    {
        return $this->getChildHtml('update_button');
    }

    /**
     * Get url for update
     *
     * @return string
     */
    public function getUpdateUrl()
    {
        return $this->getUrl('*/*/updateQty', array('order_id'=>$this->getShipment()->getOrderId()));
    }

    /**
     * Check possibility to send shipment email
     *
     * @return bool
     */
    public function canSendShipmentEmail()
    {
        return Mage::helper('sales')->canSendNewShipmentEmail($this->getOrder()->getStore()->getId());
    }

    /**
     * Checks the possibility of creating shipping label by current carrier
     *
     * @return bool
     */
    public function canCreateShippingLabel()
    {
        $shippingCarrier = $this->getOrder()->getShippingCarrier();
        return $shippingCarrier && $shippingCarrier->isShippingLabelsAvailable();
    }
}