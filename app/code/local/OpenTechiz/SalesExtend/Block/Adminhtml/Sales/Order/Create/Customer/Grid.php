<?php
class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_Create_Customer_Grid extends
    Mage_Adminhtml_Block_Sales_Order_Create_Customer_Grid
{
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumnAfter('is_fraudster', array(
            'header'    =>Mage::helper('sales')->__('Is Fraudster'),
            'index'     =>'is_fraudster',
            'type'      =>'options',
            'options'   =>array(0 => 'No', 1 => 'Yes'),
            'width'     =>'30px',
            'sortable'  =>false,
            'filter_condition_callback' => array($this, '_fraudsterFilter')
        ), 'name');

        return $this;
    }

    public function _fraudsterFilter($collection, $column){
        $value = $column->getFilter()->getValue();

        $collection->getSelect()->where('is_fraudster = ' . $value);
    }
}