<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_View_Payment_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct() {
        parent::__construct();
        $this->setId('order_paymentgrid_grid');
        $this->setDefaultSort('payment_grid_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $order_id = $this->getRequest()->getParam('order_id');
        $table_sales_invoice = Mage::getSingleton('core/resource')->getTableName('sales/invoice');
        $table_sales_creditmemo = Mage::getSingleton('core/resource')->getTableName('sales/creditmemo');
        $table_sales_order = Mage::getSingleton('core/resource')->getTableName('sales/order');
        $collection = Mage::getResourceModel("cod/paymentgrid_collection");
        if(!empty($order_id)){
            $collection->getSelect()->joinLeft( array('invoice'=> $table_sales_invoice),
                            'invoice.entity_id = main_table.invoice_id', array('invoice.order_id'))
                ->joinLeft( array('creditmemo'=> $table_sales_creditmemo),
                    'creditmemo.entity_id = main_table.credit_memo_id', array('creditmemo.order_id', 'credit_memo_id_increment' => 'creditmemo.increment_id'))
                ->where("invoice.order_id = ".$order_id." OR creditmemo.order_id = ".$order_id);
        }else{
            $collection->getSelect()->joinLeft( array('invoice'=> $table_sales_invoice),
                'invoice.entity_id = main_table.invoice_id', array('invoice.order_id'))
                ->joinLeft( array('creditmemo'=> $table_sales_creditmemo),
                    'creditmemo.entity_id = main_table.credit_memo_id', array('creditmemo.order_id'));
        }
                    
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('payment_grid_id', array(
            'header' => Mage::helper('cod')->__('Payment #'),
            'index' => 'payment_grid_id',
            'type' => 'text',
        ));

        $this->addColumn('number', array(
            'header' => Mage::helper('cod')->__('Note'),
            'index' => 'number',
            'type' => 'text',
        ));

        $this->addColumn('increment_id', array(
            'header' => Mage::helper('cod')->__('Invoice #'),
            'index' => 'increment_id',
            'type' => 'text',
            'filter_index'=>'main_table.increment_id'
        ));

        $this->addColumn('credit_memo_id', array(
            'header' => Mage::helper('cod')->__('Credit Memo #'),
            'index' => 'credit_memo_id_increment',
            'type' => 'text',
            'filter_index'=>'credit_memo_id_increment'
        ));


        $this->addColumn('payment_id', array(
            'header' => Mage::helper('cod')->__('Payment Method'),
            'index' => 'payment_id',
            'type' => 'options',
            'options' => Mage::getModel('cod/paymentgrid')->getPayType(),
        ));

        $this->addColumn('total', array(
            'header' => Mage::helper('cod')->__('Total'),
            'index' => 'total',
            'type' => 'currency',
            'align' => 'right',
            'currency_code' => Mage::app()->getStore()->getBaseCurrencyCode()
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('cod')->__('Customer'),
            'index' => 'billing_name',
            'type' => 'text',
        ));
       
       
        $this->addColumn('email', array(
            'header' => Mage::helper('cod')->__('Email'),
            'index' => 'email',
            'type' => 'text',
        ));

        $this->addColumn('received_date', array(
            'header' => Mage::helper('cod')->__('Received Date'),
            'index' => 'received_date',
            'type' => 'date',
        ));

        $this->addColumn('created_time', array(
            'header' => Mage::helper('cod')->__('Created Date'),
            'index' => 'created_time',
            'type' => 'datetime',
        ));

        $this->addColumn('update_time', array(
            'header' => Mage::helper('cod')->__('Updated Date'),
            'index' => 'update_time',
            'type' => 'datetime',
        ));



//        $this->addExportType('*/*/exportCsv', Mage::helper('web')->__('CSV'));
//        $this->addExportType('*/*/exportXml', Mage::helper('web')->__('XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        //return $this->getUrl('*/*/view', array('id' => $row->getId()));
        return '#';
    }
}