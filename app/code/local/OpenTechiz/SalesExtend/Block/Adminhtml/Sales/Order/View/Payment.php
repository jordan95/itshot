<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_Sales_Order_View_Payment extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_payment';
        $this->_blockGroup = 'opentechiz_salesextend';
        $this->_headerText = Mage::helper('opentechiz_reportExtend')->__('Actual payment records');
        parent::__construct();
        $this->removeButton('add');
    }
}