<?php
class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_Create_Form extends Mage_Adminhtml_Block_Sales_Order_Create_Form
{
    /**
     * Retrieve url for loading blocks
     * @return string
     */
    public function getLoadBlockUrl()
    {
        $params = $this->getRequest()->getParams();
        if(isset($params['type']) && $params['type'] == OpenTechiz_SalesExtend_Helper_Data::ORDER_TYPE_LAYAWAY){
            return $this->getUrl('*/*/loadBlock', array('type' => OpenTechiz_SalesExtend_Helper_Data::ORDER_TYPE_LAYAWAY));
        }

        return $this->getUrl('*/*/loadBlock');
    }
}