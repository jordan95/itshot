<?php
class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_Shipment_Packaging_Grid
    extends Mage_Adminhtml_Block_Sales_Order_Shipment_Packaging_Grid
{
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('opentechiz/shipping/grid.phtml');
    }
}