<?php
class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_View_OtherOrders extends Mage_Adminhtml_Block_Customer_Edit_Tab_Orders
{
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('sales/order_grid_collection')
            ->addFieldToSelect('entity_id')
            ->addFieldToSelect('status')
            ->addFieldToSelect('increment_id')
            ->addFieldToSelect('customer_id')
            ->addFieldToSelect('created_at')
            ->addFieldToSelect('grand_total')
            ->addFieldToSelect('order_currency_code')
            ->addFieldToSelect('store_id')
            ->addFieldToSelect('billing_name')
            ->addFieldToSelect('shipping_name')
            ->addFieldToFilter('customer_id', Mage::registry('current_customer')->getId())
            ->addFieldToSelect('order_stage')
            ->addFieldToSelect('internal_status')
            ->addFieldToSelect('order_type')
            ->setIsCustomerMode(true);
        $collection->getSelect()->joinLeft(array('order'=>'tsht_sales_flat_order'), 'order.entity_id = main_table.entity_id', array('status_grid' => 'main_table.status'))->joinLeft(array('order_item'=>'tsht_sales_flat_order_item'), 'order.entity_id = order_item.order_id', array('sum_return_qty' => 'sum(order_item.qty_returned)'));
        if($order_id = $this->getRequest()->getParam('order_id')){
            $collection->addFieldToFilter('main_table.entity_id', array('neq' => $order_id));
            $collection->addFieldToFilter('main_table.status', array('nin' => OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_View_Tabs::EXCLUDED_STATUSES));
        }
        $collection->getSelect()->group("main_table.entity_id");
        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }
    protected function _prepareColumns()
    {
        $this->addColumnAfter('status', array(
            'header'    => $this->__('Status'),
            'width'     => '100',
            'type'      => 'text',
            'index' => 'status_grid',
            'filter_index'     => 'main_table.status',
            'renderer'  => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Status'
        ),'increment_id');
        $this->addColumnAfter('order_stage', array(
            'header'    => Mage::helper('customer')->__('Stage'),
            'align'     => 'left',
            'index'     => 'order_stage',
            'filter'    => false,
            'width'     => '200px',
            'renderer'  => 'OpenTechiz_Customer_Block_Adminhtml_Sales_Renderer_Stage'
        ), 'status');
        parent::_prepareColumns();
        $this->addColumn('increment_id', array(
            'header'    => Mage::helper('customer')->__('Order #'),
            'width'     => '100',
            'index'     => 'increment_id',
            'filter_index'     => 'main_table.increment_id',
            'renderer'  => 'OpenTechiz_SalesExtend_Block_Adminhtml_Customer_View_Order_Renderer_OrderNumber'
        ));
        $this->addColumn('created_at', array(
            'header'    => Mage::helper('customer')->__('Purchase On'),
            'index'     => 'created_at',
            'filter_index'     => 'main_table.created_at',
            'type'      => 'datetime',
        ));
        $this->addColumn('grand_total', array(
            'header'    => Mage::helper('customer')->__('Order Total'),
            'index'     => 'grand_total',
            'filter_index'     => 'main_table.grand_total',
            'type'      => 'currency',
            'currency'  => 'order_currency_code',
        ));
        return $this;
    }
}