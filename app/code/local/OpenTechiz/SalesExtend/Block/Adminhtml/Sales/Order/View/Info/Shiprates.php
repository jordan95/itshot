<?php
class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_View_Info_Shiprates extends Mage_Adminhtml_Block_Sales_Order_Create_Shipping_Method_Form
{
    /**
     * Prepare layout for shipping method form
     *
     * @return $this
     */

    public function getOrder() {
        if (is_null($this->order)) {
            if (Mage::registry('current_order')) {
                $order = Mage::registry('current_order');
            }
            elseif (Mage::registry('order')) {
                $order = Mage::registry('order');
            }
            elseif (array_key_exists('order_id', $this->getRequest()->getParams())){
                $orderId = $this->getRequest()->getParam('order_id');
                $order = Mage::getModel('sales/order')->load($orderId);
            }
            else {
                $order = new Varien_Object();
            }
            $this->order = $order;
        }
        return $this->order;
    }

    public function getUpdateOrderUrl(){
        $order_id = $this->getOrder()->getId();
        return $this->getUrl('adminhtml/order/changeShippingMethod', array('order_id' => $order_id));
    }

    /**
     * Retrieve current selected shipping method
     *
     * @return string
     */
    public function getShippingMethod()
    {
        return $this->getOrder()->getShippingMethod();
    }

    /**
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if ($this->getData('quote')) {
            return $this->getData('quote');
        }

        /** @var Mage_Sales_model_Order $order */
        $order = $this->getOrder() ;
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = Mage::getSingleton('mageworx_ordersedit/edit')->getQuoteByOrder($order);

        return $quote;
    }

    public function getShippingRates()
    {
//        if (Mage::helper('mageworx_ordersedit')->isShippingCostRecalculationEnabled()) {
        $quote= $this->getQuote();
        $address = $quote->getShippingAddress();
        $address->setCollectShippingRates(true);
        $this->_rates = $address->collectShippingRates()->getGroupedAllShippingRates();
        $address->save();

        return $this->_rates;
//        }
//        return parent::getShippingRates();
    }

    public function getActiveMethodRate($rateGroup = null)
    {
        if(!$rateGroup){
            return parent::getActiveMethodRate();
        }else {
            $rates = $rateGroup;
            if (is_array($rates)) {
                foreach ($rates as $group) {
                    foreach ($group as $code => $rate) {
                        if ($rate->getCode() == $this->getShippingMethod()) {
                            return $rate;
                        }
                    }
                }
            }
            return false;
        }
    }

    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/' . $action);
    }
    public function isEditable(){
        $order = $this->getOrder() ;
        if ($this->_isAllowedAction('ship') && $order->canShip()
            && !$order->getForcedDoShipmentWithInvoice()) {
            return true;
        }
        return false;
    }

}
