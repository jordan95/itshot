<?php
/**
 * Opentechiz
 * Admin Order Editor extension
 *
 * @category   Opentechiz
 * @package    OpenTechiz_SalesExtend
 * @copyright  Copyright (c) 2016 MageWorx (http://www.mageworx.com/)
 */

class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_Edit_Form_Items_Grid extends MageWorx_OrdersEdit_Block_Adminhtml_Sales_Order_Edit_Form_Items_Grid
{
    protected function _prepareColumns()
    {
        
        $this->addColumnAfter('special_price', array(
            'header'    => Mage::helper('sales')->__('Price'),
            'column_css_class' => 'price',
            'align'     => 'center',
            'type'      => 'currency',
            'currency_code' => $this->getStore()->getCurrentCurrencyCode(),
            'rate'      => $this->getStore()->getBaseCurrency()->getRate($this->getStore()->getCurrentCurrencyCode()),
            'index'     => 'special_price',
            'renderer'  => 'opentechiz_salesextend/adminhtml_sales_order_edit_form_items_grid_renderer_price'
        ),'sku');
        parent::_prepareColumns();
        $this->removeColumn('price');

        return $this;
    }

    protected function _prepareCollection()
    {
        $order = $this->getOrder();

        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection
            ->setStore($this->getStore())
            ->addAttributeToSelect($attributes)
//            ->addAttributeToFilter('status', 1)
            ->addAttributeToSelect('sku')
            ->addStoreFilter()
            ->addAttributeToFilter('type_id', array_keys(
                Mage::getConfig()->getNode('adminhtml/sales/order/create/available_product_types')->asArray()
            ))
            ->addAttributeToSelect('gift_message_available');
        //only allow to add return fee for repair order
        if($order && $order->getOrderType() == 2){
            $collection->addFieldToFilter('sku', OpenTechiz_Return_Helper_Data::RETURN_FEE_ITEM_SKU);
        }
        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($collection);

        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }
}