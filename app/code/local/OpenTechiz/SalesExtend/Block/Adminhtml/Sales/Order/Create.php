<?php
class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_Create extends Mage_Adminhtml_Block_Sales_Order_Create
{
    public function getCancelUrl()
    {
        if ($this->_getSession()->getOrder()->getId()) {
            $url = $this->getUrl('*/sales_order/view', array(
                'order_id' => Mage::getSingleton('adminhtml/session_quote')->getOrder()->getId()
            ));
        } else {
            $url = $this->getUrl('*/*/cancel', array('type' => $this->getRequest()->getParam('type')));
        }

        return $url;
    }
}