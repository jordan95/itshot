<?php
class OpenTechiz_SalesExtend_Block_Adminhtml_Sales_Order_View_ActualOrderItem extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_sales_order_view';
    protected $_blockGroup = 'opentechiz_salesextend';
    protected $_headerText = 'Actual Order Item';
    const ITEM_RESERVED_STATUS_NUMBERS = [10, 15, 20, 25, 26];

    public function __construct()
    {
        parent::__construct();
    }

    public function _prepareLayout()
    {
        $this->setTemplate('opentechiz/sales_extend/actual_order_item.phtml');
    }

    public function getAllOrderItems(){
        $orderId = Mage::app()->getRequest()->getParam('order_id');
        $allItems = [];
        $orderItemCollection = Mage::getModel('sales/order_item')->getCollection()->addFieldToFilter('order_id', $orderId);
        foreach ($orderItemCollection as $orderItem){
            if($orderItem->getProductType() == 'virtual') continue;
            
            $itemReservedCollection = Mage::getModel('opentechiz_inventory/order')->getCollection()
                ->addFieldToFilter('order_item_id', $orderItem->getId())
                ->join(array('item' => 'opentechiz_inventory/item'), 'item.item_id = main_table.item_id', '*');
            foreach ($itemReservedCollection as $reservedItem){
                $orderItem->setData('barcode', $reservedItem->getBarcode())->setData('state', $reservedItem->getState());
                array_push($allItems, $orderItem->getData());
            }
        }
        return $allItems;
    }
}