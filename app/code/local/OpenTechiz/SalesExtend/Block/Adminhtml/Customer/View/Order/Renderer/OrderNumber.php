<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Customer_View_Order_Renderer_OrderNumber extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
         $value  = $row->getData('state');
         $sum_return_qty  = (int)$row->getData('sum_return_qty');
         $order_number  = $row->getData('increment_id');
         if($sum_return_qty > 0){
            echo "<span style='background:grey;padding:2px 5px;margin-right:5px;color:#fff;'>R</span>";
         }
         return $order_number;
    }

}