<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2018 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Customer edit block
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class OpenTechiz_SalesExtend_Block_Adminhtml_Customer_Edit extends TBT_Rewards_Block_Manage_Customer_Edit
{
    public function getHeaderText()
    {
        if (Mage::registry('current_customer')->getId()) {
            if(Mage::registry('current_customer')->getIsFraudster()){
                return $this->escapeHtml(Mage::registry('current_customer')->getName() . ' (Fraudster)');
            }else {
                return $this->escapeHtml(Mage::registry('current_customer')->getName());
            }
        } else {
            return Mage::helper('customer')->__('New Customer');
        }
    }

    public function __construct()
    {
        parent::__construct();

        if(Mage::registry('current_customer')->getIsFraudster()){
            $this->_addButton('fraud', array(
                'label' => Mage::helper('customer')->__('Unmark as Fraudster'),
                'onclick' => 'setLocation(\'' . $this->getUnmarkFraudUrl() . '\')',
                'class' => 'fraud',
            ), 0);
        }else {
            $this->_addButton('fraud', array(
                'label' => Mage::helper('customer')->__('Mark as Fraudster'),
                'onclick' => 'setLocation(\'' . $this->getMarkFraudUrl() . '\')',
                'class' => 'fraud',
            ), 0);
        }
    }

    public function getMarkFraudUrl()
    {
        return $this->getUrl('adminhtml/fraudster/markFraud', array('customer_id' => $this->getCustomerId()));
    }

    public function getUnmarkFraudUrl()
    {
        return $this->getUrl('adminhtml/fraudster/unmarkFraud', array('customer_id' => $this->getCustomerId()));
    }
}