<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Item extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_item';
        $this->_blockGroup = 'opentechiz_salesextend';
        $this->_headerText = Mage::helper('opentechiz_salesextend')->__('Order Item Grid');
        parent::__construct();
        $this->_removeButton('add');
    }

    public function getHeaderCssClass()
    {
        return 'icon-head head-warehouse-stone';
    }
}