<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Fraudster extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $fraudster =  $row->getData('is_fraudster');
        if($fraudster == "1"){
            return "<div><span style='background:red;padding:2px 5px;color:#fff;'>Yes</span></div>";
        }
        return "No";

    }

}