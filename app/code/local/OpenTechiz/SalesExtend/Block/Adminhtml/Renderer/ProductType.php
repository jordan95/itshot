<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_ProductType extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $sku = $row->getData('sku');
        $sku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];

        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        if($product) {
            if (Mage::helper('personalizedproduct')->isPersonalizedProduct($product)) {
                return 'Personalized';
            } else {
                return 'Non-Personalized';
            }
        }else{
            return '';
        }
    }
}