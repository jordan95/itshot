<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Available extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $orderSku = $row->getData('sku');
        $count = 0;
        $results = Mage::helper('opentechiz_inventory')->getAvailableSize($orderSku);
        foreach ($results as $key => $value) {
            $count += (int)$value['qty'] - (int)$value['on_hold'];
        }
        return (string)$count;
    }
}