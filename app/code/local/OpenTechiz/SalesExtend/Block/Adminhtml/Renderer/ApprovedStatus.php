<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_ApprovedStatus extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $orderId =  $row->getData('entity_id');
        if(!Mage::registry('orderModel')) Mage::register('orderModel', Mage::getModel('sales/order'));
        $orderModel = Mage::registry('orderModel')->load($orderId);
        switch ($orderModel->getOrderApproved()){
            case 0: $name = 'No';break;
            case 1: $name = 'Yes';break;
            default: $name = $orderModel->getOrderApproved();break;
        }

        $html = $name;
        return $html;
    }
}