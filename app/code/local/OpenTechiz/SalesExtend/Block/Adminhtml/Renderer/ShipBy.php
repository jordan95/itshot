<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_ShipBy extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $orderId =  $row->getData('entity_id');
        $id = 'shipby-'.$orderId;
        $html = '<input name="date" id="shipby-'.$orderId.'" value="'.explode(' ', $row->getData('ship_by'))[0].'" title="From" type="text" class="input-text ship-'.$orderId.'" style="width:90px !important;">
                            <img src="'.$this->getSkinUrl('images/grid-cal.gif') .'" class="v-middle" id="shipby_trig_'.$orderId.'" />
                            <script type="text/javascript">
                                //<![CDATA[
                                Calendar.setup({
                                    inputField: "shipby-'.$orderId.'",
                                    ifFormat: "%Y-%m-%d",
                                    showsTime: false,
                                    button: "shipby_trig_'.$orderId.'",
                                    align: "Bl",
                                    singleClick : true
                                });
                                //]]>
                            </script>';

        $html .= '<img src="'.Mage::getBaseUrl('skin').'adminhtml/default/default/opentechiz/image/tick.png" onclick="changeShipBy('. $orderId .')" width=15px height=15px style=" margin-top:5px">';

        $html .= '<script type="text/javascript" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS).'/opentechiz/jquery-3.3.1.min.js">jQuery.noConflict();</script>
                  <script>
                        function changeShipBy(id){
                            var order_id = id;
                            var date = document.getElementsByClassName("ship-"+id)[0].value;

                            jQuery.ajax({
                                url: "'. Mage::helper('adminhtml')->getUrl('adminhtml/order/changeShipBy') .'",
                                type: "post",
                                dataType: "json",
                                data: { order_id: order_id, date: date },
                                beforeSend:function() {
                                    Element.show("loading-mask");
                                },
                                complete:function() {
                                    Element.hide("loading-mask");
                                },
                                success: function(transport){
                                    var data = transport;
                                    if(data["error"] == true){
                                        alert("Something went wrong");
                                    } else {
                                        alert("Updated");
                                    }
                                }
                            })
                        }
                </script>';
        return $html;
    }

    public function renderExport(Varien_Object $row) {
        $orderId =  $row->getData('entity_id');
        if(!Mage::registry('orderModel')) Mage::register('orderModel', Mage::getModel('sales/order'));
        $order = Mage::registry('orderModel')->load($orderId);

        return $order->getShipBy();
    }
}