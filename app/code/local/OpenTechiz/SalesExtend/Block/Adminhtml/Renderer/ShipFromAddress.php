<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_ShipFromAddress extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $line1 = $row->getData('address_line_1');
        $line2 = $row->getData('address_line_2');
        $html = $line1.'<br>'.$line2;

        return $html;
    }
}