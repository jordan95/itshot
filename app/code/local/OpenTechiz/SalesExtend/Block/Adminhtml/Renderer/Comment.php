<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Comment extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $comment = $row->getData('comment');
        if(!empty($comment)){
        	$comment = strip_tags($comment);
        	if(strlen($comment) >150){
        		$comment = substr($comment,0,150)."...";
        	}
        	
        }
        
        return "<span style='color:red'>".$comment."</span>";
    }
}