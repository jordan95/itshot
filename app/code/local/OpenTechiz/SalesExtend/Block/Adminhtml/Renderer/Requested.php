<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Requested extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $orderItemId = $row->getData('item_id');
        $item = Mage::getModel('sales/order_item')->load($orderItemId);
        
        $qty_invoiced = $item->getQtyInvoiced();
        $qty_ordered = $item->getQtyOrdered();
        $remaining_qty = $item->getRemainingQty();
        if($item->getIsRequestedToProcess() == 1){
            $count = $item->getQtyOrdered() - $item->getRemainingQty();
            $qty_available_request = $item->getRemainingQty() - $item->getQtyOrdered() + $item->getQtyInvoiced();
        }
        else{
            $count = 0;
            $qty_available_request = $item->getQtyInvoiced();
        }
        
        $row->setData('qty_available_request',$qty_available_request);
        return (string)$count;
    }
}