<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Term extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
//        $orderId =  $row->getData('entity_id');
//        if(!Mage::registry('orderModel')) Mage::register('orderModel', Mage::getModel('sales/order'));
//        $orderModel = Mage::registry('orderModel')->load($orderId);

        return OpenTechiz_SalesExtend_Helper_Data::TERM[$row->getTerm()];
    }
}