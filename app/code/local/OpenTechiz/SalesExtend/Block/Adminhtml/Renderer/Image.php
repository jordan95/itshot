<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $data = $row->getData('product_options');
        if($data && $data != '' && isset($data['options']) && (count($data['options']) > 0)) {
            $options = $row->getData('product_options');
            $originalSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $row->getData('sku'))[0];
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $originalSku);
            if($product && $product->getId()) {
                $product = Mage::getModel('catalog/product')->load($product->getId());
                $productUrl = Mage::helper('adminhtml')->getUrl('adminhtml/catalog_product/edit', array('id' => $product->getId()));

                $product->setSku($row->getData('sku'));
                $options = unserialize($options);
                if(isset($options['info_buyRequest']) && array_key_exists('options', $options['info_buyRequest'])) {
                    if (array_key_exists('quotation_product_id', $options['info_buyRequest']['options'])) {
                        $quoteProductId = $options['info_buyRequest']['options']['quotation_product_id'];
                        $images = Mage::getModel('opentechiz_quotation/product')->load($quoteProductId)->getQpImage();
                        $image = trim(explode(',', $images)[0]);
                        $html = '<a href="' . $productUrl . '"><img src="' . $image . '" width="120px" height="120px"></a>';
                    } else {
                        $html = '<a href="' . $productUrl . '"><img src="' . Mage::helper('catalog/image')->init($product, 'thumbnail') . '" width="120px" height="120px"></a>';
                    }
                }else{
                    $html = '<a href="' . $productUrl . '"><img src="' . Mage::helper('catalog/image')->init($product, 'thumbnail') . '" width="120px" height="120px"></a>';
                }
            }else{
                $html = '';
            }
        } else{
            $originalSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $row->getData('sku'))[0];
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $originalSku);
            if($product && $product->getId()) {
                $product = Mage::getModel('catalog/product')->load($product->getId());
                $product->setSku($row->getData('sku'));
                $productUrl = Mage::helper('adminhtml')->getUrl('adminhtml/catalog_product/edit', array('id' => $product->getId()));
                $html = '<a href="' . $productUrl . '"><img src="' . Mage::helper('catalog/image')->init($product, 'thumbnail') . '" width="120px" height="120px"></a>';
            }else{
                $html = '';
            }
        }

        return $html;
    }
}