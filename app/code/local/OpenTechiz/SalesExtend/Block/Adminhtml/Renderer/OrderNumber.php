<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_OrderNumber extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
    	 $value  = $row->getData('state');
         $sum_return_qty  = (int)$row->getData('sum_return_qty');
         $order_number  = $row->getData('increment_id');
         if($sum_return_qty > 0){
            echo "<span style='background:grey;padding:2px 5px;margin-right:5px;color:#fff;'>R</span>";
         }
    	 if($value !="canceled" && $value !="closed" && $value !="holded" && $value !="cancelled" && $value !="complete"){
    	 	return "<span style='background:green;padding:2px 5px;margin-right:5px;color:#fff;'>A</span>".$order_number;
    	 }
    	 if($value =="holded"){
    	 	return "<span style='background:orange;padding:2px 5px;margin-right:5px;color:#fff;'>H</span>".$order_number;
    	 }
    	 if($value =="canceled" || $value =="closed" || $value =="cancelled"){
    	 	return "<span style='background:red;padding:2px 5px;margin-right:5px;color:#fff;'>C</span>".$order_number;
    	 }
         if($value =="complete"){
            return "<span style='background:blue;padding:2px 5px;margin-right:5px;color:#fff;'>O</span>".$order_number;
         }

         return $order_number;
    }

}