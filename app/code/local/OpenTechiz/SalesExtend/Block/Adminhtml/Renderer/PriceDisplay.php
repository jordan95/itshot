<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_PriceDisplay extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $price = $row->getData('price_incl_tax');
        $formatPrice = number_format((float)$price, 2, '.', ',');

        return $formatPrice;
    }
}