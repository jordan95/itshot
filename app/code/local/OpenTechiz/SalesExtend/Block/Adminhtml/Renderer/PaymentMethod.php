<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_PaymentMethod extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $orderId =  $row->getData('entity_id');
        $order = Mage::getModel('sales/order')->load($orderId);
        try {
            $paymentCode = $order->getPayment()->getMethodInstance()->getCode();
            $paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
            if($paymentCode == 'affirm'){
                $paymentTitle = 'Affirm';
            }
            return $paymentTitle;
        } catch (Exception $exc) {
            return "";
        }
    }
}