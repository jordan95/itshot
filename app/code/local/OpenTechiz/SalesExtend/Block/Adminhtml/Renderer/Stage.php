<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Stage extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $orderId =  $row->getData('entity_id');
        $id = 'stage-'.$orderId;
        $html = '<select name="date" id="'.$id.'" title="From" type="text" class="input-text stage-'.$orderId.'" style="width:90px !important;">';
        foreach (OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE as $value => $title ){
            $selected = '';
            if($value == $row->getData('order_stage')) $selected = 'selected';
            $html .= '<option value="'.$value.'" '.$selected.'>'.$title.'</option>';
        }
        $html .= '</select>';
        $html .= '<img src="'.Mage::getBaseUrl('skin').'adminhtml/default/default/opentechiz/image/tick.png" onclick="changeStage('. $orderId .')" width=15px height=15px style=" margin-top:5px">';
        $html .= '<script>
                        function changeStage(id){
                            var order_id = id;
                            var e = document.getElementById("stage-"+id);
                            var stage = e.options[e.selectedIndex].value;

                            jQuery.ajax({
                                url: "'. Mage::helper('adminhtml')->getUrl('adminhtml/order/changeStage') .'",
                                type: "post",
                                dataType: "json",
                                data: { order_id: order_id, stage: stage },
                                beforeSend:function() {
                                    Element.show("loading-mask");
                                },
                                complete:function() {
                                    Element.hide("loading-mask");
                                },
                                success: function(transport){
                                    var data = transport;
                                    if(data["error"] == true){
                                        alert("Something went wrong");
                                    } else {
                                        alert("Updated");
                                    }
                                }
                            })
                        }
                </script>';
        return $html;
    }

    public function renderExport(Varien_Object $row) {
        $orderId =  $row->getData('entity_id');
        if(!Mage::registry('orderModel')) Mage::register('orderModel', Mage::getModel('sales/order'));
        $order = Mage::registry('orderModel')->load($orderId);

        return $order->getOrderStage();
    }
}