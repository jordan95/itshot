<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_InternalStatus extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $status = OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS;
        if($row->getData('order_type') == 1){
            $status = OpenTechiz_SalesExtend_Helper_Data::LAYAWAY_ORDER_INTERNAL_STATUS;
        }
        if($row->getData('order_type') == 2){
            $status = OpenTechiz_SalesExtend_Helper_Data::REPAIR_ORDER_INTERNAL_STATUS;
        }
//        $orderId =  $row->getData('entity_id');
//        $id = 'internalStatus-'.$orderId;
//        $html = '<select name="date" id="'.$id.'" title="From" type="text" class="input-text internalStatus-'.$orderId.'" style="width:90px !important;">';
//        foreach ($status as $value => $title ){
//            $selected = '';
//            if($value == $row->getData('internal_status')) $selected = 'selected';
//            $html .= '<option value="'.$value.'" '.$selected.'>'.$title.'</option>';
//        }
//        $html .= '</select>';
//        $html .= '<img src="'.Mage::getBaseUrl('skin').'adminhtml/default/default/opentechiz/image/tick.png" onclick="changeInternalStatus('. $orderId .')" width=15px height=15px style=" margin-top:5px">';
//        $html .= '<script>
//                        function changeInternalStatus(id){
//                            var order_id = id;
//                            var e = document.getElementById("internalStatus-"+id);
//                            var status = e.options[e.selectedIndex].value;
//
//                            jQuery.ajax({
//                                url: "'. Mage::helper('adminhtml')->getUrl('adminhtml/order/changeInternalStatus') .'",
//                                type: "post",
//                                dataType: "json",
//                                data: { order_id: order_id, status: status },
//                                beforeSend:function() {
//                                    Element.show("loading-mask");
//                                },
//                                complete:function() {
//                                    Element.hide("loading-mask");
//                                },
//                                success: function(transport){
//                                    var data = transport;
//                                    if(data["error"] == true){
//                                        alert("Something went wrong");
//                                    } else {
//                                        alert("Updated");
//                                    }
//                                }
//                            })
//                        }
//                </script>';
//        return $html;
        $index =  $row->getData('internal_status');
        return isset($status[$index]) ? $status[$index] : "";
    }

    public function renderExport(Varien_Object $row) {
        $orderId =  $row->getData('entity_id');
        if(!Mage::registry('orderModel')) Mage::register('orderModel', Mage::getModel('sales/order'));
        $order = Mage::registry('orderModel')->load($orderId);

        return $order->getInternalStatus();
    }
}