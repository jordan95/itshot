<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_ShippingDescription extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $orderId =  $row->getData('entity_id');
        $order = Mage::getModel('sales/order')->load($orderId);
        if($order->getShippingDescription()) return $order->getShippingDescription();
        else return 'No shipping method for this order';
    }
}