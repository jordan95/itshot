<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value = $row->getData('status');
        $list_status = Mage::getSingleton('sales/order_config')->getStatuses();
        if(array_key_exists($value, $list_status)){
            $data = $list_status[$value];
            return $data;
        }
         return  $value;
    }

}