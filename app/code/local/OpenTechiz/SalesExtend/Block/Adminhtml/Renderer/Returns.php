<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Returns extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        if (!$orderId =  $row->getData('entity_id')) {
            return '';
        }
        /** @var OpenTechiz_Return_Model_Resource_Return_Collection $returns */
        $returns = Mage::getModel('opentechiz_return/return')->getCollection()
            ->join(
                ['order_item' => 'sales/order_item'],
                'main_table.order_item_id = order_item.item_id AND order_item.order_id = ' . $orderId,
                []
            );

        $html = [];
        /** @var OpenTechiz_Return_Model_Return $return */
        foreach ($returns as $return) {
            $url = Mage::getUrl('adminhtml/sales_returns/edit', array('id' => $return->getId()));
            $html[] = '<a href="' . $url . '">#' . $return->getIncrementId() . '</a>';
        }

        return implode(', ', $html);
    }
}
