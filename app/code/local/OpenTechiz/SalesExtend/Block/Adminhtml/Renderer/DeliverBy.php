<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_DeliverBy extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $orderId =  $row->getData('entity_id');

        $html = '<input name="date" id="deliverby-'.$orderId.'" value="'.explode(' ', $row->getData('deliver_by'))[0].'" title="From" type="text" class="input-text deliver-'.$orderId.'" style="width:90px !important;">
                            <img src="'.$this->getSkinUrl('images/grid-cal.gif') .'" class="v-middle" id="deliverby_trig_'.$orderId.'" />
                            <script type="text/javascript">
                                //<![CDATA[
                                Calendar.setup({
                                    inputField: "deliverby-'.$orderId.'",
                                    ifFormat: "%Y-%m-%d",
                                    showsTime: false,
                                    button: "deliverby_trig_'.$orderId.'",
                                    align: "Bl",
                                    singleClick : true
                                });
                                //]]>
                            </script>';

        $html .= '<img src="'.Mage::getBaseUrl('skin').'adminhtml/default/default/opentechiz/image/tick.png" onclick="changeDeliverBy('. $orderId .')" width=15px height=15px style=" margin-top:5px">';

        $html .= '<script type="text/javascript">jQuery.noConflict();</script>
                  <script>
                        function changeDeliverBy(id){
                            var order_id = id;
                            var date = document.getElementsByClassName("deliver-"+id)[0].value;

                            jQuery.ajax({
                                url: "'. Mage::helper('adminhtml')->getUrl('adminhtml/order/changeDeliverBy') .'",
                                type: "post",
                                dataType: "json",
                                data: { order_id: order_id, date: date },
                                beforeSend:function() {
                                    Element.show("loading-mask");
                                },
                                complete:function() {
                                    Element.hide("loading-mask");
                                },
                                success: function(transport){
                                    var data = transport;
                                    if(data["error"] == true){
                                        alert("Something went wrong");
                                    } else {
                                        alert("Updated");
                                    }
                                }
                            })
                        }
                </script>';
        return $html;
    }

    public function renderExport(Varien_Object $row) {
        $orderId =  $row->getData('entity_id');
        if(!Mage::registry('orderModel')) Mage::register('orderModel', Mage::getModel('sales/order'));
        $order = Mage::registry('orderModel')->load($orderId);

        return $order->getDeliverBy();
    }
}