<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_ShipFrom_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('shipFromGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }


    protected function _prepareCollection()
    {
        // Get the collection for the grid
        $collection = Mage::getModel('opentechiz_salesextend/shipFrom')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/index', array('_current'=>true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('source', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Order Source'),
            'width' => '120',
            'index' => 'source',
            'align' => 'center',
            'type' => 'options',
            'options' => OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Name'),
            'width' => '100',
            'index' => 'name',
            'align' => 'center'
        ));
        $this->addColumn('company', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Company'),
            'index' => 'company',
            'width' => '100',
        ));
        $this->addColumn('address', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Address'),
            'index' => 'address_line_1',
            'width' => '300',
            'renderer' => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_ShipFromAddress'
        ));
        $this->addColumn('city', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('City'),
            'index' => 'city',
            'width' => '50',
        ));
        $this->addColumn('state', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('State'),
            'index' => 'state',
            'width' => '100',
            'type' => 'options',
            'options' => $this->getStatesArray()
        ));

        $this->addColumn('zipcode', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Zip Code'),
            'index' => 'zipcode',
            'width' => '100',
        ));

        $this->addColumn('country', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Country'),
            'index' => 'country',
            'width' => '100',
            'type' => 'options',
            'options' => $this->getCountryArray()
        ));

        $this->addColumn('action',
            array(
                'header' => Mage::helper('opentechiz_production')->__('Action'),
                'width' => '50',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('opentechiz_production')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id',
                        'data-column' => 'action',
                    ),
                    array(
                        'caption' => Mage::helper('opentechiz_production')->__('Delete'),
                        'url' => array('base' => '*/*/delete'),
                        'field' => 'id',
                        'data-column' => 'action',
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
                'align' => 'center'
            )
        );
        return parent::_prepareColumns();
    }

//    protected function _prepareMassaction()
//    {
//        return $this;
//    }

    public function getStatesArray(){
        $array = Mage::getModel('directory/region_api')->items('US');
        $results = [];
        foreach ($array as $state){
            $results[$state['name']] = $state['name'];
        }
        return $results;
    }

    public function getCountryArray(){
        $countryList = Mage::getModel('directory/country')->getResourceCollection()
            ->loadByStore()
            ->toOptionArray(true);
        $results = [];
        foreach ($countryList as $country){
            $results[$country['value']] = $country['label'];
        }
        return $results;
    }
}