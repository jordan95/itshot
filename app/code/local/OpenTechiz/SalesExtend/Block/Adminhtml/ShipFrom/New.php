<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_ShipFrom_New extends OpenTechiz_SalesExtend_Block_Adminhtml_ShipFrom_Edit
{
    protected  $_controller = 'adminhtml_salesextend_shipFrom_New';
    protected $_headerText = 'New Ship From Address';
    protected $_currentStock = null;

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('New Ship From Address');

        return parent::_prepareLayout();
    }
}