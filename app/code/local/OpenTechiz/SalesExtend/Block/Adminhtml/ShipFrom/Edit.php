<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_ShipFrom_Edit extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_salesextend_shipFrom_Edit';
    protected $_blockGroup = 'opentechiz_salesextend';
    protected $_headerText = 'Edit Ship From Address';
    protected $_currentStock = null;

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Edit Ship From Address');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function _toHtml()
    {
        $this->setTemplate('opentechiz/sales_extend/shipfrom.phtml');

        return parent::_toHtml();
    }

    public function getFormAction(){
        return $this->getUrl('adminhtml/shipFrom/save');
    }

    public function getDeleteAction(){
        return $this->getUrl('adminhtml/shipFrom/delete');
    }

    public function getHelper(){
        return Mage::helper('opentechiz_salesextend');
    }

    public function getCurrentAddress(){
        $address = Mage::registry('shipFrom');
        if($address && $address->getId()){
            return $address;
        }else{
            return false;
        }
    }

    public function getPossibleSource(){
        $allSources = OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE;
        $collection = Mage::getModel('opentechiz_salesextend/shipFrom')->getCollection();
        $currentAddress = $this->getCurrentAddress();
        $sourceIds = [];
        foreach ($collection as $address){
            if($currentAddress && $currentAddress->getSource() == $address->getSource()){
                continue;
            }
            array_push($sourceIds, $address->getSource());
        }
        $result = [];
        foreach ($allSources as $key => $value){
            if(!in_array($key, $sourceIds)){
                $result[$key] = $value;
            }
        }
        return $result;
    }

    public function getStates(){
        $array = Mage::getModel('directory/region_api')->items('US');
        $results = [];
        foreach ($array as $state){
            $results[$state['name']] = $state['name'];
        }
        return $results;
    }
}