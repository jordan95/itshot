<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_Item_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('itemGrid');
        $this->setNoFilterMassactionColumn(true);
        $this->setDefaultSort('item_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(false);
    }


    protected function _prepareCollection()
    {
        // Get the collection for the grid
        $collection = Mage::getResourceModel('sales/order_item_collection');

        if($orderId = $this->getRequest()->getParams()['order_id']){
            $collection->addFieldToFilter('order_id', $orderId);
        } else {
            // Join the order table
            $collection->join('order', 'order.entity_id = main_table.order_id');
            $collection->join(array('order' => 'sales_flat_order'), 'order.entity_id = main_table.entity_id', array('order.shipping_description'))
                // Get the address
                ->join(array('a' => 'order_address'), 'order.entity_id = a.parent_id AND a.address_type  NOT LIKE \'billing\' ', array(
                    'street' => 'street',
                    'telephone' => 'telephone',
                    'created_at' => 'main_table.created_at',
                    'product_options' => 'main_table.product_options'
                ))
                // Concat customer name
                ->addExpressionFieldToSelect(
                    'customer_name',
                    'CONCAT({{customer_firstname}}, \' \', {{customer_lastname}})',
                    array('customer_firstname' => 'order.customer_firstname', 'customer_lastname' => 'order.customer_lastname'));
        }

        $collection->getSelect()->where("main_table.qty_ordered > main_table.qty_refunded AND main_table.sku != '".OpenTechiz_Return_Helper_Data::RETURN_FEE_ITEM_SKU."'");
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        if (!$orderId = $this->getRequest()->getParams()['order_id']) {
            $isInOrderView = false;
        }else{
            $isInOrderView = true;
        }

        $this->addColumn('image', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Thumbnail'),
            'width' => '120',
            'index' => 'product_options',
            'align' => 'center',
            'renderer' => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Image'
        ));
        $this->addColumn('item_id', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('ID'),
            'width' => '50',
            'index' => 'item_id',
            'align' => 'center'
        ));
        if (!$isInOrderView) {
            $this->addColumn('increment_id', array(
                'header' => Mage::helper('opentechiz_salesextend')->__('Order #'),
                'index' => 'increment_id',
                'width' => '50',
            ));
        }
        $this->addColumn('created_at', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Order Date'),
            'type' => 'datetime',
            'index' => 'created_at',
            'width' => '100',
            'filter_index' => 'main_table.created_at'
        ));
        if (!$isInOrderView) {
            $this->addColumn('customer_name', array(
                'header' => Mage::helper('opentechiz_salesextend')->__('Customer Name'),
                'index' => array('customer_firstname', 'customer_lastname'),
                'type' => 'concat',
                'separator' => ' ',
                'filter_index' => new Zend_Db_Expr("CONCAT(order.customer_firstname, ' ', order.customer_lastname)"),
                'width' => '140px',
            ));
        }
        if (!$isInOrderView) {
            $this->addColumn('street', array(
                'header' => Mage::helper('opentechiz_salesextend')->__('Str. Address'),
                'index' => 'street',
                'width' => '100'
            ));
        }
        if (!$isInOrderView) {
            $this->addColumn('telephone', array(
                'header' => Mage::helper('opentechiz_salesextend')->__('Phone'),
                'index' => 'telephone',
                'width' => '50'
            ));
        }
        if (!$isInOrderView) {
            $this->addColumn('customer_email', array(
                'header' => Mage::helper('opentechiz_salesextend')->__('Email'),
                'index' => 'customer_email',
                'width' => '50',
            ));
        }
        $this->addColumn('name', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Product Name'),
            'index' => 'name',
            'width' => '50',
        ));
        $this->addColumn('sku', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('SKU'),
            'index' => 'sku',
            'width' => '50',
        ));
        $this->addColumn('qty_ordered', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Qty Ordered'),
            'index' => 'qty_ordered',
            'text' => 'text',
            'width' => '50',
            'type' => 'number'
        ));

        if (!$isInOrderView) {
            $this->addColumn('shipping_method', array(
                'header' => Mage::helper('opentechiz_salesextend')->__('Shipping Method'),
                'index' => 'shipping_description',
                'width' => '50'
            ));
        }

        $this->addColumn('price_incl_tax', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Price (Incl. Tax)'),
            'index' => 'price_incl_tax',
            'width' => '50',
            'type' => 'currency',
            'currency' => 'base_currency_code',
            'renderer' => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_PriceDisplay'
        ));

        if (!$isInOrderView) {
            $this->addColumn('order_approved', array(
                'header' => Mage::helper('opentechiz_salesextend')->__('Approved'),
                'index' => 'order_approved',
                'type' => 'options',
                'sortable' => false,
                'width' => '50',
                'align' => 'center',
                'options' => array(
                    0 => "No",
                    1 => "Yes"
                )
            ));
        }
        if (!$isInOrderView) {
            $this->addColumn('status', array(
                'header' => Mage::helper('opentechiz_salesextend')->__('Status'),
                'width' => '80px',
                'index' => 'status',
                'align' => 'center',
                'sortable' => false,
                'type' => 'options',
                'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
            ));
        }
        if ($isInOrderView) {
            $this->addColumn('product_type', array(
                'header' => Mage::helper('opentechiz_salesextend')->__('Product Type'),
                'type' => 'options',
                'filter' => false,
                'sortable' => false,
                'width' => '80px',
                'align' => 'center',
                'renderer' => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_ProductType'
            ));
            $this->addColumn('available_qty', array(
                'header' => Mage::helper('opentechiz_salesextend')->__('Available Quantity in Stock'),
                'filter' => false,
                'sortable' => false,
                'width' => '100px',
                'renderer' => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Available'
            ));
            $this->addColumn('request_qty', array(
                'header' => Mage::helper('opentechiz_salesextend')->__('Requested Quantity'),
                'filter' => false,
                'sortable' => false,
                'width' => '100px',
                'renderer' => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Requested'
            ));
            $this->addColumn('skip_cost_check', array(
                'header' => Mage::helper('opentechiz_salesextend')->__('Is Skip Cost Check'),
                'width' => '100px',
                'index' => 'skip_cost_check',
                'align' => 'center',
                'type' => 'options',
                'options' => [ 0 => 'No', 1 => 'Yes'],
            ));
//            $this->addColumn('qty_available_request', array(
//                'header' => Mage::helper('opentechiz_salesextend')->__('Available Request Qty'),
//                'filter' => false,
//                'sortable' => false,
//                'width' => '100px',
//                'index' => 'qty_available_request'
//            ));

        }
        $this->addColumn('is_requested_to_process', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Is Requested To Process'),
            'index' => 'is_requested_to_process',
            'type' => 'options',
            'sortable' => false,
            'width' => '120px',
            'align' => 'center',
            'options' => array(
                0 => "No",
                1 => "Yes"
            )
        ));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('item_id');
        $this->getMassactionBlock()->setFormFieldName('item');
        $this->getMassactionBlock()->setUseSelectAll(false);

        //request to process
        $this->getMassactionBlock()->addItem('request_to_process', array(
            'label'=> Mage::helper('opentechiz_salesextend')->__('Request To Process'),
            'url'      => $this->getUrl('adminhtml/item/massRequestToProcess')
        ));

        //if is role is admin (id=1) enable skip cost check action
        $admin_user_session = Mage::getSingleton('admin/session');
        $adminuserId = $admin_user_session->getUser()->getUserId();
        $role_data = Mage::getModel('admin/user')->load($adminuserId)->getRole()->getData();

        if($role_data['role_id'] == 1 || $role_data['role_name'] == 'Administrators'){
            $this->getMassactionBlock()->addItem('skip_cost_check', array(
                'label'=> Mage::helper('opentechiz_salesextend')->__('Skip cost check'),
                'url'      => $this->getUrl('adminhtml/item/massSkipCostCheck'),
                'additional' => array(
                    'visibility' => array(
                        'name' => 'allow',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => Mage::helper('opentechiz_inventory')->__('Allow'),
                        'values' => [
                            0 => 'No',
                            1 => 'Yes'
                        ]
                    )
                )
            ));
        }

        //add non-personalized items stock request
//        $this->getMassactionBlock()->addItem('non_personalized_stock_request', array(
//            'label'=> Mage::helper('opentechiz_salesextend')->__('Add Non-Personalized Stock Request'),
//            'url'      => $this->getUrl('*/*/massAddNonPersonalizedStockRequest')
//        ));
        $this->getMassactionBlock()->removeItem('delete_order');
        return $this;
    }
}