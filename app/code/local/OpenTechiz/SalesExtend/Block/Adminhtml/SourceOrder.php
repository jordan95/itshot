<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_SourceOrder
    extends Mage_Adminhtml_Block_Template
{
    public function appendBlockHtmlToParent($ifConfigPath = null)
    {
        $layoutAppend = Mage::getModel('opentechiz_salesextend/orderSource')
            ->setParentBlock($this->getParentBlock())
            ->setIfConfig($ifConfigPath)
            ->add($this, 'before')
            ->append();
        
        return $this;
    }
    public function getUpdateOrderUrl(){
        $order_id = $this->getOrder()->getId();
        return $this->getUrl('adminhtml/order/changeOrderManagementData', array('order_id' => $order_id));
    }

    public function getOrder() {
        if (is_null($this->order)) {
            if (Mage::registry('current_order')) {
                $order = Mage::registry('current_order');
            }
            elseif (Mage::registry('order')) {
                $order = Mage::registry('order');
            }
            else {
                $orderId = $this->getRequest()->getParam('order_id');
                $order = Mage::getModel('sales/order')->load($orderId);
            }
            if(!$order){
                $order = new Varien_Object();
            }
            $this->order = $order;
        }
        return $this->order;
    }

    public function getOrderSignifydCase(){
        $incrementId = $this->getOrder()->getIncrementId();
        $case = Mage::getModel('signifyd_connect/case')->load($incrementId, 'order_increment');
        return $case;
    }

    protected function _toHtml()
    {
        return parent::_toHtml();
    }
    public function preventEditSource($state){
        $list_state = array("canceled","closed","complete","holded");
        if(in_array($state,$list_state)){
            return true;
        }
        return false;
    }
    public function showMessage($state){
        $check_state_message = array("canceled","closed","complete");
        if(strpos($state, 'holded') !== false) {
            $message = $this->__(" ( This order is On Hold, please remove Hold to do modifications )");
            return $message;
        }
        if(in_array($state,$check_state_message)){
            $message = $this->__(" ( This order is not active for further updates )");
            return $message;
        }
        return '';
    }
    public function getRelatedOrderLink($order){
        $data_link = array();
        $allItem = $order->getAllItems();
        $sales_order_item =  Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $link_related_return_order='';
        $link_related_repair_order='';
        $link_related_exchange_order='';
        $link_origin_order = '';
        $is_get_origin_order = 0;
        foreach($allItem as $item){

            $collection_return = Mage::getModel("opentechiz_return/return")->getCollection()->addFieldToFilter('order_item_id',array('eq'=>$item->getId()));
            $collection_return->getSelect()
                              ->joinLeft(array('sales_order_item' => $sales_order_item), "main_table.order_item_id = sales_order_item.item_id", array('order_id'=>'sales_order_item.order_id'));
            if($collection_return->count()>0){

                foreach($collection_return as $return){
                    
                    switch($return->getSolution()){
                      case 10:
                        $new_order = Mage::getModel('sales/order')->loadByIncrementId($return->getReturnFeeOrderId());
                        if($new_order){
                            $url = Mage::getModel("core/url")->getUrl("adminhtml/sales_order/view/", array("order_id" => $new_order->getId()));
                            $link_related_repair_order .= "<a href=\"{$url}\" target=\"__blank\">".$return->getReturnFeeOrderId()."</a>, ";
                            $data_link['repair'] = $link_related_repair_order;
                        }
                        $url = Mage::getModel("core/url")->getUrl("adminhtml/sales_returns/edit/", array("id" => $return->getId()));
                        $link_related_return_order .= "<a href=\"{$url}\" target=\"__blank\">".$return->getIncrementId()."</a>, ";
                        $data_link['return'] = $link_related_return_order;
                        break;
                      case 20:
                        $new_order = Mage::getModel('sales/order')->loadByIncrementId($return->getReturnFeeOrderId());
                        if($new_order){
                            $url = Mage::getModel("core/url")->getUrl("adminhtml/sales_order/view/", array("order_id" => $new_order->getId()));
                            $link_related_exchange_order .= "<a href=\"{$url}\" target=\"__blank\">".$return->getReturnFeeOrderId()."</a>, ";
                            $data_link['exchange'] = $link_related_exchange_order;
                        }
                        $url = Mage::getModel("core/url")->getUrl("adminhtml/sales_returns/edit/", array("id" => $return->getId()));
                        $link_related_return_order .= "<a href=\"{$url}\" target=\"__blank\">".$return->getIncrementId()."</a>, ";
                        $data_link['return'] = $link_related_return_order;
                        break;
                      default:
                        $url = Mage::getModel("core/url")->getUrl("adminhtml/sales_returns/edit/", array("id" => $return->getId()));
                        $link_related_return_order .= "<a href=\"{$url}\" target=\"__blank\">".$return->getIncrementId()."</a>, ";
                        $data_link['return'] = $link_related_return_order;
                    }

                }
            }else{
                if($is_get_origin_order) continue;
                $return = Mage::getModel("opentechiz_return/return")->getCollection()->addFieldToSelect('order_item_id')->addFieldToFilter('return_fee_order_id',array('eq'=>$order->getIncrementId()))->getFirstItem();
                if($return){
                    $order_item = Mage::getModel("sales/order_item")->load($return->getOrderItemId());
                    if($order_item->getId()){
                        $new_order = Mage::getModel('sales/order')->load($order_item->getOrderId());
                        if($new_order){
                            $url = Mage::getModel("core/url")->getUrl("adminhtml/sales_order/view/", array("order_id" => $new_order->getId()));
                            $link_origin_order .= "<a href=\"{$url}\" target=\"__blank\">".$new_order->getIncrementId()."</a> ";
                            $data_link['origin_order'] = $link_origin_order;
                            $is_get_origin_order = 1;
                        }
                    }
                }
            }
        }
        if(isset($data_link['return'])){
            $data_link['return'] = substr(trim($data_link['return']), 0, -1);
        }
        if(isset($data_link['exchange'])){
            $data_link['exchange'] = substr(trim($data_link['exchange']), 0, -1);
        }
        if(isset($data_link['repair'])){
            $data_link['repair'] = substr(trim($data_link['repair']), 0, -1);
        }

        return $data_link;
    }

    public function allowChangeOrderData($order){
        $statusAllow = OpenTechiz_SalesExtend_Helper_Data::INTERNAL_STATUTES_TO_ALLOW_EDIT_DATA_AFTER_ORDER_COMPLETE;
        if(in_array($order->getInternalStatus(), $statusAllow)){
            return true;
        }else return false;
    }
}
