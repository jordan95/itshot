<?php

class OpenTechiz_SalesExtend_Block_Adminhtml_ShipFrom extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_shipFrom';
        $this->_blockGroup = 'opentechiz_salesextend';
        $this->_headerText = Mage::helper('opentechiz_salesextend')->__('Ship From Addresses');
        parent::__construct();
//        $this->_removeButton('add');
    }
}