<?php

class OpenTechiz_SalesExtend_Block_Rewrite_Adminhtml_Cod_Paymentform extends OnlineBiz_COD_Block_Paymentform {

    protected function _construct() {
        parent::_construct();
        if($this->getRequest()->getControllerName() ==  'sales_order_creditmemo') {
            $this->setTemplate('opentechiz/sales_extend/cod/payment/credit_memo_form.phtml');
        }else {
            $this->setTemplate('cod/payment/form.phtml');
        }
    }

    public function _getCreditMemo(){
        if(!Mage::registry('current_credit_memo')){
            Mage::register('current_credit_memo', Mage::getModel('sales/order_creditmemo')->load(Mage::app()->getRequest()->getParam('creditmemo_id')));
        }
        return Mage::registry('current_credit_memo');
    }

    public function _getPaymentGridCreditMemo(){
        $cmemoId = $this->_getCreditMemo()->getId();
        $paygrid_collection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->addfieldtofilter('credit_memo_id', array('eq' => $cmemoId));
        return $paygrid_collection;
    }
}

?>
