<?php

if (class_exists("Idev_OneStepCheckout_Block_Adminhtml_Sales_Order_View_Comment")) {

    class OpenTechiz_SalesExtend_Block_Rewrite_Adminhtml_Sales_Order_View_Items_Pure extends Idev_OneStepCheckout_Block_Adminhtml_Sales_Order_View_Comment
    {
        
    }

} else {

    class OpenTechiz_SalesExtend_Block_Rewrite_Adminhtml_Sales_Order_View_Items_Pure extends Mage_Adminhtml_Block_Sales_Order_View_Items
    {
        
    }

}

class OpenTechiz_SalesExtend_Block_Rewrite_Adminhtml_Sales_Order_View_Items extends OpenTechiz_SalesExtend_Block_Rewrite_Adminhtml_Sales_Order_View_Items_Pure
{

    public function displayPrices($basePrice, $price, $strong = false, $separator = '<br />')
    {
        if (!Mage::helper("opentechiz_salesextend")->getTurnOffForeignCurrency()) {
            return parent::displayPrices($basePrice, $price, $strong, $separator);
        } else {
            $order = $this->getOrder();
            if ($order && $order->isCurrencyDifferent()) {
                $res = '<strong>';
                $res .= $order->formatBasePrice($basePrice);
                $res .= '</strong>';
                if (!Mage::helper("opentechiz_salesextend")->getTurnOffForeignCurrency()) {
                    $res .= $separator;
                    $res .= '[ddd' . $order->formatPrice($price) . ']';
                }
            } elseif ($order) {
                $res = $order->formatPrice($price);
                if ($strong) {
                    $res = '<strong>' . $res . '</strong>';
                }
            } else {
                $res = Mage::app()->getStore()->formatPrice($price);
                if ($strong) {
                    $res = '<strong>' . $res . '</strong>';
                }
            }
            return $res;
        }
    }

}
