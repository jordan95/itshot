<?php

class OpenTechiz_SalesExtend_Block_Rewrite_Adminhtml_Cod_Payment_Search_Grid extends OnlineBiz_COD_Block_Adminhtml_Order_Payment_Search_Grid {

    protected function _prepareCollection() {
        $collection = Mage::getModel('cod/payment')->getCollection();
        $creditMemo = Mage::registry('current_creditmemo');
        
        if($creditMemo && $creditMemo->getId()){
            $collection->addFieldToFilter('name', array('neq' => 'StoreCredit'));
        }else{
            $requestParams = $this->getRequest()->getParams();
            if(isset($requestParams['creditmemo_id'])){
                $collection->addFieldToFilter('name', array('neq' => 'StoreCredit'));
            }
        }
        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }
}

