<?php

class OpenTechiz_SalesExtend_Helper_OrderValidation extends Mage_Core_Helper_Abstract
{
    const ORDER_VALIDATION_LOG_FILE = 'order_validation.log';

    public function validateCreditCardOrder($order){
        $isPaidWithCC = Mage::getModel('opentechiz_salesextend/observer')->isPaidWithCreditCard($order);
        $orderTypeIsLayawayOrRepair = in_array($order->getOrderType(), array(1, 2));

        $customer_id = $order->getCustomerId();
        $customer = Mage::getModel('customer/customer')->load($customer_id);
        $isCustomerTrusted = (Mage::helper('opentechiz_salesextend')->isTrustedCustomer($customer) && Mage::helper('opentechiz_salesextend')->isInfoMatch($customer));

        $isCustomerFraudster = $customer->getIsFraudster();

        $authorisedNetInfo = Mage::getModel('medialounge_checkout/authorisednet_data')->getRecordInfoByQuoteId($order->getQuoteId());
        $isErrorWithCC = false;
        if(isset($authorisedNetInfo['error_message']) && $authorisedNetInfo['error_message']){
            $isErrorWithCC = true;
        }

        $result = [];
        $result['is_paid_with_cc'] = $isPaidWithCC;
        $result['is_layaway_or_repair'] = $orderTypeIsLayawayOrRepair;
        $result['is_customer_trusted'] = $isCustomerTrusted;
        $result['is_customer_fraudster'] = $isCustomerFraudster;
        $result['is_error_with_cc'] = $isErrorWithCC;

        return $result;
    }

    public function checkIfSendToSignifyd($order){
        $validateData = $this->validateCreditCardOrder($order);
        Mage::log('OrderId: '.$order->getIncrementId(),null, self::ORDER_VALIDATION_LOG_FILE);
        Mage::log('checkIfSendToSignifyd:',null, self::ORDER_VALIDATION_LOG_FILE);
        Mage::log($validateData,null, self::ORDER_VALIDATION_LOG_FILE);
        if (!$validateData['is_paid_with_cc']
            || $validateData['is_layaway_or_repair']
            || $validateData['is_customer_trusted']
            || $validateData['is_customer_fraudster']
            || $validateData['is_error_with_cc']
        ) {
            return false;
        }else{
            return true;
        }
    }

    public function checkIfCanAutoChargeCCOnOrderCreate($order){
        $validateData = $this->validateCreditCardOrder($order);
        $countInvoices = count($order->getInvoiceCollection());
        Mage::log('OrderId: '.$order->getIncrementId(),null, self::ORDER_VALIDATION_LOG_FILE);
        Mage::log('checkIfCanAutoChargeCCOnOrderCreate:',null, self::ORDER_VALIDATION_LOG_FILE);
        Mage::log($validateData,null, self::ORDER_VALIDATION_LOG_FILE);
        Mage::log('Count Invoices: '.$countInvoices, null, self::ORDER_VALIDATION_LOG_FILE);
        if($validateData['is_paid_with_cc']
            && !$validateData['is_error_with_cc']
            && !$validateData['is_customer_fraudster']
            && $validateData['is_customer_trusted']
            && $countInvoices == 0
        ){
            return true;
        }else{
            return false;
        }
    }

    public function checkIfCanAutoChargeCCOnManualStageChange($order){
        $validateData = $this->validateCreditCardOrder($order);
        $countInvoices = count($order->getInvoiceCollection());
        Mage::log('OrderId: '.$order->getIncrementId(),null, self::ORDER_VALIDATION_LOG_FILE);
        Mage::log('checkIfCanAutoChargeCCOnManualStageChange:',null, self::ORDER_VALIDATION_LOG_FILE);
        Mage::log($validateData,null, self::ORDER_VALIDATION_LOG_FILE);
        Mage::log('Count Invoices: '.$countInvoices, null, self::ORDER_VALIDATION_LOG_FILE);
        if($validateData['is_paid_with_cc'] && $countInvoices == 0){
            return true;
        } else{
            return false;
        }
    }
}