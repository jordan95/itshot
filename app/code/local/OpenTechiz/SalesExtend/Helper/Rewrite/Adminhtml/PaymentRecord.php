<?php
class OpenTechiz_SalesExtend_Helper_Rewrite_Adminhtml_PaymentRecord extends Mage_Core_Helper_Abstract
{
    const AVAILABLE_PAYMENT_METHOD_FOR_RECORD = [
        1 => 'Affirm',
        2 => 'AmazonJNS',
        3 => 'AmazonJNSCanada',
        4 => 'AmazonWebsite',
        'Amazon_payments' => 'AmazonWebsite',
        'Checkout By Amazon' => 'AmazonWebsite',
        'Pay by Amazon' => 'AmazonWebsite',
        5 => 'CreditCard',
        'Amex (Authorizenet)' => 'CreditCard',
        'Amex (Verisign)' => 'CreditCard',
        'authorizenet' => 'CreditCard',
        'DebitCard' => 'CreditCard',
        'Discover (Authorizenet)' => 'CreditCard',
        'VISA (Authorizenet)' => 'CreditCard',
        'Discover (Verisign)' => 'CreditCard',
        'Master Card (Authorizenet)' => 'CreditCard',
        'Master Card (Verisign)' => 'CreditCard',
        'Visa (Verisign)' => 'CreditCard',
        6 => 'Bank Wire',
        'banktransfer' => 'Bank Wire',
        'banktransfer_mc' => 'Bank Wire',
        'WesternUnion' => 'Bank Wire',
        7 => 'Cash',
        8 => 'Check(MoneyOrder)',
        'Check' => 'Check(MoneyOrder)',
        'checkmo' => 'Check(MoneyOrder)',
        'DirectCashDeposit' => 'Check(MoneyOrder)',
        'MoneyGram' => 'Check(MoneyOrder)',
        9 => 'ClaimPayment',
        10 => 'GE',
        11 => 'GiftCard',
        'Gift Certificate' => 'GiftCard',
        12 => 'Google Checkout',
        13 => 'Main11',
        14 => 'MultiplePayment',
        15 => 'NewEgg',
        16 => 'Overstock',
        17 => 'PayPal',
        'PayPal (Standard)' => 'PayPal',
        'paypal_standard' => 'PayPal',
        'PayPal Express Checkout' => 'PayPal',
        'paypal_express' => 'PayPal',
        18 => 'Rakuten (Buy.com)',
        19 => 'Sears',
        20 => 'StoreCredit',
        21 => 'Zelle',
        22 => 'ScarpGold',
        23 => 'Trend',
        24 => 'From Import Data',
        'Paymentmethod' => 'From Import Data',
        '' => 'From Import Data',
        25 => 'Acima',
        'Acima (Mastercard)' => 'Acima',
        'acimacheckout' => 'Acima',
        26 => 'Offline Refund',
        27 => 'No Payment Information Required',
        'free' => 'No Payment Information Required',
    ];

    public function getPaymentMethodId($name){
        foreach (self::AVAILABLE_PAYMENT_METHOD_FOR_RECORD as $key => $value){
            if(str_replace(' ', '', strtolower($name)) == str_replace(' ', '', strtolower($key))){
                $name = $value;
            }
        }
        foreach (self::AVAILABLE_PAYMENT_METHOD_FOR_RECORD as $key => $value){
            if(str_replace(' ', '', strtolower($name)) == str_replace(' ', '', strtolower($value))
                && is_numeric($key)){
                return $key;
            }
            if(stripos($value, $name) !== false && is_numeric($key)){
                return $key;
            }
        }
        return $name;
    }
}
