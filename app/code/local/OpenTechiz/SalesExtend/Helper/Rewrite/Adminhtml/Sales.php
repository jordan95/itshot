<?php
class OpenTechiz_SalesExtend_Helper_Rewrite_Adminhtml_Sales extends Mage_Adminhtml_Helper_Sales
{
    const CURRENCY_NOTE_FOR_CART_PAGE = 'Prices are in USD';

    public function displayPrices($dataObject, $basePrice, $price, $strong = false, $separator = '<br/>')
    {
        $order = false;
        if ($dataObject instanceof Mage_Sales_Model_Order) {
            $order = $dataObject;
        } else {
            $order = $dataObject->getOrder();
        }

        if ($order && $order->isCurrencyDifferent()) {
            $res = '<strong>';
            $res .= $order->formatBasePrice($basePrice);
            $res .= '</strong>';
            if (!Mage::helper("opentechiz_salesextend")->getTurnOffForeignCurrency()) {
                $res .= $separator;
                $res .= '[' . $order->formatPrice($price) . ']';
            }
        } elseif ($order) {
            $res = $order->formatPrice($price);
            if ($strong) {
                $res = '<strong>' . $res . '</strong>';
            }
        } else {
            $res = Mage::app()->getStore()->formatPrice($price);
            if ($strong) {
                $res = '<strong>' . $res . '</strong>';
            }
        }
        return $res;
    }

}
