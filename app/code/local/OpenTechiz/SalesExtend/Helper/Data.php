<?php

class OpenTechiz_SalesExtend_Helper_Data extends Mage_Core_Helper_Abstract
{
    const OPPENTECHIZ_SALESEXTEND_CHANGE_APPROVE         =   [
        '0' => 'No',
        '1' => 'Yes'
    ];
    const TERM = [
        0 => 'Under Verification',
        1 => 'Process ASAP',
        2 => 'Contact Customer ASAP',
        3 => 'Out of Stock',
        4 => 'Waiting for Customer ID/CC',
        5 => 'WFR by Customer',
        6 => 'Item(s) Ready',
        7 => 'Paid',
        8 => 'Shipped',
        9 => 'Refunded',
        10 => 'Voided',
        11 => 'Declined By Customer',
        12 => 'Need to Order',
        13 => 'Ordered from Factory',
        14 => 'OutOfStocksEmailed',
        15 => 'Not Verified Yet',
        16 => 'Manual Verification',
        17 => 'Alena',
        18 => 'Denis Verification',
        19 => 'Inga',
        20 => 'Oksana',
        21 => 'Sasha\'s Order',
        22 => 'Repair',
        23 => 'Not Processed Layaway',
        24 => 'Layaway',
        25 => 'Processed Layaway',
        26 => 'Expired Layaway',
        27 => 'Guaranteed',
        28 => 'Rep Approved',
        29 => 'PayPal/GE',
        30 => 'Taner',
        31 => 'Irene',
        32 => 'OCBC',
        33 => 'Completed',
        34 => 'Need To Process',
        35 => 'Waiting for item(s)',
        36 => 'Processing',
        37 => 'Need to Ship',
        38 => 'WILL PICK UP',
        39 => 'Anna',
        40 => 'alla',
        41 => 'Tanya',
        42 => 'Wire/Money Gram/Check',
        43 => 'Issue Refund Check',
        44 => 'Waiting for copy of id/cc',
        45 => 'FINANCE',
        46 => 'Partially Verified',
        47 => 'Item(s) Ready',
        48 => 'YuliyaT',
        49 => 'Katya',
        50 => 'Denise',
        51 => 'RV',
        52 => 'FraudOrder',
        53 => 'InHouse-Video',
        54 => 'Alena',
        55 => 'Eliazer',
        57 => 'LAYAWAY',
        58 => 'House Processing',
        59 => 'InHouse-Images',
        60 => 'Lena',
        61 => 'Sviatlana',
        62 => 'InHouse-Descriptions',
        63 => 'Ronnie',
        64 => 'Due on Reciept',
        65 => 'PICKED UP in showroom'
    ];

    const VERIFICATION = [
        0 => 'Not Verified',
        1 => 'Auto Approved',
        2 => 'Guaranteed',
        3 => 'Management Approved',
        4 => 'Manual Verification',
        6 => 'Rep Approval',
        7 => 'Auto Verified',
        8 => 'Contact Customer',
        9 => 'Not Applicable',
        10 => 'No Value (Migrated)',
        11 => 'Declined',
        12 => 'Paypal Pending'
    ];

    const SELECTABLE_VERIFICATION_STATUS = [
        0 => 'Not Verified',
        2 => 'Guaranteed',
        3 => 'Management Approved',
        11 => 'Declined'
    ];

    const VERIFICATION_STATUS_NOT_ALLOWED_TO_MOVE_TO_PROCESSING = [
        0, 8, 9, 11, 12
    ];

    const VERIFICATION_STATUSES_ALLOWED_TO_CHANGE_WHEN_CHANGE_STAGE = [
        0, 4, 8
    ];

    const ACTUAL_SHIPPING_METHOD_CODES = [
        0 => 'fedex',
        1 => 'usps'
    ];

    const ORDER_TYPE = [
        0 => 'Standard',
        1 => 'Layaway',
        2 => 'Repair',
        3 => 'Exchange'
    ];
    const ORDER_TYPE_STANDARD_CODE = 0;
    const ORDER_TYPE_LAYAWAY_CODE = 1;
    const ORDER_TYPE_REPAIR_CODE = 2;
    const ORDER_TYPE_EXCHANGE_CODE = 3;
    
    const ORDER_VERIFICATION = 0;
    const ORDER_PROCESSING = 1;
    const FULFILLMENT = 2;
    const BILLING = 3;
    const PREPSHIPMENT = 4;
    const SHIPPED = 5;
    const NOTAPPLICABLE = 6;

    const ORDER_TYPE_STANDARD = 'standard';
    const ORDER_TYPE_LAYAWAY = 'layaway';

    const ORDER_STAGE = [
        0 => 'Order Verification',
        1 => 'Order Processing',
        2 => 'Fulfillment',
        3 => 'Billing',
        4 => 'Prep Shipment',
        5 => 'Shipped',
        6 => 'Not Applicable'
    ];
    const ORDER_INTERNAL_STATUS_WAITING_ON_CUSTOMER = 14; 
    const ORDER_INTERNAL_STATUS_MANUFACTURING = 2; 
    const ORDER_INTERNAL_STATUS_WAITING_ON_VENDOR = 4; 
    const ORDER_INTERNAL_STATUS_FIRST_PAYMENT_COMPLETE = 5; 
    const ORDER_INTERNAL_STATUS_WAITING_ON_FINAL_PAYMENT = 26; 
    const ORDER_INTERNAL_STATUS = [
        0 => 'New',
        1 => 'Item(s) Collection',
        2 => 'Item(s) Manufacturing',
        3 => 'Engraving/Sizing',
        4 => 'Waiting on Vendor',
        6 => 'Item(s) Ready',
        7 => 'Payment Complete',
        9 => 'Label and Invoice Ready',
        10 => 'Ready for Packaging',
        11 => 'Packaging Complete',
        12 => 'Tracking # exists',
        14 => 'Waiting on Customer',
        16 => 'Picked up from store',
        18 => 'Offer Alternatives',
        20 => 'Invoice ready',
        21 => 'Need to Order',
        22 => 'Not Applicable',
        23 => 'No Value (Migrated)',
        24 => 'Contact Customer',
        28 => 'Showroom Pickup',
        29 => 'Contact Customer',
        30 => 'Payment Review',
        31 => 'Manual Verification',
        32 => 'Awaiting CC Payment'
    ];

    const LAYAWAY_ORDER_INTERNAL_STATUS = [
        0 => 'Not Processed',
        1 => 'Item(s) Collection',
        2 => 'Item(s) Manufacturing',
        3 => 'Engraving/Sizing',
        4 => 'Waiting on Vendor',
        5 => 'First Payment Complete',
        6 => 'Item(s) Ready',
        7 => 'Payment Complete',
        9 => 'Label and Invoice Ready',
        10 => 'Ready for Packaging',
        11 => 'Packaging Complete',
        12 => 'Tracking # exists',
        13 => 'Expired Layaway',
        14 => 'Waiting on Customer',
        15 => 'Verify Order',
        18 => 'Offer Alternatives',
        20 => 'Invoice ready',
        21 => 'Need to Order',
        22 => 'Not Applicable',
        23 => 'No Value (Migrated)',
        24 => 'Contact Customer',
        25 => 'Process Layaway',
        26 => 'Waiting on Final Payment',
        27 => 'New',
        28 => 'Showroom Pickup',
        29 => 'Contact Customer'
    ];

    const REPAIR_ORDER_INTERNAL_STATUS = [
        0 => 'Item Received',
        6 => 'Item(s) Ready',
        7 => 'Payment Complete',
        9 => 'Label and Invoice Ready',
        10 => 'Ready for Packaging',
        11 => 'Packaging Complete',
        12 => 'Tracking # exists',
        14 => 'Waiting on Customer',
        17 => 'Repair in Progress',
        20 => 'Invoice ready',
        22 => 'Not Applicable',
        23 => 'No Value (Migrated)',
        24 => 'Contact Customer',
        28 => 'Showroom Pickup',
        29 => 'Contact Customer'
    ];

    const INTERNAL_STATUTES_TO_ALLOW_EDIT_DATA_AFTER_ORDER_COMPLETE = [
        22, 23
    ];

    const EDITABLE_INTERNAL_STATUS = [
        0,1,2,4,6,7,9,10,11,12,14,16,17,18,20,21,23,24,26,27,28,30,31
    ];

    const COLOR = [
        'yellow' => 'ye',
        'rose'   => 'ro',
        'white'  => 'wh'
    ];

    const DEFAULT_STATUS_FOR_EACH_STAGES = [
        0 => 0,
        1 => 0,
        2 => 1,
        3 => 6,
        4 => 7,
        5 => 12
    ];

    const IMPOSSIBLE_STAGE_CHANGES = [
        4 => 5,
        5 => 4
    ];

    const POSSIBLE_STAGE_CHANGES = [
        0 => array(0,1),
        1 => array(0,1,2),
        2 => array(1,2,3),
        3 => array(2,3,4),
        4 => array(3,4),
        5 => array(4,5),
        6 => array(6)
    ];

    const POSSIBLE_STAGE_CHANGES_REPAIR = [
        1 => array(1,2),
        2 => array(2),
        3 => array(3,4),
        4 => array(3,4),
        5 => array(4,5),
    ];

    const STATUS_STAGE_TYPE_RELATION = [
        0 => array(
            0 => [0,14,24,29,30,31,32],
            1 => [0,14,18],
            2 => [1,2,3,4,21],
            3 => [6,14],
            4 => [7,10,11,9],
            5 => [12,22,23,28],
            6 => [22]
        ),
        1 => array(
            1 => [0,5,13,14,15,18,25,27],
            2 => [1,2,3,4,21],
            3 => [6,14,26],
            4 => [7,10,11,9],
            5 => [12,22,23,28],
            6 => [22]
        ),
        2 => array(
            1 => [0,14],
            2 => [17],
            3 => [6,14],
            4 => [7,10,11,9],
            5 => [12,22,23,28],
        ),
        3 => array(
            0 => [0,14,24,29,30],
            1 => [0,14,18],
            2 => [1,2,3,4,21],
            3 => [6,14],
            4 => [7,10,11,9],
            5 => [12,22,23,28],
            6 => [22]
        )
    ];

    const AUTO_STAGE_STATUS_CHANGE_ROUTE = [
        0 => array( //to Order Verification                                //destination stage
            29 => array( //to Contact customer                             //destination status
                array(0, null), //from Order Verification/any status       //current stage/status, null = any
            ),
            30 => array( //to Payment Review
                array(0, null), //from Order Verification/any status
            ),
            31 => array( //to Manual Verification
                array(0, null), //from Order Verification/any status
            ),
            32 => array( //to Awaiting CC payment
                array(0, null), //from Order Verification/any status
            )
        ),
        1 => array( //to Order Processing
            0 => array( //to Not Processed/New/Item Received
                array(0, null) //from Order Verification/any status
            ),
            5 => array( //to First Payment Complete
                array(1, 0),  //from Order Processing/Expired Layaway
                array(1, 13),  //from Order Processing/Expired Layaway
                array(1, 14),  //from Order Processing/Waiting for Customer
                array(1, 18),  //from Order Processing/Offer Alternatives
                array(1, 27),  //from Order Processing/New
                array(0, null) //from Order Verification/any status
            ),
            15 => array( //to Verify Order
                array(1, null) //from Order Processing/any status
            ),
            25 => array( //to Process Layaway
                array(1, 15) //from Order Processing/Verify Order
            )
        ),
        2 => array( //to Fulfilment
            1 => array( //to Item Collection
                array(2, null), //from Fulfilment/any status
            ),
            17 => array( //to Item in repair
                array(1, null) //from Processing
            )
        ),
        3 => array( //to Billing
            6 => array( //to Item Ready
                array(2, null), //from Fulfilment/any status
                array(3, null),  //from Billing/any status
                array(4, null)  //from Ship Prep/any status
            ),
            26 => array( //to Item Ready
                array(2, null), //from Fulfilment/any status
                array(3, null),  //from Billing/any status
                array(4, null)  //from Ship Prep/any status
            ),
        ),
        4 => array( //to Prep Ship
            7 => array( //to Payment Complete
                array(3, 6), //from Billing/Item Ready
            )
        ),
        5 => array( //to Shipped
            12 => array( //to Tracking Exist
                array(4, null), //from Prep Ship/any status
                array(5, null)
            ),
            22 => array( //to Not Applicable
                array(4, null), //from Prep Ship/any status
                array(5, null)
            ),
            28 => array( //to Showroom Pickup
                array(4, null), //from Prep Ship/any status
                array(5, null)
            )
        ),
    ];
    const ORDER_REPAIR_TYPE = 2;
    const STORE_CREDIT_PAYMENT_METHOD_FOR_RECORD = 'StoreCredit';

    const MARKETPLACE_ORDER_SOURCE = [2,4,5,6,7,9,10,11,13,14,15,16,17,18,19,20,21,22];
    const DEACTIVE_ORDER_STATE =  ['canceled','closed','holded','complete'];
    const CANCEL_CLOSE_ORDER_STATE =  ['canceled','closed'];
    public function needResize($order){
        $items = $order->getAllItems();
        $result = false;
        //check if available in stock with exact sku
        foreach ($items as $item) {
            if($item->getQtyOrdered() == $item->getQtyRefunded() + $item->getQtyCanceled()){
                continue;
            }
            $sku = $item->getSku();
            $productId = $item->getProductId();
            if(!$productId || !Mage::getModel('catalog/product')->load($productId) || $item->getProductType() == 'virtual'){
                continue;
            }
            //check if item(s) have quotation
            if(strpos($sku, 'quotation') !== false) {
                return false;
            }
            $instockProduct = Mage::getModel('opentechiz_inventory/instock')->load($sku, 'sku');
            if($instockProduct && $instockProduct->getId()){
                $qtyOrdered = $item->getQtyOrdered() - $item->getQtyRefunded() - $item->getQtyCanceled();
                if($instockProduct->getQty() - $instockProduct->getOnHold() - $qtyOrdered < 0){
                    return true;
                }
            }else{
                return true;
            }

        }
        return $result;
    }

    public function needEngrave($order){
        $items = $order->getAllItems();
        $result = false;
        //check if item(s) have engraving option
        foreach ($items as $item){
            if($item->getQtyOrdered() == $item->getQtyRefunded() + $item->getQtyCanceled()){
                continue;
            }
            $options = $item->getProductOptions();
            if (!is_array($options)) $options = unserialize($options);
            if(isset($options['options'])) {
                foreach ($options['options'] as $option) {
                    if (strtolower($option['label']) == OpenTechiz_PersonalizedProduct_Helper_Data::ENGRAVING_KEYWORD
                        && strtolower($option['value']) != OpenTechiz_PersonalizedProduct_Helper_Data::DONT_ENGRAVE) {
                        $result = true;
                        break;
                    }
                }
            }
        }
        return $result;
    }

    public function canAutoChangeOrderStageStatus($order, $newStage, $newStatus){
        $stage = $order->getOrderStage();
        $status = $order->getInternalStatus();
        $orderType = $order->getOrderType();
        if($newStage === null){
            $newStage = $stage;
        }
        if($newStatus === null){
            $newStatus = $status;
        }

        $orderStateToNotChangeData = array(
            Mage_Sales_Model_Order::STATE_CLOSED,
            Mage_Sales_Model_Order::STATE_CANCELED,
            Mage_Sales_Model_Order::STATE_HOLDED
        );

        if(in_array($order->getState(), $orderStateToNotChangeData)) return false; //stop change stage status if order is not active
        if(!$order->getOrderType() && !$order->getOrderStage()) return true; //skip check if order is created

        //change order to shipped regardless of current stage if order is paid #3269
        if($newStage == 5 && $order->getBaseRemainingAmount() == 0){
            return true;
        }

        $possibleChange = self::AUTO_STAGE_STATUS_CHANGE_ROUTE;
        if(!isset($possibleChange[$newStage][$newStatus])) return false;

        $possibleStatusArray = $possibleChange[$newStage][$newStatus];
        $passed = false;
        foreach ($possibleStatusArray as $array){
            if($array[1] === null){ //if == null, get all possible status
                if(($orderType == 1 || $orderType == 2) && $stage == 0){ //some type doesn't have stage 0
                    if($orderType == 1){
                        $array[1] = array_keys(self::LAYAWAY_ORDER_INTERNAL_STATUS);
                    }elseif($orderType == 2){
                        $array[1] = array_keys(self::REPAIR_ORDER_INTERNAL_STATUS);
                    }
                }else {
                    $array[1] = self::STATUS_STAGE_TYPE_RELATION[$orderType][$stage];
                }
            }else{
                $array[1] = explode(',', $array[1]);
            }
            if($stage == $array[0] && in_array($status, $array[1])){
                $passed = true;
            }
        }
        return $passed;
    }

    public function getPossibleStageChanges($order){
        $results = self::POSSIBLE_STAGE_CHANGES;
        if(!$this->checkStockForOrder($order)){
            unset($results[2]);
            $results[2] = array(1,2);
        }
        if($order->getPayment()->getMethod() == 'paypal_express'){
            unset($results[1]);
            $results[1] = array(1,2);
        }

        $orderTotal = round($order->getBaseGrandTotal(), 4);
        $paid = round($this->getTotalPaymentRecordForOrder($order), 4);
        if($order->getOrderType() == 2){
            $results = self::POSSIBLE_STAGE_CHANGES_REPAIR;
            if($paid < $orderTotal) {
                $results[3] = array(3);
            }
        }elseif($paid < $orderTotal) {
            $results[3] = array(2,3);
        }


        return $results;
    }

    public function checkStockForOrder($order){
        $flag = true;
        foreach ($order->getAllItems() as $item){

            $productId = $item->getProductId();
            $qtyOrdered = $item->getQtyOrdered() - $item->getQtyRefunded() - $item->getQtyCanceled();
            if(!$productId || !Mage::getModel('catalog/product')->load($productId)
                || $item->getProductType() == 'virtual'){
                continue;
            }
            if($qtyOrdered < 1){
                continue;
            }
            $orderSku = $item->getSku();
            $result = Mage::helper('opentechiz_inventory')->getAvailableSize($orderSku);
            if(count($result) < 1){
                $flag = false;
                break;
            }else{
                $qtyInStock = 0;
                foreach ($result as $instock){
                    $qtyInStock = $qtyInStock + $instock['qty'] - $instock['on_hold'];
                }

                if($qtyInStock < $qtyOrdered){
                    $flag = false;
                }
            }
        }
        return $flag;
    }

    public function getAllowedStatusChange($order){
        $internalStatuses = $order->getInternalStatuses();
        $internalStatuses = array_map('intval',explode(',', $internalStatuses));
        if(in_array(15, $internalStatuses)){
            array_push($internalStatuses, 25);
        }
        if(count($order->getInvoiceCollection()) > 0 && !in_array(8, $internalStatuses)){
            array_push($internalStatuses, 8);
        }

        $resultArray = array_merge($internalStatuses, self::EDITABLE_INTERNAL_STATUS);
        //additional rules
        if(($this->needResize($order) || $this->needEngrave($order)) && $this->checkStockForOrder($order)){
            array_push($resultArray, 3);
        }

        //check for payment, remove status item ready
        if($order->getOrderType() == 1 && $order->getOrderStage() == 3) {
            $totalPaid = Mage::helper('opentechiz_salesextend')->getTotalPaymentRecordForOrder($order);
            $orderTotal = round($order->getBaseGrandTotal(), 4);
            $totalPaid = round($totalPaid, 4);
            if ($totalPaid < $orderTotal) {
                $resultArray = array_diff($resultArray, [6]);
            }
        }

        return $resultArray;
    }

    public function getDefaultStatusForStage($order){
        $statuses = OpenTechiz_SalesExtend_Helper_Data::DEFAULT_STATUS_FOR_EACH_STAGES;
        //if is layaway and not paid
        //wait for final payment
        if($order->getOrderType() == 1){
            $totalPaid = $this->getTotalPaymentRecordForOrder($order);
            $orderTotal = round($order->getBaseGrandTotal(), 4);
            $totalPaid = round($totalPaid, 4);
            if ($totalPaid < $orderTotal) {
                $statuses[3] = 26;
            }
        }
        return $statuses;
    }

    public function isOrderHasItemInRepair($order){
        $itemInRepairCollectionSize = Mage::getModel('sales/order_item')->getCollection()
            ->join(array('request' => 'opentechiz_production/product'), 'request.order_item_id = main_table.item_id', '*')
            ->addFieldToFilter('order_id', $order->getId())
            ->addFieldToFilter('request.status', array('neq' => 50))
            ->addFieldToFilter('production_type', 5)->getSize();
        if($itemInRepairCollectionSize) return true;
        else return false;
    }

    public function getStatusArrayByType($type){
        if($type == 0 || $type == 3){
            return self::ORDER_INTERNAL_STATUS;
        }elseif($type == 1){
            return self::LAYAWAY_ORDER_INTERNAL_STATUS;
        }elseif ($type == 2){
            return self::REPAIR_ORDER_INTERNAL_STATUS;
        }else{
            return null;
        }
    }

    public function getStages($order){
        $orderType = $order->getOrderType();
        $stages = [];
        if($orderType == 0 || $orderType == 3){
            $stages = array(0,1,2,3,4,5,6);
        }elseif($orderType == 1){
            $stages = array(1,2,3,4,5,6);
        }elseif ($orderType == 2){
            $stages = array(1,2,3,4,5);
        }
        if($stages == []) return [];
        else{
            $results = [];
            foreach ($stages as $stage){
                $results[$stage] = self::ORDER_STAGE[$stage];
            }
            return $results;
        }
    }

    public function getInternalStatus($type, $stage){

        $statuses = array();
        if(isset(self::STATUS_STAGE_TYPE_RELATION[$type][$stage])){
            $statuses = self::STATUS_STAGE_TYPE_RELATION[$type][$stage];
        }

        $results = [];

        $statusArray = $this->getStatusArrayByType($type);

        foreach ($statuses as $status){
            $results[$status] = $statusArray[$status];
        }
        return $results;
    }

    public function getOrderInternalStatus($order){
        $orderType = $order->getOrderType();
        if($orderType == 0 || $orderType == 3){
            return self::ORDER_INTERNAL_STATUS;
        }elseif($orderType == 1){
            return self::LAYAWAY_ORDER_INTERNAL_STATUS;
        }elseif($orderType == 2){
            return self::REPAIR_ORDER_INTERNAL_STATUS;
        }
    }

    public function getAllInternalStatus(){
        $standardArray = self::ORDER_INTERNAL_STATUS;
        $layawayArray = self::LAYAWAY_ORDER_INTERNAL_STATUS;
        $repairArray = self::REPAIR_ORDER_INTERNAL_STATUS;
        $result = [];

        foreach ($standardArray as $key => $value){
            $result[$key] = $value;
        }

        foreach ($layawayArray as $key => $value){
            if(isset($result[$key]) && $result[$key] == $value){
                continue;
            }else{
                if(!isset($result[$key])){
                    $result[$key] = $value;
                }else{
                    array_push($result, $value);
                }
            }
        }

        foreach ($repairArray as $key => $value){
            if(isset($result[$key]) && $result[$key] == $value){
                continue;
            }else{
                if(!isset($result[$key])){
                    $result[$key] = $value;
                }else{
                    array_push($result, $value);
                }
            }
        }
        $result = array_flip($result);
        foreach ($result as $key => $value){
            $result[$key] = $key;
        }

        return $result;
    }

    public function getAllInternalStatusKey(){
        $standardArray = self::ORDER_INTERNAL_STATUS;
        $layawayArray = self::LAYAWAY_ORDER_INTERNAL_STATUS;
        $repairArray = self::REPAIR_ORDER_INTERNAL_STATUS;
        $result = [];

        foreach ($standardArray as $key => $value){
            $result[$value] = $key;
        }
        foreach ($layawayArray as $key => $value){
            $result[$value] = $key;
        }
        foreach ($repairArray as $key => $value){
            $result[$value] = $key;
        }

        return $result;
    }

    //param order
    //return array of item sku has profit problem
    public function checkProfitOrder($order){
        $percent = (float)Mage::getStoreConfig('opentechiz_salesextend/general/min_profit');
        if(!is_numeric($percent)){
            $percent = 20;
        }

        $orderId = $order->getId();
        $itemCollection = Mage::getModel('sales/order_item')->getCollection()->addFieldToFilter('order_id',$orderId);
        $skusHasErrorCost = '';

        foreach ($itemCollection as $orderItem){
            //skip cost check
            if($orderItem->getSkipCostCheck() == 1) continue;

            $sku = $orderItem->getSku();
            $skuParts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
            if(count($skuParts)>1 && is_numeric($skuParts[count($skuParts)-1])){
                unset($skuParts[count($skuParts)-1]);
            }
            $sku = implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $skuParts);

            $inventoryItemCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()
                ->addFieldToFilter('sku', array('like' => $sku));
            if(count($inventoryItemCollection) > 0) {
                $lastCost = (float)$inventoryItemCollection->getFirstItem()->getLastCost();
            }else{
                $lastCost = 0;
            }
            $price = $orderItem->getPrice();
            $minPrice = (float)$lastCost*(100+$percent)/100;

            if($minPrice > $price){
                $skusHasErrorCost .= $skusHasErrorCost.$sku.', ';
            }
        }

        $skusHasErrorCost = trim($skusHasErrorCost);
        $skusHasErrorCost = trim($skusHasErrorCost, ',');
        return $skusHasErrorCost;
    }

    public function updateOrderGrid($order, $column_name){

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $table = $resource->getTableName('sales/order_grid');
        $query = "UPDATE {$table} SET ".$column_name." = '".$order->getData($column_name)."' WHERE entity_id = '" .$order->getId()."'";
        $writeConnection->query($query);
    }

    public function getImageByColor($product, $sku, $imageUrl = null){
        $SkuParts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
        $realSku = $SkuParts[0];
        if($imageUrl == null) {
            if ($product == null) {
                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $realSku);
            }
            if($product && $product->getId()) {
                $product->setSku($sku);
                return Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(120, 120);
            }
        }

        return $imageUrl;
    }
    public function getFullImageByColor($product, $sku, $imageUrl = null){
        $SkuParts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
        $realSku = $SkuParts[0];
        if($imageUrl == null) {
            if ($product == null) {
                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $realSku);
            }
            if($product && $product->getId()) {
                $product->setSku($sku);
                return Mage::helper('catalog/image')->init($product, 'thumbnail');
            }
        }

        return $imageUrl;
    }

    public function haveTransactionToCapture($order){
        $payment = $order->getPayment();
        $authTransaction = $payment->getAuthorizationTransaction();

        if(!$authTransaction) return false;
        return true;
    }

    public function tryInvoiceAndCaptureOrder($order){
        Mage::log('Capture order '.$order->getIncrementId(), null, 'auto_charge_cc.log');
        try {
            if($order->canInvoice()) {
                $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);

                $invoice->register();

                $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($order);
                $transactionSave->save();
            }
        }catch(Exception $e){
            Mage::log('Failed auto charge order '.$order->getIncrementId(), null, 'auto_charge_cc.log');
            Mage::log($e->getMessage(), null, 'auto_charge_cc.log');

            Mage::getSingleton('adminhtml/session')
                ->addError('Failed auto charge order, '.$e->getMessage());

            //if error, double check invoiced qty
            $this->syncInvoicedQty($order);
        }
    }

    public function syncInvoicedQty($order){
        foreach ($order->getAllItems() as $item){
            $id = $item->getId();
            $invoicedItem = Mage::getModel('sales/order_invoice_item')->load($id, 'order_item_id');
            if(!$invoicedItem || !$invoicedItem->getId()){
                $item->setQtyInvoiced(0)
                    ->settax_invoiced(0)->setbase_tax_invoiced(0)
                    ->setdiscount_invoiced(0)->setbase_discount_invoiced(0)
                    ->setrow_invoiced(0)->setbase_row_invoiced(0)
                    ->sethidden_tax_invoiced(0)->setbase_hidden_tax_invoiced(0)
                    ->save();
            }
        }
    }

    public function changeOrderStage($order, $stage){
        $currentStage = $order->getOrderStage();
        $current_internal_status = $order->getInternalStatus();
        //return if no stage change
        if($currentStage == $stage) return '';

        foreach (self::IMPOSSIBLE_STAGE_CHANGES as $key => $value){
            if($currentStage == $key && $stage == $value){
                return 'Cannot change to selected stage';
            }
        }
        //actions
        $newStatus = null;
        //verification => processing
        if($currentStage == 0 && $stage == 1) {
            if(in_array($order->getVerification(), self::VERIFICATION_STATUS_NOT_ALLOWED_TO_MOVE_TO_PROCESSING)){
                return 'Order not verified. To proceed it must be approved by Management.';
            }
            //auto create invoice and capture payment
            if(Mage::helper('opentechiz_salesextend/orderValidation')->checkIfCanAutoChargeCCOnManualStageChange($order)){
                $this->tryInvoiceAndCaptureOrder($order);

                //move to await CC if failed charge
                if($order->getOrderType() == self::ORDER_TYPE_STANDARD_CODE) {
                    $totalPaid = round($this->getTotalPaymentRecordForOrder($order), 2);
                    $orderTotal = round($order->getBaseGrandTotal(), 2);
                    if ($totalPaid < $orderTotal) {
                        $this->updateStageAndStatus($order, self::ORDER_VERIFICATION, 32);
                        return 'Order was not charged';
                    }
                }
            }
        }
        //processing => verification
        if($currentStage == 1 && $stage == 0) {
            $this->updateOrderVerification($order);
        }
        //processing => fulfillment item collection
        if($currentStage == 1 && $stage == 2) {
            if($order->getOrderType() == 1){ #1960 prevent user from moving to Fulfillment if not 50% paid
                $totalPaid = $this->getTotalPaymentRecordForOrder($order);
                $halfTotal = $order->getBaseGrandTotal()/2;
                if($totalPaid < $halfTotal){
                    return 'You can not move this Layaway order into Fulfillment stage because we haven\'t 
                    collected 50% of payments from the customer. If you need to order an item on this order, 
                    please contact management';
                }
            }
            if($order->getOrderType() == 2){
                $description = '';
                $orderItemId = 0;
                //get descriptiton contain return data
                foreach ($order->getAllItems() as $item) {
                    if (strpos($item->getDescription(), '#') !== false) {
                        $description = $item->getDescription();
                    }
                }
                //get return order item id
                foreach ($order->getAllItems() as $item) {
                    if($item->getSku() != OpenTechiz_Return_Helper_Data::RETURN_FEE_ITEM_SKU){
                        $orderItemId = $item->getId();
                    }
                }
                if($description == ''){
                    return 'Cannot find return corresponding to this order';
                }
                $returnIncrement = explode(' ', $description);
                $returnIncrement = $returnIncrement[count($returnIncrement) - 1];
                $returnIncrement = trim($returnIncrement, '#');
                $return = Mage::getModel('opentechiz_return/return')->load($returnIncrement, 'increment_id');
                $itemBarcode = $return->getItemBarcode();
                if(!$itemBarcode){
                    return 'No barcode found for returned item';
                }
                $item = Mage::getModel('opentechiz_inventory/item')->load($itemBarcode, 'barcode');
                $itemId = $item->getId();
                //delete old order-item link
                $oldOrderLink = Mage::getModel('opentechiz_inventory/order')->getCollection()->addFieldToFilter('item_id', $itemId)
                    ->addFieldToFilter('order_item_id', $return->getOrderItemId())->getFirstItem();
                if($oldOrderLink && $oldOrderLink->getId()){
                    $oldOrderLink->delete();
                }

                //add item-order_item link to new order item
                $data = [];
                $data['order_item_id'] = $orderItemId;
                $data['item_id'] = $itemId;
                Mage::getModel('opentechiz_inventory/order')->setData($data)->save();
                //change order item id in production request
                $productionRequest = Mage::getModel('opentechiz_production/product')->getCollection()
                    ->addFieldtoFilter('barcode',$itemBarcode)
                    ->addFieldtoFilter('status',array('neq'=>'50'))
                    ->addFieldtoFilter('item_state',array('in'=>['0','4']))
                    ->setOrder('created_at', 'DESC')->getFirstItem();
                $productionRequest->setOrderItemId($orderItemId)->save();
                $newStatus = 17;
            }else {
                if (!$order->getAllItems() || count($order->getAllItems()) == 0) {
                    $order = Mage::getModel('sales/order')->load($order->getId());
                }
                $costcheck = $this->checkProfitOrder($order);
                if ($costcheck != '') {
                    return 'There are item(s) have error cost/profit: ' . $costcheck;
                }

                foreach ($order->getAllItems() as $item) {
                    //prevent adding non-product
                    if ($item->getSku() == OpenTechiz_Return_Helper_Data::RETURN_FEE_ITEM_SKU) {
                        continue;
                    }
                    $productId = $item->getProductId();
                    if(!$productId || !Mage::getModel('catalog/product')->load($productId) || $item->getProductType() == 'virtual'){
                        continue;
                    }

                    $itemModel = Mage::getModel('opentechiz_salesextend/item');
                    $itemModel->requestToProcess($item->getId());
                    $item->setIsRequestedToProcess(1)->save();
                }
            }

        }
        //fulfillment => processing new
        if($currentStage == 2 && $stage == 1) {
            if (!$order->getAllItems() || count($order->getAllItems()) == 0) {
                $order = Mage::getModel('sales/order')->load($order->getId());
            }
            $orderItems = $order->getAllItems();
            //get all affected items
            $itemIdsArray = array();
            $itemIds = array();
            foreach ($orderItems as $orderItem){
                $orderItemId = $orderItem->getId();
                $linkCollection = Mage::getModel('opentechiz_inventory/order')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItemId);
                $itemOrders = $linkCollection;
                foreach ($linkCollection as $link){
                    $itemIdsArray[$link->getItemId()] = $orderItem->getProductId();
                    $link->delete(); //delete item-orderItem link
                }
                // add log stock
                $qty_released = 0;
                if(count($itemOrders) > 0){
                    $itemIds = [];
                    foreach ($itemOrders as $itemOrder){
                        array_push($itemIds, $itemOrder->getItemId());
                    }
                    $qty_released = Mage::getModel('opentechiz_inventory/item')->getCollection()
                        ->addFieldToFilter('item_id', array('in' => $itemIds))
                        ->getSize();
                }
                if($qty_released > 0){
                   $requestCollection = Mage::getModel('opentechiz_inventory/inventory_instockRequest')->getCollection()
                            ->addFieldToFilter('order_item_id', $orderItem->getId());
                    if(count($requestCollection) > 0){
                        foreach ($requestCollection as $request){
                            //release stock
                            $request_product =Mage::getModel('opentechiz_inventory/inventory_instockRequest')->load($request->getId());
                            $stock = Mage::getModel('opentechiz_inventory/instock')->load($request_product->getInstockProductId());
                            Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                                'qty' => (int)$qty_released,
                                'on_hold' => 0,
                                'type' => 18,
                                'stock_id' => $stock->getId(),
                                'qty_before_movement' => $stock->getQty(),
                                'on_hold_before_movement' => $stock->getOnHold(),
                                'sku' => $stock->getSku(),
                                'order_id' => $order->getId()
                            ));
                        }
                    }
                }
            }
            foreach ($itemIdsArray as $key => $value){
                array_push($itemIds, $key);
            }
            $itemCollection = Mage::getModel('opentechiz_inventory/item')->getCollection()
                ->addFieldToFilter('item_id', array('in' => $itemIds));


            foreach ($itemCollection as $item){
                if(in_array($item->getState(), array(1, 10, 25, 26))) { //reserved, ship ready, repairing
                    $item->setState(35)->save(); //in stock
                }
            }

            //delete old process request
            foreach ($orderItems as $orderItem) {
                $orderItem->setIsRequestedToProcess(0)->save();
            }
            $processRequestCollection = Mage::getModel('opentechiz_production/process_request')->getCollection()
                ->addFieldToFilter('order_id', $order->getId());
            foreach ($processRequestCollection as $processRequest){
                $processRequest->delete();
            }
            foreach ($orderItems as $orderItem) {
                //delete instock request
                $instockRequestCollection = Mage::getModel('opentechiz_inventory/inventory_instockRequest')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItem->getId());
                foreach ($instockRequestCollection as $instockRequest){
                    $instockRequest->delete();
                }

                //remove order item id from production request
                $productionRequestCollection = Mage::getModel('opentechiz_production/product')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItem->getId());
                foreach ($productionRequestCollection as $productionRequest){
                    if($productionRequest->getProductionType() == 5){
                        $productionRequest->delete();
                    }else {
                        $productionRequest->setOrderItemId(null)->save();
                    }
                }
            }

            //remove status resize/engrave from passed list
            $this->removeStatusFromOrder(3, $order);
        }
        //fulfillment => billing item ready
        if($currentStage == 2 && $stage == 3) {
            $items = $order->getAllItems();
            //check if item(s) have engraving option
            if($this->needEngrave($order)){
                return 'There are item(s) that have engraving option selected.';
            }
            //check if available in stock with exact sku
            if($this->needResize($order)){
                return 'There are item(s) don\'t have enough qty of the same size in stock. You need to process manually to resize item(s).';
            }
            //check if order already have item in repair
            if($this->isOrderHasItemInRepair($order)){
                return 'Order already have items in repair, please proceed with those items first.';
            }

            //delete all existing stock request
            foreach ($items as $item){
                $requestCollection = Mage::getModel('opentechiz_inventory/inventory_instockRequest')->getCollection()
                    ->addFieldToFilter('order_item_id', $item->getId());
                if(count($requestCollection) > 0){
                    foreach ($requestCollection as $request){
                        //release stock
                        Mage::getModel('opentechiz_inventory/inventory_instockRequest')->releaseItem($request->getId());
                        $request->delete();
                    }
                }
            }
            //add stock request
            foreach ($items as $item) {
                $productId = $item->getProductId();
                if(!$productId || !Mage::getModel('catalog/product')->load($productId) || $item->getProductType() == 'virtual'){
                    continue;
                }
                $totalQty = (int)$item->getQtyOrdered() - $item->getQtyRefunded() - $item->getQtyCanceled();
                for ($i = 0; $i < $totalQty; $i++){
                    $create = Mage::getModel('opentechiz_production/process_request')->createStockProductRequest($item->getId(),$order->getId());
                    if (!$create) {
                        return 'Something\'s wrong when creating stock request.';
                        break;
                    }
                }
            }
            //accept said stock request
            foreach ($items as $item) {
                $productId = $item->getProductId();
                if(!$productId || !Mage::getModel('catalog/product')->load($productId) || $item->getProductType() == 'virtual'){
                    continue;
                }
                $requestCollection = Mage::getModel('opentechiz_inventory/inventory_instockRequest')->getCollection()
                    ->addFieldToFilter('order_item_id', $item->getId());
                foreach ($requestCollection as $request){
                    $error = Mage::getModel('opentechiz_inventory/inventory_instockRequest')->takeItem($request->getId());
                    if($error != ''){
                        return $error;
                        break;
                    }
                    $request->setStatus(1)->save();
                }
            }
            foreach ($items as $item) {
                $productId = $item->getProductId();
                if(!$productId || !Mage::getModel('catalog/product')->load($productId) || $item->getProductType() == 'virtual'){
                    continue;
                }
                //set requested to process true
                // MUST RELOAD MODEL TO SAVE THE DATA FROM OBSERVER BECAUSE IT WILL RESET DATA
                $itemLoad = Mage::getModel('sales/order_item')->load($item->getId());
                $itemLoad->setIsRequestedToProcess(1)->save();
                //delete all process request
                $processRequests = Mage::getModel('opentechiz_production/process_request')->getCollection()
                    ->addFieldToFilter('order_item_id', $item->getId());
                if (count($processRequests) > 0) {
                    foreach ($processRequests as $processRequest) {
                        $processRequest->delete();
                    }
                }
            }
        }
        //billing => fulfillment item collection
        if($currentStage == 3 && $stage == 2) {
            if (!$order->getAllItems() || count($order->getAllItems()) == 0) {
                $order = Mage::getModel('sales/order')->load($order->getId());
            }
            $orderItems = $order->getAllItems();
            //get all affected items
            $itemIdsArray = array();
            $itemIds = array();
            foreach ($orderItems as $orderItem){
                $orderItemId = $orderItem->getId();
                $linkCollection = Mage::getModel('opentechiz_inventory/order')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItemId);
                foreach ($linkCollection as $link){
                    $itemIdsArray[$link->getItemId()] = $orderItem->getProductId();
                    $link->delete(); //delete item-orderItem link
                }
                //log stock
                $stock = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', $orderItem->getSku())->getFirstItem();
                Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                    'qty' => (int)$orderItem->getQtyOrdered(),
                    'on_hold' => '0',
                    'type' => 14,
                    'stock_id' => $stock->getId(),
                    'qty_before_movement' => $stock->getQty(),
                    'on_hold_before_movement' => $stock->getOnHold(),
                    'sku' => $stock->getSku(),
                    'order_id' => $order->getId()
                ));
            }
            foreach ($itemIdsArray as $key => $value){
                array_push($itemIds, $key);
            }

            //items go back to stock
            $itemCollection = Mage::getModel('opentechiz_inventory/item')->getCollection()
                ->addFieldToFilter('item_id', array('in' => $itemIds));
            foreach ($itemCollection as $item){
                $item->setState(35)->save(); //in stock
            }

            //delete old process request
            foreach ($orderItems as $orderItem) {
                $orderItem->setIsRequestedToProcess(0)->save();
            }
            $processRequestCollection = Mage::getModel('opentechiz_production/process_request')->getCollection()
                ->addFieldToFilter('order_id', $order->getId());
            foreach ($processRequestCollection as $processRequest){
                $processRequest->delete();
            }
            foreach ($orderItems as $orderItem) {
                //delete instock request
                $instockRequestCollection = Mage::getModel('opentechiz_inventory/inventory_instockRequest')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItem->getId());
                foreach ($instockRequestCollection as $instockRequest){
                    $instockRequest->delete();
                }

                //remove order item id from production request
                $productionRequestCollection = Mage::getModel('opentechiz_production/product')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItem->getId());
                foreach ($productionRequestCollection as $productionRequest){
                    $productionRequest->setOrderItemId(null)->save();
                }
            }
            //remove status items ready
            $orderStatuses = $order->getInternalStatuses();
            $orderStatuses = explode(',', $orderStatuses);
            foreach ($orderStatuses as $key => $value){
                if($value == 6) unset($orderStatuses[$key]);
                if($value == 2) unset($orderStatuses[$key]);
            }
            $orderStatuses = implode(',', $orderStatuses);
            $order->setInternalStatuses($orderStatuses)->save();
            //add new process request
            foreach ($orderItems as $orderItem) {
                $productId = $orderItem->getProductId();
                if(!$productId || !Mage::getModel('catalog/product')->load($productId) || $orderItem->getProductType() == 'virtual'){
                    continue;
                }
                $itemModel = Mage::getModel('opentechiz_salesextend/item');
                $itemModel->requestToProcess($orderItem->getId());
                $orderItem->setIsRequestedToProcess(1)->save();
            }

            //remove status resize/engrave from passed list
            $this->removeStatusFromOrder(3, $order);
        }
        //billing => prep ship payment complete
        if($currentStage == 3 && $stage == 4) {
            //if check cost at shipping, re-enable cost check
            foreach ($order->getAllItems() as $item){
                $item->setSkipCostCheck(0)
                    ->setCheckCostAtShipping(0)
                    ->save();
            }
        }
//        }
        //prep ship => invoice created
        //just save

        //update stage and status
        if($newStatus == null){
            $newStatus = $this->getDefaultStatusForStage($order)[$stage];
        }
        $isManual = true;
        if($currentStage == 2 && $stage == 3) $isManual = false; //exception for items ready status to check for ship prep

        $this->updateStageAndStatus($order, $stage, $newStatus, $isManual, true);
        return '';
    }

    public function updateOrderVerification($order, $is_order_paid = false)
    {
        $verificationStatus = $order->getVerification(); //default
        $paymentMethod = $order->getPayment()->getMethod();
        //get verification status
        if ($paymentMethod == 'authorizenet') {
            // Only for CC
            $customer_id = $order->getCustomerId();
            $customer = Mage::getModel('customer/customer')->load($customer_id);
            if ($customer->getIsFraudster()) {
                $verificationStatus = 8; // Contact Customer
            }
            $case = Mage::getModel('signifyd_connect/case')->load($order->getIncrementId());
            $guarantee = str_replace(' ', '', $case->getGuarantee());
            if ($case && $case->getOrderIncrement()) {
                if ($guarantee == 'DECLINED' || $guarantee == 'CANCELED') {
                    $verificationStatus = 4; // Manual Verification
                } elseif ($guarantee == 'APPROVED') {
                    // $verificationStatus = 7; // Auto Verified
                    $verificationStatus = 2; // Guaranteed
                }
            }
            if ($this->isTrustedCustomer($customer) && $this->isInfoMatch($customer)) {
                // $verificationStatus = 7; // Auto Verified
                $verificationStatus = 1; // Auto Approval
            }
        } elseif ($paymentMethod == 'paypal_express') {
            $verificationStatus = 2; // Guaranteed
        }
        if (in_array($order->getOrderType(), array(1, 2))) {
            $verificationStatus = 9; // Not Applicable
        }
        if(in_array($verificationStatus, array(0, 12)) && $is_order_paid) {
            $verificationStatus = 2; // Guaranteed
        }
        $this->updateTermAndVerification($order, null, $verificationStatus);
    }

    public function isOrderPaid($order) {
        $paidAmount = $this->getTotalPaymentRecordForOrder($order);
        return round($paidAmount, 4) >= round($order->getBaseGrandTotal(), 4);
    }

    public function checkReservedItems($order){
        $reservedItemState = OpenTechiz_Inventory_Helper_Data::RESERVED_STATE;
        foreach ($order->getAllItems() as $item){
            $orderItemId = $item->getId();
            $qty = $item->getQtyOrdered() - $item->getQtyRefunded() - $item->getQtyCanceled();
            $reservedOrderItem = Mage::getModel('opentechiz_inventory/order')->getCollection()
                ->addFieldToFilter('order_item_id', $orderItemId)
                ->join(array('item' => 'opentechiz_inventory/item'), 'item.item_id = main_table.item_Id', '*')
                ->addFieldToFilter('state', array('in' => $reservedItemState));
            if(count($reservedOrderItem) != $qty){
                return false;
            }
        }
        return true;
    }

    public function updateStageAndStatus($order, $stage = null, $status = null, $isManual = null, $isUser = false){
        $order_stage_before_save = $order->getOrderStage();
        $order_internal_status_before_save = $order->getInternalStatus();
        if($stage === null && $status === null) return;
        if($status !== null && in_array($order->getOrderType(), array(2)) && !array_key_exists($status, self::REPAIR_ORDER_INTERNAL_STATUS)) return;
        //check if status change is valid
        $currentStage = $stage;
        if($currentStage === null){
            $currentStage = $order->getOrderStage();
            $type = $order->getOrderType();
            if(!isset(OpenTechiz_SalesExtend_Helper_Data::STATUS_STAGE_TYPE_RELATION[$type][$currentStage])) return;
            if(!in_array($status, OpenTechiz_SalesExtend_Helper_Data::STATUS_STAGE_TYPE_RELATION[$type][$currentStage])) return;
        }

        //return if cannot changed auto
        if(!$isManual && !$this->canAutoChangeOrderStageStatus($order, $stage, $status)){
            return;
        }

        //prevent change to same status multiple time in one request
        $flagName = 'updateOrder'.$order->getId().'ToStage'.$stage.'Status'.$status;
        if(!Mage::registry($flagName)){
            Mage::register($flagName, true);
        }else{
            return;
        }

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        Mage::dispatchEvent('opentechiz_order_stage_status_change_before', array(
            'order'=>$order, 'stage' => $stage, 'status' => $status, 'user' => $isUser
        ));

        $data = [];
        if($stage !== null){
            $data['order_stage'] = $stage;
        }
        if($status !== null){
            $data['internal_status'] = $status;
        }
        //save last history data
        $newLog = Mage::getModel('opentechiz_reportExtend/log_orderStatus')->getCollection()
            ->addFieldToFilter('order_id', $order->getIncrementId())->setOrder('id', Varien_Data_Collection::SORT_ORDER_DESC)
            ->getFirstItem();

        if($newLog && $newLog->getId()) {
            $data['last_history_id'] = $newLog->getId();
        }
        $order->addData($data)->save();

        //log if order stage & status is actually saved
        $query = "select order_stage, internal_status from tsht_sales_flat_order where entity_id = ".$order->getId();
        $orderData = $readConnection->fetchAll($query);
        Mage::log('line 1350', null, 'log_order_stage_status_after_saved.log');
        Mage::log($orderData, null, 'log_order_stage_status_after_saved.log');

        if($stage !== null){
            $this->updateOrderGrid($order, 'order_stage');
        }
        if($status !== null){
            $this->updateOrderGrid($order, 'internal_status');
            $this->addStatusToOrder($status, $order);
        }

        $order->setIsManual($isManual);

        Mage::dispatchEvent('opentechiz_order_stage_status_change_after', array('order'=>$order,'stage' => $order->getOrderStage(),'stage_before'=>$order_stage_before_save,'internal_status_before'=>$order_internal_status_before_save));

        //log if order stage & status is actually saved
        $query = "select order_stage, internal_status from tsht_sales_flat_order where entity_id = ".$order->getId();
        $orderData = $readConnection->fetchAll($query);
        Mage::log('line 1366', null, 'log_order_stage_status_after_saved.log');
        Mage::log($orderData, null, 'log_order_stage_status_after_saved.log');
    }

    public function updateTermAndVerification($order, $term = null, $verification = null, $isUser = false){
        if($term === null && $verification === null) return;

        //prevent change to same status multiple time in one request
        $flagName = 'updateOrder'.$order->getId().'ToTerm'.$term.'Verification'.$verification;
        if(!Mage::registry($flagName)){
            Mage::register($flagName, true);
        }else{
            return;
        }

        Mage::dispatchEvent('opentechiz_order_term_verification_change_before', array(
            'order'=>$order, 'term' => $term, 'verification' => $verification, 'user' => $isUser
        ));

        $data = [];
        if($term !== null){
            $data['term'] = $verification;
        }
        if($verification !== null){
            $data['verification'] = $verification;
        }

        //save last history data
        $newLog = Mage::getModel('opentechiz_reportExtend/log_orderStatus')->getCollection()
            ->addFieldToFilter('order_id', $order->getIncrementId())->setOrder('id', 'DESC')
            ->getFirstItem();
        if($newLog && $newLog->getId()) {
            $data['last_history_id'] = $newLog->getId();
        }

        $order->addData($data)->save();

        //log if order verification is actually saved
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $query = "select verification from tsht_sales_flat_order where entity_id = ".$order->getId();
        $orderData = $readConnection->fetchAll($query);
        Mage::log('line 1410', null, 'log_order_stage_status_after_saved.log');
        Mage::log($orderData, null, 'log_order_stage_status_after_saved.log');
    }

    public function addStatusToOrder($status, $order){
        $orderStatuses = $order->getInternalStatuses();
        if($orderStatuses == '' || $orderStatuses == null || !$orderStatuses){
            $orderStatuses = $status;
        }else{
            $orderStatuses = explode(',', $orderStatuses);
            if(!in_array($status, $orderStatuses)){
                array_push($orderStatuses, $status);
            }
            $orderStatuses = implode(',', $orderStatuses);
        }
        $order->setInternalStatuses($orderStatuses)->save();
    }

    public function removeStatusFromOrder($status, $order){
        $orderStatuses = $order->getInternalStatuses();
        $orderStatuses = explode(',', $orderStatuses);
        $needUpdate = false;
        foreach ($orderStatuses as $key => $value){
            if($status ==  $value){
                unset($orderStatuses[$key]);
                $needUpdate = true;
            }
        }
        if($needUpdate) {
            $orderStatuses = implode(',', $orderStatuses);
            $order->setInternalStatuses($orderStatuses)->save();
        }
    }

    public function orderHasStatus($status, $order){
        $orderStatuses = $order->getInternalStatuses();
        if($orderStatuses == '' || $orderStatuses == null || !$orderStatuses){
            return false;
        }else{
            $orderStatuses = explode(',', $orderStatuses);
            if(!in_array($status, $orderStatuses)){
                return false;
            }else{
                return true;
            }
        }
    }

    public function isTrustedCustomer($customer){
        $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', $customer->getId())->setOrder('entity_id', 'ASC');
        foreach ($orders as $order){
            $orderTime = $order->getCreatedAt();

            if(strtotime("-5 months") >= strtotime($orderTime) &&
                ($order->getShipmentsCollection()->count() > 0)){
                return true;
            }
        }
        return false;
    }

    public function isOldCustomer($customer){
        $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', $customer->getId())->setOrder('entity_id', 'ASC');

        if(count($orders) > 1){
            return true;
        }
        else return false;
    }

    public function isInfoMatch($customer){
        $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', $customer->getId())->setOrder('entity_id', Varien_Data_Collection::SORT_ORDER_DESC);

        if(count($orders) > 1){
            //this order shipping address
            $order = $orders->getFirstItem();
            $thisShippingAddress = $order->getShippingAddress()->getData();

            //last shipped order shipping address
            /* @var $shippedOrders Mage_Sales_Model_Resource_Order_Collection */
            $shippedOrders = Mage::getResourceModel('sales/order_collection')
                ->addFieldToSelect('*')
                ->addFieldToFilter('customer_id', $customer->getId())
                ->addFieldToFilter('status', array('like' => '%complete%'))
                ->setOrder('entity_id', Varien_Data_Collection::SORT_ORDER_ASC);
            $shippedShippingAddresses = [];
            foreach ($shippedOrders as $shippedOrder){
                $orderTime = $shippedOrder->getCreatedAt();
                $currentTime = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                $currentTime = strtotime($currentTime);

                if(strtotime("-5 month", $currentTime) < strtotime($orderTime)){
                    continue;
                }

                array_push($shippedShippingAddresses, $shippedOrder->getShippingAddress()->getData());
            }

            if(count($shippedOrders) > 0) {
                $addressKeyArray = array('region', 'postcode', 'street', 'city', 'country_id');
                foreach ($shippedShippingAddresses as $shippedShippingAddress){
                    $addressDiff = false;
                    foreach (array_diff($shippedShippingAddress, $thisShippingAddress) as $key => $value){
                        if(in_array($key, $addressKeyArray)){
                            $addressDiff = true;
                        }
                    }

                    if(!$addressDiff){
                        return true;
                    }
                }
                return false;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function isInStock($order){
        $isInStock = true;
        foreach ($order->getAllItems() as $item) {
            $sku = $item->getSku();
            $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
            if(count($parts) > 1 && is_numeric($parts[count($parts) - 1])) {
                unset($parts[count($parts) - 1]);
            }
            $sku = implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $parts);

            $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
            $result = $conn->fetchAll("SELECT * FROM tsht_inventory_instock_product where sku like '" . $sku . "%' and qty > 0 and on_hold < qty");

            if(count($result) == 0){
                $isInStock = false;
                break;
            }
        }
        return $isInStock;
    }

    public function isLayawayOrderPaid($order){
        $partialPayment = Mage::getModel('partialpayment/partialpayment')->load($order->getId(), 'order_id');
        if($partialPayment && $partialPayment->getId()) {
            $installmentCollection = Mage::getModel('partialpayment/installment')->getCollection()
                ->addFieldToFilter('partial_payment_id', $partialPayment->getId());
            foreach ($installmentCollection as $installment){
                if(in_array($installment->getInstallmentStatus(), array('Remaining', 'Canceled', 'Failed'))){
                    return false;
                    break;
                }
            }
            return true;
        }else{
            return false;
        }
    }

    public function getPartialPaymentOrder($order){
        $orderId = $order->getId();
        return Mage::getModel('partialpayment/partialpayment')->load($orderId, 'order_id');
    }

    public function getTurnOffForeignCurrency()
    {
        return (bool) Mage::getStoreConfig("opentechiz_salesextend/general/turn_off_foreign_currency");
    }
    public function getAllPaymentMethod(){
        $payments = Mage::getSingleton('payment/config')->getActiveMethods();
        $methods = array();
        foreach ($payments as $paymentCode=>$paymentModel) {
            $paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
            if($paymentCode == 'affirm'){
                $paymentTitle = 'Affirm';
            }
            $methods[$paymentCode] = $paymentTitle;
        }
        return $methods;
    }
    public function getCustomerFraudster($email,$customer_id){
        if(!empty($customer_id)){
            $customer = Mage::getModel('customer/customer')->load($customer_id);
            if($customer->getId()){
                if($customer->getIsFraudster() == "1"){
                    return true;
                }
            }
        }else{
            if($email !=''){
                $customer = Mage::getModel('customer/customer')->setWebsiteId(1)->loadByEmail($email);
                if($customer->getId()){
                    if($customer->getIsFraudster() == "1"){
                        return true;
                    }
                }
            }
        }
        
        return false;
    }

    public function getTotalAllPaymentRecordCreditmemo($creditmemo){
        $paymentRecords = Mage::getModel('cod/paymentgrid')->getCollection()
            ->addFieldToFilter('credit_memo_id', $creditmemo->getId());
        $total = 0;
        foreach ($paymentRecords as $paymentRecord){
            $total += abs($paymentRecord->getTotal());
        }
        return $total;
    }

    public function getPaymentMethodForStoreCredit(){
        $methodName = self::STORE_CREDIT_PAYMENT_METHOD_FOR_RECORD;
        $paymentMethodCollection = Mage::getModel('cod/payment')->getCollection()
            ->addFieldToFilter('name', $methodName);
        if (count($paymentMethodCollection) > 0) {
            $paymentMethod = $paymentMethodCollection->getFirstItem();
        } else {
            $data = [];
            $data['name'] = $methodName;
            $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $paymentMethod = Mage::getModel('cod/payment')->setData($data)->save();
        }
        return $paymentMethod;
    }
    public function getItemIdInventory($order_item_id){
        $item_id = 0;
        $order_item = Mage::getModel('opentechiz_inventory/order')->getCollection()->addFieldtoFilter('order_item_id',$order_item_id)->getFirstItem();
        if($order_item){
            $item_id  =  $order_item->getItemId();
        }
        return $item_id;
    }
    public function getTotalPaymentRecordForOrder($order){
        $orderId = $order->getId();
        $paymentMethodCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->addFieldToFilter('order_id', $orderId);
        $total = 0;
        foreach ($paymentMethodCollection as $record){
            $total += $record->getTotal();
        }

        return $total;
    }
    public function isOrderFullyPaid($order){
        $totalPaid = $this->getTotalPaymentRecordForOrder($order);
        $grandTotal = $order->getBaseGrandTotal();

        $orderTotal = round($grandTotal, 4);
        $totalPaid = round($totalPaid, 4);

        return ($totalPaid >= $orderTotal);
    }
    public function IsAllInstock($items){
        $all_item_instock = 1;
        foreach ($items as $item) {
            $orderSku = $item->getSku();
            $qty = 0;
            $result = Mage::helper('opentechiz_inventory')->getAvailableSize($orderSku);
            if(is_array($result) || is_object($result)){
                foreach ($result as $key => $value){
                    $qty += (int)$value['qty'] - (int)$value['on_hold'];
                }
                if($qty == 0){
                    $all_item_instock = 0;
                }
            }else{
                $all_item_instock = 0;
            }

        }
        return $all_item_instock;
    }

    public function sendEmailOrderBilling($order, $days)
    {
        if (!Mage::getStoreConfigFlag("opentechiz_salesextend/notify_billing/enable")) {
            return;
        }
        $sender_email = Mage::getStoreConfig('quotes/email/sender_email_identity');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_' . $sender_email . '/email');
        $mailTemplate = Mage::getModel('core/email_template');
        $mailTemplate->setSenderName('ItsHot.com');
        $mailTemplate->setSenderEmail($senderEmail);
        $subject = Mage::getStoreConfig("opentechiz_salesextend/notify_billing/subject");
        $subject = str_replace("{order_number}", $order->getIncrementId(), $subject);
        $email_to = Mage::getStoreConfig("opentechiz_salesextend/notify_billing/email_to");
        $mailTemplate->setTemplateSubject($subject);
        $expired_date = Mage::getModel('core/date')->date('Y-m-d', strtotime($order->getCreatedAt() . " + {$days} days"));
        $mailTemplate->setTemplateText("Authorize Approval may expire soon. Charge order #{$order->getIncrementId()} before {$expired_date}.");

        try {
            $mailTemplate->send($email_to, $subject);
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
    public function haveReturnItem($order_item)
    {
        $have_return_item = 0;
        foreach($order_item as $item):
            if($item->getQtyReturned() > 0){
                $have_return_item = 1;
                break;
            }
        endforeach;
        return $have_return_item;
    }
    public function PreventChangeOrderStage($order, $stage){
        $currentStage = $order->getOrderStage();

        if($order->getOrderType() == 2){
            if(isset(self::POSSIBLE_STAGE_CHANGES_REPAIR[$currentStage])){
                if(!in_array($stage,self::POSSIBLE_STAGE_CHANGES_REPAIR[$currentStage])){
                    return 'Can not change the stage from '. self::ORDER_STAGE[$currentStage] .' to '.self::ORDER_STAGE[$stage];
                }
            }
        }else{
            if(isset(self::POSSIBLE_STAGE_CHANGES[$currentStage])){
                if(!in_array($stage,self::POSSIBLE_STAGE_CHANGES[$currentStage])){
                    return 'Can not change the stage from '.self::ORDER_STAGE[$currentStage] .' to '.self::ORDER_STAGE[$stage];
                }
            }
        }
    }

    public function getOrderStageAsString($orderStage)
    {
        return isset(self::ORDER_STAGE[$orderStage]) ? self::ORDER_STAGE[$orderStage] : '';
    }

    public function getOrderTypeAsString($orderType)
    {
        return isset(self::ORDER_TYPE[$orderType]) ? self::ORDER_TYPE[$orderType] : '';
    }

}
