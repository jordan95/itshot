<?php

class OpenTechiz_SalesExtend_Helper_Email extends Mage_Core_Helper_Abstract
{
    public function emailNotiOrderStatusChange($order, $oldStatus, $newStatus, $reason){
        try {
            // Set sender information
            $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
            $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

            // Getting recipient E-Mail
            $to = explode(',', Mage::getStoreConfig('opentechiz_salesextend/waiting_on_vendor/status_noti_email'));

            $emailTemplate = Mage::getModel('core/email_template');
            $emailTemplate->loadDefault('order_status_change_noti_tpl');
            $emailTemplate->setTemplateSubject('Order Internal Status Change Notification (' . Mage::helper('core')->formatDate() . ')');

            $emailTemplate->setSenderName($senderName);
            $emailTemplate->setSenderEmail($senderEmail);

            $emailTemplateVariables = array();
            $emailTemplateVariables['order_id'] = $order->getIncrementId();
            $emailTemplateVariables['old_status'] = OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS[$oldStatus];
            $emailTemplateVariables['new_status'] = OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS[$newStatus];
            $emailTemplateVariables['reason'] = $reason;

            foreach ($to as $recipientEmail) {
                if ($recipientEmail) {
                    $emailTemplate->send($recipientEmail, 'System', $emailTemplateVariables);
                }
            }
        }catch (Exception $e){
            return;
        }
    }

    public function getCashOrderData(){
        $cashAndCheckMethods = array('money', 'check', 'cash');
        $orderStatusToExclude = "('complete', 'canceled', 'closed', 'holded')";

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $data = [];
        foreach ($cashAndCheckMethods as $cashAndCheckMethod){
            $query = "SELECT *  FROM `tsht_sales_flat_order_payment` 
                      join tsht_sales_flat_order on tsht_sales_flat_order_payment.parent_id = tsht_sales_flat_order.entity_id
                      WHERE `method` LIKE '".$cashAndCheckMethod."%' and tsht_sales_flat_order.status not in ".$orderStatusToExclude."
                      and order_stage < 5 ORDER BY `tsht_sales_flat_order`.`created_at` DESC";
            $results = $readConnection->fetchAll($query);
            foreach ($results as $result){
                $orderData = [];
                $orderData['increment_id'] = $result['increment_id'];
                $orderData['base_grand_total'] = round($result['base_grand_total'],2);
                $orderData['created_at'] = $result['created_at'];
                $orderData['created_at'] = date('m/d/Y', strtotime($orderData['created_at']));
                $orderData['order_stage'] = OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE[$result['order_stage']];
                $statusArray = Mage::helper('opentechiz_salesextend')->getStatusArrayByType($result['order_type']);
                $orderData['internal_status'] = $statusArray[$result['internal_status']];

                array_push($data, $orderData);
            }
        }
        return $data;
    }
}