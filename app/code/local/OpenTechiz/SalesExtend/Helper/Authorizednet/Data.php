<?php

class OpenTechiz_SalesExtend_Helper_Authorizednet_Data extends Mage_Core_Helper_Abstract
{
    public function deleteExistingAuthorizedDataErrorRecord($order){
        if($quoteId = $this->isAuthorizedDataCreated($order->getQuoteId())){
            $connection = Mage::helper('medialounge_checkout/payment')->getSecondConnection('write');
            $connection->delete(Mage::getModel('core/resource')->getTableName('medialounge_checkout/authorisednet_data'), 'quote_id = '.$quoteId);
        }
    }

    public function isAuthorizedDataCreated($quoteId){
        $connection = Mage::helper('medialounge_checkout/payment')->getSecondConnection('read');
        $select = $connection->select();
        $select->from(Mage::getModel('core/resource')->getTableName('medialounge_checkout/authorisednet_data'));
        $select->reset(Varien_Db_Select::COLUMNS)->columns(['quote_id']);
        $select->where('quote_id=?', $quoteId);
        $recordId = $connection->fetchOne($select);

        return $recordId;
    }
}