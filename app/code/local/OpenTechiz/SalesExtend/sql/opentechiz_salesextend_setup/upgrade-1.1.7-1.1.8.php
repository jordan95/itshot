<?php
$installer = $this;

$installer->startSetup();

if ($installer->getConnection()->tableColumnExists($installer->getTable('sales/order'), 'ship_by')) {
    if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales/order_grid'), 'ship_by')) {
        $installer->run("ALTER TABLE `{$this->getTable('sales/order_grid')}` ADD `ship_by` DATETIME NOT NULL DEFAULT current_timestamp;");
    }
    $installer->run("UPDATE `{$this->getTable('sales/order_grid')}` AS sog, `{$this->getTable('sales/order')}` AS so
        SET sog.`ship_by` = IFNULL(so.`ship_by`, 0)
        WHERE sog.`entity_id` = so.`entity_id`");
}

if ($installer->getConnection()->tableColumnExists($installer->getTable('sales/order'), 'deliver_by')) {
    if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales/order_grid'), 'deliver_by')) {
        $installer->run("ALTER TABLE `{$this->getTable('sales/order_grid')}` ADD `deliver_by` DATETIME NOT NULL DEFAULT current_timestamp;");
    }
    $installer->run("UPDATE `{$this->getTable('sales/order_grid')}` AS sog, `{$this->getTable('sales/order')}` AS so
        SET sog.`deliver_by` = IFNULL(so.`deliver_by`, 0)
        WHERE sog.`entity_id` = so.`entity_id`");
}

if ($installer->getConnection()->tableColumnExists($installer->getTable('sales/order'), 'order_stage')) {
    if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales/order_grid'), 'order_stage')) {
        $installer->run("ALTER TABLE `{$this->getTable('sales/order_grid')}` ADD `order_stage` Integer NOT NULL DEFAULT 0;");
    }
    $installer->run("UPDATE `{$this->getTable('sales/order_grid')}` AS sog, `{$this->getTable('sales/order')}` AS so
        SET sog.`order_stage` = IFNULL(so.`order_stage`, 0)
        WHERE sog.`entity_id` = so.`entity_id`");
}

if ($installer->getConnection()->tableColumnExists($installer->getTable('sales/order'), 'internal_status')) {
    if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales/order_grid'), 'internal_status')) {
        $installer->run("ALTER TABLE `{$this->getTable('sales/order_grid')}` ADD `internal_status` Integer NOT NULL DEFAULT 0;");
    }
    $installer->run("UPDATE `{$this->getTable('sales/order_grid')}` AS sog, `{$this->getTable('sales/order')}` AS so
        SET sog.`internal_status` = IFNULL(so.`internal_status`, 0)
        WHERE sog.`entity_id` = so.`entity_id`");
}

$installer->endSetup();