<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales_flat_order_grid'),'total_qty_returned', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'nullable'  => true,
        'default'   => 0,
        'comment'   => 'Total qty returned'
    ));
$installer->endSetup();