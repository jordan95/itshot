<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE `tsht_onlinebiz_payment_grid` 
    ADD `type` 
    varchar(50)
    not null
    default 'invoice';
");

$installer->endSetup();