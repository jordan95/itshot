<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('customer_entity'),'is_fraudster', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Is Fraudster'
    ));

$installer->endSetup();