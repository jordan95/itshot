<?php
$installer = $this;
$installer->startSetup();

$installer->run("ALTER TABLE `{$this->getTable('sales/order')}` ADD `internal_statuses` VARCHAR(100);");

$installer->endSetup();