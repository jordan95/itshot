<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales_flat_order'),'order_approved', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Order Approved',
        'values'    => array(
            0 => 'No',
            1 => 'Yes'
        )
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales_flat_order_grid'),'order_approved', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Order Approved',
        'values'    => array(
            0 => 'No',
            1 => 'Yes'
        )
    ));

$installer->endSetup();