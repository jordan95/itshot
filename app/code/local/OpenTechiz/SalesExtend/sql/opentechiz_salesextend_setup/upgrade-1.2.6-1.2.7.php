<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE `tsht_sales_flat_order_item` 
    ADD `qty_returned` 
    DECIMAL (12,4) 
    not null
    default 0;
");

$installer->endSetup();