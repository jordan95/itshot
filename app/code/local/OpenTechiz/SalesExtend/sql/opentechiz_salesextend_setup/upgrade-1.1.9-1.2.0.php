<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('ship_from_address')};
    CREATE TABLE {$this->getTable('ship_from_address')} (
      `id`   INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `source`          INT(10)       NOT NULL,
      `name`       VARCHAR(255)      NOT NULL,
      `company`       VARCHAR(255)           NOT NULL,
      `address_line_1`  VARCHAR(255)       NOT NULL,
      `address_line_2`  VARCHAR(255)       NULL,
      `telephone`     varchar(45)           NOT NULL,
      `city`           VARCHAR(45)           NULL,
      `state`         VARCHAR(45)       NOT NULL,
      `zipcode`       VARCHAR(10)       NULL,
      `country`       VARCHAR(45)       NOT NULL default 'US',
      `po_zip_code`       VARCHAR(255)       NOT NULL,
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
$installer->endSetup();