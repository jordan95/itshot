<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE `tsht_sales_flat_order` 
    ADD `last_history_id` 
    integer 
    not null
    default 0;
");

$installer->endSetup();