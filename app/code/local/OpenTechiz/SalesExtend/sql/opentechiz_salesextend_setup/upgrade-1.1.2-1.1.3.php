<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('sales_flat_order_grid'),'term', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Term'
    ));
$installer->getConnection()->addColumn($installer->getTable('sales_flat_order_grid'),'verification', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Verification'
    ));

$installer->endSetup();