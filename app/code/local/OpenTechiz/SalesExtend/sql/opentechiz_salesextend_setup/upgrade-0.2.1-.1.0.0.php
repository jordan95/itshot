<?php

$installer = $this;

$shipDate = date_create(Mage::getModel('core/date')->date('Y-m-d'));
date_add($shipDate, date_interval_create_from_date_string('3 days'));
$deliverDate = date_create(Mage::getModel('core/date')->date('Y-m-d'));
date_add($deliverDate, date_interval_create_from_date_string('10 days'));

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'),'ship_by', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATE,
        'nullable'  => false,
        'default'   => date_format($shipDate, 'Y-m-d'),
        'comment'   => 'Expected Ship Date'
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'),'deliver_by', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATE,
        'nullable'  => false,
        'default'   => date_format($deliverDate, 'Y-m-d'),
        'comment'   => 'Expected Deliver Date'
    ));
$installer->endSetup();