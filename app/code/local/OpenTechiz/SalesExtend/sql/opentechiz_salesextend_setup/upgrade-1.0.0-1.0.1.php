<?php
$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE `{$installer->getTable('sales/order')}` CHANGE `ship_by` `ship_by` DATETIME not NULL DEFAULT current_timestamp 
");

$installer->run("
ALTER TABLE `{$installer->getTable('sales/order')}` CHANGE `deliver_by` `deliver_by` DATETIME not NULL DEFAULT current_timestamp
");

$installer->endSetup();