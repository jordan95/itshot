<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales_flat_order_item'),'skip_cost_check', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Skip cost check'
    ));

$installer->endSetup();