<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales_flat_order_item'),'is_requested_to_process', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Is Requested To Process',
        'values'    => array(
            0 => 'No',
            1 => 'Yes'
        )
    ));

$installer->endSetup();