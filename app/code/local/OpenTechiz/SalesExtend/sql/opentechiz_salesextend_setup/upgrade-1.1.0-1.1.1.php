<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'),'term', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Term'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'),'verification', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Verification'
    ));

$installer->endSetup();