<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE `tsht_onlinebiz_payment_grid` 
    ADD `credit_memo_id` 
    integer 
    not null
    default 0;
");

$installer->endSetup();