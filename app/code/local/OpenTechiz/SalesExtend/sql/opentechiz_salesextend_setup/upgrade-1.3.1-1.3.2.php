<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales_flat_order'),'confirm_address_customer', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => true,
        'default'   => 0,
        'comment'   => 'confirm the address with the customer'
    ));
    
$installer->getConnection()
    ->addColumn($installer->getTable('sales_flat_shipment_track'),'notify_email', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => false,
        'default'   => false,
        'comment'   => 'Notify Email'
    ));
$installer->endSetup();