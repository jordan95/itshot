<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales_flat_order'),'order_type', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Order Type'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales_flat_order_grid'),'order_type', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Order Type'
    ));

$installer->endSetup();