<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE `tsht_sales_flat_order` 
    ADD `management_approved` 
    integer 
    not null
    default 0;
");

$installer->endSetup();