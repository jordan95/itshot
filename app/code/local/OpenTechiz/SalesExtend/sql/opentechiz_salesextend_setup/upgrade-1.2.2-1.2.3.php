<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE `tsht_onlinebiz_payment_grid` 
    ADD `transaction_id` 
    integer 
    null;
");

$installer->endSetup();