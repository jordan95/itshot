<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales_flat_order'),'notify_order_fulfillment', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => false,
        'default'   => false,
        'comment'   => 'Notify Order Fulfillment'
    ));
$installer->endSetup();