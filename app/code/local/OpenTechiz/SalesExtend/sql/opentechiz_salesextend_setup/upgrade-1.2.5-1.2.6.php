<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE `tsht_sales_flat_order_status_history` 
    ADD `has_pin_comment` 
    boolean
    null
    default 0;
");

$installer->endSetup();