<?php
$installer = $this;
$installer->startSetup();

$resource = Mage::getResourceModel('sales/order_collection');
if(!method_exists($resource, 'getEntity')){
    $table = $this->getTable('sales_flat_order');
    $query1 = 'ALTER TABLE `' . $table . '` ADD COLUMN `onestepcheckout_customercomment_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `onestepcheckout_customercomment`;';
    $query2 = 'ALTER TABLE `' . $table . '` ADD COLUMN `onestepcheckout_customercomment_by` INT(7) NULL AFTER `onestepcheckout_customercomment`;';
    $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    try {
        $connection->query($query1);
        $connection->query($query2);
    } catch (Exception $e) {

    }
}

$installer->endSetup();
?>