<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('signifyd_connect_case'), 'notify_signifyd_declined', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => false,
        'default'   => false,
        'comment'   => 'Notify Signifyd Declined'
    ));
$installer->endSetup();