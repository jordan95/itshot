<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE `{$installer->getTable('sales_flat_order_item')}` 
    ADD `child_order_increment_id` 
    text
    null;
");

$installer->endSetup();