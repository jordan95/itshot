<?php

class OpenTechiz_PartialPaymentSpliter_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getInstallmentsByPartialId($partial_payment_id){
        $collection = Mage::getModel('partialpayment/installment')->getCollection()
                            ->addFieldToFilter('partial_payment_id', $partial_payment_id);
        if ($collection->getSize()) {
            return $collection;
        }
        return false;
    }

    public function getInstallmentIdsRemaining($partial_payment_id){
    	$collection = $this->getInstallmentsByPartialId($partial_payment_id);
    	$collection->addFieldToFilter('installment_status','Remaining');
        $result = array();
        $i = 1;
        foreach ($collection as $installment) {
            $result[$i] = $installment->getId();
            $i++;
        }
        return $result;
    }

    public function isAllowSplitPaymentPrice($partial_payment,$datas){
        $remaining_amount = $partial_payment->getData('remaining_amount');
        $total_amount_post = 0;
        foreach ($datas as $data) {
            if ($data['installment_amount']) {
                $total_amount_post += $data['installment_amount'];
            }
        }

        if ($total_amount_post > $remaining_amount) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpaymentspliter')->__('Can not split item with amount submit is bigger than the total remaining amount.'));
            return false;
        }
        return true;
    }
}
