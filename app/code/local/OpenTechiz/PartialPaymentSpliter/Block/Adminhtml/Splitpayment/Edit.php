<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class OpenTechiz_PartialPaymentSpliter_Block_Adminhtml_Splitpayment_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'partialpaymentspliter';
        $this->_controller = 'adminhtml_splitpayment';

        $this->_removeButton('delete');
        $this->_removeButton('reset');
        $this->_removeButton('back');
        $this->_removeButton('save');

        $partial_payment_id = $this->getRequest()->getParam('id');

        $url_back = $this->getUrl('*/partialpayment/edit', array("id" => $partial_payment_id));
        $this->_addButton('back', array(
            'label'   => Mage::helper('adminhtml')->__('Back'),
            'onclick' => "setLocation('{$url_back}')",
            'class'   => 'back'
        ));

        $this->addButton('save_splitpayment', array(
            'label' => Mage::helper('opentechiz_purchase')->__('Save'),
            'class' => '',
            'onclick' => "submitFormEvent(this,'". $this->getUrl('*/splitpayment/save',array("id" => $partial_payment_id)) ."')"
        ));
        
        
    }

    public function getHeaderText()
    {
        if( Mage::registry('partialpayment_data') && Mage::registry('partialpayment_data')->getId() ) 
		{
            return Mage::helper('partialpaymentspliter')->__("Split Partial Payment Order's Information", $this->htmlEscape(Mage::registry('partialpayment_data')->getTitle()));
        } 
		else 
		{
            return Mage::helper('partialpaymentspliter')->__('Split Partial Payment');
        }
    }
}