<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class OpenTechiz_PartialPaymentSpliter_Block_Adminhtml_Splitpayment_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('partialpaymentspliter_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('partialpaymentspliter')->__('Partial Payment'));
	}

	protected function _beforeToHtml()
	{
		$this->addTab('form_section', array(
		  'label'     => Mage::helper('partialpaymentspliter')->__('Information'),
		  'title'     => Mage::helper('partialpaymentspliter')->__('Information'),
		  'content'   => $this->getLayout()->createBlock('partialpaymentspliter/adminhtml_splitpayment_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}