<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class OpenTechiz_PartialPaymentSpliter_Block_Adminhtml_Splitpayment_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{	 
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$this->setTemplate('opentechiz/partialpayment/split_partial_payment.phtml');
		return parent::_prepareForm();
	}
}