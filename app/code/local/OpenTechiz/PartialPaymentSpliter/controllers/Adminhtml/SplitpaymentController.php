<?php
require_once(Mage::getModuleDir('controllers','Milople_Partialpayment').DS.'Adminhtml'.DS.'PartialpaymentController.php');
class OpenTechiz_PartialPaymentSpliter_Adminhtml_SplitpaymentController extends Milople_Partialpayment_Adminhtml_PartialpaymentController
{
    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            default:
                $aclResource = 'admin/partialpaymenttop/split_partialpayment';
                break;
        }
        return Mage::getSingleton('admin/session')->isAllowed($aclResource);
    }

    public function indexAction(){
        $this->loadLayout();
        $this->_setActiveMenu('partialpayment/items');
        $this->_addContent($this->getLayout()->createBlock('partialpaymentspliter/adminhtml_splitpayment_edit'))
            ->_addLeft($this->getLayout()->createBlock('partialpaymentspliter/adminhtml_splitpayment_edit_tabs'));

        $this->getLayout()->getBlock('head')->addJs('lib/jquery/jquery-1.10.2.min.js');
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/noconflict.js');
        $this->renderLayout();
    }

    public function getHelper()
    {
        return Mage::helper('partialpaymentspliter');
    }

    public function loadDataSplitPaymentAction(){
        $post = $this->getRequest()->getPost();
        $id = $this->getRequest()->getPost('partial_payment_id');
        $part_number = $this->getRequest()->getPost('part_number');
        $start_date = $this->getRequest()->getPost('start_date');
        $result = array();
        $html = '';
        if (isset($id) && isset($part_number) && isset($start_date)) {
            $collection = $this->getHelper()->getInstallmentsByPartialId($id);
            $partial_payment = Mage::getModel('partialpayment/partialpayment')->load($id);
            $remaining_amount = $partial_payment->getData('remaining_amount');
            $childPrice = Mage::app()->getStore()->roundPrice($remaining_amount / $part_number);
            $installment_ids = $this->getHelper()->getInstallmentIdsRemaining($id);
            $totalPricePost = 0;

            $html .= '<table>';
            $html .= '<tr class="headings">';
            $html .= '<th class="a-center">Installment Amount</th>';
            $html .= '<th class="a-center">Due Date</th>';
            $html .= '</tr>';
            for ($i=1; $i <= $part_number ; $i++) {
                $totalPricePost = $totalPricePost + $childPrice;
                if ($i == $part_number) {
                    $over_price = Mage::app()->getStore()->roundPrice($totalPricePost - $remaining_amount);
                    if ($over_price > 0 && $totalPricePost > $remaining_amount) {
                        $childPrice = Mage::app()->getStore()->roundPrice($childPrice - $over_price);
                    }

                    $missing_price = Mage::app()->getStore()->roundPrice($remaining_amount - $totalPricePost);
                    if ($missing_price > 0 && $remaining_amount > $totalPricePost) {
                        $childPrice = Mage::app()->getStore()->roundPrice($childPrice + $missing_price);
                    }
                }
                
                if ($i == 1) {
                    $nextDueDate = $start_date;
                }else{
                    $dayNumber = '+' . $i .' month';
                    $nextDueDate = date('m/d/Y',strtotime($nextDueDate.'+1 month'));
                }
                $idPost = $i;
                if (isset($installment_ids[$i])) {
                    $idPost = $installment_ids[$i];
                }
                
                $html .= '<tr>';
                    $html .= '<td class="a-center">';
                        $html .='<input class="input-text required-entry" type="text" name="data['.$idPost.'][installment_amount]" value="'.$childPrice.'" />';
                        if (isset($installment_ids[$i])) {
                            $html .='<input class="input-text required-entry" type="hidden" name="data['.$idPost.'][installment_id]" value="'.$idPost.'" />';
                        }
                        
                    $html .='</td>';
                    $html .= '<td class="a-center">';
                        $html .= '<input style="margin-right:2px;" id="due_date_'.$i.'" class="input-text required-entry installment_due_date" type="text" name="data['.$idPost.'][installment_due_date]" value="'.$nextDueDate.'" />';
                        $html .= '<img src="'.Mage::getDesign()->getSkinUrl('images/grid-cal.gif').'" alt="" class="v-middle" id="due_date_trig_'.$i.'" title="Select Date">';
                        $html .= '<script>Calendar.setup({inputField: \'due_date_'.$i.'\',ifFormat: \'%m/%d/%Y\',button: \'due_date_trig_'.$i.'\',align: \'Bl\',ingleClick: true,disableFunc: disabledOldDate});</script>';
                    $html .= '</td>';
                $html .= '</tr>';
            }
            $html .= '</table>';

            $result['html'] = $html;
        }

        return $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    public function saveAction(){
        $partial_payment_id = $this->getRequest()->getParam('id');
        $dataPost = $this->getRequest()->getPost('data');
        if (isset($partial_payment_id) && isset($dataPost)) {
            
            $partial_payment = Mage::getModel('partialpayment/partialpayment')->load($partial_payment_id);
            // validate Price free when customer input
            if (!$this->getHelper()->isAllowSplitPaymentPrice($partial_payment,$dataPost)) {
                $this->_redirect('*/splitpayment/index', array('id' => $partial_payment_id));
                return;
            }
            $number = 0;
            $totalPricePost = 0;
            $countItemsPost = count($dataPost);
            $new_remaining_installments = 0;
            $remaining_amount = $partial_payment->getData('remaining_amount');
            $collection = $this->getHelper()->getInstallmentsByPartialId($partial_payment_id);
            $installments = $collection->addFieldToFilter('installment_status','Remaining');;
            // save data is exist in database
            if ($installments->getSize()) {
                foreach ($installments as $installment) {
                    $installment_id = $installment->getInstallmentId();
                    if (array_key_exists($installment_id, $dataPost)) {
                        $data = $dataPost[$installment_id];
                        $data['partial_payment_id'] = $partial_payment->getPartialPaymentId();
                        $data['installment_status'] = 'Remaining';
                        $totalPricePost = $totalPricePost + (float)$data['installment_amount'];
                        $missing_price = Mage::app()->getStore()->roundPrice($remaining_amount - $totalPricePost);
                        if ($missing_price > 0 && ++$number === $countItemsPost) {
                            $pricePost = $data['installment_amount'];
                            $data['installment_amount'] = Mage::app()->getStore()->roundPrice($pricePost + $missing_price);
                        }
                        $installment->setData($data);
                        try {
                            $installment->save();
                            $new_remaining_installments++;
                        } catch (Exception $e) {
                            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                            $this->_redirect('*/splitpayment/index', array('id' => $partial_payment_id));
                            return;
                        }
                        // remove item array post when we update successfully.
                        unset($dataPost[$installment_id]);
                    }else{
                        $installment->delete();
                    }
                }
            }

            // continue update datapost with item new.
            if (!empty($dataPost)) {
                foreach ($dataPost as $data) {
                    $installment = Mage::getModel('partialpayment/installment');
                    if (isset($data['installment_id'])) {
                        $installment->load($data['installment_id']);
                    }
                    $data['partial_payment_id'] = $partial_payment->getPartialPaymentId();
                    $data['installment_status'] = 'Remaining';
                    // we will add price missing at the last item.
                    $totalPricePost = $totalPricePost + (float)$data['installment_amount'];
                    $missing_price = Mage::app()->getStore()->roundPrice($remaining_amount - $totalPricePost);
                    if ($missing_price > 0 && ++$number === $countItemsPost) {
                        $pricePost = $data['installment_amount'];
                        $data['installment_amount'] = Mage::app()->getStore()->roundPrice($pricePost + $missing_price);
                    }
                    $installment->setData($data);
                    try {
                        $installment->save();
                        $new_remaining_installments++;
                    } catch (Exception $e) {
                        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                        $this->_redirect('*/splitpayment/index', array('id' => $partial_payment_id));
                        return;
                    }
                }
            }
            
            try {
                $new_total_installments = count($this->getHelper()->getInstallmentsByPartialId($partial_payment_id));
                $partial_payment->setData('total_installments',$new_total_installments);
                $partial_payment->setData('remaining_installments',$new_remaining_installments);
                $partial_payment->save();
                $this->savePartialpaymentProduct($partial_payment_id,$new_total_installments,$new_remaining_installments);
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/splitpayment/index', array('id' => $partial_payment_id));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('partialpaymentspliter')->__('Save partial payment successfully.'));
        $this->_redirect('*/partialpayment/edit', array('id' => $partial_payment_id));
        return;
    }

    public function savePartialpaymentProduct($partial_payment_id,$new_total_installments,$new_remaining_installments){
        if (isset($partial_payment_id)) {
            $productModel = Mage::getModel('partialpayment/product')->getCollection()
                            ->addFieldToFilter('partial_payment_id', $partial_payment_id);

            foreach($productModel as $product)
            {   
                $product->setData('total_installments',$new_total_installments);
                $product->setData('remaining_installments',$new_remaining_installments);
                try {
                    $product->save();
                } catch (Exception $e) {
                    throw new Exception($e->getMessage(), 1);
                }
            }
        }
        return;
    }
}
