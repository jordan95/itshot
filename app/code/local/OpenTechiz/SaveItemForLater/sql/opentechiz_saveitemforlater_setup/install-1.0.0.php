<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_List
 */
$this->startSetup();

$this->run("
    ALTER TABLE `{$this->getTable('amlist/item')}` ADD `type` SMALLINT NOT NULL DEFAULT 0; 
");

$this->endSetup();