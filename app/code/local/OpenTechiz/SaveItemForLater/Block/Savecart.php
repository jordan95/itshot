<?php 
class OpenTechiz_SaveItemForLater_Block_Savecart extends OpenTechiz_PersonalizedProduct_Block_Amlist_Items{

	public function getListSaveItem(){
		$allItem =array();
		if( Mage::getSingleton('customer/session')->isLoggedIn()){
	        $allItem = Mage::getModel("opentechiz_saveitemforlater/savecart")->getAllItems();
	    }
		
		return $allItem;
	}
}