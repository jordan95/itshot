<?php
class OpenTechiz_SaveItemForLater_IndexController extends Mage_Core_Controller_Front_Action
{
	protected function saveAction(){
		$collection = Mage::getModel('opentechiz_saveitemforlater/savecart')->getCollection();

	}
	public function removeItemAction() {
	    $id    = (int) $this->getRequest()->getParam('id');
	    
	    $item  = Mage::getModel('amlist/item');
	    $item->load($id);
	    if (!$item->getId()){
	        $this->_redirect('checkout/cart/');
	    }
	    
	    $list = Mage::getModel('amlist/list');
	    $list->load($item->getListId());
	    if ($list->getCustomerId() != Mage::getSingleton('customer/session')->getCustomerId()){
	        $this->_redirect('checkout/cart/');
	        return;
	    }
	    
        try {
            $item->delete();
    		Mage::getSingleton('checkout/session')->addSuccess($this->__('Product has been successfully removed'));
        }
        catch (Exception $e) {
            Mage::getSingleton('checkout/session')->addError($this->__('There was an error while removing item: %s', $e->getMessage()));
        }
        $this->_redirect('checkout/cart/');
        
	}
}