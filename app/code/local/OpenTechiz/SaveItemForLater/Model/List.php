<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_List
 */ 
class OpenTechiz_SaveItemForLater_Model_List extends Amasty_List_Model_List
{

    public function getItems()
    {
        $items = $this->getData('items');
        if (is_null($items)) {
            $collection = Mage::getResourceModel('amlist/item_collection') 
                ->addFieldToFilter('list_id', $this->getId())
                ->setOrder('item_id')
                ->load();
        
            $products = $this->_getProductsArray($collection); 
       
            $items = array();
            foreach ($collection as $item) {
                if (isset($products[$item->getProductId()])){
                    $item->setProduct($products[$item->getProductId()]);
                    $items[] = $item;
                }
            } 
            $this->setData('items', $items);
        }
        return $items;  
    }

}