<?php

class OpenTechiz_SaveItemForLater_Model_Savecart extends OpenTechiz_Quotation_Model_Wishlist
{

    public function getAllItems(){
        $listCollection = Mage::getResourceModel('amlist/list_collection')
                ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomerId())
                ->load();
            $lists = array();
            foreach ($listCollection as $list){
                $lists[$list->getId()] = $list->getTitle();
            }
            //get items from these lists
            $collection = Mage::getResourceModel('amlist/item_collection')
                ->addFieldToFilter('list_id', array('in' => array_keys($lists)))
                ->load();
            
            $items = array();
            if($collection->count()> 0 ){
                $products = $this->_getProductsArray($collection); 
                foreach ($collection as $item) {
                    if (isset($products[$item->getProductId()])){
                        $item->setProduct($products[$item->getProductId()]);
                        $items[] = $item;
                    }
                } 
            }
            return $items; 
    }
    protected function _getProductsArray($items)
    {
        $productIds = array();
        foreach ($items as $item) {
            $productIds[] = $item->getProductId();
        }
        $productIds = array_unique($productIds);
         
        $collection = Mage::getModel('catalog/product')->getResourceCollection()
             ->addIdFilter($productIds)
             ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes());
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);

        $collection->load();
        
        $products = array(); 
        foreach ($collection as $prod) {
            $products[$prod->getId()] = $prod; 
        }
        
        return $products;
    }
}