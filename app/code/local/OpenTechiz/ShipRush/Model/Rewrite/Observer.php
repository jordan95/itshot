<?php

class OpenTechiz_ShipRush_Model_Rewrite_Observer extends ShipRush_OrderButton_Model_Observer
{
    /**
     * Magento passes a Varien_Event_Observer object as
     * the first parameter of dispatched events.
     */
    public function adminhtmlWidgetContainerHtmlBefore($event) {
        $block = $event->getBlock();
        $mageordermodel = Mage::getModel('sales/order');
        $mageshipmodel = Mage::getModel('sales/order_address');
        
        $order = $mageordermodel->loadByIncrementId(100000001); //or load(1)
        
        $x = $order->getItemscollection()->getFirstItem()->sku;
        $y = 'qwerty';
        $params = Mage::app()->getRequest()->getParams();
        $onclick = "(function(e){invoke_connect()})(event)";
        if(isset($params['order_id'])){
            $currentOrderId = Mage::app()->getRequest()->getParam('order_id');
            $currentOrder = Mage::getModel('sales/order')->load($currentOrderId);
            $skus = Mage::helper('opentechiz_salesextend')->checkProfitOrder($currentOrder);
            $orderType = $currentOrder->getOrderType();
            if($currentOrder->getOrderStage() < 4){
                $onclick = 'alert(\'This order is not yet in Prep Shipment stage. \nCannot create shipment\')';
            }elseif ($skus != '' && $orderType != 2){
                $onclick = 'alert(\'These item(s) has insufficient profit compare to Last Cost: '.$skus.'\nPlease check the order again. You can skip cost check in order items view\')';
            }

            if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
                if($this->_isAllowedAction('ship') && $currentOrder->canShip()
                    && !$currentOrder->getForcedDoShipmentWithInvoice()){
                    $block->addButton('do_something_crazy', array(
                        'label'     => 'Ship w ShipRush',
                        'onclick'   => $onclick,
                        'class'     => 'myshiprush_button'
                    ),1,6);
                }
            }
        }
    }

    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/' . $action);
    }
}