<?php

class OpenTechiz_Affiliate_ClicklinkController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
    {

        $store_id = Mage::app()->getStore()->getId();
        if(Mage::helper('affiliate/data')->getEnabledStore($store_id))
        {
        	$response = new Varien_Object();
        	$request = $this->getRequest();

            $affiliate_id = $request->getParam('affiliate_id');
            $referral_to = $request->getParam('referral_to');
            $aff = Mage::getModel('migrateaff/affcustomer')->getCollection()->loadByAffId($affiliate_id);
            $realAffId = $aff->getCustomerId() ? $aff->getCustomerId() : 0;
            $customers = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->getCollection();
            $customers->addFieldToFilter('entity_id', $realAffId);

            $timeCokie = Mage::helper('affiliate/data')->getTimeCookieStore($store_id);

            if(sizeof($customers)>0)
            {
                foreach ($customers as $customer)
                {
                    $customer_id = (int)$customer->getId();
                    if(Mage::helper('affiliate')->getActiveAffiliate($customer_id) == 1 && Mage::helper('affiliate')->getLockAffiliate($customer_id) == 0)
                    {
                        $clientIP = '';
                        if(!empty(Mage::app()->getRequest()->getServer('REMOTE_ADDR'))){
                            $clientIP = Mage::app()->getRequest()->getServer('REMOTE_ADDR');
                        }
                        

                        $referral_from = '';
                        if(isset($_SERVER['HTTP_REFERER'])) {
                            $referral_from = trim($_SERVER['HTTP_REFERER']);
                        }

                        if($referral_from !=''){
                            $referral_from_domains = explode("://", $referral_from);
                            $referral_from_domain = explode("/", $referral_from_domains[1]);
                        }else{
                            $referral_from_domain[0] = '';
                        }

                        Mage::getModel('core/cookie')->set('customer',$customer_id,60*60*24*$timeCokie);
                        Mage::getModel('core/cookie')->set('mw_referral_from',$referral_from,60*60*24*$timeCokie);
                        Mage::getModel('core/cookie')->set('mw_referral_from_domain',$referral_from_domain[0],60*60*24*$timeCokie);
                        Mage::getModel('core/cookie')->set('mw_referral_to',$referral_to,60*60*24*$timeCokie);
                        $invitation_type = MW_Affiliate_Model_Typeinvitation::REFERRAL_LINK;

                        // Calculate the commission from referral visitor
                        $affiliateCustomer = Mage::getModel('affiliate/affiliatecustomers')->load($customer_id);
                        $referralVisitorCommission = Mage::helper('affiliate')->calculateReferralVisitorCommission($customer_id, $affiliateCustomer->getLinkClickIdPivot());

                        // Save new invitation to db
                        $invitationModel = Mage::getModel('affiliate/affiliateinvitation');
                        $historyData = array(
                            'customer_id'           => $customer_id,
                            'email'                 => '',
                            'status'                => MW_Affiliate_Model_Statusinvitation::CLICKLINK,
                            'ip'                    => $clientIP,
                            'count_click_link'      => 1,
                            'count_register'        => 0,
                            'count_purchase'        => 0,
                            'count_subscribe'       => 0,
                            'referral_from'         => $referral_from,
                            'referral_from_domain'  => $referral_from_domain[0],
                            'referral_to'           => $referral_to,
                            'order_id'              => '',
                            'invitation_type'       => $invitation_type,
                            'invitation_time'       => now(),
                            'commission'            => $referralVisitorCommission
                        );
                        $invitationModel->setData($historyData)->save();

                        Mage::helper('affiliate')->saveAffiliateTransactionReferral($customer_id,$referralVisitorCommission,$customer_id,$invitation_type,MW_Mwcredit_Model_Transactiontype::REFERRAL_VISITOR);

                        // If there is enough visitors to get commission then update customer overall commission
                        if($referralVisitorCommission > 0)
                        {
                            // Update link_click_id_pivot
                            $affiliateCustomer->setLinkClickIdPivot($invitationModel->getId());

                            // Update total commission in affiliate_customers
                            $currentCommission = $affiliateCustomer->getTotalCommission();
                            $affiliateCustomer->setTotalCommission($currentCommission + $referralVisitorCommission);

                            // Save affiliate_customers
                            $affiliateCustomer->save();

                            // Update customer credit
                            Mage::helper('affiliate')->insertCustomerCredit($customer_id);
                            $customerCredit = Mage::getModel('mwcredit/creditcustomer')->load($customer_id);
                            $currentCredit = $customerCredit->getCredit();
                            $newCredit = $currentCredit + $referralVisitorCommission;
                            $customerCredit->setCredit($newCredit)->save();

                            // Update credit history table
                            $creditHistoryData = array(
                                'customer_id'           => $customer_id,
                                'type_transaction'      => MW_Mwcredit_Model_Transactiontype::REFERRAL_VISITOR,
                                'status'                => MW_Mwcredit_Model_Orderstatus::COMPLETE,
                                'transaction_detail'    => $invitationModel->getId(),
                                'amount'                => $referralVisitorCommission,
                                'beginning_transaction' => $currentCredit,
                                'end_transaction'       => $newCredit,
                                'created_time'          => now()
                            );
                            Mage::getModel("mwcredit/credithistory")->setData($creditHistoryData)->save();
                        }
                    }
                }
                $response->setData('success', true);
                $response->setData('timecookie', $timeCokie);
                $response->setData('customer_id', $customer_id);
            }
	        $this->getResponse()->setHeader('Content-type', 'application/json');
	        $this->getResponse()->setBody($response->toJson());
        }
    }
	
}
