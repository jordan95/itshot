<?php
require_once(Mage::getModuleDir('controllers','MW_Affiliate').DS.'IndexController.php');
class OpenTechiz_Affiliate_IndexController extends MW_Affiliate_IndexController
{
	public function withdrawnsubmitAction()
	{
		if ($this->getRequest()->isPost())
		{
			$store_id = Mage::app()->getStore()->getId();
			$customer = Mage::getSingleton("customer/session")->getCustomer();
			$credit_customer = Mage::getModel('mwcredit/creditcustomer')->load($customer->getId());
			$affiliate_customer = Mage::getModel('affiliate/affiliatecustomers')->load($customer->getId());
			$post = $this->getRequest()->getPost();
			$your_balance = doubleval($credit_customer->getCredit());
			$withdrawn_amount = doubleval($post["withdraw_amount"]);

            $fee = Mage::helper('affiliate/data')->getFeePaymentGateway($affiliate_customer ->getPaymentGateway(),$store_id);
			if(strpos($fee, '%')) {
		    	$percent = doubleval(substr($fee, 0, strpos($fee, '%')));
		    	$fee = ($percent * $withdrawn_amount)/100;

		    } else {
		    	$fee = doubleval($fee);
		    }
			
			$withdrawn_receive = $withdrawn_amount - $fee;
			$customerid = (int)$customer->getId();
			
			$affiliate_customer = Mage::getModel('affiliate/affiliatecustomers')->load($customerid);
			$payment_gateway = $affiliate_customer->getPaymentGateway();
			$payment_email = $affiliate_customer->getPaymentEmail();
			
			/*
			if($payment_gateway == 'paypal') {
				$paypalParams = array(
									'amount' 		=> $withdrawn_receive,
									'currency'  	=> 'USD',
									'customer_email'=> 'ducnm0990@mailinator.com',
									'customer_name'	=> $customer->getName(),
								);
				
				$paypalResponse = Mage::helper('affiliate')->withdrawnPaypal($paypalParams);
				
				if($paypalResponse['status'] !== 'success') {
					$this->_getSession()->addError($this->__($paypalResponse['error']));
					$this->_redirect('affiliate/index/withdrawn/');
				}
			}
			*/
			
			if($payment_gateway == 'banktransfer') {
				$payment_email = '';
			}
		    	
		  	$bank_name 		= $affiliate_customer->getBankName();
			$name_account 	= $affiliate_customer->getNameAccount();
			$bank_country 	= $affiliate_customer->getBankCountry();
			$swift_bic 		= $affiliate_customer->getSwiftBic();
			$account_number	= $affiliate_customer->getAccountNumber();
			$re_account_number = $affiliate_customer->getReAccountNumber();
						
			$withData  =  array(
								'customer_id'		=> $customerid,
								'payment_gateway'	=> $payment_gateway,
	              				'payment_email'		=> $payment_email,
	              				'bank_name'			=> $bank_name,
					    	    'name_account'		=> $name_account,
					    	    'bank_country'		=> $bank_country,
					    	    'swift_bic'			=> $swift_bic,
					    	    'account_number'	=> $account_number,
					    	    're_account_number'	=> $re_account_number,
              					'withdrawn_amount'	=> $withdrawn_amount,
              					'fee'				=> $fee,
								'amount_receive'	=> $withdrawn_receive,
          						'status'			=> MW_Affiliate_Model_Status::PENDING,
                        		'withdrawn_time'	=> now()
						   );
			Mage::getModel('affiliate/affiliatewithdrawn')->setData($withData)->save(); 


			$creditcustomer = Mage::getModel('mwcredit/creditcustomer')->load($customerid);
    		$oldCredit = $creditcustomer->getCredit();
   			$amount = -$withdrawn_amount;
  			$newCredit = $oldCredit + $amount;
  	 		$newCredit = round($newCredit,2);
   			
   			$collectionWithdrawns = Mage::getModel('affiliate/affiliatewithdrawn')
   									->getCollection()
		                    		->addFieldToFilter('customer_id',$customerid)
	                    			->setOrder('withdrawn_id','DESC');
	        foreach($collectionWithdrawns as $collectionWithdrawn)
	        {
	        	$withdrawn_id=$collectionWithdrawn->getWithdrawnId();
	        	break;
	        }
   			
       		$historyData = array(
       							 'customer_id'			=> $customerid,
			 					 'type_transaction'		=> MW_Mwcredit_Model_Transactiontype::WITHDRAWN, 
			 					 'status'				=> MW_Mwcredit_Model_Orderstatus::PENDING,
				     		     'transaction_detail'	=> $withdrawn_id, 
				           		 'amount'				=> $amount, 
				         		 'beginning_transaction'=> $oldCredit,
				        		 'end_transaction'		=> $newCredit,
				           	     'created_time'			=> now()
       						);
   			Mage::getModel("mwcredit/credithistory")->setData($historyData)->save();
   
   			$withdrawn_amount_currency = Mage::helper('core')->currency($withdrawn_amount,true,false);
   			
   			// gui mail cho khach hang khi withdrawn bang tay
   			Mage::helper('affiliate')->sendMailCustomerRequestWithdrawn($customerid, $withdrawn_amount);
		    			
			$this->_getSession()->addSuccess($this->__("You have requested to withdraw: %s",$withdrawn_amount_currency));
			$this->_redirect('affiliate/index/withdrawn/');
		}
	}   
}