<?php
require_once(Mage::getModuleDir('controllers','MW_Affiliate').DS.'WebsiteController.php');
class OpenTechiz_Affiliate_WebsiteController extends MW_Affiliate_WebsiteController 
{
	public function verifyAction() 
	{
		$websiteId = $this->getRequest()->getParam('website_id');
		
		if(!$websiteId) {
			Mage::getSingleton('core/session')->addError(Mage::helper('affiliate')->__('Website Id does not exist'));
			$this->_redirect('*/*/');
		}
		
		$model = Mage::getModel('affiliate/affiliatewebsitemember')->load($websiteId);
		$domainName = $model->getDomainName();
		$htmlContent ='';
		$verifiedFileContent ='';
		$response_status = Mage::helper("opentechiz_affiliate")->checkResponeUrl($domainName);
   		if ($response_status){
   			$htmlContent = file_get_contents($domainName);	
   		} 
		$verifiedKey = $model->getVerifiedKey();
		
		$verifiedFileUrl  = $domainName . '/' . md5($domainName) . '.txt';
		$response_status = Mage::helper("opentechiz_affiliate")->checkResponeUrl($verifiedFileUrl);
		
   		if ($response_status){
   			$verifiedFileContent = file_get_contents($verifiedFileUrl);
   		} 
		
		
		/* Check meta-tag is inserted to affiliate website OR verified-file is upload to affiliate website host? */
		if(strpos($htmlContent, $verifiedKey) !== false || strcmp($verifiedFileContent, md5($domainName)) === 0) 
		{
			$model->setId($websiteId)->setStatus(MW_Affiliate_Model_Statuswebsite::VERIFIED);
			$model->save();
			
			Mage::getSingleton('core/session')->addSuccess(Mage::helper('affiliate')->__('Your website has been verified successfully'));
			$this->_redirect('*/*/');
		}
		else {
			Mage::getSingleton('core/session')->addError(Mage::helper('affiliate')->__('Verify unsuccessfully! Please try again'));
			$this->_redirect('*/*/');
		}
	}

	
}