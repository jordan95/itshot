<?php
require_once(Mage::getModuleDir('controllers','MW_Affiliate').DS.'Adminhtml'.DS.'Affiliate'.DS.'AffiliatememberController.php');
class OpenTechiz_Affiliate_Adminhtml_Affiliate_AffiliatememberController extends MW_Affiliate_Adminhtml_Affiliate_AffiliatememberController
{
	public function savenewAction() {
		$data = $this->getRequest()->getParams();

		if($data) {
			$customerId = Mage::getModel('customer/customer')
						  ->getCollection()
						  ->addFieldToFilter('email', array('eq' => $data['customer_email']))
						  ->getFirstItem()
						  ->getId();

			if(!$customerId){
				$this->_getSession()->addError($this->__('This member (%s) is not exist in the Customer system, please try again!',$data['customer_email']));
				$this->_redirect('*/*/index');
				return $this;
			}
			/*check customer already exists*/
			$CustomerAffiliate = Mage::getModel('affiliate/affiliatecustomers')->load($customerId);
			if($CustomerAffiliate->getId()){
				$this->_getSession()->addError($this->__('This member (%s) with the same email already exists.',$data['customer_email']));
				$this->_redirect('*/*/index');
				return $this;
			}
			/* Save affiliate customers to db */
			$affiliateCustomerModel = Mage::getModel('affiliate/affiliatecustomers');
			$customerData = array(
					'customer_id'			=> $customerId,
					'active'				=> MW_Affiliate_Model_Statusactive::ACTIVE,
					'payment_gateway'		=> $data['payment_gateway'],
					'payment_email'			=> $data['payment_email'],
					'auto_withdrawn'		=> $data['auto_withdrawn'],
					'withdrawn_level'		=> $data['withdrawn_level'],
					'reserve_level'			=> $data['reserve_level'],
					'bank_name'				=> $data['bank_name'],
					'name_account'			=> $data['name_account'],
					'bank_country'			=> $data['bank_country'],
					'swift_bic'				=> $data['swift_bic'],
					'account_number'		=> $data['account_number'],
					're_account_number'		=> $data['re_account_number'],
					'total_commission'		=> 0,
					'total_paid'			=> 0,
					'referral_code'			=> '',
					'status'				=> $data['affiliate_status'],
					'invitation_type'		=> MW_Affiliate_Model_Typeinvitation::NON_REFERRAL,
					'customer_time' 		=> now(),
					'customer_invited'		=> 0
			);
			$affiliateCustomerModel->setData($customerData)->save();
			
			/* Set Referral Code */
			Mage::helper('affiliate')->setReferralCode($customerId);
			
			/* Save affiliate websites to db */
			if(isset($data['referral_site']) && $referralSite = $data['referral_site']) {
				$sites = explode(',', $referralSite);
				$websiteModel = Mage::getModel('affiliate/affiliatewebsitemember');
				foreach($sites as $url) {
					$siteItem = array(
							'customer_id'   => $customerId,
							'domain_name'	=> trim($url),
							'verified_key'  => Mage::helper('affiliate')->getWebsiteVerificationKey(trim($url)),
							'status'		=> MW_Affiliate_Model_Statuswebsite::UNVERIFIED
					);
					$websiteModel->setData($siteItem);
					$websiteModel->save();
				}
			}
			
			/* Set group for affiliate */
			$group = array(
						    'customer_id'	=> $customerId,
							'group_id'		=> $data['group_id']
					 );
			Mage::getModel('affiliate/affiliategroupmember')->setData($group)->save();
			
			/* Send email to affilite to notify success */
			Mage::helper('affiliate')->sendMailCustomerActiveAffiliate($customerId);

            /* Add affiliate/customer link */
            $this->addAffiliateCustomerLink($customerData['customer_id']);

            $this->_getSession()->addSuccess($this->__("The member has successfully added"));
			$this->_redirect('adminhtml/affiliate_affiliatemember');
		}
		else
		{
			$this->_getSession()->addError($this->__(""));
			$this->_redirect('adminhtml/affiliate_affiliatemember');
		}
	}

	public function createAffiliateAlongWithCustomerAction(){
        $this->loadLayout();
        $this->_setActiveMenu('affiliate/member');

        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock('opentechiz_affiliate/adminhtml_affiliatemember_addWithCustomer'))
            ->_addLeft($this->getLayout()->createBlock('opentechiz_affiliate/adminhtml_affiliatemember_addWithCustomer_tabs'));

        $this->renderLayout();
    }

    public function saveNewWithCustomerAction(){
        $data = $this->getRequest()->getParams();

        if($data) {
            //create customer
            $websiteId = Mage::getModel('core/store')->load($data['store_id'])->getWebsiteId();
            $customer = Mage::getModel("customer/customer");
            $customer
                ->setFirstname($data['first_name'])
                ->setLastname($data['last_name'])
                ->setMiddlename($data['middle_name'])
                ->setEmail($data['customer_email'])
                ->setPassword($data['new_password'])
                ->setStoreId($data['store_id'])
                ->setWebsiteId($websiteId);
            $sendPassToEmail = false;
            if ($customer->getPassword() == 'auto') {
                $sendPassToEmail = true;
                $customer->setPassword($customer->generatePassword());
            }

            try{
                $customer->save();

                $newPassword = trim($data['new_password']);
                if ($newPassword == 'auto') {
                    $newPassword = $customer->generatePassword();
                } else {
                    $minPasswordLength = Mage::getModel('customer/customer')->getMinPasswordLength();
                    if (Mage::helper('core/string')->strlen($newPassword) < $minPasswordLength) {
                        Mage::throwException(Mage::helper('customer')
                            ->__('The minimum password length is %s', $minPasswordLength));
                    }
                }
                $customer->changePassword($newPassword);
                $customer->sendPasswordReminderEmail();

                // Send welcome email
                if ($customer->getWebsiteId() && (isset($data['account']['sendemail']) || $sendPassToEmail)) {
                    $storeId = $customer->getSendemailStoreId();

                    $customer->sendNewAccountEmail('registered', '', $storeId);
                }
            }
            catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('adminhtml/affiliate_affiliatemember');
                return;
            }

            //create new member
            $customerId = $customer->getId();

            if(!$customerId){
                $this->_getSession()->addError($this->__('This member (%s) is not exist in the Customer system, please try again!',$data['customer_email']));
                $this->_redirect('*/*/index');
                return $this;
            }
            /*check customer already exists*/
            $CustomerAffiliate = Mage::getModel('affiliate/affiliatecustomers')->load($customerId);
            if($CustomerAffiliate->getId()){
                $this->_getSession()->addError($this->__('This member (%s) with the same email already exists.',$data['customer_email']));
                $this->_redirect('*/*/index');
                return $this;
            }
            /* Save affiliate customers to db */
            $affiliateCustomerModel = Mage::getModel('affiliate/affiliatecustomers');
            $customerData = array(
                'customer_id'			=> $customerId,
                'active'				=> MW_Affiliate_Model_Statusactive::ACTIVE,
                'payment_gateway'		=> $data['payment_gateway'],
                'payment_email'			=> $data['payment_email'],
                'auto_withdrawn'		=> $data['auto_withdrawn'],
                'withdrawn_level'		=> $data['withdrawn_level'],
                'reserve_level'			=> $data['reserve_level'],
                'bank_name'				=> '',
                'name_account'			=> '',
                'bank_country'			=> '',
                'swift_bic'				=> '',
                'account_number'		=> '',
                're_account_number'		=> '',
                'total_commission'		=> 0,
                'total_paid'			=> 0,
                'referral_code'			=> '',
                'status'				=> $data['affiliate_status'],
                'invitation_type'		=> MW_Affiliate_Model_Typeinvitation::NON_REFERRAL,
                'customer_time' 		=> now(),
                'customer_invited'		=> 0
            );
            $affiliateCustomerModel->setData($customerData)->save();

            /* Set Referral Code */
            Mage::helper('affiliate')->setReferralCode($customerId);

            /* Save affiliate websites to db */
            if(isset($data['referral_site']) && $referralSite = $data['referral_site']) {
                $sites = explode(',', $referralSite);
                $websiteModel = Mage::getModel('affiliate/affiliatewebsitemember');
                foreach($sites as $url) {
                    $siteItem = array(
                        'customer_id'   => $customerId,
                        'domain_name'	=> trim($url),
                        'verified_key'  => Mage::helper('affiliate')->getWebsiteVerificationKey(trim($url)),
                        'status'		=> MW_Affiliate_Model_Statuswebsite::UNVERIFIED
                    );
                    $websiteModel->setData($siteItem);
                    $websiteModel->save();
                }
            }

            /* Set group for affiliate */
            $group = array(
                'customer_id'	=> $customerId,
                'group_id'		=> $data['group_id']
            );
            Mage::getModel('affiliate/affiliategroupmember')->setData($group)->save();

            /* Send email to affilite to notify success */
            Mage::helper('affiliate')->sendMailCustomerActiveAffiliate($customerId);

            /* Add affiliate/customer link */
            $this->addAffiliateCustomerLink($customerData['customer_id']);

            $this->_getSession()->addSuccess($this->__("The member has successfully added"));
            $this->_redirect('adminhtml/affiliate_affiliatemember');
        }
        else
        {
            $this->_getSession()->addError($this->__(""));
            $this->_redirect('adminhtml/affiliate_affiliatemember');
        }
    }

    public function addAffiliateCustomerLink($customerId){
        $aff = Mage::getModel('migrateaff/affcustomer')->getCollection()->loadByCustomerId($customerId);
        if(!$aff->getId()) {
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $affiliate = Mage::getModel('migrateaff/affcustomer')->getCollection();
            $tableName = $affiliate->getTable('affcustomer');
            $sql = "select MAX(aff_id) as aff_id from {$tableName}";
            $result = $write->fetchCol($sql);
            $maxId = isset($result[0]) ? $result[0] : 0; //prevent an error if table will be empty
            $nextId = (int)$maxId + 1;
            $data = array(
                'aff_id' => $nextId,
                'customer_id' => $customerId
            );
            Mage::getModel('migrateaff/affcustomer')->setData($data)->save();
        }
    }

    public function saveAction() {
        $data = $this->getRequest()->getPost();

        if($data)
        {
            $member_id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('affiliate/affiliatecustomers');

            try
            {
                if($member_id!='')
                {
                    $store_id = Mage::getModel('customer/customer')->load($member_id)->getStoreId();
                    $max = (double)Mage::helper('affiliate/data')->getWithdrawMaxStore($store_id);
                    $min = (double)Mage::helper('affiliate/data')->getWithdrawMinStore($store_id);
                    $collection = $model->load($member_id);

                    if($data['referral_code']) {
                        $collection->setReferralCode($data['referral_code']);
                    }

                    if(isset($data['affiliate_parent']) && $data['affiliate_parent'] != '')
                    {
                        $affiliateFilters = Mage::getModel('customer/customer')
                            ->getCollection()
                            ->addFieldToFilter('email',$data['affiliate_parent']);
                        if(sizeof($affiliateFilters) > 0)
                        {
                            foreach ($affiliateFilters as $affiliateFilter)
                            {
                                $mw_check = Mage::helper('affiliate')->getActiveAndEnableAffiliate($affiliateFilter->getId());
                                if($affiliateFilter->getId()!= $member_id && $mw_check == 1)
                                {
                                    $collection->setCustomerInvited($affiliateFilter->getId());

                                } else {
                                    $this->_getSession()->addError($this->__('Affiliate parent invalid'));
                                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                                    return;
                                }
                                break;
                            }
                        } else {
                            $this->_getSession()->addError($this->__('Affiliate parent invalid'));
                            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                            return;
                        }
                    } else {
                        $collection->setCustomerInvited(0);
                    }

                    $collection->setStatus($data['affiliate_status']);
                    $collection->setPaymentGateway($data['payment_gateway']);
                    if($data['payment_gateway'] != 'banktransfer' && $data['payment_gateway'] != 'check')
                    {
                        $collection->setPaymentEmail($data['payment_email']);
                        $collectionFilters = Mage::getModel('affiliate/affiliatecustomers')
                            ->getCollection()
                            ->addFieldToFilter('payment_email',$data['payment_email']);

                        if(sizeof($collectionFilters) > 0)
                        {
                            foreach($collectionFilters as $collectionFilter)
                            {
                                if($collectionFilter->getCustomerId()!= $member_id)
                                {
                                    $this->_getSession()->addError($this->__('There is already an account with this emails paypal'));
                                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                                    return;
                                }
                            }

                        }
                    } else {
                        $collection ->setPaymentEmail('');
                    }
                    $collection->setAutoWithdrawn($data['auto_withdrawn']);
                    $withdrawn_level = (double)$data['withdrawn_level'];
                    $auto_withdrawn = (int)$data['auto_withdrawn'];
                    if($auto_withdrawn == MW_Affiliate_Model_Autowithdrawn::AUTO)
                    {
                        if($withdrawn_level < $min || $withdrawn_level > $max)
                        {
                            $this->_getSession()->addError($this->__('Please insert a value of Auto payment when account balance reaches that is in range of [%s, %s]',$min,$max));
                            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                            return;

                        }
                        $collection ->setWithdrawnLevel($data['withdrawn_level']);
                    }
                    if(!$data['reserve_level']){
                        $data['reserve_level'] = 0;
                    }
                    if(!isset($data['referral_site']) || !$data['referral_site']) {
                        $data['referral_site'] = '';
                    }
                    $collection->setReferralSite($data['referral_site']);
                    $collection->setReserveLevel($data['reserve_level']);
                    $collection->save();

                    if($data['payment_gateway'] == 'banktransfer') {
                        $bank_name = $data['bank_name'];
                        $name_account = $data['name_account'];
                        $bank_country = $data['bank_country'];
                        $swift_bic = $data['swift_bic'];
                        $account_number = $data['account_number'];
                        $re_account_number = $data['re_account_number'];
                    } else {
                        $bank_name = '';
                        $name_account = '';
                        $bank_country = '';
                        $swift_bic = '';
                        $account_number = '';
                        $re_account_number = '';

                    }
                    $data_bank  =  array(
                        'bank_name'			=> $bank_name,
                        'name_account'		=> $name_account,
                        'bank_country'		=> $bank_country,
                        'swift_bic'			=> $swift_bic,
                        'account_number'		=> $account_number,
                        're_account_number'	=> $re_account_number
                    );

                    $model1 = Mage::getModel('affiliate/affiliatecustomers')->load($member_id);
                    $model1->setData($data_bank) ->setId($member_id);
                    $model1->save();

                    // them member vao group
                    if($data['group_id'] != 0)
                    {
                        // xoa cac member trong group de update lai du lieu moi
                        $group_members = Mage::getModel('affiliate/affiliategroupmember')
                            ->getCollection()
                            ->addFieldToFilter('customer_id',$member_id);

                        if(sizeof($group_members) > 0){
                            foreach ($group_members as $group_member) {
                                $group_member ->delete();
                            }
                        }
                        $datamembergroup['group_id'] = $data['group_id'];
                        $datamembergroup['customer_id'] = $member_id;
                        Mage::getModel("affiliate/affiliategroupmember")->setData($datamembergroup)->save();
                    }
                    //set total member customer program
                    Mage::helper('affiliate')->setTotalMemberProgram();

                    // edit customer
                    // truong hop them bot credit do admin voi ly do.....
                    $amountCredit = $data['amount_credit'];
                    if($amountCredit != 0)
                    {
                        /* Update affiliate customer table */
                        $affiliateCustomerModel = Mage::getModel('affiliate/affiliatecustomers')->load($member_id);
                        $oldCommission = $affiliateCustomerModel->getTotalCommission();
                        $newCommission = $oldCommission + $amountCredit;
                        $newCommission = round($newCommission, 2);
                        $affiliateCustomerModel->setData('total_commission', $newCommission)->save();

                        /* Update credit customer table */
                        $creditcustomer = Mage::getModel('mwcredit/creditcustomer')->load($member_id);
                        $oldCredit = $creditcustomer->getCredit();
                        $typeTransaction = MW_Mwcredit_Model_Transactiontype::ADMIN_CHANGE;
                        $comment = '';
                        if($data['comment']) {
                            $comment = $data['comment'];
                        }
                        $newCredit = $oldCredit + $amountCredit;
                        $newCredit = round($newCredit,2);
                        $amountCredit = round($amountCredit,2);
                        $oldCredit = round($oldCredit,2);
                        $creditcustomer->setData('credit',$newCredit)->save();

                        // Save history transaction for customer
                        $historyData = array(
                            'customer_id' 			=> $member_id,
                            'type_transaction'		=> MW_Mwcredit_Model_Transactiontype::ADMIN_CHANGE,
                            'transaction_detail'		=> $comment,
                            'amount'					=> $amountCredit,
                            'beginning_transaction'	=> $oldCredit,
                            'end_transaction'			=> $newCredit,
                            'status'					=> MW_Mwcredit_Model_Orderstatus::COMPLETE,
                            'created_time'			=> now()
                        );

                        Mage::getModel("mwcredit/credithistory")->setData($historyData)->save();

                        // gui mail cho khach hang khi admin them bot credit voi ly do.........
                        $storeId = Mage::getModel('customer/customer')->load($member_id)->getStoreId();
                        $store_name = Mage::getStoreConfig('general/store_information/name', $storeId);
                        $sender = Mage::getStoreConfig('affiliate/customer/email_sender', $storeId);
                        $email = Mage::getModel('customer/customer')->load($member_id)->getEmail();
                        $name = Mage::getModel('customer/customer')->load($member_id)->getName();
                        $teampale = 'affiliate/customer/email_template_credit_balance_changed';
                        $sender_name = Mage::getStoreConfig('trans_email/ident_'.$sender.'/name', $storeId);
                        $customer_credit_link = Mage::app()->getStore($storeId)->getUrl('mwcredit');
                        $data_mail['customer_name'] = $name;
                        $data_mail['transaction_amount'] = Mage::helper('core')->currency($amountCredit,true,false);
                        $data_mail['customer_balance'] = Mage::helper('core')->currency($newCredit,true,false);
                        $data_mail['transaction_detail'] = $comment;
                        $data_mail['transaction_time'] = now();
                        $data_mail['sender_name'] = $sender_name;
                        $data_mail['store_name'] = $store_name;
                        $data_mail['customer_credit_link'] = $customer_credit_link;
                        Mage::helper('affiliate')->_sendEmailTransactionNew($sender,$email,$name,$teampale,$data_mail,$storeId);
                    }

                    // truong hop payout cho customer
                    $payout_credit = $data['payout_credit'];
                    if(isset($payout_credit) && $payout_credit !='')
                    {
                        /* Update affiliate customer table (total_paid) */
                        $affiliateCustomerModel = Mage::getModel('affiliate/affiliatecustomers')->load($member_id);
                        $oldTotalPaid = $affiliateCustomerModel->getTotalPaid();
                        $newTotalPaid = $oldTotalPaid + $payout_credit;
                        $newTotalPaid = round($newTotalPaid, 2);
                        $affiliateCustomerModel->setData('total_paid', $newTotalPaid)->save();
                        
                        $payout_credit = (double)$payout_credit;
                        $creditcustomer = Mage::getModel('mwcredit/creditcustomer')->load($member_id);
                        $oldCredit = (double)$creditcustomer->getCredit();
                        if($payout_credit > 0 && $payout_credit <=  $oldCredit)
                        {
                            $this ->getRequestWithdrawComplete($member_id, $payout_credit);
                        }
                        else
                        {
                            $this->_getSession()->addError($this->__('Please insert a value of Payout that is in range of (%s, %s]',0,$oldCredit));
                            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                            return;
                        }
                    }
                }
                else if($member_id == '')
                {
                    $active = MW_Affiliate_Model_Statusactive::ACTIVE;
                    $payment_gateway = $data['getway_withdrawn'];
                    $paypal_email = $data['paypal_email'];
                    $auto_withdrawn = $data['auto_withdrawn'];
                    $payment_release_level = $data['payment_release_level'];
                    $reserve_level = $data['reserve_level'];
                    $referral_site = $data['referral_site'];

                    if($payment_gateway == 'check') {
                        $paypal_email = '';
                    }

                    if($payment_gateway == 'banktransfer') {
                        $paypal_email = '';
                        $bank_name = $data['bank_name'];
                        $name_account = $data['name_account'];
                        $bank_country = $data['bank_country'];
                        $swift_bic = $data['swift_bic'];
                        $account_number = $data['account_number'];
                        $re_account_number = $data['re_account_number'];
                    }

                    if($auto_withdrawn == MW_Affiliate_Model_Autowithdrawn::MANUAL) {
                        $payment_release_level = 0;
                    }
                    if(!$reserve_level) {
                        $reserve_level = 0;
                    }
                    if(!$referral_site) {
                        $referral_site = '';
                    }

                    $customerData = array(
                        'customer_id'			=> $member_id,
                        'active'				=> $active,
                        'payment_gateway'		=> $payment_gateway,
                        'payment_email'			=> $paypal_email,
                        'auto_withdrawn'		=> $auto_withdrawn,
                        'withdrawn_level'		=> $payment_release_level,
                        'reserve_level'			=> $reserve_level,
                        'bank_name'				=> $bank_name,
                        'name_account'			=> $name_account,
                        'bank_country'			=> $bank_country,
                        'swift_bic'				=> $swift_bic,
                        'account_number'		=> $account_number,
                        're_account_number'		=> $re_account_number,
                        'referral_site'			=> $referral_site,
                        'total_commission'		=> 0,
                        'total_paid'			=> 0,
                        'referral_code' 		=> '',
                        'status'				=> 1,
                        'invitation_type'		=> '',
                        'customer_time' 		=> now(),
                        'customer_invited'		=> 0
                    );
                    Mage::getModel('affiliate/affiliatecustomers')->saveCustomerAccount($customerData);
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('affiliate')->__('The member has successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('affiliate')->__('Unable to find member to save'));
        $this->_redirect('*/*/');
    }
}