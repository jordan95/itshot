<?php
class OpenTechiz_Affiliate_Block_Adminhtml_Affiliatemember_Edit_Tab_Program extends MW_Affiliate_Block_Adminhtml_Affiliatemember_Edit_Tab_Program
{
    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        $this->removeColumn('action');
        $this->addColumn('action',
            array(
                'header'    => Mage::helper('affiliate')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('affiliate')->__('View'),
                        'url'       => array('base'=> 'adminhtml/affiliate_affiliateprogram/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
            )
        );

        return $this;
    }
}