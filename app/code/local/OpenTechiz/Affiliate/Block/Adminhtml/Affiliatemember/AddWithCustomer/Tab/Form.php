<?php
class OpenTechiz_Affiliate_Block_Adminhtml_Affiliatemember_AddWithCustomer_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	public function __construct() {
		parent::__construct();
	}
	
	protected function _prepareForm()
  	{   
    	$form_member_detail = new Varien_Data_Form();
      	$this->setForm($form_member_detail);
      	$fieldset = $form_member_detail->addFieldset('affiliate_form', array('legend' => Mage::helper('opentechiz_affiliate')->__('Member Information')));

        $fieldset->addField('customer_email', 'text', array(
            'label'     => Mage::helper('opentechiz_affiliate')->__('New customer email'),
            'name'      => 'customer_email',
            'required'  => true
        ));

        $fieldset->addField('first_name', 'text', array(
            'label'     => Mage::helper('opentechiz_affiliate')->__('First name'),
            'name'      => 'first_name',
            'required'  => true
        ));

        $fieldset->addField('middle_name', 'text', array(
            'label'     => Mage::helper('opentechiz_affiliate')->__('Middle name'),
            'name'      => 'middle_name',
            'required'  => false
        ));

        $fieldset->addField('last_name', 'text', array(
            'label'     => Mage::helper('opentechiz_affiliate')->__('Last name'),
            'name'      => 'last_name',
            'required'  => true
        ));

        $storeList = [];
        $stores = Mage::app()->getStores(true);
        foreach ($stores as $store){
            $storeList[$store->getId()] = $store->getName();
        }

        $fieldset->addField('store_id', 'select', array(
            'label'     => Mage::helper('opentechiz_affiliate')->__('Associated with Store'),
            'name'      => 'store_id',
            'required'  => true,
            'values'    => $storeList
        ));

        $minPasswordLength = Mage::getModel('customer/customer')->getMinPasswordLength();
        $field = $fieldset->addField('new_password', 'text', array(
            'label'     => Mage::helper('opentechiz_affiliate')->__('Customer password'),
            'name'      => 'new_password',
            'required' => true,
            'class' => 'validate-new-password min-pass-length-' . $minPasswordLength,
            'note' 		=> Mage::helper('adminhtml')
                ->__('Password must be at least of %d characters.', $minPasswordLength)
        ));
        $field->setRenderer($this->getLayout()->createBlock('adminhtml/customer_edit_renderer_newpass'));

      	$fieldset->addField('group_id', 'select', array(
        	'label'     => Mage::helper('opentechiz_affiliate')->__('Member Group Name'),
          	'name'      => 'group_id',
      	  	'required'  => true,
          	'values'    => $this->_getGroupArray()
      	));
      	
      	$fieldset->addField('affiliate_status', 'select', array(
        	'label'     => Mage::helper('opentechiz_affiliate')->__('Status'),
          	'name'      => 'affiliate_status',
          	'values'    => MW_Affiliate_Model_Statusreferral::getOptionArray()
      	));
      	
      	$fieldset->addField('affiliate_parent', 'text', array(
        	'label'     => Mage::helper('opentechiz_affiliate')->__('Affiliate Parent'),
          	'class'     => 'validate-email',
          	'name'      => 'affiliate_parent',
      	));
      	
      	$fieldset->addField('payment_gateway', 'select', array(
        	'label'     => Mage::helper('opentechiz_affiliate')->__('Payment Method'),
          	'name'      => 'payment_gateway',
            'required'  => true,
          	'values'    => $this->_getPaymentGatewayArray(),
      	));
      	
      	$fieldset->addField('payment_email', 'text', array(
        	'label'     => Mage::helper('opentechiz_affiliate')->__('Withdrawal Notice Email'),
          	'name'      => 'payment_email',
      	  	'required'  => true,
      		'class'		=> 'validate-email',
      	));
      	
      	$fieldset->addField('auto_withdrawn', 'select', array(
        	'label'     => Mage::helper('opentechiz_affiliate')->__('Withdrawal Method'),
          	'name'      => 'auto_withdrawn',
          	'values'    => MW_Affiliate_Model_Autowithdrawn::getOptionArray()
      	));
      	
      	$fieldset->addField('withdrawn_level', 'text', array(
       		'label'     => Mage::helper('opentechiz_affiliate')->__('Auto payment when account balance reaches'),
          	'name'     	=> 'withdrawn_level',
		  	'class'    	=> 'required-entry validate-digits',
      		'note' 		=> Mage::helper('opentechiz_affiliate')->__('Note: Over reserve level'),
            'value'     => 0
      	));
      	
	   	$fieldset->addField('reserve_level', 'text', array(
        	'label'     => Mage::helper('opentechiz_affiliate')->__('Reserve Level (to be kept in account)'),
	      	'name'      => 'reserve_level',
	   	  	'class'     => 'validate-digits',
            'value'     => 0
      	));

//        $fieldset->addField('bank_name', 'text', array(
//            'label'     => Mage::helper('affiliate')->__('Bank Name'),
//            'name'      => 'bank_name',
//            'required'  => true,
//        ));
//
//        $fieldset->addField('name_account', 'text', array(
//            'label'     => Mage::helper('affiliate')->__('Name on account'),
//            'name'      => 'name_account',
//            'required'  => true,
//        ));
//
//        $fieldset->addField('bank_country', 'select', array(
//            'name'  => 'bank_country',
//            'required'  => true,
//            'label'     => Mage::helper('affiliate')->__('Bank Country'),
//            'values'    => Mage::getModel('adminhtml/system_config_source_country')->toOptionArray(),
//        ));
//
//        $fieldset->addField('swift_bic', 'text', array(
//            'label'     => Mage::helper('affiliate')->__('SWIFT code'),
//            'name'      => 'swift_bic',
//            'required'  => true,
//        ));
//
//        $fieldset->addField('account_number', 'text', array(
//            'label'     => Mage::helper('affiliate')->__('Account Number'),
//            'name'      => 'account_number',
//            'required'  => true,
//        ));
//
//        $fieldset->addField('re_account_number', 'text', array(
//            'label'     => Mage::helper('affiliate')->__('Re Account Number'),
//            'name'      => 're_account_number',
//            'class'     => 'validate-re_account_number',
//            'required'  => true
//        ));
      	
      	$fieldset->addField('referral_site', 'textarea', array(
        	'label'     => Mage::helper('opentechiz_affiliate')->__('Affiliate Website(s)'),
	      	'name'      => 'referral_site',
      	));
      
      	return parent::_prepareForm();
	}
	
	private function _getPaymentGatewayArray()
  	{
    	$arr = array();
  		$gateways = unserialize(Mage::helper('affiliate/data')->getGatewayStore());
		foreach ($gateways as $gateway)
		{
			$arr[$gateway['gateway_value']] = $gateway['gateway_title'];
		}
		return $arr;
    }
    
  	private function _getGroupArray()
    {
    	$arr = array();
    	$arr[''] = Mage::helper('affiliate')->__('Please select a group');
    	$groups = Mage::getModel('affiliate/affiliategroup')->getCollection();
		foreach ($groups as $group) 
		{   
			$group_id = $group->getGroupId();
			$group_name = $group ->getGroupName();
			$arr[$group_id] =  $group_name;
		}
		return $arr;
    }
    
}