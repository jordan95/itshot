<?php
class OpenTechiz_Affiliate_Block_Adminhtml_Affiliatehistory_Grid extends MW_Affiliate_Block_Adminhtml_Affiliatehistory_Grid
{
    /**
     * @param Varien_Data_Collection $collection
     */
    public function setCollection($collection)
    {
        if (!$this->getCollection()) {
            $holdingDays = intval(Mage::getStoreConfig('affiliate/general/commission_holding_period'));
            $collection->addExpressionFieldToSelect(
                'pay_date',
                "DATE_ADD(`{{transaction_time}}`, INTERVAL $holdingDays DAY)",
                ['transaction_time' => 'transaction_time']
            );
        }

        parent::setCollection($collection);
    }

    protected function _prepareColumns()
    {
        $this->addColumnAfter('pay_date', array(
            'header'    =>  Mage::helper('affiliate')->__('Pay Date'),
            'type'      =>  'datetime',
            'width'		=>  150,
            'align'     =>  'center',
            'index'     =>  'pay_date',
            'filter'    => false,
            'sortable'  => false,
            'gmtoffset' => true,
            'default'   =>  ' ---- '
        ), 'transaction_time');

        parent::_prepareColumns();
    }
}