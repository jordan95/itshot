<?php
class OpenTechiz_Affiliate_Model_Cron
{
    const Role_ADMINISTRATOR = 1;
    public function sendEmail(){
       $resource = Mage::getSingleton('core/resource');
       $readConnection = $resource->getConnection('core_read');

       $list_user =array();
       $admin_rule = Mage::getModel('admin/rules')->getCollection()->getSelect()->reset(Zend_Db_Select::WHERE);
       $admin_rule->where("resource_id like '%affiliate/member/active'");
       $admin_rule->where("permission = 'allow'");
       $admin_rule->orWhere("role_id = ".self::Role_ADMINISTRATOR);
       $admin_rule = $readConnection->fetchAll($admin_rule);
       if(!empty($admin_rule)){
            foreach($admin_rule as $rule){

                    $roles_users = Mage::getModel('admin/roles')->getCollection()->getSelect()->reset(Zend_Db_Select::WHERE);
                    $roles_users->where('parent_id = '.$rule['role_id']);
                    $roles_users = $readConnection->fetchAll($roles_users);

                if (!empty($roles_users))
                {
                    foreach ($roles_users as $staffUser)
                    {

                            $user = Mage::getModel('admin/user')->load($staffUser['user_id']);
                           if($user->getIsActive()){
                                $list_user[$user->getId()] = $user->getEmail();
                           }
                            
                       
                    }
                }
            }
       }
       if(!empty($list_user)){
            $list_recipient_email  = array_unique($list_user);
            $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
            $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
            $emailTemplate = Mage::getModel('core/email_template');
            $emailTemplate->loadDefault('statistic_affiliate_send_email_to_admin');
            $emailTemplate->setTemplateSubject('New Affiliates Added');

            $emailTemplate->setSenderName($senderName);
            $emailTemplate->setSenderEmail($senderEmail);
            $affiliate = Mage::getModel('affiliate/affiliatecustomers')->getCollection()->addFieldToFilter("customer_time",array("gteq"=> $this->getDate()));
            
            $list_affiliate_id = array();
            if($affiliate->count() > 0){
                foreach($affiliate as $member){
                    $list_affiliate_id[] = (int)$member->getCustomerId();
                }
                    // var_dump($list_affiliate_id);die('reree');
                $customer = Mage::getModel('customer/customer')->getCollection()->addAttributeToFilter("entity_id",array("in"=>$list_affiliate_id))->setPageSize(5000)->setCurPage(1);
                
                if($customer->count() > 0){
                    $emailTemplateVariables = ['affiliates' => $customer];
                    foreach($list_recipient_email as $email){
                            $emailTemplate->send(trim($email), null, $emailTemplateVariables);
                    }
                }
               
            }

       }
      

        
    }
    protected function getDate(){
        $dt = new DateTime;
        $dt->setTime(5, 0);
        $date = $dt->format('Y:m:d H:i:s');
        return $date;
    }
}