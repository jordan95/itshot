<?php
class OpenTechiz_Affiliate_Model_Observer
{
    public function runCron()
    {
        $store_id = Mage::app()->getStore()->getId();
        if (Mage::helper('affiliate')->getEnabled())
        {
            // gio he thong
            $day_week = (int)date('w', Mage::getModel('core/date')->timestamp(time()));
            $day_month = (int)date('j', Mage::getModel('core/date')->timestamp(time()));
            $withdrawn_period = (int)Mage::helper('affiliate/data')->getWithdrawnPeriodStore($store_id);
            $withdrawn_days = (int)Mage::helper('affiliate/data')->getWithdrawnDayStore($store_id);
            $withdrawn_month = (int)Mage::helper('affiliate/data')->getWithdrawnMonthStore($store_id);
            if(is_null($withdrawn_days)) {
                $withdrawn_days = 50;
            }
            if(is_null($withdrawn_month)) {
                $withdrawn_month = 50;
            }
            $fee = (int)Mage::helper('affiliate/data')->getFeeStore($store_id);
            if(($withdrawn_period == 1 && $day_week == $withdrawn_days) || ($withdrawn_period == 2 && $day_month == $withdrawn_month) )
            {
                $collections = Mage::getModel('affiliate/affiliatecustomers')
                    ->getCollection()
                    ->addFieldToFilter('active',MW_Affiliate_Model_Statusactive::ACTIVE)
                    ->addFieldToFilter('auto_withdrawn',MW_Affiliate_Model_Autowithdrawn::AUTO)
                    ->addFieldToFilter('status',MW_Affiliate_Model_Statusreferral::ENABLED);
                foreach ($collections as $collection)
                {
                    $withdrawn_level = $collection ->getWithdrawnLevel();
                    $customer_id = $collection ->getCustomerId();
                    $reserve_level = $collection ->getReserveLevel();
                    $withdrawn = $withdrawn_level + $reserve_level;

                    $credit = Mage::getModel('mwcredit/creditcustomer')->load($customer_id)->getCredit();
                    #calculate credit avoid duplicate when run cronjob again
                    $withdrawn_data = Mage::getModel('affiliate/affiliatewithdrawn')
                        ->getCollection()
                        ->addFieldToFilter('customer_id',array('eq'=>$customer_id))
                        ->addFieldToFilter('status',array('eq'=>1));
                    $amount_need_paid = 0;
                    if($withdrawn_data->count() > 0){
                        foreach ($withdrawn_data as $key => $item) {
                            $amount_need_paid += $item->getWithdrawnAmount();
                        }
                        $credit = $credit - $amount_need_paid;
                    }

                    if(($credit >= $withdrawn) && $credit > 0)
                    {
                        // luu vao bang withdrawn
                        $withdraw_receive = $credit - $fee;
                        $affiliate_customer = Mage::getModel('affiliate/affiliatecustomers')->load($customer_id);
                        $payment_gateway = $affiliate_customer->getPaymentGateway();
                        $payment_email = $affiliate_customer->getPaymentEmail();
                        if($payment_gateway == 'banktransfer') {
                            $payment_email = '';
                        }

                        $bank_name = $affiliate_customer->getBankName();
                        $name_account = $affiliate_customer->getNameAccount();
                        $bank_country = $affiliate_customer->getBankCountry();
                        $swift_bic = $affiliate_customer->getSwiftBic();
                        $account_number= $affiliate_customer->getAccountNumber();
                        $re_account_number = $affiliate_customer->getReAccountNumber();

                        $withdrawnData =  array(
                            'customer_id'       => $customer_id,
                            'payment_gateway'   => $payment_gateway,
                            'payment_email'     => $payment_email,
                            'bank_name'         => $bank_name,
                            'name_account'      => $name_account,
                            'bank_country'      => $bank_country,
                            'swift_bic'         => $swift_bic,
                            'account_number'    => $account_number,
                            're_account_number' => $re_account_number,
                            'withdrawn_amount'  => $credit,
                            'fee'               => $fee,
                            'amount_receive'    => $withdraw_receive,
                            'status'            => MW_Affiliate_Model_Status::PENDING,
                            'withdrawn_time'    => now()
                        );
                        Mage::getModel('affiliate/affiliatewithdrawn')->setData($withdrawnData)->save();

                        $creditcustomer = Mage::getModel('mwcredit/creditcustomer')->load($customer_id);
                        $oldCredit = $creditcustomer->getCredit();
                        $amount = - $credit;
                        $newCredit = $oldCredit + $amount;
                        $newCredit = round($newCredit,2);

                        $collectionWithdrawns = Mage::getModel('affiliate/affiliatewithdrawn')
                            ->getCollection()
                            ->addFieldToFilter('customer_id',$customer_id)
                            ->setOrder('withdrawn_id','DESC');
                        foreach($collectionWithdrawns as $collectionWithdrawn)
                        {
                            $withdrawn_id = $collectionWithdrawn->getWithdrawnId();
                            break;
                        }
                        // luu vao bang credit history
                        $historyData = array(
                            'customer_id'           => $customer_id,
                            'type_transaction'      => MW_Mwcredit_Model_Transactiontype::WITHDRAWN,
                            'status'                => MW_Mwcredit_Model_Orderstatus::PENDING,
                            'transaction_detail'    => $withdrawn_id,
                            'amount'                => $amount,
                            'beginning_transaction'=> $oldCredit,
                            'end_transaction'       => $newCredit,
                            'created_time'          => now()
                        );
                        Mage::getModel("mwcredit/credithistory")->setData($historyData)->save();

                        // gui mail cho khach hang khi rut tien tu dong
                        Mage::helper('affiliate')->sendMailCustomerRequestWithdrawn($customer_id, $credit);
                    }
                }
            }
        }
    }
	public function dispathClickLink()
    {
        $invite = Mage::app()->getRequest()->getParam('A');
        if($invite)
        {
            $referral_to = explode("?A", Mage::helper('core/url')->getCurrentUrl());
            Mage::dispatchEvent('affiliate_referral_link_click',array('invite'=>$invite,'request'=>Mage::app()->getRequest(),'referral_to'=>$referral_to[0]));

            
        }

        /* Check whether the url come from affiliate-site or not */
        if(Mage::getModel('affiliate/affiliatewebsitemember')) {
            $referredFrom = Mage::app()->getRequest()->getServer('HTTP_REFERER');
            $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);

            if($referredFrom !='' && substr_count($referredFrom,$url)== 0 ){
                if($emailInvitation = Mage::helper('affiliate')->isDirectReferral($referredFrom))
                {
                    Mage::dispatchEvent('affiliate_referral_link_click',
                        array(
                            'invite'        => $emailInvitation,
                            'request'       => Mage::app()->getRequest(),
                            'referral_to'   => Mage::helper('core/url')->getCurrentUrl()
                        ));
                    
                }
            }
        }

        $modules = Mage::getConfig()->getNode('modules')->children();
        $modulesArray = (array)$modules;
        $modules2 = array_keys((array)Mage::getConfig()->getNode('modules')->children());
        if(!in_array('MW_Mcore', $modules2) || !$modulesArray['MW_Mcore']->is('active') || Mage::getStoreConfig('mcore/config/enabled')!=1)
        {
            Mage::helper('affiliate')->disableConfig();
        }

    }
    public function clickReferralLink($argv)
    {
        $store_id = Mage::app()->getStore()->getId();
        if(Mage::helper('affiliate/data')->getEnabledStore($store_id))
        {
            $invite = $argv->getInvite();
            $request = $argv->getRequest();
            $referral_to = $argv->getReferralTo();

            //$customers = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->getCollection();
            //$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            //$where = $connection->quoteInto("md5(email)=?", $invite);
            //$customers->getSelect()->where($where);

            $aff = Mage::getModel('migrateaff/affcustomer')->getCollection()->loadByAffId($invite);
            $realAffId = $aff->getCustomerId() ? $aff->getCustomerId() : 0;
            $customers = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->getCollection();
            $customers->addFieldToFilter('entity_id', $realAffId);

            $timeCokie = Mage::helper('affiliate/data')->getTimeCookieStore($store_id);

            if(sizeof($customers)>0)
            {
                foreach ($customers as $customer)
                {
                    $customer_id = (int)$customer->getId();
                    if(Mage::helper('affiliate')->getActiveAffiliate($customer_id) == 1 && Mage::helper('affiliate')->getLockAffiliate($customer_id) == 0)
                    {
                        $clientIP = '';
                        if(!empty(Mage::app()->getRequest()->getServer('REMOTE_ADDR'))){
                            $clientIP = Mage::app()->getRequest()->getServer('REMOTE_ADDR');
                        }
                        
                        $cookie = (int)Mage::getModel('core/cookie')->get('customer');
                        if(!$cookie)
                        {
                            $referral_from = '';
                            if(isset($_SERVER['HTTP_REFERER'])) {
                                $referral_from = trim($_SERVER['HTTP_REFERER']);
                            }

                            if($referral_from !=''){
                                $referral_from_domains = explode("://", $referral_from);
                                $referral_from_domain = explode("/", $referral_from_domains[1]);
                            }else{
                                $referral_from_domain[0] = '';
                            }

                            Mage::getModel('core/cookie')->set('customer',$customer_id,60*60*24*$timeCokie);
                            Mage::getModel('core/cookie')->set('mw_referral_from',$referral_from,60*60*24*$timeCokie);
                            Mage::getModel('core/cookie')->set('mw_referral_from_domain',$referral_from_domain[0],60*60*24*$timeCokie);
                            Mage::getModel('core/cookie')->set('mw_referral_to',$referral_to,60*60*24*$timeCokie);
                            $invitation_type = MW_Affiliate_Model_Typeinvitation::REFERRAL_LINK;

                            // Calculate the commission from referral visitor
                            $affiliateCustomer = Mage::getModel('affiliate/affiliatecustomers')->load($customer_id);
                            $referralVisitorCommission = Mage::helper('affiliate')->calculateReferralVisitorCommission($customer_id, $affiliateCustomer->getLinkClickIdPivot());

                            // Save new invitation to db
                            $invitationModel = Mage::getModel('affiliate/affiliateinvitation');
                            $historyData = array(
                                'customer_id'           => $customer_id,
                                'email'                 => '',
                                'status'                => MW_Affiliate_Model_Statusinvitation::CLICKLINK,
                                'ip'                    => $clientIP,
                                'count_click_link'      => 1,
                                'count_register'        => 0,
                                'count_purchase'        => 0,
                                'count_subscribe'       => 0,
                                'referral_from'         => $referral_from,
                                'referral_from_domain'  => $referral_from_domain[0],
                                'referral_to'           => $referral_to,
                                'order_id'              => '',
                                'invitation_type'       => $invitation_type,
                                'invitation_time'       => now(),
                                'commission'            => $referralVisitorCommission
                            );
                            $invitationModel->setData($historyData)->save();

                            Mage::helper('affiliate')->saveAffiliateTransactionReferral($customer_id,$referralVisitorCommission,$customer_id,$invitation_type,MW_Mwcredit_Model_Transactiontype::REFERRAL_VISITOR);

                            // If there is enough visitors to get commission then update customer overall commission
                            if($referralVisitorCommission > 0)
                            {
                                // Update link_click_id_pivot
                                $affiliateCustomer->setLinkClickIdPivot($invitationModel->getId());

                                // Update total commission in affiliate_customers
                                $currentCommission = $affiliateCustomer->getTotalCommission();
                                $affiliateCustomer->setTotalCommission($currentCommission + $referralVisitorCommission);

                                // Save affiliate_customers
                                $affiliateCustomer->save();

                                // Update customer credit
                                Mage::helper('affiliate')->insertCustomerCredit($customer_id);
                                $customerCredit = Mage::getModel('mwcredit/creditcustomer')->load($customer_id);
                                $currentCredit = $customerCredit->getCredit();
                                $newCredit = $currentCredit + $referralVisitorCommission;
                                $customerCredit->setCredit($newCredit)->save();

                                // Update credit history table
                                $creditHistoryData = array(
                                    'customer_id'           => $customer_id,
                                    'type_transaction'      => MW_Mwcredit_Model_Transactiontype::REFERRAL_VISITOR,
                                    'status'                => MW_Mwcredit_Model_Orderstatus::COMPLETE,
                                    'transaction_detail'    => $invitationModel->getId(),
                                    'amount'                => $referralVisitorCommission,
                                    'beginning_transaction' => $currentCredit,
                                    'end_transaction'       => $newCredit,
                                    'created_time'          => now()
                                );
                                Mage::getModel("mwcredit/credithistory")->setData($creditHistoryData)->save();
                            }
                        }
                    }
                }
            }
        }
    }

}