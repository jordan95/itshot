<?php

class OpenTechiz_Affiliate_Helper_Data extends MW_Affiliate_Helper_Data
{
	public function processWithdrawn($status, $withdrawn_ids)
	{
        $is_complete = 0;
        $is_cancel = 0;
        foreach($withdrawn_ids as $withdrawn_id)
        {
        	$affiliatedrawn = Mage::getModel('affiliate/affiliatewithdrawn')->load($withdrawn_id);
    		$withdrawn_status = $affiliatedrawn->getStatus();
    		if($withdrawn_status == MW_Affiliate_Model_Status::COMPLETE) {
    			$is_complete = 1;
    		}
    		else if($withdrawn_status == MW_Affiliate_Model_Status::CANCELED) {
    			$is_cancel = 1;
    		}
        }
        if($status == MW_Affiliate_Model_Status::COMPLETE)
        {   
        	if(($is_complete == 0) && ($is_cancel == 0))
        	{
        		foreach($withdrawn_ids as $withdrawn_id)
        		{
        			$affiliatedrawn = Mage::getModel('affiliate/affiliatewithdrawn')->load($withdrawn_id);
    				$withdrawn_amount = $affiliatedrawn->getWithdrawnAmount();
    				$customer_Id = $affiliatedrawn->getCustomerId();
    				
    				/* Handle Paypal Withdrawn by Paypal Masspay */
    				if($affiliatedrawn->getPaymentGateway() == 'paypal') 
    				{
    					$customer = Mage::getModel('customer/customer')->load($customer_Id);
    					$paypalParams = array(
    							'amount' 		=> $affiliatedrawn->getAmountReceive(),
    							'currency'  	=> Mage::app()->getStore()->getCurrentCurrencyCode(),
    							'customer_email'=> $affiliatedrawn->getPaymentEmail(),
    							'customer_name'	=> $customer->getName(),
    					);
    				
    					//zend_debug::dump($paypalParams);die();
    					$paypalResponse = $this->withdrawnPaypal($paypalParams);
    					if($paypalResponse['status'] !== 'success') {
    						Mage::getSingleton('adminhtml/session')->addError($this->__($paypalResponse['error']));
    						continue;
    					} else {
    						$affiliatedrawn->setStatus(MW_Affiliate_Model_Status::COMPLETE)->save();
    						$affiliatedrawn->setWithdrawnTime(now())->save();
    					}
    				}
    				else 
    				{
    					$affiliatedrawn->setStatus(MW_Affiliate_Model_Status::COMPLETE)->save();
    					$affiliatedrawn->setWithdrawnTime(now())->save();
    				}
    				
        			// gui mail cho khach hang khi rut tien thanh cong
        			$this->sendMailCustomerWithdrawnComplete($customer_Id, $withdrawn_amount);
        		
        			// cap nhat lai trang thai trong bang credit history
        			$collection = Mage::getModel('mwcredit/credithistory')
        						  ->getCollection()
			                      ->addFieldToFilter('type_transaction', MW_Mwcredit_Model_Transactiontype::WITHDRAWN)
			                      ->addFieldToFilter('customer_id', $customer_Id)
			                      ->addFieldToFilter('transaction_detail', $withdrawn_id)
			                      ->addFieldToFilter('status', MW_Mwcredit_Model_Orderstatus::PENDING);
                    $affiliate_customer = Mage::getModel('affiliate/affiliatecustomers')->load($customer_Id);
		            foreach($collection as $credithistory)
		             {  
		             	/*
		             	 * this update has been processed right after affiliate withdrawn
		             	$oldTotalPaid = $affiliate_customer->getTotalPaid();
		             	$amount = $credithistory->getAmount();
		             	$newTotalPaid = $oldTotalPaid - $amount;
		             	$affiliate_customer->setData('total_paid',$newTotalPaid)->save();
		             	*/
		             	
		             	$status_credit = MW_Mwcredit_Model_Orderstatus::COMPLETE;
						$credithistory->setStatus($status_credit)->save();
		             }
                     /* Update affiliate customer table (total_paid) */
                    $affiliateCustomerModel = Mage::getModel('affiliate/affiliatecustomers')->load($customer_Id);
                    $oldTotalPaid = $affiliateCustomerModel->getTotalPaid();
                    $newTotalPaid = $oldTotalPaid + $withdrawn_amount;
                    $newTotalPaid = round($newTotalPaid, 2);
                    $affiliateCustomerModel->setData('total_paid', $newTotalPaid)->save();
                        
                    /* Update credit customer table */
                    $creditcustomer = Mage::getModel('mwcredit/creditcustomer')->load($customer_Id);
                    $oldCredit = $creditcustomer->getCredit();
                    $amount = -$withdrawn_amount;
                    $newCredit = $oldCredit + $amount;
                    $newCredit = round($newCredit,2);
                    $creditcustomer->setCredit($newCredit)->save();
                    
        		}
        		if(!isset($paypalResponse['error'])){
        			Mage::getSingleton('adminhtml/session')->addSuccess("You have successfully updated the withdrawn(s) status");
        		}
        	}
        	else if($is_complete == 1) {
        		Mage::getSingleton('adminhtml/session')->addError("Withdrawn_id you have chosen, having status: Complete");
        	}
        	else if($is_cancel == 1) {
        		Mage::getSingleton('adminhtml/session')->addError("Withdrawn_id you have chosen, having status: Canceled");
        	}
        	
        }
        else if($status== MW_Affiliate_Model_Status::CANCELED)
        {
        	if(($is_complete == 0) && ($is_cancel == 0))
        	{
        		foreach($withdrawn_ids as $withdrawn_id)
        		{
        			$affiliatedraw = Mage::getModel('affiliate/affiliatewithdrawn')->load($withdrawn_id);
    				$withdrawn_amount = $affiliatedraw->getWithdrawnAmount();
    				$customer_Id = $affiliatedrawn->getCustomerId();
        			$affiliatedraw->setStatus(MW_Affiliate_Model_Status::CANCELED)->save();
        			$affiliatedraw->setWithdrawnTime(now())->save();
        			
        			// gui mail cho khach hang khi rut tien that bai
        			$this ->sendMailCustomerWithdrawnCancel($customer_Id, $withdrawn_amount);
		    		
        			// cap nhat lai trang thai trong bang credit history, them kieu cancel withdrawn
        			$collection = Mage::getModel('mwcredit/credithistory')
        						  ->getCollection()
	                    		  ->addFieldToFilter('type_transaction',MW_Mwcredit_Model_Transactiontype::WITHDRAWN)
				                  ->addFieldToFilter('customer_id',$customer_Id)
				                  ->addFieldToFilter('transaction_detail',$withdrawn_id)
				                  ->addFieldToFilter('status',MW_Mwcredit_Model_Orderstatus::PENDING);
                    
        			$creditcustomer = Mage::getModel('mwcredit/creditcustomer')->load($customer_Id);
                    $oldcredit = $creditcustomer->getCredit();
		            foreach($collection as $credithistory)
		            {  
		             	$amount=$credithistory->getAmount();
						$newcredit = $oldcredit - $amount;
		             	$status_credit = MW_Mwcredit_Model_Orderstatus::CANCELED;
		             	$credithistory->setStatus($status_credit)->save();
		             	$creditcustomer->setCredit($newcredit)->save();
		             	
						// luu them vao credit history kieu cancel withdraw
			       		$historyData = array(
			       							 'customer_id'			=> $customer_Id,
						 					 'type_transaction'		=> MW_Mwcredit_Model_Transactiontype::CANCEL_WITHDRAWN, 
						 					 'status'				=> MW_Mwcredit_Model_Orderstatus::COMPLETE,
							     		     'transaction_detail'	=> $withdrawn_id, 
							           		 'amount'				=> -$amount, 
							         		 'beginning_transaction'=> $oldcredit,
							        		 'end_transaction'		=> $newcredit,
							           	     'created_time'			=> now()
			       						);
			   			Mage::getModel("mwcredit/credithistory")->setData($historyData)->save();
						
		             }
        		}
        		Mage::getSingleton('adminhtml/session')->addSuccess("You have successfully updated the Withdrawn(s) status");
        	}
        	else if($is_complete == 1) {
        		Mage::getSingleton('adminhtml/session')->addError("Withdrawn_id you have chosen, having status: Complete");
        	}
        	else if($is_cancel == 1) {
        		Mage::getSingleton('adminhtml/session')->addError("Withdrawn_id you have chosen, having status: Canceled");
        	}
        }
	}
	public function updateAffiliateInvitionNew($customer_id, $cokie, $clientIP,$referral_from,$referral_from_domain,$referral_to,$invitation_type, $isSubscribed = null)
	{
		if($cokie!= 0 )
    	{
    		if($invitation_type == MW_Affiliate_Model_Typeinvitation::REFERRAL_CODE) {
    			$referral_from = '';
    			$referral_to = '';
    			$referral_from_domain = '';
    		}
    		$email = Mage::getModel('customer/customer')->load($customer_id)->getEmail();
    		$collection = Mage::getModel('affiliate/affiliateinvitation')->getCollection()
							    				  ->addFieldToFilter('customer_id', $cokie)
						                    	  ->addFieldToFilter('email', $email);
    		
		    // neu ban be dc moi dang ky lam thanh vien cua website ?
		    // voi email trung voi email moi se update lai trang thai 
		    if(sizeof($collection)>0)
		    {
		    	foreach ($collection as $obj) 
		    	{
		    		$obj->setStatus(MW_Affiliate_Model_Statusinvitation::REGISTER);
		    		$obj->setIp($clientIP);
		    		$obj->setInvitationTime(now());
		    		$obj->setCountClickLink(0);
		    		$obj->setCountRegister(1);
		    		$obj->setCountPurChase(0);
		    		$obj->setCountSubscribe(0);
		    		$obj->setReferralFrom($referral_from);
		    		$obj->setReferralFromDomain($referral_from_domain);
		    		$obj->setReferralTo($referral_to);
		    		$obj->setOrderId('');
		    		$obj->setInvitationType($invitation_type);
		    		$obj->save();
		    	}
		    }
		    else
		    {
		    	/*---------------------- These code right below is used for Affiliate v4.0----------------------*/
		    	
		    	/* Add commission in case of visitor sign-up */
		    	$store_id = Mage::app()->getStore()->getId();
		    	$referralSignupCommission = Mage::helper('affiliate')->getReferralSignupCommission($store_id);
		    	if($referralSignupCommission > 0)
		    	{
					$this->saveAffiliateTransactionReferral($customer_id,$referralSignupCommission,$cokie,$invitation_type,MW_Mwcredit_Model_Transactiontype::REFERRAL_SIGNUP);
									
			    	$historyData = array(
			    						 'customer_id'			=> $cokie,
		                        		 'email'				=> $email, 
		                        		 'status'				=> MW_Affiliate_Model_Statusinvitation::REGISTER, 
		                        		 'ip'					=> $clientIP,
			    						 'count_click_link'		=> 0,
	                        			 'count_register'		=> 1, 
	                        			 'count_purchase'		=> 0,
										 'count_subscribe'		=> 0,		    						
	                                 	 'referral_from'		=> $referral_from, 
			    						 'referral_from_domain'	=> $referral_from_domain,
	                        			 'referral_to'			=> $referral_to,
	                        			 'order_id'				=> '',
			    	                     'invitation_type'		=> $invitation_type,
		                        		 'invitation_time'		=> now(),
			    						 'commission'			=> $referralSignupCommission
			    					);
	                Mage::getModel('affiliate/affiliateinvitation')->setData($historyData)->save();
	                
	                // Update total_commission in affiliate_customers
	                $affiliateCustomer = Mage::getModel('affiliate/affiliatecustomers')->load($cokie);
	                if($affiliateCustomer->getId()){
	                	$currentCommission = $affiliateCustomer->getTotalCommission();
		                $affiliateCustomer->setTotalCommission($currentCommission + $referralSignupCommission);
		                $affiliateCustomer->save();
	                }
	                
	                
	                // Update customer credit
	                $customerCredit = Mage::getModel('mwcredit/creditcustomer')->load($cokie);
	                if($customerCredit->getId()){
	                	$currentCredit = $customerCredit->getCredit();
		                $newCredit = $currentCredit + $referralSignupCommission;
		                $customerCredit->setCredit($newCredit)->save();
	                }
	                
	                
	                //Update credit history table
	                $creditHistoryData = array(
	                		'customer_id'			=> $cokie,
	                		'type_transaction'		=> MW_Mwcredit_Model_Transactiontype::REFERRAL_SIGNUP,
	                		'status'				=> MW_Mwcredit_Model_Orderstatus::COMPLETE,
	                		'transaction_detail'	=> $customer_id,
	                		'amount'				=> $referralSignupCommission,
	                		'beginning_transaction'	=> $currentCredit,
	                		'end_transaction'		=> $newCredit,
	                		'created_time'			=> now()
	                );
	                Mage::getModel("mwcredit/credithistory")->setData($creditHistoryData)->save();
		    	}
                /* If visitor subscribe then add commission in case of subscription */
                $referralSubscribeCommission = Mage::helper('affiliate')->getReferralSubscribeCommission($store_id);
                if($isSubscribed && $referralSubscribeCommission >0) {
                	
                	if($referral_from_domain == '') $referral_from_domain[0] ='';
                	
                	$subscribeHistoryData = array(
                			'customer_id'			=> $cokie,
                			'email'					=> $email,
                			'status'				=> MW_Affiliate_Model_Statusinvitation::SUBSCRIBE,
                			'ip'					=> $clientIP,
                			'count_click_link'		=> 0,
                			'count_register'		=> 0,
                			'count_purchase'		=> 0,
                			'count_subscribe'		=> 1,
                			'referral_from'			=> $referral_from,
                			'referral_from_domain'	=> $referral_from_domain[0],
                			'referral_to'			=> $referral_to,
                			'order_id'				=> '',
                			'invitation_type'		=> $invitation_type,
                			'invitation_time'		=> now(),
                			'commission'			=> $referralSubscribeCommission
                	);
                	Mage::getModel('affiliate/affiliateinvitation')->setData($subscribeHistoryData)->save();
                	           
					$this->saveAffiliateTransactionReferral($customer_id,$referralSubscribeCommission,$cokie,$invitation_type,MW_Mwcredit_Model_Transactiontype::REFERRAL_SUBSCRIBE);
                	
                	// Update total_commission in affiliate_customers
                	$affiliateCustomer = Mage::getModel('affiliate/affiliatecustomers')->load($cokie);
                	if($affiliateCustomer->getId()){
                		$currentCommission = $affiliateCustomer->getTotalCommission();
	                	$affiliateCustomer->setTotalCommission($currentCommission + $referralSubscribeCommission);
	                	$affiliateCustomer->save();
                	}
                	
                
                	// Update customer credit
                	$customerCredit = Mage::getModel('mwcredit/creditcustomer')->load($cokie);
                	if($customerCredit->getId()){
                		$currentCredit = $customerCredit->getCredit();
	                	$newCredit = $currentCredit + $referralSubscribeCommission;
	                	$customerCredit->setCredit($newCredit)->save();
                	}
                	
                	//Update credit history table
                	$creditHistoryData = array(
                			'customer_id'			=> $cokie,
                			'type_transaction'		=> MW_Mwcredit_Model_Transactiontype::REFERRAL_SUBSCRIBE,
                			'status'				=> MW_Mwcredit_Model_Orderstatus::COMPLETE,
                			'transaction_detail'	=> $customer_id,
                			'amount'				=> $referralSubscribeCommission,
                			'beginning_transaction'	=> $currentCredit,
                			'end_transaction'		=> $newCredit,
                			'created_time'			=> now()
                	);
                	Mage::getModel("mwcredit/credithistory")->setData($creditHistoryData)->save();
                }
                /*------------------------------- End upgrade for Affiliate v4.0--------------------------------*/
                
                
		    }	
        }
	}
	public function checkResponeUrl($url){

		$response_status = get_headers($url, 1);
   		if ($response_status[0]=='HTTP/1.1 200 OK'){
   			return true;
   		}else{
   			return false;
   		}
   		
	}
}
