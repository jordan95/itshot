<?php
class OpenTechiz_ReportExtend_Helper_Collection_Filter extends Mage_Core_Helper_Abstract
{
    protected $_orderStatusToExclude = array('holded', 'closed', 'canceled', 'complete');
    public function addOrderNeedItemFilter($collection){
        $countLinkTable = new Zend_Db_Expr("(select count(*) as count, order_item_id from tsht_inventory_order_item group by order_item_id)");
        $collection->addFieldToFilter('status', array('nin' => $this->_orderStatusToExclude))
            ->addFieldToFilter('order_type', array('neq' => 2))
            ->addFieldToFilter('order_stage', array('in' => array(1, 2)));
        $collection->getSelect()->joinLeft(array('linkCount' => $countLinkTable), 'main_table.item_id = order_item_id')
            ->where('`count` < (main_table.qty_ordered - main_table.qty_canceled - main_table.qty_refunded) or `count` is null');
        return $collection;
    }
}