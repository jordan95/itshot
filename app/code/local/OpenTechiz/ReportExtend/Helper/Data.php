<?php
class OpenTechiz_ReportExtend_Helper_Data extends Mage_Core_Helper_Abstract
{
    const goldQuality = ['10k', '14k'];

    protected $shippedStates = '(15,20)'; //Shipped out, Shipped to customer

    protected $excludedPOStatusInReport = '('.
        OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL .','.
        OpenTechiz_Purchase_Helper_Data::PO_STATUS_CLOSED .','.
        OpenTechiz_Purchase_Helper_Data::PO_STATUS_DRAFTED
    .')';

    protected $returnSolutionArray = OpenTechiz_Return_Helper_Data::ALL_SOLUTIONS;

    const STOCK_MOVEMENT_TYPE = [
        0 => 'Inventory',
        1 => 'Stone',
        2 => 'Gold'
    ];
    const ORDER_SOURCE = [
        0 => 'ItsHot.com',
        1 => 'Phone',
        2 => 'OverStock',
        3 => 'Showroom',
        4 => 'AmazonJNS',
        5 => '11MAIN',
        6 => 'EBay',
        7 => 'Warehouse',
        8 => 'Repair',
        9 => 'Wedding Band Wholesale',
        10=> 'Iced Time',
        11=> 'NewEgg',
        13=> 'Memo',
        14=> 'Bonanza',
        15=> 'Home',
        16=> 'Sears',
        17=> 'Ioffer',
        18=> 'Mercari',
        19=> 'Pricefalls',
        20=> 'Rakuten',
        21=> 'Marketplace',
        22=> 'AmazonIcedTime'
    ];

    const STAGE_STATUS_FOR_REPORT_PROGRESS = [
        [2, 2], [2, 4]
    ];

    const NAME_FOR_REPORT_PROGRESS = [
        'Manufacturing' => [2, 2],
        'Waiting on Vendor' => [2, 4]
    ];

    const EXCLUDED_ORDER_STATUS = ['closed', 'canceled', 'holded', 'layawayorder_canceled'];

    public function getReportName($stage, $status){
        $stage = $this->convertStageNameToKey($stage);
        $status = $this->convertStatusNameToKey($status);
        foreach (self::NAME_FOR_REPORT_PROGRESS as $key => $value){
            if($value[0] == $stage && $value[1] == $status){
                return $key;
            }
        }
        return '';
    }

    public function convertStageNameToKey($stage){
        $stage = strtolower($stage);
        $stageArray = OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE;
        $stageKey = null;
        foreach ($stageArray as $key => $value){
            if(strtolower(str_replace(' ', '', $value)) == $stage){
                $stageKey = $key;
                break;
            }
        }
        return $stageKey;
    }

    public function convertStatusNameToKey($status){
        $status = strtolower($status);
        $statusArray = Mage::helper('opentechiz_salesextend')->getAllInternalStatusKey();
        $statusKey = null;
        foreach ($statusArray as $value => $key){
            if(strtolower(str_replace('(s)', '', str_replace(' ', '', $value))) == $status){
                $statusKey = $key;
                break;
            }
        }
        return $statusKey;
    }

    public function isParamsAllow($stage, $status){
        $stageKey = $this->convertStageNameToKey($stage);
        if($stageKey === null) return false;

        $statusKey = $this->convertStatusNameToKey($status);
        if($statusKey === null) return false;
        foreach (self::STAGE_STATUS_FOR_REPORT_PROGRESS as $stageStatusArray){
            if($stageStatusArray[0] == $stageKey && $stageStatusArray[1] == $statusKey){
                return true;
            }
        }
        return false;
    }

    public function getTimeFilterDaily(){
        $currentTimeGmt = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $localeTime = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $difference = strtotime($currentTimeGmt) - strtotime($localeTime);

        $localeDate = explode(' ', $localeTime)[0];
        $fromLocale = $localeDate.' 00:00:00';
        $toLocale = $localeDate.' 23:59:59';


        $fromGmt = date ('Y-m-d H:i:s', strtotime($fromLocale)+$difference);
        $toGmt = date ('Y-m-d H:i:s', strtotime($toLocale)+$difference);

        return array($fromGmt, $toGmt);
    }

    public function getTimeFilterWeekly(){
        $currentTimeGmt = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $localeTime = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $difference = strtotime($currentTimeGmt) - strtotime($localeTime);

        $localeDate = explode(' ', $localeTime)[0];
        $toLocale = $localeDate.' 23:59:59';

        $fromLocaleDate = date("Y-m-d", strtotime("-144 hour", strtotime($localeTime))); //-6 days (from 0h day 0 to 23h59'59 day 6)
        $fromLocale = $fromLocaleDate.' 00:00:00';

        $fromGmt = date ('Y-m-d H:i:s', strtotime($fromLocale)+$difference);
        $toGmt = date ('Y-m-d H:i:s', strtotime($toLocale)+$difference);

        return array($fromGmt, $toGmt);
    }

    public function getDailySalesData(){
        $timeArray = $this->getTimeFilterDaily();

        $from = $timeArray[0];
        $to = $timeArray[1];
        $collection = Mage::getModel('sales/order_item')->getCollection()->addFieldToFilter('shipment.created_at', array(
            'from' => $from,
            'to' => $to
        ));
        $collection->join(array('shipment' => 'sales/shipment'), 'main_table.order_id = shipment.order_id', '*');
        $collection->join(array('order' => 'sales/order'), 'main_table.order_id = order.entity_id', array(
            'base_tax_amount' => 'order.base_tax_amount',
            'base_shipping_amount' => 'order.base_shipping_amount',
            'base_discount_amount' => 'order.base_discount_amount',
            'source' => 'order.source',
            'base_grand_total' => 'order.base_grand_total',
            'base_subtotal' => 'order.base_subtotal',
            'increment_id' => 'order.increment_id'
        ))->getSelect()->group('item_id');

        $data = [];
        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i] = [];
            $data[$i]['total'] = 0;
            $data[$i]['subtotal'] = 0;
            $data[$i]['tax'] = 0;
            $data[$i]['ship'] = 0;
            $data[$i]['discount'] = 0;
            $data[$i]['order'] = [];
            $data[$i]['item'] = '';
        }

        foreach ($collection as $item){
            $source = $item->getSource();

            if(!in_array($item->getIncrementId(), $data[$source]['order'])){
                array_push($data[$source]['order'], $item->getIncrementId());
                $data[$source]['total'] += $item->getBaseGrandTotal();
                $data[$source]['subtotal'] += $item->getBaseSubtotal();
                $data[$source]['tax'] += $item->getBaseTaxAmount();
                $data[$source]['ship'] += $item->getBaseShippingAmount();
                $data[$source]['discount'] += $item->getBaseDiscountAmount();
            }

            $data[$source]['item'] .= $item->getSku().', ';
        }

        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i]['order'] = trim(implode(',', $data[$i]['order']), ',');
            $data[$i]['item'] = trim($data[$i]['item'], ', ');
        }
        return $data;
    }

    public function getWeeklySalesData(){
        $dateArray = $this->getTimeFilterWeekly();

        $from = $dateArray[0];
        $to = $dateArray[1];

        $collection = Mage::getModel('sales/order_item')->getCollection()->addFieldToFilter('shipment.created_at', array(
            'from' => $from,
            'to' => $to
        ));
        $collection->join(array('shipment' => 'sales/shipment'), 'main_table.order_id = shipment.order_id', '*');
        $collection->join(array('order' => 'sales/order'), 'main_table.order_id = order.entity_id', array(
            'base_tax_amount' => 'order.base_tax_amount',
            'base_shipping_amount' => 'order.base_shipping_amount',
            'base_discount_amount' => 'order.base_discount_amount',
            'source' => 'order.source',
            'base_grand_total' => 'order.base_grand_total',
            'base_subtotal' => 'order.base_subtotal',
            'increment_id' => 'order.increment_id'
        ))->getSelect()->group('item_id');

        $data = [];
        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i] = [];
            $data[$i]['total'] = 0;
            $data[$i]['subtotal'] = 0;
            $data[$i]['tax'] = 0;
            $data[$i]['ship'] = 0;
            $data[$i]['discount'] = 0;
            $data[$i]['order'] = [];
            $data[$i]['item'] = '';
        }
        foreach ($collection as $item){
            $source = $item->getSource();
            if(!in_array($item->getIncrementId(), $data[$source]['order'])){
                array_push($data[$source]['order'], $item->getIncrementId());
                $data[$source]['total'] += $item->getBaseGrandTotal();
                $data[$source]['subtotal'] += $item->getBaseSubtotal();
                $data[$source]['tax'] += $item->getBaseTaxAmount();
                $data[$source]['ship'] += $item->getBaseShippingAmount();
                $data[$source]['discount'] += $item->getBaseDiscountAmount();
            }

            $data[$source]['item'] .= $item->getSku().', ';
        }

        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i]['order'] = trim(implode(', ', $data[$i]['order']), ', ');
            $data[$i]['item'] = trim($data[$i]['item'], ', ');
        }
        return $data;
    }

    public function getDateLastMonth(){
        $started = Mage::registry('dateStartedMonthlySaleReport');
        if($started) {
            $current_month = date('m', strtotime($started));
            $current_year = date('Y', strtotime($started));
        }else{
            $current_month = date('m');
            $current_year = date('Y');
        }

        if($current_month==1) {
            $lastmonth = 12;
            $current_year = $current_year-1;
        } else {
            $lastmonth = $current_month - 1;
        }

        if($lastmonth < 10) $lastmonth = '0'.$lastmonth;

        $firstdate = $current_year.'-'.$lastmonth.'-'.'01' ;
        $lastdateofmonth=date('t', $lastmonth);//number of days in given month

        $lastdate = $current_year.'-'.$lastmonth.'-'.$lastdateofmonth;

        return [$firstdate, $lastdate];
    }

    public function getDateLastMonthYear($year){
        $started = Mage::registry('dateStartedMonthlySaleReport');
        if($started) {
            $current_month = date('m', strtotime($started));
            $current_year = date('Y', strtotime($started));
        }else{
            $current_month = date('m');
            $current_year = date('Y');
        }

        if($current_month==1) {
            $lastmonth = 12;
            $current_year = $current_year-1;
        } else {
            $lastmonth = $current_month - 1;
        }

        if($lastmonth < 10) $lastmonth = '0'.$lastmonth;

        $lastyear = $current_year - $year;
        $firstdate = $lastyear.'-'.$lastmonth.'-'.'01' ;
        $lastdateofmonth = date('t', $lastmonth);//number of days in given month

        $lastdate = $lastyear.'-'.$lastmonth.'-'.$lastdateofmonth;

        return [$firstdate, $lastdate];
    }

    public function getMonthlySalesData(){
        $lastMonthDate = $this->getDateLastMonth();
        $last1YearDate = $this->getDateLastMonthYear(1);
        $last2YearDate = $this->getDateLastMonthYear(2);

        //get order total
        $lastMonthOrderCollection = $this->getOrderCollectionForTimePeriod($lastMonthDate);

        $lastMonth1YearOrderCollection = $this->getOrderCollectionForTimePeriod($last1YearDate);

        $lastMonth2YearOrderCollection = $this->getOrderCollectionForTimePeriod($last2YearDate);

        //get data ship and tax
        $orderItemlastMonthcollection = $this->getItemCollectionInPeriod($lastMonthDate[0].' 00:00:00', $lastMonthDate[1].' 00:00:00');

        $orderItemYear1collection = $this->getItemCollectionInPeriod($last1YearDate[0].' 00:00:00', $last1YearDate[1].' 00:00:00');

        $orderItemYear2collection = $this->getItemCollectionInPeriod($last2YearDate[0].' 00:00:00', $last2YearDate[1].' 00:00:00');

        $data = [];

        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i] = [];
            $data[$i]['sub_total_last_month'] = 0;
            $data[$i]['ship_last_month'] = 0;
            $data[$i]['tax_last_month'] = 0;
            $data[$i]['total_last_month'] = 0;
            $data[$i]['sub_total_year1'] = 0;
            $data[$i]['total_year1'] = 0;
            $data[$i]['ship_year1'] = 0;
            $data[$i]['tax_year1'] = 0;
            $data[$i]['sub_total_year2'] = 0;
            $data[$i]['total_year2'] = 0;
            $data[$i]['ship_year2'] = 0;
            $data[$i]['tax_year2'] = 0;
        }

        //get total from payment record
        $orderIdArray = [];
        foreach ($lastMonthOrderCollection as $item){
            $source = $item->getSource();
            if(!isset($orderIdArray[$source])) $orderIdArray[$source] = [];
            array_push($orderIdArray[$source], $item->getEntityId());
        }
        foreach ($orderIdArray as $key => $value){
            $paymentCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                ->addFieldToFilter('type', 'invoice')->addFieldToFilter('order_id', array('in' => $value));
            foreach ($paymentCollection as $payment){
                $data[$key]['total_last_month'] += $payment->getTotal();
            }
        }

        $orderIdArray = [];
        foreach ($lastMonth1YearOrderCollection as $item){
            $source = $item->getSource();
            if(!isset($orderIdArray[$source])) $orderIdArray[$source] = [];
            array_push($orderIdArray[$source], $item->getEntityId());
        }
        foreach ($orderIdArray as $key => $value){
            $paymentCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                ->addFieldToFilter('type', 'invoice')->addFieldToFilter('order_id', array('in' => $value));
            foreach ($paymentCollection as $payment){
                $data[$key]['total_year1'] += $payment->getTotal();
            }
        }

        $orderIdArray = [];
        foreach ($lastMonth2YearOrderCollection as $item){
            $source = $item->getSource();
            if(!isset($orderIdArray[$source])) $orderIdArray[$source] = [];
            array_push($orderIdArray[$source], $item->getEntityId());
        }
        foreach ($orderIdArray as $key => $value){
            $paymentCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                ->addFieldToFilter('type', 'invoice')->addFieldToFilter('order_id', array('in' => $value));
            foreach ($paymentCollection as $payment){
                $data[$key]['total_year2'] += $payment->getTotal();
            }
        }

        //get ship and tax
        $orderArray = [];
        foreach ($orderItemlastMonthcollection as $item){
            $source = $item->getSource();
            if(!in_array($item->getIncrementId(), $orderArray)){
                $data[$source]['ship_last_month'] += $item->getBaseShippingAmount();
                $data[$source]['tax_last_month'] += $item->getBaseTaxAmount();
            }
        }
        $orderArray = [];
        foreach ($orderItemYear1collection as $item){
            $source = $item->getSource();
            if(!in_array($item->getIncrementId(), $orderArray)){
                $data[$source]['ship_year1'] += $item->getBaseShippingAmount();
                $data[$source]['tax_year1'] += $item->getBaseTaxAmount();
            }
        }
        $orderArray = [];
        foreach ($orderItemYear2collection as $item){
            $source = $item->getSource();
            if(!in_array($item->getIncrementId(), $orderArray)){
                $data[$source]['ship_year2'] += $item->getBaseShippingAmount();
                $data[$source]['tax_year2'] += $item->getBaseTaxAmount();
            }
        }

        return $data;
    }

    public function getDateLastWeek(){
        $startDate = Mage::registry('dateLastWeekSaleComparisonReport');
        $dateAWeekLater = date ('Y-m-d', strtotime('+144 hour', strtotime($startDate)));
        $currentDate = date('Y-m-d');

        if(strtotime($dateAWeekLater) > strtotime($currentDate)){
            return [$startDate, $currentDate];
        }else{
            return [$startDate, $dateAWeekLater];
        }
    }

    public function getDateOneWeekAgo(){
        $dateArray = $this->getDateLastWeek();
        $from = $dateArray[0];
        $to = $dateArray[1];

        $firstdate = date ('Y-m-d', strtotime('-168 hour', strtotime($from)));
        $lastdate = date ('Y-m-d', strtotime('-168 hour', strtotime($to)));
        return [$firstdate, $lastdate];
    }

    public function getDateLastWeekYear($year){
        $dateArray = $this->getDateLastWeek();
        $from = $dateArray[0];
        $to = $dateArray[1];

        $firstdate = date ('Y-m-d', strtotime('-'.$year.' year', strtotime($from)));
        $lastdate = date ('Y-m-d', strtotime('-'.$year.' year', strtotime($to)));

        return [$firstdate, $lastdate];
    }

    public function getWeeklySalesComparisonData(){
        $oneWeekAgoDate = $this->getDateOneWeekAgo();
        $lastWeekDate = $this->getDateLastWeek();
        $last1YearDate = $this->getDateLastWeekYear(1);
        $last2YearDate = $this->getDateLastWeekYear(2);

        //get order total
        $lastWeekOrderCollection = $this->getOrderCollectionForTimePeriod($lastWeekDate);

        $oneWeekAgoOrderCollection = $this->getOrderCollectionForTimePeriod($oneWeekAgoDate);

        $lastWeek1YearOrderCollection = $this->getOrderCollectionForTimePeriod($last1YearDate);

        $lastWeek2YearOrderCollection = $this->getOrderCollectionForTimePeriod($last2YearDate);

        //get data ship and tax
        $orderItemlastWeekcollection = $this->getItemCollectionInPeriod($lastWeekDate[0].' 00:00:00', $lastWeekDate[1].' 00:00:00');

        $orderItemOneWeekAgocollection = $this->getItemCollectionInPeriod($oneWeekAgoDate[0].' 00:00:00', $oneWeekAgoDate[1].' 00:00:00');

        $orderItemYear1collection = $this->getItemCollectionInPeriod($last1YearDate[0].' 00:00:00', $last1YearDate[1].' 00:00:00');

        $orderItemYear2collection = $this->getItemCollectionInPeriod($last2YearDate[0].' 00:00:00', $last2YearDate[1].' 00:00:00');

        $data = [];

        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i] = [];
            $data[$i]['sub_total_last_week'] = 0;
            $data[$i]['ship_last_week'] = 0;
            $data[$i]['tax_last_week'] = 0;
            $data[$i]['total_last_week'] = 0;
            $data[$i]['sub_total_one_week_ago'] = 0;
            $data[$i]['ship_one_week_ago'] = 0;
            $data[$i]['tax_one_week_ago'] = 0;
            $data[$i]['total_one_week_ago'] = 0;
            $data[$i]['sub_total_year1'] = 0;
            $data[$i]['total_year1'] = 0;
            $data[$i]['ship_year1'] = 0;
            $data[$i]['tax_year1'] = 0;
            $data[$i]['sub_total_year2'] = 0;
            $data[$i]['total_year2'] = 0;
            $data[$i]['ship_year2'] = 0;
            $data[$i]['tax_year2'] = 0;
        }

        //get total from payment record
        $orderIdArray = [];
        foreach ($lastWeekOrderCollection as $item){
            $source = $item->getSource();
            if(!isset($orderIdArray[$source])) $orderIdArray[$source] = [];
            array_push($orderIdArray[$source], $item->getEntityId());
        }
        foreach ($orderIdArray as $key => $value){
            $paymentCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                ->addFieldToFilter('type', 'invoice')->addFieldToFilter('order_id', array('in' => $value));
            foreach ($paymentCollection as $payment){
                $data[$key]['total_last_week'] += $payment->getTotal();
            }
        }

        $orderIdArray = [];
        foreach ($oneWeekAgoOrderCollection as $item){
            $source = $item->getSource();
            if(!isset($orderIdArray[$source])) $orderIdArray[$source] = [];
            array_push($orderIdArray[$source], $item->getEntityId());
        }
        foreach ($orderIdArray as $key => $value){
            $paymentCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                ->addFieldToFilter('type', 'invoice')->addFieldToFilter('order_id', array('in' => $value));
            foreach ($paymentCollection as $payment){
                $data[$key]['total_one_week_ago'] += $payment->getTotal();
            }
        }

        $orderIdArray = [];
        foreach ($lastWeek1YearOrderCollection as $item){
            $source = $item->getSource();
            if(!isset($orderIdArray[$source])) $orderIdArray[$source] = [];
            array_push($orderIdArray[$source], $item->getEntityId());
        }
        foreach ($orderIdArray as $key => $value){
            $paymentCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                ->addFieldToFilter('type', 'invoice')->addFieldToFilter('order_id', array('in' => $value));
            foreach ($paymentCollection as $payment){
                $data[$key]['total_year1'] += $payment->getTotal();
            }
        }

        $orderIdArray = [];
        foreach ($lastWeek2YearOrderCollection as $item){
            $source = $item->getSource();
            if(!isset($orderIdArray[$source])) $orderIdArray[$source] = [];
            array_push($orderIdArray[$source], $item->getEntityId());
        }
        foreach ($orderIdArray as $key => $value){
            $paymentCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                ->addFieldToFilter('type', 'invoice')->addFieldToFilter('order_id', array('in' => $value));
            foreach ($paymentCollection as $payment){
                $data[$key]['total_year2'] += $payment->getTotal();
            }
        }

        //get ship and tax
        $orderArray = [];
        foreach ($orderItemlastWeekcollection as $item){
            $source = $item->getSource();
            if(!in_array($item->getIncrementId(), $orderArray)){
                $data[$source]['ship_last_week'] += $item->getBaseShippingAmount();
                $data[$source]['tax_last_week'] += $item->getBaseTaxAmount();
            }
        }
        $orderArray = [];
        foreach ($orderItemOneWeekAgocollection as $item){
            $source = $item->getSource();
            if(!in_array($item->getIncrementId(), $orderArray)){
                $data[$source]['ship_one_week_ago'] += $item->getBaseShippingAmount();
                $data[$source]['tax_one_week_ago'] += $item->getBaseTaxAmount();
            }
        }
        $orderArray = [];
        foreach ($orderItemYear1collection as $item){
            $source = $item->getSource();
            if(!in_array($item->getIncrementId(), $orderArray)){
                $data[$source]['ship_year1'] += $item->getBaseShippingAmount();
                $data[$source]['tax_year1'] += $item->getBaseTaxAmount();
            }
        }
        $orderArray = [];
        foreach ($orderItemYear2collection as $item){
            $source = $item->getSource();
            if(!in_array($item->getIncrementId(), $orderArray)){
                $data[$source]['ship_year2'] += $item->getBaseShippingAmount();
                $data[$source]['tax_year2'] += $item->getBaseTaxAmount();
            }
        }

        return $data;
    }

    public function getWeeklyRefundComparisonData(){
        $lastWeekDate = $this->getDateOneWeekAgo();
        $oneWeekAgoDate = $this->getDateLastWeek();
        $last1YearDate = $this->getDateLastWeekYear(1);
        $last2YearDate = $this->getDateLastWeekYear(2);

        //use payment record
        $lastWeekOrderCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->join(array('order' => 'sales/order'), 'order.entity_id = main_table.order_id', '*')
            ->addFieldToFilter('created_time', array(
                'from' => $lastWeekDate[0].' 00:00:00',
                'to' => $lastWeekDate[1].' 23:59:59'
            ))->addFieldToFilter('type', 'creditmemo');
        $oneWeekAgoOrderCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->join(array('order' => 'sales/order'), 'order.entity_id = main_table.order_id', '*')
            ->addFieldToFilter('created_time', array(
                'from' => $oneWeekAgoDate[0].' 00:00:00',
                'to' => $oneWeekAgoDate[1].' 23:59:59'
            ))->addFieldToFilter('type', 'creditmemo');
        $lastWeek1YearOrderCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->join(array('order' => 'sales/order'), 'order.entity_id = main_table.order_id', '*')
            ->addFieldToFilter('created_time', array(
                'from' => $last1YearDate[0].' 00:00:00',
                'to' => $last1YearDate[1].' 23:59:59'
            ))->addFieldToFilter('type', 'creditmemo');
        $lastWeek2YearOrderCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->join(array('order' => 'sales/order'), 'order.entity_id = main_table.order_id', '*')
            ->addFieldToFilter('created_time', array(
                'from' => $last2YearDate[0].' 00:00:00',
                'to' => $last2YearDate[1].' 23:59:59'
            ))->addFieldToFilter('type', 'creditmemo');

        $data = [];

        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i] = [];
            $data[$i][0] = 0;
            $data[$i][1] = 0;
            $data[$i][2] = 0;
            $data[$i][3] = 0;
        }

        foreach ($lastWeekOrderCollection as $item){
            $source = $item->getSource();

            $data[$source][0] += $item->getTotal();
        }

        foreach ($oneWeekAgoOrderCollection as $item){
            $source = $item->getSource();

            $data[$source][1] += $item->getTotal();
        }

        foreach ($lastWeek1YearOrderCollection as $item){
            $source = $item->getSource();

            $data[$source][2] += $item->getTotal();
        }

        foreach ($lastWeek2YearOrderCollection as $item){
            $source = $item->getSource();

            $data[$source][3] += $item->getTotal();
        }

        return $data;
    }

    public function getOrderCollectionForTimePeriod($dateArray){
        $orderCollection = Mage::getModel('sales/order_shipment')->getCollection()
            ->join(array('order' => 'sales/order'),
                'main_table.order_id = order.entity_id', array(
                    'entity_id' => 'order.entity_id',
                    'state' => 'order.state',
                    'source' => 'source'
                ))
            ->addFieldToFilter('main_table.created_at', array(
                'from' => $dateArray[0].' 00:00:00',
                'to' => $dateArray[1].' 23:59:59'));
        Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterNotCancelCloseOrder($orderCollection);
        $orderCollection->getSelect()->group('order.entity_id');

        return $orderCollection;
    }

    public function getItemCollectionInPeriod($start, $end){

        $collection = Mage::getModel('sales/order_item')->getCollection();
        $collection->join(array('shipment' => 'sales/shipment'), 'main_table.order_id = shipment.order_id', '*');
        $collection->join(array('order' => 'sales/order'),
                'main_table.order_id = order.entity_id', '*')
            ->addFieldToFilter('shipment.created_at', array(
                'from' => $start,
                'to' => $end
        ));

        Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterNotCancelCloseOrder($collection);

        $collection->getSelect()->group('item_id');
        return $collection;
    }

    public function getSoldData(){
        $coreFlagCode = 'stock_become_zero_report';

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $date = Mage::getModel('core/date')->gmtDate('Y-m-d');
        $flag = Mage::getModel('core/flag', array('flag_code' => $coreFlagCode))->loadSelf();

        if($flag && $flag->getFlagData()){
            $started = $flag->getFlagData();
        }else{
            $started = Mage::getModel('core/date')->gmtDate('Y-m-d').' 00:00:00';
        }

        $query = "SELECT * FROM `tsht_product_history_log` INNER join tsht_inventory_instock_product 
            on tsht_inventory_instock_product.id = tsht_product_history_log.stock_id 
            and tsht_product_history_log.timestamp >= '".$started."'
            join(SELECT group_sku,SUM(qty) total_qty
                 FROM tsht_inventory_instock_product            
                 GROUP BY group_sku) as sum_qty on sum_qty.group_sku = tsht_inventory_instock_product.group_sku
            where sum_qty.total_qty = 0
            and tsht_product_history_log.qty like '-%' ORDER BY tsht_inventory_instock_product.group_sku ASC";

        $results = $readConnection->fetchAll($query);
        $emailData = [];
        $emailedItemSkus = [];
        foreach ($results as $soldItem){
            $sku = $soldItem['sku'];
            $baseSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
            $group_sku = $soldItem['group_sku'];
            //get sku from group_sku
            $data = [];
            $data['sku'] = $sku; //sku
            if($group_sku){
                $data['sku'] = $group_sku;
            }
            if(!in_array($data['sku'], $emailedItemSkus)){
                array_push($emailedItemSkus, $data['sku']);
            }else{
                continue;
            }

            $orderItem = Mage::getModel('sales/order_item')->getCollection()
                ->join(array('order' => 'sales/order'), 'main_table.order_id = order.entity_id', '*')
                ->addFieldToFilter('sku', array('like' => $sku.'%'));
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterActiveOrder($orderItem);
            $orderItem = $orderItem->setOrder('last_history_id', 'DESC')->getFirstItem();

            $data['order_id'] = $orderItem->getIncrementId();  //order id
            $data['qty'] = $orderItem->getQtyOrdered(); //qty

            //get reorder_point
            $query = "SELECT group_sku, MAX(reorder_point) max_reorder_point
                 FROM tsht_inventory_instock_product
                 where group_sku = '".$group_sku."'";
            $reorderPoint = $readConnection->fetchAll($query)[0]['max_reorder_point'];
            $data['reorder_point'] = $reorderPoint;

            //get in openOrder
            $itemsInOrderCollection = Mage::getModel('sales/order_item')->getCollection()
                ->join(array('orderData'=>'sales/order'), 'orderData.entity_id=main_table.order_id', array('*'))
                ->join(array('inventory'=>'opentechiz_inventory/instock'), 'inventory.sku=main_table.sku')
                ->addFieldToFilter('group_sku', $group_sku);
            $itemsInOrderCollection = Mage::helper('opentechiz_reportExtend/collection_filter')->addOrderNeedItemFilter($itemsInOrderCollection);
            $qtyInOrders = 0;

            foreach ($itemsInOrderCollection as $item){
                $qtyInCurrentOrderItem = (int)($item->getQtyOrdered() - $item->getQtyCanceled() - $item->getQtyRefunded() - $item->getData('count'));
                $qtyInOrders += $qtyInCurrentOrderItem;
            }
            $data['in_open_order'] = $qtyInOrders;

            //get cost
            $helper = Mage::helper('opentechiz_inventory');
            if(strtotime($helper->getLastCostDate($sku))) {
                $data['cost_date'] = Mage::getModel('core/date')->date('M d, Y', $helper->getLastCostDate($sku));
            }else{
                $data['cost_date'] = 'N/A';
            }
            $data['cost'] = '$'.number_format($helper->getLastCost($sku), 2, '.', '');

            //get 3m/ret, 12m/ret
            $sku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
            if($group_sku){
                $sku = $group_sku;
            }
            $sku = $sku.'%';
            $data['12m'] = $this->getNumberOfSoldPerReturn($sku, 12);
            $data['3m'] = $this->getNumberOfSoldPerReturn($sku, 3);

            $itemCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', array(
                array('like' => $baseSku),
                array('like' => $baseSku.'\_%')
            ));
            $stringOther = '';

            $skus = [];
            foreach ($itemCollection as $instockItem){
                if($instockItem->getGroupSku()) {
                    $sku = $instockItem->getGroupSku();
                }else{
                    $sku = $instockItem->getSku();
                }
                if(!array_key_exists($sku, $skus)){
                    $skus[$sku] = $instockItem->getQty();
                }else{
                    $skus[$sku] += $instockItem->getQty();
                }
            }

            foreach ($skus as $sku => $qty){
                if($sku == '') continue;
                $stringOther .= $sku . ' x ' . $qty . '<br>';
            }

            //get manufacturing qty
            $sku = $baseSku;
            //get sku from group_sku
            if($group_sku){
                $sku = $group_sku;
            }
            $data['manufacture'] = $this->getCurrentManufacturingQty($sku);
            $data['other'] = $stringOther;

            $sku = $baseSku;
            $_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
            if(!$_product || !$_product->getId()){
                continue;
            }
            $imageUrl = Mage::helper('catalog/image')->init($_product, 'thumbnail')->resize(120, 120)->__toString();
            $data['price'] = '$'.number_format($_product->getSpecialPrice(), 2, '.', ''); //sales price
            $data['image'] = $imageUrl;

            //check clearance sales
            $flag = false;
            if($_product && $_product->getId()) {
                $productCats = $_product->getCategoryIds();
                foreach ($productCats as $productCat) {
                    $_cat = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getId())->load($productCat);
                    if ($_cat->getName() == 'Clearance Sale') {
                        $flag = true;
                    }
                }
            }

            if($flag == true) $data['close_out'] = 'YES';
            else $data['close_out'] = 'NO';

            array_push($emailData, $data);
        }

        return $emailData;
    }

    public function getWeeklyRefundData(){
        $dateArray = $this->getTimeFilterWeekly();

        $from = $dateArray[0];
        $to = $dateArray[1];

        $collection = Mage::getModel('opentechiz_return/return')->getCollection()->addFieldToFilter('main_table.created_at', array(
            'from' => $from,
            'to' => $to
        ))->addFieldToFilter('main_table.solution', array('in' => $this->returnSolutionArray));
        $collection->join(array('order_item' => 'sales/order_item'), 'main_table.order_item_id = order_item.item_id', '*')
            ->join(array('order' => 'sales/order'), 'order_item.order_id = order.entity_id', '*');

        $data = [];
        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i] = [];
            $data[$i]['total'] = 0;
            $data[$i]['subtotal'] = 0;

            $data[$i]['order'] = [];
            $data[$i]['item'] = '';
        }

        $orderTotalCounted = [];
        foreach ($collection as $item){
            $source = $item->getSource();
            if(in_array($item->getOrderId(), $orderTotalCounted)){
                $refunded = 0;
            }else {
                $orderId = $item->getOrderId();
                $totalRefunded = 0;
                $paymentCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                    ->addFieldToFilter('type', 'creditmemo')->addFieldToFilter('order_id', $orderId);
                foreach ($paymentCollection as $payment){
                    $totalRefunded += $payment->getTotal();
                }
                $refunded = abs(($totalRefunded));
            }

            if(!in_array($item->getIncrementId(), $data[$source]['order'])) {
                array_push($data[$source]['order'], $item->getIncrementId());
            }
            $data[$source]['total'] = $data[$source]['total'] - $refunded;
            $data[$source]['subtotal'] = $data[$source]['subtotal'] - $refunded;

            $data[$source]['item'] .= $item->getSku().', ';

            array_push($orderTotalCounted, $item->getOrderId());
        }

        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i]['order'] = trim(implode(', ', $data[$i]['order']), ', ');
            $data[$i]['item'] = trim($data[$i]['item'], ', ');
        }
        return $data;
    }

    public function getDailyRefundData(){
        $timeArray = $this->getTimeFilterDaily();

        $from = $timeArray[0];
        $to = $timeArray[1];

        $collection = Mage::getModel('opentechiz_return/return')->getCollection()->addFieldToFilter('main_table.created_at', array(
            'from' => $from,
            'to' => $to
        ))->addFieldToFilter('main_table.solution', array('in' => $this->returnSolutionArray));
        $collection->join(array('order_item' => 'sales/order_item'), 'main_table.order_item_id = order_item.item_id', '*')
            ->join(array('order' => 'sales/order'), 'order_item.order_id = order.entity_id', '*');

        $data = [];
        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i] = [];
            $data[$i]['total'] = 0;
            $data[$i]['subtotal'] = 0;

            $data[$i]['order'] = [];
            $data[$i]['item'] = '';
        }

        $orderTotalCounted = [];
        foreach ($collection as $item){
            $source = $item->getSource();
            if(in_array($item->getOrderId(), $orderTotalCounted)){
                $refunded = 0;
            }else {
                $orderId = $item->getOrderId();
                $totalRefunded = 0;
                $paymentCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                    ->addFieldToFilter('type', 'creditmemo')->addFieldToFilter('order_id', $orderId);
                foreach ($paymentCollection as $payment){
                    $totalRefunded += $payment->getTotal();
                }
                $refunded = abs(($totalRefunded));
            }

            if(!in_array($item->getIncrementId(), $data[$source]['order'])) {
                array_push($data[$source]['order'], $item->getIncrementId());
            }
            $data[$source]['total'] = $data[$source]['total'] - $refunded;
            $data[$source]['subtotal'] = $data[$source]['subtotal'] - $refunded;

            $data[$source]['item'] .= $item->getSku().', ';

            array_push($orderTotalCounted, $item->getOrderId());
        }

        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i]['order'] = trim(implode(',', $data[$i]['order']), ',');
            $data[$i]['item'] = trim($data[$i]['item'], ', ');
        }
        return $data;
    }

    public function getMonthlyRefundData(){
        $lastMonthDate = $this->getDateLastMonth();
        $last1YearDate = $this->getDateLastMonthYear(1);
        $last2YearDate = $this->getDateLastMonthYear(2);

        //use payment record
        $lastMonthOrderCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->join(array('order' => 'sales/order'), 'order.entity_id = main_table.order_id', '*')
            ->addFieldToFilter('created_time', array(
                'from' => $lastMonthDate[0].' 00:00:00',
                'to' => $lastMonthDate[1].' 00:00:00'
            ))->addFieldToFilter('type', 'creditmemo');
        $lastMonth1YearOrderCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->join(array('order' => 'sales/order'), 'order.entity_id = main_table.order_id', '*')
            ->addFieldToFilter('created_time', array(
                'from' => $last1YearDate[0].' 00:00:00',
                'to' => $last1YearDate[1].' 00:00:00'
            ))->addFieldToFilter('type', 'creditmemo');
        $lastMonth2YearOrderCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->join(array('order' => 'sales/order'), 'order.entity_id = main_table.order_id', '*')
            ->addFieldToFilter('created_time', array(
                'from' => $last2YearDate[0].' 00:00:00',
                'to' => $last2YearDate[1].' 00:00:00'
            ))->addFieldToFilter('type', 'creditmemo');

        $data = [];

        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i] = [];
            $data[$i][0] = 0;
            $data[$i][1] = 0;
            $data[$i][2] = 0;
        }

        foreach ($lastMonthOrderCollection as $item){
            $source = $item->getSource();

            $data[$source][0] += $item->getTotal();
        }

        foreach ($lastMonth1YearOrderCollection as $item){
            $source = $item->getSource();

            $data[$source][1] += $item->getTotal();
        }

        foreach ($lastMonth2YearOrderCollection as $item){
            $source = $item->getSource();

            $data[$source][2] += $item->getTotal();
        }

        return $data;
    }

    public function getUserId(){
        if(Mage::getSingleton('admin/session')->isLoggedIn()){
            $user_id = Mage::getSingleton('admin/session')->getUser()->getId();
        }else{
            $user_id = 0;
        }
        return $user_id;
    }

    public function getBecomeMoreThanZeroData(){
        $coreFlagCode = 'stock_become__more_than_zero_report';

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $date = Mage::getModel('core/date')->gmtDate('Y-m-d');
        $flag = Mage::getModel('core/flag', array('flag_code' => $coreFlagCode))->loadSelf();

        if($flag && $flag->getFlagData()){
            $started = $flag->getFlagData();
        }else {
            $started = Mage::getModel('core/date')->gmtDate('Y-m-d') . ' 00:00:00';
        }

        $query = "SELECT * FROM `tsht_product_history_log` INNER join tsht_inventory_instock_product 
        on tsht_inventory_instock_product.id = tsht_product_history_log.stock_id 
        and tsht_product_history_log.timestamp >= '".$started."' 
        where tsht_inventory_instock_product.qty > 0
        and tsht_product_history_log.qty not like '-%' 
        and tsht_product_history_log.qty not like '0' and qty_before_movement = 0";

        $results = $readConnection->fetchAll($query);
        $emailData = [];
        $emailedItemSkus = [];
        foreach ($results as $soldItem){
            $sku = $soldItem['sku'];
            $baseSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
            $group_sku = $soldItem['group_sku'];

            if(!in_array($group_sku, $emailedItemSkus)){
                array_push($emailedItemSkus, $group_sku);
            }else{
                continue;
            }

            $data = [];
            $data['sku'] = $sku; //sku
            $orderItem = Mage::getModel('sales/order_item')->getCollection()
                ->join(array('order' => 'sales/order'), 'main_table.order_id = order.entity_id', '*')
                ->addFieldToFilter('sku', array('like' => $sku.'%'));
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterActiveOrder($orderItem);
            $orderItem = $orderItem->setOrder('last_history_id', 'DESC')
                ->getFirstItem();
            $data['order_id'] = $orderItem->getIncrementId();  //order id
            $data['qty'] = $orderItem->getQtyOrdered(); //qty

            //get cost
            $helper = Mage::helper('opentechiz_inventory');
            $date = $helper->getLastCostDate($sku);
            $date .= " 10:00:00";
            if(strtotime($date)) {
                $createdtime = new Zend_Date(strtotime($date));
                $data['cost_date'] = Mage::getModel('core/date')->date('M d, Y', $createdtime);
            }else{
                $data['cost_date'] = 'N/A';
            }
            $data['cost'] = '$'.number_format($helper->getLastCost($sku), 2, '.', '');

            //get 3m/ret, 12m/ret
            $sku = $baseSku;
            if($group_sku){
                $sku = $group_sku;
            }
            $sku = $sku.'%';
            $data['12m'] = $this->getNumberOfSoldPerReturn($sku, 12);
            $data['3m'] = $this->getNumberOfSoldPerReturn($sku, 3);

            $itemCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', array('like' => $sku));
            $stringOther = '';

            $skus = [];
            foreach ($itemCollection as $instockItem){
                if($instockItem->getGroupSku()) {
                    $sku = $instockItem->getGroupSku();
                }else{
                    $sku = $instockItem->getSku();
                }
                if(!array_key_exists($sku, $skus)){
                    $skus[$sku] = $instockItem->getQty();
                }else{
                    $skus[$sku] += $instockItem->getQty();
                }
            }

            foreach ($skus as $sku => $qty){
                $stringOther .= $sku . ' x ' . $qty . '<br>';
            }

            //get manufacturing qty
            $sku = $baseSku;
            //get sku from group_sku
            if($group_sku){
                $sku = $group_sku;
            }
            $data['manufacture'] = $this->getCurrentManufacturingQty($sku);
            $data['other'] = $stringOther;

            $sku = $baseSku;
            $_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
            if(!$_product || !$_product->getId()){
                continue;
            }
            $imageUrl = Mage::helper('catalog/image')->init($_product, 'thumbnail')->resize(120, 120)->__toString();
            $data['price'] = '$'.number_format($_product->getSpecialPrice(), 2, '.', ''); //sales price
            $data['image'] = $imageUrl;

            //check clearance sales
            $flag = false;
            if($_product && $_product->getId()) {
                $productCats = $_product->getCategoryIds();
                foreach ($productCats as $productCat) {
                    $_cat = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getId())->load($productCat);
                    if ($_cat->getName() == 'Clearance Sale') {
                        $flag = true;
                    }
                }
            }

            if($flag == true) $data['close_out'] = 'YES';
            else $data['close_out'] = 'NO';

            array_push($emailData, $data);
        }

        return $emailData;
    }

    public function getReorderData(){
        $coreFlagCode = 'reorder_point_report';

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $date = Mage::getModel('core/date')->gmtDate('Y-m-d');
        $flag = Mage::getModel('core/flag', array('flag_code' => $coreFlagCode))->loadSelf();

        if($flag && $flag->getFlagData()){
            $started = $flag->getFlagData();
        }else {
            $started = Mage::getModel('core/date')->gmtDate('Y-m-d') . ' 00:00:00';
        }

        $query = "SELECT * FROM `tsht_product_history_log` INNER join tsht_inventory_instock_product 
            on tsht_inventory_instock_product.id = tsht_product_history_log.stock_id 
            and tsht_product_history_log.timestamp >= '".$started."'
            join(SELECT group_sku,SUM(qty) total_qty
                 FROM tsht_inventory_instock_product            
                 GROUP BY group_sku) as sum_qty on sum_qty.group_sku = tsht_inventory_instock_product.group_sku
            where sum_qty.total_qty <= tsht_inventory_instock_product.reorder_point
            and tsht_product_history_log.qty like '-%' ORDER BY `tsht_inventory_instock_product`.`sku` ASC";

        $results = $readConnection->fetchAll($query);

        $emailData = [];
        $emailedItemSkus = [];
        foreach ($results as $soldItem){
            $sku = $soldItem['sku'];
            $baseSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
            $group_sku = $soldItem['group_sku'];

            if(!in_array($group_sku, $emailedItemSkus)){
                array_push($emailedItemSkus, $group_sku);
            }else{
                continue;
            }

            $data = [];
            //get sku from group_sku
            if($group_sku){
                $sku = $group_sku;
            }
            $data['sku'] = $sku; //sku

            $orderItem = Mage::getModel('sales/order_item')->getCollection()
                ->join(array('order' => 'sales/order'), 'main_table.order_id = order.entity_id', '*')
                ->addFieldToFilter('sku', array('like' => $sku.'%'));
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterActiveOrder($orderItem);
            $orderItem = $orderItem->setOrder('last_history_id', 'DESC')
                ->getFirstItem();
            $data['order_id'] = $orderItem->getIncrementId();  //order id
            $data['qty'] = $orderItem->getQtyOrdered(); //qty

            //get reorder_point
            $query = "SELECT group_sku, MAX(reorder_point) max_reorder_point
                 FROM tsht_inventory_instock_product
                 where group_sku = '".$group_sku."'";
            $reorderPoint = $readConnection->fetchAll($query)[0]['max_reorder_point'];
            $data['reorder_point'] = $reorderPoint;

            //get in openOrder
            $itemsInOrderCollection = Mage::getModel('sales/order_item')->getCollection()
                ->join(array('orderData'=>'sales/order'), 'orderData.entity_id=main_table.order_id', array('*'))
                ->join(array('inventory'=>'opentechiz_inventory/instock'), 'inventory.sku=main_table.sku')
                ->addFieldToFilter('group_sku', $group_sku);
            $itemsInOrderCollection = Mage::helper('opentechiz_reportExtend/collection_filter')->addOrderNeedItemFilter($itemsInOrderCollection);
            $qtyInOrders = 0;

            foreach ($itemsInOrderCollection as $item){
                $qtyInCurrentOrderItem = (int)($item->getQtyOrdered() - $item->getQtyCanceled() - $item->getQtyRefunded() - $item->getData('count'));
                $qtyInOrders += $qtyInCurrentOrderItem;
            }
            $data['in_open_order'] = $qtyInOrders;

            //get cost
            $helper = Mage::helper('opentechiz_inventory');
            if(strtotime($helper->getLastCostDate($sku))) {
                $data['cost_date'] = Mage::getModel('core/date')->date('M d, Y', $helper->getLastCostDate($sku));
            }else{
                $data['cost_date'] = 'N/A';
            }
            $data['cost'] = '$'.number_format($helper->getLastCost($sku), 2, '.', '');

            //get 3m/ret, 12m/ret
            $sku = $baseSku;
            if($group_sku){
                $sku = $group_sku;
            }
            $sku = $sku.'%';
            $data['12m'] = $this->getNumberOfSoldPerReturn($sku, 12);
            $data['3m'] = $this->getNumberOfSoldPerReturn($sku, 3);

            $itemCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', array(
                array('like' => $baseSku),
                array('like' => $baseSku.'\_%')
            ));
            $stringOther = '';

            $skus = [];
            foreach ($itemCollection as $instockItem){
                if($instockItem->getGroupSku()) {
                    $sku = $instockItem->getGroupSku();
                }else{
                    $sku = $instockItem->getSku();
                }
                if(!array_key_exists($sku, $skus)){
                    $skus[$sku] = $instockItem->getQty();
                }else{
                    $skus[$sku] += $instockItem->getQty();
                }
            }

            foreach ($skus as $sku => $qty){
                if($sku == '') continue;
                $stringOther .= $sku . ' x ' . $qty . '<br>';
            }

            //get manufacturing qty
            $sku = $baseSku;
            //get sku from group_sku
            if($group_sku){
                $sku = $group_sku;
            }
            $data['manufacture'] = $this->getCurrentManufacturingQty($sku);
            $data['items_delivered'] = $this->getItemsDeliveredReorder($sku);
            $data['other'] = $stringOther;

            $sku = $baseSku;
            $_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
            if(!$_product || !$_product->getId()){
                continue;
            }
            $imageUrl = Mage::helper('catalog/image')->init($_product, 'thumbnail')->resize(120, 120)->__toString();
            $data['price'] = '$'.number_format($_product->getSpecialPrice(), 2, '.', ''); //sales price
            $data['image'] = $imageUrl;

            //check clearance sales
            $flag = false;
            if($_product && $_product->getId()) {
                $productCats = $_product->getCategoryIds();
                foreach ($productCats as $productCat) {
                    $_cat = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getId())->load($productCat);
                    if ($_cat->getName() == 'Clearance Sale') {
                        $flag = true;
                    }
                }
            }

            if($flag == true) $data['close_out'] = 'YES';
            else $data['close_out'] = 'NO';

            array_push($emailData, $data);
        }

        return $emailData;
    }

    public function getListSupplier(){
        $retour =array();
        $collection = Mage::getModel('opentechiz_purchase/supplier')
            ->getCollection()
            ->setOrder('name', 'asc');
        foreach ($collection as $item)
        {
            $retour[$item->getId()] = $item->getName();
        }
        return $retour;
    }

    public function getNumberOfSoldPerReturn($sku, $months){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $currentTime = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $months = '-'.$months.' months';
        $lastXMonth = date('Y-m-d H:i:s', strtotime($months, strtotime($currentTime)));

        //item shipped
        $query = "SELECT sum(qty) as count FROM `tsht_sales_flat_shipment_item` 
                      inner join tsht_sales_flat_shipment 
                      on tsht_sales_flat_shipment.entity_id=tsht_sales_flat_shipment_item.parent_id 
                      where sku LIKE '".$sku."' and created_at > '".$lastXMonth."'";
        $result = $readConnection->fetchAll($query)[0];
        $sold = (int)$result['count'];

        //item attached to order and not shipped
        $query = "SELECT count(*) as count from tsht_inventory_order_item 
                      join tsht_inventory_item on tsht_inventory_order_item.item_id = tsht_inventory_item.item_id 
                      and tsht_inventory_item.state not in ".$this->shippedStates."
                      join tsht_sales_flat_order_item 
                      on tsht_inventory_order_item.order_item_id = tsht_sales_flat_order_item.item_id
                      join tsht_sales_flat_order on tsht_sales_flat_order.entity_id = tsht_sales_flat_order_item.order_id
                      join tsht_order_status_change_log
                      on tsht_order_status_change_log.id = tsht_sales_flat_order.last_history_id
                      where tsht_sales_flat_order_item.sku like '".$sku."'
                      and tsht_order_status_change_log.updated_at > '".$lastXMonth."'";
        $result = $readConnection->fetchAll($query)[0];
        $result = (int)$result['count'];
        $sold += $result;

        $returnCollection = Mage::getModel('opentechiz_return/return')->getCollection()->addFieldToFilter('main_table.created_at', array(
            'from' => $lastXMonth,
            'to' => $currentTime
        ))->addFieldToFilter('main_table.solution', array('in' => OpenTechiz_Return_Helper_Data::ALL_SOLUTIONS))
            ->join(array('item' => 'sales/order_item'), 'item.item_id = main_table.order_item_id')->addFieldToFilter('sku', array('like' => $sku));
        $return = count($returnCollection);
        return $sold.'/'.$return;
    }

    public function getItemsDeliveredReorder($sku){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $instockItem = Mage::getModel('opentechiz_inventory/instock')->load($sku, 'sku');
        if($instockItem && $instockItem->getGroupSku()){
            $sku = $instockItem->getGroupSku();
        }

        $query = 'SELECT tsht_purchase_order_item.*,tsht_purchase_supplier.is_active_portal FROM `tsht_purchase_order_item` 
                      join tsht_purchase_order on tsht_purchase_order.po_id = tsht_purchase_order_item.po_id 
                      join tsht_purchase_supplier on tsht_purchase_supplier.supplier_id = tsht_purchase_order.supplier_id
                      where sku like "'.$sku.'%" and shipped_qty > delivered_qty and status not in '.$this->excludedPOStatusInReport;

        $results = $readConnection->fetchAll($query);
        $html = '';
        foreach ($results as $result){
            if ($result['is_active_portal'] == OpenTechiz_Purchase_Helper_Data::SUP_NOT_ACTIVE_PORTAL) {
                continue;
            }

            $shipped_qty = 0;
            $delivered_qty = 0;
            if ($result['shipped_qty']) {
                $shipped_qty = $result['shipped_qty'];
            }

            if ($result['delivered_qty']) {
                $delivered_qty = $result['delivered_qty'];
            }

            $variantSku = $result['sku'];
            $html .= '<span style="margin-left: 20px">' . $variantSku . '</span>';
            $html .= '<span> (Shipped/Delivered: ' . $shipped_qty .'/'. $delivered_qty.')</span>';
            $html .= '<br>';
            
        }

        return $html;
    }

    public function getCurrentManufacturingQty($sku){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $instockItem = Mage::getModel('opentechiz_inventory/instock')->load($sku, 'sku');
        if($instockItem && $instockItem->getGroupSku()){
            $sku = $instockItem->getGroupSku();
        }
        $manufacture = '';
        $results = Mage::helper('opentechiz_production')->queryAllProduction($sku);
        $productions = [];
        foreach ($results as $result){
            $variantSku = $result['sku'];
            $variantSkuParts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $variantSku);
            foreach ($variantSkuParts as $key => $variantSkuPart){
                if($key == 0) continue;
                $variantSkuPart = str_replace('in.', '', $variantSkuPart);
                $variantSkuPart = Mage::helper('opentechiz_inventory')->convertSizeString($variantSkuPart);
                if(is_numeric($variantSkuPart)) unset($variantSkuParts[$key]);
            }
            $variantSku = implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $variantSkuParts);
            if(isset($productions[$variantSku])) $productions[$variantSku]++;
            else $productions[$variantSku] = 1;
        }
        $total = 0;
        foreach ($productions as $key => $value){
            $total += $value;
        }
        if($total > 0) {
            $manufacture .= '<strong>In Production (Magento):</strong> <br>';
            foreach ($productions as $key => $value) {
                $manufacture .= '<span style="margin-left: 20px">' . $key . ' x ' . $value . '</span>';
                $manufacture .= '<br>';
            }
        }

        $query = 'SELECT * FROM `tsht_purchase_order_item` 
                      join tsht_purchase_order on tsht_purchase_order.po_id = tsht_purchase_order_item.po_id 
                      join tsht_purchase_supplier on tsht_purchase_supplier.supplier_id = tsht_purchase_order.supplier_id
                      where sku like "'.$sku.'%" and qty > delivered_qty and status not in '.$this->excludedPOStatusInReport;

        $results = $readConnection->fetchAll($query);
        $POs = [];
        foreach ($results as $result){
            $countPO = ($result['qty'] - $result['delivered_qty']);
            $variantSku = $result['sku'];
            $variantSkuParts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $variantSku);
            foreach ($variantSkuParts as $key => $variantSkuPart){
                if($key == 0) continue;
                $variantSkuPart = str_replace('in.', '', $variantSkuPart);
                $variantSkuPart = Mage::helper('opentechiz_inventory')->convertSizeString($variantSkuPart);
                if(is_numeric($variantSkuPart)) unset($variantSkuParts[$key]);
            }
            $variantSku = implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $variantSkuParts);
            if(isset($POs[$variantSku])){
                $POs[$variantSku]['qty'] += $countPO;
            } else {
                $POs[$variantSku] = [];
                $POs[$variantSku]['qty'] = $countPO;
                $POs[$variantSku]['delivery'] = Mage::helper('core')->formatDate($result['delivery_date']);
                $POs[$variantSku]['sup_name'] = $result['name'];
            }
        }
        $total = 0;
        foreach ($POs as $key => $value){
            $total += $value['qty'];
        }
        if($total > 0) {
            $manufacture .= '<strong>Purchase Order:</strong> <br>';
            foreach ($POs as $key => $value) {
                $explode = str_split( $value['sup_name'], 1);
                $first3Letter = '';
                $count = 0;
                foreach ($explode as $char){
                    if($char != ' '){
                        $count++;
                    }
                    if($count > 3) break;
                    $first3Letter .= $char;
                }
                
                $manufacture .= '<span style="margin-left: 20px">' . $key . ' x ' . $value['qty'] . '</span>';
                $manufacture .= '<span> (Delivery: ' . $value['delivery'] . ') '.$first3Letter.'</span>';
                $manufacture .= '<br>';
            }
        }

        return $manufacture;
    }

    public function getClearanceSaleData(){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $category = Mage::getResourceModel('catalog/category_collection')
            ->addFieldToFilter('name', 'Clearance Sale')
            ->getFirstItem();
        $categoryId = $category->getId();

        $query = "SELECT * FROM `tsht_catalog_category_product` 
                  join(SELECT product_id, group_sku, SUM(qty) total_qty FROM tsht_inventory_instock_product 
                  GROUP BY group_sku) as sum_qty on sum_qty.product_id = tsht_catalog_category_product.product_id 
                  where tsht_catalog_category_product.category_id = ".$categoryId." and total_qty = 0  
                  ORDER BY group_sku ASC";

        $results = $readConnection->fetchAll($query);
        $emailData = [];
        $emailedItemSkus = [];
        foreach ($results as $soldItem){
            $sku = $soldItem['group_sku'];
            $baseSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
            $group_sku = $soldItem['group_sku'];
            //get sku from group_sku
            $data = [];
            $data['sku'] = $sku; //sku
            if($group_sku){
                $data['sku'] = $group_sku;
            }
            if(!in_array($data['sku'], $emailedItemSkus)){
                array_push($emailedItemSkus, $data['sku']);
            }else{
                continue;
            }

            //get reorder_point
            $query = "SELECT group_sku, MAX(reorder_point) max_reorder_point
                 FROM tsht_inventory_instock_product
                 where group_sku = '".$group_sku."'";
            $reorderPoint = $readConnection->fetchAll($query)[0]['max_reorder_point'];
            $data['reorder_point'] = $reorderPoint;

            //get in openOrder
            $itemsInOrderCollection = Mage::getModel('sales/order_item')->getCollection()
                ->join(array('orderData'=>'sales/order'), 'orderData.entity_id=main_table.order_id', array('*'))
                ->join(array('inventory'=>'opentechiz_inventory/instock'), 'inventory.sku=main_table.sku')
                ->addFieldToFilter('group_sku', $group_sku);
            $itemsInOrderCollection = Mage::helper('opentechiz_reportExtend/collection_filter')->addOrderNeedItemFilter($itemsInOrderCollection);
            $qtyInOrders = 0;

            foreach ($itemsInOrderCollection as $item){
                $qtyInCurrentOrderItem = (int)($item->getQtyOrdered() - $item->getQtyCanceled() - $item->getQtyRefunded() - $item->getData('count'));
                $qtyInOrders += $qtyInCurrentOrderItem;
            }
            $data['in_open_order'] = $qtyInOrders;

            //get cost
            $helper = Mage::helper('opentechiz_inventory');
            if(strtotime($helper->getLastCostDate($sku))) {
                $data['cost_date'] = Mage::getModel('core/date')->date('M d, Y', $helper->getLastCostDate($sku));
            }else{
                $data['cost_date'] = 'N/A';
            }
            $data['cost'] = '$'.number_format($helper->getLastCost($sku), 2, '.', '');

            //get 3m/ret, 12m/ret
            $sku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
            if($group_sku){
                $sku = $group_sku;
            }
            $sku = $sku.'%';
            $data['12m'] = $this->getNumberOfSoldPerReturn($sku, 12);
            $data['3m'] = $this->getNumberOfSoldPerReturn($sku, 3);

            $itemCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', array(
                array('like' => $baseSku),
                array('like' => $baseSku.'\_%')
            ));
            $stringOther = '';

            $skus = [];
            foreach ($itemCollection as $instockItem){
                if($instockItem->getGroupSku()) {
                    $sku = $instockItem->getGroupSku();
                }else{
                    $sku = $instockItem->getSku();
                }
                if(!array_key_exists($sku, $skus)){
                    $skus[$sku] = $instockItem->getQty();
                }else{
                    $skus[$sku] += $instockItem->getQty();
                }
            }

            foreach ($skus as $sku => $qty){
                if($sku == '') continue;
                $stringOther .= $sku . ' x ' . $qty . '<br>';
            }

            //get manufacturing qty
            $sku = $baseSku;
            //get sku from group_sku
            if($group_sku){
                $sku = $group_sku;
            }
            $data['manufacture'] = $this->getCurrentManufacturingQty($sku);
            $data['other'] = $stringOther;

            $sku = $baseSku;
            $_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
            if(!$_product || !$_product->getId()){
                continue;
            }
            $imageUrl = Mage::helper('catalog/image')->init($_product, 'thumbnail')->resize(120, 120)->__toString();
            $data['price'] = '$'.number_format($_product->getSpecialPrice(), 2, '.', ''); //sales price
            $data['image'] = $imageUrl;

            array_push($emailData, $data);
        }

        return $emailData;
    }

    public function validatedEmailList($recipientEmail,$email = array()){
        
        if(!empty($email)){
            $listEmail = $email;
        }else{
            $listEmail = array();
        }
        $recipientEmail = preg_split("/[\s,;]+/", $recipientEmail);
        if(!empty($recipientEmail)){
          foreach($recipientEmail as $k => $val){
              if (strpos($val, '@') !== false) {
                  $listEmail[] = trim($val);
              } 
          }
        }
        return $listEmail;
    }

    public function numberFormat($float){
        return number_format ($float, 2, ".", "," );
    }
}
