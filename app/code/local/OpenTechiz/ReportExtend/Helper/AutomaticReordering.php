<?php
class OpenTechiz_ReportExtend_Helper_AutomaticReordering extends Mage_Core_Helper_Abstract
{
    const XML_SYSTEM_AUTOMATIC_REORDERING_MONTHS_NEW = 'automatic_reordering/general/months_new';
    const XML_SYSTEM_AUTOMATIC_REORDERING_EXCLUDE_CATEGORIES = 'automatic_reordering/general/exclude_categories';
    const XML_SYSTEM_AUTOMATIC_REORDERING_EXCLUDE_PRODUCTS = 'automatic_reordering/general/exclude_products';
    const XML_SYSTEM_AUTOMATIC_REORDERING_EXCLUDE_SUPPLIERS = 'automatic_reordering/general/exclude_suppliers';
    const XML_SYSTEM_AUTOMATIC_REORDERING_RINGS_GENDER_ATTRIBUTE = 'automatic_reordering/rings/gender_attribute';
    const XML_SYSTEM_AUTOMATIC_REORDERING_RINGS_MAN_SIZE = 'automatic_reordering/rings/man_size';
    const XML_SYSTEM_AUTOMATIC_REORDERING_RINGS_WOMAN_SIZE = 'automatic_reordering/rings/woman_size';
    const XML_SYSTEM_AUTOMATIC_REORDERING_RINGS_UNISEX_SIZE = 'automatic_reordering/rings/unisex_size';
    const GENDER_MAN_VALUE = 4975;
    const GENDER_WOMAN_VALUE = 4977;
    const GENDER_UNISEX_VALUE = 4976;

    protected $products = [];

    public function createAiReorderingReport()
    {
        $table = Mage::getSingleton('core/resource')->getTableName('opentechiz_reportExtend/automatic_reordering');
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $connection->truncateTable($table);
        $itemsToOrder = $this->getItemsToOrder();
        foreach ($itemsToOrder as $item) {
            $model = Mage::getModel('opentechiz_reportExtend/automaticReordering');
            $model->setData($item);
            $model->setCreatedAt(Mage::getSingleton('core/date')->gmtDate());
            $model->save();
        }
    }

    public function getItemsToOrder()
    {
        $result = [];
        $currentYearSoldProducts = $this->getCurrentYearSoldProducts();
        if (count($currentYearSoldProducts)) {
            $oldProductIds = $this->getOldProducts(array_keys($currentYearSoldProducts));
            $lastYearSoldProductsBefore = $this->getLastYearSoldProductsBefore($oldProductIds);
            $lastYearSoldProductsAfter = $this->getLastYearSoldProductsAfter($oldProductIds);
            $instockProducts = $this->getInstockProducts($oldProductIds);
            $activeOrderIds = $this->getActiveOrderIds();
            $activeOrderProductsQty = $this->getOrderProductsQty($oldProductIds, $activeOrderIds);
            $soldProducts = [];
            foreach ($currentYearSoldProducts as $productId => $productQty) {
                if (!isset($lastYearSoldProductsBefore[$productId]) || $lastYearSoldProductsBefore[$productId] == 0) {
                    $lastYearSoldProductsBefore[$productId] = 1;
                }
                if (!isset($lastYearSoldProductsAfter[$productId]) || $lastYearSoldProductsAfter[$productId] == 0) {
                    $lastYearSoldProductsAfter[$productId] = 1;
                }
                $qty = $productQty / $lastYearSoldProductsBefore[$productId] * $lastYearSoldProductsAfter[$productId];
                if ($qty > 0) {
                    $qty = (fmod($qty, 1) >= 0.2 ? ceil($qty) : floor($qty));
                } else {
                    $qty = 0;
                }
                $qty -= ($instockProducts[$productId]['qty'] ?? 0) + ($instockProducts[$productId]['flat_qty_in_open_po'] ?? 0) - ($activeOrderProductsQty[$productId] ?? 0);
                if ($qty > 0) {
                    $soldProducts[$productId] = $qty;
                }
            }
            if (count($soldProducts)) {
                $soldItems = $this->getSoldItems(array_keys($soldProducts));
                $result = $this->getItems($soldProducts, $soldItems);
            }
        }
        return $result;
    }

    private function getCurrentYearSoldProducts()
    {
        $productIds = $this->getExcludedProductIds();
        $productIds = implode(',', $productIds);
        $sfo = Mage::getSingleton('core/resource')->getTableName('sales/order');
        $sfoi = Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $rr = Mage::getSingleton('core/resource')->getTableName('opentechiz_return/return');
        $date = date('Y-m-d', strtotime('-3 months'));
        $sql = "SELECT sfoi.product_id, SUM(sfoi.qty_ordered - sfoi.qty_canceled - IFNULL(qr.qty_returned, 0)) AS qty
                FROM {$sfoi} AS sfoi
                JOIN {$sfo} AS sfo ON sfo.entity_id = sfoi.order_id
                LEFT JOIN (SELECT sfoi.sku, COUNT(*) AS qty_returned
                    FROM {$rr} AS rr
                    JOIN {$sfoi} AS sfoi ON sfoi.item_id = rr.order_item_id
                    WHERE rr.created_at > '{$date}' AND rr.return_type IN (15, 25) AND rr.status = 10
                    GROUP BY sfoi.sku) AS qr ON qr.sku = sfoi.sku
                WHERE sfoi.created_at > '{$date}' AND sfo.order_stage = 5 AND sfo.state != 'canceled' AND sfo.order_type != 2 AND sfoi.product_id > 0 AND sfoi.product_id NOT IN ({$productIds})
                GROUP BY sfoi.product_id
                HAVING qty > 0
                ORDER BY sfoi.product_id ASC";
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        return $conn->fetchPairs($sql);
    }

    private function getLastYearSoldProductsBefore($productIds)
    {
        $productIds = implode(',', $productIds);
        $sfo = Mage::getSingleton('core/resource')->getTableName('sales/order');
        $sfoi = Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $rr = Mage::getSingleton('core/resource')->getTableName('opentechiz_return/return');
        $startDate = date('Y-m-d', strtotime('-1 year -3 months'));
        $endDate = date('Y-m-d', strtotime('-1 year'));
        $sql = "SELECT sfoi.product_id, SUM(sfoi.qty_ordered - sfoi.qty_canceled - IFNULL(qr.qty_returned, 0)) AS qty
                FROM {$sfoi} AS sfoi
                JOIN {$sfo} AS sfo ON sfo.entity_id = sfoi.order_id
                LEFT JOIN (SELECT sfoi.sku, COUNT(*) AS qty_returned
                    FROM {$rr} AS rr
                    JOIN {$sfoi} AS sfoi ON sfoi.item_id = rr.order_item_id
                    WHERE rr.created_at > '{$startDate}' AND rr.created_at <= '{$endDate}' AND rr.return_type IN (15, 25) AND rr.status = 10
                    GROUP BY sfoi.sku) AS qr ON qr.sku = sfoi.sku
                WHERE sfoi.created_at > '{$startDate}' AND sfoi.created_at <= '{$endDate}' AND sfo.order_stage = 5 AND sfo.state != 'canceled' AND sfo.order_type != 2 AND sfoi.product_id IN ({$productIds})
                GROUP BY sfoi.product_id
                ORDER BY sfoi.product_id ASC";
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        return $conn->fetchPairs($sql);
    }

    private function getLastYearSoldProductsAfter($productIds)
    {
        $productIds = implode(',', $productIds);
        $sfo = Mage::getSingleton('core/resource')->getTableName('sales/order');
        $sfoi = Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $rr = Mage::getSingleton('core/resource')->getTableName('opentechiz_return/return');
        $startDate = date('Y-m-d', strtotime('-1 year'));
        $endDate = date('Y-m-d', strtotime('-1 year +3 months'));
        $sql = "SELECT sfoi.product_id, SUM(sfoi.qty_ordered - sfoi.qty_canceled - IFNULL(qr.qty_returned, 0)) AS qty
                FROM {$sfoi} AS sfoi
                JOIN {$sfo} AS sfo ON sfo.entity_id = sfoi.order_id
                LEFT JOIN (SELECT sfoi.sku, COUNT(*) AS qty_returned
                    FROM {$rr} AS rr
                    JOIN {$sfoi} AS sfoi ON sfoi.item_id = rr.order_item_id
                    WHERE rr.created_at > '{$startDate}' AND rr.created_at <= '{$endDate}' AND rr.return_type IN (15, 25) AND rr.status = 10
                    GROUP BY sfoi.sku) AS qr ON qr.sku = sfoi.sku
                WHERE sfoi.created_at > '{$startDate}' AND sfoi.created_at <= '{$endDate}' AND sfo.order_stage = 5 AND sfo.state != 'canceled' AND sfo.order_type != 2 AND sfoi.product_id IN ({$productIds})
                GROUP BY sfoi.product_id
                ORDER BY sfoi.product_id ASC";
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        return $conn->fetchPairs($sql);
    }

    /**
     * @param $productIds
     * @return array
     */
    private function getOldProducts($productIds)
    {
        $productIds = implode(',', $productIds);
        $sfo = Mage::getSingleton('core/resource')->getTableName('sales/order');
        $sfoi = Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $months = $this->getMonthsNew();
        $date = date('Y-m-d', strtotime('-' . $months . ' months'));
        $sql = "SELECT sfoi.product_id
                FROM {$sfoi} AS sfoi
                JOIN {$sfo} AS sfo ON sfo.entity_id = sfoi.order_id
                WHERE sfoi.created_at <= '{$date}' AND sfo.order_stage = 5 AND sfo.state != 'canceled' AND sfo.order_type != 2 AND sfoi.product_id IN ({$productIds})
                GROUP BY sfoi.product_id
                ORDER BY sfoi.product_id ASC";
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        return $conn->fetchCol($sql);
    }

    private function getSoldGroupItems($productIds)
    {
        $result = [];
        $instock = Mage::getSingleton('core/resource')->getTableName('opentechiz_inventory/instock');
        $sfo = Mage::getSingleton('core/resource')->getTableName('sales/order');
        $sfoi = Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $rr = Mage::getSingleton('core/resource')->getTableName('opentechiz_return/return');
        $months = $this->getMonthsNew();
        $date = date('Y-m-d', strtotime('-' . $months . ' months'));
        foreach ($productIds as $productId) {
            $sql = "SELECT i.group_sku, SUM(sfoi.qty_ordered - sfoi.qty_canceled - IFNULL(qr.qty_returned, 0)) AS qty, MAX(sfoi.created_at) AS created_at
                FROM {$sfoi} AS sfoi
                JOIN {$instock} AS i ON i.sku = sfoi.sku
                JOIN {$sfo} AS sfo ON sfo.entity_id = sfoi.order_id
                LEFT JOIN (SELECT sfoi.sku, COUNT(*) AS qty_returned
                    FROM {$rr} AS rr
                    JOIN {$sfoi} AS sfoi ON sfoi.item_id = rr.order_item_id
                    WHERE rr.created_at > '{$date}' AND rr.return_type IN (15, 25) AND rr.status = 10
                    GROUP BY sfoi.sku) AS qr ON qr.sku = sfoi.sku
                WHERE sfoi.created_at > '{$date}' AND sfo.order_stage IN (1, 2, 5) AND sfo.state != 'canceled' AND sfo.order_type != 2 AND sfoi.product_id = {$productId}
                GROUP BY i.group_sku
                HAVING qty > 1
                ORDER BY qty DESC, created_at DESC";
            $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
            $soldItems = $conn->fetchPairs($sql);
            if (count($soldItems)) {
                $result[$productId] = $soldItems;
            }
        }
        return $result;
    }

    private function getSoldItems($productIds)
    {
        $result = [];
        $groupSkus = $this->getSoldGroupItems($productIds);
        $instock = Mage::getSingleton('core/resource')->getTableName('opentechiz_inventory/instock');
        $sfo = Mage::getSingleton('core/resource')->getTableName('sales/order');
        $sfoi = Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $rr = Mage::getSingleton('core/resource')->getTableName('opentechiz_return/return');
        $months = $this->getMonthsNew();
        $date = date('Y-m-d', strtotime('-' . $months . ' months'));
        foreach ($productIds as $productId) {
            if (!isset($groupSkus[$productId])) {
                continue;
            }
            $skus = implode("','", array_keys($groupSkus[$productId]));
            $sql = "SELECT sfoi.sku, sfoi.name, SUM(sfoi.qty_ordered - sfoi.qty_canceled - IFNULL(qr.qty_returned, 0)) AS qty, MAX(sfoi.created_at) AS created_at
                FROM {$sfoi} AS sfoi
                JOIN {$instock} AS i ON i.sku = sfoi.sku
                JOIN {$sfo} AS sfo ON sfo.entity_id = sfoi.order_id
                LEFT JOIN (SELECT sfoi.sku, COUNT(*) AS qty_returned
                    FROM {$rr} AS rr
                    JOIN {$sfoi} AS sfoi ON sfoi.item_id = rr.order_item_id
                    WHERE rr.created_at > '{$date}' AND rr.return_type IN (15, 25) AND rr.status = 10
                    GROUP BY sfoi.sku) AS qr ON qr.sku = sfoi.sku
                WHERE sfoi.created_at > '{$date}' AND sfo.order_stage IN (1, 2, 5) AND sfo.state != 'canceled' AND sfo.order_type != 2 AND i.group_sku IN ('{$skus}')
                GROUP BY sfoi.sku
                ORDER BY qty DESC, created_at DESC";
            $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
            $soldItems = $conn->fetchAssoc($sql);
            if (count($soldItems)) {
                $result[$productId] = $soldItems;
            }
        }
        return $result;
    }

    private function getItems($products, $items)
    {
        $result = [];
        $genderAttributeCode = $this->getGenderAttributeCode();
        $storeId = Mage::app()->getStore()->getId();
        $activeOrderIds = $this->getActiveOrderIds();
        foreach ($products as $productId => $productQty) {
            if (!isset($items[$productId])) {
                continue;
            }
            $productItems = $items[$productId];
            $skus = array_keys($productItems);
            $instockItems = $this->getInstockItems($skus);
            $activeOrderItemsQty = $this->getOrderItemsQty($skus, $activeOrderIds);
            $productSupplierId = $this->getProductSupplierId($productId);
            $sum = array_sum($productItems);
            $newProductQty = $productQty;
            $resultProductItems = [];
            foreach ($activeOrderItemsQty as $sku => $itemQty) {
                if ($newProductQty <= 0) {
                    break;
                }
                if (!isset($instockItems[$sku])) {
                    continue;
                }
                $qty = $itemQty - ($instockItems[$sku]['qty'] ?? 0) - ($instockItems[$sku]['flat_qty_in_open_po'] ?? 0);
                if ($qty > 0) {
                    $newProductQty -= $qty;
                    $item['inventory_instock_product_id'] = $instockItems[$sku]['id'];
                    $item['ai_order_qty'] = $qty;
                    $item['order_qty'] = $qty;
                    $item['ai_supplier_id'] = $productSupplierId ?? null;
                    $item['supplier_id'] = $productSupplierId ?? null;
                    $resultProductItems[$sku] = $item;
                }
            }
            foreach ($productItems as $sku => $productItem) {
                if ($newProductQty <= 0) {
                    break;
                }
                if (!isset($instockItems[$sku])) {
                    continue;
                }
                $qty = $sum ? ($productItem['qty'] * $productQty / $sum) : 0;
                if ($qty > 0) {
                    $qty = ceil($qty);
                } else {
                    $qty = 0;
                }
                if (isset($resultProductItems[$sku])) {
                    $qty -= $resultProductItems[$sku]['order_qty'];
                } else {
                    $qty -= ($instockItems[$sku]['qty'] ?? 0) + ($instockItems[$sku]['flat_qty_in_open_po'] ?? 0);
                }
                if ($qty > 0) {
                    $newProductQty -= $qty;
                    if ($newProductQty < 0) {
                        $qty += $newProductQty;
                    }
                    if (($qty == 1) && $genderAttributeCode && ((stripos($productItem['name'], ' ring') !== false) || (stripos($productItem['name'], 'ring') === 0))) {
                        if (!isset($this->products[$productId]['gender'])) {
                            $this->products[$productId]['gender'] = Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, $genderAttributeCode, $storeId);
                        }
                        $gender = $this->products[$productId]['gender'];
                        $defaultSize = '';
                        if ($gender) {
                            if ($gender == self::GENDER_MAN_VALUE) {
                                $defaultSize = $this->getDefaultManRingSize();
                            } elseif ($gender == self::GENDER_WOMAN_VALUE) {
                                $defaultSize = $this->getDefaultWamanRingSize();
                            } elseif ($gender == self::GENDER_UNISEX_VALUE) {
                                $defaultSize = $this->getDefaultUnisexRingSize();
                            }
                        }
                        if ($defaultSize) {
                            if (!isset($this->products[$productId]['sku'])) {
                                $this->products[$productId]['sku'] = Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, 'sku', $storeId);
                            }
                            $productSku = $this->products[$productId]['sku'];
                            $optionSkus = substr($sku, strlen($productSku));
                            if (strpos($optionSkus, '_') === 0) {
                                $optionSkus = ltrim($optionSkus, '_');
                                $optionSkus = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $optionSkus);
                                $ringSizePosition = $this->getRingSizePosition($productId);
                                if (isset($optionSkus[$ringSizePosition])) {
                                    $optionSkus[$ringSizePosition] = $defaultSize;
                                    $newSku = $productSku . OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $optionSkus);
                                    if ($this->getItemIdBySku($newSku)) {
                                        $sku = $newSku;
                                        if (isset($resultProductItems[$sku]['order_qty'])) {
                                            $qty += $resultProductItems[$sku]['order_qty'];
                                        } else {
                                            $instockItemId = $this->getItemIdBySku($sku);
                                            if ($instockItemId) {
                                                $instockItems[$sku]['id'] = $instockItemId;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $item['inventory_instock_product_id'] = $instockItems[$sku]['id'];
                    $item['ai_order_qty'] = $qty;
                    $item['order_qty'] = $qty;
                    $item['ai_supplier_id'] = $productSupplierId ?? null;
                    $item['supplier_id'] = $productSupplierId ?? null;
                    $resultProductItems[$sku] = $item;
                }
            }
            if (array_sum(array_column($resultProductItems, 'order_qty'))) {
                $result = array_merge($result, $resultProductItems);
            }
        }
        return $result;
    }

    public function getActiveOrderIds()
    {
        return Mage::getModel("sales/order")->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToFilter('order_stage', ['in' => [1, 2]])
            ->addAttributeToFilter('state', ['nin' => ['canceled']])
            ->addAttributeToFilter('order_type', ['nin' => [2]])
            ->getAllIds();
    }

    public function getInstockProducts($productIds)
    {
        $productIds = implode(",", $productIds);
        $table = Mage::getSingleton('core/resource')->getTableName('opentechiz_inventory/instock');
        $sql = "SELECT product_id, SUM(qty) AS qty, SUM(flat_qty_in_open_po) AS flat_qty_in_open_po
                FROM {$table}
                WHERE product_id IN ({$productIds})
                GROUP BY product_id";
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        return $conn->fetchAssoc($sql);
    }

    public function getInstockItems($skus)
    {
        $skus = implode("','", $skus);
        $table = Mage::getSingleton('core/resource')->getTableName('opentechiz_inventory/instock');
        $sql = "SELECT sku, id, qty, flat_qty_in_open_po
                FROM {$table}
                WHERE sku IN ('{$skus}')";
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        return $conn->fetchAssoc($sql);
    }

    public function getOrderProductsQty($productIds, $orderIds)
    {
        $productIds = implode(",", $productIds);
        $orderIds = implode(',', $orderIds);
        $table = Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $sql = "SELECT product_id, SUM(qty_ordered - qty_canceled) as qty
                FROM {$table}
                WHERE product_id IN ({$productIds}) AND order_id IN ({$orderIds})
                GROUP BY product_id";
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        return $conn->fetchPairs($sql);
    }

    public function getOrderItemsQty($skus, $orderIds)
    {
        $skus = implode("','", $skus);
        $orderIds = implode(',', $orderIds);
        $table = Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $sql = "SELECT sku, SUM(qty_ordered - qty_canceled) as qty
                FROM {$table}
                WHERE sku IN ('{$skus}') AND order_id IN ({$orderIds})
                GROUP BY sku";
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        return $conn->fetchPairs($sql);
    }

    private function getProductSupplierId($productId)
    {
        $result = '';
        $date = date('Y-m-d', strtotime('-1 year'));
        $po = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/purchase');
        $poi = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/orderitem');
        $sql = "SELECT po.supplier_id, po.created_at, poi.price
                FROM {$poi} poi
                JOIN {$po} po ON po.po_id = poi.po_id
                WHERE poi.delivered_qty > 0 AND poi.price > 0 AND po.created_at > '{$date}' AND poi.product_id = {$productId}";
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $items = $conn->fetchAll($sql);
        if (count($items)) {
            $prices = array_column($items, 'price');
            $minPrice = min($prices);
            $minPriceItems = [];
            foreach ($items as $item) {
                if ($item['price'] == $minPrice) {
                    $minPriceItems[] = $item;
                }
            }
            $dates = array_column($minPriceItems, 'created_at');
            $maxDate = max($dates);
            foreach ($minPriceItems as $item) {
                if ($item['created_at'] == $maxDate) {
                    $result = $item['supplier_id'];
                    break;
                }
            }
        } else {
            $sql = "SELECT po.supplier_id, po.created_at, poi.price
                    FROM {$poi} poi
                    JOIN {$po} po ON po.po_id = poi.po_id
                    WHERE poi.delivered_qty > 0 AND poi.price > 0 AND poi.product_id = {$productId}
                        AND po.created_at = (
                            SELECT MAX(po.created_at)
                            FROM {$poi} poi
                            JOIN {$po} po ON po.po_id = poi.po_id
                            WHERE poi.delivered_qty > 0 AND poi.price > 0 AND po.created_at <= '{$date}' AND poi.product_id = {$productId}
                        )";
            $items = $conn->fetchAll($sql);
            if (count($items)) {
                $prices = array_column($items, 'price');
                $minPrice = min($prices);
                foreach ($items as $item) {
                    if ($item['price'] == $minPrice) {
                        $result = $item['supplier_id'];
                        break;
                    }
                }
            }
        }
        return $result;
    }

    public function getProductSuppliers($productId)
    {
        $po = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/purchase');
        $poi = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/orderitem');
        $ps = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/supplier');
        $sql = "SELECT po.supplier_id, ps.name, poi.price AS last_cost, po.created_at AS last_cost_date
                	FROM {$poi} poi
                    JOIN {$po} po ON po.po_id = poi.po_id
                    JOIN {$ps} ps ON ps.supplier_id = po.supplier_id
                    WHERE poi.product_id = {$productId} AND po.created_at = (SELECT MAX(pob.created_at)
                        FROM {$poi} poib
                        JOIN {$po} pob ON pob.po_id = poib.po_id
                        WHERE poib.delivered_qty > 0 AND poib.price > 0 AND poib.sku = poi.sku AND pob.supplier_id = po.supplier_id)
                    GROUP BY po.supplier_id";
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        return $conn->fetchAssoc($sql);
    }

    public function getItemsByGroupSku($groupSku)
    {
        return Mage::getModel('opentechiz_reportExtend/automaticReordering')->getCollection()
            ->join(array('instock' => 'opentechiz_inventory/instock'), 'main_table.inventory_instock_product_id = instock.id', ['sku', 'group_sku'])
            ->addFieldToFilter('group_sku', $groupSku);
    }

    /**
     * @return array
     */
    private function getExcludedCategoryProductIds()
    {
        $result = [];
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $select = $read->select();
        $where = [];
        $excludeCategoryPaths = $this->getExcludedCategoryPaths();
        foreach ($excludeCategoryPaths as $categoryPath) {
            $where[] = "(cce.path = '" . $categoryPath . "' OR cce.path LIKE '" . $categoryPath ."/%')";
        }
        if ($where) {
            $select->from(['ccp' => $resource->getTableName('catalog/category_product')], 'product_id')
                ->joinLeft(
                    ['cce' => $resource->getTableName('catalog/category')],
                    'cce.entity_id = ccp.category_id',
                    []
                )
                ->where(implode(' OR ', $where))
                ->distinct();
            $result = $read->fetchCol($select);
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getExcludedCategoryPaths()
    {
        $excludeCategoryIds = explode(',', Mage::getStoreConfig(self::XML_SYSTEM_AUTOMATIC_REORDERING_EXCLUDE_CATEGORIES));
        if (!$excludeCategoryIds) {
            return [];
        }
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $select = $read->select();
        $select->from($resource->getTableName('catalog/category'), 'path')
            ->where('entity_id IN (?)', $excludeCategoryIds);
        return $read->fetchCol($select);
    }

    /**
     * @return array
     */
    public function getExcludedProductSkus()
    {
        $productSkus = preg_split('/\r\n|\r|\n/', Mage::getStoreConfig(self::XML_SYSTEM_AUTOMATIC_REORDERING_EXCLUDE_PRODUCTS));
        $productSkus = array_map('trim', $productSkus);
        return $productSkus ?? [];
    }

    /**
     * @return array
     */
    public function getExcludedSupplierIds()
    {
        $excludeSupplierIds = Mage::getStoreConfig(self::XML_SYSTEM_AUTOMATIC_REORDERING_EXCLUDE_SUPPLIERS);
        if (!$excludeSupplierIds) {
            return [];
        }
        $po = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/purchase');
        $poi = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/orderitem');
        $sql = "SELECT s.product_id, s.supplier_id
                FROM (SELECT DISTINCT poi.product_id, po.supplier_id
                    FROM {$poi} AS poi
                    JOIN {$po} AS po ON po.po_id = poi.po_id
                    WHERE poi.delivered_qty > 0 AND poi.price > 0) AS s
                GROUP BY s.product_id
                HAVING COUNT(*) = 1 AND s.supplier_id IN ({$excludeSupplierIds})";
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        return $conn->fetchCol($sql);
    }

    /**
     * @return array
     */
    public function getExcludedProductIds()
    {
        $result = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('sku', ['in' => [$this->getExcludedProductSkus()]])
            ->getAllIds();
        $result = array_merge($result, $this->getExcludedCategoryProductIds());
        $result = array_merge($result, $this->getExcludedSupplierIds());
        return array_unique($result);
    }

    /**
     * @return int
     */
    public function getMonthsNew()
    {
        return (int) trim(Mage::getStoreConfig(self::XML_SYSTEM_AUTOMATIC_REORDERING_MONTHS_NEW));
    }

    /**
     * @return int
     */
    public function getGenderAttributeCode()
    {
        return Mage::getStoreConfig(self::XML_SYSTEM_AUTOMATIC_REORDERING_RINGS_GENDER_ATTRIBUTE);
    }

    /**
     * @return string
     */
    public function getDefaultManRingSize()
    {
        return trim(Mage::getStoreConfig(self::XML_SYSTEM_AUTOMATIC_REORDERING_RINGS_MAN_SIZE));
    }

    /**
     * @return string
     */
    public function getDefaultWamanRingSize()
    {
        return trim(Mage::getStoreConfig(self::XML_SYSTEM_AUTOMATIC_REORDERING_RINGS_WOMAN_SIZE));
    }

    /**
     * @return string
     */
    private function getDefaultUnisexRingSize()
    {
        return trim(Mage::getStoreConfig(self::XML_SYSTEM_AUTOMATIC_REORDERING_RINGS_UNISEX_SIZE));
    }

    public function sendNotyEmail()
    {
        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/general/automatic_reordering'));

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('automatic_reordering_report_tpl');
        $emailTemplate->setTemplateSubject('Automatic Reordering Report ('.Mage::helper('core')->formatDate().')');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();

        foreach ($to as $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send($recipientEmail, $emailTemplateVariables);
            }
        }
    }

    public function getEmailData()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $aiCollection = Mage::getModel('opentechiz_reportExtend/automaticReordering')->getCollection()
            ->addFieldToFilter('order_qty', ['gt' => 0])
            ->join(array('instock' => 'opentechiz_inventory/instock'), 'main_table.inventory_instock_product_id = instock.id')
            ->setOrder('sku', Varien_Data_Collection::SORT_ORDER_ASC);

        $emailData = [];
        foreach ($aiCollection as $aiItem) {
            $sku = $aiItem->getSku();
            $baseSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
            $group_sku = $aiItem->getGroupSku();

            $data = [];
            //get sku from group_sku
            if ($group_sku) {
                $sku = $group_sku;
            }
            $data['sku'] = $sku; //sku

            $data['qty'] = $aiItem->getQty(); //qty

            //get reorder_point
            $query = "SELECT group_sku, MAX(reorder_point) max_reorder_point
                 FROM tsht_inventory_instock_product
                 where group_sku = '".$group_sku."'";
            $reorderPoint = $readConnection->fetchAll($query)[0]['max_reorder_point'];
            $data['reorder_point'] = $reorderPoint;

            //get cost
            $helper = Mage::helper('opentechiz_inventory');
            if (strtotime($helper->getLastCostDate($sku))) {
                $data['cost_date'] = Mage::getModel('core/date')->date('M d, Y', $helper->getLastCostDate($sku));
            } else {
                $data['cost_date'] = 'N/A';
            }
            $data['cost'] = '$'.number_format($helper->getLastCost($sku), 2, '.', '');

            //get 3m/ret, 12m/ret
            $sku = $baseSku;
            if ($group_sku) {
                $sku = $group_sku;
            }
            $sku = $sku.'%';
            $data['12m'] = Mage::helper('opentechiz_reportExtend')->getNumberOfSoldPerReturn($sku, 12);
            $data['3m'] = Mage::helper('opentechiz_reportExtend')->getNumberOfSoldPerReturn($sku, 3);

            $itemCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', array(
                array('like' => $baseSku),
                array('like' => $baseSku.'\_%')
            ));
            $stringOther = '';

            $skus = [];
            foreach ($itemCollection as $instockItem) {
                if ($instockItem->getGroupSku()) {
                    $sku = $instockItem->getGroupSku();
                } else {
                    $sku = $instockItem->getSku();
                }
                if (!array_key_exists($sku, $skus)) {
                    $skus[$sku] = $instockItem->getQty();
                } else {
                    $skus[$sku] += $instockItem->getQty();
                }
            }

            foreach ($skus as $sku => $qty) {
                if ($sku == '') continue;
                $stringOther .= $sku . ' x ' . $qty . '<br>';
            }

            //get manufacturing qty
            $sku = $baseSku;
            //get sku from group_sku
            if ($group_sku) {
                $sku = $group_sku;
            }
            $data['manufacture'] = Mage::helper('opentechiz_reportExtend')->getCurrentManufacturingQty($sku);
            $data['other'] = $stringOther;

            $sku = $baseSku;
            $_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
            if (!$_product || !$_product->getId()) {
                continue;
            }
            $imageUrl = Mage::helper('catalog/image')->init($_product, 'thumbnail')->resize(120, 120)->__toString();
            $item = Mage::getModel('sales/order_item')->getCollection()
                ->addFieldToSelect('price')
                ->addFieldToFilter('sku', $aiItem->getSku())
                ->addFieldToFilter('price', ['gt' => 0])
                ->setOrder('created_at')
                ->getFirstItem();
            $data['price'] = '$'.number_format($item->getPrice(), 2, '.', ''); //sales price
            $data['image'] = $imageUrl;

            //check clearance sales
            $flag = false;
            $productCats = $_product->getCategoryIds();
            foreach ($productCats as $productCat) {
                $_cat = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getId())->load($productCat);
                if ($_cat->getName() == 'Clearance Sale') {
                    $flag = true;
                }
            }

            if ($flag == true) $data['close_out'] = 'YES';
            else $data['close_out'] = 'NO';

            // ai data
            $supplierId = $aiItem->getSupplierId();
            $supplier = '';
            if ($supplierId) {
                $suppliers = Mage::helper('opentechiz_reportExtend/automaticReordering')->getProductSuppliers($_product->getId());
                $supplier = (isset($suppliers[$supplierId]) ? $suppliers[$supplierId]['name'] . ' | '
                        . Mage::getModel('directory/currency')->format(
                            $suppliers[$supplierId]['last_cost'],
                            array('display'=>Zend_Currency::NO_SYMBOL),
                            false
                        ) . ' | '
                        . Mage::helper('core')->formatDate($suppliers[$supplierId]['last_cost_date']) : '');
            }
            $data['order_qty'] = $aiItem->getOrderQty();
            $data['supplier'] = $supplier;

            array_push($emailData, $data);
        }

        return $emailData;
    }

    public function getNumberOfSoldPerReturn($sku, $months)
    {
        $months = '-'.$months.' months';
        $lastXMonth = date('Y-m-d', strtotime($months));

        $sfo = Mage::getSingleton('core/resource')->getTableName('sales/order');
        $sfoi = Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $sql = "SELECT SUM(sfoi.qty_ordered - sfoi.qty_canceled)
                FROM {$sfoi} AS sfoi
                JOIN {$sfo} AS sfo ON sfo.entity_id = sfoi.order_id
                WHERE sfoi.created_at > '{$lastXMonth}' AND sfo.order_stage = 5 AND sfo.state != 'canceled' AND sfo.order_type != 2 AND sfoi.sku LIKE '{$sku}'";
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sold = (int)$conn->fetchOne($sql);

        $returnCollection = Mage::getModel('opentechiz_return/return')->getCollection()
            ->addFieldToFilter('main_table.created_at', array('gt' => $lastXMonth))
            ->addFieldToFilter('main_table.return_type', array('in' => array(15, 25)))
            ->addFieldToFilter('main_table.status', 10)
            ->join(array('item' => 'sales/order_item'), 'item.item_id = main_table.order_item_id')
            ->addFieldToFilter('sku', array('like' => $sku));
        $returned = count($returnCollection);

        return $sold.'/'.$returned;
    }

    /**
     * @return int|null
     */
    public function getItemIdBySku($sku)
    {
        return Mage::getModel('opentechiz_inventory/instock')->load($sku, 'sku')->getId();
    }

    /**
     * @param int $productId
     * @return int
     */
    protected function getRingSizePosition($productId)
    {
        if (!isset($this->products[$productId]['ring_size_position'])) {
            /** @var Mage_Catalog_Model_Resource_Product_Option_Collection $optionsCollection */
            $optionsCollection = Mage::getModel('catalog/product_option')->getCollection()
                ->addFieldToFilter('product_id', $productId)
                ->addTitleToResult(0)
                ->setOrder('sort_order', 'asc')
                ->setOrder('title', 'asc');

            $optionsCollection->addValuesToResult();
            $this->products[$productId]['ring_size_position'] = -1;
            $position = -1;
            foreach ($optionsCollection as $productOption) {
                $position++;
                $productOptionTitle = strtolower($productOption->getTitle());
                if (strpos($productOptionTitle, 'ring size') !== false) {
                    $this->products[$productId]['ring_size_position'] = $position;
                    break;
                }
            }
        }

        return $this->products[$productId]['ring_size_position'];
    }
}
