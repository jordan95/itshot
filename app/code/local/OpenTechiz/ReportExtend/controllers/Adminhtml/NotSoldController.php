<?php

class OpenTechiz_ReportExtend_Adminhtml_NotSoldController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Report'))->_title($this->__('Items Not Sold'));
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/not_sold');
        $block = $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_notSoldGrid');
        $block->setTemplate('opentechiz/report/not_sold.phtml');
        $this->getLayout()->getBlock('content')->append($block);
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function AjaxAction(){
        $params = $this->getRequest()->getParams();
        $return = Mage::getModel('opentechiz_reportExtend/notSold')->getNotSoldData($params);

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($return));
    }

    public function SaveProductDataAction(){
        $params = $this->getRequest()->getParams();

        $categories = [];
        foreach ($params as $key => $value){
            $keyParts = explode('_', $key);
            $productId = $keyParts[count($keyParts)-1];

            $product = Mage::getModel('catalog/product')->load($productId);
            if(!$product || !$product->getId()){
                continue;
            }

            //get categories
            if($keyParts[0] == 'category'){
                array_push($categories, $value);
            }

            //save closed out
            if(stripos($key, 'closeout') !== false) {
                $categories = $product->getCategoryIds();
                $category = Mage::getResourceModel('catalog/category_collection')
                    ->addFieldToFilter('name', 'Clearance Sale')
                    ->getFirstItem(); // The parent category
                $categoryId = $category->getId();
                if($value == 'on') {
                    array_push($categories, $categoryId);
                }else{
                    foreach ($categories as $catKey => $catValue){
                        if($catValue == $categoryId){
                            unset($categories[$catKey]);
                        }
                    }
                }
                $product->setCategoryIds($categories)->save();
            }
            //save special price
            elseif(stripos($key, 'special_price') !== false) {
                $product->setSpecialPrice($value)->save();
            }
        }

        //save categories
        if(count($categories) > 0 && isset($product) && $product->getId()) {
            $product->setCategoryIds($categories);
            $product->save();
        }
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/denis/not_sold');
    }
}