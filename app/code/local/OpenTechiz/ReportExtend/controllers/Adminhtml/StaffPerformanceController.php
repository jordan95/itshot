<?php

class OpenTechiz_ReportExtend_Adminhtml_StaffPerformanceController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Report'))->_title($this->__('Staff Performance'));
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/staff');
        $block = $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_staffPerformanceGrid');
        $block->setTemplate('opentechiz/report/staff_performance.phtml');
        $this->getLayout()->getBlock('content')->append($block);
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function AjaxAction(){
        $params = $this->getRequest()->getParams();
        $return = Mage::getModel('opentechiz_reportExtend/staffPerformance')->getStaffData($params);

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($return));
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/staff');
    }
}