<?php

class OpenTechiz_ReportExtend_Adminhtml_ProgressController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $params = $this->getRequest()->getParams();
        if(!isset($params['stage']) || !isset($params['status']) || !Mage::helper('opentechiz_reportExtend')->isParamsAllow($params['stage'], $params['status'])){
            $this->norouteAction();
            return $this;
        }
        $reportName = Mage::helper('opentechiz_reportExtend')->getReportName($params['stage'], $params['status']);
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/progress')
            ->_title($this->__('Order Progress Report').' - '.$reportName);
        $this->_addContent($this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_progress'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_progress_grid')->toHtml()
        );
    }

    protected function _isAllowed()
    {
        $params = $this->getRequest()->getParams();
        if(isset($params['status']) && $params['status'] =="waitingOnVendor"){
            return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/progress/wait_vendor');
        }
        if(isset($params['status']) && $params['status'] =="itemManufacturing"){
            return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/progress/manufacturing');
        }
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/progress');
    }
}