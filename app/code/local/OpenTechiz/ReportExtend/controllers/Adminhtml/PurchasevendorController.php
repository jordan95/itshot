<?php

class OpenTechiz_ReportExtend_Adminhtml_PurchasevendorController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport')
            ->_title($this->__('PO undelivery Report'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_purchasevendor'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_purchasevendor_grid')->toHtml()
        );
    }
    public function exportCsvAction()
    {
        $fileName   = ' PO_Vendor.csv';
        $content    = $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_purchasevendor_grid')
        ->getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }
    public function exportExcelAction()
    {
        $fileName   = 'PO_Vendor.xml';
        $grid       = $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_purchasevendor_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/purchasevendor');
    }
}