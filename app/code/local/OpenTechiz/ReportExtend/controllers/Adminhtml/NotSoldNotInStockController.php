<?php

class OpenTechiz_ReportExtend_Adminhtml_NotSoldNotInStockController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Report'))->_title($this->__('Items Not Sold not In Stock'));
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/not_sold');
        $block = $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_notSoldGrid');
        $block->setTemplate('opentechiz/report/not_sold_not_in_stock.phtml');
        $this->getLayout()->getBlock('content')->append($block);
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function AjaxAction(){
        $params = $this->getRequest()->getParams();
        $return = Mage::getModel('opentechiz_reportExtend/notSold')->getNotSoldNotInStockData($params);

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($return));
    }

    public function DisableProductAction(){
        $params = $this->getRequest()->getParams();
        //disable products
        $productDisabled = array();
        foreach ($params as $productId => $value){
            if($value == 1){
                $product = Mage::getModel('catalog/product')->load($productId);
                $product->setStatus(2);
                $product->save();

                array_push($productDisabled, $product->getSku());
            }
        }

        //refresh grid
//        $return = Mage::getModel('opentechiz_reportExtend/notSold')->getNotSoldNotInStockData($params);

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($productDisabled));
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/denis/not_sold_not_in_stock');
    }
}