<?php

class OpenTechiz_ReportExtend_Adminhtml_DailyReturnController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/daily_return')
            ->_title($this->__('Daily Return'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_dailyReturn'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_dailyReturn_grid')->toHtml()
        );
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/daily_return');
    }
}