<?php

class OpenTechiz_ReportExtend_Adminhtml_DailySoldController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/daily_sold')
            ->_title($this->__('Daily Sold'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_dailySold'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_dailySold_grid')->toHtml()
        );
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/daily_sold');
    }
}