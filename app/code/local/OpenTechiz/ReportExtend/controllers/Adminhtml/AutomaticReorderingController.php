<?php

class OpenTechiz_ReportExtend_Adminhtml_AutomaticReorderingController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/automatic_reordering')
            ->_title($this->__('Automatic Reordering'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_automaticReordering'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_automaticReordering_grid')->toHtml()
        );
    }

    public function changeStatusAction()
    {
        if ($this->getRequest()->isAjax()) {
            $groupSku = $this->getRequest()->getParam('group_sku');
            $collection = Mage::getModel('opentechiz_reportExtend/automaticReordering')->getCollection()
                ->join(array('instock' => 'opentechiz_inventory/instock'), 'main_table.inventory_instock_product_id = instock.id')
                ->addFieldToFilter('instock.group_sku', $groupSku);
            $statuses = array(
                0 => Mage::helper('adminhtml')->__('Removed'),
                1 => Mage::helper('adminhtml')->__('Added')
            );
            $linkTexts = array(
                0 => Mage::helper('adminhtml')->__('Add'),
                1 => Mage::helper('adminhtml')->__('Remove')
            );
            $result['success'] = false;
            $result['groupSku'] = $groupSku;
            foreach ($collection as $item) {
                $isActive = $item->getIsActive() ? 0 : 1;
                $item->setIsActive($isActive);
                $item->save();
                $result['statusText'] = $statuses[$item->getIsActive()];
                $result['linkText'] = $linkTexts[$item->getIsActive()];
                $result['success'] = true;
            }
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } else {
            $this->_redirect('*/*/index');
        }
    }

    public function saveQtyAction()
    {
        if ($this->getRequest()->isAjax()) {
            $id = $this->getRequest()->getParam('id');
            $orderQty = $this->getRequest()->getParam('order_qty');
            $model = Mage::getModel('opentechiz_reportExtend/automaticReordering')->load($id);
            $result['success'] = false;
            $result['id'] = $id;
            $result['orderQty'] = $orderQty;
            if ($model->getId()) {
                $model->setOrderQty($orderQty);
                $model->save();
                $result['success'] = true;
            }
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } else {
            $this->_redirect('*/*/index');
        }
    }

    public function saveSupplierAction()
    {
        if ($this->getRequest()->isAjax()) {
            $groupSku = $this->getRequest()->getParam('group_sku');
            $supplierId = $this->getRequest()->getParam('supplier_id');
            $collection = Mage::getModel('opentechiz_reportExtend/automaticReordering')->getCollection()
                ->join(array('instock' => 'opentechiz_inventory/instock'), 'main_table.inventory_instock_product_id = instock.id')
                ->addFieldToFilter('instock.group_sku', $groupSku);
            $result['success'] = false;
            $result['supplierId'] = $supplierId;
            $result['groupSku'] = $groupSku;
            foreach ($collection as $item) {
                $item->setSupplierId($supplierId);
                $item->save();
                $result['success'] = true;
            }
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } else {
            $this->_redirect('*/*/index');
        }
    }

    public function emailAction()
    {
        if ($this->getRequest()->isAjax()) {
            try {
                $supplierIds = Mage::getModel('opentechiz_reportExtend/automaticReordering')->getCollection()
                    ->addFieldToSelect('supplier_id')
                    ->addFieldToFilter('is_active', 1)
                    ->addFieldToFilter('supplier_id', ['notnull' => true])
                    ->addFieldToFilter('purchased_at', ['null' => true])
                    ->addFieldToFilter('order_qty', ['gt' => 0])
                    ->distinct(true)
                    ->getColumnValues('supplier_id');
                $itemCollection = Mage::getModel('opentechiz_reportExtend/automaticReordering')->getCollection()
                    ->addFieldToSelect(['qty' => 'order_qty', 'supplier_id'])
                    ->addFieldToFilter('is_active', 1)
                    ->addFieldToFilter('supplier_id', ['notnull' => true])
                    ->addFieldToFilter('purchased_at', ['null' => true])
                    ->addFieldToFilter('order_qty', ['gt' => 0])
                    ->join(array('instock' => 'opentechiz_inventory/instock'), 'main_table.inventory_instock_product_id = instock.id', ['product_id', 'sku']);
                $result['content'] = '<table><thead><tr><th>'
                    . Mage::helper('opentechiz_reportExtend')->__('Supplier') . '</th><th>'
                    . Mage::helper('opentechiz_reportExtend')->__('Qty') . '</th></thead><tbody>';

                foreach ($supplierIds as $supplierId) {
                    $data = [];
                    $data['source'] = OpenTechiz_Purchase_Helper_Data::PO_SOURCE_AUTO;
                    $data['supplier_id'] = $supplierId;
                    $supplierName = Mage::getModel('opentechiz_purchase/supplier')->load($supplierId)->getName();
                    $items = $itemCollection->getItemsByColumnValue('supplier_id', $supplierId);
                    if (count($items)) {
                        $purchase = Mage::getModel('opentechiz_purchase/purchase');
                        $is_new = 0;
                        $action = 'edit';
                        if (!$purchase->getId()) {
                            $is_new = 1;
                            $action = 'create';
                        }
                        $newTotal = 0;
                        $qtyTotal = 0;
                        foreach ($items as $item) {
                            $itemSuppliers = Mage::helper('opentechiz_reportExtend/automaticReordering')->getProductSuppliers($item->getProductId());
                            $item->setPrice($itemSuppliers[$supplierId]['last_cost']);
                            $newTotal += (float)$item->getPrice() * $item->getQty();
                            $qtyTotal += $item->getQty();
                        }

                        $data['total_include_tax'] = $newTotal;
                        $data['type'] = OpenTechiz_Purchase_Helper_Data::PO_TYPE_PRODUCT;

                        $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
                        $currentTime = strtotime($date);
                        $secondsToAdd = 3 * 7 * 24 * (60 * 60); //3 weeks
                        $newTime = $currentTime + $secondsToAdd;
                        $data['delivery_date'] = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $newTime);

                        if (!$purchase->getPoIncrementId())
                            $data['po_increment_id'] = $purchase->generateOrderNumber();

                        $purchase->addData($data);

                        if ($is_new) {
                            $created_at = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s");
                            $purchase->setCreatedAt($created_at);
                        }
                        if (isset($data['delivery_date'])) {
                            $purchase->setDeliveryDate($data['delivery_date']);
                        }
                        $purchase->setUpdatedAt(now());
                        $purchase->save();
                        foreach ($items as $item) {
                            $purchase_item = Mage::getModel('opentechiz_purchase/orderitem');
                            $id = $purchase_item->getCollection()->addFieldToFilter('sku', $item->getSku())->addFieldToFilter('po_id', $purchase->getId())->getFirstItem()->getId();
                            $purchase_item->load($id);

                            $product = Mage::getModel('catalog/product')->load($item->getProductId());
                            if (!$item->getOption() && $product->getId()) {
                                $productSku = $product->getSku();
                                $skus = [];
                                if ($item->getSku() != $productSku) {
                                    $optionSkus = substr($item->getSku(), strlen($productSku));
                                    if (strpos($optionSkus, '_') == 0) {
                                        $optionSkus = ltrim($optionSkus, '_');
                                        $skus = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $optionSkus);
                                    }
                                }

                                if (count($skus)) {
                                    $serializeOption = [];
                                    $serializeOption['info_buyRequest'] = [];
                                    $serializeOption['info_buyRequest']['product'] = $item->getProductId();
                                    $serializeOption['options'] = [];

                                    $optionsCollection = $product->getOptions();
                                    // if option enabled = no && hasOptions = 0
                                    if (!$optionsCollection) {
                                        $optionsCollection = $product->getProductOptionsCollection();
                                    }

                                    foreach ($optionsCollection as $option) {
                                        foreach ($option->getValues() as $value) {
                                            if (in_array($value->getSku(), $skus) ) {
                                                $serializedData = [];
                                                $serializedData['label'] = $option->getTitle();
                                                $serializedData['value'] = $value->getTitle();
                                                $serializedData['option_id'] = $option->getId();
                                                $serializedData['option_value'] = $value->getId();
                                                $serializedData['option_type'] = $option->getType();
                                                array_push($serializeOption['options'], $serializedData);
                                            }
                                        }
                                    }
                                    $item->setOption(serialize($serializeOption));
                                }
                            }

                            $priceChange = false;
                            if (round($purchase_item->getPrice(), 4) != round($item->getPrice(), 4)) {
                                $priceChange = true;
                            }

                            $item->setPoId($purchase->getId());
                            $purchase_item->addData($item->getData());

                            $purchase_item->save();
                            $item->setPurchasedAt(Mage::getSingleton('core/date')->gmtDate());
                            $item->save();

                            //change last cost and item cost if have permission
                            $allowEditPrice = Mage::getSingleton('admin/session')->isAllowed('admin/erp/purchase/product/edit_product_cost');
                            if ($allowEditPrice && $priceChange) {
                                $resource = Mage::getSingleton('core/resource');
                                $writeConnection = $resource->getConnection('core_write');

                                $cost = (float)$item->getPrice();
                                $po_id = $purchase->getId();
                                $query = "UPDATE `tsht_purchase_purchase_order_item` 
                                  join tsht_inventory_item on tsht_purchase_purchase_order_item.item_id = tsht_inventory_item.item_id 
                                  set tsht_inventory_item.cost = " . $cost . "
                                  where po_id = " . $po_id . " AND tsht_inventory_item.sku = '" . $item->getSku() . "'";
                                $writeConnection->query($query);

                                if ($purchase_item->getDeliveredQty() > 0) {
                                    $currentTime = Mage::getModel('core/date')->gmtDate('Y-m-d');
                                    $query = "UPDATE `tsht_inventory_instock_product` 
                                      set last_cost = " . $cost . ", last_cost_date = '" . $currentTime . "', last_po_id =" . $po_id . "
                                      where sku = '" . $item->getSku() . "'";
                                    $writeConnection->query($query);
                                    //update last cost,last cost date to tsht_purchase_product_supplier
                                    $product_supplier = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()->addFieldToFilter('product_id', $purchase_item->getProductId())
                                        ->addFieldToFilter('sup_id', $purchase->getSupplierId())->getFirstItem();
                                    $data['last_cost'] = $cost;
                                    $data['last_cost_date'] = $purchase->getUpdatedAt();
                                    $product_supplier->addData($data)->save();
                                }
                            }
                        }

                        $data = [];
                        $data['is_supplier_notified'] = OpenTechiz_Purchase_Helper_Data::SUP_NOTIFIED;
                        $data['is_pending_po'] = OpenTechiz_Purchase_Helper_Data::PO_EMAIL_PROCESSED;
                        $data['notified_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                        $purchase->addData($data)->save();
                        if ($purchase->getStatus() != OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL) {
                            Mage::helper('opentechiz_purchase')->sendMail($purchase, $action);
                            $dataLog = array();
                            $dataLog['log_from'] = 'app/code/local/OpenTechiz/ReportExtend/controllers/Adminhtml/AutomaticReorderingController.php';
                            $dataLog['po_id'] = $purchase->getId();
                            Mage::log($dataLog, null, 'cron_master_po.log', true);
                        }

                        $purchase->validAndUpdateStatus();
                        $admin = Mage::getSingleton('admin/session')->getUser();
                        if ($is_new && $admin->getId()) {
                            Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Add new purchase product #' . $purchase->getOrderIcrementId()));
                        }
                        $result['content'] .= '<tr><td>' . $supplierName . '</td><td>' . $qtyTotal . '</td></tr>';
                    }
                }
                $result['success'] = true;
                $result['content'] .= '</tbody></table>';
            } catch (Exception $e) {
                $result['success'] = false;
                $result['content'] = '<div>' . $e->getMessage() . '</div>';
                Mage::logException($e);
            }

            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } else {
            $this->_redirect('*/*/index');
        }
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/automatic_reordering');
    }
}