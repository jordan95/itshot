<?php

class OpenTechiz_ReportExtend_Adminhtml_DeclineReturnController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/decline_return')
            ->_title($this->__('Decline Return'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_declineReturn'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_declineReturn_grid')->toHtml()
        );
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/decline_return');
    }
}