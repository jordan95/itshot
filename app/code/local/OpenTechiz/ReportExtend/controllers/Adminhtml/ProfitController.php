<?php

class OpenTechiz_ReportExtend_Adminhtml_ProfitController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Profit'))->_title($this->__('Profit'));
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/profit');
        $block = $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_profitGrid');
        $block->setTemplate('opentechiz/report/profit.phtml');
        $this->getLayout()->getBlock('content')->append($block);
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function AjaxAction(){
        $params = $this->getRequest()->getParams();
        $return = Mage::getModel('opentechiz_reportExtend/profit')->getProfitData($params);

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($return));
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/profit');
    }
}