<?php

class OpenTechiz_ReportExtend_Adminhtml_ToOrderController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Report'))->_title($this->__('Items To Order'));
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/to_order');
        $block = $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_toOrderGrid');
        $block->setTemplate('opentechiz/report/to_order.phtml');
        $this->getLayout()->getBlock('content')->append($block);
        $this->getLayout()->getBlock('head')->addJs('mage/adminhtml/product/composite/configure.js');
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function AjaxAction(){
        $params = $this->getRequest()->getParams();
        $return = Mage::getModel('opentechiz_reportExtend/toOrder')->getToOrderData($params);

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($return));
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/denis/to_order');
    }
}