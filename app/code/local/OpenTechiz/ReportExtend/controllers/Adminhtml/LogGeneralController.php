<?php

class OpenTechiz_ReportExtend_Adminhtml_LogGeneralController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/log/general')
            ->_title($this->__('General Log'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_logGeneral'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_logGeneral_grid')->toHtml()
        );
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/log/general');
    }
}