<?php

class OpenTechiz_ReportExtend_Adminhtml_AdampierRMAController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/adampier_rma')
            ->_title($this->__('Adampier Customer RMA'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_adampierRMA'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_adampierRMA_grid')->toHtml()
        );
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/sales/adampier_rma');
    }
}