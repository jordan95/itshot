<?php

class OpenTechiz_ReportExtend_Adminhtml_ReturnRateController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Report'))->_title($this->__('Return Rate'));
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/return_rate');
        $block = $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_returnRateGrid');
        $block->setTemplate('opentechiz/report/return_rate.phtml');
        $this->getLayout()->getBlock('content')->append($block);
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function AjaxAction(){
        $params = $this->getRequest()->getParams();
        $return = Mage::getModel('opentechiz_reportExtend/returnRate')->getReturnRateData($params);

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($return));
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/return_rate');
    }
}