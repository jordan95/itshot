<?php

class OpenTechiz_ReportExtend_Adminhtml_LogOrderStatusController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('report/salesroot/order_report')
            ->_title($this->__('Order Status History'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_logOrderStatus'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_logOrderStatus_grid')->toHtml()
        );
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/report/salesroot/order_report');
    }
}