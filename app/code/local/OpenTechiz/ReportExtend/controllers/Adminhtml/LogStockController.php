<?php

class OpenTechiz_ReportExtend_Adminhtml_LogStockController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/log/stock')
            ->_title($this->__('Stock Movement Log'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_logStock'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_logStock_grid')->toHtml()
        );
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/log/stock_movement');
    }
}