<?php

class OpenTechiz_ReportExtend_Adminhtml_ProductHistoryController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp/advreport/product_history')
            ->_title($this->__('Product History Report'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_productHistory'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function refreshAction() {
        Mage::getModel('opentechiz_reportExtend/cron')->mergePOItemOItem();
        $referer = $this->getRequest()->getParam('referer', false);
        $url = $this->getUrl("*/*/index");
        if($referer){
            $url = base64_decode($referer);
        }
        $this->_redirectUrl($url);
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_reportExtend/adminhtml_productHistory_grid')->toHtml()
        );
    }
    public function AjaxAction(){
        $params = $this->getRequest()->getParams();
        $return = Mage::getModel('opentechiz_reportExtend/productHistory')->getProductHistoryData($params);

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($return));
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/advreport/productHistory');
    }
}