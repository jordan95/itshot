<?php
class OpenTechiz_ReportExtend_Model_System_Config_Source_Supplier
{
    public function toOptionArray()
    {
        $supplier =array();
        $i = 0;
        $collection = Mage::getModel('opentechiz_purchase/supplier')
            ->getCollection()
            ->setOrder('name', 'asc');
        foreach ($collection as $key=>$item)
        {
            $supplier[$i]['value'] = $item->getId();
            $supplier[$i]['label'] = $item->getName();
            $i++;
        }
        return $supplier;
    }

}