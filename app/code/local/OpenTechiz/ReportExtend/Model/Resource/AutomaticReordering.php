<?php

class OpenTechiz_ReportExtend_Model_Resource_AutomaticReordering extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Primery key auto increment flag
     *
     * @var bool
     */
    protected $_isPkAutoIncrement = false;

    protected function _construct()
    {
        $this->_init('opentechiz_reportExtend/automatic_reordering', 'inventory_instock_product_id');
    }

    /**
     * Set date of last update
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Core_Model_Resource_Db_Abstract
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        $object->setUpdatedAt(Mage::getSingleton('core/date')->gmtDate());
        return parent::_beforeSave($object);
    }
}