<?php

class OpenTechiz_ReportExtend_Model_Resource_OldOrder extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_reportExtend/oldOrder', 'old_order_id');
    }
}