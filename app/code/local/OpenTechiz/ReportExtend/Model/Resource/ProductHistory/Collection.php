<?php

class OpenTechiz_ReportExtend_Model_Resource_ProductHistory_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('opentechiz_reportExtend/productHistory');
    }
//    protected function _construct()
//    {
//        $this->_init('opentechiz_purchase/purchase');
//        $this->setMainTable('tsht_purchase_order');
//    }
//
//    protected function _initSelect()
//    {
//        $table_prefix = (string) Mage::getConfig()->getTablePrefix();
//        $readAdapter = $this->getResource()->getReadConnection();
//        $select1 = $readAdapter->select();
//        $select1->from(array('po' => $table_prefix . 'purchase_order'));
//        $select1->joinLeft(array('po_item' => $table_prefix . 'purchase_order_item'), "po.po_id = po_item.po_id");
//        $select1->joinLeft(array('po_supplier' => $table_prefix . 'purchase_supplier'), "po.supplier_id = po_supplier.supplier_id");
//        $select1->joinLeft(array(
//            'attr_pname' => $table_prefix . 'catalog_product_entity_varchar'
//                ), "attr_pname.entity_id = po_item.product_id and attr_pname.attribute_id = 71 and attr_pname.entity_type_id = 4 and attr_pname.store_id = 0");
//        $select1->joinLeft(array('order_item' => $table_prefix . 'sales_flat_order_item'), "order_item.created_at = po.created_at");
//        $select1->reset(Zend_Db_Select::COLUMNS);
//        $select1->columns(array(
//            "transaction_date" => "po.delivery_date",
//            "sku" => "po_item.sku",
//            "name" => "attr_pname.value",
//            "po_increment_id" => "po.po_increment_id",
//            "option" => "po_item.option",
//            "vendor_name" => "po_supplier.name",
//            "delivered_qty" => "po_item.delivered_qty",
//            "price_paid" => "po_item.price",
//            "customer_name" => new Zend_Db_Expr("\"\""),
//            "item_price" => new Zend_Db_Expr("null"),
//            "order_id" => new Zend_Db_Expr("\"\""),
//            "increment_id" => new Zend_Db_Expr("\"\""),
//            "qty_ordered" => new Zend_Db_Expr("null"),
//        ));
//
//        $select2 = $readAdapter->select();
//        $select2->from(array('o' => $table_prefix . 'sales_flat_order'));
//        $select2->joinLeft(array('order_item' => $table_prefix . 'sales_flat_order_item'), "o.entity_id = order_item.order_id");
//        $select2->reset(Zend_Db_Select::COLUMNS);
//        $select2->columns(array(
//            "transaction_date" => "o.created_at",
//            "sku" => "order_item.sku",
//            "name" => "order_item.name",
//            "po_increment_id" => new Zend_Db_Expr("\"\""),
//            "option" => "order_item.product_options",
//            "vendor_name" => new Zend_Db_Expr("\"\""),
//            "delivered_qty" => new Zend_Db_Expr("null"),
//            "price_paid" => new Zend_Db_Expr("null"),
//            "customer_name" => new Zend_Db_Expr("CONCAT(o.customer_firstname,\" \" ,o.customer_lastname)"),
//            "item_price" => "order_item.price",
//            "order_id" => "o.entity_id",
//            "increment_id" => "o.increment_id",
//            "qty_ordered" => "order_item.qty_ordered",
//        ));
//        $select = $readAdapter->select()
//                ->union(array($select1, $select2), Zend_Db_Select::SQL_UNION_ALL)
//                ->order('transaction_date DESC');
//        $this->getSelect()->from(array('main_table' => $select));
//        return $this;
//    }
//
    public function getTotals()
    {
        $this->_renderFilters();
        $select = clone $this->getSelect();
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->columns(array(
            'sum_delivered_qty' => new Zend_Db_Expr("SUM(delivered_qty)"),
            'sum_price_paid' => new Zend_Db_Expr("SUM(price_paid)"),
            'count_price_paid' => new Zend_Db_Expr("COUNT(price_paid)"),
            'sum_qty_ordered' => new Zend_Db_Expr("SUM(qty_ordered)"),
            'sum_item_price' => new Zend_Db_Expr("SUM(item_price)"),
            'count_item_price' => new Zend_Db_Expr("COUNT(item_price)"),
        ));
        return $this->getResource()->getReadConnection()->fetchRow($select);
    }

}
