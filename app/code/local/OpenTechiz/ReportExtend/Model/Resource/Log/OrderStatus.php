<?php

class OpenTechiz_ReportExtend_Model_Resource_Log_OrderStatus extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_reportExtend/log_orderStatus', 'id');
    }
}