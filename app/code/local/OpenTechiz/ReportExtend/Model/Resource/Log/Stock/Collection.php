<?php

class OpenTechiz_ReportExtend_Model_Resource_Log_Stock_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_reportExtend/log_stock');
    }

    //for joining with instock table
    public function addGroupBySkuFilter()
    {
        $this->getSelect()->group('instock.sku');
        return $this;
    }
}