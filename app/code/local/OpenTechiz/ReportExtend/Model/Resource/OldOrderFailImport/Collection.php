<?php

class OpenTechiz_ReportExtend_Model_Resource_OldOrderFailImport_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_reportExtend/oldOrderFailImport');
    }
}