<?php
class OpenTechiz_ReportExtend_Model_Observer
{
    public function saveGeneralAction(Varien_Event_Observer $obs)
    {
        $adminId = $obs->getAdminId();
        $action = $obs->getAction();

        $userName = Mage::getModel('admin/user')->load($adminId)->getFirstname(). ' ' .Mage::getModel('admin/user')->load($adminId)->getLastname();
        $userEmail = Mage::getModel('admin/user')->load($adminId)->getEmail();

        $data= [];
        $data['user_id'] = $adminId;
        $data['action'] = $action;
        $data['user_name'] = $userName;
        $data['user_email'] = $userEmail;
        $data['timestamp'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');

        Mage::getModel('opentechiz_reportExtend/log_general')->setData($data)->save();
    }

    public function saveStockMovement(Varien_Event_Observer $obs){
        $userId = $obs->getUserId();
        $data= [];
        if(!empty($userId)){
            $data['user_id'] = $userId;
        }else{
            $data['user_id'] = 0;
        }
        $data['description'] = $obs->getDescription();
        $data['on_hold'] = $obs->getOnHold();
        $data['qty'] = $obs->getQty();
        $data['type'] = $obs->getType();
        $data['stock_id'] = $obs->getStockId();
        $data['timestamp'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $data['qty_before_movement'] = $obs->getQtyBeforeMovement();
        $data['on_hold_before_movement'] = $obs->getOnHoldBeforeMovement();
        $data['sku_variant'] = $obs->getSku();
        $data['po_order_id'] = $obs->getPoId() ? $obs->getPoId() : '';
        $data['production_request_id'] = $obs->getProductionRequestId() ? $obs->getProductionRequestId() : '';
        $data['order_id'] = $obs->getOrderId() ? $obs->getOrderId() : '';
        $data['product_process_request_id'] = $obs->getProductProcessRequestId() ? $obs->getProductProcessRequestId() : '';
        $data['instock_request_id'] = $obs->getInstockRequestId() ? $obs->getInstockRequestId() : '';
        Mage::getModel('opentechiz_reportExtend/log_stock')->setData($data)->save();
    }
    public function saveStockProductMovement(Varien_Event_Observer $obs){
        $userId = Mage::helper("opentechiz_reportExtend")->getUserId();
        $description ='';
        $data= [];
        if(!empty($userId)){
            $data['user_id'] = $userId;
        }else{
            $data['user_id'] = 0;
        }
        if($obs->getDescription() !=''){
            $data['description'] = $obs->getDescription();
        }
        $data['on_hold'] = $obs->getOnHold();
        $data['qty'] = $obs->getQty();
        $data['type'] = $obs->getType();
        $data['stock_id'] = $obs->getStockId();
        $data['timestamp'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $data['qty_before_movement'] = $obs->getQtyBeforeMovement();
        $data['on_hold_before_movement'] = $obs->getOnHoldBeforeMovement();
        $data['sku_variant'] = $obs->getSku();
        $data['po_order_id'] = $obs->getPoId() ? $obs->getPoId() : '';
        $data['production_request_id'] = $obs->getProductionRequestId() ? $obs->getProductionRequestId() : '';
        $data['order_id'] = $obs->getOrderId() ? $obs->getOrderId() : '';
        $data['product_process_request_id'] = $obs->getProductProcessRequestId() ? $obs->getProductProcessRequestId() : '';
        $data['instock_request_id'] = $obs->getInstockRequestId() ? $obs->getInstockRequestId() : '';
        $data['return_id'] = $obs->getReturnId() ? $obs->getReturnId() : '';
        $data['inventory_item_id'] = $obs->getItemInventoryId() ? $obs->getItemInventoryId() : '';
        $data['from_sku'] = $obs->getFromSku() ? $obs->getFromSku() : '';
        $data['to_sku'] = $obs->getToSku() ? $obs->getToSku() : '';
        $data['po_return_id'] = $obs->getPoReturnId() ? $obs->getPoReturnId() : '';
        Mage::getModel('opentechiz_inventory/stockMovement')->setData($data)->save();
    }


    public function saveOrderSource(Varien_Event_Observer $obs){
        $source = Mage::app()->getRequest()->getParam('source');
//        $data['source'] = $source;
        if($source) {
            $order = $obs->getEvent()->getOrder();
            $order->setData('source', $source);
        }
    }

    public function stockBecomeZero(Varien_Event_Observer $observer){
        $stockId = $observer->getStockId();
        $becomeZeroModel = Mage::getModel('opentechiz_reportExtend/stockBecomeZero');
        $time = date("Y-m-d H:i:s");
        $data = [];
        $data['instock_id'] = $stockId;
        $data['created_at'] = $time;
        $becomeZeroModel->setData($data)->save();
    }

    public function logOrderStageAndStatus(Varien_Event_Observer $observer){
        $order = $observer->getEvent()->getOrder();
        $newStage = $observer->getEvent()->getStage();
        $newStatus = $observer->getEvent()->getStatus();
        $isUser = $observer->getEvent()->getUser();
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $needLog = false;
        if(!$order->getOrderStage() || !$order->getInternalStatus()) $order = Mage::getModel('sales/order')->load($order->getId());
        if($order->getOrderStage() != $newStage || $order->getInternalStatus() != $newStatus){
            $needLog = true;
        }

        if($needLog && ($newStage !== null || $newStatus !== null)){
            $admin = Mage::getSingleton('admin/session')->getUser();
            $adminId = 0;
            if ($admin && $admin->getId() && $isUser){//check if the admin is logged in
                $adminId = $admin->getId();
            }

            //add current stage or status if not changed
            if($newStage === null){
                $newStage = $order->getOrderStage();
            }

            if($newStatus === null){
                $newStatus = $order->getInternalStatus();
            }

            $data = [];
            $data['user_id'] = $adminId;
            $data['order_id'] = $order->getIncrementId();
            $data['order_type'] = $order->getOrderType();
            $data['old_statuses'] = $order->getOrderStage().','.$order->getInternalStatus().','.$order->getVerification();
            $data['new_statuses'] = $newStage.','.$newStatus.','.$order->getVerification();

            if($data['old_statuses'] == $data['new_statuses']) return;
            //prevent duplicate log
            $lastHistory = Mage::getModel('opentechiz_reportExtend/log_orderStatus')->getCollection()
                ->addFieldToFilter('order_id', $order->getIncrementId())
                ->setOrder('id', 'DESC')
                ->getFirstItem();
            if($lastHistory->getOldStatuses() == $data['old_statuses'] && $lastHistory->getNewStatuses() == $data['new_statuses']) return;

            Mage::getModel('opentechiz_reportExtend/log_orderStatus')->setData($data)->save();
        }
    }

    public function logOrderVerification(Varien_Event_Observer $observer){
        $order = $observer->getEvent()->getOrder();
        $newVerification = $observer->getEvent()->getVerification();
        $isUser = $observer->getEvent()->getUser();
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $needLog = false;
        if(!$order->getVerification()) $order = Mage::getModel('sales/order')->load($order->getId());
        if($order->getVerification() != $newVerification){
            $needLog = true;
        }

        if($needLog && $newVerification != null){
            $admin = Mage::getSingleton('admin/session')->getUser();
            $adminId = 0;
            if ($admin && $admin->getId() && $isUser){//check if the admin is logged in
                $adminId = $admin->getId();
            }

            $data = [];

            $data['old_statuses'] = $order->getOrderStage().','.$order->getInternalStatus().','.$order->getVerification();
            $data['user_id'] = $adminId;
            $data['order_id'] = $order->getIncrementId();
            $data['order_type'] = $order->getOrderType();
            $data['new_statuses'] = $order->getOrderStage().','.$order->getInternalStatus().','.$newVerification;
            if($data['old_statuses'] == $data['new_statuses']) return;
            if($newVerification == 9) return;

            Mage::getModel('opentechiz_reportExtend/log_orderStatus')->setData($data)->save();
        }
    }

}