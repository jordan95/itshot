<?php

class OpenTechiz_ReportExtend_Model_NotSold extends Mage_Core_Model_Abstract
{
    public function getNotSoldData($params){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $dateFrom  = $params['from'];
        $productName = $params['product_name'];
        if(!$dateFrom){
            return array();
        }
        if(!isset($params['page'])){
            $page = 1;
        }else{
            $page = $params['page'];
        }

        $time =  strtotime($dateFrom);
        $fromNewFormat = date('Y-m-d H:i:s', strtotime("+5 hour", $time));
        $skuToExclude = $this->itemNotSoldSqlString($fromNewFormat);

        $query = "SELECT tsht_sales_flat_order_item.sku as sku, tsht_catalog_product_entity.sku as base_sku, group_sku, max(tsht_sales_flat_order.created_at) as last_sold_date, last_cost, last_cost_date, tsht_inventory_instock_product.product_id as product_id, tsht_catalog_product_entity_varchar.value as name
                  FROM `tsht_sales_flat_order_item` join tsht_sales_flat_order 
                  on tsht_sales_flat_order.entity_id = tsht_sales_flat_order_item.order_id 
                  and tsht_sales_flat_order.status not in ('closed','canceled','holded')
                  join tsht_inventory_instock_product 
                  on tsht_inventory_instock_product.sku = tsht_sales_flat_order_item.sku 
                  and tsht_inventory_instock_product.qty > 0
                  join tsht_catalog_product_entity_varchar 
                  on tsht_catalog_product_entity_varchar.entity_id = tsht_inventory_instock_product.product_id
                  and tsht_catalog_product_entity_varchar.attribute_id = 71 
                  join tsht_catalog_product_entity on tsht_catalog_product_entity.entity_id = tsht_inventory_instock_product.product_id
                  where tsht_catalog_product_entity.sku not in ".$skuToExclude;
        //add product name filter
        if($productName){
            $productName = '%'.$productName.'%';
            $query .= ' and tsht_catalog_product_entity_varchar.value like "'.$productName.'"';
        }

        //add sort
        if(isset($params['col']) && isset($params['way']) && $params['col'] && $params['way']){
            if($params['col'] == 'total_qty'){ //sort by total qty
                $query .= ' join (SELECT product_id, sum(qty) as total_qty 
                            FROM `tsht_inventory_instock_product` join tsht_catalog_product_entity 
                            on tsht_catalog_product_entity.entity_id = tsht_inventory_instock_product.product_id 
                            group by product_id) as qty_table on tsht_inventory_instock_product.product_id = qty_table.product_id';
            }
            $query .= ' GROUP by tsht_inventory_instock_product.product_id';

            if ($params['col'] == 'last_sold_date') {
                $params['col'] = 'max(tsht_sales_flat_order.created_at)';
            }
            $query .= ' ORDER by ' . $params['col'] . ' ' . $params['way'];
        }else{
            $query .= ' GROUP by tsht_inventory_instock_product.product_id';
        }

        $productNotSold = $readConnection->fetchAll($query);
        $returnData = $this->getArrayDataFromSqlResult($productNotSold, $page);

        return $returnData;
    }

    public function getNotSoldNotInStockData($params){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $dateFrom  = $params['from'];
        $productName = $params['product_name'];
        if(isset($params['exclude_9']) && $params['exclude_9']) {
            $exclude9 = true;
        }else{
            $exclude9 = false;
        }
        if(isset($params['exclude_0']) && $params['exclude_0']) {
            $exclude0 = true;
        }else{
            $exclude0 = false;
        }
        if(isset($params['exclude_d']) && $params['exclude_d']) {
            $excludeD = true;
        }else{
            $excludeD = false;
        }
        if(isset($params['main_image']) && $params['main_image']) {
            $mainImage = true;
        }else{
            $mainImage = false;
        }

        if(!$dateFrom){
            return array();
        }
        if(!isset($params['page'])){
            $page = 1;
        }else{
            $page = $params['page'];
        }

        $time =  strtotime($dateFrom);
        $fromNewFormat = date('Y-m-d H:i:s', strtotime("+5 hour", $time));
        $skuToExclude = $this->itemNotSoldSqlString($fromNewFormat);

        $query = "SELECT tsht_inventory_instock_product.sku as sku, tsht_catalog_product_entity.sku as base_sku, group_sku, max(tsht_sales_flat_order.created_at) as last_sold_date, last_cost, last_cost_date, tsht_inventory_instock_product.product_id as product_id, tsht_catalog_product_entity_varchar.value as name
                  FROM  tsht_inventory_instock_product
                  left join `tsht_sales_flat_order_item` on tsht_inventory_instock_product.sku = tsht_sales_flat_order_item.sku 
                  join tsht_sales_flat_order 
                  on tsht_sales_flat_order.entity_id = tsht_sales_flat_order_item.order_id 
                  and tsht_sales_flat_order.status not in ('closed','canceled','holded')
                  join tsht_catalog_product_entity_varchar 
                  on tsht_catalog_product_entity_varchar.entity_id = tsht_inventory_instock_product.product_id
                  and tsht_catalog_product_entity_varchar.attribute_id = 71
                  join tsht_catalog_product_entity_int
                  on tsht_catalog_product_entity_int.entity_id = tsht_inventory_instock_product.product_id
                  and tsht_catalog_product_entity_int.attribute_id = 96
                  and tsht_catalog_product_entity_int.value = 1
                  join tsht_catalog_product_entity on tsht_catalog_product_entity.entity_id = tsht_inventory_instock_product.product_id
                  join (SELECT product_id, sum(qty) as total_qty 
                  FROM `tsht_inventory_instock_product` join tsht_catalog_product_entity 
                  on tsht_catalog_product_entity.entity_id = tsht_inventory_instock_product.product_id
                  group by product_id) as qty_table on tsht_inventory_instock_product.product_id = qty_table.product_id and total_qty = 0";
        //add product name filter
        if($productName){
            $productName = '%'.$productName.'%';
            $query .= ' and tsht_catalog_product_entity_varchar.value like "'.$productName.'"';
        }

        //add exclude filter
        if($exclude9){
            $query .= ' and tsht_inventory_instock_product.sku not like "9%"';
        }
        if($exclude0){
            $query .= ' and tsht_inventory_instock_product.sku not like "0%"';
        }
        if($excludeD){
            $query .= ' and tsht_inventory_instock_product.sku not like "d%" and tsht_inventory_instock_product.sku not like "D%"';
        }

        //add supplier filter
        if(isset($params['exclude_supplier']) && $params['exclude_supplier'] != ''){
            $supId = $params['exclude_supplier'];
            $query .= ' left join (SELECT product_id FROM `tsht_purchase_product_supplier` 
                where sup_id = '.$supId.') as supplier_product on supplier_product.product_id = tsht_inventory_instock_product.product_id';
            $query .= " where tsht_catalog_product_entity.sku not in ".$skuToExclude."  and supplier_product.product_id is null";
        }else{
            $query .= " where tsht_catalog_product_entity.sku not in ".$skuToExclude."";
        }

        $query .= ' GROUP by tsht_inventory_instock_product.product_id';

        //add sort
        if(isset($params['col']) && isset($params['way']) && $params['col'] && $params['way']){
            if ($params['col'] == 'last_sold_date') {
                $params['col'] = 'max(tsht_sales_flat_order.created_at)';
            }
            $query .= ' ORDER by ' . $params['col'] . ' ' . $params['way'];
        }else{
            $query .= ' ORDER by sku ASC';
        }

        $productNotSold = $readConnection->fetchAll($query);
        $returnData = $this->getArrayDataFromSqlResult($productNotSold, $page, false, $mainImage);

        return $returnData;
    }

    public function getConvertDate($date)
    {
        if(!$date || strtotime($date) < 0){
            return 'No Data';
        }
        $createdtime = new Zend_Date(strtotime($date));
        $convertDate  = Mage::getModel('core/date')->date('m/d/Y', $createdtime);

        return $convertDate;
    }

    public function itemNotSoldSqlString($fromNewFormat){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $query = "SELECT sku FROM `tsht_sales_flat_order_item` join tsht_sales_flat_order 
                  on tsht_sales_flat_order.entity_id = tsht_sales_flat_order_item.order_id 
                  and tsht_sales_flat_order.status not in ('closed','canceled','holded')
                  and tsht_sales_flat_order.created_at > '".$fromNewFormat."'";

        $productsSoldInPeriod = $readConnection->fetchAll($query);
        $skuToExclude = '("';
        foreach ($productsSoldInPeriod as $excludedSku){
            $excludedSku['base_sku'] = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $excludedSku['sku'])[0];
            $skuToExclude .= $excludedSku['base_sku'].'","';
        }

        $skuToExclude = trim($skuToExclude,',"');
        $skuToExclude .= '")';

        if($skuToExclude == '(")'){
            $skuToExclude = "('_')";
        }

        return $skuToExclude;
    }

    public function getArrayDataFromSqlResult($productNotSold, $page, $getStockData = true, $mainImage = false){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $returnData = [];
        $counter = 0;

        foreach ($productNotSold as $productData){
            $counter++;

            //paging
            $pageLast = $page*100;
            $pageFirst = $pageLast-100;
            if($counter < $pageFirst || $counter > $pageLast) continue;

            $data = [];
            $data['sku'] = $productData['group_sku'];
            $data['last_cost'] = '$'.round($productData['last_cost'], 2);
            $data['last_cost_date'] = strtotime($productData['last_cost_date']);
            if($data['last_cost_date']) {
                $data['last_cost_date'] = date('m/d/Y', $data['last_cost_date']);
            }else{
                $data['last_cost_date'] = 'N/A';
            }
            $data['last_sold_date'] = $this->getConvertDate($productData['last_sold_date']);
            $data['product_name'] = $productData['name'];
            $data['product_id'] = $productData['product_id'];
            $data['base_sku'] = $productData['base_sku'];

            $product = mage::getModel('catalog/product')->load($productData['product_id']);
            if(!$product || !$product->getId()){
                continue;
            }
            $data['special_price'] = number_format($product->getSpecialPrice(), 2, '.', '');

            $data['closeout'] = '';
            $cats = $product->getCategoryIds();

            $categoryHtml = '<select multiple="multiple" size="8" name="category_'.$product->getId().'[]" onchange="enableFormProductData(this)">';
            $categorySource = Mage::getModel('sitemapEnhancedPlus/system_config_source_category')->toOptionArray('select');
            foreach ($categorySource as $category){
                if(in_array($category['value'], $cats)){
                    $categoryHtml .= '<option value="' . $category['value'] . '" selected>' . $category['label'] . '</option>';
                }else {
                    $categoryHtml .= '<option value="' . $category['value'] . '">' . $category['label'] . '</option>';
                }
            }
            $categoryHtml .= '</select>';

            foreach ($cats as $category_id) {
                $_cat = Mage::getModel('catalog/category')->load($category_id) ;
                if($_cat->getName() == 'Clearance Sale'){
                    $data['closeout'] = 'checked';
                }
            }

            $data['category'] = $categoryHtml;

            if($mainImage){
                $product->setSku($productData['base_sku']);
            }else {
                $product->setSku($productData['sku']);
            }
            $data['image'] = Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(120, 120)->__toString();

            if($getStockData) {
                //get instock variant html
                $baseSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $data['sku'])[0];

                $query = 'SELECT * FROM tsht_inventory_instock_product where sku like "' . $baseSku . '" or sku like "' . $baseSku . '\_%"';
                $productVariants = $readConnection->fetchAll($query);
                $variantData = [];
                foreach ($productVariants as $productVariant) {
                    if (!$productVariant['group_sku']) continue;
                    if (!isset($variantData[$productVariant['group_sku']])) {
                        $variantData[$productVariant['group_sku']] = $productVariant['qty'];
                    } else {
                        $variantData[$productVariant['group_sku']] += $productVariant['qty'];
                    }
                }
                asort($variantData);
                $variantData = array_reverse($variantData, true);

                $variantHtml = '';
                foreach ($variantData as $key => $value) {
                    $variantHtml .= $key . ' x ' . $value . '<br>';
                }
                $data['qty'] = $variantHtml;
            }
            array_push($returnData, $data);
        }

        $returnData['max_page'] = ceil($counter/100);

        return $returnData;
    }
}