<?php

class OpenTechiz_ReportExtend_Model_StaffPerformance extends Mage_Core_Model_Abstract
{
    public function getStaffData($params){
        $dateFrom  = $params['from'];
        $dateTo    = $params['to'];
        $staffId   = $params['staff'];

        $time =  strtotime($dateFrom);
        $fromNewFormat = date('Y-m-d H:i:s',  strtotime("+4 hour", $time));

        $time =  strtotime($dateTo);
        $toNewFormat = date('Y-m-d H:i:s',strtotime("+28 hour", $time));

        $historyModel = Mage::getModel('opentechiz_production/production_history');
        
        $collection = $historyModel->getCollection()->addFieldToFilter('staff_id', $staffId)
            ->addFieldToFilter('finished_at', array(
                'from' => $fromNewFormat,
                'to'   => $toNewFormat
            ))->setOrder('finished_at', 'ASC');;

        $result = []; $data = [];
        foreach ($collection as $history){
            $date = date('m/d/Y' , strtotime("-4 hour", strtotime($history->getFinishedAt())));
            if(!array_key_exists($date, $data)){
                $data[$date] = [];
            }
            array_push($data[$date], $history->getData());
        }
        foreach ($data as $day => $steps){
            $Error = 0;
            $Process = 0;
            $Broke = 0;
            foreach ($steps as $step){
                $Process++;
                if($step['status'] == 1) $Broke++;
                elseif($step['status'] == 2) $Error++;
            }
            $errorRate = (float)round($Error / $Process, 4) * 100 .'%';
            $brokeRate = (float)round($Broke / $Process, 4) * 100 .'%';

            $result[$day] = [];
            $result[$day]['process'] = $Process;
            $result[$day]['error'] = $Error;
            $result[$day]['broke'] = $Broke;
            $result[$day]['error_rate'] = $errorRate;
            $result[$day]['broke_rate'] = $brokeRate;
        }

        return $result;
    }
}