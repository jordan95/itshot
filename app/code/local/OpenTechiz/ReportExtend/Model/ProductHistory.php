<?php

class OpenTechiz_ReportExtend_Model_ProductHistory extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('opentechiz_reportExtend/productHistory', 'product_history_id');
    }

//    
//    public function getProductHistoryData($params)
//    {
//        $filterDate = false;
//        if ((isset($params['from']) && $params['from']) || (isset($params['to']) && $params['to'])) {
//            $filterDate = true;
//            if (isset($params['from'])) {
//                $dateFrom = date('Y-m-d 00:00:00', strtotime($params['from']));
//            } else {
//                $dateFrom = date('Y-m-d 00:00:00');
//            }
//            if (isset($params['to'])) {
//                $dateTo = date('Y-m-d 25:59:59', strtotime($params['to']));
//            } else {
//                $dateTo = date('Y-m-d 25:59:59');
//            }
//        }
//        
//
//        $data = [];
//        $resource = Mage::getSingleton('core/resource');
//        $readAdapter = $resource->getConnection('core_read');
//        $table_prefix = (string) Mage::getConfig()->getTablePrefix();
//
//
//        $select1 = $readAdapter->select();
//        $select1->from(array('po' => $table_prefix . 'purchase_order'));
//        $select1->joinLeft(array('po_item' => $table_prefix . 'purchase_order_item'), "po.po_id = po_item.po_id");
//        $select1->joinLeft(array('po_supplier' => $table_prefix . 'purchase_supplier'), "po.supplier_id = po_supplier.supplier_id");
//        $select1->joinLeft(array(
//            'attr_pname' => $table_prefix . 'catalog_product_entity_varchar'
//                ), "attr_pname.entity_id = po_item.product_id and attr_pname.attribute_id = 71 and attr_pname.entity_type_id = 4 and attr_pname.store_id = 0");
//        $select1->joinLeft(array('order_item' => $table_prefix . 'sales_flat_order_item'), "order_item.created_at = po.created_at");
//        $select1->reset(Zend_Db_Select::COLUMNS);
//        $select1->columns(array(
//            "transaction_date" => new Zend_Db_Expr("DATE_FORMAT(po.delivery_date,'%m/%d/%Y')"),
//            "m" => "po.delivery_date",
//            "sku" => "po_item.sku",
//            "name" => "attr_pname.value",
//            "po_increment_id" => "po.po_increment_id",
//            "option" => "po_item.option",
//            "vendor_name" => "po_supplier.name",
//            "delivered_qty" => "po_item.delivered_qty",
//            "price_paid" => "po_item.price",
//            "customer_name" => new Zend_Db_Expr("\"\""),
//            "item_price" => new Zend_Db_Expr("\"\""),
//            "increment_id" => new Zend_Db_Expr("\"\""),
//            "qty_ordered" => new Zend_Db_Expr("\"\""),
//        ));
//        if ($filterDate) {
//            $select1->where("po.delivery_date >= ?", $dateFrom)
//                    ->where("po.delivery_date <= ?", $dateTo);
//        }
//        if (isset($params['sku']) && $params['sku']) {
//            $select1->where('po_item.sku = ?', $params['sku']);
//        }
//
//        $select2 = $readAdapter->select();
//        $select2->from(array('o' => $table_prefix . 'sales_flat_order'));
//        $select2->joinLeft(array('order_item' => $table_prefix . 'sales_flat_order_item'), "o.entity_id = order_item.order_id");
//        $select2->reset(Zend_Db_Select::COLUMNS);
//        $select2->columns(array(
//            "transaction_date" => new Zend_Db_Expr("DATE_FORMAT(o.created_at,'%m/%d/%Y')"),
//            "m" => "o.created_at",
//            "sku" => "order_item.sku",
//            "name" => "order_item.name",
//            "po_increment_id" => new Zend_Db_Expr("\"\""),
//            "option" => "order_item.product_options",
//            "vendor_name" => new Zend_Db_Expr("\"\""),
//            "delivered_qty" => new Zend_Db_Expr("\"\""),
//            "price_paid" => new Zend_Db_Expr("\"\""),
//            "customer_name" => new Zend_Db_Expr("CONCAT(o.customer_firstname,\" \" ,o.customer_lastname)"),
//            "item_price" => "order_item.price",
//            "increment_id" => "o.increment_id",
//            "qty_ordered" => "order_item.qty_ordered",
//        ));
//        if ($filterDate) {
//            $select2->where("o.created_at >= ?", $dateFrom)
//                    ->where("o.created_at <= ?", $dateTo);
//        }
//        if (isset($params['sku']) && $params['sku']) {
//            $select2->where('order_item.sku = ?', $params['sku']);
//        }
//        $select = $readAdapter->select()
//                ->union(array($select1, $select2))
//                ->order('m DESC')
//                ->limit(1000);
//        $result = $readAdapter->fetchAll($select);
//
//        $sum_delivered_qty = 0;
//        $sum_price_paid = 0;
//        $sum_item_price = 0;
//        $sum_qty_ordered = 0;
//
//        $item_length = count($result);
//        if ($item_length > 0) {
//            $url = Mage::getModel("core/url");
//            foreach ($result as $item) {
//                if (strlen($item['option']) > 0) {
//                    $item["option"] = $this->renderOption($item['sku'], $item['name'], $item['option']);
//                }
//                if ($item['price_paid']) {
//                    $sum_price_paid += (float) $item['price_paid'];
//                    $item['price_paid'] = Mage::helper('core')->currency($item['price_paid'], true, false);
//                }
//                if ($item['item_price']) {
//                    $sum_item_price += (float) $item['item_price'];
//                    $item['item_price'] = Mage::helper('core')->currency($item['item_price'], true, false);
//                }
//                if ($item['delivered_qty']) {
//                    $sum_delivered_qty += (int) $item['delivered_qty'];
//                    $item['delivered_qty'] = number_format($item['delivered_qty']);
//                }
//                if ($item['qty_ordered']) {
//                    $sum_qty_ordered += (int) $item['qty_ordered'];
//                    $item['qty_ordered'] = number_format($item['qty_ordered']);
//                }
//                $item["inventory_link"] = $url->getUrl("adminhtml/instockProduct/index", array("filter" => base64_encode("sku=" . $item['sku'])));
//                $data[] = $item;
//            }
//        }
//        if ($item_length == 0) {
//            $avg_price_paid = 0;
//            $avg_item_price = 0;
//        } else {
//            $avg_price_paid = $sum_price_paid / $item_length;
//            $avg_item_price = $sum_item_price / $item_length;
//        }
//        return array(
//            "sum_delivered_qty" => $sum_delivered_qty,
//            "sum_price_paid" => Mage::helper('core')->currency($sum_price_paid, true, false),
//            "avg_price_paid" => Mage::helper('core')->currency($avg_price_paid, true, false),
//            "sum_qty_ordered" => $sum_qty_ordered,
//            "sum_item_price" => Mage::helper('core')->currency($sum_item_price, true, false),
//            "avg_item_price" => Mage::helper('core')->currency($avg_item_price, true, false),
//            "data" => $data
//        );
//    }
//
//    public function renderOption($sku, $name, $value)
//    {
//        if (strpos($sku, 'quotation') === false) {
//            $value = unserialize($value);
//            $optionHtml = '';
//
//            if (array_key_exists('options', $value)) {
//                foreach ($value['options'] as $option) {
//                    $optionHtml .= '<dt>' . $option['label'] . '</dt>';
//                    $optionHtml .= '<dd>' . $option['value'] . '</dd>';
//                }
//            }
//
//            $html = '<div class="item-container">
//                    <div class="item-text">
//                        <div><strong>SKU:</strong>' . $sku . '</div>
//                        <dl class="item-options">
//                            ' . $optionHtml . '
//                        </dl>
//                    </div>
//                </div>';
//        } else {
//            $quotationProductId = explode('-', $sku)[1];
//            $optionHtml = '';
//            $optionCollection = Mage::getModel('opentechiz_quotation/productOption')->getCollection()->addFieldToFilter('quote_product_id', $quotationProductId);
//            foreach ($optionCollection as $option) {
//                if ($option->getOptionType() == 'stone') {
//                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
//                    $optionHtml .= '<dd>' . Mage::getModel('opentechiz_quotation/product')->getStoneName($option->getOptionValue()) . '</dd>';
//                } elseif ($option->getOptionType() == 'gold') {
//                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
//                    $optionHtml .= '<dd>' . Mage::getModel('opentechiz_quotation/product')->getGoldName($option->getOptionValue()) . '</dd>';
//                } else {
//                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
//                    $optionHtml .= '<dd>' . $option->getOptionValue() . '</dd>';
//                }
//            }
//            $html = '<div class="item-container">
//                    <div class="item-text">
//                        <div><strong>SKU:</strong>' . $sku . '</div>
//                        <dl class="item-options">
//                            ' . $optionHtml . '
//                        </dl>
//                    </div>
//                </div>';
//        }
//        return $html;
//    }
}
