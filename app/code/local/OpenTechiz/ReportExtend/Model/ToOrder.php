<?php

class OpenTechiz_ReportExtend_Model_ToOrder extends Mage_Core_Model_Abstract
{
    protected $excludedPOStatusInReport = '('.
    OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL .','.
    OpenTechiz_Purchase_Helper_Data::PO_STATUS_CLOSED .','.
    OpenTechiz_Purchase_Helper_Data::PO_STATUS_DRAFTED
    .')';
    protected $shippedStates = '(15,20)'; //Shipped out, Shipped to customer
    protected $clearanceSalesCategoryId = 12770;
    protected $qtyInProduction = [];
    protected $salesPerReturn3M = [];
    protected $salesPerReturn12M = [];
    protected $skuStringForQuery = '';
    protected $allSku = [];

    public function getToOrderData($params){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $inventoryHelper = Mage::helper('opentechiz_inventory');

        $type = $params['type'];
        $category = $params['category'];

        $dateFrom  = $params['from'];
        $time =  strtotime($dateFrom);
        if($time) {
            $fromNewFormat = date('Y-m-d H:i:s', strtotime("+5 hour", $time));
            $toNewFormat = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        }
        //paging
        if(!isset($params['page'])){
            $page = 1;
        }else{
            $page = $params['page'];
        }
        $pageLast = $page*100;
        $pageFirst = $pageLast-100;

        //all sku in movement
        $allSku = [];
        $allName = [];

        if($time) { //if added timestamp filter, filter time by product_history_log table
            $query = "SELECT tsht_inventory_instock_product.*, value FROM `tsht_product_history_log` 
            INNER join tsht_inventory_instock_product 
            on tsht_inventory_instock_product.id = tsht_product_history_log.stock_id 
            join tsht_catalog_product_entity_varchar 
            on tsht_catalog_product_entity_varchar.entity_id = tsht_inventory_instock_product.product_id
            and tsht_catalog_product_entity_varchar.attribute_id = 71
            and tsht_catalog_product_entity_varchar.store_id = 0 ";

            $query .= "and tsht_product_history_log.timestamp >= '" . $fromNewFormat . "'
            and tsht_product_history_log.timestamp <= '" . $toNewFormat . "'";

            if($category){
                $query .= " join tsht_catalog_category_product on tsht_catalog_category_product.product_id = tsht_inventory_instock_product.product_id
            and tsht_catalog_category_product.category_id = ".$category;
            }

            $query .= " where tsht_product_history_log.qty like '-%'";
        } else { //if no timestamp, fetch data for reorder point
            $query = "SELECT tsht_inventory_instock_product.*, value FROM tsht_inventory_instock_product 
            join tsht_catalog_product_entity_varchar 
            on tsht_catalog_product_entity_varchar.entity_id = tsht_inventory_instock_product.product_id
            and tsht_catalog_product_entity_varchar.attribute_id = 71
            and tsht_catalog_product_entity_varchar.store_id = 0 ";

            if($category){
                $query .= " join tsht_catalog_category_product on tsht_catalog_category_product.product_id = tsht_inventory_instock_product.product_id
            and tsht_catalog_category_product.category_id = ".$category;
            }

            $query .= " where tsht_inventory_instock_product.qty <= tsht_inventory_instock_product.reorder_point
            and tsht_inventory_instock_product.reorder_point > 0";
        }


        $results = $readConnection->fetchAll($query);

        foreach ($results as $result){
            if($result['group_sku'] == ''){
                $result['group_sku'] = Mage::helper('opentechiz_inventory')->getGroupSKU($result['sku']);
            }

            //get product name
            $allName[$result['group_sku']] = $result['value'];

            //check if sku already listed
            $exist = false;
            foreach ($allSku as $key => $sku){
                if(strpos($sku, $result['group_sku']) !== false){
                    $exist = true;
                    continue;
                }elseif(strpos($result['group_sku'], $sku) !== false){
                    $exist = true;
                    $allSku[$key] = $result['group_sku'];
                }
            }
            if(!$exist) {
                array_push($allSku, $result['group_sku']);
            }
        }

        //return empty if query empty
        if(!count($allSku)){
            return [];
        }

        $this->allSku = $allSku;

        //get string for queries
        $skuStringForQuery = '(';
        foreach ($allSku as $sku){
            $skuStringForQuery .= "'".$sku."',";
        }
        $skuStringForQuery = trim($skuStringForQuery, ',');
        $skuStringForQuery .= ')';
        $this->skuStringForQuery = $skuStringForQuery;

        $skuStringForRegexQuery = "'";
        foreach ($allSku as $sku){
            $skuStringForRegexQuery .= $sku.'|';
        }
        $skuStringForRegexQuery = trim($skuStringForRegexQuery, '|');
        $skuStringForRegexQuery .= "'";

        //get count all qty instock
        $query = "SELECT group_sku,SUM(qty) total_qty, product_id
                 FROM tsht_inventory_instock_product
                 where group_sku in ".$skuStringForQuery."
                 GROUP BY group_sku";
        $stockQuantities = $readConnection->fetchAll($query);
        foreach ($stockQuantities as $key => $value){
            $stockQuantities[$value['group_sku']] = $value;
            unset($stockQuantities[$key]);
        }

        //get count all reorder point
        $query = "SELECT group_sku, MAX(reorder_point) max_reorder_point
                 FROM tsht_inventory_instock_product
                 where group_sku in ".$skuStringForQuery."           
                 GROUP BY group_sku";
        $reorderPoints = $readConnection->fetchAll($query);
        foreach ($reorderPoints as $key => $value){
            $reorderPoints[$value['group_sku']] = $value;
            unset($reorderPoints[$key]);
        }

        //get count all item in production
        $query = "SELECT * FROM tsht_request_product 
                join tsht_sales_flat_order_item on tsht_request_product.order_item_id = tsht_sales_flat_order_item.item_id
                join tsht_sales_flat_order on tsht_sales_flat_order_item.order_id = tsht_sales_flat_order.entity_id
                join tsht_inventory_instock_product on tsht_inventory_instock_product.sku = tsht_sales_flat_order_item.sku
                where tsht_sales_flat_order.status not in ('complete', 'canceled', 'closed', 'holded')
                and tsht_sales_flat_order.order_stage = 2 and tsht_sales_flat_order.internal_status = 2
                and tsht_inventory_instock_product.group_sku in ".$skuStringForQuery;
        $itemsInProduction = $readConnection->fetchAll($query);
        $qtyInProduction = [];
        foreach ($itemsInProduction as $item){
            $groupSku = $item['group_sku'];
            if(!array_key_exists($groupSku, $qtyInProduction)){
                $qtyInProduction[$groupSku] = 1;
            }else{
                $qtyInProduction[$groupSku] += 1;
            }
        }
        $this->qtyInProduction = $qtyInProduction;

        //get count all item not delivered in PO
        $query = "SELECT tsht_purchase_order_item.*, group_sku, tsht_inventory_instock_product.sku as inventory_sku
                  FROM `tsht_purchase_order_item` join tsht_inventory_instock_product
                  on tsht_inventory_instock_product.sku = tsht_purchase_order_item.sku
                  and tsht_purchase_order_item.qty > tsht_purchase_order_item.delivered_qty
                  WHERE tsht_inventory_instock_product.sku REGEXP ".$skuStringForRegexQuery;
        $itemsInPO = $readConnection->fetchAll($query);
        $qtyInPO = [];
        //count total qty not delivered in POs
        foreach ($itemsInPO as $item){
            $groupSku = $item['group_sku'];
            if(!$groupSku){
                $groupSku = $inventoryHelper->getGroupSKU($item['inventory_sku']);
            }
            //in case sku in main array different
            foreach ($allSku as $poSku){
                if(strpos($groupSku, $poSku) !== false){
                    $groupSku = $poSku;
                    break;
                }
            }
            if(!array_key_exists($groupSku, $qtyInPO)){
                $qtyInPO[$groupSku] = $item['qty'] - $item['delivered_qty'];
            }else{
                $qtyInPO[$groupSku] += $item['qty'] - $item['delivered_qty'];
            }
        }

        //get count all item in open orders
        $itemsInOrderCollection = Mage::getModel('sales/order_item')->getCollection()
            ->join(array('orderData'=>'sales/order'), 'orderData.entity_id=main_table.order_id', array('*'))
            ->join(array('inventory'=>'opentechiz_inventory/instock'), 'inventory.sku=main_table.sku')
            ->addFieldToFilter('group_sku', array('in'=>$allSku));
        $itemsInOrderCollection = Mage::helper('opentechiz_reportExtend/collection_filter')->addOrderNeedItemFilter($itemsInOrderCollection);
        $qtyInOrders = [];

        foreach ($itemsInOrderCollection as $item){
            $groupSku = $item->getGroupSku();
            $qtyInCurrentOrderItem = (int)($item->getQtyOrdered() - $item->getQtyCanceled() - $item->getQtyRefunded() - $item->getData('count'));
            if(!array_key_exists($groupSku, $qtyInOrders)){
                $qtyInOrders[$groupSku] = $qtyInCurrentOrderItem;
            }else{
                $qtyInOrders[$groupSku] += $qtyInCurrentOrderItem;
            }
        }

        //get sales per return
        $this->salesPerReturn3M = $this->getNumberOfSoldPerReturn(3);
        $this->salesPerReturn12M = $this->getNumberOfSoldPerReturn(12);

        //filter results
        $results = [];
        foreach ($allSku as $groupSku){
            if(!isset($stockQuantities[$groupSku])) continue;
            $productId = $stockQuantities[$groupSku]['product_id'];
            //add qty for item not exist
            if(!isset($qtyInProduction[$groupSku])){
                $qtyInProduction[$groupSku] = 0;
            }
            if(!isset($qtyInPO[$groupSku])){
                $qtyInPO[$groupSku] = 0;
            }
            if(!isset($qtyInOrders[$groupSku])){
                $qtyInOrders[$groupSku] = 0;
            }

            $qtyAvailable = $stockQuantities[$groupSku]['total_qty'] + $qtyInProduction[$groupSku] + $qtyInPO[$groupSku];
            $qtyInOpenOrder = $qtyInOrders[$groupSku];
            $reorderPoint = $reorderPoints[$groupSku]['max_reorder_point'];

            if($type != 2) { //ignore qty and reorder point check if Regular Sales Report

                //skip if qty available > qty in open orders and reorder point
                if ($type == 0) {
                    if ($qtyAvailable > $qtyInOpenOrder + $reorderPoint) continue;
                } elseif ($type == 1) {
                    if ($qtyAvailable > $reorderPoint || $reorderPoint == 0) continue;
                }
            }

            //push to final result
            $data = [];
            $data['sku'] = $groupSku;
            $data['group_sku'] = $groupSku;
            $data['product_id'] = $productId;
            $data['qty_needed_in_order'] = $qtyInOrders[$groupSku];
            $data['reorder_point'] = $reorderPoint;
            array_push($results, $data);
        }

        $productSalesPrice = [];
        $productImages = [];
        $productUrls = [];
        $closeOutData = [];
        $skuReported = [];

        //get disabled products
        $disabledProductIds = [];
        $query = "SELECT * FROM `tsht_catalog_product_entity_int` WHERE `attribute_id` = 96 AND `value` = 2 ";
        $resultProductStatus = $readConnection->fetchAll($query);
        foreach ($resultProductStatus as $productStatusData){
            array_push($disabledProductIds, $productStatusData['entity_id']);
        }

        //get product price
        $productPriceList = [];
        $query = "SELECT * FROM `tsht_catalog_product_entity_decimal` where attribute_id = 76 ";
        $resultProductPrice = $readConnection->fetchAll($query);
        foreach ($resultProductPrice as $productPriceData){
            $productPriceList[$productPriceData['entity_id']] = $productPriceData['value'];
        }

        //get products in Clearance Sales
        $clearanceSaleList = [];
        $query = "SELECT * FROM `tsht_catalog_category_product` WHERE `category_id` = ".$this->clearanceSalesCategoryId;
        $resultClearanceSale = $readConnection->fetchAll($query);
        foreach ($resultClearanceSale as $clearanceSaleData){
            array_push($clearanceSaleList, $clearanceSaleData['product_id']);
        }

        //productCollection
        $productCollectionArray = [];
        $productCollection = Mage::getModel('catalog/product')->getCollection()->addFieldToFilter('status', 1);
        foreach ($productCollection as $productData){
            $productCollectionArray[$productData->getId()] = $productData;
        }

        $counter = 0;
        foreach ($results as $key => $soldItem){
            //remove disabled product
            if(in_array($soldItem['product_id'], $disabledProductIds)){
                unset($results[$key]);
                continue;
            }
            //avoid error when no product found
            if(!$soldItem['product_id']){
                unset($results[$key]);
                continue;
            }

            //avoid report same sku
            $group_sku = $soldItem['group_sku'];
            if(!in_array($group_sku, $skuReported)){
                array_push($skuReported, $group_sku);
            }else{
                unset($results[$key]);
                continue;
            }

            //apply paging
            $counter++;
            if($counter < $pageFirst || $counter > $pageLast){
                unset($results[$key]);
                continue;
            }

            $productSalesPrice[$group_sku] = $productPriceList[$soldItem['product_id']];
            $product = $productCollectionArray[$soldItem['product_id']];
            $product->setSku($group_sku);
            
            $productImages[$group_sku] = Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(120, 120)->__toString();
            $productUrls[$group_sku] = Mage::helper('adminhtml')->getUrl('adminhtml/catalog_product/edit/', array('id' => $product->getId()));

            if(in_array($soldItem['product_id'], $clearanceSaleList)) $closeOutData[$group_sku] = 'YES';
            else $closeOutData[$group_sku] = 'NO';
        }

        //get cost and cost date
        $instock = Mage::getModel('opentechiz_inventory/instock')->getCollection()
            ->addFieldToFilter('group_sku', array('in' => $this->allSku));
        $costData = [];
        foreach ($instock as $instockData){
            $costData[$instockData->getGroupSku()] = [];
            $costData[$instockData->getGroupSku()]['cost'] = $instockData->getLastCost();
            $costData[$instockData->getGroupSku()]['cost_date'] = $instockData->getLastCostDate();
        }

        $emailData = [];
        $emailedItemSkus = [];
        $groupSkuSort = array_column($results, 'group_sku');
        array_multisort($groupSkuSort, SORT_ASC, $results);
        foreach ($results as $soldItem){
            $sku = $soldItem['sku'];
            $baseSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
            $group_sku = $soldItem['group_sku'];

            //get sku from group_sku
            $data = [];
            $data['sku'] = $sku; //sku
            $data['name'] = $allName[$group_sku]; //product name
            $data['movement_url'] = $this->getStockMovementUrl($sku);
            if($group_sku){
                $data['sku'] = $group_sku;
            }
            if(!in_array($data['sku'], $emailedItemSkus)){
                array_push($emailedItemSkus, $data['sku']);
            }else{
                continue;
            }

            $data['price'] = '$'.number_format($productSalesPrice[$group_sku], 2, '.', ''); //sales price
            $data['price_to_sort'] = $productSalesPrice[$group_sku];
            $data['stock'] = $stockQuantities[$group_sku]['total_qty']; //qty
            $data['reorder_point'] = $soldItem['reorder_point'];
            $data['inventory_url'] = $this->getInventoryUrl($sku);

            //get cost
            if(strtotime($costData[$group_sku]['cost_date'])) {
                $data['cost_date'] = Mage::getModel('core/date')->date('M d, Y', $costData[$group_sku]['cost_date']);
            }else{
                $data['cost_date'] = 'N/A';
            }
            $data['cost'] = '$'.number_format($costData[$group_sku]['cost'], 2, '.', '');

            //get 3m/ret, 12m/ret
            $sku = $baseSku;
            if($group_sku){
                $sku = $group_sku;
            }

            $data['12m'] = $this->salesPerReturn12M[$sku];
            $data['3m'] = $this->salesPerReturn3M[$sku];

            //get manufacturing qty
            $sku = $baseSku;
            //get sku from group_sku
            if($group_sku){
                $sku = $group_sku;
            }
            $data['manufacturing'] = $this->getCurrentManufacturingQty($sku);

            //get current amount needed in open orders
            $data['on_order'] = (int)$soldItem['qty_needed_in_order'];

            //get image
            $data['url'] = $productUrls[$group_sku];
            $data['image'] = $productImages[$group_sku];

            //check clearance sales
            $data['closed_out'] = $closeOutData[$group_sku];
            $data['master_po'] = $this->getMasterPOButtonHtml($soldItem['product_id']);
            array_push($emailData, $data);
        }

        //add sort
        if(isset($params['col']) && isset($params['way']) && $params['col'] && $params['way']){
            if($params['way'] == 'desc'){
                $sortString = SORT_DESC;
            }else{
                $sortString = SORT_ASC;
            }
            $sortedArray = array();
            foreach ($emailData as $key => $row)
            {
                $sortedArray[$key] = (float)$row[$params['col']];
            }
            array_multisort($sortedArray, $sortString, $emailData);
        }
        $emailData['max_page'] = ceil($counter/100);
        return $emailData;
    }

    public function getCurrentManufacturingQty($sku){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $instockItem = Mage::getModel('opentechiz_inventory/instock')->load($sku, 'sku');
        if($instockItem && $instockItem->getGroupSku()){
            $sku = $instockItem->getGroupSku();
        }
        $manufacture = '';
        if(isset($this->qtyInProduction[$sku])){
            $manufacture .= '<strong>In Production (Magento):</strong> '.$this->qtyInProduction[$sku].'<br>';
        }

        $query = 'SELECT * FROM `tsht_purchase_order_item` 
                      join tsht_purchase_order on tsht_purchase_order.po_id = tsht_purchase_order_item.po_id 
                      where sku like "'.$sku.'%" and qty > delivered_qty and status not in '.$this->excludedPOStatusInReport;
        $results = $readConnection->fetchAll($query);
        $POs = [];
        foreach ($results as $result){
            $countPO = ($result['qty'] - $result['delivered_qty']);
            $variantSku = $result['sku'];
            $variantSkuParts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $variantSku);
            foreach ($variantSkuParts as $key => $variantSkuPart){
                if($key == 0) continue;
                $variantSkuPart = str_replace('in.', '', $variantSkuPart);
                $variantSkuPart = Mage::helper('opentechiz_inventory')->convertSizeString($variantSkuPart);
                if(is_numeric($variantSkuPart)) unset($variantSkuParts[$key]);
            }
            $variantSku = implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $variantSkuParts);

            $poUrl = Mage::helper('adminhtml')->getUrl('adminhtml/purchase_product/edit/', array('id' => $result['po_id']));
            if(isset($POs[$variantSku])){
                $POs[$variantSku]['qty'] += $countPO;
                $POs[$variantSku]['string'] .= ','. '<a href="'.$poUrl.'">'. explode(' ', $result['delivery_date'])[0].'</a>'
                    .' ('.$result['delivered_qty'].'/'.$result['qty'].')';
            }
            else{
                $POs[$variantSku] = [];
                $POs[$variantSku]['qty'] = $countPO;
                $POs[$variantSku]['string'] = '<a href="'.$poUrl.'">'. explode(' ', $result['delivery_date'])[0].'</a>'
                    .' ('.$result['delivered_qty'].'/'.$result['qty'].')';
            }
        }

        if(count($POs)) {
            $manufacture .= '<strong>In Purchase Order:</strong> <br>';
            foreach ($POs as $key => $value) {
                $manufacture .= '<span style="margin-left: 30px">' . $key . ': ' . $value['string'] . '</span>';
                $manufacture .= '<br>';
            }
        }
        return $manufacture;
    }

    public function getNumberOfSoldPerReturn($months){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $currentTime = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $monthsString = '-' . $months . ' months';
        $lastXMonth = date('Y-m-d H:i:s', strtotime($monthsString, strtotime($currentTime)));

        $data = [];
        //item shipped
        $query = "SELECT sum(tsht_sales_flat_shipment_item.qty) as `count`, group_sku FROM `tsht_sales_flat_shipment_item` 
                  inner join tsht_sales_flat_shipment 
                  on tsht_sales_flat_shipment.entity_id=tsht_sales_flat_shipment_item.parent_id
                  join tsht_inventory_instock_product on tsht_inventory_instock_product.sku = tsht_sales_flat_shipment_item.sku
                  where group_sku in " . $this->skuStringForQuery . " and created_at > '" . $lastXMonth . "' group by group_sku";
        $results = $readConnection->fetchAll($query);
        foreach ($results as $result){
            $data[$result['group_sku']] = (int)$result['count'];
        }

        //item attached to order and not shipped
        $query = "SELECT count(*) as `count`, group_sku from tsht_inventory_order_item 
                  join tsht_inventory_item on tsht_inventory_order_item.item_id = tsht_inventory_item.item_id 
                  and tsht_inventory_item.state not in " . $this->shippedStates . "
                  join tsht_sales_flat_order_item 
                  on tsht_inventory_order_item.order_item_id = tsht_sales_flat_order_item.item_id
                  join tsht_inventory_instock_product on tsht_inventory_instock_product.sku = tsht_sales_flat_order_item.sku
                  where group_sku in " . $this->skuStringForQuery . "
                  and tsht_sales_flat_order_item.created_at > '" . $lastXMonth . "' group by group_sku";
        $results = $readConnection->fetchAll($query);
        foreach ($results as $result){
            if(isset($data[$result['group_sku']])) {
                $data[$result['group_sku']] += (int)$result['count'];
            }else{
                $data[$result['group_sku']] = (int)$result['count'];
            }
        }

        //get all return qty
        $query = "SELECT *, count(*) as `count` FROM `tsht_return_returns` 
                  join tsht_sales_flat_order_item on tsht_sales_flat_order_item.item_id = tsht_return_returns.order_item_id
                  join tsht_inventory_instock_product on tsht_sales_flat_order_item.sku = tsht_inventory_instock_product.sku
                  where tsht_return_returns.created_at > '" . $lastXMonth . "'
                  and solution in (5, 10, 15, 20) and group_sku in ".$this->skuStringForQuery ." group by group_sku";
        $results = $readConnection->fetchAll($query);
        foreach ($results as $result){
            if(isset($data[$result['group_sku']])) {
                $data[$result['group_sku']] = $data[$result['group_sku']].'/'.$result['count'];
            }else{
                $data[$result['group_sku']] = '0/'.$result['count'];
            }

        }

        //add 0 data to avoid warning
        foreach ($this->allSku as $groupSku){
            if(!isset($data[$groupSku])) $data[$groupSku] = '0/0';
            if(strpos($data[$groupSku], '/') == false){
                $data[$groupSku] = $data[$groupSku].'/0';
            }
        }

        return $data;
    }

    public function getMasterPOButtonHtml($productId){
        $html = '<div style="margin-left: auto;" class="add_to_master_po_btn_'.$productId.'">
                <button  id="'.$productId.'" itemId="'.$productId.'" onclick="getSupplierSelect(this);return false;">
                    Add to master PO
                </button>
            </div>';
        return $html;
    }

    public function getStockMovementUrl($group_sku){
        $filter = 'sku_variant='.$group_sku;
        $filter = base64_encode($filter);
        $url = Mage::helper('adminhtml')->getUrl('adminhtml/stockmovement/index', array('filter'=>$filter));

        return $url;
    }

    public function getInventoryUrl($group_sku){
        $filter = 'group_sku='.$group_sku;
        $filter = base64_encode($filter);
        $url = Mage::helper('adminhtml')->getUrl('adminhtml/instockProduct/index', array('filter'=>$filter));

        return $url;
    }
}