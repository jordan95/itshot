<?php

class OpenTechiz_ReportExtend_Model_StockBecomeZero extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_reportExtend/stockBecomeZero', 'id');
    }
}