<?php

class OpenTechiz_ReportExtend_Model_Profit extends Mage_Core_Model_Abstract
{
    public function getProfitData($params){
        $helper = Mage::helper('opentechiz_inventory');
        $dateFrom  = $params['from'];
        $dateTo    = $params['to'];
        $period   = $params['period'];
        $timeStructure = 'm/d/Y';

        $fromParts = explode('/', $dateFrom);
        $toParts = explode('/', $dateTo);
        if($period == 'month') {
            $fromParts[1] = '1';
            $toParts[1] = '31';
            $timeStructure = 'm/Y';
        }
        elseif ($period == 'year') {
            $fromParts[0] = '1';
            $toParts[0] = '12';
            $fromParts[1] = '1';
            $toParts[1] = '31';
            $timeStructure = 'Y';
        }

        $newFromParts = array();
        $newFromParts[2] = $fromParts[2];
        $newFromParts[1] = $fromParts[0];
        $newFromParts[0] = $fromParts[1];
        $dateFrom = implode('/', $newFromParts);
        $time =  strtotime($dateFrom);
        $fromNewFormat = date('Y-m-d H:i:s', strtotime("+4 hour", $time));

        $newToParts = array();
        $newToParts[2] = $toParts[2];
        $newToParts[1] = $toParts[0];
        $newToParts[0] = $toParts[1];

        $dateTo = implode('/', $newToParts);
        $time =  strtotime($dateTo);
        $toNewFormat = date('Y-m-d H:i:s', strtotime("+28 hour", $time));
        
        $itemModel = Mage::getModel('opentechiz_inventory/item');
        $itemCollection = $itemModel->getCollection()->join(array('inv_item' => 'opentechiz_inventory/order'), 'main_table.item_id = inv_item.item_id', '*');

//        foreach ($itemCollection as $item){
//            var_dump($item->getId() .' - '. $item->getOrderItemId());
//        }
//        exit;

        $itemCollection->join(array('order_item' => 'sales/order_item'), 'inv_item.order_item_id = order_item.item_id', 'order_item.order_id')
            ->join(array('order' => 'sales/order'), 'order_item.order_id = order.entity_id',
                array(
                    'created_at' => 'order.created_at',
                    'subtotal'   => 'order.subtotal',
                    'tax_amount' => 'order.tax_amount',
                    'shipping_amount'=> 'order.shipping_amount',
                    'discount_amount' => 'order.discount_amount'
                ))
            ->addFieldToFilter('order.created_at', array(
                'from' => $fromNewFormat,
                'to' => $toNewFormat
            ))
            ->getSelect()
            ->group('main_table.item_id');

        $returnTable = Mage::getSingleton('core/resource')->getTableName('opentechiz_return/return');
        $itemCollection->getSelect()->joinLeft(array('return'=>$returnTable), "main_table.barcode = return.item_barcode and return.solution != 0",
            array(
                'created_at' => 'order.created_at',
                'refund_amount' => 'return.refund_amount',
                'return_fee' => 'return.return_fee'
            ));

        $result = [];
        $orderData = [];
        $itemData = [];
        $total=[];
        $refund=[];
        $tax=[];
        $ship=[];
        $discount=[];
        $cost=[];

        $dateData = [];

        foreach ($itemCollection as $item) {
            $date = date($timeStructure, strtotime("-4 hour", strtotime($item->getData('created_at'))));

            if(!in_array($date, $dateData)){
                array_push($dateData, $date);
            }

            if (!array_key_exists($date, $itemData)) {
                $itemData[$date] = [];
                $orderData[$date] = [];
                $total[$date] = 0;
                $refund[$date] = 0;
                $tax[$date] = 0;
                $ship[$date] = 0;
                $discount[$date] = 0;
                $cost[$date] = 0;
            }
            $cost[$date] += $helper->getLastCost($item->getSku());
            $refund[$date] += $item->getData('refund_amount') - $item->getData('return_fee');

            if(!in_array($item->getData('order_id'), $orderData[$date])){
                array_push($orderData[$date], $item->getData('order_id'));
                $total[$date] += $item->getData('subtotal');
                $tax[$date] += $item->getData('tax_amount');
                $ship[$date] += $item->getData('shipping_amount');
                $discount[$date] += -$item->getData('discount_amount');

            }

            if(!in_array($item->getData('order_item_id'), $itemData[$date])){
                array_push($itemData[$date], $item->getData('order_item_id'));
            }
        }

        foreach ($dateData as $day) {

            $result[$day] = [];
            $result[$day]['order'] = count($orderData[$day]);
            $result[$day]['item'] = count($itemData[$day]);
            $result[$day]['total'] = $total[$day];
            $result[$day]['refund'] = $refund[$day];
            $result[$day]['tax'] = $tax[$day];
            $result[$day]['ship'] = $ship[$day];
            $result[$day]['discount'] = $discount[$day];
            $result[$day]['cost'] = $cost[$day];
            $result[$day]['profit'] = $total[$day] - $refund[$day] - $tax[$day] - $ship[$day] - $discount[$day] - $cost[$day];
        }
        ksort($result);
        return $result;
    }
}