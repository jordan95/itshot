<?php

class OpenTechiz_ReportExtend_Model_OldInvoice extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_reportExtend/oldInvoice', 'old_invoice_id');
    }
}