<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */
class OpenTechiz_ReportExtend_Model_Cron
{

    public function sendDailySalesReportEmail()
    {
        //cronjob to get template from adminhtml
        $storeId = 0;

        $initialEnvironmentInfo = Mage::getSingleton('core/app_emulation')->startEnvironmentEmulation(
            $storeId, Mage_Core_Model_App_Area::AREA_ADMINHTML);
        $this->setAdminStoreForTemplateInCronjob();

        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/general/daily_sales'));

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('daily_sales_tpl');
        $emailTemplate->setTemplateSubject('DailySales Email ('.Mage::helper('core')->formatDate().')');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();

        foreach ($to as $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send($recipientEmail, $emailTemplateVariables);
            }
        }

        Mage::getSingleton('core/app_emulation')->stopEnvironmentEmulation($initialEnvironmentInfo);
    }

    public function sendWeeklySalesReportEmail()
    {
        //cronjob to get template from adminhtml
        $storeId = 0;

        $initialEnvironmentInfo = Mage::getSingleton('core/app_emulation')->startEnvironmentEmulation(
            $storeId, Mage_Core_Model_App_Area::AREA_ADMINHTML);
        $this->setAdminStoreForTemplateInCronjob();

        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/general/weekly_sales'));

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('weekly_sales_tpl');
        $emailTemplate->setTemplateSubject('WeeklySales Email ('.Mage::helper('core')->formatDate().')');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();

        foreach ($to as $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send($recipientEmail, $emailTemplateVariables);
            }
        }

        Mage::getSingleton('core/app_emulation')->stopEnvironmentEmulation($initialEnvironmentInfo);
    }

    public function sendWeeklySalesComparisonReportEmail(){
        $coreFlagCode = 'weekly_sales_comparison_report';
        $flag = Mage::getModel('core/flag', array('flag_code' => $coreFlagCode))->loadSelf();

        if($flag && $flag->getFlagData()){
            $last = $flag->getFlagData();
            $last = date ('Y-m-d', strtotime($last));
        }else{
            $last = Mage::getModel('core/date')->gmtDate('Y-m-d');
            $last = date ('Y-m-d', strtotime('-144 hour', strtotime($last)));
        }

        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/general/weekly_sales'));

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('weekly_sales_comparison_tpl');
        $emailTemplate->setTemplateSubject('Weekly Sales Comparison Email ('.Mage::helper('core')->formatDate().')');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();

        foreach ($to as $recipientEmail) {
            $recipientEmail = trim($recipientEmail);
            if ($recipientEmail) {
                Mage::register('dateLastWeekSaleComparisonReport', $last);
                $emailTemplate->send($recipientEmail, $emailTemplateVariables);
                Mage::unregister('dateLastWeekSaleComparisonReport');
            }
        }

        //set flag data
        $flagData = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $flag->setFlagData($flagData)->save();
    }

    public function sendMonthlySalesReportEmail()
    {
        //cronjob to get template from adminhtml
        $storeId = 0;

        $initialEnvironmentInfo = Mage::getSingleton('core/app_emulation')->startEnvironmentEmulation(
            $storeId, Mage_Core_Model_App_Area::AREA_ADMINHTML);
        $this->setAdminStoreForTemplateInCronjob();

        $coreFlagCode = 'monthly_sales_report';
        $flag = Mage::getModel('core/flag', array('flag_code' => $coreFlagCode))->loadSelf();

        if($flag && $flag->getFlagData()){
            $started = $flag->getFlagData();
        }else{
            $started = Mage::getModel('core/date')->gmtDate('Y-m-d').' 00:00:00';
        }
        $current_month = date('m');
        $current_year = date('Y');

        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/general/monthly_sales'));

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('monthly_sales_tpl');
        $emailTemplate->setTemplateSubject('Monthly Sales Email ('.Mage::helper('core')->formatDate().')');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();

        foreach ($to as $recipientEmail) {
            $recipientEmail = trim($recipientEmail);
            if ($recipientEmail) {
                $lastSentMonth = date('m', strtotime($started));
                $lastSentyear = date('Y', strtotime($started));
                while($lastSentyear < $current_year || $lastSentMonth < $current_month) {
                    if($lastSentMonth == 12){
                        $lastSentMonth = 1;
                        $lastSentyear++;
                    }else{
                        $lastSentMonth++;
                    }
                    $startedDate = $lastSentyear.'-'.$lastSentMonth.'-'.'01 00:00:00';
                    
                    Mage::register('dateStartedMonthlySaleReport', $startedDate);
                    $emailTemplate->send($recipientEmail, $emailTemplateVariables);
                    Mage::unregister('dateStartedMonthlySaleReport');
                }
            }
        }
        
        //set flag data
        $flagData = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $flag->setFlagData($flagData)->save();

        Mage::getSingleton('core/app_emulation')->stopEnvironmentEmulation($initialEnvironmentInfo);
    }

    public function sendDailySoldReportEmail()
    {
        //cronjob to get template from adminhtml
        $storeId = 0;

        $initialEnvironmentInfo = Mage::getSingleton('core/app_emulation')->startEnvironmentEmulation(
            $storeId, Mage_Core_Model_App_Area::AREA_ADMINHTML);
        $this->setAdminStoreForTemplateInCronjob();

        $coreFlagCode = 'stock_become_zero_report';
        $flag = Mage::getModel('core/flag', array('flag_code' => $coreFlagCode))->loadSelf();
        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/general/daily_sold'));

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('daily_sold_tpl');
        $emailTemplate->setTemplateSubject('DailySold Email ('.Mage::helper('core')->formatDate().')');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();

        foreach ($to as $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send($recipientEmail, $emailTemplateVariables);
            }
        }
        //set flag data
        $flagData = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $flag->setFlagData($flagData)->save();

        Mage::getSingleton('core/app_emulation')->stopEnvironmentEmulation($initialEnvironmentInfo);
    }

    public function sendClearanceSaleReportEmail()
    {
        $coreFlagCode = 'clearance_sale_report';
        $flag = Mage::getModel('core/flag', array('flag_code' => $coreFlagCode))->loadSelf();
        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/general/clearance_sale'));

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('clearance_sale_tpl');
        $emailTemplate->setTemplateSubject('Clearance Sale Zero in Stock report Email ('.Mage::helper('core')->formatDate().')');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();

        foreach ($to as $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send($recipientEmail, $emailTemplateVariables);
            }
        }
        //set flag data
        $flagData = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $flag->setFlagData($flagData)->save();
    }

    public function sendDailyReturnReportEmail()
    {
        //cronjob to get template from adminhtml
        $storeId = 0;

        $initialEnvironmentInfo = Mage::getSingleton('core/app_emulation')->startEnvironmentEmulation(
            $storeId, Mage_Core_Model_App_Area::AREA_ADMINHTML);
        $this->setAdminStoreForTemplateInCronjob();

        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/general/daily_return'));

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('daily_return_tpl');
        $emailTemplate->setTemplateSubject('DailyReturn Email ('.Mage::helper('core')->formatDate().')');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();

        foreach ($to as $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send($recipientEmail, $emailTemplateVariables);
            }
        }

        Mage::getSingleton('core/app_emulation')->stopEnvironmentEmulation($initialEnvironmentInfo);
    }

    public function checkStockBecomeMoreThanZeroAndSendMail()
    {
        $coreFlagCode = 'stock_become__more_than_zero_report';
        $flag = Mage::getModel('core/flag', array('flag_code' => $coreFlagCode))->loadSelf();
        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/general/more_than_zero'));

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('stock_become_more_than_zero_tpl');
        $emailTemplate->setTemplateSubject('Stock Become More Than Zero Email ('.Mage::helper('core')->formatDate().')');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();

        foreach ($to as $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send($recipientEmail, $emailTemplateVariables);
            }
        }
        //set flag data
        $flagData = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $flag->setFlagData($flagData)->save();
    }

    public function sendLatePurchaseOrderEmail()
    {
        //cronjob to get template from adminhtml
        $storeId = 0;

        $initialEnvironmentInfo = Mage::getSingleton('core/app_emulation')->startEnvironmentEmulation(
            $storeId, Mage_Core_Model_App_Area::AREA_ADMINHTML);
        $this->setAdminStoreForTemplateInCronjob();

        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $to = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/general/late_purchase'));

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('late_po_tpl');
        $emailTemplate->setTemplateSubject('Late Purchase Order ('.Mage::helper('core')->formatDate().')');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();

        foreach ($to as $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send($recipientEmail, $emailTemplateVariables);
            }
        }

        Mage::getSingleton('core/app_emulation')->stopEnvironmentEmulation($initialEnvironmentInfo);
    }

    public function sendOrderProgressReportEmail()
    {
        //cronjob to get template from adminhtml
        $storeId = 0;

        $initialEnvironmentInfo = Mage::getSingleton('core/app_emulation')->startEnvironmentEmulation(
            $storeId, Mage_Core_Model_App_Area::AREA_ADMINHTML);
        $this->setAdminStoreForTemplateInCronjob();

        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        $array = ['order_progress_manufacturing_tpl' => 'Manufacturing',
            'order_progress_waitonvendor_tpl' => 'Waiting on Vendor',
            'order_progress_manufacturing_over7day_tpl' => 'Manufacturing over 7 days'];
        foreach ($array as $templateName => $emailName) {
            $emailTemplate = Mage::getModel('core/email_template');
            $emailTemplate->loadDefault($templateName);
            $emailTemplate->setTemplateSubject('Order Progress ' . $emailName . ' Report Email ('.Mage::helper('core')->formatDate().')');

            $emailTemplate->setSenderName($senderName);
            $emailTemplate->setSenderEmail($senderEmail);

            // Getting recipient E-Mail
            $to = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/general/'.$templateName));

            $emailTemplateVariables = array();

            foreach ($to as $recipientEmail) {
                if ($recipientEmail) {
                    $emailTemplate->send($recipientEmail, $emailTemplateVariables);
                }
            }
        }

        Mage::getSingleton('core/app_emulation')->stopEnvironmentEmulation($initialEnvironmentInfo);
    }

    public function mergePOItemOItem()
    {
        Mage::getModel('core/config')->saveConfig('opentechiz_reportExtend/product_history/last_updated_at', date("Y-m-d H:i:s"));
        $table_prefix = (string) Mage::getConfig()->getTablePrefix();
        /* @var $readAdapter Aoe_DbRetry_Resource_Db_Pdo_Mysql_Adapter */
        $readAdapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select = $readAdapter->select();
        $select->from(array('ph' => $table_prefix . 'product_history_report'));
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->columns(array("transaction_date"));
        $select->order(array("transaction_date  DESC"));
        $result = $readAdapter->fetchRow($select);
        $transaction_date = isset($result['transaction_date']) ? $result['transaction_date'] : date('Y-m-d H:i:s');
        $readAdapter->query("
            INSERT INTO `tsht_product_history_report`(`product_history_id`, `transaction_date`,`sku`,`name`,`po_increment_id`, `option`, `vendor_name`, `delivered_qty`, `price_paid`, `customer_name`, `item_price`, `order_id`, `increment_id`, `qty_ordered`) 
            SELECT null as `product_history_id`, `po`.`delivery_date` AS `transaction_date`, `po_item`.`sku`, `attr_pname`.`value` AS `name`, `po`.`po_increment_id`, `po_item`.`option`, `po_supplier`.`name` AS `vendor_name`, `po_item`.`delivered_qty`, `po_item`.`price` AS `price_paid`, null AS `customer_name`, null AS `item_price`, null AS `order_id`, null AS `increment_id`, null AS `qty_ordered` FROM `tsht_purchase_order` AS `po`
            LEFT JOIN `tsht_purchase_order_item` AS `po_item` ON po.po_id = po_item.po_id
            LEFT JOIN `tsht_purchase_supplier` AS `po_supplier` ON po.supplier_id = po_supplier.supplier_id
            INNER JOIN `tsht_catalog_product_entity_varchar` AS `attr_pname` ON attr_pname.entity_id = po_item.product_id and attr_pname.attribute_id = 71 and attr_pname.entity_type_id = 4 and attr_pname.store_id = 0
            LEFT JOIN `tsht_sales_flat_order_item` AS `order_item` ON order_item.created_at = po.created_at
            WHERE `po`.`delivery_date` > ? AND `po`.`delivery_date` <= CURRENT_TIMESTAMP AND `po_item`.`delivered_qty` > 0
            UNION ALL
            SELECT null as `product_history_id`, `o`.`created_at` AS `transaction_date`, `order_item`.`sku`, `order_item`.`name`, null AS `po_increment_id`, `order_item`.`product_options` AS `option`, null AS `vendor_name`, null AS `delivered_qty`, null AS `price_paid`, CONCAT(o.customer_firstname,' ' ,o.customer_lastname) AS `customer_name`, `order_item`.`price` AS `item_price`, `o`.`entity_id` AS `order_id`, `o`.`increment_id`, `order_item`.`qty_ordered` FROM `tsht_sales_flat_order` AS `o` 
            LEFT JOIN `tsht_sales_flat_order_item` AS `order_item` ON o.entity_id = order_item.order_id
            WHERE `o`.`created_at` > ? AND `o`.`state` != 'canceled'
            ", array($transaction_date, $transaction_date));
    }
    public function sendLatePOOrderPerSupplierEmail()
    {
        //cronjob to get template from adminhtml
        $storeId = 0;

        $initialEnvironmentInfo = Mage::getSingleton('core/app_emulation')->startEnvironmentEmulation(
            $storeId, Mage_Core_Model_App_Area::AREA_ADMINHTML);
        $this->setAdminStoreForTemplateInCronjob();

        //Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $replyEmail = trim(Mage::getStoreConfig('opentechiz_reportExtend/general/late_purchase_reply_to'));
        $recipientEmail = Mage::getStoreConfig('opentechiz_reportExtend/general/late_purchase_per_supplier');
        $list_supplier = explode(',', Mage::getStoreConfig('opentechiz_reportExtend/listsupplier/supplier'));
        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('late_po_per_supplier_tpl');
        

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);
        $emailTemplateVariables = array();
        $email_supplier = '';
        if(!empty($list_supplier)){
            foreach($list_supplier as $supplier_id){
                $listEmail = Mage::helper("opentechiz_reportExtend")->validatedEmailList($recipientEmail);
                $late_purchase = Mage::getModel('opentechiz_purchase/purchase')->getCollection()->hasLatePurchasePerSupplier($supplier_id);
                if($late_purchase->count() > 0){

                    foreach($late_purchase as $item){
                            $email_supplier = $item->getEmail();
                            $emailTemplateVariables['supplier_id'] = $item->getSupplierId();
                            $emailTemplate->setTemplateSubject('['.$item->getName().'] Late Purchase Orders ('.Mage::helper('core')->formatDate().')');
                            break;
                    }
                    if($email_supplier !=''){
                        $listEmail = Mage::helper("opentechiz_reportExtend")->validatedEmailList($email_supplier,$listEmail);
                        $listEmail = array_unique($listEmail);
                    }
                    foreach ($listEmail as $email) {
                        if ($email) {
                            if($replyEmail !=''){
                                $emailTemplate->setReplyTo($replyEmail);
                            }
                            $emailTemplate->send($email,'', $emailTemplateVariables);
                        }
                    }
                    
                }
            }
            
        }

        Mage::getSingleton('core/app_emulation')->stopEnvironmentEmulation($initialEnvironmentInfo);
    }
   
    public function addPaymentRecordRefundOrder()
    {
        $collection = Mage::getModel("sales/order_creditmemo")->getCollection()
            ->addFieldToSelect('entity_id')
            ->addFieldToSelect('state')
            ->addFieldToSelect('base_grand_total')
            ->addFieldToFilter('main_table.state',array("eq"=>1))
            ->join(array('sales_order' => 'sales/order'),
                'main_table.order_id = sales_order.entity_id',
                array('sales_order.entity_id as order_id'))
            ->addFieldToFilter('sales_order.source',array("in"=>OpenTechiz_SalesExtend_Helper_Data::MARKETPLACE_ORDER_SOURCE));
        if($collection){  
            try{
                foreach($collection as $creditmemo){
                    $data = [];
                    $data['order_id'] = $creditmemo->getOrderId();
                    $data['payment_id'] = 7;
                    $data['number'] = "add payment by cronjob for marketplace order";
                    $data['total'] = (-1)*abs($creditmemo->getBaseGrandTotal());
                    $data['billing_name'] = $creditmemo->getBillingAddress()->getName();
                    $data['email'] = $creditmemo->getBillingAddress()->getEmail();
                    $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                    $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                    $data['received_date'] = Mage::getModel('core/date')->gmtDate('Y-m-d');
                    $data['credit_memo_id'] = $creditmemo->getId();
                    $data['type'] = "creditmemo";
                    $paymentRecord = Mage::getModel('cod/paymentgrid');
                    $paymentRecord->setData($data)->save();
                }
            }catch(\Exception $ex){
                Mage::logException($ex);
            }   
        }
    }

    public function automaticReorderingReport()
    {
        Mage::helper('opentechiz_reportExtend/automaticReordering')->createAiReorderingReport();
        Mage::helper('opentechiz_reportExtend/automaticReordering')->sendNotyEmail();
    }

    public function setAdminStoreForTemplateInCronjob(){
        $storeId = 0;
        Mage::getDesign()->setStore($storeId);
        Mage::getDesign()->setTheme((string)Mage::getConfig()->getNode("stores/admin/design/theme/default"));
    }

    public function sendReportOfRepairEmail(){
        try {
             // Set sender information
            $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
            $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

            // Getting recipient E-Mail
            $recipientEmail = Mage::getStoreConfig('opentechiz_reportExtend/general/report_of_repair');

            $collection = Mage::getModel('sales/order')->getCollection()
                ->join(array('log' => 'opentechiz_reportExtend/log_orderStatus'), 'log.id = main_table.last_history_id', '*')
                ->addFieldToFilter('main_table.order_type', array('eq' => 2))
                ->addFieldToFilter('main_table.order_stage', array('neq' => OpenTechiz_SalesExtend_Helper_Data::SHIPPED));
            Mage::getModel('opentechiz_salesextend/resource_order_filter')->filterActiveOrder($collection);
            $collection->getSelect()->where("(log.updated_at + INTERVAL 7 DAY) < DATE(NOW())");
            $collection->getSelect()->order('main_table.created_at ASC');

            $orders = $collection->getItems();

            $emailTemplate = Mage::getModel('core/email_template');
            $emailTemplate->loadDefault('report_of_repairs');
            $emailTemplate->setTemplateSubject('Report of Repairs ('.Mage::helper('core')->formatDate().')');

            $emailTemplate->setSenderName($senderName);
            $emailTemplate->setSenderEmail($senderEmail);

            if (count($orders)) {
                $emailTemplateVariables = ['orders' => $orders];
                $listEmail = Mage::helper("opentechiz_reportExtend")->validatedEmailList($recipientEmail);
                foreach ($listEmail as $email) {
                    if ($email) {
                        $emailTemplate->send(trim($email), null, $emailTemplateVariables);
                    }
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}
