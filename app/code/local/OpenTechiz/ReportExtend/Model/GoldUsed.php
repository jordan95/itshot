<?php

class OpenTechiz_ReportExtend_Model_GoldUsed extends Mage_Core_Model_Abstract
{
    const GOLD_NAMES = OpenTechiz_Material_Helper_Data::BASE_GOLD_COLOR;
    public function getGoldUsedData($params){
        $dateFrom  = $params['from'];
        $dateTo    = $params['to'];

        $time =  strtotime($dateFrom);
        $fromNewFormat = date('Y-m-d H:i:s', strtotime("+4 hour", $time));

        $time =  strtotime($dateTo);
        $toNewFormat = date('Y-m-d H:i:s',strtotime("+28 hour", $time));

        $collection = Mage::getModel('opentechiz_production/production_history')->getCollection()
            ->addFieldToFilter('main_table.step_name', array('in' => [OpenTechiz_Production_Helper_Data::PROCESS_LIST[5], OpenTechiz_Production_Helper_Data::PROCESS_LIST[45]]));
        $collection->join(array('request' => 'opentechiz_production/product'), 'main_table.personalized_id = request.personalized_id');

        $collection->addFieldToFilter('request.finished_at', array(
            'from' => $fromNewFormat,
            'to' => $toNewFormat
        ));

        if(count($collection) == 0){
            return [];
        }
        $collection->getSelect()->joinLeft(array('item' => 'tsht_sales_flat_order_item'), 'request.order_item_id = item.item_id')
            ->joinLeft(array('order' => 'tsht_sales_flat_order'), 'item.order_id = order.entity_id',array('order'=>'order.increment_id'))
            ->joinLeft(array('manufacture' => 'tsht_production_manufacture_product'), 'request.manufacture_id = manufacture.id', array('manufacture'=>'manufacture.id', 'manu_sku' => 'manufacture.sku', 'manu_name' => 'manufacture.name'));

        $result = [];
        $array = [];
        $data = $collection->getData();
        foreach ($data as $item) {
            if(!in_array($item['personalized_id'], $array)) {
                $result[$item['personalized_id']] = [];
                array_push($array,$item['personalized_id']);
            }

            if($item['order_item_id']){
                $orderItem = Mage::getModel('sales/order_item')->load($item['order_item_id']);
                $sku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $orderItem->getSku());
                $fullSku = $orderItem->getSku();
            }else{
                $manufacture = Mage::getModel('opentechiz_production/production_manufacture')->load($item['manufacture_id']);
                $sku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $manufacture->getSku());
                $fullSku = $manufacture->getSku();
            }

            if(!$fullSku) continue;
            $baseSku = $sku[0];
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $baseSku);
            $product->setSku($fullSku);
            $value = Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(120, 120)->__toString();

            $result[$item['personalized_id']]['image'] = $value;
            $time =  Mage::helper('core')->formatDate($item['finished_at']);
            $result[$item['personalized_id']]['date'] = $time;

            if(strpos($fullSku, 'quotation') === false) {
                $hasGold = false;
                foreach ($sku as $skuPart){
                    if(in_array($skuPart, self::GOLD_NAMES)){
                        $result[$item['personalized_id']]['gold'] = $skuPart;
                        $hasGold = true;
                    }
                }
                if(!$hasGold) continue;
            } else{
                $quotationProductId = $sku[count($sku) - 1];
                $goldId = Mage::getModel('opentechiz_quotation/productOption')->getCollection()
                    ->addFieldToFilter('quote_product_id', $quotationProductId)
                    ->addFieldToFilter('option_type', 'gold')
                    ->getFirstItem()->getOptionValue();
                $gold = Mage::getModel('opentechiz_material/gold')->load($goldId);
                $result[$item['personalized_id']]['gold'] = $gold->getGoldType().$gold->getGoldColor();
            }
            if(!$item['order'] || $item['order'] == null){
                $result[$item['personalized_id']]['order'] = '';
                $result[$item['personalized_id']]['product'] = $item['manu_name'];
                $result[$item['personalized_id']]['manufacture'] = $item['manufacture'];
                if(!$item['manufacture'] || $item['manufacture'] == null){
                    $option_data = unserialize($item['option']);
                    $product_id = $option_data['info_buyRequest']['product'];
                    foreach ($option_data['options'] as $key => $option) {
                        # code...
                        if($option['option_type']=='gold'){
                            $gold = $option['print_value'];
                            break;
                        }
                    }
                    $product = Mage::getModel('catalog/product')->load($product_id);
                    if($product->getId())
                        $result[$item['personalized_id']]['product'] = Mage::getModel('catalog/product')->load($product_id)->getName();
                    $result[$item['personalized_id']]['manufacture'] = '';
                    $result[$item['personalized_id']]['gold'] = $gold;
                }
            } else {
                $result[$item['personalized_id']]['order'] = $item['order'];
                $result[$item['personalized_id']]['manufacture'] = '';
                $result[$item['personalized_id']]['product'] = $item['name'];
            }

            if($item['step_name'] == OpenTechiz_Production_Helper_Data::PROCESS_LIST[5]){
                $result[$item['personalized_id']]['casting'] = $item['gold_weight'];
            }
            if($item['step_name'] == OpenTechiz_Production_Helper_Data::PROCESS_LIST[45]){
                $result[$item['personalized_id']]['finish'] = $item['gold_weight'];
            }
        }
        return $result;
    }
}