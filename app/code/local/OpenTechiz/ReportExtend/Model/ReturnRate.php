<?php

class OpenTechiz_ReportExtend_Model_ReturnRate extends Mage_Core_Model_Abstract
{
    public function getReturnRateData($params){
        $dateFrom  = $params['from'];
        $dateTo    = $params['to'];
        $period   = $params['period'];
        $timeStructure = 'm/d/Y';
        $returnType = $params['return-type'];

        $fromParts = explode('/', $dateFrom);
        $toParts = explode('/', $dateTo);
        if($period == 'month') {
            $fromParts[1] = '1';
            $toParts[1] = '31';
            $timeStructure = 'm/Y';
        }
        elseif ($period == 'year') {
            $fromParts[1] = '1';
            $toParts[1] = '31';
            $fromParts[0] = '1';
            $toParts[0] = '12';
            $timeStructure = 'Y';
        }
        
        $dateFrom = implode('/', $fromParts);
        $time =  strtotime($dateFrom);
        $fromNewFormat = date('Y-m-d H:i:s',  strtotime("+4 hour", $time));

        $dateTo = implode('/', $toParts);
        $time =  strtotime($dateTo);
        $toNewFormat = date('Y-m-d H:i:s', strtotime("+28 hour", $time));

        $orderModel = Mage::getModel('sales/order');
        $orderCollection = $orderModel->getCollection()->addFieldToFilter('created_at', array(
            'from' => $fromNewFormat,
            'to' => $toNewFormat
        ));

        $itemCollection = Mage::getModel('sales/order_item')->getCollection()->addFieldToFilter('created_at', array(
            'from' => $fromNewFormat,
            'to' => $toNewFormat
        ));

        $returnCollection = Mage::getModel('opentechiz_return/return')->getCollection()->addFieldToFilter('solution', $returnType)
            ->addFieldToFilter('created_at', array(
                'from' => $fromNewFormat,
                'to' => $toNewFormat
            ));

        $result = [];
        $orderData = [];
        $itemData = [];
        $returnData = [];

        foreach ($orderCollection as $order) {
            $date = date($timeStructure, strtotime("-4 hour", strtotime($order->getCreatedAt())));

            if (!array_key_exists($date, $orderData)) {
                $orderData[$date] = [];
            }
            array_push($orderData[$date], $order->getData());
        }
        foreach ($itemCollection as $item) {
            $date = date($timeStructure, strtotime("-4 hour", strtotime($item->getCreatedAt())));

            if (!array_key_exists($date, $itemData)) {
                $itemData[$date] = [];
            }
            array_push($itemData[$date], $item->getData());
        }
        foreach ($returnCollection as $return) {
            $date = date($timeStructure, strtotime("-4 hour", strtotime($return->getCreatedAt())));

            if (!array_key_exists($date, $returnData)) {
                $returnData[$date] = [];
            }
            array_push($returnData[$date], $return->getData());
        }

        $interval = DateInterval::createFromDateString('1 day');

        $time = strtotime($fromNewFormat);
        $convertedFromNewFormat = date('Y-m-d',  strtotime("-4 hour", $time));
        $time = strtotime($toNewFormat);
        $convertedToNewFormat = date('Y-m-d',  strtotime("-4 hour", $time));

        $period = new DatePeriod(new DateTime($convertedFromNewFormat), $interval, new DateTime($convertedToNewFormat));

        foreach ($period as $day) {
            $day = $day->format($timeStructure);
            if(!isset($orderData[$day])) $orderData[$day] = [];
            if(!isset($itemData[$day])) $itemData[$day] = [];
            if(!isset($returnData[$day])) $returnData[$day] = [];

            $result[$day] = [];
            $result[$day]['order'] = count($orderData[$day]);
            $result[$day]['item'] = count($itemData[$day]);
            $result[$day]['return'] = count($returnData[$day]);
            if($result[$day]['item'] > 0) {
                $result[$day]['rate'] = (float)count($returnData[$day]) / count($itemData[$day]);
            } else $result[$day]['rate'] = 0;
        }

        return $result;
    }
}