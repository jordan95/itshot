<?php

class OpenTechiz_ReportExtend_Model_OldOrderFailImport extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_reportExtend/oldOrderFailImport', 'id');
    }
}