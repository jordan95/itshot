<?php

class OpenTechiz_ReportExtend_Model_BrokeProduct extends Mage_Core_Model_Abstract
{
    public function getBrokeData($params){
        $dateFrom  = $params['from'];
        $dateTo    = $params['to'];
        $period   = $params['period'];

        $fromParts = explode('/', $dateFrom);
        $toParts = explode('/', $dateTo);
        if($period == 'month') {
            $fromParts[0] = '1';
            $toParts[0] = '31';
        }
        elseif ($period == 'year') {
            $fromParts[0] = '1';
            $toParts[0] = '31';
            $fromParts[1] = '1';
            $toParts[1] = '12';
        }

        krsort($fromParts);
        $dateFrom = implode('/', $fromParts);
        $time =  strtotime($dateFrom);
        $fromNewFormat = date('Y-m-d H:i:s', strtotime("+4 hour", $time));

        krsort($toParts);
        $dateTo = implode('/', $toParts);
        $time =  strtotime($dateTo);
        $toNewFormat = date('Y-m-d H:i:s',strtotime("+28 hour", $time));

        $historyModel = Mage::getModel('opentechiz_production/production_history');
        $collection = $historyModel->getCollection()->addFieldToFilter('finished_at', array(
            'from' => $fromNewFormat,
            'to' => $toNewFormat
        ));


        $result = [];
        $finishData = [];
        $brokeData = [];

        $finishedCollection = $collection->addFieldToFilter('step_name', OpenTechiz_Production_Helper_Data::PROCESS_LIST[45])
            ->addFieldToFilter('status', (int)0)->setOrder('finished_at', 'ASC');

        $brokeCollection = $historyModel->getCollection()->addFieldToFilter('finished_at', array(
            'from' => $fromNewFormat,
            'to' => $toNewFormat
        ))->addFieldToFilter('status', (int)1)->setOrder('finished_at', 'ASC');

        if($period == 'day') {

            foreach ($finishedCollection as $history) {
                $date = date('m/d/Y', strtotime("-4 hour", strtotime($history->getFinishedAt())));

                if (!array_key_exists($date, $finishData)) {
                    $finishData[$date] = [];
                }
                array_push($finishData[$date], $history->getData());
            }
            foreach ($brokeCollection as $history) {
                $date = date('m/d/Y', strtotime("-4 hour", strtotime($history->getFinishedAt())));
                if (!array_key_exists($date, $brokeData)) {
                    $brokeData[$date] = [];
                }
                array_push($brokeData[$date], $history->getData());
            }
        } elseif($period == 'month'){
            foreach ($finishedCollection as $history) {
                $date = date('m/Y', strtotime("-4 hour", strtotime($history->getFinishedAt())));
                if (!array_key_exists($date, $finishData)) {
                    $finishData[$date] = [];
                }
                array_push($finishData[$date], $history->getData());
            }
            foreach ($brokeCollection as $history) {
                $date = date('m/Y', strtotime("-4 hour", strtotime($history->getFinishedAt())));
                if (!array_key_exists($date, $brokeData)) {
                    $brokeData[$date] = [];
                }
                array_push($brokeData[$date], $history->getData());
            }
        } elseif($period == 'year'){
            foreach ($finishedCollection as $history) {
                $date = date('Y', strtotime("+5 hour", strtotime($history->getFinishedAt())));
                if (!array_key_exists($date, $finishData)) {
                    $finishData[$date] = [];
                }
                array_push($finishData[$date], $history->getData());
            }
            foreach ($brokeCollection as $history) {
                $date = date('Y', strtotime("-4 hour", strtotime($history->getFinishedAt())));
                if (!array_key_exists($date, $brokeData)) {
                    $brokeData[$date] = [];
                }
                array_push($brokeData[$date], $history->getData());
            }
        }
        foreach ($finishData as $day => $steps) {

            $result[$day] = [];
            $result[$day]['process'] = count($finishData[$day]);
            $result[$day]['broke'] = count($brokeData[$day]);
        }
        return $result;
    }
}