<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_LogStock_Renderer_Qty extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $Qty = $row->getData('qty');

        if(strpos($Qty, '-') !== false){
            $color = 'red';
        } elseif(strpos($Qty, '+') !== false){
            $color = 'green';
        } else{
            $color = 'black';
        }

        return '<span style="color: '.$color.'">'.$Qty.'</span>';
    }
}