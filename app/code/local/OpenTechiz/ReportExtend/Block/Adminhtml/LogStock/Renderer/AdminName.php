<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_LogStock_Renderer_AdminName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $adminId = $row->getData('user_id');

        $userName = Mage::getModel('admin/user')->load($adminId)->getFirstname(). ' ' .Mage::getModel('admin/user')->load($adminId)->getLastname();

        return $userName;
    }
}