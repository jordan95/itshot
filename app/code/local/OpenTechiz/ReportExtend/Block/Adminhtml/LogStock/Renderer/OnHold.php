<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_LogStock_Renderer_OnHold extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $onHold = $row->getData('on_hold');

        if (strpos($onHold, '+') !== false) {
            $color = 'red';
        } elseif(strpos($onHold, '-') !== false){
            $color = 'green';
        } else{
            $color = 'black';
        }

        return '<span style="color: '.$color.'">'.$onHold.'</span>';
    }
}