<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_LogStock_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('stockMovementGrid');
        $this->setDefaultSort('stock_movement_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_reportExtend/log_stock')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('stock_movement_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('ID'),
            'index'     => 'stock_movement_id'
        ));
        $this->addColumn('type', array(
            'header'    => Mage::helper('opentechiz_production')->__('Type'),
            'index'     => 'type',
            'width'     =>  '100',
            'type'      => 'options',
            'options'   => OpenTechiz_ReportExtend_Helper_Data::STOCK_MOVEMENT_TYPE
        ));
        $this->addColumn('stock_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Material/Stock ID'),
            'index'     => 'stock_id',
            'width'     =>  '350',
        ));
        $this->addColumn('qty', array(
            'header'    => Mage::helper('opentechiz_production')->__('Quantity changed'),
            'index'     => 'qty',
            'width'     =>  '100',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_LogStock_Renderer_Qty'
        ));
        $this->addColumn('on_hold', array(
            'header'    => Mage::helper('opentechiz_production')->__('On Hold changed'),
            'index'     => 'on_hold',
            'width'     =>  '100',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_LogStock_Renderer_OnHold'
        ));
        $this->addColumn('description', array(
            'header'    => Mage::helper('opentechiz_production')->__('Description'),
            'index'     => 'description',
            'width'     =>  '600'
        ));
        $this->addColumn('user_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('User'),
            'index'     => 'user_id',
            'width'     =>  '250',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_LogStock_Renderer_AdminName'
        ));
        $this->addColumn('timestamp', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('TimeStamp'),
            'type'      => 'datetime',
            'index'     =>  'timestamp',
            'width'     =>  '250'
        ));
        return parent::_prepareColumns();
    }

}