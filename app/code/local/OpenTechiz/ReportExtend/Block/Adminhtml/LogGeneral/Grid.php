<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_LogGeneral_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('generalLogGrid');
        $this->setDefaultSort('general_action_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_reportExtend/log_general')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('general_action_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('ID'),
            'index'     => 'general_action_id'
        ));
        $this->addColumn('user_name', array(
            'header'    => Mage::helper('opentechiz_production')->__('Name'),
            'index'     => 'user_name',
            'width'     =>  '350'
        ));
        $this->addColumn('user_email', array(
            'header'    => Mage::helper('opentechiz_production')->__('Email'),
            'index'     => 'user_email',
            'width'     =>  '350',
        ));
        $this->addColumn('action', array(
            'header'    => Mage::helper('opentechiz_production')->__('Action'),
            'index'     => 'action',
            'width'     =>  '650'
        ));
        $this->addColumn('timestamp', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('TimeStamp'),
            'type'      => 'datetime',
            'index'     =>  'timestamp',
        ));
        return parent::_prepareColumns();
    }

}