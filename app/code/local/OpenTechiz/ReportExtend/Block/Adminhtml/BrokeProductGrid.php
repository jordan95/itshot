<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_BrokeProductGrid extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_brokeProduct';
    protected $_blockGroup = 'opentechiz_reportExtend';
    protected $_headerText = 'Broke Product Report';

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Broke Product Report');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function getAjaxUrl(){
        return $this->getUrl('adminhtml/brokeProduct/ajax/');
    }

}