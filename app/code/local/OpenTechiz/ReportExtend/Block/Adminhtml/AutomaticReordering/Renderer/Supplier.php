<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_Supplier extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $suppliers = Mage::helper('opentechiz_reportExtend/automaticReordering')->getProductSuppliers($row->getProductId());
        $value = $this->_getInputValue($row);
        $html = '<div>' . (isset($suppliers[$value]) ? $suppliers[$value]['name'] . ' | '
                . Mage::getModel('directory/currency')->format(
                    $suppliers[$value]['last_cost'],
                    array('display'=>Zend_Currency::NO_SYMBOL),
                    false
                ) . ' | '
                . Mage::helper('core')->formatDate($suppliers[$value]['last_cost_date']) : '&nbsp;') . '</div>';
        $html .= '<select class="' . $this->getColumn()->getValidateClass()
            . '" name="' . $this->getColumn()->getId()
            . '" data-group-sku="' . $row->getGroupSku()
            . '" onchange="saveItemSupplier(this, \'' . $row->getGroupSku() . '\')">';
        if ($suppliers) {
            foreach ($suppliers as $key => $supplier) {
                $html .= '<option value="'
                    . $key . '"'
                    . ($key == $value ? ' selected="selected"' : '')
                    . '>' . $supplier['name'] . ' | '
                    . Mage::getModel('directory/currency')->format(
                        $supplier['last_cost'],
                        array('display'=>Zend_Currency::NO_SYMBOL),
                        false
                    ) . ' | '
                    . Mage::helper('core')->formatDate($supplier['last_cost_date']) . '</option>';
            }
        } else {
            $suppliers = Mage::helper('opentechiz_purchase')->getAllSupplier();
            $html .= '<option value="0" selected="selected"></option>';
            foreach ($suppliers as $key => $supplier) {
                $html .= '<option value="' . $key . '"' . '>' . $supplier . '</option>';
            }
        }
        $html .= '</select>';
        return $html;
    }
}