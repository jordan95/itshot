<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_ImageFromSku extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {        
        $sku = $row->getData('sku');
        
        if(strpos($sku, 'quotation') === false) {
            $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $parts[0]);
            if(count($parts) > 3){
            $product->setSku($sku);
            $imageUrl = $this->helper('catalog/image')->init($product, 'thumbnail')->resize(120, 120);

            $html = '<img src="'.$imageUrl.'" width="120px" height="120px">';

            } else $html = '<img src="'.Mage::helper('opentechiz_salesextend')->getImageByColor($product, $sku).'" width="120px" height="120px">';

        } else {
            $quotationProductId = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[1];
            $productImages = Mage::getModel('opentechiz_quotation/product')->load($quotationProductId)->getQpImage();

            $url = explode(',',$productImages)[0];
            $html = '<img src="'.$url.'" width="120px" height="120px">';
        }
        

        return $html;
    }
}