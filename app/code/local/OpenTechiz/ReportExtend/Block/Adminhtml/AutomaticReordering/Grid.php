<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('automaticReorderingGrid');
        $this->setDefaultSort('group_sku');
        $this->setDefaultDir('ASC');
        $this->setDefaultFilter(['is_active' => 1]);
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
        $additionalJavaScript = <<<JS
            function emailSuppliers() {
                let content = '<h3>Are you sure?</h3>';
                Dialog.confirm(content, {
                    draggable: true,
                    resizable: true,
                    closable: true,
                    className: 'magento',
                    windowClassName: 'popup-window',
                    title: 'Email Suppliers',
                    width: 200,
                    zIndex: 1000,
                    recenterAuto: false,
                    hideEffect: Element.hide,
                    showEffect: Element.show,
                    id: 'email_suppliers_popup',
                    okLabel: 'Yes',
                    cancelLabel: 'No',
                    ok: function (win) {
                        new Ajax.Request('{$this->getUrl('*/*/email')}', {
                            'method': 'post',
                            'onComplete': function(response){
                                let result = response.responseJSON;
                                let content = result.content;
                                Dialog.info(content, {
                                    draggable: true,
                                    resizable: true,
                                    closable: true,
                                    className: 'magento',
                                    windowClassName: 'popup-window',
                                    title: 'Email Suppliers result',
                                    width: 500,
                                    zIndex: 1000,
                                    recenterAuto: false,
                                    hideEffect: Element.hide,
                                    showEffect: Element.show,
                                    id: 'email_suppliers_result_popup'
                                });
                            }
                        });
                        return true;
                    }
                });
            }
            function changeStatus(groupSku) {
                new Ajax.Request('{$this->getUrl('*/*/changeStatus')}', {
                    'method': 'post',
                    'parameters': {group_sku: groupSku},
                    'onComplete': function(response){
                        let result = response.responseJSON;
                        if (result.success) {
                            let el = $$('a[data-group-sku='+result.groupSku+']');
                            if (el.length) {
                                el[0].innerText = result.linkText;
                                el[0].up().up().down('.current-order-status').innerText = result.statusText;
                            }
                        }
                    }
                });
            }
            function saveItemQty(qty, id) {
                let orderQty = qty.value;
                let content = '<h3>Save changes?</h3>'
                    + '<input type="hidden" name="id" value="'+id+'"/>'
                    + '<input type="hidden" name="order_qty" value="'+orderQty+'"/>';
                Dialog.confirm(content, {
                    draggable: true,
                    resizable: true,
                    closable: true,
                    className: 'magento',
                    windowClassName: 'popup-window',
                    title: 'Save Item',
                    width: 200,
                    zIndex: 1000,
                    recenterAuto: false,
                    hideEffect: Element.hide,
                    showEffect: Element.show,
                    id: 'save_item_popup',
                    okLabel: 'Yes',
                    cancelLabel: 'No',
                    ok: function (win) {
                        let id = $$('#save_item_popup input[name=id]')[0].value;
                        let orderQty = $$('#save_item_popup input[name=order_qty]')[0].value;
                        new Ajax.Request('{$this->getUrl('*/*/saveQty')}', {
                            'method': 'post',
                            'parameters': {id: id, order_qty: orderQty},
                            'onComplete': function(response){
                                let result = response.responseJSON;
                                if (result.success) {
                                    let el = $$('input[data-item-id='+result.id+']');
                                    if (el.length) {
                                        el[0].value = result.orderQty;
                                        el[0].nextElementSibling.innerHTML = result.orderQty;
                                    }
                                }
                            }
                        });
                        return true;
                    }
                });
            }
            function saveItemSupplier(supplier, groupSku) {
                let supplierId = supplier.value;
                let content = '<h3>Save changes?</h3>'
                    + '<input type="hidden" name="group_sku" value="'+groupSku+'"/>'
                    + '<input type="hidden" name="supplier_id" value="'+supplierId+'"/>';
                Dialog.confirm(content, {
                    draggable: true,
                    resizable: true,
                    closable: true,
                    className: 'magento',
                    windowClassName: 'popup-window',
                    title: 'Save Item',
                    width: 200,
                    zIndex: 1000,
                    recenterAuto: false,
                    hideEffect: Element.hide,
                    showEffect: Element.show,
                    id: 'save_item_popup',
                    okLabel: 'Yes',
                    cancelLabel: 'No',
                    ok: function (win) {
                        let groupSku = $$('#save_item_popup input[name=group_sku]')[0].value;
                        let supplierId = $$('#save_item_popup input[name=supplier_id]')[0].value;
                        new Ajax.Request('{$this->getUrl('*/*/saveSupplier')}', {
                            'method': 'post',
                            'parameters': {group_sku: groupSku, supplier_id: supplierId},
                            'onComplete': function(response){
                                let result = response.responseJSON;
                                if (result.success) {
                                    let el = $$('select[data-group-sku='+result.groupSku+']');
                                    if (el.length) {
                                        el[0].value = result.supplierId;
                                        el[0].previousElementSibling.innerHTML = el[0].selectedOptions[0].innerText;
                                    }
                                }
                            }
                        });
                        return true;
                    }
                });
            }
            function showMoreLess(el) {
                var dots = el.previousElementSibling;
                var moreText = dots.previousElementSibling;
                
                if (dots.style.display === "none") {
                    dots.style.display = "inline";
                    el.innerHTML = "show more"; 
                    moreText.style.display = "none";
                } else {
                    dots.style.display = "none";
                    el.innerHTML = "show less"; 
                    moreText.style.display = "inline";
                }
            }
JS;
        $this->setAdditionalJavaScript($additionalJavaScript);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_reportExtend/automaticReordering')->getCollection()
            ->addExpressionFieldToSelect('group_qty', 'SUM(instock.qty)', ['instock.qty'])
            ->addExpressionFieldToSelect('group_reorder_point', 'SUM(instock.reorder_point)', ['instock.reorder_point'])
            ->join(array('instock' => 'opentechiz_inventory/instock'), 'main_table.inventory_instock_product_id = instock.id');
        $collection->getSelect()->group('instock.group_sku');

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('image', array(
            'header'    => Mage::helper('opentechiz_production')->__('Gallery Image'),
            'width'     => '35px',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_ImageFromSku'
        ));
        $this->addColumn('group_sku', array(
            'header'    => Mage::helper('opentechiz_production')->__('Group SKU'),
            'index'     => 'group_sku',
            'width'     => '350'
        ));
        $this->addColumn('group_qty', array(
            'header'    => Mage::helper('opentechiz_production')->__('Qty'),
            'index'     => 'group_qty',
            'filter'    => false,
            'width'     => '100'
        ));
        $this->addColumn('group_reorder_point', array(
            'header'    => Mage::helper('opentechiz_production')->__('Reorder'),
            'index'     => 'group_reorder_point',
            'filter'    => false,
            'width'     =>  '100'
        ));
        $this->addColumn('price', array(
            'header'    => Mage::helper('opentechiz_production')->__('Sales Price'),
            'index'     => 'price',
            'width'     => '150',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_Price'
        ));
        $this->addColumn('cost', array(
            'header'    => Mage::helper('opentechiz_production')->__('Last Cost'),
            'index'     => 'sku',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_Cost'
        ));
        $this->addColumn('cost_date', array(
            'header'    => Mage::helper('opentechiz_production')->__('Last Cost Date'),
            'index'     => 'cost_date',
            'type'      => 'datetime'
        ));
        $this->addColumn('3m', array(
            'header'    => Mage::helper('opentechiz_production')->__('3MS/RET'),
            'index'     => 'sku',
            'filter'    => false,
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_SalesPerReturn'
        ));
        $this->addColumn('12m', array(
            'header'    => Mage::helper('opentechiz_production')->__('12MS/RET'),
            'index'     => '12m',
            'filter'    => false,
        ));
        $this->addColumn('other', array(
            'header'    => Mage::helper('opentechiz_production')->__('Variants in Stock'),
            'width'     => '500',
            'index'     => 'sku',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_OtherVariant'
        ));
        $this->addColumn('manufacture', array(
            'header'    => Mage::helper('opentechiz_production')->__('Manufacturing'),
            'index'     => 'manufacture',
            'width'     => '500',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_Manufacturing'
        ));
        $this->addColumn('order_qty', array(
            'header'    => Mage::helper('opentechiz_production')->__('Order'),
            'index'     => 'order_qty',
            'type'      => 'input',
            'column_css_class' => 'current-order-qty nowrap',
            'filter'    => false,
            'sortable'  => false,
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_OrderQty'
        ));
        $this->addColumn('supplier_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Supplier'),
            'index'     => 'supplier_id',
            'column_css_class' => 'current-order-supplier',
            'filter'    => false,
            'sortable'  => false,
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_Supplier'
        ));
        $this->addColumn('is_active', array(
            'header'    => Mage::helper('opentechiz_production')->__('Status'),
            'index'     => 'is_active',
            'column_css_class' => 'current-order-status',
            'type'      => 'options',
            'options'   => array(
                0 => Mage::helper('adminhtml')->__('Removed'),
                1 => Mage::helper('adminhtml')->__('Added')
            )
        ));
        $this->addColumn('change_status', array(
            'width'     => '50',
            'column_css_class' => 'current-order-change-status',
            'type'      => 'action',
            'getter'    => 'getId',
            'filter'    => false,
            'sortable'  => false,
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_ChangeStatus'
       ));

        return parent::_prepareColumns();
    }

}