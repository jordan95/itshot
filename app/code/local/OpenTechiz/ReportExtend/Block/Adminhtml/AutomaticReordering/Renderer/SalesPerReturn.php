<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_SalesPerReturn extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $sku = $row->getData('sku');
        $instockItem = Mage::getModel('opentechiz_inventory/instock')->load($sku, 'sku');
        $sku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
        if ($instockItem && $instockItem->getId()) {
            $sku = $instockItem->getGroupSku();
        }
        $sku = $sku.'%';

        $row->set12m(Mage::helper('opentechiz_reportExtend/automaticReordering')->getNumberOfSoldPerReturn($sku, 12));

        return Mage::helper('opentechiz_reportExtend/automaticReordering')->getNumberOfSoldPerReturn($sku, 3);
    }
}