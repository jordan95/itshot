<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_OrderID extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $instock_product_id   = $row->getData('id');
        $item = Mage::getModel('opentechiz_inventory/inventory_instockRequest')->getCollection()->addFieldToFilter('instock_product_id',array('eq' => $instock_product_id))->setOrder('request_id','DESC')->getFirstItem();
//        $orderItemId = Mage::helper('opentechiz_inventory')->getLastItem($sku);

        $orderId = Mage::getModel('sales/order_item')->load($item->getOrderItemId())->getOrderId();
        $incrementId = Mage::getModel('sales/order')->load($orderId)->getIncrementId();

        return '<a href="' . Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view/', array('order_id' => $orderId)) . '">' . $incrementId . '</a>';
    }
}