<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_Cost extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $sku = $row->getData('sku');

        $item = Mage::getModel('opentechiz_inventory/item')->getCollection()->AddFieldtoFilter('sku',array('like'=>$sku))->setOrder('created_at','DESC')->getFirstItem();
        $cost = number_format($item->getCost(), 2, '.', '');

        $row->setCostDate($item->getCreatedAt());
        return $cost;
    }
}