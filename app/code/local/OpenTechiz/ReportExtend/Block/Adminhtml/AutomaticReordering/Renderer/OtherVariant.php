<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_OtherVariant extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    const LENGTH = 3;

    public function render(Varien_Object $row)
    {
        $sku = $row->getData('sku');
        $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR,$sku);
        $sku = $parts[0];
        $flag = false;
        if ($flag == true) $row->setCloseout('YES');
        else $row->setCloseout('NO');
        $sku = $sku.'%';
        $itemCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', array('like' => $sku));
        $html = '<div>';

        $i = 0;
        foreach ($itemCollection as $instockItem) {
            if ($i == self::LENGTH) {
                $html .= '<div class="show-more">';
            }
            $html .= $instockItem->getQty() . ' x ' . $instockItem->getSku() . '<br>';
            $i++;
        }
        if (count($itemCollection) > self::LENGTH) {
            $html .= '</div><span class="dots">...</span>&nbsp;<a href="javascript:void(0)" onclick="showMoreLess(this)">show more</a>';
        }
        $html .= '</div>';

        return $html;
    }
}