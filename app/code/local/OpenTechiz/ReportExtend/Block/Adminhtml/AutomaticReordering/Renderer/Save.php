<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_Save extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action
{
    /**
     * Renders column
     *
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $actionAttributes = new Varien_Object();

        $action['onclick'] = "saveItemChanges({$row->getId()})";

        $actionAttributes->setData($action);
        return '<a ' . $actionAttributes->serialize() . '>' . Mage::helper('adminhtml')->__('Save') . '</a>';
    }
}