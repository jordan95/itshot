<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_OrderQty extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Input
{
    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $items = Mage::helper('opentechiz_reportExtend/automaticReordering')
            ->getItemsByGroupSku($row->getGroupSku())
            ->setOrder('order_qty', Varien_Data_Collection::SORT_ORDER_DESC);
        $html = '<div>';
        foreach ($items as $item) {
            $value = $item->getOrderQty();
            $html .= '<input type="text" style="width: 20px;" ';
            $html .= 'name="' . $this->getColumn()->getId() . '" ';
            $html .= 'data-item-id="' . $item->getId() . '" ';
            $html .= 'value="' . $item->getData($this->getColumn()->getIndex()) . '" ';
            $html .= 'class="input-text ' . $this->getColumn()->getInlineCss() . '" ';
            $html .= 'onchange="saveItemQty(this, ' . $item->getId() . ')"/>&nbsp;';
            $html .= '<span>' . $value . '</span> x ';
            $html .= '<span>' . $item->getSku() . '</span><br>';
        }
        $html .= '</div>';
        return $html;
    }
}