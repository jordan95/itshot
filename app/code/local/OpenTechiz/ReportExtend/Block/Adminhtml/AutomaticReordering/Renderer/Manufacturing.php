<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_Manufacturing extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    const LENGTH = 3;

    public function render(Varien_Object $row)
    {
        $sku = $row->getSku();
        $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR,$sku);
        $sku = $parts[0];

        if ($row->getGroupSku()) {
            $sku = $row->getGroupSku();
        }

        return Mage::helper('opentechiz_reportExtend')->getCurrentManufacturingQty($sku);
    }
}