<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_ChangeStatus extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action
{
    /**
     * Renders column
     *
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $actionAttributes = new Varien_Object();

        $linkTexts = array(
            0 => Mage::helper('adminhtml')->__('Add'),
            1 => Mage::helper('adminhtml')->__('Remove')
        );
        $actionCaption = $linkTexts[$row->getIsActive()];

        $action['onclick'] = "changeStatus('{$row->getGroupSku()}')";
        $action['data-group-sku'] = $row->getGroupSku();

        $actionAttributes->setData($action);
        return '<a ' . $actionAttributes->serialize() . '>' . $actionCaption . '</a>';
    }
}