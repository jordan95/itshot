<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering_Renderer_Price extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $item = Mage::getModel('sales/order_item')->getCollection()
            ->addFieldToSelect('price')
            ->addFieldToFilter('sku', $row->getSku())
            ->addFieldToFilter('price', ['gt' => 0])
            ->setOrder('created_at')
            ->getFirstItem();

        return number_format($item->getPrice(), 2, '.', '');
    }
}