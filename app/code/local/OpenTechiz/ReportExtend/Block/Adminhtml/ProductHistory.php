<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_productHistory';
        $this->_blockGroup = 'opentechiz_reportExtend';
        $this->_headerText = Mage::helper('opentechiz_reportExtend')->__('Product History Report');
        parent::__construct();
        $this->addButton("refresh", array(
            'label' => Mage::helper('catalog')->__('Refresh History'),
            'onclick' => "setLocation('{$this->getUrl('*/*/refresh', array('referer' => base64_encode($this->helper('core/url')->getCurrentUrl())))}')",
                )
        );
        $this->removeButton('add');
    }
    
    public function getTemplate()
    {
        return 'product_history/grid/container.phtml';
    }

    public function getLastUpdateAt()
    {
        $table_prefix = (string) Mage::getConfig()->getTablePrefix();
        /* @var $readAdapter Aoe_DbRetry_Resource_Db_Pdo_Mysql_Adapter */
        $readAdapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select = $readAdapter->select();
//        $select->reset(Zend_Db_Select::COLUMNS);
        $select->from(array('main_table' => $table_prefix . 'core_config_data'));
        $select->columns(array("main_table.value"));
      
        $select->where("`path` = 'opentechiz_reportExtend/product_history/last_updated_at'");
        $result = $readAdapter->fetchRow($select);
        if (!empty($result)) {
            return Mage::getModel('core/date')->date('M d, Y H:i:s', strtotime($result["value"]));
        }
        return false;
    }

}
