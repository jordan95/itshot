<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_NotSoldGrid extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_notSold';
    protected $_blockGroup = 'opentechiz_reportExtend';
    protected $_headerText = 'Items not Sold';

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Items Not Sold');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function getAjaxUrl(){
        return $this->getUrl('adminhtml/notSold/ajax/');
    }

    public function getNotInStockAjaxUrl(){
        return $this->getUrl('adminhtml/notSoldNotInStock/ajax/');
    }

    public function getDisableProductUrl(){
        return $this->getUrl('adminhtml/notSoldNotInStock/disableProduct/');
    }

    public function getProductDataSaveUrl(){
        return $this->getUrl('adminhtml/notSold/saveProductData/');
    }
}