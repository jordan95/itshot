<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_GoldUsedGrid extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_goldUsed';
    protected $_blockGroup = 'opentechiz_reportExtend';
    protected $_headerText = 'Gold Used Report';

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Gold Used Report');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function getAjaxUrl(){
        return $this->getUrl('adminhtml/goldUsed/ajax/');
    }

}