<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_LogStock extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_logStock';
        $this->_blockGroup = 'opentechiz_reportExtend';
        $this->_headerText = Mage::helper('opentechiz_reportExtend')->__('Stock Movement Log Grid');
        parent::__construct();
        $this->removeButton('add');
    }
}