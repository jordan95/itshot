<?php
class OpenTechiz_ReportExtend_Block_Adminhtml_Progress_Renderer_OrderId extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $orderId = $row->getData('orderId');
        $incrementId = $row->getData('increment_id');
        return '<a href="'.Mage::helper("adminhtml")->getUrl("adminhtml/sales_order/view", array('order_id' => $orderId)).'">'.$incrementId.'</a>';
    }
}