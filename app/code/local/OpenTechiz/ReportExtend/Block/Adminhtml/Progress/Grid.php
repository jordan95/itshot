<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_Progress_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $_stage = null;
    protected $_status = null;

    public function __construct()
    {
        parent::__construct();
        $this->setId('progressGrid');
//        $this->setDefaultSort('item_id');
//        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);

        $params = $this->getRequest()->getParams();
        if(isset($params['stage'])){
            $this->_stage = $params['stage'];
        }
        if(isset($params['status'])){
            $this->_status = $params['status'];
        }
       
        
    }
    
    protected function _prepareCollection()
    {
        $table = new Zend_Db_Expr("(select max(updated_at) as last_log, order_id from tsht_order_status_change_log
                group by order_id)");
        $collection = Mage::getModel('sales/order_item')->getCollection()
            ->join(array('order' => 'sales/order'), 'main_table.order_id = order.entity_id',
                array('deliver_by' => 'order.deliver_by',
                      'shipping_description' => 'order.shipping_description',
                      'source' => 'order.source',
                      'increment_id' => 'order.increment_id',
                      'orderId' => 'order.entity_id'))
            ->addFieldToFilter('product_type', array('neq' => 'virtual'))
            ->addFieldToFilter('order_stage', Mage::helper('opentechiz_reportExtend')->convertStageNameToKey($this->_stage))
            ->addFieldToFilter('internal_status', Mage::helper('opentechiz_reportExtend')->convertStatusNameToKey($this->_status))
            ->addAttributeToFilter('state', array('nin' => array("canceled","complete","holded","closed")));
        $this->setCollection($collection);
        $collection->getSelect()->join(array('log'=>$table), "order.increment_id = log.order_id", '*')
            ->order('last_log ASC');
        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('order_id', array(
            'header'    => Mage::helper('opentechiz_reportExtend')->__('Order Number'),
            'index'     => 'increment_id',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_Progress_Renderer_OrderId'
        ));
        $this->addColumn('order_source', array(
            'header'    => Mage::helper('opentechiz_reportExtend')->__('Order Source'),
            'index'     => 'source',
            'width'     =>  '350',
            'type'      => 'options',
            'options'   => OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE,
//            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_Qty'
        ));
        $this->addColumn('shipping_method', array(
            'header'    =>  Mage::helper('opentechiz_reportExtend')->__('Shipping Method'),
            'index'     => 'shipping_description',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_Progress_Renderer_ShippingMethod'
        ));
        $this->addColumn('order_date', array(
            'header'    =>  Mage::helper('opentechiz_reportExtend')->__('Order Date'),
            'index'     =>  'created_at',
            'type'      => 'datetime'
        ));
        $this->addColumn('promised_date', array(
            'header'    =>  Mage::helper('opentechiz_reportExtend')->__('Deliver By'),
            'index'     =>  'deliver_by',
            'type'      => 'date'
//            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_SalesPerReturn'
        ));
        $this->addColumn('sku', array(
            'header'    => Mage::helper('opentechiz_reportExtend')->__('SKU #'),
            'index'     => 'sku',
            'width'     =>  '350'
        ));
        $this->addColumn('image', array(
            'header'    =>  Mage::helper('opentechiz_reportExtend')->__('Image'),
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_Progress_Renderer_Image'
        ));
        $this->addColumn('price', array(
            'header'    => Mage::helper('opentechiz_reportExtend')->__('Selling Price'),
            'index'     => 'price',
            'width'     =>  '200',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_Progress_Renderer_Price'
        ));
        $this->addColumn('days_in_status', array(
            'header'    => Mage::helper('opentechiz_reportExtend')->__('Days in Status'),
            'renderer'  =>  'OpenTechiz_ReportExtend_Block_Adminhtml_Progress_Renderer_DaysInStatus'
        ));

        return parent::_prepareColumns();
    }

}