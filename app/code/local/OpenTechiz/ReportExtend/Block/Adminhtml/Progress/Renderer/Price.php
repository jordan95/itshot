<?php
class OpenTechiz_ReportExtend_Block_Adminhtml_Progress_Renderer_Price extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $price = $row->getData('price');
        return $formattedPrice = Mage::helper('core')->currency($price, true, false);
    }
}