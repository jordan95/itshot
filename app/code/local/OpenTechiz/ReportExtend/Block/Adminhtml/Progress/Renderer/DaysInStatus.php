<?php
class OpenTechiz_ReportExtend_Block_Adminhtml_Progress_Renderer_DaysInStatus extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $orderId = $row->getData('increment_id');
        $orderStatusChangeCollection = Mage::getModel('opentechiz_reportExtend/log_orderStatus')->getCollection()
            ->addFieldToFilter('order_id', $orderId)
            ->setOrder('id', 'DESC');
        $i = 0;
        $date = [];
        foreach ($orderStatusChangeCollection as $orderStatusChange){
            if($i == 1) break;
            $date[$i] = $orderStatusChange->getUpdatedAt();
            $i++;
        }
        if(!isset($date[0])){
            return 'N/A';
        }
        $datetime1 = date_create(now());
        $datetime2 = date_create($date[0]);
        $interval = date_diff($datetime1, $datetime2);
        $time = $interval->format('%a');
        if($time == '0'){
            $time = $interval->format('%H hour(s) %I minute(s) %S second(s)');
        }

        return $time;
    }
}