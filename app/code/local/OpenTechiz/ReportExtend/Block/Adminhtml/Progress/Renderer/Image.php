<?php
class OpenTechiz_ReportExtend_Block_Adminhtml_Progress_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $sku = $row->getData('sku');
        $productId = $row->getData('product_id');
        $product = Mage::getModel('catalog/product')->load($productId);
        $product->setSku($sku);

        return '<img src="'.$this->helper('catalog/image')->init($product, 'small_image')->resize(100).'" width="100px">';
    }
}