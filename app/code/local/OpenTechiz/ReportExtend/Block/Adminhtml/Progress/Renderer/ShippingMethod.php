<?php
class OpenTechiz_ReportExtend_Block_Adminhtml_Progress_Renderer_ShippingMethod
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $shippingMethod = $row->getData('shipping_description');
        $shippingMethodExploded = explode('-', $shippingMethod);
        if(count($shippingMethodExploded) == 2){
            $shippingMethod = $shippingMethodExploded[1];
        }
        return $shippingMethod;
    }
}