<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_LogGeneral extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_logGeneral';
        $this->_blockGroup = 'opentechiz_reportExtend';
        $this->_headerText = Mage::helper('opentechiz_reportExtend')->__('General Log Grid');
        parent::__construct();
        $this->removeButton('add');
    }
}