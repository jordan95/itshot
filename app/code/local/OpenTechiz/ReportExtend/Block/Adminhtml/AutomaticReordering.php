<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AutomaticReordering extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_automaticReordering';
        $this->_blockGroup = 'opentechiz_reportExtend';
        $this->_headerText = Mage::helper('opentechiz_reportExtend')->__('Automatic Reordering Report Grid');
        parent::__construct();
        $this->removeButton('add');
        $this->addButton('send', array(
            'label'     => Mage::helper('opentechiz_reportExtend')->__('Email Suppliers'),
            'onclick'   => 'emailSuppliers()'
        ));
    }
}