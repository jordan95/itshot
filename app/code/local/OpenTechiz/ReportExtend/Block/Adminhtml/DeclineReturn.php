<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DeclineReturn extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_declineReturn';
        $this->_blockGroup = 'opentechiz_reportExtend';
        $this->_headerText = Mage::helper('opentechiz_reportExtend')->__('Decline Return Report');
        parent::__construct();
        $this->removeButton('add');
    }
} 