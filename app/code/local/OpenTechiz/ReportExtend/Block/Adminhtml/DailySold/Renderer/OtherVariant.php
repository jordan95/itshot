<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_OtherVariant extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $sku = $row->getData('sku');
        $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR,$sku);
        $sku = $parts[0];
        $flag = false;

        //check clearance sales
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        if($product && $product->getId()) {
            $productCats = $product->getCategoryIds();
            foreach ($productCats as $productCat) {
                $_cat = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getId())->load($productCat);
                if ($_cat->getName() == 'Clearance Sale') {
                    $flag = true;
                }
            }
        }

        if($flag == true) $row->setCloseout('YES');
        else $row->setCloseout('NO');

        //get other variant
        if(count($parts) > 1) {
            $skuSql = $sku . '\_%';
        }else{
            $skuSql = $sku;
        }

        $itemCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', array('like' => $skuSql));
        $stringOther = '';

        $skus = [];
        foreach ($itemCollection as $instockItem){
            if($instockItem->getGroupSku()) {
                $sku = $instockItem->getGroupSku();
            }else{
                $sku = $instockItem->getSku();
            }
            if(!array_key_exists($sku, $skus)){
                $skus[$sku] = $instockItem->getQty();
            }else{
                $skus[$sku] += $instockItem->getQty();
            }
        }

        foreach ($skus as $sku => $qty){
            $stringOther .= $sku . ' x ' . $qty . '<br>';
        }

        return $stringOther;
    }
}