<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('dailySoldGrid');
        $this->setDefaultSort('item_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $date = Mage::getModel('core/date')->gmtDate('Y-m-d');

        $from = new DateTime($date.' 00:00:00');
        $from = $from->format('Y-m-d H:i:s');

        $to = new DateTime($date.' 23:59:59');
        $to = $to->format('Y-m-d H:i:s');
        $collection = Mage::getModel('opentechiz_inventory/stockMovement')->getCollection()
            ->join(array('instock' => 'opentechiz_inventory/instock'), 'main_table.stock_id = instock.id')
            ->addFieldToFilter('main_table.timestamp', array(
                'from' => $from,
                'to' => $to
            ))
            ->addFieldToFilter('main_table.qty', array('like' => '-%'))
            ->addGroupBySkuFilter()
        ;

        $totalQty = new Zend_Db_Expr("(select sum(qty) as total_qty, group_sku from tsht_inventory_instock_product group by group_sku)");
        $collection->getSelect()->joinLeft(array('total_qty'=>$totalQty), "instock.group_sku = total_qty.group_sku", array('total_qty'=>"total_qty.total_qty"))
            ->where('total_qty = 0');

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('order_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Order #'),
            'index'     => 'sku',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_OrderID'
        ));
        $this->addColumn('sku', array(
            'header'    => Mage::helper('opentechiz_production')->__('SKU'),
            'index'     => 'sku',
            'width'     =>  '350'
        ));
        $this->addColumn('qty_ordered', array(
            'header'    => Mage::helper('opentechiz_production')->__('Quantity'),
            'index'     => 'qty_ordered',
            'width'     =>  '350'
        ));
        $this->addColumn('price', array(
            'header'    => Mage::helper('opentechiz_production')->__('Sold Price'),
            'index'     => 'price',
            'width'     =>  '200',
        ));
        $this->addColumn('cost', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Last Cost'),
            'index'     =>  'sku',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_Cost'
        ));
        $this->addColumn('cost_date', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Last Cost Date'),
            'index'     =>  'cost_date',
            'type'      => 'datetime',
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM, true)
        ));
        $this->addColumn('3m', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('3MS/RET'),
            'index'     =>  'sku',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_SalesPerReturn'
        ));
        $this->addColumn('12m', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('12MS/RET'),
            'index'     =>  '12m',
        ));
        $this->addColumn('other', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Other Variant in Stock'),
            'index'     =>  'sku',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_OtherVariant'
        ));
        $this->addColumn('manufacture', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Manufacturing'),
            'index'     =>  'manufacture',
            'width'     =>  '200',
            'renderer'  =>  'OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_Manufacture'
        ));
        $this->addColumn('close_out', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Close Out'),
            'index'     =>  'closeout',
        ));
        return parent::_prepareColumns();
    }

}