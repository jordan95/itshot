<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_OrderID extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $sku = $row->getData('sku');

        $orderItem = Mage::getModel('sales/order_item')->getCollection()->addFieldToFilter('sku', $sku)
            ->setOrder('created_at', 'DESC')
            ->getFirstItem();
        $orderId = $orderItem->getOrderId();
        $incrementId = Mage::getModel('sales/order')->load($orderId)->getIncrementId();
        $row->setQtyOrdered((int)$orderItem->getQtyOrdered())
            ->setPrice('$'.number_format(round($orderItem->getPrice(), 2), 2, ',', ''))
            ->setData('order_item_id', $orderItem->getId())
            ->setData('product_options', $orderItem->getProductOptions());

        return $incrementId;
    }
}