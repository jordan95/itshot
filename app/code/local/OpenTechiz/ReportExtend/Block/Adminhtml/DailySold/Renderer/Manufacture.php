<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_Manufacture extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $reportHelper = Mage::helper('opentechiz_reportExtend');

        $sku = $row->getData('sku');
        $manufacture = $reportHelper->getCurrentManufacturingQty($sku);

        return $manufacture;
    }
}