<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_Qty extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $orderItemId = $row->getData('order_item_id');

        $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);

        return $orderItem->getQtyOrdered();
    }
}