<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_SalesPerReturn extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    protected $shippedStates = '(15,20)'; //Shipped out, Shipped to customer
    protected $_excludedOrderStatus = ['closed', 'canceled', 'holded', 'layawayorder_canceled'];

    public function render(Varien_Object $row)
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sku = $row->getData('sku');
        $sku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
        $instockItem = Mage::getModel('opentechiz_inventory/instock')->load($row->getData('sku'), 'sku');
        if($instockItem && $instockItem->getId()){
            $sku = $instockItem->getGroupSku();
        }
        $sku = $sku.'%';

        $currentTime = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $lastThreeMonth = date('Y-m-d H:i:s', strtotime("-3 months", strtotime($currentTime)));

        $lastTwelveMonth = date('Y-m-d H:i:s', strtotime("-12 months", strtotime($currentTime)));

        //item shipped
        $query = "SELECT sum(qty) as count FROM `tsht_sales_flat_shipment_item` 
                      inner join tsht_sales_flat_shipment 
                      on tsht_sales_flat_shipment.entity_id=tsht_sales_flat_shipment_item.parent_id 
                      where sku LIKE '".$sku."' and created_at > '".$lastTwelveMonth."'";
        $result = $readConnection->fetchAll($query)[0];
        $sold12m = (int)$result['count'];

        //item attached to order and not shipped
        $query = "SELECT count(*) as count from tsht_inventory_order_item 
                      join tsht_inventory_item on tsht_inventory_order_item.item_id = tsht_inventory_item.item_id 
                      and tsht_inventory_item.state not in ".$this->shippedStates."
                      join tsht_sales_flat_order_item 
                      on tsht_inventory_order_item.order_item_id = tsht_sales_flat_order_item.item_id
                      where tsht_sales_flat_order_item.sku like '".$sku."'
                      and tsht_sales_flat_order_item.created_at > '".$lastTwelveMonth."'";
        $result = $readConnection->fetchAll($query)[0];
        $result = (int)$result['count'];
        $sold12m += $result;

        $returnCollection = Mage::getModel('opentechiz_return/return')->getCollection()->addFieldToFilter('main_table.created_at', array(
            'from' => $lastTwelveMonth,
            'to' => $currentTime
        ))->addFieldToFilter('main_table.solution', array('in' => OpenTechiz_Return_Helper_Data::ALL_SOLUTIONS))
            ->join(array('item' => 'sales/order_item'), 'item.item_id = main_table.order_item_id')->addFieldToFilter('sku', array('like' => $sku));

        $return12m = count($returnCollection);

        $row->set12m($sold12m.'/'.$return12m);

        //item shipped
        $query = "SELECT sum(qty) as count FROM `tsht_sales_flat_shipment_item` 
                      inner join tsht_sales_flat_shipment 
                      on tsht_sales_flat_shipment.entity_id=tsht_sales_flat_shipment_item.parent_id 
                      where sku LIKE '".$sku."' and created_at > '".$lastThreeMonth."'";
        $result = $readConnection->fetchAll($query)[0];
        $sold3m = (int)$result['count'];

        //item attached to order and not shipped
        $query = "SELECT count(*) as count from tsht_inventory_order_item 
                      join tsht_inventory_item on tsht_inventory_order_item.item_id = tsht_inventory_item.item_id 
                      and tsht_inventory_item.state not in ".$this->shippedStates."
                      join tsht_sales_flat_order_item 
                      on tsht_inventory_order_item.order_item_id = tsht_sales_flat_order_item.item_id
                      where tsht_sales_flat_order_item.sku like '".$sku."'
                      and tsht_sales_flat_order_item.created_at > '".$lastThreeMonth."'";
        $result = $readConnection->fetchAll($query)[0];
        $result = (int)$result['count'];
        $sold3m += $result;

        $returnCollection = Mage::getModel('opentechiz_return/return')->getCollection()->addFieldToFilter('main_table.created_at', array(
            'from' => $lastThreeMonth,
            'to' => $currentTime
        ))->addFieldToFilter('main_table.solution', array('in' => OpenTechiz_Return_Helper_Data::ALL_SOLUTIONS))
            ->join(array('item' => 'sales/order_item'), 'item.item_id = main_table.order_item_id')->addFieldToFilter('sku', array('like' => $sku));

        $return3m = count($returnCollection);

        return $sold3m.'/'.$return3m;
    }
}