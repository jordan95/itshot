<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_Cost extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $sku = $row->getData('sku');
        $helper = Mage::helper('opentechiz_inventory');
        $row->setCostDate($helper->getLastCostDate($sku));
        return '$'.number_format($helper->getLastCost($sku), 2, '.', '');
    }
}