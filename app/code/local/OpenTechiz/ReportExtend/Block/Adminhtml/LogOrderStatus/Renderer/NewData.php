<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_LogOrderStatus_Renderer_NewData
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $statuses = $row->getData('new_statuses');
        $statuses = explode(',', $statuses);
        $type = $row->getData('order_type');
        $stage = $statuses[0];
        if(isset(OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE[$stage])) {
            $stage = OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE[$stage];
        }else{
            $stage = '';
        }
        $status = $statuses[1];
        $statusArray = Mage::helper('opentechiz_salesextend')->getStatusArrayByType($type);
        if(isset($statusArray[$status])) {
            $status = $statusArray[$status];
        }else{
            $status = '';
        }

        $veri = $statuses[2];
        if($type == 2) $veri = 9;
        if(isset(OpenTechiz_SalesExtend_Helper_Data::VERIFICATION[$veri])) {
            $veri = OpenTechiz_SalesExtend_Helper_Data::VERIFICATION[$veri];
        }else{
            $veri = '';
        }

        $html = '<table>
          <tr>
            <th>OrderStage</th>
            <th>Internal Status</th>
            <th>Verification Status</th>
          </tr>';
        $html .= '<tr>
            <td width="33%">'.$stage.'</td>
            <td width="33%">'.$status.'</td>
            <td width="33%">'.$veri.'</td>
          </tr>';
        $html .='</table>';
        return $html;
    }
}