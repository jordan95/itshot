<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_LogOrderStatus_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('statusLogGrid');
        $this->setNoFilterMassactionColumn(true);
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }


    protected function _prepareCollection()
    {
        // Get the collection for the grid
        $collection = Mage::getResourceModel('opentechiz_reportExtend/log_orderStatus_collection');

        if(isset($this->getRequest()->getParams()['order_id'])){
            $orderId = $this->getRequest()->getParams()['order_id'];
            $order = Mage::getModel('sales/order')->load($orderId);
            $orderIncrementId = $order->getIncrementId();
            $collection->addFieldToFilter('order_id', $orderIncrementId);
        }
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('ID'),
            'width' => '50',
            'index' => 'id',
            'align' => 'center'
        ));
        $this->addColumn('increment_id', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Order #'),
            'index' => 'order_id',
            'width' => '100',
            'align' => 'center'
        ));
        $this->addColumn('order_type', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Order Type'),
            'index' => 'order_type',
            'width' => '100',
            'type' => 'options',
            'options' => OpenTechiz_SalesExtend_Helper_Data::ORDER_TYPE,
        ));
        $this->addColumn('old_statuses', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Old Order Statuses'),
            'index' => 'old_statuses',
            'renderer' => 'OpenTechiz_ReportExtend_Block_Adminhtml_LogOrderStatus_Renderer_OldData',
            'filter_condition_callback' => array($this, '_oldStatusFilter')
        ));
        $this->addColumn('new_statuses', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('New Order Statuses'),
            'index' => 'new_statuses',
            'renderer' => 'OpenTechiz_ReportExtend_Block_Adminhtml_LogOrderStatus_Renderer_NewData',
            'filter_condition_callback' => array($this, '_newStatusFilter')
        ));
        $this->addColumn('user', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('User'),
            'width' => '50',
            'index' => 'user_id',
            'type'  => 'options',
            'options' => $this->renderUser(),
        ));
        $this->addColumn('created_at', array(
            'header' => Mage::helper('opentechiz_salesextend')->__('Created At'),
            'type' => 'datetime',
            'index' => 'updated_at',
        ));
        return parent::_prepareColumns();
    }

    protected function _oldStatusFilter($collection, $column){
        $value = $column->getFilter()->getValue();
        $value = strtolower($value);
        $allVerification = OpenTechiz_SalesExtend_Helper_Data::VERIFICATION;
        $allStatus = Mage::helper('opentechiz_salesextend')->getAllInternalStatusKey();
        $allStage = OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE;

        //verification
        $result = [];
        $i = 0;
        $string = '';
        foreach ($allVerification as $key => $verification){
            $verification = strtolower($verification);
            if(strpos($verification, $value) !== false){
                $result[] = $key;
            }
        }
        if(count($result) > 0){
            foreach ($result as $key) {
                if($i == 0){
                    $string = 'old_statuses like \'%,' . $key . '\'';
                }else{
                    $string .= 'or old_statuses like \'%,' . $key . '\'';
                }
                $i++;
            }
        }

        //status
        $result = [];
        foreach ($allStatus as $status => $key){
            $status = strtolower($status);
            if(strpos($status, $value) !== false){
                $result[] = $key;
            }
        }

        if(count($result) > 0){
            foreach ($result as $key) {
                if($i == 0){
                    $string = 'old_statuses like \'%,' . $key . ',%\'';
                }else{
                    $string .= 'or old_statuses like \'%,' . $key . ',%\'';
                }
                $i++;
            }
        }

        //stage
        $result = [];
        foreach ($allStage as $key => $stage){
            $stage = strtolower($stage);
            if(strpos($stage, $value) !== false){
                $result[] = $key;
            }
        }
        if(count($result) > 0){
            foreach ($result as $key) {
                if($i == 0){
                    $string = 'old_statuses like \'' . $key. ',%\'';
                }else{
                    $string .= 'or old_statuses like \'' . $key. ',%\'';
                }
                $i++;
            }

        }
        if($string == ''){
            $string = 'id = 0';
        }
        $collection->getSelect()->where($string);
        return $this;
    }

    protected function _newStatusFilter($collection, $column){
        $value = $column->getFilter()->getValue();
        $value = strtolower($value);
        $allVerification = OpenTechiz_SalesExtend_Helper_Data::VERIFICATION;
        $allStatus = Mage::helper('opentechiz_salesextend')->getAllInternalStatusKey();
        $allStage = OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE;

        //verification
        $result = [];
        $i = 0;
        $string = '';
        foreach ($allVerification as $key => $verification){
            $verification = strtolower($verification);
            if(strpos($verification, $value) !== false){
                $result[] = $key;
            }
        }
        if(count($result) > 0){
            foreach ($result as $key) {
                if($i == 0){
                    $string = 'new_statuses like \'%,' . $key . '\'';
                }else{
                    $string .= 'or new_statuses like \'%,' . $key . '\'';
                }
                $i++;
            }
        }

        //status
        $result = [];
        foreach ($allStatus as $status => $key){
            $status = strtolower($status);
            if(strpos($status, $value) !== false){
                $result[] = $key;
            }
        }

        if(count($result) > 0){
            foreach ($result as $key) {
                if($i == 0){
                    $string = 'new_statuses like \'%,' . $key . ',%\'';
                }else{
                    $string .= 'or new_statuses like \'%,' . $key . ',%\'';
                }
                $i++;
            }
        }

        //stage
        $result = [];
        foreach ($allStage as $key => $stage){
            $stage = strtolower($stage);
            if(strpos($stage, $value) !== false){
                $result[] = $key;
            }
        }
        if(count($result) > 0){
            foreach ($result as $key) {
                if($i == 0){
                    $string = 'new_statuses like \'' . $key. ',%\'';
                }else{
                    $string .= 'or new_statuses like \'' . $key. ',%\'';
                }
                $i++;
            }

        }
        if($string == ''){
            $string = 'id = 0';
        }
        $collection->getSelect()->where($string);
        return $this;
    }

    public function renderUser()
    {
        $userCollection = Mage::getModel('admin/user')->getCollection();
        $result = [];
        $result[0] = 'System';
        foreach ($userCollection as $user){
            $result[$user->getId()] = $user->getFirstname().' '. $user->getLastname();
        }

        return $result;
    }
}