<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_Progress extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $params = $this->getRequest()->getParams();
        $gridName = Mage::helper('opentechiz_reportExtend')->getReportName($params['stage'], $params['status']);
        $this->_controller = 'adminhtml_progress';
        $this->_blockGroup = 'opentechiz_reportExtend';
        $this->_headerText = Mage::helper('opentechiz_reportExtend')->__('Order Progress Report Grid').' - '.$gridName;
        parent::__construct();
        $this->removeButton('add');
    }
}