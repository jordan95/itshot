<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ReturnRateGrid extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_returnRate';
    protected $_blockGroup = 'opentechiz_reportExtend';
    protected $_headerText = 'Return Rate Report';

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Return Rate Report');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function getAjaxUrl(){
        return $this->getUrl('adminhtml/returnRate/ajax/');
    }

}