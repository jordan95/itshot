<?php
class OpenTechiz_ReportExtend_Block_Adminhtml_DailyReturn_Renderer_OrderID extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $orderId = $row->getData('order_id');

        $incrementId = Mage::getModel('sales/order')->load($orderId)->getIncrementId();

        return '<a href="' . Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view/', array('order_id' => $orderId)) . '">' . $incrementId . '</a>';
    }
}