<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DailyReturn_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $returnSolutionArray = OpenTechiz_Return_Helper_Data::ALL_SOLUTIONS;

    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('dailyReturnGrid');
        $this->setDefaultSort('item_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');

        $time =  strtotime($date);
        $date = date('Y-m-d', $time);

        $from = $date.' 00:00:00';
        $from = strtotime($from);
        $from = date('Y-m-d H:i:s', strtotime("+5 hour", $from));

        $to = $date.' 23:59:59';
        $to = strtotime($to);
        $to = date('Y-m-d H:i:s', strtotime("+5 hour", $to));

        $collection = Mage::getModel('opentechiz_return/return')->getCollection()->addFieldToFilter('main_table.created_at', array(
            'from' => $from,
            'to' => $to
        ))->addFieldToFilter('main_table.solution', array('in' => $this->returnSolutionArray))
            ->join(array('item_inv' => 'opentechiz_inventory/item'), 'item_inv.barcode = main_table.item_barcode')
            ->join(array('item'=>'sales/order_item'),'main_table.order_item_id = item.item_id', array(
                'order_id' => 'item.order_id',
                'sku' => 'item.sku',
                'price' => 'item.price',
                'purchased_on' => 'item.created_at'
            ));
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('order_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Order #'),
            'index'     => 'order_id',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailyReturn_Renderer_OrderID'
        ));
        $this->addColumn('sku', array(
            'header'    => Mage::helper('opentechiz_production')->__('SKU'),
            'index'     => 'sku',
            'width'     =>  '350'
        ));
        $this->addColumn('return_type', array(
            'header'    => Mage::helper('opentechiz_production')->__('Return Type'),
            'index'     => 'return_type',
            'width'     =>  '350',
            'type'      => 'options',
            'options'   => Mage::helper('opentechiz_return')->getReturnTypes(),
            'align'     => 'center'
        ));
        $this->addColumn('price', array(
            'header'    => Mage::helper('opentechiz_production')->__('Sold Price'),
            'index'     => 'price',
            'width'     =>  '200',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailyReturn_Renderer_Price'
        ));
        $this->addColumn('cost', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Cost'),
            'index'     =>  'cost',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailyReturn_Renderer_Cost'
        ));
        $this->addColumn('purchased_on', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Purchased On'),
            'index'     =>  'purchased_on',
            'type'      => 'datetime'
        ));
        $this->addColumn('3m', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('3MS/RET'),
            'index'     =>  'sku',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_SalesPerReturn'
        ));
        $this->addColumn('12m', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('12MS/RET'),
            'index'     =>  '12m',
        ));
        return parent::_prepareColumns();
    }

}