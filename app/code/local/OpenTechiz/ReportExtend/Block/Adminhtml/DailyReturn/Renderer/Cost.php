<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DailyReturn_Renderer_Cost extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $sku = $row->getData('sku');
        $helper = Mage::helper('opentechiz_inventory');
        return '$'.number_format($helper->getLastCost($sku), 2, '.', '');
    }
}