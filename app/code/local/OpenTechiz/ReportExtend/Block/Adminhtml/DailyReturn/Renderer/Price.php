<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DailyReturn_Renderer_Price extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $price = $row->getData('price');

        return number_format(round($price, 2), 2, ',', '');
    }
}