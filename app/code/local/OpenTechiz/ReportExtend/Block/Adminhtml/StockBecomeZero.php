<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_StockBecomeZero extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('becomzeroGrid');
        $this->setDefaultSort('order_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
        $this->setDefaultLimit(200);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        Mage::getModel('opentechiz_reportExtend/stockBecomeZero')->getCollection();
        $collection = Mage::getModel('opentechiz_reportExtend/stockBecomeZero')->getCollection()
            ->join(array('instock' => 'opentechiz_inventory/instock'), 'main_table.instock_id = instock.id')
        ;

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('order_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Order #'),
            'index'     => 'sku',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_OrderID'
        ));
        $this->addColumn('image', array(
            'header' => Mage::helper('opentechiz_production')->__('Image'),
            'width'     => '125px',
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_EmailTemplateGrid_Renderer_Image'
        ));
        $this->addColumn('sku', array(
            'header'    => Mage::helper('opentechiz_production')->__('SKU'),
            'index'     => 'sku',
            'width'     =>  '350'
        ));
        $this->addColumn('qty_ordered', array(
            'header'    => Mage::helper('opentechiz_production')->__('Quantity'),
            'index'     => 'qty_ordered',
            'width'     =>  '350'
        ));
        $this->addColumn('price', array(
            'header'    => Mage::helper('opentechiz_production')->__('Sold Price'),
            'index'     => 'price',
            'width'     =>  '200',
        ));
        $this->addColumn('cost', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Last Cost'),
            'index'     =>  'sku',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_Cost'
        ));
        $this->addColumn('cost_date', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Last Cost Date'),
            'index'     =>  'cost_date',
            'type'      => 'datetime'
        ));
        $this->addColumn('3m', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('3MS/RET'),
            'index'     =>  'sku',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_SalesPerReturn'
        ));
        $this->addColumn('12m', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('12MS/RET'),
            'index'     =>  '12m',
        ));
        $this->addColumn('other', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Other Variant in Stock'),
            'index'     =>  'sku',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Renderer_OtherVariant'
        ));
        $this->addColumn('manufacture', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Manufacturing'),
            'index'     =>  'manufacture',
        ));
        $this->addColumn('close_out', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Close Out'),
            'index'     =>  'closeout',
        ));

        return parent::_prepareColumns();
    }

}