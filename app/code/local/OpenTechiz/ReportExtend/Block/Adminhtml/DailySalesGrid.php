<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DailySalesGrid extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_dailySales';
    protected $_blockGroup = 'opentechiz_reportExtend';
    protected $_headerText = 'Daily Sales Report';
    protected $returnSolutionArray = OpenTechiz_Return_Helper_Data::ALL_SOLUTIONS;

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function getAjaxUrl(){
        return $this->getUrl('adminhtml/dailySales/ajax/');
    }

    public function getDailySalesData(){
        $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $time =  strtotime($date);
        $date = date('Y-m-d', $time);
        $from = $date.' 00:00:00';
        $from = strtotime($from);
        $from = date('Y-m-d H:i:s', strtotime("+5 hour", $from));

        $to = $date.' 23:59:59';
        $to = strtotime($to);
        $to = date('Y-m-d H:i:s', strtotime("+5 hour", $to));

        $collection = Mage::getModel('sales/order_item')->getCollection()->addFieldToFilter('shipment.created_at', array(
            'from' => $from,
            'to' => $to
        ));
        $collection->join(array('shipment' => 'sales/shipment'), 'main_table.order_id = shipment.order_id', '*');
        $collection->join(array('order' => 'sales/order'), 'main_table.order_id = order.entity_id', '*')->getSelect()->group('item_id');

        $data = [];
        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i] = [];
            $data[$i]['total'] = 0;
            $data[$i]['subtotal'] = 0;
            $data[$i]['tax'] = 0;
            $data[$i]['ship'] = 0;
            $data[$i]['discount'] = 0;
            $data[$i]['order'] = [];
            $data[$i]['item'] = '';
        }

        foreach ($collection as $item){
            $source = $item->getSource();

            if(!in_array($item->getIncrementId(), $data[$source]['order'])){
                array_push($data[$source]['order'], $item->getIncrementId());
                $data[$source]['total'] += $item->getBaseGrandTotal();
                $data[$source]['subtotal'] += $item->getBaseSubtotal();
                $data[$source]['tax'] += $item->getBaseTaxAmount();
                $data[$source]['ship'] += $item->getBaseShippingAmount();
                $data[$source]['discount'] += $item->getBaseDiscountAmount();
            }

            $data[$source]['item'] .= $item->getSku().', ';
        }

        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i]['order'] = trim(implode(',', $data[$i]['order']), ',');
            $data[$i]['item'] = trim($data[$i]['item'], ', ');
        }
        return $data;
    }

    public function getDailyRefundData(){
        $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $time =  strtotime($date);
        $date = date('Y-m-d', $time);

        $from = $date.' 00:00:00';
        $from = strtotime($from);
        $from = date('Y-m-d H:i:s', strtotime("+5 hour", $from));

        $to = $date.' 23:59:59';
        $to = strtotime($to);
        $to = date('Y-m-d H:i:s', strtotime("+5 hour", $to));

        $collection = Mage::getModel('opentechiz_return/return')->getCollection()->addFieldToFilter('main_table.created_at', array(
            'from' => $from,
            'to' => $to
        ))->addFieldToFilter('main_table.solution', array('in' => $this->returnSolutionArray));
        $collection->join(array('order_item' => 'sales/order_item'), 'main_table.order_item_id = order_item.item_id', '*')
            ->join(array('order' => 'sales/order'), 'order_item.order_id = order.entity_id', '*');

        $data = [];
        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i] = [];
            $data[$i]['total'] = 0;
            $data[$i]['subtotal'] = 0;

            $data[$i]['order'] = [];
            $data[$i]['item'] = '';
        }

        $orderTotalCounted = [];
        foreach ($collection as $item){
            $source = $item->getSource();
            if(in_array($item->getOrderId(), $orderTotalCounted)){
                $refunded = 0;
            }else {
                $orderId = $item->getOrderId();
                $totalRefunded = 0;
                $paymentCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                    ->addFieldToFilter('type', 'creditmemo')->addFieldToFilter('order_id', $orderId);
                foreach ($paymentCollection as $payment){
                    $totalRefunded += $payment->getTotal();
                }
                $refunded = abs(($totalRefunded));
            }

            if(!in_array($item->getIncrementId(), $data[$source]['order'])) {
                array_push($data[$source]['order'], $item->getIncrementId());
            }
            $data[$source]['total'] = $data[$source]['total'] - $refunded;
            $data[$source]['subtotal'] = $data[$source]['subtotal'] - $refunded;

            $data[$source]['item'] .= $item->getSku().', ';

            array_push($orderTotalCounted, $item->getOrderId());
        }

        foreach(OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE as $i => $name){
            $data[$i]['order'] = trim(implode(',', $data[$i]['order']), ',');
            $data[$i]['item'] = trim($data[$i]['item'], ', ');
        }
        return $data;
    }

}