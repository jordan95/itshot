<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistoryGrid extends Mage_Core_Block_Template
{

    protected $_controller = 'adminhtml_productHistory';
    protected $_blockGroup = 'opentechiz_reportExtend';
    protected $_headerText = 'Product History Report';

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Product History Report');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function getDateFromFilter()
    {
        $filter = base64_decode($this->getRequest()->getParam('filter', false));
        parse_str($filter, $params);
        return isset($params['from']) ? $params['from'] : ($filter == false ? date('m/d/Y', strtotime("- 1 year")): "");
    }

    public function getDateToFilter()
    {
        $filter = base64_decode($this->getRequest()->getParam('filter'));
        parse_str($filter, $params);
        return isset($params['to']) ? $params['to'] : ($filter == false?  date('m/d/Y'): "");
    }

    public function getSkuFilter()
    {
        $filter = base64_decode($this->getRequest()->getParam('filter'));
        parse_str($filter, $params);
        return isset($params['sku']) ? $params['sku'] : '';
    }

    public function getProductHistory()
    {
        $filter = base64_decode($this->getRequest()->getParam('filter'));
        parse_str($filter, $params);
        if (count($params) == 0) {
            return array();
        }
        return Mage::getModel('opentechiz_reportExtend/productHistory')->getProductHistoryData($params);
    }

    public function getAjaxUrl()
    {
        return $this->getUrl('adminhtml/productHistory/ajax/');
    }

}
