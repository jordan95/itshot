<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_AdampierRMA_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('adampierRMAGrid');
        $this->setDefaultSort('order_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_reportExtend/oldRma')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('opentechiz_reportExtend')->__('ID'),
            'index'     => 'old_rma_id'
        ));
        $this->addColumn('order_id', array(
            'header'    => Mage::helper('opentechiz_reportExtend')->__('Order ID'),
            'index'     => 'order_id',
            'width'     =>  '200',
            'renderer'   => 'OpenTechiz_ReportExtend_Block_Adminhtml_Renderer_OrderUrl'
        ));
//        $this->addColumn('old_invoice_id', array(
//            'header'    => Mage::helper('opentechiz_reportExtend')->__('Invoice ID'),
//            'index'     => 'old_invoice_id',
//            'width'     =>  '200',
//        ));
        $this->addColumn('customer_id', array(
            'header'    => Mage::helper('opentechiz_reportExtend')->__('Customer ID'),
            'index'     => 'customer_id',
            'width'     =>  '200',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_Renderer_CustomerUrl'
        ));
        $this->addColumn('product_description', array(
            'header'    => Mage::helper('opentechiz_reportExtend')->__('Item'),
            'index'     => 'product_description',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_Renderer_ProductName'
        ));
        $this->addColumn('qty', array(
            'header'    =>  Mage::helper('opentechiz_reportExtend')->__('Quantity'),
            'index'     =>  'qty',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_Renderer_Qty'
        ));
        $this->addColumn('amount', array(
            'header'    =>  Mage::helper('opentechiz_reportExtend')->__('Amount'),
            'index'     =>  'amount',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_Renderer_Amount'
        ));
        $this->addColumn('return_type', array(
            'header'    =>  Mage::helper('opentechiz_reportExtend')->__('Type'),
            'index'     =>  'return_type',
        ));
        $this->addColumn('is_paid', array(
            'header'    =>  Mage::helper('opentechiz_reportExtend')->__('Is Paid'),
            'index'     =>  'is_paid',
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_Renderer_IsPaid'
        ));
        return parent::_prepareColumns();
    }
}