<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DailySold extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_dailySold';
        $this->_blockGroup = 'opentechiz_reportExtend';
        $this->_headerText = Mage::helper('opentechiz_reportExtend')->__('Daily Sold Report Grid');
        parent::__construct();
        $this->removeButton('add');
    }
}