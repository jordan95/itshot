<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_Purchasevendor_Renderer_ImageFromSku extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $productId =  $row->getData('product_id');
        $product = Mage::getModel('catalog/product')->load($productId);
        $sku = $row->getData('sku');

        $product->setSku($sku);
        $imageUrl = $this->helper('catalog/image')->init($product, 'thumbnail')->resize(120, 120);
        $html = '<img src="'.$imageUrl.'" width="120px" height="120px">';

        return $html;
    }
}