<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_Purchasevendor_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('purchasvendorgrid');
        $this->setDefaultSort('supplier_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_purchase/orderitem')->getCollection();
        $collection->getSelect()->join(array('purchase_order'=>'tsht_purchase_order'), 'purchase_order.po_id = main_table.po_id', array('purchase_order.po_id','purchase_order.po_increment_id','purchase_order.created_at','purchase_order.delivery_date'))->join(array('supplier'=>'tsht_purchase_supplier'), 'supplier.supplier_id = purchase_order.supplier_id', array('supplier.name','supplier.supplier_id'))->join(array('product_supplier'=>'tsht_purchase_product_supplier'), 'product_supplier.product_id = main_table.product_id AND supplier.supplier_id = product_supplier.sup_id', array('product_supplier.sup_sku'));
        $collection->getSelect()->where("(main_table.delivered_qty = 0 OR (main_table.delivered_qty > 0 AND (main_table.qty - main_table.delivered_qty) >0)) AND purchase_order.status NOT IN (3,4) AND main_table.qty >0");
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('supplier_id', array(
            'header'    => Mage::helper('core')->__('Vendor'),
            'index'     => 'supplier_id',
            'width'     =>  '250',
            'type'      => 'options',
            'filter_index'     => 'supplier.supplier_id',
            'options'   => Mage::helper('opentechiz_reportExtend')->getListSupplier(),
            'renderer' => 'OpenTechiz_ReportExtend_Block_Adminhtml_Purchasevendor_Renderer_SupplierName'
        ));
        $this->addColumn('po_increment_id', array(
            'header'    => Mage::helper('core')->__('PO#'),
            'index'     => 'po_increment_id',
            'filter_index'     => 'po_increment_id',
            'width'     =>  '100'
        ));
        $this->addColumn('created_at', array(
            'header'    => Mage::helper('core')->__('PO Date'),
            'index'     => 'created_at',
            'filter_index'     => 'purchase_order.created_at',
            'width'     =>  '200',
            'type' => 'datetime'
        ));
        $this->addColumn('delivery_date', array(
            'header'    => Mage::helper('core')->__('Delivery Date'),
            'index'     => 'delivery_date',
            'width'     =>  '200',
            'type' => 'datetime'
        ));

        $this->addColumn('sku', array(
            'header'    => Mage::helper('core')->__('Sku'),
            'index'     => 'sku',
            'width'     =>  '200'
        ));
        $this->addColumn('sup_sku', array(
            'header'    => Mage::helper('core')->__('Supplier Code'),
            'index'     => 'sup_sku',
            'width'     =>  '200'
        ));
        $this->addColumn('gallery_image', array(
            'header'    => Mage::helper('core')->__('Image'),
            'index'     => 'gallery_image',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'OpenTechiz_ReportExtend_Block_Adminhtml_Purchasevendor_Renderer_ImageFromSku',
            'width'     =>  '120',
            'is_system' => true
        ));
        $this->addColumn('qty', array(
            'header'    => Mage::helper('core')->__('Requested Qty'),
            'index'     => 'qty',
            'width'     =>  '30'
        ));
        $this->addColumn('delivered_qty', array(
            'header'    => Mage::helper('core')->__('Delivered Qty'),
            'index'     => 'delivered_qty',
            'width'     =>  '30'
        ));
        $this->addColumn('note', array(
            'header'    => Mage::helper('core')->__('Note'),
            'index'     => 'note',
            'width'     =>  '400'
        ));
      
         $this->addExportType('*/*/exportCsv', Mage::helper('opentechiz_reportExtend')->__('CSV'));
         $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

        return parent::_prepareColumns();
    }

}