<?php
class OpenTechiz_ReportExtend_Block_Adminhtml_Purchasevendor_Renderer_SupplierName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $supplier_name = $row->getData('name');
        return $supplier_name;
    }
}