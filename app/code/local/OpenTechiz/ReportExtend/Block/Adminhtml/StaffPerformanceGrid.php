<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_StaffPerformanceGrid extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_staffPerformance';
    protected $_blockGroup = 'opentechiz_reportExtend';
    protected $_headerText = 'Staff Performance Report';

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Staff Performance Report');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function getAllStaff(){
        $staffCollection = Mage::getModel('opentechiz_production/staff')->getCollection();
//        $adminCollection = Mage::getModel('admin/user')->getCollection();
        $result = [];
        foreach($staffCollection as $staff){
            $result[$staff->getId()] = ucwords($staff->getStaffName());
        }
//        foreach($adminCollection as $admin){
//            $result[$admin->getUserId()] = ucwords($admin->getFirstname().' '.$admin->getLastname());
//        }
        return $result;
    }
    
    public function getAjaxUrl(){
        return $this->getUrl('adminhtml/staffPerformance/ajax/');
    }

}