<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_Renderer_BestSeller_Sku extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $id = $row->getData('product_id');
        $product = Mage::getModel('catalog/product')->load($id);

        return $product->getSku();
    }
}