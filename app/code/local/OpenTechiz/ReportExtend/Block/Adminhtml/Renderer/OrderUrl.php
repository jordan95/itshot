<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_Renderer_OrderUrl extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());

        $url = Mage::helper("adminhtml")->getUrl('adminhtml/sales_order/view', array('order_id' => $value));
        return '<a href="'.$url.'">'.$value.'</a>';
    }
}