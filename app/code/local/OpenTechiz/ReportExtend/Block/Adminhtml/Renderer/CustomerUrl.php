<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_Renderer_CustomerUrl extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());

        $url = Mage::helper("adminhtml")->getUrl('adminhtml/customer/edit', array('id' => $value));
        return '<a href="'.$url.'">'.$value.'</a>';
    }
}