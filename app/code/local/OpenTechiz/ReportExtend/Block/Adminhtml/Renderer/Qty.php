<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_Renderer_Qty extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $result = '';
        $qtyArray = $row->getData('qty');
        $qtyArray = explode(',', $qtyArray);
        foreach ($qtyArray as $qty){
            $qty = trim($qty);
            $result .= $qty.'<br>';
        }
        return $result;
    }
}