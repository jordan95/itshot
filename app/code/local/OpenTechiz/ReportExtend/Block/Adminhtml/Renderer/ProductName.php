<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_Renderer_ProductName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $result = '';
        $names = $row->getData('product_description');
        $names = explode(',', $names);
        foreach ($names as $name){
            $name = trim($name);
            $result .= $name.'<br>';
        }
        return $result;
    }
}