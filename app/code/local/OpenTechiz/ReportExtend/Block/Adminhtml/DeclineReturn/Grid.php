<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_DeclineReturn_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
     public function __construct()
    {
        parent::__construct();
        $this->setId('reportdeclinereturnGrid');
        $this->setDefaultSort('return_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $sales_order =  Mage::getSingleton('core/resource')->getTableName('sales/order');
        $sales_item =  Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $collection = Mage::getModel('opentechiz_return/return')->getCollection()->addFieldToFilter("main_table.status",array('in'=>array(15,52)));
        $collection->getSelect()
                    ->joinLeft(array("sales_item"=>$sales_item),"main_table.order_item_id = sales_item.item_id",array('real_order_id'=>'sales_item.order_id'))
                    ->joinLeft(array("sales_order"=>$sales_order),"sales_item.order_id = sales_order.entity_id",array('order_increment_id'=>'sales_order.increment_id'));
        //echo $collection->getSelect()->__toString();die('aa');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('return_id', array(
            'header' => Mage::helper('opentechiz_return')->__('ID #'),
            'width' => '50',
            'index' => 'return_id',
            'align' => 'center'
        ));

        $this->addColumn('increment_id', array(
            'header' => Mage::helper('opentechiz_return')->__('Increment ID'),
            'index' => 'increment_id',
            'filter_index'=>'main_table.increment_id',
            'width' => '50',
            'align' => 'center'
        ));

        $this->addColumn('return_type', array(
            'header' => Mage::helper('opentechiz_return')->__('Type'),
            'index' => 'return_type',
            'type' => 'options',
            'options' => Mage::helper('opentechiz_return')->getReturnTypes(),
            'align' => 'center',
        ));
        $this->addColumn('reason', array(
            'header' => Mage::helper('opentechiz_return')->__('Reason'),
            'index' => 'reason',
            'type' => 'options',
            'options' => Mage::helper('opentechiz_return')->getReturnReasons(),
            'align' => 'center',
        ));
        $this->addColumn('description', array(
            'header' => Mage::helper('opentechiz_return')->__('Description'),
            'index' => 'description',
        ));

        $this->addColumn('customer_name', array(
            'header'    => Mage::helper('opentechiz_return')->__('Customer Name'),
            'index'        => array('first_name', 'last_name'),
            'type'         => 'concat',
            'separator'    => ' ',
            'filter_index' => new Zend_Db_Expr("CONCAT(first_name, ' ', last_name)"),
            'width'        => '140px',
        ));

        $this->addColumn('customer_email', array(
            'header' => Mage::helper('opentechiz_return')->__('Customer Email'),
            'index' => 'email',
        ));

        $this->addColumn('customer_phone', array(
            'header' => Mage::helper('opentechiz_return')->__('Customer Phone'),
            'index' => 'phone',
        ));

        $this->addColumn('street', array(
            'header' => Mage::helper('opentechiz_return')->__('Address'),
            'index' => array('street', 'city'),
            'type'         => 'concat',
            'separator'    => '<br>',
            'filter_index' => new Zend_Db_Expr("CONCAT(street, ' ', city)"),
            'width'        => '140px',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('opentechiz_return')->__('Created At'),
            'index' => 'created_at',
            'filter_index'=>'main_table.created_at',
            'type' => 'datetime'
        ));
        $this->addColumn('updated_at', array(
            'header' => Mage::helper('opentechiz_return')->__('Updated At'),
            'index' => 'updated_at',
            'filter_index'=>'main_table.updated_at',
            'type' => 'datetime'
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('opentechiz_return')->__('Status'),
            'width' => '120',
            'index' => 'status',
            'filter_index'=>'main_table.status',
            'type' => 'options',
            'options' => array("15"=>"Denied","52"=>"Stock request decline"),
            'align' => 'center',
            'renderer' => 'OpenTechiz_Return_Block_Adminhtml_Renderer_Status'
        ));
        $this->addColumn('is_paid', array(
            'header' => Mage::helper('opentechiz_return')->__('Paid'),
            'index' => 'is_paid',
            'type' => 'options',
            'options' => array(0 => 'No', 1 => 'Yes'),
            'align' => 'center'
        ));
        $this->addColumn('order_increment_id', array(
            'header' => Mage::helper('opentechiz_return')->__('Order #'),
            'width' => '50',
            'index' => 'order_increment_id',
            'filter_index' => 'sales_order.increment_id',
            'renderer' => 'OpenTechiz_Return_Block_Adminhtml_Renderer_RealOrderLink'
        ));
        $this->addColumn('order_id', array(
            'header' => Mage::helper('opentechiz_return')->__('Return Fee Order Id'),
            'index' => 'return_fee_order_id',
            'type' => 'text',
            'renderer' => 'OpenTechiz_Return_Block_Adminhtml_Renderer_OrderLink'
        ));
       
      
        
        return parent::_prepareColumns();
    }
}