<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_LogOrderStatus extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_logOrderStatus';
        $this->_blockGroup = 'opentechiz_reportExtend';
        $this->_headerText = Mage::helper('opentechiz_reportExtend')->__('Order Status History');
        parent::__construct();
        $this->removeButton('add');
    }
}