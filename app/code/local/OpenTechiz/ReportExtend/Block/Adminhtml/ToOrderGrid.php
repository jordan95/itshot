<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ToOrderGrid extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_toOrder';
    protected $_blockGroup = 'opentechiz_reportExtend';
    protected $_headerText = 'Items To Order';

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Items To Order');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function getAjaxUrl(){
        return $this->getUrl('adminhtml/toOrder/ajax/');
    }

}