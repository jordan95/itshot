<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Return extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        if ($value) {
            $url = Mage::getModel("core/url")->getUrl("adminhtml/sales_returns/edit", array("id" => $value));
            return "<a href=\"{$url}\" target=\"_blank\">{$value}</a>";
        }
        return "";
    }

}
