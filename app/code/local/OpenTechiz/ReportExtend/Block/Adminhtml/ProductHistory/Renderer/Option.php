<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Option extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        if(strpos($row->getData('sku'), 'quotation') === false) {
        $value =  $row->getData($this->getColumn()->getIndex());
        $value = unserialize($value);
        $optionHtml = '';
        if (is_bool($value)){
            return '';
        }
        if(array_key_exists('options', $value)) {
            foreach ($value['options'] as $option) {
                $optionHtml .= '<dt>' . $option['label'] . '</dt>';
                $optionHtml .= '<dd>' . $option['value'] . '</dd>';
            }
        }

        $html = '<div class="item-container">
                    <div class="item-text">
                        <dl class="item-options">
                            '.$optionHtml.'
                        </dl>
                    </div>
                </div>';
        } else{
            $quotationProductId = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $row->getSku())[1];
            $optionHtml = '';

            $optionCollection = Mage::getModel('opentechiz_quotation/productOption')->getCollection()->addFieldToFilter('quote_product_id', $quotationProductId);
            
            foreach ($optionCollection as $option){
                if($option->getOptionType() == 'stone') {
                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
                    $optionHtml .= '<dd>' . Mage::getModel('opentechiz_quotation/product')->getStoneName($option->getOptionValue()) . '</dd>';
                } elseif ($option->getOptionType() == 'gold'){
                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
                    $optionHtml .= '<dd>' . Mage::getModel('opentechiz_quotation/product')->getGoldName($option->getOptionValue()) . '</dd>';
                } else{
                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
                    $optionHtml .= '<dd>' . $option->getOptionValue() . '</dd>';
                }
            }
            $html = '<div class="item-container">
                    <div class="item-text">
                        <dl class="item-options">
                            ' . $optionHtml . '
                        </dl>
                    </div>
                </div>';
        }
        return $html;
    }
}