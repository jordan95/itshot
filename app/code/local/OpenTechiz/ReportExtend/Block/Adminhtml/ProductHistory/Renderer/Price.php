<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Price extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        if ($value){
            return Mage::helper('core')->currency($value, true, false);
        }
        return "";
        
    }

}
