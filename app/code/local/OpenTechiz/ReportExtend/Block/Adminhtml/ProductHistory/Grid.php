<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    protected $_countTotals = true;

    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('productHistoryGrid');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);

        $locale = Mage::app()->getLocale();
        $transactionDateToFilter = $locale->date(null, null, null, false);
        $transactionDateToFilter->setHour(23);
        $transactionDateToFilter->setMinute(59);
        $transactionDateToFilter->setSecond(59);
        $transactionDateToFilter->set(date('m/d/Y'));

        $transactionDateFromFilter = $locale->date(null, null, null, false);
        $transactionDateFromFilter->setHour(00);
        $transactionDateFromFilter->setMinute(00);
        $transactionDateFromFilter->setSecond(00);
        $transactionDateFromFilter->set(date('m/d/Y', strtotime('-1 year')));

        $this->setDefaultFilter(array(
            'transaction_date' => array(
                'orig_from' => $transactionDateFromFilter,
                'orig_to' => $transactionDateToFilter,
                'from' => $transactionDateFromFilter,
                'to' => $transactionDateToFilter
            )
        ));
    }

    public function getTemplate()
    {
        return "product_history/grid.phtml";
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /* @var $collection OpenTechiz_Purchase_Model_Resource_ProductHistory_Collection */
        $collection = Mage::getResourceModel('opentechiz_reportExtend/productHistory_collection');
        $select = $collection->getSelect();
        $filter = $this->getParam($this->getVarNameFilter(), null);
        if (is_null($filter)) {
            $select->where("false");
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    public function getEmptyText()
    {
        $filter = $this->getParam($this->getVarNameFilter(), null);
        if (is_null($filter)) {
            $text = " Please enter at least 1 filter and click Search to view results.";
        } else {
            $text = parent::getEmptyText();
        }
        return $text;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    protected function _prepareColumns()
    {
        $locale = Mage::app()->getLocale();
        $transactionDateToFilter = $locale->date(null, null, null, false);
        $transactionDateToFilter->setHour(23);
        $transactionDateToFilter->setMinute(59);
        $transactionDateToFilter->setSecond(59);
        $transactionDateToFilter->set(date('m/d/Y'));

        $transactionDateFromFilter = $locale->date(null, null, null, false);
        $transactionDateFromFilter->setHour(00);
        $transactionDateFromFilter->setMinute(00);
        $transactionDateFromFilter->setSecond(00);
        $transactionDateFromFilter->set(date('m/d/Y', strtotime('-1 year')));
        $this->addColumn('transaction_date', array(
            'header' => Mage::helper('opentechiz_production')->__('Transaction Date'),
            'index' => 'transaction_date',
            'type' => 'datetime'
        ));
        $this->addColumn('sku', array(
            'header' => Mage::helper('opentechiz_production')->__('SKU'),
            'index' => 'sku',
            'renderer' => 'OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Sku',
            'width' => '100'
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('opentechiz_production')->__('Product Name'),
            'index' => 'name',
            'width' => '350',
        ));
        $this->addColumn('option', array(
            'header' => Mage::helper('opentechiz_production')->__('Product Options'),
            'index' => 'option',
            'renderer' => 'OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Option',
            'filter' => false
        ));
        $this->addColumn('po_increment_id', array(
            'header' => Mage::helper('opentechiz_production')->__('PO #'),
            'index' => 'po_increment_id',
            'renderer' => 'OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Purchase',
        ));
        $this->addColumn('vendor_name', array(
            'header' => Mage::helper('opentechiz_production')->__('Vendor'),
            'index' => 'vendor_name',
            'filter' => false
        ));
        $this->addColumn('delivered_qty', array(
            'header' => Mage::helper('opentechiz_production')->__('QTY Purchased'),
            'index' => 'delivered_qty',
            "renderer" => "OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Qty",
            'filter' => false
        ));
        $this->addColumn('price_paid', array(
            'header' => Mage::helper('opentechiz_production')->__('Price Paid'),
            'index' => 'price_paid',
            'renderer' => 'OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Cost',
            'filter' => false
        ));
        $this->addColumn('customer_name', array(
            'header' => Mage::helper('opentechiz_production')->__('Customer name'),
            'index' => 'customer_name',
            'filter' => false
        ));
        $this->addColumn('increment_id', array(
            'header' => Mage::helper('opentechiz_production')->__('Order Number'),
            'index' => 'increment_id',
            'renderer' => 'OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Order'
        ));
        $this->addColumn('item_price', array(
            'header' => Mage::helper('opentechiz_production')->__('Order Item Price'),
            'index' => 'item_price',
            'renderer' => 'OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Price',
            'filter' => false
        ));
        $this->addColumn('qty_ordered', array(
            'header' => Mage::helper('opentechiz_production')->__('QTY Sold'),
            'index' => 'qty_ordered',
            "renderer" => "OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Qty",
            'filter' => false
        ));
        return parent::_prepareColumns();
    }

    public function getTotals()
    {
        $totals = new Varien_Object();
        if (is_null(Mage::registry('product_history_totals'))) {
            Mage::register('product_history_totals', $this->getCollection()->getTotals());
        }

        $data = Mage::registry('product_history_totals');
        $fields = array(
            'transaction_date' => '',
            'sku' => '',
            'name' => '',
            "po_increment_id" => "",
            "option" => "",
            "vendor_name" => "",
            "delivered_qty" => $data['sum_delivered_qty'],
            "price_paid" => $data['sum_price_paid'],
            "customer_name" => "",
            "item_price" => $data['sum_item_price'],
            "increment_id" => "",
            "qty_ordered" => $data['sum_qty_ordered'],
        );
        //First column in the grid
        $fields['name'] = 'Totals';
        $totals->setData($fields);
        return $totals;
    }
   
}
