<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_FormatPrice extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $price = $row->getData($this->getColumn()->getIndex());
        if ($price){
            return Mage::helper('core')->currency($price, true, false);
        }
        return "";

    }

}
