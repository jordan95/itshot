<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Order extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $order_id = $row->getData('order_id');
        $value = $row->getData($this->getColumn()->getIndex());
        if ($value) {
            $url = Mage::getModel("core/url")->getUrl("adminhtml/sales_order/view", array("order_id" => $order_id));
            return "<a href=\"{$url}\" target=\"_blank\">{$value}</a>";
        }
        return "";
    }

}
