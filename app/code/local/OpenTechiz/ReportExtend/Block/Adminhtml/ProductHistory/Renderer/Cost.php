<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Cost extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $sku = $row->getData('sku');
        $helper = Mage::helper('opentechiz_inventory');
        $value = $helper->getLastCost($sku);
        if ($value){
            return Mage::helper('core')->currency($value, true, false);
        }
        return "";

    }

}
