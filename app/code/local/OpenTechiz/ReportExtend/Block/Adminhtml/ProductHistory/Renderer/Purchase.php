<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Purchase extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        if ($value) {
            $purchase = Mage::getModel('opentechiz_purchase/purchase')->load($value, 'po_increment_id');
            if($purchase->getId()){
                $url = Mage::getModel("core/url")->getUrl("adminhtml/purchase_product/edit", array("id" => $purchase->getId()));
                return "<a href=\"{$url}\" target=\"_blank\">{$value}</a>";
            }else{
                return $value;
            }
        }
        return "";
    }

}
