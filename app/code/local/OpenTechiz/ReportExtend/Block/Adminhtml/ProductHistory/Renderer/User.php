<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_User extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $user_id =  $row->getData('user_id');
        $user = Mage::getModel('admin/user')->load($user_id);
        if($user->getId()){
            $name = $user->getName();
        }else{
            $name = '';
        }
        
        return $name;
    }
}