<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ProductHistory_Renderer_Sku extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $url = Mage::getModel("core/url")->getUrl("adminhtml/instockProduct/index", array(
            "filter" => base64_encode(http_build_query(array(
                                'sku' => $row['sku']
        )))));
        return "<a href=\"{$url}\">{$row['sku']}</a>";
    }

}
