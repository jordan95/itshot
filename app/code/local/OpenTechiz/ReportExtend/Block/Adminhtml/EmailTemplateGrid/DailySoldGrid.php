<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_EmailTemplateGrid_DailySoldGrid extends OpenTechiz_ReportExtend_Block_Adminhtml_DailySold_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }

    protected function _prepareColumns()
    {
        $this->addColumnAfter('image', array(
            'header' => Mage::helper('opentechiz_production')->__('Image'),
            'width'     => '125px',
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'OpenTechiz_ReportExtend_Block_Adminhtml_EmailTemplateGrid_Renderer_Image'
        ), 'order_id');
        return parent::_prepareColumns();
    }
}