<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_EmailTemplateGrid_ReorderPointGrid extends OpenTechiz_ReportExtend_Block_Adminhtml_ReorderPoint_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_inventory/instock')->getCollection()->join(array('instock' => 'opentechiz_inventory/instock'), 'main_table.id = instock.id and instock.qty < instock.reorder_point', array('sku'));
       
        $this->setCollection($collection);
        return $this;
    }
}