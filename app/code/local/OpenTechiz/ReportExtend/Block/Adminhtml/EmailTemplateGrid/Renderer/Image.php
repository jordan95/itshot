<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_EmailTemplateGrid_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        try {
            $options = $row->getData('product_options');
            $originalSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $row->getData('sku'))[0];
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $originalSku);
            if ($product && $product->getId()) {
                $product->setSku($row->getData('sku'));
                $productUrl = Mage::helper('adminhtml')->getUrl('adminhtml/catalog_product/edit', array('id' => $product->getId()));

                if (!is_array($options)) {
                    $options = unserialize($options);
                }
                if ($options && isset($options['info_buyRequest']['options']) && array_key_exists('quotation_product_id', $options['info_buyRequest']['options'])) {
                    $quoteProductId = $options['info_buyRequest']['options']['quotation_product_id'];
                    $images = Mage::getModel('opentechiz_quotation/product')->load($quoteProductId)->getQpImage();

                    $image = trim(explode(',', $images)[0]);
                    $html = '<a href="' . $productUrl . '"><img src="' . $image . '" width="120px" height="120px"></a>';
                } else {
                    $html = '<a href="' . $productUrl . '"><img src="' . Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(120, 120) . '" width="120px" height="120px"></a>';
                }
            } else {
                $html = '';
            }

            return $html;
        }catch (Exception $e){
            return '';
        }
    }
}