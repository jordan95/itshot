<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_EmailTemplateGrid_ProgressManufacturing
    extends OpenTechiz_ReportExtend_Block_Adminhtml_Progress_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }

    protected function _prepareCollection()
    {
        $table = new Zend_Db_Expr("(select max(updated_at) as last_log, order_id from tsht_order_status_change_log
                group by order_id)");
        $collection = Mage::getModel('sales/order_item')->getCollection()
            ->join(array('order' => 'sales/order'), 'main_table.order_id = order.entity_id',
                array('deliver_by' => 'order.deliver_by',
                    'shipping_description' => 'order.shipping_description',
                    'source' => 'order.source',
                    'increment_id' => 'order.increment_id',
                    'orderId' => 'order.entity_id'))
            ->addFieldToFilter('product_type', array('neq' => 'virtual'))
            ->addFieldToFilter('order_stage', 2)
            ->addFieldToFilter('internal_status', 2)
            ->addAttributeToFilter('state', array('nin' => array("canceled","complete","holded","closed")));
        $collection->getSelect()->join(array('log'=>$table), "order.increment_id = log.order_id", '*')
            ->order('last_log ASC');

        $this->setCollection($collection);
        return $this;
    }
}