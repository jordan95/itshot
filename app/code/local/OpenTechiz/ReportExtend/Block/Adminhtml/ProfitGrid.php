<?php

class OpenTechiz_ReportExtend_Block_Adminhtml_ProfitGrid extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_profit';
    protected $_blockGroup = 'opentechiz_reportExtend';
    protected $_headerText = 'Profit Report';

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Profit Report');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function getAjaxUrl(){
        return $this->getUrl('adminhtml/profit/ajax/');
    }

}