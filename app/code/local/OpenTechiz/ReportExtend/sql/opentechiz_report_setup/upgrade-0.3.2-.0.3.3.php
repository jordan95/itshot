<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE tsht_product_history_log
    ADD `from_sku`
    VARCHAR(45)
    NULL
");
$installer->run("  
    ALTER TABLE tsht_product_history_log
    ADD `to_sku`
    VARCHAR(45)
    NULL
");
$installer->endSetup();
