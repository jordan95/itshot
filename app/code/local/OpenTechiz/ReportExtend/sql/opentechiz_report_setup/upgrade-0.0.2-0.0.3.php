<?php

$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('stock_become_zero')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('stock_become_zero'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'Id')
        ->addColumn('instock_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'instock id');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();