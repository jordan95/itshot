
<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    
    ALTER TABLE `tsht_old_system_rma` 
    CHANGE `product_description` `product_description` TEXT 
    NULL DEFAULT NULL COMMENT 'product names';

    ALTER TABLE `tsht_old_system_rma` CHANGE `qty` `qty` VARCHAR(255) 
    NOT NULL DEFAULT '0' COMMENT 'qty';
");

$installer->endSetup();