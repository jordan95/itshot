<?php

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

if ($installer->tableExists('opentechiz_purchase/purchase')) {
    $installer->getConnection()
        ->addColumn($installer->getTable('opentechiz_purchase/purchase'),
        'source',
        array(
            'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'nullable'  => false,
            'length'    => 1,
            'default'   => 2,
            'comment'   => 'Source'
            ));
}

$installer->endSetup();