<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE tsht_purchase_order
    ADD `old_data_id`
    VARCHAR(45)
    NULL
    COMMENT 'old_data_id';
");
$installer->run("  
    ALTER TABLE tsht_purchase_order_item
    ADD `old_data_id`
    VARCHAR(255)
    NULL
    COMMENT 'old_data_id';
");
$installer->run("  
    ALTER TABLE tsht_old_system_rma
    ADD `old_data_id`
    VARCHAR(45)
    not NULL unique 
    COMMENT 'old_data_id';  
");
$installer->endSetup();
