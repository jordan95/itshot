<?php
$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE `tsht_order_status_change_log` CHANGE `user_id` `user_id` INTEGER 
    not NULL DEFAULT '0' COMMENT 'user_id';
");

$installer->endSetup();