<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$installer->addAttribute('catalog_product', 'delivery_time', array(
    'group'           => 'General',
    'label'           => 'Delivery Time',
    'input'           => 'text',
    'type'            => 'text',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 0,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => '',
));
$installer->addAttribute('catalog_product', 'product_note', array(
    'group'           => 'General',
    'label'           => 'Note',
    'input'           => 'textarea',
    'type'            => 'text',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 0,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => '',
));
$installer->addAttribute('catalog_product', 'cost_time', array(
    'group'           => 'General',
    'label'           => 'LowestCost/Date',
    'input'           => 'text',
    'type'            => 'text',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 0,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => '',
));

$attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection')->addFieldToFilter('entity_type_id', 4);
foreach ($attributeSetCollection as $attributeSet) {
    $attributeSetId = $installer->getAttributeSetId(4, $attributeSet->getAttributeSetName());
    $installer->addAttributeToSet(4, $attributeSetId, 'General', 'delivery_time', 10);
    $installer->addAttributeToSet(4, $attributeSetId, 'General', 'product_note', 10);
    $installer->addAttributeToSet(4, $attributeSetId, 'General', 'cost_time', 10);
}
$installer->endSetup();
?>