<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE tsht_stock_become_zero
    ADD `created_at`
    datetime
    NULL
    COMMENT 'created';
");

$installer->endSetup();
