<?php

$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('old_system_order')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('old_system_order'))
        ->addColumn('old_order_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 20, array(
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'old order Id')
        ->addColumn('current_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true,
            'default'  => 0
        ), 'current id');

    $installer->getConnection()->createTable($table);
}

if(!$installer->tableExists('old_system_invoice')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('old_system_invoice'))
        ->addColumn('old_invoice_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 20, array(
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'old invoice Id')
        ->addColumn('current_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true,
            'default'  => 0
        ), 'current id');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();