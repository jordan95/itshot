<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE tsht_old_system_invoice
    ADD `created_at`
    datetime
    NULL
    COMMENT 'created';
");
$installer->run("  
    ALTER TABLE tsht_old_system_order
    ADD `created_at`
    datetime 
    NULL
    COMMENT 'created';
");
$installer->run("  
    ALTER TABLE tsht_old_system_recent_order
    ADD `created_at`
    datetime 
    NULL
    COMMENT 'created';
");
$installer->endSetup();
