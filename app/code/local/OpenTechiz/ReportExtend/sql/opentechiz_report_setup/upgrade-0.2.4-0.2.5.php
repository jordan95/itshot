<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS tsht_old_system_recent_order;
    CREATE TABLE tsht_old_system_recent_order (
      `old_order_id`   VARCHAR(60)  NOT NULL,
      `current_id`   INT(11)        NULL,
      PRIMARY KEY (`old_order_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
$installer->endSetup();