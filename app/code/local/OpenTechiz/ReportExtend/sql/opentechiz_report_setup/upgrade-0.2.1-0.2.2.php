<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE `tsht_old_system_order_failed_import` CHANGE `old_order_id` `old_order_id` VARCHAR(60) 
    NULL DEFAULT '0' COMMENT 'old_order_id';
    
    ALTER TABLE `tsht_old_system_order` CHANGE `old_order_id` `old_order_id` VARCHAR(60) 
    NOT NULL COMMENT 'old_order_id';
    
    ALTER TABLE `tsht_old_system_invoice` CHANGE `old_invoice_id` `old_invoice_id` VARCHAR(60) 
    NOT NULL COMMENT 'old_invoice_id';
");

$installer->endSetup();