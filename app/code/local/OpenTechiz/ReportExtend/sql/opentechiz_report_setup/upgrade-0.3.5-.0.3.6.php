<?php

$installer = $this;
$installer->startSetup();

$installer->run("  
   ALTER TABLE tsht_purchase_order 
   ADD INDEX `purchase_order_id_index` (`po_id`)
");
$installer->run("  
   ALTER TABLE tsht_purchase_supplier 
   ADD INDEX `purchase_supplier_id_index` (`supplier_id`)
");
$installer->endSetup();
