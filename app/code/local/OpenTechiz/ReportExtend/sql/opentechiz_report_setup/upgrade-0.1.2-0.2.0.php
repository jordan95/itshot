<?php

$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('order_status_change_log')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('order_status_change_log'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'Id')
        ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'order id')
        ->addColumn('order_type', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'order type')
        ->addColumn('old_statuses', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'old statuses')
        ->addColumn('new_statuses', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'new statuses')
        ->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true,
        ), 'user id');
    $installer->getConnection()->createTable($table);

    $installer->run("ALTER TABLE `{$installer->getTable('order_status_change_log')}` ADD `updated_at` DATETIME not null default CURRENT_TIMESTAMP ;");
}
