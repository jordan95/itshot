<?php

$installer = $this;
$installer->startSetup();

$installer->run("  
   ALTER TABLE tsht_product_history_log 
   ADD INDEX `log_return_id_index` (`return_id`)
");
$installer->endSetup();
