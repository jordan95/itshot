<?php

$installer = $this;
$installer->startSetup();

$installer->run("  
   ALTER TABLE  tsht_purchase_product_supplier 
   ADD INDEX `po_product_sup_pro_id_index` (`product_id`)
");
$installer->run("  
   ALTER TABLE  tsht_purchase_order_item 
   ADD INDEX `po_order_item_pro_id_index` (`product_id`)
");
$installer->endSetup();
