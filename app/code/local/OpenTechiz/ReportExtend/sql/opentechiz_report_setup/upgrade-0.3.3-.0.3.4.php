<?php

$installer = $this;
$installer->startSetup();

$installer->run("  
   ALTER TABLE tsht_product_history_log 
   ADD INDEX `sku_variant_index` (`sku_variant`)
");
$installer->run("  
   ALTER TABLE tsht_product_history_log 
   ADD INDEX `order_id_index` (`order_id`)
");
$installer->run("  
   ALTER TABLE tsht_product_history_log 
   ADD INDEX `po_order_id_index` (`po_order_id`)
");
$installer->endSetup();
