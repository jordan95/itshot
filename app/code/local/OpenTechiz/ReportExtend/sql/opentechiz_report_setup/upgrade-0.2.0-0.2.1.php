<?php

$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('old_system_order_failed_import')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('old_system_order_failed_import'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'unsigned' => true,
            'primary'  => true
        ), 'old order Id')
        ->addColumn('old_order_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 20, array(
            'nullable' => true,
            'default'  => 0
        ), 'old order Id')
        ->addColumn('file_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
            'nullable' => true,
            'default'  => ''
        ), 'old order Id')
        ->addColumn('reason', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => true,
        ), 'reason');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();