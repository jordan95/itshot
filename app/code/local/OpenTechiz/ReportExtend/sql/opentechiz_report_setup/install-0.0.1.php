<?php

$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('report_general_log')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('report_general_log'))
        ->addColumn('general_action_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'Id')
        ->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'user id')
        ->addColumn('user_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'name')
        ->addColumn('user_email', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'email')
        ->addColumn('action', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
        ), 'action taken')
        ->addColumn('timestamp', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'time');
    $installer->getConnection()->createTable($table);
}

if(!$installer->tableExists('report_stock_log')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('report_stock_log'))
        ->addColumn('stock_movement_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'Id')
        ->addColumn('timestamp', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'time')
        ->addColumn('stock_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'product id')
        ->addColumn('type', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'type')
        ->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'user_id')
        ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
        ), 'description')
        ->addColumn('qty', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false
        ), 'qty')
        ->addColumn('on_hold', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false
        ), 'qty');
    $installer->getConnection()->createTable($table);
}

$installer->endSetup();