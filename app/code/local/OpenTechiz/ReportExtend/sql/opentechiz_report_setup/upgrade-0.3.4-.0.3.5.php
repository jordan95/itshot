<?php

$installer = $this;
$installer->startSetup();

$installer->run("  
   ALTER TABLE tsht_purchase_order_item 
   ADD INDEX `po_id_item_index` (`po_id`)
");
$installer->run("  
   ALTER TABLE tsht_purchase_order 
   ADD INDEX `supplier_id_index` (`supplier_id`)
");
$installer->endSetup();
