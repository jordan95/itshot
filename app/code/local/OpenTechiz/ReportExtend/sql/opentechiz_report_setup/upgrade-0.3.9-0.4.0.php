<?php

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('opentechiz_reportExtend/automatic_reordering')
    && $installer->tableExists('opentechiz_inventory/instock')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('opentechiz_reportExtend/automatic_reordering'))
        ->addColumn('inventory_instock_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'Inventory Instock Product Id')
        ->addColumn('ai_order_qty', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            'default'  => 0
        ), 'AI Order Qty')
        ->addColumn('order_qty', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            'default'  => 0
        ), 'Order Qty')
        ->addColumn('ai_supplier_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true
        ), 'AI Supplier Id')
        ->addColumn('supplier_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true,
        ), 'Supplier Id')
        ->addColumn('purchased_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            'nullable'  => true,
        ), 'Purchased At')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            'nullable'  => false,
        ), 'Created At')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            'nullable'  => false,
        ), 'Updated At')
        ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'default'   => '1',
        ), 'Is Active')
        ->addForeignKey(
            $installer->getFkName('opentechiz_reportExtend/automatic_reordering', 'inventory_instock_product_id', 'opentechiz_inventory/instock', 'id'),
            'inventory_instock_product_id',
            $installer->getTable('opentechiz_inventory/instock'),
            'id',
            Varien_Db_Ddl_Table::ACTION_CASCADE,
            Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->setComment('Automatic Reordering');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();