<?php

$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('product_history_log')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('product_history_log'))
        ->addColumn('stock_movement_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'Id')
        ->addColumn('timestamp', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'time')
        ->addColumn('stock_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'product id')
        ->addColumn('type', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'type')
        ->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'user_id')
        ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
        ), 'description')
        ->addColumn('qty', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true
        ), 'qty')
        ->addColumn('on_hold', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true
        ), 'qty')
        ->addColumn('sku_variant', Varien_Db_Ddl_Table::TYPE_VARCHAR, 45, array(
            'nullable' => true
        ), 'sku_variant')
        ->addColumn('qty_before_movement', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true
        ), 'qty_before_movement')
        ->addColumn('on_hold_before_movement', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true
        ), 'on_hold_before_movement')
        ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true
        ), 'order_id')
        ->addColumn('production_request_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true
        ), 'production_request_id')
        ->addColumn('po_order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true
        ), 'po_order_id')
        ->addColumn('product_process_request_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true
        ), 'product_process_request_id')
        ->addColumn('instock_request_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true
        ), 'instock_request_id')
        ->addColumn('return_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true
        ), 'return_id')
        ;
    $installer->getConnection()->createTable($table);
}

$installer->endSetup();