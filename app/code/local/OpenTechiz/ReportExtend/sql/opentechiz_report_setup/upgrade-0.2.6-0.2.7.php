<?php

$installer = $this;
$installer->startSetup();

if (!$installer->tableExists('product_history_report')) {
    $installer->run("
    CREATE TABLE `tsht_product_history_report` (
        `product_history_id` int(11) NOT NULL AUTO_INCREMENT,
        `transaction_date` datetime NOT NULL,
        `sku` varchar(128) NOT NULL,
        `name` varchar(500) NULL DEFAULT NULL,
        `po_increment_id` varchar(255) NULL DEFAULT NULL,
        `option` text NULL DEFAULT NULL,
        `vendor_name` varchar(255) NULL DEFAULT NULL,
        `delivered_qty` int(11) NULL DEFAULT NULL,
        `price_paid` decimal(12,4) NULL DEFAULT NULL,
        `customer_name` varchar(500) NULL DEFAULT NULL,
        `item_price` decimal(12,4) NULL DEFAULT NULL,
        `order_id` int(255) NULL DEFAULT NULL,
        `increment_id` varchar(255) NULL DEFAULT NULL,
        `qty_ordered` int(11) NULL DEFAULT NULL,
        `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`product_history_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
    );
    $installer->run("
            INSERT INTO `tsht_product_history_report`(`product_history_id`, `transaction_date`,`sku`,`name`,`po_increment_id`, `option`, `vendor_name`, `delivered_qty`, `price_paid`, `customer_name`, `item_price`, `order_id`, `increment_id`, `qty_ordered`) SELECT null as `product_history_id`, `po`.`delivery_date` AS `transaction_date`, `po_item`.`sku`, `attr_pname`.`value` AS `name`, `po`.`po_increment_id`, `po_item`.`option`, `po_supplier`.`name` AS `vendor_name`, `po_item`.`delivered_qty`, `po_item`.`price` AS `price_paid`, null AS `customer_name`, null AS `item_price`, null AS `order_id`, null AS `increment_id`, null AS `qty_ordered` FROM `tsht_purchase_order` AS `po`
            LEFT JOIN `tsht_purchase_order_item` AS `po_item` ON po.po_id = po_item.po_id
            LEFT JOIN `tsht_purchase_supplier` AS `po_supplier` ON po.supplier_id = po_supplier.supplier_id
            INNER JOIN `tsht_catalog_product_entity_varchar` AS `attr_pname` ON attr_pname.entity_id = po_item.product_id and attr_pname.attribute_id = 71 and attr_pname.entity_type_id = 4 and attr_pname.store_id = 0
            LEFT JOIN `tsht_sales_flat_order_item` AS `order_item` ON order_item.created_at = po.created_at 
            UNION ALL
            SELECT null as `product_history_id`, `o`.`created_at` AS `transaction_date`, `order_item`.`sku`, `order_item`.`name`, null AS `po_increment_id`, `order_item`.`product_options` AS `option`, null AS `vendor_name`, null AS `delivered_qty`, null AS `price_paid`, CONCAT(o.customer_firstname,' ' ,o.customer_lastname) AS `customer_name`, `order_item`.`price` AS `item_price`, `o`.`entity_id` AS `order_id`, `o`.`increment_id`, `order_item`.`qty_ordered` FROM `tsht_sales_flat_order` AS `o` LEFT JOIN `tsht_sales_flat_order_item` AS `order_item` ON o.entity_id = order_item.order_id
            "
    );
}

$installer->endSetup();
