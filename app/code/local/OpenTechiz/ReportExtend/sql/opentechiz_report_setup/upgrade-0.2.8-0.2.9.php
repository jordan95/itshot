<?php
$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE `tsht_order_status_change_log` CHANGE `order_id` `order_id` VARCHAR(50) 
    NULL COMMENT 'order id';
");

$installer->endSetup();