<?php

$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('old_system_rma')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('old_system_rma'))
        ->addColumn('old_rma_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'RMA Id')
        ->addColumn('old_invoice_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
            'nullable' => true,
            'default'  => 0
        ), 'old invoice id')
        ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            'default'  => 0
        ), 'order id')
        ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            'default'  => 0
        ), 'customer id')
        ->addColumn('product_description', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => true,
        ), 'product')
        ->addColumn('qty', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
            'nullable' => false,
            'default'  => 0
        ), 'qty')
        ->addColumn('amount', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
            'nullable' => false,
            'default'  => 0
        ), 'amount')
        ->addColumn('is_paid', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            'default'  => 0
        ), 'paid')
        ->addColumn('return_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
            'nullable' => false,
            'default'  => 0
        ), 'type');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();