<?php

$installer = $this;
$installer->startSetup();

$installer->run("  
   ALTER TABLE tsht_purchase_order_item 
   ADD INDEX `po_order_item_sku_index` (`sku`)
");
$installer->endSetup();
