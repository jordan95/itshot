<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE tsht_product_history_log
    ADD `inventory_item_id`
    integer
    NULL
");

$installer->endSetup();
