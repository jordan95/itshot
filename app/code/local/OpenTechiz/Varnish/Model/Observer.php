<?php

class OpenTechiz_Varnish_Model_Observer
{

    public function predispatchVarnish($observer)
    {
        if(Mage::helper('custom')->isFromUsCountry()){
                setcookie("version", "US", time() + (86400 * 7200), "/");
        }else{
            if(Mage::getModel('core/cookie')->get("version")){
                Mage::getModel('core/cookie')->delete('version');
            }
            
        }
    }
}