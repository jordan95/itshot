<?php

class OpenTechiz_PartialpaymentExtended_Block_Sales_Order_Partialpayment extends Mage_Core_Block_Template
{

    protected $_order;
    protected $_source;

    public function initTotals()
    {
        /** @var $parent Mage_Adminhtml_Block_Sales_Order_Invoice_Totals */
        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();
        $this->_source = $parent->getSource();
        
        $this->_addPartialpayment();

    }
    
     public function getSource()
    {
        return $this->_source;
    }
    
        public function getStore()
    {
        return $this->_order->getStore();
    }
    
        protected function _addPartialpayment($after='grand_total')
    {
        $taxTotal = new Varien_Object(array(
            'code'      => 'partialpayment',
            'block_name'=> $this->getNameInLayout()
        ));
        $this->getParentBlock()->addTotal($taxTotal, $after);
        return $this;
    }
    
    public function getOrder()
    {
        if ($this->_order === null) {
            if ($this->hasData('order')) {
                $this->_order = $this->_getData('order');
            } elseif (Mage::registry('current_order')) {
                $this->_order = Mage::registry('current_order');
            } elseif ($this->getParentBlock()->getOrder()) {
                $this->_order = $this->getParentBlock()->getOrder();
            }
        }
        return $this->_order;
    }

    public function setOrder($order)
    {
        $this->_order = $order;
        return $this;
    }

    public function displayPrices($baseAmount, $amount)
    {
        return $this->helper('adminhtml/sales')->displayPrices($this->getOrder(), $baseAmount, $amount);
    }

}
