<?php

class OpenTechiz_PartialpaymentExtended_Block_Sales_Order_Total extends Milople_Partialpayment_Block_Sales_Order_Total
{

    public function initTotals()
    {
        $partialpayment = Mage::getModel('partialpayment/partialpayment')->load($this->getOrder()->getId(), 'order_id');
        if (!$partialpayment->getId()) {
            return $this;
        }
        return parent::initTotals();
    }

}
