<?php

class OpenTechiz_PartialpaymentExtended_Block_Adminhtml_Sales_Order_Create_Billing_Method_Form extends Mage_Adminhtml_Block_Sales_Order_Create_Billing_Method_Form
{
    public function getMethods()
    {
        $methods = parent::getMethods();
        $helper = Mage::helper('partialpayment/partialpayment');
        $payment_method = $helper->getAvaiablePaymentMethodLayaway();

        $params = $this->getRequest()->getParams();
        $controller = $this->getRequest()->getControllerName();
        $action = $this->getRequest()->getActionName();

        //check method rule in partial payment edit
        if($controller == 'partialpayment' && $action == 'edit'){
            if(isset($params['id'])){
                $ppId = $params['id'];
                $installemnts = Mage::getModel('partialpayment/installment')->getCollection()
                    ->addFieldToFilter('partial_payment_id', $ppId)
                    ->addFieldToFilter('installment_status', 'Paid')
                    ->setOrder('installment_id', 'ASC');
                $firstInstallemnt = $installemnts->getFirstItem();
                $paymentMethod = $firstInstallemnt->getPaymentMethod();

                //if first payment is not in layaway method list, allow all methods
                if(!in_array($paymentMethod, $payment_method)) {
                    return $methods;
                }
            }
        }

        //skip check method rule in order create layaway backend
        if($helper->quoteIsPartialPayment() && $controller == 'sales_order_create') {
            return $methods;
        }

        if(!$helper->isEnabled() || !$payment_method || !$helper->quoteIsPartialPayment()){
            return $methods;
        }

        $temp = array();
        foreach ($methods as $v){
            if(in_array($v->getCode(), $payment_method)){
                $temp[] = $v;
            }
        }
        return $temp;
    }
}
