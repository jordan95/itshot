<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		jordantran 
* @package		OpenTechiz_PartialpaymentExtended
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/
class OpenTechiz_PartialpaymentExtended_Block_Adminhtml_Partialpayment_Grid extends Milople_Partialpayment_Block_Adminhtml_Partialpayment_Grid
{
	protected function _prepareCollection()
	{
		$filter_param = $this->getRequest()->getParam('filter');
		//get detaful table name with prefix, if prefix is there in database
		$sales_flat_order = Mage::getSingleton('core/resource')->getTableName('sales_flat_order');
		$collection = Mage::getModel('partialpayment/partialpayment')->getCollection();
		$collection->getSelect()->joinLeft($sales_flat_order, 'main_table.order_id = '.$sales_flat_order.'.entity_id',array('increment_id','customer_firstname','customer_lastname', 'customer_email','order_currency_code','base_currency_code','created_at','status',));
		$collection->setOrder('partial_payment_id','DESC');
		$this->setCollection($collection);
		if ($this->getCollection()) {

            $this->_preparePage();

            $columnId = $this->getParam($this->getVarNameSort(), $this->_defaultSort);
            $dir      = $this->getParam($this->getVarNameDir(), $this->_defaultDir);
            $filter   = $this->getParam($this->getVarNameFilter(), null);
            if(isset($filter_param)){
        		if (is_null($filter)) {
	                $filter = $this->_defaultFilter;
	            }

	            if (is_string($filter)) {
	                $data = $this->helper('adminhtml')->prepareFilterString($filter);
	                $this->_setFilterValues($data);
	            }
	            else if ($filter && is_array($filter)) {
	                $this->_setFilterValues($filter);
	            }
	            else if(0 !== sizeof($this->_defaultFilter)) {
	                $this->_setFilterValues($this->_defaultFilter);
	            }
            }
            

            if (isset($this->_columns[$columnId]) && $this->_columns[$columnId]->getIndex()) {
                $dir = (strtolower($dir)=='desc') ? 'desc' : 'asc';
                $this->_columns[$columnId]->setDir($dir);
                $this->_setCollectionOrder($this->_columns[$columnId]);
            }

            if (!$this->_isExport) {
                $this->getCollection()->load();
                $this->_afterLoadCollection();
            }
        }

        return $this;
	}
	protected function _prepareColumns()
	{	
		parent::_prepareColumns();
//		$this->addColumn('partial_payment_status', array(
//            'header'    => Mage::helper('partialpayment')->__('Status'),
//            'align'     => 'center',
//            'width'     => '80px',
//            'index'     => 'status',
//			'type'	  => 'options',
//			'options' =>  array(
//							'pending' => 'Pending',
//							'processing' => 'Processing',
//							'canceled' => 'Canceled',
//							'complete' => 'Completed',
//							'payment_review' => 'Payment Review',
//							'layawayorder_canceled' => 'Cancel Layaway Order'
//							),
//		));

        $this->addColumn('partial_payment_status', array(
            'header'    => Mage::helper('partialpayment')->__('Status'),
            'align'     => 'center',
            'width'     => '80px',
            'index'     => 'status',
			'type'	  => 'options',
			'options' =>  array(
							'pending' => 'Pending',
							'processing' => 'Processing',
							'canceled' => 'Canceled',
							'complete' => 'Completed',
							'payment_review' => 'Payment Review',
							'layawayorder_canceled' => 'Cancel Layaway Order'
							),
            'renderer' => 'OpenTechiz_PartialpaymentExtended_Block_Adminhtml_Partialpayment_Renderer_Status',
            'filter_condition_callback' => array($this, '_statusFilter')
		));
		
     	return $this;
	}

    protected function _statusFilter($collection, $column)
    {
        $status = $column->getFilter()->getValue();

        if($status == 'complete') {
            $collection->addFieldToFilter('main_table.remaining_amount', 0);
        }else{
            $collection->addFieldToFilter(Mage::getSingleton('core/resource')->getTableName('sales_flat_order').'.status', $status)
                ->addFieldToFilter('main_table.remaining_amount', array('neq' => 0));
        }

        return $this;
    }

    public function getRowUrl($row)
	{			
	  	return $this->getUrl('*/*/edit', array('id' => $row->getPartialPaymentId()));
	}

}