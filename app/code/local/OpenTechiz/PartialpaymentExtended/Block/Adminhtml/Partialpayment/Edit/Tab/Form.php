<?php
class OpenTechiz_PartialpaymentExtended_Block_Adminhtml_Partialpayment_Edit_Tab_Form extends Milople_Partialpayment_Block_Adminhtml_Partialpayment_Edit_Tab_Form
{
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $this->setTemplate('opentechiz/partialpayment/edit_partially_paid_order.phtml');
        return $this;
    }
}