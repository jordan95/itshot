<?php

class OpenTechiz_PartialpaymentExtended_Block_Adminhtml_Partialpayment_Edit extends Milople_Partialpayment_Block_Adminhtml_Partialpayment_Edit
{

    public function __construct()
    {
        parent::__construct();
        $this->_addButton('markaspaid', array(
            'label' => Mage::helper('adminhtml')->__('Mask as Paid'),
            'class' => 'save',
            'onclick' => 'maskAsPaid(\''
            . Mage::helper('core')->jsQuoteEscape(
                    Mage::helper('adminhtml')->__('Are you sure you want to do this?')
            )
            . '\', \''
            . $this->getMaskAsPaidUrl()
            . '\')',
        ));
        $this->_formScripts[] = "
            function maskAsPaid(message, url) {
                if( confirm(message) ) {
                    editForm.submit('" . $this->getMaskAsPaidUrl() . "');
                }
                return false;
            }";
    }

    public function getMaskAsPaidUrl()
    {
        return $this->getUrl('*/*/maskAsPaid', array(
                    $this->_objectId => $this->getRequest()->getParam($this->_objectId),
                    Mage_Core_Model_Url::FORM_KEY => $this->getFormKey()
        ));
    }

}
