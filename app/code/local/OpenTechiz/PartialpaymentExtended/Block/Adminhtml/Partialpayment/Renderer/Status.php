<?php

class OpenTechiz_PartialpaymentExtended_Block_Adminhtml_Partialpayment_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $options = array(
            'pending' => 'Pending',
            'processing' => 'Processing',
            'canceled' => 'Canceled',
            'complete' => 'Completed',
            'payment_review' => 'Payment Review',
            'layawayorder_canceled' => 'Cancel Layaway Order'
        );
        if ($row->getRemainingAmount() == 0) {
            return 'Completed';
        } else {
            return isset($options[$row->getStatus()]) ? $options[$row->getStatus()] : $row->getStatus();
        }
    }
}
