<?php

class OpenTechiz_PartialpaymentExtended_Block_Checkout_Total_Renderer_Paid extends Mage_Checkout_Block_Total_Default
{

    protected function _toHtml()
    {
        $partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
        if ($partialpaymentHelper->isPartialPaymentForWholecart() && !Mage::getSingleton('core/session')->getAllowPartialPayment()) {
            return '';
        }
        return parent::_toHtml();
    }

}
