<?php

class OpenTechiz_PartialpaymentExtended_Helper_Partialpayment extends Milople_Partialpayment_Helper_Partialpayment
{

    public function getActivPaymentMethods($order_id)
    {
        $payment_method = new Milople_Partialpayment_Block_PartialpaymentInstallment();
        $paymentMethods = $payment_method->getMethods();
        $methods = array();
        $avaiable_payments = explode(',', Mage::getStoreConfig('partialpayment/general_settings/avaiable_payment_methods'));
        foreach ($paymentMethods as $paymentMethod) {
            $paymentCode = $paymentMethod->getCode();
            if(!in_array($paymentCode, $avaiable_payments)){
                continue;
            }
            if ($paymentMethod->canUseCheckout() == 1):
                $paymentTitle = Mage::getStoreConfig('payment/' . $paymentCode . '/title');
                # And followed by your code
                $methods[$paymentCode] = array(
                    'label' => $paymentTitle,
                    'value' => $paymentCode,
                );
            endif;
        }

        return $methods;
    }
    
    public function getMessageNotifyPaymentDeclined(){
        return Mage::getStoreConfig('partialpayment/general_settings/msg_notify_payment_declined');
    }

    public function quoteIsPartialPayment() {
        return $this->isEnabled() && Mage::getModel('partialpayment/calculation')->getAmountToBePaidLater();
    }
    
    public function getAvaiablePaymentMethodLayaway() {
        $payments_config = Mage::getStoreConfig('partialpayment/general_settings/avaiable_payment_methods');
        if(!$payments_config){
            return false;
        }
        return explode(',', $payments_config);
    }
    //get partial payment is allow by product on cart page
    public function getAllowPartialPaymentFromInfo($product)
    {       

        $infoBuyRequest = $product->getOptionByCode('info_buyRequest');
        //check infoBuyRequest null will return false in order to avoid error 500.
        if(!is_null($infoBuyRequest)){
            $buyRequest = new Varien_Object(unserialize($infoBuyRequest->getValue()));

            if($buyRequest['super_product_config']['product_type']=="grouped")
            {
                $session = Mage::getSingleton('admin/session');
                if ($session->isLoggedIn()) 
                {
                    $buyRequest->setAllowPartialPayment($buyRequest['allow_partial_payment']);
                }
                else
                {
                    $buyRequest->setAllowPartialPayment(Mage::getSingleton('core/session')->getData($buyRequest['super_product_config']['product_id']));
                }
            }
            return $buyRequest->getAllowPartialPayment();
        }
        $data = "Quote ID:".$product->getQuoteId();
        Mage::log($data,null,'check_not_get_infobuyrequest.log',true);
        return false;
        
    }
    public function convertOrderCurrencyAmount($price,$currentCurrency ='')
    {
         

        $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
        if ($baseCurrencyCode != $currentCurrency && $currentCurrency!="") {
            $getCurrencyRates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, $currentCurrency);
            $getcurrentCurrencyRate = $getCurrencyRates[$currentCurrency];
            $price = str_replace( ',', '', $price );
            $price = $price/$getcurrentCurrencyRate;
            $price = str_replace( ',', '', $price );
            $price = Mage::app()->getStore()->roundPrice($price);
        }
        return $price ;
    }

    // rewrite to prevent convert currency
    public function convertToCurrentCurrencyAmount ($price)
    {
        return $price;
    }
}
