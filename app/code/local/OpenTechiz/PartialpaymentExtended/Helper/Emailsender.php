<?php

class OpenTechiz_PartialpaymentExtended_Helper_Emailsender extends Milople_Partialpayment_Helper_Emailsender
{

    protected function getEmailCcForInstallmentAutoCaptureFailure()
    {
        return Mage::getStoreConfig('partialpayment/email_notification/installment_auto_capture_failure_email_cc', Mage::app()->getStore());
    }

    protected function getEmailCcForInstallmentFailure()
    {
        return Mage::getStoreConfig('partialpayment/email_notification/installment_failure_email_cc', Mage::app()->getStore());
    }

    public function sendInstallmentAutoCaptureFailureMail($customerName, $customer_email, $incrementId, $installment_amount, $partial_payment_id, $currency_code, $is_last_try)
    {
        $this->_debug('Start Send Installment Auto Capture Failure Mail');
        $partial_payment_helper = Mage::helper('partialpayment/partialpayment');
        try {
            if (!$partial_payment_helper->installmentAutoCaptureFailureEmail()) {
                $this->_debug('Enable Installment Auto Capture Failure Email: NO');
                return;
            }
            $storeId = $this->getStoreId();
            // Mail Data
            $msg = NULL;

            if (!$is_last_try) {
                $msg = '<p style="color:#000000; font-size:12px; margin-bottom: 20px;">Maintain sufficient balance in your credit card or check for the credit card expiry. <strong> <a href= "' . Mage::getBaseUrl() . '">Contact us</a></strong> if you find any problem.</p><p style="color:#000000; font-size:12px; margin-bottom: 20px;">We will re-attempt installment payment capture after two days. OR <strong><a href= "' . Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id") . '">logging into your account</a></strong> and pay installment now.</p>';
            } else {
                $msg = '<p style="color:#000000; font-size:12px; margin-bottom: 20px;">Please make sure you make a manual payment for your instalment to avoid cancellation of this order by logging into your account <a href= "' . Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id") . '">here</a>.</p>';
            }
            $data = array();
            $data['brand_label'] = $partial_payment_helper->getBrandLabel();
            $data['customer_name'] = $customerName;
            $data['order_id'] = $incrementId;
            $data['store_url'] = Mage::getBaseUrl();
            $data['user_partial_payment'] = Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id");
            $data['logo_src'] = $this->getLogoSrc();
            $data['logo_alt'] = $this->getLogoAlt();
            $data['installment_amount'] = $partial_payment_helper->getCurrencySymbol($currency_code) . $partial_payment_helper->setNumberFormat($installment_amount);
            $data['message'] = $msg;

            // Email Template 
            $translate = Mage::getSingleton('core/translate');
            $translate->setTranslateInline(false);
            $mailTemplate = Mage::getModel('core/email_template');
            $copyTo = $partial_payment_helper->emailcc();
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($customer_email, $customerName);

            $sender = $partial_payment_helper->emailsender();
            $template = $partial_payment_helper->installmentAutoCaptureFailureEmailTempalte();
            $this->_debug('Store ID:' . $storeId);
            $this->_debug('Customer Email:' . $customer_email);
            if ($copyTo && $partial_payment_helper->installmentAutoCaptureFailureEmail() == 2) {
                // Add bcc to customer email
                $ccEmails = explode(",", $copyTo);
                foreach ($ccEmails as $ccEmail) {
                    $emailInfo->addBcc($ccEmail);
                }
                $this->_debug('Email CC:' . join(', ', $ccEmails));
            } else if ($partial_payment_helper->installmentAutoCaptureFailureEmail() == 3) {
                // Add bcc to customer email
                $ccEmails = explode(",", $this->getEmailCcForInstallmentAutoCaptureFailure());
                foreach ($ccEmails as $ccEmail) {
                    $emailInfo->addBcc($ccEmail);
                }
                $this->_debug('Email CC:' . join(', ', $ccEmails));
            }
            $mailTemplate->addBcc($emailInfo->getBccEmails());
            $mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
                    ->sendTransactional(
                            $template, $sender, $emailInfo->getToEmails(), $emailInfo->getToNames(), $data, $storeId
            );

            $translate->setTranslateInline(true);
            if (!$mailTemplate->getSentSuccess()) {
                $this->_debug('Send Mail: NO');
            }
            $translate->setTranslateInline(true);
        } catch (Exception $e) {
            $this->_debug($e->getTraceAsString());
        }
        $this->_debug('End Send Installment Auto Capture Failure Mail');
    }

    public function sendInstallmentFailureMail($installment_id) //$customerName, $customer_email, $incrementId, $installment_amount, $partial_payment_id
    {
        $partial_payment_helper = Mage::helper('partialpayment/partialpayment');
        try {
            if (!$partial_payment_helper->installmentFailureEmail()) {
                return;
            }
            $installment_data = Mage::getModel('partialpayment/installment')->load($installment_id);
            $partialpayment_data = Mage::getModel('partialpayment/partialpayment')->load($installment_data->getPartialPaymentId());
            $order_data = Mage::getModel('sales/order')->load($partialpayment_data->getOrderId());
            $customerName = $order_data->getCustomerFirstname();
            $customer_email = $order_data->getCustomerEmail();
            $incrementId = $order_data->getIncrementId();
            $currency_code = $order_data->getOrderCurrency()->getCurrencyCode();
            $installment_amount = $partial_payment_helper->setNumberFormat($installment_data->getInstallmentAmount());
            $partial_payment_id = $installment_data->getPartialPaymentId();

            $storeId = $this->getStoreId();

            // Mail Data
            $data = array();
            $data['brand_label'] = $partial_payment_helper->getBrandLabel();
            $data['customer_name'] = $customerName;
            $data['order_id'] = $incrementId;
            $data['store_url'] = Mage::getBaseUrl();
            $data['user_partial_payment'] = Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id");
            $data['logo_src'] = $this->getLogoSrc();
            $data['logo_alt'] = $this->getLogoAlt();
            $data['installment_amount'] = $partial_payment_helper->getCurrencySymbol($currency_code) . $installment_amount;

            // Email Template 
            $translate = Mage::getSingleton('core/translate');
            $translate->setTranslateInline(false);
            $mailTemplate = Mage::getModel('core/email_template');
            $copyTo = $partial_payment_helper->emailcc();
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($customer_email, $customerName);
            $sender = $partial_payment_helper->emailsender();
            $template = $partial_payment_helper->installmentFailureEmailTempalte();
            if ($copyTo && $partial_payment_helper->installmentFailureEmail() == 2) {
                // Add bcc to customer email
                $ccEmails = explode(",", $copyTo);
                foreach ($ccEmails as $ccEmail) {
                    $emailInfo->addBcc($ccEmail);
                }
            } else if ($partial_payment_helper->installmentFailureEmail() == 3) {
                $ccEmails = explode(",", $this->getEmailCcForInstallmentFailure());
                foreach ($ccEmails as $ccEmail) {
                    $emailInfo->addBcc($ccEmail);
                }
            }
            $mailTemplate->addBcc($emailInfo->getBccEmails());
            $mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
                    ->sendTransactional(
                            $template, $sender, $emailInfo->getToEmails(), $emailInfo->getToNames(), $data, $storeId
            );

            $translate->setTranslateInline(true);
            if (!$mailTemplate->getSentSuccess()) {
                throw new Exception();
            }
            $translate->setTranslateInline(true);
            return;
        } catch (Exception $e) {
            return;
        }
    }

    protected function _debug($message)
    {
        Mage::log($message, null, 'partialpayment-cron.log', true);
    }
    public function sendReminderEmptyProfile($order_id) 
    {
        $partial_payment_helper = Mage::helper('partialpayment/partialpayment');
        try {
            // Email Template 
            $translate = Mage::getSingleton('core/translate');
            $translate->setTranslateInline(false);
            $mailTemplate = Mage::getModel('core/email_template');
            $emailInfo = Mage::getModel('core/email_info');
            $sender = $partial_payment_helper->emailsender();
            $storeId = $this->getStoreId();
            $senderName  = Mage::getStoreConfig('trans_email/ident_' . $sender . '/name', $storeId);
            $receiveEmail = Mage::getStoreConfig('partialpayment/email_notification/email_receive_empty_profile', $storeId);
            $emailInfo->addTo($receiveEmail, $senderName);
            
            $data = array();
            $data['order_id'] = $order_id;

            $template = "partialpayment_email_notification_empty_profile_reminder_template";
            $mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
                    ->sendTransactional(
                            $template, $sender, $receiveEmail, $senderName, $data, $storeId
            );

            $translate->setTranslateInline(true);
            if (!$mailTemplate->getSentSuccess()) {
                throw new Exception();
            }
            $translate->setTranslateInline(true);
        } catch (Exception $e) {
            throw new Exception();
        }
    }

}
