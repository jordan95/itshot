<?php

class OpenTechiz_PartialpaymentExtended_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function refreshOrderTotals(Mage_Sales_Model_Order $order)
    {
        if(!is_null(Mage::registry('add_installment_amount_payment_grid'))) {
            return;
        }
        $this->_reloadTotals($order);
        $transaction = Mage::getModel('core/resource_transaction')
                ->addObject($order);
        $paritalpayment = Mage::getModel('partialpayment/partialpayment')->load($order->getId(), 'order_id');
        if ($paritalpayment->getId()) {
            $order->setPaidAmount($order->getTotalPaid())
                    ->setPaidAmount($order->getBaseTotalPaid())
                    ->setRemainingAmount($order->getTotalDue())
                    ->setBaseRemainingAmount($order->getBaseTotalDue());
            $invoices = $order->getInvoiceCollection();
            if ($invoices) {
                foreach ($order->getInvoiceCollection() as $invoice) {
                    $invoice->setPaidAmount($order->getTotalPaid())
                            ->setPaidAmount($order->getBaseTotalPaid())
                            ->setRemainingAmount($order->getTotalDue())
                            ->setBaseRemainingAmount($order->getBaseTotalDue());
                    $invoice->save();
                }
            }

            if (!in_array($order->getState(), array('canceled', 'closed', 'completed'))) {
                $this->_reloadInstallments($order, $paritalpayment);
            } else {
                $this->_cancelInstallments($order, $paritalpayment);
            }
            $this->_reloadProducts($order, $paritalpayment);

            $paritalpayment
                    ->setTotalAmount($order->getGrandTotal())
                    ->setPaidAmount($order->getPaidAmount())
                    ->setRemainingAmount($order->getRemainingAmount());
            $transaction->addObject($paritalpayment);
        }
        $transaction->save();
    }

    public function _reloadTotals(Mage_Sales_Model_Order $order)
    {
        $total_paid = 0;
        $base_total_paid = 0;
        $total_refunded = 0;
        $base_total_refunded = 0;

        $grand_total = $order->getGrandTotal();
        $base_grand_total = $order->getBaseGrandTotal();

        // Update total paid
        $invoices = $order->getInvoiceCollection();
        if ($invoices) {
            foreach ($order->getInvoiceCollection() as $invoice) {
                if ($invoice->getState() == Mage_Sales_Model_Order_Invoice::STATE_CANCELED) {
                    continue;
                }
                $paymentRecordCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                        ->addFieldToFilter('invoice_id', $invoice->getId());
                foreach ($paymentRecordCollection as $paymentRecord) {
                    $base_total_paid += $paymentRecord->getTotal();
                }
            }
            $total_paid = Mage::helper('directory')->currencyConvert($base_total_paid, "USD", $order->getOrderCurrencyCode());

            $paid_amount = $total_paid;
            $base_paid_amount = $base_total_paid;

            $order->setTotalPaid($total_paid)
                    ->setBaseTotalPaid($base_total_paid);
        }

        // Update total refunded
        $creditmemos = $order->getCreditmemosCollection();
        if ($creditmemos) {
            foreach ($creditmemos as $creditmemo) {
                if ($creditmemo->getState() == Mage_Sales_Model_Order_Creditmemo::STATE_CANCELED) {
                    continue;
                }
                $paymentRecordCollection = Mage::getModel('cod/paymentgrid')->getCollection()
                        ->addFieldToFilter('credit_memo_id', $creditmemo->getId());
                foreach ($paymentRecordCollection as $paymentRecord) {
                    $base_total_refunded += abs($paymentRecord->getTotal());
                }
            }

            $total_refunded = Mage::helper('directory')->currencyConvert($base_total_refunded, "USD", $order->getOrderCurrencyCode());
            $order->setTotalRefunded($total_refunded)
                    ->setBaseTotalRefunded($base_total_refunded)
                    ->setTotalOfflineRefunded($total_refunded)
                    ->setBaseTotalOfflineRefunded($base_total_refunded);
        }
    }

    public function _cancelInstallments($order, $partialpayment)
    {
        $installments = Mage::getModel('partialpayment/installment')
                ->getCollection()
                ->addFieldToFilter('partial_payment_id', $partialpayment->getId())
                ->addFieldToFilter('installment_status', "Remaining");
        if ($installments->count() > 0) {
            foreach ($installments as $installment) {
                $installment->setInstallmentStatus('Canceled');
                $installment->save();
            }
        }
    }

    protected function _reloadInstallments($order, $partialpayment)
    {
        $total_paid = 0;
        $total_installments = 0;
        $paid_installments = 0;
        $total_remaining = 0;
        $remaining_installments = 0;

        $lastInstallmentPaid = null;
        $lastInstallmentRemaining = null;

        $installments = Mage::getModel('partialpayment/installment')
                ->getCollection()
                ->addFieldToFilter('partial_payment_id', $partialpayment->getId());

        $installmentsPaid = [];
        $installmentsRemaining = [];

        foreach ($installments as $installment) {
            if ($installment->getInstallmentStatus() == "Paid") {
                $total_paid += $installment->getInstallmentAmount();
                $paid_installments += 1;
                $lastInstallmentPaid = $installment;
                array_unshift($installmentsPaid, $installment);
            } else if (in_array($installment->getInstallmentStatus(), array("Remaining", "Failed", "Canceled"))) {
                $total_remaining += $installment->getInstallmentAmount();
                $remaining_installments += 1;
                $lastInstallmentRemaining = $installment;
                array_unshift($installmentsRemaining, $installment);
            }
        }

        // Paid
//        if ($total_paid < $order->getPaidAmount()) {
//            if ($lastInstallmentPaid) {
//                $lastInstallmentPaid->setInstallmentAmount($lastInstallmentPaid->getInstallmentAmount() + ($order->getPaidAmount() - $total_paid));
//                $lastInstallmentPaid->save();
//            } else {
//                $installment_due_date = date("Y-m-d", $order->getCreatedAt());
//                $installment_paid_date = date("Y-m-d", $order->getCreatedAt());
//
//                $data = array(
//                    'partial_payment_id' => $paritalpayment->getId(),
//                    'installment_amount' => $order->getPaidAmount(),
//                    'installment_due_date' => $installment_due_date,
//                    'installment_paid_date' => $installment_paid_date,
//                    'installment_status' => 'Paid'
//                );
//                Mage::getModel('partialpayment/installment')
//                        ->setData($data)
//                        ->save();
//                $paid_installments += 1;
//            }
//        } else {
//            while ($total_paid > $order->getPaidAmount()) {
//                $lastIntPaid = current($installmentsPaid);
//                if ($total_paid - $lastIntPaid->getInstallmentAmount() >= $order->getPaidAmount()) {
//                    $lastIntPaid->delete();
//                    $total_paid -= $lastIntPaid->getInstallmentAmount();
//                    $paid_installments -= 1;
//                } else {
//                    if ($lastIntPaid->getInstallmentAmount() > $total_paid - $order->getPaidAmount()) {
//                        $lastIntPaid->setInstallmentAmount($lastIntPaid->getInstallmentAmount() - ($total_paid - $order->getPaidAmount()));
//                        $lastIntPaid->save();
//                        $total_paid = $order->getPaidAmount();
//                    } else {
//                        $lastIntPaid->delete();
//                        $total_paid -= $lastIntPaid->getInstallmentAmount();
//                        $paid_installments -= 1;
//                    }
//                }
//                next($installmentsPaid);
//            }
//        }
        // Remaining
        $total_remaining = round($total_remaining, 2);
        $remainingAmount = round($order->getRemainingAmount(), 2);
        if ($total_remaining < $remainingAmount) {
            $extraAmt = $remainingAmount - $total_remaining;
            if ($lastInstallmentRemaining) {
                $lastInstallmentAmount = round($lastInstallmentRemaining->getInstallmentAmount(), 2);
                $lastInstallmentDueDate = $lastInstallmentRemaining->getInstallmentDueDate();
                $nextInstallmentDueDate = strtotime($lastInstallmentDueDate . "+1 months");
                if (!empty($installmentsRemaining[1])) {
                    $baseInstallmentRemaining = $installmentsRemaining[1];
                    $baseInstallmentAmount = round($baseInstallmentRemaining->getInstallmentAmount(), 2);
                    if ($lastInstallmentAmount + $extraAmt > $baseInstallmentAmount) {
                        $extraAmt -= ($baseInstallmentAmount - $lastInstallmentAmount);
                        $lastInstallmentAmount = $baseInstallmentAmount;
                        while ($extraAmt > 0) {
                            if ($extraAmt > $baseInstallmentAmount) {
                                $this->createInstallmentRemaining($partialpayment->getId(), $baseInstallmentAmount, $nextInstallmentDueDate);
                                $remaining_installments += 1;
                                $extraAmt -= $baseInstallmentAmount;
                            } else {
                                $this->createInstallmentRemaining($partialpayment->getId(), $extraAmt, $nextInstallmentDueDate);
                                $remaining_installments += 1;
                                $extraAmt = 0;
                            }
                            $nextInstallmentDueDate = strtotime("+1 months", $nextInstallmentDueDate);
                        }
                    } else {
                        $lastInstallmentAmount += $extraAmt;
                    }
                    $lastInstallmentRemaining->setInstallmentAmount($lastInstallmentAmount);
                    $lastInstallmentRemaining->save();
                } else {
                    while ($extraAmt > 0) {
                        if ($extraAmt > $lastInstallmentAmount) {
                            $this->createInstallmentRemaining($partialpayment->getId(), $lastInstallmentAmount, $nextInstallmentDueDate);
                            $remaining_installments += 1;
                            $extraAmt -= $lastInstallmentAmount;
                        } else {
                            $this->createInstallmentRemaining($partialpayment->getId(), $extraAmt, $nextInstallmentDueDate);
                            $remaining_installments += 1;
                            $extraAmt = 0;
                        }
                        $nextInstallmentDueDate = strtotime("+1 months", $nextInstallmentDueDate);
                    }
                }
            } else {
                if ($lastInstallmentPaid) {
                    $lastInstallmentAmount = round($lastInstallmentPaid->getInstallmentAmount(), 2);
                    $lastInstallmentDueDate = $lastInstallmentPaid->getInstallmentDueDate();
                    $nextInstallmentDueDate = strtotime($lastInstallmentDueDate . "+1 months");
                    if (!empty($installmentsPaid[1])) {
                        $baseInstallmentPaid = $installmentsPaid[1];
                        $lastInstallmentAmount = $baseInstallmentPaid->getInstallmentAmount();
                    }
                    while ($extraAmt > 0) {
                        if ($extraAmt > $lastInstallmentAmount) {
                            $this->createInstallmentRemaining($partialpayment->getId(), $lastInstallmentAmount, $nextInstallmentDueDate);
                            $remaining_installments += 1;
                            $extraAmt -= $lastInstallmentAmount;
                        } else {
                            $this->createInstallmentRemaining($partialpayment->getId(), $extraAmt, $nextInstallmentDueDate);
                            $remaining_installments += 1;
                            $extraAmt = 0;
                        }
                        $nextInstallmentDueDate = strtotime("+1 months", $nextInstallmentDueDate);
                    }
                } else {
                    $nextInstallmentDueDate = strtotime("+1 months");
                    $lastInstallmentAmount = round($extraAmt / 6, 2);
                    while ($extraAmt > 0) {
                        if ($extraAmt > $lastInstallmentAmount) {
                            $this->createInstallmentRemaining($partialpayment->getId(), $lastInstallmentAmount, $nextInstallmentDueDate);
                            $remaining_installments += 1;
                            $extraAmt -= $lastInstallmentAmount;
                        } else {
                            $this->createInstallmentRemaining($partialpayment->getId(), $extraAmt, $nextInstallmentDueDate);
                            $remaining_installments += 1;
                            $extraAmt = 0;
                        }
                        $nextInstallmentDueDate = strtotime("+1 months", $nextInstallmentDueDate);
                    }
                }
            }
        } else {
            while ($total_remaining > $remainingAmount) {
                $lastIntRemaining = current($installmentsRemaining);
                if (!$lastIntRemaining) {
                    break;
                }
                $lastIntRemainingAmt = round($lastIntRemaining->getInstallmentAmount(), 2);
                if ($total_remaining - $lastIntRemainingAmt >= $remainingAmount) {
                    $lastIntRemaining->delete();
                    $total_remaining -= $lastIntRemainingAmt;
                    $remaining_installments -= 1;
                } else {
                    if ($lastIntRemainingAmt > $total_remaining - $remainingAmount) {
                        $lastIntRemaining->setInstallmentAmount($lastIntRemainingAmt - ($total_remaining - $remainingAmount));
                        $lastIntRemaining->save();
                        $total_remaining = $remainingAmount;
                    } else {
                        $lastIntRemaining->delete();
                        $total_remaining -= $lastIntRemainingAmt;
                        $remaining_installments -= 1;
                    }
                }
                next($installmentsRemaining);
            }
        }

        $total_installments = $paid_installments + $remaining_installments;

        $partialpayment
                ->setTotalInstallments($total_installments)
                ->setPaidInstallments($paid_installments)
                ->setRemainingInstallments($remaining_installments);
    }

    protected function createInstallmentRemaining($partialpayment_id, $remaining_amount, $installment_due_date = 0)
    {
        if ($remaining_amount <= "0.01") {
            return;
        }
        if ($installment_due_date <= time()) {
            $installment_due_date = strtotime("+1 months");
        }
        $installment_due_date = date("Y-m-d", $installment_due_date);

        $data = array(
            'partial_payment_id' => $partialpayment_id,
            'installment_amount' => $remaining_amount,
            'installment_due_date' => $installment_due_date,
            'installment_paid_date' => "",
            'installment_status' => 'Remaining'
        );
        Mage::getModel('partialpayment/installment')
                ->setData($data)
                ->save();
    }

    protected function _reloadProducts($order, $paritalpayment)
    {
        $calculationModel = $this->getPartialpaymentCalculation();
        $partialpaymentHelper = $this->getPartialpaymentHelper();

        $quoteItemIds = [];
        foreach ($order->getAllVisibleItems() as $orderItem) {
            if ($orderItem->getRowTotalInclTax() == 0 || $orderItem->getStatusId() == Mage_Sales_Model_Order_Item::STATUS_REFUNDED || !$orderItem->getQuoteItemId()) {
                continue;
            }
            $_product = Mage::getModel('catalog/product')->load($orderItem->getProductId());
            $product_total_amount = Mage::helper('tax')->getPrice($_product, $orderItem->getPrice(), false) * $orderItem->getQtyOrdered();
            $product_total_installments = $paritalpayment->getTotalInstallments();
            $product_paid_amount = ($product_total_amount / $product_total_installments) * $paritalpayment->getPaidInstallments();
            $product_paid_installments = $paritalpayment->getPaidInstallments();
            $product_remaining_installments = $product_total_installments - $product_paid_installments;
            $product_remaining_amount = $product_total_amount - $product_paid_amount;
            $product_downpayment_amount = 0.1 * $product_total_amount;

            $productModelData = array(
                'partial_payment_id' => $paritalpayment->getId(),
                'sales_flat_order_item_id' => $orderItem->getQuoteItemId(),
                'downpayment' => $partialpaymentHelper->setNumberFormat($product_downpayment_amount),
                'total_installments' => $product_total_installments,
                'paid_installments' => $product_paid_installments,
                'remaining_installments' => $product_remaining_installments,
                'total_amount' => $partialpaymentHelper->setNumberFormat($product_total_amount),
                'paid_amount' => $partialpaymentHelper->setNumberFormat($product_paid_amount),
                'remaining_amount' => $partialpaymentHelper->setNumberFormat($product_remaining_amount)
            );
            Mage::getModel('partialpayment/product')->load($orderItem->getQuoteItemId(), 'sales_flat_order_item_id')
                    ->addData($productModelData)
                    ->save();
            $quoteItemIds[] = $orderItem->getQuoteItemId();
        }
        if (count($quoteItemIds) > 0) {
            $partialPaymentProducts = Mage::getModel('partialpayment/product')
                    ->getCollection()
                    ->addFieldToFilter('partial_payment_id', $paritalpayment->getId())
                    ->addFieldToFilter('sales_flat_order_item_id', array('nin' => $quoteItemIds));
            if ($partialPaymentProducts->count() > 0) {
                foreach ($partialPaymentProducts as $partialProduct) {
                    $partialProduct->delete();
                }
            }
        }
    }

    public function getPartialpaymentCalculation()
    {
        return Mage::getModel("partialpayment/calculation");
    }

    public function getPartialpaymentHelper()
    {
        return Mage::helper('partialpayment/partialpayment');
    }

}
