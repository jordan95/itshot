<?php
require_once(Mage::getModuleDir('controllers','Milople_Partialpayment').DS.'Adminhtml'.DS.'PartialpaymentController.php');
class OpenTechiz_PartialpaymentExtended_Adminhtml_PartialpaymentController extends Milople_Partialpayment_Adminhtml_PartialpaymentController
{
    const INSTALLMENT_STATUS            = 'Canceled';
    public function maskAsPaidAction()
    {
        if (!$this->_validateFormKey()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__('Invalid form key.'));
        }
        $request = $this->getRequest();
        try {
            Mage::getModel('partialpaymentextended/partialpayment')->maskAsPaid(
                    $request->getParam('id'), $request->getPost('installment_id', array())
            );
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('partialpayment')->__('Partialpayment has been mask as paid successfully.'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__($e->getMessage()));
        }
        $this->_redirect('*/*/edit', array('id' => $request->getParam('id')));
    }
    public function cancellayawayAction()
    {   
        if (!$this->_validateFormKey()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__('Invalid form key.'));
        }
        $request = $this->getRequest();
        $id  = $request->getParam('id');
        // date_default_timezone_set(Mage::app()->getStore()->getConfig('general/locale/timezone'));
        $installment_paid_date = Mage::getModel('core/date')->gmtDate('Y-m-d');
        if(isset($id)){
            try {
                $partial_payment_data = Mage::getModel('partialpayment/partialpayment')->load($id);
                if($partial_payment_data){
                    $partial_payment_id = $partial_payment_data->getPartialPaymentId();
                    $partial_payment_installment_data = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id',$partial_payment_id)->addFieldToFilter('installment_status', array('neq' => 'Paid'));
                        if($partial_payment_installment_data->count() > 0){
                            foreach($partial_payment_installment_data->getData() as $installment){
                                $installment_data = Mage::getModel('partialpayment/installment')->load($installment['installment_id']);
                                $installment_data->setInstallmentStatus(self::INSTALLMENT_STATUS);
                                $installment_data->setInstallmentPaidDate($installment_paid_date);
                                $installment_data->save();
                            }
                            $order = Mage::getModel('sales/order')->load($partial_payment_data->getOrderId());
                            if($order){
                                $order->setData('state', "canceled");
                                $order->setStatus("layawayorder_canceled");
                                $history = $order->addStatusHistoryComment('This order marked as layaway order - cancelled.', false);
                                $history->setIsCustomerNotified(false);
                                $order->save();

                                //log stock
                                foreach ($order->getAllItems() as $item) {
                                   
                                    $stock = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', $item->getSku())->getFirstItem();
                                    $qty = Mage::helper("opentechiz_inventory")->countReleasedItem($item->getId());
                                    if($qty > 0){
                                        Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                                            'qty' => $qty,
                                            'on_hold' => '0',
                                            'type' => 9,
                                            'stock_id' => $stock->getId(),
                                            'qty_before_movement' => $stock->getQty(),
                                            'on_hold_before_movement' => $stock->getOnHold(),
                                            'sku' => $stock->getSku(),
                                            'order_id'=>$order->getId()
                                        ));
                                    }
                                }

                                //cancel all order item
                                $items = $order->getAllItems();
                                foreach ($items as $item){
                                    $item->setQtyCanceled($item->getQtyOrdered() - $item->getQtyRefunded())->save();
                                }

                                Mage::dispatchEvent('layawayorder_canceled_after', array('order' => $order));
                                $this->_getSession()->addSuccess(
                                    $this->__('The order has been cancelled.')
                                );
                            }else{
                                $this->_getSession()->addSuccess(
                                    $this->__('No order is matched.')
                                );
                            }
                                
                        }else{
                            $this->_getSession()->addSuccess(
                                $this->__('The order has been pay full so could not canceled.')
                            );
                        }
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__($e->getMessage()));
            }
        }    
        $this->_redirect('*/*/edit', array('id' => $request->getParam('id')));
       
    }
    protected function _capturePayment($installments_to_pay, $order, $installment_amount, $paymentInfo, $partial_payment_id, $post) {
                                $action = Mage::app()->getRequest()->getParam('action');
        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
        $invoice->setState(Mage_Sales_Model_Order_Invoice::STATE_OPEN);

        try
        {
            if(is_array($paymentInfo) && count($paymentInfo) && isset($paymentInfo['method']))
            {
                $invoice->setBaseGrandTotal($installment_amount);
                $invoice->setGrandTotal($installment_amount);
                $invoice->getOrder()->setTotalDue($installment_amount);
                $capturePayment = Mage::getModel('partialpayment/sales_order_capture_payment');
                $capturePayment->setOrder($invoice->getOrder());
                $capturePayment->importData($paymentInfo);  
                $capturePayment->setAmountOrdered($installment_amount);
                $capturePayment->setBaseAmountOrdered($installment_amount);
                $capturePayment->setShippingAmount(0);
                $capturePayment->setBaseShippingAmount(0);
                $capturePayment->setAmountAuthorized($installment_amount);
                $capturePayment->setBaseAmountAuthorized($installment_amount);
                $clonedInvoice = clone $invoice;
                $invoice->getOrder()->addRelatedObject($capturePayment);
                
                if($paymentInfo['method'] == 'ewayrapid_ewayone')
                {
                    $action = strtolower(Mage::getStoreConfig('payment/ewayrapid_general/payment_action'));
                }
                
                if($paymentInfo['method'] == 'authorizenet')
                {
                    if(isset($post['save_for_future']) && $post['save_for_future']==1)
                    {
                        $paymentModel = Mage::getModel('partialpayment/payment_method_authorizenet');
                        $service = $paymentModel->getWebService();
                        //$service->setPayment($capturePayment->getPayment());
                        $subscription = new Varien_Object();
                        $subscription->setStoreId($order->getStoreId());
                        $service->setSubscription($subscription);
                        
                        // Create customer profile
                        $data = $service->createCIMAccountOnInstallmentPayment($order,$paymentInfo);
                        if(!empty($data->customerProfileId) && !empty($data->customerPaymentProfileId)){
                            Mage::getModel('partialpayment/partialpayment')->load($partial_payment_id)
                                    ->setAutoCaptureProfileId($data->customerProfileId)
                                    ->setAutoCapturePaymentProfileId($data->customerPaymentProfileId)
                                    ->save();
                        }
                    }

                    $capturePayment->capture($clonedInvoice);
                    $capturePayment->pay($clonedInvoice);
                    $transaction_id ='';
                    if(Mage::registry('get-transaction-id-installment')){
                        $transaction_id = Mage::registry('get-transaction-id-installment');
                    }
                    
                }else{
                    # Compatible pay flow pro payment method
                    if (strtolower(Mage::getStoreConfig('payment/' . $paymentInfo['method'] . '/payment_action')) == 'authorize' || strtolower(Mage::getStoreConfig('payment/' . $paymentInfo['method'] . '/payment_action')) == 'authorization' || $action == 'authorize') {
                        $capturePayment->_authorize(true, $installment_amount);
                    }
                    else {
                        if($capturePayment->canCapture())
                        {
                            if($paymentInfo['method'] != 'sagepaydirectpro')
                            {
                                $capturePayment->capture($clonedInvoice);
                                $capturePayment->pay($clonedInvoice);
                            }
                        }
                        else
                        {
                            $capturePayment->pay($clonedInvoice);
                        }
                    }
                }
                Mage::register('add_installment_amount_payment_grid', $installment_amount);
                foreach ($installments_to_pay as $installment) {
                    if($transaction_id !=''){
                        Mage::getModel('partialpayment/calculation')->setInstallmentSuccessData($installment['installment_id'], $order->getEntityId(), $partial_payment_id, $paymentInfo['method'],$transaction_id);
                    }else{
                        Mage::getModel('partialpayment/calculation')->setInstallmentSuccessData($installment['installment_id'], $order->getEntityId(), $partial_payment_id, $paymentInfo['method']);
                    }
                    
                }
                return true;
            }
        } 
        catch(Exception $e)
        {
            foreach ($installments_to_pay as $installment) {
                $installment_data = Mage::getModel('partialpayment/installment')->load($installment['installment_id']);
                $installment_data->setInstallmentStatus('Failed');
                $installment_data->save();
                Mage::helper('partialpayment/emailsender')->sendInstallmentFailureMail($installment['installment_id']);
            }
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__("Gateway Error: Cannot pay installment now. Please try again later."));
            $this->_redirect('*/*/edit', array('id' => $partial_payment_id));
            return false;
        }
    }

    public function splitPaymentAction(){
        $this->loadLayout();
        $this->_setActiveMenu('partialpayment/items');
        $this->_addContent($this->getLayout()->createBlock('partialpaymentextended/adminhtml_splitpayment_edit'))
            ->_addLeft($this->getLayout()->createBlock('partialpaymentextended/adminhtml_splitpayment_edit_tabs'));
        $this->renderLayout();
    }
}
