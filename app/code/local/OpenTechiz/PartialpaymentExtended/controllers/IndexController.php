<?php

require_once Mage::getModuleDir('controllers', 'Milople_Partialpayment') . DS . 'IndexController.php';

class OpenTechiz_PartialpaymentExtended_IndexController extends Milople_Partialpayment_IndexController
{

    protected function _capturePayment($model, $pmethod)
    {
        $post = $this->getRequest()->getPost();
        $model = Mage::getModel('partialpayment/partialpayment')->load($post['partial_payment_id']);
        $isnew = $model->getTotalInstallment() < $model->getPaidInstallment() ? true : false;
        if ($isnew) {
            return false;
        }
        $orderid = $model->getOrderId();
        $order = Mage::getModel("sales/order")->load($orderid);
        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
        $invoice->setState(Mage_Sales_Model_Order_Invoice::STATE_OPEN);
        $paymentInfo = $this->getRequest()->getPost('payment', array());
        $amount = 0;
        try {
            if (is_array($paymentInfo) && count($paymentInfo) && isset($paymentInfo['method'])) {
                foreach ($post['installment_id'] as $installment_id) {//pay each installment
                    $amount += $post[$installment_id];
                }

                if ($amount > 0) {
                    $invoice->setBaseGrandTotal($amount);
                    $invoice->setGrandTotal($amount);
                    $invoice->getOrder()->setTotalDue($amount);
                    $capturePayment = Mage::getModel('partialpayment/sales_order_capture_payment');
                    $capturePayment->setOrder($invoice->getOrder());
                    $capturePayment->importData($paymentInfo);
                    $capturePayment->setAmountOrdered($amount);
                    $capturePayment->setBaseAmountOrdered($amount);
                    $capturePayment->setShippingAmount(0);
                    $capturePayment->setBaseShippingAmount(0);
                    $capturePayment->setAmountAuthorized($amount);
                    $capturePayment->setBaseAmountAuthorized($amount);

                    $clonedInvoice = clone $invoice;

                    $invoice->getOrder()->addRelatedObject($capturePayment);
                    if ($paymentInfo['method'] == 'authorizenet') {
                        if (isset($post['save_for_future']) && $post['save_for_future'] == 1) {
                            $paymentModel = Mage::getModel('partialpayment/payment_method_authorizenet');
                            $service = $paymentModel->getWebService();
                            $subscription = new Varien_Object();
                            $subscription->setStoreId($order->getStoreId());
                            $service->setSubscription($subscription);

                            // Create customer profile
                            $data = $service->createCIMAccountOnInstallmentPayment($order,$paymentInfo);
                            if(!empty($data->customerProfileId) && !empty($data->customerPaymentProfileId)){
                                Mage::getModel('partialpayment/partialpayment')->load($post['partial_payment_id'])
                                        ->setAutoCaptureProfileId($data->customerProfileId)
                                        ->setAutoCapturePaymentProfileId($data->customerPaymentProfileId)
                                        ->save();
                            }
                            $orderPayment = Mage::getModel('sales/order_payment')->load($orderid,'parent_id');
                            if($orderPayment){
                                    $orderPayment->setCcType( $paymentInfo['cc_type']);
                                    $orderPayment->setCcExpMonth( $paymentInfo['cc_exp_month']);
                                    $orderPayment->setCcExpYear( $paymentInfo['cc_exp_year']);
                                    $orderPayment->setCcNumberEnc($orderPayment->encrypt( $paymentInfo['cc_number']));
                                    $orderPayment->save();
                            }
                        }
                        $capturePayment->capture($clonedInvoice);
                        $capturePayment->pay($clonedInvoice);
                        Mage::register('add_installment_amount_payment_grid', $amount);
                    }else {
                        /* Compatible pay flow pro payment method */
                        if (strtolower(Mage::getStoreConfig('payment/' . $paymentInfo['method'] . '/payment_action')) == 'authorize' || strtolower(Mage::getStoreConfig('payment/' . $paymentInfo['method'] . '/payment_action')) == 'authorization') {
                            $capturePayment->_authorize(true, $amount);
                        } else {
                            if ($capturePayment->canCapture()) {
                                if ($pmethod != 'sagepaydirectpro') {
                                    $capturePayment->capture($clonedInvoice);
                                    $capturePayment->pay($clonedInvoice);
                                }
                            } else {
                                $capturePayment->pay($clonedInvoice);
                            }
                        }
                    }
                    return true;
                }
            }
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            $this->getMaillogHelper()->sendMailToAdmin('Itshot.com: Partialpayment #' . $model->getId() . ' cannot pay installment', $e->getMessage());
            $this->_redirect('*/*/');
        }
    }

    public function reloadPriceAction()
    {
        $response = new Varien_Object();
        $response->setData([
            'errno' => 1,
            'msg' => '404! Not found.'
        ]);
        if ($this->getRequest()->isPost() && $this->getRequest()->isAjax()) {
            $price = $this->getRequest()->getPost('price', false);
            $productId = $this->getRequest()->getPost('product_id', false);
            if ($price && $productId) {
                $_product = Mage::getModel('catalog/product')->load($productId);
                if ($_product->getId()) {
                    $partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
                    $calculationModel = Mage::getModel("partialpayment/calculation");
                    $base_price = $partialpaymentHelper->convertCurrencyAmount($price);
                    $dpayment = $partialpaymentHelper->convertToCurrentCurrencyAmount($calculationModel->getDownPaymentAmount($base_price, $_product));
                    $response->setErrno(0);
                    $response->setDownPaymentAmount(Mage::helper('core')->currency($dpayment, true, false));
                    $response->setDownPaymentText(Mage::helper('core')->__(sprintf('Layaway for %s', Mage::helper('core')->currency($dpayment, true, false))));
                    $response->setMsg('Success');
                } else {
                    $response->setMsg('Product is unavaiable.');
                }
            }
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($response->toJson());
    }

    protected function getMaillogHelper()
    {
        return Mage::helper('maillog');
    }
    public function overviewAction() {  
        $post = $this->getRequest()->getPost();
        $currentUrl = $this->getRequest()->getPost('refer', Mage::getBaseUrl());
        if(!isset($post['installment_id'])){
            Mage::getSingleton('core/session')->addError("Please select an installment.");
            Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
            Mage::app()->getFrontController()->getResponse()->sendResponse(); exit;
        }
        //check compare to current amount
        if(!empty($post['installment_id'])){
            foreach ($post['installment_id'] as $installment_id) {
                $installment = Mage::getModel('partialpayment/installment')->load($installment_id);
                $trueAmount = round($installment->getInstallmentAmount(), 2);
                $postedAmount = round($post[$installment_id], 2);
                if($trueAmount != $postedAmount){
                    $message = 'Installment amount has changed, please try again.';
                    Mage::getSingleton('core/session')->addError($message);
                    Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
                    Mage::app()->getFrontController()->getResponse()->sendResponse(); exit;
                }
            }
        }
        
        if (!$this->_validateFormKey()) {
            Mage::getSingleton('core/session')->addError(Mage::helper('checkout')->__('Invalid Form Key'));
            return $this->_redirectUrl($currentUrl);
        }
        $modules = Mage::getConfig()->getNode('modules')->children();
        $modulesArray = (array)$modules;
        $message = $this->validateSecondInstallmentForm($post);
        if ($message != '') {
            Mage::getSingleton('core/session')->addError($message);
            Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
            Mage::app()->getFrontController()->getResponse()->sendResponse(); exit;
        }
        $partial_payment = Mage::getModel('partialpayment/partialpayment')->load($post['partial_payment_id']);
                if(!$partial_payment->getId()){
                    return;
                }
                Mage::register('current_partialpayment', $partial_payment);
        Mage::getSingleton('core/session')->setPartialPaymentId($post['partial_payment_id']);
        $partialpaymentHelper = Mage::helper('partialpayment/partialpayment');

        if(isset($post['payment']['cc_type'])) {
            $ccType = $post['payment']['cc_type'];
        }
        
        $partial_payment_amount = 0;
        $incrementId = $partial_payment['order_id'];
        try
        {   
            if(isset($modulesArray['Milople_Partialpaymentpaytm']))
            {
                if(strpos($post['payment']['method'], 'paytm_cc') !== false)
                {
                    foreach ($post['installment_id'] as $instllmentid) {
                        $installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
                        $partial_payment_amount += $installmentData->getInstallmentAmount();
                        $installmentids[] = $instllmentid;
                    }
                    $order = Mage::getModel('sales/order')->load($partial_payment['order_id']);
                    $installmentId = implode( "-", $installmentids );
                 
                    Mage::getSingleton('core/session')->setInstallmentId($installmentId);
                    Mage::getSingleton('core/session')->setInstallmentOrderId($order->getIncrementId());
                    
                    $post['refer'] = base64_encode($post['refer']);
                    Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('paytm/processing/installmentPaymentForm',array('incrementId'=>$incrementId,'amount'=>$partial_payment_amount,'postData'=>serialize($post))));
                    Mage::app()->getFrontController()->getResponse()->sendResponse(); 
                    exit;
                }
            }
            if(isset($modulesArray['Milople_Partialpaymentsagepay']))
            {
                if(strpos($post['payment']['method'], 'sagepaydirectpro') !== false)
                {
                    $directHelper = Mage::helper('partialpaymentsagepay/partialpaymentsagepaydirect');
                    $response = $directHelper->payWithSagepayDirectPro($post, $partial_payment->getOrderId());

                    try 
                    {
                        $error_message = '';
                        if(empty($response) || !isset($response['Status'])) 
                        {
                            $error_message = $this->__('Sage Pay is not available at this time. Please try again later.');
                        }

                        switch($response['Status'])
                        {
                            case 'FAIL':
                                $error_message = $this->__($response['StatusDetail']);
                                break;
                            case 'FAIL_NOMAIL':
                                $error_message = $this->__($response['StatusDetail']);
                                break;
                            case 'INVALID':
                                $error_message = $this->__('INVALID. %s', $response['StatusDetail']);
                                break;
                            case 'MALFORMED':
                                $error_message = $this->__('MALFORMED. %s', $response['StatusDetail']);
                                break;
                            case 'ERROR':
                                $error_message = $this->__('ERROR. %s', $response['StatusDetail']);
                                break;
                            case 'REJECTED':
                                $error_message = $this->__('REJECTED. %s', $response['StatusDetail']);
                                break;
                            case '3DAUTH':
                                $error_message = $this->__($response['StatusDetail']);
                                break;
                            default:
                                break;
                        }
                        if (!empty($error_message))
                        {
                            Mage::getSingleton('core/session')->addError($error_message);
                            Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
                            Mage::app()->getFrontController()->getResponse()->sendResponse(); exit; 
                        }
                    } 
                    catch (Exception $e) 
                        {
                            Mage::throwException($this->__('%s', $e->getMessage())
                        );
                    }
                }           
                if(strpos($post['payment']['method'], 'sagepayserver') !== false)
                {
                    $partialpaymentsagepayHelper = Mage::helper('partialpaymentsagepay/partialpaymentsagepay');
                    $response = $partialpaymentsagepayHelper->payWithSagepayServer($post, $incrementId);
                    try {
                        $error_message = '';
                        if(empty($response) || !isset($response['Status'])) {
                            $error_message = $this->__('Sage Pay is not available at this time. Please try again later.');
                        }

                        switch($response['Status']) {
                            case 'FAIL':
                                $error_message = $this->__($response['StatusDetail']);
                                break;
                            case 'FAIL_NOMAIL':
                                $error_message = $this->__($response['StatusDetail']);
                                break;
                            case 'INVALID':
                                $error_message = $this->__('INVALID. %s', $response['StatusDetail']);
                                break;
                            case 'MALFORMED':
                                $error_message = $this->__('MALFORMED. %s', $response['StatusDetail']);
                                break;
                            case 'ERROR':
                                $error_message = $this->__('ERROR. %s', $response['StatusDetail']);
                                break;
                            case 'REJECTED':
                                $error_message = $this->__('REJECTED. %s', $response['StatusDetail']);
                                break;
                            case '3DAUTH':
                                $error_message = $this->__($response['StatusDetail']);
                                break;
                            default:
                                break;
                        }
                        if (!empty($error_message)) {
                            
                            Mage::getSingleton('core/session')->addError($error_message);
                            Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
                            Mage::app()->getFrontController()->getResponse()->sendResponse(); exit; 
                        }
                        $html = '<html><body>';
                        $html .= $partialpaymentsagepayHelper->getSagepayServerForm($response);
                        $html .= '<script type="text/javascript">document.getElementById("sagepay_server_checkout").submit();</script>';
                        $html .= '</body></html>';
                        echo $html;
                        exit;
                    } catch (Exception $e) {
                        Mage::throwException(
                           $this->__('%s', $e->getMessage())
                        );
                    }
                }   
            }
            
            // for netgains stripe gateway
            if(strpos($post['payment']['method'], 'stripe') !== false)
            {               
                foreach ($post['installment_id'] as $instllmentid) 
                {
                    $installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
                    $partial_payment_amount += $installmentData->getInstallmentAmount();
                    $installmentids[] = $instllmentid; 
                }
                $installmentids = implode("-",$installmentids);
                $payment = $post['payment'];                
                try{
                    $response = Mage::getModel('partialpaymentstripe/stripe')->callInstallmentApi($installmentids,$post['partial_payment_id'],$payment,$partial_payment_amount,'authorize');
                    if($response['transaction_id'] && $response['status'] == 1)
                    {
                        $total_installment=0;
                        $incrementId = 0;
                        foreach($post['installment_id'] as $instllment_id) 
                        {
                            $calculationModel=Mage::getModel("partialpayment/calculation");
                            $partialpaymentData = Mage::getModel('partialpayment/partialpayment')->load($post['partial_payment_id']);
                            $orderId = $partialpaymentData->getOrderId();
                            $salesOrderCollection = Mage::getModel('sales/order')->load($orderId);
                            $incrementId = $salesOrderCollection->getIncrementId();                         
                            $calculationModel->setInstallmentSuccessData($instllment_id,$orderId,$post['partial_payment_id'],$post['payment']['method']);
                            $total_installment++;
                        }
                    if($total_installment == 1)
                    {
                       Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installment of order #%s has been paid successfully.",$total_installment,$incrementId));
                    }
                    else
                    {
                       Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installments of order #%s have been paid successfully.",$total_installment,$incrementId));
                    }
                                
                    Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
                    Mage::app()->getFrontController()->getResponse()->sendResponse(); 
                    }
                }
                 catch (Exception $e) {
                        Mage::throwException(
                           $this->__('%s', $e->getMessage())
                        );
                    }
                exit;
            }
            
            if(isset($modulesArray['Milople_PartialpaymentPaymentsense']))
            {               
                if(strpos($post['payment']['method'], 'Paymentsense') !== false)
                {
                    $amount = 0;
                    foreach ($post['installment_id'] as $instllmentid) 
                    {
                     $installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
                     $amount += $installmentData->getInstallmentAmount();
                     $installmentids[] = $instllmentid; 
                    }
                    
                    $request = Mage::getModel('partialpaymentPaymentsense/paymentsense');
                    $form = new Varien_Data_Form();
                    
                    $model = Mage::getModel('Paymentsense/direct');
                    $szActionURL = $model->getConfigData('hostedpaymentactionurl');
                    $form->setAction($szActionURL)
                        ->setId('HostedPaymentForm')
                        ->setName('HostedPaymentForm')
                        ->setMethod('POST')
                        ->setUseContainer(true);
                        
                    foreach ($request->getInstalmentFormFields($incrementId,$amount,implode("-",$installmentids),$post['partial_payment_id']) as $field=>$value)
                    {
                        $form->addField($field, 'hidden', array('name'=>$field, 'value'=>$value));
                    }
                    
                    $html = '<html><body>';
                    $html.= $this->__('You will be redirected to a secure payment page in a few seconds.');
                    $html.= $form->toHtml();
                    $html.= '<script type="text/javascript">document.getElementById("HostedPaymentForm").submit();</script>';
                    $html.= '</body></html>';
                    echo $html;
                    exit;
                }
            }
            
            // for paypal plus gateway 
            if(strpos($post['payment']['method'], 'iways_paypalplus_payment') !== false)
            {
               foreach ($post['installment_id'] as $instllmentid) 
                {
                    $installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
                    $partial_payment_amount += $installmentData->getInstallmentAmount();
                    $installmentids[] = $instllmentid; 
                }
                $installmentids = implode("-",$installmentids);
                $website = Mage::app()->getStore()->getWebsiteId();
                $token = Mage::getModel('partialpaymentpaypalplus/api')->setApiContextInstallment($website);
                $order = Mage::getModel('sales/order')->load($partial_payment['order_id']);
                $quoteId = $order->getQuoteId();
                $quote = Mage::getModel('sales/quote')->load($quoteId); 
                $currencyCode = $order->getOrderCurrencyCode(); 
                $installmentAmount = sprintf("%0.2f", $partial_payment_amount);
                        
                if($token)
                {
                  $redirectURL = Mage::getModel('partialpaymentpaypalplus/api')->getInstallmentPaymentExperience($quote,$installmentAmount,$currencyCode,$installmentids,$post['partial_payment_id'],$partial_payment['order_id']);
                
                  if ($redirectURL) { ?>
                       <script src="https://www.paypalobjects.com/webstatic/ppplus/ppplus.min.js" type="text/javascript">
                        
                        var payment = {};                     
                             window.ppp = PAYPAL.apps.PPP(
                             {                           
                               approvalUrl: "<?php echo $redirectURL; ?>",
                               placeholder: "ppplus",
                               mode: "<?php echo Mage::getStoreConfig('iways_paypalplus/api/mode'); ?>",
                               useraction: "commit",
                               buttonLocation: "outside",
                               country:"DE",
                               language:"<?php echo Mage::getStoreConfig('general/locale/code'); ?>",
                               onContinue: function () {
                                   payment.save()
                               },
                             });
                        window.startPPP();   
                         </script>  <?php 
                          try {
                            $responsePayPal = Mage::getModel('partialpaymentpaypalplus/api')->patchInstallmentPayment($quote,$currencyCode,$installmentAmount);
                            if ($responsePayPal) {
                                $result['success'] = true;
                            } else {
                                $result['success'] = false;
                                $result['error'] = false;
                                $result['error_messages'] = $this->__('Please select an other payment method.');

                            }
                        } catch (\Exception $e) {
                            $result['success'] = false;
                            $result['error'] = false;
                            $result['error_messages'] = $e->getMessage();
                        }
                        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                        if($result)         
                        {
                           ?>
                           <button type="submit" id="continueButton" onclick="window.ppp.doCheckout();"> </button>
                          <script type="text/javascript">                           
                                $("continueButton").click();                        
                           </script>
                            <?php                          
                          $this->getResponse()->setRedirect($redirectURL);            
                        }
                  }           
                  return;
                }               
                exit;               
            }           
            
            if(isset($modulesArray['Milople_Partialpaymentccavenue']))
            {
                if(strpos($post['payment']['method'], 'ccavenuepay') !== false)
                {
                    foreach ($post['installment_id'] as $instllmentid) {
                        $installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
                        $partial_payment_amount += $installmentData->getInstallmentAmount();
                    }
                    $html = '<html><body>';
                    $html .= Mage::helper('partialpaymentccavenue/partialpaymentccavenue')->ccavenueForm($incrementId,$partial_payment_amount,$post);
                    $html .= '<script type="text/javascript">document.getElementById("ccavenuepay_checkout").submit();</script>';
                    $html .= '</body></html>';
                    echo $html; 
                    exit;
                }
            }
            if(isset($modulesArray['Milople_Partialpaymentpayu']))
            {               
                if(strpos($post['payment']['method'], 'payucheckout_shared') !== false)
                {
                    $amount = 0;
                    foreach ($post['installment_id'] as $instllmentid) 
                    {
                     $installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
                     $amount += $installmentData->getInstallmentAmount();
                     $installmentids[] = $instllmentid; 
                    }
                    
                    $shared = Mage::getModel('partialpaymentpayu/partialpaymentpayu');
                    $form = new Varien_Data_Form();
                    $form->setAction($shared->getPayuCheckoutSharedUrl())
                        ->setId('payucheckout_shared_checkout')
                        ->setName('payucheckout_shared_checkout')
                        ->setMethod('POST')
                        ->setUseContainer(true);
                        
                    foreach ($shared->getInstalmentFormFields($incrementId,$amount,implode("-",$installmentids),$post['partial_payment_id']) as $field=>$value)
                    {
                        $form->addField($field, 'hidden', array('name'=>$field, 'value'=>$value));
                    }

                    $html = '<html><body>';
                    $html.= $this->__('You will be redirected to PayuCheckout in a few seconds.');
                    $html.= $form->toHtml();
                    $html.= '<script type="text/javascript">document.getElementById("payucheckout_shared_checkout").submit();</script>';
                    $html.= '</body></html>';
                    echo $html; 
                    exit;
                }
            }
            $partial_payment_amount =0;
            if($post['payment']['method'] == 'paypal_standard')
            {
                foreach ($post['installment_id'] as $instllmentid) {
                    $installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
                    $partial_payment_amount += $installmentData->getInstallmentAmount();
                    $installmentids[] = $instllmentid; 
                }
                    $partialid = $post['partial_payment_id'];
                    $html = '<html><body>';
                    $html.= $this->__('You will be redirected to PayPal in a few seconds.');
                    $html.= $this->getPaypalForm($incrementId, $partial_payment_amount, Mage::getUrl("partialpayment/standard/success",array('installment_id'=> implode("-",$installmentids),'partial_payment_id'=>$partialid)), Mage::getUrl("partialpayment/standard/cancel",array('installment_id'=> implode("-",$installmentids),'partial_payment_id'=>$partialid)), implode("-",$installmentids));
                    $html.= '<script type="text/javascript">document.getElementById("paypal_standard_checkout").submit();</script>';
                    $html.= '</body></html>';
                    sleep(4);
                    echo $html;

            }
            
            #compatible with paypal express
            if($post['payment']['method'] == 'paypal_express')
            {
                $response = Mage::getModel('paypal/api_nvp')->callDoExpressInstallmentPayment($post['installment_id'],$incrementId);
                if($response && isset($response['TOKEN']))
                {
                    $expressConfig = Mage::getModel('paypal/config');
                    $expressConfig->sandboxFlag = Mage::getStoreConfig("paypal/wpp/sandbox_flag", Mage::app()->getStore()->getStoreId());
                    $url = $expressConfig->getExpressCheckoutStartUrl($response['TOKEN']);
                    $this->getResponse()->setRedirect($url);
                    return;
                }
                else
                {
                    Mage::getSingleton("core/session")->addError(Mage::helper("partialpayment")->__("Payment Failed"));
                    Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
                    Mage::app()->getFrontController()->getResponse()->sendResponse();
                }
                exit;
            }

            if (Mage::getStoreConfig('payment/' . $post['payment']['method'] . '/payment_action') != '') {
                $this->_capturePayment($partial_payment, $post['payment']['method']);
            }

            if($post['payment']['method'] != 'paypal_standard')
            {
                $transaction_id ='';
                if(Mage::registry('get-transaction-id-installment')){
                    $transaction_id = Mage::registry('get-transaction-id-installment');
                }                
                $total_installment=0;
                foreach($post['installment_id'] as $instllment_id) 
                {
                    $calculationModel=Mage::getModel("partialpayment/calculation");
                    if($transaction_id !=''){
                        $calculationModel->setInstallmentSuccessData($instllment_id,$incrementId,$post['partial_payment_id'],$post['payment']['method'],$transaction_id);
                    }else{
                        $calculationModel->setInstallmentSuccessData($instllment_id,$incrementId,$post['partial_payment_id'],$post['payment']['method']);
                    }
                    $total_installment++;
                }
                if($post['payment']['method'] != 'paypal_standard')
                {   
                    $salesOrderCollection = Mage::getModel('sales/order')->load($incrementId,'entity_id');
                    $partial_payment = Mage::getModel('partialpayment/partialpayment')->load($post['partial_payment_id']);
                    
                    if($total_installment == 1)
                     {
                       Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installment of order #%s has been paid successfully.",$total_installment,$salesOrderCollection->getIncrementId()));
                     }
                     else
                     {
                       Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installments of order #%s have been paid successfully.",$total_installment,$salesOrderCollection->getIncrementId()));
                     }
                                
                    Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
                    Mage::app()->getFrontController()->getResponse()->sendResponse(); 
                    
                }
                        
            }
                
        }
        catch (Mage_Core_Exception $e)
        {   Mage::getSingleton('core/session')->addError($e->getMessage());
            Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
            Mage::app()->getFrontController()->getResponse()->sendResponse(); 
        }
    
    }

}
