<?php

class OpenTechiz_PartialpaymentExtended_Model_Partialpayment
{

    public function maskAsPaid($partialpayment_id, $installments = array())
    {
        $installments_paid = [];
        Mage::register('mask_as_paid_register', true);
        $partialpayment = Mage::getModel('partialpayment/partialpayment')->load($partialpayment_id);
        if (!$partialpayment->getId()) {
            throw new Exception('Partailpayment is not exits.');
        }
        $order = Mage::getModel('sales/order')->load($partialpayment->getOrderId());
        if (!$order->getId()) {
            throw new Exception('Order is not exits.');
        }
        $collection = Mage::getModel('partialpayment/installment')->getCollection();
        $collection->getSelect()->reset(Zend_Db_Select::COLUMNS);
        $collection->getSelect()->columns(array(
            'installment_id',
            'installment_status',
            'payment_method'
        ));
        $collection->addFieldToFilter('partial_payment_id', $partialpayment_id);
        if ($collection->count() > 0) {
            $payment_method = 'authorizenet';

            foreach ($collection as $installment) {
                if ($installment->getPaymentMethod()) {
                    $payment_method = $installment->getPaymentMethod();
                }
                if ($installment->getInstallmentStatus() !== 'Paid') {
                    $installments_paid[] = $installment->getId();
                }
            }
            if (count($installments_paid) > 0) {
                $message = '';
                foreach ($installments_paid as $installment_id) {
                    if (count($installments) === 0 || in_array($installment_id, $installments)) {
                        Mage::getModel('partialpayment/calculation')->setInstallmentSuccessData($installment_id, $partialpayment->getOrderId(), $partialpayment->getId(), $payment_method, "", true);
                        $message .= $this->_debug($partialpayment_id, $installment_id) . '<br />';
                    }
                }
                if ($message) {
                    $this->getMaillogHelper()->sendMailToAdmin('Itshot.com: Order number #' . $order->getIncrementId() . ' has been mask as paid successfully', $message);
                }
            }
        }
    }

    protected function _debug($partialpayment_id, $installment_id, $file = 'partialpayment_mask_as_paid.log')
    {
        $message = 'Partialpayment ID: ' . $partialpayment_id . '; Intallment ID: ' . $installment_id . '; Date: ' . date('Y-d-m H:i:s');
        Mage::log($message, null, $file);
        return $message;
    }

    protected function getMaillogHelper()
    {
        return Mage::helper('maillog');
    }

}
