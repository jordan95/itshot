<?php

class OpenTechiz_PartialpaymentExtended_Model_Total_Quote_Credit extends Mirasvit_Credit_Model_Total_Quote_Credit
{

    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $this->_setAddress($address);
        /**
         * Reset amounts
         */
        $this->_setAmount(0);
        $this->_setBaseAmount(0);

        $quote = $address->getQuote();
        if (!$quote->getCreditCollected()) {
            $quote->setBaseCreditAmountUsed(0)
                    ->setCreditAmountUsed(0)
                    ->setCreditCollected(true);
        }


        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }

        /** @var Mirasvit_Credit_Helper_Item $itemHelpder */
        $itemHelpder = Mage::helper('credit/item');

        $baseBalance = $balance = 0;

        if ($quote->getCustomer()->getId()) {
            if ($quote->getUseCredit()) {
                $baseBalance = Mage::getModel('credit/balance')->loadByCustomer($quote->getCustomerId())->getAmount();
                $balance = $quote->getStore()->convertPrice($baseBalance);
            }
        }
        $order = Mage::registry('ordersedit_order');
        if ($order && $order->getCreditAmount() > 0) {
            $baseBalance = $order->getBaseCreditAmount();
            $balance = $order->getCreditAmount();
        }

        $itemsTotal = 0;
        $itemsBaseTotal = 0;

        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach ($items as $item) {
            $itemsBaseTotal += $itemHelpder->calcItemBasePrice($item);
            $itemsTotal += $itemHelpder->calcItemPrice($item);
        }

        $baseBalanceLeft = $baseBalance - $quote->getBaseCreditAmountUsed();
        $balanceLeft = $balance - $quote->getCreditAmountUsed();

        $baseBalanceUsed = $baseBalanceLeft;
        $balanceUsed = $balanceLeft;

        $baseFinalSubtotal = $address->getBaseSubtotalWithDiscount();
        $finalSubtotal = $address->getSubtotalWithDiscount();
        // compatibility with some modules
        if ($baseFinalSubtotal < 0) {
            $baseFinalSubtotal = 0;
        }
        if ($finalSubtotal < 0) {
            $finalSubtotal = 0;
        }

        if ($itemsBaseTotal > $baseFinalSubtotal) {
            $baseFinalSubtotal = $itemsBaseTotal;
            $finalSubtotal = $itemsTotal;
        }

        if ($baseBalanceUsed > $baseFinalSubtotal) {
            $baseBalanceUsed = $baseFinalSubtotal;
            $balanceUsed = $finalSubtotal;
        }

        $baseBalanceTotalUsed = $quote->getBaseCreditAmountUsed() + $baseBalanceUsed;
        $balanceTotalUsed = $quote->getCreditAmountUsed() + $balanceUsed;

        $quote->setBaseCreditAmountUsed($baseBalanceTotalUsed)
                ->setCreditAmountUsed($balanceTotalUsed);

        $address->setBaseCreditAmount($baseBalanceUsed)
                ->setCreditAmount($balanceUsed);

        foreach ($items as $item) {
            if ($itemsBaseTotal > 0) {
                if (!($itemPrice = $itemHelpder->calcItemBasePrice($item))) {
                    continue;
                }
                $baseDiscount = $itemPrice / $itemsBaseTotal * $baseBalanceTotalUsed;
                $discount = $itemPrice / $itemsTotal * $balanceTotalUsed;
                if ($baseDiscount > $itemPrice) {
                    $baseDiscount = $itemPrice;
                }
                if ($discount > $itemPrice) {
                    $discount = $itemPrice;
                }
                $item->setDiscountAmount($item->getDiscountAmount() + $discount);
                $item->setBaseDiscountAmount($item->getBaseDiscountAmount() + $baseDiscount);
            }
        }

        // balance cover shipping price too
        $balanceApplied = $baseBalanceLeft - $balanceTotalUsed;
        if ($balanceApplied > 0) {
            $baseShippingDiscount = $address->getBaseShippingDiscountAmount();
            $shippingDiscount = $address->getShippingDiscountAmount();
            $baseCoverage = $address->getBaseShippingAmount() - $baseShippingDiscount;
            $coverage = $address->getShippingAmount() - $shippingDiscount;
            $baseDelta = 0;
            $delta = 0;
            if (Mage::getModel('credit/config')->isApplyCreditToTax()) {
                $baseCoverage = $address->getBaseShippingInclTax() - $baseShippingDiscount;
                $coverage = $address->getShippingInclTax() - $shippingDiscount;
                if ($balanceApplied < $baseCoverage) {
                    $percent = ($address->getBaseShippingInclTax() - $address->getBaseShippingAmount()) /
                            $address->getBaseShippingAmount();
                    $baseCoverage = $balanceApplied / (1 + $percent);
                    $coverage = $balanceApplied / (1 + $percent);
                }
            }
            if ($baseCoverage > $baseBalanceLeft - $balanceTotalUsed) {
                $baseCoverage = $coverage = $baseBalanceLeft - $balanceTotalUsed;
            }

            $address->setShippingDiscountAmount($coverage - $delta);
            $address->setBaseShippingDiscountAmount($baseCoverage - $baseDelta);

            $this->_addAmount(-1 * ($balanceUsed + $coverage - $delta));
            $this->_addBaseAmount(-1 * ($baseBalanceUsed + $baseCoverage - $baseDelta));

            if (Mage::getModel('credit/config')->isApplyCreditToTax()) {
                $baseCoverage = $address->getBaseShippingInclTax() - $baseShippingDiscount;
                $coverage = $address->getShippingInclTax() - $shippingDiscount;

                if ($baseCoverage > $baseBalanceLeft - $balanceTotalUsed) {
                    $baseCoverage = $coverage = $baseBalanceLeft - $balanceTotalUsed;
                }
            }
            $baseBalanceTotalUsed += $baseCoverage;
            $balanceTotalUsed += $coverage;
        } else {
            $this->_addAmount(-1 * ($balanceUsed));
            $this->_addBaseAmount(-1 * ($baseBalanceUsed));
        }

        $quote->setBaseCreditAmountUsed($baseBalanceTotalUsed)
                ->setCreditAmountUsed($balanceTotalUsed);

        $address->setBaseCreditAmount($baseBalanceTotalUsed)
                ->setCreditAmount($balanceTotalUsed);

        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        if ($address->getCreditAmount()) {
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => Mage::helper('credit')->__('Store Credit'),
                'value' => $address->getTotalAmount($this->getCode()),
            ));
        }

        return $this;
    }

}
