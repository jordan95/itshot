<?php

class OpenTechiz_PartialpaymentExtended_Model_Observer extends Milople_Partialpayment_Model_Observer
{

    public function checkPaymentMethodAllowed(Varien_Event_Observer $obsever)
    {
        //skip check if create order in backend
        if(Mage::app()->getStore()->isAdmin() && Mage::getDesign()->getArea() == 'adminhtml') {
            return;
        }

        $postData = Mage::app()->getRequest()->getPost();
        $payment_method = Mage::helper('checkout')->getAvaiablePaymentMethodLayaway();
        if (isset($postData['payment']['method']) && Mage::helper('partialpayment/partialpayment')->quoteIsPartialPayment() && $payment_method && !in_array($postData['payment']['method'], $payment_method)) {
            Mage::throwException(Mage::helper('checkout')->__('Sorry! Your payment method is not support with the layaway.'));
            exit;
        }
    }

    public function checkCustomerCanPay(Varien_Event_Observer $obsever)
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            Mage::throwException(Mage::helper('checkout')->__('Please login.'));
            exit;
        }
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $request = Mage::app()->getRequest();
        $customerId = $customer->getId();
        $partialpaymentId = $request->getPost('partial_payment_id', false);
        $partialpayment = Mage::getModel('partialpayment/partialpayment')->load($partialpaymentId);
        $referer = Mage::app()->getRequest()->getPost('refer', Mage::getBaseUrl());
        if (!$partialpayment->getId()) {
            Mage::getSingleton('core/session')->addError(Mage::helper('checkout')->__('An error occurred during pay for installments. Please try again.'));
            Mage::app()->getResponse()
                ->setRedirect($referer)
                ->sendResponse();
            exit;
        }

        $order = Mage::getModel('sales/order')->load($partialpayment->getOrderId());
        if (!$order->getId()) {
            Mage::getSingleton('core/session')->addError(Mage::helper('checkout')->__('An error occurred during pay for installments. Please try again.'));
            Mage::app()->getResponse()
                ->setRedirect($referer)
                ->sendResponse();
            exit;
        }

        if ($customer->getId() != $order->getCustomerId()) {
            Mage::getSingleton('core/session')->addError(Mage::helper('checkout')->__('An error occurred during pay for installments. Please try again.'));
            Mage::app()->getResponse()
                ->setRedirect($referer)
                ->sendResponse();
            exit;
        }

        //skip check payment method if create order in backend
        if(Mage::app()->getStore()->isAdmin() && Mage::getDesign()->getArea() == 'adminhtml') {
            return;
        }

        $payment = $request->getPost('payment', array());
        $payment_method = Mage::helper('checkout')->getAvaiablePaymentMethodLayaway();
        if (isset($payment['method']) && $payment_method && !in_array($payment['method'], $payment_method)) {
            Mage::getSingleton('core/session')->addError(Mage::helper('checkout')->__('Sorry! Your payment method is not support with the layaway.'));
            Mage::app()->getResponse()
                ->setRedirect($referer)
                ->sendResponse();
            exit;
        }
    }

    public function AddToCartAfter(\Varien_Event_Observer $observer)
    {
        $partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
        if (!$partialpaymentHelper->isEnabled() || $partialpaymentHelper->isPartialPaymentForWholecart()) {
            return;
        }
        parent::AddToCartAfter($observer);
    }

    public function setWholeCartSession($observer)
    {
        $partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
        if (!$partialpaymentHelper->isEnabled() || !$partialpaymentHelper->isPartialPaymentForWholecart() || !$partialpaymentHelper->isValidCustomer()) {
            return;
        }
        $allow_partial_payment = Mage::app()->getRequest()->getPost('allow_partial_payment', false);
        if (!$allow_partial_payment) {
            $allow_partial_payment = Mage::app()->getRequest()->getQuery('allow_partial_payment', false);
        }
        $optional = $partialpaymentHelper->isAllowFullPayment();
        $isFlexyAllow = $partialpaymentHelper->isAllowFlexyPayment();
        $calculationModel = Mage::getModel("partialpayment/calculation");
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $subTotal = (float) $partialpaymentHelper->convertCurrencyAmount($calculationModel->getSubtotal($quote), "USD");
        $minOrderAmount = (float) $partialpaymentHelper->getMinimumWholecartOrderAmount();

        if ($subTotal < $minOrderAmount || Mage::app()->getRequest()->getPost('remove_layaway_option')) {
            Mage::getSingleton('core/session')->setAllowPartialPayment(0);
            Mage::getSingleton('core/session')->unsetAllowPartialPayment();
        } elseif ($allow_partial_payment > 0 && !$optional && !$isFlexyAllow) {
            Mage::getSingleton('core/session')->setAllowPartialPayment(1);
        }
        Mage::dispatchEvent('after_set_allow_partialpayment', array('quote' => $quote));
    }

    public function setWholeCartSessionInAdmin($observer)
    {
        $partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
        if (!Mage::helper('core')->isModuleEnabled('Milople_Partialpayment') && !$partialpaymentHelper->isEnabled() || !$partialpaymentHelper->isPartialPaymentForWholecart()) {
            return;
        }

        $order = $observer->getOrder();
        if ($order->getPaidAmount() > 0) {
            Mage::getSingleton('core/session')->setAllowPartialPayment(1);
        }
    }

    public function loadPointsSpendingForEditOrder($observer)
    {
        if (!Mage::helper('core')->isModuleEnabled('TBT_Rewards')) {
            return;
        }
        $order = $observer->getOrder();
        $quote = $observer->getQuote();
        if (!Mage::helper('rewards/config')->allowCatalogRulesInAdminOrderCreate()) {
            $quote->updateDisabledEarnings();
            return $this;
        }
        if ($order->hasPointsSpending()) {
            $totalSpentPoints = 0;
            foreach ($order->getTotalSpentPoints() as $_totalSpentPoints) {
                $totalSpentPoints += $_totalSpentPoints;
            }
            $quote->setPointsSpending($totalSpentPoints);
        }
    }

    public function removeWholeCartSession($observer)
    {
        $partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
        if (!$partialpaymentHelper->isEnabled() || !$partialpaymentHelper->isPartialPaymentForWholecart()) {
            return;
        }
        $quote = $observer->getEvent()->getQuote();
        $subTotal = $partialpaymentHelper->convertCurrencyAmount($calculationModel->getSubtotal($quote));
        $minOrderAmount = 0;
        $minOrderAmount = $partialpaymentHelper->getMinimumWholecartOrderAmount();
        if ($quote->getItemsCount() == 0 || $subTotal < $minOrderAmount) {
            Mage::getSingleton('core/session')->setAllowPartialPayment(0);
            Mage::getSingleton('core/session')->unsetAllowPartialPayment();
        }
        Mage::dispatchEvent('after_set_allow_partialpayment', array('quote' => $quote));
    }

    public function salesOrderPlaced($observer)//it will store data in partial payment table
    {
        $calculationModel = Mage::getModel("partialpayment/calculation");
        $partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
        $order = $observer->getEvent()->getOrder();
        $partial_payment_id = 0;
        $payment_method = NULL;

        try {
            if ($calculationModel->getAmountToBePaidLater() > 0) {
                $paid_amount = $order->getBasePaidAmount();
                $remaining_amount = $order->getBaseRemainingAmount();

                $order_id = $order->getEntityId();
                $total_amount = $order->getBaseGrandTotal();
                $payment_method = $order->getPayment()->getMethod();

                $paid_installments = 1;
                $total_installments = 0;
                $items = $order->getQuote()->getAllVisibleItems();
                $installmentValue = 0;
                $total_price = 0;
                $profId = NULL;
                $rId = NULL;
                if ($partialpaymentHelper->isPartialPaymentForWholecart()) {
                    if ($partialpaymentHelper->isAllowFlexyPayment()) {
                        $total_installments = Mage::getSingleton('core/session')->getAllowPartialPayment();
                    } else {
                        $total_installments = $partialpaymentHelper->getTotalIinstallments();
                    }
                    $total_price += $order->getBaseSubtotal();
                } else {
                    foreach ($items as $item) {
                        $product = Mage::getModel('catalog/product')->load($item->getProductId());
                        if ($partialpaymentHelper->getAllowPartialPaymentFromInfo($item)) {
                            if ($partialpaymentHelper->isAllowFlexyPayment($product) && $partialpaymentHelper->getTotalIinstallments($product) > $total_installments) {
                                $total_installments = $partialpaymentHelper->getAllowPartialPaymentFromInfo($item);
                            } else if ($partialpaymentHelper->getTotalIinstallments($product) > $total_installments) {
                                $total_installments = $partialpaymentHelper->getTotalIinstallments($product);
                            }
                        }
                    }
                }
                $remaining_installments = $total_installments - $paid_installments;

                $payment_method = $order->getPayment()->getMethod();

                if ($payment_method != 'paypal_standard' || $payment_method != 'ccavenuepay' || $payment_method != 'payucheckout_shared') {
                    $partial_payment_status = 'Processing';
                } else {
                    $partial_payment_status = 'Pending';
                }

                if ($payment_method == 'paypal_standard' || $payment_method == 'ccavenuepay' || $payment_method == 'payucheckout_shared') {
                    $remaining_amount = $total_amount;
                    $paid_amount = 0;
                    $remaining_installments = $total_installments;
                    $paid_installments = 0;
                }

                $installmentValue = 0;
                if ($partialpaymentHelper->isPartialPaymentForOutOfStockProducts()) {
                    $is_preordered = 1;
                } else {
                    $is_preordered = 0;
                }
                $partialpaymentModelData = array(
                    'order_id' => $order_id,
                    'is_preordered' => $is_preordered,
                    'total_installments' => $total_installments,
                    'paid_installments' => $paid_installments,
                    'remaining_installments' => $remaining_installments,
                    'total_amount' => $partialpaymentHelper->setNumberFormat($total_amount),
                    'paid_amount' => $partialpaymentHelper->setNumberFormat($paid_amount),
                    'remaining_amount' => $partialpaymentHelper->setNumberFormat($remaining_amount),
                    'auto_capture_profile_id' => self::$profileId,
                    'auto_capture_payment_profile_id' => self::$realId
                );
                $partialpaymentModel = Mage::getModel('partialpayment/partialpayment')->setData($partialpaymentModelData)->save();
                $order->addStatusHistoryComment('Layaway order deposit', false);
                if((self::$profileId =='' || self::$realId =='') && ($payment_method != "paypal_express")){
                    $sendemail  = Mage::helper('partialpayment/emailsender')->sendReminderEmptyProfile($order->getIncrementId());
                }
                $partial_payment_id = $partialpaymentModel->getId();
                foreach ($items as $item) {
                    $item_id = $item->getId();
                    $product_total_installments = 1;
                    $product_paid_installments = 1;
                    $product_remaining_installments = 0;
                    $_product = Mage::getModel('catalog/product')->load($item->getProductId());
                    $product_total_amount = Mage::helper('tax')->getPrice($_product, $item->getPrice(), false);
                    $product_paid_amount = $item->getPrice(); //for non partial payment product
                    $product_remaining_amount = 0; //for non partial payment product
                    if (($partialpaymentHelper->isPartialPaymentForSpecificProduct() || $partialpaymentHelper->isPartialPaymentForAllProduct() || $partialpaymentHelper->isPartialPaymentForOutOfStockProducts()) && $partialpaymentHelper->getAllowPartialPaymentFromInfo($item)) {//if partial payment selected for product 
                        if ($partialpaymentHelper->isAllowFlexyPayment($_product)) {
                            $product_total_installments = $partialpaymentHelper->getAllowPartialPaymentFromInfo($item);
                            $product_paid_amount = $calculationModel->getDownPaymentAmount($product_total_amount, $_product, $product_total_installments);
                        } else {
                            $product_total_installments = $partialpaymentHelper->getTotalIinstallments($_product);
                            $product_paid_amount = $calculationModel->getDownPaymentAmount($product_total_amount, $_product);
                        }
                        $product_remaining_installments = $product_total_installments - $product_paid_installments;
                        $product_remaining_amount = $product_total_amount - $product_paid_amount;
                    } else if ($partialpaymentHelper->isPartialPaymentForWholecart()) {//if whole cart
                        if ($partialpaymentHelper->isAllowFlexyPayment($_product)) {
                            $product_total_installments = $total_installments;
                            $product_remaining_installments = $product_total_installments - $product_paid_installments;
                            $product_paid_amount = $calculationModel->getDownPaymentAmount($product_total_amount, null, $product_total_installments);
                            $product_remaining_amount = $product_total_amount - $product_paid_amount;
                        } else {
                            $product_total_installments = $total_installments;
                            $product_remaining_installments = $product_total_installments - $product_paid_installments;
                            $product_paid_amount = $calculationModel->getDownPaymentAmount($product_total_amount, null, $product_total_installments);
                            $product_remaining_amount = $product_total_amount - $product_paid_amount;
                        }
                    }
                    $product_total_amount *= $item->getQty();
                    $product_paid_amount *= $item->getQty();
                    $product_remaining_amount *= $item->getQty();
                    if ($payment_method == 'paypal_standard' || $payment_method == 'ccavenuepay' || $payment_method == 'payucheckout_shared') {
                        $productModelData = array(
                            'partial_payment_id' => $partial_payment_id,
                            'sales_flat_order_item_id' => $item_id,
                            'downpayment' => $partialpaymentHelper->setNumberFormat($product_paid_amount),
                            'total_installments' => $product_total_installments,
                            'paid_installments' => 0,
                            'remaining_installments' => $product_total_installments,
                            'total_amount' => $partialpaymentHelper->setNumberFormat($product_total_amount),
                            'paid_amount' => 0,
                            'remaining_amount' => $partialpaymentHelper->setNumberFormat($product_total_amount)
                        );
                    } else {
                        $productModelData = array(
                            'partial_payment_id' => $partial_payment_id,
                            'sales_flat_order_item_id' => $item_id,
                            'downpayment' => $partialpaymentHelper->setNumberFormat($product_paid_amount),
                            'total_installments' => $product_total_installments,
                            'paid_installments' => $product_paid_installments,
                            'remaining_installments' => $product_remaining_installments,
                            'total_amount' => $partialpaymentHelper->setNumberFormat($product_total_amount),
                            'paid_amount' => $partialpaymentHelper->setNumberFormat($product_paid_amount),
                            'remaining_amount' => $partialpaymentHelper->setNumberFormat($product_remaining_amount)
                        );
                    }
                    $productModel = Mage::getModel('partialpayment/product')->setData($productModelData)->save();
                }
                /* set installments */
                $transactionId = NULL;
                $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
                $installment_due_date = new Zend_Date($currentTimestamp);

                $tax = $order->getTaxAmount();
                $shipping_tax = 0;

                if ($tax > 0) {
                    $shipping_tax = (float) $order->getShippingAmount() + $tax;
                } else {
                    $shipping_tax = (float) $order->getShippingAmount();
                }
                $discount = abs($order->getDiscountAmount());
                $shipping_tax += $order->getSurchageAmount();
                $totlaremainngamount = 0;
                $grand_total = $order->getBaseGrandTotal();
                for ($i = 1; $i <= $total_installments; $i++) {
                    if ($i == 1) {
                        $installmentAmount = $partialpaymentHelper->setNumberFormat($order->getBasePaidAmount());
                        if ($payment_method == 'paypal_standard' || $payment_method == 'ccavenuepay' || $payment_method == 'payucheckout_shared') {
                            $installmentModelData = array('partial_payment_id' => $partial_payment_id, 'installment_amount' => $installmentAmount, 'installment_due_date' => $installment_due_date->toString('yyyy-MM-dd'), 'installment_status' => 'Remaining');
                        } else {
                            $installmentModelData = array('partial_payment_id' => $partial_payment_id, 'installment_amount' => $installmentAmount, 'installment_due_date' => $installment_due_date->toString('yyyy-MM-dd'), 'installment_paid_date' => $installment_due_date->toString('yyyy-MM-dd'), 'installment_status' => 'Paid', 'payment_method' => $payment_method, 'txn_id' => $transactionId);
                        }
                        $totlaremainngamount += $installmentAmount;
                    } else {
                        $installmentAmount = 0;
                        if ($partialpaymentHelper->isPartialPaymentForWholecart()) {
                            if ($i <= $partialpaymentHelper->getTotalIinstallments()) {
                                $installmentAmount = $partialpaymentHelper->setNumberFormat($order->getBaseRemainingAmount() / ($total_installments - 1));
                            }
                        } else {
                            foreach ($items as $item) {
                                $_product = Mage::getModel('catalog/product')->load($item->getProductId());
                                $price = $item->getRowTotal();
                                if ($partialpaymentHelper->getAllowPartialPaymentFromInfo($item)) {//if partial payment selected for product 
                                    if ($partialpaymentHelper->isAllowFlexyPayment($_product)) {
                                        if ($i <= $partialpaymentHelper->getAllowPartialPaymentFromInfo($item)) {
                                            $installmentAmount += $partialpaymentHelper->setNumberFormat($calculationModel->getInstallments($price, $_product, $partialpaymentHelper->getAllowPartialPaymentFromInfo($item)));
                                        }
                                    } else if ($i <= $partialpaymentHelper->getTotalIinstallments($_product)) {
                                        $installmentAmount += $partialpaymentHelper->setNumberFormat($calculationModel->getInstallments($price, $_product));
                                    }
                                }
                            }
                            if (!$partialpaymentHelper->isShippingAndTaxOnDownPayment()) {
                                $installmentAmount += $partialpaymentHelper->setNumberFormat($shipping_tax / ($total_installments - 1));
                            }
                            if (!$partialpaymentHelper->isDiscountOnDownPayment()) {
                                $installmentAmount -= $partialpaymentHelper->setNumberFormat($discount / ($total_installments - 1));
                            }
                        }

                        if ($i == $total_installments) {
                            $installmentAmount = $grand_total - $totlaremainngamount;
                        } else {
                            $totlaremainngamount += $installmentAmount;
                        }
                        // next installment date calculation
                        $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
                        $date = new Zend_Date($currentTimestamp);

                        if ($partialpaymentHelper->getPaymentplan() == 1) {
                            $date->addMonth($i - 1);
                        } else if ($partialpaymentHelper->getPaymentplan() == 2) {
                            $date->addWeek($i - 1);
                        } else if ($partialpaymentHelper->getPaymentplan() == 3) {
                            $days = $partialpaymentHelper->getNoOfDays() * ($i - 1);
                            $date->addDay($days);
                        }

                        $installmentModelData = array('partial_payment_id' => $partial_payment_id, 'installment_amount' => $installmentAmount, 'installment_due_date' => $date->toString('yyyy-MM-dd'), 'installment_status' => 'Remaining');
                    }
                    $installmentModel = Mage::getModel('partialpayment/installment')->setData($installmentModelData)->save();
                }

                $currency_code = $order->getOrderCurrency()->getCurrencyCode();

                if ($payment_method != 'paypal_standard' && $payment_method != 'ccavenuepay' && $payment_method != 'payucheckout_shared') {
                    if ($order->getCustomerId()) {
                        $customer_first_name = $order->getCustomerFirstname();
                        $customer_email = $order->getCustomerEmail();
                    } else {
                        $customer_first_name = $order->getBillingAddress()->getFirstname();
                        $customer_email = $order->getBillingAddress()->getEmail();
                    }
                    $partial_payment_emailsender = Mage::helper('partialpayment/emailsender');
                    $partial_payment_emailsender->sendEmailSuccess($customer_first_name, $customer_email, $order->getIncrementId(), $partial_payment_id, $currency_code);
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        if ($partialpaymentHelper->isPartialPaymentForWholecart())
            Mage::getSingleton('core/session')->unsAllowPartialPayment();
    }

    public function updateMessaggeForCreditCardFailure(Varien_Event_Observer $observer)
    {
        $error_checker = $observer->getErrorChecker();
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $subtotal = $quote->getSubtotal();
        $max_subtotal = Mage::helper('ipsecurityextended')->getMaximumSubtotal();
        if (Mage::helper('checkout')->quoteIsPartialPayment() || Mage::registry('current_partialpayment') || $subtotal > $max_subtotal || Mage::app ()->getStore ()->isAdmin()) {
            $error_checker->setShouldBeCatchException(true);
            $error_checker->setMessage(Mage::helper('partialpayment/partialpayment')->getMessageNotifyPaymentDeclined());
        }
    }
    
    public function disableStoreCredit(Varien_Event_Observer $observer){
        if(Mage::helper('partialpayment/partialpayment')->quoteIsPartialPayment()){
            Mage::throwException('Sorry! Layaway order can not apply store credit.');
        }
    }
    
    public function removeStoreCredit(Varien_Event_Observer $obsever){
        if((bool)Mage::getSingleton('core/session')->getAllowPartialPayment()){
            $quote = $obsever->getQuote();
            $quote->setUseCredit(false);
        }
    }

    public function MwOrderEdit($observer)
    {
        $order = $observer->getEvent()->getOrder();
        Mage::helper('partialpaymentextended')->refreshOrderTotals($order);
    }
    
    public function addPaymentRecordInstallmentPaid(Varien_Event_Observer $observer)
    {
        $installment = $observer->getInstallment();
        $data = $installment->getData();
        if (isset($data['partial_payment_id']) && isset($data['installment_status'])) {
            $partialpayment = Mage::getModel('partialpayment/partialpayment')->load($data['partial_payment_id']);
            if ($partialpayment && $partialpayment->getId()) {
                $order = Mage::getModel('sales/order')->load($partialpayment->getOrderId());

                if ($data['installment_status'] == "Paid" && !empty($data['transaction_id'])) {
                    $payment = $order->getPayment();
                    $payment->setTransactionId($data['transaction_id']); // Make it unique.
                    $transaction = $payment->addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE, null, false, false);
                    $transaction->setIsClosed(0); // Close the transaction on return?
                    $transaction->save();
                }
            }
        }
    }

    public function recollectInstallmentFailed(Varien_Event_Observer $observer)
    {
        $installment = $observer->getInstallment();
        $data = $installment->getData();
        if (empty($data['partial_payment_id']) || empty($data['installment_status']) || $data['installment_status'] != "Failed") {
            return;
        }
        $table_prefix = (string) Mage::getConfig()->getTablePrefix();
        $resource = Mage::getSingleton('core/resource');
        $readAdapter = $resource->getConnection('core_read');
        $writeAdapter = $resource->getConnection('core_write');

        $select = $readAdapter->select();
        $select->from(array('main_table' => $table_prefix . 'partial_payment_installments'), array('installment_id', 'installment_amount'));
        $select->where("main_table.partial_payment_id = ? AND main_table.installment_status = 'Remaining'", $data['partial_payment_id']);
        $select->order(array('main_table.installment_id DESC'));
        $select->limit(1);
        $result = $readAdapter->fetchRow($select);
        if (empty($result)) {
            // create new installment remaining
            $installment_due_date = strtotime("+1 months");
            $installment_due_date = date("Y-m-d", $installment_due_date);

            $data = array(
                'partial_payment_id' => $data['partial_payment_id'],
                'installment_amount' => $data['installment_amount'],
                'installment_due_date' => $installment_due_date,
                'installment_paid_date' => "",
                'installment_status' => 'Remaining'
            );
            Mage::getModel('partialpayment/installment')
                    ->setData($data)
                    ->save();
        } else {
            $installemnt_amount = $data['installment_amount'] + $result['installment_amount'];
            $writeAdapter->update($table_prefix . 'partial_payment_installments', array('installment_amount' => $installemnt_amount), array('installment_id = ?' => $result['installment_id']));
        }
    }
    
    public function reloadTotals($observer)
    {
        $invoice = $observer->getInvoice();
        $order = $invoice->getOrder();
        $paritalpayment = Mage::getModel('partialpayment/partialpayment')->load($order->getId(), 'order_id');
        if ($paritalpayment->getId()) {
            $order->setTotalPaid($order->getPaidAmount())
                    ->setBaseTotalPaid($order->getBasePaidAmount())
                    ->setTotalDue($order->getRemainingAmount())
                    ->setBaseTotalDue($order->getBaseRemainingAmount());
        }
    }
    
//    public function refundStoreCredit(Varien_Event_Observer $observer) {
//        $order = $observer->getOrder();
//        $customerId = $order->getCustomerId();
//        if(!$customerId) {
//            return;
//        }
//        $balance = Mage::getModel('credit/balance')->loadByCustomer($customerId);
//        if ($order->getBaseCreditAmount() > 0) {
//            $balance->addTransaction(
//                    $order->getBaseCreditAmount(), Mirasvit_Credit_Model_Transaction::ACTION_REFUNDED, Mage::helper('credit')->__('Cancel Layaway Order #%s', $order->getIncrementId())
//            );
//        }
//    }

     public function cancelInstallments(\Varien_Event_Observer $observer)
    {
        $order = $observer->getOrder();
        if (in_array($order->getState(), array('canceled', 'closed', 'completed'))) {
            $paritalpayment = Mage::getModel('partialpayment/partialpayment')->load($order->getId(), 'order_id');
            Mage::helper('partialpaymentextended')->_cancelInstallments($order, $paritalpayment);
        }
    }

}
