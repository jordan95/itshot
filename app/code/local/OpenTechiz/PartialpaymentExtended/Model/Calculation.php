<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class OpenTechiz_PartialpaymentExtended_Model_Calculation extends Milople_Partialpayment_Model_Calculation
{
	public function setInstallmentAsUnpaid ($installment, $partial_payment, $installment_status, $installment_due_date)
	{
		$partial_payment->setPaidAmount($partial_payment->getPaidAmount() - $installment->getInstallmentAmount());
		$partial_payment->setRemainingAmount($partial_payment->getRemainingAmount() + $installment->getInstallmentAmount());
		$partial_payment->setPaidInstallments($partial_payment->getPaidInstallments() - 1);
		$partial_payment->setRemainingInstallments($partial_payment->getRemainingInstallments() + 1);
		$partial_payment->save();		

		$productModel = Mage::getModel('partialpayment/product')->getCollection()
						->addFieldToFilter('partial_payment_id', $partial_payment->getPartialPaymentId());

		foreach($productModel as $product)
		{	
			$average_installment_amount = $product->getTotalAmount() / $product->getTotalInstallments();
			$product->setRemainingInstallments($product->getRemainingInstallments() + 1);
			$product->setRemainingAmount($product->getRemainingAmount() + $average_installment_amount);
			$product->setPaidInstallments($product->getPaidInstallments() - 1);
			$product->setPaidAmount($product->getPaidAmount() - $average_installment_amount);
			$product->save();
		}

		if($partial_payment->getPaidInstallments() == 0)
		{
			$partial_payment->setRemainingAmount($partial_payment->getTotalAmount());
			$partial_payment->setPaidAmount(0);
			$partial_payment->setRemainingInstallments($partial_payment->getTotalInstallments());
			$partial_payment->save();
		}

		$installment_due_date = explode('-', $installment_due_date);
		$installment->setInstallmentDueDate($installment_due_date[2] . '-' . $installment_due_date[0] . '-' . $installment_due_date[1]);
		$installment->setInstallmentStatus($installment_status);
        if($installment_status == 'Remaining'){
            $installment->setPaymentMethod('');
        }
		$installment->save();

		$salesOrderCollection = Mage::getModel('sales/order')->load($partial_payment->getOrderId());
		$paid_amount = $salesOrderCollection->getPaidAmount() - Mage::helper('partialpayment/partialpayment')->convertToOrderCurrencyAmount($installment->getInstallmentAmount(), $salesOrderCollection->getOrderCurrencyCode());
		$remaining_amount = $salesOrderCollection->getRemainingAmount() + Mage::helper('partialpayment/partialpayment')->convertToOrderCurrencyAmount($installment->getInstallmentAmount(), $salesOrderCollection->getOrderCurrencyCode());
		$base_paid_amount = $salesOrderCollection->getBasePaidAmount() - $installment->getInstallmentAmount();
		$base_remaining_amount = $salesOrderCollection->getBasePaidAmount() + $installment->getInstallmentAmount();

		$salesOrderCollection->setPaidAmount($paid_amount);
		$salesOrderCollection->setRemainingAmount($remaining_amount);
		$salesOrderCollection->setBasePaidAmount($base_paid_amount);
		$salesOrderCollection->setBaseRemainingAmount($base_remaining_amount);
		$salesOrderCollection->save();

		# to update invoice and credit memo 
		$invoiceCollection = Mage::getModel('sales/order_invoice')->load($partial_payment->getOrderId(), 'order_id');
		$creditMemoCollection = Mage::getModel('sales/order_creditmemo')->load($partial_payment->getOrderId(), 'order_id');

		$invoice_data = $invoiceCollection->getData();
		$credit_memo_data = $creditMemoCollection->getData();

		if(!empty($invoice_data))
		{
			$invoiceCollection->setPaidAmount($paid_amount);
			$invoiceCollection->setRemainingAmount($remaining_amount);
			$invoiceCollection->setBasePaidAmount($base_paid_amount);
			$invoiceCollection->setBaseRemainingAmount($base_remaining_amount);
			$invoiceCollection->save();
		}

		if(!empty($credit_memo_data))
		{
			$creditMemoCollection->setPaidAmount($paid_amount);
			$creditMemoCollection->setRemainingAmount($remaining_amount);
			$creditMemoCollection->setBasePaidAmount($base_paid_amount);
			$creditMemoCollection->setBaseRemainingAmount($base_remaining_amount);
			$creditMemoCollection->save();
		}

	}
	public function setOrderSuccessData($orderId)
	{
		$partial_payment = Mage::getModel('partialpayment/partialpayment')->getCollection();
		$partial_payment->addFieldToFilter('order_id',$orderId);
		$calculationModel = Mage::getModel("partialpayment/calculation");
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		$order = Mage::getModel('sales/order')->load($orderId);
		// date_default_timezone_set(Mage::app()->getStore()->getConfig('general/locale/timezone'));
		foreach ($partial_payment as $partial_item)
		{
			$totalAmount = $partial_item->getTotalAmount();
			$totalInstallment = $partial_item->getTotalInstallments();			
			$installmentData = Mage::getModel('partialpayment/installment')->getCollection()
							->addFieldToFilter('partial_payment_id', $partial_item->getPartialPaymentId())
							->getData();
			
			$amount = 0;
			if(isset($installmentData[0]['installment_id'])) 
			{
				$installmentModel = Mage::getModel('partialpayment/installment')->load($installmentData[0]['installment_id']);
				$amount = $installmentModel->getInstallmentAmount();
				$partial_payment_save = Mage::getModel('partialpayment/partialpayment')
									->setPaidAmount($amount)
									->setPaidInstallments(1)
									->setRemainingAmount($totalAmount - $amount)
									->setRemainingInstallments($totalInstallment-1)								
									->setPartialPaymentId($partial_item->getPartialPaymentId())
									->save();
				$installmentModel->setInstallmentPaidDate(Mage::getModel('core/date')->gmtDate('Y-m-d'))
					->setInstallmentStatus('Paid')
					->setPaymentMethod($order->getPayment()->getMethodInstance()->getCode())
					->save();
			}
			
			foreach ($order->getAllVisibleItems() as $item) 
			{
				$partialPaymentProducts = Mage::getModel('partialpayment/product')->getCollection()->addFieldToFilter('sales_flat_order_item_id',$item->getQuoteItemId());
				$_product = Mage::getModel('catalog/product')->load($item->getProductId());
				
				foreach($partialPaymentProducts as $partialPaymentProduct)
				{ 
				
				if($partialpaymentHelper->isAllowFlexyPayment($_product))
				{
					$paid= $partialpaymentHelper->setNumberFormat($calculationModel->getDownPaymentAmount($item->getPrice(),$_product,$totalInstallment));
				}
				else
				{
					$paid= $partialpaymentHelper->setNumberFormat($calculationModel->getDownPaymentAmount($item->getPrice(),$_product));
				}
				$paid *= $item->getQtyOrdered();
				
				$paidInstallment=Mage::helper('partialpayment/partialpayment')->convertCurrencyAmount($paid);
				$remainingAmount =$partialPaymentProduct->getTotalAmount()-$paidInstallment;
									
				$partialPaymentProduct->setPaidInstallments(1)
										->setRemainingInstallments($partialPaymentProduct->getTotalInstallments()-1)
										->setPaidAmount($paidInstallment)
										->setRemainingAmount($remainingAmount)
										->save();
				}
			}
			$currency_code = $order->getOrderCurrency()->getCurrencyCode();
			Mage::helper('partialpayment/emailsender')->sendEmailSuccess($order->getCustomerFirstname(), $order->getCustomerEmail(), $order->getIncrementId(), $partial_item->getPartialPaymentId(),$currency_code);
		}
	}

	public function setInstallmentSuccessData ($installment_id, $order_id, $partial_payment_id, $payment_method, $transaction_id = '', $maskAsPaid = false)
	{
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		$installmentData = Mage::getModel('partialpayment/installment')->load($installment_id);
		$instalmentamt = $installmentData->getInstallmentAmount();
		$partial_payment = Mage::getModel('partialpayment/partialpayment')->load($partial_payment_id);
		$installment_total_amount = 0;
		// date_default_timezone_set(Mage::app()->getStore()->getConfig('general/locale/timezone')); 
		if($maskAsPaid || (($partial_payment->getTotalAmount() > $partial_payment->getPaidAmount()) && ($partial_payment->getRemainingAmount() > 0) && (round($partial_payment->getRemainingAmount(),2) >= round($instalmentamt,2))))
		{	
			$partial_payment->setPaidAmount($partial_payment->getPaidAmount() + $instalmentamt);
			$partial_payment->setRemainingAmount($partial_payment->getRemainingAmount() - $instalmentamt);
			$partial_payment->setPaidInstallments($partial_payment->getPaidInstallments() + 1);
			$partial_payment->setRemainingInstallments($partial_payment->getRemainingInstallments() - 1);
			$partial_payment->save();		

			$productModel = Mage::getModel('partialpayment/product')->getCollection()
							->addFieldToFilter('partial_payment_id', $partial_payment_id);

			foreach($productModel as $product)
			{	
				if($product->getRemainingInstallments()>0)
				{
					$remain = $product->getRemainingAmount()/$product->getRemainingInstallments();
					$product->setRemainingInstallments($product->getRemainingInstallments() - 1);
					$product->setRemainingAmount($product->getRemainingAmount() - $remain);
					$product->setPaidInstallments($product->getPaidInstallments() + 1);
					$product->setPaidAmount($product->getPaidAmount() + $remain);
					$product->save();
				}
			}
			if($partial_payment->getTotalInstallments() == $partial_payment->getPaidInstallments())
			{	
				$partial_payment->setRemainingAmount(0);
				$partial_payment->setPaidAmount($partial_payment->getTotalAmount());
				$partial_payment->save();
			}
			$installmentData->setInstallmentPaidDate(Mage::getModel('core/date')->gmtDate('Y-m-d'));
			$installmentData->setInstallmentStatus('Paid');
			$installmentData->setPaymentMethod($payment_method);
			$installmentData->setTransactionId($transaction_id);
			$installmentData->save();
		}

		$salesOrderCollection = Mage::getModel('sales/order')->load($order_id);
		$currency_code = $salesOrderCollection->getOrderCurrencyCode();
		
		# to update invoice and credit memo 
		$invoiceCollection = Mage::getModel('sales/order_invoice')->load($order_id,'order_id');
		$creditMemoCollection = Mage::getModel('sales/order_creditmemo')->load($order_id,'order_id');
		if(Mage::registry('mask_as_paid_register')){
			$payment = $salesOrderCollection->getPayment();
			$paid_amount_installment = $partialpaymentHelper->convertToOrderCurrencyAmount($instalmentamt,$currency_code);
			$methodName = $payment->getMethodInstance()->getCode();

			$paymentMethodCollection = Mage::getModel('cod/payment')->getCollection()->addFieldToFilter('name', $methodName);
			if (count($paymentMethodCollection) > 0) {
                        $paymentMethod = $paymentMethodCollection->getFirstItem();
                        $paymentMethodId = $paymentMethod->getId();
            }else{
            	$paymentMethodId = Mage::helper('opentechiz_salesextend/rewrite_adminhtml_paymentRecord')->getPaymentMethodId($methodName);
            }
            
            $data = [];
            $data['order_id'] = $order_id;
            $data['invoice_id'] = $invoiceCollection->getId();
            $data['increment_id'] = $invoiceCollection->getIncrementId();
            $data['payment_id'] = $paymentMethodId;
            $data['total'] = $paid_amount_installment;
            $data['billing_name'] = $salesOrderCollection->getBillingAddress()->getName();
            $data['email'] = $salesOrderCollection->getBillingAddress()->getEmail();
            $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['received_date'] = Mage::getModel('core/date')->gmtDate('Y-m-d');
            $data['number'] = OpenTechiz_Payment_Helper_Data::MARK_AS_PAID_PAYMENT_NOTE;
            $data['user_id'] = Mage::helper('opentechiz_payment')->getUserId();
            $paymentRecord = Mage::getModel('cod/paymentgrid');
            $paymentRecord->setData($data)->save();
		}
		// Recollect totals
		Mage::helper('partialpaymentextended')->_reloadTotals($salesOrderCollection);
                                $salesOrderCollection->setPaidAmount($salesOrderCollection->getTotalPaid())
                                       ->setPaidAmount($salesOrderCollection->getBaseTotalPaid())
                                       ->setRemainingAmount($salesOrderCollection->getTotalDue())
                                       ->setBaseRemainingAmount($salesOrderCollection->getBaseTotalDue());
                                $salesOrderCollection->save();
                                # to update invoice and credit memo 
                                $invoiceCollection = Mage::getModel('sales/order_invoice')->load($order_id, 'order_id');
                                $creditMemoCollection = Mage::getModel('sales/order_creditmemo')->load($order_id, 'order_id');

                                $invoice_data = $invoiceCollection->getData();
                                $credit_memo_data = $creditMemoCollection->getData();
                                if (!empty($invoice_data)) {
                                    $invoiceCollection->setPaidAmount($salesOrderCollection->getTotalPaid());
                                    $invoiceCollection->setRemainingAmount($salesOrderCollection->getBaseTotalPaid());
                                    $invoiceCollection->setBasePaidAmount($salesOrderCollection->getTotalDue());
                                    $invoiceCollection->setBaseRemainingAmount($salesOrderCollection->getBaseTotalDue());
                                    $invoiceCollection->save();
                                }
                                if (!empty($credit_memo_data)) {
                                    $creditMemoCollection->setPaidAmount($salesOrderCollection->getTotalPaid());
                                    $creditMemoCollection->setRemainingAmount($salesOrderCollection->getBaseTotalPaid());
                                    $creditMemoCollection->setBasePaidAmount($salesOrderCollection->getTotalDue());
                                    $creditMemoCollection->setBaseRemainingAmount($salesOrderCollection->getBaseTotalDue());
                                    $creditMemoCollection->save();
                                }
                                Mage::helper('partialpayment/emailsender')->sendInstallmentConfirmationMail($salesOrderCollection->getCustomerFirstname(),$salesOrderCollection->getCustomerEmail(),$salesOrderCollection->getIncrementId(),$instalmentamt,$partial_payment_id,$currency_code);
	}
}
?>
