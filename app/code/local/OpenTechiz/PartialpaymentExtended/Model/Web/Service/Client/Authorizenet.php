<?php

class OpenTechiz_PartialpaymentExtended_Model_Web_Service_Client_Authorizenet extends Milople_Partialpayment_Model_Web_Service_Client_Authorizenet
{

    public function createCIMAccountOnInstallmentPayment($order, $paymentInfo)
    {
        $data = new stdClass();
        // Payment
        $payment = $this->getPayment();
        $paymentData = $this->_convertPaymentFromOrderPaymentInfo($paymentInfo);

        // Create customer profile
        $result = $this->createCustomerProfile($order, $paymentData, 'none');
        if (!$result) {
            Mage::throwException("Cant create customer profile");
        }
        $customerProfileId = '';
        if ($result instanceof Milople_Partialpayment_Model_Web_Service_Client_Authorizenet_Error) {
            // Error occured
            if ($result->getCode() == self::ERR_CODE_DUPLICATE_CUSTOMER) {
                // Duplicate customer
                if (preg_match("/\s+([0-9]+)\s+/", $result->getText(), $matches)) {
                    $customerProfileId = $matches[1];
                } else {
                    throw new Mage_Core_Exception("Customer duplicate responded but can't find customer id in reply");
                }
            }
        } else {
            $customerProfileId = $result->customerProfileId;
        }
        if (!$customerProfileId) {
            Mage::throwException("Customer Profile ID is required");
        }
        $customerPaymentProfile = $this->createCustomerPaymentProfile($customerProfileId, $order, $paymentData, true);
        if (!$customerPaymentProfile) {
            Mage::throwException("Cant create customer Payment Profile Id");
        }
        $data->customerProfileId = $customerPaymentProfile->CreateCustomerPaymentProfileResult->customerProfileId;
        $data->customerPaymentProfileId = $customerPaymentProfile->CreateCustomerPaymentProfileResult->customerPaymentProfileId;
        return $data;
    }

    public function createCIMAccount()
    {
        $data = new stdClass();

        /* @var $quote Mage_Sales_Model_Quote */
        $quote = $this->getPayment()->getQuote();

        // Payment
        $payment = $this->getPayment();
        $paymentData = [];
        if ($payment instanceof Mage_Sales_Model_Quote_Payment) {
            $paymentData = $this->_convertPayment($payment);
        } elseif (is_array($payment)) {
            $paymentData = $this->_convertPaymentFromOrderPaymentInfo($payment);
        }

        // Create customer profile
        $result = $this->createCustomerProfile($quote, $paymentData);

        if (!$result) {
            Mage::throwException("Cant create customer profile");
        }

        $customerProfileId = '';
        if ($result instanceof Milople_Partialpayment_Model_Web_Service_Client_Authorizenet_Error) {
            if ($result->getCode() == self::ERR_CODE_DUPLICATE_CUSTOMER) {
                if (preg_match("/\s+([0-9]+)\s+/", $result->getText(), $matches)) {
                    $customerProfileId = $matches[1];
                } else {
                    Mage::throwException("Customer duplicate responded but can't find customer id in reply");
                }
            }
        } else {
            $customerProfileId = $result->customerProfileId;
        }
        if (!$customerProfileId) {
            Mage::throwException("Customer Profile ID is required");
        }
        $customerPaymentProfile = $this->createCustomerPaymentProfile($customerProfileId, $quote, $paymentData);
        if (!$customerPaymentProfile) {
            Mage::throwException("Cant create customer Payment Profile Id");
        }
        $data->customerProfileId = $customerPaymentProfile->customerProfileId;
        $data->customerPaymentProfileId = $customerPaymentProfile->customerPaymentProfileId;
        return $data;
    }

    protected function createCustomerProfile($source, $payment, $validationMode = 'liveMode')
    {
        // Billing Address
        $billTo = $source->getBillingAddress();

        $this->getRequest()
                ->reset()
                ->setData(array(
                    'merchantAuthentication' => array(
                        'name' => trim($this->getApiLoginId()),
                        'transactionKey' => trim($this->getTransactionKey())
                    ),
                    'profile' => array(
                        'merchantCustomerId' => $source->getCustomerId(),
                        'email' => $source->getCustomerEmail(),
                        'paymentProfiles' => array(
                            'CustomerPaymentProfileType' => array(
                                'customerType' => 'individual',
                                'billTo' => array(
                                    'firstName' => $billTo->getFirstname(),
                                    'lastName' => $billTo->getLastname(),
                                    'address' => $billTo->getStreet(-1),
                                    'state' => $billTo->getRegion(),
                                    'city' => $billTo->getCity(),
                                    'company' => $billTo->getCompany(),
                                    'country' => $billTo->getCountry(),
                                    'zip' => $billTo->getPostcode(),
                                    'phoneNumber' => $billTo->getTelephone(),
                                    'faxNumber' => $billTo->getFax(),
                                ),
                                'payment' => $payment
                            )
                        )
                    ),
                    'validationMode' => $validationMode
        ));
        return $this->_runRequest('CreateCustomerProfile', array(self::ERR_CODE_DUPLICATE_CUSTOMER));
    }

    protected function createCustomerPaymentProfile($customerId, $source, $payment, $cimRequest = false, $validationMode = 'liveMode')
    {
        //Billing to
        $billTo = $source->getBillingAddress();

        $paymentProfile = array(
            'merchantAuthentication' => array(
                'name' => trim($this->getApiLoginId()),
                'transactionKey' => trim($this->getTransactionKey())
            ),
            'customerProfileId' => $customerId,
            'paymentProfile' => array(
                'billTo' => array(
                    'firstName' => $billTo->getFirstname(),
                    'lastName' => $billTo->getLastname(),
                    'address' => $billTo->getStreet(-1),
                    'state' => $billTo->getRegion(),
                    'city' => $billTo->getCity(),
                    'company' => $billTo->getCompany(),
                    'country' => $billTo->getCountry(),
                    'zip' => $billTo->getPostcode(),
                    'phoneNumber' => $billTo->getTelephone()
                ),
                'payment' => $payment
            ),
            'validationMode' => $validationMode,
            'cimRequest' => $cimRequest
        );
        $this->getRequest()->reset()->setData($paymentProfile);
        return $this->_runRequest('CreateCustomerPaymentProfile', array(self::ERR_CODE_DUPLICATE_CUSTOMER));
    }

}
