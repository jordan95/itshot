<?php

class OpenTechiz_PartialpaymentExtended_Model_Credit_Observer_Output extends Mirasvit_Credit_Model_Observer_Output
{
    
    public function appendOnestepcheckoutCreditBlock($block, $transport)
    {
        return;
    }

    public function afterOutput($obj)
    {
        $block = $obj->getEvent()->getBlock();
        $transport = $obj->getEvent()->getTransport();

        if (empty($transport)) {
            return $this;
        }

        $this->appendCartCreditBlock($block, $transport);

        if (Mage::app()->getRequest()->getModuleName() == 'onestepcheckout' && !Mage::app()->getRequest()->isAjax()) {
            $this->appendOnestepcheckoutCreditBlock($block, $transport);
        }

        return $this;
    }

}
