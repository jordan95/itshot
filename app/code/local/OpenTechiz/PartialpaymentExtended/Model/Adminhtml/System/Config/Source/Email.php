<?php

class OpenTechiz_PartialpaymentExtended_Model_Adminhtml_System_Config_Source_Email extends Varien_Object
{
    static public function toOptionArray()
    {
        return array(   
            1 => Mage::helper('partialpayment')->__('To Customers'),
            2 => Mage::helper('partialpayment')->__('To Customers and CC Both'),
            3 => Mage::helper('partialpayment')->__('To Customers and Custom CC'),
            0 => Mage::helper('partialpayment')->__('No')
        );
    }
}
