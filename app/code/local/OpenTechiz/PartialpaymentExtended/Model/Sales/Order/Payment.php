<?php


class OpenTechiz_PartialpaymentExtended_Model_Sales_Order_Payment extends Milople_Partialpayment_Model_Sales_Order_Payment
{
	public function capture($invoice)
    {
		try {
			if (is_null($invoice)) {
				$invoice = $this->_invoice();
				$this->setCreatedInvoice($invoice);
				return $this; // @see Mage_Sales_Model_Order_Invoice::capture()
			}

			$order = $this->getOrder();
			$incrementId = $order->getEntityId();

			$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
			$partialpaymentOrder = Mage::getModel('partialpayment/partialpayment')->getCollection()
				->addFieldToFilter('order_id',$incrementId)
				->load();

			if(sizeof($partialpaymentOrder->getData()) == 0)
			{	
				if(($invoice->getBasePaidAmount() > 0) && ($invoice->getBaseRemainingAmount() > 0)&& ($invoice->getSubtotalInclTax() >= $partialpaymentHelper->getMinimumWholecartOrderAmount()) && ($invoice->getGrandTotal() > $invoice->getBaseRemainingAmount()) && ($invoice->getBasePaidAmount() > $invoice->getTaxAmount()))
				{
					$capture_amount = $invoice->getBasePaidAmount();
					$amountToCapture = $this->_formatAmount($capture_amount);
				}else{
					$amountToCapture = $this->_formatAmount($invoice->getBaseGrandTotal());
				}
			}
			else
			{
				foreach($partialpaymentOrder as $partialpaymentModel)
				{
					$partialpaymentId = $partialpaymentModel->getPartialPaymentId();
					$installmentModel = Mage::getModel('partialpayment/installment')->getCollection()
						->addFieldToFilter('partial_payment_id',$partialpaymentId)
						->getFirstItem() ;								
					$capture_amount = $installmentModel->getInstallmentAmount();
					$amountToCapture = $this->_formatAmount($capture_amount);
				}
			}

			// prepare parent transaction and its amount
			$paidWorkaround = 0;
			if (!$invoice->wasPayCalled()) {
				$paidWorkaround = (float)$amountToCapture;
			}
			$this->_isCaptureFinal($paidWorkaround);

			$this->_generateTransactionId(
				Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE,
				$this->getAuthorizationTransaction()
			);

			Mage::dispatchEvent('sales_order_payment_capture', array('payment' => $this, 'invoice' => $invoice));
                        $pendingChanges = $this->getMwEditHelper()->getPendingChanges($order->getId());
                        if(!empty($pendingChanges) && !$this->getTransactionId() && !$invoice->getTransactionId()){
                            return $this;
                        }
			/**
			 * Fetch an update about existing transaction. It can determine whether the transaction can be paid
			 * Capture attempt will happen only when invoice is not yet paid and the transaction can be paid
			 */
			if ($invoice->getTransactionId()) {
				$this->getMethodInstance()
					->setStore($order->getStoreId())
					->fetchTransactionInfo($this, $invoice->getTransactionId());
			}
			$status = true;
			if (!$invoice->getIsPaid() && !$this->getIsTransactionPending()) {
				// attempt to capture: this can trigger "is_transaction_pending"
				$this->getMethodInstance()->setStore($order->getStoreId())->capture($this, $amountToCapture);

				$transaction = $this->_addTransaction(
					Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE,
					$invoice,
					true
				);

				if ($this->getIsTransactionPending()) {
					$message = Mage::helper('sales')->__('Capturing amount of %s is pending approval on gateway.', $this->_formatPrice($amountToCapture));
					$state = Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW;
					if ($this->getIsFraudDetected()) {
						$status = Mage_Sales_Model_Order::STATUS_FRAUD;
					}
					$invoice->setIsPaid(false);
				} else { // normal online capture: invoice is marked as "paid"
					$message = Mage::helper('sales')->__('Captured amount of %s online.', $this->_formatPrice($amountToCapture));
					$state = Mage_Sales_Model_Order::STATE_PROCESSING;

					$invoice->setIsPaid(true);

					$this->_updateTotals(array('base_amount_paid_online' => $amountToCapture));
				}
				if ($order->isNominal()) {
					$message = $this->_prependMessage(Mage::helper('sales')->__('Nominal order registered.'));
				} else {
					$message = $this->_prependMessage($message);
					$message = $this->_appendTransactionToMessage($transaction, $message);
				}
				$order->setState($state, $status, $message);
				$this->getMethodInstance()->processInvoice($invoice, $this); // should be deprecated
				return $this;
			}
			Mage::throwException(
				Mage::helper('sales')->__('The transaction "%s" cannot be captured yet.', $invoice->getTransactionId())
			);
		}
		catch(Exception $e)
		{
			throw new Mage_Core_Exception($e->getMessage());
		}
    }
        /**
     * @return MageWorx_OrdersEdit_Helper_Edit
     */
    protected function getMwEditHelper()
    {
        return Mage::helper('mageworx_ordersedit/edit');
    }
    
    public function registerRefundNotification($amount)
    {
        $notificationAmount = $amount;
        $this->_generateTransactionId(Mage_Sales_Model_Order_Payment_Transaction::TYPE_REFUND, $this->_lookupTransaction($this->getParentTransactionId())
        );
        if ($this->_isTransactionExists()) {
            return $this;
        }
        $order = $this->getOrder();
        $invoice = $this->_getInvoiceForTransactionId($this->getParentTransactionId());
        $creditmemos = $order->getCreditmemosCollection();
        $creditmemo = null;
        if (count($creditmemos) > 0) {
            foreach ($creditmemos as $_creditmemo) {
                if ($_creditmemo->canCancel()) {
                    $creditmemo = $_creditmemo;
                }
            }
        }
        $data = [];
        $data['payment_id'] = 17;
        $data['billing_name'] = $order->getBillingAddress()->getName();
        $data['email'] = $order->getBillingAddress()->getEmail();
        $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $data['received_date'] = Mage::getModel('core/date')->gmtDate('Y-m-d');
        $data['order_id'] = $order->getId();
        $data['number'] = OpenTechiz_Payment_Helper_Data::ONLINE_REFUNDED_PAYMENT_NOTE;
        $data['user_id'] = Mage::helper('opentechiz_payment')->getUserId();
        if ($invoice && !$creditmemo) {
            $data['invoice_id'] = $invoice->getId();
            $data['type'] = 'invoice';
        } else if ($creditmemo) {
            $data['credit_memo_id'] = $creditmemo->getId();
            $data['type'] = 'creditmemo';
        } else { 
            return $this;
        }
        $data['total'] = (-1) * $amount;
        Mage::register('payment_record_data', $data);
        $transaction = $this->_addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_REFUND);
        $message = $this->_prependMessage(
                Mage::helper('sales')->__('Registered notification about refunded amount of %s.', $this->_formatPrice($amount))
        );
        $message = $this->_appendTransactionToMessage($transaction, $message);
        $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true, $message);
        return $this;
    }

}