<?php

$installer = $this;
$installer->startSetup();

try {
    $installer->run("
        ALTER TABLE {$this->getTable('onlinebiz_payment_grid')} 
        ADD `user_id`
        integer(5) 
        NULL
        DEFAULT 0;
    ");
}catch (Exception $e){

}

$installer->endSetup();