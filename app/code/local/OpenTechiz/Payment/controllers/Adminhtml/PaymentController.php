<?php

class OpenTechiz_Payment_Adminhtml_PaymentController extends Mage_Adminhtml_Controller_Action
{
      protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales')
            ->_title($this->__('All Payment Record'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_payment/adminhtml_payment'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_payment/adminhtml_payment_grid')->toHtml()
        );
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/sales/invoice/allpayment');
    }
}