<?php

class OpenTechiz_Payment_Block_Adminhtml_Payment_Renderer_OrderLink extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $order_number =  $row->getData($this->getColumn()->getIndex());
        $order_id =  $row->getData('order_id');
        if (!empty($order_number)) {
	      	$url = Mage::getModel("core/url")->getUrl("adminhtml/sales_order/view", array("order_id" => $order_id));
	        return "<a href=\"{$url}\" target=\"_blank\">#{$order_number} </a>";
	    }
        return '';
    }
}