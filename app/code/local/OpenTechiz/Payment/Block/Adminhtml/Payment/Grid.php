<?php

class OpenTechiz_Payment_Block_Adminhtml_Payment_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    
    public function __construct() {
        parent::__construct();
        $this->setId('all_payment_grid');
        $this->setDefaultSort('payment_grid_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $table_user =  Mage::getSingleton('core/resource')->getTableName('admin/user');
        $table_order =  Mage::getSingleton('core/resource')->getTableName('sales/order');
        $table_creditmemo =  Mage::getSingleton('core/resource')->getTableName('sales/creditmemo');
        $table_transaction =  "tsht_sales_payment_transaction";
        $collection = Mage::getModel('cod/paymentgrid')->getResourceCollection();
        $collection->getSelect()
                    ->joinLeft(array('sales_order' => $table_order), "main_table.order_id = sales_order.entity_id", array('order_increment_id'=>'sales_order.increment_id'))
                    ->joinLeft(array('payment_transaction' => $table_transaction), "main_table.transaction_id = payment_transaction.transaction_id", array('txn_id'=>'payment_transaction.txn_id'))
                    ->joinLeft(array('creditmemo' => $table_creditmemo), "main_table.credit_memo_id = creditmemo.entity_id", array('credit_increment_id'=>'creditmemo.increment_id'))
                    ->joinLeft(array('user' => $table_user), "main_table.user_id = user.user_id", array('username'));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('payment_grid_id', array(
            'header' => Mage::helper('opentechiz_payment')->__('Payment #'),
            'index' => 'payment_grid_id',
            'type' => 'text',
            'width'=> '80'
        ));
        $this->addColumn('payment_id', array(
            'header' => Mage::helper('opentechiz_payment')->__('Payment Method'),
            'index' => 'payment_id',
            'filter_index' => 'main_table.payment_id',
            'type' => 'options',
            'options' => Mage::getModel('cod/paymentgrid')->getPayType(),
        ));
        $this->addColumn('txn_id', array(
            'header' => Mage::helper('opentechiz_payment')->__('Transaction Id'),
            'index' => 'txn_id',
            'type' => 'text'
        ));
        $this->addColumn('order_increment_id', array(
            'header' => Mage::helper('opentechiz_payment')->__('Order #'),
            'index' => 'order_increment_id',
            'filter_index' => 'sales_order.increment_id',
            'type' => 'text',
            'renderer' =>'OpenTechiz_Payment_Block_Adminhtml_Payment_Renderer_OrderLink'
        ));
        $this->addColumn('credit_increment_id', array(
            'header' => Mage::helper('opentechiz_payment')->__('Credit Memo #'),
            'index' => 'credit_increment_id',
            'filter_index' => 'creditmemo.increment_id',
            'type' => 'text',
            'renderer' =>'OpenTechiz_Payment_Block_Adminhtml_Payment_Renderer_CreditMemoLink'
        )); 
        $this->addColumn('increment_id', array(
            'header' => Mage::helper('opentechiz_payment')->__('Invoice #'),
            'index' => 'increment_id',
            'filter_index' => 'main_table.increment_id',
            'type' => 'text',
            'renderer' =>'OpenTechiz_Payment_Block_Adminhtml_Payment_Renderer_InvoiceLink'
        ));
        $this->addColumn('total', array(
            'header' => Mage::helper('opentechiz_payment')->__('Total'),
            'index' => 'total',
            'filter_index' => 'main_table.total',
            'type' => 'text',
            'filter'    => false,
            'width'=> '100', 
            'renderer' =>'OpenTechiz_Payment_Block_Adminhtml_Payment_Renderer_FormatPrice'
        ));
        $this->addColumn('created_time', array(
            'header' => Mage::helper('opentechiz_payment')->__('Created Date'),
            'index' => 'created_time',
            'type' => 'datetime',
        ));

        $this->addColumn('update_time', array(
            'header' => Mage::helper('opentechiz_payment')->__('Updated Date'),
            'index' => 'update_time',
            'type' => 'datetime',
        ));
        $this->addColumn('number', array(
            'header' => Mage::helper('opentechiz_payment')->__('Note'),
            'index' => 'number',
            'type' => 'text',
        ));
        $this->addColumn('username', array(
            'header' => Mage::helper('opentechiz_payment')->__('User'),
            'index' => 'username',
            'type' => 'text',
            'width'=> '100',    
            'renderer' =>'OpenTechiz_Payment_Block_Adminhtml_Payment_Renderer_User'
        ));
        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return '#';
    }


}