<?php

class OpenTechiz_Payment_Block_Adminhtml_Payment_Renderer_User extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $username =  $row->getData($this->getColumn()->getIndex());
        if($username ==''){
        	return 'system';
        }
        return $username;
        
    }
}