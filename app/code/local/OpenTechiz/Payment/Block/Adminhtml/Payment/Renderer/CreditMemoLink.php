<?php

class OpenTechiz_Payment_Block_Adminhtml_Payment_Renderer_CreditMemoLink extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $creditmemo_number =  $row->getData($this->getColumn()->getIndex());
        $creditmemo_id =  $row->getData('credit_memo_id');
        if (!empty($creditmemo_number)) {
	      	$url = Mage::getModel("core/url")->getUrl("adminhtml/sales_creditmemo/view", array("creditmemo_id" => $creditmemo_id));
	        return "<a href=\"{$url}\" target=\"_blank\">#{$creditmemo_number} </a>";
	    }
        return '';
    }
}