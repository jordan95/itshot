<?php

class OpenTechiz_Payment_Block_Adminhtml_Payment_Renderer_InvoiceLink extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $invoice_number =  $row->getData($this->getColumn()->getIndex());
        $invoice_id =  $row->getData('invoice_id');
        if (!empty($invoice_number)) {
	      	$url = Mage::getModel("core/url")->getUrl("adminhtml/sales_invoice/view", array("invoice_id" => $invoice_id));
	        return "<a href=\"{$url}\" target=\"_blank\">#{$invoice_number} </a>";
	    }
        return '';
    }
}