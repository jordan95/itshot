<?php

class OpenTechiz_Payment_Block_Adminhtml_Payment extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_payment';
        $this->_blockGroup = 'opentechiz_payment';
        $this->_headerText = Mage::helper('opentechiz_inventory')->__('All Payment Record');
        parent::__construct();
       
        $this->_removeButton('add');
    }
}