<?php
class OpenTechiz_Payment_Helper_Data extends Mage_Core_Helper_Abstract
{
	const ONLINE_CAPTURE_PAYMENT_NOTE = "Automatically created by an online transaction captured";
	const ONLINE_REFUNDED_PAYMENT_NOTE = "Automatically created by an online transaction refunded";
	const MARK_AS_PAID_PAYMENT_NOTE = "Automatically created by manual installment update";
	public function getUserId(){
        if(Mage::getSingleton('admin/session')->isLoggedIn()){
            $user_id = Mage::getSingleton('admin/session')->getUser()->getId();
        }else{
            $user_id = 0;
        }
        return $user_id;
    }
}