<?php

class OpenTechiz_IpsecurityExtended_Model_Observer extends MSA_Ipsecurity_Model_Observer
{

    public function getQuote() {
        return Mage::getSingleton('checkout/session')->getQuote();
    }
    
    public function CheckBlockedIp($observer)
    {
        if(Mage::app()->getStore()->isAdmin()){
            return;
        }
        $postData = Mage::app()->getRequest()->getPost();

        $email = '';
        $quote = $this->getQuote();
        if (isset($postData["billing"]["email"])) {
            $email = $postData["billing"]["email"];
        }elseif (isset($postData["order"]["account"]["email"])) {
            $email = $postData["order"]["account"]["email"];
        }
        if (isset($postData["customer_email"])) {
            $email = $postData["customer_email"];
        }
        $customer_id = Mage::getSingleton('adminhtml/session_quote')->getCustomerId();

        if (!$email) {
            //check cutomer session and get email
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $email = $customer->getEmail();
        }
        if(!$email) {
            $email = Mage::getSingleton('checkout/session')->getCustomerEmail();
        }

        if(!$email) {
            $email = $this->getQuote()->getCustomerEmail();
        }

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)&&$customer_id != -1){
            Mage::register('email_blocked', true);
            Mage::throwException(Mage::helper('ipsecurity')->__('Your email is not valid. Please check it or try again with other email.'));
            exit;
        }

        //get client IP address
        $clientIp = Mage::helper('ipsecurity/customcode')->getip_index();
        $objModel = Mage::getModel("ipsecurity/ipsecurity");

        //Check if IP or Email Address is blocked and display message to user
        $chekIPAdd = $objModel->checkUserIPAdd($clientIp);
        $chekEmail = $objModel->checkUserEmail($email);
        $blocked = false;
        $blockType = "";
        $subtotal = $quote->getSubtotal();
        if ($chekIPAdd == true) {
            $blocked = true;
            $blockType = "IP";
        } else if ($chekEmail == true) {
            $blocked = true;
            $blockType = "Email";
        }elseif (Mage::helper('ipsecurityextended')->quoteValidate()){
             $blocked = true;
             $blockType = "Maximum Subtotal";
        } else {
            $blocked = false;
        }

        if ($blocked) {
            $notify = new Varien_Object();
            $notify->setMessage(Mage::helper('ipsecurity')->__(Mage::getStoreConfig('ipsecurityextended/general/error_msg')));
            Mage::dispatchEvent('do_filter_blocked_message', array(
                'payment_method' => Mage::helper('ipsecurityextended')->getPaymentMethod(),
                'notify' => $notify
                    ));
            Mage::throwException($notify->getMessage() . '(' . $blockType . ')');
            exit;
        }
    }
    
    public function ipchecker_form(Varien_Event_Observer $observer)
    {
        $email = Mage::app()->getRequest()->getPost('email', false);
        $clientIp = Mage::helper('ipsecurity/customcode')->getip_index();
        $objModel = Mage::getModel("ipsecurity/ipsecurity");
        $chekIPAdd = $objModel->checkUserIPAdd($clientIp);
        $chekEmail = $email ? $objModel->checkUserEmail($email) : true;

        $blocked = false;
        $message = '';
        if ($chekIPAdd == true) {
            $message = 'Sorry, Your IP is blocked. Please contact administrator to resolve this issue.';
            $blocked = true;
        } if ($chekEmail == true) {
            $message = 'Sorry, Your email is blocked. Please contact administrator to resolve this issue.';
            $blocked = true;
        } else {
            $blocked = false;
        }

        if ($blocked) {
            Mage::getSingleton('core/session')->addError($message);
            Mage::app()->getResponse()->setRedirect(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB))->sendResponse();
            exit;
        }else{
            Mage::log(sprintf("Email: %s    IP: %s", $email, $clientIp), null, "ipchecker_form.log");
        }
    }

    public function ipchecker_login($observer)
    {
       $login = Mage::app()->getRequest()->getPost('login');
        $email = $observer->getEmail();
        if (isset($login['username'])) {
            $email = $login['username'];
        }

        if(!$email){
            return;
        }

        $clientIp = Mage::helper('ipsecurity/customcode')->getip_index();
        $objModel = Mage::getModel("ipsecurity/ipsecurity");
        $chekIPAdd = $objModel->checkUserIPAdd($clientIp);
        $chekEmail = $objModel->checkUserEmail($email);

        $blocked = false;
        if ($chekIPAdd == true) {
            $blocked = true;
        } else if ($chekEmail == true) {
            $blocked = true;
        } else {
            $blocked = false;
        }

        if ($blocked) {
            Mage::getSingleton('core/session')->addError('Sorry, Your email is blocked. Please contact administrator to resolve this issue.');
            Mage::app()->getResponse()->setRedirect(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB))->sendResponse();
            exit;
        }
    }

}
