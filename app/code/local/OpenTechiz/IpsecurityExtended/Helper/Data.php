<?php

class OpenTechiz_IpsecurityExtended_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function quoteValidate()
    {
        if(!$this->isEnable()){
            return false;
        }
        $subtotal = $this->getQuote()->getSubtotal();
        $payment_method = $this->getPaymentMethod();
        $payment_methods = $this->getEnablePaymentMethods();
        if(in_array($payment_method, $payment_methods) && $subtotal > $this->getMaximumSubtotal()){
            return true;
        }
        return false;
    }

    public function getMaximumSubtotal()
    {
        return (float) Mage::getStoreConfig('ipsecurityextended/general/maximum_subtotal');
    }

    public function isEnable()
    {
        return (bool) Mage::getStoreConfig('ipsecurityextended/general/enable');
    }

    public function getEnablePaymentMethods()
    {
        return explode(',', Mage::getStoreConfig('ipsecurityextended/general/enable_payment_methods'));
    }

    public function getQuote()
    {
        return Mage::getSingleton('checkout/session')->getQuote();
    }

    public function getPaymentMethod()
    {
        return $this->getQuote()->getPayment()->getMethod();
    }

}
