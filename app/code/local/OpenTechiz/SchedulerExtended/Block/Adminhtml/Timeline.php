<?php

/**
 * Timeline block
 *
 * @author Fabrizio Branca
 */
class OpenTechiz_SchedulerExtended_Block_Adminhtml_Timeline extends Aoe_Scheduler_Block_Adminhtml_Timeline
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
    }


    /**
     * Load schedules
     *
     * @return void
     */
    protected function loadSchedules()
    {
        $time_limit = Mage::getStoreConfig('system/cron/times_limit');
        $date_filter = Mage::getModel('core/date')->date('Y-m-d H:i:s', strtotime('-' . $time_limit . 'day'));

        /* @var Mage_Cron_Model_Resource_Schedule_Collection $collection */
        $collection = Mage::getModel('cron/schedule')->getCollection();
        $collection->addFieldToFilter('scheduled_at', array('gt' => $date_filter));
        
        $minDate = null;
        $maxDate = null;

        foreach ($collection as $schedule) {
            /* @var Aoe_Scheduler_Model_Schedule $schedule */
            $startTime = $schedule->getStarttime();
            if (empty($startTime)) {
                continue;
            }
            $minDate = is_null($minDate) ? $startTime : min($minDate, $startTime);
            $maxDate = is_null($maxDate) ? $startTime : max($maxDate, $startTime);
            $this->schedules[$schedule->getJobCode()][] = $schedule;
        }

        $this->starttime = $this->hourFloor(strtotime($minDate));
        $this->endtime = $this->hourCeil(strtotime($maxDate));
    }

}
