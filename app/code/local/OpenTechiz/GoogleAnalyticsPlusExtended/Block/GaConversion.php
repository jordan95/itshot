<?php

class  OpenTechiz_GoogleAnalyticsPlusExtended_Block_GaConversion extends Fooman_GoogleAnalyticsPlus_Block_GaConversion
{
    public function isEnabled()
    {
        $amount_maximum = Mage::getStoreConfig('google/analyticsplus_classic/amount_maximum');
        $amount = floatval($amount_maximum);
        return (($amount > 0 && $this->getValue() > 0 && $amount > $this->getValue()) || !$amount) && Mage::getStoreConfigFlag('google/analyticsplus_classic/conversionenabled');
    }
}

