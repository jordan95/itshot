<?php

class OpenTechiz_GoogleAnalyticsPlusExtended_Block_Remarketing extends Fooman_GoogleAnalyticsPlus_Block_Remarketing
{

    public function getEcommCategory()
    {
        if ($this->getPageType() == self::GA_PAGETYPE_PRODUCT) {
            return parent::getProductCategory(Mage::registry('current_product'));
        } else if ($this->getPageType() == self::GA_PAGETYPE_CATEGORY) {
            return Mage::registry('current_category')->getName();
        }
        return false;
    }

    public function shouldInclude()
    {
        $type = $this->getPageType();
        $amount_maximum = Mage::getStoreConfig('google/analyticsplus_dynremarketing/amount_maximum');
        $amount = floatval($amount_maximum);
        if ($amount > 0 && ($type == self::GA_PAGETYPE_PURCHASE || $type == self::GA_PAGETYPE_CART)) {
            $pageValue = $this->getPageValue();
            return $amount > $pageValue && parent::shouldInclude();
        } else {
            return parent::shouldInclude();
        }
    }
    
    public function getProdId()
    {
        $products = array();
        switch ($this->getPageType()) {
            case self::GA_PAGETYPE_PRODUCT:
                $products[] = $this->getConfiguredFeedId(Mage::registry('current_product'));
                return $this->getArrayReturnValue($products, "''", false, true);
                break;
            case self::GA_PAGETYPE_CART:
                $quote = Mage::getSingleton('checkout/session')->getQuote();
                if ($quote) {
                    foreach ($quote->getAllItems() as $item) {
                        $products[] = $this->getConfiguredFeedId($item->getProduct());
                    }
                }
                return $this->getArrayReturnValue($products, "''", true, true);
                break;
            case self::GA_PAGETYPE_PURCHASE:
                if ($this->_getOrder()) {
                    foreach ($this->_getOrder()->getAllItems() as $item) {
                        $products[] = $this->getConfiguredFeedId($item->getProduct());
                    }
                }
                return $this->getArrayReturnValue($products, "''", true, true);
                break;
        }
        return "''";
    }
    
    public function getArrayReturnValue($values, $default = '', $sort = false, $shouldQuote = false)
    {
        if (empty($values)) {
            return $default;
        }
        if ($sort) {
            asort($values);
        }
        $temp = array();
        foreach ($values as $value) {
            $temp[] = $this->prepareValue($value, $shouldQuote);
        }
        if (sizeof($temp) == 1) {
            return reset($temp);
        } else {
            return '[' . implode(',', $temp) . ']';
        }
    }
    
    public function prepareValue(&$value, $shouldQuote = false)
    {
        $value = $this->jsQuoteEscape($value);
        // quote if value is not numeric
        if (!ctype_digit($value) || $shouldQuote) {
            $value = "'$value'";
        }
        return $value;
    }

}
