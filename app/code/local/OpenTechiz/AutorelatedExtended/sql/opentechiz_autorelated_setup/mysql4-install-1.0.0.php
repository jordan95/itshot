<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('autorelatedextended/productkeywords');
$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
if ($connection->isTableExists($tableName)) {
    $connection->dropTable($tableName);
}

$table = $connection->newTable($tableName)
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
            'identity' => true,
            'primary' => true,
            'unsigned' => true,
            'nullable' => false,
                ], 'Id')
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
            'nullable' => false,
                ], 'Entity ID')
        ->addColumn('keywords', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
            'nullable' => true,
                ], 'Keywords')
        ->addIndex($installer->getIdxName('autorelatedextended/productkeywords', array('entity_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE), array('entity_id'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->addIndex($installer->getIdxName('autorelatedextended/productkeywords', array('keywords'), Varien_Db_Adapter_Interface::INDEX_TYPE_FULLTEXT), array('keywords'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_FULLTEXT))
        ->setComment('Product Keywords')
        ->setOption('type', 'MyISAM')
        ->setOption('charset', 'utf8');

$connection->createTable($table);
$installer->endSetup();
