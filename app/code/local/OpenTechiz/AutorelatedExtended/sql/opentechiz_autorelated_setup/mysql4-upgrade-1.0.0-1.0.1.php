<?php

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$connection->addColumn($installer->getTable('awautorelated/blocks'), 'related_product_ids', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => true,
    'comment'   => 'Related Product IDs'
));
$connection->addColumn($installer->getTable('awautorelated/blocks'), 'viewed_product_ids', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => true,
    'comment'   => 'Viewed Product IDs'
));
$installer->endSetup();
