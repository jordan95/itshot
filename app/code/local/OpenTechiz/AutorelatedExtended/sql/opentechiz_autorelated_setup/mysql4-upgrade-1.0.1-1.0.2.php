<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('catalog/product')}` ADD `aw_product_related` text NULL COMMENT 'AW Product Related';");
$installer->endSetup();
