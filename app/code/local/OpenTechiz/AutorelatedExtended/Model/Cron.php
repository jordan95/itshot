<?php

class OpenTechiz_AutorelatedExtended_Model_Cron
{

    const AW_PRODUCT_RELATED = 'aw_product_related';
    const AW_PRODUCT_RELATED_LASTTIME = 'aw_product_related_lasttime';
    const AW_PRODUCT_RELATED_LASTID = 'aw_product_related_lastid';

    protected $_entityId;
    protected $_from;
    protected $_to;
    protected $_flag;
    protected $_blockType = array(
        AW_Autorelated_Model_Source_Type::PRODUCT_PAGE_BLOCK,
        OpenTechiz_AutorelatedExtended_Model_Source_Type::FTS_PRODUCT_PAGE_BLOCK
    );
    protected $_attributeToSelect = array(
        'name',
        'price',
        'category_ids',
        'c2c_gender_for_filter',
        'meta_keyword',
        'attribute_set_id',
        'c2c_itemcode',
        'c2c_other',
        'sku',
        'c2c_colors'
    );
    
    protected $_debug = false;
    protected $_limit = 10;

    public function setFrom($from)
    {
        $this->_from = (int) $from;
        return $this;
    }

    public function setTo($to)
    {
        $this->_to = (int) $to;
        return $this;
    }

    public function setEntityId($entityId)
    {
        $this->_entityId = explode(',', $entityId);
        return $this;
    }

    public function setBlockType($type)
    {
        $this->_blockType = explode(',', $type);
        return $this;
    }

    public function setAttributeToSelect($attrs)
    {
        $this->_attributeToSelect = explode(',', $attrs);
        return $this;
    }

    public function setLimit($limit)
    {
        $this->_limit = $limit;
        return $this;
    }

    public function cronProductRelated()
    {
        $last_time = $this->getLastUpdate();
        $last_id = $this->getLastID();
        $products = $this->getProducCollectionLastUpdate($last_time, $last_id);
        if (!$products->count()) {
            return;
        }
        $blocks = $this->getBlocks($this->_blockType);
        if (!$blocks->count()) {
            return;
        }
        foreach ($products as $product) {
            $aw_product_related = $product->getData('aw_product_related');
            $data = $aw_product_related ? json_decode($aw_product_related, true) : array();
            foreach ($blocks as $block) {
                $blockInstance = null;
                switch ($block->getData('type')) {
                    case AW_Autorelated_Model_Source_Type::PRODUCT_PAGE_BLOCK:
                        $blockInstance = $this->getLayout()->createBlock('awautorelated/blocks_product');
                        break;
                    case OpenTechiz_AutorelatedExtended_Model_Source_Type::FTS_PRODUCT_PAGE_BLOCK:
                        $blockInstance = $this->getLayout()->createBlock('autorelatedextended/blocks_ftsproduct');
                        break;
                }
                if (!$blockInstance) {
                    continue;
                }
                $block->callAfterLoad();
                $blockInstance->setData($block->getData());
                $blockInstance->setCli(true);
                $gCondition = $block->getRelatedProducts()->getGeneral();
                $filteredIds = array();
                if (!empty($gCondition)) {
                    $blockInstance->_initCollection();
                    $filteredIds = $blockInstance->filterByAtts($product, $gCondition);
                    $data[$block->getId()] = $filteredIds;
                }
            }
            $this->updateAwProductRelated($product->getId(), json_encode($data));
            $entity_id = $product->getData('entity_id');
            $last_time = $product->getData('updated_at');
            if ($last_id < $entity_id) {
                $last_id = $entity_id;
            }
        }
        $this->saveLastUpdate($last_time, $last_id);
    }

    protected function getLastUpdate()
    {
        return $this->_limit ? $this->getData(self::AW_PRODUCT_RELATED_LASTTIME) : null;
    }

    protected function getLastID()
    {
        return (int) $this->_limit ? $this->getData(self::AW_PRODUCT_RELATED_LASTID) : null;
    }

    public function getData($name)
    {
        $flag = $this->getFlag();
        $data = $flag->getFlagData();
        if ($data && isset($data[$name])) {
            return $data[$name];
        }
        return null;
    }


    protected function saveLastUpdate($last_time, $last_id)
    {
        $flag = $this->getFlag();
        $flag->setFlagData(array(
            self::AW_PRODUCT_RELATED_LASTTIME => $last_time,
            self::AW_PRODUCT_RELATED_LASTID => $last_id
        ));
        $flag->save();
    }

    protected function getFlag()
    {
        if (!$this->_flag) {
            $this->_flag = Mage::getModel('core/flag', array('flag_code' => self::AW_PRODUCT_RELATED))->loadSelf();
        }
        return $this->_flag;
    }

    protected function getLayout()
    {
        return Mage::getSingleton('core/layout');
    }

    protected function getRequest()
    {
        return Mage::app()->getRequest();
    }

    protected function getProducCollectionLastUpdate($last_time = null, $last_id = null)
    {
        $request = $this->getRequest();
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addAttributeToSelect($this->_attributeToSelect);
        $collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
        $page = $request->getQuery('page');
        if ($page) {
            $collection->setCurPage($page);
        }
        //$limit = $request->getQuery('limit', 10);
        if ($this->_limit) {
            $collection->getSelect()->limit($this->_limit);
        }
        
        if ($last_time && $last_id) {
            $collection->addFieldToFilter(array(
               array(
                   'attribute' => 'updated_at',
                   'gt' => $last_time
               ),
               array(
                   'attribute' => 'entity_id',
                   'gt' => $last_id
               ) 
            ));
        } else if ($last_time) {
            $collection->addFieldToFilter('updated_at', array('gt' => $last_time));
        } else if ($last_id) {
            $collection->addFieldToFilter('entity_id', array('gt' => $last_id));
        }
        
        if ($this->_from) {
            $collection->addFieldToFilter('entity_id', array('gteq' => $this->_from));
        }
        if ($this->_to) {
            $collection->addFieldToFilter('entity_id', array('lteq' => $this->_to));
        }
        if ($this->_entityId) {
            $collection->addFieldToFilter('entity_id', array('in' => $this->_entityId));
        }
        $collection->addAttributeToSort('updated_at', Varien_Data_Collection::SORT_ORDER_ASC);
        if($this->_debug) {
            echo $collection->getSelect();
        }
        return $collection;
    }

    protected function getBlocks($type)
    {
        $blocks = Mage::getModel('awautorelated/blocks')->getCollection();
        if (is_array($type)) {
            $blocks->addFieldToFilter('type', array('in' => $type));
        } else {
            $blocks->addFieldToFilter('type', $type);
        }
        $blocks->addStatusFilter();
        return $blocks;
    }

    protected function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    protected function getConnection()
    {
        return $this->getResource()->getConnection('core_read');
    }

    protected function updateAwProductRelated($product_id, $data)
    {
        $resource = $this->getResource();
        $write = $resource->getConnection('core_write');
        $catalog_product_entity = $resource->getTableName('catalog/product');
        $write->query("UPDATE {$catalog_product_entity} SET aw_product_related = '{$data}' WHERE entity_id = {$product_id}");
    }

}
