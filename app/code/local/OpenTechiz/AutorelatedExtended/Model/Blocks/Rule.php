<?php

class OpenTechiz_AutorelatedExtended_Model_Blocks_Rule extends AW_Autorelated_Model_Blocks_Rule
{

    public function prepareSqlForAtt($attribute, $joinedAttributes, $collection, $operator, $value)
    {
        $nOperator = array('==', '!{}', '{}', '!()', '()');
        $mOperator = array('=', 'NOT LIKE', 'LIKE', 'NOT IN', 'IN');

        //category operator
        $cnOperator = array('==', '!=', '<=', '>=', '=', '>', '<', '!{}', '{}', '!()', '()', 'NOT LIKE', 'LIKE');
        $cmOperator = array('IN', 'NOT IN', 'NOT IN', 'NOT IN', 'IN', 'NOT IN', 'NO IN', 'NOT IN', 'IN',
            'NOT IN', 'IN', 'NOT IN', 'IN'
        );

        $operator = str_replace($nOperator, $mOperator, $operator);
        if ($attribute == 'category_ids') {
            $operator = str_replace($cnOperator, $cmOperator, $operator);
        }

        if (($operator == 'LIKE' || $operator == 'NOT LIKE') && is_array($value)) {
            if ($operator == 'LIKE') {
                $operator = 'FIND_IN_SET';
            } else {
                $operator = '!FIND_IN_SET';
            }
        }

        /* Quote rule value depending operator type */
        switch ($operator) {
            case 'LIKE':
            case 'NOT LIKE':
                $value = $collection->getConnection()->quote("%{$value}%");
                break;
            case 'IN':
            case 'NOT IN':
                if (!is_array($value)) {
                    $value = array_filter(array_map('trim', explode(',', $value)));
                }
                $value = '(' . $collection->getConnection()->quote($value) . ')';
                break;
            case 'FIND_IN_SET':
            case '!FIND_IN_SET':
                $_resultSql = '(' . $collection->getConnection()->quote($value[0])
                        . ', att_table_' . $attribute . '.' . 'value)'
                ;
                unset($value[0]);
                foreach ($value as $_conditionValue) {
                    $_resultSql .= ' OR ' . $operator . '(' . $collection->getConnection()->quote($_conditionValue)
                            . ', att_table_' . $attribute . '.' . 'value)'
                    ;
                }
                $value = $_resultSql;
                break;
            default:
                if (is_array($value)) {
                    $value = implode(',', $value);
                }
                $value = $collection->getConnection()->quote($value);
        }

        if (!in_array($attribute, $this->_joinedAttributes)) {
            array_push($this->_joinedAttributes, $attribute);
            $att = Mage::getModel('catalog/product')->getResource()->getAttribute($attribute);

            if (!$att && $attribute != 'category_ids')
                return null;

            switch ($attribute) {
                case 'meta_keyword':
                    $collection
                            ->getSelect()
                            ->joinLeft(
                                    array(
                                'fts_keywords' => $collection->getTable('autorelatedextended/productkeywords')
                                    ), 'fts_keywords.entity_id = e.entity_id', array()
                            )
                    ;
                    break;
                case 'sku':
                    $collection
                            ->getSelect()
                            ->join(
                                    array(
                                'att_table_' . $attribute => $att->getBackend()->getTable()
                                    ), 'att_table_' . $attribute . '.entity_id = e.entity_id', array('sku')
                            )
                    ;
                    break;
                case 'category_ids':
                    $collection
                            ->getSelect()
                            ->joinLeft(
                                    array(
                                'att_table_' . $attribute => $collection->getTable('catalog/category_product')
                                    ), 'att_table_' . $attribute . '.product_id = e.entity_id', array('category_id')
                            )
                    ;
                    break;
                case 'attribute_set_id':
                    $collection
                            ->getSelect()
                            ->joinLeft(
                                    array(
                                'att_table_' . $attribute => $att->getBackend()->getTable()
                                    ), 'att_table_' . $attribute . '.entity_id = e.entity_id', array('attribute_set_id')
                            )
                    ;
                    break;
                case 'price':
                    $collection->addPriceData();
                    break;
                default:
                    $collection
                            ->getSelect()
                            ->joinLeft(
                                    array(
                                'att_table_' . $attribute => $att->getBackend()->getTable()
                                    ), 'att_table_' . $attribute . '.entity_id = e.entity_id AND att_table_'
                                    . $attribute . '.attribute_id = ' . $att->getId(), array('value')
                            )
                    ;
            }
        }

        switch ($attribute) {
            case 'meta_keyword': {
                    $keywords = explode(',', trim($value, " \t\n\r\0\x0B\'"));
                    $temp = array();
                    foreach ($keywords as $word) {
                        $temp[] = addslashes(trim($word));
                    }
                    $words = array_unique(preg_split("/[\s,]+/", $value));
                    foreach ($words as $w) {
                        if (is_numeric($w) || !$w) {
                            continue;
                        }
                        $temp[] = addslashes(trim($w, " \t\n\r\0\x0B\'"));
                    }
                    if (count($temp) > 0) {
                        $pattern = sprintf('"%s"', join('" >"', $temp));
                        $where = "(MATCH(fts_keywords.keywords) AGAINST ('>{$pattern}' IN BOOLEAN MODE))";
                    }
                    break;
                }
            case 'sku':
                $where = '(att_table_' . $attribute . '.' . 'sku' . ' ' . $operator . ' ' . $value . ')';
                break;
            case 'category_ids':
                $where = '(IFNULL(att_table_' . $attribute . '.' . 'category_id,\'\')'
                        . ' ' . $operator . ' ' . $value . ' AND e.entity_id ' . $operator
                        . '(SELECT `product_id` FROM `' . $collection->getTable('catalog/category_product')
                        . '` WHERE `category_id` IN' . $value . ')' . '    )'
                ;
                break;
            case 'price':
                $where = '(IF(price_index.final_price IS NULL,' .
                        'IF(price_index.min_price > 0, price_index.min_price, price_index.max_price),' .
                        'IF(price_index.final_price > 0, price_index.final_price, price_index.min_price) ) ' . $operator . ' ' . $value . ')'
                ;
                break;
            case 'attribute_set_id':
                $where = '(IFNULL(att_table_' . $attribute . '.'
                        . 'attribute_set_id,\'\')' . ' ' . $operator . ' ' . $value . ')'
                ;
                break;
            default:
                $where = '(IFNULL(att_table_' . $attribute . '.' . 'value,\'\')'
                        . ' ' . $operator . ' ' . $value . ')'
                ;
                if ($operator == 'FIND_IN_SET' || $operator == '!FIND_IN_SET') {
                    $where = '(' . $operator . ' ' . $value . ')';
                }
        }
        return $where;
    }

}
