<?php

class OpenTechiz_AutorelatedExtended_Model_Blocks_Product_Rulerelated extends AW_Autorelated_Model_Blocks_Product_Ruleviewed
{

    public function getMatchingProductIds($matches = null)
    {
        if (boolval($matches)) {
            return $matches;
        }
        return parent::getMatchingProductIds();
    }

}
