<?php

class OpenTechiz_AutorelatedExtended_Model_Source_Type extends AW_Autorelated_Model_Source_Type
{

    const FTS_PRODUCT_PAGE_BLOCK = 19;
    const FTS_PRODUCT_PAGE_BLOCK_LABEL = 'Product Related';

    public function toOptionArray()
    {
        $_helper = $this->_getHelper();
        $result = parent::toOptionArray();
        $result[] = array('value' => self::FTS_PRODUCT_PAGE_BLOCK, 'label' => $_helper->__(self::FTS_PRODUCT_PAGE_BLOCK_LABEL));
        return $result;
    }

    public function toArray()
    {
        $_helper = $this->_getHelper();
        $result = parent::toArray();
        $result[self::FTS_PRODUCT_PAGE_BLOCK] = $_helper->__(self::FTS_PRODUCT_PAGE_BLOCK_LABEL);
        return $result;
    }

}
