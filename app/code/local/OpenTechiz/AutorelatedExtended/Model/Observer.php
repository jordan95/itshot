<?php

class OpenTechiz_AutorelatedExtended_Model_Observer
{
    protected $_crosssellsBlocksSize = null;
    public function saveKeywords($observer)
    {
        $product = $observer->getProduct();
        $keyword = Mage::getModel('autorelatedextended/keywords')->load($product->getId(), 'entity_id');
        $keyword->setData('entity_id', $product->getId());
        $keyword->setData('keywords', $product->getData('meta_keyword'));
        $keyword->save();
    }

    public function beforeSaveAutorelatedBlocks($observer)
    {
        $block = $observer->getBlock();
        $relatedPrids = array();
        $viewedPrids = array();
        $productLimit = 3;

        $viewed_cons = array();
        $conditions = array();
        $viewed = @unserialize($block->getCurrentlyViewed());
        $related = @unserialize($block->getRelatedProducts());

        if (sizeof($viewed) && isset($viewed['conditions'])) {
            $viewed_cons = $viewed['conditions'];
        }
        if (sizeof($related) && isset($related['related'])) {
            $conditions = $related['related'];
            $qty = 0;
            if (isset($related['product_qty']) && $related['product_qty']) {
                $qty = $related['product_qty'];
            } elseif (isset($related['count']) && $related['count']) {
                $qty = $related['count'];
            }
            $productLimit = $qty + $productLimit;
        }
        if (isset($viewed_cons['viewed'])) {
            $model = Mage::getModel('awautorelated/blocks_product_ruleviewed');
            $model->setWebsiteIds(1);
            $model->getConditions()->loadArray($viewed_cons, 'viewed');
            $match = $model->getMatchingProductIds();
            $viewedPrids[1] = $match;
        }
        if (isset($conditions['conditions']['related'])) {
            $relatedmodel = Mage::getModel('awautorelated/blocks_product_rulerelated');
            $relatedmodel->setWebsiteIds(1);
            $relatedmodel->getConditions()->loadArray($conditions['conditions'], 'related');
            $matchIds = $relatedmodel->getMatchingProductIds();
            $relatedPrids[1] = $matchIds;
        }
        $block->setData('related_product_ids', json_encode($relatedPrids));
        $block->setData('viewed_product_ids', json_encode($viewedPrids));
    }

    public function getDefaultStoreId()
    {
        if (strlen($code = Mage::getSingleton('adminhtml/config_data')->getStore())) { // store level
            $store_id = Mage::getModel('core/store')->load($code)->getId();
        } elseif (strlen($code = Mage::getSingleton('adminhtml/config_data')->getWebsite())) { // website level
            $website_id = Mage::getModel('core/website')->load($code)->getId();
            $store_id = Mage::app()->getWebsite($website_id)->getDefaultStore()->getId();
        } else { // default level
            $store_id = 0;
        }
        return $store_id;
    }

    public function replaceCrossselsBlock($observer)
    {
        $checkoutPage = array('cart_index_checkout', 'checkout_cart_index');

        if (!in_array($this->getPageView(), $checkoutPage))
            return;

        if (!$this->_getShoppingCartCrosssellsBlocks() || !$observer->getBlock() instanceof Mage_Checkout_Block_Cart) {
            return;
        }
        /** @var $layout Mage_Core_Model_Layout */
        $layout = Mage::app()->getLayout();
        /** @var $helper AW_Autorelated_Helper_Data */
        $helper = Mage::helper('awautorelated');
        if (!$helper->getExtDisabled()) {
            /** @var $shoppingCartBlock Mage_Checkout_Block_Cart */
            $shoppingCartBlock = $observer->getBlock();
            /** @var $arpBlock AW_Autorelated_Block_Blocks */
            $arpBlock = $layout->createBlock('awautorelated/blocks', 'aw.arp2.shc.crosssells');

            $crosssellBlock = $shoppingCartBlock->getChild('crosssell');
            if ($crosssellBlock instanceof AW_Relatedproducts_Block_Relatedproducts) {
                $crosssellBlock->setData('_aw_arp2_cs_block', $arpBlock);
            } else {
                $shoppingCartBlock->setChild('crosssell', $arpBlock);
            }
        }
        return $this;
    }

    private function getPageView()
    {
        $controllerName = Mage::app()->getRequest()->getControllerName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        return $controllerName . '_' . $actionName . '_' . $routeName;
    }

    protected function _getShoppingCartCrosssellsBlocks()
    {
        if (!$this->_crosssellsBlocksSize) {
            /** @var $collection AW_Autorelated_Model_Mysql4_Blocks_Collection */
            $collection = Mage::getModel('awautorelated/blocks')->getCollection();
            $collection->addStoreFilter()
                    ->addTypeFilter(AW_Autorelated_Model_Source_Type::SHOPPING_CART_BLOCK)
                    ->addPositionFilter(AW_Autorelated_Model_Source_Position::REPLACE_CROSSSELS_BLOCK);
            $this->_crosssellsBlocksSize = $collection->getSize() > 0;
        }
        return $this->_crosssellsBlocksSize;
    }

    public function replaceFormkeyInAwBlock(Varien_Event_Observer $observer)
    {
        $awBlockClass = array(
            'OpenTechiz_AutorelatedExtended_Block_Blocks',
            'OpenTechiz_AutorelatedExtended_Block_Blocks_Ftsproduct',
            'AW_Autorelated_Block_Blocks_Homepage',
            'AW_Autorelated_Block_Blocks_Category',
            'AW_Autorelated_Block_Blocks_Shoppingcart',
            'AW_Autorelated_Block_Blocks_Product',
            'AW_Autorelated_Block_Blocks'
        );
        $block = $observer->getEvent()->getBlock();
        if (!$block)
            return;

        $blockClass = get_class($block);
        if (!in_array($blockClass, $awBlockClass))
            return;

        $html = $observer->getTransport()->getHtml();
        if (!trim($html))
            return;

        $formKeyPlaceholder = OpenTechiz_AutorelatedExtended_Helper_Data::FORM_KEY_PLACEHOLDER;
        $currentFormKey = Mage::getSingleton('core/session')->getFormKey();
        $newHtml = str_replace($formKeyPlaceholder, $currentFormKey, $html);
        $observer->getTransport()->setHtml($newHtml);
    }

}
