<?php

class OpenTechiz_AutorelatedExtended_Model_Keywords extends Mage_Core_Model_Abstract
{
    protected $_eventPrefix = 'autorelatedextended_keywords';
    protected $_eventObject = 'keyword';
    
    protected function _construct()
    {
        $this->_init('autorelatedextended/keywords');
    }

}
