<?php

class OpenTechiz_AutorelatedExtended_Model_Mysql4_Keywords extends Mage_Core_Model_Mysql4_Abstract
{

    protected function _construct()
    {
        $this->_init('autorelatedextended/productkeywords', 'id');
    }

}
