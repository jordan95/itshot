<?php
require_once Mage::getModuleDir('controllers','AW_Autorelated').DS.'Adminhtml/Awautorelated/BlocksgridController.php';

class OpenTechiz_AutorelatedExtended_Adminhtml_Awautorelated_BlocksgridController extends AW_Autorelated_Adminhtml_Awautorelated_BlocksgridController
{
    
    public function editAction()
    {
        $id = (int)$this->getRequest()->getParam('id');
        $type = Mage::getModel('awautorelated/blocks')->getTypeById($id);

        switch ($type) {
             case OpenTechiz_AutorelatedExtended_Model_Source_Type::FTS_PRODUCT_PAGE_BLOCK:
                return $this->_redirect('*/awautorelated_ftsproductblock/edit', array('id' => $id));
                break;
            case AW_Autorelated_Model_Source_Type::PRODUCT_PAGE_BLOCK:
                return $this->_redirect('*/awautorelated_productblock/edit', array('id' => $id));
                break;
            case AW_Autorelated_Model_Source_Type::CATEGORY_PAGE_BLOCK:
                return $this->_redirect('*/awautorelated_categoryblock/edit', array('id' => $id));
                break;
            case AW_Autorelated_Model_Source_Type::SHOPPING_CART_BLOCK:
                return $this->_redirect('*/awautorelated_shoppingcartblock/edit', array('id' => $id));
                break;
        }
        return $this->_redirect('*/*/list');
    }

    public function selecttypeAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->_initAction()->_setTitle('Select block type')->renderLayout();
        } else {
            $blockType = $this->getRequest()->getParam('block_type');
            $blockTypesModel = Mage::getModel('awautorelated/source_type');
            $_redirect = '*/*/selecttype';
            if ($blockType && $blockTypesModel->getOption($blockType) !== false) {
                switch ($blockType) {
                    case OpenTechiz_AutorelatedExtended_Model_Source_Type::FTS_PRODUCT_PAGE_BLOCK:
                        $_redirect = '*/awautorelated_ftsproductblock/new';
                        break;
                    case AW_Autorelated_Model_Source_Type::PRODUCT_PAGE_BLOCK:
                        $_redirect = '*/awautorelated_productblock/new';
                        break;
                    case AW_Autorelated_Model_Source_Type::CATEGORY_PAGE_BLOCK:
                        $_redirect = '*/awautorelated_categoryblock/new';
                        break;
                    case AW_Autorelated_Model_Source_Type::SHOPPING_CART_BLOCK:
                        $_redirect = '*/awautorelated_shoppingcartblock/new';
                        break;
                }
            }
            return $this->_redirect($_redirect);
        }
        return $this;
    }
}