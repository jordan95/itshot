<?php

require_once Mage::getModuleDir('controllers', 'AW_Autorelated') . DS . 'Adminhtml/Awautorelated/ProductblockController.php';

class OpenTechiz_AutorelatedExtended_Adminhtml_Awautorelated_FtsproductblockController extends AW_Autorelated_Adminhtml_Awautorelated_ProductblockController
{
     protected function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('awautorelated/blocks')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            //$model = Mage::getModel('awautorelated/rule');


            Mage::register('productblock_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('catalog/awautorelated');
            $this->_setTitle($id ? 'Edit Product Block' : 'Add Product Block');
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true)->setCanLoadRulesJs(true);

            $this->_addContent($this->getLayout()->createBlock('autorelatedextended/adminhtml_blocks_ftsproduct_edit'))
                ->_addLeft($this->getLayout()->createBlock('autorelatedextended/adminhtml_blocks_ftsproduct_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('awautorelated')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }
    public function saveAction()
    {
        if ($this->getRequest()->isPost()) {
            $id = ($this->getRequest()->getParam('saveasnew')) ? null : (int)$this->getRequest()->getParam('id');
            $blockModel = Mage::getModel('awautorelated/blocks')->load($id);
            try {
                $data = $this->getRequest()->getParams();
                unset($data['id']);
                $data = $this->_filterDates($data, array('date_from', 'date_to'));

                $data = $this->_prepareRelatedAndViewedTabsData($data);

                $blockModel->addData($data);
                $blockModel->setType(OpenTechiz_AutorelatedExtended_Model_Source_Type::FTS_PRODUCT_PAGE_BLOCK);
                $blockModel->save();
                Mage::getSingleton('adminhtml/session')->addSuccess("Block successfully saved");
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }

            if ($this->getRequest()->getParam('back') || $this->getRequest()->getParam('saveasnew')) {
                return $this->_redirect('*/*/edit', array('id' => $blockModel->getId()));
            }
        }
        return $this->_redirect('*/awautorelated_blocksgrid/list');
    }
    
    protected function _prepareRelatedAndViewedTabsData(array $data)
    {
        $rule = $data['rule'];
        $rule['viewed'] = Mage::helper('awautorelated')->updateChild(
            $rule['viewed'],
            'catalogrule/rule_condition_combine',
            'awautorelated/catalogrule_rule_condition_combine'
        );

        $rule['related'] = Mage::helper('awautorelated')->updateChild(
            $rule['related'],
            'catalogrule/rule_condition_combine',
            'awautorelated/catalogrule_rule_condition_combine'
        );

        $conditions = Mage::helper('awautorelated')->convertFlatToRecursive($rule, array('viewed', 'related'));
        $data['currently_viewed']['conditions'] = $conditions['viewed']['viewed_conditions_fieldset'];
        $data['related_products']['conditions'] = $conditions['related']['related_conditions_fieldset'];

        $general = $data['general'];
        $filtered = array();
        foreach ($general as $row) {
            if (!empty($row['att']) && !empty($row['condition']))
                $filtered[] = $row;
        }

        $relatedPostData = $data['related_products'];

        $relatedData = array(
            'general'           => $filtered,
            'related'           => $data['related_products'],
            'product_qty'       => $data['product_qty'],
            'maximum_limit_result_related'       => $data['maximum_limit_result_related'],
            'prior_category_ids'       => $data['prior_category_ids'],
            'prior_price'       => $data['prior_price'],
            'prior_c2c_gender'       => $data['prior_c2c_gender'],
            'prior_meta_keyword'       => $data['prior_meta_keyword'],
            'percent_max_price_allow'       => $data['percent_max_price_allow'],
            'show_out_of_stock' => $relatedPostData['show_out_of_stock'],
            'order'             => $relatedPostData['order']
        );

        $data['related_products'] = $relatedData;
        return $data;
    }
}
