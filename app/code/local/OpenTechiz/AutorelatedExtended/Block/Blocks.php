<?php

class OpenTechiz_AutorelatedExtended_Block_Blocks extends AW_Autorelated_Block_Blocks {
    public function getBlocksHtml()
    {
        $out = '';
        foreach ($this->getBlocks() as $block) {
            $blockInstance = null;
            switch ($block->getData('type')) {
                case OpenTechiz_AutorelatedExtended_Model_Source_Type::FTS_PRODUCT_PAGE_BLOCK:
                    $blockInstance = $this->getLayout()->createBlock('autorelatedextended/blocks_ftsproduct');
                    break;
                case AW_Autorelated_Model_Source_Type::PRODUCT_PAGE_BLOCK:
                    $blockInstance = $this->getLayout()->createBlock('awautorelated/blocks_product');
                    break;
                case AW_Autorelated_Model_Source_Type::CATEGORY_PAGE_BLOCK:
                    $blockInstance = $this->getLayout()->createBlock('awautorelated/blocks_category');
                    break;
                case AW_Autorelated_Model_Source_Type::SHOPPING_CART_BLOCK:
                    $blockInstance = $this->getLayout()->createBlock('awautorelated/blocks_shoppingcart');
                    break;
            }
            if ($blockInstance) {
                $block->callAfterLoad();
                $blockInstance->setData($block->getData())
                    ->setParent($this);
                $out .= $blockInstance->toHtml();
            }
        }
        return $out;
    }
}
