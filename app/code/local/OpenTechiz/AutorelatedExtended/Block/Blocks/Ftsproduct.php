<?php

class OpenTechiz_AutorelatedExtended_Block_Blocks_Ftsproduct extends OpenTechiz_AutorelatedExtended_Block_Blocks_Product
{

    private $_ifColumnRelevance = array();

    public function filterByAtts(Mage_Catalog_Model_Product $currentProduct, $atts, $ids = null)
    {
        // if aw_product_related is false and not cli then use default filter
        // frontend
        if (!$this->getCli()) {
            return parent::filterByAtts($currentProduct, $atts, $ids);
        } 
        $this->_joinedAttributes = array();
        $collection = $this->_collection;
        $rule = Mage::getModel('awautorelated/blocks_rule');
        $prior = array(
            'category_ids' => $this->getPriorCategoryIds(),
            'price' => $this->getPriorPrice(),
            'c2c_gender' => $this->getPriorGender(),
            'meta_keyword' => $this->getPriorKeyword()
        );
        $collection->getSelect()->order('relevance DESC');
        foreach ($atts as $at) {
            /*
             *  collect category ids related to product
             *  If category is anchor we should implode all of its subcategories as value
             *  If it's not we should get only its id
             *  If there is no category in product, get all categories product is in
             */
            if ($at['att'] == 'category_ids') {
                $category = $currentProduct->getCategory();
                if ($category instanceof Varien_Object) {
                    if ($category->getIsAnchor()) {
                        $value = $category->getAllChildren();
                    } else {
                        $value = $category->getId();
                    }
                } else {
                    $conditionCategoryIds = array();
                    $productCategoryIds = $currentProduct->getCategoryIds();
                    $conditions = $this->getRelatedProducts()->getRelated();
                    if (isset($conditions['conditions']['related'])) {
                        foreach ($conditions['conditions']['related'] as $_condition) {
                            if ($_condition['attribute'] == 'category_ids') {
                                foreach (explode(',', $_condition['value']) as $_value) {
                                    $conditionCategoryIds[] = trim($_value);
                                }
                            }
                        }
                    }
                    $filteredCategoryIds = $this->_filterCategoryIds($currentProduct->getCategoryIds());
                    $value = implode(',', $filteredCategoryIds);
                    $value = !empty($value) ? $value : null;
                }
            } elseif ($at['att'] == 'price') {
                if (
                        $currentProduct->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE ||
                        $currentProduct->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_GROUPED
                ) {
                    $taxHelper = Mage::helper('tax');
                    $value = $taxHelper->displayPriceIncludingTax() ?
                            $taxHelper->getPrice($currentProduct, $currentProduct->getMinimalPrice(), true) :
                            $taxHelper->getPrice($currentProduct, $currentProduct->getMinimalPrice())
                    ;
                } else {
                    $value = $currentProduct->getFinalPrice();
                }
            } else {
                $value = $currentProduct->getData($at['att']);
            }
            if (is_null($value)) {
                $collection = NULL;
                return false;
            }
            $sql = $rule->prepareSqlForAtt($at['att'], $this->_joinedAttributes, $collection, $at['condition'], $value);
            if ($sql) {
                $score = isset($prior[$at['att']]) ? $prior[$at['att']] : 3;
                if ($at['att'] == 'meta_keyword') {
                    $collection->getSelect()->columns(array('matched' => new Zend_Db_Expr($sql)));
                    $collection->getSelect()->order('matched DESC');
                } else if ($at['att'] == 'price') {
                    $percentMaxPriceAllow = $this->getPercentMaxPriceAllow();
                    if ($percentMaxPriceAllow) {
                        $priceColumn = '(IF(price_index.final_price IS NULL,' .
                                'IF(price_index.min_price > 0, price_index.min_price, price_index.max_price),' .
                                'IF(price_index.final_price > 0, price_index.final_price, price_index.min_price) )';
                        $price = floatval($value);
                        $value_new = (1 + intval($percentMaxPriceAllow) / 100) * $price;
                        $priceSql = $priceColumn . ' BETWEEN ' . $collection->getConnection()->quote(floatval($value)) . ' AND ' . $collection->getConnection()->quote($value_new) . ')';
                        $this->_ifColumnRelevance[] = sprintf('IF(%s, %s, %s)', $priceSql, 1, 0);
                    }
                }
                $collection->getSelect()->orWhere($sql);
                $this->_ifColumnRelevance[] = sprintf('IF(%s, %s, %s)', $sql, $score, 0);
            }
        }
        if ($ids) {
            $collection->getSelect()->where('e.entity_id IN(' . implode(',', $ids) . ')');
        }
        $collection->getSelect()->group('e.entity_id');
        $collection->getSelect()->columns(array('e.entity_id', 'relevance' => new Zend_Db_Expr(sprintf('(%s)', join(' + ', $this->_ifColumnRelevance)))));
        $collection->getSelect()->limit($this->getMaximumLimitResultRelated());
        $ids = array();
        foreach ($collection as $item) {
            $ids[] = $item->getData('entity_id');
        }
        return $ids;
    }

    public function getPercentMaxPriceAllow()
    {
        return $this->_getRelatedProducts()->getData('percent_max_price_allow');
    }

    public function getMaximumLimitResultRelated()
    {
        return $this->_getRelatedProducts()->getData('maximum_limit_result_related');
    }

    public function getPriorCategoryIds()
    {
        return $this->_getRelatedProducts()->getData('prior_category_ids');
    }

    public function getPriorPrice()
    {
        return $this->_getRelatedProducts()->getData('prior_price');
    }

    public function getPriorGender()
    {
        return $this->_getRelatedProducts()->getData('prior_c2c_gender');
    }

    public function getPriorKeyword()
    {
        return $this->_getRelatedProducts()->getData('prior_meta_keyword');
    }

    private function addRelevanceAttrSet($collection, Mage_Catalog_Model_Product $currentProduct)
    {
        $attribute = 'attribute_set_id';
        $operator = '=';
        $att = Mage::getModel('catalog/product')->getResource()->getAttribute($attribute);
        $collection
                ->getSelect()
                ->joinLeft(
                        array(
                    'att_table_' . $attribute => $att->getBackend()->getTable()
                        ), 'att_table_' . $attribute . '.entity_id = e.entity_id', array('attribute_set_id')
                )
        ;
        $this->IfColumnRelevance('(IFNULL(att_table_' . $attribute . '.'
                . 'attribute_set_id,\'\')' . ' ' . $operator . ' ' . $collection->getConnection()->quote($value) . ')', 10);
        return $this;
    }

    private function addRelevancePrice($collection, Mage_Catalog_Model_Product $currentProduct)
    {
        if (
                $currentProduct->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE ||
                $currentProduct->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_GROUPED
        ) {
            $taxHelper = Mage::helper('tax');
            $value = $taxHelper->displayPriceIncludingTax() ?
                    $taxHelper->getPrice($currentProduct, $currentProduct->getMinimalPrice(), true) :
                    $taxHelper->getPrice($currentProduct, $currentProduct->getMinimalPrice())
            ;
        } else {
            $value = $currentProduct->getFinalPrice();
        }
        if (is_null($value)) {
            $collection = NULL;
            return $this;
        }
        $collection->addPriceData();
        $percent = Mage::getStoreConfig('autorelated/general/maximum_price_percent');
        $percentNumber = intval($percent);
        $column = '(IF(price_index.final_price IS NULL,' .
                'IF(price_index.min_price > 0, price_index.min_price, price_index.max_price),' .
                'IF(price_index.final_price > 0, price_index.final_price, price_index.min_price) )';
        $this->IfColumnRelevance($column . ' >= ' . $collection->getConnection()->quote($value) . ')', 10);
        if ($percentNumber) {
            $price = floatval($value);
            $value_new = (1 + $percentNumber / 100) * $price;
            $this->IfColumnRelevance($column . ' BETWEEN ' . $collection->getConnection()->quote($value) . ' AND ' . $collection->getConnection()->quote($value_new) . ')', 20);
        }
        return $this;
    }

    private function IfColumnRelevance($condition, $scoreSuccess = 1, $scoreFailure = 0)
    {
        $this->_ifColumnRelevance[] = sprintf('IF(%s, %s, %s)', $condition, $scoreSuccess, $scoreFailure);
        return $this;
    }

    private function addFilterKeywords($collection, Mage_Catalog_Model_Product $currentProduct, $meta_keyword_related = null)
    {
        $productKeywordsTbl = Mage::getSingleton('core/resource')->getTableName('autorelatedextended/productkeywords');
        $collection
                ->getSelect()
                ->joinLeft(
                        array(
                    'productKeywords' => $productKeywordsTbl
                        ), 'productKeywords.entity_id = e.entity_id', array()
                )
        ;
        $this->addColumnRelevance($collection, $currentProduct);
        return $this;
    }

    private function addFilterCategory($collection, Mage_Catalog_Model_Product $currentProduct, $meta_keyword_related = '')
    {
        $category = $currentProduct->getCategory();
        if ($category instanceof Varien_Object) {
            if ($category->getIsAnchor()) {
                $value = $category->getAllChildren();
            } else {
                $value = $category->getId();
            }
        } else {
            $conditionCategoryIds = array();
            $productCategoryIds = $currentProduct->getCategoryIds();
            $conditions = $this->getRelatedProducts()->getRelated();
            if (isset($conditions['conditions']['related'])) {
                foreach ($conditions['conditions']['related'] as $_condition) {
                    if ($_condition['attribute'] == 'category_ids') {
                        foreach (explode(',', $_condition['value']) as $_value) {
                            $conditionCategoryIds[] = trim($_value);
                        }
                    }
                }
            }
            $filteredCategoryIds = $this->_filterCategoryIds($currentProduct->getCategoryIds());
            $value = implode(',', $filteredCategoryIds);
            $value = !empty($value) ? $value : null;
        }
        if (is_null($value)) {
            $collection = NULL;
            return $this;
        }
        $value = '(' . $collection->getConnection()->quote($value) . ')';
        $attibute = 'category_ids';
        $operator = 'IN';
        $collection
                ->getSelect()
                ->joinLeft(
                        array(
                    'att_table_' . $attribute => $collection->getTable('catalog/category_product')
                        ), 'att_table_' . $attribute . '.product_id = e.entity_id', array('category_id')
                )
        ;
        $where = '(IFNULL(att_table_' . $attribute . '.' . 'category_id,\'\')'
                . ' ' . $operator . ' ' . $value . ' AND e.entity_id ' . $operator
                . '(SELECT `product_id` FROM `' . $collection->getTable('catalog/category_product')
                . '` WHERE `category_id` IN ' . $value . '))';
        $this->IfColumnRelevance($where, 10);
        if ($meta_keyword_related) {
            $where .= ' OR MATCH(productKeywords.keywords) AGAINST (\'>' . $meta_keyword_related . '\' IN BOOLEAN MODE)';
        }
        $collection->getSelect()->where($where);
        return $this;
    }

    private function addColumnRelevance($collection, Mage_Catalog_Model_Product $currentProduct)
    {
        $meta_keyword = $currentProduct->getData('meta_keyword');
        $keywords = explode(",", $meta_keyword);
        $temp = array();
        foreach ($keywords as $word) {
            $temp[] = addslashes(trim($word));
        }
        $words = array_unique(preg_split("/[\s,]+/", $meta_keyword));
        foreach ($words as $w) {
            if (is_numeric($w) || !$w) {
                continue;
            }
            $temp[] = addslashes(trim($w));
        }
        $meta_keyword_related = sprintf('"%s"', join('" >"', $temp));
        if (count($temp) > 0) {
            $this->_ifColumnRelevance[] = 'MATCH(productKeywords.keywords) AGAINST (\'>' . $meta_keyword_related . '\' IN BOOLEAN MODE)';
            $this->addFilterCategory($collection, $currentProduct, $meta_keyword_related);
        }
        $this->addRelevancePrice($collection, $currentProduct)
                ->addRelevanceAttrSet($collection, $currentProduct);

        $collection->getSelect()->columns(array('relevance' => new Zend_Db_Expr(sprintf('(%s)', join(' + ', $this->_ifColumnRelevance)))));
        $collection->getSelect()->order('relevance DESC');
        return $this;
    }

    private function repeatWords(&$target, $words, $length = 2)
    {
        $indexs = array();
        foreach ($words as $word) {
            $ws = $this->extractWords($word, $length);
            foreach ($ws as $w) {
                $indexs[$w] = !isset($indexs[$w]) ? 1 : $indexs[$w] + 1;
            }
        }
        foreach ($indexs as $k => $v) {
            if ($v <= 1) {
                continue;
            }
            if (!in_array($k, $target)) {
                $target[] = addslashes($k);
            }
        }
        return $this;
    }

    private function extractWords($s, $length = 2)
    {
        $output = array();
        $a = preg_split("/[\s,]+/", trim($s));
        $len = count($a);
        for ($i = 0; $i < $len - 1; $i++) {
            $temp = array();
            for ($j = 0; $j < $length; $j++) {
                if (!isset($a[$i + $j])) {
                    continue;
                }
                $temp[] = $a[$i + $j];
            }
            $output[] = trim(join(' ', $temp));
        }
        return $output;
    }

}
