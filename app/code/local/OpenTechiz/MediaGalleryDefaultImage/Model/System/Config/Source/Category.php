<?php

class OpenTechiz_MediaGalleryDefaultImage_Model_System_Config_Source_Category
{
    /**
     * @return array
     */
    public function toOptionArray($isMultiselect = false)
    {
        $options = array();
        if (!$isMultiselect) {
            $options[] = array(
                'label' => Mage::helper('opentechiz_jewerlymargins')->__('-- None --'),
                'value' => ''
            );
        }

        $categoriesTreeView = $this->getCategoriesTreeView();

        foreach ($categoriesTreeView as $value) {
            $catName = $value['label'];
            $catId = $value['value'];
            $catLevel = $value['level'];

            $hyphen = '';
            for ($i = 1; $i < $catLevel; $i++){
                $hyphen = $hyphen . '-';
            }

            $catName = $hyphen ? $hyphen . ' ' . $catName : $catName;
            $options[] = array(
                'label' => $catName,
                'value' => $catId
            );
        }

        return $options;
    }

    /**
     * @return array
     */
    private function getCategoriesTreeView() {
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSort('path', 'asc')
            ->addFieldToFilter('is_active', ['eq'=>'1'])
            ->load()
            ->toArray();

        $categoryList = array();
        foreach ($categories as $id => $category) {
            if (isset($category['name'])) {
                $categoryList[] = array(
                    'label' => $category['name'],
                    'level'  =>$category['level'],
                    'value' => $id
                );
            }
        }

        return $categoryList;
    }
}
