<?php

class Opentechiz_MediaGalleryDefaultImage_Model_Observer
{

    public function update_media_gallery_default_image(Varien_Event_Observer $observer)
    {
        if(!is_null(Mage::registry(__FUNCTION__))) {
            return;
        }
        $helper = $this->getHelper();
        if (!$helper->isModuleEnabled()) {
            return;
        }
        $product = $observer->getProduct();
        if ($product->getData('disable_default_image')) {
            return;
        }
        $conditionIds = $helper->getConditionIdsByProduct($product);
        $product->setData(OpenTechiz_MediaGalleryDefaultImage_Helper_Data::ATTR_CODE, join(',', $conditionIds));        
        Mage::register(__FUNCTION__, true);
    }

    public function populateAll()
    {
        $this->getHelper()->populateAll();
    }

    protected function getHelper()
    {
        return Mage::helper('media_gallery_default_image');
    }

}
