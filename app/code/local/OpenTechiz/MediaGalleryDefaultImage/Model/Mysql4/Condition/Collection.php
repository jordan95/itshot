<?php

class OpenTechiz_MediaGalleryDefaultImage_Model_Mysql4_Condition_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('media_gallery_default_image/condition');
    }
}
