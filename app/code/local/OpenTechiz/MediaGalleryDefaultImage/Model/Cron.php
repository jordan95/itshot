<?php

class Opentechiz_MediaGalleryDefaultImage_Model_Cron
{

    public function populateAll()
    {
        $this->getHelper()->populateAll();
    }

    protected function getHelper()
    {
        return Mage::helper('media_gallery_default_image');
    }

}
