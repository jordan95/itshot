<?php

class OpenTechiz_MediaGalleryDefaultImage_Model_Condition extends Mage_Core_Model_Abstract
{

    protected $_eventPrefix = 'media_gallery_default_image_condition';
    protected $_eventObject = 'condition';

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    protected function _construct()
    {
        $this->_init('media_gallery_default_image/condition');
    }

    public function populateAll()
    {
        $productCollection = Mage::getModel('catalog/product')->getCollection();
        $productCollection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
        foreach ($productCollection as $p) {
            $product = Mage::getModel('catalog/product')->load($p->getId());
            $this->populateProduct($product);
        }
    }

    public function populateProduct(Mage_Catalog_Model_Product $product)
    {
        $defaultImageIds = $product->getData(OpenTechiz_MediaGalleryDefaultImage_Helper_Data::ATTR_CODE);
        if ($this->validate($product)) {
            if (empty($defaultImageIds)) {
                $defaultImageIds = $this->getId();
            } else {
                $temp = explode(',', $defaultImageIds);
                if (!in_array($this->getId(), $temp)) {
                    $temp[] = $this->getId();
                }
                $defaultImageIds = join(',', $temp);
            }
        } else if (!empty($defaultImageIds)) {
            $defaultImageIds = explode(',', $defaultImageIds);
            $temp = array();
            foreach ($defaultImageIds as $id) {
                if ($this->getId() == $id) {
                    continue;
                }
                $temp[] = $id;
            }
            $defaultImageIds = join(',', $temp);
        }
        Mage::getSingleton('catalog/resource_product_action')->updateAttributes(array($product->getId()), array(
            OpenTechiz_MediaGalleryDefaultImage_Helper_Data::ATTR_CODE => $defaultImageIds,
                ), $product->getStoreId());
    }

    public function validate(Mage_Catalog_Model_Product $product)
    {
        $categoryIds = $product->getCategoryIds();
        if (!$categoryIds || $this->isExcluded($product)) {
            return false;
        }
        if ($product->getData('disable_default_image')) {
            return false;
        }
        $allCategoryIds = $this->getAllCategoryIds();
        if (!$allCategoryIds) {
            return false;
        }
        foreach ($categoryIds as $catId) {
            if (in_array($catId, $allCategoryIds)) {
                return true;
            }
        }
        return false;
    }

    public function isExcluded(Mage_Catalog_Model_Product $product)
    {
        $excludeProductIds = $this->getExcludeProductIds();
        if ($excludeProductIds) {
            $excludeProductIds = explode(',', $excludeProductIds);
            if (in_array($product->getId(), $excludeProductIds)) {
                return true;
            }
        }
        return false;
    }

    public function getAllCategoryIds()
    {
        $key = __CLASS__ . __FUNCTION__ . $this->getId();
        if (!Mage::registry($key)) {
            $categoryIds = $this->getCategoryIds();
            if (!$categoryIds) {
                return false;
            }
            $configCategoryIds = explode(',', $categoryIds);
            $categoryIds = [];
            foreach ($configCategoryIds as $categoryId) {
                $catIds = $this->getAllChidrenCategory($categoryId);
                foreach ($catIds as $_id) {
                    $existed = false;
                    foreach ($categoryIds as $_cid) {
                        if ($_cid == $_id) {
                            $existed = true;
                            break;
                        }
                    }
                    if (!$existed) {
                        $categoryIds[] = $_id;
                    }
                }
            }
            Mage::register($key, $categoryIds);
        }
        return Mage::registry($key);
    }

    public function getAllChidrenCategory($categoryId)
    {
        $category = Mage::getModel('catalog/category')->load($categoryId);
        return $category->getResource()->getAllChildren($category);
    }

    public function getHelper()
    {
        return Mage::helper('media_gallery_default_image');
    }

    protected function _beforeDelete()
    {
        $path = $this->getHelper()->getSaveTo($this->getId());
        $files = glob($path . '/*'); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file)) {
                unlink($file); // delete file
            }
        }
        if (is_dir($path)) {
            rmdir($path);
        }
        return parent::_beforeDelete();
    }

}
