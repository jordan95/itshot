<?php

class OpenTechiz_MediaGalleryDefaultImage_Adminhtml_ConditionController extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction()
    {
        $this->loadLayout()
                ->_addBreadcrumb('Media Gallery Default Image', 'Conditions Manager');
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    public function populateAllAction()
    {
        try {
            $this->getHelper()->populateAll();
            Mage::getSingleton('adminhtml/session')->addSuccess('Populate all products completed');
        } catch (Exception $exc) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/index');
        return;
    }

    public function populateAction()
    {
        $id = $this->getRequest()->getParam('id');
        $conditionModel = Mage::getModel('media_gallery_default_image/condition')->load($id);
        if (!$conditionModel->getId()) {
            Mage::getSingleton('adminhtml/session')->addError('Condition does not exist');
            $this->_redirect('*/*/');
            return;
        }
        try {
            $conditionModel->populateAll();
            Mage::getSingleton('adminhtml/session')->addSuccess('Populate completed');
        } catch (Exception $exc) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/edit', array('id' => $conditionModel->getId()));
        return;
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $conditionModel = Mage::getModel('media_gallery_default_image/condition')->load($id);

        if ($conditionModel->getId() || $id == 0) {
            Mage::register('current_condition', $conditionModel);
            $this->loadLayout();
            $this->getLayout()->getBlock('head')
                    ->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()
                            ->createBlock('media_gallery_default_image/adminhtml_condition_edit'))
                    ->_addLeft($this->getLayout()
                            ->createBlock('media_gallery_default_image/adminhtml_condition_edit_tabs')
            );
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError('Condition does not exist');
            $this->_redirect('*/*/');
        }
    }

    public function saveAction()
    {
        if ($this->getRequest()->getPost()) {
            try {
                $postData = $this->getRequest()->getPost();
                $id = $this->getRequest()->getParam('id');
                $helper = $this->getHelper();
                $conditionModel = Mage::getModel('media_gallery_default_image/condition');
                $postData['category_ids'] = join(',', $postData['category_ids']);
                if ($id <= 0) {
                    $conditionModel
                            ->setCreatedAt(Mage::getSingleton('core/date')->gmtDate())
                            ->save();
                } else {
                    $conditionModel->load($id);
                }
                if (!empty($postData['photo_for_all']['delete'])) {
                    $postData['photo_for_all'] = '';
                    @unlink(Mage::getBaseDir('media') . DS . $conditionModel->getData('photo_for_all'));
                }
                if (!empty($postData['photo_for_men']['delete'])) {
                    $postData['photo_for_men'] = '';
                    @unlink(Mage::getBaseDir('media') . DS . $conditionModel->getData('photo_for_men'));
                }
                if (!empty($postData['photo_for_women']['delete'])) {
                    $postData['photo_for_women'] = '';
                    @unlink(Mage::getBaseDir('media') . DS . $conditionModel->getData('photo_for_women'));
                }
                if (!empty($postData['photo_for_unisex']['delete'])) {
                    $postData['photo_for_unisex'] = '';
                    @unlink(Mage::getBaseDir('media') . DS . $conditionModel->getData('photo_for_unisex'));
                }
                if (!empty($postData['photo_for_junior']['delete'])) {
                    $postData['photo_for_junior'] = '';
                    @unlink(Mage::getBaseDir('media') . DS . $conditionModel->getData('photo_for_junior'));
                }
                if (!empty($postData['photo_for_boy']['delete'])) {
                    $postData['photo_for_boy'] = '';
                    @unlink(Mage::getBaseDir('media') . DS . $conditionModel->getData('photo_for_boy'));
                }
                if (!empty($postData['photo_for_girl']['delete'])) {
                    $postData['photo_for_girl'] = '';
                    @unlink(Mage::getBaseDir('media') . DS . $conditionModel->getData('photo_for_girl'));
                }
                if (isset($_FILES['photo_for_all']['name']) and ( file_exists($_FILES['photo_for_all']['tmp_name']))) {
                    $postData['photo_for_all'] = $helper->uploadImage('photo_for_all', $conditionModel->getId());
                } else if (!empty($postData['photo_for_all']['value'])) {
                    $postData['photo_for_all'] = $postData['photo_for_all']['value'];
                }
                if (isset($_FILES['photo_for_men']['name']) and ( file_exists($_FILES['photo_for_men']['tmp_name']))) {
                    $postData['photo_for_men'] = $helper->uploadImage('photo_for_men', $conditionModel->getId());
                } else if (!empty($postData['photo_for_men']['value'])) {
                    $postData['photo_for_men'] = $postData['photo_for_men']['value'];
                }
                if (isset($_FILES['photo_for_women']['name']) and ( file_exists($_FILES['photo_for_women']['tmp_name']))) {
                    $postData['photo_for_women'] = $helper->uploadImage('photo_for_women', $conditionModel->getId());
                } else if (!empty($postData['photo_for_women']['value'])) {
                    $postData['photo_for_women'] = $postData['photo_for_women']['value'];
                }
                if (isset($_FILES['photo_for_unisex']['name']) and ( file_exists($_FILES['photo_for_unisex']['tmp_name']))) {
                    $postData['photo_for_unisex'] = $helper->uploadImage('photo_for_unisex', $conditionModel->getId());
                } else if (!empty($postData['photo_for_unisex']['value'])) {
                    $postData['photo_for_unisex'] = $postData['photo_for_unisex']['value'];
                }
                if (isset($_FILES['photo_for_junior']['name']) and ( file_exists($_FILES['photo_for_junior']['tmp_name']))) {
                    $postData['photo_for_junior'] = $helper->uploadImage('photo_for_junior', $conditionModel->getId());
                } else if (!empty($postData['photo_for_junior']['value'])) {
                    $postData['photo_for_junior'] = $postData['photo_for_junior']['value'];
                }
                if (isset($_FILES['photo_for_boy']['name']) and ( file_exists($_FILES['photo_for_boy']['tmp_name']))) {
                    $postData['photo_for_boy'] = $helper->uploadImage('photo_for_boy', $conditionModel->getId());
                } else if (!empty($postData['photo_for_boy']['value'])) {
                    $postData['photo_for_boy'] = $postData['photo_for_boy']['value'];
                }
                if (isset($_FILES['photo_for_girl']['name']) and ( file_exists($_FILES['photo_for_girl']['tmp_name']))) {
                    $postData['photo_for_girl'] = $helper->uploadImage('photo_for_girl', $conditionModel->getId());
                } else if (!empty($postData['photo_for_girl']['value'])) {
                    $postData['photo_for_girl'] = $postData['photo_for_girl']['value'];
                }
                $conditionModel
                        ->addData($postData)
                        ->setUpdateAt(Mage::getSingleton('core/date')->gmtDate())
                        ->save();
                Mage::getSingleton('adminhtml/session')->addSuccess('Condition saved');
                $this->_redirect('*/*/edit', array('id' => $conditionModel->getId()));
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

            $this->_redirect('*/*/');
        }
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $conditionModel = Mage::getModel('media_gallery_default_image/condition');
                $conditionModel->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess('Condition deleted');
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massPopulateAction()
    {
        $itemsIds = $this->getRequest()->getPost('item_ids');
        if (empty($itemsIds)) {
            Mage::getSingleton('adminhtml/session')->addSuccess('Please select any items to populate.');
            $this->_redirect('*/*/');
            return;
        }
        try {
            $collection = Mage::getModel('media_gallery_default_image/condition')->getCollection();
            $collection->addFieldToFilter('id', array('in' => $itemsIds));
            foreach ($collection as $condition) {
                $condition->populateAll();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess('Populate completed.');
            $this->_redirect('*/*/');
            return;
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            $this->_redirect('*/*/', array('id' => $this->getRequest()->getParam('id')));
        }
    }

    public function massDeleteAction()
    {
        $itemsIds = $this->getRequest()->getPost('item_ids');
        if (empty($itemsIds)) {
            Mage::getSingleton('adminhtml/session')->addSuccess('Please select any items to delete.');
            $this->_redirect('*/*/');
            return;
        }
        try {
            $collection = Mage::getModel('media_gallery_default_image/condition')->getCollection();
            $collection->addFieldToFilter('id', array('in' => $itemsIds));
            foreach ($collection as $condition) {
                $condition->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess('Items deleted.');
            $this->_redirect('*/*/');
            return;
        } catch (Exception $exc) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            $this->_redirect('*/*/', array('id' => $this->getRequest()->getParam('id')));
        }
    }

    public function getHelper()
    {
        return Mage::helper('media_gallery_default_image');
    }

}
