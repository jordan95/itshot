<?php

class OpenTechiz_MediaGalleryDefaultImage_Helper_Data extends Mage_Core_Helper_Abstract
{

    const XML_MEDIA_GALLERY_DEFAULT_IMAGE_ENABLE = 'media_gallery_default_image/general/enable';
    // Attribute
    const ATTR_CODE = 'default_image_ids';
    const ATTR_GENDER_CODE = 'c2c_gender_for_filter';
    // Upload
    const CATALOG_PRODUCT_DIR = 'catalog/product';
    const UPLOAD_DIR = 'mediagallerydefaultimage';
    // Gender
    const GENDER_MEN = "Men's";
    const GENDER_WOMEN = "Women's";
    const GENDER_UNISEX = "Unisex";
    const GENDER_BOY = "Boys";
    const GENDER_GIRL = "Girls";
    const GENDER_JUNIOR = "Juniors";

    public function populateAll()
    {
        $productCollection = Mage::getModel('catalog/product')->getCollection();
        $productCollection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
        $select = $productCollection->getSelect();
        $select->reset(Zend_Db_Select::COLUMNS)
                ->columns(array('entity_id'));
        foreach ($productCollection as $product) {
            $this->populateProduct($product);
        }
    }

    public function populateProduct(Mage_Catalog_Model_Product $product)
    {
        $conditionIds = $this->getConditionIdsByProduct($product);
        Mage::getSingleton('catalog/resource_product_action')->updateAttributes(array($product->getId()), array(
            self::ATTR_CODE => join(',', $conditionIds),
                ), $product->getStoreId());
    }

    public function getConditionIdsByProduct(Mage_Catalog_Model_Product $product)
    {
        $collection = Mage::getModel('media_gallery_default_image/condition')->getCollection();
        $collection->addFieldToFilter('status', OpenTechiz_MediaGalleryDefaultImage_Model_Condition::STATUS_ACTIVE);
        $collection->setOrder('priority', Varien_Data_Collection::SORT_ORDER_DESC);
        $conditionIds = array();
        foreach ($collection as $condition) {
            if ($condition->validate($product)) {
                $conditionIds[] = $condition->getId();
            }
        }
        return $conditionIds;
    }

    public function uploadImage($inputName, $id)
    {
        try {
            $path = self::CATALOG_PRODUCT_DIR . '/' . self::UPLOAD_DIR . '/' . $id;
            $saveTo = Mage::getBaseDir('media') . DS . $path;
            if (!is_dir_writeable($saveTo)) {
                \mkdir($saveTo, 0755, true);
            }
            $uploader = new Varien_File_Uploader($inputName);
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
            $uploader->setAllowRenameFiles(false);
            $uploader->save($saveTo, $_FILES[$inputName]['name']);
            return $path . '/' . $_FILES[$inputName]['name'];
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function getSaveTo($id)
    {
        $path = self::CATALOG_PRODUCT_DIR . '/' . self::UPLOAD_DIR . '/' . $id;
        return Mage::getBaseDir('media') . DS . $path;
    }

    public function isModuleEnabled()
    {
        return Mage::getStoreConfig(self::XML_MEDIA_GALLERY_DEFAULT_IMAGE_ENABLE);
    }

    public function getImagesByCondition($conditionId, Mage_Catalog_Model_Product $product)
    {
        $data = array();
        $conditionModel = Mage::getModel('media_gallery_default_image/condition')->load($conditionId);
        if (!$conditionModel->getId() || !$conditionModel->getStatus() || $conditionModel->isExcluded($product)) {
            return array();
        }
        $genderText = $product->getResource()
                ->getAttribute(self::ATTR_GENDER_CODE)
                ->getFrontend()
                ->getOption($product->getData(self::ATTR_GENDER_CODE));

        $photo_for_all = $conditionModel->getData('photo_for_all');
        if ($photo_for_all) {
            $image = new Varien_Object(array(
                'url' => Mage::getBaseUrl('media') . '/' . $photo_for_all,
                'file' => str_replace(self::CATALOG_PRODUCT_DIR . '/', '', $photo_for_all)
            ));
            $data[] = $image;
        }

        if ($genderText == self::GENDER_MEN) {
            $photo_for_men = $conditionModel->getData('photo_for_men');
            if ($photo_for_men) {
                $image = new Varien_Object(array(
                    'url' => Mage::getBaseUrl('media') . '/' . $photo_for_men,
                    'file' => str_replace(self::CATALOG_PRODUCT_DIR . '/', '', $photo_for_men)
                ));
                $data[] = $image;
            }
        } else if ($genderText == self::GENDER_WOMEN) {
            $photo_for_women = $conditionModel->getData('photo_for_women');
            if ($photo_for_women) {
                $image = new Varien_Object(array(
                    'url' => Mage::getBaseUrl('media') . '/' . $photo_for_women,
                    'file' => str_replace(self::CATALOG_PRODUCT_DIR . '/', '', $photo_for_women)
                ));
                $data[] = $image;
            }
        } else if ($genderText == self::GENDER_UNISEX) {
            $photo_for_unisex = $conditionModel->getData('photo_for_unisex');
            if ($photo_for_unisex) {
                $image = new Varien_Object(array(
                    'url' => Mage::getBaseUrl('media') . '/' . $photo_for_unisex,
                    'file' => str_replace(self::CATALOG_PRODUCT_DIR . '/', '', $photo_for_unisex)
                ));
                $data[] = $image;
            }
        } else if ($genderText == self::GENDER_JUNIOR) {
            $photo_for_junior = $conditionModel->getData('photo_for_junior');
            if ($photo_for_junior) {
                $image = new Varien_Object(array(
                    'url' => Mage::getBaseUrl('media') . '/' . $photo_for_junior,
                    'file' => str_replace(self::CATALOG_PRODUCT_DIR . '/', '', $photo_for_junior)
                ));
                $data[] = $image;
            }
        } else if ($genderText == self::GENDER_GIRL) {
            $photo_for_girl = $conditionModel->getData('photo_for_girl');
            if ($photo_for_girl) {
                $image = new Varien_Object(array(
                    'url' => Mage::getBaseUrl('media') . '/' . $photo_for_girl,
                    'file' => str_replace(self::CATALOG_PRODUCT_DIR . '/', '', $photo_for_girl)
                ));
                $data[] = $image;
            }
        } else if ($genderText == self::GENDER_BOY) {
            $photo_for_boy = $conditionModel->getData('photo_for_boy');
            if ($photo_for_boy) {
                $image = new Varien_Object(array(
                    'url' => Mage::getBaseUrl('media') . '/' . $photo_for_boy,
                    'file' => str_replace(self::CATALOG_PRODUCT_DIR . '/', '', $photo_for_boy)
                ));
                $data[] = $image;
            }
        }

        return $data;
    }

    public function getMediaGalleryDefaultImageURL(Mage_Catalog_Model_Product $product)
    {
        $data = array();
        $conditionIds = explode(',', $product->getData(self::ATTR_CODE));
        if (!$conditionIds) {
            return array();
        }
        foreach ($conditionIds as $conditionId) {
            $images = $this->getImagesByCondition($conditionId, $product);
            foreach ($images as $img) {
                $data[] = $img;
            }
        }
        return $data;
    }

    public function getMediaImageUrl($filename, $type)
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "opentechiz/mediagallerydefaultimage/{$type}/" . $filename;
    }

    public function getMediaImageDir($filename, $type)
    {
        return "/mediagallerydefaultimage/{$type}/" . $filename;
    }

    public function getCategoryIds($group)
    {
        $configCategoryIds = $this->getConfigCategories($group);
        $categoryIds = [];
        foreach ($configCategoryIds as $categoryId) {
            $catIds = $this->getAllChidrenCategory($categoryId);
            foreach ($catIds as $_id) {
                $existed = false;
                foreach ($categoryIds as $_cid) {
                    if ($_cid == $_id) {
                        $existed = true;
                        break;
                    }
                }
                if (!$existed) {
                    $categoryIds[] = $_id;
                }
            }
        }
        return $categoryIds;
    }

    public function getAllChidrenCategory($categoryId)
    {
        $category = Mage::getModel('catalog/category')->load($categoryId);
        return $category->getResource()->getAllChildren($category);
    }

    public function getConfig($group, $path, $isArray = false)
    {
        $config = Mage::getStoreConfig("media_gallery_default_image/{$group}/{$path}");
        if ($isArray) {
            $config = explode(',', $config);
        }
        return $config;
    }

}
