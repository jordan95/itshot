<?php

$installer = $this;
$installer->startSetup();
$installer->addAttribute(
        Mage_Catalog_Model_Product::ENTITY, 'default_image_ids', array(
    'type' => 'text',
    'label' => 'Media Gallery Default Image Type',
    'input' => 'hidden',
    'class' => '',
    'group' => 'Images',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "visible" => true,
    "required" => false,
    "user_defined" => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'visible_on_front' => true,
    'unique' => false,
        )
);
$installer->addAttribute(
        Mage_Catalog_Model_Product::ENTITY, 'disable_default_image', array(
    'type' => 'text',
    'label' => 'Disable Default Image',
    'input' => 'select',
    'class' => '',
    'source' => 'eav/entity_attribute_source_boolean',
    'group' => 'Images',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "visible" => true,
    "required" => false,
    "user_defined" => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'visible_on_front' => true,
    'unique' => false,
        )
);
$installer->run("
    DROP TABLE IF EXISTS {$installer->getTable('media_gallery_default_image_condition')};    
    CREATE TABLE  {$installer->getTable('media_gallery_default_image_condition')} (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) CHARACTER SET utf32 NOT NULL,
        `category_ids` text,
        `photo_for_all` varchar(255),
        `photo_for_men` varchar(255),
        `photo_for_women` varchar(255),
        `photo_for_unisex` varchar(255),
        `photo_for_junior` varchar(255),
        `photo_for_boy` varchar(255),
        `photo_for_girl` varchar(255),
        `exclude_product_ids` varchar(255),
        `status` tinyint(1) NOT NULL DEFAULT '1',
        `priority` int(10),
        `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `status` (`status`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
$installer->endSetup();
