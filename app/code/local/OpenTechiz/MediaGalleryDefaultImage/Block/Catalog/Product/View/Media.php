<?php

class OpenTechiz_MediaGalleryDefaultImage_Block_Catalog_Product_View_Media extends Mage_Catalog_Block_Product_View_Media
{
    protected $_extraImagesLoaded;
    
    public function getGalleryImages()
    {
        $helper = Mage::helper('media_gallery_default_image');
        $product = $this->getProduct();
        if(!$helper->isModuleEnabled() || $this->_isGalleryDisabled || $product->getData('disable_default_image')) {
            return parent::getGalleryImages();
        }
        $collections = parent::getGalleryImages();
        if(!$this->_extraImagesLoaded) {
            $extraImageURLs = $helper->getMediaGalleryDefaultImageURL($product);
            foreach ($extraImageURLs as $image) {
                $collections->addItem($image);
            }
            $this->_extraImagesLoaded = true;
        }
        return $collections;
    }
}
