<?php

class OpenTechiz_MediaGalleryDefaultImage_Block_Adminhtml_Condition extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_condition';
        $this->_blockGroup = 'media_gallery_default_image';
        $this->_headerText = Mage::helper('adminhtml')->__('Media Gallery Default Image');
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add Condition');
        $this->_addButton('populateAll', array(
            'label'     => Mage::helper('adminhtml')->__('Populate All Products'),
            'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/populateAll') .'\')',
            'class'     => 'save',
        ));
        parent::__construct();
    }

}
