<?php

class OpenTechiz_MediaGalleryDefaultImage_Block_Adminhtml_Condition_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $helper = Mage::helper('media_gallery_default_image');
        $form = new Varien_Data_Form(array(
                'enctype' => 'multipart/form-data',
            ));
        $fieldset = $form->addFieldset('condition_form', array('legend' => $helper->__('Condition information')));
        $fieldset->addField('name', 'text', array(
            'label' => 'Name',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'name',
        ));

        $fieldset->addField('category_ids', 'multiselect', array(
            'label' => 'Categories',
            'name' => 'category_ids[]',
             'values' => Mage::getModel('media_gallery_default_image/system_config_source_category')->toOptionArray(),
        ));

        $fieldset->addField('photo_for_all', 'image', array(
            'label' => 'Photo For All',
            'name' => 'photo_for_all',
        ));

        $fieldset->addField('photo_for_men', 'image', array(
            'label' => 'Photo For Men',
            'name' => 'photo_for_men',
        ));

        $fieldset->addField('photo_for_women', 'image', array(
            'label' => 'Photo For Women',
            'name' => 'photo_for_women',
        ));

        $fieldset->addField('photo_for_unisex', 'image', array(
            'label' => 'Photo For Unisex',
            'name' => 'photo_for_unisex',
        ));
        
        $fieldset->addField('photo_for_junior', 'image', array(
            'label' => 'Photo For Junior',
            'name' => 'photo_for_junior',
        ));
        
        $fieldset->addField('photo_for_boy', 'image', array(
            'label' => 'Photo For Boy',
            'name' => 'photo_for_boy',
        ));
        
        $fieldset->addField('photo_for_girl', 'image', array(
            'label' => 'Photo For Girl',
            'name' => 'photo_for_girl',
        ));
        
        $fieldset->addField('exclude_product_ids', 'text', array(
            'label' => 'Exclude Product IDs',
            'name' => 'exclude_product_ids',
        ));
        $fieldset->addField('priority', 'text', array(
            'label' => 'Priority',
            'name' => 'priority',
        ));

        $fieldset->addField('status', 'select', array(
            'label' => $helper->__('Status'),
            'name' => 'status',
            'values' => $this->getStatusOtions(),
        ));

        if (Mage::registry('current_condition')) {
            $form->setValues(Mage::registry('current_condition')->getData());
        }
        $this->setForm($form);
        return parent::_prepareForm();
    }

    public function getStatusOtions()
    {
        return array(
            array(
                'value' => OpenTechiz_MediaGalleryDefaultImage_Model_Condition::STATUS_ACTIVE,
                'label' => Mage::helper('adminhtml')->__('Active'),
            ),
            array(
                'value' => OpenTechiz_MediaGalleryDefaultImage_Model_Condition::STATUS_DEACTIVE,
                'label' => Mage::helper('adminhtml')->__('Deactive'),
            ),
        );
    }

}
