<?php

class OpenTechiz_MediaGalleryDefaultImage_Block_Adminhtml_Condition_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('conditionGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('item_ids');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('adminhtml')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('adminhtml')->__('Are you sure?')
        ));
        $this->getMassactionBlock()->addItem('populate', array(
            'label' => Mage::helper('adminhtml')->__('Populate'),
            'url' => $this->getUrl('*/*/massPopulate'),
        ));

        return $this;
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('media_gallery_default_image/condition')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('adminhtml')->__('ID'),
            'index' => 'id',
            'width' => '100px',
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('adminhtml')->__('Name'),
            'align' => 'left',
            'index' => 'name',
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('adminhtml')->__('Status'),
            'type' => 'options',
            'align' => 'left',
            'width' => '100px',
            'index' => 'status',
            'options' => $this->getStatusOtions(),
        ));
        $this->addColumn('priority', array(
            'header' => Mage::helper('adminhtml')->__('Priority'),
            'index' => 'priority',
            'width' => '100px',
        ));
        $this->addColumn('created_at', array(
            'header' => Mage::helper('adminhtml')->__('Created At'),
            'type' => 'datetime',
            'align' => 'left',
            'width' => '200px',
            'index' => 'created_at',
        ));

        return parent::_prepareColumns();
    }

    public function getStatusOtions()
    {
        return array(
            OpenTechiz_MediaGalleryDefaultImage_Model_Condition::STATUS_DEACTIVE => Mage::helper('adminhtml')->__('Deactive'),
            OpenTechiz_MediaGalleryDefaultImage_Model_Condition::STATUS_ACTIVE => Mage::helper('adminhtml')->__('Active'),
        );
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}
