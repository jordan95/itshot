<?php

class OpenTechiz_MediaGalleryDefaultImage_Block_Adminhtml_Condition_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('condition_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('media_gallery_default_image')->__('Condition Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('media_gallery_default_image')->__('Condition Information'),
            'title' => Mage::helper('media_gallery_default_image')->__('Condition Information'),
            'content' => $this->getLayout()->createBlock('media_gallery_default_image/adminhtml_condition_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}
