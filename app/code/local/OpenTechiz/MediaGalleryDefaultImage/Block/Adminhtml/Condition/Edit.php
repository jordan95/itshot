<?php

class OpenTechiz_MediaGalleryDefaultImage_Block_Adminhtml_Condition_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'media_gallery_default_image';
        $this->_controller = 'adminhtml_condition';
        if (Mage::registry('current_condition') && Mage::registry('current_condition')->getId()) {
            $this->_addButton('populate', array(
                'label' => Mage::helper('customer')->__('Populate'),
                'onclick' => 'setLocation(\'' . $this->getUrl('*/*/populate', array('id' => Mage::registry('current_condition')->getId())) . '\')',
                'class' => 'save',
                    ), 0);
        }
        $this->_updateButton('save', 'label', 'Save condition');
        $this->_updateButton('delete', 'label', 'Delete condition');
    }

    public function getHeaderText()
    {
        if (Mage::registry('current_condition') && Mage::registry('current_condition')->getId()) {
            return 'Edit a condition ' . $this->htmlEscape(Mage::registry('current_condition')->getName());
        } else {
            return 'Add a condition';
        }
    }

}
