<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Product options text type block
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class OpenTechiz_PersonalizedProduct_Block_Catalog_Product_View_Options_Type_Gold
    extends Mage_Catalog_Block_Product_View_Options_Abstract
{
    public function getValuesHtml()
    {
        $_option = $this->getOption();
        $configValue = $this->getProduct()->getPreconfiguredValues()->getData('options/' . $_option->getId());
        $store = $this->getProduct()->getStore();

        $selectHtml = '<ul id="options-'.$_option->getSortOrder().'-list" class="options-list">';
        $require = ($_option->getIsRequire()) ? ' validate-one-required-by-name' : '';
        $arraySign = '';

        $type = 'radio';
        $class = 'radio';
        if (!$_option->getIsRequire()) {
            $selectHtml .= '<li><input type="radio" id="options_' . $_option->getSortOrder() . '" class="'
                . $class . ' product-custom-option" name="options[' . $_option->getId() . ']"'
                . ($this->getSkipJsReloadPrice() ? '' : ' onclick="opConfig.reloadPrice()"')
                . ' value="" checked="checked" /><span class="label"><label for="options_'
                . $_option->getSortOrder() . '">' . $this->__('None') . '</label></span></li>';
        }
        $count = 1;
        foreach ($_option->getValues() as $_value) {
            $count++;

            $priceStr = $this->_formatPrice(array(
                'is_percent'    => ($_value->getPriceType() == 'percent'),
                'pricing_value' => $_value->getPrice($_value->getPriceType() == 'percent')
            ));

            $htmlValue = $_value->getOptionTypeId();
            if ($arraySign) {
                $checked = (is_array($configValue) && in_array($htmlValue, $configValue)) ? 'checked' : '';
            } else {
                $checked = $configValue == $htmlValue ? 'checked' : '';
            }

            $title = trim(strtolower(str_replace(' ', '', $_value->getTitle())));
            $_value->setTitle($title);

            $data = Mage::helper('personalizedproduct/personalizedPriceHelper')->getAlloyPrice(Mage::registry('product')->getId());
            $_value->setPrice($data[$title]);

            $selectHtml .= '<li>'.'<input data="'.$_option->getSortOrder().'-'. $_value->getTitle() .'" type="' . $type . '" class="' . $class . ' ' . $require
                . ' product-custom-option gold-type" '
                . ($this->getSkipJsReloadPrice() ? '' : ' onclick="opConfig.reloadPrice()"')
                . ' name="options[' . $_option->getId() . ']' . $arraySign . '" id="options_' . $_option->getSortOrder()
                . '_' . $count . '" value="' . $htmlValue . '" ' . $checked . ' price="'
                . $this->helper('core')->currencyByStore($_value->getPrice(true), $store, false) . '" />'
                . '<span class="label"><label for="options_' . $_option->getSortOrder() . '_' . $count . '">'
                . $this->escapeHtml($_value->getTitle()) . ' ' . $priceStr . '</label></span>';
            if ($_option->getIsRequire()) {
                $selectHtml .= '<script type="text/javascript">' . '$(\'options_' . $_option->getSortOrder() . '_'
                    . $count . '\').advaiceContainer = \'options-' . $_option->getSortOrder() . '-container\';'
                    . '$(\'options_' . $_option->getSortOrder() . '_' . $count
                    . '\').callbackFunction = \'validateOptionsCallback\';' . '</script>';
            }
            $selectHtml .= '</li>';
        }
        $selectHtml .= '</ul>';
        return $selectHtml;
    }

    public function getOptionName(){
        return $this->getOption()->getTitle();
    }

    public function getOptionId(){
        return $this->getOption()->getId();
    }

    public function getSortOrder(){
        return $this->getOption()->getSortOrder();
    }

    public function getGoldName(){
        $golds = [];
        foreach ($this->getOption()->getValues() as $_value){
            array_push($golds, $_value->getTitle());
        }
        return $golds;
    }

    public function getMediaUrl(){
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    }

    public function getHasOptionParam(){
        if(!Mage::registry('has_option_param')) return false;
        else return Mage::registry('has_option_param');
    }

    public function cmp($a, $b)
    {
        return strcmp($a->sort_order, $b->sort_order);
    }

    public function getDefaultGold(){
        $_product= Mage::registry('product');

        $attributeDetails = Mage::getSingleton("eav/config")
            ->getAttribute("catalog_product", 'default_alloy');
        $optionValue = $attributeDetails->getSource()->getOptionText($_product->getDefaultAlloy());

        $optionValue = str_replace(' ', '', $optionValue);
        $optionValue = strtolower($optionValue);

        return $optionValue;
    }
}
