<?php
class OpenTechiz_PersonalizedProduct_Block_Catalog_Product_Grid extends Amasty_Pgrid_Block_Adminhtml_Catalog_Product_Grid
{
    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        $store = $this->_getStore();

        $this->addColumnAfter('retail_price',
            array(
                'header'=> Mage::helper('catalog')->__('Retail Price'),
                'type'  => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'price',
        ),'sku');

        $this->addColumnAfter('special_price',
            array(
                'header'=> Mage::helper('catalog')->__('Selling Price'),
                'type'  => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'special_price',
        ),'sku');

        $this->sortColumnsByOrder();
        $this->removeColumn('price');
        return $this;
    }

    protected function _beforeLoadCollection()
    {
        $this->getCollection()->addAttributeToSelect('special_price');
        return $this;
    }
}