<?php
class OpenTechiz_PersonalizedProduct_Block_Catalog_Product_List_Toolbar extends Medialounge_Catalog_Block_Product_Toolbar{

    /**
     * Set collection to pager
     *
     * @param Varien_Data_Collection $collection
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    //this was commented out before for some error in search
    public function setCollection($collection)
    {
        $this->_collection = $collection;
        $this->_collection->setCurPage($this->getCurrentPage());

        // we need to set pagination only if passed value integer and more that 0
        $limit = (int)$this->getLimit();
        if ($limit) {
            $this->_collection->setPageSize($limit);
        }

        if ($this->getCurrentOrder()) {
            if($this->getCurrentOrder() == 'price' && $this->getRequest()->getControllerName() == 'category') {
                $this->_collection->getSelect()->order('price.' . $this->getCurrentOrder() . ' ' . $this->getCurrentDirection());
            }else{
                $this->_collection->setOrder($this->getCurrentOrder(), $this->getCurrentDirection());
            }
        }

        return $this;
    }
        protected function _afterToHtml($html)
        {
            Mage::helper("fpc/response")->updateIgnoredUrlParams($html);
            return parent::_afterToHtml($html);
        }
}