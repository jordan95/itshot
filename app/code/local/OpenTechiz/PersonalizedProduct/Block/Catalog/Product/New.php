<?php

class OpenTechiz_PersonalizedProduct_Block_Catalog_Product_New extends Medialounge_Catalog_Block_Category_Newproduct
{
    const DEFAULT_QUALITY = OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_QUALITY;
    const DEFAULT_STONE = OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_STONE;

    /**
     * get listing price
     *
     * @param
     * @return  string
     */
    public function getPriceHtml($_product, $displayMinimalPrice = false, $idSuffix = '', $isPersonalized = false){
        if($isPersonalized) {
            $productId = $_product->getId();

            if (!$this->getStoneCollection()) {
                $stoneCollection = Mage::getModel('personalizedproduct/price_stone')->getCollection();
                $stoneCollection->getSelect()->join(array('stone_price' => 'tsht_material_stone'), 'stone_price.stone_id = main_table.stone_id', '*');
                $this->setStoneCollection($stoneCollection);
            }
            if (!$this->getGoldCollection()) {
                $goldCollection = Mage::getModel('personalizedproduct/price_alloy')->getCollection();
                $goldCollection->getSelect()->join(array('gold_price' => 'tsht_material_gold'), 'gold_price.gold_id = main_table.gold_id', '*');
                $this->setGoldCollection($goldCollection);
            }
            if (!$this->getListingCollection()) {
                $listingCollection = Mage::getModel('personalizedproduct/price_product')->getCollection();
                $this->setListingCollection($listingCollection);
            }

            $stoneName = $_product->getAttributeText("default_stone");
            $alloyName = $_product->getAttributeText("default_alloy");

            $stoneName = str_replace(' ', '', $stoneName);
            $stoneName = strtolower($stoneName);

            $alloyName = str_replace(' ', '', $alloyName);
            $alloyName = strtolower($alloyName);

            $alloyCollection = $this->getGoldCollection();
            $alloyId = $this->getAlloyId($alloyCollection, $alloyName, $productId);

            $stone = $this->getStoneCollection()->addFieldToFilter('stone_type', $stoneName)->addFieldToFilter('product_id', $productId);

            if ($stoneName == 'diamond') {
                $stone->addFieldToFilter('quality', self::DEFAULT_QUALITY);
            }
            $stoneSelectGroup = $stone->getFirstItem()->getStoneSelectGroup();

            $price = $this->getListingCollection()->addFieldToFilter('product_id', $productId)->addFieldToFilter('stone_select_group', $stoneSelectGroup)
                ->addFieldToFilter('gold_id', $alloyId)->getFirstItem()->getPrice();

            $price = number_format((float)$price, 2, '.', '');

            if ($price > 0) {
                return '<div class="price-box">
                        <p class="special-price">
                             <span id="product-price-<?php echo $_product->getId() ?>" class="price">'
                    . Mage::helper('core')->currency($price, true, false)
                    . '</span>
                        </p>
                    </div>';
            } else {
                return parent::getPriceHtml($_product, $displayMinimalPrice, $idSuffix);
            }
        }else{
            return parent::getPriceHtml($_product, $displayMinimalPrice, $idSuffix);
        }
    }

    /**
     * get listing image url
     *
     * @param
     * @return  string
     */

    public function getAlloyId($alloyCollection, $alloyName, $productId){
        foreach($alloyCollection as $alloy){
            $product_id = $alloy->getProductId();
            $type = $alloy->getGoldType();
            $name = $alloy->getGoldColor();
            $alloyFullName = $type.$name;
            $alloyFullName = str_replace(' ', '', $alloyFullName);
            $alloyFullName = strtolower($alloyFullName);
            if($alloyName == $alloyFullName && $product_id == $productId){
                $alloyId = $alloy->getGoldId();
                break;
            }
        }
        return $alloyId;
    }
}