<?php

class OpenTechiz_PersonalizedProduct_Block_Catalog_Product_WidgetTabs extends Medialounge_ProductTabs_Block_Widget_Tabs
{
    const DEFAULT_QUALITY = OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_QUALITY;
    const DEFAULT_STONE = OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_STONE;

    public function getPriceHtml($_product, $displayMinimalPrice = false, $idSuffix = ''){
        $productId = $_product->getId();
        if(!$this->getStoneCollection()) {
            $stoneCollection = Mage::getModel('personalizedproduct/price_stone')->getCollection();
            $stoneCollection->getSelect()->join(array('stone_price' => 'tsht_material_stone'), 'stone_price.stone_id = main_table.stone_id', '*');
            $this->setStoneCollection($stoneCollection);
        }
        if(!$this->getGoldCollection()) {
            $goldCollection = Mage::getModel('personalizedproduct/price_alloy')->getCollection();
            $goldCollection->getSelect()->join(array('gold_price' => 'tsht_material_gold'), 'gold_price.gold_id = main_table.gold_id', '*');
            $this->setGoldCollection($goldCollection);
        }
        $listingCollection = Mage::getModel('personalizedproduct/price_product')->getCollection();

        $stoneName = $_product->getAttributeText("default_stone");
        $alloyName = $_product->getAttributeText("default_alloy");

        $stoneName = str_replace(' ', '', $stoneName);
        $stoneName = strtolower($stoneName);

        $alloyName = str_replace(' ', '', $alloyName);
        $alloyName = strtolower($alloyName);

        $alloyCollection = $this->getGoldCollection();
        $alloyId = $this->getAlloyId($alloyCollection, $alloyName, $productId);

        $stoneCollection = $this->getStoneCollection();
        $stone = $stoneCollection->addFieldToFilter('stone_type', $stoneName);

        if($stoneName == 'diamond'){
            $stone->addFieldToFilter('quality', self::DEFAULT_QUALITY);
        }
        $stoneSelectGroup = $stone->getFirstItem()->getStoneSelectGroup();

        $price = $listingCollection->addFieldToFilter('product_id', $productId)->addFieldToFilter('stone_select_group', $stoneSelectGroup)
            ->addFieldToFilter('gold_id', $alloyId)->getFirstItem()->getPrice();

        $price = number_format((float)$price, 2, '.', '');
        if($price > 0) {
            return '<div class="price-box">
                        <p class="special-price">
                             <span id="product-price-<?php echo $_product->getId() ?>" class="price">'
                .Mage::helper('core')->currency($price, true, false)
                .'</span>
                        </p>
                    </div>';
        } else {
            return parent::getPriceHtml($_product, $displayMinimalPrice, $idSuffix);
        }
    }

    public function getAlloyId($alloyCollection, $alloyName, $productId){
        $alloyId = 0;
        foreach($alloyCollection as $alloy){
            $product_id = $alloy->getProductId();
            $type = $alloy->getGoldType();
            $name = $alloy->getGoldColor();
            $alloyFullName = $type.$name;
            $alloyFullName = str_replace(' ', '', $alloyFullName);
            $alloyFullName = strtolower($alloyFullName);
            if($alloyName == $alloyFullName && $product_id == $productId){
                $alloyId = $alloy->getGoldId();
                break;
            }
        }
        return $alloyId;
    }

    public function getBestsellingProducts()
    {
        $products = [];
        $storeId = Mage::app()->getStore()->getId();

        $itemsCollection = Mage::getResourceModel('sales/order_item_collection')
            ->join('order', 'order_id=entity_id')
            ->addFieldToFilter('main_table.store_id', array('eq' => $storeId))
            ->setOrder('main_table.created_at', 'desc')
            ->setPageSize($this->getLimit());
        $select = $itemsCollection->getSelect();
        $select->limit($this->getLimit());
        $select->columns( array( 'item_created' => new Zend_Db_Expr("max(main_table.created_at)")));
        $select->order('item_created DESC');
        $select->group(array('main_table.product_id'));
        $collection = Mage::getModel('catalog/product')->getCollection();
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collection->addAttributeToSelect($attributes);
        if ($itemsCollection->count() > 0) {
            $pids = array();
            foreach ($itemsCollection as $item) {
                $product = Mage::getModel('catalog/product')
                    ->setStoreId($storeId)
                    ->load($item->getProductId());
                if($product->getSku() != 'quotation' && $product->getSku() != 'mw_virtual_surcharge'
                    && $product->getSku() != 'return_fee') {
                    $pids[] = $item->getProductId();
                }
            }

            $collection->addAttributeToFilter('entity_id', array('in' => $pids));
        }
        $collection->addFinalPrice()
            ->addStoreFilter()
            ->addAttributeToFilter('status', 1)
            ->addUrlRewrite();
        $this->addStockFilter($collection);
        $collection->getSelect()->limit($this->getLimit());
        return $collection;
    }
}