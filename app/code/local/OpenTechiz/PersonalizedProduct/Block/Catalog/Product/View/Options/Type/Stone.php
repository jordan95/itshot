<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Product options text type block
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class OpenTechiz_PersonalizedProduct_Block_Catalog_Product_View_Options_Type_Stone
    extends Mage_Catalog_Block_Product_View_Options_Abstract
{
    /**
     * Return html for control element
     *
     * @return string
     */
    public function getValuesHtml()
    {
        $_option = $this->getOption();

        Mage::unregister('optionId');
        Mage::register('optionId', $_option->getId());
        $configValue = $this->getProduct()->getPreconfiguredValues()->getData('options/' . $_option->getId());

        $store = $this->getProduct()->getStore();

        $selectHtml = '<ul id="options-'.$_option->getSortOrder().'-list" class="options-list">';
        $require = ($_option->getIsRequire()) ? ' validate-one-required-by-name' : '';
        $arraySign = '';

        $type = 'radio';
        $class = 'radio';
        if (!$_option->getIsRequire()) {
            $selectHtml .= '<li><input type="radio" id="options_' . $_option->getSortOrder() . '" class="'
                . $class . ' product-custom-option" name="options[' . $_option->getId() . ']"'
                . ($this->getSkipJsReloadPrice() ? '' : ' onclick="opConfig.reloadPrice()"')
                . ' value="" checked="checked" /><span class="label"><label for="options_'
                . $_option->getSortOrder() . '">' . $this->__('None') . '</label></span></li>';
        }
        $count = 1;
        $data = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStonePrice($_option->getId(),Mage::registry('product')->getId());
        $qualityData = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStoneWithQualityPrice(Mage::registry('optionId'),Mage::registry('product')->getId());

        foreach ($_option->getValues() as $_value) {
            $count++;

            $priceStr = $this->_formatPrice(array(
                'is_percent'    => ($_value->getPriceType() == 'percent'),
                'pricing_value' => $_value->getPrice($_value->getPriceType() == 'percent')
            ));

            if($this->isDiamond($_value->getSku())){
                $quality = str_replace("diamond", "", $_value->getSku());
                $priceStr = '<span class="price-notice">+$'.number_format($qualityData[$quality], 2).'</span></span>';
            }

            $htmlValue = $_value->getOptionTypeId();

            if ($arraySign) {
                $checked = (is_array($configValue) && in_array($htmlValue, $configValue)) ? 'checked' : '';
            } else {
                $checked = $configValue == $htmlValue ? 'checked' : '';
            }

            $_value->setPrice($data[strtolower($_value->getTitle())]);

            $selectHtml .= '<li>'.'<input data="'.$_option->getSortOrder().'-'. $_value->getSku() .'" type="' . $type . '" class="' . $class . ' ' . $require
                . ' product-custom-option stone-type" '
                . ($this->getSkipJsReloadPrice() ? '' : ' onclick="opConfig.reloadPrice()"')
                . ' name="options[' . $_option->getId() . ']' . $arraySign . '" id="options_' . $_option->getSortOrder()
                . '_' . $count . '" value="' . $htmlValue . '" ' . $checked . ' price="'
                . $this->helper('core')->currencyByStore($_value->getPrice(true), $store, false) . '" />'
                . '<span class="label"><label for="options_' . $_option->getSortOrder() . '_' . $count . '">'
                . $this->escapeHtml($_value->getSku()) . ' ' . $priceStr . '</label></span>';
            if ($_option->getIsRequire()) {
                $selectHtml .= '<script type="text/javascript">' . '$(\'options_' . $_option->getSortOrder() . '_'
                    . $count . '\').advaiceContainer = \'options-' . $_option->getSortOrder() . '-container\';'
                    . '$(\'options_' . $_option->getSortOrder() . '_' . $count
                    . '\').callbackFunction = \'validateOptionsCallback\';' . '</script>';
            }
            $selectHtml .= '</li>';
        }
        $selectHtml .= '</ul>';
        return $selectHtml;
    }

    public function getOptionName(){
        return $this->getOption()->getTitle();
    }

    public function getOptionId(){
        return $this->getOption()->getId();
    }

    public function getSortOrder(){
        return $this->getOption()->getSortOrder();
    }

    public function getStoneName(){
        $stones = [];
        foreach ($this->getOption()->getValues() as $_value){
            array_push($stones, $_value->getTitle());
        }
        return $stones;
    }

    public function getStoneNames($valueArray){
        $stones = [];
        foreach ($valueArray as $_value){
            array_push($stones, $_value->getTitle());
        }
        return $stones;
    }

    public function getGoldNames($valueArray){
        $golds = [];
        foreach ($valueArray as $_value){
            array_push($golds, str_replace(' ', '', strtolower($_value->getSku())));
        }
        return $golds;
    }

    public function getStoneSkus($valueArray){
        $sku = [];
        foreach ($valueArray as $_value){
            array_push($sku, $_value->getSku());
        }
        return $sku;
    }

    public function hasDiamond($valueArray){
        foreach ($valueArray as $_value){
            if(strtolower($_value->getTitle()) == 'diamond' || $_value->getSku() == 'diamondi'){
                return true;
                break;
            }
        }
        return false;
    }

    public function getDisplayName($name){
        return OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_TYPE[trim(str_replace(' ', '', strtolower($name)))];
    }

    public function getStoneSku(){
        $sku = [];
        foreach ($this->getOption()->getValues() as $_value){
            array_push($sku, $_value->getSku());
        }
        return $sku;
    }

    public function getMediaUrl(){
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    }

    public function getHasOptionParam(){
        if(!Mage::registry('has_option_param')) return false;
        else return Mage::registry('has_option_param');
    }

    public function getProductOptionStoneData(){
        $options = [];
        $product = Mage::registry('product');
        $optionData = $product->getOptions();
//        usort($optionData, "cmp");

        foreach ($optionData as $option){
            if($option->getType() == 'stone'){
                $data = [];
                foreach ($option->getValues() as $value){
                    array_push($data, strtolower( trim(str_replace(' ', '', $value->getTitle()))));
                }
                array_push($options, $data);
            }
        }
        return json_encode($options);
    }

    public function cmp($a, $b)
    {
        return strcmp($a->sort_order, $b->sort_order);
    }

    public function getProductType(){
        $product = Mage::registry('product');
        $categoryIds = $product->getCategoryIds();
        $data = [];
        $type = '';

        foreach($categoryIds as $id) {
            $category = Mage::getModel('catalog/category')->load($id);
            array_push($data, $category->getName());
        }

        foreach ($data as $category){
            if(strpos($category, 'Ring') !== false) $type = 'ring';
            else if(strpos($category, 'Earring') !== false) $type = 'earring';
            else if(strpos($category, 'Bracelet') !== false) $type = 'bracelet';
            else if(strpos($category, 'Necklace') !== false) $type = 'necklace';
            else $type = 'jewelry';
        }
        return $type;
    }

    public function hasQuality(){
        if($this->getQualityPrice())
            return true;
        else return false;
    }

    public function isJewelry(){
        $_product= Mage::registry('product');
        if($_product->getOptions()){
            foreach ($_product->getOptions() as $option) {
                if ($option->getType() == 'stone' || $option->getType() == 'gold') return true;
            }
        }
        return false;
    }

    public function getQualityPrice(){
        $data = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStoneWithQualityPrice(Mage::registry('optionId'),Mage::registry('product')->getId());

        foreach($data as $key=>$value){
            $results[$key] = $value;
        }


        return json_encode($results);
    }

    public function isDiamond($stoneName){
        $arrayQuality=['si', 'i', 'vs'];
        $quality = str_replace("diamond", "", str_replace(' ', '', trim(strtolower($stoneName))));
        return in_array($quality, $arrayQuality);
    }

    public function getDataSku($stoneName){
        $quality = str_replace("diamond", "", str_replace(' ', '', trim(strtolower($stoneName))));
        $result[0]='diamond';
        $result[1]=$quality;
        return $result;
    }

    public function getDefaultStone(){
        $_product= Mage::registry('product');

        $attributeDetails = Mage::getSingleton("eav/config")
            ->getAttribute("catalog_product", 'default_stone');
        $optionValue = $attributeDetails->getSource()->getOptionText($_product->getDefaultStone());

        $optionValue = str_replace(' ', '', $optionValue);
        $optionValue = strtolower($optionValue);

        return $optionValue;
    }

    public function getDefaultGold(){
        $_product= Mage::registry('product');

        $attributeDetails = Mage::getSingleton("eav/config")
            ->getAttribute("catalog_product", 'default_alloy');
        $optionValue = $attributeDetails->getSource()->getOptionText($_product->getDefaultAlloy());

        $optionValue = str_replace(' ', '', $optionValue);
        $optionValue = strtolower($optionValue);
        
        return $optionValue;
    }
}
