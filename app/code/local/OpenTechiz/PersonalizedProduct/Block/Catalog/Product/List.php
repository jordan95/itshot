<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2018 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
if ('true' == (string) Mage::getConfig()->getNode('modules/Amasty_Conf/active')) {

    class OpenTechiz_PersonalizedProduct_Block_Catalog_Product_List_Pure extends Amasty_Conf_Block_Catalog_Product_List
    {

    }

} else {

    class OpenTechiz_PersonalizedProduct_Block_Catalog_Product_List_Pure extends Mage_Catalog_Block_Product_List
    {

    }

}

class OpenTechiz_PersonalizedProduct_Block_Catalog_Product_List extends OpenTechiz_PersonalizedProduct_Block_Catalog_Product_List_Pure
{
    const DEFAULT_QUALITY = OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_QUALITY;
    const DEFAULT_STONE = OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_STONE;
    protected $currentStoneFilter;
    protected $currentAlloyFilter;
    protected $goldCollection = [];
    protected $stoneCollection = [];
    protected $listingCollection = [];
    protected $defaultStone = [];
    protected $defaultStone2 = [];
    protected $defaultStone3 = [];
    protected $defaultStone4 = [];
    protected $defaultAlloy = [];

    protected $personalizedAttrSetId = 0;

    /**
     * get stone name in filter
     *
     * @param
     * @return  string
     */
    public function getStoneFilter($product){
        if($this->currentStoneFilter){
            return $this->currentStoneFilter;
        }
        $optionValue = null;
        if(array_key_exists('stone', $this->getRequest()->getParams())) {
            $stone_id = $this->getRequest()->getParams()['stone'];

            $attributeDetails = Mage::getSingleton("eav/config")
                ->getAttribute("catalog_product", 'stone');
            $optionValue = $attributeDetails->getSource()->getOptionText($stone_id);

            $optionValue = str_replace(' ', '', $optionValue);
            $optionValue = strtolower($optionValue);

            if ($optionValue) {
                $this->currentStoneFilter = $optionValue;
            }
        }
        if(!$optionValue){
            if(!is_object($product)) return false;

            if(array_key_exists($product->getId(), $this->defaultStone)){
                return $this->defaultStone[$product->getId()];
            }else {
//                $product = Mage::getModel('catalog/product')->load($productId);
                return $product->getAttributeText("default_stone");
            }
        }

        return $optionValue;
    }

    /**
     * get alloy name in filter
     *
     * @param
     * @return  string
     */
    public function getAlloyFilter($product){
        if(!$product) return false;
        if($this->currentAlloyFilter){
            return $this->currentAlloyFilter;
        }
        $optionValue = null;
        if(array_key_exists('alloy', $this->getRequest()->getParams())) {
            $alloy_id = $this->getRequest()->getParams()['alloy'];

            $attributeDetails = Mage::getSingleton("eav/config")
                ->getAttribute("catalog_product", 'alloy');
            $optionValue = $attributeDetails->getSource()->getOptionText($alloy_id);

            $optionValue = str_replace(' ', '', $optionValue);
            $optionValue = strtolower($optionValue);

            if ($optionValue) {
                $this->currentAlloyFilter = $optionValue;
            }
        }
        if(!$optionValue){
            if(!is_object($product)) return false;

            if(array_key_exists($product->getId(), $this->defaultAlloy)){
                return $this->defaultAlloy[$product->getId()];
            }else {
//                $product = Mage::getModel('catalog/product')->load($productId);
                return $product->getAttributeText("default_alloy");
            }
        }

        return $optionValue;
    }

    /**
     * check if filter is active
     *
     * @param
     * @return  boolean
     */
    public function haveStoneAlloyFilter($_product){
        if($this->getAlloyFilter($_product) || $this->getStoneFilter($_product)){
            return true;
        } else return false;
    }

    /**
     * get listing image url
     *
     * @param
     * @return  string
     */
    public function getProductImage($_product){
        $_imgSize = 300;
        if($this->personalizedAttrSetId == 0){
            $attrSetName = OpenTechiz_PersonalizedProduct_Helper_Data::ATTRIBUTE_SET_NAME;
            $this->personalizedAttrSetId = Mage::getModel('eav/entity_attribute_set')
                ->load($attrSetName, 'attribute_set_name')
                ->getAttributeSetId();
        }

        $flag = false;
        if($_product->getAttributeSetId() == $this->personalizedAttrSetId) $flag = true;
//        $id = $_product->getId();
//        $product = Mage::getModel('catalog/product')->load($id);
//
//        foreach($product->getOptions() as $option) {
//            if($option->getType() == 'stone' || $option->getType() == 'gold') $flag = true;
//        }

        if($flag == 'true') {
            $stoneName = $this->getStoneFilter($_product);
            $alloyName = $this->getAlloyFilter($_product);
            $sku = $_product->getSku();

            if ($stoneName && $alloyName) {
                $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . $stoneName;
                if($this->defaultStone2[$_product->getId()]){
                    $sku.=OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.strtolower($this->defaultStone2[$_product->getId()]);
                }
                if($this->defaultStone3[$_product->getId()]){
                    $sku.=OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.strtolower($this->defaultStone3[$_product->getId()]);
                }
                if($this->defaultStone4[$_product->getId()]){
                    $sku.=OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.strtolower($this->defaultStone4[$_product->getId()]);
                }
                $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR. $alloyName .OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR. '1';
                $_product->setSku($sku);
            }
        }
        $url = Mage::helper('catalog/image')->init($_product, 'small_image')->resize($_imgSize);

        return $url;
    }

    /**
     * get listing price
     *
     * @param   Mage_Catalog_Product $_product
     *  @param  boolean $isPersonalized
     * @return  string
     */
    public function getProductPrice($_product, $isPersonalized){
        if($isPersonalized){
            $productId = $_product->getId();

            if(count($this->stoneCollection) == 0){
                $stoneCollection = Mage::getModel('personalizedproduct/price_stone')->getCollection();
                $stoneCollection->getSelect()->join(array('stone_price' => 'tsht_material_stone'), 'stone_price.stone_id = main_table.stone_id', '*');
                $this->stoneCollection = $stoneCollection;
            }

            if(count($this->goldCollection) == 0){
                $goldCollection = Mage::getModel('personalizedproduct/price_alloy')->getCollection();
                $goldCollection->getSelect()->join(array('gold_price' => 'tsht_material_gold'), 'gold_price.gold_id = main_table.gold_id', '*');
                $this->goldCollection = $goldCollection;
            }

            if(count($this->listingCollection) == 0){
                $listingCollection = Mage::getModel('personalizedproduct/price_product')->getCollection();
                $this->listingCollection = $listingCollection;
            }

            $stoneName = strtolower(trim(str_replace(' ', '', $this->getStoneFilter($_product))));
            $alloyName = strtolower(trim(str_replace(' ', '', $this->getAlloyFilter($_product))));

            $alloyCollection = $this->goldCollection;
            $alloyId = $this->getAlloyId($alloyCollection, $alloyName, $productId);

            $stone = $this->stoneCollection->addFieldToFilter('product_id', $productId)->addFieldToFilter('stone_type', $stoneName);

            if($stoneName == self::DEFAULT_STONE){
                $stoneId = $stone->addFieldToFilter('quality', self::DEFAULT_QUALITY)->getFirstItem()->getStoneSelectGroup();
            }else {
                $stoneId = $stone->getFirstItem()->getStoneSelectGroup();
            }

            $price = $this->listingCollection->addFieldToFilter('product_id', $productId)->addFieldToFilter('stone_select_group', $stoneId)
                ->addFieldToFilter('gold_id', $alloyId)->getFirstItem()->getPrice();
            if($price) {
                $price = number_format((float)$price, 2, '.', '');
            }else{
                $price = 0;
            }

            $_product->setPrice($price);
            $_product->setFinalPrice($price);
            $_product->setMinimalPrice($price);
        }
        return $this->getPriceHtml($_product, true);
    }

    /**
     * get alloy id
     *
     * @param   array $alloyCollection
     * @param   string  $alloyName
     * @param   integer $productId
     * @return  integer
     */
    public function getAlloyId($alloyCollection, $alloyName, $productId){
        foreach($alloyCollection as $alloy){
            $product_id = $alloy->getProductId();
            $type = $alloy->getGoldType();
            $name = $alloy->getGoldColor();
            $alloyFullName = $type.$name;
            $alloyFullName = str_replace(' ', '', $alloyFullName);
            $alloyFullName = strtolower($alloyFullName);
            if($alloyName == $alloyFullName && $product_id == $productId){
                $alloyId = $alloy->getGoldId();
                break;
            }
        }
        return $alloyId;
    }

    public function getProductType($product){
        $categoryIds = $product->getCategoryIds();
        $data = [];
        $type = '';

        foreach($categoryIds as $id) {
            $category = Mage::getModel('catalog/category')->load($id);
            array_push($data, $category->getName());
        }

        foreach ($data as $category){
            if(strpos($category, 'Ring') !== false) $type = 'ring';
            if(strpos($category, 'Earring') !== false) $type = 'earring';
            if(strpos($category, 'Bracelet') !== false) $type = 'bracelet';
            if(strpos($category, 'Necklace') !== false) $type = 'necklace';
        }
        return $type;
    }

    public function getSideStoneOnUrl($productId){
        $stoneCollection = Mage::getModel('personalizedproduct/price_stone')->getCollection()->addFieldToFilter('product_id', $productId);
        $result = [];

        foreach($stoneCollection as $stone){
            $optionId = $stone->getOptionId();
            if(!in_array($optionId,$result)) array_push($result, $optionId);
        }
        $count = count($result)-1;
        $string = '';
        for($i=0;$i<$count;$i++){
            $string=$string.'-and-'.self::DEFAULT_STONE;
        }
        return $string;
    }

    public function getDefaultData($productCollection){
        $ids = '(';
        foreach ($productCollection as $product){
            $ids .= $product->getId() .',';
        }

        $ids = trim($ids, ',');
        $ids .=')';
        if($ids != '()') {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $query = 'SELECT * FROM ' . $resource->getTableName('personalizedproduct/defaultData') . ' where product_id in ' . $ids;
            $results = $readConnection->fetchAll($query);

            foreach ($results as $product) {
                $this->defaultStone[$product['product_id']] = $product['center_stone'];
                $this->defaultStone2[$product['product_id']] = $product['stone_2'];
                $this->defaultStone3[$product['product_id']] = $product['stone_3'];
                $this->defaultStone4[$product['product_id']] = $product['stone_4'];
                $this->defaultAlloy[$product['product_id']] = $product['metal'];
            }
        }
    }

    /**
     * get retail price for collection
     *
     * @param   array $productCollection
     * @return  array
     */
    public function getRetailPrice($productCollection){
        $ids = '(';
        foreach ($productCollection as $product){
            $ids .= $product->getId() .',';
        }

        $ids = trim($ids, ',');
        $ids .=')';
        if($ids != '()') {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $query = 'SELECT * FROM tsht_catalog_product_entity_decimal where `attribute_id` = 75 and entity_id in ' . $ids;
            $results = $readConnection->fetchAll($query);

            $priceArray = [];
            foreach ($results as $result){
                $priceArray[$result['entity_id']] = $result['value'];
            }
            return $priceArray;
        }
        return [];
    }

    public function removeQualityOnDefaultStoneName($stoneName){
        $stoneName = strtolower(trim($stoneName));
        foreach (OpenTechiz_PersonalizedProduct_Helper_Data::STONES_WITH_QUALITY as $stoneWithQuality){
            if(strpos($stoneName, $stoneWithQuality) !== false){
                foreach (OpenTechiz_PersonalizedProduct_Helper_Data::QUALITY as $quality){
                    if($stoneName == $stoneWithQuality.$quality) {
                        $stoneName = str_replace($quality, '', $stoneName);
                    }
                }
            }
        }

        return $stoneName;
    }

    /**
     * get description for collection
     *
     * @param   array $collection
     * @return  array
     */

    public function getDescriptionArray($collection){
        $productIds = '(';
        foreach ($collection as $product){
            $productIds .=$product->getId().',';
        }
        $productIds = trim($productIds, ',');
        $productIds .= ')';

        if($productIds != '()') {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $query = 'SELECT * FROM tsht_catalog_product_entity_text where `attribute_id` = 72 and entity_id in ' . $productIds;
            $results = $readConnection->fetchAll($query);

            $descriptionArray = [];
            foreach ($results as $result){
                $descriptionArray[$result['entity_id']] = $result['value'];
            }

            return $descriptionArray;
        } else{
            return [];
        }
    }
}
