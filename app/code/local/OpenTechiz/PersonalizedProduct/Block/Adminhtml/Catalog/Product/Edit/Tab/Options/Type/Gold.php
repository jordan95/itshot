<?php
class OpenTechiz_PersonalizedProduct_Block_Adminhtml_Catalog_Product_Edit_Tab_Options_Type_Gold extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Options_Type_Abstract
{
    protected $addButtonHtml = '';
    protected $addDeleteHtml = '';

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('opentechiz/personalizedproduct/catalog/product/edit/options/type/gold.phtml');
        $this->setCanEditPrice(true);
        $this->setCanReadPrice(true);
    }


    protected function _prepareLayout()
    {
        $this->setChild('add_gold_row_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label' => Mage::helper('catalog')->__('Add New Row'),
                    'class' => 'add add-select-row',
                    'id'    => 'add_gold_row_button_{{id}}',
                    'on_click' => "goldOptionType.addNewGoldRow({{option_id}})"
                ))
        );

        $this->setChild('delete_gold_row_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label' => Mage::helper('catalog')->__('Delete Row'),
                    'class' => 'delete delete-select-row icon-btn',
                    'id'    => 'delete_gold_row_button',
                    'onclick' => "goldOptionType.remove({{id}}, {{gold_id}})"
                ))
        );

        $this->setChild('gold_title_select',
            $this->getLayout()->createBlock('adminhtml/html_select')
                ->setData(array(
                    'id' => 'product_option_{{id}}_gold_{{gold_id}}_title_select',
                    'class' => 'select product-option-price-type gold_title_select'
                ))
        );
        $this->getChild('gold_title_select')->setName('product[options][{{id}}][values][{{gold_id}}][title]')
            ->setOptions($this->getGoldTitleOption());

        return parent::_prepareLayout();

        parent::_prepareLayout();
    }

    public function getGoldTitleOption(){
        return OpenTechiz_Material_Helper_Data::GOLD_NAME_FRONT_END;
    }

    public function getAddButtonHtml()
    {
        return $this->getChildHtml('add_gold_row_button');
    }

    public function getAjaxOptionGoldListUrl(){
        return Mage::getUrl('adminhtml/price_gold/getOptionValue');
    }

    public function getDeleteButtonHtml()
    {
        return $this->getChildHtml('delete_gold_row_button');
    }

    public function getTitleSelectHtml(){
        return $this->getChildHtml('gold_title_select');
    }

    public function getPriceTypeSelectHtml()
    {
        $this->getChild('option_price_type')
            ->setData('id', 'product_option_{{id}}_{{gold_id}}_price_type')
            ->setName('product[options][{{id}}][values][{{gold_id}}][price_type]');

        return parent::getPriceTypeSelectHtml();
    }
}