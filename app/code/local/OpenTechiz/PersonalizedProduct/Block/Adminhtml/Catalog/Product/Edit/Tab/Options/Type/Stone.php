<?php
class OpenTechiz_PersonalizedProduct_Block_Adminhtml_Catalog_Product_Edit_Tab_Options_Type_Stone extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Options_Type_Abstract
{
    protected $addButtonHtml = '';
    protected $addDeleteHtml = '';

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('opentechiz/personalizedproduct/catalog/product/edit/options/type/stone.phtml');
        $this->setCanEditPrice(true);
        $this->setCanReadPrice(true);
    }


    protected function _prepareLayout()
    {
        $this->setChild('add_stone_row_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label' => Mage::helper('catalog')->__('Add New Row'),
                    'class' => 'add add-select-row',
                    'id'    => 'add_stone_row_button_{{id}}',
                    'on_click' => "stoneOptionType.addNewStoneRow({{option_id}})"
                ))
        );

        $this->setChild('delete_stone_row_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label' => Mage::helper('catalog')->__('Delete Row'),
                    'class' => 'delete delete-select-row icon-btn',
                    'id'    => 'delete_stone_row_button',
                    'onclick' => "stoneOptionType.remove({{id}}, {{stone_id}})"
                ))
        );

        $this->setChild('stone_title_select',
            $this->getLayout()->createBlock('adminhtml/html_select')
                ->setData(array(
                    'id' => 'product_option_{{id}}_stone_{{stone_id}}_title_select',
                    'class' => 'select product-option-price-type stone_title_select'
                ))
        );
        $this->getChild('stone_title_select')->setName('product[options][{{id}}][values][{{stone_id}}][title]')
            ->setOptions($this->getStoneTitleOption());
        
        return parent::_prepareLayout();

        parent::_prepareLayout();
    }

    public function getStoneTitleOption(){
        return OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_TYPE;
    }

    public function getAddButtonHtml()
    {
        return $this->getChildHtml('add_stone_row_button');
    }

    public function getDeleteButtonHtml()
    {
        return $this->getChildHtml('delete_stone_row_button');
    }

    public function getAjaxOptionStoneListUrl(){
        return Mage::getUrl('adminhtml/price_stone/getOptionValue');
    }

    public function getTitleSelectHtml(){
        return $this->getChildHtml('stone_title_select');
    }

    public function getPriceTypeSelectHtml()
    {
        $this->getChild('option_price_type')
            ->setData('id', 'product_option_{{id}}_{{stone_id}}_price_type')
            ->setName('product[options][{{id}}][values][{{stone_id}}][price_type]');

        return parent::getPriceTypeSelectHtml();
    }
}