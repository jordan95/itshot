<?php

class OpenTechiz_PersonalizedProduct_Block_Adminhtml_Price_Alloy_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('alloyPriceGrid');
        $this->setDefaultSort('product_option_alloy_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getModel('personalizedproduct/price_alloy')->getCollection();
        $collection->getSelect()->join( array('alloy'=> 'tsht_material_gold'), 'alloy.gold_id = main_table.gold_id',
            array('gold_type'=>  'alloy.gold_type',
                    'gold_color' => 'alloy.gold_color'
                ));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('product_option_alloy_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('ID'),
            'index'     => 'product_option_alloy_id',
            'width'     => '50',
            'align'     => 'center'
        ));
        $this->addColumn('product_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Product #'),
            'index'     => 'product_id',
            'width'     =>  '100',
        ));
        $this->addColumn('gold_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Gold #'),
            'width'     => '100',
            'index'     => 'gold_id',
        ));
        $this->addColumn('gold_type', array(
            'header'    => Mage::helper('opentechiz_production')->__('Gold Type'),
            'index'     => 'gold_type',
            'type'      => 'options',
            'options'   => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_TYPE,
        ));
        $this->addColumn('gold_color', array(
            'header'    => Mage::helper('opentechiz_production')->__('Gold Color'),
            'index'     => 'gold_color',
            'type'      => 'options',
            'options'   => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_COLOR,
        ));

        $this->addExportType('*/*/exportCsv', 'CSV');

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('product_option_alloy_id');
        $this->getMassactionBlock()->setFormFieldName('alloy_option');
        $this->getMassactionBlock()->setUseSelectAll(false);

        // MassDelete
        $this->getMassactionBlock()->addItem('delete_alloy_option', array(
            'label' => Mage::helper('personalizedproduct')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('personalizedproduct')->__('Are you sure?')
        ));

        return $this;
    }
}