<?php

class OpenTechiz_PersonalizedProduct_Block_Adminhtml_Price_Stone_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('stonePriceGrid');
        $this->setDefaultSort('product_option_stone_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getModel('personalizedproduct/price_stone')->getCollection();
        $collection->getSelect()->join(array('stone' => 'tsht_material_stone'), 'stone.stone_id = main_table.stone_id',
            array(
                'weight'        => 'stone.weight',
                'shape'         => 'stone.shape',
                'diameter'      => 'stone.diameter',
                'quality'       => 'stone.quality',
                'type'          => 'stone.stone_type',
                'main_table.stone_id' => 'main_table.stone_id'
            ));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('product_option_stone_id', array(
            'header' => Mage::helper('opentechiz_production')->__('ID'),
            'index' => 'product_option_stone_id',
            'align' => 'center',
            'width' => '50',
        ));
        $this->addColumn('product_id', array(
            'header' => Mage::helper('opentechiz_production')->__('Product #'),
            'index' => 'product_id',
            'width' => '80',
        ));
        $this->addColumn('option_name', array(
            'header' => Mage::helper('opentechiz_production')->__('Option Name'),
//            'width'     => '600px',
            'index' => 'option_name'
        ));

        $this->addColumn('stone_id', array(
            'header' => Mage::helper('opentechiz_production')->__('Stone #'),
            'width' => '50',
            'index' => 'main_table.stone_id',
            'align' => 'center',
        ));
        $this->addColumn('stone_type', array(
            'header' => Mage::helper('opentechiz_production')->__('Stone Type'),
            'width' => '100',
            'index' => 'type',
            'type'  => 'options',
            'is_system' => true,
            'options'   => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_TYPE,
        ));

        $this->addColumn('weight', array(
            'header' => Mage::helper('opentechiz_production')->__('Weight'),
            'width' => '100',
            'index' => 'weight',
            'type'  => 'number',
            'is_system' => true,
        ));
        $this->addColumn('shape', array(
            'header' => Mage::helper('opentechiz_production')->__('Shape'),
            'width' => '200',
            'index' => 'shape',
            'type'  => 'options',
            'is_system' => true,
            'options'   => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_SHAPE,
        ));
        $this->addColumn('diameter', array(
            'header' => Mage::helper('opentechiz_production')->__('Diameter'),
            'width' => '200',
            'index' => 'diameter',
            'is_system' => true,
        ));
        $this->addColumn('quality', array(
            'header' => Mage::helper('opentechiz_production')->__('Quality'),
            'width' => '200',
            'index' => 'quality',
            'type'  => 'options',
            'options'   => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_QUALITY,
            'is_system' => true,
        ));
        $this->addColumn('stone_select_group', array(
            'header' => Mage::helper('opentechiz_production')->__('Group #'),
            'width' => '100',
            'index' => 'stone_select_group',
            'type'  => 'number'
        ));
        $this->addColumn('quantity', array(
            'header' => Mage::helper('opentechiz_production')->__('Qty'),
            'width' => '100',
            'index' => 'quantity',
            'type'  => 'number'
        ));

        $this->addExportType('*/*/exportCsv', 'CSV');

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('product_option_stone_id');
        $this->getMassactionBlock()->setFormFieldName('stone_option');
        $this->getMassactionBlock()->setUseSelectAll(false);

        // MassDelete
        $this->getMassactionBlock()->addItem('delete_stone_option', array(
            'label' => Mage::helper('personalizedproduct')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('personalizedproduct')->__('Are you sure?')
        ));

        return $this;
    }
}