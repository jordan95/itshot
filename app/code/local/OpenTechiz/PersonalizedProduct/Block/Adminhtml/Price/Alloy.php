<?php

class OpenTechiz_PersonalizedProduct_Block_Adminhtml_Price_Alloy extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_price_alloy';
        $this->_blockGroup = 'personalizedproduct';
        $this->_headerText = Mage::helper('personalizedproduct')->__('Gold Options');
        parent::__construct();
        $this->removeButton('add');

        $this->_addButton('import', array(
            'label' => 'Import CSV',
            'onclick' => 'document.getElementById(\'uploadTarget\').click();',
            'class' => 'add',
            'after_html' => '<form id="uploadForm" method="POST" action="'.$this->getUrl('*/*/importCSV', array()).'" enctype="multipart/form-data">
            <input name="form_key" type="hidden" value="'.Mage::getSingleton('core/session')->getFormKey().'" />
            <input type="file" name="file" style="display:none;" id="uploadTarget"/>
        </form>
        <script type="text/javascript">
            document.getElementById(\'uploadTarget\').addEventListener(\'change\', function(){
                document.getElementById(\'uploadForm\').submit();
            }, false);
        </script>',
        ));
    }
}