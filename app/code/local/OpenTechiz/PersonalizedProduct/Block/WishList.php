<?php

class OpenTechiz_PersonalizedProduct_Block_WishList
    extends Medialounge_Theme_Block_Wishlist
{
    protected $_products = [];
    protected $_productSku = [];

    public function _construct()
    {
        $this->_product_ids = $this->getWishlistProductIds();
        parent::_construct();
    }

    public function getWishlistItems()
    {
        if ($this->hasWishlistItems()) {
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter(
                    'entity_id', array('in' => [$this->_product_ids])
                );
            foreach ($collection as $product){
                if(array_key_exists($product->getId(), $this->_productSku)){
                    $product->setSku($product->getSKu().OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.$this->_productSku[$product->getId()]);
                }
            }
            return $collection;
        }

        return [];
    }

    public function getWishlistProductIds()
    {
        $ids = [];
        $collection =  Mage::getResourceModel('amlist/item_collection')
            ->addFieldToFilter('list_id', Mage::helper('medialounge_theme')->getLastListId())
            ->setOrder('item_id')
            ->load();

        foreach ($collection as $item) {
            $ids[] = $item->getProductId();
            $buyRequest = unserialize($item->getBuyRequest());
            if(array_key_exists('options', $buyRequest)) {
                if (array_key_exists('quotation_product_id', $buyRequest['options'])) {
                    $this->_productSku[$item->getProductId()] = $buyRequest['options']['quotation_product_id'];
                }
            }
        }
        return $ids;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getTemplate()) {
            return '';
        }
        $html = $this->renderView();
        return $html;
    }

    public function hasWishlistItems()
    {
        return $this->getItemCount() > 0;
    }

    public function getItemCount()
    {
        return count($this->_product_ids);
    }
}