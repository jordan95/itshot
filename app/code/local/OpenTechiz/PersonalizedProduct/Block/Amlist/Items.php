<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_List
 */
class OpenTechiz_PersonalizedProduct_Block_Amlist_Items extends Amasty_List_Block_Items
{
    protected $product;
    public function getProductItemImage($item){
        $buyRequest = unserialize($item->getBuyRequest());
        if(!isset($buyRequest['options'])){
            $buyRequest['options'] = array();
        }
        if(array_key_exists('quotation_product_id', $buyRequest['options'])) {
            $this->product = $item->getProduct();
            $productId = $this->product->getId();
            $product = Mage::getModel('catalog/product')->load($productId);

            $product->setSku('quotation-' .$buyRequest['options']['quotation_product_id']);
            $url = $this->helper('catalog/image')->init($product, 'small_image')->resize(113, 113);
            return $url;
        }

        $this->product = $item->getProduct();
        $productId = $this->product->getId();
        $product = Mage::getModel('catalog/product')->load($productId);
        if(Mage::helper('personalizedproduct')->isPersonalizedProduct($product)) {
            $buyRequest = unserialize($item->getBuyRequest());
            $options = $buyRequest['options'];
            if ($options) {
                $flag = true; // flag to check if smthing wrong with product options
                $finalPrice = 0.00;
                if (array_key_exists('partialpayment', $options)) {
                    unset($options['partialpayment']);
                }
                foreach ($options as $id => $value) {
                    if (!$product->getOptionById($id)) {
                        $flag = false;
                    }
                }
                if ($flag == true) {
                    $sku = $product->getSku();
                    $data = [];
                    foreach ($options as $id => $value) {
                        $option = $product->getOptionById($id);
                        if ($option->getType() == 'gold') {
                            $alloy = $option->getValueById($value)->getSku();
                        } else if ($option->getType() == 'stone') {
                            $data[$option->getSortOrder()] = trim(strtolower(str_replace(' ', '', $option->getValueById($value)->getTitle())));
                        }
                    }
                    for ($i = 0; $i < count($data); $i++) {
                        $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . $data[$i];
                    }
                    $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . $alloy . OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.'1';
                    $product->setSku($sku);

                    //set price for product
                    foreach ($options as $id => $value) {
                        $option = $product->getOptionById($id);
                        if ($option->getType() == 'stone') {
                            $data = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStonePrice($id, $productId);
                            $stoneQualityData = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStoneWithQualityPrice($id, $productId);

                            $stone = $option->getValueById($value);
                            $stoneTitle = str_replace(' ', '', trim(strtolower($stone->getTitle())));
                            if ($stoneTitle == 'diamond') {
                                $sku = $stone->getSku();
                                $quality = str_replace("diamond", "", $sku);;
                                $finalPrice += $stoneQualityData[$quality];

                            } else $finalPrice += $data[$stoneTitle];
                        } elseif ($option->getType() == 'gold') {
                            $goldData = Mage::helper('personalizedproduct/personalizedPriceHelper')->getAlloyPrice($productId);

                            $gold = $option->getValueById($value);
                            $title = str_replace(' ', '', str_replace(' ', '', trim(strtolower($gold->getTitle()))));
                            $finalPrice += $goldData[$title];
                        }
                    }
                    $finalPrice += (float)$product->getData('additional_fee');
                    $this->product->setFinalPrice($finalPrice);
                } else {
                    //problem occur, get default price for product
                    $stoneCollection = Mage::getModel('personalizedproduct/price_stone')->getCollection();
                    $stoneCollection->getSelect()->join(array('stone_price' => 'tsht_material_stone'), 'stone_price.stone_id = main_table.stone_id', '*');
                    if (!$this->getGoldCollection()) {
                        $goldCollection = Mage::getModel('personalizedproduct/price_alloy')->getCollection();
                        $goldCollection->getSelect()->join(array('gold_price' => 'tsht_material_gold'), 'gold_price.gold_id = main_table.gold_id', '*');
                        $this->setGoldCollection($goldCollection);
                    }
                    $listingCollection = Mage::getModel('personalizedproduct/price_product')->getCollection();

                    $stoneName = $product->getAttributeText("default_stone");
                    $alloyName = $product->getAttributeText("default_alloy");

                    $stoneName = str_replace(' ', '', $stoneName);
                    $stoneName = strtolower($stoneName);

                    $alloyName = str_replace(' ', '', $alloyName);
                    $alloyName = strtolower($alloyName);

                    $alloyCollection = $this->getGoldCollection();
                    $alloyId = $this->getAlloyId($alloyCollection, $alloyName, $productId);

                    $stone = $stoneCollection->addFieldToFilter('stone_type', $stoneName)->addFieldToFilter('product_id', $productId);

                    if ($stoneName == 'diamond') {
                        $stone->addFieldToFilter('quality', self::DEFAULT_QUALITY);
                    }
                    $stoneSelectGroup = $stone->getFirstItem()->getStoneSelectGroup();

                    $finalPrice = $listingCollection->addFieldToFilter('product_id', $productId)->addFieldToFilter('stone_select_group', $stoneSelectGroup)
                        ->addFieldToFilter('gold_id', $alloyId)->getFirstItem()->getPrice();

                    $finalPrice = number_format((float)$finalPrice, 2, '.', '');

                    $this->product->setFinalPrice($finalPrice);
                }
            }
        } else{
            $options = $buyRequest['options'];
            $productOptions = $product->getOptions();
            $color = '';
            if(isset($productOptions)){
                foreach ($productOptions as $productOption){
                    if(strpos(strtolower($productOption->getTitle()), 'color') !== false ||
                        strpos(strtolower($productOption->getTitle()), 'colour') !== false){
                        $colorOptionId = $productOption->getId();
                        if(isset($options[$colorOptionId])){
                            $valueId = $options[$colorOptionId];
                            foreach ($productOption->getValues() as $value){
                                if($value->getId() == $valueId){
                                    $color = strtolower($value->getTitle());
                                }
                            }
                        }
                    }
                }
            }
            if($color != ''){
                $product->setSku($product->getSku().OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.$color);
            }
        }

        $url = $this->helper('catalog/image')->init($product, 'small_image')->resize(113, 113);
        /* if no image and parent exist*/
        if($item->getGroupedParentProduct() && ($url == '' || strpos($url, 'placeholder')) ){
            $product = $item->getGroupedParentProduct();
            $url = $this->helper('catalog/image')->init($product, 'small_image')->resize(113, 113);
        }
        return $url;
    }

    public function getProductItemUrl($item){
        $buyRequest = unserialize($item->getBuyRequest());
        $url = parent::getProductItemUrl($item);
        if(!isset($buyRequest['options'])){
            $buyRequest['options'] = array();
        }
        if(array_key_exists('quotation_product_id', $buyRequest['options'])) {
            $quoteProduct = Mage::getModel('opentechiz_quotation/product')->load($buyRequest['options']['quotation_product_id']);

            $url .= '/'.$quoteProduct->getQpName();
        }

        return $url;
    }

    public function getItemPrice($item){
        $buyRequest = unserialize($item->getBuyRequest());
        if(!isset($buyRequest['options'])){
            $buyRequest['options'] = array();
        }
        if(array_key_exists('quotation_product_id', $buyRequest['options'])) {
            $quoteProduct = Mage::getModel('opentechiz_quotation/product')->load($buyRequest['options']['quotation_product_id']);

            return $quoteProduct->getQpPrice();
        }

        return $item->getPrice();
    }

    public function getAlloyId($alloyCollection, $alloyName, $productId){
        foreach($alloyCollection as $alloy){
            $product_id = $alloy->getProductId();
            $type = $alloy->getGoldType();
            $name = $alloy->getGoldColor();
            $alloyFullName = $type.$name;
            $alloyFullName = str_replace(' ', '', $alloyFullName);
            $alloyFullName = strtolower($alloyFullName);
            if($alloyName == $alloyFullName && $product_id == $productId){
                $alloyId = $alloy->getGoldId();
                break;
            }
        }
        return $alloyId;
    }
}