<?php
class OpenTechiz_PersonalizedProduct_Block_ProductUpdate extends AW_Productupdates_Block_Product {
    public function getSmallImages(){
        $_product = $this->getProduct();
        $_imgSize = 64;
        $results = [];
        foreach ($_product->getMediaGalleryImages() as $_image):
//            $thumbnailUrl = $this->helper('catalog/image')
//                ->init($_product, 'image', $_image->getFile())
//                ->keepFrame(false)
//                ->constrainOnly(true)->resize(64);
//            Mage::log($_image->getFile());
            array_push($results, Mage::helper('catalog/image')->init($_product, 'small_image')->resize($_imgSize));
        endforeach;

        return json_encode($results);
    }

    public function getLargeImages(){
        $_product = $this->getProduct();
        $_imgSize = 375;
        $results = [];
        foreach ($_product->getMediaGalleryImages() as $_image):
//            $thumbnailUrl = $this->helper('catalog/image')
//                ->init($_product, 'image', $_image->getFile())
//                ->keepFrame(false)
//                ->constrainOnly(true)->resize(375);
            array_push($results, Mage::helper('catalog/image')->init($_product, 'small_image')->resize($_imgSize));
        endforeach;

        return json_encode($results);
    }
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $product = $this->getProduct();
            $title = $product->getMetaTitle();
            $sku_product = $product->getSku();
            if ($title) {
                if (strpos($title, $sku_product) === false) {
                    $headBlock->setTitle($title." ".$sku_product);
                }else{
                    $headBlock->setTitle($title);
                }
                
            }else{
                $product_name = $product->getName();
                if (strpos($product_name, $sku_product) === false) {
                    $headBlock->setTitle($product_name." ".$sku_product);
                }else{
                    $headBlock->setTitle($product_name);
                }
            }
            $description = $product->getMetaDescription();
            if ($description) {
                if (strpos($description, $sku_product) === false) {
                    $headBlock->setDescription(($description." ".$this->__('Item Code: %s', $sku_product)));
                }else{
                    $headBlock->setDescription(($description));
                }
                
            } else {
                if (strpos(Mage::helper('core/string')->substr($product->getDescription(), 0, 255), $sku_product) === false) {
                    $headBlock->setDescription(Mage::helper('core/string')->substr($product->getDescription(), 0, 255)." ".$this->__('Item Code: %s', $sku_product));
                }else{
                    $headBlock->setDescription(Mage::helper('core/string')->substr($product->getDescription(), 0, 255));
                }
                
            }
        }

        return $this;
    }
}