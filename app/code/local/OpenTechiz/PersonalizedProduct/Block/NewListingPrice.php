<?php
/**
 * Sweet Tooth
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Sweet Tooth SWEET TOOTH POINTS AND REWARDS
 * License, which extends the Open Software License (OSL 3.0).
 * The Sweet Tooth License is available at this URL:
 *      https://www.sweettoothrewards.com/terms-of-service
 * The Open Software License is available at this URL:
 *      http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * By adding to, editing, or in any way modifying this code, Sweet Tooth is
 * not held liable for any inconsistencies or abnormalities in the
 * behaviour of this code.
 * By adding to, editing, or in any way modifying this code, the Licensee
 * terminates any agreement of support offered by Sweet Tooth, outlined in the
 * provided Sweet Tooth License.
 * Upon discovery of modified code in the process of support, the Licensee
 * is still held accountable for any and all billable time Sweet Tooth spent
 * during the support process.
 * Sweet Tooth does not guarantee compatibility with any other framework extension.
 * Sweet Tooth is not responsbile for any inconsistencies or abnormalities in the
 * behaviour of this code if caused by other framework extension.
 * If you did not receive a copy of the license, please send an email to
 * support@sweettoothrewards.com or call 1.855.699.9322, so we can send you a copy
 * immediately.
 *
 * @category   [TBT]
 * @package    [TBT_Rewards]
 * @copyright  Copyright (c) 2014 Sweet Tooth Inc. (http://www.sweettoothrewards.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Product View Points
 *
 * @category   TBT
 * @package    TBT_Rewards
 * * @author     Sweet Tooth Inc. <support@sweettoothrewards.com>
 */
class OpenTechiz_PersonalizedProduct_Block_NewListingPrice extends TBT_RewardsOnly_Block_Product_Price
{
    protected $normalPriceData = [];

    public function getProduct()
    {
        if(!$this->isEnabled()) {
            return parent::getProduct();
        }
        $p = $this->_getData('product');
        if(empty($p)) {
            $p = Mage::registry('product');
        }
        if (!empty($p)) {
            if($p instanceof TBT_Rewards_Model_Catalog_Product) {
                $this->_product = $p;
            } else {
                $this->_product = TBT_Rewards_Model_Catalog_Product::wrap($p);
            }
        } else {
            $this->_product = Mage::getModel('rewards/catalog_product');
        }

        $normalPrice = Mage::helper('personalizedproduct')->getProductNormalPrice($this->_product->getId());
        if ($normalPrice) {
            $this->product->setFinalPrice($normalPrice);
        }
//        if(Mage::helper('personalizedproduct')->isPersonalizedProduct($this->_product->getId())){
//            $this->_product->setFinalPrice(Mage::helper('personalizedproduct/personalizedPriceHelper')
//                ->getDefaultPersonalizedPrice($this->_product->getId()));
//        }

        return $this->_product;
    }
}