<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog image helper
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
if ('true' == (string) Mage::getConfig()->getNode('modules/Amasty_Shopby/active')) {

    class OpenTechiz_PersonalizedProduct_Helper_Rewrite_Catalog_Image_Pure extends Amasty_Shopby_Helper_Image
    {

    }

} else {

    class OpenTechiz_PersonalizedProduct_Helper_Rewrite_Catalog_Image_Pure extends Mage_Catalog_Helper_Image
    {

    }

}
class OpenTechiz_PersonalizedProduct_Helper_Rewrite_Catalog_Image extends OpenTechiz_PersonalizedProduct_Helper_Rewrite_Catalog_Image_Pure
{
    protected $isAdmin = false;
    protected $getIsAdmin = false;
    
    const ATTR_MEDIA_GALLERY = 88;

    const COLOR = [
        'yellow' => ['ye', 'mainye'],
        'rose'   => ['ro','mainro'],
        'white'  => ['wh','mainwh'],
        'white/yellow' => ['wy', "mainwy"],
        'white/rose' => ['wr', "mainwr"],
        'white/yellow/rose' => ['wyr', "mainwyr"],
        'black' => ['bl', 'mainbl'],
    ];

    /**
     * Crop position
     *
     * @var string
     */
    protected $_cropPosition;

    /**
     * Adpative resize flag
     *
     * @var bool
     */
    protected $_scheduleAdaptiveResize = false;

    /**
     * Reset all previos data
     *
     * @return Bolevar_AdaptiveResize_Helper_Image
     */
    protected function _reset()
    {
        $this->_scheduleAdaptiveResize = false;
        $this->_cropPosition = 0;
        parent::_reset();
    }

    /**
     * Set crop bosition
     *
     * @param string $position top, bottom or center
     *
     * @return \Bolevar_AdaptiveResize_Helper_Image
     */
    public function setCropPosition($position)
    {
        $this->_cropPosition = $position;
        return $this;
    }

    private function getImageByLabel($labels, $product){
        $select = $this->getConnection()->select();
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->from(array('main_table' => $this->getTableName("catalog_product_entity_media_gallery")), array("value_id", "value"));
        $select->join(array("lab" => $this->getTableName("catalog_product_entity_media_gallery_value")), "main_table.value_id = lab.value_id AND lab.disabled = 0", array("label"));
        $select->where("main_table.attribute_id = ?", self::ATTR_MEDIA_GALLERY);
        $select->where("main_table.entity_id = ?", $product->getId());
        $select->where("lab.label IN(?)", $labels);
        $result = $this->getConnection()->fetchAll($select);
        $imageUrl = false;
        foreach ($labels as $l){
            foreach ($result as $item){
                if ($l == $item['label']){
                    $imageUrl = $item['value'];
                }
            }
        }
        return $imageUrl;
    }
    
    private function getTableName($table)
    {
        return $this->getResource()->getTableName($table);
    }

    private function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    private function getConnection()
    {
        return $this->getResource()->getConnection('core_read');
    }
    /**
     * Return generated image url
     *
     * @return string
     */
    public function __toString()
    {
        try {
            $product = $this->getProduct();
            $sku = $product->getSku();

            $skuParts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);

            if (strpos($sku, 'quotation') !== false && count($skuParts) > 1) {
                $quoteProductId = $skuParts[1];
                $quoteProduct = Mage::getModel('opentechiz_quotation/product')->load($quoteProductId);
                return explode(',', $quoteProduct->getQpImage())[0];
            } elseif (strpos($sku, 'quotation') !== false){
                return Mage::getDesign()->getSkinUrl($this->getPlaceholder());
            }

            if($this->getIsAdmin == false){
                $this->isAdmin = Mage::app()->getStore()->isAdmin();
                $this->getIsAdmin = true;
            }
            $isPersonalized = false;
            if(Mage::helper('personalizedproduct')->isPersonalizedProduct($product)){
                $isPersonalized = true;
            }

            if($isPersonalized) {
                if (count(explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)) == 1) {
                    $sku = $this->getDefaultOptionSku($product);
                }

                if (count(explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)) > 3) {
                    $url = Mage::getBaseUrl() . 'media/personalized/product/';
                    $url = str_replace('index.php/', '', $url);
                    $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
                    $imgName = $parts[0];
                    $imgName .= '-' . $parts[count($parts) - 2];
                    for ($i = 1; $i < count($parts) - 2; $i++) {
                        $stoneName = $parts[$i];
                        if (in_array(str_replace('diamond', '', $stoneName), OpenTechiz_PersonalizedProduct_Helper_Data::QUALITY)) {
                            $stoneName = 'diamond';
                        }
                        $imgName .= '-' . $stoneName;
                    }
                    $imgName .= '-1.jpg';
                    $url .= $imgName;
                }

                if(!isset($url)){
                    $url = Mage::getDesign()->getSkinUrl($this->getPlaceholder());
                }
            } else {
                $model = $this->_getModel();
                $skuParts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
                $color = '';
                if(count($skuParts) > 1 ){
                    // get corlor from sku
                    $color = false;
                    foreach ($skuParts as $skuPart){
                        if(array_key_exists($skuPart, self::COLOR)) {
                            $color = $skuPart;
                            break;
                        }
                    } 
                    if($color && count(self::COLOR[$color]) > 0){
                        $imageUrl = $this->getImageByLabel(self::COLOR[$color], $this->getProduct());
                        if($imageUrl){
                            $this->setImageFile($imageUrl);
                        }
                    }
                }

                if ($this->getImageFile()) {
                    $model->setBaseFile($this->getImageFile());
                } else {
                    $product = $this->getProduct();
                    $baseFile = $product->getData($model->getDestinationSubdir());
                    if(!$baseFile){
                        $product = Mage::getModel('catalog/product')->load($product->getId());
                        $baseFile = $product->getData($model->getDestinationSubdir());
                    }
                    $model->setBaseFile($baseFile);
                }

                if ($model->isCached()) {
                    return $model->getUrl();
                } else {
                    if ($this->_scheduleRotate) {
                        $model->rotate($this->getAngle());
                    }

                    if ($this->_scheduleResize) {
                        $model->resize();
                    }

                    if ($this->getWatermark()) {
                        $model->setWatermark($this->getWatermark());
                    }

                    $url = $model->saveFile()->getUrl();
                }
            }

            $url = str_replace(' ', '', $url);
            return $url;

        } catch (Exception $e) {
//            Mage::logException($e);
            $url = Mage::getDesign()->getSkinUrl($this->getPlaceholder());
        }
        return $url;
    }

    public function image_exists($url){
//        $ch = curl_init($url);
//        curl_setopt($ch, CURLOPT_NOBODY, true);
//        curl_exec($ch);
//        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//        if($code == 401){
//            curl_setopt($ch, CURLOPT_USERPWD, "test:!test123!");
//            curl_exec($ch);
//            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//        }
//        if ($code != 404) {
//            $status = true;
//        } else {
//            $status = false;
//        }
//        curl_close($ch);

        $basePath = Mage::getBaseDir('media').'/catalog/product';
        $filename = $basePath.$url;
        return is_readable($filename);
    }

    public function getDefaultOptionSku($product){
        $isPersonalized = false;
        if(Mage::helper('personalizedproduct')->isPersonalizedProduct($product)){
            $isPersonalized = true;
        }

        $sku = $product->getSku();
        if($isPersonalized) {
            $flag = false;
            $product = Mage::getModel('catalog/product')->load($product->getId());
//            foreach ($product->getOptions() as $option) {
//                if ($option->getType() == 'stone' || $option->getType() == 'gold') {
//                    $flag = true;
//                    break;
//                }
//            }
//            if ($flag == true) {
            $defaultSideStone = OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_STONE;
            $defaultStone = $product->getAttributeText("default_stone");
            $defaultAlloy = $product->getAttributeText("default_alloy");

            $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . $defaultStone;
            foreach ($product->getOptions() as $option) {
                if ($option->getType() == 'stone' && $option->getSortOrder() > 0) {
                    $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . $defaultSideStone;
                }
            }
            $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . $defaultAlloy . OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.'1.jpg';
//            }
        }

        return $sku;
    }

    /**
     * Init Image processor model
     *
     * Rewrited to change model
     *
     * @param Mage_Catalog_Model_Product $product       product
     * @param string                     $attributeName attribute name
     * @param string                     $imageFile     image file name
     *
     * @return \Bolevar_AdaptiveResize_Helper_Image
     */

    public function init(Mage_Catalog_Model_Product $product, $attributeName, $imageFile=null)
    {
        $this->_reset();
        $this->_setModel(Mage::getModel('catalog/product_image'));
        $this->_getModel()->setDestinationSubdir($attributeName);
        $this->setProduct($product);

        $this->setWatermark(
            Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_image")
        );
        $this->setWatermarkImageOpacity(
            Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_imageOpacity")
        );
        $this->setWatermarkPosition(
            Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_position")
        );
        $this->setWatermarkSize(
            Mage::getStoreConfig("design/watermark/{$this->_getModel()->getDestinationSubdir()}_size")
        );

        if ($imageFile) {
            $this->setImageFile($imageFile);
        } else {
            // add for work original size
            $this->_getModel()->setProduct($this->getProduct());
            //$this->_getModel()->setBaseFile($this->getProduct()->getData($this->_getModel()->getDestinationSubdir()));
        }
        return $this;
    }
    public function getModel()
    {
        return $this->_model;
    }
}
