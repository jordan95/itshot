<?php
class OpenTechiz_PersonalizedProduct_Helper_PersonalizedPriceHelper extends Mage_Core_Helper_Abstract
{
    const DEFAULT_QUALITY = OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_QUALITY;
    const DEFAULT_STONE = OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_STONE;
    protected $stonePriceArray = [];
    protected $alloyPriceArray = [];
    
    public function getStonePrice($optionId, $product_id)
    {
        $collection = Mage::getModel('personalizedproduct/price_stone')->getCollection()->addFieldToFilter('product_id', $product_id)
            ->addFieldToFilter('option_id', $optionId);
        $results = [];
        $stoneGroups = [];
        foreach ($collection as $stonePriceEntity){
            $stoneId = $stonePriceEntity->getStoneId();
            $group = $stonePriceEntity->getStoneSelectGroup();

            $stonePrice = $this->getStonePriceArray($stoneId);
            $name = str_replace(' ', '', trim(strtolower($stonePrice->getStoneType())));

            $priceEach = $stonePrice->getPrice();
            $quantity = $stonePriceEntity->getQuantity();
            $price = $priceEach*$quantity;

            if(!array_key_exists($group, $stoneGroups)){
                $stoneGroups[$group] = [];
            }
            if(!array_key_exists($name, $stoneGroups[$group])){
                $stoneGroups[$group][$name] = $price;
                if($stonePrice->getQuality()) $stoneGroups[$group]['quality'] = true;
            }else {
                $stoneGroups[$group][$name] += $price;
                if($stonePrice->getQuality()) $stoneGroups[$group]['quality'] = true;
            }
        }
        foreach ($stoneGroups as $group){
            foreach($group as $key => $value){
                if(array_key_exists('quality', $group)){
                    $results[$key] = 0;
                } else
                    $results[$key] = $value;
            }
        }

        return $results;
    }

    public function getStoneWithQualityPrice($optionId, $product_id){
        $collection = Mage::getModel('personalizedproduct/price_stone')->getCollection()->addFieldToFilter('product_id', $product_id)
            ->addFieldToFilter('option_id', $optionId);
        $results = [];
        $stoneGroups = [];

        foreach ($collection as $stonePriceEntity){

            $stoneId = $stonePriceEntity->getStoneId();
            $group = $stonePriceEntity->getStoneSelectGroup();
            $stonePrice = $this->getStonePriceArray($stoneId);

            $quality = str_replace(' ', '', trim(strtolower($stonePrice->getQuality())));
            $price = $stonePrice->getPrice() * $stonePriceEntity->getQuantity();


            if(!array_key_exists($group, $stoneGroups)){
                $stoneGroups[$group][$quality] = $price;
            }else {
                $stoneGroups[$group][$quality] += $price;
            }
        }

        foreach ($stoneGroups as $group){
            foreach($group as $key => $value){
                if($key != null && $key != '') $results[$key] = $value;
            }
        }
        return $results;
    }

    public function getAlloyPrice($product_id)
    {
        $collection = Mage::getModel('personalizedproduct/price_alloy')->getCollection()->addFieldToFilter('product_id', $product_id);
        $product = Mage::getModel('catalog/product')->load($product_id);
        $results = [];

        foreach ($collection as $alloyPriceEntity){
            $goldId = $alloyPriceEntity->getGoldId();
            $alloyPrice = $this->getAlloyPriceArray($goldId);

            $goldType= trim(strtolower($alloyPrice->getGoldType()));
            $goldColor= trim(strtolower($alloyPrice->getGoldColor()));
            $name = $goldType.$goldColor;
            $name = str_replace(' ', '', $name);
            $name = trim($name);
            
            $dataHelper = Mage::helper('personalizedproduct/data');
            $goldWeight = ($dataHelper::ALLOY_DENSITY[$goldType]/$dataHelper::ALLOY_DENSITY['18k']) * (float)$product->getData('base_weight');

            $price =  $alloyPrice->getPrice() * $goldWeight * $product->getData('price_ratio') ;

            $results[$name] = $price;
        }
        return $results;
    }

    public function getDefaultPersonalizedPrice($productId){
        $_product = Mage::getModel('catalog/product')->load($productId);
        $productId = $_product->getId();

        $stoneCollection = Mage::getModel('personalizedproduct/price_stone')->getCollection();
        $stoneCollection->getSelect()->join(array('stone_price' => 'tsht_material_stone'), 'stone_price.stone_id = main_table.stone_id', '*');
        
        $goldCollection = Mage::getModel('personalizedproduct/price_alloy')->getCollection();
        $goldCollection->getSelect()->join(array('gold_price' => 'tsht_material_gold'), 'gold_price.gold_id = main_table.gold_id', '*');
        
        $listingCollection = Mage::getModel('personalizedproduct/price_product')->getCollection();

        $stoneName = $_product->getAttributeText("default_stone");
        $alloyName = $_product->getAttributeText("default_alloy");

        $stoneName = str_replace(' ', '', $stoneName);
        $stoneName = strtolower($stoneName);

        $alloyName = str_replace(' ', '', $alloyName);
        $alloyName = strtolower($alloyName);

        $alloyCollection = $goldCollection;
        $alloyId = $this->getAlloyId($alloyCollection, $alloyName, $productId);

        $stone = $stoneCollection->addFieldToFilter('stone_type', $stoneName)->addFieldToFilter('product_id', $productId);

        if($stoneName == 'diamond'){
            $stone->addFieldToFilter('quality', self::DEFAULT_QUALITY);
        }
        $stoneSelectGroup = $stone->getFirstItem()->getStoneSelectGroup();

        $price = $listingCollection->addFieldToFilter('product_id', $productId)->addFieldToFilter('stone_select_group', $stoneSelectGroup)
            ->addFieldToFilter('gold_id', $alloyId)->getFirstItem()->getPrice();

        $price = number_format((float)$price, 2, '.', '');

        return $price;
    }

    public function getAlloyId($alloyCollection, $alloyName, $productId){
        foreach($alloyCollection as $alloy){
            $product_id = $alloy->getProductId();
            $type = $alloy->getGoldType();
            $name = $alloy->getGoldColor();
            $alloyFullName = $type.$name;
            $alloyFullName = str_replace(' ', '', $alloyFullName);
            $alloyFullName = strtolower($alloyFullName);
            if($alloyName == $alloyFullName && $product_id == $productId){
                $alloyId = $alloy->getGoldId();
                break;
            }
        }
        return $alloyId;
    }

    public function getStonePriceArray($stoneId){
        if(count($this->stonePriceArray) > 0 && isset($this->stonePriceArray[$stoneId])){
            return $this->stonePriceArray[$stoneId];
        }else{
            $stoneCollection = Mage::getModel('opentechiz_material/stone')->getCollection();
            foreach ($stoneCollection as $stone){
                $this->stonePriceArray[$stone->getId()] = $stone;
            }

            return $this->stonePriceArray[$stoneId];
        }
    }

    public function getAlloyPriceArray($goldId){
        if(count($this->alloyPriceArray) > 0 && isset($this->alloyPriceArray[$goldId])){
            return $this->alloyPriceArray[$goldId];
        }else{
            $alloyCollection = Mage::getModel('opentechiz_material/gold')->getCollection();
            foreach ($alloyCollection as $alloy){
                $this->alloyPriceArray[$alloy->getId()] = $alloy;
            }

            return $this->alloyPriceArray[$goldId];
        }
    }
}