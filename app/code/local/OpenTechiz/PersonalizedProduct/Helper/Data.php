<?php

class OpenTechiz_PersonalizedProduct_Helper_Data extends Mage_Core_Helper_Abstract
{
    const ATTRIBUTE_SET_NAME = 'Personalized Product';
    const ATTRIBUTE_SET_ID = 105;
    const ENGRAVING_KEYWORD = 'engraving';
    const ENGRAVING_TEXT_KEYWORD = 'engraving text';
    const DEFAULT_MAIN_STONE = 'diamond';
    const DEFAULT_STONE = 'diamond';
    const DEFAULT_QUALITY = 'si';
    const DEFAULT_ALLOY = 'silver';
    const QUALITY = ['si', 'vs', 'i'];
    const STONES_WITH_QUALITY = ['diamond'];
    const DEFAULT_QUALITY_ARRAY = ['' => 'none', 'si' => 'SI', 'vs' => 'VS', 'i' => 'I'];
    const DONT_ENGRAVE = 'don\'t engrave';
    const SIZE = 'size';
    const ALLOY_DENSITY = [
        '18k' => 15.51,
        '14k' => 13.10,
        '10k' => 11.50,
        'silver' => 10.36,
        'platinum' => 21.45,
        'palladium' => 12.02
    ];

    const OPPENTECHIZ_OPTION_LABEL_POSITION = [
        'Center Stone' => 0,
        'Stone 2' => 1,
        'Stone 3' => 2,
        'Stone 4' => 3,
    ];

    const ALLOY_OPTION = [
        'platinum' =>'Platinum',
        'silver' => 'Silver',
        '10kwhitegold'  => '10K White Gold',
        '10kyellowgold' => '10K Yellow Gold',
        '10krosegold'   => '10K Rose Gold',
        '10kgreengold'  => '10K Green Gold',
        '14kwhitegold'  => '14K White Gold',
        '14kyellowgold' => '14K Yellow Gold',
        '14krosegold'   => '14K Rose Gold',
        '14kgreengold'  => '14K Green Gold',
        '18kwhitegold'  => '18K White Gold',
        '18kyellowgold' => '18K Yellow Gold',
        '18krosegold'   => '18K Rose Gold',
        '18kgreengold'  => '18K Green Gold',
    ];

    const BIRTH_STONE = [
        'amethyst' => 'February',
        'aquamarine' => 'March',
        'bluesapphire' => 'September',
        'bluetopaz' => 'December',
        'citrine' => 'November',
        'diamond' => 'April',
        'diamondblack' => 'April',
        'diamondyellow' => 'April',
        'diamondpink' => 'April',
        'diamondgreen' => 'April',
        'diamondblue' => 'April',
        'diamondbrown' => 'April',
        'blackdiamond' => 'April',
        'yellowdiamond' => 'April',
        'pinkdiamond' => 'April',
        'greendiamond' => 'April',
        'bluediamond' => 'April',
        'browndiamond' => 'April',
        'emerald' => 'May',
        'greenamethyst' => 'February',
        'moissanite' => '',
        'morganite' => 'November',
        'peridot' => 'August',
        'pinksapphire' => 'September',
        'quartzrose' => 'November',
        'quartzsmoky' => 'November',
        'redgarnet' => 'January',
        'rhodolitegarnet' => 'January',
        'ruby' => 'July',
        'tanzanite' => 'December',
        'tourmalinegreen' => 'October',
        'tourmalinepink' => 'October',
        'whitesapphire' => 'September',
        'yellowsapphire' => 'September',
    ];

    const STONE_ANNIVERSARY = [
        'diamond' => '60th',
        'diamondblack' => '60th',
        'diamondyellow' => '60th',
        'diamondpink' => '60th',
        'diamondgreen' => '60th',
        'diamondblue' => '60th',
        'diamondbrown' => '60th',
        'blackdiamond' => '60th',
        'yellowdiamond' => '60th',
        'pinkdiamond' => '60th',
        'greendiamond' => '60th',
        'bluediamond' => '60th',
        'browndiamond' => '60th',
        'ruby' => '40th',
        'emerald' => '4th',
        'bluesapphire' => '4th',
        'pinksapphire' => '45th',
        'whitesapphire' => '5th and 45th',
        'yellowsapphire' => '5th and 45th',
        'aquamarine' => '19th',
        'amethyst' => '6th',
        'tanzanite' => '24th',
        'citrine' => '13th',
        'morganite' => '',
        'peridot' => '16th',
        'bluetopaz' => '4th',
        'tourmalinepink' => '8th',
        'redgarnet' => '2nd',
        'rhodolitegarnet' => '2nd',
        'tourmalinegreen' => '8th',
        'quartzrose' => '',
        'quartzsmoky' => '',
        'greenamethyst' => '6th',
        'moissanite' => '',
    ];

    protected $personalized_attribute_set = null;
    protected $normalPriceData;

    public function isPersonalizedProduct($product){
        $attributeSetId = $product->getAttributeSetId();
        if(!$attributeSetId) {
            $product = Mage::getModel('catalog/product')->load($product->getId());
            $attributeSetId = $product->getAttributeSetId();
        }

        if($this->personalized_attribute_set == null){
            $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
            $attributeSetModel->getCollection()
                ->addFieldToFilter('attribute_set_name', array('like' => '%'.self::ATTRIBUTE_SET_NAME.'%'))
                ->getFirstItem();
            $this->personalized_attribute_set = $attributeSetModel->getId();
        }

        if(!$this->personalized_attribute_set){
            $this->personalized_attribute_set = self::ATTRIBUTE_SET_ID;
        }

        if($this->personalized_attribute_set == $attributeSetId){
            return true;
        } else return false;
    }

    public function canProduceProduct($productId){
        $product = Mage::getModel('catalog/product')->load($productId);
        $productionBrands = Mage::getStoreConfig('opentechiz_production/general/can_produce');
        $productionBrands = explode(',', $productionBrands);
        if(!$product->getData('c2c_brand')) return true;
        elseif(in_array(str_replace(' ', '', strtolower($product->getData('c2c_brand'))), $productionBrands)) return true;
        else return false;
    }

    public function hasEngravingOption($productId){
        $product = Mage::getModel('catalog/product')->load($productId);
        $hasEngraving = false;
        foreach ($product->getOptions() as $option) {
            if(strpos($option->getTitle(), 'Engraving') !== false){
                $hasEngraving = true;
                break;
            }
        }
        return $hasEngraving;
    }

    public function getProductNormalPrice($productId)
    {
        if ($this->normalPriceData === null) {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $query = $readConnection->select()
                ->from($resource->getTableName('product_listing_price'), ['product_id', 'price']);
            $results = [];
            foreach ($readConnection->fetchAll($query) as $result){
                $results[$result['product_id']] = $result['price'];
            }
            $this->normalPriceData = $results;
        }
        return isset($this->normalPriceData[$productId]) ? $this->normalPriceData[$productId] : '';
    }
}