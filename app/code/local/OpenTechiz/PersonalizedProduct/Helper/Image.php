<?php
class OpenTechiz_PersonalizedProduct_Helper_Image extends Mage_Core_Helper_Abstract {
    const PATH_CONFIG = 'web/customlink/base_path_layer';
    public function getBasePathLayer($width = false, $height = false)
    {
        if(!$path = Mage::getStoreConfig(self::PATH_CONFIG))
        {
            $path = Mage::getBaseUrl() . 'image_render.php?path=';
        }
        if($width && $height)
            $path = str_replace(',q_auto/media', ',q_auto,w_'. $width .',h_'. $height .'/media', $path);
        if(strpos($path, 'image_render.php') != false && $width && $height)
        {
            $path = $path . 'size/' . $width . 'x' . $height . '/';
        }
        return $path;
    }
    public function prepareParamLayer($data)
    {
        $result = array();
        foreach($data as $key => $value)
        {
            if($key == 'alloy')
            {
                $value = $this->cutOffFromString($value);
            }elseif(in_array($key, array('0', '1', '2','3')))
            {
                $value = $value . '_' . $this->getDefaultQuality($value);
            }
            $result[$key] = $value;
        }
        return $result;
    }
    public function getDefaultQuality($namestone)
    {
        return '';
    }
    public function convertArrayToPathstring($array)
    {
        $path = array();
        foreach($array as $key =>$value)
        {
            $path[] = $key . '/' . $value;
        }
        return implode('/', $path);
    }
    public function addDataImageForProduct($_product, $_options)
    {
        $alloyTitle = strtolower(str_replace(' ','',trim('Alloy/Colour')));
        $diamondTitle = strtolower(str_replace(' ','',trim('Stone/Diamonds')));
        $diamond2Title = strtolower(str_replace(' ','',trim('Stone 2')));
        $diamond3Title = strtolower(str_replace(' ','',trim('Stone 3')));
        $diamond4Title = strtolower(str_replace(' ','',trim('Stone 4')));
        $pearlTitle = strtolower(str_replace(' ','',trim('Pearl')));
        foreach ($_options as $key => $option) {
            $defaultTitle = '';
            if(isset($option['default_title']))
            {
                $defaultTitle = strtolower(str_replace(' ','',trim($option['default_title'])));
            }
            if(isset($option['label']))
            {
                $defaultTitle = strtolower(str_replace(' ','',trim($option['label'])));
            }
            $sku = '';
            $labsku = '';
            if(isset($option['sku_value']))
                $sku = $option['sku_value'];
            if(!$sku && isset($option['op_selected']) && $option['op_selected'])
                $sku = $option['op_selected']->getSku();
            $quality = '';
            if(isset($option['stone_quality']) && isset($option['stone_quality']['quality']))
            {
                $quality =  Mage::helper('personalizedproduct/stone')->getRealQuality($option['stone_quality']['quality']);
                if($option['stone_quality']['heated'] && $option['stone_quality']['heated'] != 'Heated')
                    $quality = $quality . '_' . $option['stone_quality']['heated'];
            }
            if(isset($option['additional_values']['stone_synthetic'])){
                $dataSynthetic = $option['additional_values']['stone_synthetic'];
                if(isset($dataSynthetic["stone_type"])){
                    $labsku = $dataSynthetic["stone_type"];
                    $quality = Mage::getSingleton("syntheticstone/stone")->getStoneQuality();
                }
            }
            if ($sku) {
                if ($defaultTitle == $alloyTitle) {
                    $_product->setAlloySku($sku);
                }else
                    if ($defaultTitle == $pearlTitle) {
                        $_product->setPearlSku($sku);
                    }else
                        if ($defaultTitle == $diamond2Title) {
                            $_product->setStoneTwoSku($sku);
                            $_product->setStone2Quality($quality);
                        }
                if ($defaultTitle == $diamond4Title) {
                    $_product->setStoneFourSku($sku);
                    $_product->setStone4Quality($quality);
                }
                if ($defaultTitle == $diamond3Title) {
                    $_product->setStoneThreeSku($sku);
                    $_product->setStone3Quality($quality);
                }else
                    if ($defaultTitle == 'profile') {
                        $_product->setProfileSku($sku);
                    }else
                        if ($defaultTitle == 'surface') {
                            $_product->setSurfaceSku($sku);
                        }else
                            if ($defaultTitle == 'width') {
                                $_product->setWidthSku($sku);
                                $_product->setOpwidthSku($sku);
                            }else
                                if($defaultTitle == 'accentcolour')
                                {
                                    $_product->setAccentcolorSku($sku);
                                }else
                                    if($defaultTitle == $diamondTitle)
                                    {
                                        $_product->setDiamondSku($sku);
                                        $_product->setDiamondQuality($quality);
                                        $_product->setLabSku($labsku);
                                    }
            }
        }
    }
    public function getProductImageUrlTypeLayer($_product, $width = null, $height = null)
    {
        if(!$_product || !$_product->getImageLoadType() == 'layer' || !$_product->getSkuImage())
            return false;
        $sku = strtolower($_product->getSkuImage());
        if(!$imgps = Mage::app()->getRequest()->getParam("imgps"))
            $imgps = 1;
        if($_product->getImagePosition())
            $imgps = $_product->getImagePosition();
        $params = array();
        //if(!is_null($width) || !is_null($height))
        //$params['size'] = $width . 'x' . $height;

        $params['view'] = $imgps;
        $params['sku'] = $sku;

        if($pearl = $_product->getPearlSku())
            $params['pearl'] = $pearl;
        if($diamond = $_product->getDiamondSku())
        {
            if(!$quality = $_product->getDiamondQuality())
                $quality = $this->getDefaultQuality($diamond);
            if($_product->getLabSku()){
                $diamond = $diamond . '_' .'lab';
            }
            $diamond = $diamond . '_' . $quality;
            $params['diamond'] = $diamond;
        }
        if($stone2 = $_product->getStoneTwoSku())
        {
            $quality = $this->getDefaultQuality($stone2);
            if($_product->getStone2Quality())
                $quality = $_product->getStone2Quality();
            elseif($_product->getStoneTwoQuality())
                $quality = $_product->getStoneTwoQuality();
            $stone2 = $stone2 . '_' . $quality;
            $params['stone2'] = $stone2;
        }
        if($stone3 = $_product->getStoneThreeSku())
        {
            $quality = $this->getDefaultQuality($stone3);
            if($_product->getStone3Quality())
                $quality = $_product->getStone3Quality();
            elseif($_product->getStoneThreeQuality())
                $quality = $_product->getStoneThreeQuality();
            $stone3 = $stone3 . '_' . $quality;
            $params['stone3'] = $stone3;
        }
        if($stone4 = $_product->getStoneFourSku())
        {
            $quality = $this->getDefaultQuality($stone4);
            if($_product->getStone4Quality())
                $quality = $_product->getStone4Quality();
            elseif($_product->getStoneFourQuality())
                $quality = $_product->getStoneFourQuality();
            $stone4 = $stone4 . '_' . $quality;
            $params['stone4'] = $stone4;
        }
        if($alloycolour = $_product->getAlloySku())
            $params['alloycolour'] = $this->cutOffFromString($alloycolour);
        if($accentcolor = $_product->getAccentcolorSku())
            $params['accent'] = $accentcolor;
        if($profile = $_product->getProfileSku())
            $params['profile'] = $profile;
        if($surface = $_product->getSurfaceSku())
            $params['surface'] = $surface;
        if($optwidth = $_product->getOpwidthSku())
            $params['width'] = $optwidth;
        elseif($width = strtolower($_product->getWidthSku()))
            $names[] = $width;
        $path = $this->convertArrayToPathstring($params) . '.jpg';
        if($_product->getRenderForPdf())
            return $this->_saveFileLayerImage($path, $width, $height);
        $basePath = $this->getBasePathLayer($width, $height);
        return $basePath . $path;
    }
    protected function _removeQualityString($path)
    {
        // ++ fix diamond => diamond-brillant
        if(strpos($path, '/diamond_') !== false || strpos($path, '/diamond.png') !== false)
        {
            $path = str_replace('/diamond_', "/diamond-brillant_", $path);
            $path = str_replace('/diamond.png', "/diamond-brillant.png", $path);
            if(file_exists($path))
                return $path;
        }
        if(strpos($path, '_diamond_') !== false || strpos($path, '_diamond.jpg') !== false)
        {
            $path = str_replace('_diamond_', "_diamond-brillant_", $path);
            $path = str_replace('_diamond.jpg', "_diamond-brillant.jpg", $path);
            if(file_exists($path))
                return $path;
        }
        // remove quality
        $listquality = array(
            '_a.png', '_aa.png', '_aaa.png', '_aaaa.png', '_aaa_colombian.png', '_aaaa_colombian.png' ,'_aaa_unheated.png', '_aaaa_unheated.png', '_african.png'
        );
        foreach($listquality as $name)
        {
            $path = str_replace($name, ".png", $path);
        }
        $listquality = array(
            '_a.jpg', '_aa.jpg', '_aaa.jpg', '_aaaa.jpg', '_aaa_colombian.jpg', '_aaaa_colombian.jpg' ,'_aaa_unheated.jpg', '_aaaa_unheated.jpg', '_african.jpg'
        );
        foreach($listquality as $name)
        {
            $path = str_replace($name, ".jpg", $path);
        }

        return $path;
    }
    protected function _saveFileLayerImage($path, $width = null, $height = null)
    {
        $mediaUrl = Mage::getBaseDir('media') . DS . 'product';
        $mediaDir = Mage::getBaseDir('media');
        $path = strtolower($path);
        $crpath = str_replace('\\','/', $path);
        try{
            $path = trim($path, '/');
            $path = str_replace('.jpg', '', $path);
            $listPaths = explode('/', $path);
            $listParams = array();
            $i = 0;
            foreach($listPaths as $key=>$value)
            {
                if($i++ % 2 == 0)
                    $listParams[$value] = $listPaths[$key + 1];
            }
            $sku = '';
            $view = '';
            if(isset($listParams['sku']))
            {
                $sku = $listParams['sku'];
                unset($listParams['sku']);
            }
            if(isset($listParams['view']))
            {
                $view = $listParams['view'];
                unset($listParams['view']);
            }
            $size = '';
            if(isset($listParams['size']))
            {
                $size = $listParams['size'];
                unset($listParams['size']);
            }

            $image = new \Imagick($mediaUrl . DS .'notfound.jpg');
            // make alloy colour as first layer.
            if(isset($listParams['alloycolour']))
            {
                $url = $mediaUrl . DS . 'layer' . DS . $sku . DS . 'view' . $view . DS . 'alloycolour' . DS . $listParams['alloycolour'] . '.jpg';
                $urlNew = $url;
                $baseDiamondUrl = $url;
                //+++ check base image jpg in alloycolour_diamond OR diamond
                if(!file_exists($url) && isset($listParams['diamond']))
                {
                    $urlNew = $mediaUrl . DS . 'layer' . DS . $sku . DS . 'view' . $view . DS . 'alloycolour_diamond' . DS . $listParams['alloycolour'] . '_'. $listParams['diamond']  .'.jpg';
                    $baseDiamondUrl = $mediaUrl . DS . 'layer' . DS . $sku . DS . 'view' . $view . DS . 'diamond' . DS . $listParams['diamond']  .'.jpg';
                    if(!file_exists($urlNew))
                    {
                        $urlNew = $this->_removeQualityString($urlNew);
                    }
                    if(!file_exists($baseDiamondUrl))
                    {
                        $baseDiamondUrl = $this->_removeQualityString($baseDiamondUrl);
                    }
                }
                //--- end check
                if(file_exists($url))
                {
                    $layer = new \Imagick(realpath($url));
                    $image->addImage($layer);
                    unset($listParams['alloycolour']);
                }
                elseif($urlNew && file_exists($urlNew))
                {
                    $layer = new \Imagick(realpath($urlNew));
                    $image->addImage($layer);
                    unset($listParams['diamond']);
                    unset($listParams['alloycolour']);
                }elseif(file_exists($baseDiamondUrl))
                {
                    $layer = new \Imagick(realpath($baseDiamondUrl));
                    $image->addImage($layer);
                    $url = $mediaUrl . DS . 'layer' . DS . $sku . DS . 'view' . $view . DS . 'alloycolour' . DS . $listParams['alloycolour'] . '.png';
                    if(file_exists($url))
                    {
                        $layer = new \Imagick(realpath($url));
                        $image->addImage($layer);
                        unset($listParams['diamond']);
                        unset($listParams['alloycolour']);
                    }
                }elseif($newpath = $this->_checkImageIsOldVersion($sku, $view, $listParams)) // check image type full name. ( not layer )
                {
                    $layer = new \Imagick(realpath($newpath));
                    $image->addImage($layer);
                }
                else
                {
                    //throw new Exception('Missing Layer Alloy Color');
                }
            }
            // merger layer
            foreach($listParams as $folder=>$name)
            {
                if($folder == 'alloycolour')
                    $name .= '.jpg';
                else
                    $name .= '.png';
                $url = $mediaUrl . DS . 'layer' . DS . $sku . DS . 'view' . $view . DS . $folder . DS . $name;
                if(!file_exists($url))
                {
                    $url = $this->_removeQualityString($url);
                    if(!file_exists($url))
                        continue;
                }
                $layer = new \Imagick(realpath($url));
                $image->addImage($layer);
            }
            $image->setImageFormat('jpg');
            $result = $image->mergeImageLayers(13);
            if($width && $height)
            {
                $result->resizeImage($width, $height, Imagick::FILTER_LANCZOS, 1);
            }
            $pathFile = $mediaDir . DS . 'tmp' . DS . 'catalog' . DS . 'product' . DS . $crpath;
            $customDir = Mage::getBaseDir('media').DS.'foldername';
            $ioFile = new Varien_Io_File();
            $ioFile->checkAndCreateFolder(dirname($pathFile));
            $result->writeImage($pathFile);
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'tmp' . DS . 'catalog' . DS . 'product' . DS . $crpath;
        }catch(Exception $e)
        {
            return '';
        }
    }
    protected function _checkImageIsOldVersion($sku, $view, &$listParams)
    {
        $mediaUrl = Mage::getBaseDir('media') . DS . 'product';
        $path = $mediaUrl . DS . 'layer' . DS . $sku . DS . 'view' . $view;
        foreach($listParams as $key=>$value)
        {
            $newvalue = $this->_removeQualityStringForOldVersion($value);
            $path .= DS . $key . DS . $newvalue;
        }
        if(file_exists( $path . '.jpg'))
        {
            $listParams = array();
            return $path . '.jpg';
        }
        return false;
    }
    protected function _removeQualityStringForOldVersion($string)
    {
        $listquality = array(
            '_aaaa', '_aaa', '_aa', '_a', '_aaa_colombian', '_aaaa_colombian' ,'_aaa_unheated', '_aaaa_unheated', '_aaa_african', '_aaaa_african', '_african'
        );
        foreach($listquality as $name)
        {
            $string = str_replace($name, "", $string);
        }
        return $string;
    }
}