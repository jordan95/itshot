<?php
class OpenTechiz_PersonalizedProduct_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    /**
     * Initialize Controller Router
     *
     * @param Varien_Event_Observer $observer
     */
    public function initControllerRouters($observer)
    {
        /* @var $front Mage_Core_Controller_Varien_Front */
        $front = $observer->getEvent()->getFront();

        $front->addRouter('category_seo_url', $this);//replace router_name with a suitable name
    }

    /**
     * Validate and Match router with the Page and modify request
     *
     * @param Zend_Controller_Request_Http $request
     * @return bool
     */
    public function match(Zend_Controller_Request_Http $request)
    {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
            exit;
        }

        $identifier = trim($request->getPathInfo(), '/');
        $condition = new Varien_Object(array(
            'identifier' => $identifier,
            'continue' => true
        ));
        Mage::dispatchEvent('opentechiz_router_before_match_category_seo_url', array(
            'router' => $this,
            'condition' => $condition
        ));

        $identifier = $condition->getIdentifier();
        $remainingParams = '';
        if (strpos($identifier, '?') !== false) {
            $remainingParams = array_pop(explode('?', $identifier));
        }
        $identifier = str_replace($remainingParams, '', $identifier);

        $parts = explode('/', $identifier);

        //stop illegal routing
        $possibleUrl =[];

        for($i = 1; $i < count($parts); $i++ ){
            $url = '';
            for($j = 0; $j <= $i; $j ++){
                $url .= $parts[$j].'/';
            }
            $url = trim($url, '/');
            array_push($possibleUrl, $url);
        }
        $currentUrl = '';
        for ($i = 1; $i < count($possibleUrl); $i++) {
            $oUrlRewriteCollection = Mage::getModel('core/url_rewrite')
                ->getCollection()
                ->addFieldToFilter('request_path', $possibleUrl[$i]);
            if(count($oUrlRewriteCollection) == 0){
                $currentUrl = $possibleUrl[$i]; //not in rewrite collection
                break;
            }
        }
        //check last valid url
        if(isset($possibleUrl[$i-1])) {
            if ($possibleUrl[$i-1] == '') return false;
            if ($currentUrl == '' || !in_array($currentUrl, array($possibleUrl[$i - 1] . '/s', $possibleUrl[$i - 1] . '/a', $possibleUrl[$i - 1] . '/p'))) return false;
        }
        //

        $possibleUrl =[];

        for($i = 1; $i < count($parts); $i++ ){
            $url = '';
            for($j = 0; $j < $i; $j ++){
                $url .= $parts[$j].'/';
            }
            $url = trim($url, '/');
            array_push($possibleUrl, $url);
        }

        $realUrl = '';

        for ($i = 1; $i < count($possibleUrl); $i++) {
            $oUrlRewriteCollection = Mage::getModel('core/url_rewrite')
                ->getCollection()
                ->addFieldToFilter('request_path', $possibleUrl[$i]);
            if(count($oUrlRewriteCollection) == 0){
                $realUrl = $possibleUrl[$i-1];
                break;
            }
        }

        if($realUrl == '') return false;
        else{
            $params = str_replace($realUrl, '', $identifier);
            $params = explode('/', trim($params,'/'));
            $arrayParams = [];
            $returnArray = [];

            for($i = 0; $i < count($params)-1; $i+=2){
                $arrayParams[$params[$i]] = $params[$i+1];
            }

            if(count($params) > 1 && $params[0] == 'p'){
                $targetPath = Mage::getModel('core/url_rewrite')->load($realUrl, 'request_path');
                $targetPathArray = explode('/', $targetPath->getTargetPath());
                $productId = array_pop($targetPathArray);


                $request->setRequestUri('/' . $realUrl);
                $request->setPathInfo('/' . $realUrl);


                $request->setModuleName('catalog')
                    ->setControllerName('product')
                    ->setActionName('view')
                    ->setParam('id', (int)$productId);
                $request->setAlias(
                    Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
                    $realUrl
                );
                return true;
            }
            $hasStoneFilter = false;
            $hasAlloyFilter = false;
            foreach ($arrayParams as $key => $value) {
                if($key == 's') {
                    $attribute = Mage::getSingleton('eav/config')
                        ->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'stone'); // color is the attribute code here

                    if ($attribute->usesSource()) {
                        $options = $attribute->getSource()->getAllOptions(false);
                        foreach ($options as $option){
                            if($value == str_replace(' ', '', strtolower($option['label']))) {
                                $returnArray['stone'] = $option['value'];
                                break;
                            }
                        }
                    }
                }
                if($key == 'a') {
                    $attribute = Mage::getSingleton('eav/config')
                        ->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'alloy'); // color is the attribute code here

                    if ($attribute->usesSource()) {
                        $options = $attribute->getSource()->getAllOptions(false);
                        foreach ($options as $option){
                            if($value == str_replace(' ', '', strtolower($option['label']))) {
                                $returnArray['alloy'] = $option['value'];
                                break;
                            }
                        }
                    }
                }
            }

            $targetPath = Mage::getModel('core/url_rewrite')->load($realUrl, 'request_path');
            $targetPathArray = explode('/', $targetPath->getTargetPath());
            $categoryId = array_pop($targetPathArray);

            if(count($returnArray) > 0){
                $realUrl .= '?';
                foreach ($returnArray as $key => $value){
                    $realUrl .= $key.'='.$value.'&';
                }
                $realUrl = trim($realUrl, '&');
                $request->setRequestUri('/' . $realUrl);
                $request->setPathInfo('/' . $realUrl);


                $request->setModuleName('catalog')
                    ->setControllerName('category')
                    ->setActionName('view')
                    ->setParam('id', (int)$categoryId);
                $request->setAlias(
                    Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
                    $realUrl
                );
                return true;
            } else return false;
        }
    }
}
