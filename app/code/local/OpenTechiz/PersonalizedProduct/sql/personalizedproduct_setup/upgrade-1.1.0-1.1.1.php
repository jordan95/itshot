<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('personalized_product_option_alloy')} 
    ADD `gold_type`
    VARCHAR(45)
    NOT NULL
    AFTER `gold_id`;
    
    ALTER TABLE {$this->getTable('personalized_product_option_alloy')} 
    ADD `gold_color`
    VARCHAR(45)
    NOT NULL
    AFTER `gold_type`;
    
    ALTER TABLE {$this->getTable('personalized_product_option_alloy')}
    CHANGE `gold_id` `gold_id`
    INT(11)
    NULL
    COMMENT 'GoldId';
");

$installer->endSetup();