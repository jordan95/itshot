<?php

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable('tsht_product_listing_price')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Product Id')
    ->addColumn('price', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
        'nullable' => false,
    ), 'Listing Price');
$installer->getConnection()->createTable($table);

$installer->endSetup();