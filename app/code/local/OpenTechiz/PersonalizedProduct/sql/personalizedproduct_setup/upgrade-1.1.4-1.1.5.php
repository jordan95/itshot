<?php

$newSetName    = 'Personalized Product';
$originalSetId = 4;

/** @var Mage_Catalog_Model_Product_Attribute_Set_Api */
Mage::getModel('catalog/product_attribute_set_api')
    ->create($newSetName, $originalSetId);

$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$installer->addAttribute('catalog_product', 'num_of_view', array(
    'label'           => 'Number Of Angle View',
    'input'           => 'text',
    'type'            => 'int',
    'required'        => 1,
    'visible_on_front'=> 1,
    'filterable'      => 0,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => '',
));

$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$installer->addAttribute('catalog_product', 'price_ratio', array(
    'label'           => 'Price Ratio',
    'input'           => 'text',
    'type'            => 'decimal',
    'required'        => 1,
    'visible_on_front'=> 1,
    'filterable'      => 0,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => 'Alloy price = (current alloy) weight x alloy price x price ratio',
));

$installer->addAttribute('catalog_product', 'base_weight', array(
    'label'           => 'Metal Base Weight',
    'input'           => 'text',
    'type'            => 'decimal',
    'required'        => 1,
    'visible_on_front'=> 1,
    'filterable'      => 0,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => 'Use 18K Gold as base weight',
));

$installer->addAttribute('catalog_product', 'additional_fee', array(
    'label'           => 'Additional Fee',
    'input'           => 'text',
    'type'            => 'decimal',
    'required'        => 1,
    'visible_on_front'=> 1,
    'filterable'      => 0,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => 'Product price = alloy price + stone price + additional fee',
));

// Add new Attribute group
$groupName = 'Metal Weight';
$entityTypeId = $installer->getEntityTypeId('catalog_product');
$attributeSetId = Mage::getModel('eav/entity_attribute_set')->getCollection()
    ->addFieldToFilter('attribute_set_name', $newSetName)->getFirstItem()->getId();

$installer->addAttributeGroup($entityTypeId, $attributeSetId, $groupName, 1);
$attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, $groupName);

// Add existing attribute to group
$attributeId = $installer->getAttributeId($entityTypeId, 'price_ratio');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
$attributeId = $installer->getAttributeId($entityTypeId, 'base_weight');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
$attributeId = $installer->getAttributeId($entityTypeId, 'additional_fee');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);

$installer->addAttribute('catalog_product', 'default_stone', array(
    'label'                 => 'Default Center Stone',
    'input'                 => 'select',
    'type'                  => 'varchar',
    'required'              => 1,
    'visible_on_front'      => false,
    'filterable'            => 0,
    'filterable_in_search' => 0,
    'searchable'            => 0,
    'used_in_product_listing' => true,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_Material_Helper_Data::ALL_STONE_TYPE),
    'note'            => ''
));

$installer->addAttribute('catalog_product', 'stone', array(
    'label'                 => 'Stone',
    'input'                 => 'multiselect',
    'type'                  => 'text',
    'required'              => 1,
    'visible_on_front'      => false,
    'filterable'            => 1,
    'filterable_in_search' => 1,
    'searchable'            => 1,
    'used_in_product_listing' => true,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_Material_Helper_Data::ALL_STONE_TYPE),
    'note'            => ''
));

$installer->addAttribute('catalog_product', 'default_alloy', array(
    'label'                 => 'Default Metal',
    'input'                 => 'select',
    'type'                  => 'varchar',
    'required'              => 1,
    'visible_on_front'      => false,
    'filterable'            => 0,
    'filterable_in_search' => 0,
    'searchable'            => 0,
    'used_in_product_listing' => true,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_Material_Helper_Data::ALL_GOLD_TYPE),
    'note'            => ''
));

$installer->addAttribute('catalog_product', 'alloy', array(
    'label'                 => 'Metal',
    'input'                 => 'multiselect',
    'type'                  => 'text',
    'required'              => 1,
    'visible_on_front'      => false,
    'filterable'            => 1,
    'filterable_in_search' => 1,
    'searchable'            => 1,
    'used_in_product_listing' => true,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_Material_Helper_Data::ALL_GOLD_TYPE),
    'note'            => ''
));

$attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, 'General');
$attributeId = $installer->getAttributeId($entityTypeId, 'num_of_view');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
$attributeId = $installer->getAttributeId($entityTypeId, 'default_stone');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
$attributeId = $installer->getAttributeId($entityTypeId, 'stone');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
$attributeId = $installer->getAttributeId($entityTypeId, 'default_alloy');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
$attributeId = $installer->getAttributeId($entityTypeId, 'alloy');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);