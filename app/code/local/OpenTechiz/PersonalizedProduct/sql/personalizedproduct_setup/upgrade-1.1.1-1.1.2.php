<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('personalized_product_option_stone')} 
    ADD `stone_type`
    VARCHAR(45)
    NOT NULL
    AFTER `stone_id`;
    
    ALTER TABLE {$this->getTable('personalized_product_option_stone')}
    CHANGE `stone_id` `stone_id`
    INT(11)
    NULL
    COMMENT 'StoneId';
");

$installer->endSetup();