<?php

$installer = $this;

$installer->startSetup();

$installer->run("
UPDATE tsht_eav_attribute 
SET backend_model = 'eav/entity_attribute_backend_array' 
WHERE tsht_eav_attribute.attribute_code = 'stone'"
);

$installer->run("
UPDATE tsht_eav_attribute 
SET backend_model = 'eav/entity_attribute_backend_array' 
WHERE tsht_eav_attribute.attribute_code = 'alloy'"
);

$installer->endSetup();