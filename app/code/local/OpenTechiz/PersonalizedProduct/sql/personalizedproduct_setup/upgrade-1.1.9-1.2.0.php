<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE tsht_catalog_product_option_type_value
    ADD `is_show_in_frontend`
    integer
    not NULL default 1
    COMMENT 'show in frontend';
");
$installer->endSetup();
