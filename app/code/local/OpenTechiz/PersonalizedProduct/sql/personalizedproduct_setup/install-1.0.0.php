<?php

$installer = $this;

$installer->startSetup();
if(!$installer->tableExists('personalizedproduct/price_stone')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('personalizedproduct/price_stone'))
        ->addColumn('product_option_stone_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'StoneOptionId')
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'ProductId')
        ->addColumn('option_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'OptionId')
        ->addColumn('option_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'OptionName')
        ->addColumn('stone_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'StoneId')
        ->addColumn('quantity', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'QuantityStone')
        ->addColumn('stone_select_group', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'StoneSelectGroup');
    $installer->getConnection()->createTable($table);
}
if(!$installer->tableExists('personalizedproduct/price_alloy')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('personalizedproduct/price_alloy'))
        ->addColumn('product_option_alloy_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'StoneOptionId')
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'ProductId')
        ->addColumn('gold_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'GoldId')
        ->addColumn('weight', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
            'nullable' => false,
        ), 'AlloyWeight');
    $installer->getConnection()->createTable($table);
}
if(!$installer->tableExists('personalizedproduct/price_product')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('personalizedproduct/price_product'))
        ->addColumn('product_listing_price_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'StoneOptionId')
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'ProductId')
        ->addColumn('gold_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'Gold')
        ->addColumn('stone_select_group', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'StoneSelectGroup')
        ->addColumn('price', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
            'nullable' => false,
        ), 'ListingPrice');
    $installer->getConnection()->createTable($table);
}
$installer->endSetup();