<?php
$newSetName    = 'Personalized Product';

$attributeSetId = Mage::getModel('eav/entity_attribute_set')->getCollection()
    ->addFieldToFilter('attribute_set_name', $newSetName)->getFirstItem()->getId();

$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$installer->addAttribute('catalog_product', 'default_stone_quality', array(
    'label'                 => 'Default Center Stone Quality',
    'input'                 => 'select',
    'type'                  => 'varchar',
    'required'              => 1,
    'visible_on_front'      => false,
    'filterable'            => 0,
    'filterable_in_search' => 0,
    'searchable'            => 0,
    'used_in_product_listing' => false,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_QUALITY_ARRAY),
    'note'            => 'Default value for Center Stone Quality, choose none if default stone have no quality'
));

$installer->addAttribute('catalog_product', 'default_stone_2', array(
    'label'                 => 'Default Stone 2',
    'input'                 => 'select',
    'type'                  => 'varchar',
    'required'              => 0,
    'visible_on_front'      => false,
    'filterable'            => 0,
    'filterable_in_search' => 0,
    'searchable'            => 0,
    'used_in_product_listing' => false,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_Material_Helper_Data::ALL_STONE_TYPE),
    'note'            => 'Default value for option Stone 2'
));

$installer->addAttribute('catalog_product', 'default_stone_2_quality', array(
    'label'                 => 'Default Stone 2 Quality',
    'input'                 => 'select',
    'type'                  => 'varchar',
    'required'              => 0,
    'visible_on_front'      => false,
    'filterable'            => 0,
    'filterable_in_search' => 0,
    'searchable'            => 0,
    'used_in_product_listing' => false,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_QUALITY_ARRAY),
    'note'            => 'Default value for Stone 2 Quality'
));

$installer->addAttribute('catalog_product', 'default_stone_3', array(
    'label'                 => 'Default Stone 3',
    'input'                 => 'select',
    'type'                  => 'varchar',
    'required'              => 0,
    'visible_on_front'      => false,
    'filterable'            => 0,
    'filterable_in_search' => 0,
    'searchable'            => 0,
    'used_in_product_listing' => false,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_Material_Helper_Data::ALL_STONE_TYPE),
    'note'            => 'Default value for option Stone 3'
));

$installer->addAttribute('catalog_product', 'default_stone_3_quality', array(
    'label'                 => 'Default Stone 3 Quality',
    'input'                 => 'select',
    'type'                  => 'varchar',
    'required'              => 0,
    'visible_on_front'      => false,
    'filterable'            => 0,
    'filterable_in_search' => 0,
    'searchable'            => 0,
    'used_in_product_listing' => false,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_QUALITY_ARRAY),
    'note'            => 'Default value for Stone 3 Quality'
));

$installer->addAttribute('catalog_product', 'default_stone_4', array(
    'label'                 => 'Default Stone 4',
    'input'                 => 'select',
    'type'                  => 'varchar',
    'required'              => 0,
    'visible_on_front'      => false,
    'filterable'            => 0,
    'filterable_in_search' => 0,
    'searchable'            => 0,
    'used_in_product_listing' => false,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_Material_Helper_Data::ALL_STONE_TYPE),
    'note'            => 'Default value for option Stone 4'
));

$installer->addAttribute('catalog_product', 'default_stone_4_quality', array(
    'label'                 => 'Default Stone 4 Quality',
    'input'                 => 'select',
    'type'                  => 'varchar',
    'required'              => 0,
    'visible_on_front'      => false,
    'filterable'            => 0,
    'filterable_in_search' => 0,
    'searchable'            => 0,
    'used_in_product_listing' => false,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_QUALITY_ARRAY),
    'note'            => 'Default value for Stone 4 Quality'
));

$entityTypeId = $installer->getEntityTypeId('catalog_product');
$attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, 'General');
$attributeId = $installer->getAttributeId($entityTypeId, 'default_stone_quality');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
$attributeId = $installer->getAttributeId($entityTypeId, 'default_stone_2_quality');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
$attributeId = $installer->getAttributeId($entityTypeId, 'default_stone_3_quality');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
$attributeId = $installer->getAttributeId($entityTypeId, 'default_stone_4_quality');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
$attributeId = $installer->getAttributeId($entityTypeId, 'default_stone_2');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
$attributeId = $installer->getAttributeId($entityTypeId, 'default_stone_3');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
$attributeId = $installer->getAttributeId($entityTypeId, 'default_stone_4');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);