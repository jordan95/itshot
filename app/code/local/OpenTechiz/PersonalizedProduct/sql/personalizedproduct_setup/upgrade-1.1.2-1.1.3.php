<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('personalized_product_option_stone')} 
    DROP `stone_type`;
");

$installer->endSetup();