<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
// Remove Product Attribute
$installer->removeAttribute('catalog_product', 'num_of_view');
$installer->removeAttribute('catalog_product', 'price_ratio');
$installer->removeAttribute('catalog_product', 'base_weight');
$installer->removeAttribute('catalog_product', 'additional_fee');
$installer->removeAttribute('catalog_product', 'default_stone');
$installer->removeAttribute('catalog_product', 'stone');
$installer->removeAttribute('catalog_product', 'default_alloy');
$installer->removeAttribute('catalog_product', 'alloy');

$entityTypeId = $installer->getEntityTypeId('catalog_product');
$array = [95, 99, 100, 101, 102, 103, 104];
foreach ($array as $id) {
    $attributeSetId = $id;
    $installer->removeAttributeGroup($entityTypeId, $attributeSetId, 'Alloy Weight', 1);
}