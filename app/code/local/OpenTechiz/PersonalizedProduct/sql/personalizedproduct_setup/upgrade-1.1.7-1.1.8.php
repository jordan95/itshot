<?php

$installer = $this;

$installer->startSetup();
if(!$installer->tableExists('personalizedproduct/defaultData')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('personalizedproduct/defaultData'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'id')
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'product_id')
        ->addColumn('metal', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'default metal')
        ->addColumn('center_stone', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'default stone')
        ->addColumn('stone_2', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ), 'default stone')
        ->addColumn('stone_3', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ), 'default stone')
        ->addColumn('stone_4', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => true,
        ), 'default stone');
    $installer->getConnection()->createTable($table);
}
$installer->endSetup();