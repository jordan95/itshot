<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$installer->addAttribute('catalog_product', 'price_ratio', array(
'label'           => 'Price Ratio',
'input'           => 'text',
'type'            => 'decimal',
'required'        => 1,
'visible_on_front'=> 1,
'filterable'      => 0,
'searchable'      => 0,
'comparable'      => 0,
'user_defined'    => 1,
'is_configurable' => 0,
'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
'note'            => 'Alloy price = (current alloy) weight x alloy price x price ratio',
));

$installer->addAttribute('catalog_product', 'base_weight', array(
    'label'           => 'Alloy Base Weight',
    'input'           => 'text',
    'type'            => 'decimal',
    'required'        => 1,
    'visible_on_front'=> 1,
    'filterable'      => 0,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => 'Use 18K Gold as base weight',
));

$installer->addAttribute('catalog_product', 'additional_fee', array(
    'label'           => 'Additional Fee',
    'input'           => 'text',
    'type'            => 'decimal',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 0,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'note'            => 'Product price = alloy price + stone price + additional fee',
));

// Add new Attribute group
$groupName = 'Alloy Weight';
$entityTypeId = $installer->getEntityTypeId('catalog_product');
$array = [95, 99, 100, 101, 102, 103, 104];
foreach ($array as $id){
    $attributeSetId = $id;
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, $groupName, 1);
    $attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, $groupName);

// Add existing attribute to group
    $attributeId = $installer->getAttributeId($entityTypeId, 'price_ratio');
    $installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
    $attributeId = $installer->getAttributeId($entityTypeId, 'base_weight');
    $installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
    $attributeId = $installer->getAttributeId($entityTypeId, 'addtional_fee');
    $installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
}

$installer->endSetup();
?>