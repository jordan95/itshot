<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$installer->addAttribute('catalog_product', 'default_stone', array(
    'group'                 => 'General',
    'label'                 => 'Default Stone',
    'input'                 => 'select',
    'type'                  => 'varchar',
    'required'              => 1,
    'visible_on_front'      => false,
    'filterable'            => 0,
    'filterable_in_search' => 0,
    'searchable'            => 0,
    'used_in_product_listing' => true,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_Material_Helper_Data::ALL_STONE_TYPE),
    'note'            => ''
));

$installer->addAttribute('catalog_product', 'stone', array(
    'group'                 => 'General',
    'label'                 => 'Stone',
    'input'                 => 'multiselect',
    'type'                  => 'varchar',
    'required'              => 0,
    'visible_on_front'      => false,
    'filterable'            => 1,
    'filterable_in_search' => 1,
    'searchable'            => 1,
    'used_in_product_listing' => true,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_Material_Helper_Data::ALL_STONE_TYPE),
    'note'            => ''
));

$installer->addAttribute('catalog_product', 'default_alloy', array(
    'group'                 => 'General',
    'label'                 => 'Default Alloy',
    'input'                 => 'select',
    'type'                  => 'varchar',
    'required'              => 1,
    'visible_on_front'      => false,
    'filterable'            => 0,
    'filterable_in_search' => 0,
    'searchable'            => 0,
    'used_in_product_listing' => true,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_Material_Helper_Data::ALL_GOLD_TYPE),
    'note'            => ''
));

$installer->addAttribute('catalog_product', 'alloy', array(
    'group'                 => 'General',
    'label'                 => 'Alloy',
    'input'                 => 'multiselect',
    'type'                  => 'varchar',
    'required'              => 0,
    'visible_on_front'      => false,
    'filterable'            => 1,
    'filterable_in_search' => 1,
    'searchable'            => 1,
    'used_in_product_listing' => true,
    'visible_in_advanced_search' => false,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'option'          => array('values' => OpenTechiz_Material_Helper_Data::ALL_GOLD_TYPE),
    'note'            => ''
));

$installer->endSetup();
