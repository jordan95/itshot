<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addIndex(
    $installer->getTable('personalized_product_option_alloy'),
    $installer->getIdxName('personalized_product_option_alloy', array('product_id')),
    array('product_id'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
);

$installer->getConnection()->addIndex(
    $installer->getTable('personalized_product_option_stone'),
    $installer->getIdxName('personalized_product_option_stone', array('product_id')),
    array('product_id'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
);

$installer->getConnection()->addIndex(
    $installer->getTable('product_listing_price'),
    $installer->getIdxName('product_listing_price', array('product_id')),
    array('product_id'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
);

$installer->getConnection()->addIndex(
    $installer->getTable('personalized_product_listing_price'),
    $installer->getIdxName('personalized_product_listing_price', array('product_id')),
    array('product_id'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
);

$installer->endSetup();