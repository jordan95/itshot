<?php

class OpenTechiz_PersonalizedProduct_Model_Observer
{
    const DEFAULT_QUALITY = OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_QUALITY;
    const DEFAULT_STONE = OpenTechiz_PersonalizedProduct_Helper_Data::DEFAULT_STONE;
    protected $isNew = false;
    protected $priceChanged = false;

    public function addStoneAndGoldOption($observer){
        $product = $observer->getProduct();
        $productPostData = Mage::app()->getRequest()->getParam('product');
        if(is_array($productPostData) && array_key_exists('options', $productPostData)) {
            $optionList = Mage::app()->getRequest()->getParam('product')['options'];
            if ($optionList) {
                foreach ($optionList as $option) {

                    if (!$option['is_delete'] && $option['type'] == 'stone') {
                        //get sample data for stone option from existed design
                        $stoneSampleEntity = Mage::getModel('personalizedproduct/price_stone')->getCollection()
                            ->addFieldToFilter('option_id', $option['id'])->getFirstItem();
                        $stoneGroup = $stoneSampleEntity->getStoneSelectGroup();
                        $collectionSample = Mage::getModel('personalizedproduct/price_stone')->getCollection()->addFieldToFilter('stone_select_group', $stoneGroup)
                            ->join(array('stone' => 'opentechiz_material/stone'), 'stone.stone_id = main_table.stone_id', '*');
                        $data = [];
                        foreach ($collectionSample as $sample) {
                            $stoneData = [];
                            $stoneData['quantity'] = $sample->getQuantity();
                            $stoneData['shape'] = $sample->getShape();
                            $stoneData['diameter'] = $sample->getDiameter();
                            array_push($data, $stoneData);
                        }

                        //loop each option value to see if data exist
                        $collection = Mage::getModel('personalizedproduct/price_stone')->getCollection()->addFieldToFilter('option_id', $option['id'])
                            ->join(array('stone' => 'opentechiz_material/stone'), 'stone.stone_id = main_table.stone_id', '*');
                        foreach ($option['values'] as $value) {
                            $flag = false;
                            foreach ($collection as $stone) {
                                if (isset($value['sku']) && $value['sku'] == strtolower(str_replace(' ', '', trim($stone->getStoneType() . $stone->getQuality())))) {
                                    $flag = true;
                                }
                            }
                            if ($flag == false) {
                                foreach ($data as $stoneGroupInfo) {
                                    $stoneInstock = Mage::getModel('opentechiz_material/stone')->getCollection()->addFieldToFilter('stone_type', $value['title'])
                                        ->addFieldToFilter('shape', $stoneGroupInfo['shape'])->addFieldToFilter('diameter', $stoneGroupInfo['diameter'])
                                        ->getFirstItem();
                                    $stoneId = $stoneInstock->getId();
                                    //get new stone select group
                                    $currentStoneGroup = Mage::getModel('personalizedproduct/price_stone')->getCollection()->setOrder('stone_select_group', 'DESC')->getFirstItem()->getStoneSelectGroup();
                                    $newStoneGroup = (int)$currentStoneGroup + 1;

                                    //add new stone price entity
                                    $newData = [];
                                    $newData['stone_select_group'] = $newStoneGroup;
                                    $newData['stone_id'] = $stoneId;
                                    $newData['option_id'] = $option['id'];
                                    $newData['option_name'] = $option['title'];
                                    $newData['product_id'] = $product->getId();
                                    $newData['quantity'] = $stoneGroupInfo['quantity'];

                                    Mage::getModel('personalizedproduct/price_stone')->setData($newData)->save();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function getStoneFilter($productId){
        $stone_id = Mage::app()->getRequest()->getParams()['stone'];

        $attributeDetails = Mage::getSingleton("eav/config")
            ->getAttribute("catalog_product", 'stone');
        $optionValue = $attributeDetails->getSource()->getOptionText($stone_id);

        $optionValue = str_replace(' ', '', $optionValue);
        $optionValue = strtolower($optionValue);

        if(!$optionValue){
            $product = Mage::getModel('catalog/product')->load($productId);
            return $product->getAttributeText("default_stone");
        }
        return $optionValue;
    }

    public function getAlloyFilter($productId){
        $alloy_id = Mage::app()->getRequest()->getParams()['alloy'];

        $attributeDetails = Mage::getSingleton("eav/config")
            ->getAttribute("catalog_product", 'alloy');
        $optionValue = $attributeDetails->getSource()->getOptionText($alloy_id);

        $optionValue = str_replace(' ', '', $optionValue);
        $optionValue = strtolower($optionValue);

        if(!$optionValue){
            $product = Mage::getModel('catalog/product')->load($productId);
            return $product->getAttributeText("default_alloy");
        }

        return $optionValue;
    }

    public function getAlloyId($alloyCollection, $alloyName, $productId){
        foreach($alloyCollection as $alloy){
            $product_id = $alloy->getProductId();
            $type = $alloy->getGoldType();
            $name = $alloy->getGoldColor();
            $alloyFullName = $type.$name;
            $alloyFullName = str_replace(' ', '', $alloyFullName);
            $alloyFullName = strtolower($alloyFullName);
            if($alloyName == $alloyFullName && $product_id == $productId){
                $alloyId = $alloy->getGoldId();
                break;
            }
        }
        return $alloyId;
    }

    public function changeProductPageLayout($observer){
        $fullActionName = $observer->getEvent()->getAction()->getFullActionName();
        if ($fullActionName == 'catalog_product_view') {
            $productId = $observer->getEvent()->getAction()->getRequest()->getParam('id');
            $product = Mage::getModel('catalog/product')->load($productId);
            $sku = $product->getSku();
            if(Mage::helper('personalizedproduct')->isPersonalizedProduct($product)) {
                Mage::app()->getLayout()->getUpdate()->addHandle('MY_HANDLE_catalog_product_view_left');
            }elseif(strpos($sku, 'quotation') === false) {
                Mage::app()->getLayout()->getUpdate()->addHandle('MY_HANDLE_catalog_product_view_right');
            }
        }
    }

    public function checkIfProductIsNewOrHasPriceChange($observer){
        $product = $observer->getEvent()->getProduct();
        if($product->isObjectNew()){
            $this->isNew = true;
        }

        $newproduct = $observer->getProduct();
        $oldproductData = $observer->getEvent()->getDataObject()->getOrigData();

        if($oldproductData && (array_key_exists('price', $oldproductData) || array_key_exists('special_price', $oldproductData))) {
            if (($oldproductData['price'] != $newproduct->getData('price')) || ($oldproductData['special_price'] != $newproduct->getData('special_price'))) {
                $this->priceChanged = true;
            }
        }
    }

    public function runUpdateListingPriceOnProductSave($observer){
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $readConnection = $resource->getConnection('core_read');
        $product = $observer->getProduct();
        $productId = $product->getId();
        if(!array_key_exists('special_price', $product->getData())){
            return;
        }
        $price = $product->getSpecialPrice();
        if(!$price){
            $price = $product->getPrice();
        }
        if(!$price){
             $price = 0;
        }

        $query = "SELECT * FROM tsht_product_listing_price where product_id = ".$productId;
        $results = $readConnection->fetchAll($query);
        if(count($results) > 0) {
            $query = "UPDATE tsht_product_listing_price set price = " . $price . " WHERE product_id = " . $productId;
            $writeConnection->query($query);
        }else{
            $query = "INSERT INTO tsht_product_listing_price (product_id, price) 
                          values (".$productId.",".$price.")";
            $writeConnection->query($query);
        }
    }

    public function saveHiddenProductOptionValue($observer){
        $request = $observer->getEvent()->getRequest();
        if(isset($request->getParams()['product'])){

            $productData = $request->getParams()['product'];
            if(isset($productData['options'])) {
                foreach ($productData['options'] as $optionKey => $option) {
                    if ($option['type'] == 'drop_down') {
                        foreach ($option['values'] as $valueKey => $value) {
                            if (isset($value['show_in_frontend'])) {
                                $productData['options'][$optionKey]['values'][$valueKey]['is_show_in_frontend'] = 1;
                                $is_show_in_frontend = 1;
                            } else {
                                $productData['options'][$optionKey]['values'][$valueKey]['is_show_in_frontend'] = 0;
                                $is_show_in_frontend = 0;
                            }


                            $resource = Mage::getSingleton('core/resource');
                            $writeConnection = $resource->getConnection('core_write');
                            $table = $resource->getTableName('catalog/product_option_type_value');

                            $query = "UPDATE {$table} SET is_show_in_frontend = {$is_show_in_frontend} WHERE option_type_id = "
                                . (int)$valueKey;

                            $writeConnection->query($query);
                        }
                    }
                }
            }
            $request->setParam('product', $productData);
        }
    }
}