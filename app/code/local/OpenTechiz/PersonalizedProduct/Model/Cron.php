<?php
class OpenTechiz_PersonalizedProduct_Model_Cron
{
    const MAIN_STONE_OPTION_NAME = 'Center Stone';
    const STONE_2_OPTION_NAME = 'Stone 2';
    const STONE_3_OPTION_NAME = 'Stone 3';
    const STONE_4_OPTION_NAME = 'Stone 4';
    protected $stonePriceArray = [];
    protected $alloyPriceArray = [];

    public function updateListingPrice()
    {
        $stoneCollection = Mage::getModel('personalizedproduct/price_stone')->getCollection();
        $listingModel = Mage::getModel('personalizedproduct/price_product');
        $productIds = $stoneCollection->getColumnValues('product_id');
        $productIds = array_unique($productIds, SORT_REGULAR);
        $products = Mage::getModel('catalog/product')->getCollection()
            ->addFieldToFilter('entity_id', array('in' => $productIds))
            ->addAttributeToSelect('base_weight')
            ->addAttributeToSelect('price_ratio')
            ->addAttributeToSelect('additional_fee')
            ->addAttributeToSelect('default_stone_2')
            ->addAttributeToSelect('default_stone_3')
            ->addAttributeToSelect('default_stone_4')
            ->addAttributeToSelect('default_stone_2_quality')
            ->addAttributeToSelect('default_stone_3_quality')
            ->addAttributeToSelect('default_stone_4_quality');

        $stonePriceCollection = Mage::getModel('opentechiz_material/stone')->getCollection();
        foreach ($stonePriceCollection as $stonePrice){
            $this->stonePriceArray[$stonePrice->getId()] = $stonePrice;
        }
        $alloyPriceCollection = Mage::getModel('opentechiz_material/gold')->getCollection();
        foreach ($alloyPriceCollection as $alloyPrice){
            $this->alloyPriceArray[$alloyPrice->getId()] = $alloyPrice;
        }

        foreach ($products as $product){
            $productId = $product->getId();
            $stoneCollectionEachProduct = Mage::getModel('personalizedproduct/price_stone')
                ->getCollection()
                ->addFieldToFilter('product_id', $productId);

            $mainStoneGroups = [];
            foreach ($stoneCollectionEachProduct as $stone) {
                if ($stone->getOptionName() == self::MAIN_STONE_OPTION_NAME) {
                    $stoneId = $stone->getStoneId();
                    $stoneData = $this->stonePriceArray[$stoneId];

                    $price = $stoneData->getPrice();
                    $quantity = $stone->getQuantity();

                    if (array_key_exists($stone->getStoneSelectGroup(), $mainStoneGroups)) {
                        $mainStoneGroups[$stone->getStoneSelectGroup()] += $price * $quantity;
                    } else {
                        $mainStoneGroups[$stone->getStoneSelectGroup()] = $price * $quantity;
                    }
                }
            }
            $alloyCollection = Mage::getModel('personalizedproduct/price_alloy')->getCollection()->addFieldToFilter('product_id', $productId);

            foreach ($mainStoneGroups as $key => $price) {
                foreach ($alloyCollection as $alloyPriceEntity) {
                    $totalPrice = 0;
                    $goldId = $alloyPriceEntity->getGoldId();
                    $alloyPrice = $this->alloyPriceArray[$goldId];

                    $alloyType = trim(strtolower($alloyPrice->getGoldType()));
                    $dataHelper = Mage::helper('personalizedproduct/data');

                    $alloyWeight = ($dataHelper::ALLOY_DENSITY[$alloyType]/$dataHelper::ALLOY_DENSITY['18k']) * $product->getData('base_weight');

                    $totalPrice += $alloyPrice->getPrice() * $alloyWeight * $product->getData('price_ratio');
                    $totalPrice += $price;
                    $totalPrice += $this->getSideStonePrice($product);
                    $totalPrice += $product->getData('additional_fee');
                    $data = [];
                    $data['product_id'] = $productId;
                    $data['gold_id'] = $goldId;
                    $data['stone_select_group'] = $key;
                    $data['price'] = $totalPrice;

                    $exist = $listingModel->getCollection()->addFieldToFilter('product_id', $productId)
                        ->addFieldToFilter('gold_id', $goldId)->addFieldToFilter('stone_select_group', $key)->getFirstItem();
                    if ($exist->getId()) {
                        $exist->addData($data)->setId($exist->getId())->save();
                    } else {
                        $listingModel->setData($data);
                        $listingModel->save();
                    }
                }
            }
        }
        Mage::dispatchEvent('material_update_price_cron_run_after');
    }

    public function getSideStonePrice($product){
        $productId = $product->getId();
        $stoneCollection = Mage::getModel('personalizedproduct/price_stone')->getCollection()->addFieldToFilter('product_id', $productId)->addFieldToFilter('option_name', ['neq' => self::MAIN_STONE_OPTION_NAME]);

        $defaultStone2 = $product->getAttributeText("default_stone_2");
        $defaultStone3 = $product->getAttributeText("default_stone_3");
        $defaultStone4 = $product->getAttributeText("default_stone_4");
        $defaultStone2Quality = $product->getAttributeText("default_stone_2_quality");
        if($defaultStone2Quality == '' || $defaultStone2Quality == 'none') $defaultStone2Quality = null;
        $defaultStone3Quality = $product->getAttributeText("default_stone_3_quality");
        if($defaultStone3Quality == '' || $defaultStone3Quality == 'none') $defaultStone3Quality = null;
        $defaultStone4Quality = $product->getAttributeText("default_stone_4_quality");
        if($defaultStone4Quality == '' || $defaultStone4Quality == 'none') $defaultStone4Quality = null;

        $totalPrice = 0;
        foreach ($stoneCollection as $stone){
            $data = $this->stonePriceArray[$stone->getStoneId()];
            $quantity = $stone->getQuantity();

            $stoneType = trim(str_replace(' ', '', strtolower($data->getStoneType())));
            $stoneQuality = trim(str_replace(' ', '', strtolower($data->getQuality())));

            $optionName = $stone->getOptionName();
            if($optionName == self::STONE_2_OPTION_NAME) {
                if (($stoneType == trim(str_replace(' ', '', strtolower($defaultStone2)))) && ($stoneQuality == trim(str_replace(' ', '', strtolower($defaultStone2Quality))))) {
                    $totalPrice += ($data->getPrice() * $quantity);
                }
            }
            else if($optionName == self::STONE_3_OPTION_NAME) {
                if (($stoneType == trim(str_replace(' ', '', strtolower($defaultStone3)))) && ($stoneQuality == trim(str_replace(' ', '', strtolower($defaultStone3Quality))))) {
                    $totalPrice += ($data->getPrice() * $quantity);
                }
            }
            else if($optionName == self::STONE_4_OPTION_NAME) {
                if (($stoneType == trim(str_replace(' ', '', strtolower($defaultStone4)))) && ($stoneQuality == trim(str_replace(' ', '', strtolower($defaultStone4Quality))))) {
                    $totalPrice += ($data->getPrice() * $quantity);
                }
            }
        }
        return $totalPrice;
    }

    public function addNormalProductListingPrice(){
        $listingModel = Mage::getModel('personalizedproduct/price_product');
        $personalizedProductIds = $listingModel->getCollection()->getColumnValues('product_id');
        $personalizedProductIds = array_unique($personalizedProductIds, SORT_REGULAR);
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        //create temp table to store data

        $query = "CREATE TEMPORARY TABLE temp_product_listing_price
        (product_id int(10) NOT NULL,
          price float not null
        )";
        $writeConnection->query($query);

        foreach ($personalizedProductIds as $personalizedProductId){
            $price = Mage::helper('personalizedproduct/personalizedPriceHelper')->getDefaultPersonalizedPrice($personalizedProductId);

            $query = "INSERT INTO temp_product_listing_price (product_id, price) VALUES (".$personalizedProductId.",".$price.")";
            $writeConnection->query($query);
        }

        if(count($personalizedProductIds) > 0) {
            $personalizedProductIds = '(' . implode(',', $personalizedProductIds) . ')';
        }
        $flatTables = [];

        for ($i = 1; $i < 10; $i++){
            if(Mage::getSingleton('core/resource')
                ->getConnection('core_write')
                ->isTableExists('tsht_catalog_product_flat_'.$i)){
                array_push($flatTables, $i);
            } else{
                break;
            }
        }

        foreach ($flatTables as $flatTable){
            $productFlatTable = "tsht_catalog_product_flat_".$flatTable;
            if(count($personalizedProductIds) > 0) {
                $query = "INSERT INTO temp_product_listing_price (product_id, price)
                      SELECT entity_id, special_price
                      FROM " . $productFlatTable . "
                      WHERE entity_id not in " . $personalizedProductIds;
            }else{
                $query = "INSERT INTO temp_product_listing_price (product_id, price)
                      SELECT entity_id, special_price
                      FROM " . $productFlatTable;
            }
            $writeConnection->query($query);

            //set price if special_price is null
            $query = "UPDATE temp_product_listing_price as temp join ".$productFlatTable." as flat
                      on temp.product_id = flat.entity_id
                      SET temp.price = flat.price where flat.special_price is null";
            $writeConnection->query($query);
        }

        //truncate original data

        $query = "TRUNCATE TABLE tsht_product_listing_price";
        $writeConnection->query($query);

        //copy data from temp table to original table

        $query = "INSERT INTO tsht_product_listing_price
                  SELECT * FROM temp_product_listing_price";
        $writeConnection->query($query);

        //delete temp table

        $query = "DROP TEMPORARY TABLE temp_product_listing_price";
        $writeConnection->query($query);
    }

    public function populateDefaultPersonalizedProductOption(){
        $attrSetName = OpenTechiz_PersonalizedProduct_Helper_Data::ATTRIBUTE_SET_NAME;
        $attributeSetId = Mage::getModel('eav/entity_attribute_set')
            ->load($attrSetName, 'attribute_set_name')
            ->getAttributeSetId();

        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addFieldToFilter('attribute_set_id', $attributeSetId)
            ->addAttributeToSelect('default_stone')
            ->addAttributeToSelect('default_stone_quality')
            ->addAttributeToSelect('default_alloy')
            ->addAttributeToSelect('default_stone_2')
            ->addAttributeToSelect('default_stone_2_quality')
            ->addAttributeToSelect('default_stone_3')
            ->addAttributeToSelect('default_stone_3_quality')
            ->addAttributeToSelect('default_stone_4')
            ->addAttributeToSelect('default_stone_4_quality')
            ;
        $defaultModel = Mage::getModel('personalizedproduct/defaultData');

        foreach ($collection as $product){
            $centerStone = '';
            $stone2 = '';
            $stone3 = '';
            $stone4 = '';
            $metal = '';
            if($product->getData('default_stone')){
                $centerStone .= $product->getAttributeText('default_stone');
            }
            if($product->getData('default_stone_quality')){
                if($product->getAttributeText('default_stone_quality') != 'none'){
                    $centerStone .= $product->getAttributeText('default_stone_quality');
                }
            }
            if($product->getData('default_stone')){
                $stone2 .= $product->getAttributeText('default_stone_2');
            }
            if($product->getData('default_stone_quality')){
                if($product->getAttributeText('default_stone_quality') != 'none') {
                    $stone2 .= $product->getAttributeText('default_stone_2_quality');
                }
            }
            if($product->getData('default_stone')){
                $stone3 .= $product->getAttributeText('default_stone_3');
            }
            if($product->getData('default_stone_quality')){
                if($product->getAttributeText('default_stone_quality') != 'none') {
                    $stone3 .= $product->getAttributeText('default_stone_3_quality');
                }
            }
            if($product->getData('default_stone')){
                $stone4 .= $product->getAttributeText('default_stone_4');
            }
            if($product->getData('default_stone_quality')){
                if($product->getAttributeText('default_stone_quality') != 'none') {
                    $stone4 .= $product->getAttributeText('default_stone_4_quality');
                }
            }
            if($product->getData('default_alloy')){
                $metal .= $product->getAttributeText('default_alloy');
            }

            $model = $defaultModel->load($product->getId(), 'product_id');
            if($model->getId()){
                $data= [];
                $data['center_stone'] = $centerStone;
                $data['stone_2'] = $stone2;
                $data['stone_3'] = $stone3;
                $data['stone_4'] = $stone4;
                $data['metal'] = $metal;
                $model->addData($data)->setId($model->getId())->save();
            }else{
                $defaultModel->setProductId($product->getId())->setCenterStone($centerStone)->setStone2($stone2)->setStone3($stone3)
                    ->setStone4($stone4)->setMetal($metal)->save();
            }
        }
    }
}