<?php

class OpenTechiz_personalizedProduct_Model_Resource_Price_Alloy extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('personalizedproduct/price_alloy', 'product_option_alloy_id');
    }
}