<?php

class OpenTechiz_personalizedProduct_Model_Resource_Price_Stone extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('personalizedproduct/price_stone', 'product_option_stone_id');
    }
}