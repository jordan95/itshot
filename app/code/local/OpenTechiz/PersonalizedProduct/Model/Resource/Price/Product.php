<?php

class OpenTechiz_personalizedProduct_Model_Resource_Price_Product extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('personalizedproduct/price_product', 'product_listing_price_id');
    }
}