<?php
class OpenTechiz_PersonalizedProduct_Model_Resource_Price_Stone_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('personalizedproduct/price_stone');
    }
}