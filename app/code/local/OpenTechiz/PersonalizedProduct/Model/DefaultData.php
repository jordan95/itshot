<?php

class OpenTechiz_PersonalizedProduct_Model_DefaultData extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('personalizedproduct/defaultData');
    }
}