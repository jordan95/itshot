<?php

class OpenTechiz_PersonalizedProduct_Model_Price_Stone extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('personalizedproduct/price_stone');
    }
}