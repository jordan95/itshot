<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog product link model
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class OpenTechiz_PersonalizedProduct_Model_Rewrite_Catalog_Product_Image extends Mage_Catalog_Model_Product_Image
{
    const POSTION_TOP     = "top";
    const POSITION_BOTTOM = "bottom";
    const POSITION_CENTER = "center";

    // change default quality to 100, old = 90
    protected $_quality = 90;

    /**
     * Crop position from top
     *
     * @var float
     */
    protected $_topRate = 0.5;

    /**
     * Crop position from bootom
     *
     * @var float
     */
    protected $_bottomRate = 0.5;

    /**
     * Adaptive Resize
     *
     * @return Bolevar_AdaptiveResize_Model_Catalog_Product_Image
     */
    public function adaptiveResize()
    {
        if (is_null($this->getWidth())) {
            return $this;
        }

        if (is_null($this->getHeight())) {
            $this->setHeight($this->getWidth());
        }

        $processor = $this->getImageProcessor();

        $currentRatio = $processor->getOriginalWidth() / $processor->getOriginalHeight();
        $targetRatio = $this->getWidth() / $this->getHeight();

        if ($targetRatio > $currentRatio) {
            $processor->resize($this->getWidth(), null);
        } else {
            $processor->resize(null, $this->getHeight());
        }

        $diffWidth  = $processor->getOriginalWidth() - $this->getWidth();
        $diffHeight = $processor->getOriginalHeight() - $this->getHeight();

        $processor->crop(
            floor($diffHeight * $this->_topRate),
            floor($diffWidth / 2),
            ceil($diffWidth / 2),
            ceil($diffHeight * $this->_bottomRate)
        );

        return $this;
    }

    /**
     * Set crop position
     *
     * @param string $position top, bottom or center
     *
     * @return Bolevar_AdaptiveResize_Model_Catalog_Product_Image
     */
    public function setCropPosition($position)
    {
        switch ($position) {
            case self::POSTION_TOP:
                $this->_topRate    = 0;
                $this->_bottomRate = 1;
                break;
            case self::POSITION_BOTTOM:
                $this->_topRate    = 1;
                $this->_bottomRate = 0;
                break;
            default:
                $this->_topRate    = 0.5;
                $this->_bottomRate = 0.5;
        }
        return $this;
    }


//    public function setBaseFile($file)
//    {
//        if(!$_product = $this->getProduct()) {
//
//            $baseDir = Mage::getSingleton('catalog/product_media_config')->getBaseMediaPath();
//
//            $this->_baseFile = $baseDir . $file;
//
//            $this->_newFile = $baseDir . $file; // the $file contains heading slash
//
//            return $this;
//        }
//
//        if(!$imgps = Mage::app()->getRequest()->getParam("imgps"))
//            $imgps = 1;
//        if($_product->getImagePosition())
//            $imgps = $_product->getImagePosition();
//        $this->_isBaseFilePlaceholder = false;
//        $setId = $_product->getAttributeSetId();
//        $pearl = $_product->getPearlSku();
//        $accentcolor = strtolower($_product->getAccentcolorSku());
//        $folderSet = Mage::getBaseDir("media") . DS . "product" . DS . "set" . $setId . DS;
//        if (($file) && (0 !== strpos($file, "/", 0))) {
//            $file = "/" . $file;
//        }
//        $sku = strtolower($_product->getSkuImage());
//        $baseSku = str_replace(array("-men","-women"), "", $sku);
//        $baseDir = Mage::getSingleton("catalog/product_media_config")->getBaseMediaPath();
//        $attributeSetCustoms = Mage::getStoreConfig("checkout/cart/attributsetcustom");
//        $attributeSetCustoms = str_replace(" ","",$attributeSetCustoms);
//        $attributesSet = explode(",", $attributeSetCustoms);
//        if($baseSku && $_product->getAlloySku() && $this->cdnMediaExists($folderSet. $baseSku . DS . $sku . '_main.jpg'))
//        {
//            $file = $baseSku . DS . $sku . '_main.jpg';
//            $baseDir = $folderSet;
//        }
//        elseif($baseSku && $_product->getAlloySku() && $this->cdnMediaExists(Mage::getBaseDir("media") . DS . "productlist" . DS . $sku ."_main.jpg") && in_array($setId, $attributesSet)){
//            $file = $sku ."_main.jpg";
//            $baseDir = Mage::getBaseDir("media") . DS . "productlist" . DS;
//        }
//        if($baseSku && !$_product->getMoreImage()) {
//            if($_product->getDiamondSku() && $_product->getAlloySku())
//            {
//                $diamond = $this->_getStringSplit($_product->getDiamondSku());
//                $alloy = $this->_getStringSplit($_product->getAlloySku());
//                $names = array($alloy);
//                if($accentcolor)
//                    $names[] = $accentcolor;
//                if($pearl)
//                    $names[] = $pearl;
//                $names[] = $diamond;
//                $image = implode('_', $names);
//                $pearlName = $alloy . "_" . $pearl . "_" . $diamond;
//                if($this->cdnMediaExists($folderSet . $sku . DS . $image . "_$imgps.jpg")){
//                    $file = $sku . DS . $image ."_$imgps.jpg";
//                    $baseDir = $folderSet;
//                }elseif($this->cdnMediaExists(Mage::getBaseDir("media") . DS . "productlist" . DS . $sku ."_" . $image . ".jpg")){
//                    $file = $sku ."_". $image .".jpg";
//                    $baseDir = Mage::getBaseDir("media") . DS . "productlist" . DS;
//                }elseif($pearl && $this->cdnMediaExists($folderSet . $sku . DS . $pearlName . "_$imgps.jpg"))
//                {
//                    $file = $sku . DS . $pearlName . "_$imgps.jpg";
//                    $baseDir = $folderSet;
//                }
//            }elseif($_product->getAlloySku())
//            {
//                $alloy = $this->_getStringSplit($_product->getAlloySku());
//                $names = array($alloy);
//                $dirTrauring = Mage::getBaseDir("media") . DS . "trauring" . DS . "alloycolour" . DS ;
//                $dirNew = Mage::getBaseDir("media") . DS . "productlist" . DS;
//                if($accentcolor)
//                    $names[] = $accentcolor;
//                if($profile = strtolower($_product->getProfileSku()))
//                    $names[] = $profile;
//                if($surface = strtolower($_product->getSurfaceSku()))
//                    $names[] = $surface;
//                if($width = strtolower($_product->getOpwidthSku()))
//                    $names[] = $width;
//                elseif($width = strtolower($_product->getWidthSku()))
//                    $names[] = $width;
//                if($pearl)
//                    $names[] = $pearl;
//                // fix image name of product diamonds.
//                if($_product->getAttributeSetId() == 55)
//                {
//                    $names[] = 'diamond';
//                }
//                $pearlName = $alloy . "_" . $pearl . "_diamond";
//                $alloy = implode('_', $names);
//                if(preg_match("/-women/", $sku) && $this->cdnMediaExists($folderSet . $baseSku . DS . $alloy . "_2.jpg")){
//                    $file = $baseSku . DS . $alloy ."_2.jpg";
//                    $baseDir = $folderSet;
//                }
//                elseif(preg_match("/-men/", $sku) && $this->cdnMediaExists($folderSet . $baseSku . DS . $alloy . "_3.jpg")){
//                    $file = $baseSku . DS . $alloy ."_3.jpg";
//                    $baseDir = $folderSet;
//                }elseif($this->cdnMediaExists($folderSet . $sku . DS . $alloy . "_$imgps.jpg")){
//                    $file = $sku . DS . $alloy ."_$imgps.jpg";
//                    $baseDir = $folderSet;
//                }elseif($this->cdnMediaExists($folderSet . $baseSku . DS . $alloy . "_diamond_$imgps.jpg")){
//                    $file = $baseSku . DS . $alloy ."_diamond_$imgps.jpg";
//                    $baseDir = $folderSet;
//                }elseif($this->cdnMediaExists($folderSet . $baseSku . DS . $alloy . "_1.jpg")){
//                    $file = $baseSku . DS . $alloy ."_1.jpg";
//                    $baseDir = $folderSet;
//                }elseif($this->cdnMediaExists(Mage::getBaseDir("media") . DS . "productlist" . DS . $sku ."_" . $alloy . ".jpg")){
//                    $file = $sku ."_". $alloy .".jpg";
//                    $baseDir = Mage::getBaseDir("media") . DS . "productlist" . DS;
//                }elseif($this->cdnMediaExists(Mage::getBaseDir("media") . DS . "productlist" . DS . $baseSku ."_" . $alloy . ".jpg")){
//                    $file = $baseSku ."_". $alloy .".jpg";
//                    $baseDir = Mage::getBaseDir("media") . DS . "productlist" . DS;
//                }elseif($pearl && $this->cdnMediaExists($folderSet . $sku . DS . $pearlName . "_$imgps.jpg"))
//                {
//                    $file = $sku . DS . $pearlName . "_$imgps.jpg";
//                    $baseDir = $folderSet;
//                };
//            }
//        }elseif($baseSku && ($moreImg = $_product->getMoreImage()))
//        {
//            if(is_array($moreImg))
//            {
//                if($pearl && isset($moreImg["btw"]))
//                {
//                    $moreImg["btw"] = $pearl;
//                }
//                $moreImg = array_filter($moreImg);
//                $moreImg = strtolower(implode("_", $moreImg));
//                $moreImg = $sku . DS . $moreImg . ".jpg";
//            }
//            $moreImg = str_replace($sku . "_", $sku . DS, $moreImg);
//            $moreImg = str_replace(".jpg", "_$imgps.jpg", $moreImg);
//            $womenImg = str_replace($sku, $baseSku, $moreImg);
//            $womenImg = str_replace("_1.jpg", "_2.jpg", $womenImg);
//            $menImg = str_replace("_2.jpg", "_3.jpg", $womenImg);
//            if(preg_match("/-women/", $sku) && $this->cdnMediaExists($folderSet . $womenImg)){
//                $file = $womenImg;
//                $baseDir = $folderSet;
//            }elseif(preg_match("/-men/", $sku) && $this->cdnMediaExists($folderSet . $menImg)){
//                $file = $menImg;
//                $baseDir = $folderSet;
//            }
//            elseif($this->cdnMediaExists($folderSet . $moreImg)){
//                $file = $moreImg;
//                $baseDir = $folderSet;
//            }elseif($this->cdnMediaExists(Mage::getBaseDir("media") . DS . "productlist" . DS . $moreImg)){
//                $file = $moreImg;
//                $baseDir = Mage::getBaseDir("media") . DS . "productlist" . DS;
//            }
//        };
//
//
//        if ("/no_selection" == $file) {
//            $file = null;
//        }
//
//        if (!$file) {
//            // check if placeholder defined in config
//            $isConfigPlaceholder = Mage::getStoreConfig("catalog/placeholder/{$this->getDestinationSubdir()}_placeholder");
//            $configPlaceholder   = "/placeholder/" . $isConfigPlaceholder;
//            if ($isConfigPlaceholder && $this->_fileExists($baseDir . $configPlaceholder)) {
//                $file = $configPlaceholder;
//            }
//            else {
//                // replace file with skin or default skin placeholder
//                $skinBaseDir     = Mage::getDesign()->getSkinBaseDir();
//                $skinPlaceholder = "/images/catalog/product/placeholder/{$this->getDestinationSubdir()}.jpg";
//                $file = $skinPlaceholder;
//                if (file_exists($skinBaseDir . $file)) {
//                    $baseDir = $skinBaseDir;
//                }
//                else {
//                    $baseDir = Mage::getDesign()->getSkinBaseDir(array("_theme" => "default"));
//                    if (!file_exists($baseDir . $file)) {
//                        $baseDir = Mage::getDesign()->getSkinBaseDir(array("_theme" => "default", "_package" => "base"));
//                    }
//                }
//            }
//            $this->_isBaseFilePlaceholder = true;
//        }
//        $baseFile = $baseDir . $file;
//
//        if ((!$file) || (!$this->cdnMediaExists($baseFile))) {
//            throw new Exception(Mage::helper("catalog")->__("Image file was not found."));
//        }
//
//        $this->_baseFile = $baseFile;
//
//        $this->_newFile = $baseDir . $file; // the $file contains heading slash
//
////        var_dump($baseFile);exit;
//
//        return $this;
//    }
    protected function _getStringSplit($string, $char = "-", $position = 0)
    {
        $string = strtolower($string);
        $string = explode($char, $string);
        $string = $string[$position];
        return $string;
    }
    public function checkIsBaseFilePlaceholder()
    {
        return $this->_isBaseFilePlaceholder;
    }
    public function getUrl()
    {
        $baseDir = Mage::getBaseDir('media');
        $path = str_replace($baseDir . DS, "", $this->_newFile);
        if($url = Mage::helper('personalizedproduct/image')->getProductImageUrlTypeLayer($this->getProduct(), $this->getWidth(), $this->getHeight()))
        {
            return $url;
        }
        return Mage::getBaseUrl("media"). str_replace(DS, '/', $path);
    }

    function cdnMediaExists($file_path) {
        return true;
        $url = Mage::getBaseDir("media");
        $ch = @curl_init($url);
        @curl_setopt($ch, CURLOPT_HEADER, TRUE);
        @curl_setopt($ch, CURLOPT_NOBODY, TRUE);
        @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $status = array();
        preg_match('/HTTP\/.* ([0-9]+) .*/', @curl_exec($ch) , $status);
        return ($status[1] == 200);
    }
}
