<?php
class OpenTechiz_PersonalizedProduct_Model_Rewrite_Catalog_Layer extends Mage_Catalog_Model_Layer{

    /**
     * Initialize product collection
     *
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection
     * @return OpenTechiz_PersonalizedProduct_Model_Rewrite_Catalog_Layer
     */
    public function prepareProductCollection($collection)
    {
        $collection
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite($this->getCurrentCategory()->getId());
        $collection->getSelect()->join(array('price' => 'tsht_product_listing_price' ), 'e.entity_id = price.product_id',
            array(
                'price' => 'price.price', //this was commented out before for some error in search
                'final_price' => 'price.price'
            ));

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        return $this;
    }


}