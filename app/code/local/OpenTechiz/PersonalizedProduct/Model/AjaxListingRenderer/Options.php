<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Ajaxcartpro
 * @version    3.2.11
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class OpenTechiz_PersonalizedProduct_Model_AjaxListingRenderer_Options extends AW_Ajaxcartpro_Model_Renderer_Options
{
    protected function _customBlock($block, $layout)
    {
        $block->setTemplate('opentechiz/personalizedproduct/ajaxcartpro/options.phtml');
        $price = $layout->getBlock('product.clone_prices');
        //remove catalog_msrp
        $block->append($price, 'product_price');
        return $block;
    }
}