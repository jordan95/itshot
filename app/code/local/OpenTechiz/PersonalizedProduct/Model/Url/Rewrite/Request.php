<?php
class OpenTechiz_PersonalizedProduct_Model_Url_Rewrite_Request extends Mage_Core_Model_Url_Rewrite_Request
{
    public function rewrite()
    {
        if (!Mage::isInstalled()) {
            return false;
        }

        if (!$this->_request->isStraight()) {
            Varien_Profiler::start('mage::dispatch::db_url_rewrite');
            $this->_rewriteDb();
            Varien_Profiler::stop('mage::dispatch::db_url_rewrite');
        }

        Varien_Profiler::start('mage::dispatch::config_url_rewrite');
        $this->_rewriteConfig();
        Varien_Profiler::stop('mage::dispatch::config_url_rewrite');

        //check if requested url is url_rewrite + /string
        //if true, ignore /string
        $base_url = $this->_request->getOriginalPathInfo();
        $base_url = trim($base_url, '/');
        $base_url = trim($base_url, '#');

        //stop illegal routing
        $possibleUrl =[];
        $parts = explode('/', $base_url);
        for($i = 0; $i < count($parts); $i++ ){
            $url = '';
            for($j = 0; $j <= $i; $j ++){
                $url .= $parts[$j].'/';
            }
            $url = trim($url, '/');
            array_push($possibleUrl, $url);
        }

        $currentUrl = '';
        for ($i = 1; $i < count($possibleUrl); $i++) {
            $oUrlRewriteCollection = Mage::getModel('core/url_rewrite')
                ->getCollection()
                ->addFieldToFilter('request_path', $possibleUrl[$i]);
            if(count($oUrlRewriteCollection) == 0){
                $currentUrl = $possibleUrl[$i]; //not in rewrite collection
                break;
            }
        }
        //check last valid url
        if(isset($possibleUrl[$i-1])) {
            if ($possibleUrl[$i - 1] == '') return false;
            if ($currentUrl == '' || !in_array($currentUrl, array($possibleUrl[$i - 1] . '/s', $possibleUrl[$i - 1] . '/a', $possibleUrl[$i - 1] . '/p'))) return false;
        }
        //

        $oUrlRewriteCollection = Mage::getModel('core/url_rewrite')
            ->getCollection()
            ->addFieldToFilter('request_path', $base_url);

        $hasOptionParam = false;

        if (count($oUrlRewriteCollection) == 0) {
            //check for /p/
            $possibleUrl =[];
            $parts = explode('/', $base_url);
            for($i = 1; $i <= count($parts); $i++ ){
                $url = '';
                for($j = 0; $j < $i; $j ++){
                    $url .= $parts[$j].'/';
                }
                $url = trim($url, '/');
                array_push($possibleUrl, $url);
            }
            $realUrl = '';
            for ($i = 1; $i < count($possibleUrl); $i++) {
                $oUrlRewriteCollection = Mage::getModel('core/url_rewrite')
                    ->getCollection()
                    ->addFieldToFilter('request_path', $possibleUrl[$i]);
                if(count($oUrlRewriteCollection) == 0){
                    $realUrl = $possibleUrl[$i-1];
                    break;
                }
            }

            if($realUrl == '') return false;
//            else{
//                $lastPart = str_replace($realUrl, '', $base_url);
//                if(strpos($lastPart, '/p/') === false) return false;
//            }

            //remove last part to get real url
            $url = substr($base_url, 0, strrpos($base_url, '/'));
            $url = trim($url, '/');
            $url = trim($url, '/p');

            $oUrlRewriteCollection = Mage::getModel('core/url_rewrite')
                ->getCollection()
                ->addFieldToFilter('request_path', $url);

            if (count($oUrlRewriteCollection) > 0) {
                $hasOptionParam = true;
                $this->_request->setRequestUri('/' . $oUrlRewriteCollection->getData()[0]['target_path']);
                $this->_request->setPathInfo($oUrlRewriteCollection->getData()[0]['target_path']);
            }
        }
        if(Mage::registry('has_option_param')) Mage::unregister('has_option_param');
        Mage::register('has_option_param', $hasOptionParam);
        return true;
    }
}
