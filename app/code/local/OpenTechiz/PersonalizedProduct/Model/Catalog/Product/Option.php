<?php
class OpenTechiz_PersonalizedProduct_Model_Catalog_Product_Option extends Mage_Catalog_Model_Product_Option
{
    const OPTION_GROUP_STONE       = 'stone';
    const OPTION_TYPE_STONE_TYPE   = 'stone';
    const OPTION_GROUP_GOLD       = 'gold';
    const OPTION_TYPE_GOLD_TYPE   = 'gold';
    /**
     * Get group name of option by given option type
     *
     * @param string $type
     * @return string
     */
    public function getGroupByType($type = null)
    {
//        if (is_null($type)) {
//            $type = $this->getType();
//        }
//
//        $group = parent::getGroupByType($type);
//        if( $group == '' && $type = self::OPTION_TYPE_STONE_TYPE ){
//            $group = self::OPTION_GROUP_STONE;
//        }
//        else if( $group == '' && $type = self::OPTION_TYPE_GOLD_TYPE ){
//            $group = self::OPTION_GROUP_GOLD;
//        }
//        return $group;
        if (is_null($type)) {
            $type = $this->getType();
        }
        $optionGroupsToTypes = array(
            parent::OPTION_TYPE_FIELD => parent::OPTION_GROUP_TEXT,
            parent::OPTION_TYPE_AREA => parent::OPTION_GROUP_TEXT,
            parent::OPTION_TYPE_FILE => parent::OPTION_GROUP_FILE,
            parent::OPTION_TYPE_DROP_DOWN => parent::OPTION_GROUP_SELECT,
            parent::OPTION_TYPE_RADIO => parent::OPTION_GROUP_SELECT,
            parent::OPTION_TYPE_CHECKBOX => parent::OPTION_GROUP_SELECT,
            parent::OPTION_TYPE_MULTIPLE => parent::OPTION_GROUP_SELECT,
            parent::OPTION_TYPE_DATE => parent::OPTION_GROUP_DATE,
            parent::OPTION_TYPE_DATE_TIME => parent::OPTION_GROUP_DATE,
            parent::OPTION_TYPE_TIME => parent::OPTION_GROUP_DATE,
            self::OPTION_TYPE_STONE_TYPE => self::OPTION_GROUP_STONE,
            self::OPTION_TYPE_GOLD_TYPE => self::OPTION_GROUP_GOLD,
        );

        return isset($optionGroupsToTypes[$type])?$optionGroupsToTypes[$type]:'';
    }

    public function groupFactory($type)
    {
        if( $type == self::OPTION_TYPE_STONE_TYPE ){
            return Mage::getModel('OpenTechiz_PersonalizedProduct_Model_Catalog_Product_Option_Type_Stone');
        }
        else if( $type == self::OPTION_TYPE_GOLD_TYPE ){
            return Mage::getModel('OpenTechiz_PersonalizedProduct_Model_Catalog_Product_Option_Type_Gold');
        }
        return parent::groupFactory($type);
    }

    public function updateStoneOptions($productIds){
        $collection = Mage::getModel('personalizedproduct/price_stone')->getCollection()
            ->addFieldToFilter('product_id', array('in' => $productIds))
            ->join(array('stone' => 'opentechiz_material/stone'), 'main_table.stone_id = stone.stone_id', '*');
        $productids = [];

        foreach ($collection as $productid) {
            if(!in_array($productid->getProductId(), $productids)) {
                array_push($productids, $productid->getProductId());
            }
        }
        foreach ($productids as $productId){
            $product = Mage::getModel('catalog/product')->load($productId);
            if(Mage::helper('personalizedproduct')->isPersonalizedProduct($product)) {
                $custom_options = [];
                $centerStoneCollection = Mage::getModel('personalizedproduct/price_stone')->getCollection()
                    ->join(array('stone' => 'opentechiz_material/stone'), 'main_table.stone_id = stone.stone_id', '*')
                    ->addFieldToFilter('product_id', $productId)->addFieldToFilter('option_name', 'Center Stone');

                $custom_options[] = array(
                    'is_delete' => 0,
                    'title' => 'Center Stone',
                    'previous_group' => '',
                    'previous_type' => '',
                    'type' => 'stone',
                    'is_require' => 1,
                    'sort_order' => 0,
                    'values' => array()
                );

                $skuAll = [];
                foreach ($centerStoneCollection as $value) {
                    $sku = $value->getStoneType() . $value->getQuality();
                    if (!in_array($sku, $skuAll)) array_push($skuAll, $sku);
                    else continue;
                    $title = ucfirst($value->getStoneType());
                    $price_type = 'fixed';
                    $price = 0;
                    $sort_order = 0;

                    $custom_options[count($custom_options) - 1]['values'][] = array(
                        'is_delete' => 0,
                        'title' => $title,
                        'option_type_id' => -1,
                        'price_type' => $price_type,
                        'price' => $price,
                        'sku' => $sku,
                        'sort_order' => $sort_order,
                    );
                }

                $stoneTwoCollection = Mage::getModel('personalizedproduct/price_stone')->getCollection()
                    ->join(array('stone' => 'opentechiz_material/stone'), 'main_table.stone_id = stone.stone_id', '*')
                    ->addFieldToFilter('product_id', $productId)->addFieldToFilter('option_name', 'Stone 2');

                if (count($stoneTwoCollection) > 0) {
                    $custom_options[] = array(
                        'is_delete' => 0,
                        'title' => 'Stone 2',
                        'previous_group' => '',
                        'previous_type' => '',
                        'type' => 'stone',
                        'is_require' => 1,
                        'sort_order' => 1,
                        'values' => array()
                    );

                    $skuAll = [];
                    foreach ($stoneTwoCollection as $value) {
                        $sku = $value->getStoneType() . $value->getQuality();
                        if (!in_array($sku, $skuAll)) array_push($skuAll, $sku);
                        else continue;
                        $title = ucfirst($value->getStoneType());
                        $price_type = 'fixed';
                        $price = 0;
                        $sort_order = 0;

                        $custom_options[count($custom_options) - 1]['values'][] = array(
                            'is_delete' => 0,
                            'title' => $title,
                            'option_type_id' => -1,
                            'price_type' => $price_type,
                            'price' => $price,
                            'sku' => $sku,
                            'sort_order' => $sort_order,
                        );
                    }
                }
                $stoneThreeCollection = Mage::getModel('personalizedproduct/price_stone')->getCollection()
                    ->join(array('stone' => 'opentechiz_material/stone'), 'main_table.stone_id = stone.stone_id', '*')
                    ->addFieldToFilter('product_id', $productId)->addFieldToFilter('option_name', 'Stone 3');

                if (count($stoneThreeCollection) > 0) {
                    $custom_options[] = array(
                        'is_delete' => 0,
                        'title' => 'Stone 3',
                        'previous_group' => '',
                        'previous_type' => '',
                        'type' => 'stone',
                        'is_require' => 1,
                        'sort_order' => 2,
                        'values' => array()
                    );

                    $skuAll = [];
                    foreach ($stoneThreeCollection as $value) {
                        $sku = $value->getStoneType() . $value->getQuality();
                        if (!in_array($sku, $skuAll)) array_push($skuAll, $sku);
                        else continue;
                        $title = ucfirst($value->getStoneType());
                        $price_type = 'fixed';
                        $price = 0;
                        $sort_order = 0;

                        $custom_options[count($custom_options) - 1]['values'][] = array(
                            'is_delete' => 0,
                            'title' => $title,
                            'option_type_id' => -1,
                            'price_type' => $price_type,
                            'price' => $price,
                            'sku' => $sku,
                            'sort_order' => $sort_order,
                        );
                    }
                }
                $stoneFourCollection = Mage::getModel('personalizedproduct/price_stone')->getCollection()
                    ->join(array('stone' => 'opentechiz_material/stone'), 'main_table.stone_id = stone.stone_id', '*')
                    ->addFieldToFilter('product_id', $productId)->addFieldToFilter('option_name', 'Stone 4');

                if (count($stoneFourCollection) > 0) {
                    $custom_options[] = array(
                        'is_delete' => 0,
                        'title' => 'Stone 4',
                        'previous_group' => '',
                        'previous_type' => '',
                        'type' => 'stone',
                        'is_require' => 1,
                        'sort_order' => 3,
                        'values' => array()
                    );

                    $skuAll = [];
                    foreach ($stoneFourCollection as $value) {
                        $sku = $value->getStoneType() . $value->getQuality();
                        if (!in_array($sku, $skuAll)) array_push($skuAll, $sku);
                        else continue;
                        $title = ucfirst($value->getStoneType());
                        $price_type = 'fixed';
                        $price = 0;
                        $sort_order = 0;

                        $custom_options[count($custom_options) - 1]['values'][] = array(
                            'is_delete' => 0,
                            'title' => $title,
                            'option_type_id' => -1,
                            'price_type' => $price_type,
                            'price' => $price,
                            'sku' => $sku,
                            'sort_order' => $sort_order,
                        );
                    }
                }

//            var_dump($custom_options);exit;
                $product = Mage::getModel('catalog/product')->load($productId);
                foreach ($product->getOptions() as $o) {
                    if ($o->getType() == 'stone') {
                        $o->getValueInstance()->deleteValue($o->getId());
                        $o->deletePrices($o->getId());
                        $o->deleteTitles($o->getId());
                        $o->delete();
                    }
                }
                /* Add the custom options specified in the CSV import file */
                if (count($custom_options)) {
                    foreach ($custom_options as $option) {

                        $opt = Mage::getModel('catalog/product_option');
                        $opt->setProduct($product);
                        $opt->addOption($option);
                        $opt->saveOptions();
                        $StonePricecollection = Mage::getModel('personalizedproduct/price_stone')->getCollection()
                            ->addFieldToFilter('product_id', $productId)->addFieldToFilter('option_name', $opt->getTitle());
                        foreach ($StonePricecollection as $stonePrice) {
                            $stonePrice->setOptionId($opt->getId())->save();
                        }
                    }
                }
            }
        }
    }

    public function updateAlloyOptions($productIds)
    {
        $collection = Mage::getModel('personalizedproduct/price_alloy')->getCollection()
            ->addFieldToFilter('product_id', array('in' => $productIds))
            ->join(array('gold' => 'opentechiz_material/gold'), 'main_table.gold_id = gold.gold_id', '*');
        $productids = [];

        foreach ($collection as $productid) {
            if (!in_array($productid->getProductId(), $productids)) {
                array_push($productids, $productid->getProductId());
            }
        }
        foreach ($productids as $productId) {
            $custom_options = [];
            $alloyCollection = Mage::getModel('personalizedproduct/price_alloy')->getCollection()
                ->join(array('gold' => 'opentechiz_material/gold'), 'main_table.gold_id = gold.gold_id', '*')
                ->addFieldToFilter('product_id', $productId);

            $custom_options[] = array(
                'is_delete' => 0,
                'title' => 'Metal',
                'previous_group' => '',
                'previous_type' => '',
                'type' => 'gold',
                'is_require' => 1,
                'sort_order' => 4,
                'values' => array()
            );

            $skuAll = [];
            foreach ($alloyCollection as $value) {
                $sku = str_replace(' ', '', $value->getGoldType() . $value->getGoldColor());
                if (!in_array($sku, $skuAll)) array_push($skuAll, $sku);
                else continue;
                $title = ucfirst($value->getGoldType()) .' '. ucfirst($value->getGoldColor());
                $price_type = 'fixed';
                $price = 0;
                $sort_order = 0;

                $custom_options[count($custom_options) - 1]['values'][] = array(
                    'is_delete' => 0,
                    'title' => $title,
                    'option_type_id' => -1,
                    'price_type' => $price_type,
                    'price' => $price,
                    'sku' => $sku,
                    'sort_order' => $sort_order,
                );
            }
            $product = Mage::getModel('catalog/product')->load($productId);
            foreach ($product->getOptions() as $o) {
                if($o->getType() == 'gold') {
                    $o->getValueInstance()->deleteValue($o->getId());
                    $o->deletePrices($o->getId());
                    $o->deleteTitles($o->getId());
                    $o->delete();
                }
            }
            /* Add the custom options specified in the CSV import file */
            if(count($custom_options)) {
                foreach($custom_options as $option) {

                    $opt = Mage::getModel('catalog/product_option');
                    $opt->setProduct($product);
                    $opt->addOption($option);
                    $opt->saveOptions();
                    
                }
            }
        }
    }
}