<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Product type price model
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class OpenTechiz_PersonalizedProduct_Model_Catalog_Product_Type_Price extends Mage_Catalog_Model_Product_Type_Price
{
    protected function _applyOptionsPrice($product, $qty, $finalPrice)
    {
        if ($optionIds = $product->getCustomOption('option_ids')) {
            if(Mage::helper('personalizedproduct')->isPersonalizedProduct($product)){
                $finalPrice = 0;
            }
            $basePrice = $finalPrice;
            foreach (explode(',', $optionIds->getValue()) as $optionId) {
                if ($option = $product->getOptionById($optionId)) {
                    $confItemOption = $product->getCustomOption('option_'.$option->getId());
                    $group = $option->groupFactory($option->getType())
                        ->setOption($option)
                        ->setConfigurationItemOption($confItemOption);

                    $data = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStonePrice($option->getId(),$product->getId());
                    $goldData = Mage::helper('personalizedproduct/personalizedPriceHelper')->getAlloyPrice($product->getId());
                    $stoneQualityData = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStoneWithQualityPrice($option->getId(),$product->getId());

                    if($product->getOptionById($optionId)->getType() == 'stone' ) {
                        $stone = $option->getValueById($confItemOption->getValue());
                        $stoneTitle = str_replace(' ', '', trim(strtolower($stone->getTitle())));
                        if($stoneTitle == 'diamond'){
                            $sku = $stone->getSku();
                            $quality = str_replace("diamond", "", $sku);;

                            $stone->setPrice($stoneQualityData[$quality]);
                        } else
                            $stone->setPrice($data[$stoneTitle]);
                    }
                    if($product->getOptionById($optionId)->getType() == 'gold' ) {
                        $gold = $option->getValueById($confItemOption->getValue());
                        $title = str_replace(' ', '', str_replace(' ', '', trim(strtolower($gold->getTitle()))));
                        $gold->setPrice($goldData[$title]);
                    }
                    $finalPrice += $group->getOptionPrice($confItemOption->getValue(), $basePrice);

                }
            }
        }
        $product = Mage::getModel('catalog/product')->load($product->getId());
        $finalPrice += (float)$product->getData('additional_fee');

        //for ordersurcharge price
        if($product->getSku() == 'mw_virtual_surcharge'){
            $session = Mage::getSingleton('customer/session');
            $quote = Mage::getModel('sales/quote')->load($session->getQuoteId());
            $surchargeId = $quote->getData('surcharge_id');

            $surcharge = Mage::getModel('mageworx_orderssurcharge/surcharge')->load($surchargeId);
            $finalPrice = $surcharge->getBaseTotalDue();
        }

        return $finalPrice;
    }
}