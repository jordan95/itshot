<?php

class OpenTechiz_PersonalizedProduct_Adminhtml_Price_StoneController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Catalog'))->_title($this->__('Product Option Stone'));
        $this->loadLayout()
            ->_setActiveMenu('catalog');
        $this->_addContent($this->getLayout()->createBlock('personalizedproduct/adminhtml_price_stone'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('personalizedproduct/adminhtml_price_stone_grid')->toHtml()
        );
    }

    /**
     * MassDelete action
     */
    public function massDeleteAction()
    {
        $stoneIds = $this->getRequest()->getParam('stone_option');
        if (!is_array($stoneIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($stoneIds as $stoneId) {
                    $stone = Mage::getModel('personalizedproduct/price_stone')->load($stoneId);
                    $stone->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($stoneIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function getOptionValueAction(){
        $params = $this->getRequest()->getParams();
        $optionId = (int)$params['id'];

        //get shape and size of stones for comparison
        $sampleData = Mage::getModel('personalizedproduct/price_stone')->getCollection()->addFieldToFilter('option_id', $optionId)
            ->getFirstItem();

        $stoneGroup = $sampleData->getStoneSelectGroup();

        $collectionSample = Mage::getModel('personalizedproduct/price_stone')->getCollection()->addFieldToFilter('stone_select_group', $stoneGroup)
            ->join(array('stone' => 'opentechiz_material/stone'), 'stone.stone_id = main_table.stone_id', '*');
        $data = [];
        foreach ($collectionSample as $sample){
            $stoneData = [];
            $stoneData['quantity'] = $sample->getQuantity();
            $stoneData['shape'] = $sample->getShape();
            $stoneData['diameter'] = $sample->getDiameter();
            array_push($data, $stoneData);
        }

        $stoneCollection = Mage::getModel('opentechiz_material/stone')->getCollection();

        $selectableStone = [];

        foreach (OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_TYPE as $key=>$value){
            $flag = false;
            foreach($data as $sampleData){
                foreach ($stoneCollection as $stone){
                    if($stone->getStoneType() == $key && $stone->getShape() == $sampleData['shape'] && $stone->getDiameter() == $sampleData['diameter']){
                        $flag = true;
                    }
                }
                if ($flag == false){
                    break;
                }
            }
            if($flag == true) $selectableStone[$key] = $value;
        }

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($selectableStone));
    }

    public function exportCsvAction()
    {
        $fileName = 'stoneoptions_' . date('Y_m_d_h_i_s') . '.csv';
        $content = $this->getLayout()->createBlock('personalizedproduct/adminhtml_price_stone_grid');
        $this->_prepareDownloadResponse($fileName, $content->getCsvFile());
    }

    public function importCSVAction()
    {
        if ($this->getRequest()->getPost()) {
            if (isset($_FILES['file']['name']) && (file_exists($_FILES['file']['tmp_name']))) {
                try {
                    $file = $_FILES['file']['name'];
                    $path = Mage::getBaseDir('var') . DS . 'import' . DS;
                    $uploader = new Varien_File_Uploader('file');
                    $uploader->setAllowedExtensions(array('csv'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);
                    $uploader->save($path, $file);
                    $file = Mage::app()->getConfig()->getTempVarDir() . '/import/' . $file;
                    $csv = new Varien_File_Csv();
                    $priceStoneModel = Mage::getModel('personalizedproduct/price_stone');
                    $allIds = $priceStoneModel->getCollection()->getAllIds();
                    $importIds = [];
                    $productIds = [];

                    foreach ($csv->getData($file) as $line => $row) {
                        if ($line == 0) {
                            foreach ($row as $i => $rowHeader) {
                                $headerRow[$i] = $rowHeader;
                            }
                            continue; // skip header row
                        }
                        $optionName = $row[2];
                        $optionName = strtolower(str_replace(' ', '', $optionName));
                        $productId = $row[1];
                        $optionId = 0;
                        array_push($productIds, $productId);

                        foreach (Mage::getModel('catalog/product')->load($productId)->getOptions() as $option){
                            $optionTitle = strtolower(str_replace(' ', '', $option->getTitle()));
                            if($optionName == $optionTitle){
                                $optionId = $option->getId();
                                break;
                            }
                        }
                        array_push($importIds, (int)$row[0]);
                        if (in_array((int)$row[0], $allIds))
                        {
                            $priceStoneModel = Mage::getModel('personalizedproduct/price_stone');
                            $priceStoneModel->load((int)$row[0])
                                ->setProductId($row[1])
                                ->setOptionName($row[2])
                                ->setOptionId($optionId)
                                ->setStoneId($row[3])
                                ->setStoneSelectGroup($row[4])
                                ->setQuantity($row[5])
                                ->save();
                        } else {
                            $priceStoneModel = Mage::getModel('personalizedproduct/price_stone');
                            $priceStoneModel->setProductId($row[1])
                                ->setOptionName($row[2])
                                ->setOptionId($optionId)
                                ->setStoneId($row[3])
                                ->setStoneSelectGroup($row[4])
                                ->setQuantity($row[5])
                                ->save();
                        }
                    }
                    Mage::getModel('personalizedproduct/catalog_product_option')->updateStoneOptions($productIds);
                    
                    Mage::dispatchEvent('material_data_change_save_after');

                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Import Completed.'));
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
            $this->_redirect('*/*');
        }
    }
}