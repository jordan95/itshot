<?php

class OpenTechiz_PersonalizedProduct_Adminhtml_Price_GoldController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Catalog'))->_title($this->__('Product Option Gold'));
        $this->loadLayout()
            ->_setActiveMenu('catalog');
        $this->_addContent($this->getLayout()->createBlock('personalizedproduct/adminhtml_price_alloy'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('personalizedproduct/adminhtml_price_alloy_grid')->toHtml()
        );
    }

    /**
     * MassDelete action
     */
    public function massDeleteAction()
    {
        $alloyIds = $this->getRequest()->getParam('alloy_option');
        if (!is_array($alloyIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($alloyIds as $alloyId) {
                    $alloy = Mage::getModel('personalizedproduct/price_alloy')->load($alloyId);
                    $alloy->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($alloyIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function getOptionValueAction(){
        $goldModel = Mage::getModel('opentechiz_material/gold')->getCollection();
        $selectableGold = [];

        foreach ($goldModel as $gold){
            $sku = str_replace(' ', '', strtolower(trim($gold->getGoldType().$gold->getGoldColor())));
            $name = '';
            $goldType = str_replace(' ', '', strtolower(trim($gold->getGoldType())));
            $goldColor = str_replace(' ', '', strtolower(trim($gold->getGoldColor())));
            if(array_key_exists($goldType, OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_TYPE)){
                $name.= OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_TYPE[$goldType];
            }
            if(array_key_exists($goldColor, OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_COLOR)){
                $name.= ' '.OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_COLOR[$goldColor];
            }

            $selectableGold[$sku] = $name;
        }
        
        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($selectableGold));
    }

    public function exportCsvAction()
    {
        $fileName = 'goldoptions_' . date('Y_m_d_h_i_s') . '.csv';
        $content = $this->getLayout()->createBlock('personalizedproduct/adminhtml_price_alloy_grid');
        $this->_prepareDownloadResponse($fileName, $content->getCsvFile());
    }

    public function importCSVAction()
    {
        if ($this->getRequest()->getPost()) {
            if (isset($_FILES['file']['name']) && (file_exists($_FILES['file']['tmp_name']))) {
                try {
                    $file = $_FILES['file']['name'];
                    $path = Mage::getBaseDir('var') . DS . 'import' . DS;
                    $uploader = new Varien_File_Uploader('file');
                    $uploader->setAllowedExtensions(array('csv'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);
                    $uploader->save($path, $file);
                    $file = Mage::app()->getConfig()->getTempVarDir() . '/import/' . $file;
                    $csv = new Varien_File_Csv();
                    $priceGoldModel = Mage::getModel('personalizedproduct/price_alloy');
                    $allIds = $priceGoldModel->getCollection()->getAllIds();
                    $importIds = [];
                    $productIds = [];

                    foreach ($csv->getData($file) as $line => $row) {
                        if ($line == 0) {
                            foreach ($row as $i => $rowHeader) {
                                $headerRow[$i] = $rowHeader;
                            }
                            continue; // skip header row
                        }
                        
                        $productId = $row[1];
                        array_push($productIds, $productId);
                        $goldType = strtolower(trim($row[3]));

                        array_push($importIds, $row[0]);
                        if (in_array($row[0], $allIds))
                        {
                            $priceGoldModel = Mage::getModel('personalizedproduct/price_alloy');
                            $priceGoldModel->load($row[0])
                                ->setProductId($row[1]);

                            // check if gold color exist
                            if ($row[4]) {
                                $goldColor = strtolower(trim(preg_replace('/\s+/', '', $row[4])));
                                $priceGoldModel->setGoldId((int) $this->_getGoldId($goldType, $goldColor))
                                    ->setGoldType($goldType)
                                    ->setGoldColor($goldColor);
                            } else {
                                // $goldColor = strtolower(trim(preg_replace('/\s+/', '', $row[4])));
                                $priceGoldModel->setGoldId((int) $this->_getSilverId($goldType))
                                    ->setGoldType($goldType);
                            }
                            $priceGoldModel->save();
                        } else {
                            $priceGoldModel = Mage::getModel('personalizedproduct/price_alloy');
                            $priceGoldModel->setProductId($row[1]);

                            // check if gold color exist
                            if ($row[4]) {
                                $goldColor = strtolower(trim(preg_replace('/\s+/', '', $row[4])));
                                $priceGoldModel->setGoldId((int) $this->_getGoldId($goldType, $goldColor))
                                    ->setGoldType($goldType)
                                    ->setGoldColor($goldColor);
                            } else {
                                // $goldColor = strtolower(trim(preg_replace('/\s+/', '', $row[4])));
                                $priceGoldModel->setGoldId((int) $this->_getSilverId($goldType))
                                    ->setGoldType($goldType);
                            }
                            $priceGoldModel->save();
                        }
                    }

                    Mage::getModel('personalizedproduct/catalog_product_option')->updateAlloyOptions($productIds);

                    Mage::dispatchEvent('material_data_change_save_after');

                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Import Completed.'));
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
            $this->_redirect('*/*');
        }
    }

    protected function _getGoldId($goldType, $goldColor)
    {
        $gold = Mage::getModel('opentechiz_material/gold')
            ->getCollection()
            ->addFieldToFilter('gold_type', array('eq' => $goldType))
            ->addFieldToFilter('gold_color', array('eq' => $goldColor))
            ->getData();
        return $gold[0]['gold_id'];
    }

    protected function _getSilverId($goldType)
    {
        $gold = Mage::getModel('opentechiz_material/gold')
            ->getCollection()
            ->addFieldToFilter('gold_type', array('eq' => $goldType))
            ->getData();
        return $gold[0]['gold_id'];
    }
}