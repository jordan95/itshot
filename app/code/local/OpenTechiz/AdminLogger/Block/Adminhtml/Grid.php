<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class OpenTechiz_AdminLogger_Block_Adminhtml_Grid extends MDN_AdminLogger_Block_Grid
{

    protected function _prepareColumns()
    {
        parent::_prepareColumns();                
        $this->addColumn('al_date', array(
            'header'=> Mage::helper('AdminLogger')->__('Date'),
            'index' => 'al_date',
            'type' => 'datetime',
             'width' => '180px',
            'filter_condition_callback' => array($this, '_DateTimeFilter')
        ));

        return $this;
    }
    public function addColumn($columnId, $column)
    {
        if (is_array($column)) {
            $this->_columns[$columnId] = $this->getLayout()->createBlock('opentechiz_adminlogger/adminhtml_widget_grid_column')
                ->setData($column)
                ->setGrid($this);
        }
        /*elseif ($column instanceof Varien_Object) {
            $this->_columns[$columnId] = $column;
        }*/
        else {
            throw new Exception(Mage::helper('adminhtml')->__('Wrong column format.'));
        }

        $this->_columns[$columnId]->setId($columnId);
        $this->_lastColumnId = $columnId;
        return $this;
    }
    protected function _DateTimeFilter($collection, $column)
    {
        $date_from = '';
        $date_to = '';
        $orig_from = $column->getFilter()->getValue()['orig_from'];
        $orig_to = $column->getFilter()->getValue()['orig_to'];
        if($orig_from !=''){
            $date_from =  Mage::helper('opentechiz_adminlogger')->_convertDate($orig_from,'from');
             $collection->addFieldToFilter('al_date', array('gteq' => $date_from));
        }
        if($orig_to !=''){
            $date_to   =  Mage::helper('opentechiz_adminlogger')->_convertDate($orig_to,'to');
            $collection->addFieldToFilter('al_date', array('lteq' => $date_to));
        }

        return $this;
    }
}
