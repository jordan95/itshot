<?php

class OpenTechiz_AdminLogger_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function _convertDate($date,$typeDate){
        $date = trim(str_replace(array('AM', 'PM'),'',$date));
        $splitDate = preg_split('/[\s]+/', $date);
        $convertDate = explode('/', $splitDate[0]);
        $formatDate = $convertDate[2] . '-' . $convertDate[0] . '-' . $convertDate[1] . " ".$splitDate[1];
        if(is_null($splitDate[1]) && $typeDate =='from'){
        	$formatDate = $formatDate." 00:00:00";
        	$formatDate =  date("Y-m-d H:i:s", strtotime($formatDate));
        	return $formatDate;
        }
        if(is_null($splitDate[1]) && $typeDate =='to'){
        	$formatDate = $formatDate." 23:59:59";
        	$formatDate =  date("Y-m-d H:i:s", strtotime($formatDate));
        	return $formatDate;
        }
        $formatDate = trim($formatDate);
        $formatDate =  date("Y-m-d H:i:s", strtotime('+4 hours', strtotime($formatDate)));
        return $formatDate;
    }
}