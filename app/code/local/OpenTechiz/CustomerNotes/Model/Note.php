<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_CustomerNotes_Model_Note extends Mage_Core_Model_Abstract {
    protected function _construct()
    {
        $this->_init('opentechiz_customernotes/note');
    }
}