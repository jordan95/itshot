<?php

class OpenTechiz_CustomerNotes_Model_Notes_Api extends Mage_Customer_Model_Api_Resource
{

    public function items($customerId)
    {
        $collection = Mage::getModel('opentechiz_customernotes/note')
                ->getCollection()
                ->setOrder('note_id', 'DESC')
                ->addFieldToFilter('customer_id', array('eq' => $customerId));
        $result = array();
        foreach ($collection as $note) {
            $result[] = array(
                'note_id' => $note->getId(),
                'note_content' => $note->getNoteContent(),
                'created_at' => $note->getCreatedAt()
            );
        }
        return $result;
    }

}
