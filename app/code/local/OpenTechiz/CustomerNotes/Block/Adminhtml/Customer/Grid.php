<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_CustomerNotes_Block_Adminhtml_Customer_Grid extends OpenTechiz_SalesExtend_Block_Adminhtml_Customer_Grid
{
    public function setCollection($collection)
    {
        $collection->addAttributeToSelect('latest_comment');
        parent::setCollection($collection);
    }
    protected function _prepareColumns()
    {
        /* Adding notes column to the customer grid */
        $this->addColumn('latest_comment', array(
            'header'    =>  Mage::helper('opentechiz_customernotes')->__('Last Comment'),
            'index'     =>  'latest_comment',
            'type'      =>  'text',
        ));

        /* Show the Notes column after Customer Since column */
        $this->addColumnsOrder('content_notes', 'customer_since');

        return parent::_prepareColumns();
    }
}