<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_CustomerNotes_Block_Adminhtml_Customer_Note
    extends Mage_Adminhtml_Block_Template
    implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    /**
     * Set the template for the block
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('customernotes/customer/note.phtml');
    }
    /**
     * Retrieve the label used for the tab relating to this block
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Notes');
    }
    /**
     * Retrieve the title used by this tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Notes');
    }
    /**
     * Determines whether to display the tab
     * Add logic here to decide whether you want the tab to display
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }
    /**
     * Stops the tab being hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        if (Mage::registry('current_customer')->getId()) {
            return false;
        }
        return true;
    }
}
