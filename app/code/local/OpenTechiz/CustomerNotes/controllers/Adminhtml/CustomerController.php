<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_CustomerNotes_Adminhtml_CustomerController extends Mage_Adminhtml_CustomerController
{
    public function saveNoteAction()
    {
        /* User */
        $user = Mage::getSingleton('admin/session');

        /* Model */
        $model = Mage::getModel('opentechiz_customernotes/note');

        /* Data from POST method */
        $comment = $this->getRequest()->getPost('cmeditor');
        $customerId = $this->getRequest()->getParam('id');

        /* Customer */
        $customer = Mage::getModel('customer/customer')->load($customerId);
        /* Get user_id */
        $userId = $user->getUser()->getUserId();


        $createdAt = Mage::getModel('core/date')->timestamp();

        /* Save data to database */
        $model->setData('customer_id', $customerId);
        $model->setData('user_id', $userId);
        $model->setData('note_content', $comment);
        $model->setData('created_at', $createdAt);
        $customer->setData('latest_comment', $comment);
        try {
            // Save to "opentechiz_customernotes/note" model
            $model->save();
            // Save latest comment to "customer/customer" model
            $customer->save();
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Submit Sucessfully.'));
        } catch (Exception $error) {
            Mage::getSingleton('adminhtml/session')->addError($error->getMessage());
            return false;
        }
        $this->_redirectReferer();
    }

    public function delNoteAction()
    {
        $customerId = $this->getRequest()->getParam('customerId');
        $customer = Mage::getModel('customer/customer');
        $noteId = $this->getRequest()->getParam('noteId');
        $comment = Mage::getModel('opentechiz_customernotes/note');
        try {
            $comment->load($noteId);
            $comment->delete();
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Comment deleted.'));
        } catch (Exception $error) {
            Mage::getSingleton('adminhtml/session')->addError($error->getMessage());
            return false;
        }

        /* Update latest comment after delete */
        $latestComment = $comment->getCollection()->addFieldToFilter('customer_id', $customerId)->setOrder('note_id', 'DESC')->getData();
        if(isset($latestComment))   {
            $customer->load($customerId)->setData('latest_comment', $latestComment[0]['note_content'])->save();
        } else {
            $customer->load($customerId)->setData('latest_comment', '')->save();
        }

        $this->_redirectReferer();
    }
}