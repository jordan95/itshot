<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_CustomerNotes_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function addFrausterOrderNote($customer, $order){
        $data = [];
        $data['customer_id'] = $customer->getId();
        $data['user_id'] = 0;
        $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $data['note_content'] =  "Fraudster customer placed order #".$order->getIncrementId();
        Mage::getModel('opentechiz_customernotes/note')->setData($data)->save();
    }
}