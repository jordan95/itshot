<?php
$installer = $this;
$installer->startSetup();
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$entityTypeId = $setup->getEntityTypeId('customer');
$attributeSetId = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

//drop attribute if it already exists (to avoid problems)
$setup->removeAttribute('customer', 'latest_comment');
//now recreate the attribute
$setup->addAttribute('customer', 'latest_comment', array(
    'type' => 'varchar',
    'input' => 'text',
    'label' => Mage::helper('core')->__("Latest Comment"),
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 0,
    'default'   => '',
    'visible_on_front' => 0,
    'sort_order'    => 900
));
//adding the attribute in backend Forms in case you want that
Mage::getSingleton('eav/config')
    ->getAttribute('customer', 'latest_comment')
    ->setData('used_in_forms', array('adminhtml_customer', 'customer_edit'))
    ->save();
$installer->endSetup();