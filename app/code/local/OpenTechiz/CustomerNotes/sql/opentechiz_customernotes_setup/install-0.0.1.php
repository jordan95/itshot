<?php
$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('customer_notes')};
    CREATE TABLE {$this->getTable('customer_notes')} (
      `note_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
      `customer_id` INT(11) NOT NULL,
      `user_id` INT(11) NOT NULL,
      `note_content` text NOT NULL,
      `created_at` DATETIME NULL,
      PRIMARY KEY (`note_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
$installer->endSetup();