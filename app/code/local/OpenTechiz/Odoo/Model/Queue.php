<?php

class OpenTechiz_Odoo_Model_Queue extends Mage_Core_Model_Abstract
{
	protected function _construct()
    {
        $this->_init('odoo/queue');
    }

    public function requestTicketOdoo($is_cron_job = false){
    	$url_ticket = Mage::getModel('odoo/api_odoo')->getEndpoint("/web/helpdesk_custom/ticket");
    	try {
    		$data_request = unserialize($this->getDataRequest());
            if ($this->getAttachmentFirst() != '') {
                $data_request['attachment_1'] = $this->getAttachmentFirst();
            }

            if ($this->getAttachmentSecond() != '') {
                $data_request['attachment_2'] = $this->getAttachmentSecond();
            }
        	$result = Mage::getModel('odoo/api_odoo')->request($url_ticket, $data_request);
            $this->setUpdatedAt(now());

            if(!empty($result['result'])){
                if (!empty($result['result']['error'])) {
                    $this->failTicket($result['result']['error']);
                    if ($is_cron_job) {
                        Mage::helper('odoo')->sendEmailAdminError($this);
                    }
                    return $this;
                }
                
            }

        	if (empty($result['error'])) {
        		$this->setStatus(OpenTechiz_Odoo_Helper_Data::STT_SUCESSFUL);
                $this->setMessage('');
        	}else{
                $this->failTicket($result['error']['message']);
                // send email notify admin about cron job failed.
                if ($is_cron_job) {
                    Mage::helper('odoo')->sendEmailAdminError($this);
                }
        	}
    	} catch (Exception $e) {
            $this->failTicket($e->getMessage());
    	}

        return $this;
    }

    public function failTicket($message){
        $this->setStatus(OpenTechiz_Odoo_Helper_Data::STT_FAILED);
        $this->setMessage($message);
        $this->setUpdatedAt(now());
        Mage::log($message, null, 'odoo_error.log');
    }

    public function validateQueue(){
        return in_array($this->getStatus(), array(OpenTechiz_Odoo_Helper_Data::STT_FAILED,OpenTechiz_Odoo_Helper_Data::STT_PENDING));
    }
}
