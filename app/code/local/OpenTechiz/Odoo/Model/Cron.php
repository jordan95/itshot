<?php

class OpenTechiz_Odoo_Model_Cron extends Mage_Core_Model_Abstract
{
	public function requestTicketOdooApi(){
		if (!$this->getHelper()->isEnabled()) {
            Mage::log("Please setting Enable Odoo config for Cronjob", null, 'odoo_error.log');
            return $this;
        }

        $collection = Mage::getModel('odoo/queue')->getCollection();
        $collection->addFieldToFilter('status',OpenTechiz_Odoo_Helper_Data::STT_PENDING);
        $collection->getSelect()->limit(5);
        
        foreach ($collection as $queue) {
            if ($queue->validateQueue()) {
                try {
                    $queue->requestTicketOdoo(true);
                    $queue->save();
            	} catch (Exception $e) {
                    Mage::log($e->getMessage(), null, 'odoo_error.log');
            	}
            }
        }
        
        return $this;
	}

	protected function getHelper()
    {
        return Mage::helper("odoo");
    }
}