<?php

class OpenTechiz_Odoo_Model_Api_Odoo
{
    protected $sessionID = null;

    protected function auth($db, $login, $password)
    {
        $curl = curl_init();
        $data = [
            "params" => [
                "db" => $db,
                "login" => $login,
                "password" => $password,
            ],
        ];
        $url = $this->getEndpoint("/web/session/authenticate");
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HEADER => 1,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        ));

        $result = curl_exec($curl);
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($result, 0, $header_size);
        $body = substr($result, $header_size);
        $data = json_decode($body, true);
        if (empty($data['error'])) {
            preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
            return $matches[1][0];
        } else {
            Mage::throwException($data['error']['message']);
        }
        curl_close($curl);
    }

    public function getSessionID()
    {
        if (!$this->sessionID) {
            $db = $this->getDb();
            $login = $this->getHelper()->getLoginConfig();
            $password = $this->getHelper()->getPasswordConfig();
            $this->sessionID = $this->auth($db, $login, $password);
        }
        return $this->sessionID;
    }

    public function request($url, $data)
    {
        // Mage::log($url, null, 'odoo.log');
        // Mage::log($data, null, 'odoo.log');
        $sessionID = $this->getSessionID();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Cookie: " . $sessionID,
            ),
        ));
        $result = curl_exec($curl);
        return json_decode($result, true);
    }

    public function submitTicket(array $data,$attachment_1 = '',$attachment_2 = '')
    {
        try {
            $queue = Mage::getModel('odoo/queue');

            if ($attachment_1 != "") {
                $attachment = $this->convertAttachmentToBase64Encode($attachment_1);
                $queue->setAttachmentFirst($attachment);
            }
            if ($attachment_2 != "") {
                $attachment = $this->convertAttachmentToBase64Encode($attachment_2);
                $queue->setAttachmentSecond($attachment);
            }

            $queue->setStatus(OpenTechiz_Odoo_Helper_Data::STT_PENDING);
            $queue->setDataRequest(serialize($data));
            $queue->setCreatedAt(now());
            $queue->save();
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
        }
    }

    protected function convertAttachmentToBase64Encode($filename)
    {
        return base64_encode(file_get_contents(Mage::getBaseDir('media') . '/upload/' . Varien_File_Uploader::getCorrectFileName($filename)));
    }

    public function submitTicket_BK(array $data)
    {
        if (!$this->getHelper()->isEnabled()) {
            return;
        }
        $url = $this->getEndpoint("/web/helpdesk_custom/ticket");
        $result = $this->request($url, $data);
        if (empty($result['error'])) {
            return $result;
        } else {
            Mage::log("=========================", null, 'odoo_error.log');
            Mage::log("Request URL: ". $url, null, 'odoo_error.log');
            Mage::log($data, null, 'odoo_error.log');
            Mage::log($result['error'], null, 'odoo_error.log');
            Mage::throwException($result['error']['message']);
        }
    }

    public function getBaseUrl()
    {
        $subdomain = "";
        if ($this->getHelper()->isTestMode()) {
            $subdomain = "itshot-trn";
        } else {
            $subdomain = "itshot";
        }
        return "https://$subdomain.ciel-it.com";
    }

    public function getDb()
    {
        $db = $this->getHelper()->getDbConfig();
        if ($this->getHelper()->isTestMode()) {
            $db .= "-trn";
        } else {
            $db .= "-prd";
        }
        return $db;
    }

    public function getEndpoint($path)
    {
        return $this->getBaseUrl() . $path;
    }

    protected function getHelper()
    {
        return Mage::helper("odoo");
    }

}
