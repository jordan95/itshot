<?php

class OpenTechiz_Odoo_Helper_Data extends Mage_Core_Helper_Abstract
{

    const MODULE_KEY_ENABLE = 'odoo/general/enable';
    const XML_ODOO_GENERAL_SEND_EMAIL = 'odoo/general/send_email';
    const XML_ODOO_GENERAL_DB = 'odoo/general/db';
    const XML_ODOO_GENERAL_LOGIN = 'odoo/general/login';
    const XML_ODOO_GENERAL_PASSWORD = 'odoo/general/password';
    const XML_ODOO_GENERAL_TESTMODE = 'odoo/general/testmode';

    /* Define Email */
    const XML_PATH_RECIPIENT_NAME = 'odoo/email/recipient_name';
    const XML_PATH_RECIPIENT_EMAIL = 'odoo/email/recipient_email';

    /* Define Status Queue */
    const STT_PENDING   = 0;
    const STT_SUCESSFUL = 1;
    const STT_FAILED    = 2;

    public function getStatusQueue($key = null){
        $list = array(
            self::STT_PENDING => $this->__('Pending'),
            self::STT_SUCESSFUL => $this->__('Sucessful'),
            self::STT_FAILED => $this->__('Failed')
        );
        if($key && isset($list[$key])){
            return $list[$key];
        }
        return $list;
    }  

    public function sendPaymentFailedTicket($quote, $message, $checkoutType = 'onepage')
    {
        if (empty($message)) {
            return;
        }

        $objModel = Mage::getModel("ipsecurity/ipsecurity");
        $email = $quote->getCustomerEmail();
        $clientIp = Mage::helper('ipsecurity/customcode')->getip_index();
        $objModel = Mage::getModel("ipsecurity/ipsecurity");

        if (($email && $objModel->checkUserEmail($email)) || $objModel->checkUserIPAdd($clientIp)) {
            return;
        }
        if (!$quote->getShippingAddress()) {
            return;
        }
        /* @var $quote Mage_Sales_Model_Quote */
        $telephone = $quote->getShippingAddress()->getTelephone();
        $countryId = $quote->getShippingAddress()->getCountryId();
        if ($countryId != "US" || !$telephone) {
            return;
        }
        $billing_shipping_same_address = "No";
        if($quote->getShippingAddress()->getSameAsBilling()){
            $billing_shipping_same_address = "Yes";
        }
        $payment_method= $quote->getPayment()->getMethodInstance()->getTitle();
        $first_name = $quote->getCustomerFirstname();
        $last_name = $quote->getCustomerLastname();
        $email = $quote->getCustomerEmail();
        $shipping_method = $quote->getShippingAddress()->getShippingDescription();
        $reason = $message;
        $ticket_items = [];
        foreach ($quote->getAllVisibleItems() as $_item) {
            /* @var $_item Mage_Sales_Model_Quote_Item */
            $itemName = $_item->getProduct()->getName();
            if($_item->getSku()) {
                $itemName .= '(Item Code: ' . $_item->getSku() . ')';
            }
            $ticket_items [] = $itemName . '  x ' . $_item->getQty() . '  '
                    . $quote->getStoreCurrencyCode() . ' '
                . $_item->getProduct()->getFinalPrice($_item->getQty()) ;
        }
        $ticketData = [
            "CustomerFirstName"  => $first_name,
            "CustomerLastName"  => $last_name,
            "CustomerAddressCity" => $quote->getShippingAddress()->getCity(),
            "CustomerAddressStreet" => $quote->getShippingAddress()->getStreet1(),
            "CustomerAddressStreet2" => $quote->getShippingAddress()->getStreet2(),
            "CustomerAddressZipCode" => $quote->getShippingAddress()->getPostcode(),
            "CustomerAddressCountry" => $countryId,
            "CustomerAddressState" => $quote->getShippingAddress()->getRegionCode(),
            "customer_email" => $email,
            "customer_phone" => $telephone,
            "customer_name" => $first_name,
            "ticket_reason" => $message,
            "ticket_items" => $ticket_items,
            "total" => floatval($quote->getGrandTotal()),
            "shipping" => $shipping_method,
            "payment_method" => $payment_method,
            "billing_shipping_same_address" => $billing_shipping_same_address,
            "ticket_subject" => "Payment Transaction Failed Reminder",
            "team_id" => "Sales"
        ];
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');
        $odoo_payment_transaction_failed = $resource->getTableName('odoo_payment_transaction_failed');
        $id = (int) $readConnection->fetchOne("SELECT id FROM $odoo_payment_transaction_failed WHERE email = ? AND date(created_at) = CURRENT_DATE LIMIT 1", array($email));
        if ($id) {
            return;
        }
        try {
            Mage::getModel('odoo/api_odoo')->submitTicket($ticketData);
            $writeConnection->query("INSERT INTO $odoo_payment_transaction_failed(first_name, last_name, email, telephone, country_id, shipping_method, address, reason, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)", array(
                $first_name,
                $last_name,
                $email,
                $telephone,
                $countryId,
                $shipping_method,
                $quote->getShippingAddress()->getStreetFull(),
                $reason
            ));
        } catch (Exception $exc) {
            Mage::throwException($exc->getMessage());
        }
    }

    public function isModuleActive()
    {
        return Mage::getConfig()
                        ->getModuleConfig("OpenTechiz_Odoo")
                        ->is('active', 'true');
    }

    public function getDbConfig()
    {
        return Mage::getStoreConfig(self::XML_ODOO_GENERAL_DB);
    }

    public function getLoginConfig()
    {
        return Mage::getStoreConfig(self::XML_ODOO_GENERAL_LOGIN);
    }

    public function getPasswordConfig()
    {
        return Mage::getStoreConfig(self::XML_ODOO_GENERAL_PASSWORD);
    }

    public function isTestMode()
    {
        return Mage::getStoreConfigFlag(self::XML_ODOO_GENERAL_TESTMODE);
    }

    public function isEnabled()
    {
        return (bool)Mage::getStoreConfigFlag(self::MODULE_KEY_ENABLE);
    }

    public function isEnabledSendEmail()
    {
        return (bool)Mage::getStoreConfigFlag(self::XML_ODOO_GENERAL_SEND_EMAIL);
    }

    public function getOdooRecipientEmails()
    {
        $recipient_email = Mage::getStoreConfig(self::XML_PATH_RECIPIENT_EMAIL);
        $emails = preg_split("/[\s,;]+/", $recipient_email);
        return $emails;
    }

    public function getOdooRecipientName()
    {
        return Mage::getStoreConfig(self::XML_PATH_RECIPIENT_NAME);
    }

    public function sendEmailAdminError($queue){
        $emails = $this->getOdooRecipientEmails();
        $toEmail = current($emails);
        $ccEmails = array();
        foreach ($emails as $e) {
            if ($e != $toEmail) {
                $ccEmails[] = $e;
            }
        }
        $subject = $this->__("Odoo Submit Ticket Error.");
        $message ='<p><b>Odoo Queue #</b> '. $queue->getId() .' submit failed. You can do it again manually.</p>';

        $mail = Mage::getModel('core/email');
        $mail->setToName($this->getOdooRecipientName());
        $mail->setToEmail($toEmail);
        $mail->setFromEmail("sales@itshot.com");
        $mail->setFromName("ItsHot.com");
        $mail->setType('html');
        $mail->setSubject($subject);
        $mail->setBody($message);
        try {
            $mail->send();
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
        }
        return $this;
    }

}
