<?php
class OpenTechiz_Odoo_Block_Adminhtml_Queue extends Mage_Adminhtml_Block_Widget_Grid_Container {
	public function __construct() {
		$this->_controller = 'adminhtml_queue';
		$this->_blockGroup = 'odoo';
		$this->_headerText = Mage::helper('emailnotify')->__('Manage Queue Ticket');
		parent::__construct();
	}
}
