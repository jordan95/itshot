<?php
class OpenTechiz_Odoo_Block_Adminhtml_Queue_Renderer_Datarequest extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
	public function render(Varien_Object $row) {
		$data = unserialize($row->getDataRequest());

		if ($data['ticket_subject']) {
			return $data['ticket_subject'];
		}
		
		return;
	}
}
?>
