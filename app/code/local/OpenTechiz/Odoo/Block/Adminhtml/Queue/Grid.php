<?php
class OpenTechiz_Odoo_Block_Adminhtml_Queue_Grid extends Mage_Adminhtml_Block_Widget_Grid {
	public function __construct() {
		parent::__construct();
		$this->setId('queueGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}
	
	protected function _prepareCollection() {
		$collection = Mage::getModel('odoo/queue')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}
	
	protected function _prepareColumns() {
		$this->addColumn('id',array(
			'header'=>	Mage::helper('odoo')->__('Id'),
			'width'	=>	'50px',
			'index'	=>	'id',
		));

		$this->addColumn('data_request',array(
			'header'	=>	Mage::helper('odoo')->__('Ticket Subject'),
			'index'		=>	'data_request',
			'renderer' 	=> 'OpenTechiz_Odoo_Block_Adminhtml_Queue_Renderer_Datarequest',
			'filter'=>false,
            'sortable'  => false,
		));

		$this->addColumn('status',array(
			'header'	=>	Mage::helper('odoo')->__('Status'),
			'index'		=>	'status',
			'type'		=>	'options',
			'options'	=>	Mage::helper('odoo')->getStatusQueue(),
		));

		$this->addColumn('message',array(
			'header'	=>	Mage::helper('odoo')->__('Info'),
			'index'		=>	'message',
			'type'		=>	'text',
			'filter'=>false,
            'sortable'  => false,
		));

		$this->addColumn('created_at',array(
			'header'	=>Mage::helper('odoo')->__('Created At'),
			'width'		=>'150px',
			'index'		=>'created_at',
			'type'		=>'datetime',
		));

		$this->addColumn('updated_at',array(
			'header'	=>	Mage::helper('odoo')->__('Updated At'),
			'index'		=>	'updated_at',
			'width'		=>	'150px',
			'type'		=>	'datetime',
		));

		return parent::_prepareColumns();
	}
	
	protected function _prepareMassaction() {
		$this->setMassactionIdField('id');
		$this->getMassactionBlock()->setFormFieldName('item_ids');
		
		$this->getMassactionBlock()->addItem('create_ticket',array(
			'label'=>Mage::helper('emailnotify')->__('Create Odoo ticket manually'),
			'url'=>$this->getUrl('*/*/massRequestTicket'),
		));
		return $this;
	}
}
