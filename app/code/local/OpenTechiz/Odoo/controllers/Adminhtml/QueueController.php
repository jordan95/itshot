<?php
class OpenTechiz_Odoo_Adminhtml_QueueController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('customer/customer')
			->_addBreadcrumb(Mage::helper('odoo')->__('Manage Queue Ticket'),Mage::helper('emailnotify')->__('Manage Queue Ticket'));
		return $this;
	}
	
	public function indexAction() {
		$this->_title($this->__('Odoo'))->_title($this->__('Queue Ticket'));
        $this->_initAction()
            ->renderLayout();
	}

	public function massRequestTicketAction(){
		$itemsId = $this->getRequest()->getPost('item_ids');
		if ($itemsId) {
			$item_error = array();

			$collection = Mage::getModel('odoo/queue')->getCollection();
			$collection->addFieldToFilter('id', array('in'=> $itemsId));
			foreach ($collection as $queue) {
				if ($queue->validateQueue()) {
	                try {
	                    $queue->requestTicketOdoo();
	                    $queue->save();
	            	} catch (Exception $e) {
	                    $item_error[] = $queue->getId();
	            	}
	            }
			}

			if (count($item_error)) {
				Mage::getSingleton("adminhtml/session")->addError($this->__("Items get error when save: %s", implode(',', $item_error)));
			}

			Mage::getSingleton("adminhtml/session")->addSuccess($this->__("Successfully."));
		}

		$this->_redirect('*/*');
	    return;
	}

}