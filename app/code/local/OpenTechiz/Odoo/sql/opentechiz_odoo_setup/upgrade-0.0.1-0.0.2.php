<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('odoo/queue')};
    CREATE TABLE {$this->getTable('odoo/queue')} (
      `id`                   INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `data_request`         TEXT DEFAULT NULL,
      `status`               INT(5)       NOT NULL DEFAULT 0,
      `message`              TEXT DEFAULT NULL,
      `created_at`           TIMESTAMP NULL,
      `updated_at`           TIMESTAMP NULL,
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();