<?php
$installer = $this;
$installer->startSetup();

$installer->run("ALTER TABLE `tsht_odoo_queue` ADD `attachment_1` MEDIUMBLOB NULL DEFAULT NULL AFTER `data_request`;");
$installer->run("ALTER TABLE `tsht_odoo_queue` ADD `attachment_2` MEDIUMBLOB NULL DEFAULT NULL AFTER `attachment_1`;");
$installer->endSetup();