<?php

$installer = $this;
$installer->startSetup();

if(!$installer->tableExists($installer->getTable('odoo_payment_transaction_failed'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('odoo_payment_transaction_failed'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'unsigned' => true,
            'primary'  => true
        ), 'Id')
        ->addColumn('first_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
            'nullable' => true
        ), "First Name")
        ->addColumn('last_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
            'nullable' => true
        ), "Last Name")
        ->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
            'nullable' => true
        ), "Email")
        ->addColumn('telephone', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
            'nullable' => true
        ), "Telephone")
        ->addColumn('country_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 200, array(
            'nullable' => true
        ), "Country ID")
        ->addColumn('shipping_method', Varien_Db_Ddl_Table::TYPE_VARCHAR, 200, array(
            'nullable' => true
        ), "Shipping Method")
        ->addColumn('address', Varien_Db_Ddl_Table::TYPE_VARCHAR, 200, array(
            'nullable' => true
        ), "Shipping Method")
        ->addColumn('reason', Varien_Db_Ddl_Table::TYPE_VARCHAR, 500, array(
            'nullable' => true
        ), "Shipping Method")
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'Created At');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();