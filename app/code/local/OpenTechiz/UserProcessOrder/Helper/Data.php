<?php

class OpenTechiz_UserProcessOrder_Helper_Data extends Mage_Core_Helper_Abstract
{

	public function getAllUser(){

          $adminUser = Mage::getModel('admin/user');
		  $collection = $adminUser->getCollection()->load(); 
		  $user = array();
		  foreach($collection->getData() as $k => $val){
		      $user[$val['user_id']] = $val['username'];
		  }
		  return $user;
    }
    public function getUserProcessed($user_id){

        $user_data = Mage::getModel('admin/user')->load($user_id);
        if($user_data){
        	return $user_data->getUsername();
        }
       
		return '';
    }
    public function getUserId(){
        if(Mage::getSingleton('admin/session')->isLoggedIn()){
            $user_id = Mage::getSingleton('admin/session')->getUser()->getId();
        }else{
            $user_id = 0;
        }
        return $user_id;
    }
    public function getListUser($by_user){
    	  
          $adminUser = Mage::getModel('admin/user');
		  $collection = $adminUser->getCollection()->load(); 
		  $user = array();
		  $option ="";
		  foreach($collection->getData() as $k => $val){
		  	if($val['user_id'] == $by_user){
		  		$option .="<option value=\"{$val['user_id']}\" selected>{$val['username']}</option>";
		  	}else{
		  		$option .="<option value=\"{$val['user_id']}\" >{$val['username']}</option>";
		  	}
		  }
		  return $option;
    }
    public function allowChangeSalePerson(){
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/change_sales_person');
    }

}
