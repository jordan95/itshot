<?php
class OpenTechiz_UserProcessOrder_Model_Observer_ChangeUser{
    public function updateUser($observer){
    	$order = $observer->getEvent()->getOrder();
        $params = Mage::app()->getRequest()->getParams();
        if(isset($params['salesperson'])) {
            $order->setCreateOrderByUser($params['salesperson']);
        }
        return $this;
    }

}