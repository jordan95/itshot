<?php
class OpenTechiz_UserProcessOrder_Model_Observer_ProcessByUser{
    public function updateUser($observer){
        $order = $observer->getEvent()->getOrder();
        $stage = (int)$observer->getEvent()->getStage();
        $stagebefore = (int)$observer->getEvent()->getStageBefore();
        $internal_status = (int)$order->getInternalStatus();
        $internal_status_before = (int)$observer->getEvent()->getInternalStatusBefore();
        $user_id =(int) $this->getUserId();
        $current_user_id =(int) $order->getProcessedByUser();
        if(((!empty($user_id) && $stage <= 3 && ($stagebefore != $stage) && $stagebefore !==4) || (!empty($user_id) && $stage < 3 && ($internal_status_before != $internal_status))) && ($current_user_id != $user_id)){
            $order->setProcessedByUser($user_id);
            $order->save();
        }
    }
    public function getUserId(){
        if(Mage::getSingleton('admin/session')->isLoggedIn()){
            $user_id = Mage::getSingleton('admin/session')->getUser()->getId();
        }else{
            $user_id = 0;
        }
        return $user_id;
    }
}