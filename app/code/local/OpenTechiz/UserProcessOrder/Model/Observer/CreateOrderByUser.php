<?php
class OpenTechiz_UserProcessOrder_Model_Observer_CreateOrderByUser{
    public function updateUser($observer){
        $order = $observer->getEvent()->getOrder();
        $user_id = Mage::helper("opentechiz_userprocessorder")->getUserId();
        $order->setCreateOrderByUser($user_id)->save();
        return $this;
    }

}