<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    ALTER TABLE {$this->getTable('sales/order')} 
    ADD `create_order_by_user`
    INT(7)
    NULL
    DEFAULT NULL;
");
$installer->endSetup();