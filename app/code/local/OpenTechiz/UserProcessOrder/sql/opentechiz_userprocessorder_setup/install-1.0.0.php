<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    ALTER TABLE {$this->getTable('sales/order')} 
    ADD `processed_by_user`
    INT(7)
    NULL
    DEFAULT NULL;
");
$installer->endSetup();