<?php

class OpenTechiz_GtmExtension_Block_Gtm extends CueBlocks_GtmExtension_Block_Gtm {


    private $_storeId = 0;
    private $_pid = false;
    private $_pid_prefix = "";
    private $_pid_prefix_ofcp = 0;
    private $_pid_ending = "";
    private $_pid_ending_ofcp = 0;

    private function getEcommProdid($product) {
        $ecomm_prodid = (string) ($product->getId() ? $product->getId() : $product->getSku());
        $ofcp = false;
        if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE ||
            $product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_GROUPED) {
            $ofcp = true;
        }

        if (!empty($this->_pid_prefix) && (($this->_pid_prefix_ofcp === 1 && $ofcp) ||
                $this->_pid_prefix_ofcp === 0)) {
            $ecomm_prodid = $this->_pid_prefix . $ecomm_prodid;
        }

        if (!empty($this->_pid_ending) && (($this->_pid_ending_ofcp === 1 && $ofcp) ||
                $this->_pid_ending_ofcp === 0)) {
            $ecomm_prodid .= $this->_pid_ending;
        }

        return $ecomm_prodid;
    }

    protected function _getRemarketingData() {
        $params = array();
        $inclTax = false;
        if ((int) Mage::getStoreConfig('google/affiliate/gdrt_tax', $this->_storeId) === 1) {
            $inclTax = true;
        }

        $page_type = Mage::app()->getRequest()->getModuleName() . '-' . Mage::app()->getFrontController()->getRequest()->getControllerName();

        $params['ecomm_pagetype'] = 'other';
        switch ($page_type) {
            case 'cms-index':
                $params['ecomm_pagetype'] = 'home';
                break;

            case 'nsearch-index':
                $params['ecomm_pagetype'] = 'searchresults';
                break;

            case 'catalog-category':
                $category = Mage::registry('current_category');
                $params['ecomm_pagetype'] = 'category';
                $params['ecomm_category'] = (string) $category->getName();
                unset($category);
                break;

            case 'catalog-product':
                $product = Mage::registry('current_product');
                $totalvalue = Mage::helper('tax')->getPrice($product, $product->getFinalPrice(), $inclTax);

                $params['ecomm_prodid'] = $this->getEcommProdid($product);
                $params['ecomm_pagetype'] = 'product';
                $params['ecomm_totalvalue'] =  number_format($totalvalue, '2', '.', '');
                unset($product);
                break;

            case 'checkout-cart':
                $cart = Mage::getSingleton('checkout/session')->getQuote();
                $items = $cart->getAllVisibleItems();

                if (count($items) > 0) {
                    $data = array();
                    $totalvalue = 0;
                    foreach ($items as $item) {
                        $data[0][] = $this->getEcommProdid($item->getProduct());
                        $data[1][] = (int) $item->getQty();
                        $totalvalue += $inclTax ? $item->getRowTotalInclTax() : $item->getRowTotal();
                    }
                    $params['ecomm_prodid'] = $data[0];
                    $params['ecomm_pagetype'] = 'cart';
                    $params['ecomm_quantity'] = $data[1];
                    $params['ecomm_totalvalue'] =  number_format($totalvalue, '2', '.', '');
                } else
                    $params['ecomm_pagetype'] = 'siteview';

                unset($cart, $items, $item, $data);
                break;

            case 'onestepcheckout-index':
                if ('index' == $this->getRequest()->getActionName()) {
                    $cart = Mage::getSingleton('checkout/session')->getQuote();
                    $params['ecomm_pagetype'] = 'cart';
                } elseif ('success' == $this->getRequest()->getActionName()) {
                    $cart = Mage::getModel('sales/order')->loadByIncrementId(Mage::getSingleton('checkout/session')->getLastRealOrderId());
                    $params['ecomm_pagetype'] = 'purchase';
                }


                $items = $cart->getAllVisibleItems();

                if (count($items) > 0) {
                    $data = array();
                    $totalvalue = 0;

                    if ('index' == $this->getRequest()->getActionName()) {
                        foreach ($items as $item) {
                            $data[0][] = $this->getEcommProdid($item->getProduct());
                            $data[1][] = (int) $item->getQty();
                            $totalvalue += $inclTax ? $item->getRowTotalInclTax() : $item->getRowTotal();
                        }
                    } elseif ('success' == $this->getRequest()->getActionName()) {
                        foreach ($items as $item) {
                            $data[0][] = $this->getEcommProdid($item->getProduct());
                            $data[1][] = (int) $item->getData('qty_ordered');
                            $totalvalue += $inclTax ? $item->getRowTotalInclTax() : $item->getRowTotal();
                        }
                    }

                    $params['ecomm_prodid'] = $data[0];

                    $params['ecomm_quantity'] = $data[1];
                    $params['ecomm_totalvalue'] =  number_format($totalvalue, '2', '.', '');
                }
                unset($order, $items, $item);
                break;

            case 'checkout-onepage':
                if ('index' == $this->getRequest()->getActionName()) {
                    $cart = Mage::getSingleton('checkout/session')->getQuote();
                    $params['ecomm_pagetype'] = 'cart';
                } elseif ('success' == $this->getRequest()->getActionName()) {
                    $cart = Mage::getModel('sales/order')->loadByIncrementId(Mage::getSingleton('checkout/session')->getLastRealOrderId());
                    $params['ecomm_pagetype'] = 'purchase';
                }


                $items = $cart->getAllVisibleItems();

                if (count($items) > 0) {
                    $data = array();
                    $totalvalue = 0;

                    if ('index' == $this->getRequest()->getActionName()) {
                        foreach ($items as $item) {
                            $data[0][] = $this->getEcommProdid($item->getProduct());
                            $data[1][] = (int) $item->getQty();
                            $totalvalue += $inclTax ? $item->getRowTotalInclTax() : $item->getRowTotal();
                        }
                    } elseif ('success' == $this->getRequest()->getActionName()) {
                        foreach ($items as $item) {
                            $data[0][] = $this->getEcommProdid($item->getProduct());
                            $data[1][] = (int) $item->getData('qty_ordered');
                            $totalvalue += $inclTax ? $item->getRowTotalInclTax() : $item->getRowTotal();
                        }
                    }

                    $params['ecomm_prodid'] = $data[0];

                    $params['ecomm_quantity'] = $data[1];
                    $params['ecomm_totalvalue'] = number_format($totalvalue, '2', '.', '');
                }
                unset($order, $items, $item);
                break;

            case 'amlist-list':
                if(Mage::registry('current_list')) {
                    $wishlistItems = Mage::registry('current_list')->getItems();
                    if (count($wishlistItems) > 0) {
                        $data = array();
                        $totalvalue = 0;
                        foreach ($wishlistItems as $item) {
                            $data[0][] = $this->getEcommProdid($item->getProduct()); //get prod id

                            //get price total
                            $buyRequest = unserialize($item->getBuyRequest());
                            if(!isset($buyRequest['options'])){
                                $buyRequest['options'] = array();
                            }
                            if(array_key_exists('quotation_product_id', $buyRequest['options'])) {
                                $quoteProduct = Mage::getModel('opentechiz_quotation/product')->load($buyRequest['options']['quotation_product_id']);

                                $totalvalue += $quoteProduct->getQpPrice();
                            }else{
                                $totalvalue += $item->getPrice();
                            }
                        }

                        $params['ecomm_prodid'] = $data[0];
                        $params['ecomm_totalvalue'] = number_format($totalvalue, '2', '.', '');
                    }
                }
                break;

            default:
                break;
        }

        return $params;
    }



}
