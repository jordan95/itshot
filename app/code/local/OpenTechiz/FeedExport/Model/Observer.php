<?php

/**
 * OnlineBiz Software Solution
 * 
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Jordan Tran<jordan@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2020, OnlineBiz Software Solution
 * 
 * Create at: April 7, 2020 2:27:27 PM
 */
class OpenTechiz_FeedExport_Model_Observer
{

    public function Checkfeedexportrunning()
    {
        $lastest_date_generated = '';
        $collection = Mage::getModel('feedexport/feed')->load(1);
        $lastest_date_generated = $collection->getGeneratedAt();
        if($lastest_date_generated!=''){
            $lastest_time = strtotime($lastest_date_generated);
            $now_time = time();
            $second_time = $now_time - $lastest_time;
            if($second_time > 86400){
                Mage::dispatchEvent('maillog_send_email_to_admin', array(
                    'message' =>  "Sales feed were not updated.<br/><br/>Script ends at: ".date("d-m-Y H:i:s"),
                    'subject' => 'ItsHot.com: Cronjob Check Updating Google Feed Export'
                        )
                );
            }

        }
        
      
    }

}
