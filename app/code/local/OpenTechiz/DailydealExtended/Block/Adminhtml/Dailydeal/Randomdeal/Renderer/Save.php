<?php

class OpenTechiz_DailydealExtended_Block_Adminhtml_Dailydeal_Randomdeal_Renderer_Save extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        if($row->getSaveType() == 0){
            return sprintf("%s%%", $row->getSave());
        } else if ($row->getSaveType() == 1){
            $price = Mage::getModel('catalog/product')->load($row->getProductId())->getPrice();
            $save = round(($price - $row->getDealPrice())/ $price * 100);
            return sprintf("%s%%", $save);
        }
        return $save ;
    }

}
