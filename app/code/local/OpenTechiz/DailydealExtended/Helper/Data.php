<?php

class OpenTechiz_DailydealExtended_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function warmCache(Magestore_Dailydeal_Model_Dailydeal $dailydeal)
    {
        $id_path = array();
        $id_path[] = sprintf("product/%s", $dailydeal->getProductId());
        $categoryIds = $this->getCategoryIdsByProductIds($dailydeal->getProductId());
        foreach ($categoryIds as $cID){
            $id_path[] = sprintf("category/%s", $cID);
        }
        /* @var $urlRewriteCollection Mage_Core_Model_Resource_Url_Rewrite_Collection */
        $urlRewriteCollection = Mage::getModel('core/url_rewrite')
                ->getCollection()
                ->addFieldToFilter("store_id", $dailydeal->getStoreId())
                ->addFieldToFilter("id_path", $id_path);
        if ($urlRewriteCollection->count() == 0) {
            return;
        }
        $url = array();
        foreach ($urlRewriteCollection as $item){
            $url[] = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $item->getRequestPath();
        }
        $fpccrawlerCollection = Mage::getModel("fpccrawler/crawler_url")
                ->getCollection()
                ->addFieldToFilter("url", $url);
        if ($fpccrawlerCollection->count() == 0) {
            return;
        }
        foreach ($fpccrawlerCollection as $urlCrawler) {
            /* @var $urlCrawler OpenTechiz_FpcCrawler_Model_Crawler_Url */
            try {
                $urlCrawler->warmCache();
            } catch (Exception $exc) {
                Mage::logException($exc);
            }
        }

        //clean varnish cache on daily-deals page
        Mage::helper("opentechiz_fpccrawler")->purgeVarnish(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'daily-deals');
    }
    
    public function getTableName($table)
    {
        return $this->getResource()->getTableName($table);
    }
    
    public function getCategoryIdsByProductIds($pID)
    {
        $sql = 'SELECT DISTINCT(a.category_id)';
        $sql .= ' FROM ' . $this->getTableName('catalog_category_product') . ' AS a';
        $sql .= ' WHERE a.product_id = ' . $pID;
        return $this->queryIds($sql);
    }

    public function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    public function getConnection()
    {
        return $this->getResource()->getConnection('core_read');
    }

    public function queryIds($sql)
    {
        return $this->getConnection()->fetchCol($sql);
    }
}
