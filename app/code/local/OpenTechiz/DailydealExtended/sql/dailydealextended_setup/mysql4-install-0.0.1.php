<?php


$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('dailydeal/dailydeal'),'save_type', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'default'   => 0,
    'nullable'  => false,
    'length'    => 2,
    'before'     => 'save', // column name to insert new column after
    'comment'   => 'save type'
    ));   
$installer->endSetup();