<?php

class OpenTechiz_DailydealExtended_Model_Observer
{

    public function cleanDailydealCache(\Varien_Event_Observer $observer)
    {
        if (!Mage::helper('core')->isModuleEnabled('Mirasvit_Fpc') || !Mage::app()->useCache('fpc')) {
            return;
        }

        /* @var $dailydeal Magestore_Dailydeal_Model_Dailydeal */
        $dailydeal = $observer->getDailydeal();
        if (!$dailydeal->getId()) {
            return;
        }
        $key = "dailydeal_cache_product_" . $dailydeal->getProductId();
        if (Mage::registry($key)) {
            return;
        }
        $shouldWarmCache = false;
        if ($dailydeal->getOrigData("save") != $dailydeal->getData("save") ||
                $dailydeal->getOrigData("status") != $dailydeal->getData("status") ||
                $dailydeal->getOrigData("start_time") != $dailydeal->getData("start_time") ||
                $dailydeal->getOrigData("close_time") != $dailydeal->getData("close_time")
        ) {
            $shouldWarmCache = true;
        }
        if ($shouldWarmCache) {
            Mage::helper("dailydealextended")->warmCache($dailydeal);
        }
        Mage::register($key, true);
    }

    public function removeDailydealCache(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('core')->isModuleEnabled('Mirasvit_Fpc') || !Mage::app()->useCache('fpc')) {
            return;
        }
        $dailydeal = $observer->getDailydeal();
        Mage::helper("dailydealextended")->warmCache($dailydeal);
    }

}
