<?php

class OpenTechiz_DailydealExtended_Model_Cron_UpdateStatus
{

    public function run()
    {
        Mage::helper('dailydeal')->updateDailydealStatus();
    }

}
