<?php

class OpenTechiz_DailydealExtended_Model_Catalog_Product extends TBT_RewardsOnly_Model_Catalog_Product
{

    public function getFinalPrice($qty = null)
    {
        if (!Mage::helper('core')->isModuleEnabled('Magestore_Dailydeal')) {
            $finalPrice = parent::getFinalPrice($qty);
        }
        $save_dailydeal = $this->_getData('save_dailydeal');
        if ($save_dailydeal) {
            $price = $this->getPrice();
            $finalPrice = $price - $save_dailydeal * $price / 100;
            $this->setData('final_price', $finalPrice);
        } else {
            $finalPrice = parent::getFinalPrice($qty);
        }

        return $finalPrice;
    }

}
