<?php

require_once(Mage::getModuleDir('controllers', 'Magestore_Dailydeal') . DS . 'Adminhtml' . DS . 'DailydealController.php');

class OpenTechiz_DailydealExtended_Adminhtml_DailydealController extends Magestore_Dailydeal_Adminhtml_DailydealController
{

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            if (isset($_FILES['thumbnail']['name']) && $_FILES['thumbnail']['name'] != '') {
                try {
                    /* Starting upload */
                    $uploader1 = new Varien_File_Uploader('thumbnail');

                    // Any extention would work
                    $uploader1->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                    $uploader1->setAllowRenameFiles(false);

                    // Set the file upload mode 
                    // false -> get the file directly in the specified folder
                    // true -> get the file in the product like folders 
                    //	(file.jpg will go in something like /media/f/i/file.jpg)
                    $uploader1->setFilesDispersion(false);

                    // We set media as the upload dir
                    $path_thumbnail_image = Mage::getBaseDir('media') . DS;
                    $uploader1->save($path_thumbnail_image . 'dailydeal/main', $_FILES['thumbnail']['name']);
                } catch (Exception $e) {
                    
                }

                //this way the name is saved in DB
                $data['thumbnail_image'] = 'dailydeal/main/' . $_FILES['thumbnail']['name'];
            }
            if (isset($data['thumbnail'])) {
                $delete_thumbnail = ($data['thumbnail']);
                if ($delete_thumbnail['delete'] == 1) {
                    $data['thumbnail_image'] = null;
                }
            }
            if (isset($data['candidate_product_id']) && $data['candidate_product_id']) {
                $data['product_id'] = $data['candidate_product_id'];
            }

            if (isset($data['product_name']) && $data['product_name'] == '') {
                unset($data['product_name']);
            }
            $data = $this->_filterDateTime($data, array('start_time', 'close_time'));
            $model = Mage::getModel('dailydeal/dailydeal')->load($this->getRequest()->getParam('id'));

            try {
                $data['start_time'] = date('Y-m-d H:i:s', Mage::getModel('core/date')->gmtTimestamp(strtotime($data['start_time'])));
                $data['close_time'] = date('Y-m-d H:i:s', Mage::getModel('core/date')->gmtTimestamp(strtotime($data['close_time'])));
            } catch (Exception $e) {
                
            }

            $price = Mage::getModel('catalog/product')->load($data['product_id'])->getPrice();
            if($data['save_type'] == 0){
                $data['deal_price'] = $price - $data['save'] * $price / 100;
            }else if ($data['save_type'] == 1){
                $data['deal_price'] = $data['save'];
            }
            
            $data['status'] = $data['status_form'];
            $data['store_id'] = implode(',', $data['stores']);
            $model->setData($data)
                    ->setId($this->getRequest()->getParam('id'));
            try {
                if ($price >= $data['deal_price']) {
                    if (Mage::getModel('core/date')->timestamp($data['start_time']) <= Mage::getModel('core/date')->timestamp($data['close_time'])) {
                        $model->save();
                        if (!Mage::registry('is_random_dailydeal'))
                            Mage::helper('dailydeal')->updateDailydealStatus();
                        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('dailydeal')->__('Deal was successfully saved'));
                    }else {
                        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('dailydeal')->__('Start time must be smaller than close time!'));
                    }
                } else {
                    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('dailydeal')->__('Deal Price must be smaller than Product price'));
                }
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('dailydeal')->__('Unable to find deal to save'));
        $this->_redirect('*/*/');
    }

}
