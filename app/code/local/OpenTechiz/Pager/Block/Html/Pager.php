<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright  Copyright (c) 2006-2018 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Html page block
 *
 * @category   Mage
 * @package    Mage_Page
 * @author      Magento Core Team <core@magentocommerce.com>
 *
 * @todo        separate order, mode and pager
 */
class OpenTechiz_Pager_Block_Html_Pager extends Amasty_Shopby_Block_Catalog_Pager
{
    public function getBeforeMiddlePageNum()
    {
        $haft_total = $this->getMiddlePageNum();
        $collection = $this->getCollection();
        $total = $collection->getLastPageNumber();
        $current = $collection->getCurPage();
        $before_middle_link = floor(($haft_total + $current)/2);
        if($haft_total != 0){
            if(($before_middle_link > $current && $before_middle_link < $haft_total && $before_middle_link > $current+4)){
                return $before_middle_link;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
        
    }
    public function getMiddlePageNum()
    {
        $collection = $this->getCollection();
        $total = $collection->getLastPageNumber();
        $current = $collection->getCurPage();
        $middle_link = floor(($current + $total)/2);

        if(($middle_link > $current && $middle_link <$total && $middle_link > $current+4)){
            return $middle_link;
        }else{
            return 0;
        }

    }
    public function getAfterMiddlePageNum()
    {        
        $haft_total = $this->getMiddlePageNum();
        $collection = $this->getCollection();
        $total = $collection->getLastPageNumber();
        $current = $collection->getCurPage();
        $after_middle_link = floor(($haft_total + $total)/2);
        if($haft_total != 0){
            if(($after_middle_link > $haft_total && $after_middle_link < $total && $after_middle_link > $current+4)){
                return $after_middle_link;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }

}
