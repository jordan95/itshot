<?php

class OpenTechiz_ImageOptimizerExtended_Helper_Apptrian_Data extends Apptrian_ImageOptimizer_Helper_Data
{

    public function clearIndex()
    {
        $r = true;
        try {
            Mage::helper('feedexport/io')->write($this->getIndexPath(), '');
        } catch (Exception $exc) {
            $r = false;
        }
        return $r;
    }

    /**
     * Optimization process.
     *
     * @return boolean
     */
    public function optimize()
    {
        $this->loadIndex();

        // Get Batch Size
        $batchSize = (int) $this->getConfig(
            'apptrian_imageoptimizer/general/batch_size'
        );

        // Get array of files for optimization limited by batch size
        $files = $this->getFiles($batchSize);

        $id          = '';
        $item        = array();
        $toUpdate    = array();
        $encodedPath = '';
        $decodedPath = '';
        $filePath    = '';
        $baseDir = $this->getBaseDir();
        $isCloudfrontEnabled = $this->_isCloudfrontEnabled();
        $toInvalidate = [];

        // Optimize batch of files
        foreach ($files as $id => $item) {
            $encodedPath = $item['f'];
            $decodedPath = utf8_decode($encodedPath);
            if (substr($decodedPath, 0, 1) !== '/') {
                $decodedPath = $baseDir . DS . $decodedPath;
            }
            $filePath    = realpath($decodedPath);

            // If image exists, optimize else remove it from database
            if (file_exists($filePath)) {
                if ($this->optimizeFile($filePath)) {
                    $toUpdate[$id]['f'] = $encodedPath;
                    if ($isCloudfrontEnabled) {
                        $toInvalidate[] = $encodedPath;
                    }
                }
            } else {
                // Remove files that do not exist anymore from the index
                unset($this->_index[$id]);
            }
        }

        $i = '';
        $f = array();

        // Itereate over $toUpdate array and set modified time
        // (mtime) takes a split second to update
        foreach ($toUpdate as $i => $f) {
            $encodedPath = $f['f'];
            $decodedPath = utf8_decode($encodedPath);
            if (substr($decodedPath, 0, 1) !== '/') {
                $decodedPath = $baseDir . DS . $decodedPath;
            }
            $filePath    = realpath($decodedPath);

            if (file_exists($filePath)) {
                // Update optimized file information in index
                $this->_index[$i]['t'] = filemtime($filePath);
            }

            // Free Memory
            unset($toUpdate[$i]);
        }

        if ($toInvalidate) {
            $this->_invalidateFiles($toInvalidate);
        }

        return $this->saveIndex();
    }

    protected function _isCloudfrontEnabled()
    {
        return class_exists('Mgt_CloudfrontInvalidation_Helper_Data') &&
            Mgt_CloudfrontInvalidation_Helper_Data::isEnabled() &&
            Mage::getStoreConfig('apptrian_imageoptimizer/general/invalidate_cloudfront');
    }

    protected function _invalidateFiles($filePaths)
    {
        $relativePath = trim(Mage::getStoreConfig('mgt_cloudfront_invalidation/mgt_cloudfront_invalidation/base_path'));
        if ($relativePath) {
            $relativePath = rtrim($relativePath, '/') . DS;
            $regExp = str_replace('/', '\/', $relativePath);
            foreach ($filePaths as $key => $path) {
                if (strpos($path, $relativePath) === 0) {
                    $filePaths[$key] = preg_replace("/^$regExp/", '', $path);
                }
            }
        }
        Mage::getSingleton('mgt_cloudfront_invalidation/cloudfront')->invalidate($filePaths);
    }

    /**
     * Scan and reindex process.
     *
     * @return boolean
     */
    public function scanAndReindex()
    {
        $this->loadIndex();

        $id          = '';
        $item        = array();
        $encodedPath = '';
        $decodedPath = '';
        $filePath    = '';
        $baseDir = $this->getBaseDir();

        // Check index for files that need to be updated and/or removed
        foreach ($this->_index as $id => $item) {
            $encodedPath = $item['f'];
            $decodedPath = utf8_decode($encodedPath);
            if (substr($decodedPath, 0, 1) !== '/') {
                $decodedPath = $baseDir . DS . $decodedPath;
            }
            $filePath    = realpath($decodedPath);

            if (file_exists($filePath)) {
                if ($item['t'] != 0 && filemtime($filePath) != $item['t']) {
                    // Update time to 0 in index so it can be optimized again
                    $this->_index[$id]['t'] = 0;
                }
            } else {
                // Remove files that do not exist anymore from the index
                unset($this->_index[$id]);
            }
        }

        $paths    = $this->getPaths();
        $path     = '';

        // Scan for new files and add them to the index
        foreach ($paths as $path) {
            $this->scanAndReindexPath($path);
        }

        return $this->saveIndex();
    }

    /**
     * @return array
     */
    public function getExcludePaths()
    {
        $paths = array();

        $pathsString = trim(
            trim($this->getConfig('apptrian_imageoptimizer/general/exclude_paths'), ';')
        );

        $rawPaths = explode(';', $pathsString);

        foreach ($rawPaths as $p) {
            $trimmed = trim(trim($p), '/');
            $dirs = explode('/', $trimmed);
            $paths[] = implode(DS, $dirs);
        }

        return array_unique($paths);
    }

    /**
     * Scans provided path for images and adds them to index.
     *
     * @param string $path
     */
    public function scanAndReindexPath($path)
    {
        $id          = '';
        $encodedPath = '';
        $filePath    = '';
        $file        = null;
        $baseDir = $this->getBaseDir() . DS . $path;
        $realBaseDir = realpath($baseDir);
        $excludePaths = [];
        foreach ($this->getExcludePaths() as $excludePath) {
            if (strpos($excludePath, $path) === 0) {
                $excludePaths[] = $this->getBaseDir() . DS . $excludePath;
            }
        }

        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator(
                $baseDir,
                RecursiveDirectoryIterator::FOLLOW_SYMLINKS
            )
        );

        foreach ($iterator as $file) {
            if ($file->isFile()
                && preg_match(
                    '/^.+\.(jpe?g|gif|png)$/i',
                    $file->getFilename()
                )
            ) {
                foreach ($excludePaths as $excludePath) {
                    if (strpos($file->getPath(), $excludePath) === 0) {
                        continue 2;
                    }
                }
                $filePath = $file->getRealPath();
                if (!is_writable($filePath)) {
                    continue;
                }

                $relativePath = $path . DS . str_replace($realBaseDir . DS, '', $filePath);
                $encodedPath = utf8_encode($relativePath);
                $id          = md5($encodedPath);

                // Add only if file is not already in the index
                if (!isset($this->_index[$id])) {
                    $this->_index[$id] = array('f' => $encodedPath, 't' => 0);
                }
            }

            // Free Memory
            $file = null;
        }

        // Free Memory
        $iterator = null;
    }
    public function invalidateFromScript($filePaths)
    {
        $this->_invalidateFiles($filePaths);
    }

}
