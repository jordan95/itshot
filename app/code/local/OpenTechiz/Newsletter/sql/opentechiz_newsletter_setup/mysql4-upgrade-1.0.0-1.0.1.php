<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();
$table = $installer->getTable('newsletter_subscriber');
   $installer->run("
        ALTER TABLE `$table`
            ADD COLUMN (`flag_check_update`  tinyint  NULL DEFAULT 0)
    ");

$installer->endSetup();