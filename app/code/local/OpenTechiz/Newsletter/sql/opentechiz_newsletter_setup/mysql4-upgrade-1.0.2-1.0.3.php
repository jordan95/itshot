<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();
$table = $installer->getTable('newsletter_subscriber');
   $installer->run("
        ALTER TABLE `$table`
            ADD COLUMN (`segment_general_interest` INT(2) DEFAULT 0),
            ADD COLUMN (`segment_bridal_jewelry` INT(2) DEFAULT 0),
            ADD COLUMN (`segment_watches` INT(2) DEFAULT 0),
            ADD COLUMN (`segment_men_jewelry` INT(2) DEFAULT 0),
            ADD COLUMN (`segment_designers` INT(2) DEFAULT 0),
            ADD COLUMN (`segment_hip_hop_jewelry` INT(2) DEFAULT 0),
            ADD COLUMN (`segment_color_diamond_jewelry` INT(2) DEFAULT 0),
            ADD COLUMN (`segment_dailydeal` INT(2) DEFAULT 0),
            ADD COLUMN (`segment_weekly_deals` INT(2) DEFAULT 0),
            ADD COLUMN (`segment_women_jewelry` INT(2) DEFAULT 0)     
    ");

$installer->endSetup();