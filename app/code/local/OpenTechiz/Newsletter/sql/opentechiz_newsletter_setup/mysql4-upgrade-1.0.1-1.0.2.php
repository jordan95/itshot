<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();
$table = $installer->getTable('newsletter_subscriber');
   $installer->run("
        ALTER TABLE `$table`
            ADD COLUMN (`birthday` TIMESTAMP  NULL),
            ADD COLUMN (`anniversary` TIMESTAMP  NULL),
            ADD COLUMN (`telephone` VARCHAR(50)  NULL)
    ");

$installer->endSetup();