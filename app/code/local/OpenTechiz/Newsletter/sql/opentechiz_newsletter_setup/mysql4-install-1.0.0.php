<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();
$table = $installer->getTable('newsletter_subscriber');
   $installer->run("
        ALTER TABLE `$table`
            ADD COLUMN (`segment_name` varchar(255) NULL)
    ");

$installer->endSetup();