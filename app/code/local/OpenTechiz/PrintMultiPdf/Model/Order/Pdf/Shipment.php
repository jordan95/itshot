<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2020 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Sales Order Shipment PDF model
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class OpenTechiz_PrintMultiPdf_Model_Order_Pdf_Shipment extends Mage_Sales_Model_Order_Pdf_Shipment
{
    /**
     * Draw table header for product items
     *
     * @param  Zend_Pdf_Page $page
     * @return void
     */
     public $rectangle;
    protected function _drawHeader(Zend_Pdf_Page $page)
    {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.7));
        $page->setLineWidth(1);
        $page->drawRectangle(25, $this->y, 145, $this->y-25);
        $page->drawRectangle(145, $this->y, 175, $this->y-25);
        $page->drawRectangle(175, $this->y, 425, $this->y-25);
        $page->drawRectangle(425, $this->y, 570, $this->y-25);
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_setFontBold($page, 12);
        $page->drawText(Mage::helper('sales')->__('Qty'), 150, ($this->y - 17), 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('Product'), 250, ($this->y - 17), 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('SKU'), 480, ($this->y - 17), 'UTF-8');
        $this->y -= 25;


        //columns headers
        // $lines[0][] = array(
        //     'text' => Mage::helper('sales')->__('Products'),
        //     'feed' => 100,
        // );

        // $lines[0][] = array(
        //     'text'  => Mage::helper('sales')->__('Qty'),
        //     'feed'  => 35
        // );

        // $lines[0][] = array(
        //     'text'  => Mage::helper('sales')->__('SKU'),
        //     'feed'  => 565,
        //     'align' => 'right'
        // );

        // $lineBlock = array(
        //     'lines'  => $lines,
        //     'height' => 10
        // );

        // $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->y -= 20;
    }

    /**
     * Return PDF document
     *
     * @param  Mage_Sales_Model_Order_Shipment[] $shipments
     * @return Zend_Pdf
     */
    public function getPdf($shipments = array())
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('shipment');
        $width = 120;
        $height = 120;
        
        $pdf = new Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);
        foreach ($shipments as $shipment) {
            if ($shipment->getStoreId()) {
                Mage::app()->getLocale()->emulate($shipment->getStoreId());
                Mage::app()->setCurrentStore($shipment->getStoreId());
            }
            $page  = $this->newPage();
            $order = $shipment->getOrder();
            /* Add image */
            //$this->insertLogo($page, $shipment->getStore());
            /* Add address */
            // $this->insertAddress($page, $shipment->getStore());
            /* Add head */
            $this->insertOrder(
                $page,
                $shipment,
                Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_SHIPMENT_PUT_ORDER_ID, $order->getStoreId())
            );
            /* Add document text and number */
            $this->insertDocumentNumber(
                $page,
                Mage::helper('sales')->__('Packingslip # ') . $shipment->getIncrementId()
            );
            /* Add table */
            $this->_drawHeader($page);
            /* Add body */
            $totalNumberItems = 0;
            foreach ($shipment->getAllItems() as $item) {
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }
                $product = Mage::getModel('catalog/product')->load($item->getProductId());
                if($product->getIsVirtual() || !$product->getId() || (int)$item->getQty() == 0) continue;
                $totalNumberItems += (int)$item->getQty();
                /* Draw item */
                $locate_img = $this->y;
                $this->_drawItem($item, $page, $order);
                /* Draw product image */
                $product->setSku($item->getSku());
                $this->insertImage($product, 35, (int)($locate_img - 95), 135, (int)($locate_img+5), $width, $height, $page);
                $page->drawText(Mage::helper('sales')->__('Packaged By......................'), 435, ($locate_img - 70), 'UTF-8');
                $page = end($pdf->pages);
                
            }
            //add comment customer
            if($order->getOnestepcheckoutCustomercomment() !=''){
                $customerComment = Mage::helper('sales')->__('Customer Comment: ') . $order->getOnestepcheckoutCustomercomment();
                $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
                $page->setLineWidth(1);
                $page->drawRectangle(25, $this->rectangle+1, 570, $this->rectangle-30);
                $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
                $this->_setFontBold($page, 10);
                $page->drawText($customerComment, 35, ($this->rectangle -17), 'UTF-8');
                $this->_setFontRegular($page, 10);
                $page->drawText(Mage::helper('sales')->__('Total # of Items: '.$totalNumberItems), 480, ($this->rectangle - 70), 'UTF-8');
                
                $this->_setFontItalic($page, 8);
                $page->drawText(Mage::helper('sales')->__('Return Policy:'), 30, ($this->rectangle - 100), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('All items must be returned unworn, accompanied by the original invoice, packaging and un-detached tags within 30 days from shipping. Please Note: '), 30, ($this->rectangle - 115), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('Custom Orders are not returnable and not refundable. Please call us at 212-398-3123 Mon-Fri 10am-7pm EST to get a Return For Inspection (RFI)'), 30, ($this->rectangle - 130), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('number prior to returning the merchandise. Packages without RFI numbers will not be accepted. Engraved and Sized Items are not returnable.'), 30, ($this->rectangle - 145), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('(Items are considered sized if the standard size has been changed to either lesser or greater. Women’s Standard Ring Size is 7-7.5; Men’s Standard'), 30, ($this->rectangle - 160), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('Ring Size is 10).'), 30, ($this->rectangle - 175), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('You will receive a full refund minus the shipping and handling charges. Shipping and handling charges are not refundable.  Refund will be issued'), 30, ($this->rectangle - 190), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('in the form of original payment (bank wire transfers will be refunded by check). All exchanges are final and are not returnable.'), 30, ($this->rectangle - 205), 'UTF-8');

            }else{
                $page->drawText(Mage::helper('sales')->__('Total # of Items: '.$totalNumberItems), 480, ($this->rectangle - 40), 'UTF-8');
                $this->_setFontItalic($page, 8);
                $page->drawText(Mage::helper('sales')->__('Return Policy:'), 30, ($this->rectangle - 70), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('All items must be returned unworn, accompanied by the original invoice, packaging and un-detached tags within 30 days from shipping. Please Note: '), 30, ($this->rectangle - 85), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('Custom Orders are not returnable and not refundable. Please call us at 212-398-3123 Mon-Fri 10am-7pm EST to get a Return For Inspection (RFI)'), 30, ($this->rectangle - 100), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('number prior to returning the merchandise. Packages without RFI numbers will not be accepted. Engraved and Sized Items are not returnable.'), 30, ($this->rectangle - 115), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('(Items are considered sized if the standard size has been changed to either lesser or greater. Women’s Standard Ring Size is 7-7.5; Men’s Standard'), 30, ($this->rectangle - 130), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('Ring Size is 10).'), 30, ($this->rectangle - 145), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('You will receive a full refund minus the shipping and handling charges. Shipping and handling charges are not refundable.  Refund will be issued'), 30, ($this->rectangle - 160), 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('in the form of original payment (bank wire transfers will be refunded by check). All exchanges are final and are not returnable.'), 30, ($this->rectangle - 175), 'UTF-8');

            }
            
            
            
        }

        
        $this->_afterGetPdf();
        if ($shipment->getStoreId()) {
            Mage::app()->getLocale()->revert();
        }
        return $pdf;
    }


    protected function insertImage($image, $x1, $y1, $x2, $y2, $width, $height, &$page)
    {
        if (!is_null($image)) {
            try{
                $width = (int) $width;
                $height = (int) $height;

                //Get product image and resize it
                
                $imagePath = Mage::helper('catalog/image')->init($image, 'thumbnail')->resize($width, $height);
                
                if(Mage::getBaseUrl() =="https://www.itshot.com/"){
                    $imageLocation = substr($imagePath,strlen(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA)));
                    $imageLocation = "media/".$imageLocation;
                }else{
                    $imageLocation = substr($imagePath,strlen(Mage::getBaseUrl()));

                }
                $image = Zend_Pdf_Image::imageWithPath($imageLocation);

                //Draw image to PDF
                $page->drawImage($image, $x1, $y1, $x2, $y2);
            }
            catch (Exception $e) {
                return false;
            }
        }
    }
     protected function insertOrder(&$page, $obj, $putOrderId = true)
    {
        if ($obj instanceof Mage_Sales_Model_Order) {
            $shipment = null;
            $order = $obj;
        } elseif ($obj instanceof Mage_Sales_Model_Order_Shipment) {
            $shipment = $obj;
            $order = $shipment->getOrder();
        }

        $this->y = $this->y ? $this->y : 815;
        $top = $this->y;

   
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->setDocHeaderCoordinates(array(25, $top, 570, $top - 55));
        $this->_setFontRegular($page, 10);

        if ($putOrderId) {
            $page->drawText(
                Mage::helper('sales')->__('Order # ') . $order->getRealOrderId(),
                25,
                ($top -= 30),
                'UTF-8'
            );
        }
        $page->drawText(
            Mage::helper('sales')->__('Order Date: ') . Mage::helper('core')->formatDate(
                $order->getCreatedAtStoreDate(),
                'medium',
                false
            ),
            25,
            ($top -= 15),
            'UTF-8'
        );

        $top -= 10;

        $this->_setFontBold($page, 12);
        
            $this->y -= 65;
      
    }
    public function insertDocumentNumber(Zend_Pdf_Page $page, $text)
    {
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, 10);
        $docHeader = $this->getDocHeaderCoordinates();
        $page->drawText($text, 25, $docHeader[1] - 15, 'UTF-8');
    }
   
}
