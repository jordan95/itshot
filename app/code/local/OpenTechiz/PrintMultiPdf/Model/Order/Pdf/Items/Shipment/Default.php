<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2020 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Sales Order Shipment Pdf default items renderer
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class OpenTechiz_PrintMultiPdf_Model_Order_Pdf_Items_Shipment_Default extends Mage_Sales_Model_Order_Pdf_Items_Shipment_Default
{
    /**
     * Draw item line
     */
    public function draw()
    {
        $order  = $this->getOrder();
        $item   = $this->getItem();
        $pdf    = $this->getPdf();
        $page   = $this->getPage();
        $lines  = $top_lines = $line_engraving = array();
        $product_name_height = 0;
        $product_option_height = 0;
        $product_option_name = array();
        // draw Product name
        $top_lines[0][] = array(
            'text' => Mage::helper('core/string')->str_split($item->getName(), 46, true, true),
            'feed' => 185,
        );

        // draw QTY
        $top_lines[0][] = array(
            'text'  => $item->getQty()*1,
            'feed'  => 155
        );
        // draw SKU
        $top_lines[0][] = array(
            'text'  => Mage::helper('core/string')->str_split($this->getSku($item), 25),
            'feed'  => 435,
            'align' => 'left'
        );
       
        $top =$this->_pdf->y;
        // Custom options
        $options = $this->getItemOptions();
        if ($options) {
            foreach ($options as $option) {
                // draw options label

               $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
               $width = $this->widthForStringUsingFontSize($option['label'],$font,12);
               $position = $width + 185;  //postion value option
                if ($option['value']) {
                    if (isset($option['print_value'])) {
                        $_printValue = $option['print_value'];
                    } else {
                        $_printValue = strip_tags($option['value']);
                    }
                    if($option['label'] == "Engraving" || $option['label'] == "Engraving Text"){
                        $line_engraving[] = array(
                            array(
                                'text' => $option['label'].': ',
                                'font' => 'regular',
                                'feed' => 185,
                                'font' => 'bold'
                            ),
                            array(
                                'text' => $_printValue,
                                'font' => 'regular',
                                'feed' => $position
                            )
                        );
                        $product_option_name[] = $option['label'];
                    }else{
                        $lines[] = array(
                            array(
                                'text' => $option['label'].': ',
                                'font' => 'regular',
                                'feed' => 185,
                                'font' => 'bold'
                            ),
                            array(
                                'text' => $_printValue,
                                'font' => 'regular',
                                'feed' => $position
                            )
                        );
                        $product_option_name[] = $option['label'];
                    }
                    
                    
                }

            }
        }       
        
        $product_option_height = $this->_calcAddressHeight($product_option_name);
        $product_name_height = $this->_calcAddressHeight(array($item->getName()));
        $total_height = $product_option_height + $product_name_height + 10;
        
        if(!empty($line_engraving)){
            $total_height += 10;
        }
        // 110 is height of row
        if((110 - $total_height) < 10 && (110 - $total_height >=-10)){
            $total_height +=10;
        }
        $max_height = max(110, $total_height);
        $positionY = $max_height - $total_height;

        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $page->setLineWidth(1);
        // draw Rectangle columns in row
        $page->drawRectangle(25, $top+20, 145, $top-$max_height);
        $page->drawRectangle(145, $top+20, 175, $top-$max_height);
        $page->drawRectangle(175, $top+20, 425, $top-$max_height);
        $page->drawRectangle(425, $top+20, 570, $top-$max_height);
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_pdf->rectangle = $top-$max_height;
        $lineBlock = array(
            'lines'  => $top_lines,
            'height' => 15
        );
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $page = $pdf->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        $this->_pdf->y -= 10; //create space between product title with option name
         $lineBlock1 = array(
            'lines'  => $lines,
            'height' => 15
        );
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $page = $pdf->drawLineBlocks($page, array($lineBlock1), array('table_header' => true));
        if(!empty($line_engraving)){
            $this->_pdf->y -= 10; //create space between option engaving with another option
             $lineBlock_end = array(
                'lines'  => $line_engraving,
                'height' => 15
            );
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $page = $pdf->drawLineBlocks($page, array($lineBlock_end), array('table_header' => true));
        }
        $positionY = $top - $max_height - $this->_pdf->y - 20;
        $this->_pdf->y += $positionY;
        $this->setPage($page);
    }
    public function widthForStringUsingFontSize($string, $font, $fontSize)
    {
         $drawingString = iconv('UTF-8', 'UTF-16BE//IGNORE', $string);
         $characters = array();
         for ($i = 0; $i < strlen($drawingString); $i++) {
             $characters[] = (ord($drawingString[$i++]) << 8 ) | ord($drawingString[$i]);
         }
         $glyphs = $font->glyphNumbersForCharacters($characters);
         $widths = $font->widthsForGlyphs($glyphs);
         $stringWidth = (array_sum($widths) / $font->getUnitsPerEm()) * $fontSize;
         return $stringWidth;
    }
    protected function _calcAddressHeight($address)
    {
        $y = 0;
        foreach ($address as $value) {
            if ($value !== '') {
                $text = array();
                foreach (Mage::helper('core/string')->str_split($value, 55, true, true) as $_value) {
                    $text[] = $_value;
                }
                foreach ($text as $part) {
                    $y += 15;
                }
            }
        }
        return $y;
    }
}
