<?php

class OpenTechiz_Affirm_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getiDevOSCheckoutUrl()
    {
        $paramHttps = (Mage::app()->getStore()->isCurrentlySecure()) ? array('_forced_secure' => true) : array();
        return Mage::getUrl('onestepcheckout/index/index/form_key/' . Mage::getSingleton('core/session')->getFormKey(), $paramHttps);
    }

}
