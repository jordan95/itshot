<?php

class OpenTechiz_SearchOptimizes_Model_Catalogsearch_Resource_Fulltext_Collection extends Mirasvit_SearchIndex_Model_Catalogsearch_Resource_Fulltext_Collection{
    public function addSearchFilter($query)
    {
        $this->_isSearchFiltersApplied = true; 
        return parent::addSearchFilter($query);
    }
    
    protected function _addRelevanceSorting()
    {
        return $this;
    }
}