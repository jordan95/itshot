<?php

class OpenTechiz_SearchOptimizes_Helper_Query extends Mirasvit_Misspell_Helper_Query
{

    public function getCurrentPhase()
    {
        return $this->getQueryText();
    }

    public function getMisspellPhase()
    {
        return strip_tags(Mage::app()->getFrontController()->getRequest()->getParam('o'));
    }

    public function getFallbackPhase()
    {
        return strip_tags(Mage::app()->getFrontController()->getRequest()->getParam('f'));
    }

    public function getQueryText()
    {
        return Mage::helper('catalogsearch')->getQueryText();
    }

    /**
     * @param string $phase
     * @param int|null $storeId
     * @return false|string
     */
    public function suggestFallbackPhase($phase, $storeId = null)
    {
        $wordsCount = (int) Mage::getStoreConfig('misspell/general/words_count');
        if ($wordsCount && $wordsCount < count(explode(' ', $phase))) {
            return false;
        }
        return parent::suggestFallbackPhase($phase, $storeId);
    }
}
