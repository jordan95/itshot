<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Productupdates
 * @version    2.0.8
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class OpenTechiz_Productupdates_Model_Observer extends AW_Productupdates_Model_Observer
{

    public function updateStockIndex($observer)
    {
        $product = $observer->getProduct();
        if (!$product->getStockData('is_in_stock')) {
            if($product->getStockData('is_in_stock') ==''){
                $stock_status = $this->getStockStatus($product->getId()) ? 1 : 0;
                Mage::getResourceModel('productupdates/catalogrule')->updateInventoryStockRow(
                    $product->getId(),
                    $stock_status
                );
            }else{
                Mage::getResourceModel('productupdates/catalogrule')->updateInventoryStockRow(
                    $product->getId(),
                    $product->getStockData('is_in_stock')
                );
            }
        }
    }
    public function getStockStatus($productId)
    {
        $_tablestockIndex = Mage::getSingleton('core/resource')->getTableName('productupdates/inventoryindex');
        $query = "Select * from `{$_tablestockIndex}` WHERE `{$_tablestockIndex}`.`product_id` = {$productId}";
        $read = Mage::getSingleton('core/resource')->getConnection('core_read'); 
        $collection = $read->fetchAll($query);
        $stock_status = '';
        foreach($collection as $key => $data)
        {
            $stock_status = $data['stock_status'];
        }

        return $stock_status;
    }

}