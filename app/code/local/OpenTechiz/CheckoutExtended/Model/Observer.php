<?php

class OpenTechiz_CheckoutExtended_Model_Observer
{

    public function removeSpaceCouponField(Varien_Event_Observer $observer)
    {
        $coupon_code = Mage::app()->getRequest()->getParam('coupon_code');
        if ($coupon_code) {
            Mage::app()->getRequest()->setParam('coupon_code', preg_replace('/\s+/', '', $coupon_code));
        }
    }

    public function setBeforeAuthUrl(Varien_Event_Observer $observer)
    {
        Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::helper('core/url')->getCurrentUrl());
    }

}
