<?php
if(class_exists('TBT_Rewards_Model_Ajaxcartpro_Observer')) {
class OpenTechiz_CheckoutExtended_Model_Ajaxcartpro_Observer extends TBT_Rewards_Model_Ajaxcartpro_Observer
{

    public function beforeRenderLayout($observer)
    {
        $request = Mage::app()->getFrontController()->getRequest();
        if ($request->getParam('awacp', false)) {
            $layout = Mage::app()->getFrontController()->getAction()->getLayout();
            $response = Mage::getModel('ajaxcartpro/response');

            $parts = $request->getParam('block');
            if (is_array($parts)) {
                try {
                    $actionData = Zend_Json::decode(stripslashes($request->getParam('actionData', '[]')));
                    $renderer = Mage::getModel('ajaxcartpro/renderer')->setActionData($actionData);
                    $html = $renderer->renderPartsFromLayout($layout, $parts);
                    $response->setBlock($html);
                } catch (AW_Ajaxcartpro_Exception $e) {
                    $response->addError($e->getMessage());
                } catch (Exception $e) {
                    $response->addError("An error occurred while processing your request");
                    Mage::logException($e);
                }
            }
            $this->_sendResponse($response);
        }
    }

}
}