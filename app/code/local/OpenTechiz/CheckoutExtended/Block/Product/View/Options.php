<?php

class OpenTechiz_CheckoutExtended_Block_Product_View_Options extends Mage_Catalog_Block_Product_View_Options
{

    public function getOptionHtml(Mage_Catalog_Model_Product_Option $option)
    {
        $renderer = $this->getOptionRender(
                $this->getGroupOfOption($option->getType())
        );
        if (is_null($renderer['renderer'])) {
            $renderer['renderer'] = $this->getLayout()->createBlock($renderer['block'])
                    ->setTemplate($renderer['template']);
        }
        return $renderer['renderer']
                        ->setProduct($this->getProduct())
                        ->setOption($option)
                        ->setQuoteItem($this->getQuoteItem())
                        ->toHtml();
    }

}
