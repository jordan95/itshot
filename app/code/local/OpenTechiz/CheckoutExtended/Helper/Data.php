<?php

class OpenTechiz_CheckoutExtended_Helper_Data extends Mage_Checkout_Helper_Data
{

    public function sendPaymentFailedEmail($checkout, $message, $checkoutType = 'onepage')
    {
        $translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);

        $mailTemplate = Mage::getModel('core/email_template');
        /* @var $mailTemplate Mage_Core_Model_Email_Template */

        $template = Mage::getStoreConfig('checkout/payment_failed/template', $checkout->getStoreId());

        $copyTo = $this->_getEmails('checkout/payment_failed/copy_to', $checkout->getStoreId());
        $copyMethod = Mage::getStoreConfig('checkout/payment_failed/copy_method', $checkout->getStoreId());
        if ($copyTo && $copyMethod == 'bcc') {
            $mailTemplate->addBcc($copyTo);
        }

        $_reciever = Mage::getStoreConfig('checkout/payment_failed/reciever', $checkout->getStoreId());
        $sendTo = array(
            array(
                'email' => Mage::getStoreConfig('trans_email/ident_' . $_reciever . '/email', $checkout->getStoreId()),
                'name' => Mage::getStoreConfig('trans_email/ident_' . $_reciever . '/name', $checkout->getStoreId())
            )
        );

        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $sendTo[] = array(
                    'email' => $email,
                    'name' => null
                );
            }
        }
        $shippingMethod = '';
        if ($shippingInfo = $checkout->getShippingAddress()->getShippingMethod()) {
            $data = explode('_', $shippingInfo);
            $shippingMethod = $data[0];
        }

        $paymentMethod = '';
        if ($paymentInfo = $checkout->getPayment()) {
            $paymentMethod = $paymentInfo->getMethod();
        }

        $items = '';
        foreach ($checkout->getAllVisibleItems() as $_item) {
            /* @var $_item Mage_Sales_Model_Quote_Item */
            $items .= $_item->getProduct()->getName() . '  x ' . $_item->getQty() . '  '
                    . $checkout->getStoreCurrencyCode() . ' '
                    . $_item->getProduct()->getFinalPrice($_item->getQty()) . "\n";
        }
        $total = $checkout->getStoreCurrencyCode() . ' ' . $checkout->getGrandTotal();
        $client_info = $this->client_info();
        foreach ($sendTo as $recipient) {
            $dataCombine = array(
                'reason' => $message,
                'checkoutType' => $checkoutType,
                'dateAndTime' => Mage::app()->getLocale()->date(),
                'customer' => Mage::helper('customer')->getFullCustomerName($checkout),
                'customerEmail' => $checkout->getCustomerEmail(),
                'billingAddress' => $checkout->getBillingAddress(),
                'shippingAddress' => $checkout->getShippingAddress(),
                'shippingMethod' => Mage::getStoreConfig('carriers/' . $shippingMethod . '/title'),
                'paymentMethod' => Mage::getStoreConfig('payment/' . $paymentMethod . '/title'),
                'items' => nl2br($items),
                'total' => $total,
            );
            if ($client_info) {
                $dataCombine['clientInfo'] = $client_info;
            }
            $dataCombine['IP'] = $this->get_client_ip();
            $mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $checkout->getStoreId()))
                    ->sendTransactional(
                            $template, Mage::getStoreConfig('checkout/payment_failed/identity', $checkout->getStoreId()), $recipient['email'], $recipient['name'], $dataCombine
            );
        }

        $translate->setTranslateInline(true);

        return $this;
    }

    protected function client_info() {
        $browser = Mage::getModel('opentechiz/browser');
        return $browser->__toString();
    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    
    public function quoteIsPartialPayment() {
        return Mage::helper('partialpayment/partialpayment')->isEnabled() && Mage::getModel('partialpayment/calculation')->getAmountToBePaidLater();
    }
    
    public function getAvaiablePaymentMethodLayaway() {
        $payments_config = Mage::getStoreConfig('partialpayment/general_settings/avaiable_payment_methods');
        if(!$payments_config){
            return false;
        }
        return explode(',', $payments_config);
    }

}
