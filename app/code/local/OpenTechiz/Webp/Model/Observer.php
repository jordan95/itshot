<?php

class OpenTechiz_Webp_Model_Observer extends Yireo_Webp_Model_Observer
{
    /**
     * Listen to the event core_block_abstract_to_html_after
     *
     * @parameter Varien_Event_Observer $observer
     *
     * @return $this
     */
    public function coreBlockAbstractToHtmlAfter($observer)
    {
        if ($this->helper->canUse() == false) {
            return $this;
        }

        $transport = $observer->getEvent()->getTransport();
        $block = $observer->getEvent()->getBlock();

        if ($this->isAllowedBlock($block) == false) {
            return $this;
        }

        $html = $transport->getHtml();

        preg_match_all('/\ (src|data\-src|data\-image|data\-zoom\-image)=\"([^\"]+)\.(png|jpg|jpeg)/i', $html, $matches);

        $imageList = array();
        $srcTags = [];
        foreach ($matches[0] as $index => $match) {

            // Convert the URL to a valid path
            $imageUrl = $matches[2][$index] . '.' . $matches[3][$index];
            $webpUrl = $this->convertImageUrlToWebp($imageUrl);

            if (empty($webpUrl)) {
                continue;
            }

            // Replace the img tag in the HTML
            $htmlTag = $matches[0][$index];
            $srcTag = $matches[1][$index];
            $srcTags[$srcTag] = 1;
            $hash = md5($srcTag . $imageUrl);
            $newHtmlTag = str_replace($srcTag . '="' . $imageUrl, ' ' . $srcTag . '="' . Mage::getBaseUrl("skin") . 'frontend/default/default/images/webp/placeholder.png" ' . $srcTag . '-img="' . $hash, $htmlTag);
            $html = str_replace($htmlTag, $newHtmlTag, $html);

            // Add the images to the return-array
            $imageList[$hash] = array('orig' => $imageUrl, 'webp' => $webpUrl);
        }

        // Add a JavaScript-list to the HTML-document
        if (empty($imageList)) {
            return $this;
        }

        $newHtml = $this->getScriptHtmlLines($imageList, $srcTags);

        if ($block->getNameInLayout() == 'root') {
            $pattern = '/<script.*src="(.*js\/lazyimageloader\/lazysizes.min.js.*)"+.*\/script>/';
            if (preg_match($pattern, $html, $matches)) {
                $html = preg_replace($pattern, '', $html);
                $newHtml[] = "<script type=\"text/javascript\">var lazyLoadScriptUrl = '" . $matches[1] . "';</script>";
            }
            $newHtml[] = '<script type="text/javascript" src="' . Mage::getBaseUrl('js') . 'opentechiz/jquery.detect.js?v=2"></script>';
        }

        $html = $this->addScriptToBody($html, $newHtml);
        $transport->setHtml($html);

        return $this;
    }

    /**
     * @param array $imageList
     * @param array $srcTags
     *
     * @return array
     */
    protected function getScriptHtmlLines($imageList, $srcTags)
    {
        $newHtml = array();

        $newHtml[] = '<script>';
        $newHtml[] = 'var SKIN_URL = \'' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN) . '\';';
        $newHtml[] = 'var WEBP_SRC_TAGS = ' . json_encode(array_keys($srcTags)) . ';';
        $newHtml[] = 'if(webpReplacements == null) { var webpReplacements = new Object(); }';
        foreach ($imageList as $name => $value) {
            $newHtml[] = 'webpReplacements[\'' . $name . '\'] = ' . json_encode($value);
        }
        $newHtml[] = '</script>';

        return $newHtml;
    }
}
