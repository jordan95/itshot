<?php

class OpenTechiz_Webp_Helper_Data extends Yireo_Webp_Helper_Data
{
    /**
     * Method to check whether this extension is enabled
     *
     * @return bool
     */
    public function canUse()
    {
        if ($this->isEnabled() == false) {
            return false;
        }

        // Check for GD support
        if ($this->hasGdSupport()) {
            return true;
        }

        // Check for potential cwebp execution
        if ($this->hasBinarySupport()) {
            return true;
        }

        return false;
    }
}
