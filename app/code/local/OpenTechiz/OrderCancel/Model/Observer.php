<?php


class OpenTechiz_OrderCancel_Model_Observer
{
    public function adminhtmlWidgetContainerHtmlBefore($event)
    {
        $block = $event->getBlock();
        $order = $block->getOrder();
        if($order){
            $order_id = $order->getId();
            if($order->getState() != 'canceled'){
                $partial_payment_data = Mage::getModel('partialpayment/partialpayment')->getCollection()->addFieldToFilter('order_id',$order_id);
                if($partial_payment_data->count() > 0){
                    if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
                        $message = Mage::helper('core')->__('Are you sure you want to do this?');
                        $block->addButton('cancel_layaway_order', array(
                            'label'     => Mage::helper('core')->__('Cancel Layaway Order'),
                            'onclick'   => "confirmSetLocation('{$message}', '{$block->getUrl('*/sales_order/cancellayaway')}')"
                        ),1,8);
                    }
                }elseif($order->getOrderType() == 1){
                    $message = Mage::helper('core')->__('Are you sure you want to do this?');
                    $block->addButton('cancel_layaway_order', array(
                        'label'     => Mage::helper('core')->__('Cancel Layaway Order'),
                        'onclick'   => "confirmSetLocation('{$message}', '{$block->getUrl('*/*/cancelManualLayaway')}')"
                    ),1,8);
                }
            }
            
        }
        if ($block instanceof Milople_Partialpayment_Block_Adminhtml_Partialpayment_Edit) {
            if( Mage::registry('partialpayment_data') && Mage::registry('partialpayment_data')->getId() ){
                $partial_id = Mage::registry('partialpayment_data')->getId();
                $message = Mage::helper('core')->__('Are you sure you want to do this?');
                $block->addButton('cancel_layaway_order', array(
                    'label'     => Mage::helper('core')->__('Cancel Layaway Order'),
                    'onclick'   => "confirmSetLocation('{$message}', '{$block->getUrl('*/*/cancellayaway',array('id'=>$partial_id))}')"
                ));
                if (Mage::registry('partialpayment_data')->getRemainingAmount() > 0) {
                    $block->addButton('split_payment', array(
                        'label'     => Mage::helper('core')->__('Split Payment'),
                        'onclick' => 'setLocation(\'' . $block->getUrl('*/splitpayment/index',array('id'=>$partial_id)) . '\')'
                    ));
                }
            } 
            
        }
        
        
    }

}
