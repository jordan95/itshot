<?php
//require_once 'TBT' . DS . 'Rewards' . DS . 'controllers'  . DS . 'Adminhtml' . DS . 'Sales' . DS . 'OrderController.php';
require_once(Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Sales' . DS . 'OrderController.php');

class OpenTechiz_OrderCancel_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController
{
    const BILLING_STAGE = 3;
    public function cancelAction()
    {
        if ($order = $this->_initOrder()) {
            try {
                $order->cancel()
                    ->save();
                $this->_getSession()->addSuccess(
                    $this->__('The order has been cancelled.')
                );
                if($order->getOrderStage() == self::BILLING_STAGE){
                    foreach ($order->getAllItems() as $item) {
                        //log stock
                        $stock = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', $item->getSku())->getFirstItem();
                        $qty = (int)$item->getQtyOrdered() - (int)$item->getQtyRefunded();
                        Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                            'qty' => $qty,
                            'on_hold' => '0',
                            'type' => 9,
                            'stock_id' => $stock->getId(),
                            'qty_before_movement' => $stock->getQty(),
                            'on_hold_before_movement' => $stock->getOnHold(),
                            'sku' => $stock->getSku(),
                            'order_id'=>$order->getId()
                        ));
                    }
                    
                }
                
            }
            catch (Exception $e) {
                //$this->_getSession()->addError($this->__('The order has not been cancelled.'));
                Mage::logException($e);
                $order->setData('state', "canceled");
                $order->setStatus("canceled");
                $history = $order->addStatusHistoryComment('Order marked as cancelled.', false);
                $history->setIsCustomerNotified(false);
                $order->save();
            }
            $this->_redirect('*/sales_order/view', array('order_id' => $order->getId()));
        }
    }
    public function cancellayawayAction()
    {
        if ($order = $this->_initOrder()) {
            try {
                $partial_id = '';
                $installment_status ='Canceled';
                date_default_timezone_set(Mage::app()->getStore()->getConfig('general/locale/timezone'));
                $installment_due_date = Mage::getModel('core/date')->gmtDate('Y-m-d');

                $order_id = $order->getId();
                $partial_payment_data = Mage::getModel('partialpayment/partialpayment')->getCollection()->addFieldToFilter('order_id',$order_id);

                if($partial_payment_data->count() > 0){

                    foreach($partial_payment_data->getData() as $data){
                        $partial_id  = $data['partial_payment_id'];
                    }

                    if($partial_id !=''){
                        $partial_payment_installment_data = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id',$partial_id)->addFieldToFilter('installment_status', array('neq' => 'Paid'));
                        if($partial_payment_installment_data->count() > 0){
                            foreach($partial_payment_installment_data->getData() as $installment){
                                $installment_data = Mage::getModel('partialpayment/installment')->load($installment['installment_id']);
                                $installment_data->setInstallmentStatus($installment_status);
                                $installment_data->setInstallmentPaidDate($installment_due_date);
                                $installment_data->save();
                            }
                            $order->cancel();
                            $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, "layawayorder_canceled", 'Order marked as layaway order - cancelled.', false);
                            $order->save();
                             //log stock
                            foreach ($order->getAllItems() as $item) {
                                
                                $stock = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', $item->getSku())->getFirstItem();
                                $qty = Mage::helper("opentechiz_inventory")->countReleasedItem($item->getId());
                                if($qty > 0){
                                    Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                                        'qty' => $qty,
                                        'on_hold' => '0',
                                        'type' => 9,
                                        'stock_id' => $stock->getId(),
                                        'qty_before_movement' => $stock->getQty(),
                                        'on_hold_before_movement' => $stock->getOnHold(),
                                        'sku' => $stock->getSku(),
                                        'order_id'=>$order->getId()
                                    ));
                                }
                                
                            }
                            //cancel all order item
                            $items = $order->getAllItems();
                            foreach ($items as $item){
                                $item->setQtyCanceled($item->getQtyOrdered() - $item->getQtyRefunded())->save();
                            }
                            $this->_getSession()->addSuccess(
                                $this->__('The order has been cancelled.')
                            );
                           
                        }else{
                            $this->_getSession()->addSuccess(
                                $this->__('The order has been pay full so could not canceled.')
                            );
                        }
                    }
                    
                }
                
                
            }catch (Exception $e) {
                $this->_getSession()->addError($this->__('The order has not been cancelled!'));
                Mage::logException($e);
                
            }
            $this->_redirect('*/sales_order/view', array('order_id' => $order->getId()));
        }
    }
    public function cancelManualLayawayAction()
    {
        if ($order = $this->_initOrder()) {
            try {
                $order->setData('state', "canceled");
                $order->setStatus("layawayorder_canceled");
                $history = $order->addStatusHistoryComment('Order marked as layaway order - cancelled.', false);
                $history->setIsCustomerNotified(false);
                $order->save();
                //log stock
                foreach ($order->getAllItems() as $item) {

                    $stock = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToFilter('sku', $item->getSku())->getFirstItem();
                    $qty = Mage::helper("opentechiz_inventory")->countReleasedItem($item->getId());
                    if ($qty > 0) {
                        Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                            'qty' => $qty,
                            'on_hold' => '0',
                            'type' => 9,
                            'stock_id' => $stock->getId(),
                            'qty_before_movement' => $stock->getQty(),
                            'on_hold_before_movement' => $stock->getOnHold(),
                            'sku' => $stock->getSku(),
                            'order_id' => $order->getId()
                        ));
                    }

                }
                //cancel all order item
                $items = $order->getAllItems();
                foreach ($items as $item) {
                    $item->setQtyCanceled($item->getQtyOrdered() - $item->getQtyRefunded())->save();
                }
                Mage::dispatchEvent('layawayorder_canceled_after', array('order' => $order));

                $this->_getSession()->addSuccess(
                    $this->__('The order has been cancelled.')
                );
            }catch (Exception $e) {
                $this->_getSession()->addError($this->__('The order has not been cancelled!'));
                Mage::logException($e);

            }
            $this->_redirect('*/sales_order/view', array('order_id' => $order->getId()));
        }
    }
}
