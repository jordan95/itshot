<?php
class OpenTechiz_CustomOptions_Model_Observer
{
    public function removeSpecialCharForSKU($observer){
        $specialChars = OpenTechiz_CustomOptions_Helper_Data::SPECIAL_CHAR_TO_AVOID_IN_SKU;

        $request = Mage::app()->getRequest();
        if(isset($request->getParams()['product'])) {
            $productData = $request->getParams()['product'];
            //check special char in product sku
            $productSku = $productData['sku'];
            foreach ($specialChars as $specialChar) {
                if (strpos($productSku, $specialChar) !== false) {
                    $productSku = str_replace($specialChar, '', $productSku);
                    $isChanged = true;
                }
            }
            if(isset($isChanged)){
                $product = $observer->getEvent()->getProduct();
                $product->setSku($productSku);
            }
        }
    }

    public function removeSpecialCharForProductOptions($observer){
        $specialChars = OpenTechiz_CustomOptions_Helper_Data::SPECIAL_CHAR_TO_AVOID_IN_SKU;
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $table = $resource->getTableName('catalog/product_option_type_value');

        $request = Mage::app()->getRequest();
        if(isset($request->getParams()['product'])) {
            $productData = $request->getParams()['product'];
            if (isset($productData['options'])) {
                foreach ($productData['options'] as $optionKey => $option) {
                    if ($option['type'] == 'drop_down') {
                        foreach ($option['values'] as $valueKey => $value) {
                            if (isset($value['sku'])) {
                                $valueSku = $value['sku'];
                                $isValueChanged = false;
                                foreach ($specialChars as $specialChar) {
                                    if (strpos($valueSku, $specialChar) !== false) {
                                        $valueSku = str_replace($specialChar, null, $valueSku);
                                        $isValueChanged = true;
                                    }
                                }
                                if ($isValueChanged) {
                                    $query = "UPDATE {$table} SET sku = '{$valueSku}' WHERE option_type_id = "
                                        . (int)$valueKey;

                                    $writeConnection->query($query);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}