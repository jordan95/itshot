<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('catalog/product_option')}` ADD `default_value` text NULL COMMENT 'Default Value';");
$installer->endSetup();
