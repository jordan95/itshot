<?php
class OpenTechiz_CustomOptions_Adminhtml_CustomOptionsController extends Mage_Adminhtml_Controller_Action
{
    public function validateAction()
    {
        $request = Mage::app()->getRequest();
        if (!$request->isAjax()) {
            return false;
        }
        $productId = $request->getParam('productId');
        $skus = $request->getParam('skus');

        $response = '';
        if (!$productId) {
            $response = 'Empty Product ID';
            return $this->sendResponse($response);
        }
        if (!$skus) {
            $response = 'SKUs are empty. Please, fill SKUs and try again.';
            return $this->sendResponse($response);
        }

        $mainProduct = Mage::getModel('catalog/product')->load($productId);
        if (!$mainProduct->getId()) {
            $response = 'Could not find Product';
            return $this->sendResponse($response);
        }
        $mainOptions = $mainProduct->getOptions();
        if (!$mainOptions) {
            $response = 'Product does not have options';
            return $this->sendResponse($response);
        }

        $skus = explode(',', $skus);
        $notFoundSkus = [];
        $withOptionsSkus = [];
        $withStockSkus = [];
        $correctSkus = [];
        foreach ($skus as $sku) {
            $productId = Mage::getModel('catalog/product')->getIdBySku(trim($sku));
            if (!$productId) {
                $notFoundSkus[] = $sku;
                continue;
            }
            $product = Mage::getModel('catalog/product')->load($productId);
            if (!$product || !$product->getId()) {
                $notFoundSkus[] = $sku;
                continue;
            }
            $correct = true;
            if ($product->getOptions()) {
                $withOptionsSkus[] = $sku;
                $correct = false;
            }
            $qty = $this->getInStockQty($productId);
            if ($qty > 0) {
                $withStockSkus[] = $sku;
                $correct = false;
            }
            if ($correct) {
                $correctSkus[] = $sku;
            }
        }

        if ($notFoundSkus) {
            $response .= '<br>These SKUs were not found:<br>';
            $response .= implode(', ', $notFoundSkus) . '<br>';
        }

        if ($withOptionsSkus) {
            $response .= '<br>These SKUs already have options which will be replaced:<br>';
            $response .= implode(', ', $withOptionsSkus) . '<br>';
        }

        if ($withStockSkus) {
            $response .= '<br>These SKUs have On-hand QTY:<br>';
            $response .= implode(', ', $withStockSkus) . '<br>';
        }

        if ($response && $correctSkus) {
            $response .= '<br>Other SKUs:<br>';
            $response .= implode(', ', $correctSkus) . '<br>';
        }

        if ($response) {
            $response .= '<br><br>Are you sure that you want to continue?<br>';
        } else {
            $response = 1;
        }

        return $this->sendResponse($response);
    }

    public function duplicateAction()
    {
        $request = Mage::app()->getRequest();
        if (!$request->isAjax()) {
            return false;
        }
        $productId = $request->getParam('productId');
        $skus = $request->getParam('skus');

        $response = '';
        if (!$productId) {
            $response = 'Empty Product ID';
            return $this->sendResponse($response);
        }
        if (!$skus) {
            $response = 'SKUs are empty. Please, fill SKUs and try again.';
            return $this->sendResponse($response);
        }

        $mainProduct = Mage::getModel('catalog/product')->load($productId);
        if (!$mainProduct->getId()) {
            $response = 'Could not find Product';
            return $this->sendResponse($response);
        }
        $mainOptions = $mainProduct->getOptions();
        if (!$mainOptions) {
            $response = 'Product does not have options';
            return $this->sendResponse($response);
        }

        $skus = explode(',', $skus);
        $successSkus = [];
        $withOptionsSkus = [];
        $withStockSkus = [];
        $notFoundSkus = [];
        $failedSkus = [];
        foreach ($skus as $sku) {
            try {
                $productId = Mage::getModel('catalog/product')->getIdBySku(trim($sku));
                if (!$productId) {
                    $notFoundSkus[] = $sku;
                    continue;
                }
                $product = Mage::getModel('catalog/product')->load($productId);
                if (!$product || !$product->getId()) {
                    $notFoundSkus[] = $sku;
                    continue;
                }
                $qty = $this->getInStockQty($productId);
                if ($qty > 0) {
                    $withStockSkus[] = $sku;
                }
                $options =  $product->getOptions();
                if ($options) {
                    $withOptionsSkus[] = $sku;
                    foreach ($options as $option) {
                        $option->delete();
                    }
                }
                $mainProduct->getOptionInstance()->duplicate($mainProduct->getId(), $product->getId());
                $product->setHasOptions(true)->save();
                $successSkus[] = $sku;
            } catch (\Exception $e) {
                $failedSkus[] = $sku;
            }
        }

        if ($successSkus) {
            $response .= '<br>Successfully updated SKUs:<br>';
            $response .= implode(', ', $successSkus) . '<br>';
        }

        if ($withOptionsSkus) {
            $response .= '<br>SKUs with replaced options:<br>';
            $response .= implode(', ', $withOptionsSkus) . '<br>';
        }

        if ($withStockSkus) {
            $response .= '<br>These SKUs have On-hand QTY:<br>';
            $response .= implode(', ', $withStockSkus) . '<br>';
        }

        if ($notFoundSkus) {
            $response .= '<br>These SKUs were not found:<br>';
            $response .= implode(', ', $notFoundSkus) . '<br>';
        }

        if ($failedSkus) {
            $response .= '<br>Failed SKUs:<br>';
            $response .= implode(', ', $failedSkus) . '<br>';
        }

        return $this->sendResponse($response);
    }

    /**
     * @param string $response
     * @return $this
     */
    protected function sendResponse($response)
    {
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));

        return $this;
    }

    /**
     * @param int $productId
     * @return int
     */
    protected function getInStockQty($productId)
    {
        /** @var OpenTechiz_Inventory_Model_Resource_Instock $resource */
        $resource = Mage::getResourceModel('opentechiz_inventory/instock');
        $readConnection = $resource->getReadConnection();
        $sql = $readConnection->select()
            ->from($resource->getMainTable(), ['SUM(qty)'])
            ->where('product_id = ?', $productId)
            ->group('product_id');
        return (int)$readConnection->fetchOne($sql);
    }
}
