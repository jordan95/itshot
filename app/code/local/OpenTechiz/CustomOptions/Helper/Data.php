<?php

class OpenTechiz_CustomOptions_Helper_Data extends Mage_Core_Helper_Abstract
{
    const SPECIAL_CHAR_TO_AVOID_IN_SKU = ['\'', "\"", '%', '_', '\\', OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR];
    
}