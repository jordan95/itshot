<?php

class OpenTechiz_Menuadminpro_Block_Top extends Medialounge_Menuadminpro_Block_Top
{
    public function drawMenu($region = 'top', $pid = 0, $cmspercent = 0, $productpercent = 0, $itemsperrow = 5)
    {
        if (empty($this->_columnsDefault)):
            $this->_columnsDefault = $itemsperrow;
        endif;
        $debug_item = 39444444444444444; //9 buy 4 categories
        $html_wide  = '';
        $html_small = '';
        $items      = $this->getChildrens($pid, $region);
        $width_rows_menu = '';
        if (empty($items)) {
            $html['wide']  = $html_wide;
            $html['small'] = $html_small;

            return $html;
        }
        foreach ($items as $k => $item) {
            if (isset($item['itemsperrowsub']) && ! empty($item['itemsperrowsub'])):
                if ($item['itemsperrowsub'] == 0):
                    $item['itemsperrowsub'] = 1;
                endif;
                $itemsperrow = $item['itemsperrowsub'];
            else:
                $itemsperrow = $this->_columnsDefault;
            endif;

            if ($item['type'] == 4) {
                $cmsblock_percent = $productblock_percent = 0;

                if (($item['cmsblockposition'] == 'right' || $item['cmsblockposition'] == 'left') && ! empty($item['cmsblockpercent'])):
                    $cmsblock_percent = $item['cmsblockpercent'];
                endif;

                if ($item['menuadminpro_products'] != '' && $item['productstotal'] > 0 && ! empty($item['productblockpercent'])):
                    $productblock_percent = $item['productblockpercent'];
                endif;

                $width_rows_menu = 100 - $cmsblock_percent - $productblock_percent;
                $childrens       = $this->renderCategoriesMenuHtml(0, $itemsperrow, $item['distributeitemssub']);

                if ($item['distributeitemssub']):

                    if ( ! empty($childrens['wideinlinecontinuos']) && is_array($childrens['wideinlinecontinuos'])) {
                        $html_wide2 = '';
                        foreach ($childrens['wideinlinecontinuos'] as $k => $child):
                            if (isset($child['top'])):
                                $html_wide2 .= $child['top'];
                            elseif (isset($child['sub'])):
                                if (isset($childrens['wideinlinecontinuos'][$k - 1]['top'])):
                                    $html_wide2 .= '<div class="sub-nav">';
                                    $html_wide2 .= '<div class="sub-nav-group"';
                                    if(is_numeric($width_rows_menu)){
                                        $html_wide2 .= ' style="width:' . $width_rows_menu . '%;"';
                                    }
                                    $html_wide2 .= '>';
                                    $html_wide2 .= '<div class="sub-nav-row">';
                                endif;

                                $cc              = 0;
                                $_collectionSize = count($child['sub']);
                                $width_column    = 100 / $itemsperrow;
                                $percolumn       = ceil($_collectionSize / $itemsperrow);

                                foreach ($child['sub'] as $sub):
                                    if ($cc++ % $percolumn == 0):
                                        $html_wide2 .= '<ul class="sub-nav-column" style="width:' . $width_column . '%;">';
                                    endif;

                                    $html_wide2 .= $sub;
                                    if ($cc % $percolumn == 0 || $cc == $_collectionSize):
                                        $html_wide2 .= '</ul>';
                                    endif;
                                endforeach;

                                if ( ! isset($childrens['wideinlinecontinuos'][$k + 1]) || isset($childrens['wideinlinecontinuos'][$k + 1]['top'])):
                                    $html_wide2 .= '</div>';
                                    $html_wide2 .= '</div>';
                                    $html_wide2 .= '</div>';
                                endif;
                            endif;

                        endforeach;

                    }
                    //$this->outItemContinuous = array();

                    if (isset($html_wide2) && ! empty($html_wide2)):
                        $html_wide .= $html_wide2;
                    endif;
                else:
                    $html_wide .= $childrens['wideinline'];
                endif;

                $html_small .= $childrens['small'];
            } else {
                $html_wide .= '<li';
                $html_small .= '<li';

                $html_wide .= ' class="menu-level-' . ($item['level'] - 1);
                $html_small .= ' class="menu-level-' . ($item['level'] - 1);

                if ( ! empty($item['cssclass'])) {
                    $html_wide .= ' ' . $item['cssclass'];
                    $html_small .= ' ' . $item['cssclass'];
                }
                $html_wide .= '">' . "\n";
                $html_small .= '">' . "\n";

                $link = $this->drawLink($item);

                $html_wide .= $link;
                $html_small .= $link;

                if ($item['type'] == 6 || $item['cmsblock'] != '' || $item['menuadminpro_products'] != '' || $item['header'] != '' || $item['footer'] != '') {
                    /* dropdown menu start here*/
                    $class_debug = '';
                    if ($item['item_id'] == $debug_item):
                        $class_debug = ' dropdown-menu-debug';
                    endif;

                    $html_wide .= '<div class="sub-nav full-width' . $class_debug . '">' . "\n";
                    $html_wide .= '<div class="sub-nav-inner">' . "\n";
                    $html_small .= '<ul class="sub-nav-small">' . "\n";

                    /* dropdown top */
                    if ($item['header'] != '') {
                        $html_wide .= '<div class="sub-nav-row sub-nav-header">' . $item['header'] . '</div>';
                    }

                    if ($item['cmsblock'] != '' && $item['cmsblockposition'] == 'top') {
                        $html_wide .= '<div class="sub-nav-row sub-nav-cmsblock-top">' . $this->getCmsBlockHtml($item) . '</div>';
                    }


                    if ($item['type'] == 6):
                        $cmsblock_percent = $productblock_percent = 0;

                        if (($item['cmsblockposition'] == 'right' || $item['cmsblockposition'] == 'left') && ! empty($item['cmsblockpercent'])):
                            $cmsblock_percent = $item['cmsblockpercent'];
                        endif;

                        if ($item['menuadminpro_products'] != '' && $item['productstotal'] > 0 && ! empty($item['productblockpercent'])):
                            $productblock_percent = $item['productblockpercent'];
                        endif;

                        $width_rows_menu = 100 - $cmsblock_percent - $productblock_percent;
                    endif;

                    if ($item['type'] == 6) {
                        $this->outItemContinuous = array();
                        $childrens               = $this->renderCategoriesMenuHtml(0, $itemsperrow,
                            $item['distributeitemssub']);
                        $html_wide .= '<div class="sub-nav-group"';
                        if(is_numeric($width_rows_menu)){
                            $html_wide .= ' style="width:' . $width_rows_menu . '%;"';
                        }
                        $html_wide .= '>';
                        if ($item['distributeitemssub']) {
                            if ( ! empty($childrens['widecontinuos']) && is_array($childrens['widecontinuos'])) {
                                $html_wide .= '<div class="sub-nav-row">';
                                $cc              = 0;
                                $_collectionSize = count($childrens['widecontinuos']);
                                $width_column    = 100 / $itemsperrow;
                                $percolumn       = ceil($_collectionSize / $itemsperrow);

                                foreach ($childrens['widecontinuos'] as $child):
                                    if ($cc++ % $percolumn == 0):
                                        $html_wide .= '<ul class="sub-nav-column" style="width:' . $width_column . '%;">';
                                    endif;
                                    $html_wide .= $child;
                                    if ($cc % $percolumn == 0 || $cc == $_collectionSize):
                                        $html_wide .= '</ul>';
                                    endif;
                                endforeach;
                                $html_wide .= '</div>';
                            }
                        } else {
                            $html_wide .= $childrens['wide'];
                        }
                        $html_wide .= '</div>';
                        $html_small .= $childrens['small'];
                        $this->outItemContinuous = array();
                    }

                    if ($item['is_en_add_cms_block'] && $item['add_cms_block']) {
                        $html_wide  .= '<div class="sub-nav-group">';
                        $html_wide  .= '<div class="sub-nav-row">';
                        $html_wide  .= $this->getAdditionalCmsBlockHtml($item);
                        $html_wide  .= '</div></div>';
                        $html_small  .= '<li>' . $this->getAdditionalCmsBlockHtml($item) . '</li>';
                    } else {
                        if ($this->hasChildrens($item['item_id'], $region)) {
                            if ($item['distributeitemssub'] == 1) {
                                /* continuos items */
                                $this->outItemContinuous = array();
                                $childrens               = $this->drawItemContinuous($region, $item['item_id'],
                                    $itemsperrow);
                                if ( ! empty($this->outItemContinuous) && is_array($this->outItemContinuous)) {

                                    $html_wide .= '<div class="sub-nav-group"';
                                    if(is_numeric($width_rows_menu)){
                                        $html_wide .= 'style="width:' . $width_rows_menu . '%;"';
                                    }
                                    $html_wide.='>';
                                    $html_wide .= '<div class="sub-nav-row">';

                                    $cc              = 0;
                                    $_collectionSize = count($this->outItemContinuous);
                                    $width_column    = 100 / $itemsperrow;
                                    $percolumn       = ceil($_collectionSize / $itemsperrow);
                                    $html_wide2      = '';
                                    foreach ($this->outItemContinuous as $child):
                                        if ($cc++ % $percolumn == 0):
                                            $html_wide .= '<ul class="sub-nav-column" style="width:' . $width_column . '%;">';
                                        endif;
                                        $html_wide .= $child;
                                        if ($cc % $percolumn == 0 || $cc == $_collectionSize):
                                            $html_wide .= '</ul>';
                                        endif;
                                    endforeach;
                                    $html_wide .= '</div></div>';
                                }
                                $html_small .= $childrens['small'];
                                $this->outItemContinuous = array();
                            } else {
                                $childrens = $this->drawItem($region, $item['item_id'], $itemsperrow);
                                if ( ! empty($childrens['wide'])) {
                                    $html_wide .= '<div class="sub-nav-group"';
                                    if(is_numeric($width_rows_menu)){
                                        $html_wide .= ' style="width:' . $width_rows_menu . '%;"';
                                    }
                                    $html_wide .= '>' . $childrens['wide'] . '</div>';
                                    $html_small .= $childrens['small'];
                                }
                            }
                        }
                    }

                    /* dropdown right */
                    if ($item['menuadminpro_products'] != '' && $item['productblockposition'] == 'right') {
                        $html_wide .= '<div class="sub-nav-group"';
                        if(isset($item['productblockpercent']) && is_numeric($item['productblockpercent'])){
                            $html_wide .= ' style="width:' . $item['productblockpercent'] . '%;"'; 
                        }
                        $html_wide .= '>';
                        $html_wide .= '<div class="sub-nav-row sub-nav-product-right">' . $this->getProductsBlockHtml($item) . '</div>';
                        $html_wide .= '</div>';
                    }

                    if ($item['cmsblock'] != '' && $item['cmsblockposition'] == 'right') {
                        $html_wide .= '<div class="sub-nav-group"';
                        if(isset($item['cmsblockpercent']) && is_numeric($item['cmsblockpercent'])){
                            $html_wide .= ' style="width:' . $item['cmsblockpercent'] . '%;"';
                        }
                        $html_wide .= '>';
                        $html_wide .= '<div class="sub-nav-row sub-nav-cmsblock-right">' . $this->getCmsBlockHtml($item) . '</div>';
                        $html_wide .= '</div>';
                    }

                    /* dropdown bottom */
                    if ($item['cmsblock'] != '' && $item['cmsblockposition'] == 'bottom') {
                        $html_wide .= '<div class="sub-nav-row sub-nav-cmsblock-bottom">' . $this->getCmsBlockHtml($item) . '</div>';
                    }

                    if ($item['footer'] != '') {
                        $html_wide .= '<div class="sub-nav-row sub-nav-footer">' . $item['footer'] . '</div>';
                    }

                    $html_wide .= '</div>' . "\n"; // close sub-nav
                    $html_wide .= '</div>' . "\n"; // close sub-nav
                    $html_small .= '</ul>' . "\n";
                    /* dropdown menu end here*/
                }

                $html_wide .= '</li>' . "\n";
                $html_small .= '</li>' . "\n";

            }
        }
        $html['wide'] = $html_wide;
        //Mage::log($html_small);
        $html['small'] = $html_small;

        return $html;
    }


}