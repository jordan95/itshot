<?php
$installer = $this;
$installer->startSetup();

$table = $installer->getTable('dictionary');

if ($installer->getConnection()->isTableExists($table)) {
    $installer->getConnection()->addIndex($table, 'search_name', array('search_name'));
}

$installer->endSetup();