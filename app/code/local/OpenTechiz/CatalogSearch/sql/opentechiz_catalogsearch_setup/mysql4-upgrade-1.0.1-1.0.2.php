<?php
	$this->startSetup();
	$this->run("
	    ALTER TABLE `{$this->getTable('dictionary')}` 
		ADD UNIQUE(`search_name`);
	");
	$this->endSetup();
?>