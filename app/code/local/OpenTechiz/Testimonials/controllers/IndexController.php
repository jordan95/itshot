<?php
require_once(Mage::getModuleDir('controllers','Jextn_Testimonials').DS.'IndexController.php');

class OpenTechiz_Testimonials_IndexController extends Jextn_Testimonials_IndexController
{
    public function indexAction()
    {
		$this->loadLayout();
		////set Page Title, added by Mahipal S Adhikari on 12-Jun-2013
		$this->getLayout()->getBlock('head')->setTitle(Mage::helper('testimonials')->getTestimonialsTitle());
		$this->getLayout()->getBlock('head')->setKeywords("itshot.com reviews,  itshot.com customer reviews, itshot.com testimonials, itshot.com ratings and reviews");
		$this->getLayout()->getBlock('head')->setDescription("ItsHot.com Reviews - Read customer reviews, ratings and testimonials about our products services and share your experience at ItsHot.com");
		$this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
		$this->renderLayout();
    }
}
