<?php
require_once(Mage::getModuleDir('controllers','Jextn_Testimonials').DS.'SubmitController.php');

class OpenTechiz_Testimonials_SubmitController extends Jextn_Testimonials_SubmitController {

	public function postAction() {
		$post = $this->getRequest()->getPost();
		if($post) {

			$model = Mage::getModel('testimonials/testimonials');
			$model->setData($post)->setId($this->getRequest()->getParam('id'));
			try {
				$error = false;
				if(!Zend_Validate::is(trim($post['name']),'NotEmpty')) {
					$error = true;
				}
				if(!Zend_Validate::is(trim($post['email']),'EmailAddress')) {
					$error = true;
				}
				if(!Zend_Validate::is(trim($post['rating']),'NotEmpty')) {
					$error = true;
				}
				if(!Zend_Validate::is(trim($post['content']),'NotEmpty')) {
					$error = true;
				}
				if($error) {
					throw new Exception();
				}
				
				$url = '';
				if(isset($post['url'])){
					$url = $post['url'];
				}
				if($url != '') {

					$checkval = "http://";
					$pos = strpos($url,$checkval);
					if($pos === false) {
						$churl = $checkval.$url;
						$model->setUrl($churl);
					}
					else {
						$model->setUrl($url);
					}
				}

				if(Mage::helper('testimonials')->getAutoApproved() == 1) {
					$model->setStatus(1);
				}
				else {
					$model->setStatus(2);
				}
				if(!$model->getId()) {
					$model->setCreatedTime(now());
				}
				$model->setUpdateTime(now());

				$model->save();             
                if(Mage::helper('testimonials')->getAutoApproved()==1){
                    $status = TBT_Rewards_Model_Transfer_Status::STATUS_APPROVED;
                } else {
                    $status = Mage::helper('rewards/config')->getInitialTransferStatusAfterReview();
                }
				
				$data['customer_id'] = Mage::getSingleton('customer/session')->getCustomerId();
				$data['quantity'] = 500;
				$data['comments'] = 'Points received for rating the store.';
				$data ['status_id'] = $status;
				$data ['reason_id'] = 15;
				$data ['currency_id'] = 1;
				$reward_id = $this->getRequest ()->getParam ('id');
				$reward_model = Mage::getModel('rewards/transfer');
				$reward_model->load($reward_id);
				$reward_model = Mage::getModel ('rewards/transfer');
				$reward_model->setData($data)->setId($reward_id);
				if($this->getRequest ()->getParam('creation_ts') == NULL) {
					$reward_model->setCreatedAt(now())->setUpdatedAt(now());
				}
				else {
					$reward_model->setUpdatedAt(now());
				}
				$reward_model->save();
				
				Mage::getSingleton('customer/session')->addSuccess(Mage::helper('testimonials')->__('Thank you, your testimonial was submitted for moderation.'));
				
				$this->_redirect('customer-review-testimonials/index');
				return;
			}
			catch(Exception $e) {
				Mage::getSingleton('customer/session')->addError(Mage::helper('testimonials')->__('Unable to submit your request. Please, try again later'));
				$this->_redirect('customer-review-testimonials/index');
				return;
			}
		}
		else {
			$this->_redirect('customer-review-testimonials/index/');
		}
	}
}
