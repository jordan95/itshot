<?php

class OpenTechiz_Testimonials_Block_Testimonials extends OpenTechiz_Review_Block_Testimonials
{

    public function getListOrder()
    {
        $list_order = array();
        $list_order_id =array();
        $list_order_id_exists =array();
         if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $orders = Mage::getResourceModel('sales/order_collection')
                        ->addFieldToSelect('*')
                        ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
                        ->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
                        ->setOrder('created_at', 'desc')
                    ;   
            if($orders->count() > 0){
                foreach ($orders as $order){
                   $list_order[$order->getId()] = $order->getIncrementId();
                   $list_order_id[] = $order->getId();
                }
                $collection = Mage::getModel('testimonials/testimonials')->getCollection()
                            ->distinct(true)
                            ->addFieldToSelect('order_id')
                            ->addFieldToFilter('order_id', array('in' => $list_order_id))
                            ->setOrder('created_time','desc');
                if($collection->count()>0){
                    foreach($collection->getData() as $value){
                        $list_order_id_exists[] = $value['order_id'];
                    }
                    foreach($list_order_id_exists as $val){
                        if(array_key_exists($val, $list_order)){
                            unset($list_order[$val]);
                        }
                        
                    }
                }
            }
        }
        return $list_order;
    }

}
