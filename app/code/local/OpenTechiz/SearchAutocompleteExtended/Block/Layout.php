<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Sphinx Search Ultimate
 * @version   2.3.3.1
 * @build     1291
 * @copyright Copyright (C) 2016 Mirasvit (http://mirasvit.com/)
 */



class OpenTechiz_SearchAutocompleteExtended_Block_Layout extends Mirasvit_SearchAutocomplete_Block_Layout
{

    public function addSearchAutocomplete()
    {
        $this->addForm();

        return $this;
    }

}
