<?php

class OpenTechiz_SearchAutocompleteExtended_Block_Result extends Mirasvit_SearchAutocomplete_Block_Result
{
    public function validWordsMaxLength($searchTerm, $maxlength = 3) {
        $searchTerm = trim($searchTerm);
        if ($searchTerm) {
            $searchTerm = str_replace(array('    ', '   ', '  ', ' '), ' ', $searchTerm);
            $words = explode(' ', $searchTerm);
            $word = end($words);
            return strlen($word) > $maxlength;
        }
        return false;
    }
    
    public function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->setTemplate('opentechiz/autocomplete/result.phtml');
        return $this;
    }
}
