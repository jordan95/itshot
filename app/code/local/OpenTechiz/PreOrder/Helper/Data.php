<?php

class OpenTechiz_PreOrder_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function isEnable()
    {
        return Mage::getStoreConfig('cataloginventory/item_options/preorder_enable');
    }

    public function getPreOrderMessage(Mage_Catalog_Model_Product $product)
    {
        if (!$this->canPreOrder($product)) {
            return '';
        }
        /* @var $stock_item Mage_CatalogInventory_Model_Stock_Item */
        $stock_item = $product->getStockItem();

        $expected_ship_date = $stock_item->getData('expected_ship_date');
        $preorder_message = $stock_item->getPreorderMessage();
        $preorder_expected_ship_date_format = Mage::getStoreConfig('cataloginventory/item_options/preorder_expected_ship_date_format');
        if(!$expected_ship_date || !$preorder_message){
            return 'Temporarily out of stock.';
        } elseif($expected_ship_date){
            $date = date($preorder_expected_ship_date_format, strtotime($expected_ship_date));
            return str_replace('{{expected_ship_date}}', $date, $preorder_message);
        } else {
            return '';
        }
    }

    public function showPreOrderMessage(Mage_Catalog_Model_Product $product)
    {
        echo $this->getPreOrderMessage($product);
    }

    public function canPreOrder(Mage_Catalog_Model_Product $product)
    {
        if (!$this->isEnable()) {
            return false;
        }

        /* @var $stock_item Mage_CatalogInventory_Model_Stock_Item */
        $stock_item = $product->getStockItem();
        if (!$stock_item instanceof Mage_CatalogInventory_Model_Stock_Item) {
            $stock_item = Mage::getModel('cataloginventory/stock_item')->load($product->getId(), 'product_id');
        }
        $backorders = $stock_item->getBackorders();
        if (
                !$stock_item->getManageStock() || $backorders == Mage_CatalogInventory_Model_Stock::BACKORDERS_NO
        ) {
            return false;
        }
        $pre_order = new Varien_Object();
        $canPreOrder = $backorders == Mage_CatalogInventory_Model_Stock::BACKORDERS_YES_NONOTIFY || $backorders == Mage_CatalogInventory_Model_Stock::BACKORDERS_YES_NOTIFY;
        $pre_order->setCanPreOrder($canPreOrder);
        Mage::dispatchEvent('can_pre_order', array('product' => $product, 'pre_order' => $pre_order));

        return $pre_order->getCanPreOrder();
    }

    public function getPreOrderLabel()
    {
        return $this->__(Mage::getStoreConfig('cataloginventory/item_options/preorder_label'));
    }

}
