<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('cataloginventory/stock_item')}` ADD `preorder_message` text NULL COMMENT 'PreOrder Message';");
$installer->run("ALTER TABLE `{$installer->getTable('cataloginventory/stock_item')}` ADD `expected_ship_date` DATETIME NULL COMMENT 'Expected ship date';");
$installer->endSetup();
