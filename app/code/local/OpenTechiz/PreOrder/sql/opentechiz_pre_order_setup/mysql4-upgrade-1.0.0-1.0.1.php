<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('cataloginventory/stock_item')}` ADD `use_config_preorder_message` smallint(5) NULL COMMENT 'Use Config PreOrder Message' DEFAULT 1;");
$installer->endSetup();
