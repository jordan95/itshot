<?php

class OpenTechiz_PreOrder_Model_Observer
{
    
    const ATTR_ID_C2C_DISP_CUSTOM_STOCK_MSG = 321;

    public function setCanPreOrder(Varien_Event_Observer $observer)
    {
        $product = $observer->getProduct();
        $pre_order = $observer->getPreOrder();
        $c2c_disp_custom_stock_msg = $product->getData('c2c_disp_custom_stock_msg');
        if(!is_numeric($c2c_disp_custom_stock_msg)){
            if(Mage::registry('c2c_disp_custom_stock_msg_'. $product->getId()) === NULL){
                $entityTypeId = 4;
                $entityId = $product->getId();
                $attributeId = self::ATTR_ID_C2C_DISP_CUSTOM_STOCK_MSG;
                $table =  $this->getTableName('catalog_product_entity_int');
                $sql = "SELECT value  FROM `$table` WHERE `entity_type_id` = $entityTypeId AND `attribute_id` = $attributeId AND `entity_id` = $entityId";
                $c2c_disp_custom_stock_msg = $this->getConnection()->fetchOne($sql);
                Mage::register('c2c_disp_custom_stock_msg_'. $product->getId(), $c2c_disp_custom_stock_msg);
            }else{
                $c2c_disp_custom_stock_msg = Mage::registry('c2c_disp_custom_stock_msg_'. $product->getId());
            }
            $product->setData('c2c_disp_custom_stock_msg', $c2c_disp_custom_stock_msg);
        }
        
        $pre_order->setCanPreOrder($pre_order->getCanPreOrder() && (bool)$c2c_disp_custom_stock_msg);
    }
    
    public function getTableName($table)
    {
        return $this->getResource()->getTableName($table);
    }

    public function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    public function getConnection()
    {
        return $this->getResource()->getConnection('core_read');
    }
}
