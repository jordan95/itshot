<?php

if (Mage::helper('core')->isModuleEnabled('Unirgy_Dropship')){
    class OpenTechiz_PreOrder_Model_Cataloginventory_Stock_Item_Pure extends Unirgy_Dropship_Model_Stock_Item{}
}else {
    class OpenTechiz_PreOrder_Model_Cataloginventory_Stock_Item_Pure extends Mage_CatalogInventory_Model_Stock_Item{}
}

class OpenTechiz_PreOrder_Model_Cataloginventory_Stock_Item extends Mage_CatalogInventory_Model_Stock_Item
{
    const XML_PATH_PREORDER_MESSAGE = 'cataloginventory/item_options/preorder_message';
    
    public function checkQuoteItemQty($qty, $summaryQty, $origQty = 0)
    {
        $result = parent::checkQuoteItemQty($qty, $summaryQty, $origQty);
        $_product = $this->getProduct();
        if(Mage::helper('pre_order')->canPreOrder($_product)){
            $result->setMessage(Mage::helper('pre_order')->getPreOrderMessage($_product));
        }
        return $result;
    }
    
    public function getPreorderMessage()
    {
        if ($this->getUseConfigPreorderMessage()) {
            return Mage::getStoreConfig(self::XML_PATH_PREORDER_MESSAGE);
        }
        return $this->getData('preorder_message');
    }
}
