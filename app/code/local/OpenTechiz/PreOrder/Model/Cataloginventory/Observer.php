<?php

class OpenTechiz_PreOrder_Model_Cataloginventory_Observer extends Mage_CatalogInventory_Model_Observer
{
    protected function _prepareItemForSave($item, $product)
    {
        parent::_prepareItemForSave($item, $product);
        if (!is_null($product->getData('stock_data/preorder_message'))
            && is_null($product->getData('stock_data/use_config_preorder_message'))) {
            $item->setData('use_config_preorder_message', false);
        }
        return $this;
    }
    
    public function copyInventoryData($observer)
    {
        /** @var Mage_Catalog_Model_Product $currentProduct */
        $currentProduct = $observer->getEvent()->getCurrentProduct();
        /** @var Mage_Catalog_Model_Product $newProduct */
        $newProduct = $observer->getEvent()->getNewProduct();

        $newProduct->unsStockItem();
        $stockData = array(
            'use_config_min_qty'        => 1,
            'use_config_min_sale_qty'   => 1,
            'use_config_max_sale_qty'   => 1,
            'use_config_backorders'     => 1,
            'use_config_preorder_message'     => 1,
            'use_config_notify_stock_qty' => 1
        );
        $currentStockItem = $currentProduct->getStockItem();
        if ($currentStockItem) {
            $stockData += array(
                'use_config_enable_qty_inc'  => $currentStockItem->getData('use_config_enable_qty_inc'),
                'enable_qty_increments'             => $currentStockItem->getData('enable_qty_increments'),
                'use_config_qty_increments'         => $currentStockItem->getData('use_config_qty_increments'),
                'qty_increments'                    => $currentStockItem->getData('qty_increments'),
            );
        }
        $newProduct->setStockData($stockData);

        return $this;
    }
}
