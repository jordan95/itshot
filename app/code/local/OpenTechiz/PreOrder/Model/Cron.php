<?php
class OpenTechiz_PreOrder_Model_Cron
{
    const DISPLAY_CUSTOM_STOCK_ATTRIBUTE_CODE = 'c2c_disp_custom_stock_msg';

    public function updateDisplayCustomOutOfStock()
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addAttributeToSelect(self::DISPLAY_CUSTOM_STOCK_ATTRIBUTE_CODE)
            ->addFieldToFilter(self::DISPLAY_CUSTOM_STOCK_ATTRIBUTE_CODE, 1)
            ->addFieldToFilter('status', 1);

        $collection->joinTable(
            ['oii' => $collection->getResource()->getTable('opentechiz_inventory/instock')],
            'product_id=entity_id',
            [
                'qty' => 'qty',
                'instock_sku' => 'sku',
                'product_id' => 'product_id',
            ],
            ['qty' => ['gt' => 0]]
        );

        $collection->joinTable(
            ['soi' => $collection->getResource()->getTable('sales/order_item')],
            'sku=instock_sku',
            ['order_id' => 'order_id'],
            []
        );

        $collection->joinTable(
            ['so' => $collection->getResource()->getTable('sales/order')],
            'entity_id=order_id',
            ['order_created_at' => 'MAX(so.created_at)'],
            []
        );

        $collection->groupByAttribute('product_id');
        $collection->groupByAttribute('entity_id');

        $productIds = $collection->getColumnValues('entity_id');

        if (!$productIds) {
            return;
        }

        $updatedProducts = [];
        foreach ($productIds as $productId) {
            $product = Mage::getModel('catalog/product')->load($productId);
            if (!$product || !$product->getId()) {
                continue;
            }
            $product->setData(self::DISPLAY_CUSTOM_STOCK_ATTRIBUTE_CODE, 0);
            $product->save();
            $updatedProducts[] = $product;
        }

        if ($updatedProducts) {
            $this->sendEmail($updatedProducts);
        }
    }

    /**
     * @param array $products
     */
    private function sendEmail($products){
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        if (!$to = Mage::getStoreConfig('cataloginventory/item_options/preorder_email')) {
            return;
        }
        $to = explode(',', $to);

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('preorder_display_updated_products');
        $emailTemplate->setTemplateSubject('Custom Out of Stock set to No');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = ['products' => $products];

        foreach ($to as $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send(trim($recipientEmail), null, $emailTemplateVariables);
            }
        }
    }
}
