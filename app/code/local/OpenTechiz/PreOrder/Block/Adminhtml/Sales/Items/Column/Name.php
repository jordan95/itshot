<?php

if (Mage::helper('core')->isModuleEnabled('OpenTechiz_OrdersEditExtend')){
    class OpenTechiz_PreOrder_Block_Adminhtml_Sales_Items_Column_Name_Pure extends OpenTechiz_OrdersEditExtend_Block_Adminhtml_Order_Item_Column_Name{}
}else {
    class OpenTechiz_PreOrder_Block_Adminhtml_Sales_Items_Column_Name_Pure extends Mage_Adminhtml_Block_Sales_Items_Column_Name{}
}

class OpenTechiz_PreOrder_Block_Adminhtml_Sales_Items_Column_Name extends Mage_Adminhtml_Block_Sales_Items_Column_Name
{
    public function getTemplate()
    {
        if (Mage::helper('core')->isModuleEnabled('OpenTechiz_OrdersEditExtend')){
            return 'preorder/sales/items/column/sales-items-column-name.phtml';
        }else {
            return 'preorder/sales/items/column/name.phtml';
        }
    }
}
