<?php

class OpenTechiz_PreOrder_Block_Adminhtml_Catalog_Product_Edit_Tab_Inventory extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Inventory
{

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('preorder/catalog/product/tab/inventory.phtml');
    }

}
