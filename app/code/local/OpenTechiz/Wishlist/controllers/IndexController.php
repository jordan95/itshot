<?php

class OpenTechiz_Wishlist_IndexController extends Mage_Core_Controller_Front_Action
{

    public function loginRequiredAction()
    {
        $request = $this->getRequest();
        $referer ='';
        try {
            $referer = $this->getWishlistModel()->generateRefererUrl(
                    $request->getParam('p'), $request->getParam('entity_type', 'product')
            );
            $type = $request->getParam('type', 'login');
            if ($type === 'login') {
                $url = Mage::getUrl('customer/account/login', array('referer' => Mage::helper('core')->urlEncode($referer)));
            } else {
                $url = Mage::getUrl('customer/account/create', array('referer' => Mage::helper('core')->urlEncode($referer)));
            }
        } catch (Exception $exc) {
            $url = Mage::getUrl();
        }
        if($referer){
            Mage::getSingleton('customer/session')->setBeforeAuthUrl($referer);
        }
        header('Location:' . $url);
        exit;
    }
    
    public function redirectToAction(){
        $request = $this->getRequest();
        $target = $request->getParam('target', false);
        $type = $request->getParam('type', 'login');
        $url = Mage::getUrl('customer/account/'. $type, array('referer' => Mage::helper('core')->urlEncode($target)));
        Mage::getSingleton('customer/session')->setBeforeAuthUrl($target);
        header('Location:' . $url);
        exit;
    }

    public function getWishlistModel()
    {
        return Mage::getSingleton('opentechiz_wishlist/wishlist');
    }

    public function saveForLaterAction()
    {
        if ($this->_validateFormKey()) {
            $id = (int) $this->getRequest()->getParam('id');
            $sku = $this->getRequest()->getParam('quote_product_sku');
            if ($id ) {
                try {
                    if($sku) $this->getWishlistModel()->saveItemForLater($id, $sku);
                    else  $this->getWishlistModel()->saveItemForLater($id);
                    $this->_getSession()->addSuccess($this->__('You have saved this 1 item for later.'));
                } catch (Mage_Core_Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                } catch (Exception $e) {
                    $this->_getSession()->addError($this->__('Cannot save item for later.'));
                    Mage::logException($e);
                }
            }
        } else {
            $this->_getSession()->addError($this->__('Cannot save item for later.'));
        }
        $this->_redirect('checkout/cart');
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }
    
    public function addToCartAction()
    {
        if ($this->_validateFormKey()) {
            $id = (int) $this->getRequest()->getParam('id');
            if ($id) {
                try {
                    $this->getWishlistModel()->addToCart($id);
                    $this->_getSession()->addSuccess($this->__('You have added item to cart successfully.'));
                    $item  = Mage::getModel('amlist/item');
                    $item->load($id);
                    $item->delete();
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                    Mage::logException($e);
                }
            }
        } else {
            $this->_getSession()->addError($this->__('Cannot add item to cart.'));
        }
        $this->_redirect('checkout/cart');
    }

}
