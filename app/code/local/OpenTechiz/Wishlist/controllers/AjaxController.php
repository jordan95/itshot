<?php

class OpenTechiz_Wishlist_AjaxController extends Mage_Core_Controller_Front_Action
{

    public function reactAction()
    {
        $request = $this->getRequest();
        if ($request->isAjax()) {
            $response = new Varien_Object();
            try {
                $this->getWishlistModel()->react($request->getParam('p'), $request->getParam('type', 'like'));
                $response->setCode(0);
                $response->setMessage('Success');
            } catch (Exception $e) {
                $response->setCode($e->getCode());
                $response->setMessage($e->getMessage());
            }

            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody($response->toJson());
        } else {
            $redirect_url = $request->getQuery('redirect_url', Mage::getBaseUrl());
            try {
                $this->getWishlistModel()->react($request->getParam('p'));
                Mage::getSingleton("core/session")->addSuccess("You have reacted success.");
            } catch (Exception $exc) {
                Mage::getSingleton("core/session")->addError($exc->getMessage());
            }
            $this->_redirectUrl($redirect_url);
        }
    }

    public function getWishlistModel()
    {
        return Mage::getSingleton('opentechiz_wishlist/wishlist');
    }
    
    public function addToCartAction()
    {
        $request = $this->getRequest();
        $response = new Varien_Object();
        $response->setCode(1);
        $response->setMessage($this->__('404, Not found!'));
        if ($request->isAjax()) {
            if (!$this->_validateFormKey()) {
                $response->setMessage($this->__('Form Key is not valid!'));
            }
            try {
                $this->getWishlistModel()->addToCart($request->getParam('id'));
                $response->setCode(0);
                $response->setMessage($this->__('You have added item to cart successfully'));
            } catch (Exception $e) {
                $response->setCode($e->getCode());
                $response->setMessage($e->getMessage());
            }
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($response->toJson());
    }

}
