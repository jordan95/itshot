<?php

class OpenTechiz_Wishlist_Model_Observer
{

    public function getProductsLiked(Varien_Event_Observer $observer)
    {
        if(Mage::registry('wishlist_liked') !== null){
            return;
        }
        $collection = $observer->getCollection();
        $pids = array();
        foreach ($collection as $_product){
            $pids[] = $_product->getId();
        }
        if(count($pids) > 0){
            $wishlist_liked = array();
            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $customer = Mage::getSingleton('customer/session')->getCustomer();
                $list = Mage::getModel('amlist/list')->getCollection();
                $list->addCustomerFilter($customer->getId());
                $wIds = $list->getAllIds();
                if (count($wIds) > 0) {
                    $item = Mage::getModel('amlist/item')->getCollection();
                    $item->getSelect()
                            ->join(
                                    array('amlist' => $item->getResource()->getTable('amlist/list')), 'amlist.list_id = main_table.list_id', array()
                    );
                    $item->addFieldToFilter('amlist.list_id', array('in' => $wIds));
                    $item->addFieldToFilter('main_table.product_id', array('in' => $pids));
                    $item->getSelect()->reset(Zend_Db_Select::COLUMNS);
                    $item->getSelect()->columns(array('product_id'));
                    if($item->count()){
                        foreach ($item as $m){
                            $wishlist_liked[] = $m->getProductId();
                        }
                    }
                }
            }
            Mage::register('wishlist_liked', $wishlist_liked);
        }
    }

}
