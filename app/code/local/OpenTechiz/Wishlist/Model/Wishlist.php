<?php

class OpenTechiz_Wishlist_Model_Wishlist
{

    public function createList($data)
    {
        return $this->getListModel()
                        ->setData($data)
                        ->save();
    }

    public function getListId()
    {
        $list_id = $this->getListModel()->getLastListId($this->getCustomerId());
        if (!$list_id) {
            $list = $this->createList(array(
                'title' => 'React List',
                'customer_id' => $this->getCustomerId(),
                'is_default' => 1
            ));
            $list_id = $list->getId();
        }
        return $list_id;
    }

    public function react($product_id, $type = 'like')
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            throw new \Exception($this->__('Please login'), 4);
        }

        $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($product_id);
        if (!$product->getId()) {
            throw new \Exception($this->__('Product Unavaiable'), 3);
        }
        $list_id = $this->getListId();
        $item = Mage::getModel('amlist/item')
                ->setProductId($product->getId())
                ->setBuyRequest($this->prepareBuyRequest($product, true))
                ->setListId($list_id)
                ->setQty(1);
        $id = $item->findDuplicate();
        if ($type === 'like') {
            if ($id) {
                throw new \Exception($this->__('You are liked it'), 1);
            }
            $item->save();
        } else {
            if (!$id) {
                throw new \Exception($this->__('You are unlike it'), 1);
            }
            $item->setId($id);
            $item->delete();
        }
    }

    public function saveItemForLater($quote_item_id)
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            throw new \Exception($this->__('Please login'), 4);
        }
        $quote_item = $this->_getCart()->getQuote()->getItemById($quote_item_id);
        if (!$quote_item || !$quote_item->getId()) {
            throw new \Exception('Quote item is not exist');
        }

        $list_id = $this->getListId();
        $options = array();
        $product = $quote_item->getProduct();
        $custom_options = Mage::helper('catalog/product_configuration')->getCustomOptions($quote_item);
        foreach ($custom_options as $opt) {
            if (isset($opt['option_type']) && $opt['option_type'] == 'drop_down') {
                $vals = $product->getOptionById($opt['option_id'])->getValues();
                foreach ($vals as $val) {
                    if ($val->getData('title') == $opt['value']) {
                        $options[$opt['option_id']] = $val->getData('option_type_id');
                    }
                }
            } else {
                $options[$opt['option_id']] = $opt['value'];
            }
        }

        $item = Mage::getModel('amlist/item')
                ->setProductId($quote_item->getProductId())
                ->setBuyRequest(serialize(array(
                    'product' => $quote_item->getProductId(),
                    'options' => $options,
                )))
                ->setListId($list_id)
                ->setQty($quote_item->getQty());
        $id = $item->findDuplicate();
        if ($id) {
            $item->setId($id);
        }
        $item->save();
        $this->_getCart()->removeItem($quote_item_id)
                ->save();
    }

    protected function prepareBuyRequest(Mage_Catalog_Model_Product $product, $serialize = false)
    {
        $productOptions = $product->getOptions();
        $options = array();
        foreach ($productOptions as $_option) {
            if ($_option->getIsRequire() && $_option->getGroupByType($_option->getType()) === Mage_Catalog_Model_Product_Option::OPTION_GROUP_SELECT) {
                $value = current($_option->getValues());
                $options[$_option->getId()] = $_option->getDefaultValue() ? $_option->getDefaultValue() : $value->getId();
            }
        }
        $data = array(
            'product' => $product->getId(),
            'options' => $options
        );
        return $serialize ? serialize($data) : $data;
    }

    public function generateProductRefererUrl($id)
    {
        $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($id);
        if (!$product->getId()) {
            throw new \Exception($this->__('Product Unavaiable'), 3);
        }
        $query = array();
        if (Mage::getSingleton('customer/session')->getBeforeAuthUrl()) {
            $query['redirect_url'] = Mage::getSingleton('customer/session')->getBeforeAuthUrl();
        }
        return Mage::getUrl('ot_wishlist/ajax/react', array('p' => $id, '_query' => $query));
    }

    public function generateRefererUrl($id, $entity_type = 'product')
    {
        $url = Mage::getUrl();
        switch ($entity_type) {
            case 'product': {
                    $url = $this->generateProductRefererUrl($id);
                    break;
                }
            case 'quote_item': {
                    $url = Mage::helper('opentechiz_wishlist')->getSaveForLaterUrl($id);
                    break;
                }
        }

        return $url;
    }

    protected function getListModel()
    {
        return Mage::getModel('amlist/list');
    }

    protected function getCustomerId()
    {
        return Mage::getSingleton('customer/session')->getCustomer()->getId();
    }

    public function __($text)
    {
        return Mage::helper('opentechiz_wishlist')->__($text);
    }

    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    public function addToCart($item_id)
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            throw new \Exception($this->__('Please login'), 4);
        }
        if (!$this->canAccess($item_id, $this->getCustomerId())) {
            throw new \Exception($this->__('You can not access this item.'), 4);
        }
        $item = Mage::getModel('amlist/item')->load($item_id);
        if (!$item->getId()) {
            throw new \Exception($this->__('404, Not found!'), 4);
        }

        $qty = $item->getQty();
        $product = Mage::getModel('catalog/product')
                ->load($item->getProductId())
                ->setQty(max(0.01, $qty));

        $req = unserialize($item->getBuyRequest());
        $req['qty'] = $product->getQty();
        $quote = $this->_getCart();
        $quote->addProduct($product, $req);
        $quote->save();
    }

    public function canAccess($item_id, $customer_id)
    {
        return (bool) $this->getConnection()->fetchOne(
                        $this->getConnection()
                                ->select()
                                ->from(array('a' => $this->getTableName('am_list_item')), array('item_id'))
                                ->join(array('b' => $this->getTableName('am_list')), 'a.list_id = b.list_id', array())
                                ->where('a.item_id = ?', $item_id)
                                ->where('b.customer_id = ?', $customer_id)
        );
    }

    public function getTableName($table)
    {
        return $this->getResource()->getTableName($table);
    }

    public function getConnection()
    {
        return $this->getResource()->getConnection('core_read');
    }

    public function getResource()
    {
        return \Mage::getSingleton('core/resource');
    }

}
