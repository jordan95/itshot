<?php

class OpenTechiz_Wishlist_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function canLike(Mage_Catalog_Model_Product $product)
    {
        return !$this->isLiked($product) && Mage::getSingleton('customer/session')->isLoggedIn();
    }

    public function isLiked(Mage_Catalog_Model_Product $product)
    {
        return in_array($product->getId(), $this->getProductsLiked());
    }

    public function getProductsLiked()
    {
        return Mage::registry('wishlist_liked');
    }

    public function getSaveForLaterUrl($item_id)
    {
        return Mage::getUrl('ot_wishlist/index/saveForLater', array(
                    'id' => $item_id,
                    'form_key' => Mage::getSingleton('core/session')->getFormKey(),
                    Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => Mage::helper('core/url')->getEncodedUrl()
        ));
    }
    
    public function getAddToCartUrl($item_id)
    {
        return Mage::getUrl('ot_wishlist/index/addToCart', array(
                    'id' => $item_id,
                    'form_key' => Mage::getSingleton('core/session')->getFormKey(),
                    Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => Mage::helper('core/url')->getEncodedUrl()
        ));
    }

    public function isActive()
    {
        return Mage::getStoreConfig('amlist/general/active');
    }

}
