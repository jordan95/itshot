<?php

class OpenTechiz_ShopbyExtended_Model_Url_Builder extends Amasty_Shopby_Model_Url_Builder {

    public function getNotFilterQueryParams() {
        $this->updateEffectiveQuery();
        $query = array();
        
        foreach ($this->effectiveQuery as $origAttrCode => $ids) {
            $attrCode = str_replace(array('_', '-'), self::$specialChar, $origAttrCode);

            if ((!isset(self::$optionsHash[$attrCode]))) {
                $query[] = array(
                    'code' => $origAttrCode,
                    'option' => addslashes($ids),
                    'type' => 'get'
                );
            }
        }

        return $query;
    }

}
