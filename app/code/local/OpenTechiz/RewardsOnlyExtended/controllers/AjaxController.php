<?php

class OpenTechiz_RewardsOnlyExtended_AjaxController extends Mage_Core_Controller_Front_Action
{

    public function predictpointsAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            header('HTTP/1.0 403 Forbidden');
            die();
        }
        $data = array(
            'errno' => 1,
            'msg' => $this->__('404, Not found')
        );
        $pid = $this->getRequest()->getPost('pid', false);
        if ($pid) {
            $products = Mage::getModel('catalog/product')->getCollection()
                    ->addAttributeToFilter('entity_id', array('in' => $pid));
            foreach ($products as $product) {
                $predictPointsBlock = $this->getLayout()->createBlock('rewards/product_predictpoints', 'rewards_catalog_product_list_predictpoints');
                $predictPointsBlock->setProduct($product);
                $data['html'][$product->getId()] = $predictPointsBlock->toHtml();
            }
            $data['errno'] = 0;
            $data['msg'] = $this->__('Success');
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(json_encode($data));
    }

}
