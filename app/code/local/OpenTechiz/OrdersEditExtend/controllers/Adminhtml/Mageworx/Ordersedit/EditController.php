<?php

require_once(Mage::getModuleDir('controllers', 'MageWorx_OrdersEdit') . DS . 'Adminhtml' . DS . 'Mageworx' . DS . 'Ordersedit' . DS . 'EditController.php');

class OpenTechiz_OrdersEditExtend_Adminhtml_Mageworx_Ordersedit_EditController extends MageWorx_OrdersEdit_Adminhtml_Mageworx_Ordersedit_EditController
{

    protected $_availableTotals = array(
        'shipping_tax_amount',
        'base_shipping_tax_amount',
        'base_shipping_tax_amount',
        'subtotal',
        'base_subtotal',
        'subtotal_incl_tax',
        'base_subtotal_incl_tax',
        'grand_total',
        'base_grand_total',
        'tax_amount',
        'base_tax_amount',
        'discount_amount',
        'base_discount_amount',
        'shipping_amount',
        'base_shipping_amount',
        'shipping_incl_tax',
        'base_shipping_incl_tax',
        'hidden_tax_amount',
        'base_hidden_tax_amount'
    );
    protected $_convertor;

    protected function _construct()
    {
        $this->_convertor = Mage::getModel('sales/convert_order');
    }

    protected function init($applyNewChanges = false)
    {
        // Get base order id and load order and quote
        $orderId = $this->getRequest()->getParam('order_id');

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($orderId);
        $this->order = $order;
        $this->origOrder = clone $order;
        Mage::register('ordersedit_order', $order);

        /* @var $quote Mage_Sales_Model_Quote */
        $quote = Mage::getModel('mageworx_ordersedit/edit')->getQuoteByOrder($order);
        $quote->setIsSuperMode(true);

        // Get id of the currently edited block
        $blockId = $this->getRequest()->getParam('block_id');
        $editedBlock = $this->getRequest()->getParam('edited_block');
        $this->blockId = $blockId ? $blockId : $editedBlock;
        Mage::register('ordersedit_block_id', $blockId);

        // Get pending changes
        $pendingChanges = $this->getMwEditHelper()->getPendingChanges($orderId);
        if ($applyNewChanges) {
            $data = $this->getRequest()->getPost();
            $pendingChanges = $this->getMwEditHelper()->addPendingChanges($orderId, $data);
            $this->order->addData($data);
        }

        $surcharge = $this->getRequest()->getParam('surcharge');
        if ($surcharge) {
            $pendingChanges['surcharge'] = $surcharge;
            $this->surchargeFlag = true;
        }

        $this->pendingChanges = $pendingChanges;
        Mage::register('ordersedit_pending_changes', $pendingChanges);

        // Update quote if pending changes exists
        if (!empty($pendingChanges)) {
            $quote = Mage::getSingleton('mageworx_ordersedit/edit_quote')->applyDataToQuote($quote, $pendingChanges);
        }
        $this->_getRewardsSession()
                ->setEditQuoteId($quote->getId());
        $quote->setTotalsCollectedFlag(false)
                ->collectTotals();
        $quote->save();
        $this->quote = $quote;
        Mage::register('ordersedit_quote', $quote);
        return $this;
    }
    
    protected function _getRewardsSession()
    {
        return Mage::getSingleton('rewards/session');
    }

    public function saveOrderAction()
    {
        $this->init();
        /** @var MageWorx_OrdersEdit_Model_Edit $editModel */
        $editModel = Mage::getSingleton('mageworx_ordersedit/edit');
        /** @var MageWorx_OrdersEdit_Model_Edit_Quote $editQuoteModel */
        $editQuoteModel = Mage::getSingleton('mageworx_ordersedit/edit_quote');

        try {
            if (!empty($this->pendingChanges)) {
                // We can not save the order with the grand total smaller than 0
                if ($this->quote->getBaseGrandTotal() < 0) {
                    throw new Exception('GT < 0');
                }
                $canSave = false;
                $items = $this->quote->getAllItems();
                if(count($items) == 0) {
                     throw new Exception('No items selected.');
                }
                foreach ( $items as $quoteItem) {
                    if($quoteItem->getProductType() != "virtual") {
                        $canSave = true;
                    }
                }
                if(!$canSave && $this->quote->getBaseGrandTotal() == 0) {
                     throw new Exception('All items are virtual product type.');
                }

                Mage::dispatchEvent('mwoe_save_order_before', array(
                    'quote' => $this->quote,
                    'order' => $this->order,
                    'orig_order' => $this->origOrder,
                    'changes' => $this->pendingChanges
                ));

                //check if order changed in quote item
                $isChangedInQuoteItem = false;
                if(isset($this->pendingChanges['quote_items']) || isset($this->pendingChanges['product_to_add'])){
                    $isChangedInQuoteItem = true;
                }
                Mage::register('quote_order_item_changed_after_edit', $isChangedInQuoteItem);
                
                if(isset($this->pendingChanges['redemption_uses'])) {
                    $this->order->setRewardsDiscountAmount($this->quote->getRewardsDiscountAmount());
                    $this->order->setRewardsDiscountTaxAmount($this->quote->getRewardsDiscountTaxAmount());
                    $this->order->setRewardsBaseDiscountAmount($this->quote->getRewardsBaseDiscountAmount());
                    $this->order->setRewardsBaseDiscountTaxAmount($this->quote->getRewardsBaseDiscountTaxAmount());
                    $this->order->setRewardsCartDiscountMap($this->quote->getRewardsCartDiscountMap());
                }
                $this->order->setAppliedRuleIds($this->quote->getAppliedRuleIds());
                // Applies the pending changes to the quote and save the order
                $editModel->setQuote($this->quote);
                $editModel->setOrder($this->order);
                $editModel->setChanges($this->pendingChanges);
                $editModel->saveOrder();

                // Removes "is_temporary" flag from the items
                $editQuoteModel->saveTemporaryItems($this->quote, 0, false);

                Mage::dispatchEvent('mwoe_save_order_after', array(
                    'quote' => $this->quote,
                    'order' => $this->order,
                    'orig_order' => $this->origOrder,
                    'changes' => $this->pendingChanges
                ));

                Mage::dispatchEvent('refresh_order_totals', array(
                    'order' => $this->order
                ));

                // Create an invoice or credit memo for the saved order if needed
                $this->afterSaveOrder();
            }

            // Remove pending changes from the session
            $this->resetPendingChanges();
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The order changes have been saved'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')
                    ->addError($this->__('An error occurred while saving the order ' . $e->getMessage()));
        }

        $this->_redirectReferer();
    }

    public function loadEditFormAction()
    {
        $this->init();
        $block = $this->getBlockDataById();
        if (!$block || !$this->order) {
            return $this;
        }

        if (empty($this->pendingChanges)) {
            if (Mage::getModel('tax/config')->shippingPriceIncludesTax()) {
                Mage::getSingleton('adminhtml/session_quote')
                        ->setData('base_shipping_custom_price', $this->order->getBaseShippingInclTax());
            } else {
                Mage::getSingleton('adminhtml/session_quote')
                        ->setData('base_shipping_custom_price', $this->order->getBaseShippingAmount());
            }
            foreach ($this->quote->getAllItems() as $quoteItem) {
                $orderItem = $this->order->getItemByQuoteItemId($quoteItem->getId());
                if (!$orderItem) {
                    $quoteItem->delete();
                    $this->quote->getItemsCollection()->removeItemByKey($quoteItem->getId());
                }
            }
            $this->removeTempQuoteItems();
        }

        /** @var Mage_Core_Block_Abstract $form */
        $form = $this->getLayout()->createBlock($block['block']);
        $form->addData(array('quote' => $this->quote, 'order' => $this->order));

        $buttons = $this->getLayout()->createBlock('core/template')
                ->setTemplate('mageworx/ordersedit/edit/buttons.phtml');

        // Render messages block
        $errors = $this->getLayout()->createBlock('adminhtml/messages')
                ->setMessages(Mage::getSingleton('adminhtml/session')->getMessages(true))
                ->getGroupedHtml();
        $customScript = '<script type="text/javascript">
                            var element = document.getElementById("telephone"); 
                            if(element){
                                var trimStr = element.value;
                                trimStr = trimStr.trim(element.value);
                                var rm_space_string = trimStr.replace(/\s+/g, "");
                                var rm_dash_string = rm_space_string.replace(/-/g, "");
                                element.value = rm_dash_string;
                                $("telephone").addEventListener("change", function(e){
                                    var trimStr = element.value;
                                    trimStr = trimStr.trim(element.value);
                                    var rm_space_string = trimStr.replace(/\s+/g, "");
                                    var rm_dash_string = rm_space_string.replace(/-/g, "");
                                    element.value = rm_dash_string;
                                });
                            }

                            var elementPostcode = document.getElementById("postcode"); 
                            if(elementPostcode){
                                var trimStr = elementPostcode.value;
                                trimStr = trimStr.trim(elementPostcode.value);
                                var rm_space_string = trimStr.replace(/\s+/g, "");
                                var rm_dash_string = rm_space_string.replace(/-/g, "");
                                elementPostcode.value = rm_dash_string;
                                $("telephone").addEventListener("change", function(e){
                                    var trimStr = elementPostcode.value;
                                    trimStr = trimStr.trim(elementPostcode.value);
                                    var rm_space_string = trimStr.replace(/\s+/g, "");
                                    var rm_dash_string = rm_space_string.replace(/-/g, "");
                                    elementPostcode.value = rm_dash_string;
                                });
                            }
                        </script>';

        $html = $errors . $form->toHtml() . $buttons->toHtml() . $customScript;
        $html = str_replace('var VatParameters', 'VatParameters', $html);
        $this->getResponse()->setBody($html);

        return $this;
    }

    protected function afterSaveOrder()
    {
        /** @var countable $invoices */
        $invoices = $this->order->getInvoiceCollection();
        $invoicesCount = count($invoices);
        if (!$invoicesCount) {
            // Do not process order without invoices
            return $this;
        }

        $alreadyPaidBase = $this->origOrder->getBaseTotalPaid() - $this->origOrder->getBaseTotalRefunded();
        $ordersBaseGT = $this->order->getBaseGrandTotal();
        $ordersBaseCanceled = $this->origOrder->getBaseTotalCanceled();

        $editCreditmemo = Mage::getSingleton('mageworx_ordersedit/edit_creditmemo');
        $_itemsToRefund = $editCreditmemo->getItemsToRefund();
        $lastInvoicePending = null;
        $lastInvoicePaid = null;
        foreach ($invoices as $invoice) {
            if ($invoice->canCancel()) {
                $lastInvoicePending = $invoice;
            }
            if ($invoice->getState() == Mage_Sales_Model_Order_Invoice::STATE_PAID) {
                $lastInvoicePaid = $invoice;
            }
        }
        if ($lastInvoicePending) {
            // Sync order info to last invoice pending
            $this->applyOrderToInvoice($this->order, $this->origOrder, $lastInvoicePending, $_itemsToRefund);
        } elseif ($lastInvoicePaid) {
            $this->applyOrderToInvoice($this->order, $this->origOrder, $lastInvoicePaid, $_itemsToRefund);
        }

        $creditmemos = $this->order->getCreditmemosCollection();
        $needCreditmemo = false;

        foreach ($_itemsToRefund as $id => $qty) {
            if ($id != 0) {
                $needCreditmemo = true;
                break;
            }
        }

        $lastCreditmemoPending = null;
        if (count($creditmemos) > 0) {
            foreach ($creditmemos as $creditmemo) {
                if ($creditmemo->canCancel()) {
                    $lastCreditmemoPending = $creditmemo;
                }
            }

            if ($lastCreditmemoPending) {
                // Sync order info to last credit pending
                foreach ($this->_availableTotals as $code) {
                    $diff = 0;
                    if ($lastInvoicePending) {
                        $diff = $this->order->getData($code) - $this->origOrder->getData($code);
                    } else if ($lastInvoicePaid) {
                        $diff = $this->order->getData($code) - $this->origOrder->getData($code);
                    }
                    if (!$diff) {
                        continue;
                    }
                    if ($ordersBaseGT < ($alreadyPaidBase - $ordersBaseCanceled)) {
                        $lastCreditmemoPending->setData($code, $lastCreditmemoPending->getData($code) + $diff);
                    } else {
                        $lastCreditmemoPending->setData($code, $lastCreditmemoPending->getData($code) - $diff);
                    }
                }
                $paymentRecord = Mage::getModel('cod/paymentgrid')->getCollection()->addFieldToFilter('credit_memo_id', $lastCreditmemoPending->getId());
                if ($lastCreditmemoPending->getBaseGrandTotal() <= 0 && $paymentRecord->count() == 0) {
                    $lastCreditmemoPending->isDeleted(true);
                }
                $lastCreditmemoPending->save();
            }
        }
        if (!$lastCreditmemoPending && ($needCreditmemo || $ordersBaseGT < ($alreadyPaidBase - $ordersBaseCanceled))) {
            Mage::getSingleton('mageworx_ordersedit/edit_creditmemo')->refundChanges(
                    $this->origOrder, $this->order
            );
        }
        $this->_getRewardsSession()->clear();
        return $this;
    }

    protected function applyOrderToCreditmemo($order, $creditmemo)
    {
        $creditmemo->setOrder($order)
                ->setStoreId($order->getStoreId())
                ->setCustomerId($order->getCustomerId())
                ->setBillingAddressId($order->getBillingAddressId())
                ->setShippingAddressId($order->getShippingAddressId());
        $total_qty = $creditmemo->getTotalQty();
        $order->setBaseSubtotalRefunded($order->getBaseSubtotalRefunded() - $creditmemo->getBaseSubtotal());
        $order->setSubtotalRefunded($order->getSubtotalRefunded() - $creditmemo->getSubtotal());

        $order->setBaseTaxRefunded($order->getBaseTaxRefunded() - $creditmemo->getBaseTaxAmount());
        $order->setTaxRefunded($order->getTaxRefunded() - $creditmemo->getTaxAmount());

        $order->setBaseShippingRefunded($order->getBaseShippingRefunded() - $creditmemo->getBaseShippingAmount());
        $order->setShippingRefunded($order->getShippingRefunded() - $creditmemo->getShippingAmount());
        $items = $creditmemo->getItemsCollection();
        foreach ($items as $_creditmemoItem) {
            $orderItem = Mage::getModel('sales/order_item')->load($_creditmemoItem->getOrderItemId());
            if (!$orderItem->getId()) {
                $total_qty -= $_creditmemoItem->getQty();
                $_creditmemoItem->isDeleted(true);
                $_creditmemoItem->delete();
                $items->removeItemByKey($_creditmemoItem->getId());
            } else {
                $orderItem = $_creditmemoItem->getOrderItem();
                $orderItem->setQtyRefunded($orderItem->getQtyRefunded() - $_creditmemoItem->getQty());
                $orderItem->setTaxRefunded($orderItem->getTaxRefunded() - $orderItem->getBaseTaxAmount() * $_creditmemoItem->getQty() / $orderItem->getQtyOrdered());
                $orderItem->setHiddenTaxRefunded($orderItem->getHiddenTaxRefunded() - $orderItem->getHiddenTaxAmount() * $_creditmemoItem->getQty() / $orderItem->getQtyOrdered());
            }
        }
        foreach ($order->getAllItems() as $_orderItem) {
            if ($_orderItem->getRowTotalInclTax() == 0 ||
                    $_orderItem->getStatusId() != Mage_Sales_Model_Order_Item::STATUS_REFUNDED ||
                    $_orderItem->getQtyOrdered() <= $_orderItem->getQtyRefunded()) {
                continue;
            }
            $creditmemoItem = Mage::getModel('sales/order_creditmemo_item')->load($_orderItem->getId(), 'order_item_id');
            if (!$creditmemoItem->getId()) {
                $_creditmemoItem = $this->_convertor->itemToCreditmemoItem($_orderItem);
                $qty = $_orderItem->getQtyOrdered() - $_orderItem->getQtyRefunded();
                $total_qty += $qty;
                $_creditmemoItem->setQty($qty);
                $creditmemo->addItem($_creditmemoItem);
            } else {
                $orderItem = $_creditmemoItem->getOrderItem();
                $orderItem->setQtyRefunded($orderItem->getQtyRefunded() - $_creditmemoItem->getQty());
                $orderItem->setTaxRefunded($orderItem->getTaxRefunded() - $orderItem->getBaseTaxAmount() * $_creditmemoItem->getQty() / $orderItem->getQtyOrdered());
                $orderItem->setHiddenTaxRefunded($orderItem->getHiddenTaxRefunded() - $orderItem->getHiddenTaxAmount() * $_creditmemoItem->getQty() / $orderItem->getQtyOrdered());
            }
        }

        $creditmemo->setTotalQty($total_qty);
        foreach ($this->_availableTotals as $code) {
            $creditmemo->setData($code, 0);
        }

        $creditmemo->collectTotals();

        $order->setBaseSubtotalRefunded($order->getBaseSubtotalRefunded() - $creditmemo->getBaseSubtotal());
        $order->setSubtotalRefunded($order->getSubtotalRefunded() - $creditmemo->getSubtotal());

        $order->setBaseTaxRefunded($order->getBaseTaxRefunded() - $creditmemo->getBaseTaxAmount());
        $order->setTaxRefunded($order->getTaxRefunded() - $creditmemo->getTaxAmount());

        $order->setBaseShippingRefunded($order->getBaseShippingRefunded() - $creditmemo->getBaseShippingAmount());
        $order->setShippingRefunded($order->getShippingRefunded() - $creditmemo->getShippingAmount());

        $transaction = Mage::getModel('core/resource_transaction')
                ->addObject($creditmemo)
                ->addObject($creditmemo->getOrder());

        $transaction->save();
    }

    protected function applyOrderToInvoice($order, $origOrder, $invoice, &$_itemsToRefund = array())
    {
        /* @var $invoice Mage_Sales_Model_Order_Invoice */
        $invoice->setOrder($order)
                ->setStoreId($order->getStoreId())
                ->setCustomerId($order->getCustomerId())
                ->setBillingAddressId($order->getBillingAddressId())
                ->setShippingAddressId($order->getShippingAddressId());
        $total_qty = $invoice->getTotalQty();
        if ($order->getTotalInvoiced() >= $invoice->getGrandTotal()) {
            $order->setTotalInvoiced($order->getTotalInvoiced() - $invoice->getGrandTotal());
            $order->setBaseTotalInvoiced($order->getBaseTotalInvoiced() - $invoice->getBaseGrandTotal());
        } else {
            $order->setTotalInvoiced(0);
            $order->setBaseTotalInvoiced(0);
        }
        
        if ($order->getSubtotalInvoiced() >= $invoice->getSubtotal()) {
            $order->setSubtotalInvoiced($order->getSubtotalInvoiced() - $invoice->getSubtotal());
            $order->setBaseSubtotalInvoiced($order->getBaseSubtotalInvoiced() - $invoice->getBaseSubtotal());
        } else {
            $order->setSubtotalInvoiced(0);
            $order->setBaseSubtotalInvoiced(0);
        }

        if ($order->getTaxInvoiced() >= $invoice->getTaxAmount()) {
            $order->setTaxInvoiced($order->getTaxInvoiced() - $invoice->getTaxAmount());
            $order->setBaseTaxInvoiced($order->getBaseTaxInvoiced() - $invoice->getBaseTaxAmount());
        } else {
            $order->setTaxInvoiced(0);
            $order->setBaseTaxInvoiced(0);
        }

        if ($order->getHiddenTaxInvoiced() >= $invoice->getHiddenTaxAmount()) {
            $order->setHiddenTaxInvoiced($order->getHiddenTaxInvoiced() - $invoice->getHiddenTaxAmount());
            $order->setBaseHiddenTaxInvoiced($order->getBaseHiddenTaxInvoiced() - $invoice->getBaseHiddenTaxAmount());
        } else {
            $order->setHiddenTaxInvoiced(0);
            $order->setBaseHiddenTaxInvoiced(0);
        }

        if ($order->getShippingTaxInvoiced() >= $invoice->getShippingTaxAmount()) {
            $order->setShippingTaxInvoiced($order->getShippingTaxInvoiced() - $invoice->getShippingTaxAmount());
            $order->setBaseShippingTaxInvoiced($order->getBaseShippingTaxInvoiced() - $invoice->getBaseShippingTaxAmount());
        } else {
            $order->setShippingTaxInvoiced(0);
            $order->setBaseShippingTaxInvoiced(0);
        }
        
        if ($order->getShippingInvoiced() >= $invoice->getShippingAmount()) {
            $order->setShippingInvoiced($order->getShippingInvoiced() - $invoice->getShippingAmount());
            $order->setBaseShippingInvoiced($order->getBaseShippingInvoiced() - $invoice->getBaseShippingAmount());
        } else {
            $order->setShippingInvoiced(0);
            $order->setBaseShippingInvoiced(0);
        }

        if ($order->getDiscountInvoiced() <= $invoice->getDiscountAmount()) {
            $order->setDiscountInvoiced($order->getDiscountInvoiced() - $invoice->getDiscountAmount());
            $order->setBaseDiscountInvoiced($order->getBaseDiscountInvoiced() - $invoice->getBaseDiscountAmount());
        } else {
            $order->setDiscountInvoiced(0);
            $order->setBaseDiscountInvoiced(0);
        }

        if ($order->getBaseTotalInvoicedCost() >= $invoice->getBaseCost()) {
            $order->setBaseTotalInvoicedCost($order->getBaseTotalInvoicedCost() - $invoice->getBaseCost());
            $order->setBaseCreditInvoiced($order->getBaseCreditInvoiced() - $invoice->getBaseCreditAmount());
        } else {
            $order->setBaseTotalInvoicedCost(0);
            $order->setBaseCreditInvoiced(0);
        }

        /* @var $items Mage_Sales_Model_Resource_Order_Invoice_Item_Collection */
        $items = $invoice->getItemsCollection();
        foreach ($items as $_invoiceItem) {
            $orderItem = Mage::getModel('sales/order_item')->load($_invoiceItem->getOrderItemId());
            if (!$orderItem->getId() || !empty($_itemsToRefund[$orderItem->getId()])) {
                $total_qty -= $_invoiceItem->getQty();
                $_invoiceItem->isDeleted(true);
                $_invoiceItem->delete();
                if ($orderItem->getId()) {
                    $orderItem->isDeleted(true);
                    $orderItem->delete();
                    $quoteItem = Mage::getModel('sales/quote_item')->load($orderItem->getQuoteItemId());
                    if ($quoteItem->getId()) {
                        $quoteItem->isDeleted(true);
                        $quoteItem->delete();
                    }
                }
                $items->removeItemByKey($_invoiceItem->getId());
                unset($_itemsToRefund[$orderItem->getId()]);
            } else {
                $orderItem = $order->getItemById($orderItem->getId());

                if ($orderItem->getQtyInvoiced() > $_invoiceItem->getQty()) {
                    $orderItem->setQtyInvoiced($orderItem->getQtyInvoiced() - $_invoiceItem->getQty());
                } else if ($orderItem->getQtyInvoiced() < 0 || $orderItem->getQtyInvoiced() <= $_invoiceItem->getQty()) {
                    $orderItem->setQtyInvoiced(0);
                }

                if ($_invoiceItem->getQty() > $orderItem->getQtyOrdered()) {
                    $_invoiceItem->setQty($orderItem->getQtyOrdered());
                } else {
                    $_invoiceItem->setQty($orderItem->getQtyOrdered() - $orderItem->getQtyInvoiced());
                }

                $orderItem->setTaxInvoiced($orderItem->getTaxInvoiced() - $_invoiceItem->getTaxAmount());
                $orderItem->setBaseTaxInvoiced($orderItem->getBaseTaxInvoiced() - $_invoiceItem->getBaseTaxAmount());

                $orderItem->setHiddenTaxInvoiced($orderItem->getHiddenTaxInvoiced() - $_invoiceItem->getHiddenTaxAmount());
                $orderItem->setBaseHiddenTaxInvoiced($orderItem->getBaseHiddenTaxInvoiced() - $_invoiceItem->getBaseHiddenTaxAmount());

                $orderItem->setDiscountInvoiced($orderItem->getDiscountInvoiced() - $_invoiceItem->getDiscountAmount());
                $orderItem->setBaseDiscountInvoiced($orderItem->getBaseDiscountInvoiced() - $_invoiceItem->getBaseDiscountAmount());

                $orderItem->setRowInvoiced($orderItem->getRowInvoiced() - $_invoiceItem->getRowTotal());
                $orderItem->setBaseRowInvoiced($orderItem->getBaseRowInvoiced() - $_invoiceItem->getBaseRowTotal());
            }
        }

        foreach ($order->getAllItems() as $_orderItem) {
            if ($_orderItem->getStatusId() == Mage_Sales_Model_Order_Item::STATUS_REFUNDED ||
                    $_orderItem->getQtyOrdered() <= $_orderItem->getQtyInvoiced()) {
                continue;
            }
            $invoiceItem = Mage::getModel('sales/order_invoice_item')->load($_orderItem->getId(), 'order_item_id');
            if (!$invoiceItem->getId()) {
                $_invoiceItem = $this->_convertor->itemToInvoiceItem($_orderItem);
                $qty = $_orderItem->getQtyOrdered() - $_orderItem->getQtyInvoiced();
                $total_qty += $qty;
                $_invoiceItem->setQty($qty);
                $invoice->addItem($_invoiceItem);
            }
        }

        $invoice->setTotalQty($total_qty);
        foreach ($this->_availableTotals as $code) {
            $invoice->setData($code, 0);
        }
        // Collect invoice totals
        $invoice->collectTotals();

        /* @var $order Mage_Sales_Model_Order */
        $order->setTotalInvoiced($order->getTotalInvoiced() + $invoice->getGrandTotal());
        $order->setBaseTotalInvoiced($order->getBaseTotalInvoiced() + $invoice->getBaseGrandTotal());

        $order->setSubtotalInvoiced($order->getSubtotalInvoiced() + $invoice->getSubtotal());
        $order->setBaseSubtotalInvoiced($order->getBaseSubtotalInvoiced() + $invoice->getBaseSubtotal());

        $order->setTaxInvoiced($order->getTaxInvoiced() + $invoice->getTaxAmount());
        $order->setBaseTaxInvoiced($order->getBaseTaxInvoiced() + $invoice->getBaseTaxAmount());

        $order->setHiddenTaxInvoiced($order->getHiddenTaxInvoiced() + $invoice->getHiddenTaxAmount());
        $order->setBaseHiddenTaxInvoiced($order->getBaseHiddenTaxInvoiced() + $invoice->getBaseHiddenTaxAmount());

        $order->setShippingTaxInvoiced($order->getShippingTaxInvoiced() + $invoice->getShippingTaxAmount());
        $order->setBaseShippingTaxInvoiced($order->getBaseShippingTaxInvoiced() + $invoice->getBaseShippingTaxAmount());

        $order->setShippingInvoiced($order->getShippingInvoiced() + $invoice->getShippingAmount());
        $order->setBaseShippingInvoiced($order->getBaseShippingInvoiced() + $invoice->getBaseShippingAmount());

        $order->setDiscountInvoiced($order->getDiscountInvoiced() + $invoice->getDiscountAmount());
        $order->setBaseDiscountInvoiced($order->getBaseDiscountInvoiced() + $invoice->getBaseDiscountAmount());
        $order->setBaseTotalInvoicedCost($order->getBaseTotalInvoicedCost() + $invoice->getBaseCost());

        foreach ($order->getAllItems() as $orderItem) {
            foreach ($invoice->getAllItems() as $invoiceItem) {
                if ($orderItem->getId() == $invoiceItem->getOrderItemId()) {
                    $orderItem->setQtyInvoiced($orderItem->getQtyInvoiced() + $invoiceItem->getQty());
                    $orderItem->setTaxInvoiced($orderItem->getTaxInvoiced() + $invoiceItem->getTaxAmount());
                    $orderItem->setBaseTaxInvoiced($orderItem->getBaseTaxInvoiced() + $invoiceItem->getBaseTaxAmount());

                    $orderItem->setHiddenTaxInvoiced($orderItem->getHiddenTaxInvoiced() + $invoiceItem->getHiddenTaxAmount());
                    $orderItem->setBaseHiddenTaxInvoiced($orderItem->getBaseHiddenTaxInvoiced() + $invoiceItem->getBaseHiddenTaxAmount());

                    $orderItem->setDiscountInvoiced($orderItem->getDiscountInvoiced() + $invoiceItem->getDiscountAmount());
                    $orderItem->setBaseDiscountInvoiced($orderItem->getBaseDiscountInvoiced() + $invoiceItem->getBaseDiscountAmount());

                    $orderItem->setRowInvoiced($orderItem->getRowInvoiced() + $invoiceItem->getRowTotal());
                    $orderItem->setBaseRowInvoiced($orderItem->getBaseRowInvoiced() + $invoiceItem->getBaseRowTotal());
                    break;
                }
            }
        }
        $invoice->setRewardsCartDiscountMap($order->getRewardsCartDiscountMap());

        $transaction = Mage::getModel('core/resource_transaction')
                ->addObject($invoice)
                ->addObject($invoice->getOrder());

        $transaction->save();
    }

    protected function _canInvoiceItem($item, $qtys = array())
    {
        if ($item->getLockedDoInvoice()) {
            return false;
        }
        $this->updateLocaleNumbers($qtys);

        if ($item->isDummy()) {
            if ($item->getHasChildren()) {
                foreach ($item->getChildrenItems() as $child) {
                    if (empty($qtys)) {
                        if ($child->getQtyToInvoice() > 0) {
                            return true;
                        }
                    } else {
                        if (isset($qtys[$child->getId()]) && $qtys[$child->getId()] > 0) {
                            return true;
                        }
                    }
                }
                return false;
            } else if ($item->getParentItem()) {
                $parent = $item->getParentItem();
                if (empty($qtys)) {
                    return $parent->getQtyToInvoice() > 0;
                } else {
                    return isset($qtys[$parent->getId()]) && $qtys[$parent->getId()] > 0;
                }
            }
        } else {
            return $item->getQtyToInvoice() > 0;
        }
    }

    protected function getMwHelper()
    {
        return Mage::helper('mageworx_ordersedit');
    }

}
