<?php

class OpenTechiz_OrdersEditExtend_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_SYSTEM_COMMENT_TEMPLATES = 'mageworx_ordersmanagement/ordersedit/system_comment_templates';

    public function collectTotals(Mage_Sales_Model_Order $origOrder, Mage_Sales_Model_Order $order)
    {
        $partialpaymentModel = Mage::getModel('partialpayment/partialpayment')->load($order->getId(), 'order_id');
        if (!$partialpaymentModel->getId()) {
            return;
        }
        $partialpaymentModel->setTotalAmount($order->getGrandTotal());
        if ($partialpaymentModel->getPaidAmount() > $order->getGrandTotal()) {
            $partialpaymentModel->setPaidAmount($order->getGrandTotal());
        }
        if ($order->getGrandTotal() != $origOrder->getGrandTotal()) {
            if ($origOrder->getGrandTotal() > $order->getGrandTotal()) {
                $diff = $origOrder->getGrandTotal() - $order->getGrandTotal();
                $partialpaymentModel->setRemainingAmount($partialpaymentModel->getRemainingAmount() - $diff);
                $partialpaymentModel->setRemainingInstallments($partialpaymentModel->getRemainingInstallments() - $diff);
                while ($diff > 0) {
                    $partial_payment_installments = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id', array('eq' => $partialpaymentModel->getId()));
                    $lastInstallment = $partial_payment_installments->getLastItem();
                    if ($diff >= $lastInstallment->getInstallmentAmount()) {
                        $partialpaymentModel->setTotalInstallments($partialpaymentModel->getTotalInstallments() - 1);
                        $lastInstallment->delete();
                        $remainingCount = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id', $partialpaymentModel->getId())->addFieldToFIlter('installment_status', 'Remaining')->getSize();
                        if (!$remainingCount) {
                            $diff = 0;
                        } else {
                            $diff = $diff - $lastInstallment->getInstallmentAmount();
                        }
                    } else {
                        $newAmount = $lastInstallment->getInstallmentAmount() - $diff;
                        $lastInstallment->setInstallmentAmount($newAmount)->save();
                        $diff = 0;
                    }
                }
            } else {
                $diff = $order->getGrandTotal() - $origOrder->getGrandTotal();
                $partialpaymentModel->setRemainingAmount($partialpaymentModel->getRemainingAmount() + $diff);
                $partialpaymentModel->setRemainingInstallments($partialpaymentModel->getRemainingInstallments() + $diff);
                $remainingCount = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id', $partialpaymentModel->getId())->addFieldToFIlter('installment_status', 'Remaining')->getSize();
                $partial_payment_installments = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id', array('eq' => $partialpaymentModel->getId()));
                $lastInstallment = $partial_payment_installments->getLastItem();
                if (!$remainingCount) {
                    $installment_due_date = time();
                    if ($lastInstallment->getInstallmentDueDate()) {
                        $installment_due_date = strtotime($lastInstallment->getInstallmentDueDate() . "+1 months");
                    }
                    if ($installment_due_date <= time()) {
                        $installment_due_date = strtotime("+1 months");
                    }
                    $installment_due_date = date("Y-m-d", $installment_due_date);
                    $data = array('partial_payment_id' => $partialpaymentModel->getId(), 'installment_amount' => $diff, 'installment_due_date' => $installment_due_date, 'installment_paid_date' => '', 'installment_status' => 'Remaining');
                    $partial_payment_installments = Mage::getModel('partialpayment/installment')->setData($data)->save();
                    $partialpaymentModel->setTotalInstallments($partialpaymentModel->getTotalInstallments() + 1);
                } else {
                    $newAmount = $lastInstallment->getInstallmentAmount() + $diff;
                    $lastInstallment->setInstallmentAmount($newAmount)->save();
                }
            }
        }
        $remainingInstallmentCount = Mage::getModel('partialpayment/installment')->getCollection()
                        ->addFieldToFilter('partial_payment_id', $partialpaymentModel->getId())
                        ->addFieldToFIlter('installment_status', 'Remaining')->getSize();
        $paidInstallmentCount = Mage::getModel('partialpayment/installment')->getCollection()
                        ->addFieldToFilter('partial_payment_id', $partialpaymentModel->getId())
                        ->addFieldToFIlter('installment_status', 'Paid')->getSize();
        $partialpaymentModel->setPaidInstallments($paidInstallmentCount)
                ->setRemainingInstallments($remainingInstallmentCount)
                ->save();
        // Re-collect order
        $this->recollectProducts($order);
    }

    public function recollectProducts($order)
    {
        $partialpaymentModel = Mage::getModel('partialpayment/partialpayment')->load($order->getId(), 'order_id');
        if (!$partialpaymentModel->getId()) {
            return;
        }

        $calculationModel = $this->getPartialpaymentCalculation();
        $partialpaymentHelper = $this->getPartialpaymentHelper();

        $quoteItemIds = [];
        foreach ($order->getAllVisibleItems() as $orderItem) {
            if ($orderItem->getRowTotalInclTax() == 0 || $orderItem->getStatusId() == Mage_Sales_Model_Order_Item::STATUS_REFUNDED) {
                continue;
            }
            $_product = Mage::getModel('catalog/product')->load($orderItem->getProductId());
            $product_total_amount = Mage::helper('tax')->getPrice($_product, $orderItem->getPrice(), false) * $orderItem->getQtyOrdered();
            $product_total_installments = $partialpaymentModel->getTotalInstallments();
            $product_paid_amount = ($product_total_amount / $product_total_installments) * $partialpaymentModel->getPaidInstallments();
            $product_paid_installments = $partialpaymentModel->getPaidInstallments();
            $product_remaining_installments = $product_total_installments - $product_paid_installments;
            $product_remaining_amount = $product_total_amount - $product_paid_amount;
            $product_downpayment_amount = 0.1 * $product_total_amount;

            $productModelData = array(
                'partial_payment_id' => $partialpaymentModel->getId(),
                'sales_flat_order_item_id' => $orderItem->getQuoteItemId(),
                'downpayment' => $partialpaymentHelper->setNumberFormat($product_downpayment_amount),
                'total_installments' => $product_total_installments,
                'paid_installments' => $product_paid_installments,
                'remaining_installments' => $product_remaining_installments,
                'total_amount' => $partialpaymentHelper->setNumberFormat($product_total_amount),
                'paid_amount' => $partialpaymentHelper->setNumberFormat($product_paid_amount),
                'remaining_amount' => $partialpaymentHelper->setNumberFormat($product_remaining_amount)
            );
            Mage::getModel('partialpayment/product')->load($orderItem->getQuoteItemId(), 'sales_flat_order_item_id')
                    ->addData($productModelData)
                    ->save();
            $quoteItemIds[] = $orderItem->getQuoteItemId();
        }
        if (count($quoteItemIds) > 0) {
            $partialPaymentProducts = Mage::getModel('partialpayment/product')
                    ->getCollection()
                    ->addFieldToFilter('partial_payment_id', $partialpaymentModel->getId())
                    ->addFieldToFilter('sales_flat_order_item_id', array('nin' => $quoteItemIds));
            if ($partialPaymentProducts->count() > 0) {
                foreach ($partialPaymentProducts as $partialProduct) {
                    $partialProduct->delete();
                }
            }
        }
    }

    public function getPartialpaymentCalculation()
    {
        return Mage::getModel("partialpayment/calculation");
    }

    public function getPartialpaymentHelper()
    {
        return Mage::helper('partialpayment/partialpayment');
    }

    /**
     * @param string $comment
     * @return bool
     */
    public function isSystemComment($comment)
    {
        foreach ($this->getSystemCommentTemplates() as $template) {
            if ($template && stripos($comment, $template) !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getSystemCommentTemplates()
    {
        $templates = preg_split('/\r\n|\r|\n/', trim(Mage::getStoreConfig(self::XML_SYSTEM_COMMENT_TEMPLATES)));
        return $templates ?? [];
    }
}
