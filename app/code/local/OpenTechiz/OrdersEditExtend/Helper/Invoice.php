<?php

class OpenTechiz_OrdersEditExtend_Helper_Invoice extends Mage_Core_Helper_Abstract
{

    public function getTotalPaid($invoiceId)
    {
        $total_paid = 0;
        $collection = Mage::getModel('cod/paymentgrid')->getCollection()
                ->addFieldToFilter('invoice_id', array('eq' => $invoiceId));
        if ($collection->count() > 0) {
            foreach ($collection as $item) {
                $total_paid += $item->getTotal();
            }
        }
        return $total_paid;
    }

}
