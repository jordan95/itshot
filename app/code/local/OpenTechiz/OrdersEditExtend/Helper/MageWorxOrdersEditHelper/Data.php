<?php

/**
 * MageWorx
 * Admin Order Editor extension
 *
 * @category   MageWorx
 * @package    MageWorx_OrdersEdit
 * @copyright  Copyright (c) 2016 MageWorx (http://www.mageworx.com/)
 */
class OpenTechiz_OrdersEditExtend_Helper_MageWorxOrdersEditHelper_Data extends MageWorx_OrdersEdit_Helper_Data
{
    public function isOrderEditable(Mage_Sales_Model_Order $order)
    {
        $orderStageAllowEdit = [0,1,2,3,4];
        if(!in_array($order->getOrderStage(), $orderStageAllowEdit)){
            return false;
        }

        /** @see MageWorx_OrdersSurcharge_Model_Observer_Order::checkIsOrderEditable() */
        if (Mage::getSingleton('adminhtml/session')->getBlockEditOrder()) {
            return false;
        }
        
        if ($order->getStatus() == "canceled" || $order->getState() == "canceled" || $order->getState() == "holded" || $order->getState() == "closed") {
            return false;
        }

        return true;
    }
    public function getRateCurrency(){
        $baseCurrency = Mage::app()->getStore()->getBaseCurrencyCode();
        $allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
        $rates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrency, array_values($allowedCurrencies));
        if(isset($rates)){
            return $rates; 
        }
        return false;
    }
    public function getConvertAmount($amount,$rates){
        $currency_code = Mage::app()->getStore()->getCurrentCurrencyCode();
        $baseCurrency = Mage::app()->getStore()->getBaseCurrencyCode();
        $allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
        $rates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrency, array_values($allowedCurrencies));
        if(isset($rates)){
            if($rates[$currency_code]!=0){
                $amount = $amount*$rates[$currency_code];
            }
            
        }
        return $amount;
    }
}
