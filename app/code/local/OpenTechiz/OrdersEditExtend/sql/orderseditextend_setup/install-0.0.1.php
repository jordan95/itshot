<?php
$installer = $this;
$installer->startSetup();

// Quote Address
$installer->addAttribute('quote_address', 'mwo_surcharge_amount', array('type'=>'decimal'));
$installer->addAttribute('quote_address', 'mwo_base_surcharge_amount', array('type'=>'decimal'));

// Quote
$installer->addAttribute('quote', 'mwo_surcharge_amount', array('type'=>'decimal'));
$installer->addAttribute('quote', 'mwo_base_surcharge_amount', array('type'=>'decimal'));

// Order
$installer->addAttribute('order', 'mwo_surcharge_amount', array('type'=>'decimal'));
$installer->addAttribute('order', 'mwo_base_surcharge_amount', array('type'=>'decimal'));

$installer->endSetup();
