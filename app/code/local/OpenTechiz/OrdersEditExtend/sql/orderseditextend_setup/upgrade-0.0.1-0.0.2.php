<?php
/** @var Mage_Sales_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

$installer->addAttribute(
    'order',
    'shipping_hold_at',
    [
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required' => false,
    ]
);

$installer->endSetup();
