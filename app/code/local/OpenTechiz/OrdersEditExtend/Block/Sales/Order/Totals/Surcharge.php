<?php
class OpenTechiz_OrdersEditExtend_Block_Sales_Order_Totals_Surcharge extends MageWorx_OrdersSurcharge_Block_Sales_Order_Totals_Surcharge{
    
    public function initTotals()
    {
        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();
        $this->_source = $parent->getSource();

        if ($this->_source->getMwoBaseSurchargeAmount() > 0) {
            $this->_addSurchargeTotal();
        }

        return $this;
    }
}
