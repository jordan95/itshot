<?php 
class OpenTechiz_OrdersEditExtend_Block_Adminhtml_Sales_Order_Totals_Surcharge extends MageWorx_OrdersSurcharge_Block_Adminhtml_Sales_Order_Totals_Surcharge{
    
    public function initTotals()
    {
        /** @var $parent Mage_Adminhtml_Block_Sales_Order_Totals|Mage_Adminhtml_Block_Sales_Order_Invoice_Totals */
        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();
        $this->_source = $parent->getSource();

        if ($this->_order->getMwoBaseSurchargeAmount() > 0) {
            $this->_addSurchargeTotal();
        }

        return $this;
    }
}
