<?php
/**
 * MageWorx
 * Admin Order Editor extension
 *
 * @category   MageWorx
 * @package    MageWorx_OrdersEdit
 * @copyright  Copyright (c) 2016 MageWorx (http://www.mageworx.com/)
 */

class OpenTechiz_OrdersEditExtend_Block_Adminhtml_Sales_Order_Changed_SpendPoints extends Mage_Adminhtml_Block_Widget
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_order_spendpoints');
        $this->setTemplate('opentechiz/ordersedit/changed/spendpoints.phtml');
    }

    /**
     * @return string
     */
    public function getCurrentSpendingPoints()
    {
        $quote = Mage::getSingleton ( 'rewards/session' )->getQuote ();
        $uses = $quote->getPointsSpending();
        $uses = min ( $quote->getMaxSpendablePoints (), $uses );
        return number_format($uses). " ItsHot Reward Points";
    }
}