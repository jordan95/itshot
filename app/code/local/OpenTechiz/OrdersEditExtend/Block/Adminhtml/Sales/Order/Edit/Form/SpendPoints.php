<?php

class OpenTechiz_OrdersEditExtend_Block_Adminhtml_Sales_Order_Edit_Form_SpendPoints extends Mage_Adminhtml_Block_Widget//Mage_Adminhtml_Block_Sales_Order_Create_Abstract
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_order_spendpoints');
        $this->setTemplate('opentechiz/ordersedit/edit/spendpoints.phtml');
    }

    protected function _construct()
    {
        parent::_construct();
        $layout = Mage::app()->getLayout();
        $rewards_redemption = $layout->createBlock("rewards/checkout_cart", "rewards_redemption")
                ->setTemplate("opentechiz/ordersedit/checkout/redemption.phtml", "rewards_js_points_captions");
        $rewards_js_points_captions = $layout->createBlock("core/template")->setTemplate("rewards/js/captions.phtml");
        $rewards_redemption->setChild("rewards_cartpoints_spender_js", $rewards_js_points_captions);
        $rewards_points_cart_minibox = $layout->createBlock("rewards/checkout_cart", "rewards_points_cart_minibox")->setTemplate("rewards/checkout/cartmini.phtml");
        $rewards_redemption->setChild("rewards_cartpoints_spender", $rewards_points_cart_minibox);
        $points_slider = $layout->createBlock("rewards/checkout_cart_slider", "points_cart_box.slider")->setTemplate("rewards/checkout/cart/slider.phtml");
        $rewards_points_cart_minibox->setChild("points_slider", $points_slider);
        $minicart_js = $layout->createBlock("core/template", "points_cart_box.js")->setTemplate("rewards/checkout/minicart_js.phtml");
        $rewards_points_cart_minibox->setChild("minicart_js", $minicart_js);
        $nondbps_js = $layout->createBlock("core/template", "points_cart_box_nondpbs.js")->setTemplate("rewards/checkout/nondbps_js.phtml");
        $rewards_points_cart_minibox->setChild("nondbps_js", $nondbps_js);
        $this->setChild("rewards_redemption", $rewards_redemption);
    }

    public function getSpendPoints()
    {
        if ($this->getParentBlock()) {
            /** @var Mage_Sales_Model_Order $order */
            $order = $this->getParentBlock()->getOrder();
        } elseif ($orderId = $this->getRequest()->getParam('order_id')) {
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->load($orderId);
        } else {
            return null;
        }

        return $order->getTotalSpentPointsAsString();
    }

}
