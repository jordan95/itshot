<?php

class OpenTechiz_OrdersEditExtend_Block_Adminhtml_Sales_Order_Totals extends MageWorx_OrdersEdit_Block_Adminhtml_Sales_Order_Totals
{

    protected function _getTotalRenderer($code)
    {
        $blockName = $code . '_total_renderer';
        $block = $this->getLayout()->getBlock($blockName);
        if (!$block) {
            $block = $this->_defaultRenderer;
            $config = Mage::getConfig()->getNode("global/sales/quote/totals/{$code}/admin_renderer");
            if ($config) {
                $block = (string) $config;
            }

            $block = $this->getLayout()->createBlock($block, $blockName);
        }
        /**
         * Transfer totals to renderer
         */
        $block->setTotals($this->getTotals());
        return $block;
    }

    public function renderTotal($total, $area = null, $colspan = 1)
    {
        return $this->_getTotalRenderer($total->getCode())
                        ->setTotal($total)
                        ->setColspan($colspan)
                        ->setRenderingArea(is_null($area) ? -1 : $area)
                        ->toHtml();
    }

    public function renderTotals($area = null, $colspan = 1)
    {
        $html = '';
        foreach ($this->getTotals() as $total) {
            if ($total->getArea() != $area && $area != -1) {
                continue;
            }
            $html .= $this->renderTotal($total, $area, $colspan);
        }
        return $html;
    }
    
     public function getTotals()
    {
        $totals = $this->getData('totals');

        //for shipping incl. tax on "New Totals" block
        if ((Mage::helper('tax')->displayShippingPriceIncludingTax() || Mage::helper('tax')->displayShippingBothPrices()) &&
            isset($totals['shipping'])) {
            $totals['shipping']->setValue($this->getSource()->getShippingAddress()->getShippingInclTax());
        }

        $order = $this->getOrder();
        $rate = $order->getBaseToOrderRate();

        foreach ($totals as $code => $total) {
//            $base = $total->getValue() / $rate;
//            if($code == 'remaining') {
//                $base = $base - $order->getPaidAmount();
//                $total->setData('value', $base);
//            }
            if($total->getTitle() == 'Down Payment'){
                $total->setTitle('Paid Amount');
                //unset($totals[$code]);
            }
        }
        foreach ($totals as $code => $total) {
            $base = $total->getValue() / $rate;

            $total->setData('base_value', $base);
        }

        return $totals;
    }

}
