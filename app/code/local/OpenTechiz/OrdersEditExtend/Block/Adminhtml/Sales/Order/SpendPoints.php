<?php

class OpenTechiz_OrdersEditExtend_Block_Adminhtml_Sales_Order_SpendPoints extends Mage_Adminhtml_Block_Widget//Mage_Adminhtml_Block_Sales_Order_Create_Abstract
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_order_spendpoints');
        $this->setTemplate('opentechiz/ordersedit/spendpoints/form.phtml');
    }

    public function getSpendPoints()
    {
        if ($this->getParentBlock()) {
            /** @var Mage_Sales_Model_Order $order */
            $order = $this->getParentBlock()->getOrder();
        } elseif ($orderId = $this->getRequest()->getParam('order_id')) {
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->load($orderId);
        } else {
            return null;
        }

        return $order->getTotalSpentPointsAsString();
    }

}
