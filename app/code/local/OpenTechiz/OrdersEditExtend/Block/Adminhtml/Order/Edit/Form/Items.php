<?php
class OpenTechiz_OrdersEditExtend_Block_Adminhtml_Order_Edit_Form_Items extends MageWorx_OrdersEdit_Block_Adminhtml_Sales_Order_Edit_Form_Items
{
    /**
     * Preapre layout to show "edit order items" form
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        Mage_Adminhtml_Block_Sales_Order_Create_Items::_prepareLayout();

        $grid = $this->getLayout()->createBlock('mageworx_ordersedit/adminhtml_sales_order_edit_form_items_itemsgrid')->setTemplate('opentechiz/ordersedit/edit/items/grid.phtml');
        $this->append($grid);

        return $this;
    }
}