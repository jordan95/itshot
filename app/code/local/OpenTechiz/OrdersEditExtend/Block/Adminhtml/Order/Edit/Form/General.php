<?php
/**
 * MageWorx
 * Admin Order Editor extension
 *
 * @category   MageWorx
 * @package    MageWorx_OrdersEdit
 * @copyright  Copyright (c) 2016 MageWorx (http://www.mageworx.com/)
 */

class OpenTechiz_OrdersEditExtend_Block_Adminhtml_Order_Edit_Form_General extends MageWorx_OrdersEdit_Block_Adminhtml_Sales_Order_Edit_Form_General
{
    /**
     * Preapre form to edit general info of order
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();

        $form = $this->getForm();
        $form->getElements()->remove('increment_id');
        $this->setForm($form);

        return $this;
    }
}