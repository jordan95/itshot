<?php

class OpenTechiz_OrdersEditExtend_Block_Adminhtml_Order_Item_Column_Name extends Mage_Adminhtml_Block_Sales_Items_Column_Name
{
    public function getTemplate(){
        return 'opentechiz/ordersedit/sales-items-column-name.phtml';
    }

    public function getImageByColor($product, $sku){
        return Mage::helper('opentechiz_salesextend')->getImageByColor($product, $sku);
    }
}
