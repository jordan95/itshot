<?php

class OpenTechiz_OrdersEditExtend_Model_Observer extends MageWorx_OrdersEdit_Model_Observer
{

    public function preventApprovalEarningPoints(Varien_Event_Observer $observer)
    {
        $transfer = $observer->getRewardsTransfer();
        if ($transfer->getReasonId() != Mage::helper('rewards/transfer_reason')->getReasonId('order')) {
            return;
        }
        $request = Mage::app()->getRequest();
        $module_controller = $request->getControllerName();
        $module_controller_action = $request->getActionName();
        if($module_controller != "manage_transfer"  || $module_controller_action != "save") {
            return;
        } 
        $order = Mage::getModel("sales/order")->load($transfer->getReferenceId());
        if (!$order->getId()) {
            return;
        }
        if ($order->getState() != Mage_Sales_Model_Order::STATE_COMPLETE) {
            if ($transfer->getOrigData('status_id') != $transfer->getData('status_id') && $transfer->getData('status_id') == TBT_Rewards_Model_Transfer_Status::STATUS_APPROVED) {
                Mage::getSingleton('adminhtml/session')->addError("You can not approval the points if the order not completed yet");
                header('Location: ' . Mage::helper('rewards/transfer_reason')->getAdminReferenceUrl($transfer->getReasonId(), $transfer->getReferenceId()));
                exit();
            }
        } else {
            if ($transfer->getOrigData('status_id') != $transfer->getData('status_id') && $transfer->getData('status_id') == TBT_Rewards_Model_Transfer_Status::STATUS_CANCELLED) {
                Mage::getSingleton('adminhtml/session')->addError("You can not cancelled the points if the order was completed");
                header('Location: ' . Mage::helper('rewards/transfer_reason')->getAdminReferenceUrl($transfer->getReasonId(), $transfer->getReferenceId()));
                exit();
            }
        }
    }

    public function insertSpendPointsBlock($observer)
    {
        if (!Mage::helper('rewards/config')->allowCatalogRulesInAdminOrderCreate()) {
            return;
        }
        /** @var Varien_Object $transport */
        $transport = $observer->getTransport();
        /** @var Mage_Core_Block_Abstract $block */
        $block = $observer->getBlock();
        if ($block->getType() == 'adminhtml/sales_order_view_items' && $block->getNameInLayout() == 'order_items') {
            /** @var string $oldHtml */
            $oldHtml = $transport->getHtml();

            /** @var string $spendpointsBlockHtml */
            $spendpointsBlockHtml = Mage::getSingleton('core/layout')
                    ->createBlock('orderseditextend/adminhtml_sales_order_spendPoints', 'spendpoints')
                    ->toHtml();

            /** @var string $newHtml */
            $newHtml = $oldHtml . $spendpointsBlockHtml; // append spend points block html
            $transport->setHtml($newHtml);
        }

        return;
    }

    public function resetSessionEditChanges($observer)
    {
        $request = $observer->getEvent()->getControllerAction()->getRequest()->getParams();
        if (isset($request['order_id'])) {
            $orderId = $request['order_id'];
            Mage::helper('mageworx_ordersedit/edit')->resetPendingChanges($orderId);
            $order = Mage::getModel('sales/order')->load($orderId);
            if (!$order->getId()) {
                return Mage::app()->getResponse()
                                ->setRedirect(Mage::helper('adminhtml')->getUrl('adminhtml/sales_order'))
                                ->sendResponse();
            }
            Mage::helper('mageworx_ordersedit/edit')->removeTempQuoteItems($order);

            // refresh order totals
            Mage::helper('partialpaymentextended')->refreshOrderTotals($order);
            $this->_getRewardsSession()->unsEditQuoteId();
        }

        return;
    }

    protected function _getRewardsSession()
    {
        return Mage::getSingleton('rewards/session');
    }

    public function removeRequestProcessOnOrderItemDelete($observer){
        $orderItem = $observer->getEvent()->getItem();
        $processRequestCollection = Mage::getModel('opentechiz_production/process_request')->getCollection()
            ->addFieldToFilter('order_item_id', $orderItem->getId());
        foreach ($processRequestCollection as $processRequest){
            $processRequest->delete();
        }
    }

    public function checkOrderStage($observer){
        $originalOrder = $observer->getEvent()->getOrigOrder();
        $changes = $observer->getEvent()->getChanges();

        //prevent edit order after fulfillment
        //prevent move to billing and edit order at the same time
        if(array_key_exists('quote_items', $changes) && count($changes['quote_items']) > 0
            && $originalOrder->getOrderStage() > 2){
            Mage::getSingleton('core/session')->addError(Mage::helper('orderseditextend')->__('Order is already at Billing Stage and cannot be edited'));
            Mage::app()->getResponse()->setRedirect(Mage::getUrl('adminhtml/sales_order/view', array('order_id' => $originalOrder->getId())))->sendResponse();
            exit;
        }
    }

    /**
     * Save mageworx order status history
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesOrderStatusHistoryAfterSave($observer)
    {
        /** @var Mage_Sales_Model_Order_Status_History $history */
        $history = $observer->getEvent()->getStatusHistory();
        if (Mage::helper('orderseditextend')->isSystemComment($history->getComment())) {
            return;
        }
        Mage::getModel('mageworx_ordersedit/order_status_history')->updateHistory($history);
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function applyDataToQuote(Varien_Event_Observer $observer)
    {
        $newData = $observer->getEvent()->getData('new_data');
        $order = Mage::registry('ordersedit_order');
        if (!$order) {
            return;
        }

        if (array_key_exists('shipping_hold_at', $newData)) {
            $order->setData('shipping_hold_at', $newData['shipping_hold_at'] ? 1 : 0);
        }
    }
}
