<?php

class OpenTechiz_OrdersEditExtend_Model_Total_Quote_Discount extends Mage_SalesRule_Model_Quote_Discount
{

    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);

        $quote = $address->getQuote();

        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }

        $order = Mage::registry('ordersedit_order');
        if (!$order) {
            return;
        }
        $pendingChanges = $this->getMwEditHelper()->getPendingChanges($order->getId());
        $orderCurrencyCode = $order->getOrderCurrencyCode();
        $adjusmentCounpon = true;
        $op_coupon_code = "";
        $baseDiscountAmount = 0;
        $discountAmount = 0;

        if (isset($pendingChanges['coupon_code']) || isset($pendingChanges['op_coupon_amount'])) {
            if (isset($pendingChanges['coupon_code']) && $pendingChanges['coupon_code'] == "") {
                $adjusmentCounpon = false;
            } else {
                if (!isset($pendingChanges['op_coupon_action']) || !isset($pendingChanges['op_coupon_amount']) || (isset($pendingChanges['off_coupon_code']) && $pendingChanges['off_coupon_code'] == 0)) {
                    $adjusmentCounpon = false;
                } else {
                    $op_coupon_action = $pendingChanges['op_coupon_action'];
                    $op_coupon_amount = $pendingChanges['op_coupon_amount'];
                    $op_coupon_code = $pendingChanges['op_coupon_code'];
                }
            }
        } else {
            $orderCouponCode = $order->getCouponCode();
            preg_match("/Adjustment Administrator \(type: (by_percent|cart_fixed) - amount: (\d+(\.\d+)?)\)/", $orderCouponCode, $matches);
            if (empty($matches)) {
                $adjusmentCounpon = false;
            } else {
                $op_coupon_action = $matches[1];
                $op_coupon_amount = $matches[2];
                $op_coupon_code = $orderCouponCode;
            }
        }

        if ($adjusmentCounpon) {
            if ($op_coupon_action == "cart_fixed") {
                $baseDiscountAmount = floatval($op_coupon_amount);
                $discountAmount = Mage::helper('directory')->currencyConvert($baseDiscountAmount, "USD", $orderCurrencyCode);
            } else if ($op_coupon_action == "by_percent") {
                $baseDiscountAmount = floatval($op_coupon_amount) * $address->getBaseSubtotal() / 100;
                $discountAmount = Mage::helper('directory')->currencyConvert($baseDiscountAmount, "USD", $orderCurrencyCode);
            }
        }
        
        if ($discountAmount <= 0) {
            return;
        }

        $total = (float)$address->getBaseSubtotal();
        foreach ($items as $item) {
            if ($item->getNoDiscount()) {
                $item->setDiscountAmount(0);
                $item->setBaseDiscountAmount(0);
            }else {
                if ($item->getParentItemId()) {
                    continue;
                }
                $rat = $item->getPrice() / $total;
                $ratdisc = $discountAmount * $rat;
                $baseRatdisc = $baseDiscountAmount * $rat;
                $item->setDiscountAmount(($item->getDiscountAmount() + $ratdisc) * $item->getQty());
                $item->setBaseDiscountAmount(($item->getBaseDiscountAmount() + $baseRatdisc) * $item->getQty());
            }
        }
        $this->_addAmount(-$discountAmount)
                   ->_addBaseAmount(-$baseDiscountAmount);
        $descriptionArr = [$op_coupon_code];
        
        $address->setDiscountDescriptionArray($descriptionArr);

        Mage::getSingleton('adminhtml/session')->setCouponMessage(Mage::helper('checkout/cart')
                        ->__('New discount was applied.'));
        if ($op_coupon_code) {
            $quote->setCouponCode($op_coupon_code);
        }
        $this->_calculator->prepareDescription($address);
        $this->_calculator->reset($address);
        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $quote = $address->getQuote();
        $amount = $address->getDiscountAmount();

        if ($amount != 0) {
            $order = Mage::registry('ordersedit_order');
            $description = $address->getDiscountDescription();
            if ($order && !empty($order->getCouponCode()) && empty($description)) {
                $description = $order->getCouponCode();
            }

            if (strlen($description)) {
                $title = Mage::helper('sales')->__('Discount (%s)', $description);
            } else {
                $title = Mage::helper('sales')->__('Discount');
            }
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => $title,
                'value' => $amount
            ));
        }
        return $this;
    }

    protected function getMwEditHelper()
    {
        return Mage::helper('mageworx_ordersedit/edit');
    }

}
