<?php

class OpenTechiz_OrdersEditExtend_Model_Observer_Order extends MageWorx_OrdersSurcharge_Model_Observer_Order{
    
    public function checkRefund($observer)
    {
        $helper = $this->getHelper();
        if ($helper->isDisabled()) {
            return $this;
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        $surchargeId = $order->getSurchargeId();
        if (!$surchargeId) {
            return $this;
        }

        if ($order->getBaseTotalPaid() &&
            $order->getBaseTotalRefunded() &&
            $order->getMwoBaseSurchargeAmount() &&
            $order->getBaseTotalPaid() == $order->getBaseTotalRefunded() &&
            $order->getMwoBaseSurchargeAmount() == $order->getBaseTotalRefunded()
        ) {
            $surcharge = Mage::getModel('mageworx_orderssurcharge/surcharge')->load($surchargeId);
            $surcharge->refund($order);
        }

        return $this;
    }
}
