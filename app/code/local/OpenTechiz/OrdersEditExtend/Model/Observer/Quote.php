<?php

class OpenTechiz_OrdersEditExtend_Model_Observer_Quote extends MageWorx_OrdersSurcharge_Model_Observer_Quote{

    public function activatePaymentMethod($observer)
    {
        $helper = $this->getHelper();
        if ($helper->isDisabled()) {
            return $this;
        }

        /** Unused params
        $result = $observer->getResult();
        $method = $observer->getMethodInstance();
         */
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $observer->getQuote();
        if (!$quote) {
            return $this;
        }

        $billing = $quote->getBillingAddress();
        if ($billing->getMwoBaseSurchargeAmount() >= 0.0001 && $quote->getBaseSubtotal() == 0) {
            $quote->setBaseSubtotal(0.0001);
        }

        return $this;
    }
}
