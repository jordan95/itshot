<?php

class OpenTechiz_OrdersEditExtend_Model_Rewrite_Observer_Order_Edit
{

    /**
     * Add "create/remove surcharge" button to temp totals block
     *
     * @param $observer
     * @return $this
     */
    public function addSurchargeButton($observer)
    {
        $helper = Mage::helper('mageworx_orderssurcharge');
        if ($helper->isDisabled()) {
            return $this;
        }

        /** @var MageWorx_OrdersEdit_Block_Adminhtml_Sales_Order_Totals $block */
        $block = $observer->getBlock();
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $block->getSource();
        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::registry('ordersedit_order');

        if (!$order) {
            return $this;
        }

        if ($order->getStatus() == 'pending') {
            return $this;
        }

        // Do not show the button when a order linked amount eq. quote linked amount
        if ($quote->getBaseGrandTotal() <= $order->getBaseGrandTotal() && $quote->getBaseLinkedAmount() == $order->getBaseLinkedAmount()) {
            return $this;
        }

        $isPartialPayment = false;
        if (Mage::getModel('partialpayment/partialpayment')->load($order->getId(), 'order_id')->getId()) {
            $isPartialPayment = true;
        }

        if (!$isPartialPayment) {
            $applyWithSurchargeButton = $block->getButtonHtml(
                    $helper->__('Apply & Send Payment Link'), 'orderEdit.saveOrderWithSurcharge();', 'mw-totals-button mw_floater-right mw_br'
            );
            //$block->addButton('apply_surcharge_and_save', $applyWithSurchargeButton);
            //issue #1451
        }

        $baseLinkedAmountOrder = $order->getBaseLinkedAmount();
        $baseLinkedAmountNew = $quote->getBaseGrandTotal() - ($order->getBaseTotalDue() + $baseLinkedAmountOrder + $order->getBaseGrandTotal());

        if ($baseLinkedAmountNew > 0) {
            $baseAmount = $baseLinkedAmountOrder + $baseLinkedAmountNew;
            $amount = $order->getStore()->convertPrice($baseAmount);
            $surchargeAmount = $order->getStore()->formatPrice($amount);
            $message = $helper->__('Need to be paid');
            $afterTotalsHtml = '<tr class="possible-linked"><td class="label">' . $message . '</td><td>' . $surchargeAmount . '</td></tr>';
            $block->addAfterTotalsHtml($afterTotalsHtml);
        }

        return $this;
    }

    public function recollectShipRate($observer)
    {
        $store = Mage::app()->getStore();
        $order = $observer->getEvent()->getOrder();
        $quoteId = $order->getQuoteId();
        $quote = Mage::getModel('sales/quote')->setStore($store)->load($quoteId);

        $address = $quote->getShippingAddress();

        $address->setCollectShippingRates(true);
        $this->_rates = $address->collectShippingRates()->getGroupedAllShippingRates();
        $address->save();
    }

    public function unsetCouponCode($observer)
    {
        /* @var $request Mage_Core_Controller_Request_Http */
        $request = $observer->getControllerAction()->getRequest();
        if (!$request->isAjax() || !$request->isPost() || !$request->getPost("op_coupon_action", false) || !$request->getPost("op_coupon_amount", false)) {
            return;
        }
        if ($request->getPost("off_coupon_code", false)) {
            $coupon_code = sprintf("Adjustment Administrator (type: %s - amount: %s)", $request->getPost("op_coupon_action"), $request->getPost("op_coupon_amount"));
            $request->setPost("op_coupon_code", $coupon_code);
            unset($_POST['coupon_code']);
        }
    }

    public function setDiscount($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $order = Mage::registry('ordersedit_order');
        if (!$order) {
            return;
        }
        $pendingChanges = $this->getMwEditHelper()->getPendingChanges($order->getId());
        $orderCurrencyCode = $order->getOrderCurrencyCode();
        $adjusmentCounpon = true;
        $op_coupon_code = "";
        $baseDiscountAmount = 0;
        $discountAmount = 0;

        if (isset($pendingChanges['coupon_code']) || isset($pendingChanges['op_coupon_amount'])) {
            if (isset($pendingChanges['coupon_code']) && $pendingChanges['coupon_code'] == "") {
                $adjusmentCounpon = false;
            } else {
                if (!isset($pendingChanges['op_coupon_action']) || !isset($pendingChanges['op_coupon_amount']) || (isset($pendingChanges['off_coupon_code']) && $pendingChanges['off_coupon_code'] == 0)) {
                    $adjusmentCounpon = false;
                } else {
                    $op_coupon_action = $pendingChanges['op_coupon_action'];
                    $op_coupon_amount = $pendingChanges['op_coupon_amount'];
                    $op_coupon_code = $pendingChanges['op_coupon_code'];
                }
            }
        } else {
            $orderCouponCode = $order->getCouponCode();
            preg_match("/Adjustment Administrator \(type: (by_percent|cart_fixed) - amount: (\d+(\.\d+)?)\)/", $orderCouponCode, $matches);
            if (empty($matches)) {
                $adjusmentCounpon = false;
            } else {
                $op_coupon_action = $matches[1];
                $op_coupon_amount = $matches[2];
                $op_coupon_code = $orderCouponCode;
            }
        }

        if ($adjusmentCounpon) {
            if ($op_coupon_action == "cart_fixed") {
                $baseDiscountAmount = floatval($op_coupon_amount);
                $discountAmount = Mage::helper('directory')->currencyConvert($baseDiscountAmount, "USD", $orderCurrencyCode);
            } else if ($op_coupon_action == "by_percent") {
                $baseDiscountAmount = floatval($op_coupon_amount) * $quote->getBaseSubtotal() / 100;
                $discountAmount = Mage::helper('directory')->currencyConvert($baseDiscountAmount, "USD", $orderCurrencyCode);
            }
        }

        // add discount from rewards points
        if ($order->getRewardsCartDiscountMap()) {
            $rewardsCartDiscountMap = json_decode($order->getRewardsCartDiscountMap(), true);
            foreach ($rewardsCartDiscountMap as $key => $value) {
                $discountAmount += $value['discount_amount'];
                $baseDiscountAmount += $value['base_discount_amount'];
            }
            if ($op_coupon_code == "") {
                $op_coupon_code = "Redeem Points";
            }
        }

        if ($quote->getId() && $discountAmount > 0) {
            $total = $quote->getBaseSubtotal();
            $quote->setSubtotal(0);
            $quote->setBaseSubtotal(0);

            $quote->setSubtotalWithDiscount(0);
            $quote->setBaseSubtotalWithDiscount(0);

            $quote->setGrandTotal(0);
            $quote->setBaseGrandTotal(0);
            $canAddItems = $quote->isVirtual() ? ('billing') : ('shipping');
            foreach ($quote->getAllAddresses() as $address) {

                $address->setSubtotal(0);
                $address->setBaseSubtotal(0);

                $address->setGrandTotal(0);
                $address->setBaseGrandTotal(0);

                $address->collectTotals();

                $quote->setSubtotal((float) $quote->getSubtotal() + $address->getSubtotal());
                $quote->setBaseSubtotal((float) $quote->getBaseSubtotal() + $address->getBaseSubtotal());

                $quote->setSubtotalWithDiscount(
                        (float) $quote->getSubtotalWithDiscount() + $address->getSubtotalWithDiscount()
                );
                $quote->setBaseSubtotalWithDiscount(
                        (float) $quote->getBaseSubtotalWithDiscount() + $address->getBaseSubtotalWithDiscount()
                );

                $quote->setGrandTotal((float) $quote->getGrandTotal() + $address->getGrandTotal());
                $quote->setBaseGrandTotal((float) $quote->getBaseGrandTotal() + $address->getBaseGrandTotal());

                $quote->save();

                $quote->setGrandTotal($quote->getSubtotal() - $discountAmount)
                        ->setBaseGrandTotal($quote->getBaseSubtotal() - $baseDiscountAmount)
                        ->setSubtotalWithDiscount($quote->getSubtotal() + $discountAmount)
                        ->setBaseSubtotalWithDiscount($quote->getBaseSubtotal() + $baseDiscountAmount)
                        ->save();


                if ($address->getAddressType() == $canAddItems) {
                    //echo $address->setDiscountAmount; exit;
                    $address->setSubtotalWithDiscount((float) $address->getSubtotalWithDiscount() - $discountAmount);
                    $address->setGrandTotal((float) $address->getGrandTotal() - $discountAmount);
                    $address->setBaseSubtotalWithDiscount((float) $address->getBaseSubtotalWithDiscount() - $baseDiscountAmount);
                    $address->setBaseGrandTotal((float) $address->getBaseGrandTotal() - $baseDiscountAmount);
                    if ($address->getDiscountDescription()) {
                        $address->setDiscountAmount(-($address->getDiscountAmount() - $discountAmount));
                        $address->setDiscountDescription($address->getDiscountDescription() . ', ' . $op_coupon_code);
                        $address->setBaseDiscountAmount(-($address->getBaseDiscountAmount() - $baseDiscountAmount));
                    } else {
                        $address->setDiscountAmount(-($discountAmount));
                        $address->setDiscountDescription($op_coupon_code);
                        $address->setBaseDiscountAmount(-($baseDiscountAmount));
                    }
                    $address->save();
                }//end: if
            } //end: foreach
            foreach ($quote->getAllItems() as $item) {
                //We apply discount amount based on the ratio between the GrandTotal and the RowTotal
                $rat = $item->getPrice() / $total;
                $ratdisc = $discountAmount * $rat;
                $baseRatdisc = $baseDiscountAmount * $rat;
                $item->setDiscountAmount(($item->getDiscountAmount() + $ratdisc) * $item->getQty());
                $item->setBaseDiscountAmount(($item->getBaseDiscountAmount() + $baseRatdisc) * $item->getQty())
                        ->save();
            }
            Mage::getSingleton('adminhtml/session')->setCouponMessage(Mage::helper('checkout/cart')
                            ->__('New discount was applied.'));
            if ($op_coupon_code) {
                $quote->setCouponCode($op_coupon_code);
            }
        }
    }

    public function setDiscountCustom($observer)
    {
        $order = $observer->getOrder();
        $pendingChanges = $this->getMwEditHelper()->getPendingChanges($order->getId());
        if (!isset($pendingChanges['op_coupon_code']) || (isset($pendingChanges['off_coupon_code']) && $pendingChanges['off_coupon_code'] == 0)) {
            return;
        }
        if (Mage::registry('op_history_coupon_code') !== null) {
            return;
        }

        $order->addStatusHistoryComment(sprintf("Coupon Code has been changed from %s to %s ", $order->getCouponCode(), $pendingChanges['op_coupon_code']), false)
                ->setIsCustomerNotified(false);
        $order->setCouponCode($pendingChanges['op_coupon_code']);
        $order->save();
        Mage::register('op_history_coupon_code', true);
    }

    public function applyRewardPoints(Varien_Event_Observer $observer)
    {
        $order = $observer->getOrder();
        $totalPointsEarning = $this->_getRewardsSess()->getTotalPointsEarning();
        $totalPointsSpending = $this->_getRewardsSess()->getTotalPointsSpending();
        $defaultCurrencyId = Mage::helper('rewards/currency')->getDefaultCurrencyId();

        // Reward Points
        $this->_getAdjuster()
                ->setOrder($order);

        // Sepend points
        if ($this->_getRewardsSess()->hasRedemptions()) {
            if (!empty($totalPointsSpending[$defaultCurrencyId])) {
                $this->_getAdjuster()->setAdjustedSpent(floor($totalPointsSpending[$defaultCurrencyId]));
            } else {
                $this->_getAdjuster()->setAdjustedSpent(0);
            }
        } else {
            $this->_getAdjuster()->setAdjustedSpent(0);
        }

        // Earn points
        if ($this->_getRewardsSess()->hasDistributions()) {
            if (!empty($totalPointsEarning[$defaultCurrencyId])) {
                $this->_getAdjuster()->setAdjustedEarned(floor($totalPointsEarning[$defaultCurrencyId]));
            } else {
                $this->_getAdjuster()->setAdjustedEarned(0);
            }
        } else {
            $this->_getAdjuster()->setAdjustedEarned(0);
        }
        Mage::dispatchEvent('rewards_adjust_points_init_before', array('adjuster' => $this->_getAdjuster()));
        $this->_getAdjuster()->init();
        $this->_getAdjuster()
                ->setTransferComments(sprintf("[Edit Order #%s] - Adjustment Reward Points", $order->getIncrementId()))
                ->execute();
        $this->_getRewardsSess()
                ->clear();
    }

    /**
     * @return TBT_Rewards_Model_Sales_Order_Transfer_Adjuster
     */
    protected function _getAdjuster()
    {
        return Mage::getSingleton('rewards/sales_order_transfer_adjuster');
    }

    private function _getRewardsSess()
    {
        return Mage::getSingleton('rewards/session');
    }

    protected function getMwEditHelper()
    {
        return Mage::helper('mageworx_ordersedit/edit');
    }

}
