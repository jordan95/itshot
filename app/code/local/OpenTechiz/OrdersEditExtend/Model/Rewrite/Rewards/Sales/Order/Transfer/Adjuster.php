<?php

class OpenTechiz_OrdersEditExtend_Model_Rewrite_Rewards_Sales_Order_Transfer_Adjuster extends TBT_Rewards_Model_Sales_Order_Transfer_Adjuster
{

    protected function _adjustOrderPoints($type, $adjustedPointsSum, $customerId = null)
    {
        $transfers = $this->getTransfers($type);
        $orderId = $this->getOrder()->getId();

        if (!$customerId) {
            $customerId = $this->getOrder()->getCustomerId();
        }

        $currencyIds = Mage::getSingleton('rewards/currency')->getAvailCurrencyIds();
        $currencyId = $currencyIds[0];

        $comments = $this->getTransferComments();

        $hasPending = count($transfers[self::TRANSFER_STATUS_PENDING_APPROVAL]) > 0 ||
                count($transfers[self::TRANSFER_STATUS_PENDING_EVENT]) > 0 ||
                count($transfers[self::TRANSFER_STATUS_PENDING_TIME]) > 0;

        $approvedTransfers = $transfers[self::TRANSFER_STATUS_APPROVED];
        $pendingTransfers = array_merge($transfers[self::TRANSFER_STATUS_PENDING_APPROVAL], $transfers[self::TRANSFER_STATUS_PENDING_EVENT], $transfers[self::TRANSFER_STATUS_PENDING_TIME]);

        // we only have to revoke/cancel transfers if the new adjusted value is LESS than the original
        if ($adjustedPointsSum < $this->getOrigPoints($type)) {
            $remainingPoints = $adjustedPointsSum;
            $cancellationQueue = array();
            // we go through the approved transfers first so they're less likely to be revoked
            foreach ($approvedTransfers as $transfer) {
                if ($remainingPoints >= abs($transfer->getQuantity())) {
                    $remainingPoints -= abs($transfer->getQuantity());
                } else {
                    $cancellationQueue[] = $transfer;
                }
            }
            // we go through the pending transfers last because they're easier to cancel
            foreach ($pendingTransfers as $transfer) {
                if ($remainingPoints >= abs($transfer->getQuantity())) {
                    $remainingPoints -= abs($transfer->getQuantity());
                } else {
                    $cancellationQueue[] = $transfer;
                }
            }

            // cancel/revoke any transfers that didn't make the cut!
            foreach ($cancellationQueue as $transfer) {
                if ($transfer->getStatusId() == self::TRANSFER_STATUS_APPROVED) {
                    $transfer->revoke();
                } else {
                    $transfer->setStatusId(null, self::TRANSFER_STATUS_CANCELLED)
                            ->save();
                }
            }

            // no need for a new transfer to make up the difference, nice!
            if ($remainingPoints <= 0) {
                return $this;
            }

            if ($type == self::TRANSFER_TYPE_SPENDING) {
                $remainingPoints *= -1;
            }

            // only use the Approved status if ALL transfer were approved... otherwise use Pending
            $status = ($hasPending || Mage::registry("ordersedit_order")) ? self::TRANSFER_STATUS_PENDING_EVENT :
                    self::TRANSFER_STATUS_APPROVED;

            // time to create a new transfer to make up the difference
            $this->_makeAdjustmentTransfer($remainingPoints, $status, $customerId, $orderId, $comments);

            return $this;
        }

        // since the adjusted value is MORE than the original, all we have to do is create a new transfer
        if ($adjustedPointsSum > $this->getOrigPoints($type)) {
            $remainingPoints = $adjustedPointsSum - $this->getOrigPoints($type);
            if ($type == self::TRANSFER_TYPE_SPENDING) {
                // if it's a spending transfer we should double-check that the customer can afford it
                $customer = Mage::getModel('rewards/customer')->load($customerId);
                if (!$customer->canAfford($remainingPoints, $currencyId)) {
                    // TODO: log the error here
                    Mage::throwException(
                            $this->__("Failed to adjust points on this order.  The customer has %s but Points Spent was increased by %s.", Mage::getModel('rewards/points')->set($currencyId, $customer->getUsablePointsBalance($currencyId)), Mage::getModel('rewards/points')->set($currencyId, $remainingPoints)
                            )
                    );
                }
                $remainingPoints *= -1;
            }

            // only use the Approved status if ALL transfer were approved... otherwise use Pending
            $status = ($hasPending || Mage::registry("ordersedit_order")) ? self::TRANSFER_STATUS_PENDING_EVENT :
                    self::TRANSFER_STATUS_APPROVED;
            $this->_makeAdjustmentTransfer($remainingPoints, $status, $customerId, $orderId, $comments);

            return $this;
        }

        return $this;
    }

}
