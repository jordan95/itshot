<?php

if ('true' == (string) Mage::getConfig()->getNode('modules/TBT_Rewards/active')) {

    class OpenTechiz_OrdersEditExtend_Model_SalesRule_Validator_Pure extends TBT_Rewards_Model_Salesrule_Discount_Validator
    {
        
    }

} else {

    class OpenTechiz_OrdersEditExtend_Model_SalesRule_Validator_Pure extends Mage_SalesRule_Model_Validator
    {
        
    }

}

class OpenTechiz_OrdersEditExtend_Model_SalesRule_Validator extends OpenTechiz_OrdersEditExtend_Model_SalesRule_Validator_Pure
{

    public function init($websiteId, $customerGroupId, $couponCode)
    {
        $this->setWebsiteId($websiteId)
                ->setCustomerGroupId($customerGroupId)
                ->setCouponCode($couponCode);

        $key = $websiteId . '_' . $customerGroupId . '_' . $couponCode;
        if (!isset($this->_rules[$key])) {
            $collection = Mage::getResourceModel('salesrule/rule_collection')
                    ->setValidationFilter($websiteId, $customerGroupId, $couponCode);
            $select = $collection->getSelect();
            if (Mage::registry("ordersedit_order")) {
                $orderEdit = Mage::registry("ordersedit_order");
                $pendingChanges = Mage::helper('mageworx_ordersedit/edit')->getPendingChanges($orderEdit->getId());
                if (!isset($pendingChanges['coupon_code'])) {
                    $orderCouponCode = $orderEdit->getCouponCode();
                    preg_match("/Adjustment Administrator \(type: (by_percent|cart_fixed) - amount: (\d+(\.\d+)?)\)/", $orderCouponCode, $matches);
                    $adjusmentCounpon = true;
                    if (empty($matches)) {
                        $adjusmentCounpon = false;
                    }
                    if (!$adjusmentCounpon) {
                        if ($orderEdit->getAppliedRuleIds()) {
                            $select->orWhere("main_table.rule_id IN (?)", explode(",", $orderEdit->getAppliedRuleIds()));
                        }
                    }
                } else {
                    $coupon = Mage::getModel('salesrule/coupon')->loadByCode($pendingChanges['coupon_code']);
                    if ($coupon->getId()) {
                        $select->orWhere("main_table.rule_id = ?", $coupon->getRuleId());
                    }
                    if ($orderEdit->getAppliedRuleIds()) {
                        $appliedRuleIds = explode(",", $orderEdit->getAppliedRuleIds());
                        foreach ($appliedRuleIds as $ruleId) {
                            $rule = Mage::getModel('salesrule/rule')->load($ruleId);
                            if (!$rule->getId() || $rule->getCouponType() != 1) {
                                continue;
                            }
                            $select->orWhere("main_table.rule_id = ?", $ruleId);
                        }
                    }
                }
            }
            $this->_rules[$key] = $collection->load();
        }
        return $this;
    }

    protected function _canProcessRule($rule, $address)
    {
        if ($rule->hasIsValidForAddress($address) && !$address->isObjectNew()) {
            if (Mage::registry("ordersedit_order")) {
                return true;
            } else {
                return $rule->getIsValidForAddress($address);
            }
        }

        /**
         * check per coupon usage limit
         */
        if ($rule->getCouponType() != Mage_SalesRule_Model_Rule::COUPON_TYPE_NO_COUPON) {
            $couponCode = $address->getQuote()->getCouponCode();
            if (strlen($couponCode)) {
                $coupon = Mage::getModel('salesrule/coupon');
                $coupon->load($couponCode, 'code');
                if ($coupon->getId()) {
                    // check entire usage limit
                    if ($coupon->getUsageLimit() && $coupon->getTimesUsed() >= $coupon->getUsageLimit()) {
                        $rule->setIsValidForAddress($address, false);
                        return false;
                    }
                    // check per customer usage limit
                    $customerId = $address->getQuote()->getCustomerId();
                    if ($customerId && $coupon->getUsagePerCustomer()) {
                        $couponUsage = new Varien_Object();
                        Mage::getResourceModel('salesrule/coupon_usage')->loadByCustomerCoupon(
                                $couponUsage, $customerId, $coupon->getId()
                        );
                        if ($couponUsage->getCouponId() &&
                                $couponUsage->getTimesUsed() >= $coupon->getUsagePerCustomer()
                        ) {
                            $rule->setIsValidForAddress($address, false);
                            return false;
                        }
                    }
                }
            }
        }

        /**
         * check per rule usage limit
         */
        $ruleId = $rule->getId();
        if ($ruleId && $rule->getUsesPerCustomer()) {
            $customerId = $address->getQuote()->getCustomerId();
            $ruleCustomer = Mage::getModel('salesrule/rule_customer');
            $ruleCustomer->loadByCustomerRule($customerId, $ruleId);
            if ($ruleCustomer->getId()) {
                if ($ruleCustomer->getTimesUsed() >= $rule->getUsesPerCustomer()) {
                    $rule->setIsValidForAddress($address, false);
                    return false;
                }
            }
        }
        $rule->afterLoad();
        /**
         * quote does not meet rule's conditions
         */
        if (!$rule->validate($address)) {
            $rule->setIsValidForAddress($address, false);
            return false;
        }
        /**
         * passed all validations, remember to be valid
         */
        $rule->setIsValidForAddress($address, true);
        return true;
    }

}
