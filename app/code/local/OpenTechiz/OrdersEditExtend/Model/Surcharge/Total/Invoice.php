<?php

class OpenTechiz_OrdersEditExtend_Model_Surcharge_Total_Invoice extends MageWorx_OrdersSurcharge_Model_Surcharge_Total_Invoice{
    
    public function collect(Mage_Sales_Model_Order_Invoice $invoice) {

        if ($invoice->getMwoBaseSurchargeAmount() > 0) {
            $grandTotal = $invoice->getGrandTotal() + $invoice->getMwoSurchargeAmount();
            $invoice->setGrandTotal($grandTotal);
            $baseGrandTotal = $invoice->getBaseGrandTotal() + $invoice->getMwoBaseSurchargeAmount();
            $invoice->setBaseGrandTotal($baseGrandTotal);
        }

        return $this;
    }
}
