<?php

class OpenTechiz_OrdersEditExtend_Model_Surcharge_Total_Creditmemo extends MageWorx_OrdersSurcharge_Model_Surcharge_Total_Creditmemo{
    
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo) {
        if ($creditmemo->getMwoBaseSurchargeAmount()) {
            $grandTotal = $creditmemo->getGrandTotal() + $creditmemo->getMwoSurchargeAmount();
            $creditmemo->setGrandTotal($grandTotal);
            $baseGrandTotal = $creditmemo->getBaseGrandTotal() + $creditmemo->getMwoBaseSurchargeAmount();
            $creditmemo->setBaseGrandTotal($baseGrandTotal);
        }

        return $this;
    }
}
