<?php

class OpenTechiz_OrdersEditExtend_Model_Edit_Invoice extends MageWorx_OrdersEdit_Model_Edit_Invoice
{

    public function invoiceChanges(
    Mage_Sales_Model_Order $origOrder, Mage_Sales_Model_Order $newOrder, $capture = Mage_Sales_Model_Order_Invoice::NOT_CAPTURE, $dummy = false
    )
    {
        $helper = $this->getMwHelper();
        $autoInvoiceRefund = $helper->isAutoInvoice();
        if (!$autoInvoiceRefund && !$dummy) {
            // Do not process the order if the manual invoicing is enabled in the system configuration
            return $this;
        }

        /** @var Mage_Sales_Model_Order_Invoice $invoice */
        /* @var $invoice Mage_Sales_Model_Order_Invoice */
        $invoice = Mage::getModel('sales/service_order', $newOrder)->prepareInvoice();
        if (!$dummy) {
            foreach ($this->_availableTotals as $code) {
                $diff = $newOrder->getData($code) - $origOrder->getData($code);
                if (!$diff) {
                    continue;
                }

                $invoice->setData($code, $diff);
            }
        } else {
            foreach ($this->_availableTotals as $code) {
                $invoice->setData($code, 0);
            }
        }
        $items = $invoice->getAllItems();
        if (count($items) == 0 && $invoice->getGrandTotal() == 0) {
            return $this;
        }


        $invoice->setRequestedCaptureCase($capture);
        $invoice->register();

        $transaction = Mage::getModel('core/resource_transaction')
                ->addObject($invoice)
                ->addObject($invoice->getOrder());

        $transaction->save();

        $this->updateInvoicedItems($newOrder);

        return $this;
    }

}
