<?php

class OpenTechiz_OrdersEditExtend_Model_Edit_Creditmemo extends MageWorx_OrdersEdit_Model_Edit_Creditmemo
{

    public function refundChanges(Mage_Sales_Model_Order $origOrder, Mage_Sales_Model_Order $newOrder, $creditmemo = null)
    {
        if(!$creditmemo) {
            $cmData = array();
            $cmData['qtys'] = array(0 => 0);

            $creditmemo = Mage::getModel('sales/service_order', $newOrder)->prepareCreditmemo($cmData);
        }

        foreach ($this->_availableTotals as $code) {
            $diff = $origOrder->getData($code) - $newOrder->getData($code);
            if (!$diff) {
                continue;
            }

            $creditmemo->setData($code, $diff);
        }

        // Return refunded items to stock
        foreach ($creditmemo->getAllItems() as $creditmemoItem) {
            $creditmemoItem->setBackToStock(true);
        }
        $creditmemo->setState(Mage_Sales_Model_Order_Creditmemo::STATE_OPEN);
        $creditmemo->save();

        return $this;
    }

}
