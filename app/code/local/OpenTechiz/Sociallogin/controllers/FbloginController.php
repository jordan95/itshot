<?php
require_once 'Magestore/Sociallogin/controllers/FbloginController.php';

class OpenTechiz_Sociallogin_FbloginController extends Magestore_Sociallogin_FbloginController
{
    public function loginAction()
    {
        if (!Mage::helper('magenotification')->checkLicenseKeyFrontController($this)) {
            return;
        }
        
        $request = $this->getRequest();
        if ($redirect_url = $request->getQuery('redirect_url', false)) {
            $this->getSession()->setRedirectUrlAfterLogin(urldecode($redirect_url));
             return $this->_redirectUrl(Mage::getModel('sociallogin/fblogin')->getFbLoginUrl());
        }
        
        //initialize facebook sdk
        $fb = new Facebook\Facebook([
            'app_id' => Mage::helper('sociallogin')->getFbAppId(),
            'app_secret' => Mage::helper('sociallogin')->getFbAppSecret(),
            'default_graph_version' => 'v2.10',
        ]);

        $helper = $fb->getRedirectLoginHelper();
        if (isset($_GET['state'])) {
            $helper->getPersistentDataHandler()->set('state', $_GET['state']);
        }

        $permissions = ['email']; // optional
        try {
            if ($this->getSession()->getFacebookAccessToken()) {
                $accessToken = $this->getSession()->getFacebookAccessToken();
            } else {
                $accessToken = $helper->getAccessToken();
            }
        } catch (Facebook\Exceptions\facebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (isset($accessToken)) {
            if ($this->getSession()->getFacebookAccessToken()) {
                $fb->setDefaultAccessToken($this->getSession()->getFacebookAccessToken());
            } else {
                $this->getSession()->setFacebookAccessToken((string) $accessToken);
                // OAuth 2.0 client handler
                $oAuth2Client = $fb->getOAuth2Client();
                // Exchanges a short-lived access token for a long-lived one
                $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken((string) $accessToken);
                $this->getSession()->setFacebookAccessToken((string) $longLivedAccessToken);
                $fb->setDefaultAccessToken($this->getSession()->getFacebookAccessToken());
            }

            try {
                $profile_request = $fb->get('/me?fields=name,first_name,last_name,email');
                $profile = $profile_request->getGraphUser();

                $user = array();
                $user['email'] = $profile->getProperty('email');    //  To Get Facebook email
                $user['firstname'] = $profile->getProperty('first_name');   // To Get Facebook first name
                $user['lastname'] = $profile->getProperty('last_name'); // To Get Facebook last name

                $store_id = Mage::app()->getStore()->getStoreId(); //add
                $website_id = Mage::app()->getStore()->getWebsiteId(); //add

                $customer = Mage::helper('sociallogin')->getCustomerByEmail($user['email'], $website_id);
                //fix isssue #1073 when don't get an email 
                if(empty($user['email'])){
                    Mage::getSingleton('core/session')->addError('Please login facebook with your email address.');
                    return $this->_redirect("/");
                }
                if (!$customer || !$customer->getId()) {
                    //Login multisite
                    $customer = Mage::helper('sociallogin')->createCustomerMultiWebsite($user, $website_id, $store_id);
                    if (Mage::getStoreConfig(('sociallogin/general/send_newemail'), Mage::app()->getStore()->getId()))
                        $customer->sendNewAccountEmail('registered', '', Mage::app()->getStore()->getId());
                    if (Mage::getStoreConfig('sociallogin/fblogin/is_send_password_to_customer')) {
                        $customer->sendPasswordReminderEmail();
                    }
                }
                // fix confirmation
                if ($customer->getConfirmation()) {
                    try {
                        $customer->setConfirmation(null);
                        $customer->save();
                    } catch (Exception $e) {
                        
                    }
                }
                Mage::getSingleton('customer/session')->setCustomerAsLoggedIn($customer);
                $post_redirect = $this->_loginPostRedirect();
                die("<script type=\"text/javascript\">if(navigator.userAgent.match('CriOS')){window.location.href=\"" . $post_redirect . "\";}else{try{window.opener.location.href=\"" . $post_redirect . "\";}catch(e){window.opener.location.reload(true);} window.close();}</script>");
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
        } else {
            $loginUrl = $helper->getLoginUrl(Mage::helper('sociallogin')->getAuthUrl(), $permissions);
            return $this->_redirectUrl($loginUrl);
        }
    }


}
