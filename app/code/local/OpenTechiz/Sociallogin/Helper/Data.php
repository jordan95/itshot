<?php
class OpenTechiz_Sociallogin_Helper_Data extends Magestore_Sociallogin_Helper_Data{


	//create customer login multisite
	public function createCustomerMultiWebsite($data, $website_id, $store_id)
	{
		if(!isset($data['firstname'])){
			$data['firstname'] = '';
		}
		if(!isset($data['lastname'])){
			$data['lastname'] = '';
		}
		$customer = Mage::getModel('customer/customer')->setId(null);
		$customer ->setFirstname($data['firstname'])
					->setLastname($data['lastname'])
					->setEmail($data['email'])
					->setWebsiteId($website_id)
					->setStoreId($store_id)
					->save()
					;
		$newPassword = $customer->generatePassword();
		$customer->setPassword($newPassword);
		try{
			$customer->save();
		}catch(Exception $e){}
		return $customer;
	}
}