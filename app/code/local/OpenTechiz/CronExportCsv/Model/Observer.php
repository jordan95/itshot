<?php

/**
 * OnlineBiz Software Solution
 * 
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Jordan Tran<jordan@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2020, OnlineBiz Software Solution
 * 
 * Create at: April 7, 2020 2:27:27 PM
 */
class OpenTechiz_CronExportCsv_Model_Observer
{
    const FIELD_ORDER_TYPE_STANDARD                             = 0;
    const FIELD_ORDER_TYPE_LAYAWAY                              = 1;
    const FIELD_ORDER_TYPE_EXCHANGE                             = 3;
    const FIELD_ORDER_SOURCE_ITSHOT                             = 0;
    const FIELD_ORDER_SOURCE_PHONE                              = 1;
    const FIELD_ORDER_SOURCE_SHOWROOM                           = 3;
    const FIELD_ORDER_SOURCE_REPAIR                             = 8;
    const FIELD_ORDER_SOURCE_LAYAWAY                            = 12;
    const FIELD_ORDER_STAGE_ORDER_VERTIFICATION                 = 0;
    const FIELD_ORDER_STAGE_PREP_SHIPMENT                       = 4;
    const FIELD_ORDER_STAGE_SHIPPED                             = 5;
    const FIELD_ORDER_STAGE_NOT_APPLICATION                     = 6;
    const FIELD_ORDER_INTERNAL_STATUS_WAITING_CUSTOMER          = 14;
    const FIELD_ORDER_INTERNAL_STATUS_NEW                       = 0;
    const FIELD_ORDER_INTERNAL_STATUS_NOT_APPLICABLE            = 22;
    const FIELD_ORDER_TERM_WFR_BY_CUSTOMER                      = 5;
    const FILE_ITEMCOST_EXPORT                                  = 'ItemCost_new.csv';
    const FILE_MANUALORDER_EXPORT                               = 'ManualOrders_new.csv';
    const FILE_VOIDED_EXPORT                                    = 'VoidedOrders_new.csv';
    const FILE_APPROVEDORDERS_EXPORT                            = 'ApprovedOrders_new.csv';
    const FILE_MARKETPLACE_EXPORT                               = 'MarketplaceOrders_new.csv';
    public function Exportitemcostcsv()
    {       
        if($this->isAllowCron()){
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $file_export = $this->getFileNameExport(self::FILE_ITEMCOST_EXPORT);
            $path = Mage::getBaseDir('var') . DS . 'export' . DS . $file_export;
            $this->createFolderExport();
            $csv = new Varien_File_Csv();
            $csvData = array();
            $order_data = array();
            $csvdata[] =array('sku'=>'Sku','max_price'=>'Cost');
            $table_catalog_flat = "tsht_catalog_product_flat_1";
            $collection = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()
                            ->addFieldToSelect(new Zend_Db_Expr('MAX(main_table.last_cost) AS max_price'))
                            ->getSelect()->join( array('catalog_product'=> $table_catalog_flat), 
                                'catalog_product.entity_id = main_table.product_id', array('catalog_product.sku'))->group('catalog_product.sku');
            $data = $readConnection->fetchAll($collection);

            if(!empty($data)){
                foreach($data as $key => $value){
                    $order_data['sku'] = $value['sku'];
                    $order_data['max_price'] = $value['max_price'];
                    $csvdata[] = $order_data;
                }
                if(count($csvdata)>0){
                    $csv->saveData($path, $csvdata);
                    $this->transferFileToServer($path,$file_export);
                }
                
            }
        }
           
    }
    public function Exportmanualordercsv()
    {
        if($this->isAllowCron()){
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $file_export = $this->getFileNameExport(self::FILE_MANUALORDER_EXPORT);
            $path = Mage::getBaseDir('var') . DS . 'export' . DS . $file_export;
            $this->createFolderExport();
            $csv = new Varien_File_Csv();
            $csvData = array();
            $order_data = array();
            $csvdata[] =array('sku'=>'SKU','soldprice'=>'SoldPrice','ordernumber'=>'OrderNumber','orderdate'=>'OrderDate');

            $table_catalog_product = "tsht_catalog_product_entity";
            $table_sales_order_item = $resource->getTableName('sales/order_item');
            $condition = "order_type =".self::FIELD_ORDER_TYPE_LAYAWAY." AND source in (".self::FIELD_ORDER_SOURCE_PHONE.",".self::FIELD_ORDER_SOURCE_LAYAWAY.",".self::FIELD_ORDER_SOURCE_SHOWROOM.") AND main_table.created_at >='2019-01-01' AND state NOT IN('canceled', 'closed', 'holded')";
            $collection = Mage::getModel('sales/order')->getCollection()
                            ->addFieldToSelect('increment_id')
                            ->addFieldToSelect('created_at')
                            ->addFieldToFilter('order_type',array('eq'=>self::FIELD_ORDER_TYPE_STANDARD))
                            ->addFieldToFilter('source',array(array('eq'=>self::FIELD_ORDER_SOURCE_PHONE),array('eq'=>self::FIELD_ORDER_SOURCE_SHOWROOM)))
                            ->addFieldToFilter('order_stage',array('neq'=>self::FIELD_ORDER_STAGE_ORDER_VERTIFICATION))
                            ->addFieldToFilter('order_stage',array('neq'=>self::FIELD_ORDER_STAGE_NOT_APPLICATION))
                            ->addFieldToFilter('term',array('neq'=>self::FIELD_ORDER_TERM_WFR_BY_CUSTOMER))
                            ->addFieldToFilter('main_table.created_at',array('gteq'=>'2019-01-01'))
                            ->addFieldToFilter('state',array('nin'=>array("canceled","closed","holded")))
                             ->getSelect()->join( array('order_item'=> $table_sales_order_item), 
                                'order_item.order_id = main_table.entity_id', array('order_item.price'))->join( array('catalog_product'=> $table_catalog_product), 
                                'catalog_product.entity_id = order_item.product_id', array('catalog_product.sku'))->columns('DATE(main_table.created_at) as custom_created_at')
                            ->orWhere(new Zend_Db_Expr($condition))->order(array('custom_created_at DESC','main_table.increment_id DESC'));  

            $data = $readConnection->fetchAll($collection);
            if(!empty($data)){
                foreach($data as $key => $value){
                    $order_data['sku'] = $value['sku'];
                    $order_data['soldprice'] = $value['price'];
                    $order_data['ordernumber'] = $value['increment_id'];
                    $order_data['orderdate'] = $this->getFormatDate($value['created_at']);
                    $csvdata[] = $order_data;
                }
                if(count($csvdata)>0){
                    $csv->saveData($path, $csvdata);
                    $this->transferFileToServer($path,$file_export);
                }
            }
        }
    }
    public function Exportvoidedordercsv()
    {  
        if($this->isAllowCron()){
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $file_export = $this->getFileNameExport(self::FILE_VOIDED_EXPORT);
            $path = Mage::getBaseDir('var') . DS . 'export' . DS . $file_export;
            $this->createFolderExport();
            $csv = new Varien_File_Csv();
            $csvData = array();
            $order_data = array();
            $csvdata[] =array('sku'=>'SKU','soldprice'=>'SoldPrice','ordernumber'=>'OrderNumber','orderdate'=>'OrderDate');
            $table_sales_order_item = $resource->getTableName('sales/order_item');
            $table_catalog_product = "tsht_catalog_product_entity";
            $condition = "state IN('canceled', 'closed', 'holded') AND main_table.created_at >='2019-01-01'";
            $collection = Mage::getModel('sales/order')->getCollection()
                            ->addFieldToSelect('increment_id')
                            ->addFieldToSelect('created_at')
                            ->addFieldToFilter('order_stage',array(array('eq'=>self::FIELD_ORDER_STAGE_ORDER_VERTIFICATION),array('eq'=>self::FIELD_ORDER_STAGE_NOT_APPLICATION)))
                            ->addFieldToFilter('internal_status',array(array('eq'=>self::FIELD_ORDER_INTERNAL_STATUS_WAITING_CUSTOMER),array('eq'=>self::FIELD_ORDER_INTERNAL_STATUS_NOT_APPLICABLE)))
                            ->addFieldToFilter('main_table.created_at',array('gteq'=>'2019-01-01'))->getSelect()->join( array('order_item'=> $table_sales_order_item), 
                                'order_item.order_id = main_table.entity_id', array('order_item.price'))->join( array('catalog_product'=> $table_catalog_product), 
                                'catalog_product.entity_id = order_item.product_id', array('catalog_product.sku'))->columns('DATE(main_table.created_at) as custom_created_at')->order(array('custom_created_at DESC','main_table.increment_id DESC'))->orWhere(new Zend_Db_Expr($condition));
            $data = $readConnection->fetchAll($collection);
            if(!empty($data)){
                foreach($data as $key => $value){
                    $order_data['sku'] = $value['sku'];
                    $order_data['soldprice'] = $value['price'];
                    $order_data['ordernumber'] = $value['increment_id'];
                    $order_data['orderdate'] = $this->getFormatDate($value['created_at']);
                    $csvdata[] = $order_data;
                }
                if(count($csvdata)>0){
                    $csv->saveData($path, $csvdata);
                    $this->transferFileToServer($path,$file_export);
                }
            }
        }

    }
    public function Exportapprovedordercsv()
    {  
        if($this->isAllowCron()){
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $file_export = $this->getFileNameExport(self::FILE_APPROVEDORDERS_EXPORT);
            $path = Mage::getBaseDir('var') . DS . 'export' . DS . $file_export;
            $this->createFolderExport();
            $csv = new Varien_File_Csv();
            $csvData = array();
            $order_data = array();
            $csvdata[] =array('ordernumber'=>'OrderNumber','orderdate'=>'OrderDate','source'=>'OrderSource','total'=>'OrderTotal','verification'=>'Verification','term'=>'Terms');
            $table_sales_order_item = $resource->getTableName('sales/order_item');
            $collection = Mage::getModel('sales/order')->getCollection()
                            ->addFieldToSelect('increment_id')
                            ->addFieldToSelect('created_at')
                            ->addFieldToSelect('source')
                            ->addFieldToSelect('base_grand_total')
                            ->addFieldToSelect('verification')
                            ->addFieldToSelect('term')
                            ->addFieldToFilter('order_stage',array('neq'=>self::FIELD_ORDER_STAGE_ORDER_VERTIFICATION))
                            ->addFieldToFilter('order_stage',array('neq'=>self::FIELD_ORDER_STAGE_NOT_APPLICATION))
                            ->addFieldToFilter('term',array('neq'=>self::FIELD_ORDER_TERM_WFR_BY_CUSTOMER))
                            ->addFieldToFilter('order_type',array(array('eq'=>self::FIELD_ORDER_TYPE_STANDARD),array('eq'=>self::FIELD_ORDER_TYPE_LAYAWAY)))
                            ->addFieldToFilter('source',array(array('eq'=>self::FIELD_ORDER_SOURCE_ITSHOT),array('eq'=>self::FIELD_ORDER_SOURCE_PHONE),array('eq'=>self::FIELD_ORDER_SOURCE_SHOWROOM),array('eq'=>self::FIELD_ORDER_SOURCE_LAYAWAY)))
                            ->addFieldToFilter('state',array('nin'=>array("canceled","closed","holded")))
                            ->getSelect()->where('main_table.created_at >= DATE_SUB(now(), INTERVAL 12 MONTH)')->order(array('source ASC','created_at ASC')); 
            $data = $readConnection->fetchAll($collection);
            if(!empty($data)){
                foreach($data as $key => $value){
                    $order_data['ordernumber'] = $value['increment_id'];
                    $order_data['orderdate'] = $this->getFormatDate($value['created_at']);
                    if (array_key_exists( $value['source'],OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE)){
                        $order_data['source'] = OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE[$value['source']];
                    }
                    $order_data['total'] = $value['base_grand_total'];
                    
                    if (array_key_exists( $value['verification'],OpenTechiz_SalesExtend_Helper_Data::VERIFICATION)){
                        $order_data['verification'] = OpenTechiz_SalesExtend_Helper_Data::VERIFICATION[$value['verification']];
                    }
                    if (array_key_exists( $value['term'],OpenTechiz_SalesExtend_Helper_Data::TERM)){
                        $order_data['term'] = OpenTechiz_SalesExtend_Helper_Data::TERM[$value['term']];
                    }
                    $csvdata[] = $order_data;
                }
                if(count($csvdata)>0){
                    $csv->saveData($path, $csvdata);
                    $this->transferFileToServer($path,$file_export);
                }
            }
        }

    }
    public function Exportmarketplaceordercsv()
    {  
        if($this->isAllowCron()){
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $file_export = $this->getFileNameExport(self::FILE_MARKETPLACE_EXPORT);
            $path = Mage::getBaseDir('var') . DS . 'export' . DS . $file_export;
            $this->createFolderExport();
            $csv = new Varien_File_Csv();
            $csvData = array();
            $order_data = array();
            $csvdata[] =array('ordernumber'=>'OrderNumber','orderdate'=>'OrderDate','source'=>'OrderSource','total'=>'OrderTotal','verification'=>'Verification','term'=>'Terms');
            $table_sales_order_item = $resource->getTableName('sales/order_item');
            $collection = Mage::getModel('sales/order')->getCollection()
                            ->addFieldToSelect('increment_id')
                            ->addFieldToSelect('created_at')
                            ->addFieldToSelect('source')
                            ->addFieldToSelect('base_grand_total')
                            ->addFieldToSelect('verification')
                            ->addFieldToSelect('term')
                            ->addFieldToFilter('order_stage',array('neq'=>self::FIELD_ORDER_STAGE_ORDER_VERTIFICATION))
                            ->addFieldToFilter('order_stage',array('neq'=>self::FIELD_ORDER_STAGE_NOT_APPLICATION))
                            ->addFieldToFilter('term',array('neq'=>self::FIELD_ORDER_TERM_WFR_BY_CUSTOMER))
                            ->addFieldToFilter('order_type',array(array('eq'=>self::FIELD_ORDER_TYPE_STANDARD),array('eq'=>self::FIELD_ORDER_TYPE_LAYAWAY)))
                            ->addFieldToFilter('source',array('in'=>OpenTechiz_SalesExtend_Helper_Data::MARKETPLACE_ORDER_SOURCE))
                            ->addFieldToFilter('state',array('nin'=>array("canceled","closed","holded")))
                            ->getSelect()->where('main_table.created_at >= DATE_SUB(now(), INTERVAL 12 MONTH)')->order(array('source ASC','created_at ASC')); 
            $data = $readConnection->fetchAll($collection);
            if(!empty($data)){
                foreach($data as $key => $value){
                    $order_data['ordernumber'] = $value['increment_id'];
                    $order_data['orderdate'] = $this->getFormatDate($value['created_at']);
                    if (array_key_exists( $value['source'],OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE)){
                        $order_data['source'] = OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE[$value['source']];
                    }
                    $order_data['total'] = $value['base_grand_total'];
                    
                    if (array_key_exists( $value['verification'],OpenTechiz_SalesExtend_Helper_Data::VERIFICATION)){
                        $order_data['verification'] = OpenTechiz_SalesExtend_Helper_Data::VERIFICATION[$value['verification']];
                    }
                    if (array_key_exists( $value['term'],OpenTechiz_SalesExtend_Helper_Data::TERM)){
                        $order_data['term'] = OpenTechiz_SalesExtend_Helper_Data::TERM[$value['term']];
                    }
                    $csvdata[] = $order_data;
                }
                if(count($csvdata)>0){
                    $csv->saveData($path, $csvdata);
                    $this->transferFileToServer($path,$file_export);
                }
            }
        }

    }
    public function createFolderExport(){
        $folder_include_file_export = Mage::getBaseDir('var') . DS . 'export';
        if (!file_exists($folder_include_file_export)) {
            mkdir($folder_include_file_export, 0777, true);
        }
    }
    public function transferFileToServer($local_file,$file_export){
        $enable_transfer_ftp  = $this->getConfigData("enable");
            $host = $this->getConfigData("host_name");
            $port = $this->getConfigData("port");
            $user = $this->getConfigData("user_name");
            $pass = $this->getConfigData("password");
            $path = $this->getConfigData("path");
            $server_file = $file_export;
            if($enable_transfer_ftp){
                if(file_exists($local_file)){
                
                    $connection = ftp_connect($host,$port) or die("Could not connect to $host"); 
                    $isLoggedIn = false;
                    try {
                        $isLoggedIn = ftp_login($connection,$user,$pass);
                    } catch (Exception $e) {
                        $isLoggedIn = false;
                    }
                    if ($isLoggedIn) {
                        try {
                            ftp_pasv($connection, true);
                        } catch (Exception $e) {
                            Mage::throwException('Can\'t change FTP mode');
                        }

                        try {
                            ftp_chdir($connection, $path);
                        } catch (Exception $e) {
                            Mage::throwException('Can\'t change destination directory');
                        }
                        try {
                            ftp_put($connection, $server_file,$local_file , FTP_ASCII);
                            return true;
                        } catch (Exception $e) {
                            Mage::throwException('There was a problem while uploading file "'.$local_file.'"');
                        }
                        // close connection
                        ftp_close($connection);
                    }else {
                            Mage::throwException('Authenticate failure. Can\'t login to host "'.$host.'"');
                    }
                } 
            }
               
    }
    public function getFormatDate($created_at){ 
        $format_date = Mage::helper('core')->formatDate($created_at);
        return $format_date;
    }
    public function isAllowCron(){ 
       $enable_cron  = Mage::getStoreConfig('exportdata/general/enable');
       return $enable_cron;
    }
    public function getConfigData($name){ 
       $data  = Mage::getStoreConfig('exportdata/ftpsetting/'.$name);
       return $data;
    }
    public function getFileNameExport($filename){ 
       $enable_live_mode  = Mage::getStoreConfig('exportdata/general/enable_live_mode');
       if($enable_live_mode){
            return $filename;
       }else{
            return 'TEST_'.$filename;
       }
    }
}
