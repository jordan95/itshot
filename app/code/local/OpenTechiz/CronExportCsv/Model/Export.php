<?php

/**
 * Opentechiz Software Solution
 * 
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Jordan Tran<jordan@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2020, Opentechiz Software Solution
 * 
 * Create at: August 18, 2020 3:27:27 PM
 */
class OpenTechiz_CronExportCsv_Model_Export
{
    const FILE_CURRENT_STOCK_EXPORT   = 'Current_Stock.csv';
    const FILE_CURRENT_STOCK_EXPORT_FULL_OPTION = 'CurrentStockFullOptions.csv';
    const SUPPLLIER_LUCCELLO   = 90;
    public function Exportcurrentstockcsv()
    {   
        $file_export = self::FILE_CURRENT_STOCK_EXPORT;
        $path = Mage::getBaseDir('var') . DS . 'export' . DS . $file_export;
        $this->createFolderExport();
        $csv = new Varien_File_Csv();
        $product_data = $gold_color = $list_product_id = $list_product_id_luccello = array();
        $csvData = array();
        $csvdata[] =array('sku'=>'Sku','gold_color'=>'Gold Color','qty'=>'Quantity','supplied_item'=>'Luccello');
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');    
        $table_catalog_product = "tsht_catalog_product_entity";
        $table_purchase_product_supplier = "tsht_purchase_product_supplier";
        $collection_data = Mage::getModel('catalog/product')->getCollection()
                        ->getSelect()->join( array('product_option'=> "tsht_catalog_product_option"),'e.entity_id = product_option.product_id', array('product_option.option_id'))->join( array('product_option_title'=> "tsht_catalog_product_option_title"),'product_option.option_id = product_option_title.option_id', array('product_option_title.title'))->where("lower(title) LIKE lower('%Gold Color%')");
                $all_product_gold_color = $readConnection->fetchAll($collection_data);
                foreach($all_product_gold_color as $key => $value){
                    $list_product_id[] = $value['entity_id'];
                }
        $collection_product_supplier = Mage::getModel('opentechiz_purchase/productsupplier')
                                        ->getCollection()
                                        ->addFieldToSelect('product_id')
                                        ->addFieldToFilter('sup_id',array("eq"=>self::SUPPLLIER_LUCCELLO));
                foreach($collection_product_supplier as $k => $item){
                    $list_product_id_luccello[] = $item->getProductId();
                }
        $collection = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToSelect('sku')->addFieldToSelect('qty')->addFieldToFilter('qty',array("gt"=>0))
                        ->getSelect()
                        ->join( array('catalog_product'=> $table_catalog_product),'catalog_product.entity_id = main_table.product_id', array('catalog_product.sku as sku_product','catalog_product.entity_id')); 
        $data = $readConnection->fetchAll($collection);
        if(!empty($data)){
            foreach($data as $key => $value){
                if(empty($value['sku'])){
                    $product_data['sku'] = $value['sku'];
                }else{
                    $product_data['sku'] = $value['sku_product'];
                }

                $gold_color = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $value['sku']);
                $productId = $value['entity_id'];
                if(in_array($productId,$list_product_id)){
                    if(!isset($gold_color[1])){
                        continue;
                    }
                    $product_data['gold_color'] = $gold_color[1];
                }else{
                    $product_data['gold_color'] ='';
                }
                
                $product_data['qty'] = $value['qty'];
                if(in_array($productId,$list_product_id_luccello)){
                    $product_data['supplied_item'] =1;
                }else{
                    $product_data['supplied_item'] =0;
                }
                $csvdata[] = $product_data;
            }
            if(count($csvdata)>0){
                $csv->saveData($path, $csvdata);
            }
        }
    }
    public function Exportcurrentstockfullstockcsv()
    {   
        $file_export = self::FILE_CURRENT_STOCK_EXPORT_FULL_OPTION;
        $path = Mage::getBaseDir('var') . DS . 'export' . DS . $file_export;
        $this->createFolderExport();
        $csv = new Varien_File_Csv();
        $product_data = $gold_color = $list_product_id = $list_product_id_luccello =$data_qty_and_supplier = array();
        $csvData = $csvTemporData  =  array();
        $csvdata[] =array('sku'=>'Sku','qty'=>'Quantity','supplied_item'=>'Luccello');
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');    
        $table_catalog_product = "tsht_catalog_product_entity";
        $table_purchase_product_supplier = "tsht_purchase_product_supplier";
        $collection_data = Mage::getModel('catalog/product')->getCollection()
                        ->getSelect()->join( array('product_option'=> "tsht_catalog_product_option"),'e.entity_id = product_option.product_id', array('product_option.option_id'))->join( array('product_option_title'=> "tsht_catalog_product_option_title"),'product_option.option_id = product_option_title.option_id', array('product_option_title.title'))->where("lower(title) NOT LIKE lower('%Engraving%')");
                $all_product_gold_color = $readConnection->fetchAll($collection_data);
                foreach($all_product_gold_color as $key => $value){
                    $list_product_id[] = $value['entity_id'];
                }
        $collection_product_supplier = Mage::getModel('opentechiz_purchase/productsupplier')
                                        ->getCollection()
                                        ->addFieldToSelect('product_id')
                                        ->addFieldToFilter('sup_id',array("eq"=>self::SUPPLLIER_LUCCELLO));
                foreach($collection_product_supplier as $k => $item){
                    $list_product_id_luccello[] = $item->getProductId();
                }
       $collection = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldToSelect('sku')->addFieldToSelect('qty')->addFieldToFilter('qty',array("gt"=>0))
                        ->getSelect()
                        ->join( array('catalog_product'=> $table_catalog_product),'catalog_product.entity_id = main_table.product_id', array('catalog_product.sku as sku_product','catalog_product.entity_id'))->order('catalog_product.entity_id ASC'); 
        $data = $readConnection->fetchAll($collection);
        if(!empty($data)){
		    $i=0;
            foreach($data as $key => $value){
			
				$parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $value['sku']);
                if($value['sku'] =="loose_diamond_princess"){
                    continue;
                }
				if(count($parts) > 1){
					$product_data =  array_map(array($this, 'nullify'), $product_data);

					if(empty($value['sku'])){
						$product_data['sku'] = $value['sku'];
					}else{
						$product_data['sku'] = $value['sku_product'];
					}
					 $productId = $value['entity_id'];
					$product_data['qty'] = $value['qty'];
					if(in_array($productId,$list_product_id_luccello)){
						$product_data['supplied_item'] =1;
					}else{
						$product_data['supplied_item'] =0;
					}
					 $product = Mage::getModel('catalog/product')->load($productId);
					  $j = 1;
					foreach ($product->getOptions() as $option){
						 if(isset($parts[$j])) {
							if (strpos($option->getTitle(), 'Engraving') === false) {
								$key_option = preg_replace('/\s+/', '_', strtolower(trim($option->getTitle())));
							    if(!in_array(strtolower(trim($option->getTitle())),$csvdata)){
								   $csvdata[0][$key_option] = strtolower(trim($option->getTitle()));
								}
								 $product_data[$key_option] = $parts[$j] ;
									$j++;
							}
						 }
					}

					$csvTemporData[$i+1] = $product_data;
					$i++;		
				}
               

            }
            $csvdata = $csvdata + $csvTemporData;
            if(count($csvdata)>0){
                $csv->saveData($path, $csvdata);
            }
        }
    }
    public function createFolderExport(){
        $folder_include_file_export = Mage::getBaseDir('var') . DS . 'export';
        if (!file_exists($folder_include_file_export)) {
            mkdir($folder_include_file_export, 0777, true);
        }
    }
	public function nullify(){
		
	}

}

