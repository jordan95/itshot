<?php
class OpenTechiz_Production_Adminhtml_GoldScrapController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Production'))->_title($this->__('Gold Scrap'));
        $this->loadLayout()
            ->_setActiveMenu('erp');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_production/adminhtml_goldScrap'));
        return $this;
    }

    protected function _initModel($requestParam = 'id')
    {
        $model = Mage::registry('current_gold_scrap');
        if ($model) {
            return $model;
        }
        $model = Mage::getModel('opentechiz_production/goldScrap');
        $model->setStoreId($this->getRequest()->getParam('store', 0));

        $id = $this->getRequest()->getParam($requestParam);
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::throwException(Mage::helper('opentechiz_production')->__('Wrong Item requested.'));
            }
        }
        Mage::register('current_gold_scrap', $model);

        return $model;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_production/adminhtml_goldScrap_grid')->toHtml()
        );
    }

    public function editAction()
    {
        $model = $this->_initModel();

        $this->loadLayout()
            ->_setActiveMenu('erp');
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock('opentechiz_production/adminhtml_goldScrap_edit'))
            ->_addLeft($this->getLayout()->createBlock('opentechiz_production/adminhtml_goldScrap_edit_tabs'));

        $this->renderLayout();
    }

    /**
     * New product action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        $model = $this->_initModel();
        if ($post = $this->getRequest()->getPost()) {
            try {

                $model->addData($post);
                $model->save();

                $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('The Item has been update.'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/');
            }
        }

        return $this;
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/production/goldcastings');
    }
}