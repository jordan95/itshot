<?php

class Opentechiz_Production_Adminhtml_StaffController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            case 'edit':
                $aclResource = 'admin/erp/production/staffs/staffedit';
                break;
            case 'delete':
                $aclResource = 'admin/erp/production/staffs/staffdelete';
                break;
            default:
                $aclResource = 'admin/erp/production/staffs';
                break;
        }

        return Mage::getSingleton('admin/session')->isAllowed($aclResource);
    }

    protected function _initModel($requestParam = 'id')
    {
        $model = Mage::registry('current_staff');
        if ($model) {
            return $model;
        }
        $model = Mage::getModel('opentechiz_production/staff');
        $model->setStoreId($this->getRequest()->getParam('store', 0));

        $id = $this->getRequest()->getParam($requestParam);
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::throwException(Mage::helper('opentechiz_production')->__('Wrong Item requested.'));
            }
        }
        Mage::register('current_staff', $model);

        return $model;
    }

    protected function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp/production/staffs');
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock('opentechiz_production/adminhtml_staff'));
        $this->getLayout()->getBlock('head')->setTitle($this->__('Staff Grid'));
        $this->renderLayout();

    }

    public function editAction()
    {
        $model = $this->_initModel();

        $this->loadLayout()
            ->_setActiveMenu('erp/production/staffs');
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock('opentechiz_production/adminhtml_staff_edit'))
            ->_addLeft($this->getLayout()->createBlock('opentechiz_production/adminhtml_staff_edit_tabs'));

        $this->renderLayout();
    }

    /**
     * New product action
     */
    public function newAction()
    {

        $this->_forward('edit');
    }

    public function saveAction()
    {
        $model = $this->_initModel();
        if ($post = $this->getRequest()->getPost()) {
            try {
                $post['production_step'] = implode(',', $post['production_step']);
                $model->addData($post);
                $model->save();
                if(!$model->getStaffCode()) $model->setStaffCode($model->getId().$model->getStaffName())->save();
                $admin = Mage::getSingleton('admin/session')->getUser();
                if ($admin->getId()) {
                    Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Save staff #' . $this->getRequest()->getParam('id')));
                }
                $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('The Staff has been save.'));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/');
        return $this;
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        $model = $this->_initModel();
        if ($model->getId()) {
            try {
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('The staff has been deleted.'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect("*/*/");
    }


    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'staff.csv';
        $grid = $this->getLayout()->createBlock('opentechiz_production/adminhtml_staff_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'staff.xml';
        $grid = $this->getLayout()->createBlock('opentechiz_production/adminhtml_staff_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }
}
