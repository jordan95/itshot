<?php
class OpenTechiz_Production_Adminhtml_ProcessController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Production'))->_title($this->__('Process'));
        $this->loadLayout()
            ->_setActiveMenu('erp');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_production/adminhtml_process'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function ajaxGetStaffAction(){
        $params = $this->getRequest()->getParams();
        $return = [];
        $return['html'] = '<option value="">Select</option>';
        if(array_key_exists('type', $params) && array_key_exists('status', $params)){
            $staves = Mage::getModel('opentechiz_production/staff')->getCollection()->addFieldToFilter('staff_type', $params['type'])
                ->addFieldToFilter('status', 1);
            foreach ($staves as $staff){
                $steps = explode(',', $staff->getProductionStep());
                if (in_array($params['status'], $steps) && $staff->getStatus() != 0) {
                    $return['html'] .= '<option value="' . $staff->getId() . '">' . $staff->getStaffName() . '</option>';
                }
            }
            $return['error'] = 0;
        }else{
            $error = 1;
            $return['error'] = $error;
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('adminhtml')->__(
                    'Missing parameters.'
                )
            );
        }
        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($return));
    }

    public function ajaxAction(){
        $data = $this->getRequest()->getParams();

        if(array_key_exists('note', $data)){
            $note = $data['note'];
        }else{
            $note = '';
        }

        $logicModel = Mage::getModel('opentechiz_production/production_stepProcessing');
        $requestModel = Mage::getModel('opentechiz_production/product');
        $stoneRequestModel = Mage::getModel('opentechiz_material/stoneRequest');

        if(!isset($data['newbarcode'])){
            $newBarcode = true;
        }else {
            $newBarcode = $data['newbarcode'];
            $newBarcode = ($newBarcode === 'true');
        }
        $html = '';
        $return = [];

        $barcode = trim($data['barcode']);

        $requests = $requestModel->getCollection()->addFieldtoFilter('barcode',$barcode)
            ->addFieldtoFilter('status',array('neq'=>'50'))
            ->addFieldtoFilter('item_state',array('in'=>['0','4']))
            ->setOrder('created_at', 'DESC');
        $request = $requests->getFirstItem();
        if($request->getId()){
            $options = unserialize($request->getOption());
            //prevent repair process when repair order is not in repair stage
            if($request->getProductionType() == 2){
                $orderItem = Mage::getModel('sales/order_item')->load($request->getOrderItemId());
                $order = Mage::getModel('sales/order')->load($orderItem->getOrderId());
                if($order->getOrderType() != 2 && $order->getOrderStage() != 2){
                    $error = 1;
                    $return['error'] = $error;
                    Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('adminhtml')->__(
                            'Repair Order is not created or Repair Order Stage is not Fulfillment'
                        )
                    );
                    $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
                    $this->getResponse()->setBody(json_encode($return));
                }
            }

            if(!isset($error)) {
                if (isset($options['info_buyRequest']['product'])) {
                    $productId = unserialize($request->getOption())['info_buyRequest']['product'];
                    $product = Mage::getModel('catalog/product')->load($productId);
                } else {
                    $item = Mage::getModel('opentechiz_inventory/item')->load($barcode, 'barcode');
                    $sku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $item->getSku())[0];
                    $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                }
                $isPersonalized = Mage::helper('personalizedproduct')->isPersonalizedProduct($product);
                if ($request->getItemState() != 0 && $request->getItemState() != 4) {
                    $error = 1;
                    $return['error'] = $error;
                    Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('adminhtml')->__(
                            'The item requested is no longer in production!'
                        )
                    );
                    $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
                    $this->getResponse()->setBody(json_encode($return));
                } else {
                    $error = 0;
                    $return['error'] = $error;
                    if (array_key_exists('casting_code', $data)) {
                        $data['casting_code'] = trim(str_replace(' ', '', $data['casting_code']));
                        $itemModel = Mage::getModel('opentechiz_inventory/item');
                        $item = $itemModel->load($request->getBarcode(), 'barcode');
                        $check_option = Mage::helper('opentechiz_production')->checkGoldOption($data['casting_code'], $item);
                        if (!$check_option && ($isPersonalized || !(strpos($item->getSku(), 'quotation') === false))) {
                            $error = 1;
                            $return['error'] = $error;
                            //mage::log($check,null,'testdata.log',true);
                            Mage::getSingleton('adminhtml/session')->addError(
                                Mage::helper('adminhtml')->__(
                                    'Gold casting not fit to option. Please check again'
                                )
                            );
                            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
                            $this->getResponse()->setBody(json_encode($return));
                            return;
                        }

                        $check = $logicModel->addProductionGoldUse($data['casting_code'], $data['gold_weight'], $request->getId());

//                    $stoneCost = $itemModel->getCurrentStoneCost($data['option'], $product);
                        //add gold cost
                    }
                    if (isset($check)) {
                        $error = 1;
                        $return['error'] = $error;
                        //mage::log($check,null,'testdata.log',true);
                        Mage::getSingleton('adminhtml/session')->addError(
                            Mage::helper('adminhtml')->__(
                                'Gold used more than current gold casting weight or wrong gold casting code . Please check.'
                            )
                        );
                        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
                        $this->getResponse()->setBody(json_encode($return));
                    } else {
                        if (array_key_exists('casting_code', $data)) {
                            $goldCost = $itemModel->getCurrentAlloyCost($data['casting_code'], $data['gold_weight']);

                            $cost = $item->getCost() + $goldCost;
                            $item->setCost(number_format($cost, 2, '.', ''))->save();
                        }
                        $currentStep = $request->getStatus();
                        if ($newBarcode == true) {
                            $html .= $logicModel->getFormHtml($currentStep);
                            $return['html'] = $html;
                            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
                            $this->getResponse()->setBody(json_encode($return));
                        } else {
                            if (array_key_exists('status', $data)) {
                                $isBroken = $data['status'];
                            } else {
                                $isBroken = false;
                            }
                            $request_stone = $stoneRequestModel->load($request->getId(), 'personalized_id');
                            $productSku = $product->getSku();
                            if (array_key_exists('redo', $data) && $data['redo'] != '') {
                                //check if possible to redo process
                                if ($request->getProductionType() == 2) {

                                    $processArray = json_decode(Mage::helper('opentechiz_return')->getProductionStep($request->getId()));

                                    if ($processArray) {
                                        $processArray = array_filter($processArray);
                                        $a = array();
                                        $processFlip = array_flip(OpenTechiz_Production_Helper_Data::PROCESS_LIST);

                                        foreach ($processArray as $key => $value) {
                                            # code...
                                            $a[] = $processFlip[$value];
                                        }

                                        $processArray = $a;
                                    } else {
                                        $processArray = OpenTechiz_Production_Helper_Data::PROCESSES;
                                    }
                                } elseif ($request->getProductionType() == 5) {
                                    $processArray = OpenTechiz_Production_Helper_Data::RESIZE_AND_ENGRAVE_PROCESSES;
                                } else {
                                    $processArray = OpenTechiz_Production_Helper_Data::PROCESSES;
                                }

                                if (!in_array((int)$data['redo'], $processArray)) {
                                    $return['error'] = 1;
                                    Mage::getSingleton('adminhtml/session')->addError(
                                        Mage::helper('adminhtml')->__(
                                            'You can\'t redo this step in this production type', $barcode
                                        )
                                    );
                                } else {
                                    $logicModel->saveHistory($request, $data);
                                    $logicModel->redoStep($request, $data['redo'], $note);
                                }
                            } else if ((int)$currentStep == 25 && $request_stone->getId() && (int)$request_stone->getData('status') != 1 && (Mage::helper('personalizedproduct')->isPersonalizedProduct($product) || strpos($productSku, 'quotation') !== false)) {
                                $error = 1;
                                $return['error'] = $error;
                                Mage::getSingleton('adminhtml/session')->addError(
                                    Mage::helper('adminhtml')->__(
                                        'Cant finish this step because requested stones were declined or pending!'
                                    )
                                );
                            } else if ($isBroken) {
                                $logicModel->saveHistory($request, $data);
                                $logicModel->markAsBroken($request, $note);
                                $admin = Mage::getSingleton('admin/session')->getUser();
                                if ($admin->getId()) {
                                    Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Mark request #' . $request->getId() . ' as broken'));
                                }
                                $error = 1;
                                $return['error'] = $error;
                                $type = $request->getProductionType();

                                if ((int)$type == 3 || $request->getManufactureId() || array_key_exists('quotation_product_id', unserialize($request->getOption())['info_buyRequest']['options'])) {
                                    Mage::getSingleton('adminhtml/session')->addSuccess(
                                        Mage::helper('adminhtml')->__(
                                            'The item is marked as "Broken" and a new process request is created!'
                                        )
                                    );
                                } else {
                                    if ((int)$type == 2) {
                                        Mage::getSingleton('adminhtml/session')->addSuccess(
                                            Mage::helper('adminhtml')->__(
                                                'The item is marked as "Broken" and please reprocess return !'
                                            )
                                        );
                                    } elseif ((int)$type == 4) {
                                        Mage::getSingleton('adminhtml/session')->addSuccess(
                                            Mage::helper('adminhtml')->__(
                                                'The item is marked as "Broken" !'
                                            )
                                        );
                                    } else {
                                        Mage::getSingleton('adminhtml/session')->addSuccess(
                                            Mage::helper('adminhtml')->__(
                                                'The item is marked as "Broken" and a new process request is created!'
                                            )
                                        );
                                    }
                                }
                            } else if (!$isBroken) {
                                $logicModel->saveHistory($request, $data);
                                $logicModel->changeCurrentStepToNextStep($currentStep, $request);
                                $admin = Mage::getSingleton('admin/session')->getUser();
                                if ($admin->getId()) {
                                    Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Finish step ' . OpenTechiz_Production_Helper_Data::PROCESS_LIST[$currentStep] . ' of request #' . $request->getId()));
                                }
                            }

                            if ($request->getStatus() == 50) {
                                $logicModel->markAsFinished($request);
                                $error = 1;
                                $return['error'] = $error;
                                Mage::getSingleton('adminhtml/session')->addSuccess(
                                    Mage::helper('adminhtml')->__(
                                        'Production finished!'
                                    )
                                );
                            }
                            $html .= $logicModel->getFormHtml($request->getStatus());
                            $return['html'] = $html;

                            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
                            $this->getResponse()->setBody(json_encode($return));

                        }
                    }
                }
            }
        } else{
            $error = 1;
            $return['error'] = $error;
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('adminhtml')->__(
                    'Barcode doesnt exist or the item with this barcode is no longer in production!',$barcode
                )
            );
            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($return));
        }
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/production/process');
    }
}