<?php


class OpenTechiz_Production_Adminhtml_ManufactureController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Production'))->_title($this->__('Manufacture'));
        $this->loadLayout()
            ->_setActiveMenu('erp');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_production/adminhtml_manufacture'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function productGridAction(){
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_production/adminhtml_request_manufacture_products')->toHtml()
        );
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/production/manufacture');
    }
}