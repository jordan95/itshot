<?php


class OpenTechiz_Production_Adminhtml_Request_ProcessController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp')
            ->_title($this->__('ERP'))
            ->_title($this->__('Production'))
            ->_title($this->__('Product Process Request'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_production/adminhtml_request_process'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_production/adminhtml_request_process_grid')->toHtml()
        );
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/production/process');
    }

    public function productionRequestAction(){
        $ids = $this->getRequest()->getParam('ids');
        $processRequestModel = Mage::getModel('opentechiz_production/process_request');
        $processRequestCollection = $processRequestModel->getCollection()->addFieldToFilter('id', array('in' => $ids));
        $success = count($ids);
        $failure = 0;

        $hasProcessedItem = false;
        foreach ($processRequestCollection as $request) {
            if($request->getStatus() != 0){
                $hasProcessedItem = true;
            }
        }

        if($hasProcessedItem == false) {
            foreach ($processRequestCollection as $request) {
                $orderItemId = $request->getOrderItemId();
                $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);
                if (strpos($orderItem->getSku(), 'quotation') !== false) {
                    $processRequestModel->requestToProcessQuotation($orderItemId);
                    $request->setStatus(1)->save();
                    $order = Mage::getModel('sales/order')->load($orderItem->getOrderId());
                    Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, null, 2);
                } else {
                    if ($this->isProductionPossible($orderItemId)) {
                        $processRequestModel->createProductionRequest($orderItemId);
                        $request->setStatus(1)->save();

                        //change order stage and status
                        $order = Mage::getModel('sales/order')->load($orderItem->getOrderId());
                        Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, null, 2);
                    } else {
                        $failure++;
                        $success--;
                    }
                }
            }

            if ($success == 0) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('adminhtml')->__(
                        '%d Request was not updated because product(s) cannot be produce', $failure
                    )
                );
            } elseif ($failure == 0) {
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('%d Production Process Request was updated!', $success));
            } else {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        '%d Production Process Request was updated, %d Request was not updated because product(s) cannot be produce', count($ids), $failure
                    )
                );
            }
            $this->_redirect('*/*/index');
        }else{
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('adminhtml')->__(
                    'Selected records contain processed request(s). Please check and try again'
                )
            );
            $this->_redirect('*/*/index');
        }
    }

    public function stockRequestAction(){
        $ids = $this->getRequest()->getParam('ids');

        $processRequestModel = Mage::getModel('opentechiz_production/process_request');
        $processRequestCollection = $processRequestModel->getCollection()->addFieldToFilter('id', array('in' => $ids));
        $success = count($ids);
        $failure = 0;

        $hasProcessedItem = false;
        foreach ($processRequestCollection as $request) {
            if($request->getStatus() != 0){
                $hasProcessedItem = true;
            }
        }

        if($hasProcessedItem == false) {
            foreach ($processRequestCollection as $request) {
                $orderItemId = $request->getOrderItemId();
                $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);
                $order = Mage::getModel('sales/order')->load($orderItem->getOrderId());
                $flag = $processRequestModel->createStockProductRequest($orderItemId,$order->getId(),$request->getId());
                if (!$flag) {
                    $failure++;
                    $success--;
                    
                    Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, null, 3);
                } else {
                    $request->setStatus(2)->save();

                    //change order stage and status
                    Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, null, 1);
                }
            }
            if ($failure == 0) {
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('%d Process Request was updated!', $success));
            } elseif ($success == 0) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('No Quantity in stock; No Request was updated!'));
            } else {
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('%d Process Request was updated, %d Request are not updated due to insufficient stock', $success, $failure));
            }

            $this->_redirect('*/*/index');
        }else{
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('adminhtml')->__(
                    'Selected records contain processed request(s). Please check and try again'
                )
            );
            $this->_redirect('*/*/index');
        }
    }
    
    public function isProductionPossible($orderItemId){
        $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);

        if(Mage::helper('personalizedproduct')->canProduceProduct($orderItem->getProductId())) return true;
        else return false;
    }
}