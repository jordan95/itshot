<?php


class OpenTechiz_Production_Adminhtml_Request_ProductController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp')
            ->_title($this->__('ERP'))
            ->_title($this->__('Production'))
            ->_title($this->__('Manage Production Requests'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_production/adminhtml_request_product_grid')->toHtml()
        );
    }

    public function newAction()
    {
        $this->loadLayout()->_setActiveMenu('erp/production/manufacture');
        $this->renderLayout();
    }

    public function manufactureAction(){
        $params = $this->getRequest()->getParams();
        $logicModel = Mage::getModel('opentechiz_production/production_manufacture');

        $logicModel->saveProductRequest($params);

        Mage::getSingleton('adminhtml/session')->addSuccess(
            Mage::helper('adminhtml')->__(
                'Production process was updated!'
            )
        );

        $this->_redirect('*/*/index');
    }

    public function ajaxAction(){
        $productId = $this->getRequest()->getParam('product_id');
        $logicModel = Mage::getModel('opentechiz_production/production_manufacture');

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        return $this->getResponse()->setBody(json_encode($logicModel->manufactureForm($productId)));
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/production/items');
    }

    public function printbarcodeAction(){
        if ($this->getRequest()->getParam('item')) {

            $item_ids = $this->getRequest()->getParam('item');
            $number = 0;
            try {
                $content = '';
                foreach ($item_ids as $item_id) {
                    # code...
                    $block = $this->getLayout()->createBlock('core/template');
                    $block->setTemplate('opentechiz/production/barcodepdf.phtml');
                    $item_model = Mage::getModel('opentechiz_production/product')->load($item_id) ;
                    $file = Zend_Barcode::draw('code128', 'image', array('text' => $item_model->getBarcode()), array());

                    $barcode_path="media/upload/".$item_model->getBarcode().".png";
                    $store_image = imagepng($file,Mage::getBaseDir('media')."/upload/".$item_model->getBarcode().".png");

                    $block->setimagepath( $barcode_path);
                    $content .= $block->toHtml();
                    $number++;
                    if ($number < count($item_ids)) {
                        $content .= '<pagebreak />';
                    }
                }


                require_once(Mage::getBaseDir('lib') . DS . 'mpdf/vendor/autoload.php');

                if($content != '') {
                    $mpdf = new \Mpdf\Mpdf(array('utf-8', array(70,30), '', 'dejavusans', 0, 0, 0, 0, 0, 0,'P'));
                    $mpdf->SetDisplayMode('fullpage');
                    $mpdf->WriteHTML($content);
                    $mpdf->Output('certificate.pdf','D');
                    exit;
                }

            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/');
            }
        }
        else{
            $this->_getSession()->addError($this->__('You need choose item to print'));
            $this->_redirect('*/*/');
        }
    }

    public function printPDFAction(){
        if ($this->getRequest()->getParam('item')){

            $ids = $this->getRequest()->getParam('item');
            $quality = OpenTechiz_PersonalizedProduct_Helper_Data::QUALITY;

            foreach ($ids as $id) {
                try {
                    $data = [];
                    $production = Mage::getModel('opentechiz_production/product')->load($id);
                    $barcode = $production->getBarcode();

                    $item = Mage::getModel('opentechiz_inventory/item')->load($barcode, 'barcode');
                    $productSku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $item->getSku())[0];
                    $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productSku);
                    $product = Mage::getModel('catalog/product')->load($product->getId());

                    $isPersonalized = Mage::helper('personalizedproduct')->isPersonalizedProduct($product);
                    if(strpos($item->getSku(), 'quotation') !== false){
                        $data['is_quotation'] = true;
                    }else $data['is_quotation'] = false;

                    if($production->getOrderItemId()){
                        $orderItem = Mage::getModel('sales/order_item')->load($production->getOrderItemId());
                        $order = Mage::getModel('sales/order')->load($orderItem->getOrderId());

                        $data['order_id'] = $order->getIncrementId();
                        $data['ship_by'] = $order->getShipBy();
                        $data['order_date'] = $order->getCreatedAt();
                    }

                    $data['barcode'] = $barcode;
                    $data['sku'] = $item->getSku();

                    //images
                    if($isPersonalized) {
                        $skuPart = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $data['sku']);
                        $sku = $skuPart[0];
                        $metal = $skuPart[count($skuPart) - 2];
                        $imageName = $sku;
                        $imageName .= '-' . $metal;
                        for ($i = 1; $i < count($skuPart) - 2; $i++) {
                            if (strpos($skuPart[$i], 'diamond') !== false) {
                                $diamondType = str_replace('diamond', '', $skuPart[$i]);
                                if (in_array($diamondType, $quality)) {
                                    $skuPart[$i] = 'diamond';
                                }
                            }
                            $imageName .= '-' . $skuPart[$i];
                        }
                        $_GET['path'] = $imageName . '-1.jpg';
                        ob_start(); // begin collecting output
                        include(Mage::getBaseDir() . DS . 'image_render_for_pdf.php');
                        $data['image'] = ob_get_contents();
                        ob_end_clean();

                        $data['images'] = [];
                        ob_start();
                        for ($i = 1; $i <= 2; $i++) {

                            $_GET['path'] = $imageName . '-' . $i . '.jpg';
                            include(Mage::getBaseDir() . DS . 'image_render_for_pdf.php');
                            $contents = ob_get_clean();

                            array_push($data['images'], $contents);
                        }
                        ob_end_clean();
                    }else{
                        $data['image'] = Mage::helper('catalog/image')->init($product, 'thumbnail')->__toString();
                        $data['image'] = Mage::helper('opentechiz_salesextend')->getImageByColor($product, $data['sku'], $data['image']);
                        $data['images'] = [];

                        foreach ($product->getMediaGalleryImages() as $image)
                        {
                            $file_name = $image->getFile();
                            array_push($data['images'], 'media/catalog/product/'.$file_name);
                        }
                    }
                    //


                    $data['production_type'] = $production->getProductionType();
                    if(!$data['image'] || $data['image'] == ''){
                        $data['image'] = Mage::getBaseDir('media').'/upload/small_image.jpg';
                    }
                    $data['manufacturer'] = $product->getAttributeText('manufacturer');
                    $data['product_name'] = $item->getName();
                    $data['is_personalized'] = $isPersonalized;
                    $data['personalized_id'] = $production->getId();
                    $data['product_id'] = $product->getId();
                    $data['options'] = $item->getOptions();
                    $data['width'] = $product->getAttributeText('c2c_width_1');
                    $data['weight'] = $product->getWeight();

                    if($isPersonalized || $data['is_quotation']) {
                        $data['stone_info'] = Mage::getModel('opentechiz_material/stoneRequest')->load($production->getPersonalizedId(), 'personalized_id')->getStones();
                    }

                    require_once(Mage::getBaseDir('lib') . DS . 'mpdf/vendor/autoload.php');
                    $block = $this->getLayout()->createBlock('core/template');
                    $block->setItemId($id);
                    $block->setItemData($data);
                    $block->setTemplate('opentechiz/production/manufacture_pdf.phtml');

                    $mpdf = new \Mpdf\Mpdf(array('c', 'A4', '', '', 0, 0, 0, 0, 0, 0));
                    $mpdf->showImageErrors = true;
                    $mpdf->SetDisplayMode('fullpage');
                    $mpdf->AddPage('', // L - landscape, P - portrait
                        '', '', '', '',
                        5, // margin_left
                        5, // margin right
                        10, // margin top
                        20, // margin bottom
                        0, // margin header
                        0); // margin footer
                    $mpdf->WriteHTML($block->toHtml());
                    $mpdf->Output('manufacture.pdf', 'D');
                    exit;

                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                    $this->_redirect('*/*/');
                }
            }

            $this->_redirect('*/*/');
        }else{
            $this->_getSession()->addError($this->__('You need choose item to print'));
            $this->_redirect('*/*/');
        }
    }

}
