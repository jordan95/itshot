<?php
/**
 * Created by PhpStorm.
 * Date: 20-09-2017
 * Time: 13:19
 */

class OpenTechiz_Production_Adminhtml_Manage_BrokenstoneController extends Mage_Adminhtml_Controller_Action
{
    public function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('productions/procedure_process')
            ->_addBreadcrumb($this->__('erp'), $this->__('production'))
            ->_addBreadcrumb($this->__('Manage Broken Stones'), $this->__('Manage Broken Stones'));
        return $this;
    }

    protected function _initModel($requestParam = 'id')
    {
        $model = Mage::registry('current_brokenstone');
        if ($model) {
            return $model;
        }
        $model = Mage::getModel('opentechiz_production/brokenstone');
        $id = $this->getRequest()->getParam($requestParam);
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::throwException(Mage::helper('opentechiz_production')->__('Wrong Item requested.'));
            }
        }
        Mage::register('current_brokenstone', $model);

        return $model;
    }

    public function indexAction() {
        $this->loadLayout()
            ->_setActiveMenu('erp/production/brokenstone');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_production/adminhtml_manage_brokenstone'));
        $this->_title($this->__("Manage Broken Stones"));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_production/adminhtml_manage_brokenstone_grid')->toHtml()
        );
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('opentechiz_production/brokenstone')->load($id);

        if($model->getId() || $id == 0) {
            Mage::register('current_brokenstone', $model);
            $this->_initAction();
            $this->_title($this->__("Manage Broken Stones"));
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $editblock = $this->getLayout()->createBlock('opentechiz_production/adminhtml_manage_brokenstone_edit');
            $edittab = $this->getLayout()->createBlock('opentechiz_production/adminhtml_manage_brokenstone_edit_tabs');
            $this->_addContent($editblock)->_addLeft($edittab);

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_production')->__('Item does not exist!'));
            $this->_redirect('*/*/');
        }
    }

    public function saveAction() {
        $model = $this->_initModel();
        if($this->getRequest()->getPost()) {
            try {
                $postData = $this->getRequest()->getPost();
                $qualities = OpenTechiz_PersonalizedProduct_Helper_Data::QUALITY;

                if (!isset($postData['stone_name']) && isset($postData['stone_name_custom'])) {
                    $postData['stone_name'] = $postData['stone_name_custom'];
                }
                $model->addData($postData);
                $model->setCreatedAt(Varien_Date::now());
                //var_dump($data);die;
                $model->save();

                $stoneName = explode('-', $postData['stone_name']);
                $stoneType = $stoneName[0];
                if(in_array(str_replace('diamond', '', $stoneType), $qualities)){
                    $quality = str_replace('diamond', '', $stoneType);
                    $stoneType = 'diamond';
                }else{
                    $quality = null;
                }

                $shape = $stoneName[1];
                $diameter = $stoneName[2];
                if($quality) {
                    $stone = Mage::getModel('opentechiz_material/stone')->getCollection()->addFieldToFilter('stone_type', $stoneType)
                        ->addFieldToFilter('quality', $quality)->addFieldToFilter('shape', $shape)->addFieldToFilter('diameter', $diameter)
                        ->getFirstItem();
                }else{
                    $stone = Mage::getModel('opentechiz_material/stone')->getCollection()->addFieldToFilter('stone_type', $stoneType)
                        ->addFieldToFilter('shape', $shape)->addFieldToFilter('diameter', $diameter)
                        ->getFirstItem();
                }
                if((int)$stone->getQty() >= (int)$postData['broken_qty']) {
                    $stone->setQty((int)$stone->getQty() - (int)$postData['broken_qty'])->save();

                    //log
                    $admin = Mage::getSingleton('admin/session')->getUser();
                    if ($admin->getId()) {
                        Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                            'user_id' => $admin->getId(),
                            'description' => 'Mark '. (int)$postData['broken_qty'].' Stone #' . $stone->getId() . ' as broken',
                            'qty' => '-' . (int)$postData['broken_qty'],
                            'on_hold' => '',
                            'type' => 0,
                            'stock_id' => $stone->getId()
                        ));
                    }
                }else{
                    Mage::getSingleton('adminhtml/session')->addError("Quantity you input is larger than quantity in stock");
                    $this->_redirect('*/*/new');
                    return;
                }

                Mage::getSingleton('adminhtml/session')->addSuccess("Item was successfully saved!");
                Mage::getSingleton('adminhtml/session')->setBrokenstoneData(false);
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setBrokenstoneData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::unregister('current_brokenstone');
        $this->_redirect('*/*/');
    }

    public function getDatastoneBrokenAction(){
        $params = $this->getRequest()->getParams();
        $response = array();
        if (isset($params['barcode']) && $params['barcode']) {
            $barcode = str_replace(' ', '', $params['barcode']);
            $item = Mage::getModel('opentechiz_inventory/item')->load($barcode, 'barcode');
            $array_stoneName = array();
            if ($item->getData()) {
                $stoneinfo = Mage::helper('opentechiz_production/stoneInfo')->getStoneInfoBroken($item);
                $array_stoneName = array_merge($stoneinfo, $array_stoneName);

            }else{
                $response['error'] = $this->__("Barcode doesn't exist.");
            }
            if($array_stoneName == [] && !array_key_exists('error', $response)){
                $response['error'] = $this->__("Item exists but it seems to have no stone data");
            }
        }
        $response['list_stones'] = $array_stoneName;
        return $this->getResponse()->setBody(Zend_Json::encode($response));
    }
}