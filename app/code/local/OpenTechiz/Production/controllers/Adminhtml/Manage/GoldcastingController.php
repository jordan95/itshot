<?php


class OpenTechiz_Production_Adminhtml_Manage_GoldcastingController extends Mage_Adminhtml_Controller_Action
{

    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            case 'delete':
                $aclResource = 'admin/erp/production/goldcastings/goldcastingdelete';
                break;
            case 'edit':
                $aclResource = 'admin/erp/production/goldcastings/goldcastingedit';
                break;
            default:
                $aclResource = 'admin/erp/production/goldcastings';
                break;
        }
        return Mage::getSingleton('admin/session')->isAllowed($aclResource);
    }

    protected function _initModel($requestParam = 'id')
    {
        $model = Mage::registry('current_gold_casting');
        if ($model) {
            return $model;
        }
        $model = Mage::getModel('opentechiz_production/goldcasting');
        $model->setStoreId($this->getRequest()->getParam('store', 0));

        $id = $this->getRequest()->getParam($requestParam);
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::throwException(Mage::helper('opentechiz_production')->__('Wrong Item requested.'));
            }
        }
        Mage::register('current_gold_casting', $model);

        return $model;
    }

    protected function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp/production/goldcastings');
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('opentechiz_production/adminhtml_manage_goldcasting'));
        $this->getLayout()->getBlock('head')->setTitle($this->__('Gold Casting Grid'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();

        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_production/adminhtml_manage_goldcasting_grid')->toHtml()
        );
    }

    public function editAction()
    {
        $model = $this->_initModel();

        $this->loadLayout()
            ->_setActiveMenu('erp/production/goldcastings');
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock('opentechiz_production/adminhtml_manage_goldcasting_edit'))
            ->_addLeft($this->getLayout()->createBlock('opentechiz_production/adminhtml_manage_goldcasting_edit_tabs'));

        $this->renderLayout();
    }

    /**
     * New product action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        $model = $this->_initModel();
        if ($post = $this->getRequest()->getPost()) {
            try {

                $model->addData($post);
                $model->save();
                $remain_weight = Mage::helper('opentechiz_production')->getRemainWeight($model);
                if($remain_weight == 0)
                    $model->setStatus(0)->save();

                $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('The Item has been update.'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/');
            }
        }

        return $this;
    }
     /**
     * MassStatus action
     */
    public function massStatusAction()
    {
        $goldcastingIds = $this->getRequest()->getParam('gold_casting_id');

        if (!is_array($goldcastingIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s).'));
        } else {
            try {
                $admin = Mage::getSingleton('admin/session')->getUser();
                foreach ($goldcastingIds as $goldcastingId) {
                    $goldcasting =  Mage::getModel('opentechiz_production/goldcasting')->load($goldcastingId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true);
                    $goldcasting->save();
                   

                    if($admin->getId()){
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'MassAction change status gold casting #'.$goldcastingId));
                    }
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated.', count($goldcastingIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction()
    {
        $goldcasting = $this->_initModel();
        if ($goldcasting->getId() ) {
            try {
                $goldcasting->setStatus(OpenTechiz_Production_Model_Goldcasting::GOLD_STATUS_CLOSED)
                    ->save();
                $this->_getSession()->addSuccess($this->__('This item has been closed'));
            } catch (Exception $exception) {
                $this->_getSession()->addError($exception->getMessage());
            }
        }
        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
    }


}