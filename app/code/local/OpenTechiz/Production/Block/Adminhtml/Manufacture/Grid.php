<?php

class OpenTechiz_Production_Block_Adminhtml_Manufacture_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('manufactureGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_production/production_manufacture')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Manufacture #'),
            'index'     => 'id',
            'width'     => '80',
            'align'     => 'center'
        ));
        $this->addColumn('image', [
            'header'    => Mage::helper('opentechiz_production')->__('Gallery Image'),
            'index'     => 'image',
            'width'     => '125px',
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'OpenTechiz_Production_Block_Adminhtml_Renderer_Image'
        ]);
        $this->addColumn('options', array(
            'header'    => Mage::helper('opentechiz_production')->__('Options Information'),
            'index'     => 'options',
            'width'     =>  '500',
            'renderer'  => 'OpenTechiz_Production_Block_Adminhtml_Renderer_CustomOptions'
        ));
        $this->addColumn('name', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Product Name'),
            'type'      => 'text',
            'index'     =>  'name',
        ));
        $this->addColumn('sku', array(
            'header'    => Mage::helper('opentechiz_production')->__('SKU'),
            'index'     => 'sku',
            'width'     =>  '100',
        ));
        $this->addColumn('qty', array(
            'header'    => Mage::helper('opentechiz_production')->__('Quantity'),
            'index'     => 'qty',
            'width'     =>  '50'
        ));
        $this->addColumn('created_at', array(
            'header'    => Mage::helper('opentechiz_production')->__('Created At'),
            'index'     => 'created_at',
            'type'      => 'datetime',
        ));
//        $this->addColumn('status', array(
//            'header'    => Mage::helper('opentechiz_production')->__('Status'),
//            'width'     => '100',
//            'index'     => 'status',
//            'type'      => 'options',
//            'options'   => OpenTechiz_Production_Helper_Data::PROCESS_STATUS
//        ));
        $this->addColumn('note', array(
            'header'    => Mage::helper('opentechiz_production')->__('Comment'),
            'index'     => 'note',
            'width'     =>  '100'
        ));
        $this->addColumn('admin_user_id', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Admin User'),
            'index'     =>  'admin_user_id',
            'renderer'  => 'OpenTechiz_Production_Block_Adminhtml_Renderer_AdminName',
            'filter'    => false,
            'sortable'  => false,
        ));
        return parent::_prepareColumns();
    }
}