<?php

class Opentechiz_Production_Block_Adminhtml_Staff_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = "id";
        $this->_blockGroup = "opentechiz_production";
        $this->_controller = "adminhtml_staff";
        $this->_updateButton("save", "label", Mage::helper("opentechiz_production")->__("Save Staff"));
//				$this->_updateButton("delete", "label", Mage::helper("opentechiz_production")->__("Delete Staff"));

        if (!Mage::getSingleton('admin/session')->isAllowed('admin/erp/production/staffs/staffdelete')) {
            $this->_removeButton('delete');
        }

    }

    public function getHeaderText()
    {
        if (Mage::registry("current_staff") && Mage::registry("current_staff")->getId()) {

            return Mage::helper("opentechiz_production")->__("Edit Staff '%s'", $this->htmlEscape(Mage::registry("current_staff")->getData('staff_name')));

        } else {

            return Mage::helper("opentechiz_production")->__("Add Staff");

        }
    }
}