<?php

class Opentechiz_Production_Block_Adminhtml_Staff_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

        public function __construct()
        {
                parent::__construct();
                $this->setId("staffGrid");
                $this->setDefaultSort("id");
                $this->setDefaultDir("DESC");
                $this->setSaveParametersInSession(true);
        }

        protected function _prepareCollection()
        {
                $collection = Mage::getModel("opentechiz_production/staff")->getCollection();
                $this->setCollection($collection);
                return parent::_prepareCollection();
        }

    protected function _prepareColumns()
    {
        $this->addColumn("id", array(
            "header" => Mage::helper("opentechiz_production")->__("ID"),
            "align" => "left",
            "width" => "30px",
            "type" => "number",
            "index" => "id",
        ));

        $this->addColumn("staff_name", array(
            "header" => Mage::helper("opentechiz_production")->__("Staff name"),
            "index" => "staff_name",
        ));

//        $this->addColumn("staff_code", array(
//            "header" => Mage::helper("opentechiz_production")->__("Staff code"),
//            "index" => "staff_code",
//        ));

        $this->addColumn("status", array(
            "header" => Mage::helper("opentechiz_production")->__("Status"),
            "index" => "status",
            'align' => 'center',
            'type' => 'options',
            'options' => OpenTechiz_Production_Helper_Data::OPPENTECHIZ_STAFF_STATUS
        ));

        $this->addColumn("staff_type", array(
            "header" => Mage::helper("opentechiz_production")->__("Type"),
            "index" => "staff_type",
            'align' => 'center',
            'type' => 'options',
            'options' => OpenTechiz_Production_Helper_Data::STAFF_TYPE
        ));

        $this->addColumn("is_owner", array(
            "header" => Mage::helper("opentechiz_production")->__("Owner"),
            "index" => "is_owner",
            'align' => 'center',
            'type' => 'options',
            'options' => array(
                0 => 'No',
                1 => 'Yes'
            )
        ));

        $this->addColumn("assignment", array(
            "header" => Mage::helper("opentechiz_production")->__("Manufacturing assignment"),
            "index" => "production_step",
            'align' => 'left',
            'renderer' => 'OpenTechiz_Production_Block_Adminhtml_Renderer_StaffAssignment'
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('admin/erp/production/staffs/staffedit')) {
            $this->addColumn('action',
                array(
                    'header' => Mage::helper('opentechiz_production')->__('Action'),
                    'width' => '50',
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('opentechiz_production')->__('Edit'),
                            'url' => array('base' => '*/*/edit'),
                            'field' => 'id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'is_system' => true,
                    'align' => 'center'
                )
            );
        }

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

        public function getRowUrl($row)
        {
            if (Mage::getSingleton('admin/session')->isAllowed('admin/erp/production/staffs/staffedit')) {
                    return $this->getUrl("*/*/edit", array("id" => $row->getId()));
            }
        }		

}