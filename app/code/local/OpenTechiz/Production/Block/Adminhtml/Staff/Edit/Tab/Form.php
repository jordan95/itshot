<?php
class Opentechiz_Production_Block_Adminhtml_Staff_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("production_form", array("legend"=>Mage::helper("opentechiz_production")->__("Staff information")));
        $fieldset->addField("staff_name", "text", array(
            "label" => Mage::helper("opentechiz_production")->__("Staff name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "staff_name",
        ));

//        $fieldset->addField("staff_code", "text", array(
//            "label" => Mage::helper("opentechiz_production")->__("Staff code"),
//            "class" => "required-entry",
//            "required" => true,
//            "name" => "staff_code",
//        ));

        $fieldset->addField("status", "select", array(
            "label" => Mage::helper("opentechiz_production")->__("Status"),
            'values'   => OpenTechiz_Production_Helper_Data::OPPENTECHIZ_STAFF_STATUS ,
            "name" => "status",
        ));

        $fieldset->addField("staff_type", "select", array(
            "label" => Mage::helper("opentechiz_production")->__("Type"),
            'values'   => OpenTechiz_Production_Helper_Data::STAFF_TYPE ,
            "name" => "staff_type",
        ));

        $fieldset->addField("is_owner", "select", array(
            "label" => Mage::helper("opentechiz_production")->__("Is owner"),
            'values'   => array(0 => 'No', 1 => 'Yes') ,
            "name" => "is_owner",
        ));

         $fieldset->addField('production_step', 'multiselect', array(
            'label'     => Mage::helper('opentechiz_production')->__('Manufacturing Assignment'),
            "required" => true,
            'values'   => $this->_getStep(),
            'name' => 'production_step[]'
        ));

        if (Mage::getSingleton("adminhtml/session")->getStaffData())
        {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getStaffData());
            Mage::getSingleton("adminhtml/session")->setStaffData(null);
        }
        elseif(Mage::registry("current_staff")) {
            $form->setValues(Mage::registry("current_staff")->getData());
        }
        return parent::_prepareForm();
    }

     protected function _getStep()
    {
        $types = OpenTechiz_Production_Helper_Data::PROCESS_LIST;
        $options = array();
        foreach($types as $value => $label)
        {
            $options[] = array('label'=> $label, 'value' => $value);
        }
        return $options;
    }
}
