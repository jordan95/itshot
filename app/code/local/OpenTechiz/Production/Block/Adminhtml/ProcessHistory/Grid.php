<?php

class OpenTechiz_Production_Block_Adminhtml_ProcessHistory_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('historyGrid');
        $this->setDefaultSort('finished_at');
        $this->setNoFilterMassactionColumn(true);
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_production/production_history')->getCollection();
        $collection->getSelect()
            ->joinLeft(array('order_item' => 'tsht_sales_flat_order_item'), 'order_item.item_id = main_table.item_id',
                array('order_item.order_id', 'main_table.item_id' => 'main_table.item_id'))
            ->joinLeft(array('personalized' => 'tsht_request_product'), 'main_table.personalized_id = personalized.personalized_id',
                array('personalized_id' => 'personalized.personalized_id', 'manufacture_id' => 'personalized.manufacture_id'));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('personalized_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Personalized #'),
            'index'     => 'personalized_id',
            'width'     =>  '50',
            'filter_index'  => 'personalized.personalized_id'
        ));
        $this->addColumn('order_item_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Order Item #'),
            'index'     => 'main_table.item_id',
            'width'     =>  '100'
        ));
        $this->addColumn('manufacture_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Manufacture #'),
            'index'     => 'manufacture_id',
            'width'     =>  '100',
            'renderer' => 'OpenTechiz_Production_Block_Adminhtml_Renderer_ManufactureId'
        ));
        $this->addColumn('step_name', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Step Name'),
            'type'      => 'text',
            'index'     =>  'step_name',
        ));
        $this->addColumn('gold_weight', array(
            'header'    => Mage::helper('opentechiz_production')->__('Gold Weight (gr)'),
            'index'     => 'gold_weight',
            'width'     =>  '100'
        ));
        $this->addColumn('stone_weight', array(
            'header'    => Mage::helper('opentechiz_production')->__('Stone Weight'),
            'index'     => 'stone_weight',
            'width'     =>  '100'
        ));
        $this->addColumn('item_weight', array(
            'header'    => Mage::helper('opentechiz_production')->__('Item Weight'),
            'width'     =>  '100',
            'filter'    => false,
            'sortable'  => false,
            'renderer'  => 'OpenTechiz_Production_Block_Adminhtml_Renderer_Itemweight'
        ));
        $this->addColumn('staff_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Staff'),
            'index'     => 'staff_id',
            'type'      => 'text',
            'renderer'  => 'OpenTechiz_Production_Block_Adminhtml_Renderer_Manufacture_StaffName'
        ));
        $this->addColumn('owner_staff_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Owner'),
            'index'     => 'owner_staff_id',
            'type'      => 'text',
            'renderer'  => 'OpenTechiz_Production_Block_Adminhtml_Renderer_Manufacture_OwnerStaffName'
        ));
        $this->addColumn('started_at', array(
            'header'    => Mage::helper('opentechiz_production')->__('Created At'),
            'index'     => 'started_at',
            'type'      => 'datetime',
            'filter_index' => 'main_table.started_at'
        ));
        $this->addColumn('finished_at', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Finished At'),
            'type'      => 'datetime',
            'index'     =>  'finished_at',
            'filter_index' => 'main_table.finished_at'
        ));
        $this->addColumn('status', array(
            'header'    => Mage::helper('opentechiz_production')->__('Status'),
            'width'     => '100',
            'index'     => 'main_table.status',
            'type'      => 'options',
            'filter_index' => 'main_table.status',
            'options'   => OpenTechiz_Production_Helper_Data::PROCESS_STATUS,
            'renderer'  => 'OpenTechiz_Production_Block_Adminhtml_Renderer_HistoryStatus'
        ));
        $this->addColumn('note', array(
            'header'    => Mage::helper('opentechiz_production')->__('Comment'),
            'index'     => 'note',
            'width'     =>  '100'
        ));
        $this->addColumn('admin_user_id', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Admin User'),
            'index'     =>  'admin_id',
            'renderer'  => 'OpenTechiz_Production_Block_Adminhtml_Renderer_AdminName',
            'sortable'  => false,
            'filter'    => false
        ));
        return parent::_prepareColumns();
    }
}