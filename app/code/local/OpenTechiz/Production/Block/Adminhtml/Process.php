<?php

class OpenTechiz_Production_Block_Adminhtml_Process extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_process';
    protected $_blockGroup = 'opentechiz_production';
    protected $_headerText = 'Update Production Process';

    public function __construct()
    {
        parent::__construct();
    }
    
    public function getFormAction(){
        return Mage::getUrl('adminhtml/process/ajax/');
    }
}