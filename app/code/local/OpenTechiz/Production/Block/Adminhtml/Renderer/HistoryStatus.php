<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_HistoryStatus extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $status =  $row->getData('status');

        if($status == 0) $color = 'green';
        elseif($status == 1) $color = 'red';
        else $color= 'black';

        $html = '<span style="color:'.$color.'" >'.OpenTechiz_Production_Helper_Data::PROCESS_STATUS[$status].'</span>';
        return $html;
    }
}