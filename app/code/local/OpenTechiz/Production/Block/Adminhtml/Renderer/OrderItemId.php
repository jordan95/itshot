<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_OrderItemId extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $orderItemId = $row->getOrderItemId();
        return '<a href="' . Mage::helper('adminhtml')->getUrl('adminhtml/request_process/index/') . '">' . $orderItemId . '</a>';
    }
}