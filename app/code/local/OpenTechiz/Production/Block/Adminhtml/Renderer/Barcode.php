<?php
class OpenTechiz_Production_Block_Adminhtml_Renderer_Barcode extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $barcode = $row->getBarcode();
        return '<a href="'.$this->getUrl('adminhtml/process/index/', array('barcode' => $barcode)).'">'.$barcode.'</a>';
    }
}