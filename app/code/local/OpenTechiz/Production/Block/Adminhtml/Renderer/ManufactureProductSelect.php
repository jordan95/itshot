<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_ManufactureProductSelect extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $productId =  $row->getData('entity_id');

        $html = '<a href="#/" onclick="jsGetProduct('.$productId.')">Select</a>';
        return $html;
    }
}