<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_ManufactureId extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $manufactureId = $row->getManufactureId();
        $filter = 'id='.$manufactureId;
        $filter = base64_encode($filter);
        $url = Mage::helper('adminhtml')->getUrl('adminhtml/manufacture/index', array('filter'=>$filter));
        return '<a href="' . $url . '">' . $manufactureId . '</a>';
    }
}