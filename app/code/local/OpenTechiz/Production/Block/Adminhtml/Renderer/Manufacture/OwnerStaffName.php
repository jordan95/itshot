<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_Manufacture_OwnerStaffName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $staffId =  $row->getOwnerStaffId();
        return Mage::getModel('opentechiz_production/staff')->load($staffId)->getStaffName();
    }
}