<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_OrderId extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $incrementId = $row->getOrderId();
        $orderId = Mage::getModel('sales/order')->loadByIncrementId($incrementId)->getId();
        return '<a href="' . Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view/', array('order_id' => $orderId)) . '">' . $incrementId . '</a>';
    }
}