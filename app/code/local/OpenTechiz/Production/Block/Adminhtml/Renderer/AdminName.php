<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_AdminName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $this->_idManufacture() ? $id = $row->getAdminUserId() : $id = $row->getAdminId();

        $data = Mage::getModel('admin/user')->load($id)->getData();

        return $data['firstname'] . ' ' . $data['lastname'];
    }

    protected function _idManufacture()
    {
        if (Mage::app()->getRequest()->getControllerName() === 'manufacture') {
            return true;
        }
    }
}