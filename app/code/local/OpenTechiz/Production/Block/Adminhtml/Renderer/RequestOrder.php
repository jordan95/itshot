<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_RequestOrder extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $incrementId = $row->getData('increment_id');
        $orderId = Mage::getModel('sales/order')->loadByIncrementId($incrementId)->getId();
        $personalized_id = $row->getData('personalized_id');
        $sku = $row->getData('sku');

        $return = Mage::helper('opentechiz_return')->getReturnByProductionId($personalized_id);
        if ($return && $return_number = $return->getIncrementId()) {
            if($return->getSolution() != OpenTechiz_Return_Helper_Data::REPAIRITEM){
                return '';
            }
            $returnId = $return->getId();
            $returnIcrementId = 'Return #' .$return->getIncrementId();
            return '<a href="' . Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view/', array('order_id' => $orderId)) . '">' . $incrementId . '</a><br>(<a href="' . Mage::helper('adminhtml')->getUrl('adminhtml/sales_returns/edit/', array('id' => $returnId)) . '">' . $returnIcrementId . '</a>)';
        }


        if (strpos($sku, 'quotation') !== false) {
            $quotationProductId = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[1];
            $quotationProduct = Mage::getModel('opentechiz_quotation/product')->load($quotationProductId);
            $quotationId = 'Quotation #' . $quotationProduct->getQuoteId();
            return '<a href="' . Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view/', array('order_id' => $orderId)) . '">' . $incrementId . '</a><br>(<a href="' . Mage::helper('adminhtml')->getUrl('adminhtml/sales_quotations/detail/', array('id' => $quotationProduct->getQuoteId())) . '">' . $quotationId . '</a>)';
        }

        return '<a href="' . Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view/', array('order_id' => $orderId)) . '">' . $incrementId . '</a>';
    }
}