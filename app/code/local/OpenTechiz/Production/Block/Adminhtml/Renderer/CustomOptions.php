<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_CustomOptions extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    const DIAMOND_QUALITY = ['si', 'i', 'vs'];

    public function render(Varien_Object $row)
    {
        $html = "";
        $value = $row->getData($this->getColumn()->getIndex());
        $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $row->getSku());
        if(strpos($row->getData('sku'), OpenTechiz_Quotation_Helper_Data::QUOTATION_PRODUCT_SKU) === false) {
            $manufactureId = $row->getManufactureId();


            if($value) {
                $value = unserialize($value);
                $optionHtml = '';
                if (array_key_exists('options', $value)) {
                    foreach ($value['options'] as $option) {
                        if ($option['option_type'] == 'stone') {
                            $optionHtml .= '<dt>' . $option['label'] . '</dt>';
                            $stoneType = strtolower(str_replace(' ', '', $option['value']));

                            $quality = '';
                            if (strpos($stoneType, 'diamond')) {
                                $quality = str_replace('diamond', '', $stoneType);
                                if (in_array($quality, self::DIAMOND_QUALITY)) {
                                    $quality = strtoupper($quality);
                                } else $quality = '';
                            }
                            $optionHtml .= '<dd>' . OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_TYPE[$stoneType];
                            if ($quality != '') {
                                $optionHtml .= ' - ' . $quality;
                            }
                            $optionHtml .= '</dd>';
                        } elseif ($option['option_type'] == 'gold') {
                            $optionHtml .= '<dt>' . $option['label'] . '</dt>';
                            $optionHtml .= '<dd>' . OpenTechiz_Material_Helper_Data::GOLD_NAME_FRONT_END[strtolower(str_replace(' ', '', $option['value']))] . '</dd>';
                        } else {
                            $optionHtml .= '<dt>' . $option['label'] . '</dt>';
                            $optionHtml .= '<dd>' . $option['value'] . '</dd>';
                        }
                    }
                }
                $html .= '<dl class="item-options">
                            ' . $optionHtml . '
                        </dl>';
            }
            $html .='        </div>
                </div>';
        } else if ($row->getSku() && isset($parts[1])
            && strpos($row->getData('sku'), OpenTechiz_Quotation_Helper_Data::QUOTATION_PRODUCT_SKU) !== false){
            $quotationProductId = $parts[1];
            $optionHtml = '';

            $optionCollection = Mage::getModel('opentechiz_quotation/productOption')->getCollection()->addFieldToFilter('quote_product_id', $quotationProductId);
            foreach ($optionCollection as $option){
                if($option->getOptionType() == 'stone') {
                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
                    $optionHtml .= '<dd>' . Mage::getModel('opentechiz_quotation/product')->getStoneName($option->getOptionValue()) . '</dd>';
                } elseif ($option->getOptionType() == 'gold'){
                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
                    $optionHtml .= '<dd>' . Mage::getModel('opentechiz_quotation/product')->getGoldName($option->getOptionValue()) . '</dd>';
                } else{
                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
                    $optionHtml .= '<dd>' . $option->getOptionValue() . '</dd>';
                }
            }
            $html = '<div class="item-container">
                    <div class="item-text">
                        <dl class="item-options">
                            ' . $optionHtml . '
                        </dl>
                    </div>
                </div>';
        }
        return $html;
    }
}