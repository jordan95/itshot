<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_Manufacture_Name extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $manufactureId =  $row->getManufactureId();
        if($manufactureId){
            $manufactureEntity = Mage::getModel('opentechiz_production/production_manufacture')->load($manufactureId);
            return $manufactureEntity->getName();
        }else{
            return $row->getData($this->getColumn()->getIndex());
        }
    }
}