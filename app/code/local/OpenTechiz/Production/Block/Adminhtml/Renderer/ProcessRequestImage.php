<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_ProcessRequestImage extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $sku = $row->getData('sku');
        $productId = $row->getData('product_id');
        $_product = Mage::getModel('catalog/product')->load($productId);
        $_product->setSku($sku);
        $url = Mage::helper('catalog/image')->init($_product, 'thumbnail');

        return '<img src="' . $url . '" width="120px" height="120px">';
    }
}