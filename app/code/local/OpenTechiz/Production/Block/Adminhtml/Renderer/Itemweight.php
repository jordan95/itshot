<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_Itemweight extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $gold_weight =  $row->getData('gold_weight');
        $stone_weight =  $row->getData('stone_weight');
        $item_weight = $gold_weight + $stone_weight;
        return $item_weight?$item_weight:'0';
    }
}