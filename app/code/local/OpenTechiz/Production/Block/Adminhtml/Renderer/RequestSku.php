<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_RequestSku extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $order_item_id = $row->getData('order_item_id');
        $type = $row->getData('production_type');
         $manufactureId = $row->getManufactureId();
        if ($manufactureId) {
                $manufactureEntity = Mage::getModel('opentechiz_production/production_manufacture')->load($manufactureId);
                $sku = $manufactureEntity->getSku();
                $row->setSku($sku);
        }
        if($order_item_id&&$type==0){
            $order_item = Mage::getModel('sales/order_item')->load($order_item_id);
                $sku = $order_item->getSku();
                $row->setSku($sku);
        }

        return $row->getSku() ;
    }
}