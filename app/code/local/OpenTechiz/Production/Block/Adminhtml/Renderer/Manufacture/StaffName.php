<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_Manufacture_StaffName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $staffId =  $row->getStaffId();
        if($staffId == 0) {
            $id = $row->getAdminId();
            $data = Mage::getModel('admin/user')->load($id)->getData();
            return $data['firstname'] . ' ' . $data['lastname'];
        }
        return Mage::getModel('opentechiz_production/staff')->load($staffId)->getStaffName();
    }
}