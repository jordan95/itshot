<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_AvailableInStock extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $orderSku = $row->getSku();
        if(!$orderSku || $orderSku == ''){
            $row->setReserved('');
            return '';
        }
        if(strpos($orderSku, 'quotation') === false) {
            $results = Mage::helper('opentechiz_inventory')->getAvailableSize($orderSku);
            $html = '';
            $reservedHtml = '';
            $count = 0;
            foreach ($results as $result) {
                $html .= '<p>';
                $html .= $result['sku'] .' x '. ((int)$result['qty'] - (int)$result['on_hold']);
                $html .= '</p>';

                $count++;
                $reservedHtml .= $result['sku'] .' x '. $result['on_hold'];
                if($count < count($result) && count($result) > 1 ) {
                    $reservedHtml .= '   ';
                }
            }

            if(count($results) == 0) {
                $row->setReserved('0');
                return '0';
            }
            $row->setReserved($reservedHtml);
            return $html;
        } else return '0';
    }
}