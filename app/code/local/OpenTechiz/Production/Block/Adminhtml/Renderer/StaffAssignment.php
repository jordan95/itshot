<?php

class OpenTechiz_Production_Block_Adminhtml_Renderer_StaffAssignment extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $steps = $row->getData('production_step');
        $steps = explode(',', $steps);
        $html = '';
        foreach ($steps as $step){
            $html .= OpenTechiz_Production_Helper_Data::PROCESS_LIST[$step];
            $html .= '<br>';
        }

        return $html ;
    }
}