<?php


class Opentechiz_Production_Block_Adminhtml_Staff extends Mage_Adminhtml_Block_Widget_Grid_Container{
    public function __construct()
    {
        $this->_controller = 'adminhtml_staff';
        $this->_blockGroup = 'opentechiz_production';
        $this->_headerText = Mage::helper('opentechiz_production')->__('Manage Staffs');
        parent::__construct();
    }

}