<?php

class OpenTechiz_Production_Block_Adminhtml_GoldScrap extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_goldScrap';
        $this->_blockGroup = 'opentechiz_production';
        $this->_headerText = Mage::helper('opentechiz_production')->__('Gold Scrap');

        parent::__construct();
//        $this->_removeButton('add');
    }
}