<?php
class OpenTechiz_Production_Block_Adminhtml_GoldScrap_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $item = Mage::registry('current_gold_scrap');

        $fieldset = $form->addFieldset('gold_scrap_form', array('legend'=>Mage::helper('opentechiz_production')->__('Gold Scrap Information')));

        $fieldset->addField('type', 'select', array(
            'label'     => $this->__('Metal'),
            'name'      => 'type',
            'values'    => $this->getMetalTypes(),
            'required'	=> true,
        ));

        $fieldset->addField('qty', 'text', array(
            'label'     => $this->__('Quantity'),
            'name'      => 'qty',
            'required'	=> true,
            'class'		=> 'validate-digits',
        ));

        $fieldset->addField('note', 'textarea', array(
            'label'     => $this->__('Note'),
            'name'      => 'note',
            'required'	=> false,
        ));

        $fieldset->addField('price', 'text', array(
            'label'     => $this->__('Price'),
            'name'      => 'price',
            'required'	=> true,
            'class'		=> 'validate-digits',
        ));

    return parent::_prepareForm();
    }

    public function getMetalTypes(){
        $metal = OpenTechiz_Material_Helper_Data::GOLD_NAME_FRONT_END;
        $metal['mix'] = 'Mix';
        return array_reverse($metal);
    }
}