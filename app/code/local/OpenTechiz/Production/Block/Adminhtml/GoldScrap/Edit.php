<?php
class OpenTechiz_Production_Block_Adminhtml_GoldScrap_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected $_objectId = 'id';
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'opentechiz_production';
        $this->_controller = 'adminhtml_goldScrap';
        parent::__construct();

        $this->_updateButton('delete', 'label', 'Close ');
        $this->_removeButton('delete');

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('gold_scrap_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'gold_scrap_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'gold_scrap_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
    }

    public function getHeaderText()
    {
        return Mage::helper('opentechiz_production')->__('Content');
    }

}