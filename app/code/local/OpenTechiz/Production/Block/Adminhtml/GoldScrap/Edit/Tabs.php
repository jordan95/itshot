<?php
class OpenTechiz_Production_Block_Adminhtml_GoldScrap_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
     public function __construct()
    {
        parent::__construct();
        $this->setId('gold_scrap_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('opentechiz_production')->__('Gold Information'));
    }

    protected function _beforeToHtml()
    {
        $logBlock = $this->getLayout()->createBlock('opentechiz_production/adminhtml_goldScrap_edit_tab_log');
        $item = Mage::registry('current_gold_scrap');
        $this->addTab('form_section', array(
            'label'     => Mage::helper('opentechiz_production')->__('General'),
            'title'     => Mage::helper('opentechiz_production')->__('General'),
            'content'   => $this->getLayout()->createBlock('opentechiz_production/adminhtml_goldScrap_edit_tab_form')->toHtml(),
        ));

        if($item->getId()):

            $this->addTab('history', array(
                    'label'     => Mage::helper('opentechiz_production')->__('Log Usage'),
                    'content'   => $logBlock->setGoldCastingCode($item->getGoldCastingCode())->toHtml(),
                ));
        endif;
        return parent::_beforeToHtml();
    }
}