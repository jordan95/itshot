<?php

class OpenTechiz_Production_Block_Adminhtml_GoldScrap_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('goldScrapGrid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_production/goldScrap')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('opentechiz_production')->__('ID #'),
            'index'     => 'id',
        ));
        $this->addColumn('type', array(
            'header'    => Mage::helper('opentechiz_production')->__('Metal'),
            'index'     => 'type',
            'type'      => 'options',
            'options'   => $this->getMetalTypes()
        ));
        $this->addColumn('qty', array(
            'header'    => Mage::helper('opentechiz_production')->__('Quantity (gr)'),
            'index'     => 'qty',
        ));
        $this->addColumn('note', array(
            'header'    => Mage::helper('opentechiz_production')->__('Note'),
            'index'     => 'note',
        ));
        $this->addColumn('price', array(
            'header'    => Mage::helper('opentechiz_production')->__('Sold Price'),
            'index'     => 'price',
            'width'     =>  '50'
        ));
        $this->addColumn('created_at', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Created At'),
            'type'      => 'datetime',
            'index'     =>  'created_at'
        ));
        $this->addColumn('updated_at', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Updated At'),
            'type'      => 'datetime',
            'index'     =>  'updated_at'
        ));

        return parent::_prepareColumns();
    }

    public function getMetalTypes(){
        $metal = OpenTechiz_Material_Helper_Data::GOLD_NAME_FRONT_END;
        $metal['mix'] = 'Mix';
        return array_reverse($metal);
    }

    protected function _prepareMassaction()
    {

    }
}