<?php

class OpenTechiz_Production_Block_Adminhtml_Request_Manufacture extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_request_product';
    protected $_blockGroup = 'opentechiz_production';
    protected $_headerText = 'Create production request for item to stock';

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Create production request for item to stock');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function getFormAction(){
        return Mage::helper("adminhtml")->getUrl('adminhtml/request_product/manufacture/');
    }

    public function getAjaxAction(){
        return $this->getUrl('adminhtml/request_product/ajax/');
    }
}