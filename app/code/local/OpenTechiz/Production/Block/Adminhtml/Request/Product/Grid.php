<?php

class OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('productGrid');
        $this->setDefaultSort('personalized_id');
        $this->setNoFilterMassactionColumn(true);
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_production/product')->getCollection();
        $collection->getSelect()->joinLeft(array("order_item" => 'tsht_sales_flat_order_item'),
            'main_table.order_item_id=order_item.item_id and main_table.personalized_id > 0',
            array())
            ->joinLeft(array("order" => 'tsht_sales_flat_order'),
                'order_item.order_id=order.entity_id',
                array('order_created_at' => 'order.created_at',
                    'production_process_status' => 'main_table.status',
                    'ship_by' => 'order.ship_by',
                    'source' => 'order.source',
                    'deliver_by' => 'order.deliver_by',
                    'order_status' => 'order.status',
                    'request_created_at' => 'main_table.created_at',
                    'increment_id' => 'increment_id',
                    'item_id' => 'order_item.item_id'))
            ->joinLeft(array("item" => 'tsht_inventory_item'),
                'main_table.barcode=item.barcode',
                array('sku' => 'item.sku',
                    'name' => 'item.name',
                    'item.barcode' => 'item.barcode'));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('personalized_id', array(
            'header' => Mage::helper('opentechiz_production')->__('Personalized #'),
            'index' => 'personalized_id',
            'width' => '50',
        ));
        $this->addColumn('image', array(
            'header' => Mage::helper('opentechiz_production')->__('Gallery Image'),
            'index' => 'image',
            'sortable' => false,
            'filter' => false,
            'renderer' => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Image'
        ));
        $this->addColumn('option', array(
            'header' => Mage::helper('opentechiz_production')->__('Options Information'),
            'index' => 'option',
            'width' => '500',
            'sortable' => false,
            'renderer' => 'OpenTechiz_Production_Block_Adminhtml_Renderer_ProductionCustomOptions'
        ));
        $this->addColumn('increment_id', array(
            'header' => Mage::helper('opentechiz_production')->__('Order #'),
            'index' => 'increment_id',
            'width' => '100',
            'renderer' => 'OpenTechiz_Production_Block_Adminhtml_Renderer_RequestOrder'
        ));
        $this->addColumn('order_item_id', array(
            'header' => Mage::helper('opentechiz_production')->__('Order Item #'),
            'index' => 'order_item_id',
            'width' => '100',
            'renderer' => 'OpenTechiz_Production_Block_Adminhtml_Renderer_OrderItemId'
        ));
        $this->addColumn('manufacture_id', array(
            'header' => Mage::helper('opentechiz_production')->__('Manufacture #'),
            'index' => 'manufacture_id',
            'width' => '100',
            'renderer' => 'OpenTechiz_Production_Block_Adminhtml_Renderer_ManufactureId'
        ));
        $this->addColumn('barcode', array(
            'header' => Mage::helper('opentechiz_production')->__('Barcode #'),
            'index' => 'barcode',
            'filter_index'=>'main_table.barcode',
            'renderer' => 'OpenTechiz_Production_Block_Adminhtml_Renderer_Barcode'
        ));
        $this->addColumn('production_type', array(
            'header' => Mage::helper('opentechiz_production')->__('Production Type'),
            'index' => 'production_type',
            'type' => 'options',
            'options' => OpenTechiz_Production_Helper_Data::OPPENTECHIZ_PRODUCTION_PRODUCTION_TYPE
        ));
        $this->addColumn('request_created_at', array(
            'header' => Mage::helper('opentechiz_production')->__('Production Requested At'),
            'type' => 'datetime',
            'index' => 'request_created_at',
            'filter_index'=>'main_table.created_at'
        ));
        $this->addColumn('order_created_at', array(
            'header' => Mage::helper('opentechiz_production')->__('Order Date'),
            'type' => 'datetime',
            'index' => 'order_created_at',
            'filter_index'=>'order.created_at'
        ));
        $this->addColumn('source', array(
            'header' => Mage::helper('opentechiz_production')->__('Order Source'),
            'index' => 'source',
            'type' => 'options',
            'options' => OpenTechiz_ReportExtend_Helper_Data::ORDER_SOURCE
        ));
        $this->addColumn('ship_by', array(
            'header' => Mage::helper('opentechiz_production')->__('Ship By'),
            'type' => 'date',
            'index' => 'ship_by',
        ));
        $this->addColumn('deliver_by', array(
            'header' => Mage::helper('opentechiz_production')->__('Deliver By'),
            'type' => 'date',
            'index' => 'deliver_by',
        ));
        $this->addColumn('order_status', array(
            'header' => Mage::helper('opentechiz_production')->__('Order Status'),
            'width' => '100',
            'index' => 'order_status',
            'align' => 'center',
            'filter_index' => 'order.status',
            'type' => 'options',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));
        $this->addColumn('sku', array(
            'header' => Mage::helper('opentechiz_production')->__('Item SKU'),
            'index' => 'sku',
            'width' => '100',
            'filter' => false,
            'renderer' => 'OpenTechiz_Production_Block_Adminhtml_Renderer_RequestSku'
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('opentechiz_production')->__('Product Name'),
            'index' => 'name',
            'filter_index' => 'item.name'
        ));
        $this->addColumn('item_state', array(
            'header' => Mage::helper('opentechiz_production')->__('State'),
            'width' => '100',
            'index' => 'item_state',
            'align' => 'center',
            'type' => 'options',
            'options' => OpenTechiz_Production_Helper_Data::OPPENTECHIZ_PRODUCTION_STATE,
        ));

        $this->addColumn('production_process_status', array(
            'header' => Mage::helper('opentechiz_production')->__('Process Status'),
            'width' => '100',
            'index' => 'production_process_status',
            'filter_index'=>'main_table.status',
            'align' => 'center',
            'type' => 'options',
            'options' => OpenTechiz_Production_Helper_Data::PROCESS_LIST
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('item_id');
        $this->getMassactionBlock()->setFormFieldName('item');
        $this->getMassactionBlock()->setUseSelectAll(false);

        // MassPrintBarcode
        $this->getMassactionBlock()->addItem('print_barcode', array(
            'label' => Mage::helper('opentechiz_production')->__('Print Barcode'),
            'url' => $this->getUrl('*/*/printbarcode')
        ));

        // MassPrintProductionPDF
        $this->getMassactionBlock()->addItem('print_production_pdf', array(
            'label' => Mage::helper('opentechiz_production')->__('Print Production PDF'),
            'url' => $this->getUrl('*/*/printPDF')
        ));
        return $this;
    }
}