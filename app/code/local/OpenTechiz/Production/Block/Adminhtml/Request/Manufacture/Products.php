<?php

class OpenTechiz_Production_Block_Adminhtml_Request_Manufacture_Products extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('productGrid');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
    }

    public function getGridUrl()
    {
        return $this->getUrl('adminhtml/manufacture/productGrid', array('_current'=>true));
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('name');;
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('ID'),
            'width'     => '10%',
            'index'     => 'entity_id',
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('opentechiz_production')->__('Product Name'),
            'index'     => 'name',
            'width'     =>  '600'
        ));
        $this->addColumn('sku', array(
            'header'    => Mage::helper('opentechiz_production')->__('SKU'),
            'index'     => 'sku',
            'width'     =>  '50',
        ));
        $this->addColumn('action', array(
            'header'    => Mage::helper('opentechiz_production')->__('Select'),
            'width'     => '50',
            'renderer'  => 'OpenTechiz_Production_Block_Adminhtml_Renderer_ManufactureProductSelect',
            'filter'    => false,
            'sortable'  => false,
            'is_system' => true,
        ));
        return parent::_prepareColumns();
    }
}