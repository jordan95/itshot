<?php

class OpenTechiz_Production_Block_Adminhtml_Request_Process extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_request_process';
        $this->_blockGroup = 'opentechiz_production';
        $this->_headerText = Mage::helper('opentechiz_production')->__('Product Process Requests');
        
        parent::__construct();
        $this->_removeButton('add');
    }
}