<?php

class OpenTechiz_Production_Block_Adminhtml_Request_Product extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_request_product';
        $this->_blockGroup = 'opentechiz_production';
        $this->_headerText = Mage::helper('opentechiz_production')->__('Manage Production Requests');
        $this->_addButtonLabel = Mage::helper('opentechiz_production')->__('Create for instock items');
        parent::__construct();
    }
}