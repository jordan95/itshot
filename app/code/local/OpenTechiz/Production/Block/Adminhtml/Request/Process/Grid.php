<?php

class OpenTechiz_Production_Block_Adminhtml_Request_Process_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('productProcessGrid');
        $this->setNoFilterMassactionColumn(true);
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_production/process_request')->getCollection();
        $collection -> getSelect() -> joinLeft(array("order_item"=>'tsht_sales_flat_order_item'),
            'main_table.order_item_id=order_item.item_id',
            '*')-> joinLeft(array("order"=>'tsht_sales_flat_order'),
            'main_table.order_id=order.entity_id',
            array('status' => 'main_table.status',
                'order_id' => 'order.increment_id',
                'product_id' => 'order_item.product_id',
                'ship_by' => 'order.ship_by',
                'deliver_by' => 'order.deliver_by',
                'created_at' => 'main_table.created_at'));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('sku', array(
            'header'    => Mage::helper('opentechiz_production')->__('SKU'),
            'index'     => 'sku',
            'width'     =>  '100',
        ));
        $this->addColumn('image', array(
            'header'    => Mage::helper('opentechiz_production')->__('Gallery Image'),
            'index'     => 'product_id',
            'sortable'  => false,
            'filter'    => false,
//            'renderer'  => 'OpenTechiz_Production_Block_Adminhtml_Renderer_ProcessRequestImage'
            'renderer' => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_Image'
        ));
        $this->addColumn('name', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Product Name'),
            'index'     =>  'name',
        ));
        $this->addColumn('option', array(
            'header'    => Mage::helper('opentechiz_production')->__('Options Information'),
            'index'     => 'product_options',
            'width'     =>  '500',
            'sortable'  => false,
            'renderer'  => 'OpenTechiz_Production_Block_Adminhtml_Renderer_CustomOptions'
        ));
        $this->addColumn('increment_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Order #'),
            'index'     => 'order_id',
            'width'     =>  '100',
            'sort_index'=> 'order.increment_id',
            'filter_index'=> 'order.increment_id',
            'renderer'  => 'OpenTechiz_Production_Block_Adminhtml_Renderer_OrderId'
        ));
        $this->addColumn('request_created_at', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Requested At'),
            'type'      => 'datetime',
            'index'     =>  'created_at',
            'sort_index'=>  'main_table.created_at',
            'filter_index' => 'main_table.created_at'
        ));
        $this->addColumn('ship_by', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Ship By'),
            'type'      => 'date',
            'index'     => 'ship_by',
        ));
        $this->addColumn('deliver_by', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Deliver By'),
            'type'      => 'date',
            'index'     => 'deliver_by',
        ));
        
        $this->addColumn('available', array(
            'header'    => Mage::helper('opentechiz_production')->__('Available in Stock'),
            'filter'    => false,
            'width'     => '200px',
            'sortable'  => false,
            'renderer'  => 'OpenTechiz_Production_Block_Adminhtml_Renderer_AvailableInStock'
        ));
        $this->addColumn('reserved', array(
            'header'    => Mage::helper('opentechiz_production')->__('Reserved'),
            'filter'    => false,
            'width'     => '200px',
            'sortable'  => false,
            'index'     => 'reserved'
        ));
        $this->addColumn('status', array(
            'header'    => Mage::helper('opentechiz_production')->__('Status'),
            'width'     => '100',
            'index'     => 'status',
            'filter_index'=>'main_table.status',
            'align'     => 'center',
            'type'      => 'options',
            'options'   => OpenTechiz_Production_Helper_Data::PRODUCT_PROCESS_REQUEST_STATUS,
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('ids');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

        // Mass Request Production
        //#1621 Engraving process improvement 1 - remove Production Request option
//        $this->getMassactionBlock()->addItem('production_request', array(
//            'label'=> Mage::helper('opentechiz_production')->__('Request Production'),
//            'url'      => $this->getUrl('*/*/productionRequest')
//        ));
        // Mass Request Product From Stock
        $this->getMassactionBlock()->addItem('stock_request', array(
            'label'=> Mage::helper('opentechiz_production')->__('Request Product from Stock'),
            'url'      => $this->getUrl('*/*/stockRequest')
        ));
//        // MassDelete
//        $this->getMassactionBlock()->addItem('production_request', array(
//            'label'=> Mage::helper('opentechiz_production')->__('Add Purchase Order '),
//            'url'      => $this->getUrl('*/*/printbarcode')
//        ));
        return $this;
    }
}