<?php

class OpenTechiz_Production_Block_Adminhtml_ProcessHistory extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_processHistory';
        $this->_blockGroup = 'opentechiz_production';
        $this->_headerText = Mage::helper('opentechiz_production')->__('Production Process History');
        parent::__construct();

        // must be placed after the parent::__construct();
        $this->_removeButton('add');
    }
}