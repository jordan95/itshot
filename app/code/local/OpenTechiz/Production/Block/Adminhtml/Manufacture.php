<?php

class OpenTechiz_Production_Block_Adminhtml_Manufacture extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected  $_controller = 'adminhtml_manufacture';
    protected $_blockGroup = 'opentechiz_production';
    protected $_headerText = 'Manufacture';

    public function __construct()
    {
        parent::__construct();
    }

    public function getCreateUrl()
    {
        return $this->getUrl('adminhtml/request_product/new');
    }

    protected function getAddButtonLabel()
    {
        return Mage::helper('opentechiz_production')->__('Add new manufacture request');
    }
}