<?php
/**
 * Created by PhpStorm.
 * Date: 20-09-2017
 * Time: 16:43
 */

class OpenTechiz_Production_Block_Adminhtml_Manage_Brokenstone_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('manage_broken_stone_grid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('opentechiz_production/brokenstone')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('barcode', array(
            'header' => Mage::helper('opentechiz_production')->__("Barcode"),
            'index' => 'barcode',
            'type' => 'text',
        ));

        $this->addColumn('stone_name', array(
            'header' => Mage::helper('opentechiz_production')->__("Stone Name"),
            'index' => 'stone_name',
            'type' => 'text',
        ));

//        $this->addColumn('stone_detail', array(
//            'header' => Mage::helper('opentechiz_production')->__("Stone Detail"),
//            'index' => 'stone_detail',
//            'type' => 'text',
//            'sortable' => false,
//            'filter' => false,
////            'renderer' => 'OnlineBiz_opentechiz_production_Block_Adminhtml_Grid_Renderer_Stonedetail',
//        ));

        $this->addColumn('broken_qty', array(
            'header' => Mage::helper('opentechiz_production')->__("Broken Qty"),
            'index' => 'broken_qty',
            'type' => 'number'
        ));

        $this->addColumn('broken_reason', array(
            'header' => Mage::helper('opentechiz_production')->__("Broken Reason"),
            'index' => 'broken_reason',
            'type' => 'options',
            'options' => Mage::helper('opentechiz_production')->getListBrokenReasons()
        ));

        $this->addColumn('staff', array(
            'header' => Mage::helper('opentechiz_production')->__("Staff"),
            'index' => 'staff',
            'type' => 'options',
            'options' => Mage::helper('opentechiz_production')->getListStaffStoneSetting()
        ));

        $this->addColumn('created_at',array(
            'header' => Mage::helper('opentechiz_production')->__('Created At'),
            'type'   => 'date',
            'index'  => 'created_at'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

//    public function getRowUrl($row) {
//        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
//    }
}