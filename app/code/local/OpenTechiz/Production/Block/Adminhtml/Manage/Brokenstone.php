<?php
/**
 * Created by PhpStorm.
 * Date: 20-09-2017
 * Time: 16:28
 */

class OpenTechiz_Production_Block_Adminhtml_Manage_Brokenstone extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'opentechiz_production';
        $this->_controller = 'adminhtml_manage_brokenstone';
        $this->_headerText = Mage::helper('opentechiz_production')->__('Manage Broken Stones');

        parent::__construct();

    }
}