<?php

class OpenTechiz_Production_Block_Adminhtml_Manage_Goldcasting_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * OpenTechiz_Production_Block_Adminhtml_Request_Product_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('goldcastingGrid');
        $this->setTemplate('opentechiz/production/grid.phtml');
        $this->setDefaultSort('gold_casting_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_production/goldcasting')->getCollection()
        ;
        $collection->joinGetGoldUsage();
        $column = new Zend_Db_Expr("main_table.weight - ifnull(usage.total_weight_usage, 0) - ifnull(main_table.weight_scrap, 0)*if(main_table.ring_type=1,0,1) - ifnull(main_table.leftover_weight,0)*if(main_table.ring_type=1,1,0)");
        $collection->getSelect()->columns(array('weight_remain'=>$column));
        /*echo (string)$collection->getSelect();
        die();*/
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('gold_casting_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('ID'),
            'index'     => 'gold_casting_id'
        ));
        $this->addColumn('gold_casting_code', array(
            'header'    => Mage::helper('opentechiz_production')->__('Gold Casting Code'),
            'index'     => 'gold_casting_code',
            'filter_index' => 'main_table.gold_casting_code'
        ));
        $this->addColumn('gold_bar_serial_no', array(
            'header'    => Mage::helper('opentechiz_production')->__('Gold bar serial no #'),
            'index'     => 'gold_bar_serial_no',
            'width'     =>  '50',
        ));

        $this->addColumn('gold_bar_serial_no2', array(
            'header'    => Mage::helper('opentechiz_production')->__('Second Gold bar serial no'),
            'index'     => 'gold_bar_serial_no2',
            'width'     =>  '100'
        ));
        $this->addColumn('gold_bar_weight_2', array(
            'header'    => Mage::helper('opentechiz_production')->__('Second Gold bar weight'),
            'index'     => 'gold_bar_weight_2',
        ));

        $this->addColumn('weight', array(
            'header'    => Mage::helper('opentechiz_production')->__('Weight'),
            'index'     => 'weight'
        ));
         $this->addColumn('gold_type', array(
            'header'    => Mage::helper('opentechiz_production')->__('Gold Type'),
            'index'     => 'gold_type',
             'filter_index'     => 'gold_table.gold_type',
             'type' => 'options',
            'options' => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_TYPE
        ));
            $this->addColumn('gold_color', array(
            'header'    => Mage::helper('opentechiz_production')->__('Gold Color'),
            'index'     => 'gold_color',
             'filter_index'     => 'gold_table.gold_color',
             'type' => 'options',
            'options' => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_COLOR
        ));
         
        $this->addColumn('pipe_weight', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Pipe weight'),
            'index'     =>  'pipe_weight',
        ));
        $this->addColumn('weight_usage', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Weight use'),
            'index'     =>  'weight_usage',
        ));
        $this->addColumn('weight_scrap', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Weight scap'),
            'index'     =>  'weight_scrap',
        ));
        $this->addColumn('leftover_weight', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Leftover weight'),
            'index'     =>  'leftover_weight',
        ));
        $this->addColumn('weight_remain', array(
            'header' => Mage::helper('opentechiz_production')->__('Weight remaining (theory)'),
            'index' => 'weight_remain',
            'filter_index'=> new Zend_Db_Expr("main_table.weight - ifnull(usage.total_weight_usage, 0) - ifnull(main_table.weight_scrap, 0)*if(main_table.ring_type=1,0,1) - ifnull(main_table.leftover_weight,0)*if(main_table.ring_type=1,1,0)")
        ));
        $this->addColumn('ring_type', array(
            'header'    => Mage::helper('opentechiz_production')->__('Ring Type'),
            'index'     => 'ring_type',
            'type'      => 'options',
            'options'   => OpenTechiz_Production_Helper_Data::OPPENTECHIZ_RING_TYPE ));
        $this->addColumn('created_at', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Created At'),
            'type'      => 'datetime',
            'index'     =>  'created_at',
        ));
        $this->addColumn('updated_at', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Updated At'),
            'type'      => 'datetime',
            'index'     =>  'updated_at',
        ));
        $this->addColumn('status', array(
            'header'    => Mage::helper('opentechiz_production')->__('Status'),
            'width'     => '100',
            'index'     => 'status',
            'align'     =>  'center',
            'type'      => 'options',
            'options'   => OpenTechiz_Production_Helper_Data::OPPENTECHIZ_GOLD_CASTING_STATUS 
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('admin/erp/production/goldcastings/goldcastingedit')) {
            $this->addColumn('action',
                array(
                    'header' => Mage::helper('opentechiz_production')->__('Action'),
                    'width' => '50',
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('opentechiz_production')->__('Edit'),
                            'url' => array('base' => '*/*/edit'),
                            'field' => 'id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'is_system' => true,
                    'align' => 'center'
                ));
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this|Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('gold_casting_id');
        $this->getMassactionBlock()->setFormFieldName('gold_casting_id');
        $this->getMassactionBlock()->setUseSelectAll(false);
        $status = OpenTechiz_Production_Helper_Data::OPPENTECHIZ_GOLD_CASTING_STATUS ;

        // Change status
        $this->getMassactionBlock()->addItem('status', array(
            'label'=> Mage::helper('opentechiz_material')->__('Change status'),
            'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('opentechiz_material')->__('Status'),
                    'values' => $status
                )
            )
        ));

        return $this;
    }

    public function getAdditionsHtml()
    {
        $read = Mage::getSingleton('core/resource')->getConnection('core_read');
        $collection = $this->getCollection();
        $select = $collection->getSelect();
        $ret  = clone $select;
        $ret->reset(Zend_Db_Select::COLUMNS);
        $ret->reset(Zend_Db_Select::ORDER);
        $ret->reset(Zend_Db_Select::LIMIT_COUNT);
        $ret->reset(Zend_Db_Select::LIMIT_OFFSET);
        $ret->columns(array(
            'total_weight'=>"sum(ifnull(main_table.weight, 0))",
            'total_usage'=>"sum(ifnull(usage.total_weight_usage, 0))",
            "total_scrap"=>"sum(ifnull(main_table.weight_scrap, 0))",
            "total_remaining"=> "sum(main_table.weight) - sum(ifnull(usage.total_weight_usage, 0)) - sum(ifnull(main_table.weight_scrap, 0)*if(main_table.ring_type=1,0,1)) - sum(ifnull(main_table.leftover_weight,0)*if(main_table.ring_type=1,1,0))",
            'total_leftover'=>"sum(ifnull(main_table.leftover_weight, 0))",
            'total_vestigial'=>"sum(ifnull(main_table.weight_scrap, 0)) - sum(ifnull(main_table.leftover_weight, 0))"));
        $htmlRp = '';
        if($data = $read->fetchAssoc($ret)):
            $htmlRp .= '<div class="grid"><table class="report data"><tr class="headings"><th>'.$this->__('Total weight (gr)') .'</th><th>'.$this->__('Total weight usage (gr)') .'</th><th>' . $this->__('Total weight scrap (gr)'). '</th><th>' . $this->__('Total weight remaining (gr)'). '</th><th>' . $this->__('Total Leftover (gr)'). '</th></tr>';
            //sales total - cost total = earned money
            foreach($data as $values)
            {
                $htmlRp .= '<tr><td>' . $values['total_weight'] . '</td>';
                $htmlRp .= '<td>' . $values['total_usage'] . '</td>';
                $htmlRp .= '<td>' . $values['total_scrap'] . '</td>';
                $htmlRp .= '<td>' . $values['total_remaining'] . '</td>';
                $htmlRp .= '<td>' . $values['total_leftover'] . '</td></tr>';
               
            }
            $htmlRp .= '</table></div>';
        endif;
        $html = $htmlRp;
        return $html;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}