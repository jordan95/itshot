<?php
/**
 * Created by PhpStorm.
 * Date: 21-09-2017
 * Time: 14:53
 */

class OpenTechiz_Production_Block_Adminhtml_Manage_Brokenstone_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('id'=>$this->getRequest()->getParam('id'))),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}