<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Order history tab
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class OpenTechiz_Production_Block_Adminhtml_Manage_Goldcasting_Edit_Tab_Log
    extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
    {
        parent::__construct();
        $this->setId('manage_gold_log_grid');
		$this->setFilterVisibility(false);
        $this->setPagerVisibility(false);
		$this->setCountTotals(true);
        $this->setSaveParametersInSession(true);
		$this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
    }
	public function getCountTotals()
    {
		$read = Mage::getSingleton('core/resource')->getConnection('core_read');
        if (!$this->getTotals()) {
            $collection = $this->getCollection();
			if (count($collection->getSize()) < 1) {
                $this->setTotals(new Varien_Object());
            } else {
				$ret = $collection->getSelect();
				$ret->reset(Zend_Db_Select::COLUMNS);
				$ret->reset(Zend_Db_Select::LIMIT_COUNT);
				$ret->reset(Zend_Db_Select::LIMIT_OFFSET);
				//$ret->columns(array('increment_id' => "concat('" .$this->_('Total'). "')"));
				$ret->columns(array('weight_usage' => "sum(main_table.weight_usage)"));
				$ret->columns(array('current_weight' => "gold.weight - sum(main_table.weight_usage) - gold.weight_scrap"));
				$ret->columns(array('gold_weight' => "gold.weight"));
				$ret->columns(array('scrap' => "gold.weight_scrap"));
				$data = $read->fetchAssoc($ret);
				foreach($data as $item)
				{
					$item['increment_id'] = $this->__('Total');
					$this->setCurrentWeight($item['current_weight']);
					$this->setUseWeight($item['weight_usage']);
					$this->setScrapWeight($item['scrap']);
					$this->setGoldWeight($item['gold_weight']);
					$items = new Varien_Object();
					$items->addData($item);
					$this->setTotals($items);
				}
            }
        }
        return parent::getCountTotals();
    }

	protected function _prepareCollection()
    {
		$read = Mage::getSingleton('core/resource')->getConnection('core_read');
		$collection = Mage::getModel('opentechiz_production/goldusage')->getCollection();
		if($code = Mage::registry('current_gold_casting')->getGoldCastingCode())
		{
			$collection->addFieldToFilter('main_table.gold_casting_code', $code);
		}
        

		$collection->joinGold(array('scrap'=>'gold.weight_scrap'));
		$this->setCollection($collection);
        return parent::_prepareCollection();
    }
   
    protected function _prepareColumns()
    {
		
		$this->addColumn('item_id', array(
            'header'=>Mage::helper('opentechiz_production')->__('Personalized item # '),
            'index'  => 'personalized_id',
            'filter_index' => 'main_table.personalized_id',
			'sortable'      => false,
			'totals_label'  => Mage::helper('sales')->__(''),
        ));
	
		$this->addColumn('weight_usage', array(
            'header'=> Mage::helper('opentechiz_production')->__('Weight Usage'),
            'index' => 'weight_usage',
			'sortable'      => false,
        ));
	
		$this->addColumn('created_at', array(
            'header' => Mage::helper('opentechiz_production')->__('Created At'),
            'index' => 'created_at',
			'sortable'      => false,
            'filter_index' => 'main_table.created_at',
			'type' => 'datetime'
        ));
		$this->addColumn('user_name', array(
            'header'=> Mage::helper('opentechiz_production')->__('By User'),
            'index' => 'user_name',
			'sortable'      => false,
			'totals_label'  => Mage::helper('sales')->__('')
        ));
		return parent::_prepareColumns();
    }
	public function getGridUrl()
    {
        return $this->getUrl('*/*/gridlog', array('_current'=>true, 'code'=>$this->getCode()));
    }
	protected function _toHtml()
    {
        $html = parent::_toHtml();
        if($this->getGoldWeight() > 0)
		{
			$html .= '<div><span style="color:green;">'.$this->__('Gold casting Weight: %s Gramm', $this->getGoldWeight()).'</span></div>';
		}
		elseif($this->getGoldWeight() <= 0)
		{
			$html .= '<div><span style="color:red;">'.$this->__('Gold casting Weight: %s Gramm', $this->getGoldWeight()).'</span></div>';
		}
		if($this->getUseWeight() > 0)
		{
			$html .= '<div><span style="color:green;">'.$this->__('Weight usage: %s Gramm', $this->getUseWeight()).'</span></div>';
		}
		elseif($this->getUseWeight() <= 0)
		{
			$html .= '<div><span style="color:red;">'.$this->__('Weight usage: %s Gramm', $this->getUseWeight()).'</span></div>';
		}

		
		if($this->getScrapWeight())
		{
			$html .= '<div><span style="color:green;">'.$this->__('Weight scrap: %s Gramm', $this->getScrapWeight()).'</span></div>';
		}
		
		if($this->getCurrentWeight() > 0)
		{
			$html .= '<div><span style="color:green;">'.$this->__('Weight remaining: %s Gramm', $this->getCurrentWeight()).'</span></div>';
		}
		elseif($this->getCurrentWeight() <= 0)
		{
			$html .= '<div><span style="color:red;">'.$this->__('Weight remaining: %s Gramm', $this->getCurrentWeight()).'</span></div>';
		}
		
		
		return $html;
    }

	protected function _preparePage()
    {
        return false;
    }
}
