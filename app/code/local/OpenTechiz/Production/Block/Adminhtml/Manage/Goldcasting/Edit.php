<?php
class OpenTechiz_Production_Block_Adminhtml_Manage_Goldcasting_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
 protected $_objectId = 'id';
public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'opentechiz_production';
        $this->_controller = 'adminhtml_manage_goldcasting';
        parent::__construct();

       $this->_updateButton('delete', 'label', 'Close ');
    if (!Mage::getSingleton('admin/session')->isAllowed('admin/erp/production/goldcastings/goldcastingdelete')) {
        $this->_removeButton('delete');
    }

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('gold_casting_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'gold_casting_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'gold_casting_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
    }

    public function getHeaderText()
    {
        return Mage::helper('opentechiz_production')->__('Content');
    }

}