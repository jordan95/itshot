<?php
/**
 * Created by PhpStorm.
 * Date: 21-09-2017
 * Time: 14:52
 */

class OpenTechiz_Production_Block_Adminhtml_Manage_Brokenstone_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'opentechiz_production';
        $this->_controller = 'adminhtml_manage_brokenstone';

        $this->_removeButton('delete');
    }

    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    public function getHeaderText()
    {
        return Mage::helper('opentechiz_production')->__('Content');
    }
}