<?php
/**
 * Created by PhpStorm.
 * Date: 21-09-2017
 * Time: 14:53
 */

class OpenTechiz_Production_Block_Adminhtml_Manage_Brokenstone_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('brokenstone_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('opentechiz_production')->__('Tabs'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form', array(
            'label'     => Mage::helper('catalog')->__('Setting'),
            'title'       =>  Mage::helper('catalog')->__('Setting'),
            'content'   => $this->getLayout()->createBlock('opentechiz_production/adminhtml_manage_brokenstone_edit_tab_form', 'broken.stone')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}