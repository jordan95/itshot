<?php
/**
 * Created by PhpStorm.
 * Date: 21-09-2017
 * Time: 14:53
 */

class OpenTechiz_Production_Block_Adminhtml_Manage_Brokenstone_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $helper = Mage::helper('opentechiz_production');
        $fieldset = $form->addFieldset('brokenstone_form',array('legend' => $helper->__("Information")));

        $url = Mage::helper('adminhtml')->getUrl('adminhtml/manage_brokenstone/getDatastoneBroken');

        $fieldset->addField('barcode', 'text', array(
            'label'     => $helper->__('Barcode'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'barcode',
            'onchange'  => "getDatastoneBroken(this,'{$url}')",
            'after_element_html' => '<small id="error-barcode" style="color: red; display: none"></small><script>jQuery.noConflict()</script>',
        ));

        
        if (Mage::getSingleton('adminhtml/session')->getBrokenstoneData() || Mage::registry('current_brokenstone')->getData()) {
            $fieldset->addField('stone_name', 'text', array(
                'label'     => $helper->__('Stone Name'),
                'class'     => 'required-entry',
                'required'  => true,
                'name'      => 'stone_name',
            ));
        }else{
            $fieldset->addField('stone_name', 'select', array(
                'label'     => $helper->__('Stone Name'),
                'class'     => 'required-entry',
                'required'  => true,
                'name'      => 'stone_name',
//                'after_element_html' => '<input style="display:none;" disabled="disabled" id="stone_name_custom" name="stone_name_custom" value="" class="required-entry input-text" type="text">',
            ));

//            $fieldset->addField('custom_stone_name', 'checkbox', array(
//                'label'     => $helper->__('Custom Stone Name'),
//                'checked'    => '',
//            ));
        }
        

        $fieldset->addField('broken_qty', 'text', array(
            'label'     => $helper->__('Broken Qty'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'broken_qty',
        ));

        $fieldset->addField('broken_reason', 'select', array(
            'label'     => $helper->__('Broken Reason'),
            'name'      => 'broken_reason',
            'values' => $helper->getListBrokenReasons(),
        ));

        $fieldset->addField('staff', 'select', array(
            'label'     => $helper->__('Staff'),
            'name'      => 'staff',
            'values' => $helper->getListStaffStoneSetting(),
        ));

        $fieldset->addField('comment', 'textarea', array(
            'label'     => $helper->__('Comment'),
            'name'      => 'comment',
        ));

        if ( Mage::getSingleton('adminhtml/session')->getBrokenstoneData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getBrokenstoneData());
            Mage::getSingleton('adminhtml/session')->setBrokenstoneData(null);
        } elseif ( Mage::registry('current_brokenstone') ) {
            $form->setValues(Mage::registry('current_brokenstone')->getData());
        }
        return parent::_prepareForm();
    }
}