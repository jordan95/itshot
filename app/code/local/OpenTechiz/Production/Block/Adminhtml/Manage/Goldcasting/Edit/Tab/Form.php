<?php
class OpenTechiz_Production_Block_Adminhtml_Manage_Goldcasting_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
 /**
 * prepare tab form's information
 *
 * @return Tutorialmagento_Salestaff_Block_Adminhtml_Salestaff_Edit_Tab_Form
 */
 protected function _prepareForm()
 {
        $form = new Varien_Data_Form();
        $this->setForm($form);

 		/*Edit field as text type*/
        $disabled = false;
		$item = Mage::registry('current_gold_casting');
        if(!$item->isCanEdit())
		  $disabled = true;
		$fieldset = $form->addFieldset('gold_casting_form', array('legend'=>Mage::helper('opentechiz_production')->__('Gold casting information')));

		/*if(!$item->getGoldCode())
		{
			$item->setGoldCode($this->generateGoldCode());
		}*/
        if($item->getId()) {
            $fieldset->addField('gold_status', 'note', array(
                'label'     => $this->__('Status'),
                'text'     => $this->_getGoldStatus()
            ));

        }
        $ringtypeField = $fieldset->addField('ring_type', 'radios', array(
            'label' => $this->__('Ring Type'),
            'name' => 'ring_type',
            'onclick' => "",
            'onchange' => "",
            //'required' => true,
            'values' => array(
                array('value'=>'1','label'=>'Diamond Ring'),
                array('value'=>'2','label'=>'Wedding Ring'),
            ),
            'disabled' => $disabled,
            'readonly' => false,
            'tabindex' => 1,
            'class' => "ring_type validate-one-required-by-name"
        ));
        if(!$item->getId()) {
            $ringtypeField->setAfterElementHtml('<script>
            //<![CDATA[
            	jQuery.noConflict();
             jQuery(document).ready(function() { 
                 jQuery("#pipe_weight").parent().parent()[0].hide();
                            jQuery("#weight_scrap").parent().parent()[0].hide();  
                $$(".ring_type").each(function(curInput) {
                    Event.observe(curInput, "change", function() {
                        var ringTypeValue = jQuery(this).val();
                        var numberOfDiamondRings = parseInt(jQuery("#number_of_diamond_ring").val()) + 1;
                        var numberOfWeddingRings = parseInt(jQuery("#number_of_wedding_ring").val()) + 1;
                        var today = new Date();
                        var date = today.getDate();
                        if(date < 10) date = "0" + date;
                        var month = today.getMonth() + 1;
                        if(month < 10) month = "0" + month;
                        var year = today.getFullYear();
                        
                        if(ringTypeValue == 2) {
                            var code = date.toString() + month.toString() + year.toString() + "BORU" + numberOfWeddingRings;
                            jQuery("#pipe_weight").parent().parent()[0].show();
                             jQuery("#weight_scrap").parent().parent()[0].show();
                        } else if(ringTypeValue == 1) {
                            var code = date.toString() + month.toString() + year.toString() + "D" + numberOfDiamondRings;
                            jQuery("#pipe_weight").parent().parent()[0].hide();
                            jQuery("#weight_scrap").parent().parent()[0].hide();
                        }
                        jQuery("#gold_casting_code").val(code);
                    });
                });
            });
            //]]>
            </script>');
        }

		$fieldset->addField('gold_casting_code', 'text', array(
		  'label'     => $this->__('Gold Casting Code'),
		  'name'      => 'gold_casting_code',
		  'required'	=> true,
          'readonly'    => true,
		  'disabled' 	=> $disabled
		));
		/*$alloy_field = $fieldset->addField('gold_alloy', 'select', array(
			'label'     => $this->__('Gold Alloy'),
			'name'      => 'gold_alloy',
			'required'	=> true,
			'values' 	=> $this->_getAlloys(),
			'disabled' 	=> $disabled
		));*/

        if($item->getId()){
    		 $fieldset->addField('gold_colour', 'note', array(
                'label' => $this->__('Gold information'),
                'text' => $this->_getGoldInfo()
            ));
        }

		$fieldset->addField('weight', 'text', array(
			'label'     => $this->__(' Weight'),
			'name'      => 'weight',
			'required'	=> true,
			'class'		=> 'validate-number',
			'note'	=>$this->__('Gramm'),
			'disabled' 	=> $disabled,
            'note'  =>$this->__('Gramm ( Second Gold Bar Weight + Gold Bar Weight )')
		));
		$fieldset->addField('qty', 'text', array(
			'label'     => $this->__('Gold Qty'),
            'note'	=>  $this->__('The number of production items we expect to be produced with this gold casting'),
			'name'      => 'qty',
			'required'	=> true,
			'class'		=> 'validate-digits',
			'disabled' 	=> $disabled
		));
        if(!$item->getId() || $item->getRingType() != 1) {
            
            $pipe_weight_field = $fieldset->addField('pipe_weight', 'text', array(
                'label'     => $this->__('Pipe Weight'),
                'name'      => 'pipe_weight',
                'required'	=> false,
                'class'		=> 'validate-number',
                'note'	=>$this->__('Gramm'),
            ));

            $fieldset->addField('weight_scrap', 'text', array(
                'label'     => $this->__('Weight Scrap'),
                'name'      => 'weight_scrap',
                'required'	=> false,
                'class'		=> 'validate-number',
                'note'	=>$this->__('Gramm ( weight - usage weight  - pipe weight) - The real gold lost weight on production, can override manually'),
            ));

            $pipe_weight_field->setAfterElementHtml('<script>
            //<![CDATA[
             
                jQuery("#pipe_weight").change(function() {
                    var pipe_weight = jQuery("#pipe_weight").val();
                    var gold_weight = jQuery("#weight").val();     
                    var total_weight_usage = jQuery("#total_usage").val();
                    //Weight scrap = (gold weight - gold usage) - pipe
                    var scrap = gold_weight-total_weight_usage - pipe_weight;
                    jQuery("#weight_scrap").val(scrap);
                    //Remaining weight has to calculate; gold weight - gold usage - scrap = remaining
                    var remaining = gold_weight - total_weight_usage - scrap
                    jQuery("#remaining_weight").text(remaining);
                });
             
            //]]>
            </script>');
        }
		
        $fieldset->addField('gold_bar_serial_no', 'text', array(
            'label'     => $this->__('Gold Bar Serial No'),
            'name'      => 'gold_bar_serial_no',
            'required'  => true,

        ));
      
        $fieldset->addField('gold_bar_serial_no2', 'text', array(
            'label'     => $this->__('Second Gold Bar Serial No'),
            'name'      => 'gold_bar_serial_no2',
            'required'  => false,

        ));
        $fieldset->addField('gold_bar_weight_2', 'text', array(
            'label'     => $this->__('Second Gold Bar Weight'),
            'name'      => 'gold_bar_weight_2',
            'required'  => false,
            'class'     => 'validate-number',
            'note'	    => $this->__('Gramm'),
        ));

        $leftoverField = $fieldset->addField('leftover_weight', 'text', array(
            'label'     => $this->__('Leftover Weight'),
            'name'      => 'leftover_weight',
            'required'  => false,
            'class'     => 'validate-number',
            'note'	    => $this->__('Gramm'),
        ));


      
        if($item->getId() && $item->getRingType() != 1) {
            $fieldset->addField('remaining_weight', 'note', array(
                'label' => $this->__('Remaining Weight'),
                'text' => $this->_getWeddingRingRemainingWeight()
            ));
        } elseif($item->getId() && $item->getRingType() == 1) {
            $leftoverField->setAfterElementHtml('<script>
            //<![CDATA[
             
                jQuery("#leftover_weight").change(function() {
                    var leftover_weight = jQuery("#leftover_weight").val();
                    var gold_weight = jQuery("#weight").val();     
                    var total_weight_usage = jQuery("#total_usage").val();
                    //Remaining weight has to calculate: gold weight - leftover - gold usage = remaining
                    var remaining = gold_weight - total_weight_usage - leftover_weight
                    jQuery("#remaining_weight").text(remaining);
                });
             
            //]]>
            </script>');

            $fieldset->addField('remaining_weight', 'note', array(
                'label' => $this->__('Remaining Weight'),
                'text' => $this->_getDiamondRingRemainingWeight()
            ));
        }

		$fieldset->addField('gold_description', 'textarea', array(
			'label'     => $this->__('Gold Description'),
			'name'      => 'gold_description',
			'required'	=> false,
		));
        $form->setValues(Mage::registry('current_gold_casting')->getData());
		$fieldset->addField('number_of_diamond_ring', 'hidden', array(
            'name' => 'number_of_diamond_ring',
            'value' => $this->_getNumberOfDiamondRingOfCurrentDay()
        ));

        $fieldset->addField('number_of_wedding_ring', 'hidden', array(
            'name' => 'number_of_wedding_ring',
            'value' => $this->_getNumberOfWeddingRingOfCurrentDay()
        ));
        
        if($item->getId()) {
            $fieldset->addField('remain_weight', 'hidden', array(
                'name' => 'remain_weight',
                'value' => ''
            ));

            $fieldset->addField('total_usage', 'hidden', array(
                'name' => 'total_usage',
                'value' => $this->getTotalGoldUsage()
            ));
        }
 return parent::_prepareForm();
 }

    protected function _getGoldInfo()
	{
		$item = Mage::registry('current_gold_casting');
        $gold_bar_serial_no = $item->getData('gold_bar_serial_no');
        $goldbar = Mage::getModel('opentechiz_material/goldbar')->loadBySerial($gold_bar_serial_no);
        $gold = Mage::getModel('opentechiz_material/gold')->load($goldbar->getGoldId());
		return $gold->getGoldType().' '.$gold->getGoldColor();
	}
     protected function _getNumberOfDiamondRingOfCurrentDay() {
	    $today = date('Y-m-d');
        $result = Mage::getModel('opentechiz_production/goldcasting')->getCollection()
            ->addFieldToFilter('ring_type', 1)
            ->addFieldToFilter('created_at', array('from' => $today.' 00:00:00', 'to' => $today.' 23:59:59'))
            ->getSize();
	    return $result;
    }
    protected function _getWeddingRingRemainingWeight() {
        //Remaining weight has to calculate; gold weight - gold usage - scrap = remaining
        $item = Mage::registry('current_gold_casting');
        $itemId = $item->getId();

        $collection = Mage::getModel('opentechiz_production/goldcasting')->getCollection();
        $collection->joinGetGoldUsage();
        $column = new Zend_Db_Expr("main_table.weight - ifnull(usage.total_weight_usage, 0) - ifnull(main_table.weight_scrap, 0)");
        $collection->getSelect()->columns(array('weight_remain'=>$column));
        $collection->addFieldToFilter('main_table.gold_casting_id', array('eq' => $itemId));

        $remaining_weight = $collection->getFirstItem()->getWeightRemain();
        return $remaining_weight;

    }
    protected function _getDiamondRingRemainingWeight() {
        //Remaining weight has to calculate: gold weight - leftover- gold usage = remaining
        $item = Mage::registry('current_gold_casting');
        $itemId = $item->getId();

        $collection = Mage::getModel('opentechiz_production/goldcasting')->getCollection();
        $collection->joinGetGoldUsage();
        $column = new Zend_Db_Expr("main_table.weight - ifnull(usage.total_weight_usage, 0) - ifnull(main_table.leftover_weight, 0)");
        $collection->getSelect()->columns(array('weight_remain'=>$column));
        $collection->addFieldToFilter('main_table.gold_casting_id', array('eq' => $itemId));

        $remaining_weight = $collection->getFirstItem()->getWeightRemain();
        return $remaining_weight;
    }
    protected function _getNumberOfWeddingRingOfCurrentDay() {
        $today = date('Y-m-d');
        $result = Mage::getModel('opentechiz_production/goldcasting')->getCollection()
            ->addFieldToFilter('ring_type', 2)
            ->addFieldToFilter('created_at', array('from' => $today.' 00:00:00', 'to' => $today.' 23:59:59'))
            ->getSize();
        return $result;
    }



    protected function _getGoldStatus() {
        $item = Mage::registry('current_gold_casting');
        $gold_status = $item->getStatus();
        $html = '';
        if($gold_status == 1) {
            $html .= '<span style="color:green">'.$this->__('Available').'</span>';
        } else {
            $html .= '<span style="color:red">'.$this->__('Closed').'</span>';
        }
        return $html;
    }

    protected function getTotalGoldUsage() {
        $collection = Mage::getModel('opentechiz_production/goldusage')->getCollection();
        $collection->addFieldToFilter('gold_casting_code', array('eq' => Mage::registry('current_gold_casting')->getGoldCastingCode()));
        $total = 0;
        foreach ($collection as $item) {
            $total += $item->getWeightUsage();
        }
        return $total;
    }
}