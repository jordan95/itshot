<?php

class OpenTechiz_Production_Block_Adminhtml_Manage_Goldcasting extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_manage_goldcasting';
        $this->_blockGroup = 'opentechiz_production';
        $this->_headerText = Mage::helper('opentechiz_production')->__('Gold Casting Grid');
        parent::__construct();
    }
}