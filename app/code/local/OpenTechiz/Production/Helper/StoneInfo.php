<?php

class OpenTechiz_Production_Helper_StoneInfo extends Mage_Core_Helper_Abstract
{
    public function getStoneInfoBroken($item){
        $qualities = OpenTechiz_PersonalizedProduct_Helper_Data::QUALITY;
        $itemOption = unserialize($item->getData('options'));
        $result = [];
        if(array_key_exists('quotation_product_id', $itemOption['info_buyRequest']['options'] )){
            $quotationProductId =  $itemOption['info_buyRequest']['options']['quotation_product_id'];
            $quotationOptionCollection = Mage::getModel('opentechiz_quotation/productOption')->getCollection()
                ->addFieldToFilter('quote_product_id', $quotationProductId)->addFieldToFilter('option_type', 'stone');
            $stoneIds = [];
            foreach ($quotationOptionCollection as $option){
                array_push($stoneIds, $option->getOptionValue());
            }
            $stoneCollection = Mage::getModel('opentechiz_material/stone')->getCollection()
                ->addFieldToFilter('stone_id', array('in' => $stoneIds));
            foreach ($stoneCollection as $stone){
                $string = $stone->getStoneType().$stone->getQuality().'-'.$stone->getShape().'-'.$stone->getDiameter();
                array_push($result, $string);
            }
        }else{
            $productId =  $itemOption['info_buyRequest']['product'];
            $product = Mage::getModel('catalog/product')->load($productId);
            $stoneCollection = Mage::getModel('personalizedproduct/price_stone')->getCollection()
                ->join(array('stone' => 'opentechiz_material/stone'), 'main_table.stone_id = stone.stone_id', '*')
                ->addFieldToFilter('product_id', $productId);

            if(array_key_exists('options', $itemOption)){
                foreach ($itemOption['options'] as $option){
                    if($option['option_type'] == 'stone'){
                        $productOption = $product->getOptionById($option['option_id']);
                        $value = $productOption->getValueById($option['option_value']);
                        $stoneSku = $value->getSku();
                        if(in_array(str_replace('diamond','', $stoneSku), $qualities)){
                            $stoneSku = 'diamond';
                            $quality = str_replace('diamond','', $stoneSku);
                        }else{
                            $quality = null;
                        }

                        if($quality){
                            $stone = $stoneCollection->addFieldToFilter('stone_type', $stoneSku)->addFieldToFilter('option_id', $option['option_id'])->addFieldToFilter('quality', $quality)
                                ->getFirstItem();
                        }else{
                            $stone = $stoneCollection->addFieldToFilter('stone_type', $stoneSku)->addFieldToFilter('option_id', $option['option_id'])->getFirstItem();
                        }
                        $string = $stoneSku.$stone->getQuality().'-'.$stone->getShape().'-'.$stone->getDiameter();
                        array_push($result, $string);
                    }
                }
            }
        }

        return $result;
    }
}