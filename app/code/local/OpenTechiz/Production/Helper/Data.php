<?php

class OpenTechiz_Production_Helper_Data extends Mage_Core_Helper_Abstract
{
    const OPPENTECHIZ_PRODUCTION_PRODUCTION_TYPE = [
        'To Customer',
        'To Stock',
        'Return Repair',
        'Return Reproduction',
        'Repair',
        'Resize and Engrave'
    ];
    const OPPENTECHIZ_RING_TYPE = [
       '2' => 'Wedding Ring',
        '1' => 'Diamond Ring'
    ];
    const OPPENTECHIZ_GOLD_CASTING_STATUS = [
        '0' => 'Closed',
        '1' => 'Available'
    ];
    const OPPENTECHIZ_STAFF_STATUS = [
        '1' => 'Enable',
        '0' => 'Disable'
    ];
    const OPPENTECHIZ_PRODUCTION_STATE = [
        0 =>'In Production',
        1 =>'Production Finished',
        2 =>'Canceled',
        3 =>'Broken',
        4 =>'In Repair',
        5 =>'Repair Finished',
    ];

    const OPPENTECHIZ_PRODUCTION_PROCESS_STATUS = [
        'Not Ordered',
        'Ordered'
    ];

    const PRODUCT_PROCESS_REQUEST_STATUS = [
        0 => 'Pending',
        1 => 'Production Requested',
        2 => 'Stock Requested',
    ];

    const PROCESS_STATUS = [
        '2' => 'Redo',
        '0' => 'Finished',
        '1' => 'Broken'
    ];

    const STAFF_TYPE = [
        0   => 'Internal Manufacturer',
        1   => 'Vendor',
    ];

    const PROCESSES = [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50];
//    const REDOABLE_PROCESSES = [10, 30, 35, 40];

    const GOLD_WEIGHT_HIDDEN = [1];
    const GOLD_WEIGHT_DISABLED = [45];
    const GOLD_WEIGHT_REQUIRED = [5,10,15,20,25,30,35,40];

    const STONE_WEIGHT_HIDDEN = [1,5,10,15,20];
    const STONE_WEIGHT_DISABLED = [30,35,40,45];
    const STONE_WEIGHT_REQUIRED = [25];

    const RESIZE_AND_ENGRAVE_PROCESSES = [38, 40, 45, 50];

    const PROCESS_LIST = [
        1 => 'Rubber Mould Creating',
        5 => 'Casting',
        10=> 'Grinding',
        15=> 'Filing/Assembly',
        20=> 'Polishing 1',
        25=> 'Stone Setting',
        30=> 'Polishing 2',
        35=> 'Rhodium Plating',
        38=> 'Resizing',
        40=> 'Engraving',
        45=> 'Quality Control',
        50=> 'Finished'
    ];

    public function getRedoAbleProcesses(){
        $results = explode(',', $configValue = Mage::getStoreConfig('opentechiz_production/general/redo_process'));
        foreach ($results as $key => $result){
            if(trim($result) == '') unset($results[$key]);
        }
        if(count($results) == 0) return array();
        return $results;
    }

     public function checkGoldOption($castingCode,$item){
        $goldCasting = Mage::getModel('opentechiz_production/goldcasting')->load($castingCode, 'gold_casting_code');
        $goldBarSerial = $goldCasting->getData('gold_bar_serial_no');
        $goldBarModel = Mage::getModel('opentechiz_material/goldbar');

        $joinData = $goldBarModel->getCollection()->join(array('gold'=> 'opentechiz_material/gold'),'main_table.gold_id = gold.gold_id and main_table.serial_no = "'. $goldBarSerial.'"')->getFirstItem();
        $sku = $item->getSku();
      if(strpos($sku , 'quotation') === false){
        $gold_color = $joinData->getData('gold_color');
        $gold_type = $joinData->getData('gold_type');
        $gold_sku = strtolower($gold_type.$gold_color);
        $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
        if(in_array($gold_sku, $parts))
            return true;
        }
      else{
        $option =  unserialize($item->getOptions());
        $quotation_productid = $option['info_buyRequest']['options']['quotation_product_id'];
        $gold_id = $joinData->getData('gold_id');
        $quote_gold_id = Mage::getModel('opentechiz_quotation/productOption')->getCollection()->AddFieldtoFilter('quote_product_id',$quotation_productid)->AddFieldtoFilter('option_type','gold')->getFirstItem()->getOptionValue();
        if($gold_id == $quote_gold_id)
            return true;
      }
        return false;
    }
    public function getRemainWeight($gold_casting){
        $collection = Mage::getModel('opentechiz_production/goldcasting')->getCollection();
        $collection->joinGetGoldUsage();
        $itemId = $gold_casting->getId();
        
        $collection->addFieldToFilter('main_table.gold_casting_id', array('eq' => $itemId));
        $data = $collection->getFirstItem()->getData();
        if($gold_casting->getRingType()==2)
            $remaining_weight = $data['weight'] - $data['weight_usage'] - $data['weight_scrap'];
        else
            $remaining_weight = $data['weight'] - $data['weight_usage'] - $data['leftover_weight'];

        
        return $remaining_weight;
    }

    public function getListBrokenReasons() {
        $reasons = array(
            '1' => $this->__('Broken In Production'),
            '2' => $this->__('Broken In Repair'),
            '3' => $this->__('Stone Lost'),
        );
        return $reasons;
    }

    public function getListStaffStoneSetting() {
        $result = array();
        $useStaff = Mage::getStoreConfig('opentechiz_production/general/use_staff');

        if(!$useStaff){
            $users = Mage::getModel('admin/user');
            $usersCollection = $users->getCollection();
            foreach($usersCollection as $user){
                $result[$user->getId()] = $user->getFirstname() . ' ' . $user->getLastname();
            }
        }else{
            $users = Mage::getModel('opentechiz_production/staff');
            $usersCollection = $users->getCollection();
            foreach($usersCollection as $user){
                $result[$user->getId()] = $user->getStaffName();
            }
        }

        return $result;
    }

    public function queryAllProduction($sku){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $query = "SELECT * FROM tsht_request_product 
                join tsht_sales_flat_order_item on tsht_request_product.order_item_id = tsht_sales_flat_order_item.item_id
                join tsht_sales_flat_order on tsht_sales_flat_order_item.order_id = tsht_sales_flat_order.entity_id
                where tsht_sales_flat_order.status not in ('complete', 'canceled', 'closed', 'holded')
                and tsht_sales_flat_order.order_stage = 2 and tsht_sales_flat_order.internal_status = 2
                and tsht_sales_flat_order_item.sku like '".$sku."%'";
        $results = $readConnection->fetchAll($query);

        return $results;
    }
}