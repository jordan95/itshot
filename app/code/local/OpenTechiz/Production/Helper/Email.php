<?php

class OpenTechiz_Production_Helper_Email extends Mage_Core_Helper_Abstract
{
    public function sendEmailProduction($status, $personalizedArray){
        $emailText = Mage::getStoreConfig('opentechiz_production/general/receiver_email');
        $emailText = trim($emailText, ',');
        $emailToArray = explode(',', $emailText);

        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        $idHtml = '';

        if(gettype($personalizedArray) == 'string'){
            $personalizedArray = trim($personalizedArray, ',');
            $personalizedArray = explode(',', $personalizedArray);
        }

        $count = count($personalizedArray);

        if($count < 2){
            foreach($personalizedArray as $id){
                $idHtml .= '#'.$id;
            }
        } else{
            for($i = 0; $i< $count - 2; $i ++ ){
                $idHtml .= '#'. $personalizedArray[$i];
            }
            $idHtml = trim($idHtml, ',');
            $idHtml .= ' and ' . '#'.$personalizedArray[$count-1];
        }

        $emailTemplate = Mage::getModel('core/email_template');
        if($status == 'start'){
            $emailTemplate->loadDefault('production_noty_start_tpl');
            $emailTemplate->setTemplateSubject(Mage::app()->getStore()->getName().': Production Start Notification');
        } else if($status == 'finish'){
            $emailTemplate->loadDefault('production_noty_finish_tpl');
            $emailTemplate->setTemplateSubject(Mage::app()->getStore()->getName().': Production Finish Notification');
        }

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();
        $emailTemplateVariables['id'] = $idHtml;
        $emailTemplateVariables['url'] = Mage::getBaseUrl().'admin/request_product/index';

        foreach ($emailToArray as  $recipientEmail) {
            if ($recipientEmail) {
                $emailTemplate->send($recipientEmail,'Admin', $emailTemplateVariables);
            }
        }
    }
}