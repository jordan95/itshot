<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('opentechiz_production/product'),'finished_at', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'nullable'  => true,
        'after'     => 'created_at',
        'comment'   => 'finished at'
    ));
$installer->endSetup();