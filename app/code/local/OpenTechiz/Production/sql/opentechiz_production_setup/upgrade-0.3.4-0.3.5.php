<?php

$installer = $this;
$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('opentechiz_production/goldScrap')};
CREATE TABLE {$this->getTable('opentechiz_production/goldScrap')} (
  `id`   INT(11) UNSIGNED   NOT NULL AUTO_INCREMENT,
  `type`      varchar(30)   NOT NULL,
  `price`      double       NOT NULL,
  `qty`       double        not null, 
  `note`      text ,
  `created_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$installer->endSetup();