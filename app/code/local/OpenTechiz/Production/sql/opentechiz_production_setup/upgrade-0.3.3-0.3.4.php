<?php

$installer = $this;
$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('opentechiz_production/process_request')};
CREATE TABLE {$this->getTable('opentechiz_production/process_request')} (
  `id`   INT(11) UNSIGNED         NOT NULL AUTO_INCREMENT,
  `order_item_id`      INT(11)       NOT NULL,
  `order_id`      INT(11)       NOT NULL,
  `status`      INT(11)           NOT NULL DEFAULT 0,
  `created_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('opentechiz_production/purchase')};
CREATE TABLE {$this->getTable('opentechiz_production/purchase')} (
  `id`   INT(11) UNSIGNED         NOT NULL AUTO_INCREMENT,
  `order_id`      INT(11)       NOT NULL,
  `qty`      INT(11)           NOT NULL DEFAULT 1,
  `created_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$installer->endSetup();