<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection();

$tableName = $installer->getTable('request_product');
$installer->run(" ALTER TABLE {$tableName} ADD `item_state` VARCHAR(100) NOT NULL  ; ");
$installer->endSetup();

$installer->run("
    
    ALTER TABLE {$this->getTable('opentechiz_production/goldcasting')} 
    CHANGE `pipe_weight` `pipe_weight`    double(12,4)  NOT NULL DEFAULT '0.0000';
    
");
$installer->run("
    
    ALTER TABLE {$this->getTable('opentechiz_production/goldcasting')} 
    CHANGE `weight_scrap` `weight_scrap`    double(12,4)  NOT NULL DEFAULT '0.0000';
    
");
$installer->run("
    
    ALTER TABLE {$this->getTable('opentechiz_production/goldcasting')} 
    CHANGE `leftover_weight` `leftover_weight`    double(12,4)  NOT NULL DEFAULT '0.0000';
    
");
$installer->endSetup();