<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection();

$tableName = $installer->getTable('opentechiz_production/production_history');
$installer->run(" ALTER TABLE {$tableName} ADD `owner_staff_id` INTEGER NOT NULL DEFAULT 0  ; ");

$installer->endSetup();