<?php
/**
 * Created by PhpStorm.
 * Date: 30-08-2017
 * Time: 13:45
 */

$upgrade = $this;
$upgrade->startSetup();

$tableName = $upgrade->getTable('opentechiz_production/brokenstone');
if ($upgrade->getConnection()->isTableExists($tableName) != true) {
    $table = $upgrade->getConnection()->newTable($tableName)
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
            'primary' => true,
            'unsigned' => true,
            'nullable' => false,
            'identity' => true,
        ), 'Id')
        ->addColumn('barcode', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ),'barcode')
        ->addColumn('stone_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ),'Stone Name')
        ->addColumn('broken_qty', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
            'nullable' => false,
        ),'Broken Qty')
        ->addColumn('broken_reason', Varien_Db_Ddl_Table::TYPE_TINYINT, 3, array(
            'nullable' => true,
        ),'Broken Reason')
        ->addColumn('staff', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ),'Staff')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, array(
            'nullable' => false,
        ),'Staff')
        ->addColumn('comment', Varien_Db_Ddl_Table::TYPE_TEXT, array(
            'nullable' => true,
        ),'Staff')
        ->setComment('Broken Stone Table');
    $upgrade->getConnection()->createTable($table);
}
$upgrade->endSetup();