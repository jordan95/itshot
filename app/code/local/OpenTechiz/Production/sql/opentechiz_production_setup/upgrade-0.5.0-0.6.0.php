<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection();

$tableName = $installer->getTable('opentechiz_production/staff');
$installer->run(" ALTER TABLE {$tableName} ADD `is_owner` INTEGER NOT NULL DEFAULT 0  ; ");

$installer->endSetup();