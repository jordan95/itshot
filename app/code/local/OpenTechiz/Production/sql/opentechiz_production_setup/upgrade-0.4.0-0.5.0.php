<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection();

$tableName = $installer->getTable('opentechiz_production/staff');
$installer->run(" ALTER TABLE {$tableName} ADD `staff_type` INTEGER NOT NULL DEFAULT 0  ; ");

$installer->endSetup();