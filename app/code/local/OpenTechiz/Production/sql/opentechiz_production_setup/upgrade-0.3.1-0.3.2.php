<?php
$installer = $this;
$installer->startSetup();
$installer->run("
    
    ALTER TABLE {$this->getTable('opentechiz_production/goldusage')} 
    CHANGE `weight_usage` `weight_usage`    double(12,4)  NOT NULL DEFAULT '0.0000';
    
");
$installer->endSetup();