<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('production_gold_casting')};
    CREATE TABLE {$this->getTable('production_gold_casting')} (
      `gold_casting_id`    INT(11)       UNSIGNED  NOT NULL AUTO_INCREMENT,
      `gold_casting_code`    		   VARCHAR(45)   NOT NULL,
      `ring_type` INT(1)        NOT NULL,
      `gold_bar_serial_no`       VARCHAR(45)  NOT NULL,
      `weight`      double(12,4)  NOT NULL DEFAULT '0.0000',
      `gold_bar_serial_no2`     VARCHAR(45)       NULL,
      `gold_bar_weight_2`    double(12,4)  NOT NULL DEFAULT '0.0000',
      `qty`             VARCHAR(255)  NOT NULL,
      `pipe_weight`       		   double(12,4)    DEFAULT NULL,      
      `weight_scrap`        	   double(12,4)    DEFAULT NULL,
      `leftover_weight`            double(12,4)    DEFAULT NULL,
      `description`  VARCHAR(255)   NULL,
      `created_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `updated_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      `status`             INT(1)        NOT NULL DEFAULT '1',
      PRIMARY KEY (`gold_casting_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('opentechiz_production/goldusage')};
    CREATE TABLE {$this->getTable('opentechiz_production/goldusage')} (
    `id` int(11) NOT NULL auto_increment,
    `gold_casting_code` varchar(255) NOT NULL,
    `personalized_id` int(11) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `weight_usage` decimal(11, 2) NULL,
    `user_name` varchar(255) NULL,
    PRIMARY KEY (`id`),
    UNIQUE `GOLD_CODE_PERSONALIZED_ID` (`gold_casting_code`, `personalized_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();