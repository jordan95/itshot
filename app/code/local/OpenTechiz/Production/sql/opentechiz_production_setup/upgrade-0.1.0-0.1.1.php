<?php

$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('opentechiz_production/production_history')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('opentechiz_production/production_history'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'Id')
        ->addColumn('personalized_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'PersonalizedProductId')
        ->addColumn('stone_request_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        ), 'stone_request_id')
        ->addColumn('item_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'Order Item Id')
        ->addColumn('stone_weight', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
            'nullable' => false,
        ), 'stone weight after step')
        ->addColumn('gold_weight', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
            'nullable' => false,
        ), 'gold weight after step')
        ->addColumn('step_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false,
        ), 'Step Name')
        ->addColumn('staff_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'Staff Id')
        ->addColumn('admin_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'Admin Id')
        ->addColumn('started_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'Starting Time')
        ->addColumn('finished_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'Finishing Time')
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'Process Status')
        ->addColumn('note', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
        ), 'Note');
    $installer->getConnection()->createTable($table);
}

$installer->endSetup();