<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('request_product')};
    CREATE TABLE {$this->getTable('request_product')} (
      `personalized_id`    INT(11)       UNSIGNED  NOT NULL AUTO_INCREMENT,
      `barcode`    		   VARCHAR(45)   NOT NULL,
      `image`        	   VARCHAR(255)  NOT NULL,
      `order_item_id`      INT(11)       NULL,
      `manufacture_id`     INT(11)       NULL,
      `production_type`    INT(11)       NOT NULL DEFAULT '0',
      `option`             VARCHAR(255)  NOT NULL,
      `note`       		   VARCHAR(255)  NOT NULL,
      `created_at` 		   DATETIME      NOT NULL,
      `status`        	   INT(1)        NOT NULL,
      PRIMARY KEY (`personalized_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
$installer->endSetup();