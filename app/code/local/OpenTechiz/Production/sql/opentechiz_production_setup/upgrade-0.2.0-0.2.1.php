<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('opentechiz_production/staff')};
    CREATE TABLE {$this->getTable('opentechiz_production/staff')} (
      `id`    INT(11)       UNSIGNED  NOT NULL AUTO_INCREMENT,
      `staff_code`           VARCHAR(45)   NOT NULL,
      `staff_name`    		   VARCHAR(45)   NOT NULL,
      `production_step`             VARCHAR(255)  NOT NULL,
      `status`             INT(1)        NOT NULL DEFAULT '1',
      PRIMARY KEY (`id`),
      UNIQUE `STAFF_CODE` (`staff_code`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);