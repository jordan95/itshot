<?php
class Opentechiz_Production_Model_System_Config_Source_Brand
{
    public function toOptionArray()
    {
        $options = [];
        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'c2c_brand');

        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
            usort($options, array('Opentechiz_Production_Model_System_Config_Source_Brand','compare_label'));
        }
        return $options;
    }

    private static function compare_label($a, $b)
    {
        return strnatcmp($a['label'], $b['label']);
    }
}