<?php
class Opentechiz_Production_Model_System_Config_Source_Redo
{
    public function toOptionArray()
    {
        $options = [];
        $unRedoAble = [1, 5, 45, 50];
        foreach (OpenTechiz_Production_Helper_Data::PROCESS_LIST as $key => $value) {
            if(in_array($key, $unRedoAble)) continue;
            array_push($options, array('value' => $key, 'label' => $value));
        }
        return $options;
    }
}