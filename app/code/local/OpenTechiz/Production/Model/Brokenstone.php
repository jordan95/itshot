<?php
/**
 * Created by PhpStorm.
 * Date: 20-09-2017
 * Time: 16:47
 */

class OpenTechiz_Production_Model_Brokenstone extends Mage_Core_Model_Abstract
{
    public function _construct() {
        parent::_construct();
        $this->_init('opentechiz_production/brokenstone');
    }
}