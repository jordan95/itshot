<?php

class OpenTechiz_Production_Model_Process_Request extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_production/process_request');
    }

    public function createProductionRequest($orderId){
        $isInstock = false;
        $model = Mage::getSingleton('sales/order_item')->load($orderId);
        if($model && $model->getId()) {
            $requestModel = Mage::getModel('opentechiz_production/product');
            $itemModel = Mage::getModel('opentechiz_inventory/item');
            $product = Mage::getModel('catalog/product')->load($model->getProductId());

            $data = [];
            $productData = unserialize($model->getData('product_options'));
            $data['image'] = $itemModel->imageUrlFromOption($productData, $product);
            $data['order_item_id'] = $orderId;
            $data['production_type'] = OpenTechiz_Production_Helper_Data::OPPENTECHIZ_PRODUCTION_PRODUCTION_TYPE[0];
            $data['option'] = $model->getData('product_options');
            $data['note'] = '';
            $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['status'] = OpenTechiz_Production_Helper_Data::PROCESSES[0];
            $data['item_state'] = 0;

            $stoneCost = $itemModel->getCurrentStoneCost($data['option'], $product);

            $cost = $stoneCost;
            $data['option'] = unserialize($data['option']);
            $data['option']['info_buyRequest']['product'] = $product->getId();
            $data['option'] = serialize($data['option']);
            $idArray = [];

            //save
            $requestModel->setData($data)->save();
            //save item
            $itemData = [];
            $itemData['sku'] = $model->getSku();
            $itemData['name'] = $model->getName();
            $itemData['image'] = $data['image'];
            $itemData['type'] = 0;
            $itemData['options'] = $data['option'];
            $itemData['note'] = '';
            $itemData['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $itemData['updated_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $itemData['state'] = 0;
            $itemData['cost'] = $cost;
            $itemModel->setData($itemData)->save();

            $item_id = $itemModel->getId();

            $itemData['barcode'] = $itemModel->generateBarcode($item_id);
            $itemModel->addData($itemData)->setId($item_id)->save();
            //add barcode
            $id = $requestModel->getId();
            $data['barcode'] = $itemData['barcode'];
            $requestModel->addData($data)->setId($id)->save();
            array_push($idArray, $id);

            if (Mage::helper('personalizedproduct')->isPersonalizedProduct($product)) {
                Mage::getModel('opentechiz_material/stoneRequest')->addStoneRequest($id, $model->getProductId(), $productData['options']);
            } else {
                Mage::getModel('opentechiz_material/stoneRequest')->addStoneRequestNonPersonalized($id, $model->getProductId());
            }

            Mage::helper('opentechiz_production/email')->sendEmailProduction('start', $idArray);

            $model->setIsRequestedToProcess(1);
            $model->save();
        }
    }

    public function createStockProductRequest($orderId,$order_real_id = '',$request_id = ''){
        $orderItem = Mage::getSingleton('sales/order_item')->load($orderId);
        $requestModel = Mage::getModel('opentechiz_production/product');
        $itemModel = Mage::getModel('opentechiz_inventory/item');

        $instockRequestModel = Mage::getModel('opentechiz_inventory/inventory_instockRequest');
        $instockModel = Mage::getModel('opentechiz_inventory/instock');
        $inventoryOrderModel = Mage::getModel('opentechiz_inventory/order');

        //check if available in stock
        $orderSku = $orderItem->getSku();
        //check if reserved from production
        $orderItemReservedItemCollection = Mage::getModel('opentechiz_inventory/order')->getCollection()
            ->join(array('item' => 'opentechiz_inventory/item'), 'main_table.order_item_id = '.$orderId.' AND main_table.item_id = item.item_id AND item.state in (35,26)', '*');
        //get all item bind to this order item
        $itemIds = [];
        if(count($orderItemReservedItemCollection)){
            foreach ($orderItemReservedItemCollection as $item){
                array_push($itemIds, $item->getItemId());
            }
        }
        $currentItemStock = Mage::getModel('opentechiz_inventory/instock')->load($orderSku, 'sku');
        if(count($itemIds)){ //if reserved from production
            $itemReservedCollection = Mage::getModel('opentechiz_inventory/item')->getCollection()
                ->addFieldToFilter('state', 26)->addFieldToFilter('sku', $orderSku);
            $itemReserved = $itemReservedCollection->getFirstItem();
            $instockRequestData = [];
            $instockRequestData['order_item_id'] = $orderId;
            $instockRequestData['instock_product_id'] = $currentItemStock->getId();
            $instockRequestData['barcodes'] = $itemReserved->getBarcode();
            $instockRequestData['qty'] = 1;
            $instockRequestModel->setData($instockRequestData)->save();
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $table = $resource->getTableName('opentechiz_inventory/inventory_instockRequest');
            $query = "UPDATE {$table} SET barcodes = '".$itemReserved->getBarcode()."' WHERE request_id = ".$instockRequestModel->getId();
            $writeConnection->query($query);
            return true;
        }else { //else
            $qty = 1;
            $result = Mage::helper('opentechiz_inventory')->getAvailableSize($orderSku, true);

            if (!empty($result)) {
                $isInstock = true;
                $data = $result[max(array_keys($result))];
                //create instock request
                $instockRequestData = [];
                $instockRequestData['order_item_id'] = $orderId;
                $instockRequestData['instock_product_id'] = $data['id'];
                $itemLeft = $data['qty'] - $data['on_hold'];
                if ($itemLeft >= $qty) $instockRequestData['qty'] = $qty;
                else {
                    $instockRequestData['qty'] = $itemLeft;
                }

                //change state in item - create item_order
                $collection = $itemModel->getCollection()->addFieldToFilter('sku', $data['sku'])->addFieldToFilter('state', 35);
                $orderData = [];
                $orderData['order_item_id'] = $orderId;
                $stock = Mage::getModel('opentechiz_inventory/instock')->load($data['id']);
                if($stock->getId()){
                    Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                        'qty' => 0,
                        'on_hold' => '+' . $instockRequestData['qty'],
                        'type' => 13,
                        'stock_id' => $data['id'],
                        'qty_before_movement' => $stock->getQty(),
                        'on_hold_before_movement' => $stock->getOnHold(),
                        'sku' => $stock->getSku(),
                        'order_id' =>$order_real_id,
                        'product_process_request_id' =>$request_id
                         
                    ));
                }
                $i = 0;
                $barcodes = [];
                foreach ($collection as $item) {
                    $i++;
                    if ($i > $instockRequestData['qty']) break;
                    $item->addData([
                        'state' => 25,
                        'updated_at' => Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s')
                    ])
                        ->setId($item->getId())
                        ->save();
                    $orderData['item_id'] = $item->getId();
                    $inventoryOrderModel->setData($orderData)->save();
                    array_push($barcodes, $item->getBarcode());

                    //set new item barcode to return exchange
                    $order = Mage::getSingleton('sales/order')->load($orderItem->getOrderId());
                    if($order && $order->getOrderType() == 3){
                        foreach ($order->getAllItems() as $orderItem){
                            if (strpos($orderItem->getDescription(), '#') !== false) {
                                $description = $orderItem->getDescription();
                            }
                        }
                        if(isset($description)) {
                            $returnIncrement = explode(' ', $description);
                            $returnIncrement = $returnIncrement[count($returnIncrement) - 1];
                            $returnIncrement = trim($returnIncrement, '#');
                            $return = Mage::getModel('opentechiz_return/return')->load($returnIncrement, 'increment_id');
                            $return->setNewItemBarcode($item->getBarcode())->save();
                        }
                    }
                }
              
                $instockRequestData['barcodes'] = implode(',', $barcodes);
                $instockRequestModel->setData($instockRequestData)->save();
                $resource = Mage::getSingleton('core/resource');
                $writeConnection = $resource->getConnection('core_write');
                $table = $resource->getTableName('opentechiz_inventory/inventory_instockRequest');
                $query = "UPDATE {$table} SET barcodes = '".$instockRequestData['barcodes']."' WHERE request_id = ".$instockRequestModel->getId();
                $writeConnection->query($query);
                return true;
            } else {
                return false;
            }
        }
    }

    public function requestToProcessQuotation($orderId){

        $quoteProductModel = Mage::getModel('opentechiz_quotation/product');
        $quoteProductOptionModel = Mage::getModel('opentechiz_quotation/productOption');
        $requestModel = Mage::getModel('opentechiz_production/product');
        $itemModel = Mage::getModel('opentechiz_inventory/item');
        $orderItem = Mage::getModel('sales/order_item')->load($orderId);

        $data = [];
        $productData = unserialize($orderItem->getData('product_options'));
        $quoteProductId = $productData['info_buyRequest']['options']['quotation_product_id'];
        $quoteProduct = $quoteProductModel->load($quoteProductId);

        $quoteProductImageArray = explode(',', $quoteProduct->getQpImage());
        if(count($quoteProductImageArray)>0) {
            $data['image'] = $quoteProductImageArray[0];
        } else $data['image'] = '';
        $data['order_item_id'] = $orderItem->getId();
        $data['production_type'] = OpenTechiz_Production_Helper_Data::OPPENTECHIZ_PRODUCTION_PRODUCTION_TYPE[0];
        $data['option'] = $orderItem->getData('product_options');
        $data['note'] = '';
        $data['item_state'] = 0;
        $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $data['status'] = OpenTechiz_Production_Helper_Data::PROCESSES[0];

        $orderData = [];
        $orderData['order_item_id'] = $orderItem->getId();

        //calculate cost
        $optionCollection = Mage::getModel('opentechiz_quotation/productOption')->getCollection()->addFieldToFilter('quote_product_id', $quoteProductId);
        $cost = 0.00;
        foreach ($optionCollection as $option){
            if($option->getOptionType() == 'stone'){
                $stoneId = $option->getOptionValue();
                $qty = $option->getQty();
                $price = Mage::getModel('opentechiz_material/stone')->load($stoneId)->getPrice() * $qty;
                $cost += $price;
            } else if($option->getOptionType() == 'gold'){
                $goldId = $option->getOptionValue();
                $qty = $option->getQty();
                $price = Mage::getModel('opentechiz_material/stone')->load($goldId)->getPrice() * $qty;
                $cost += $price;
            }
        }
        $idArray = [];

        //save
        $requestModel->setData($data)->save();
        //save item
        $itemData = [];
        $itemData['sku'] = $orderItem->getSku();
        $itemData['name'] = $orderItem->getName();
        $itemData['image'] = $data['image'];
        $itemData['type'] = 0;
        $itemData['options'] = $data['option'];
        $itemData['note'] = '';
        $itemData['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $itemData['updated_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $itemData['state'] = 0;
        $itemData['cost'] = $cost;
        $itemModel->setData($itemData)->save();

        $item_id = $itemModel->getId();
        $orderData['item_id'] = $item_id;

        Mage::getModel('opentechiz_inventory/order')->setData($orderData)->save();

        $itemData['barcode'] = $itemModel->generateBarcode($item_id);
        $itemModel->addData($itemData)->setId($item_id)->save();
        //add barcode
        $id = $requestModel->getId();
        $data['barcode'] = $itemData['barcode'];

        $requestModel->addData($data)->setId($id)->save();
        array_push($idArray, $id);

        $stoneOptionCollection = $quoteProductOptionModel->getCollection()
            ->addFieldToFilter('quote_product_id', $quoteProduct->getId())
            ->addFieldToFilter('option_type', 'stone');
        Mage::getModel('opentechiz_material/stoneRequest')->addStoneRequestQuotation($id, $stoneOptionCollection);

        Mage::helper('opentechiz_production/email')->sendEmailProduction('start', $idArray);

        $orderItem->setIsRequestedToProcess(1)->save();
    }
}