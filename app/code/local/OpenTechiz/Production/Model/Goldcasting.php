<?php

class OpenTechiz_Production_Model_Goldcasting extends Mage_Core_Model_Abstract
{
    const GOLD_STATUS_AVAILABLE = '1';
    const GOLD_STATUS_CLOSED = '0';
    protected function _construct()
    {
        $this->_init('opentechiz_production/goldcasting');
    }

    protected function _afterLoad()
    {
        parent::_afterLoad();
        $this->setOldData($this->getData());

    }
    protected function _beforeSave(){
        $old_data = $this->getOldData();
        $goldbar_no = array();
        $goldbar_no[] = $this->getData('gold_bar_serial_no') ;
        $goldbar_no[] = $this->getData('gold_bar_serial_no2');
        array_filter($goldbar_no);
        foreach ($goldbar_no as $key => $value) {
            # code...
            if(empty($value))
                continue;
            $goldBar = Mage::getModel('opentechiz_material/goldbar')->loadBySerial($value);
            if($key==1){
                if(isset($gold_id)&&$goldBar->getGoldId() != $gold_id )
                    throw Mage::exception('Mage_Eav', Mage::helper('eav')->__('2 gold bar must same type. Please check.',$value));
                $castingweight = $this->getData('gold_bar_weight_2') - $this->getOldData('gold_bar_weight_2');
            }
            else{
                $gold_id = $goldBar->getGoldId();
                $currentWeight = $this->getData('weight');
                if(!$currentWeight) $currentWeight = 0;
                $currentWeight2 = $this->getData('gold_bar_weight_2');
                if(!$currentWeight2) $currentWeight2 = 0;
                $oldWeight = $this->getOldData('weight');
                if(!$oldWeight) $oldWeight = 0;
                $oldWeight2 = $this->getOldData('gold_bar_weight_2');
                if(!$oldWeight2) $oldWeight2 = 0;
                $castingweight = $currentWeight - $currentWeight2 - $oldWeight + $oldWeight2;
            }
            $total_gold_used = $goldBar->getWeightUse() + $castingweight;
            if($goldBar->getWeight() < $total_gold_used){

                throw Mage::exception('Mage_Eav', Mage::helper('eav')->__('Gold casting weight  more than current gold bar %s. Please check.',$value));
            }
        }

    }
    protected function _afterSave()
    {
        $old_data = $this->getOldData();

        $goldbar_no[] = $old_data['gold_bar_serial_no'];
        $goldbar_no[] = $old_data['gold_bar_serial_no2'];
        $goldbar_no[] = $this->getData('gold_bar_serial_no');
        $goldbar_no[] = $this->getData('gold_bar_serial_no2');

        $this->updateGoldbarWeight($goldbar_no);
        parent::_afterSave();
    }
    public function updateGoldbarWeight($goldbars_no){
        foreach ($goldbars_no as $goldbar_no) {
            $goldCollection1 = Mage::getModel('opentechiz_production/goldcasting')->getCollection();
            $goldCollection1->addFieldToFilter('gold_bar_serial_no', array('eq' => $goldbar_no));
            $total_gold_used = 0;
            foreach ($goldCollection1 as $gold1) {
                $gold1_used =  $gold1->getWeight() - $gold1->getData('gold_bar_weight_2');
                $total_gold_used += $gold1_used;
            }

            $goldCollection2 = Mage::getModel('opentechiz_production/goldcasting')->getCollection();
            $goldCollection2->addFieldToFilter('gold_bar_serial_no2', array('eq' => $goldbar_no));

            foreach ($goldCollection2 as $gold2) {
                $gold2_used =  $gold2->getData('gold_bar_weight_2');
                $total_gold_used += $gold2_used;
            }
            $goldBar = Mage::getModel('opentechiz_material/goldbar')->loadBySerial($goldbar_no);

            $change = $total_gold_used - $goldBar->getWeightUse();
            if($goldBar->getGoldBarId()&&$change){
                Mage::getModel('opentechiz_material/goldbar')->load($goldBar->getGoldBarId())->setWeightUse($total_gold_used)->setUpdatedAt(now())->save();
                $goldBar->updateStatus($goldBar->getGoldBarId());
                $gold_id = $goldBar->getGoldId();

                Mage::getModel('opentechiz_material/gold')->updateWeight($goldBar, $gold_id);
                $admin = Mage::getSingleton('admin/session')->getUser();
                if($admin->getId()){
                    Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                        'user_id' => $admin->getId(),
                        'description' => 'Get weight from gold bar '.$goldbar_no.' for gold casting #'.$this->getData('gold_casting_code'),
                        'qty' => '-'.$change,
                        'on_hold' => '0',
                        'type' => 2,
                        'stock_id' =>$gold_id
                    ));
                }
            }
        }

    }
    public function loadByCode($code)
    {
        return $this->getCollection()->addFieldToFilter('gold_casting_code', $code)->getFirstItem();
    }
    public function isCanEdit()
    {
        if(!is_null($this->getData('can_edit')))
            return $this->getData('can_edit');
        $result = true;
        if(Mage::getModel('opentechiz_production/goldusage')->getCollection()->addFieldToFilter('gold_casting_code', $this->getGoldCastingCode())->getSize() )
            $result = false;
        $this->setData('can_edit', $result);
        return $this->getData('can_edit');
    }
}