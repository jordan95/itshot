<?php

class OpenTechiz_Production_Model_Production_StepProcessing extends Mage_Core_Model_Abstract
{
    public function getFormHtml($status)
    {
        $html = '';
        $html .= '<tr>';
        $html .= '<td class="label"><label><strong>Current process:</strong></label></td>';
        $html .= '<td class="value">' . OpenTechiz_Production_Helper_Data::PROCESS_LIST[$status] . '</td>';
        $html .= '</tr>';

        if ($status <= 25) {
            $html .= '<tr class="' . $this->getGoldHidden($status) . '"><td class="label"><label><strong>Gold weight (gr):</strong></label></td>';
            $html .= '<td class="value"><input name="gold_weight" type="number" step="any" min="0" class="gold-weight-input input-text ' .
                $this->getGoldHidden($status) . '" ' .
                $this->getGoldRequiredAndDisabled($status) .
                '/></td>';
            $html .= '</tr>';

            $html .= '<tr class="' . $this->getStoneHidden($status) . '"><td class="label"><label><strong>Stone weight (gr):</strong></label></td>';
            $html .= '<td class="value"><input name="stone_weight" type="number" step="any" min="0" class="stone-weight-input input-text ' .
                $this->getStoneHidden($status) . '" ' . $this->getStoneRequiredAndDisabled($status) . ' /></td>';
            $html .= '</tr>';
        } else {
            $html .= '<tr class="' . $this->getGoldHidden($status) . '"><td class="label"><label><strong>Item weight (gr):</strong></label></td>';
            $html .= '<td class="value"><input name="item_weight" class="item-weight-input input-text" type="number" step="any" min="0" required';
            /*  if ($status == 45) $html .= 'disabled';*/
            $html .= '/></td>';
            $html .= '</tr>';
        }

        $html .= '<tr>';
        $html .= '<td class="label"><label><strong>Owner:</strong></label></td>';
        $html .= '<td class="value">';
        $html .= '<select name="owner_staff_id" id="owner_staff_id"  required>';
        $html .= '<option value="">Select</option>';
        $html .= $this->getOwnerStaff($status);
        $html .= '</select>';
        $html .= '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td class="label"><label><strong>Staff:</strong></label></td>';
        $html .= '<td class="value">';
        $html .= '<select name="staff_type" id="staff_type" onchange="changeStaffType(this, \''.$status.'\')" required>' . $this->getStaffType() . '</select>';
        $html .= '</td>';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="value">';
        $html .= '<select name="staff_id" id="staff_id" required>';
        $html .= '<option value="">Select</option>';
        $html .= $this->getStaff($status);
        $html .= '</select>';
        $html .= '<p class="note"><span>Person who perform this step.</span></p>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<td class="value" id="staff_id_td">';
        $html .= '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td class="label"></td>';
        $html .= '<td class="value">';
        $html .= '<input type="checkbox" class="checkbox" name="status" onchange="addReasonRequired(this)" value="1"> <label>Broken</label>';
        $html .= '<p class="note"><span>Check if item is broken.</span></p>';
        $html .= '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td class="label"><label><strong>Additional note:</strong></label></td>';
        $html .= '<td class="value">';
        $html .= '<textarea id="add-note" name="note" class="process-note"></textarea>';
        $html .= '</td>';
        $html .= '</tr>';

        if ($status == 5) {
            $html .= '<tr>';
            $html .= '<td class="label"><label><strong>Casting code:</strong></label></td>';
            $html .= '<td class="value">';
            $html .= '<input name="casting_code" class="text-field input-text" required/>';
            $html .= '<p class="note"><span>Code of gold casting which created this ring.</span></p>';
            $html .= '</td>';
            $html .= '</tr>';
        }

        $processes = OpenTechiz_Production_Helper_Data::PROCESSES;
        rsort($processes);

        if ($status == $processes[1]) {
            $html .= '<tr>';
            $html .= '<td class="label"><label><strong>Redo action (Select step to start redo):</strong></label></td>';
            $html .= '<td><select name="redo">';
            $html .= '<option value="">None</option>';
            foreach (Mage::helper('opentechiz_production')->getRedoAbleProcesses() as $value) {
                $html .= '<option value="' . $value . '">' . OpenTechiz_Production_Helper_Data::PROCESS_LIST[$value] . '</option>';
            }
            $html .= '</select>';
            $html .= '<p class="note"><span>Note: if the quality is good, choose "none".</span></p>';
            $html .= '</td>';
            $html .= '</tr>';
        }
        return $html;
    }

    public function getStaff($status){
        $staffModel = Mage::getModel('opentechiz_production/staff');
        $collection = $staffModel->getCollection();
        $html = '';
        foreach ($collection as $staff) {
            $steps = explode(',', $staff->getProductionStep());
            if (in_array($status, $steps) && $staff->getStatus() != 0 && $staff->getStaffType() == 0) {
                $html .= '<option value="' . $staff->getId() . '">' . $staff->getStaffName() . '</option>';
            }
        }
        return $html;
        //change to admin instead of staff
//        $html = '';
//        $user = Mage::getSingleton('admin/session')->getUser();
//        if($user){
//            $html .= $user->getFirstname().' '.$user->getLastname();
//        }
//        return $html;
    }

    public function getOwnerStaff($status){
        $staffModel = Mage::getModel('opentechiz_production/staff');
        $collection = $staffModel->getCollection()->addFieldToFilter('is_owner', 1);
        $html = '';
        foreach ($collection as $staff) {
            $steps = explode(',', $staff->getProductionStep());
            if (in_array($status, $steps) && $staff->getStatus() != 0) {
                $html .= '<option value="' . $staff->getId() . '">' . $staff->getStaffName() . '</option>';
            }
        }
        return $html;
    }

    public function getStaffType(){
        $html = '';
        foreach (OpenTechiz_Production_Helper_Data::STAFF_TYPE as $key => $value){
            if($key == 0){
                $html.= '<option value="'.$key.'" selected>'.$value.'</option>';
            }else{
                $html.= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        return $html;
    }

    public function getGoldRequiredAndDisabled($step)
    {
        $html = '';
        if (in_array($step, OpenTechiz_Production_Helper_Data::GOLD_WEIGHT_DISABLED)) {
            $html .= 'disabled';
        }
        if (in_array($step, OpenTechiz_Production_Helper_Data::GOLD_WEIGHT_REQUIRED)) {
            $html .= 'required';
        }
        return $html;
    }

    public function getGoldHidden($step)
    {
        $html = '';
        if (in_array($step, OpenTechiz_Production_Helper_Data::GOLD_WEIGHT_HIDDEN)) {
            $html .= 'hidden';
        }
        return $html;
    }

    public function getStoneRequiredAndDisabled($step)
    {
        $html = '';
        if (in_array($step, OpenTechiz_Production_Helper_Data::STONE_WEIGHT_DISABLED)) {
            $html .= 'disabled';
        }
        if (in_array($step, OpenTechiz_Production_Helper_Data::STONE_WEIGHT_REQUIRED)) {
            $html .= ' required';
        }
        return $html;
    }

    public function getStoneHidden($step)
    {
        $html = '';
        if (in_array($step, OpenTechiz_Production_Helper_Data::STONE_WEIGHT_HIDDEN)) {
            $html .= 'hidden';
        }
        return $html;
    }

    public function addProductionGoldUse($code, $goldUsed, $itemId)
    {
        $data = [];
        $data['gold_casting_code'] = $code;
        $data['weight_usage'] = $goldUsed;
        $data['personalized_id'] = $itemId;
        try {
            Mage::getModel('opentechiz_production/goldusage')->setData($data)->save();
        } catch (Exception $e) {
            return $e;
        }
    }

    public function changeCurrentStepToNextStep($currentStep, $request)
    {
        if ($request->getProductionType() == 2||$request->getProductionType() == 4 ) {

            $processArray = json_decode(Mage::helper('opentechiz_return')->getProductionStep($request->getId()));

            if ($processArray) {
                $processArray = array_filter($processArray);
                $a = array();
                $processFlip = array_flip(OpenTechiz_Production_Helper_Data::PROCESS_LIST);

                foreach ($processArray as $key => $value) {
                    $a[] = $processFlip[$value];
                }

                $processArray = $a;
            } else {
                $processArray = OpenTechiz_Production_Helper_Data::PROCESSES;
            }
        } elseif($request->getProductionType() == 5){
            $processArray = OpenTechiz_Production_Helper_Data::RESIZE_AND_ENGRAVE_PROCESSES;
        } else {
            $processArray = OpenTechiz_Production_Helper_Data::PROCESSES;
        }
        $index = array_search($currentStep, $processArray);
        if ($index !== false && $index < count($processArray) - 1){
            //increase process step
            $nextStep = $processArray[$index + 1];

            //skip if no need to engrave
            $hasEngraving = true;
            $options = unserialize($request->getOption());

            if(isset($options['options'])) {
                foreach ($options['options'] as $option) {

                    if (strpos(strtolower($option['value']), OpenTechiz_PersonalizedProduct_Helper_Data::DONT_ENGRAVE) !== false) {
                        $hasEngraving = false;
                        break;
                    }
                }
            }

            if($nextStep == 40 && $hasEngraving == false){
                $nextStep = $processArray[$index + 2];
            }
        }

        $data = [];
        $data['status'] = $nextStep;

        $request->addData($data)->setId($request->getId())->save();
    }

    public function saveHistory($request, $data)
    {
        $historyModel = Mage::getModel('opentechiz_production/production_history');
        $history = [];
        $history['personalized_id'] = $request->getId();
        $history['item_id'] = $request->getOrderItemId();
        if ($request->getStatus() <= 25) {
            if(array_key_exists('stone_weight', $data)) {
                $history['stone_weight'] = $data['stone_weight'];
            }else{
                $history['stone_weight'] = 0;
            }
            if(array_key_exists('gold_weight', $data)) {
                $history['gold_weight'] = $data['gold_weight'];
            }else{
                $history['gold_weight'] = 0;
            }

        } else {
            $history['stone_weight'] = 0;
            $history['gold_weight'] = 0;
        }
        $history['step_name'] = OpenTechiz_Production_Helper_Data::PROCESS_LIST[$request->getStatus()];
        if(array_key_exists('staff_id', $data)) {
            $history['staff_id'] = $data['staff_id'];
        }else{
            $history['staff_id'] = 0;
        }
        if(array_key_exists('owner_staff_id', $data)) {
            $history['owner_staff_id'] = $data['owner_staff_id'];
        }
        $history['admin_id'] = Mage::getSingleton('admin/session')->getUser()->getUserId();
        $history['finished_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        if (!array_key_exists('status', $data)) $history['status'] = 0;
        else $history['status'] = 1;
        if(array_key_exists('note', $data)) $history['note'] = $data['note'];
        else $history['note'] = '';

        if ($request->getStatus() == min(OpenTechiz_Production_Helper_Data::PROCESSES)) {
            $history['started_at'] = $request->getCreatedAt();
        } else {
            $lastHistory = $historyModel->getCollection()->addFieldToFilter('personalized_id', $request->getId())
                ->setOrder('id', 'DESC')->getFirstItem();
            $history['started_at'] = $lastHistory->getFinishedAt();
            if (!$history['started_at']) $history['started_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            if (!array_key_exists('stone_weight', $data) && $request->getStatus() > 25) {
                $history['stone_weight'] = $lastHistory->getStoneWeight();
                $history['gold_weight'] = (float)$data['item_weight'] - (float)$history['stone_weight'];
            }
        }
        $historyModel->setData($history)->save();
    }

    public function redoStep($request, $redo, $note = '')
    {
        $data = [];
        $data['status'] = $redo;
        $data['note'] = $note;
        $request->addData($data)->setId($request->getId())->save();

        $historyModel = Mage::getModel('opentechiz_production/production_history');
        $redoneStep = $historyModel->getCollection()->addFieldToFilter('personalized_id', $request->getId())->addFieldToFilter('step_name', OpenTechiz_Production_Helper_Data::PROCESS_LIST[$redo])
            ->addFieldToFilter('status', 0)->setOrder('id', 'DESC')->getFirstItem();
        $data = [];
        $data['status'] = 2;
        $redoneStep->addData($data)->setId($redoneStep->getId())->save();

        $admin = Mage::getSingleton('admin/session')->getUser();
        if ($admin->getId()) {
            Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Request #' . $request->getId() . ' redo step ' . OpenTechiz_Production_Helper_Data::PROCESS_LIST[$redo]));
        }
    }

    public function markAsBroken($request, $note = '')
    {
        $data = [];
        $data['item_state'] = 3;
        $data['note'] = $note;
        $data['finished_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');

        $request->addData($data)->setId($request->getId())->save();

        if($productId = unserialize($request->getOption())['info_buyRequest']['product']) {
            $product = Mage::getModel('catalog/product')->load($productId);
            if (Mage::helper('personalizedproduct')->isPersonalizedProduct($product) && ($request->getProductionType() != 5)) {
                Mage::getModel('opentechiz_material/stoneRequest')->returnStone($request->getId());
            }
        }

        if($quoteProductId = unserialize($request->getOption())['info_buyRequest']['options']['quotation_product_id']){
            Mage::getModel('opentechiz_material/stoneRequest')->returnStone($request->getId());
        }

        $oldBarcode = $request->getBarcode();
        $this->markItemBroken($oldBarcode);
        $itemId = Mage::getModel('opentechiz_inventory/item')->load($oldBarcode, 'barcode')->getId();
        if($itemId) $this->deleteOldItemOrderBind($request->getOrderItemId(), $itemId);

        $type = $request->getProductionType();
        //die();
        if ((int)$type == 2 && !array_key_exists('quotation_product_id', unserialize($request->getOption())['info_buyRequest']['options'])) {
            $returns = Mage::helper('opentechiz_return')->getReturnByBarcode($request->getBarcode());

            if ($returns) {
                $return = $returns->getFirstItem();
                $return->setStatus(46);
                $return->save();
            } else
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('adminhtml')->__(
                        'Item state changed but something wrong. Has more than 1 returns with item barcode'
                    )
                );

            return;
        }

        if((int)$type == 3||$request->getManufactureId() || array_key_exists('quotation_product_id', unserialize($request->getOption())['info_buyRequest']['options'])) {
            $newBarcode = $this->createNewItemForBroken($request, $note);
            if((int)$type == 3){
                $return = Mage::helper('opentechiz_return')->getReturnByProductionId($request->getId());
                if ($return) {
                    $return->setNewItemBarcode($newBarcode);
                    $return->save();
                }
            }
             if((int)$type == 2){
                $returns = Mage::helper('opentechiz_return')->getReturnByBarcode($request->getBarcode());

                if ($returns) {
                    $return = $returns->getFirstItem();
                    $return->setStatus(50);
                    $return->setNewItemBarcode($newBarcode);
                    $return->save();
                } else
                    Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('adminhtml')->__(
                            'Item state changed but something wrong. Has more than 1 returns with item barcode'
                        )
                    );

             }

            $requestModel = Mage::getModel('opentechiz_production/product');
            $requestData['image'] = $request->getImage();
            $requestData['barcode'] = $newBarcode;
            $requestData['order_item_id'] = $request->getOrderItemId();
            $requestData['manufacture_id'] = $request->getManufactureId();
            $requestData['production_type'] = $request->getProductionType();
            $requestData['option'] = $request->getOption();
            $requestData['note'] = $note;
            $requestData['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $requestData['status'] = 1;
            $requestData['item_state'] = 0;
            $requestModel->setData($requestData)->save();

            Mage::helper('opentechiz_production/email')->sendEmailProduction('start', $requestModel->getId());

            $option = unserialize($request->getOption());
            if ($option['info_buyRequest']['options']['quotation_product_id']) {  //stone request for quotation product
                $quoteProductOptionModel = Mage::getModel('opentechiz_quotation/productOption');
                $stoneOptionCollection = $quoteProductOptionModel->getCollection()
                    ->addFieldToFilter('quote_product_id', $option['info_buyRequest']['options']['quotation_product_id'])
                    ->addFieldToFilter('option_type', 'stone');
                Mage::getModel('opentechiz_material/stoneRequest')->addStoneRequestQuotation($requestModel->getId(), $stoneOptionCollection);
            } else if (array_key_exists('product', unserialize($request->getOption())['info_buyRequest'])) {
                Mage::getModel('opentechiz_material/stoneRequest')->addStoneRequest($requestModel->getId(), unserialize($request->getOption())['info_buyRequest']['product'], unserialize($request->getOption())['options']);
            } else {
                $first_key = key(unserialize($request->getOption())['info_buyRequest']['options']);
                $productId = Mage::getModel('catalog/product_option')->load($first_key)->getProductId();
                Mage::getModel('opentechiz_material/stoneRequest')->addStoneRequest($requestModel->getId(), $productId, unserialize($request->getOption())['options']);
            }
        }
        else{
            //create new process request
            $data = [];
            $data['order_item_id'] = $request->getOrderItemId();
            $data['order_id'] = Mage::getModel('sales/order_item')->load($data['order_item_id'])->getOrderId();

            $requestModel = Mage::getModel('opentechiz_production/process_request');
            $requestModel->setData($data)->save();
            $item = Mage::helper('opentechiz_return')->getItembybarcode($request->getBarcode())->getFirstItem();
            $this->deleteOldItemOrderBind($request->getOrderItemId(), $item->getId());
        }
    }

    public function markAsFinished($request)
    {
        $data = [];
        $data['item_state'] = 1;
        $data['finished_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');

        if ($request->getProductionType() == 2 || $request->getProductionType() == 3 || $request->getProductionType() == 5) {
            $return = Mage::helper('opentechiz_return')->getReturnByProductionId($request->getId());

            if ($return&&$return->getId()) {

                switch ($request->getProductionType()) {
                    case 2:
                        $return->setStatus(45);
                        $data['item_state'] = 5;
                        break;
                    case 3:
                        $return->setStatus(55);
                        break;
                    case 5:
                        $return->setStatus(55);
                        break;
                }
                $return->save();
            }
        }

        $request->addData($data)->setId($request->getId())->save();
        if ($request->getOrderItemId()) {
            $orderItemId = $request->getOrderItemId();
            $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);
            if(in_array($request->getProductionType(), array(2,3,4,5))) {
                $barcode = $request->getBarcode();
                //if this production request is resize and engrave, move to item ready
                $item = Mage::getModel('opentechiz_inventory/item')->load($barcode, 'barcode');
                $item->addData(['state' => 10])->setId($item->getId())->save();

                $orderItemId = $request->getOrderItemId();
                $returnModel = Mage::getModel('opentechiz_return/return');
                $return = $returnModel->load($orderItemId, 'order_item_id');
                if ($return && $return->getReturnFeeOrderId()) { //if return order
                    $order = Mage::getModel('sales/order')->loadByIncrementId($return->getReturnFeeOrderId());
                } else { //if standard order
                    $order = Mage::getModel('sales/order')->load($orderItem->getOrderId());
                }

                //if all item is ready, move to billing item ready
                $allItemReady = true;
                $allItemLinked = Mage::getModel('opentechiz_inventory/order')->getCollection()
                    ->join(array('item' => 'opentechiz_inventory/item'), 'main_table.item_id = item.item_id', '*')
                    ->join(array('order_item' => 'sales/order_item'), 'main_table.order_item_id = order_item.item_id', '*')
                    ->addFieldToFilter('order_item.order_id', $order->getId())
                    ;
                foreach ($allItemLinked as $item){
                    if($item->getState() != 10){
                        $allItemReady = false;
                        break;
                    }
                }
                if($allItemReady) {
                    if($order->getOrderType() == 2 && $order->getOrderStage() < 2){
                        Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 2, 17);
                    }
                    //check payment if layaway to change to wait for final payment
                    $totalPaid = Mage::helper('opentechiz_salesextend')->getTotalPaymentRecordForOrder($order);
                    $orderTotal = round($order->getBaseGrandTotal(), 4);
                    $totalPaid = round($totalPaid, 4);

                    //check if admin is logged in
                    $admin = Mage::getSingleton('admin/session')->getUser();
                    $isUser = false;
                    if ($admin && $admin->getId()){
                        $isUser = true;
                    }

                    if ($totalPaid < $orderTotal && $order->getOrderType() == 1) {
                        Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 3, 26, null, $isUser);
                    }else{
                        Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 3, 6, null, $isUser);
                    }
                }

                //delete process request
                $processRequest = Mage::getModel('opentechiz_production/process_request')->getCollection()
                    ->addFieldToFilter('order_item_id', $orderItemId)
                    ->addFieldToFilter('status', 1)->getFirstItem();
                if ($processRequest && $processRequest->getId()) {
                    $processRequest->delete();
                }
            }else{
                //change item state to instock
                
                $barcode = $request->getBarcode();
                $item = Mage::getModel('opentechiz_inventory/item')->load($barcode, 'barcode');
                $sku = $item->getSku();
                $inStock = Mage::getModel('opentechiz_inventory/instock')->load($sku, 'sku');
                $item->addData(['state' => 35])->setId($item->getId())->save();
                //add to stock
                //check if exist in stock
                if ($inStock->getId()) {
                        Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                            'qty' => 1,
                            'on_hold' => '0',
                            'type' => 4,
                            'stock_id' => $inStock->getId(),
                            'qty_before_movement' => $inStock->getQty(),
                            'on_hold_before_movement' => $inStock->getOnHold(),
                            'sku' => $inStock->getSku(),
                            'production_request_id' =>$request->getId()
                        ));
                } //if not create new
                else {
                    $data = [];
                    $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
                    $productSku = $parts[0];
                    $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productSku);
                    $productId = $product->getId();
                    $data['sku'] = $sku;
                    $data['product_id'] = $productId;
                    $data['qty'] = 1;
                    $inStock->setData($data)->save();

                    Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                        'qty' => 1,
                        'on_hold' => '0',
                        'type' => 4,
                        'stock_id' => $inStock->getId(),
                        'qty_before_movement' => 0,
                        'on_hold_before_movement' => 0,
                        'sku' => $inStock->getSku(),
                        'production_request_id' =>$request->getId()
                    ));
                }
                $order = Mage::getModel('sales/order')->load($orderItem->getOrderId());
                Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, 2, 1);
            }
        } else {
            //change item state to instock
            $barcode = $request->getBarcode();
            $item = Mage::getModel('opentechiz_inventory/item')->load($barcode, 'barcode');
            $sku = $item->getSku();
            $inStock = Mage::getModel('opentechiz_inventory/instock')->load($sku, 'sku');
            $item->addData(['state' => 35])->setId($item->getId())->save();
            //check if exist in stock
            if ($inStock->getId()) {
                    Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                        'qty' => 1,
                        'on_hold' => '0',
                        'type' => 4,
                        'stock_id' => $inStock->getId(),
                        'qty_before_movement' => $inStock->getQty(),
                        'on_hold_before_movement' => $inStock->getOnHold(),
                        'sku' => $inStock->getSku(),
                        'production_request_id' =>$request->getId()
                    ));
            } //if not create new
            else {
                $data = [];
                $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
                $productSku = $parts[0];
                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productSku);
                $productId = $product->getId();
                $data['sku'] = $sku;
                $data['product_id'] = $productId;
                $data['qty'] = 1;
                $inStock->setData($data)->save();

                Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                    'qty' => 1,
                    'on_hold' => '0',
                    'type' => 4,
                    'stock_id' => $inStock->getId(),
                    'qty_before_movement' => 0,
                    'on_hold_before_movement' => 0,
                    'sku' => $inStock->getSku(),
                    'production_request_id' =>$request->getId()
                ));
            }
        }
        //set item sku to match option after production
        $productionOptions = unserialize($request->getData()['option']);
        $item = Mage::getModel('opentechiz_inventory/item')->load($barcode, 'barcode');
        $values = array();
        if(array_key_exists('options', $productionOptions)){
            if($request->getProductionType() == 2||$request->getProductionType() == 4) {
                foreach ($productionOptions['options'] as $option){
                    $option_value = Mage::getResourceModel('catalog/product_option_value_collection')
                        ->addFieldToFilter('option_type_id', $option['option_value'])->getFirstItem();
                    if ($sku = $option_value->getData('sku')) {
                        $values[] = $sku;
                    }
                }
            }
            foreach ($productionOptions['options'] as $option){
                if(strpos(strtolower($option['label']), OpenTechiz_PersonalizedProduct_Helper_Data::SIZE) !== false||$request->getProductionType() == 2||$request->getProductionType() == 4){
                    $size = str_replace('(custom)', '', strtolower($option['value']));
                    $size = str_replace(' ', '', $size);
                    $itemSku = $item->getSku();
                    $skuParts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $itemSku);

                    if(count($values)>0) $itemSku = $skuParts[0].OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $values);
                    else {
                        if(count($skuParts) > 1 && is_numeric($skuParts[count($skuParts)-1])) {
                            $skuParts[count($skuParts) - 1] = $size;
                        }
                        $itemSku = implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $skuParts);
                    }
                    
                    $item->setSku($itemSku)->setOptions(serialize($productionOptions))->save();
                    break;
                }
            }
        }
        Mage::helper('opentechiz_production/email')->sendEmailProduction('finish', $request->getId());
    }

    public function markItemBroken($oldBarcode)
    {
        $item = Mage::getModel('opentechiz_inventory/item')->load($oldBarcode, 'barcode');
        $item->addData(['state' => 45, 'updated_at' => Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s')])->setId($item->getId())->save();
    }

    public function createNewItemForBroken($request, $note)
    {
        $itemModel = Mage::getModel('opentechiz_inventory/item');
        $itemData = [];
        if($request->getBarcode()&&$request->getOrderItemId()) {
            $item = Mage::helper('opentechiz_return')->getItembybarcode($request->getBarcode())->getFirstItem();
            $itemData['sku'] = $item->getSku();
            $itemData['name'] = $item->getName();

            $this->deleteOldItemOrderBind($request->getOrderItemId(), $item->getId());
        }
        if($request->getManufactureId()){
            $manufactureModel = Mage::getModel('opentechiz_production/production_manufacture')->load($request->getManufactureId());
            $itemData['sku'] = $manufactureModel->getSku();
            $itemData['name'] = $manufactureModel->getName();
        }
        $itemData['image'] = $request->getImage();
        $itemData['type'] = 0;
        $itemData['options'] = $request->getOption();
        $itemData['note'] = $note;
        $itemData['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $itemData['updated_at'] = $itemData['created_at'];
        $itemData['state'] = 0;

        $itemModel->setData($itemData)->save();
        $id = $itemModel->getId();

        $this->createNewItemOrderBind($request->getOrderItemId(), $id);

        $itemModel->addData(['barcode' => Mage::getModel('opentechiz_inventory/item')->generateBarcode($id)])
            ->setId($id)->save();

        return $itemModel->getBarcode();
    }

    public function createNewItemOrderBind($orderId, $itemId)
    {
        $data = [];
        $data['order_item_id'] = $orderId;
        $data['item_id'] = $itemId;
        Mage::getModel('opentechiz_inventory/order')->setData($data)->save();
    }

    public function deleteOldItemOrderBind($order_item_id, $itemId)
    {
        $orderItems = Mage::getModel('opentechiz_inventory/order')->getCollection()->addFieldToFilter('order_item_id',$order_item_id)->addFieldToFilter('item_id',$itemId);
        foreach ($orderItems as $orderItem) {
            Mage::getModel('opentechiz_inventory/order')->load($orderItem->getId())->delete();
        }
    }
}