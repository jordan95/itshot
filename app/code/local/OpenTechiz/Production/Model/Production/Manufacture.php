<?php

class OpenTechiz_Production_Model_Production_Manufacture extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_production/production_manufacture');
    }

    public function manufactureForm($productId){
        $product = Mage::getModel('catalog/product')->load($productId);
        $html = '';

        $html .= '<input type="hidden" name="product_id" value="'.$productId.'"/>';
        $html .= '<br />';
        $html .= '<table cellspacing="0" class="form-list">';
        $html .= '<tbody>';

        foreach($product->getOptions() as $option) {
            $html .= '<tr>';

            if (strpos(strtolower($option->getTitle()), 'engraving') === false)
            {
                $html .= '<td class="label"><strong>Select ' . $option->getTitle() . ':</strong></td>';
                $html .= '<td class="value">';
                $html .= '<select required name="product_option_'.$option->getId().'_'.$option->getSortOrder().'">';
                $html .= '<option value="">-- Select Option --</option>';
                foreach($option->getValues() as $value) {
                    $html .='<option value="' . $value->getSku() . '">' . $value->getSku() . '</option>';
                }
                $html .= '</select>';
                $html .= '</td>';
            }

            $html .= '</tr>';
        }

        $html .= '<tr>';
        $html .= '<td class="label"><strong>Quantity:</strong></td>';
        $html .= '<td class="value"><input required name="qty" class="text-field input-text" /></td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td class="label"><strong>Note:</strong></td>';
        $html .= '<td class="value"><textarea name="note"></textarea></td>';
        $html .= '</tr>';

        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '<br />';
        return $html;
    }

    public function saveProductRequest($params){
        $itemModel = Mage::getModel('opentechiz_inventory/item');
        $result = [];
        $productId = $params['product_id'];
        $result['info_buyRequest']=[];
        $result['info_buyRequest']['product'] = $productId;
        $product = Mage::getModel('catalog/product')->load($productId);
        $skuOptionArray = [];
        $result['options'] = [];

        foreach($params as $key=>$value){
            $data = [];
            if (strpos($key, 'product_option') !== false) {
                $parts = explode('_', $key);

                foreach($product->getOptions() as $option) {
                    if($option->getId() == $parts[2]) {

                        $data['label'] = $option->getTitle();
                        foreach ($option->getValues() as $optionValue) {
                            if ($optionValue->getSku() == $value) {
                                $data['value'] = $value;
                                $data['option_id'] = $option->getId();
                                $data['option_value'] = $optionValue->getId();
                                $data['option_type'] = $option->getType();
                            }
//                            else {
//                                $data['value'] = $value;
//                                $data['option_id'] = $option->getId();
//                                $data['option_value'] = $optionValue->getId();
//                                $data['option_type'] = $option->getType();
//                            }
                        }


                    }
                }

                $skuOptionArray[$parts[3]] = $value;
                array_push($result['options'], $data);
            }
        }
        $oldSku = $product->getSku();
        $productData = $result;
        $data=[];

        $data['image'] = $itemModel->imageUrlFromOption($productData, $product);

        // convert to lower and remove space
        $newSkuOpt = array();
        foreach ($skuOptionArray as $key=>$item) {
            $newSkuOpt[$key] = strtolower(str_replace(' ', '', $item));
        }

        // remove element empty
        foreach ($newSkuOpt as $key=>$item) {
            if ($item == '') {
                unset($newSkuOpt[$key]);
            }
        }

        $newSkuOpt = implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $newSkuOpt);
        $sku = $oldSku.OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.$newSkuOpt;
        $qty = $params['qty'];
        $note = $params['note'];

        $productionType = 1;

        $result = serialize($result);
        $data['production_type'] = $productionType;
        $data['option'] = $result;
        $data['note'] = $note;
        $data['status'] = OpenTechiz_Production_Helper_Data::PROCESSES[0];
        $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');

        $productRequest = Mage::getModel('opentechiz_production/product');
        $this->saveManufacture($sku, $qty, $product->getName(), $data['image'], $note, $data['option']);
        $manufactureId = $this->getId();
        $data['manufacture_id'] = $manufactureId;
        $data['item_state'] = 0;
        $idArray = [];
        for($i = 0; $i < $qty; $i++){
            $productRequest->setData($data)->save();
            $id = $productRequest->getId();
            array_push($idArray, $id);
            $barcode = $itemModel->createItemWithProductionRequest($data, $productId, $sku);

            $data['barcode'] = $barcode;
            $productRequest->addData($data)->setId($id)->save($data);

            Mage::getModel('opentechiz_material/stoneRequest')->addStoneRequest($id, $productId, $productData['options']);
        }

        Mage::helper('opentechiz_production/email')->sendEmailProduction('start', $idArray);
    }

    public function saveManufacture($sku, $qty, $name, $image, $note, $options){
        $data = [];
        $data['sku'] = $sku;
        $data['qty'] = $qty;
        $data['name'] = $name;
        $data['image'] = $image;
        $data['note'] = $note;
        $data['options'] = $options;
        $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $data['status'] = 0;
        $data['admin_user_id'] = Mage::getSingleton('admin/session')->getUser()->getUserId();

        $this->setData($data)->save();
    }
}