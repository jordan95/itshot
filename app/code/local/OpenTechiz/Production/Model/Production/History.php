<?php

class OpenTechiz_Production_Model_Production_History extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_production/production_history');
    }
}