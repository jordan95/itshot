<?php

class OpenTechiz_Production_Model_Resource_Goldcasting_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_production/goldcasting');
    }

    public function joinGetGoldUsage()
	{
		$select = $this->getSelect();
		$table = new Zend_Db_Expr("(select sum(weight_usage) as total_weight_usage, gold_casting_code from ". $this->getTable('opentechiz_production/goldusage') ." group by gold_casting_code)");
		$gold_table = new Zend_Db_Expr("(SELECT `serial_no`,`gold_type`,`gold_color` FROM ". $this->getTable('opentechiz_material/goldbar') ." join ". $this->getTable('opentechiz_material/gold') ." on ". $this->getTable('opentechiz_material/goldbar') .".`gold_id` = ". $this->getTable('opentechiz_material/gold') .".`gold_id`)"); 
		$select->joinLeft(array('usage'=>$table), "main_table.gold_casting_code = usage.gold_casting_code", array('weight_usage'=>"usage.total_weight_usage"));
		$select->joinLeft(array('gold_table'=>$gold_table), "main_table.gold_bar_serial_no = gold_table.serial_no", array('gold_color'=>"gold_table.gold_color",'gold_type'=>"gold_table.gold_type"));
		
		return $this;
	}
}