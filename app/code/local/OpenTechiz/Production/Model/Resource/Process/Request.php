<?php

class OpenTechiz_Production_Model_Resource_Process_Request extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_production/process_request', 'id');
    }
}