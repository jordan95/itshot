<?php

class OpenTechiz_Production_Model_Resource_Product extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_production/product', 'personalized_id');
    }
}