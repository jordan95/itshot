<?php
class Opentechiz_Production_Model_Resource_Purchase_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct(){
        $this->_init("opentechiz_production/purchase");
    }
}
