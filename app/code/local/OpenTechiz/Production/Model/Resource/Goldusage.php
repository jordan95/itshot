<?php

class OpenTechiz_Production_Model_Resource_Goldusage extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct()
    {     
        $this->_init('opentechiz_production/goldusage', 'id');
    }
	public function getGoldUsaged($object)
	{
		$read = $this->_getReadAdapter();
		$select = $read->select()->from(array('main_table'=>$this->getTable('opentechiz_production/goldusage')), array());
		$select->where('gold_casting_code =?', $object->getGoldCastingCode());
		if($object->getId())
			$select->where('id !=?', $object->getId());

		$select->columns(array('total_weight_usaged'=>"sum(weight_usage)"));

		$data = array_column($read->fetchAssoc($select), 'total_weight_usaged');

		if(isset($data[0]) && is_numeric($data[0]))
			return $data[0];
		return 0;
	}
}