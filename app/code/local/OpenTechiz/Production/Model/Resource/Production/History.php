<?php

class OpenTechiz_Production_Model_Resource_Production_History extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_production/production_history', 'id');
    }
}