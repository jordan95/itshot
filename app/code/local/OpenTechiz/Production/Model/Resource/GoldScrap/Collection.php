<?php

class OpenTechiz_Production_Model_Resource_GoldScrap_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_production/goldScrap');
    }
}