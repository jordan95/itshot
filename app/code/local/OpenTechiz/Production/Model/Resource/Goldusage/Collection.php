<?php
class OpenTechiz_Production_Model_Resource_Goldusage_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
		parent::_construct();
        $this->_init('opentechiz_production/goldusage');
    }

	public function joinGold($array = array('*'))
	{
		$this->getSelect()->join(array('gold'=>$this->getTable('opentechiz_production/goldcasting')), "main_table.gold_casting_code = gold.gold_casting_code", $array);
		return $this;
	}
}