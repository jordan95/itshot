<?php

class OpenTechiz_Production_Model_Goldusage extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('opentechiz_production/goldusage');
    }
	protected function _beforeSave()
    {
		parent::_beforeSave();
		if(!$this->getCreatedAt())
		{
			$this->setCreatedAt(date('y-m-d h:s:m'));
		}
		if(!$this->getUserName())
			$this->setUserName(Mage::getSingleton('admin/session')->getUser()->getName());
		$this->validateGoldWeight();
		return $this;
	}
	public function validateGoldWeight()
	{
		$goldusaged = $this->getResource()->getGoldUsaged($this);
		$weighgtUsaged = $goldusaged + $this->getData('weight_usage');
		$gold = Mage::getModel('opentechiz_production/goldcasting')->loadByCode($this->getGoldCastingCode());
		if(!$gold->getStatus())
		{
			throw Mage::exception('Mage_Eav', Mage::helper('eav')->__("Gold casting has closed: %s. Please check.", $this->getGoldCastingCode()));
		}
		if(!$gold->getWeight() || $gold->getWeight() < $weighgtUsaged + $gold->getWeightScrap())
		{
			throw Mage::exception('Mage_Eav', Mage::helper('eav')->__("Gold used more than current Gold Weight Or Missing item with Degree Number : %s. Please check.", $this->getGoldCastingCode()));
		}
		return true;
	}
	
}