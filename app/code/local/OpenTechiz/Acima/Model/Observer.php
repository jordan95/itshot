<?php

class OpenTechiz_Acima_Model_Observer
{

    public function excludeTax(Varien_Event_Observer $observer){

        
            $quote = $observer->getEvent()->getQuote();
            $payment = $quote->getPayment();
            if($payment){
            	$payment_method  = $payment->getMethod();
            	if($payment_method ==="acimacheckout"){
            		$items = $observer->getEvent()->getQuote()->getAllVisibleItems();
		            foreach($items as $item){
		                $item->getProduct()->setTaxClassId(0); // set tax id in order to exclude tax to order.
		            }
            	}
            	
            }
    	
    }


}
