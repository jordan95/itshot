<?php

class OpenTechiz_LazyImageLoader_Helper_Data extends Mage_Core_Helper_Abstract
{

    const ENABLE_CONFIG_PATH = 'lazyimageloader/general/enable';
    const XML_ENABLE_OPTIMIZATION_MOBILE_CONFIG_PATH = 'lazyimageloader/general/enable_optimization_mobile';
    const PAGE_APPLICABLE_CONFIG_PATH = 'lazyimageloader/general/page_applicable';
    const PAGES_CONFIG_PATH = 'lazyimageloader/general/pages';

    public function isEnabled()
    {
        return Mage::getStoreConfig(self::ENABLE_CONFIG_PATH);
    }

    public function getPageApplicable()
    {
        return Mage::getStoreConfig(self::PAGE_APPLICABLE_CONFIG_PATH);
    }

    public function getPages()
    {
        return explode(',', Mage::getStoreConfig(self::PAGES_CONFIG_PATH));
    }
    
    public function canOptimizationForMobile(){
        return Mage::getModel('lazyimageloader/browser')->isMobile() && $this->isEnabledOptimizationForMobile();
    }
    
    public function isEnabledOptimizationForMobile(){
        return Mage::getStoreConfig(self::XML_ENABLE_OPTIMIZATION_MOBILE_CONFIG_PATH);
    }

    public function canAccessPage($handle)
    {
        $page_applicable = $this->getPageApplicable();

        if ($page_applicable == 'none') {
            return false;
        }

        if ($page_applicable == 'all' || in_array($handle, $this->getPages())) {
            return true;
        }
        return false;
    }
}