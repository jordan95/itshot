<?php
class OpenTechiz_LazyImageLoader_Model_Source_Config_Applicable {
    public function toOptionArray()
    {
        return array(
            array('value' => 'none', 'label' => Mage::helper('lazyimageloader')->__('None')),
            array('value' => 'all', 'label' => Mage::helper('lazyimageloader')->__('All allowed pages')),
            array('value' => 'specific_pages', 'label' => Mage::helper('lazyimageloader')->__('Specific Pages'))
        );
    }
}