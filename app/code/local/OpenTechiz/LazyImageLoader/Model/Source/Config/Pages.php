<?php

class OpenTechiz_LazyImageLoader_Model_Source_Config_Pages
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'cms_index_index', 'label' => Mage::helper('lazyimageloader')->__('Home Page')),
            array('value' => 'catalog_category_view', 'label' => Mage::helper('lazyimageloader')->__('Category Page')),
            array('value' => 'catalog_product_view', 'label' => Mage::helper('lazyimageloader')->__('Product Page')),
        );
    }
}