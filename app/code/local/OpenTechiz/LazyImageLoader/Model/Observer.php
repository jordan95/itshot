<?php

class OpenTechiz_LazyImageLoader_Model_Observer
{


    public function appendClassLazyload(Varien_Event_Observer $observer)
    {
        $controller_action = $observer->getEvent()->getControllerAction();
        $handle = $controller_action->getFullActionName();
        $helper = $this->getHelper();
        $response = $controller_action->getResponse();
        if (!$response) {
            return $this;
        }
        $html = $response->getBody();
        if ($html == '' || !$helper->isEnabled()) return;
        if($helper->canOptimizationForMobile()){
            $html = preg_replace('#<img(.*?)class=["\'](.*?)lazy-hidden-m(.*?)\/?>#i', '', $html);
        }
        
        if ($helper->canAccessPage($handle)) {
            $html = preg_replace('#<img(.*?)class=["\'](.*?)["\'](.*?)\/?>#i', '<img${1} class="${2} lazyload" ${3}/>', $html);
            $html = preg_replace('#<img(.*?)\/?>#i', '<img${1} class="lazyload"/>', $html);
            $html = preg_replace('#<img(.*?)class=["\'](.*?)["\'](.*?)class="lazyload"\/>#i', '<img${1} class="${2}" ${3}/>', $html);
            $html = preg_replace('#<img(.*?)src=["\'](.*?)["\'](.*?)\/?>#i', '<img${1} data-src="${2}" ${3}/>', $html);
        }
        $response->setBody($html);
    }

    /**
     * @return OpenTechiz_LazyImageLoader_Helper_Data
     */
    protected function getHelper() {
        return Mage::helper('lazyimageloader');
    }
}