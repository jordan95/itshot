<?php

class OpenTechiz_Catalog_Block_Meta extends Mage_Core_Block_Template
{
    public function getOpenGraphTags()
    {
        $tags = array();
        if ($this->getRequest()->getControllerName() == 'product') {
            if (!$product = Mage::registry('current_product')) {
                $product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('id'));
            }
            $currentUrl = $product->getProductUrl();
			$tags = array(
				'og:title' => $this->getLayout()->getBlock('head')->getTitle(),
				'og:type' => 'website',
				'og:url' => $currentUrl,
				'og:image' => (isset($product) && $product->getId()) ? $product->getImageUrl() : Mage::helper('fbintegrator')->getStoreLogo(),
				'og:description' => htmlspecialchars(strip_tags((isset($product) && $product->getId()) ? $product->getShortDescription() : $this->getLayout()->getBlock('head')->getTitle())),
				'fb:app_id' => Mage::helper('fbintegrator')->getAppKey(),
				'og:locale' => Mage::getBlockSingleton('fbintegrator/fb')->getLocaleCode(),
			);
        }
        return $tags;
    }
}
