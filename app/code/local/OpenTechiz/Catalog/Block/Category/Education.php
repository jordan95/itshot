<?php

class OpenTechiz_Catalog_Block_Category_Education extends Mage_Catalog_Block_Navigation
{
    public function __construct(array $args)
    {
        parent::__construct($args);
    }

    public function getRootCategory()
    {
        $collection = Mage::getModel('catalog/category')->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('is_active','1')
                    ->addAttributeToFilter('page_layout', 'cms_education_layout');

        $collection->getSelect()->order('level');
        $collection->getSelect()->limit(1);

        return $collection->getFirstItem();
    }

    public function getTreeCategories($parentId, $isChild){
        $allCats = Mage::getModel('catalog/category')->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('is_active','1')
                    ->addAttributeToFilter('parent_id',array('eq' => $parentId));
        $html = '';
        $current = $this->getCurrentCategory();
        $store = Mage::app()->getStore();
        
        $class = ($isChild) ? "sub-cat-list" : "cat-list";
        $html .= '<ul class="'.$class.'">';
        foreach ($allCats as $category) 
        {
            $title = ($category->getH2TagName())? $category->getH2TagName() : $category->getName();
            $categoryUrl = Mage::helper('catalog/category')->getCategoryUrl($category);
            $class = '';
            if ($current->getId() == $category->getId()) {
                $class = 'current';
            }

            $html .= '<li class="'.$class.'">';
            $html .= '<a href="'.$categoryUrl.'">' . $title . '</a>';
            $subcats = $category->getChildren();
            if($subcats != ''){
                $html .= $this->getTreeCategories($category->getId(), true);
            }
            $html .= '</li>';
        }
        $html .= '</ul>';
        return $html;
    }

    /**
     * @return bool|Varien_Data_Collection
     */
    public function getSubCategoryList()
    {
        if (!$this->hasData('sub_category_list')) {
            if ($this->getCurrentCategory()) {
                /** @var $currentCategory Mage_Catalog_Model_Category */
                /** @var Mage_Catalog_Model_Resource_Category_Collection $categories */
                $categories = $this->getCurrentCategory()->getCollection();
                $categories->addAttributeToSelect('url_key')
                    ->addAttributeToSelect('thumbnail')
                    ->addAttributeToSelect('description')
                    ->addAttributeToSelect('name')
                    ->addAttributeToSelect('all_children')
                    ->addAttributeToSelect('is_anchor')
                    ->addAttributeToSelect('intro_text')
                    ->setOrder('position', Varien_Db_Select::SQL_ASC)
                    ->joinUrlRewrite()
                    ->addAttributeToFilter('is_active', 1)
                    ->addIdFilter($this->getCurrentCategory()->getChildren());

            } else {
                $categories = new Varien_Data_Collection();
            }

            $this->setData('sub_category_list',$categories);
            $this->setData('sub_featured_category_list',$featuredCategories);
        }

        return $this->getData('sub_category_list');
    }
    
    /**
     * Enter description here...
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getCurrentCategory()
    {
        if (Mage::registry('current_category')) {
            return parent::getCurrentCategory();
        }

        return $this->getStoreRootCategory();
    }

    /**
     * @return Mage_Catalog_Model_Category
     */
    private function getStoreRootCategory()
    {
        $rootCategoryId = Mage::app()->getStore()->getRootCategoryId();

        return Mage::getModel('catalog/category')->load($rootCategoryId);
    }

}