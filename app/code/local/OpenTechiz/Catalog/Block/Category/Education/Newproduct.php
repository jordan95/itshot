<?php
/**
 * Templete use: app/design/frontend/ml/itshot/template/catalog/product/new_education.phtml
 * Custom setting in tab Custom Design from category type Education layout 3 column
 * 
 */

class OpenTechiz_Catalog_Block_Category_Education_Newproduct extends Medialounge_Catalog_Block_Newproduct
{
    

    public function getCacheKeyInfo()
    {
        $categoryId = $this->getCurrentCategory() ?  $this->getCurrentCategory()->getId() : '';

        return array('NEW_PRODUCT', $categoryId);
    }

    protected function _prepareLayout()
    {
        if (!$this->getCurrentCategory()) {
            return false;
        }

        return parent::_prepareLayout();
    }

    /**
     * Prepare and return product collection
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection|Object|Varien_Data_Collection
     */
    protected function _getProductCollection()
    {
        
        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
            ->addAttributeToSelect('*');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToSort('created_at', 'desc')
            ->setPageSize(15)
            ->setCurPage(1);

        // avoiding product duplication
        $collection->getSelect()->group('e.entity_id');

        return $collection;
    }

    /**
     * @return bool|Mage_Catalog_Model_Category
     */
    public function getCurrentCategory()
    {
        if ($category = Mage::registry('current_category')) {
            return $category;
        }

        return false;
    }

}
