<?php

class OpenTechiz_Catalog_Block_Category_Newproduct extends Medialounge_Catalog_Block_Category_Newproduct
{

    protected function _getProductCollection()
    {
        $collection = parent::_getProductCollection();
        $collection->getSelect()->reset(Zend_Db_Select::ORDER);
        $collection->addAttributeToSort('priority', 'desc');
        $collection->addAttributeToSort('created_at', 'desc');
        return $collection;
    }

}
