<?php

class OpenTechiz_Catalog_Block_Adminhtml_Category_Edit_Form extends Mage_Adminhtml_Block_Catalog_Category_Edit_Form
{

    protected function _afterToHtml($html)
    {
        $text = Mage::helper('core')->jsQuoteEscape(Mage::helper('catalog')->__('Are you sure you want to disable all products in this category?'));
        $resetUrl = $this->getUrl('*/*/edit', array('_current'=>true));
        $html .= <<<EOD
                <script type="text/javascript">
                    function changeStatusAllProducts(url, statusText) {
                        if (confirm('Are you sure you want to '+ statusText +' all products in this category?')) {
                          var messagesContainer = $('messages');
                          new Ajax.Request(url + (url.match(new RegExp("\\\?")) ? '&isAjax=true' : '?isAjax=true'), {
                            onSuccess: function (response) {
                              if (response.responseJSON) {
                                $(messagesContainer).update(response.responseJSON.messages);
                                categoryReset('{$resetUrl}', true);
                              }
                            }
                          });
                        }
                      }
                </script>
EOD;
        return parent::_afterToHtml($html);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $category = $this->getCategory();
        $categoryId = (int) $category->getId();
        if ($categoryId && !in_array($categoryId, $this->getRootIds())) {
            $this->addAdditionalButton('disable_all_product_btn', array(
                'name' => 'disable_all_product_btn',
                'title' => 'Disable All Products',
                'type' => "button",
                'onclick' => "changeStatusAllProducts('" . $this->getUrl('*/*/changeStatusAllProducts', array('_current' => true, 'status' => Mage_Catalog_Model_Product_Status::STATUS_DISABLED)) . "', 'disable')",
                'label' => Mage::helper('catalog')->__('Disable All Products')
                    )
            );
        }
        if ($categoryId && !in_array($categoryId, $this->getRootIds())) {
            $this->addAdditionalButton('enable_all_product_btn', array(
                'name' => 'enable_all_product_btn',
                'title' => 'Enable All Products',
                'type' => "button",
                'onclick' => "changeStatusAllProducts('" . $this->getUrl('*/*/changeStatusAllProducts', array('_current' => true,  'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)) . "', 'enable')",
                'label' => Mage::helper('catalog')->__('Enable All Products')
                    )
            );
        }
        return parent::_prepareLayout();
    }

}
