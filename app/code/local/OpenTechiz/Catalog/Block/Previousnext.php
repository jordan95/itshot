<?php

class OpenTechiz_Catalog_Block_Previousnext extends Medialounge_Catalog_Block_Breadcrumbs
{
    protected $_category = '';
    protected $_previousProduct='';
    protected $_nextProduct = '';
    const DISPLAY_PRODUCT_THUMBNAILS = 'previousnext/general/display_product_thumbnails';
    const PRODUCT_THUMBNAIL_HEIGHT = 'previousnext/general/product_thumbnail_height';
    const PRODUCT_THUMBNAIL_WIDTH = 'previousnext/general/product_thumbnail_width';
    const STRING_ENDING = 'previousnext/general/ending';
    const STRING_LENGTH_BY_DEFAULT = 40;
    const STRING_LENGTH = 'previousnext/general/symbolsnumber';
    const PRODUCT_THUMBNAIL_HEIGHT_BY_DEFAULT = 100;
    const PRODUCT_THUMBNAIL_WIDTH_BY_DEFAULT = 100;
    
    public function getProduct(){
        $product = Mage::registry('current_product');
        if($product){
            return $product;
        }else{
            return false;
        }
        
    }
    protected function getCurrentCategory()
    {
        $category_id =false;
        if(!empty($this->getDefaultBreadcrumbCategories())){
            $_category = $this->getDefaultBreadcrumbCategories()->getData();
            if(!empty($_category) && is_array($_category)){
                $current_category = end($_category);
                $category_id = $current_category['entity_id'];
            }
        }
        
        return $category_id;
        
    }
    public function getPreProduct($productslist,$current_product){
        foreach($productslist as $product)
        {
            if($product->getSku() == $current_product->getSku()){
                continue;
            }
             $_previousProduct = $product;
        }
        return $_previousProduct;
    }
    public function getNextProduct($productslist,$pre_product,$current_product){
        foreach($productslist as $product)
        {
            if($product->getSku() == $current_product->getSku() || $product->getSku() ==$pre_product->getSku()){
                continue;
            }
             $_nextProduct = $product;
        }
        return $_nextProduct;
    }
    protected function getProductLinkUrl($product)
    {
        $additional = array();
        if (!Mage::getStoreConfig(Mage_Catalog_Helper_Product::XML_PATH_PRODUCT_URL_USE_CATEGORY)) {
            $additional['_ignore_category'] = true;
        }
        $url = $product->getUrlModel()->getUrl($product, $additional);
        return $url;
    }
    protected function getProductThumbUrl($product)
    {
        if ($this->isDisplayProductThumbnails()) {
            $thumb = $this->helper('catalog/image')
                ->init(
                    $product,
                    'thumbnail'
                )
                ->resize(
                    $this->getProductThumbnailWidth(),
                    $this->getProductThumbnailHeight()
                )
            ;
            return $thumb;
        }
        return '';
    }
    public function isDisplayProductThumbnails()
    {
        return Mage::getStoreConfig(self::DISPLAY_PRODUCT_THUMBNAILS);
    }
    public function getProductThumbnailHeight()
    {
        $value = (int)Mage::getStoreConfig(self::PRODUCT_THUMBNAIL_HEIGHT);
        if ($value == 0) {
            $value = self::PRODUCT_THUMBNAIL_HEIGHT_BY_DEFAULT;
        }
        return $value;
    }

    public function getProductThumbnailWidth()
    {
        $value = (int)Mage::getStoreConfig(self::PRODUCT_THUMBNAIL_WIDTH);
        if ($value == 0) {
            $value = self::PRODUCT_THUMBNAIL_WIDTH_BY_DEFAULT;
        }
        return $value;
    }
    public function getPreviousProductLabel($product)
    {

        return htmlspecialchars($product->getName());
    }
    public function getProductText($product)
    {

        if ($product) {
            $productName = $product->getName();
            $origLength = strlen($productName);
            $readyLink = mb_substr($productName, 0, $this->getStringLength(), 'utf-8');
            $newLength = strlen($readyLink);

            if ($newLength < $origLength) {
                $readyLink .= Mage::getStoreConfig(self::STRING_ENDING);
            }
        } 
        
        return $readyLink;
    }
    protected function getStringLength()
    {
        $str = (int)Mage::getStoreConfig(self::STRING_LENGTH);
        if ($str == 0) {
            $str = self::STRING_LENGTH_BY_DEFAULT;
        }
        return $str;
    }

}