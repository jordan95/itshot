<?php

class OpenTechiz_Catalog_Block_Product_View_Affirm extends Mage_Catalog_Block_Product_View
{
	public function getCurrentProduct(){
        $product = Mage::registry('product');
        if(!$product || !$product->getId()){
            $params = $this->getRequest()->getParams();
            if(!isset($params['id'])){
                return null;
            }else{
                $productId = $params['id'];
                $product = Mage::getModel('catalog/product')->load($productId);
            }
        }

        return $product;
    }
}
