<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 24/07/2017
 * Time: 16:25
 */

class OpenTechiz_Catalog_Block_Product_View_Attributes extends Medialounge_Catalog_Block_Product_View_Attributes
{
    private static $groups = [
        'Item Information',
        'Diamond Information',
        'Watch Information',
        'Gemstone Information',
        'Eyewear Information',
    ];

    public function getAdditionalGroupAttributes()
    {
        $product = $this->getProduct();
        $setId = $product->getAttributeSetId();

        $list = [];


        if ($setId) {
            $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
                ->setAttributeSetFilter($setId)
                ->addFieldToFilter('attribute_group_name', ['in'=> self::$groups])
                ->setSortOrder()
                ->load();

            foreach ($groupCollection as $group) {
                $data = [];
                $attributes = $product->getAttributes($group->getId(), true);

                foreach ($attributes as $attribute) {
                    if ($attribute->getIsVisibleOnFront()) {
                        $value = $attribute->getFrontend()->getValue($product);

                        if (!$product->hasData($attribute->getAttributeCode())) {
                            $value = Mage::helper('catalog')->__('N/A');
                        } elseif ((string)$value == '') {
                            $value = Mage::helper('catalog')->__('No');
                        } elseif ($attribute->getFrontendInput() == 'price' && is_string($value) && $value !="0") {
                            $value = Mage::app()->getStore()->convertPrice($value, true);
                        }
                        if (is_string($value) && strlen($value) && $value !=="N/A" && $value !=="No" && $value !="0") {

                            if(strlen(trim($value))> 1 && substr(trim($value),0,1) =="0"){
                                if(substr(trim($value), 1,1) =="-" ||substr(trim($value), 1,1) =="." || substr(trim($value), 1,1) =="," || is_numeric(substr(trim($value), 1,1))){
                                    $data[$attribute->getAttributeCode()] = array(
                                        'label' => $attribute->getStoreLabel(),
                                        'value' => $value,
                                        'code'  => $attribute->getAttributeCode()
                                    );
                                }
                            }else{
                                $data[$attribute->getAttributeCode()] = array(
                                    'label' => $attribute->getStoreLabel(),
                                    'value' => $value,
                                    'code'  => $attribute->getAttributeCode()
                                );
                            }
                            
                        }
                    }
                }

                $list[$group->getAttributeGroupName()] =  $data;
            }
        }

        return $list;
    }
    public function setProduct($product){
         $this->_product = $product;
         return $this;     
    }
}