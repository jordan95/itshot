<?php

class OpenTechiz_Catalog_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction() {
        
    }

    public function ajaxGetSubCategoryAction()
    {
        $response = new Varien_Object();
        $canRequest = true;
        $request = $this->getRequest();
        $current_category = $request->getQuery('currentCategory', false);
        $current_page = $request->getQuery('currentPage', 1);
        if(!$current_category){
            $canRequest = false;
        }
        $html  = $this->getLayout()
                ->createBlock('medialounge_catalog/subcategorywithproduct')
                ->setCategoryId($current_category)
                ->setCurrentPage($current_page)
                ->setTemplate('catalog/category/subcategory/list.phtml')
                ->toHtml();
        if($html == ''){
            $canRequest = false;
        }
        if($canRequest){
            $response->setData('nextPage', $current_page + 1);
        }
        $response->setData('html', $html);
        $response->setData('canRequest', $canRequest);
        
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($response->toJson());
    }
    protected function ajaxGetProductInfoAction()
    {
        $response = new Varien_Object();
        $request = $this->getRequest();
        $product_id = $request->getParam('product_id');
        $product = Mage::getModel('catalog/product')->load($product_id); 
        $html  = $this->getLayout()
                ->createBlock('catalog/product_view_attributes')
                ->setProduct($product)
                ->setTemplate('catalog/product/view/attributes.phtml')
                ->toHtml();
        $response->setData('html', $html);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($response->toJson());
    }
}
