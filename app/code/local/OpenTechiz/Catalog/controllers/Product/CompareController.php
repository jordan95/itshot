<?php


/**
 * Catalog comapare controller
 *
 * @category   Mage
 * @package    OpenTechiz_Catalog
 * @author     JordanTran <jordan@onlinebizsoft.com>
 */
require_once(Mage::getModuleDir('controllers','Mage_Catalog').DS.'Product'.DS.'CompareController.php');
class OpenTechiz_Catalog_Product_CompareController extends Mage_Catalog_Product_CompareController
{
 
    public function addAction()
    {
        if (!$this->_validateFormKey()) {
            $this->_redirectReferer();
            return;
        }

        $productId = (int) $this->getRequest()->getParam('product');
        if ($this->isProductAvailable($productId)
            && (Mage::getSingleton('log/visitor')->getId() || Mage::getSingleton('customer/session')->isLoggedIn())
        ) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);

            if ($product->getId()/* && !$product->isSuper()*/) {
                Mage::getSingleton('catalog/product_compare_list')->addProduct($product);
                Mage::getSingleton('catalog/session')->addSuccess(
                    $this->__('%s has been added to comparison list. <a href="%s" class="view-list-compare js-compare"> View list here.</a>', Mage::helper('core')->escapeHtml($product->getName()), Mage::helper('medialounge_catalog/compared')->getListUrl())
                );
                Mage::dispatchEvent('catalog_product_compare_add_product', array('product'=>$product));
            }

            Mage::helper('catalog/product_compare')->calculate();
        }

        $this->_redirectReferer();
    }

}
