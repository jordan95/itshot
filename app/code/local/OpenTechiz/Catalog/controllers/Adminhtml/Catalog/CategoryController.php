<?php

require_once 'Mage' . DS . 'Adminhtml' . DS . 'controllers' . DS . 'Catalog' . DS . 'CategoryController.php';

class OpenTechiz_Catalog_Adminhtml_Catalog_CategoryController extends Mage_Adminhtml_Catalog_CategoryController
{

    public function changeStatusAllProductsAction()
    {
        if (!$category = $this->_initCategory()) {
            return;
        }
        $response = new Varien_Object();
        $category = Mage::getModel('catalog/category')->load($category->getId());
        $status = $this->getRequest()->getParam('status');
        $categoryIds = $this->_filterCategoryIds(array($category->getId()));
        $productIds = $this->getProductIdsByCategoryIds($categoryIds);
        Mage::getSingleton('catalog/product_action')
                ->updateAttributes($productIds, array('status' => $status), 1);
        $category->setData('is_active', $status == Mage_Catalog_Model_Product_Status::STATUS_ENABLED ? 1 : 0 );
        $category->save();
        $statusText = $status == Mage_Catalog_Model_Product_Status::STATUS_ENABLED ? 'enabled' : 'disabled';
        $msg = $this->__('Total of %d product(s) have been %s.', count($productIds), $statusText);
        $response->setData('messages', '<ul class="messages"><li class="success-msg"><ul><li><span>' . $msg . '</span></li></ul></li></ul>');
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($response->toJson());
    }

    public function getProductIdsByCategoryIds($categoryIds = array())
    {
        $productIds = array();
        $sql = '';
        if (count($categoryIds) > 0) {
            $sql = 'SELECT DISTINCT(a.product_id)';
            $sql .= ' FROM ' . $this->getTableName('catalog_category_product') . ' AS a';
            $sql .= ' WHERE a.category_id IN(' . join(',', $categoryIds) . ')';
            $productIds = $this->queryIds($sql);
        }
        return $productIds;
    }

    public function _filterCategoryIds(array $ids)
    {
        $filteredCategoryIds = array();
        foreach ($ids as $categoryId) {

            if ($categoryId == Mage::app()->getStore()->getRootCategoryId()) {
                continue;
            }
            $_category = Mage::getModel('catalog/category')->load($categoryId);

            if ($_category->getIsAnchor()) {
                $categoryChildren = $_category->getAllChildren(true);

                $key = array_search($categoryId, $categoryChildren);
                if ($key == false) {
                    unset($categoryChildren[$key]);
                }

                $intersect = array_intersect($ids, $categoryChildren);
                if (count($intersect)) {
                    continue;
                }
            }
            $filteredCategoryIds[] = $categoryId;
        }
        return $filteredCategoryIds;
    }

    public function getTableName($table)
    {
        return $this->getResource()->getTableName($table);
    }

    public function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    public function getConnection()
    {
        return $this->getResource()->getConnection('core_read');
    }

    public function queryIds($sql)
    {
        return $this->getConnection()->fetchCol($sql);
    }

}
