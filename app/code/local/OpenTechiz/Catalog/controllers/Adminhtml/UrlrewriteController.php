<?php


/**
 * Urlrewrites adminhtml controller
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Jordan <jordan@onlinebizsoft.com>
 */
require_once 'Mage' . DS . 'Adminhtml' . DS . 'controllers' . DS . 'UrlrewriteController.php';
class OpenTechiz_Catalog_Adminhtml_UrlrewriteController extends Mage_Adminhtml_UrlrewriteController
{
   
    public function saveAction()
    {
        $this->_initRegistry();

        if ($data = $this->getRequest()->getPost()) {
            $session = Mage::getSingleton('adminhtml/session');
            try {
                // set basic urlrewrite data
                $model = Mage::registry('current_urlrewrite');

                // Validate request path
                $requestPath = $this->getRequest()->getParam('request_path');
                Mage::helper('core/url_rewrite')->validateRequestPath($requestPath);

                // Proceed and save request
                $model->setIdPath($this->getRequest()->getParam('id_path'))
                    ->setTargetPath($this->getRequest()->getParam('target_path'))
                    ->setOptions($this->getRequest()->getParam('options'))
                    ->setDescription($this->getRequest()->getParam('description'))
                    ->setModifiedDate(date('m/d/Y H:i:s'))
                    ->setRequestPath(strtolower($requestPath));

                if (!$model->getId()) {
                    $model->setIsSystem(0);
                }
                if (!$model->getIsSystem()) {
                    $model->setStoreId($this->getRequest()->getParam('store_id', 0));
                }

                // override urlrewrite data, basing on current registry combination
                $category = Mage::registry('current_category')->getId() ? Mage::registry('current_category') : null;
                if ($category) {
                    $model->setCategoryId($category->getId());
                }
                $product  = Mage::registry('current_product')->getId() ? Mage::registry('current_product') : null;
                if ($product) {
                    $model->setProductId($product->getId());
                }
                if ($product || $category) {
                    $catalogUrlModel = Mage::getSingleton('catalog/url');
                    $idPath = $catalogUrlModel->generatePath('id', $product, $category);

                    // if redirect specified try to find friendly URL
                    $found = false;
                    if (in_array($model->getOptions(), array('R', 'RP'))) {
                        $rewrite = Mage::getResourceModel('catalog/url')
                            ->getRewriteByIdPath($idPath, $model->getStoreId());
                        if (!$rewrite) {
                            $exceptionTxt = 'Chosen product does not associated with the chosen store or category.';
                            Mage::throwException($exceptionTxt);
                        }
                        if($rewrite->getId() && $rewrite->getId() != $model->getId()) {
                            $model->setIdPath($idPath);
                            $model->setTargetPath($rewrite->getRequestPath());
                            $found = true;
                        }
                    }

                    if (!$found) {
                        $model->setIdPath($idPath);
                        $model->setTargetPath($catalogUrlModel->generatePath('target', $product, $category));
                    }
                }

                // save and redirect
                $model->save();
                $session->addSuccess(Mage::helper('adminhtml')->__('The URL Rewrite has been saved.'));
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $session->addError($e->getMessage())
                    ->setUrlrewriteData($data);
            } catch (Exception $e) {
                $session->addException($e, Mage::helper('adminhtml')->__('An error occurred while saving URL Rewrite.'))
                    ->setUrlrewriteData($data);
                // return intentionally omitted
            }
        }
        $this->_redirectReferer();
    }

}
