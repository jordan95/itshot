<?php

class OpenTechiz_Catalog_Model_Observer
{

    public function productEditable(Varien_Event_Observer $observer)
    {
        $form = $observer->getForm();
        $elements = $form->getElements();
        foreach ($elements as $elm) {
            /* @var $elm Varien_Data_Form_Element_Fieldset */
            foreach ($elm->getElements() as $e) {
                /* @var $e Varien_Data_Form_Element_Text */
                if(!Mage::helper("opentechizcatalog")->isEditable()) {
                    if(!in_array($e->getId(), Mage::helper("opentechizcatalog")->getFieldsEditable())) {
                        if($e->getId() == "sku") {
                            $e->setReadonly(true, false);
                        } else {
                            $e->setReadonly(true, true);
                        }
                    }
                }
            }
        }
    }

    public function redirectCategoryDisable($observer)
    {
        $controller_action = $observer->getControllerAction();
        $categoryId = (int) $controller_action->getRequest()->getParam('id', false);
        if (!$categoryId) {
            return false;
        }
        $category = Mage::getModel('catalog/category')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($categoryId);
        if (!Mage::helper('catalog/category')->canShow($category)) {
            $categoryParent = $category->getParentCategory();
            if ($categoryParent->getLevel() > 1) {
                $redirect_url = $categoryParent->getUrl();
            } else {
                $redirect_url = Mage::getBaseUrl();
            }
            $configPath = 'sitemap_enhanced_plus/general/category_filter';
            $category_filter = Mage::getStoreConfig($configPath);
            if ($category_filter) {
                $category_filters = explode(',', $category_filter);
                if (!in_array($category->getId(), $category_filters)) {
                    $category_filters[] = $category->getId();
                }
                Mage::getConfig()->saveConfig($configPath, join(',', $category_filters));
            }
            $response = Mage::app()->getFrontController()->getResponse();
            $response->setRedirect($redirect_url, 301);
            $response->sendResponse();
            exit();
        }
    }

    public function addCreatedAtForProductCreate($observer){
        $product = $observer->getProduct();
        
        if(!$product->getCreatedAt() || !strtotime($product->getCreatedAt())){
            $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');

            $product->setCreatedAt($date);
        }
    }

     public function removeAllSpaceForSku($observer)
    {
        $product = $observer->getProduct();
        $sku = $product->getSku();
        if ($sku) {
            $product->setSku(preg_replace('/\s+/', '', $sku));
        }
    }

    public function changeUpdatedAt($observer){

        $product = $observer->getProduct();
        $change_category = $product->getCategoryIds();
        if(!empty($change_category)){

            $current_product = Mage::getModel('catalog/product')->load($product->getId());
            $current_category = $current_product->getCategoryIds();
            $category_id_added = array_diff($change_category,$current_category);
            if(!empty($category_id_added)){
                foreach($category_id_added as $cate_id){
                    $cate = Mage::getModel('catalog/category')->load($cate_id);
                    $cate->setUpdatedAt(date('Y-m-d H:i:s'));
                    $cate->save();
                }
               
            }
        }
        
    }

}
