<?php

class OpenTechiz_Catalog_Model_Cron_Popularity
{
    const ATTRIBUTEID_C2C_POPULARITY = 2056;
    public function run()
    {
        $this->changeProductToNewPosition();
        $this->changeProductToOldPosition();
    }
    
    private function getResource(){
        return Mage::getSingleton('core/resource');
    }
    
    private function getConnection(){
        return $this->getResource()->getConnection('core_write');
    }
    
    private function updateProductPopularity($productId, $popularity){
        $conn = $this->getConnection();
        $attr = self::ATTRIBUTEID_C2C_POPULARITY;
        $query = "UPDATE `tsht_catalog_product_entity_int` SET `value` = '{$popularity}' WHERE   `attribute_id` = {$attr} AND `entity_id` = {$productId}";
        $conn->query($query);
    }

    private function changeProductToNewPosition()
    {
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->addAttributeToSelect('c2c_popularity')
                ->addAttributeToFilter('c2c_distributor', 4971) // Distributor ItsHot
                ->addAttributeToFilter('c2c_popularity', array('gt' => 0));
        $conn = $this->getConnection();
        if ($collection->count() > 0) {
            foreach ($collection as $item) {
                $new_c2c_popularity = 0 - $item->getData('c2c_popularity');
                $this->updateProductPopularity($item->getId(), $new_c2c_popularity);
            }
        }
    }

    private function changeProductToOldPosition()
    {
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->addAttributeToSelect('c2c_popularity')
                ->addAttributeToFilter('c2c_distributor', 4971) // Distributor ItsHot
                ->addAttributeToFilter('c2c_popularity', array('lt' => 0));
        if ($collection->count() > 0) {
            foreach ($collection as $item) {
                $new_c2c_popularity = abs($item->getData('c2c_popularity'));
                $this->updateProductPopularity($item->getId(), $new_c2c_popularity);
            }
        }
    }

}
