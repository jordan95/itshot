<?php

class OpenTechiz_Catalog_Model_Cron_Review
{

    public function reportNewestReviewDateForCategory()
    {
        Mage::app()->setCurrentStore('default');
        $activeCategories = array();
        foreach ($this->getStoreCategories() as $child) {
            if ($child->getIsActive() && $this->_hasProducts($child->getId())) {
                $activeCategories[] = $child;
            }
        }
        $activeCategoriesCount = count($activeCategories);
        $hasActiveCategoriesCount = ($activeCategoriesCount > 0);

        if (!$hasActiveCategoriesCount) {
            return '';
        }

        $message = <<<EOD
                
                <table>
                    <tr>
                        <th>Category Name</th>
                        <th>URL</th>
                        <th>Review Date</th>
                    </tr>
EOD;
        $data = array();
        foreach ($activeCategories as $category) {
            $this->_renderCategoryMenuItemHtml($data, $category);
        }
        usort($data, function ($a, $b){
            return $a['created_at'] > $b['created_at'];
        });
        foreach ($data as $item){
            $message .= $item['html'];
        }
        $message .= '</table>';
        $message .= "<br/><br/>Script ends at (US Time): " . Mage::getModel('core/date')->date('M d, Y H:i:s');
        Mage::dispatchEvent('maillog_send_email_to_admin', array(
            'message' => $message,
            'subject' => 'ItsHot.com: Cronjob Report Newest Review Date Weekly'
                )
        );
    }

    public function _hasProducts($categoryID)
    {
        return $this->getHelper()->_hasProducts($categoryID);
    }

    public function getHelper()
    {
        return Mage::helper('opentechizcatalog');
    }

    protected function _renderCategoryMenuItemHtml(&$data, $category, $level = 0, $parentName = "")
    {
        if (!$category->getIsActive()) {
            return '';
        }
        $html = "";

        // get all children
        // If Flat Data enabled then use it but only on frontend
        $flatHelper = Mage::helper('catalog/category_flat');
        if ($flatHelper->isAvailable() && $flatHelper->isBuilt(true) && !Mage::app()->getStore()->isAdmin()) {
            $children = (array) $category->getChildrenNodes();
            $childrenCount = count($children);
        } else {
            $children = $category->getChildren();
            $childrenCount = $children->count();
        }
        // select active children
        $activeChildren = array();
        foreach ($children as $child) {
            if ($child->getIsActive() && $this->_hasProducts($child->getId())) {
                $activeChildren[] = $child;
            }
        }

        $catName = $this->escapeHtml($category->getName());
        if ($parentName) {
            $catName = $parentName . '/' . $catName;
        }
        
        $result= $this->getNewestReviewDate($category);
        $created_at = 0;
        $createdStr = 'N/A';
        if($result){
            $created_at = strtotime($result["created_at"]);
            $date = new Zend_Date($created_at);
            $createdStr = Mage::getModel('core/date')->date('M d, Y', $date);
            $createdStr = $createdStr . " (ID: " . $result['review_id'] . ")";
        }

        // assemble list item with attributes
        $html = "<tr>";
        $html .= "<td>" . $catName . "</td>";
        $html .= "<td>" . $this->getCategoryUrl($category) . "</td>";
        $html .= "<td>" . $createdStr . "</td>";
        $html .= "</tr>";
        $data[] = [
            'html' => $html,
            'created_at' => $created_at
        ];
        // render children
        foreach ($activeChildren as $child) {
            $this->_renderCategoryMenuItemHtml(
                    $data, $child, ($level + 1), $catName
            );
        }
    }

    public function getNewestReviewDate($category)
    {
        $catIDs = $category->getAllChildren(true);
        if (count($catIDs) == 0) {
            return "";
        }
        $query = $this->getConnection()->select()
                ->from(array('e' => $this->getResource()->getTableName('catalog/category_product')), array('product_id'))
                ->where('e.category_id IN (?)', $catIDs);

        $productIDs = $this->queryIds($query);
        if (count($productIDs) === 0) {
            return "";
        }
        $query2 = $this->getConnection()->select()
                ->from(array('e' => $this->getResource()->getTableName('review/review')), array('review_id', 'created_at'))
                ->where('e.status_id = ?', Mage_Review_Model_Review::STATUS_APPROVED)
                ->where('e.entity_id = ?', Mage_Review_Model_Review::ENTITY_PRODUCT)
                ->where('e.entity_pk_value IN (?)', $productIDs)
                ->order("e.created_at DESC")
                ->limit(1);

        return $this->getConnection()->fetchRow($query2);
    }

    public function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    public function getConnection()
    {
        return $this->getResource()->getConnection('core_read');
    }

    public function queryIds($sql)
    {
        return $this->getConnection()->fetchCol($sql);
    }

    public function escapeHtml($data, $allowedTags = null)
    {
        return Mage::helper('core')->escapeHtml($data, $allowedTags);
    }

    public function getCategoryUrl($category)
    {
        if ($category instanceof Mage_Catalog_Model_Category) {
            $url = $category->getUrl();
        } else {
            $url = $this->_getCategoryInstance()
                    ->setData($category->getData())
                    ->getUrl();
        }

        return $url;
    }

    public function getStoreCategories()
    {
        $helper = Mage::helper('catalog/category');
        return $helper->getStoreCategories();
    }

}
