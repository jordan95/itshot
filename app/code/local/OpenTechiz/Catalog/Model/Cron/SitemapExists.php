<?php

class OpenTechiz_Catalog_Model_Cron_SitemapExists
{

    const SITEMAP_CRONJOB_CODE = 'sitemapEnhancedPlus_generate';

    public function run()
    {
        $sitemaps = array(
            'sitemap_index_index.xml'
        );
        $sitemap_dir = $this->getSitemapsDir();
        $notify = false;
        foreach ($sitemaps as $sitemap) {
            if (!$this->sitemapExists($sitemap_dir . $sitemap) || !$this->sitemapGenerated()) {
                $notify = true;
                break;
            }
        }
        if ($notify) {
            $this->notifyAdmin(
                    'Sitemap is not exists or not genreated.<br/><br/>Script ends at: ' . date("d-m-Y H:i:s"), 'ItsHot.com:  Sitemap is not exists or not genreated.'
            );
        }
    }

    protected function sitemapExists($filename)
    {
        return file_exists($filename);
    }

    protected function sitemapGenerated()
    {
        $collection = $this->getSitemapCollection();
        return $collection->count();
    }

    protected function notifyAdmin($message, $subject)
    {
        Mage::dispatchEvent('maillog_send_email_to_admin', array(
            'message' => $message,
            'subject' => $subject
                )
        );
    }

    public function getSitemapCollection()
    {
        /** @var Mage_Cron_Model_Resource_Schedule_Collection $collection */
        $collection = Mage::getModel('cron/schedule')->getCollection();
        $collection->addFieldToFilter('status', 'success')
                ->addFieldToFilter('job_code', self::SITEMAP_CRONJOB_CODE)
                ->addFieldToFilter('finished_at', array('gteq' => date('Y-m-d')));
        $select = $collection->getSelect();
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->columns('schedule_id');
        $select->limit(1);
        return $collection;
    }

    protected function getSitemapsDir()
    {
        return Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . 'sitemaps' . DIRECTORY_SEPARATOR;
    }

}
