<?php

class OpenTechiz_Catalog_Model_Cron_ExcludeCategoryInMenu
{

    const XML_EXCLUDE_CATEGORY_IN_MENU_PATH = "catalog/breadcrumb/exclude_categories_in_menu";

    public function run()
    {
        $collection = $this->getCategoryCollection();
        $category_filters = array();
        $new_exclude_categories = array();
        $remove_exclude_categories = array();
        $categorieIDs = $this->getConfigExcludeCategoryIds();
        foreach ($collection as $_category) {
            if (!Mage::helper('opentechizcatalog')->_hasProducts($_category->getId())) {
                $new_exclude_categories[] = $_category->getId();
            } elseif (in_array($_category->getId(), $categorieIDs)) {
                $remove_exclude_categories[] = $_category->getId();
            }
        }

        if (count($new_exclude_categories) > 0) {
            $categorieIDs = array_unique(array_merge($categorieIDs, $new_exclude_categories));
            $this->updateConfigInMenu($new_exclude_categories, 0);
        }

        if (count($remove_exclude_categories) > 0) {
            $categorieIDs = array_diff($categorieIDs, $remove_exclude_categories);
            $this->updateConfigInMenu($remove_exclude_categories, 1);
        }
        Mage::getConfig()->saveConfig(self::XML_EXCLUDE_CATEGORY_IN_MENU_PATH, join(",", $categorieIDs));
    }

    public function updateConfigInMenu($categoryIDs, $include_in_menu)
    {
        $this->getConnection()->query("UPDATE `tsht_catalog_category_entity_int` SET `value` = {$include_in_menu} WHERE `store_id`=0 AND `attribute_id`=67 AND `entity_id` IN (" . join(",", $categoryIDs) . ")");
        if (Mage::helper('catalog/category_flat')->isEnabled()) {
            $this->getConnection()->query("UPDATE `tsht_catalog_category_flat_store_1` SET `include_in_menu` = {$include_in_menu} WHERE `entity_id` IN (" . join(",", $categoryIDs) . ")");
        }
    }

    private function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    private function getConnection()
    {
        return $this->getResource()->getConnection('core_write');
    }

    public function getConfigExcludeCategoryIds()
    {
        if (Mage::getStoreConfig(self::XML_EXCLUDE_CATEGORY_IN_MENU_PATH)) {
            return preg_split('/[\s,]+/', Mage::getStoreConfig(self::XML_EXCLUDE_CATEGORY_IN_MENU_PATH));
        }
        return array();
    }

    public function getCategoryCollection()
    {
        $categoryIDs = $this->getConfigExcludeCategoryIds();
        $collection = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect("*")
                ->addIsActiveFilter()
                ->addAttributeToFilter('display_mode', array("PRODUCTS", "PRODUCTS_AND_PAGE"));
        $collection->addAttributeToFilter("include_in_menu", 1);
        if (count($categoryIDs) > 0) {
            $select = $collection->getSelect();
            $select->orWhere("e.entity_id IN (?)", array($categoryIDs));
        }
        return $collection;
    }

}
