<?php

class OpenTechiz_Catalog_Model_Cron_Video
{

    const ATTRIBUTE_ID_IS_ACTIVE = 42;

    public function run()
    {
        $this->generate();
    }

    protected function notifyAdmin($message, $subject)
    {
        Mage::dispatchEvent('maillog_send_email_to_admin', array(
            'message' => $message,
            'subject' => $subject
                )
        );
    }

    protected function saveXml($filename)
    {
        Mage::helper('feedexport/io')->write($filename, $this->toXml());
    }

    private function renderVideoItemXml($title, $description, $video_url, $video_thumb_url, $product_url)
    {
        return '<url><loc>'.$product_url.'</loc><video:video><video:title><![CDATA['.$title.']]></video:title><video:description><![CDATA['.$description.']]></video:description><video:content_loc>'.$video_url.'</video:content_loc><video:thumbnail_loc>'.$video_thumb_url.'</video:thumbnail_loc><video:family_friendly>yes</video:family_friendly></video:video></url>';
    }
    
    protected function getItemCode($product) {
        return $product->getData('sku');
    }

    private function getVideoXml()
    {
        $xml = '';
        $collection = $this->getProductVideoCollection();
        if ($collection->count() > 0) {
			$missing = [];
            foreach ($collection as $productVideo) {
                $filename = $this->getVideoFilename($productVideo);
                if (!$filename || !$this->videoExists($filename)) {
					$missing[] = array(
						$productVideo->getData('sku'),
						$productVideo->getProductUrl(),
						$productVideo->getData('c2c_video_object'),
					);
                    continue;
                }
                $itemCode = $this->getItemCode($productVideo);
                $title = $productVideo->getName();
                if($itemCode){
                    $title.= '(Item Code: '.$itemCode.')';
                }
                $description = $this->getVideoDescription($productVideo);
                $video_url = $this->getVideoUrl($productVideo);
                $video_thumb_url = $this->getVideoThumbUrl($productVideo);
                $product_url = $productVideo->getProductUrl();
                $xml .= $this->renderVideoItemXml($title, $description, $video_url, $video_thumb_url, $product_url);
            }
			if(count($missing) > 0){
				$this->saveVideoMissingCsv($missing);
			}
        }
        return $xml;
    }

    protected function getVideoPath($product)
    {
        if ($product->getData('video_path')) {
            return $product->getData('video_path');
        }
        $videoDir = $this->getVideoDir();
        $url_key = $product->getData('url_key');
        $filename = '';
        if (file_exists($videoDir . $url_key . '.mp4')) {
            $filename = $url_key . '.mp4';
        } elseif (file_exists($videoDir . $url_key . '.wmv')) {
            $filename = $url_key . '.wmv';
        } elseif (file_exists($videoDir . $url_key . '.avi')) {
            $filename = $url_key . '.avi';
        } elseif (file_exists($videoDir . $url_key . '.flv')) {
            $filename = $url_key . '.flv';
        }
        return $filename;
    }

    protected function getVideoFilename($product)
    {
        $videoDir = $this->getVideoDir();
        $videoPath = $this->getVideoPath($product);
        return $videoPath ? $videoDir . $videoPath : false;
    }

    protected function getCategoryId($productId)
    {
        $conn = $this->getConnection();
        $catSQL = "SELECT category_id FROM " . $this->getTableName('catalog_category_product') . " WHERE product_id=" . $productId;
        $categories = $conn->fetchCol($catSQL);
        $catId = -1;
        if (count($categories) > 0) {
            $catSQL2 = "SELECT `entity_id` FROM " . $this->getTableName('catalog_category_entity_int') . " where `attribute_id`=" . self::ATTRIBUTE_ID_IS_ACTIVE . " AND `entity_id` IN(" . join(',', $categories) . ") LIMIT 1";
            $catStrActive = $conn->fetchCol($catSQL2);
            if (count($catStrActive) > 0) {
                $catId = $catStrActive[0];
            }
        }

        return $catId;
    }

    protected function getCategory($productId)
    {
        $catId = $this->getCategoryId($productId);
        return Mage::getModel('catalog/category')->load($catId);
    }

    protected function getVideoDescription($product)
    {
        return $product->getDescription();
    }

    public function getVideoUrl($product)
    {
        $path = $this->getVideoPath($product);
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product/video/' . $path;
    }

    protected function getVideoThumbUrl($product)
    {
        $productMediaConfig = Mage::getModel('catalog/product_media_config');
        return $productMediaConfig->getMediaUrl($product->getImage());
    }

    public function toXml()
    {
        $videoXml = $this->getVideoXml();
        return '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">'.$videoXml.'</urlset>';
    }

    protected function videoExists($filename)
    {
        return file_exists($filename);
    }

    protected function getVideoDir()
    {
        return Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'catalog' . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR;
    }

    public function generate($filename = null)
    {
        if (!$filename) {
            $filename = Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . 'sitemaps' . DIRECTORY_SEPARATOR . 'video.xml';
        }
        $this->saveXml($filename);
        $this->notifyAdmin(
                'Video Feed Export Completed.<br/><br/>Script ends at: ' . date("d-m-Y H:i:s"), 
                'ItsHot.com: Cronjob Video Feed Export Completed'
        );
    }

    public function getProductVideoCollection()
    {
        /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        Mage::app()->setCurrentStore(1);
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addUrlRewrite()
                ->addStoreFilter();
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collection->addAttributeToSelect($attributes)
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('c2c_video_object')
                ->addAttributeToSelect('description')
                ->addAttributeToFilter('c2c_video_object', array('notnull' => true))
                ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                ->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        //Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
        $collection->getSelect()->joinLeft(array('v' => 'tmp_videos'), 'e.entity_id = v.productid');
		$collection->getSelect()->columns('sku');
        return $collection;
    }

    public function getTableName($table)
    {
        return $this->getResource()->getTableName($table);
    }

    public function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    public function getConnection()
    {
        return $this->getResource()->getConnection('core_read');
    }
	
	protected function saveVideoMissingCsv($data){
		$filename = Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'log'.DIRECTORY_SEPARATOR.'video-missing.csv';
		$fp = fopen($filename, 'w');
		fputcsv($fp, array('Item Code','Product URL', 'Youtube Link'));
		foreach ($data as $fields) {
			fputcsv($fp, $fields);
		}
		fclose($fp);
	}

}
