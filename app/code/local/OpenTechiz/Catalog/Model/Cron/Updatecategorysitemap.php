<?php
class OpenTechiz_Catalog_Model_Cron_Updatecategorysitemap
{
    public function run()
    {
        $tree = Mage::getResourceModel('catalog/category_tree');

        $collection = $tree->getCollection('path');
        $collection
            ->addAttributeToFilter('level', array('nin' => array(0, 1)))
            ->addAttributeToFilter('is_active', 1);
           
        $collection->getSelect()->joinLeft(
            array("catalog_varchar" => 'tsht_catalog_category_entity_varchar'),
            "e.entity_id = catalog_varchar.entity_id and catalog_varchar.attribute_id = 49 and catalog_varchar.`store_id` = 0",
            array("display_mode" => "catalog_varchar.value")
        );
        $category_filter = array();

        if($collection){
            foreach ($collection as $cate) {
                if($cate->getDisplayMode() != 'PAGE'){
                    $category = Mage::getModel('catalog/category')
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->load($cate->getId());
                    if (!Mage::helper('opentechizcatalog')->_hasProducts($cate->getId())) {  
                         $category_filter[] = $category->getId(); // get all category id exclude.
                    }
                }
            }
            if(!empty($category_filter)){
                foreach($category_filter as $key => $category_id){
                    $category_exclude = Mage::getModel('catalog/category')->load($category_id);
                    $list_childcategory = $category_exclude->getChildren(); // get all category child of category exclude

                    if($list_childcategory !=''){
                      $catList = explode(",", $list_childcategory);
                      foreach($catList as $cat)
                      {
                        if (Mage::helper('opentechizcatalog')->_hasProducts($cat)) {      //if any child category have product will remove from list exclude 
                            if (($k = array_search($category_id, $category_filter)) !== false) {
                                unset($category_filter[$k]);
                            }
                            break;
                        } 
                      }
                    }
                }
            }
            $configPath = 'sitemap_enhanced_plus/general/category_filter';
            if(!empty($category_filter)){
                $update_list_category = join(',', $category_filter);
                Mage::getConfig()->saveConfig($configPath,$update_list_category);
                Mage::log($update_list_category,null,'list_update_category.log',true);
            }else{
                Mage::getConfig()->saveConfig($configPath,'');
                Mage::log('There are no any category is excluded',null,'list_update_category.log',true);
            }
        }
        
    }

}
