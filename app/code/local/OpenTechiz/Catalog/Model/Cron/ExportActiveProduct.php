<?php

class OpenTechiz_Catalog_Model_Cron_ExportActiveProduct
{

    protected function getCategories($catIDs){
        if (!Mage::helper('catalog/category_flat')->isEnabled()) {
            $productCats = Mage::getModel('catalog/category')->getCollection()
                    ->addAttributeToSelect('name')
                    ->addAttributeToSelect('url')
                    ->addIdFilter($catIDs);
        } else {
            $productCats = Mage::getModel('catalog/category')->getCollection()
                    ->addNameToResult()
                    ->addUrlRewriteToResult()
                    ->addStoreFilter()
                    ->addIsActiveFilter()
                    ->addAttributeToFilter('include_in_menu', 1)
                    ->addIdFilter($catIDs);
        }
        return $productCats;
    }

    public function run()
    {
        Mage::app()->setCurrentStore('default');
        $collection = $this->getProductCollection();
        if ($collection->count() > 0) {
            $data = array();
            foreach ($collection as $_product) {
                // $gender = "No Gender";
                $gender = "";
                if(Mage::helper('catalog/product_flat')->isEnabled() && $_product->getData('c2c_gender_for_filter')){
                    $gender = $_product->getResource()->getAttribute('c2c_gender_for_filter')->getFrontend()->getValue($_product);
                }else{
                    if ($_product->getData("c2c_gender_for_filter")) {
                        $gender = $_product->getAttributeText("c2c_gender_for_filter");
                    }
                }

                // $metal = "No Metal";
                $metal = "";
                if(Mage::helper('catalog/product_flat')->isEnabled() && $_product->getData('c2c_met_filter')){
                    $metal = $_product->getResource()->getAttribute('c2c_met_filter')->getFrontend()->getValue($_product);
                }else{
                    if ($_product->getData("c2c_met_filter")) {
                        $metal = $_product->getAttributeText("c2c_met_filter");
                    }
                }
                

                //$diamond_carat = "No Diamond Carat Weight";
                $c2c_carat_weight = "";
                if ($_product->getData("c2c_carat_weight")) {
                    $c2c_carat_weight = $_product->getData("c2c_carat_weight");
                } 
                
                $c2c_carat_weight_display = "";
                if ($_product->getData("c2c_carat_weight")){
                    $c2c_carat_weight_display = $_product->getData("c2c_carat_weight");
                }

                $retail_price = Mage::helper('core')->currency($_product->getPrice(), true, false);
                $selling_price = Mage::helper('core')->currency($_product->getSpecialPrice(), true, false);
                // $category = "No Category";
                // $lowest_category = "No Lowest Category";
                $category = "";
                $lowest_category = "";
                $excluded_cats = Mage::helper('opentechizcatalog')->getConfigExcludeCategoryIds();
                $categoryIds = array_diff($_product->getCategoryIds(), $excluded_cats);
                $categories = false;
                if (count($_product->getCategoryIds()) > 0) {
                    $category = "";
                    $categoryIds = array_diff($_product->getCategoryIds(), $excluded_cats);
                    $categories = $this->getCategories($categoryIds);
                    if ($categories->count() > 0) {
                        $cats = array();
                        foreach ($categories as $cat) {
                            $cats[] = $cat->getName() . "(" . $cat->getId() . ")";
                        }
                        $category = join(', ', $cats);
                    }
                }

                if ($_product->getData('breadcrumb_path')) {
                    $catIds = array_diff(explode('>', $_product->getData('breadcrumb_path')), $excluded_cats);
                    if (count($catIds) > 0) {
                        $lowes_catID = end($catIds);
                        $c = Mage::getModel('catalog/category')->load($lowes_catID);
                        $lowest_category = $c->getName();
                    }
                } else {
                    if ($categories && $categories->count() > 0) {
                        $level = 0;
                        foreach ($categories as $_category) {
                            if ($_category->getLevel() > $level) {
                                $level = $_category->getLevel();
                                $lowest_category = $_category->getName();
                            }
                        }
                    }
                }
                $data[] = array(
                    $_product->getSku(),
                    $retail_price,
                    $selling_price,
                    $_product->getProductUrl(),
                    $category,
                    $gender,
                    join(" ", array($gender, $metal, $lowest_category, $c2c_carat_weight ."ctw", $c2c_carat_weight_display)),
                );
            }
            $this->saveAsCsv($data);
        }
    }

    protected function getProductCollection()
    {
        $collection = Mage::getModel('catalog/product')->getCollection();
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collection->addAttributeToSelect($attributes);
        $collection->addAttributeToSelect('sku');
        $collection->addAttributeToSelect('c2c_gender_for_filter');
        $collection->addAttributeToSelect('c2c_carat_weight');
        $collection->addAttributeToSelect('c2c_carat_weight');
        $collection->addAttributeToSelect('c2c_met_filter');
        $collection->addAttributeToSelect('breadcrumb_path');
        $collection->addCategoryIds();
        $collection->addUrlRewrite();
        $collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);  
        return $collection;
    }
    protected function saveAsCsv($data){
    $filename = Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'export'.DIRECTORY_SEPARATOR.'active_products_for_affiliates.csv';
    $fp = fopen($filename, 'w');
    fputcsv($fp, array('SKU','Retail Price', 'Selling Price', 'URL', 'Category', 'Gender', 'Name'));
    foreach ($data as $fields) {
        fputcsv($fp, $fields);
    }
    fclose($fp);
}

}
