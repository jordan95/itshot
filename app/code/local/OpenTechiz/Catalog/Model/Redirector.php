<?php

class OpenTechiz_Catalog_Model_Redirector extends Optimiseweb_Redirects_Model_Redirector
{
    protected function disabledProductCheck($request)
    {
        if ($request->getActionName() !== 'noRoute') {
            if ((bool) Mage::getStoreConfig('optimisewebredirects/disabled_products/enabled')) {
                if (($request->getModuleName() == 'catalog') AND ( $request->getControllerName() == 'product') AND ( $request->getActionName() == 'view')) {
                    $productId = Mage::app()->getRequest()->getParam('id');
                    $storeId = Mage::app()->getStore()->getId();
                    $status = Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, 'status', $storeId);
                    if ($status == 2) {
                        return Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, 'url_path', $storeId);
                    }
                }
            }
        }
        return FALSE;
    }

    protected function notvisibleProductCheck($request)
    {
        if ($request->getActionName() !== 'noRoute') {
            if ((bool) Mage::getStoreConfig('optimisewebredirects/notvisible_products/enabled')) {
                if (($request->getModuleName() == 'catalog') AND ( $request->getControllerName() == 'product') AND ( $request->getActionName() == 'view')) {
                    $productId = Mage::app()->getRequest()->getParam('id');
                    $storeId = Mage::app()->getStore()->getId();
                    $visibility = Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, 'visibility', $storeId);
                    if ($visibility == 1) {
                        return Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, 'url_path', $storeId);
                    }
                }
            }
        }
        return FALSE;
    }

    protected function disabledCategoryCheck($request)
    {
        if ($request->getActionName() !== 'noRoute') {
            if ((bool) Mage::getStoreConfig('optimisewebredirects/disabled_products/enabled')) {
                if (($request->getModuleName() == 'catalog') AND ( $request->getControllerName() == 'category') AND ( $request->getActionName() == 'view')) {
                    $categoryId = Mage::app()->getRequest()->getParam('id');
                    $storeId = Mage::app()->getStore()->getId();
                    $isActive = Mage::getResourceModel('catalog/category')->getAttributeRawValue($categoryId, 'is_active', $storeId);
                    if (!$isActive === null && $isActive == 0) {
                        return Mage::getResourceModel('catalog/category')->getAttributeRawValue($categoryId, 'url_path', $storeId);
                    }
                }
            }
        }
        return FALSE;
    }
}
