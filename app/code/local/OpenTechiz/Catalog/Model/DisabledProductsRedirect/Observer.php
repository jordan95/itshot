<?php

class OpenTechiz_Catalog_Model_DisabledProductsRedirect_Observer
{

    private static $permanentRedirectCode = 301;

    /**
     * Process redirect to category page for products with no route
     *
     * @param Varien_Event_Observer  $observer
     *
     * @return OpenTechiz_Catalog_Model_DisabledProductsRedirect_Observer
     *
     */
    public function redirectIfProductIsDisabled(Varien_Event_Observer $observer)
    {
        $controller = $observer->getData('controller_action');
        $productId = (int) $controller->getRequest()->getParam('id');

        // don't produce any action if functionality is disabled or we can't get current product id
        if (!$productId || !$this->isUrlRewriteForDisabledProductActive()) {
            return $this;
        }

        $status = Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, 'status', $this->getStoreId());
        $product = Mage::getModel('catalog/product');
        $product->setId($productId)
            ->setData('status', $status);

        if (!$product->isVisibleInCatalog()) {
            $redirectUrl = $this->getRedirectUrlToFirstCategory($product, $observer);
            if ($redirectUrl) {
                $this->addNotice();
                $controller->getResponse()
                        ->setRedirect($redirectUrl, self::$permanentRedirectCode);
            } else {
                $controller->getResponse()->setRedirect(Mage::getBaseUrl())->sendResponse();
                exit;
            }
        }

        return $this;
    }

    /**
     * Get redirect url for current product
     *
     * @param Mage_Catalog_Model_Product $product
     * @param Varien_Event_Observer $observer
     *
     * @return string
     */
    protected function getRedirectUrlToFirstCategory($product, $observer)
    {
        // trying to get the product category form request params
        $categoryId = $observer->getParams() != null ? $observer->getParams()->getCategoryId() : false;

        if ($categoryId) {
            $productCategories = array($categoryId);
        } else {
            $productCategories = $product->getCategoryIds();
        }

        if (!count($productCategories)) {
            return Mage::getUrl();
        }

        // load only necessary attributes for better performance, also get only available categories
        $categories = Mage::getModel('catalog/category')->getCollection()
                ->addNameToResult()
                ->addUrlRewriteToResult()
                ->addIsActiveFilter()
                ->addAttributeToFilter('include_in_menu', 1)
                ->addIdFilter($productCategories);
        if (
                !Mage::helper('catalog/category_flat')->isEnabled() &&
                $categories instanceof Mage_Catalog_Model_Resource_Category_Flat_Collection
        ) {
            $categories->addStoreFilter();
        }
        $category = $categories->getFirstItem();

        return $category->getId() ? $category->getUrl() : '';
    }

    /**
     * check out the product visibility via core API
     *
     * @param $product
     *
     * @return bool
     */
    protected function canAvoidNoRoute($product)
    {
        return !Mage::helper('catalog/product')->canShow($product);
    }

    protected function addNotice()
    {
        Mage::getSingleton('core/session')->addNotice($this->getDisabledProductUrlRewriteNotice());
    }

    protected function getStoreId()
    {
        return Mage::app()->getStore()->getId();
    }

    private function getDisabledProductUrlRewriteNotice()
    {
        return Mage::getStoreConfig('disabled_redirect/settings/notice', Mage::app()->getStore());
    }

    private function isUrlRewriteForDisabledProductActive()
    {
        return Mage::getStoreConfig('disabled_redirect/settings/active', Mage::app()->getStore());
    }
}
