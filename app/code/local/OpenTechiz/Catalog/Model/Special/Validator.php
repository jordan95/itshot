<?php

class OpenTechiz_Catalog_Model_Special_Validator extends TBT_Rewards_Model_Special_Validator
{
    protected $_ruleCollection;

    public function getApplicableRules($action = null, $or_action = null)
    {
        $resultCollection = array();
        if ($this->_ruleCollection === null) {
            $this->_ruleCollection = Mage::getModel('rewards/special')->getCollection();
        }
        foreach ($this->_ruleCollection as $rule) {
            if ($this->isRuleValid($rule, $action)) {
                $resultCollection[] = $rule;
            } else if ($or_action != null) {
                if ($this->isRuleValid($rule, $or_action)) {
                    $resultCollection[] = $rule;
                }
            }
        }

        return $resultCollection;
    }
}
