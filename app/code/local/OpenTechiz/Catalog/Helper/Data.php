<?php

class OpenTechiz_Catalog_Helper_Data extends Mage_Core_Helper_Abstract
{

    const XML_WEB_URL_ENABLE_PREFIX_CATEGORY_URL = "web/url/enable_prefix_category_url";
    public function isSuperAdmin()
    {
        return Mage::getSingleton('admin/session')->isAllowed("catalog/product_admin");
    }

    public function isEditable()
    {
        $current_product = Mage::registry("current_product");
        if (!$current_product instanceof Mage_Catalog_Model_Product) {
            return true;
        }

        if ($this->isSuperAdmin()) {
            return true;
        } else {
            if ($current_product->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_ENABLED) {
                return false;
            } else {
                return true;
            }
        }
    }

    public function getFieldsEditable()
    {
        return ["price", "special_price", "special_from_date", "special_to_date"];
    }

    public function _hasProducts($categoryId)
    {
        $resource = Mage::getSingleton('core/resource');
        $tsht_catalog_product_entity = $resource->getTableName('catalog_product_entity');
        $tsht_catalog_category_product_index = $resource->getTableName('catalog_category_product_index');
        $db = $resource->getConnection('core_read');
        $query = $db->select()
                ->from(array('e' => $tsht_catalog_product_entity), array('total_rows' => new Zend_Db_Expr('COUNT(e.entity_id)')))
                ->join(array('cat_index' => $tsht_catalog_category_product_index), 'e.entity_id = cat_index.product_id AND cat_index.store_id=1', array())
                ->where('cat_index.category_id = ?', $categoryId);
        $total_rows = $db->fetchCol($query);
        return isset($total_rows[0]) && $total_rows[0] > 0 ? true : false;
    }

    public function getConfigExcludeCategoryIds()
    {
        if (Mage::getStoreConfig('catalog/breadcrumb/exclude_categories')) {
            return preg_split('/[\s,]+/', Mage::getStoreConfig('catalog/breadcrumb/exclude_categories'));
        }
        return array();
    }

    public function getSubCategory($_category)
    {
        $activeChildren = array();
        foreach (explode(',', $_category) as $child) {
            if ($this->_hasProducts($child)) {
                $activeChildren[] = $child;
            }
        }
        return $activeChildren;
    }

    public function canAppendPrefixCategoryUrl()
    {
        return (bool) Mage::getStoreConfig(self::XML_WEB_URL_ENABLE_PREFIX_CATEGORY_URL);
    }
    public function getUpdatedAtData($update_at){
        $new_date = '';
        $date  = Mage::helper('core')->formatDate($update_at, 'short',false);
        if($date !=""){
            $date = date_create($date);
            $new_date =  date_format($date,"m/d/y");
            
        }
        return $new_date;
    }

}
