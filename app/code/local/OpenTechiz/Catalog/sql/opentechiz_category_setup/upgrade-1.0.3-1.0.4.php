<?php 
$resource = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');
$eav_attribute =  $resource->getTableName('eav_attrbiute');
$catalog_eav_attribute =  $resource->getTableName('catalog_eav_attribute');


$collection = Mage::getModel('eav/entity_attribute')
            ->getCollection()
            ->addFieldToSelect('attribute_id')
            ->addFieldToSelect('attribute_code')
            ->addFieldToFilter('attribute_code', 'position')
            ->addFieldToFilter('entity_type_id', 3);
$collection->getSelect()->join($catalog_eav_attribute, '`main_table`.`attribute_id` ='.$catalog_eav_attribute.'.`attribute_id`', 
                    array('is_visible'));
if($collection){
    foreach ($collection->getData() as $data) {
        $query = "UPDATE {$catalog_eav_attribute} SET is_visible = 1 WHERE attribute_id = ". (int)$data['attribute_id'];
        $writeConnection->query($query);
    }
}
