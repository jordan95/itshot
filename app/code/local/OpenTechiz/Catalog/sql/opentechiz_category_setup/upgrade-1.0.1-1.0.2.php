<?php
$installer = $this;
$installer->startSetup();
$installer->addAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'breadcrumb_path',
    array(
        'type'                    => 'text',
        'label'                   => 'Breadcrumb Path',
        'input'                   => 'text',
        'class'                   => '',
        'group'                   => 'General',
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        "visible"  => true,
        "required" => false,
        "user_defined"  => true,
        'default'                 => '',
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => true,
        'unique'                  => false,
    )
);
$this->endSetup();