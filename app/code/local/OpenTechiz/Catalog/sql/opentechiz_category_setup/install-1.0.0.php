<?php
$installer = $this;
$installer->startSetup();
$prodEntityTypeId = $installer->getEntityTypeId('catalog_product');
//$installer->addAttribute('catalog_product', 'priority', array(
//    'type' => 'varchar',
//    'group' => 'General',
//    'input' => 'text',
//    'label' => 'Priority',
//    'required' => false,
//    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
//    'visible' => true,
//    'visible_on_front' => true,
//    'unique' => false,
//    'filterable' => 1,
//    'searchable' => 0,
//    'comparable' => 0,
//    'user_defined' => 1,
//    'is_configurable' => 0
//));
$installer->updateAttribute($prodEntityTypeId, 'created_at', 'is_visible', 1);
$installer->updateAttribute($prodEntityTypeId, 'created_at', 'frontend_input', 'datetime');
$installer->updateAttribute($prodEntityTypeId, 'created_at', 'frontend_label', 'Created at');
$installer->updateAttribute($prodEntityTypeId, 'created_at', 'is_user_defined', 1);

$installer->endSetup();
