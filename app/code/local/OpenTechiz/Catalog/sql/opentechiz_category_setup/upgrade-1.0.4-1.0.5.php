<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$installer->addAttribute('catalog_product', 'c2c_gemstonecaratweight', array(
    'label'           => 'Gemstone Carat Weight',
    'input'           => 'text',
    'type'            => 'decimal',
    'required'        => 0,
    'visible_on_front'=> 1,
    'filterable'      => 0,
    'searchable'      => 0,
    'comparable'      => 0,
    'user_defined'    => 1,
    'is_configurable' => 0,
    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

// get Attribute group
$groupName = 'Gemstone Information';
$entityTypeId = $installer->getEntityTypeId('catalog_product');
$array = [100, 101, 102];
foreach ($array as $id){
    $attributeSetId = $id;
    $attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, $groupName);

// Add existing attribute to group
    $attributeId = $installer->getAttributeId($entityTypeId, 'c2c_gemstonecaratweight');
    $installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);
}

$installer->endSetup();
?>