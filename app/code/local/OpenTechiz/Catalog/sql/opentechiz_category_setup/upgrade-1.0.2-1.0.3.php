<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();
$table = $installer->getTable('core_url_rewrite');
   $installer->run("
        ALTER TABLE `$table`
            ADD COLUMN (`modified_date` DATETIME NULL DEFAULT NULL)  
    ");

$installer->endSetup();