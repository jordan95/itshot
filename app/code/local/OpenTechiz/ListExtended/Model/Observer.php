<?php

class OpenTechiz_ListExtended_Model_Observer
{

    const ATTRIBUTE_POPULARITY_CODE = 'c2c_popularity';

    public function increasePopularity($product)
    {
        $attribute_code = self::ATTRIBUTE_POPULARITY_CODE;
        $value = $product->getData($attribute_code);
        $resource = $product->getResource();
        $product->setData($attribute_code, $value + 1);
        $resource->saveAttribute($product, $attribute_code);
    }

    public function decreasePopularity($product)
    {
        $attribute_code = self::ATTRIBUTE_POPULARITY_CODE;
        $value = $product->getData($attribute_code);
        $resource = $product->getResource();
        $product->setData($attribute_code, $value - 1);
        $resource->saveAttribute($product, $attribute_code);
    }

    public function increasePopularityInWishlist(Varien_Event_Observer $observer)
    {
        $item = $observer->getItem();
        $productId = $item->getProductId();
        $product = Mage::getModel('catalog/product')->load($productId);
        $this->increasePopularity($product);
    }

    public function decreasePopularityInWishlist(Varien_Event_Observer $observer)
    {
        $item = $observer->getItem();
        $productId = $item->getProductId();
        $product = Mage::getModel('catalog/product')->load($productId);
        $this->decreasePopularity($product);
    }

    public function increasePopularityInCart(Varien_Event_Observer $observer)
    {
        $item = $observer->getQuoteItem();
        $product = $item->getProduct();
        $this->increasePopularity($product);
    }

    public function decreasePopularityInCart(Varien_Event_Observer $observer)
    {
        $item = $observer->getQuoteItem();
        $product = $item->getProduct();
        $this->decreasePopularity($product);
    }
    
    public function increasePopularityInPlaceOrder(Varien_Event_Observer $observer){
        $order = $observer->getEvent()->getOrder();
        $items = $order->getAllVisibleItems();
        if(count($items) > 0){
            foreach ($items as $item){
                $this->increasePopularity($item->getProduct());
            }
        }
    }

}
