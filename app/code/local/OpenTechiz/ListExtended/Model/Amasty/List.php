<?php

class OpenTechiz_ListExtended_Model_Amasty_List extends Amasty_List_Model_List
{

    public function addItem($productId, $customOptions, $description = '')
    {
        $item = Mage::getModel('amlist/item')
                ->setProductId($productId)
                ->setListId($this->getId())
                ->setDescr($description)
                ->setQty(1);

        if ($customOptions) {
            foreach ($customOptions as $product) {
                $options = $product->getCustomOptions();
                foreach ($options as $option) {
                    if ($option->getProductId() == $productId && $option->getCode() == 'info_buyRequest') {
                        $v = unserialize($option->getValue());

                        $qty = isset($v['qty']) ? max(0.01, $v['qty']) : 1;
                        $item->setQty($qty);

                        // to be able to compare request in future
                        $unusedVars = array('list', 'qty', 'list_next', 'related_product', 'form_key');
                        foreach ($unusedVars as $k) {
                            if (isset($v[$k])) {
                                unset($v[$k]);
                            }
                        }
                        if(isset($v['options']['partialpayment'])){
                            unset($v['options']['partialpayment']);
                        }
                        $item->setBuyRequest(serialize($v));
                    }
                }
            }
        }

        // check if we already have the same item in the list.
        // if yes - set it's id to the current item
        $id = $item->findDuplicate();
        if ($id) {
            $item->setId($id);
        } else {
            $item->save();
            Mage::dispatchEvent('amlist_add_item', array(
                'item' => $item
            ));
        }
        return $item;
    }

}
