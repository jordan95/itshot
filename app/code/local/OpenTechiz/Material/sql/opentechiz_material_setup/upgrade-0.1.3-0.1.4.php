<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('material_gold_bar')}
    CHANGE `po_id` `po_increment_id` VARCHAR(25) NOT NULL;
");

$installer->endSetup();