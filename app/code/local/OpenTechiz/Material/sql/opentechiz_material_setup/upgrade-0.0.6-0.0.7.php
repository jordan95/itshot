<?php

$installer = $this;

$installer->startSetup();

$installer->run("    
    ALTER TABLE {$this->getTable('material_gold')}
    CHANGE `metal_color` `gold_color`
    VARCHAR(45)
    NOT NULL;
    
    ALTER TABLE {$this->getTable('material_gold')}
    CHANGE `alloy_type` `gold_type`
    VARCHAR(45)
    NOT NULL;
");

$installer->endSetup();