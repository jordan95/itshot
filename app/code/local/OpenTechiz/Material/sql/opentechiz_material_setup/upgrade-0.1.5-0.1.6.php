<?php
$config = array(
    'path' => 'opentechiz_material/data_update/cron_is_run',
    'value' => '1',
);

$setup = new Mage_Core_Model_Config();
$setup->saveConfig($config['path'], $config['value'], 'default', 1);
