<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('material_gold')}
    ADD CONSTRAINT unique_gold_color_with_gold_type 
    UNIQUE unique_gold_color_with_gold_type(
        `gold_type`,
        `gold_color`);
        
    ALTER TABLE {$this->getTable('material_gold')}
    CHANGE `reorder_point` `reorder_point`
    FLOAT
    NULL
    DEFAULT '0';
");

$installer->endSetup();