<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

$installer = $this;

$installer->startSetup();

$installer->run("

    ALTER TABLE {$this->getTable('material_gold_bar')} 
    ADD UNIQUE (`serial_no`);
    
    ALTER TABLE {$this->getTable('material_gold')} 
    ADD UNIQUE (`alloy_name`);
    
    ALTER TABLE {$this->getTable('material_stone')} 
    ADD UNIQUE (`stone_name`);
    
    ALTER TABLE {$this->getTable('material_gold_bar')}
    CHANGE `created_at` `created_at`
    TIMESTAMP
    NULL
    DEFAULT CURRENT_TIMESTAMP;
    
    ALTER TABLE {$this->getTable('material_gold')}
    CHANGE `created_at` `created_at`
    TIMESTAMP
    NULL
    DEFAULT CURRENT_TIMESTAMP;
    
    ALTER TABLE {$this->getTable('material_stone')}
    CHANGE `created_at` `created_at`
    TIMESTAMP
    NULL
    DEFAULT CURRENT_TIMESTAMP;
    
    ALTER TABLE {$this->getTable('material_gold_bar')}
    CHANGE `updated_at` `updated_at`
    TIMESTAMP
    NULL;
    
    ALTER TABLE {$this->getTable('material_gold')}
    CHANGE `updated_at` `updated_at`
    TIMESTAMP
    NULL;
    
    ALTER TABLE {$this->getTable('material_stone')}
    CHANGE `updated_at` `updated_at`
    TIMESTAMP
    NULL;
    
");

$installer->endSetup();