<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('material_gold')}
    CHANGE `gold_color` `gold_color`
    VARCHAR(45)
    NULL;
");

$installer->endSetup();