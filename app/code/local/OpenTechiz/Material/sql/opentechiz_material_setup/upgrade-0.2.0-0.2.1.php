<?php

$installer = $this;
$installer->startSetup();
$installer->run("
  ALTER TABLE {$this->getTable('material_daily_qty_golds')} 
  CHANGE `qty` `weight` 
  DOUBLE(12,4)
  NOT NULL
  DEFAULT '0.0000';
");
$installer->endSetup();