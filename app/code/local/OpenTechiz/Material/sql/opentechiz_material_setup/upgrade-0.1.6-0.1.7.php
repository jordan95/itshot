<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('material_stone')}
    CHANGE `price` `price`
    FLOAT
    UNSIGNED
    NOT NULL
    COMMENT 'Price';
");

$installer->endSetup();