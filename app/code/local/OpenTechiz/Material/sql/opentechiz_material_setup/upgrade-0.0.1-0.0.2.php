<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

$installer = $this;

$installer->startSetup();

$this->getConnection()->renameTable($this->getTable('stone'),$this->getTable('material_stone'));
$this->getConnection()->renameTable($this->getTable('gold'),$this->getTable('material_gold'));
$this->getConnection()->renameTable($this->getTable('gold_bar'),$this->getTable('material_gold_bar'));

$installer->endSetup();