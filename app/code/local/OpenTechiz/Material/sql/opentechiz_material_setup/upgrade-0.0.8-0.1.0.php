<?php

$installer = $this;
$installer->startSetup();

if(!$installer->tableExists('opentechiz_material/stoneRequest')) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('opentechiz_material/stoneRequest'))
        ->addColumn('stone_request_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'Id')
        ->addColumn('personalized_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'PersonalizedProductId')
        ->addColumn('stones', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
        ), 'stones')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'Creating Time')
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            'default'  => 0
        ), 'Status');
    $installer->getConnection()->createTable($table);
}
$installer->endSetup();