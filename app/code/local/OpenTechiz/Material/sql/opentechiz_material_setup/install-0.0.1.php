<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('stone')};
    CREATE TABLE {$this->getTable('stone')} (
      `stone_id`      INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `stone_name`    VARCHAR(45)       NOT NULL,
      `stone_type`    VARCHAR(45)       NOT NULL,
      `weight`        VARCHAR(45)       NOT NULL,
      `shape`         VARCHAR(45)       NULL,
      `diameter`      VARCHAR(45)       NULL,
      `price`         FLOAT             NOT NULL,
      `quality`       VARCHAR(45)       NULL,
      `qty`           INT(11)           NOT NULL DEFAULT '0',
      `on_hold`       INT(11)           NULL,
      `reorder_point` INT(11)           NULL,
      `ideal_stock_level` INT(11)       NULL,
      `shelf_location`    VARCHAR(200)  NULL,
      `created_at` DATETIME             NULL,
      `updated_at` DATETIME             NULL,
      `status`        INT(1)            NOT NULL DEFAULT '1',
      PRIMARY KEY (`stone_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
      
    DROP TABLE IF EXISTS {$this->getTable('gold')};
    CREATE TABLE {$this->getTable('gold')} (
      `gold_id`       INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `alloy_type`    VARCHAR(45)       NOT NULL,
      `alloy_name`    VARCHAR(45)       NOT NULL,
      `price`         FLOAT             NOT NULL,
      `created_at` DATETIME             NULL,
      `updated_at` DATETIME             NULL,
      `qty`           INT(11)           NOT NULL DEFAULT '0',
      `on_hold`       INT(11)           NULL,
      `reorder_point` INT(11)           NULL,
      `status`        INT(1)            NOT NULL DEFAULT '1',
      PRIMARY KEY (`gold_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
     
    DROP TABLE IF EXISTS {$this->getTable('gold_bar')};
    CREATE TABLE {$this->getTable('gold_bar')} (
      `gold_bar_id`   INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `serial_no`     VARCHAR(20)       NOT NULL,
      `gold_id`       INT(11)           NOT NULL,
      `sup_id`        INT(11)           NOT NULL,
      `created_at` DATETIME             NULL,
      `updated_at` DATETIME             NULL,
      `status`        INT(1)            NOT NULL DEFAULT '1',
      PRIMARY KEY (`gold_bar_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
$installer->endSetup();