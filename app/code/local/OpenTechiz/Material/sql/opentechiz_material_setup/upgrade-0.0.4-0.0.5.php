<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('material_stone')}
    DROP `stone_name`;
    
    ALTER TABLE {$this->getTable('material_gold')}
    DROP `alloy_name`;
    
    ALTER TABLE {$this->getTable('material_gold')} 
    ADD `metal_color`
    VARCHAR(45)
    NOT NULL
    AFTER `alloy_type`;
");

$installer->endSetup();