<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

$installer = $this;

$installer->startSetup();

$installer->run("
    
    ALTER TABLE {$this->getTable('material_gold')} 
    CHANGE `weight` `weight` 
    DOUBLE(12,4)
    NOT NULL
    COMMENT 'Weight'
    DEFAULT '0.0000';
    
    ALTER TABLE {$this->getTable('material_gold')}
    CHANGE `reorder_point` `reorder_point` 
     DOUBLE(12,4)
    NOT NULL
    COMMENT 'ReOrder Weight'
    DEFAULT '0.0000';
    
    ALTER TABLE {$this->getTable('material_gold_bar')} 
    CHANGE `weight` `weight` 
    DOUBLE(12,4)
    NOT NULL
    COMMENT 'Weight'
    DEFAULT '0.0000';
    ALTER TABLE {$this->getTable('material_gold_bar')} 
    CHANGE `weight_use` `weight_use` 
    DOUBLE(12,4)
    NOT NULL
    COMMENT 'Weight Use'
    DEFAULT '0.0000';
");

$installer->endSetup();