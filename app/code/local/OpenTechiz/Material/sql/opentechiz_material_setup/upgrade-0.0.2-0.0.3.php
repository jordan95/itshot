<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

$installer = $this;

$installer->startSetup();

$installer->run("

    ALTER TABLE {$this->getTable('material_gold_bar')}
    ADD `weight`
    FLOAT
    UNSIGNED
    NOT NULL
    COMMENT 'Weight'
    AFTER `sup_id`;
    
    ALTER TABLE {$this->getTable('material_gold_bar')}
    ADD `weight_use`
    FLOAT
    UNSIGNED
    NULL
    COMMENT 'Weight Use'
    AFTER `weight`;
    
    ALTER TABLE {$this->getTable('material_gold')} 
    CHANGE `qty` `weight` 
    FLOAT
    UNSIGNED
    NOT NULL
    COMMENT 'Weight'
    DEFAULT '0';
    
    ALTER TABLE {$this->getTable('material_gold')}
    CHANGE `reorder_point` `reorder_point` 
    FLOAT
    NULL;
    
");

$installer->endSetup();