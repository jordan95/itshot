<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('material_stone')}
    CHANGE `on_hold` `on_hold`
    INT(11)
    NOT NULL
    DEFAULT '0';
");

$installer->endSetup();