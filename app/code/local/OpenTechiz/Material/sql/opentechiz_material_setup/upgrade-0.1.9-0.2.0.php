<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('material_daily_qty_stones')};
    CREATE TABLE {$this->getTable('material_daily_qty_stones')} (
      `id`            INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `stone_id`      INT(11)           NOT NULL,
      `time`          DATETIME          NOT NULL,
      `qty`           INT(11)           NULL,
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
     
    DROP TABLE IF EXISTS {$this->getTable('material_daily_qty_golds')};
    CREATE TABLE {$this->getTable('material_daily_qty_golds')} (
      `id`            INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `gold_id`       INT(11)           NOT NULL,
      `time`          DATETIME          NOT NULL,
      `qty`           INT(11)           NULL,
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
$installer->endSetup();