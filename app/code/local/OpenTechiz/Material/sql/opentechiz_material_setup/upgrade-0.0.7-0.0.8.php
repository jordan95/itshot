<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('material_gold')}
    DROP `on_hold`;
    
    ALTER TABLE {$this->getTable('material_stone')} 
    ADD `on_hold`
    INT(11)
    NULL
    AFTER `qty`
");

$installer->endSetup();