<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('material_stone')}
    DROP `on_hold`;
    
    ALTER TABLE {$this->getTable('material_gold_bar')} 
    CHANGE `sup_id` `po_id` 
    INT(11) 
    NULL;
");

$installer->endSetup();