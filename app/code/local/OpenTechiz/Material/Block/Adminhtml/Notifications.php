<?php
class OpenTechiz_Material_Block_Adminhtml_Notifications extends Mage_Adminhtml_Block_Template
{
    public function getMessage()
    {
        $message = '';
        $isCronned = Mage::getStoreConfig('opentechiz_material/data_update/cron_is_run');
        if($isCronned == 0){
            $message = 'Your material data has been updated. You need to run cron "personalizedproduct_updatelistingprice" in order to sync price and clear cache!';
        }
        return $message;
    }
}