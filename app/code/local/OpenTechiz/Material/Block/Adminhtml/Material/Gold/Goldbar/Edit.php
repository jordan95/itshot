<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Block_Adminhtml_Material_Gold_Goldbar_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'opentechiz_material';
        $this->_controller = 'adminhtml_material_gold_goldbar';
        parent::__construct();

        if (!Mage::getSingleton('admin/session')->isAllowed('admin/erp/material/gold/goldbar/delete')) {
            $this->_removeButton('delete');
        }

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('gold_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'gold_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'gold_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
    }

    public function getHeaderText()
    {
        if (Mage::registry('current_goldbar') && Mage::registry('current_goldbar')->getId()) {
            return Mage::helper('opentechiz_material')->__("Edit gold bar for '%s'", $this->htmlEscape(Mage::registry('current_goldbar')->getSerialNo()));
        } else {
            return Mage::helper('opentechiz_material')->__('Add New Gold Bar');
        }
    }

    public function getGoldBar()
    {
        if (!$this->hasData('current_goldbar')) {
            $this->setData('current_goldbar', Mage::registry('current_goldbar'));
        }
        return $this->getData('current_goldbar');
    }
}