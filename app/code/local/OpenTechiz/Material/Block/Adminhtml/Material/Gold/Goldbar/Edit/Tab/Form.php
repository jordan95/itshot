<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Block_Adminhtml_Material_Gold_Goldbar_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('opentechiz/material/gold/goldbar/edit/main.phtml');
    }

    public function getGoldBar()
    {
        if (!$this->hasData('current_goldbar'))
        {
            $this->setData('current_goldbar', Mage::registry('current_goldbar'));
        }
        return $this->getData('current_goldbar');
    }
}
