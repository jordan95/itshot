<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Block_Adminhtml_Material_Stone_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'opentechiz_material';
        $this->_controller = 'adminhtml_material_stone';
        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('opentechiz_material')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('opentechiz_material')->__('Delete'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('stone_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'stone_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'stone_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
    }

    public function getHeaderText()
    {
        $stone = $this->_getStone();
        if ($stone && $stone->getId()) {
            $stoneTypeVal = $stone->getStoneType();
            $stoneType = OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_TYPE;
            return Mage::helper('opentechiz_material')->__("Edit stone for '%s'", $this->htmlEscape($stoneType[$stoneTypeVal]));
        } else {
            return Mage::helper('opentechiz_material')->__('Add New Stone');
        }
    }

    public function _getStone()
    {
        if (!$this->hasData('stone_data')) {
            $this->setData('stone_data', Mage::registry('stone_data'));
        }
        return $this->getData('stone_data');
    }
}