<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Block_Adminhtml_Material_Stone_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('stoneGrid');
        $this->setDefaultSort('stone_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_material/stone')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();

        parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('stone_id', array(
            'header' => Mage::helper('opentechiz_material')->__('ID'),
            'width' => '50',
            'index' => 'stone_id',
            'align' => 'center'
        ));
        $this->addColumn('stone_type', array(
            'header' => Mage::helper('opentechiz_material')->__('Type'),
            'index' => 'stone_type',
            'width' => '150',
            'type' => 'options',
            'options' => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_TYPE
        ));
        $this->addColumn('stone_size', array(
            'header' => Mage::helper('opentechiz_material')->__('Weight'),
            'index' => 'weight',
            'width' => '100'
        ));
        $this->addColumn('quality', array(
            'header' => Mage::helper('opentechiz_material')->__('Quality'),
            'index' => 'quality',
            'width' => '50',
            'type' => 'options',
            'options' => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_QUALITY,
        ));
        $this->addColumn('shape', array(
            'header' => Mage::helper('opentechiz_material')->__('Shape'),
            'index' => 'shape',
            'width' => '100',
            'type' => 'options',
            'options' => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_SHAPE
        ));
        $this->addColumn('diameter', array(
            'header' => Mage::helper('opentechiz_material')->__('Diameter'),
            'index' => 'diameter',
            'width' => '50'
        ));
        $this->addColumn('price', array(
            'header' => Mage::helper('catalog')->__('Price'),
            'index' => 'price',
            'renderer' => 'OpenTechiz_Material_Block_Adminhtml_Renderer_Price'
        ));
        $this->addColumn('current_qty', array(
            'header' => Mage::helper('opentechiz_material')->__('Current Quantity'),
            'index' => 'qty',
            'width' => '50',
            'type' => 'number',
        ));
        $this->addColumn('on_hold', array(
            'header' => Mage::helper('opentechiz_material')->__('On Hold'),
            'index' => 'on_hold',
            'width' => '50',
            'type' => 'number',
            'is_system' => true,
        ));
        $this->addColumn('reorder_point', array(
            'header' => Mage::helper('opentechiz_material')->__('Reorder Point'),
            'index' => 'reorder_point',
            'width' => '50',
            'type' => 'number',
        ));
        $this->addColumn('ideal_stock_level', array(
            'header' => Mage::helper('opentechiz_material')->__('Ideal Stock Level'),
            'index' => 'ideal_stock_level',
            'text' => 'text',
            'width' => '50',
            'type' => 'number',
        ));
        $this->addColumn('shelf_location', array(
            'header' => Mage::helper('opentechiz_material')->__('Shelf Location'),
            'index' => 'shelf_location',
            'text' => 'text',
            'width' => '50',
            'type' => 'text',
        ));
        $this->addColumn('created_at', array(
            'header' => Mage::helper('opentechiz_material')->__('Created at'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '170',
        ));
        $this->addColumn('updated_at', array(
            'header' => Mage::helper('opentechiz_material')->__('Updated at'),
            'index' => 'updated_at',
            'type' => 'datetime',
            'width' => '170',
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('opentechiz_material')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                0 => 'Disable',
                2 => 'Reorder',
                3 => 'OK'
            ),
            'renderer' => 'OpenTechiz_Material_Block_Adminhtml_Material_Stone_Renderer_Status',
            'is_system' => true,
        ));
        $this->addColumn('action',
            array(
                'header' => Mage::helper('opentechiz_material')->__('Action'),
                'width' => '50',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('opentechiz_material')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id',
                        'data-column' => 'action',
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
                'align' => 'center',
            ));

        $this->addExportType('*/*/exportCsv', Mage::helper('opentechiz_material')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('opentechiz_material')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('stone_id');
        $this->getMassactionBlock()->setFormFieldName('stone');
        $this->getMassactionBlock()->setUseSelectAll(false);
        $status = OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STATUS;

        // MassDelete
        $this->getMassactionBlock()->addItem('delete_stone', array(
            'label' => Mage::helper('opentechiz_material')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('opentechiz_material')->__('Are you sure?')
        ));

        // Change status
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('opentechiz_material')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('opentechiz_material')->__('Status'),
                    'values' => $status
                )
            )
        ));

        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}