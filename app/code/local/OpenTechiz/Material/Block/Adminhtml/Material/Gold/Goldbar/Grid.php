<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Block_Adminhtml_Material_Gold_Goldbar_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('goldBarGrid');
        $this->setDefaultSort('serial_no');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_material/goldbar')
            ->getCollection()
            ->join(array('a' => 'gold'), 'main_table.gold_id = a.gold_id', array(
                'gold_type'     => 'gold_type',
                'gold_color'       => 'gold_color',
            ))
            ->join(array('po' => 'opentechiz_purchase/purchase'), 'main_table.po_increment_id = po.po_increment_id', array(
                'supplier_id' => 'supplier_id'
            ))
            ->join(array('sup' => 'opentechiz_purchase/supplier'), 'po.supplier_id = sup.supplier_id', array(
                'name' => 'name'
            ));
        $this->setCollection($collection);
        return parent::_prepareCollection();

        parent::_prepareCollection();
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('serial_no', array(
            'header' => Mage::helper('opentechiz_material')->__('Serial No'),
            'width' => '50',
            'index' => 'serial_no',
        ));
        $this->addColumn('gold_type', array(
            'header' => Mage::helper('opentechiz_material')->__('Gold Type'),
            'index' => 'gold_type',
            'type' => 'options',
            'options' => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_TYPE
        ));

        $this->addColumn('gold_color', array(
            'header' => Mage::helper('opentechiz_material')->__('Gold Color'),
            'index' => 'gold_color',
            'type' => 'options',
            'options' => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_COLOR,
        ));
        $this->addColumn('purchase_order', array(
            'header' => Mage::helper('opentechiz_material')->__('Purchase Order'),
            'type' => 'text',
            'index' => 'po_increment_id',
            'filter_index' => 'main_table.po_increment_id',
            'sort_index' => 'main_table.po_increment_id'
        ));
        $this->addColumn('supplier', array(
            'header' => Mage::helper('opentechiz_material')->__('Supplier'),
            'type' => 'text',
            'index' => 'name',
            'is_system' => true,
            'width' => '120'
        ));
        $this->addColumn('weight', array(
            'header' => Mage::helper('opentechiz_material')->__('Weight (gr)'),
            'index' => 'weight',
            'width' => '120',
            'type' => 'text',
            'filter_index' => 'main_table.weight'
        ));
        $this->addColumn('remain_weight', array(
            'header' => Mage::helper('opentechiz_material')->__('Remaining Weight (gr)'),
            'width' => '120',
            'type' => 'text',
            'renderer' => 'OpenTechiz_Material_Block_Adminhtml_Material_Gold_Manage_Renderer_Remain',
            'align' => 'right',
            'filter' => false,
            'is_system' => true,
        ));
        $this->addColumn('created_at', array(
            'header' => Mage::helper('opentechiz_material')->__('Created at'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '170',
            'align' => 'right',
            'filter_index' => 'main_table.created_at',
            'is_system' => true,
        ));
        $this->addColumn('updated_at', array(
            'header' => Mage::helper('opentechiz_material')->__('Updated at'),
            'index' => 'updated_at',
            'type' => 'datetime',
            'width' => '170',
            'align' => 'right',
            'filter_index' => 'main_table.updated_at',
            'is_system' => true,
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('opentechiz_material')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'status',
            'type' => 'options',
            'options' => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLDBAR_STATUS,
            'align' => 'center',
            'filter_index' => 'main_table.status',
            'is_system' => true,
        ));
        $this->addColumn('action',
            array(
                'header' => Mage::helper('opentechiz_material')->__('Action'),
                'width' => '50',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('opentechiz_material')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id',
                        'data-column' => 'action',
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
                'align' => 'center'
            ));

        $this->addExportType('*/*/exportCsv', Mage::helper('opentechiz_material')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('opentechiz_material')->__('XML'));

        return parent::_prepareColumns();
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}