<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Block_Adminhtml_Material_Gold_Manage extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_material_gold_manage';
        $this->_blockGroup = 'opentechiz_material';
        $this->_headerText = Mage::helper('opentechiz_material')->__('Gold Manage');
        $this->_addButtonLabel = Mage::helper('opentechiz_material')->__('Add New Gold');
        parent::__construct();

        $this->_addButton('import', array(
            'label' => Mage::helper('opentechiz_material')->__('Import CSV'),
            'onclick' => 'document.getElementById(\'uploadTarget\').click();',
            'class' => 'add',
            'after_html' => '<form id="uploadForm" method="POST" action="'.$this->getUrl('*/*/importCSV', array()).'" enctype="multipart/form-data">
            <input name="form_key" type="hidden" value="'.Mage::getSingleton('core/session')->getFormKey().'" />
            <input type="file" name="file" style="display:none;" id="uploadTarget"/>
        </form>
        <script type="text/javascript">
            document.getElementById(\'uploadTarget\').addEventListener(\'change\', function(){
                document.getElementById(\'uploadForm\').submit();
            }, false);
        </script>',
        ));
    }

    public function getHeaderCssClass()
    {
        return 'icon-head head-material-gold-manage-goldbar';
    }
}