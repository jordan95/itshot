<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Block_Adminhtml_Material_Stone_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();

        if(Mage::registry('stone_data') && Mage::registry('stone_data')->getId())
        {
            $this->setTemplate('opentechiz/material/stone/edit/main.phtml');
        }
        else
        {
            $this->setTemplate('opentechiz/material/stone/edit/add.phtml');
        }

    }

    public function getStone()
    {
        if (!$this->hasData('stone_data'))
        {
            $this->setData('stone_data', Mage::registry('stone_data'));
        }
        return $this->getData('stone_data');
    }
}
