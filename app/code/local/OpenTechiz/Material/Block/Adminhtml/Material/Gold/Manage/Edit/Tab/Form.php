<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Block_Adminhtml_Material_Gold_Manage_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('opentechiz/material/gold/manage/edit/main.phtml');
    }

    public function getGold()
    {
        if (!$this->hasData('current_gold'))
        {
            $this->setData('current_gold', Mage::registry('current_gold'));
        }
        return $this->getData('current_gold');
    }
}
