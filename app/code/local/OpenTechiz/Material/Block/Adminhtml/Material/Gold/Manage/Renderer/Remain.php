<?php

class OpenTechiz_Material_Block_Adminhtml_Material_Gold_Manage_Renderer_Remain
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = 0;
        $weight = $row->getData('weight');
        $use = $row->getData('weight_use');
        $value = $weight-$use;
       
        return sprintf("%.4f",  $value);
    }
}