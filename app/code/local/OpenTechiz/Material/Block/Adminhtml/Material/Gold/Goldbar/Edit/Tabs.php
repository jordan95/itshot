<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Block_Adminhtml_Material_Gold_Goldbar_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('goldTabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('opentechiz_material')->__('Gold Bar Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('opentechiz_material')->__('General'),
            'title'     => Mage::helper('opentechiz_material')->__('General'),
            'content'   => $this->getLayout()->createBlock('opentechiz_material/adminhtml_material_gold_goldbar_edit_tab_form')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }

    public function getGoldBar()
    {
        if (!$this->hasData('current_goldbar'))
        {
            $this->setData('current_goldbar', Mage::registry('current_goldbar'));
        }
        return $this->getData('current_goldbar');
    }
}