<?php

class OpenTechiz_Material_Block_Adminhtml_Material_Gold_Manage_Renderer_Status
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $status = $row->getData($this->getColumn()->getIndex());
        if ($status != 0) {
            if ($status == 2) {
                $value = 'Reorder';
                $colour = 'red';
            }
            if ($status == 3) {
                $value = 'OK';
                $colour = 'lightgreen';
            }
        } else {
            $value = 'Disable';
            $colour = 'black';
        }

        return '<div style="color:#FFF;font-weight:bold;background:' . $colour . ';border-radius:8px;width:100%;">' . $value . '</div>';
    }
}