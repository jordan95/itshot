<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Block_Adminhtml_Material_Gold_Manage_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'opentechiz_material';
        $this->_controller = 'adminhtml_material_gold_manage';
        parent::__construct();

        // Remove delete button.
        $this->_removeButton('delete');

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);
    }

    public function getHeaderText()
    {
        $gold = Mage::registry('current_gold');
        if ($gold && $gold->getId()) {
            $goldTypeVal = $gold->getGoldType();
            $goldColorVal = $gold->getGoldColor();
            $goldColor = OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_COLOR;
            $goldType = OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_TYPE;
            if ($gold->getGoldType() == '10k' || $gold->getGoldType() == '14k' || $gold->getGoldType() == '18k')
                $label = $goldType[$goldTypeVal] .' / '. $goldColor[$goldColorVal];
            else
            $label = $goldType[$goldTypeVal];
            return Mage::helper('opentechiz_material')->__("Edit gold for '%s'", $this->htmlEscape($label));
        } else {
            return Mage::helper('opentechiz_material')->__('Add New Gold');
        }
    }
}