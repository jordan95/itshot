<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Block_Adminhtml_Material_Gold_Manage_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('goldQtyTabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('opentechiz_material')->__('Gold Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('opentechiz_material')->__('General'),
            'title'     => Mage::helper('opentechiz_material')->__('General'),
            'content'   => $this->getLayout()->createBlock('opentechiz_material/adminhtml_material_gold_manage_edit_tab_form')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }

    public function getGold()
    {
        if (!$this->hasData('current_gold'))
        {
            $this->setData('current_gold', Mage::registry('current_gold'));
        }
        return $this->getData('current_gold');
    }
}