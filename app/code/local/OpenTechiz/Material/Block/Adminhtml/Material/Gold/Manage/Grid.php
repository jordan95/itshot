<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Block_Adminhtml_Material_Gold_Manage_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('goldManageGrid');
        $this->setDefaultSort('gold_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_material/gold')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();

        parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('gold_id', array(
            'header' => Mage::helper('opentechiz_material')->__('ID'),
            'width' => '50',
            'index' => 'gold_id',
            'align' => 'center'
        ));
        $this->addColumn('gold_type', array(
            'header' => Mage::helper('opentechiz_material')->__('Gold Type'),
            'index' => 'gold_type',
            'type' => 'options',
            'options' => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_TYPE
        ));
        $this->addColumn('gold_color', array(
            'header' => Mage::helper('opentechiz_material')->__('Gold Color'),
            'index' => 'gold_color',
            'type' => 'options',
            'options' => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_COLOR
        ));
        $this->addColumn('price', array(
            'header' => Mage::helper('catalog')->__('Price'),
            'width' => '120',
            'index' => 'price',
            'renderer' => 'OpenTechiz_Material_Block_Adminhtml_Renderer_Price'
        ));
        $this->addColumn('weight', array(
            'header' => Mage::helper('opentechiz_material')->__('Weight (gr)'),
            'index' => 'weight',
            'type' => 'text',
            'width' => '150',
            'is_system' => true,
        ));
        $this->addColumn('reorder_point', array(
            'header' => Mage::helper('opentechiz_material')->__('Reorder Point (gr)'),
            'index' => 'reorder_point',
            'type' => 'text',
            'width' => '150',
            'is_system' => true,
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('opentechiz_material')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                0 => 'Disable',
                2 => 'Reorder',
                3 => 'OK'
            ),
            'align' => 'center',
            'renderer' => 'OpenTechiz_Material_Block_Adminhtml_Material_Gold_Manage_Renderer_Status',
            'is_system' => true,
        ));
        $this->addColumn('action',
            array(
                'header' => Mage::helper('opentechiz_material')->__('Action'),
                'width' => '50',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('opentechiz_material')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id',
                        'data-column' => 'action',
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
                'align' => 'center'
            ));

        $this->addExportType('*/*/exportCsv', Mage::helper('opentechiz_material')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('opentechiz_material')->__('XML'));

        return parent::_prepareColumns();

    }

    /**
     * @return $this|Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('gold_id');
        $this->getMassactionBlock()->setFormFieldName('gold');
        $this->getMassactionBlock()->setUseSelectAll(false);
        $status = OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STATUS;

        // Change status
        $this->getMassactionBlock()->addItem('status', array(
            'label'=> Mage::helper('opentechiz_material')->__('Change status'),
            'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('opentechiz_material')->__('Status'),
                    'values' => $status
                )
            )
        ));

        return $this;
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}