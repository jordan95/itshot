<?php

class OpenTechiz_Material_Block_Adminhtml_Material_StoneRequest_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('requestGrid');
        $this->setDefaultSort('stone_request_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_material/stoneRequest')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('stone_request_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('ID'),
            'index'     => 'stone_request_id',
            'width'     => '60px',
        ));
        $this->addColumn('personalized_id', array(
            'header'    => Mage::helper('opentechiz_production')->__('Personalized #'),
            'index'     => 'personalized_id',
            'width'     =>  '60px',
            'renderer'  => 'OpenTechiz_Material_Block_Adminhtml_Renderer_PersonalizedId'
        ));
        $this->addColumn('image', array(
            'header'    => Mage::helper('opentechiz_production')->__('Thumbnail'),
            'width'     =>  '120px',
            'index'     => 'personalized_id',
            'renderer'  => 'OpenTechiz_Material_Block_Adminhtml_Renderer_PersonalizedImage'
        ));
        $this->addColumn('stones', array(
            'header'    =>  Mage::helper('opentechiz_production')->__('Stone Required'),
            'width'     => '600px',
            'index'     =>  'stones',
            'renderer'  => 'OpenTechiz_Material_Block_Adminhtml_Renderer_StoneRequested'
        ));
        $this->addColumn('created_at', array(
            'header'    => Mage::helper('opentechiz_production')->__('Created At'),
            'width'     => '300px',
            'index'     => 'created_at',
            'type'      => 'datetime',
        ));
        $this->addColumn('status', array(
            'header'    => Mage::helper('opentechiz_production')->__('Status'),
            'width'     => '80',
            'index'     => 'status',
            'type'      => 'options',
            'options'   => OpenTechiz_Material_Helper_Data::STONE_REQUEST_STATUS,
            'renderer'  => 'OpenTechiz_Material_Block_Adminhtml_Renderer_Status'
        ));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('stone_request_id');
        $this->getMassactionBlock()->setFormFieldName('stone_request');
        $this->getMassactionBlock()->setUseSelectAll(false);

        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('opentechiz_material')->__('Accept/Decline'),
            'url' => $this->getUrl('*/*/massStatus'),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('opentechiz_material')->__('Status'),
                    'values' => [1 => 'Accept', 2 => 'Decline']
                )
            )
        ));

        return $this;
    }
}