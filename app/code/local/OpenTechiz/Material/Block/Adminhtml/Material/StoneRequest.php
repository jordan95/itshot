<?php

class OpenTechiz_Material_Block_Adminhtml_Material_StoneRequest extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_material_stoneRequest';
        $this->_blockGroup = 'opentechiz_material';
        $this->_headerText = Mage::helper('opentechiz_material')->__('Stone Requests');
        parent::__construct();
        $this->removeButton('add');
    }
}