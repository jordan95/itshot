<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Block_Adminhtml_Material_Stone_Edit_Tabs
    extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('stone_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('opentechiz_material')->__('Stone Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('opentechiz_material')->__('General'),
            'title'     => Mage::helper('opentechiz_material')->__('General'),
            'content'   => $this->getLayout()->createBlock('opentechiz_material/adminhtml_material_stone_edit_tab_general')->toHtml(),
            'active'    => Mage::registry('stone_data')->getId() ? false : true
        ));

        return parent::_beforeToHtml();
    }

    public function getStone()
    {
        if (!$this->hasData('stone_data'))
        {
            $this->setData('stone_data', Mage::registry('stone_data'));
        }
        return $this->getData('stone_data');
    }
}