<?php

class OpenTechiz_Material_Block_Adminhtml_Material_Stone_Renderer_Status
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {

        $currentQty = $row->getQty();
        $onHold = $row->getOnHold();
        $idealStock = $row->getIdealStockLevel();
        $result = $currentQty - $onHold;
        $status = $row->getData($this->getColumn()->getIndex());
        if ($status != 0) {
            if ($status == 2) {
                $value = 'Reorder';
                $bg = 'red';
                $color = 'fff';
            }
            if ($status == 3) {
                $value = 'OK';
                $bg = 'lightgreen';
                $color = 'fff';
            }
            if ($status == 3 && $result < $idealStock) {
                $value = 'OK';
                $bg = 'yellow';
                $color = '000';
            }
        } else {
            $value = 'Disable';
            $bg = 'black';
            $color = 'fff';
        }

        return '<div style="color:#'.$color.';font-weight:bold;background:' . $bg . '; border-radius:8px; width:100%; text-align: center">' . $value . '</div>';
    }
}