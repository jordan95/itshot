<?php

class OpenTechiz_Material_Block_Adminhtml_Renderer_Price extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $html = Mage::helper('core')->currency($row->getData('price'), true, false);
        return $html;
    }
}