<?php

class OpenTechiz_Material_Block_Adminhtml_Renderer_PersonalizedId extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $personalizedId = $row->getPersonalizedId();
        $filter = 'personalized_id='.$personalizedId;
        $filter = base64_encode($filter);
        $url = Mage::helper('adminhtml')->getUrl('adminhtml/request_product/index', array('filter'=>$filter));
        return '<a href="' . $url . '">' . $personalizedId . '</a>';
    }
}