<?php

class OpenTechiz_Material_Block_Adminhtml_Renderer_StoneRequested extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $values =  $row->getStones();
        $values = unserialize($values);

        $html = '<table>
          <tr>
            <th>Stone #</th>
            <th>Stone Type</th>
            <th>Weight</th>
            <th>Shape</th>
            <th>Diameter</th>
            <th>Quality</th>
            <th>Quantity</th>
          </tr>';
        foreach ($values as $value){
            $html .= '<tr>
            <td>'.$value['id'].'</td>
            <td>'.$value['name'].'</td>
            <td>'.$value['weight'].'</td>
            <td>'.$value['shape'].'</td>
            <td>'.$value['diameter'].'</td>
            <td>'.$value['quality'].'</td>
            <td>'.$value['qty'].'</td>
          </tr>';
        }
        $html .='</table>';
        return $html;
    }
}