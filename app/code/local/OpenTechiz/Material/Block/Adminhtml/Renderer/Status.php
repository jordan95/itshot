<?php

class OpenTechiz_Material_Block_Adminhtml_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value = $row->getStatus();
        switch ($value) {
            case 0:
                {
                    $bg = '#fcce77';
                    $color = 'black';
                    break;
                }
            case 1:
                {
                    $bg = 'green';
                    $color = 'white';
                    break;
                }
            case 2:
                {
                    $bg = 'red';
                    $color = 'white';
                    break;
                }
            default:
                {
                    $bg = 'black';
                    $color = 'white';
                    break;
                }
        }

        $html = '<div style="color:' . $color . '; font-weight:bold; background:' . $bg . '; border-radius:8px; width:100%;
         text-align: center">' . OpenTechiz_Material_Helper_Data::STONE_REQUEST_STATUS[$value] . '</div>';
        return $html;
    }
}