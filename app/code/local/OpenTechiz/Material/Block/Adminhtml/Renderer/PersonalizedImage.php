<?php

class OpenTechiz_Material_Block_Adminhtml_Renderer_PersonalizedImage extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $personalizedId =  $row->getData('personalized_id');
        $personalizedProduct = Mage::getModel('opentechiz_production/product')->load($personalizedId);

        $data = unserialize($personalizedProduct->getOption());
        if(isset($data['info_buyRequest']['options']['quotation_product_id'])) {
            if ($quoteProductId = $data['info_buyRequest']['options']['quotation_product_id']) {
                $productImages = Mage::getModel('opentechiz_quotation/product')->load($quoteProductId)->getQpImage();
                $value = explode(',', $productImages)[0];
            } else {
                $value = $personalizedProduct->getImage();
            }
        } else {
            $value = $personalizedProduct->getImage();
        }
        $html = '<img src="'.$value.'" width="140px" height="140px" />';
        return $html;
    }
}