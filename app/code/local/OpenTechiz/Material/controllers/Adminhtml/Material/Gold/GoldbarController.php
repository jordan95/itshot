<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Adminhtml_Material_Gold_GoldbarController extends Mage_Adminhtml_Controller_Action
{
    protected function _initGoldBar($idFieldName = 'id')
    {
        $goldBarId = $this->getRequest()->getParam($idFieldName);
        $goldBar = Mage::getModel('opentechiz_material/goldbar');

        if ($goldBarId) {
            $goldBar->load($goldBarId);
        }

        Mage::register('current_goldbar', $goldBar);
        return $this;
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp/material/gold/goldbar')
            ->_title($this->__('ERP'))
            ->_title($this->__('Material'))
            ->_title($this->__('Gold'))
            ->_title($this->__('Gold Bar Manage'));
        return $this;
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function deleteAction()
    {
        $this->_initGoldBar();
        $goldBar = Mage::registry('current_goldbar');
        $goldBarId = $goldBar->getId();
        $gold = Mage::getModel('opentechiz_material/gold');
        if ($goldBarId > 0) {
            try {
                $goldBar = Mage::getModel('opentechiz_material/goldbar');

                $goldBar->load($goldBarId);
                $goldId = $goldBar->getGoldId();
                $goldBar->delete();
                $gold->updateWeight($goldBar, $goldId);
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('The gold bar has been deleted.'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/material_gold_goldbar');
    }

    /**
     * Edit action
     */
    public function editAction()
    {
        $this->_initGoldBar();
        $goldBar = Mage::registry('current_goldbar');
        $goldBarId = $goldBar->getId();
        if ($goldBarId || $goldBarId == $this->getRequest()->getParam('id')) {
            $this->_initAction();
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('opentechiz_material/adminhtml_material_gold_goldbar_edit'))
                ->_addLeft($this->getLayout()->createBlock('opentechiz_material/adminhtml_material_gold_goldbar_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_material')->__('Gold bar does not exists'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Save action
     */
    public function saveAction()
    {
        $this->_initGoldBar();
        if ($data = $this->getRequest()->getPost()) {
            $redirectBack = $this->getRequest()->getParam('back', false);

            //init model and set data
            $goldBar = Mage::registry('current_goldbar');
            $goldBarId = $this->getRequest()->getParam('id');
            try {
                if ($this->_getPoIncrementId($data['po_increment_id'])->count() != 0)
                {
                    if (!isset($goldBarId)) {
                        $goldBar->setSerialNo($data['serial_no'])
                            ->setGoldId($data['gold_id'])
                            ->setPoIncrementId($data['po_increment_id'])
                            ->setWeight(floatval($data['weight']))
                            ->setStatus($data['status'])
                            ->setCreatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                            ->setUpdatedAt(Mage::helper('opentechiz_material')->_getCurrentTime());
                        $seialNo = $goldBar->getCollection()->addFieldToFilter('serial_no', $data['serial_no'])->getData();
                        if ($seialNo) {
                            throw new Exception('Gold bar already exists!');
                        } else {
                            $goldBar->save();
                        }
                        Mage::getModel('opentechiz_material/gold')->updateWeight($goldBar, $goldBar->load($goldBarId)->getGoldId());
                        $admin = Mage::getSingleton('admin/session')->getUser();
                        if($admin->getId()){
                            Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                                'user_id' => $admin->getId(),
                                'description' => 'Add new gold bar #'.$goldBar->getSerialNo(),
                                'qty' => '+'.$goldBar->getWeight(),
                                'on_hold' => '0',
                                'type' => 2,
                                'stock_id' =>$goldBar->getGoldId()
                            ));
                        }
                    } else {
                        // Get gold_id before changeNo heartbeat task found. Check if cron is configured correctly.
                        $goldIdBefore = $goldBar->load($goldBarId)->getGoldId();
                        $weightBefore = $goldBar->getWeight();
                        $goldBar->load($goldBarId)
                            ->setSerialNo($data['serial_no'])
                            ->setGoldId($data['gold_id'])
                            ->setPoIncrementId($data['po_increment_id'])
                            ->setWeight(floatval($data['weight']))
                            ->setUpdatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                            ->save();
                        // Get gold_id after change
                        $goldIdAfter = $goldBar->load($goldBarId)->getGoldId();
                        if($goldIdBefore!=$goldIdAfter){
                             $admin = Mage::getSingleton('admin/session')->getUser();
                             if($change&&$admin->getId()){
                                 Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                                    'user_id' => $admin->getId(),
                                    'description' => 'gold bar #'.$goldBar->getSerialNo().'change gold type',
                                    'qty' => '+'.$goldBar->getWeight(),
                                    'on_hold' => '0',
                                    'type' => 2,
                                    'stock_id' =>$goldBar->getGoldId()
                                  ));

                                 Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                                    'user_id' => $admin->getId(),
                                    'description' => 'gold bar #'.$goldBar->getSerialNo().'change gold type',
                                    'qty' => '-'.$goldBar->getWeight(),
                                    'on_hold' => '0',
                                    'type' => 2,
                                    'stock_id' =>$goldIdBefore
                                  ));
                            }
                        }else{
                           $change = $goldBar->getWeight() - $weightBefore;
                              $admin = Mage::getSingleton('admin/session')->getUser();
                             if($change&&$admin->getId()){
                                 Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                                'user_id' => $admin->getId(),
                                'description' => 'gold bar #'.$goldBar->getSerialNo().'change weight',
                                'qty' => $change>0?'+'.$goldBar->getWeight():'-'.$goldBar->getWeight(),
                                'on_hold' => '0',
                                'type' => 2,
                                'stock_id' =>$goldBar->getGoldId()
                            ));
                             }
                        }
                        Mage::dispatchEvent('material_goldbar_save_after', array('gold_bar_id' => $goldBarId, 'gold_id_before' => $goldIdBefore, 'gold_id_after' => $goldIdAfter));
                    }
                } else {
                    throw new Exception('Purchase order not exists!');
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('The gold bar has been saved.'));

                // Update status for gold_bar
                $goldBar->updateStatus($goldBarId);

                if ($redirectBack) {
                    $this->_redirect('*/*/edit', array(
                        'id' => $goldBar->getId(),
                        '_current' => true
                    ));
                    return;
                }
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData(false);
                $this->getResponse()->setRedirect($this->getUrl('*/material_gold_goldbar/edit', array('id' => $goldBar->getId())));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function exportCsvAction()
    {
        $fileName = 'goldbars_' . date('Y_m_d_h_i_s') . '.csv';
        $content = $this->getLayout()->createBlock('opentechiz_material/adminhtml_material_gold_goldbar_grid');
        $this->_prepareDownloadResponse($fileName, $content->getCsvFile());
    }

    public function exportXmlAction()
    {
        $fileName = 'goldbars_' . date('Y_m_d_h_i_s') . '.xml';
        $content = $this->getLayout()->createBlock('opentechiz_material/adminhtml_material_gold_goldbar_grid');
        $this->_prepareDownloadResponse($fileName, $content->getExcelFile());
    }

    public function importCSVAction()
    {
        if ($this->getRequest()->getPost()) {
            if (isset($_FILES['file']['name']) && (file_exists($_FILES['file']['tmp_name']))) {
                try {
                    $file = $_FILES['file']['name'];
                    $path = Mage::getBaseDir('var') . DS . 'import' . DS;
                    $uploader = new Varien_File_Uploader('file');
                    $uploader->setAllowedExtensions(array('csv', 'xml'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);
                    $uploader->save($path, $file);
                    $file = Mage::app()->getConfig()->getTempVarDir() . '/import/' . $file;
                    $csv = new Varien_File_Csv();
                    $model = Mage::getModel('opentechiz_material/goldbar');
                    foreach ($csv->getData($file) as $line => $row) {
                        if (0 == $line) {
                            foreach ($row as $i => $rowHeader) {
                                $headerRow[$i] = $rowHeader;
                            }
                            continue; // skip header row
                        }
                        $serialNo = $row[0];
                        $type = strtolower(trim($row[1]));
                        $goldColor = Mage::helper('opentechiz_material')->_processGoldColorVal($row[2]);
                        $purchaseOrder = $row[3];
                        $weight = (float) $row[4];
                        $goldBar = $this->_getGoldBarId($serialNo)->getData();
                        if ($this->_getPoIncrementId($purchaseOrder)->count() != 0) {
                            // check if gold bar exists
                            if ($this->_getGoldBarId($serialNo)->count() != 0) {
                                // check if gold color exists
                                if ($goldColor) {
                                    $goldType = Mage::helper('opentechiz_material')->_processGoldTypeVal($type);
                                    $gold = $this->_getGoldId($goldType, $goldColor)->getData();
                                    $goldId = (int) $gold[0]['gold_id'];
                                    $model->load($goldBar[0]['gold_bar_id'])
                                        ->setSerialNo($serialNo)
                                        ->setGoldId($goldId)
                                        ->setPoIncrementId($purchaseOrder)
                                        ->setWeight($weight)
                                        ->setUpdatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                        ->save();
                                    Mage::getModel('opentechiz_material/gold')->updateWeight($model, $goldId);
                                } else {
                                    $silverType = $type;
                                    $silver = $this->_getSilverId($silverType)->getData();
                                    $silverId = $silver[0]['gold_id'];
                                    $model->load($goldBar[0]['gold_bar_id'])
                                        ->setSerialNo($serialNo)
                                        ->setGoldId($silverId)
                                        ->setPoIncrementId($purchaseOrder)
                                        ->setWeight($weight)
                                        ->setUpdatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                        ->save();
                                    Mage::getModel('opentechiz_material/gold')->updateWeight($model, $silverId);
                                }
                            } else {
                                // check if gold color exists
                                if ($goldColor) {
                                    $goldType = Mage::helper('opentechiz_material')->_processGoldTypeVal($type);
                                    $gold = $this->_getGoldId($goldType, $goldColor)->getData();
                                    $goldId = (int) $gold[0]['gold_id'];
                                    $model = Mage::getModel('opentechiz_material/goldbar');
                                    $model->setSerialNo($serialNo)
                                        ->setGoldId($goldId)
                                        ->setPoIncrementId($purchaseOrder)
                                        ->setWeight($weight)
                                        ->setCreatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                        ->setUpdatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                        ->save();
                                    Mage::getModel('opentechiz_material/gold')->updateWeight($model, $goldId);
                                } else {
                                    $silverType = $type;
                                    $silver = $this->_getSilverId($silverType)->getData();
                                    $silverId = $silver[0]['gold_id'];
                                    $model = Mage::getModel('opentechiz_material/goldbar');
                                    $model->setSerialNo($serialNo)
                                        ->setGoldId($silverId)
                                        ->setPoIncrementId($purchaseOrder)
                                        ->setWeight($weight)
                                        ->setCreatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                        ->setUpdatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                        ->save();
                                    Mage::getModel('opentechiz_material/gold')->updateWeight($model, $silverId);
                                }
                            }
                        } else {
                            throw new Exception('Purchase order not exists!');
                        }
                    }
                    $admin = Mage::getSingleton('admin/session')->getUser();
                    if($admin->getId()){
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Import gold bars data'));
                    }
                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('The gold bar has been imported.'));
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
            $this->_redirect('*/*');
        }
    }

    protected function _getGoldId($goldType, $goldColor)
    {
        $gold = Mage::getModel('opentechiz_material/gold');
        $item = $gold->getCollection()
            ->addFieldToFilter('gold_type', array('eq' => $goldType))
            ->addFieldToFilter('gold_color', array('eq' => $goldColor));
        return $item;
    }

    protected function _getSilverId($goldType)
    {
        $gold = Mage::getModel('opentechiz_material/gold');
        $item = $gold->getCollection()
            ->addFieldToFilter('gold_type', array('eq' => $goldType));
        return $item;
    }

    protected function _getGoldBarId($serialNo)
    {
        $goldBar = Mage::getModel('opentechiz_material/goldbar');
        $item = $goldBar->getCollection()
            ->addFieldToSelect('gold_bar_id')
            ->addFieldToFilter('serial_no', array('eq' => $serialNo));
        return $item;
    }

    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            case 'delete':
                $aclResource = 'admin/erp/material/gold/goldbar/delete';
                break;
            default:
                $aclResource = 'admin/erp/material/gold/goldbar';
                break;
        }
        return Mage::getSingleton('admin/session')->isAllowed($aclResource);
    }

    protected function _getPoIncrementId($input)
    {
        $purchase = Mage::getModel('opentechiz_purchase/purchase');
        $item = $purchase->getCollection()
            ->addFieldToFilter('po_increment_id', array('eq' => $input));
        return $item;
    }
}