<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Adminhtml_Material_StoneController extends Mage_Adminhtml_Controller_Action
{
    protected function _initStone($idFieldName = 'id')
    {
        $stoneId = (int)$this->getRequest()->getParam($idFieldName);
        $stone = Mage::getModel('opentechiz_material/stone');

        if ($stoneId) {
            $stone->load($stoneId);
        }

        Mage::register('stone_data', $stone);
        return $this;
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp/material/stone')
            ->_title($this->__('ERP'))
            ->_title($this->__('Material'))
            ->_title($this->__('Stone'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    /**
     * Edit stone action
     */
    public function editAction()
    {
        $this->_initStone();
        $stone = Mage::registry('stone_data');
        $stoneId = $stone->getId();
        if ($stoneId || $stoneId == $this->getRequest()->getParam('id')) {

            $this->_initAction();

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('opentechiz_material/adminhtml_material_stone_edit'))
                ->_addLeft($this->getLayout()->createBlock('opentechiz_material/adminhtml_material_stone_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_material')->__('Stone does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * New stone action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        $this->_initStone();
        if ($data = $this->getRequest()->getPost()) {
            $redirectBack = $this->getRequest()->getParam('back', false);
            $stone = Mage::registry('stone_data');
            $stoneId = $this->getRequest()->getPost('id');

            try {
                if ($data['price'] < 0) {
                    throw new Exception('Invalid price!');
                }
                if ($data['weight'] < 0) {
                    throw new Exception('Invalid weight!');
                }
                if ($data['qty'] < 0) {
                    throw new Exception('Invalid quantity!');
                }
                else {
                    if (!isset($stoneId)) {
                        $stone->setData($data)
                            ->setCreatedAt(now())
                            ->setUpdatedAt(now())
                            ->save();
                    } else {
                        $stone->load($stoneId)
                            ->setStoneType($data['stone_type'])
                            ->setWeight($data['weight'])
                            ->setQuality($data['quality'])
                            ->setShape($data['shape'])
                            ->setDiameter($data['diameter'])
                            ->setPrice($data['price'])
                            ->setReorderPoint($data['reorder_point'])
                            ->setIdealStockLevel($data['ideal_stock_level'])
                            ->setShelfLocation($data['shelf_location'])
                            ->setUpdatedAt(now())
                            ->save();
                    }
                }

                $stone->checkStatus($stone->getStatus(), $stone->getQty() - $stone->getOnHold(), $stone->getReorderPoint(), $stoneId);

                $admin = Mage::getSingleton('admin/session')->getUser();
                if($admin->getId()){
                    Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Save stone data #'.$stone->getId()));
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('The stone has been saved.')
                );

                if ($redirectBack) {
                    $this->_redirect('*/*/edit', array(
                        'id' => $stone->getId(),
                        '_current' => true
                    ));
                    return;
                }
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData(false);
                $this->getResponse()->setRedirect($this->getUrl('*/material_stone/edit', array('id' => $stone->getId())));
                return;
            }
        }
        $this->getResponse()->setRedirect($this->getUrl('*/material_stone'));
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        $this->_initStone();
        $stone = Mage::registry('stone_data');
        if ($stone->getId()) {
            try {
                $stone->load($stone->getId());
                $stone->delete();

                $admin = Mage::getSingleton('admin/session')->getUser();
                if($admin->getId()){
                    Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Delete stone data #'.$stone->getId()));
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('The stone has been deleted.'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/material_stone');
    }

    /**
     * MassDelete action
     */
    public function massDeleteAction()
    {
        $stoneIds = $this->getRequest()->getParam('stone');
        if (!is_array($stoneIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($stoneIds as $stoneId) {
                    $stone = Mage::getModel('opentechiz_material/stone')->load($stoneId);
                    $stone->delete();

                    $admin = Mage::getSingleton('admin/session')->getUser();
                    if($admin->getId()){
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'MassAction delete stone data #'.$stoneId));
                    }
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($stoneIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * MassStatus action
     */
    public function massStatusAction()
    {
        $stoneIds = $this->getRequest()->getParam('stone');

        if (!is_array($stoneIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                $admin = Mage::getSingleton('admin/session')->getUser();
                foreach ($stoneIds as $stoneId) {
                    $stone = Mage::getSingleton('opentechiz_material/stone')
                        ->load($stoneId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                    $stone->checkStatus($stone->getStatus(), $stone->getQty() - $stone->getOnHold(), $stone->getReorderPoint(), $stoneId);

                    if($admin->getId()){
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'MassAction Status change stone data #'.$stoneId));
                    }
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($stoneIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Export customer grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'stones_' . date('Y_m_d_h_i_s') . '.csv';
        $content = $this->getLayout()->createBlock('opentechiz_material/adminhtml_material_stone_grid');
        $this->_prepareDownloadResponse($fileName, $content->getCsvFile());
    }

    /**
     * Export customer grid to XML format
     */
    public function exportXmlAction()
    {
        $fileName = 'stone_' . date('Y_m_d_h_i_s') . '.xml';
        $content = $this->getLayout()->createBlock('opentechiz_material/adminhtml_material_stone_grid');
        $this->_prepareDownloadResponse($fileName, $content->getExcelFile());
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/material/stone');
    }

    public function importCSVAction()
    {
        if ($this->getRequest()->getPost()) {
            if (isset($_FILES['file']['name']) && (file_exists($_FILES['file']['tmp_name']))) {
                try {
                    $file = $_FILES['file']['name'];
                    $path = Mage::getBaseDir('var') . DS . 'import' . DS;
                    $uploader = new Varien_File_Uploader('file');
                    $uploader->setAllowedExtensions(array('csv', 'xml'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);
                    $uploader->save($path, $file);
                    $file = Mage::app()->getConfig()->getTempVarDir() . '/import/' . $file;
                    $csv = new Varien_File_Csv();
                    $allIds = Mage::getModel('opentechiz_material/stone')->getCollection()->getAllIds();
                    foreach ($csv->getData($file) as $line => $row) {
                        if (0 == $line) {
                            foreach ($row as $i => $rowHeader) {
                                $headerRow[$i] = $rowHeader;
                            }
                            continue; // skip header row
                        }
                        $stoneType = $this->_processVal($row[1]);
                        $quality = $this->_processVal($row[3]);
                        $shape = $this->_processVal($row[4]);
                        if (in_array($row[0], $allIds))
                        {
                            Mage::getModel('opentechiz_material/stone')->load($row[0])
                                ->setStoneType($stoneType)
                                ->setWeight($row[2])
                                ->setQuality($quality)
                                ->setShape($shape)
                                ->setDiameter($row[5])
                                ->setPrice($this->_processPrice($row[6]))
                                ->setQty($row[7])
                                ->setReorderPoint($row[8])
                                ->setIdealStockLevel($row[9])
                                ->setShelfLocation($row[10])
                                ->setCreatedAt($row[11])
                                ->setUpdatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                ->save();
                        } else {
                            Mage::getModel('opentechiz_material/stone')->setStoneType($stoneType)
                                ->setStoneType($stoneType)
                                ->setWeight($row[2])
                                ->setQuality($quality)
                                ->setShape($shape)
                                ->setDiameter($row[5])
                                ->setPrice($this->_processPrice($row[6]))
                                ->setQty($row[7])
                                ->setReorderPoint($row[8])
                                ->setIdealStockLevel($row[9])
                                ->setShelfLocation($row[10])
                                ->setCreatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                ->setUpdatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                ->setStatus(3)
                                ->save();
                        }
                    }
                    $admin = Mage::getSingleton('admin/session')->getUser();
                    if($admin->getId()){
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Import stone data'));
                    }

                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('The stone has been imported.'));
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
            $this->_redirect('*/*');
        }
    }

    protected function _processVal($input)
    {
        $output = strtolower(trim(preg_replace('/\s+/', '', $input)));
        return $output;
    }

    protected function _processPrice($input)
    {
        $symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
        $output = trim(str_replace($symbol, '', $input));
        return $output;
    }
}