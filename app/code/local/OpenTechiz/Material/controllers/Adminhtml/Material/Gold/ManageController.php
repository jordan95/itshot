<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Adminhtml_Material_Gold_ManageController extends Mage_Adminhtml_Controller_Action
{
    protected function _initGold($idFieldName = 'id')
    {
        $goldId = (int)$this->getRequest()->getParam($idFieldName);
        $gold = Mage::getModel('opentechiz_material/gold');

        if ($goldId) {
            $gold->load($goldId);
        }

        Mage::register('current_gold', $gold);
        return $this;
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp/material/gold/goldmanage')
            ->_title($this->__('ERP'))
            ->_title($this->__('Material'))
            ->_title($this->__('Gold'))
            ->_title($this->__('Gold Manage'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this->_initGold();
        $gold = Mage::registry('current_gold');
        $goldId = $gold->getId();
        if ($goldId || $goldId == $this->getRequest()->getParam('id')) {
            $this->_initAction();
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('opentechiz_material/adminhtml_material_gold_manage_edit'))
                ->_addLeft($this->getLayout()->createBlock('opentechiz_material/adminhtml_material_gold_manage_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_material')->__('Gold does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Save action
     */
    public function saveAction()
    {
        $this->_initGold();
        if ($data = $this->getRequest()->getPost()) {
            $redirectBack = $this->getRequest()->getParam('back', false);

            //init model and set data
            $gold = Mage::registry('current_gold');
            $goldId = $gold->getId();

            try {
                if (!$goldId) {
                    if ($data['gold_type'] == '10k' || $data['gold_type'] == '14k' || $data['gold_type'] == '18k') {
                        $gold->setData($data)
                            ->setCreatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                            ->setUpdatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                            ->save();
                    } else{
                        if ($this->_checkSilverTypeExists($data['gold_type'])->count() == 0) {
                            $gold->setData($data)
                                ->setCreatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                ->setUpdatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                ->save();
                        } else {
                            throw new Exception("This type has exists.");
                        }
                    }
                } else {
                    if ($data['gold_type'] == '10k' || $data['gold_type'] == '14k' || $data['gold_type'] == '18k') {
                        $gold->load($goldId)
                            ->setGoldType($data['gold_type'])
                            ->setGoldColor($data['gold_color'])
                            ->setPrice($data['price'])
                            ->setUpdatedAt(now())
                            ->setReorderPoint($data['reorder_point'])
                            ->setStatus($data['status'])
                            ->save();
                    } else {
                        $gold->load($goldId)
                            ->setGoldType($data['gold_type'])
                            ->setPrice($data['price'])
                            ->setUpdatedAt(now())
                            ->setReorderPoint($data['reorder_point'])
                            ->setStatus($data['status']);
                        $gold->save();
                    }
                }

                $gold->checkStatus($gold->getStatus(), $gold->getWeight(), $gold->getReorderPoint(), $goldId);

                $admin = Mage::getSingleton('admin/session')->getUser();
                if($admin->getId()){
                    Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Save gold #'.$goldId));
                }
                Mage::dispatchEvent('material_data_change_save_after');
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('The gold has been saved.')
                );

                if ($redirectBack) {
                    $this->_redirect('*/*/edit', array(
                        'id' => $gold->getId(),
                        '_current' => true
                    ));
                    return;
                }
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData(false);
                $this->getResponse()->setRedirect($this->getUrl('*/material_gold_manage/edit', array('id' => $gold->getId())));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * MassStatus action
     */
    public function massStatusAction()
    {
        $goldIds = $this->getRequest()->getParam('gold');

        if (!is_array($goldIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s).'));
        } else {
            try {
                $admin = Mage::getSingleton('admin/session')->getUser();
                foreach ($goldIds as $goldId) {
                    $gold = Mage::getSingleton('opentechiz_material/gold')->load($goldId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true);
                    $gold->save();
                    $gold->checkStatus($gold->getStatus(), $gold->getWeight(), $gold->getReorderPoint(), $goldId);

                    if($admin->getId()){
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'MassAction change status gold #'.$goldId));
                    }
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated.', count($goldIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Export customer grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'golds_' . date('Y_m_d_h_i_s') . '.csv';
        $content = $this->getLayout()->createBlock('opentechiz_material/adminhtml_material_gold_manage_grid');
        $this->_prepareDownloadResponse($fileName, $content->getCsvFile());
    }

    /**
     * Export customer grid to XML format
     */
    public function exportXmlAction()
    {
        $fileName = 'golds_' . date('Y_m_d_h_i_s') . '.xml';
        $content = $this->getLayout()->createBlock('opentechiz_material/adminhtml_material_gold_manage_grid');
        $this->_prepareDownloadResponse($fileName, $content->getExcelFile());
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/material/gold/goldmanage');
    }

    public function importCSVAction()
    {
        if ($this->getRequest()->getPost()) {
            if (isset($_FILES['file']['name']) && (file_exists($_FILES['file']['tmp_name']))) {
                try {
                    $file = $_FILES['file']['name'];
                    $path = Mage::getBaseDir('var') . DS . 'import' . DS;
                    $uploader = new Varien_File_Uploader('file');
                    $uploader->setAllowedExtensions(array('csv', 'xml'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);
                    $uploader->save($path, $file);
                    $file = Mage::app()->getConfig()->getTempVarDir() . '/import/' . $file;
                    $csv = new Varien_File_Csv();
                    $allIds = Mage::getModel('opentechiz_material/gold')->getCollection()->getAllIds();
                    foreach ($csv->getData($file) as $line => $row) {
                        if (0 == $line) {
                            foreach ($row as $i => $rowHeader) {
                                $headerRow[$i] = $rowHeader;
                            }
                            continue; // skip header row
                        }

                        // get gold color
                        $goldColor = Mage::helper('opentechiz_material')->_processGoldColorVal($row[2]);
                        // check if gold id exists
                        if (in_array($row[0], $allIds)) {
                            // check gold color exists
                            if ($goldColor) {
                                // this is gold
                                $goldType = strtolower($row[1]);
                                Mage::getModel('opentechiz_material/gold')->load($row[0])
                                    ->setGolType($goldType)
                                    ->setGoldColor($goldColor)
                                    ->setPrice($this->_processPrice($row[3]))
                                    ->setUpdatedAt(now())
                                    ->save();
                            } else {
                                $silverType = Mage::helper('opentechiz_material')->_processSilverTypeVal($row[1]);
                                Mage::getModel('opentechiz_material/gold')->load($row[0])
                                    ->setGoldType($silverType)
                                    ->setPrice($row[3])
                                    ->setUpdatedAt(now())
                                    ->save();
                            }
                        } else {
                            // check gold color exists
                            if ($goldColor) {
                                // this is gold
                                $goldType = strtolower($row[1]);
                                echo $goldType.'<br>';
                                Mage::getModel('opentechiz_material/gold')->setGoldType($goldType)
                                    ->setGoldColor($goldColor)
                                    ->setPrice($this->_processPrice($row[3]))
                                    ->setCreatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                    ->setUpdatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                    ->setStatus(3)
                                    ->save();
                            } else {
                                $silverType = Mage::helper('opentechiz_material')->_processSilverTypeVal($row[1]);
                                if ($this->_checkSilverTypeExists($silverType)->count() == 0) {
                                    Mage::getModel('opentechiz_material/gold')->setGoldType($silverType)
                                        ->setPrice($this->_processPrice($row[3]))
                                        ->setCreatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                        ->setUpdatedAt(Mage::helper('opentechiz_material')->_getCurrentTime())
                                        ->setStatus(3)
                                        ->save();
                                } else {
                                    throw new Exception($row[1].' has exists!');
                                }
                            }
                        }
                    }
                    $admin = Mage::getSingleton('admin/session')->getUser();
                    if($admin->getId()){
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Import gold data'));
                    }
                    Mage::dispatchEvent('material_data_change_save_after');
                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('The gold has been imported.'));
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
            $this->_redirect('*/*');
        }
    }

    protected function _checkSilverTypeExists($silverType)
    {
        $gold = Mage::getModel('opentechiz_material/gold');
        $item = $gold->getCollection()
            ->addFieldToFilter('gold_type', array('eq' => $silverType));
        return $item;
    }

    protected function _processPrice($input)
    {
        $symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
        $output = trim(str_replace($symbol, '', $input));
        return $output;
    }
}