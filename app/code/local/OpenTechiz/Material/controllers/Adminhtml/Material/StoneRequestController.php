<?php

class OpenTechiz_Material_Adminhtml_Material_StoneRequestController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_title($this->__('Material'))->_title($this->__('Stone Requests'));
        $this->loadLayout()
            ->_setActiveMenu('erp');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_material/adminhtml_material_stoneRequest'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_material/adminhtml_material_stoneRequest_Grid')->toHtml()
        );
    }

    public function massStatusAction()
    {
        $stoneIds = $this->getRequest()->getParam('stone_request');
        if (!$stoneIds) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                $error = 0;
                foreach ($stoneIds as $stoneId) {
                    $stone = Mage::getSingleton('opentechiz_material/stoneRequest')
                        ->load($stoneId);
                    $admin = Mage::getSingleton('admin/session')->getUser();

                    if($this->getRequest()->getParam('status') == 1){
                        if($stone->checkAmountInStock($stone)){
                            $stone->takeStone($stone);
                            Mage::getSingleton('opentechiz_material/stoneRequest')
                                ->load($stoneId)
                                ->setStatus($this->getRequest()->getParam('status'))
                                ->setIsMassupdate(true)
                                ->save();
                            if($admin->getId()){
                                Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Accept Stone request #'.$stoneId));
                            }
                        } else $error = 1;
                    } else {
                        $stone->releaseStone($stone);
                        Mage::getSingleton('opentechiz_material/stoneRequest')
                            ->load($stoneId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                        if($admin->getId()){
                            Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Decline Stone request #'.$stoneId));
                        }
                    }
                }
                if($error == 0) {
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', count($stoneIds))
                    );
                }
                elseif($error == 1) {
                    $this->_getSession()->addError(
                        $this->__('There are requests that cant be accepted due to lack of stones in stock')
                    );
                }
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
}