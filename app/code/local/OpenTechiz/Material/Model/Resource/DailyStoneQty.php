<?php

class OpenTechiz_Material_Model_Resource_DailyStoneQty extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_material/dailyStoneQty', 'id');
    }
}