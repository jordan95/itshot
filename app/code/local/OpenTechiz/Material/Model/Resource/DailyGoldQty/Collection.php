<?php

class OpenTechiz_Material_Model_Resource_DailyGoldQty_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_material/dailyGoldQty');
    }
}