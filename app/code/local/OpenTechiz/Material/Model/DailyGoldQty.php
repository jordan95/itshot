<?php

class OpenTechiz_Material_Model_DailyGoldQty extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_material/dailyGoldQty');
    }

    public function _processWasUsedEach7d ($goldId, $currentWeight) {
        $period = '7d';
        $oldWeight =  $this->_getGoldOldQty($period, $goldId);
        if ($oldWeight != null) {
            $result = $oldWeight - $currentWeight;
            return number_format($result,4);
        } else {
            return 'null';
        }
    }

    public function _processWasUsedEach14d ($goldId, $currentWeight) {
        $period = '14d';
        $oldWeight =  $this->_getGoldOldQty($period, $goldId);
        if ($oldWeight != null) {
            $result = $oldWeight - $currentWeight;
            return number_format($result,4);
        } else {
            return 'null';
        }
    }

    public function _processWasUsedEach30d ($goldId, $currentWeight) {
        $period = '30d';
        $oldWeight =  $this->_getGoldOldQty($period, $goldId);
        if ($oldWeight != null) {
            $result = $oldWeight - $currentWeight;
            return number_format($result,4);
        } else {
            return 'null';
        }
    }

    protected function _getGoldOldQty($period, $goldId) {

        // get current time when cron job send email
        $currentTime = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');

        // check period
        switch ($period) {
            case '7d':
                $date = date('Y-m-d H:i:s', strtotime($currentTime. ' - 7 days'));
                break;
            case '14d':
                $date = date('Y-m-d H:i:s', strtotime($currentTime. ' - 14 days'));
                break;
            case '30d':
                $date = date('Y-m-d H:i:s', strtotime($currentTime. ' - 30 days'));
                break;
        }

        // get time start to end
        $dateStart = date('Y-m-d' . ' 00:00:00', strtotime($date));
        $dateEnd = date('Y-m-d' . ' 23:59:59', strtotime($date));

        $rows = $this->getCollection()
            ->addFieldToFilter('time', array('from' => $dateStart, 'to' => $dateEnd))
            ->addFieldToFilter('gold_id', array('eq' => $goldId));

        if ($rows->count() > 0) {
            foreach ($rows as $item) {
                $oldWeight = $item->getWeight();
            }
        } else {
            $oldWeight = null;
        }

        return $oldWeight;
    }
}