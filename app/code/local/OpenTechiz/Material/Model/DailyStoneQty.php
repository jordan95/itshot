<?php

class OpenTechiz_Material_Model_DailyStoneQty extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_material/dailyStoneQty');
    }

    public function _processWasUsedEach7d ($stoneId, $currentQty) {
        $period = '7d';
        $oldQty =  $this->_getStoneOldQty($period, $stoneId);
        if ($oldQty != null) {
            return $oldQty - $currentQty;
        } else {
            return 'null';
        }
    }

    public function _processWasUsedEach14d ($stoneId, $currentQty) {
        $period = '14d';
        $oldQty =  $this->_getStoneOldQty($period, $stoneId);
        if ($oldQty != null) {
            return $oldQty - $currentQty;
        } else {
            return 'null';
        }
    }

    public function _processWasUsedEach30d ($stoneId, $currentQty) {
        $period = '30d';
        $oldQty =  $this->_getStoneOldQty($period, $stoneId);
        if ($oldQty != null) {
            return $oldQty - $currentQty;
        } else {
            return 'null';
        }
    }

    protected function _getStoneOldQty($period, $stoneId) {

        // get current time when cron job send email
        $currentTime = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');

        // check period
        switch ($period) {
            case '7d':
                $date = date('Y-m-d H:i:s', strtotime($currentTime. ' - 7 days'));
                break;
            case '14d':
                $date = date('Y-m-d H:i:s', strtotime($currentTime. ' - 14 days'));
                break;
            case '30d':
                $date = date('Y-m-d H:i:s', strtotime($currentTime. ' - 30 days'));
                break;
        }

        // get time start to end
        $dateStart = date('Y-m-d' . ' 00:00:00', strtotime($date));
        $dateEnd = date('Y-m-d' . ' 23:59:59', strtotime($date));

        $rows = $this->getCollection()
            ->addFieldToFilter('time', array('from' => $dateStart, 'to' => $dateEnd))
            ->addFieldToFilter('stone_id', array('eq' => $stoneId));

        if ($rows->count() > 0) {
            foreach ($rows as $item) {
                $oldQty = $item->getQty();
            }
        } else {
            $oldQty = null;
        }

        return $oldQty;
    }
}