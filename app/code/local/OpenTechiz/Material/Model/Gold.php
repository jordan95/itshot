<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Model_Gold extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_material/gold');
    }

    public function updateWeight($goldBarModel, $goldId)
    {
        // Filter collection by gold_id.
        $goldBarCollection = $goldBarModel->getCollection()->addFieldToFilter('gold_id', $goldId)->getData();
        $model = $this->load($goldId);
        //Calculate weight
        $totalWeight = 0;
        $totalWeightUse = 0;
        foreach ($goldBarCollection as $items) {
            $totalWeight += $items['weight'];
            $totalWeightUse += $items['weight_use'];
        }

        // Set manage to gold model.
        $model->setWeight($totalWeight - $totalWeightUse)->save();

        // Check then set status
        $this->checkStatus($model->getStatus(), $model->getWeight(), $model->getReorderPoint(), $goldId);
    }

    public function reUpdateWeight($goldBarModel, $goldIds)
    {
        foreach ($goldIds as $goldId)
        {
            $this->updateWeight($goldBarModel, $goldId);
        }
    }

    public function checkStatus($currentStatus, $currentWei, $reorderValue, $goldId)
    {
        $model = $this->load($goldId);
        if ($currentStatus != 0) {
            if ($currentWei < $reorderValue) {
                $model->setStatus(2);
            }
            if ($currentWei >= $reorderValue) {
                $model->setStatus(3);
            }
            $model->save();
        }
    }
}