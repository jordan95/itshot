<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Model_Observer
{
    public function updateGoldWeight($observer)
    {
        $goldBarId = $observer->getGoldBarId();
        $goldIdBefore = $observer->getGoldIdBefore();
        $goldIdAfter = $observer->getGoldIdAfter();
        $goldModel = Mage::getModel('opentechiz_material/gold');
        $goldBarModel = Mage::getModel('opentechiz_material/goldbar');
        $goldId = $goldBarModel->load($goldBarId)->getGoldId();
        if ($goldIdBefore === $goldIdAfter)
        {
            $goldModel->updateWeight($goldBarModel, $goldId);
        }
        else {
            $goldIds = [$goldIdBefore, $goldIdAfter];
            $goldModel->reUpdateWeight($goldBarModel, $goldIds);
        }
    }

    public function addNotification(){
        Mage::getConfig()->saveConfig('opentechiz_material/data_update/cron_is_run', 0, 'default', 0);
    }

    public function removeNotification(){
        Mage::getConfig()->saveConfig('opentechiz_material/data_update/cron_is_run', 1, 'default', 0);
    }
}