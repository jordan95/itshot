<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Model_Stone extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_material/stone');
    }

    public function checkStatus($currentStatus, $currentQty, $reorderValue, $stoneId)
    {
        if ($currentStatus != 0) {
            if ($currentQty < $reorderValue) {
                $this->load($stoneId)->setStatus(2)->save();
            }
            if ($currentQty >= $reorderValue) {
                $this->load($stoneId)->setStatus(3)->save();
            }
        }
    }
}