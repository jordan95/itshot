<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Model_Cron
{
    const STONE_MODEL = 'opentechiz_material/stone';
    const GOLD_MODEL = 'opentechiz_material/gold';

    const  DAILY_STONE_QTY_MODEL = 'opentechiz_material/dailyStoneQty';
    const  DAILY_GOLD_QTY_MODEL = 'opentechiz_material/dailyGoldQty';

    /**
     * @return bool
     */
    protected function _checkCountCollection()
    {
        $stoneModel = Mage::getModel(self::STONE_MODEL);
        $stoneCollection = $stoneModel->getCollection()
            ->addFieldToFilter('status', array('eq' => 2));

        $goldModel = Mage::getModel(self::GOLD_MODEL);
        $goldCollection = $goldModel->getCollection()
            ->addFieldToFilter('status', array('eq' => 2));

        if ($stoneCollection->count() > 0 || $goldCollection->count() > 0)
            return true;
    }

    public function sendReorderNotyEmail()
    {
        if ($this->_checkCountCollection() != false) {
            // Set sender information
            $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
            $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

            // Getting recipient E-Mail
            $to = explode(',', Mage::getStoreConfig('opentechiz_material/general/recipient_email'));

            $emailTemplate = Mage::getModel('core/email_template');
            $emailTemplate->loadDefault('material_noty_reorder_tpl');
            $emailTemplate->setTemplateSubject(Mage::app()->getStore()->getName() . ': Material ReOrder Notification');

            $emailTemplate->setSenderName($senderName);
            $emailTemplate->setSenderEmail($senderEmail);

            $emailTemplateVariables = array();

            foreach ($to as $recipientEmail) {
                if ($recipientEmail) {
                    $emailTemplate->send($recipientEmail, $emailTemplateVariables);
                }
            }

        }
    }

    public function saveDailyCurrentQty()
    {
        $stoneModel = Mage::getModel(self::STONE_MODEL);
        $goldModel = Mage::getModel(self::GOLD_MODEL);
        $this->_saveDataStoneQty($stoneModel);
        $this->_saveDataGoldQty($goldModel);
        $this->_checkCondition();
    }

    protected function _saveDataStoneQty($model)
    {
        $stoneQtyModel = Mage::getModel(self::DAILY_STONE_QTY_MODEL);
        $collection = $model->getCollection();
        $data = array();
        foreach ($collection as $item) {
            $data['stone_id']   = $item->getStoneId();
            $data['time']       = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['qty']        = $item->getQty();

            $stoneQtyModel->setData($data)
                ->save();
        }
    }

    protected function _saveDataGoldQty($model)
    {
        $goldQtyModel = Mage::getModel(self::DAILY_GOLD_QTY_MODEL);
        $collection = $model->getCollection();
        $data = array();
        foreach ($collection as $item) {
            $data['gold_id']   = $item->getGoldId();
            $data['time']      = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['weight']    = $item->getWeight();

            $goldQtyModel->setData($data)
                ->save();
        }
    }

    protected function _checkCondition() {
        $stone = Mage::getModel(self::DAILY_STONE_QTY_MODEL);
        $gold = Mage::getModel(self::DAILY_GOLD_QTY_MODEL);
        $currentTime = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $date = date('Y-m-d H:i:s', strtotime($currentTime. ' - 31 days'));
        $dateStart = date('Y-m-d' . ' 00:00:00', strtotime($date));
        $dateEnd = date('Y-m-d' . ' 23:59:59', strtotime($date));
        $this->_delItemsExceed30Days($stone, $dateStart, $dateEnd);
        $this->_delItemsExceed30Days($gold, $dateStart, $dateEnd);
    }

    protected function _delItemsExceed30Days($model, $dateStart, $dateEnd) {
        $rows = $model->getCollection()->addFieldToFilter('time', array('from' => $dateStart, 'to' => $dateEnd))->addFieldToSelect('id');
        if ($rows->count() > 0) {
            foreach ($rows->getData() as $item) {
                $id = $item['id'];
                $model->load($id)->delete();
            }
        }
    }
}
