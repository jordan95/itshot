<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Model_StoneRequest extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_material/stoneRequest');
    }

    public function addStoneRequest($personalizedId, $productId, $optionArray){
        $product = Mage::getModel('catalog/product')->load($productId);
        if(Mage::helper('personalizedproduct')->isPersonalizedProduct($product)) {
            $data = [];
            $data['personalized_id'] = $personalizedId;
            $stoneOptionModel = Mage::getModel('personalizedproduct/price_stone');
//        $product = Mage::getModel('catalog/product')->load($productId);

            $result = [];
            $admin = Mage::getSingleton('admin/session')->getUser();

            $barcode = Mage::getModel('opentechiz_production/product')->load($personalizedId)->getBarcode();
            $sku = Mage::getModel('opentechiz_inventory/item')->load($barcode, 'barcode')->getSku();

            $optionData = [];
            $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
            for ($i = 1; $i < count($parts) - 2; $i++) {
                switch ($i){
                    case 1:
                        $key = 'Center Stone';
                        break;
                    case 2:
                        $key = 'Stone 2';
                        break;
                    case 3:
                        $key = 'Stone 3';
                        break;
                    case 4:
                        $key = 'Stone 4';
                        break;

                }
                $optionData[$key] = $parts[$i];
            }

            foreach ($optionArray as $option) {
                if ($option['option_type'] == 'stone') {
                    $option['value'] = $optionData[$option['label']];
                    $stoneSku = strtolower(str_replace(' ', '', $option['value']));
                    $quality = '';
                    if (strpos($stoneSku, 'diamond') !== false) {
                        $quality = str_replace('diamond', '', $stoneSku);
                        if(in_array($quality, OpenTechiz_PersonalizedProduct_Helper_Data::QUALITY)){
                            $stoneSku = 'diamond';
                        } else{
                            $quality = '';
                        }
                    }

                    $query = 'stone.stone_id = main_table.stone_id and main_table.product_id = "' . $productId . '" and main_table.option_name = "' . $option['label'] . '" and stone.stone_type = "' . $stoneSku . '"';
                    if ($quality != '') {
                        $query .= 'and stone.quality = "' . $quality . '"';
                    }
                    $collection = $stoneOptionModel->getCollection()->join(array('stone' => 'opentechiz_material/stone'), $query, '*');

                    foreach ($collection as $requiredStone) {
                        $array = [];
                        $array['id'] = $requiredStone->getStoneId();
                        $array['name'] = $requiredStone->getStoneType();
                        $array['weight'] = $requiredStone->getWeight();
                        $array['shape'] = $requiredStone->getShape();
                        $array['diameter'] = $requiredStone->getDiameter();
                        $array['quality'] = $requiredStone->getQuality();
                        $array['qty'] = $requiredStone->getQuantity();

                        array_push($result, $array);

                        if ($admin->getId()) {
                            Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                                'user_id' => $admin->getId(),
                                'description' => 'Stone Request for production with personalized id #' . $data['personalized_id'],
                                'qty' => '0',
                                'on_hold' => '+' . $array['qty'],
                                'type' => 1,
                                'stock_id' => $array['id']
                            ));
                        }
                    }
                }
            }
            $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['stones'] = serialize($result);

            $this->setData($data)->save();
            $this->putStoneOnHold(serialize($result));
        }
    }

    public function addStoneRequestQuotation($personalizedId, $optionArray){
        $data = [];
        $data['personalized_id'] = $personalizedId;
        $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');

        $result = [];
        $stoneModel = Mage::getModel('opentechiz_material/stone');
        $admin = Mage::getSingleton('admin/session')->getUser();

        foreach ($optionArray as $requiredStone){
            $stone = $stoneModel->load($requiredStone->getData('option_value'));
            $array = [];
            $array['id'] = $stone->getId();
            $array['name'] = $stone->getStoneType();
            $array['weight'] = $stone->getWeight();
            $array['shape'] = $stone->getShape();
            $array['diameter'] = $stone->getDiameter();
            $array['quality'] = $stone->getQuality();
            $array['qty'] = $requiredStone->getData('qty');
            array_push($result, $array);

            if($admin->getId()){
                Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                    'user_id' => $admin->getId(),
                    'description' => 'Stone Request for quotation production with personalized id #'.$data['personalized_id'],
                    'qty' => '0',
                    'on_hold' => '+'.$array['qty'],
                    'type' => 1,
                    'stock_id' => $array['id']
                ));
            }
        }
        $data['stones'] = serialize($result);
        $this->setData($data)->save();
        $this->putStoneOnHold(serialize($result));
    }

    public function addStoneRequestNonPersonalized($personalizedId, $productId){
        $data = [];
        $result = [];
        $data['personalized_id'] = $personalizedId;
        $stoneOptionModel = Mage::getModel('personalizedproduct/price_stone');

        $query = 'stone.stone_id = main_table.stone_id';
        $collection = $stoneOptionModel->getCollection()
            ->join(array('stone' => 'opentechiz_material/stone'), $query, '*')
            ->addFieldToFilter('product_id', $productId);

        foreach ($collection as $requiredStone) {
            $array = [];
            $array['id'] = $requiredStone->getStoneId();
            $array['name'] = $requiredStone->getStoneType();
            $array['weight'] = $requiredStone->getWeight();
            $array['shape'] = $requiredStone->getShape();
            $array['diameter'] = $requiredStone->getDiameter();
            $array['quality'] = $requiredStone->getQuality();
            $array['qty'] = $requiredStone->getQuantity();

            array_push($result, $array);

            $admin = Mage::getSingleton('admin/session')->getUser();
            if ($admin->getId()) {
                Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                    'user_id' => $admin->getId(),
                    'description' => 'Stone Request for production with personalized id #' . $data['personalized_id'],
                    'qty' => '0',
                    'on_hold' => '+' . $array['qty'],
                    'type' => 1,
                    'stock_id' => $array['id']
                ));
            }
        }

        if(count($result)>0) {
            $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['stones'] = serialize($result);

            $this->setData($data)->save();
            $this->putStoneOnHold(serialize($result));
        }
    }

    public function putStoneOnHold($stones){
        $stones = unserialize($stones);
        $stoneModel = Mage::getModel('opentechiz_material/stone');

        foreach ($stones as $stone){
            $stoneData = $stoneModel->load($stone['id']);
            $stoneData->setOnHold((int)$stoneData->getOnHold() + (int)$stone['qty'])->save();
        }
    }

    public function takeStone($request){
        $stones = $request->getStones();
        $stones = unserialize($stones);
        $stoneModel = Mage::getModel('opentechiz_material/stone');
        $admin = Mage::getSingleton('admin/session')->getUser();

        foreach ($stones as $stone){
            $stoneData = $stoneModel->load($stone['id']);
            $stoneData->setOnHold((int)$stoneData->getOnHold() - (int)$stone['qty'])
                ->setQty((int)$stoneData->getQty() - (int)$stone['qty'])->save();

            if($admin->getId()){
                Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                    'user_id' => $admin->getId(),
                    'description' => 'Accept Stone Request #'.$request->getId(),
                    'qty' => '-'.$stone['qty'],
                    'on_hold' => '-'.$stone['qty'],
                    'type' => 1,
                    'stock_id' => $stone['id']
                ));
            }
        }
    }

    public function releaseStone($request){
        $stones = $request->getStones();
        $stones = unserialize($stones);
        $stoneModel = Mage::getModel('opentechiz_material/stone');
        $admin = Mage::getSingleton('admin/session')->getUser();

        foreach ($stones as $stone){
            $stoneData = $stoneModel->load($stone['id']);
            $stoneData->setOnHold((int)$stoneData->getOnHold() - (int)$stone['qty'])->save();

            if($admin->getId()){
                Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                    'user_id' => $admin->getId(),
                    'description' => 'Decline Stone Request #'.$request->getId(),
                    'qty' => '0',
                    'on_hold' => '-'.$stone['qty'],
                    'type' => 1,
                    'stock_id' => $stone['id']
                ));
            }
        }
    }

    public function checkAmountInStock($request){
        $stones = $request->getStones();
        $stones = unserialize($stones);
        $stoneModel = Mage::getModel('opentechiz_material/stone');
        $flag = true;

        foreach ($stones as $stone) {
            $stoneData = $stoneModel->load($stone['id']);
            if(((int)$stoneData->getQty() - (int)$stone['qty'] < 0)||$stoneData->getStatus()==0) $flag = false;
        }

        return $flag;
    }

    public function returnStone($requestId){
        $request = $this->load($requestId, 'personalized_id');
        if($request && $request->getStoneRequestId()) {
            $admin = Mage::getSingleton('admin/session')->getUser();

            if ($request->getStatus() == 0) {
                $this->releaseStone($request);
                $request->setStatus(2)->save();
            } elseif ($request->getStatus() == 1) {
                $request->setStatus(3)->save();
                $stones = $request->getStones();
                $stoneModel = Mage::getModel('opentechiz_material/stone');

                foreach (unserialize($stones) as $stone) {
                    $stoneData = $stoneModel->load($stone['id']);
                    $stoneData->setQty((int)$stoneData->getQty() + (int)$stone['qty'])->save();

                    if ($admin->getId()) {
                        Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                            'user_id' => $admin->getId(),
                            'description' => 'Return Stone of Stone Request #' . $request->getId(),
                            'qty' => '+' . $stone['qty'],
                            'on_hold' => '',
                            'type' => 1,
                            'stock_id' => $stone['id']
                        ));
                    }
                }
            }
        }
    }
}