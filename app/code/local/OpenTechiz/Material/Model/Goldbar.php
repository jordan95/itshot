<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Model_Goldbar extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('opentechiz_material/goldbar');
    }

    public function loadBySerial($serial_no)
    {
        return $this->getCollection()->addFieldToFilter('serial_no', $serial_no)->getFirstItem();
    }

    public function updateStatus($goldBarId)
    {
        $currentWei = $this->load($goldBarId)->getWeight();
        $currentWeiUse = $this->load($goldBarId)->getWeightUse();
        $remainWei = $currentWei-$currentWeiUse;
        $currentStatus = $this->load($goldBarId)->getStatus();
        if ($currentStatus != 0) {
            if ($remainWei <= 0) {
                $this->load($goldBarId)->setStatus(0)->save();
            }
            else {
                $this->load($goldBarId)->setStatus(1)->save();
            }
        }
    }

    protected function _afterLoad()
    {
        parent::_afterLoad();
        $this->setOldData($this->getData());

    }
    protected function _beforeSave(){
        
        $weight = $this->getData('weight');
        
        $purchase = Mage::getModel('opentechiz_purchase/purchase');
        $purchase_id = $purchase->getCollection()
            ->addFieldToFilter('po_increment_id', array('eq' => $this->getData('po_increment_id')))->getFirstItem()->getId();
        
            # code...
        $purchase_gold = Mage::getModel('opentechiz_purchase/purchasegold')->getCollection()
            ->addFieldToFilter('po_id', array('eq' => $purchase_id))->addFieldToFilter('gold_id', array('eq' => $this->getData('gold_id')))->getFirstItem();

        

        if($purchase_gold->getId())
            $purchase_weight = $purchase_gold->getQty();
        else
            throw Mage::exception('Mage_Eav', Mage::helper('eav')->__('This purchase Order not ordered this gold . Please check.'));

        $goldBars = Mage::getModel('opentechiz_material/goldbar')->getCollection()->addFieldToFilter('po_increment_id', array('eq' => $this->getData('po_increment_id')))->addFieldToFilter('gold_id', array('eq' => $this->getData('gold_id')));
        $weight_added = 0;
        foreach ($goldBars as $goldBar) {
            # code...
            if($this->getId()&&$goldBar->getId()==$this->getId())
                continue;
            $weight_added += $goldBar->getWeight();
        }
        
        if($weight+$weight_added > $purchase_weight){
                 
            throw Mage::exception('Mage_Eav', Mage::helper('eav')->__('Total gold bar weight  more than purchase gold weight. Please check.'));
        }else{
            $po_weight = $weight+$weight_added;
            $this->setWeightDelivered($po_weight);
            $this->setPoGoldId($purchase_gold->getId());
        }
        
        
    }

    protected function _afterSave(){
        parent::_afterSave();
        $po_weight = $this->getWeightDelivered();
        $purchasegold_Id =  $this->getPoGoldId();

        if($this->getData('gold_id') != $this->getOldData('gold_id')||$this->getOldData('po_increment_id')!=$this->getData('po_increment_id')){
            $oldpurchase_id = Mage::getModel('opentechiz_purchase/purchase')->getCollection()
                ->addFieldToFilter('po_increment_id', array('eq' => $this->getOldData('po_increment_id')))->getFirstItem()->getId();

            # code...
            $oldpurchase_gold = Mage::getModel('opentechiz_purchase/purchasegold')->getCollection()
                ->addFieldToFilter('po_id', array('eq' => $oldpurchase_id))->addFieldToFilter('gold_id', array('eq' => $this->getOldData('gold_id')))->getFirstItem();
            $old_po_weight = $oldpurchase_gold->getDeliveredQty() - $this->getOldData('weight');
            $oldpurchase_gold->setDeliveredQty($old_po_weight)->save();
        }
        $purchase_gold = Mage::getModel('opentechiz_purchase/purchasegold')->load($purchasegold_Id)->setDeliveredQty($po_weight)->save();

        if((float)$this->getWeight() == (float)$this->getWeightUse()
            && $this->getWeight() > 0
            && $this->getStatus() != 1){
            $this->setStatus(1)->save();
        }elseif((float)$this->getWeight() > (float)$this->getWeightUse() && $this->getStatus() != 0){
            $this->setStatus(0)->save();
        }
    }

       /**
     * Processing object after delete data
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterDelete()
    {
        $oldpurchase_id = Mage::getModel('opentechiz_purchase/purchase')->getCollection()
            ->addFieldToFilter('po_increment_id', array('eq' => $this->getOldData('po_increment_id')))->getFirstItem()->getId();
        
            # code...
           $oldpurchase_gold = Mage::getModel('opentechiz_purchase/purchasegold')->getCollection()
            ->addFieldToFilter('po_id', array('eq' => $oldpurchase_id))->addFieldToFilter('gold_id', array('eq' => $this->getOldData('gold_id')))->getFirstItem();
           $old_po_weight = $oldpurchase_gold->getDeliveredQty() - $this->getOldData('weight');
           $oldpurchase_gold->setDeliveredQty($old_po_weight)->save();
       
    }
}