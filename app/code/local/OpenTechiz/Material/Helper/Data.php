<?php

/**
 * OpenTechiz Software Solution
 *
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Hiep Mai <hiepmt@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018 , OpenTechiz Software Solution
 *
 */

class OpenTechiz_Material_Helper_Data extends Mage_Core_Helper_Abstract
{
    const BASE_GOLD_COLOR = ['white', 'yellow', 'rose', 'white/yellow', 'yellow/rose', 'white/rose'];

    const OPPENTECHIZ_MATERIAL_STONE_TYPE = [
        'amethyst' => 'Amethyst',
        'aquamarine' => 'Aquamarine',
        'citrine' => 'Citrine',
        'diamond' => 'Diamond',
        'diamondi' => 'Diamond',
        'diamondvs' => 'Diamond',
        'diamondsi' => 'Diamond',
        'emerald' => 'Emerald',
        'morganite' => 'Morganite',
        'peridot' => 'Peridot',
        'ruby' => 'Ruby',
        'tanzanite' => 'Tanzanite',
        'diamondblack' => 'Black Diamond',
        'diamondblue' => 'Blue Diamond',
        'diamondbrown' => 'Brown Diamond',
        'diamondgreen' => 'Green Diamond',
        'diamondpink' => 'Pink Diamond',
        'diamondyellow' => 'Yellow Diamond',
        'blackdiamond' => 'Black Diamond',
        'bluediamond' => 'Blue Diamond',
        'browndiamond' => 'Brown Diamond',
        'greendiamond' => 'Green Diamond',
        'pinkdiamond' => 'Pink Diamond',
        'yellowdiamond' => 'Yellow Diamond',
        'bluesapphire' => 'Blue Sapphire ',
        'pinksapphire' => 'Pink Sapphire',
        'whitesapphire' => 'White Sapphire',
        'yellowsapphire' => 'Yellow Sapphire',
        'bluetopaz' => 'Blue Topaz',
        'greenamethyst' => 'Green Amethyst',
        'moissanite' => 'Moissanite',
        'quartzrose' => 'Quartz Rose',
        'quartzsmoky' => 'Quartz Smoky',
        'redgarnet' => 'Red Garnet',
        'rhodolitegarnet' => 'Rhodolite Garnet',
        'tourmalinegreen' => 'Tourmaline Green',
        'tourmalinepink' => 'Tourmaline Pink',
    ];

    const OPPENTECHIZ_MATERIAL_STONE_SHAPE = [
        'baguette' => 'Baguette',
        'hexagon' => 'Hexagon',
        'cushion' => 'Cushion',
        'triangle'=> 'triangle',
        'emeraldcut' => 'Emerald Cut',
        'heart' => 'Heart',
        'oval' => 'Oval',
        'pear' => 'Pear',
        'princess' => 'Princess',
        'round' => 'Round',
        'trillion' => 'Trillion'
    ];

    const OPPENTECHIZ_MATERIAL_STATUS = [
        '0' => 'Disable',
        '1' => 'Enable',
    ];

    const OPPENTECHIZ_MATERIAL_GOLDBAR_STATUS = [
        '0' => 'Open',
        '1' => 'Consumed',
    ];

    const OPPENTECHIZ_MATERIAL_GOLD_TYPE = [
        '10k' => '10K',
        '14k' =>'14K',
        '18k' => '18K',
        'platinum' =>'Platinum',
        'silver' => 'Silver'
    ];

    const GOLD_NAME_FRONT_END = [
        '10kwhitegold' => '10K White Gold',
        '14kwhitegold' =>'14K White Gold',
        '18kwhitegold' => '18K White Gold',
        '10kyellowgold' => '10K Yellow Gold',
        '14kyellowgold' =>'14K Yellow Gold',
        '18kyellowgold' => '18K Yellow Gold',
        '10krosegold' => '10K Rose Gold',
        '14krosegold' =>'14K Rose Gold',
        '18krosegold' => '18K Rose Gold',
        '10kgreengold' => '10K Green Gold',
        '14kgreengold' =>'14K Green Gold',
        '18kgreengold' => '18K Green Gold',
        'platinum' =>'Platinum',
        'silver' => 'Silver'
    ];

    const OPPENTECHIZ_MATERIAL_GOLD_COLOR = [
        'whitegold' => 'White Gold',
        'yellowgold' =>'Yellow Gold',
        'rosegold' =>'Rose Gold',
        'greengold' => 'Green Gold'
    ];

    const OPPENTECHIZ_MATERIAL_STONE_QUALITY = [
        'vs' => 'VS',
        'si' => 'SI',
        'i' => 'I',
        'vs-si' => 'VS - SI',
        'si-i' => 'SI - I',
        'aaa' => 'AAA'
    ];

    const STONE_REQUEST_STATUS = [
        0 => 'Pending',
        1 => 'Accepted',
        2 => 'Declined',
        3 => 'Returned',
    ];

    const ALL_GOLD_TYPE = [
        '18kwhitegold' ,
        '14kwhitegold',
        '10kwhitegold',
        '18kyellowgold',
        '14kyellowgold',
        '10kyellowgold',
        '18krosegold',
        '14krosegold',
        '10krosegold',
        'platinum',
        'palladium',
        'silver'
    ];

    const ALL_STONE_TYPE = [
        'alexandrite',
        'amber',
        'amethyst',
        'ametrine',
        'aquamarine',
        'citrine',
        'diamond',
        'fancydiamond',
        'emerald',
        'garnet',
        'iolite',
        'jade',
        'kunzite',
        'lapislazuli',
        'moonstone',
        'morganite',
        'opal',
        'pearl',
        'peridot',
        'rosequartz',
        'ruby',
        'sapphire',
        'spinel',
        'sunstone',
        'tanzanite',
        'topaz',
        'tourmaline',
        'turquoise',
        'zircon',
        'diamondblack',
        'diamondblue',
        'diamondbrown',
        'diamondgreen',
        'diamondpink',
        'diamondyellow',
        'bluesapphire',
        'pinksapphire',
        'whitesapphire',
        'yellowsapphire',
        'bluetopaz',
        'greenamethyst',
        'moissanite',
        'quartzrose',
        'quartzsmoky',
        'redgarnet',
        'rhodolitegarnet',
        'tourmalinegreen',
        'tourmalinepink' ,
    ];


    public function _processGoldTypeVal($input)
    {
        $new = explode(' ', $input);
        $output = strtolower(trim($new[1]));
        return $output;
    }

    public function _processSilverTypeVal($input)
    {
        $output = strtolower(trim($input));
        return $output;
    }

    public function _processGoldColorVal($input)
    {
        $output = strtolower(trim(preg_replace('/\s+/', '', $input)));
        return $output;
    }

    public function _getCurrentTime()
    {
        return Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
    }
}