<?php

/**
 * MageWorx
 * Admin Order Editor extension
 *
 * @category   MageWorx
 * @package    MageWorx_OrdersEdit
 * @copyright  Copyright (c) 2016 MageWorx (http://www.mageworx.com/)
 */
class OpenTechiz_CustomEditOrder_Model_Edit extends MageWorx_OrdersEdit_Model_Edit
{

    protected function checkQuoteItems($quoteId, Mage_Sales_Model_Order $order)
    {
        if ($this->_quoteItemsAlreadyChecked) {
            return $this;
        }

        // Core Resource
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        // Table
        $tsht_catalog_product_entity = $resource->getTableName('catalog_product_entity');
        $tsht_catalog_product_option = $resource->getTableName('catalog_product_option');
        $tsht_catalog_product_option_type_value = $resource->getTableName('catalog_product_option_type_value');
        $tsht_catalog_product_option_title = $resource->getTableName('catalog_product_option_title');
        $tsht_catalog_product_option_type_title = $resource->getTableName('catalog_product_option_type_title');

        /** @var MageWorx_OrdersEdit_Helper_Edit $editHelper */
        $editHelper = Mage::helper('mageworx_ordersedit/edit');

        /** @var Mage_Sales_Model_Order_item $orderItem */
        $quote = Mage::getModel('sales/quote')->setStoreId($order->getStoreId())->load($quoteId);
        $quote->setIsSuperMode(true);
        # add
        foreach ($order->getAllItems() as $orderItem) {
            /** @var $orderItem Mage_Sales_Model_Order_Item */
            $quoteItemId = $orderItem->getQuoteItemId();
            $quoteItem = Mage::getModel('sales/quote_item')->load($quoteItemId);
            if ($quoteItem && $editHelper->isQuoteItemAvailable($quoteItem, $orderItem)) {
                if ($quote && $quote->getId() && $quoteItem->getProductId()) {
                    $quoteItem->setQuote($quote);
                    $quoteItem->setQty($orderItem->getQtyOrdered());
                    if (!$quoteItem->getCustomPrice()) {
                        $quoteItem->setCustomPrice((float) $orderItem->getPrice());
                    }
                    $orderItemProductOptions = $orderItem->getProductOptions();
                    if (!empty($orderItemProductOptions['options'])) {
                        $productOptionChanged = false;
                        foreach ($orderItemProductOptions['options'] as $i => $_option) {
                            $option = Mage::getModel('catalog/product_option')->load($_option['option_id']);
                            if (!$option->getId()) {
                                $productOptionChanged = true;
                                $query = $readConnection->select();
                                $query->from(array("e" => $tsht_catalog_product_entity));
                                $query->reset(Zend_Db_Select::COLUMNS);
                                $query->join(array("opt" => $tsht_catalog_product_option), '`e`.`entity_id` = `opt`.`product_id`', array("option_id", "option_type" => "type"));
                                $query->join(array("otv" => $tsht_catalog_product_option_type_value), '`otv`.`option_id` = `opt`.`option_id`', array("option_value" => "option_type_id"));
                                $query->join(array("ot" => $tsht_catalog_product_option_title), '`ot`.`option_id` =`opt`.`option_id` AND `ot`.`title` = "' . $_option['label'] . '"', array("label" => "title"));
                                $query->join(array("ott" => $tsht_catalog_product_option_type_title), '`otv`.`option_type_id` = `ott`.`option_type_id` AND (`ott`.`title` = "' . $_option['value'] . '" OR `otv`.`sku` = "' . $_option['value'] . '")', array("value" => "title", "sku" => "otv.sku"));
                                $query->where("e.entity_id = ?", $orderItem->getProductId());
                                $result = $readConnection->fetchRow($query);
                                if (!empty($result)) {
                                    $orderItemProductOptions['options'][$i]['option_id'] = $result['option_id'];
                                    $orderItemProductOptions['options'][$i]['option_value'] = $result['option_value'];
                                }
                            }
                        }

                        if ($productOptionChanged) {
                            $orderItemProductOptions['info_buyRequest']['options'] = [];
                            foreach ($orderItemProductOptions['options'] as $o) {
                                $orderItemProductOptions['info_buyRequest']['options'][$o['option_id']] = $o['option_value'];
                            }
                            $orderItem->setProductOptions($orderItemProductOptions);
                            $orderItem->save();
                        }
                    }
                    $quoteItemOptions = Mage::getResourceModel('sales/quote_item_option_collection');
                    $quoteItemOptions->addItemFilter($quoteItem->getId());
                    $info_buyRequest = null;
                    if ($quoteItemOptions->count() > 0) {
                        foreach ($quoteItemOptions as $_itemOption) {
                            if ($_itemOption->getCode() == 'info_buyRequest') {
                                $info_buyRequest = unserialize($_itemOption->getValue());
                            }
                            $_itemOption->delete();
                        }
                    }
                    $options = [];
                    if (!empty($orderItemProductOptions['info_buyRequest'])) {
                        if ($info_buyRequest) {
                            if (!empty($orderItemProductOptions['options'])) {
                                foreach ($orderItemProductOptions['options'] as $_option) {
                                    $info_buyRequest['options'][$_option['option_id']] = $_option['option_value'];
                                }
                            }
                            $options[] = [
                                'item_id' => $quoteItem->getId(),
                                'product_id' => $quoteItem->getProductId(),
                                'code' => 'info_buyRequest',
                                'value' => serialize($info_buyRequest)
                            ];
                        } else {
                            $options[] = [
                                'item_id' => $quoteItem->getId(),
                                'product_id' => $quoteItem->getProductId(),
                                'code' => 'info_buyRequest',
                                'value' => serialize($orderItemProductOptions['info_buyRequest'])
                            ];
                        }
                    }
                    if (!empty($orderItemProductOptions['options'])) {
                        $optionIds = [];
                        foreach ($orderItemProductOptions['options'] as $_option) {
                            $optionIds[] = $_option['option_id'];
                            $options[] = [
                                'item_id' => $quoteItem->getId(),
                                'product_id' => $quoteItem->getProductId(),
                                'code' => 'option_' . $_option['option_id'],
                                'value' => $_option['option_value']
                            ];
                        }
                        if (count($_option['option_id']) > 0) {
                            $options[] = [
                                'item_id' => $quoteItem->getId(),
                                'product_id' => $quoteItem->getProductId(),
                                'code' => 'option_ids',
                                'value' => join(",", $optionIds)
                            ];
                        }
                    }
                    if (count($options) > 0) {
                        $quoteItem->setOptions($options);
                    }
                    if (!$quoteItem->getOriginalCustomPrice()) {
                        $quoteItem->setOriginalCustomPrice((float) $orderItem->getOriginalPrice());
                    }
                    $quoteItem->setRowTotal($orderItem->getPrice() * $orderItem->getQtyOrdered());
                    $quoteItem->setBaseRowTotal($orderItem->getBasePrice() * $orderItem->getQtyOrdered());
                    $quoteItem->setDataChanges(true);
                    $quoteItem->save();
                }
                continue;
            }

            if ($quote->getId()) {
                $product = Mage::getModel('catalog/product')->setStoreId($order->getStoreId())->load($orderItem->getProductId());
                if ($product->getId() && $quoteId) {
                    try {
                        $newQuoteItem = Mage::getModel('sales/convert_order')->itemToQuoteItem($orderItem);
                        //$newQuoteItem->unsetData('custom_price');
                        //$newQuoteItem->unsetData('original_custom_price');
                        $newQuoteItem->setCustomPrice((float) $orderItem->getPrice());
                        // fix task 2834, set exact price to quote in order to avoid when edit order wrong price.
                        if($order->getOrderType() == OpenTechiz_SalesExtend_Helper_Data::ORDER_REPAIR_TYPE){
                             $newQuoteItem->setOriginalCustomPrice((float) $orderItem->getPrice());
                        }else{
                            $newQuoteItem->setOriginalCustomPrice((float) $orderItem->getOriginalPrice());
                        }
                        
                        $newQuoteItem->setRowTotal($orderItem->getPrice() * $orderItem->getQtyOrdered());
                        $newQuoteItem->setBaseRowTotal($orderItem->getBasePrice() * $orderItem->getQtyOrdered());
                        $qty = $this->getQtyRest($orderItem, true);
                        $newQuoteItem->setQuoteId($quoteId)
                                ->setProduct($product)
                                ->setQuote($quote)
                                ->setQty($qty)
                                ->save();

                        $orderItem->setQuoteItemId($newQuoteItem->getItemId())->save();
                    } catch (Exception $e) {
                        Mage::getSingleton('core/session')->addError($e->getMessage());
                    }
                }
            }
        }

        $this->_quoteItemsAlreadyChecked = true;

        return $this;
    }

    public function getQuoteByOrder(Mage_Sales_Model_Order $order)
    {
        /** @var MageWorx_OrdersEdit_Helper_Edit $editHelper */
        $editHelper = Mage::helper('mageworx_ordersedit/edit');

        if (!$this->getQuote()) {
            $quoteId = $order->getQuoteId();
            $storeId = $order->getStoreId();
            $this->checkOrderQuote($order);
            $this->checkQuoteItems($quoteId, $order);

            /** @var Mage_Sales_Model_Quote $quote */
            $quote = Mage::getModel('sales/quote')->setStoreId($storeId)->load($quoteId);
            $quote->setIsSuperMode(true);
            $editHelper->removeRefundedItemsFromQuote($quote, $order);
            $this->setQuote($quote);
        } else {
            $quote = $this->getQuote();
        }
        if ($order->getCustomerBalanceAmount() > 0) {
            $quote->setOrderCustomerBalanceAmount($order->getCustomerBalanceAmount());
        }
        return $quote;
    }

    protected function checkOrderQuote(Mage_Sales_Model_Order $order)
    {
        $quoteId = $order->getQuoteId();
        $quote = Mage::getModel('sales/quote')->setStoreId($order->getStoreId())->load($quoteId);
        $converter = Mage::getSingleton('sales/convert_order');
        if ($quote && $quote->getId()) {
            $quote = $converter->toQuote($order, $quote);
        } else {
            $quote = $converter->toQuote($order);
        }
        $quote->setIsSuperMode(true);
        // Copy shipping address data
        if ($order->getShippingAddress()) {
            $shippingAddress = $quote->getShippingAddress();
            Mage::helper('core')->copyFieldset('sales_convert_order_address', 'to_quote_address', $order->getShippingAddress(), $shippingAddress);
            Mage::helper('core')->copyFieldset('sales_convert_order', 'to_quote_address_shipping', $order, $shippingAddress);
        }

        // Copy billing address
        if ($order->getBillingAddress()) {
            $billingAddress = $quote->getBillingAddress();
            Mage::helper('core')->copyFieldset('sales_convert_order_address', 'to_quote_address', $order->getBillingAddress(), $billingAddress);
        }

        // Copy payment
        if ($order->getPayment()) {
            $payment = $quote->getPayment();
            $converter->paymentToQuotePayment($order->getPayment(), $payment);
        }

        // Recreate shipping rates
        if ($quote->getShippingAddress() && !$quote->getShippingAddress()->getGroupedAllShippingRates()) {
            $quote->getShippingAddress()->setCollectShippingRates(true)->collectShippingRates();
        }

        $quote->save();
        
        $order->setQuoteId($quote->getId())->save();

        return $this;
    }

    /**
     * Save payment method
     *
     * @return $this
     * @throws Exception
     */
    protected function updatePayment()
    {

        if (!isset($this->changes['payment'])) {
            return $this;
        }

        $quotePayment = $this->quote->getPayment();
        $orderPayment = $this->order->getPayment();
        // void payment 
        if ($orderPayment->getMethodInstance()->getCode() == "authorizenet") {
            if($orderPayment->canVoid(new Varien_Object())){
                $orderPayment->getMethodInstance()->setStore($this->order->getStoreId())->void($orderPayment);
            }
        }

        $orderPayment = $this->getConverter()->paymentToOrderPayment($quotePayment, $orderPayment);
        $orderPayment->save();
        $quotePayment->save();
        // add new transaction
        if ($this->changes['payment']['method'] == "authorizenet") {
            $obj = new Varien_Object();
            $obj->setData($this->changes['payment']);
            if (!$orderPayment->getMethodInstance() instanceof  Mage_Paygate_Model_Authorizenet) {
                $instance = Mage::helper('payment')->getMethodInstance("authorizenet");
                $instance->setInfoInstance($orderPayment);
                $orderPayment->setMethodInstance($instance);
            }
            $orderPayment->getMethodInstance()->assignData($obj);
            try {
                $orderPayment->place();
                Mage::getSingleton('adminhtml/session')->addSuccess('Authorized Successfully');
            } catch (Exception $exc) {
                // Contact Customer
                $this->order->setInternalStatus(24);
                Mage::getSingleton('adminhtml/session')->addError('Authorization failed, contact bank or try different payment method');
            }
        }

        if ($this->changes['payment']['method'] == 'MultiplePayment' && $multiplePayment = $this->changes['payment']['MultiplePayment']) {
            $methods = $multiplePayment['method'];
            $amounts = $multiplePayment['amount'];
            $order = $this->getOrder();
            $payments = Mage::getModel('PaymentTracker/Payment')->getCollection()->AddFieldtoFilter('ptp_order_id', $order->getId());
            foreach ($payments as $id => $value) {
                # code...
                Mage::getModel('PaymentTracker/Payment')->load($id)->delete();
            }
            foreach ($methods as $key => $value) {
                # code...
                if ($value) {
                    Mage::getModel('PaymentTracker/Payment')->addPayment($value, $amounts[$key], date('Y-m-d'), 'edit order payment', $this->order->getId());
                }
            }

            $totalPaid = Mage::helper('PaymentTracker/Order')->getTotalPaid($order);
            $totalDue = Mage::helper('PaymentTracker/Order')->getTotalDue($order);

            if ($totalPaid != $order->gettotal_paid()) {
                $order->setbase_total_paid($totalPaid)
                        ->settotal_paid($totalPaid)
                        ->setbase_total_due($totalDue)
                        ->settotal_due($totalDue);
            }

            //update
        }

        unset($this->changes['payment']);

        return $this;
    }

//    protected function removeOrderItem(Mage_Sales_Model_Order $order, Mage_Sales_Model_Order_Item $orderItem)
//    {
//        $invoices = $order->getInvoiceCollection();
//        $canDelete = false;
//        if (count($invoices) > 0) {
//            foreach ($invoices as $invoice) {
//                foreach ($invoice->getAllItems() as $item) {
//                    if ($item->getOrderItemId() == $orderItem->getId()) {
//                        if ($invoice->canCancel()) {
//                            $this->cancelOrderItem($orderItem, $item->getQty());
//                            $canDelete = true;
//                        } else if ($invoice->canRefund()) {
//                            $this->refundOrderItem($orderItem, $item->getQty());
//                        }
//                    }
//                }
//            }
//        } else {
//            $canDelete = true;
//        }
//
//        if (($orderItem->getQtyOrdered() && $orderItem->getQtyOrdered() == $orderItem->getQtyCanceled() && $canDelete) || $canDelete) {
//            if ($orderItem->getChildrenItems()) {
//                /** @var Mage_Sales_Model_Order_Item $childOrderItem */
//                foreach ($orderItem->getChildrenItems() as $childOrderItem) {
//                    Mage::getModel('sales/quote_item')->load($childOrderItem->getQuoteItemId())->delete();
//                    $childOrderItem->isDeleted(true);
//                    $childOrderItem->delete();
//                }
//            }
//            Mage::getModel('sales/quote_item')->load($orderItem->getQuoteItemId())->delete();
//            $orderItem->isDeleted(true);
//            $orderItem->delete();
//        }
//
//        if ($orderItem->getProductType() == 'bundle' || $orderItem->getProductType() == 'configurable') {
//            /** @var Mage_Sales_Model_Order_Item $childOrderItem */
//            foreach ($orderItem->getChildrenItems() as $childOrderItem) {
//                $this->cancelOrderItem($childOrderItem, $this->getQtyRest($childOrderItem, false));
//            }
//        }
//        $orderItem
//                ->setSubtotal(0)
//                ->setBaseSubtotal(0)
//                ->setRowTotalInclTax(0)
//                ->setBaseRowTotalInclTax(0)
//                ->setTaxAmount(0)
//                ->setBaseTaxAmount(0)
//                ->setTaxPercent(0)
//                ->setDiscountAmount(0)
//                ->setBaseDiscountAmount(0)
//                ->setRowTotal(0)
//                ->setBaseRowTotal(0);
//        return $this;
//    }

    protected function changeItemQty($orderItem, $newQty, $origQtyOrdered)
    {
        $orderItemQty = $this->getQtyRest($orderItem);

        if ($newQty < $orderItemQty) {
            $qtyToRemove = $orderItemQty - $newQty;
            $orderItem->setQtyOrdered($newQty);
        } else {
            $qtyDiff = $newQty - $orderItemQty;
            $this->removeQtyFromStock($orderItem->getProductId(), $qtyDiff);
            $orderItem->setQtyOrdered($origQtyOrdered + $qtyDiff);
        }
        $orderItem->save();

        return $this;
    }

    protected function removeQtyFromStock($productId, $qty)
    {
        /** @var Mage_CatalogInventory_Model_Stock_Item $stockItem */
        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
        if(!$stockItem->getId()) {
            return;
        }
        $qtyAfter = $stockItem->getQty() - $qty;
        if ($qtyAfter <= 0) {
            $isInStock = 0;
            $qty = 0;
        } else {
            $isInStock = 1;
            $qty = $qtyAfter;
        }
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $cataloginventory_stock_item =  $resource->getTableName('cataloginventory_stock_item');
        $writeConnection->query( "UPDATE {$cataloginventory_stock_item} SET is_in_stock = $isInStock, qty = $qty WHERE product_id = " . $productId);
    }

    protected function postProcessItems()
    {
        $quote = $this->getQuote();
        $order = $this->getOrder();

        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
        foreach ($quote->getAllVisibleItems() as $quoteItem) {
            /** @var Mage_Sales_Model_Order_Item $orderItem */
            $orderItem = $order->getItemByQuoteItemId($quoteItem->getItemId());

            if (isset($orderItem) && (in_array($orderItem->getItemId(), $this->_savedOrderItems))) {
                continue;
            }
            //check quote item
            $quoteItem->checkData();
            if ($quoteItem->getHasError()) {
                Mage::throwException("Validate Quote Item not valid data.");
            }
            /** @var Mage_Sales_Model_Order_Item $orderItem */
            $orderItem = $this->getConverter()->itemToOrderItem($quoteItem, $orderItem);
            $orderItem->setOrderId($order->getId());
            $stockItem = $quoteItem->getProduct()->getStockItem();
            if ($stockItem instanceof Mage_CatalogInventory_Model_Stock_Item) {
                if (!$stockItem->getIsInStock()) {
                    $orderItem->setOriginalPrice($quoteItem->getProduct()->getSpecialPrice());
                    $orderItem->setBaseOriginalPrice($quoteItem->getProduct()->getSpecialPrice());
                }
            }
            $orderItem->save();

            $quoteChildrens = $quoteItem->getChildren();
            $orderChildrens = array();
            foreach ($quoteChildrens as $childQuoteItem) {

                /** @var Mage_Sales_Model_Order_Item $childOrderItem */
                $childOrderItem = $order->getItemByQuoteItemId($childQuoteItem->getItemId());

                if (isset($childOrderItem) && in_array($childOrderItem->getItemId(), $this->_savedOrderItems)) {
                    continue;
                }

                /** @var Mage_Sales_Model_Order_Item $childOrderItem */
                $childOrderItem = $this->getConverter()->itemToOrderItem($childQuoteItem, $childOrderItem);
                $childOrderItem->setOrderId($order->getId());
                $childOrderItem->setParentItem($orderItem);
                $childOrderItem->setParentItemId($orderItem->getId());
                $childOrderItem->save();
                $orderChildrens[] = $childOrderItem;
            }

            if (!empty($orderChildrens)) {
                foreach ($orderChildrens as $child) {
                    $orderItem->addChildItem($child);
                }
                $orderItem->save();
            }

            /** @var Mage_Sales_Model_Resource_Order_Item_Collection $orderItemsCollection */
            $orderItemsCollection = $order->getItemsCollection();
            if ($orderItemsCollection->getItemById($orderItem->getId())) {
                $orderItemsCollection->removeItemByKey($orderItem->getId());
            }
            $orderItemsCollection->addItem($orderItem);

            /*             * * Add new items to log ** */
            $changedItem = $quoteItem;

            $qtyBefore = 0;
            $itemChange = array(
                'name' => $changedItem->getName(),
                'qty_before' => $qtyBefore,
                'qty_after' => $changedItem->getQty(),
                'sku' => $changedItem->getSku(),
            );

            if(Mage::registry('quote_order_item_changed_after_edit')) {
                $this->getLogModel()->addItemChange($changedItem->getId(), $itemChange);
            }
        }
    }

    /**
     * Save changed order products
     *
     * @return $this
     */
    protected function saveOldOrderItems()
    {
        $quote = $this->getQuote();
        $order = $this->getOrder();
        $log = $this->getLogModel();
        $helper = Mage::helper('mageworx_ordersedit/edit');
        $quoteItemsChanges = $this->getChanges('quote_items');

        if (empty($quoteItemsChanges)) {
            return $this;
        }

        foreach ($quoteItemsChanges as $itemId => $params) {

            /**
             * @var Mage_Sales_Model_Quote_Item $quoteItem
             * @var Mage_Sales_Model_Order_Item $orderItem
             * @var MageWorx_OrdersEdit_Model_Edit_Quote_Convert $converter
             * @var MageWorx_OrdersEdit_Model_Edit_Log $log
             * @var MageWorx_OrdersEdit_Helper_Edit $helper
             * @var Mage_Sales_Model_Quote_Item $childQuoteItem
             * @var Mage_Sales_Model_Order_Item $childOrderItem
             */
            $quoteItem  = $quote->getItemById($itemId);
            $orderItem  = $order->getItemByQuoteItemId($itemId);
            $converter  = $this->getConverter();

            if (!$orderItem || !$helper->checkOrderItemForCancelRefund($orderItem)) {
                continue;
            }

            $orderItemQty = $this->getQtyRest($orderItem);

            if (isset($params['qty']) && $params['qty'] < 0.001) {
                $params['action'] = 'remove';
            }

            if ((isset($params['action']) && $params['action'] == 'remove')) {
                $this->removeOrderItem($order, $orderItem);

                $price = $this->getItemPricesConsideringTax($orderItem);
                $itemChange = array(
                    'name'         => $orderItem->getName(),
                    'qty_before'   => $orderItemQty,
                    'qty_after'    => '',
                    'price_before' => $price['before'],
                    'price_after'  => $price['after'],
                    'sku' => $orderItem->getSku(),
                );
                $log->addItemChange($orderItem->getId(), $itemChange);

                continue;
            }

            if (isset($params['qty']) && $params['qty'] != $orderItemQty) {

                $origQtyOrdered = $orderItem->getQtyOrdered();
                $orderItem = $converter->itemToOrderItem($quoteItem, $orderItem);

                /* Change main item qty */
                $this->changeItemQty($orderItem, $params['qty'], $origQtyOrdered);

                /* Change qty of child products if exists */
                if ($orderItem->getProductType() == 'bundle' || $orderItem->getProductType() == 'configurable') {
                    foreach ($quote->getAllItems() as $childQuoteItem) {
                        if ($childQuoteItem->getParentItemId() == $quoteItem->getId()) {

                            /* Recalculate totals of new order item */
                            $childQuoteItem->save();
                            $childOrderItem = $order->getItemByQuoteItemId($childQuoteItem->getId());
                            $origChildQtyOrdered = $childOrderItem->getQtyOrdered();
                            $childOrderItem = $converter->itemToOrderItem($childQuoteItem, $childOrderItem);

                            /* Change child item qty and save */
                            $this->changeItemQty($childOrderItem, $params['qty'] * $childQuoteItem->getQty(), $origChildQtyOrdered);
                            $childOrderItem->save();
                            $this->_savedOrderItems[] = $childOrderItem->getItemId();

                        }
                    }
                }
            } elseif ($quoteItem->getPrice() != $orderItem->getPrice()) {
                $orderItem = $converter->itemToOrderItem($quoteItem, $orderItem);
            }

            if (isset($params['instruction'])) {
                $orderItem->setData('instruction', trim($params['instruction']));
            }

            $price = $this->getItemPricesConsideringTax($orderItem);
            $itemChange = array(
                'name'         => $orderItem->getName(),
                'qty_before'   => $orderItemQty,
                'qty_after'    => $quoteItem->getQty(),
                'price_before' => $price['before'],
                'price_after'  => $price['after'],
                'sku'    => $quoteItem->getSku(),
            );

            /* Check Discount changes */
            if (isset($params['use_discount'])
                && $params['use_discount'] == 1
                && $quoteItem->getOrigData('discount_amount') == 0
                && $quoteItem->getData('discount_amount') > 0
            ) {
                $itemChange['discount'] = 1;
            } elseif ($quoteItem->getData('discount_amount') < 0.001 && $quoteItem->getOrigData('discount_amount') > 0) {
                $itemChange['discount'] = -1;
            }

            /* Add item changes to log */
            if ($itemChange['qty_before'] != $itemChange['qty_after']
                || $itemChange['price_before'] != $itemChange['price_after']
                || isset($itemChange['discount'])
            ) {
                $log->addItemChange($orderItem->getId(), $itemChange);
            }

            if(!empty($params['use_discount']) && $quoteItem->getDiscountAmount() != $orderItem->getDiscountAmount()) {
                $orderItem->setDiscountAmount($quoteItem->getDiscountAmount())
                    ->setBaseDiscountAmount($quoteItem->getBaseDiscountAmount());
            }

            $quoteItem->save();
            $orderItem->save();

            $this->_savedOrderItems[] = $orderItem->getItemId();
        }

        return $this;
    }

    /**
     * Add new products to order
     *
     * @return $this
     */
    protected function saveNewOrderItems()
    {
        if (empty($this->changes['product_to_add'])) {
            return $this;
        }

        $quote = $this->getQuote();
        $order = $this->getOrder();

        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
        foreach ($quote->getAllItems() as $quoteItem) {
            /** @var Mage_Sales_Model_Order_Item $orderItem */
            $orderItem = $order->getItemByQuoteItemId($quoteItem->getItemId());
            if ($orderItem && $orderItem->getItemId()) {
                continue;
            }

            $quoteItem->save();

            $orderItem = $this->getConverter()->itemToOrderItem($quoteItem, $orderItem);
            $order->addItem($orderItem);
            if ($orderItem->save()) {
                $this->removeQtyFromStock($orderItem->getProductId(), $orderItem->getQtyOrdered());
            }

            /*** Add new items to log ***/
            $changedItem = $quoteItem;
            $itemChange = array(
                'name'       => $changedItem->getName(),
                'qty_before' => 0,
                'qty_after'  => $changedItem->getQty(),
                'sku'        => $changedItem->getSku(),
            );
            $this->getLogModel()->addItemChange($changedItem->getId(), $itemChange);

            $this->_savedOrderItems[] = $orderItem->getItemId();
        }

        /** @var Mage_Sales_Model_Quote_Item $childQuoteItem */
        foreach ($quote->getAllItems() as $childQuoteItem) {
            /** @var Mage_Sales_Model_Order_Item $childOrderItem */
            $childOrderItem = $order->getItemByQuoteItemId($childQuoteItem->getItemId());

            /*** Add items relations for configurable and bundle products ***/
            if ($childQuoteItem->getParentItemId()) {
                /** @var Mage_Sales_Model_Order_Item $parentOrderItem */
                $parentOrderItem = $order->getItemByQuoteItemId($childQuoteItem->getParentItemId());

                $childOrderItem->setParentItemId($parentOrderItem->getItemId());
                $childOrderItem->save();
            }
        }

        return $this;
    }
}
