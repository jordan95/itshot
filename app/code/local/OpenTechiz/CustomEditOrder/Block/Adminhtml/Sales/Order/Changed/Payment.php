<?php
/**
 * MageWorx
 * Admin Order Editor extension
 *
 * @category   MageWorx
 * @package    MageWorx_OrdersEdit
 * @copyright  Copyright (c) 2016 MageWorx (http://www.mageworx.com/)
 */

class OpenTechiz_CustomEditOrder_Block_Adminhtml_Sales_Order_Changed_Payment extends MageWorx_OrdersEdit_Block_Adminhtml_Sales_Order_Changed_Payment
{
    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
    	$payment = $this->getQuote()->getPayment();
    	$payment->setparent_id($this->getOrder()->getId());
        $this->setPayment($payment);
        return $this;
    }
    public function setPayment($payment)
    {
        $paymentInfoBlock = Mage::helper('payment')->getInfoBlock($payment);
        $this->setChild('info', $paymentInfoBlock);
        $this->setData('payment', $payment);
        return $this;
    }
}