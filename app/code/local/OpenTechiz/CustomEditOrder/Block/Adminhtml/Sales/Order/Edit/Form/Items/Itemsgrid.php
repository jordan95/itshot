<?php

class OpenTechiz_CustomEditOrder_Block_Adminhtml_Sales_Order_Edit_Form_Items_Itemsgrid extends MageWorx_OrdersEdit_Block_Adminhtml_Sales_Order_Edit_Form_Items_Itemsgrid
{
    public function getStore()
    {
        $order = $this->getOrder() ? $this->getOrder() : Mage::registry('ordersedit_order');
        $current_currency = Mage::getModel('directory/currency')->load($order->getOrderCurrencyCode());
        $storeId = $order->getStoreId();
        $store = Mage::getModel('core/store')->load($storeId);
        $store->setData("current_currency",$current_currency);
        return $store;
    }

    public function isOrderRepair(){
        $order = $this->getOrder() ? $this->getOrder() : Mage::registry('ordersedit_order');
        return ($order->getOrderType() == 2);
    }

    public function isReturnFee($item){
        return ($item->getSku() == OpenTechiz_Return_Helper_Data::RETURN_FEE_ITEM_SKU);
    }
}
