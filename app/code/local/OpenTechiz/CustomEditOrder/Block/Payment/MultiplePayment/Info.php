<?php

class OpenTechiz_CustomEditOrder_Block_Payment_MultiplePayment_Info extends MDN_PaymentTracker_Block_Payment_MultiplePayment_Info{

   
    public function getPayments()
    {
        $payments = array();
        $total_paid = 0;
        if($multiplePayment = $this->getInfo()->getData('MultiplePayment')){
            $methods = $multiplePayment['method'];
            $amounts = $multiplePayment['amount'];
            foreach ($methods as $key => $value) {
                # code...
                if($value){
                 $payments[$key]['ptp_method'] = $value;
                 $payments[$key]['ptp_amount'] = $amounts[$key];
                 $total_paid += $amounts[$key];
                 $this->setTotalPaid($total_paid);
                }
            }
        
            return $payments;
        }
        if ($this->getOrder()) {
            
            $collection = Mage::getModel('PaymentTracker/Payment')
                ->getCollection()
                ->addFieldToFilter('ptp_order_id', $this->getOrder()->getId());
            return $collection;
        }
        else
            return array();
    }

    

    public function getTotalDue()
    {
        return  $this->getOrder()->getTotalDue();
    }

}