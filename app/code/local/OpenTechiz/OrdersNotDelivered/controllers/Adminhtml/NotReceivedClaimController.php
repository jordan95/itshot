<?php

class OpenTechiz_OrdersNotDelivered_Adminhtml_NotReceivedClaimController extends Mage_Adminhtml_Controller_Action
{

    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            default:
                $aclResource = 'admin/sales/ordersnotdelivered';
                break;
        }
        return Mage::getSingleton('admin/session')->isAllowed($aclResource);
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales')
            ->_title($this->__('Orders Not Delivered'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function loadFormAction()
    {
        $this->loadLayout();
        $form = $this->getLayout()->getBlock('ordersnotdelivered.form');
        $shipmentId = $this->getRequest()->getParam('shipment_id');
        $shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);
        if (!$shipment->getId()) {
            Mage::throwException("Shipment is not exist");
        }
        $form->setShipment($shipment);
        $this->renderLayout();
    }

    public function saveAction()
    {
        $shipmentId = $this->getRequest()->getParam('shipment_id');
        $shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);
        if (!$shipment->getId()) {
            Mage::throwException("Shipment is not exist");
        }
        /* @var $order Mage_Sales_Model_Order */
        $order = $shipment->getOrder();
        if (!$this->getRequest()->isPost()) {
            return $this->getResponse()->setBody("Invalid request");
        }
        $data = $this->getRequest()->getPost();
        $data['order_id'] = $order->getId();
        $data['shipment_id'] = $shipment->getId();
        
        $track = Mage::getModel('sales/order_shipment_track')->load($data['track_id']);
        if(!$track->getId()){
            return $this->getResponse()->setBody('Shipping and Tracking Information is invalid');
        }
        $order->addStatusHistoryComment(sprintf("Not Delivered. %s", $data['notes']));
        $transaction = Mage::getModel('core/resource_transaction');
        $ordersnotdelivered = Mage::getModel('ordersnotdelivered/ordersnotdelivered')->load($data['track_id'], 'track_id');
        if ($ordersnotdelivered->getId()) {
            return $this->getResponse()->setBody('Shipping and Tracking Information is reported');
        }
        $ordersnotdelivered->addData($data);
        $helper = Mage::helper('ordersnotdelivered');
        $claim_paid = '$' . $data['claim_paid'];
        $claim_status = null;
        if (array_key_exists($data['claim_status'], $helper->getClaimStatus())) {
            $claim_status = $helper->getClaimStatus($data['claim_status']);
        }
        try {
            $transaction->addObject($ordersnotdelivered)
                    ->addObject($order);
            $transaction->save();
            $subject = str_replace("{{order_increment}}", $order->getIncrementId(), $helper->getSubjectTemplate());
            $message =<<<EOD
                    <p><b>Order # </b>{$order->getIncrementId()}</p>
                    <p><b>Received Notification: </b>{$data['received_notification']}</p>
                    <p><b>Shipping Carrier:</b>{$track->getTitle()} - {$track->getTrackNumber()}</p>
                    <p><b>Claim Filed: </b>{$data['claim_filed']}</p>
                    <p><b>Claim Status: </b>{$claim_status}</p>
                    <p><b>Claim Paid: </b>{$claim_paid}</p>
                    <p><b>Notes: </b>{$data['notes']}</p>
EOD;
            $helper->sendEmailOrdersNotDelivered($subject, $message);
            return $this->getResponse()->setBody("Thank you! We have received your request.");
        } catch (Exception $exc) {
            return $this->getResponse()->setBody($exc->getMessage());
        }
    }

    public function massUpdateStatusAction(){
        $itemIds = $this->getRequest()->getPost('item_ids');
        $status = $this->getRequest()->getPost('claim_status');
        $updatedItems = array();
        if(!empty($itemIds)) {
            $collection = Mage::getModel('ordersnotdelivered/ordersnotdelivered')->getCollection()
                ->addFieldToFilter('id', array('in' => $itemIds));
            foreach ($collection as $ordersnotdelivered) {
                if ($ordersnotdelivered->getData('claim_status') != $status) {
                    try {
                        $ordersnotdelivered->setData('claim_status',$status);
                        $ordersnotdelivered->save();
                        $updatedItems[] = $ordersnotdelivered->getId();    
                    } catch (Exception $e) {
                        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                    }
                }
            }

            $message = Mage::helper('adminhtml')->__('No items changed');
            if(!empty($updatedItems))
                $message = Mage::helper('adminhtml')->__('Items %s was change', implode(',',$updatedItems));

            Mage::getSingleton('adminhtml/session')->addSuccess($message);
            $this->_redirect('*/*/');
            return $this;
        }
    }

    protected function _initOrdersnotdelivered($idFieldName = 'id')
    {
        $id = (int)$this->getRequest()->getParam($idFieldName);
        $ordersnotdelivered = Mage::getModel('ordersnotdelivered/ordersnotdelivered');
        $ordersnotdelivered->load($id);
        if ($ordersnotdelivered->getId()) {
            Mage::register('ordersnotdelivered', $ordersnotdelivered);
            return $ordersnotdelivered;
        }else{
            return false;
        }
    }

    public function editAction(){
        if ($ordersnotdelivered = $this->_initOrdersnotdelivered()) {
            $this->_initAction();
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);{}
            $this->_addContent($this->getLayout()->createBlock('ordersnotdelivered/adminhtml_sales_claim_edit'))
                ->_addLeft($this->getLayout()->createBlock('ordersnotdelivered/adminhtml_sales_claim_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_purchase')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function updateAction(){
        if ($data = $this->getRequest()->getPost()) {
            try {
                $id = (int)$this->getRequest()->getParam('id');
                $ordersnotdelivered = Mage::getModel('ordersnotdelivered/ordersnotdelivered')->load($id);
                if ($ordersnotdelivered->getId()) {
                    // if ($data['track_id']) {
                    //     $ordersnotdelivered->setData('track_id',$data['track_id']);
                    // }

                    if ($data['track_number']) {
                        $trackingCollection = Mage::getModel('sales/order_shipment_track')->getCollection()->addFieldToFilter('track_number', $data['track_number']);
                        $track = $trackingCollection->getFirstItem();
                        $ordersnotdelivered->setData('track_id',$track->getEntityId());
                    }

                    if ($data['received_notification']) {
                        $ordersnotdelivered->setData('received_notification',$data['received_notification']);
                    }
                    if ($data['claim_filed']) {
                        $ordersnotdelivered->setData('claim_filed',$data['claim_filed']);
                    }
                    if ($data['claim_number']) {
                        $ordersnotdelivered->setData('claim_number',$data['claim_number']);
                    }
                    if ($data['claim_status']) {
                        $ordersnotdelivered->setData('claim_status',$data['claim_status']);
                    }
                    if ($data['claim_paid']) {
                        $ordersnotdelivered->setData('claim_paid',$data['claim_paid']);
                    }
                    if ($data['notes']) {
                        $ordersnotdelivered->setData('notes',$data['notes']);
                    }
                    $ordersnotdelivered->save();
                }
                
                Mage::getSingleton('adminhtml/session')->addSuccess('Item save success.');
            } catch (Exception $e) {
               Mage::getSingleton('adminhtml/session')->addError($e->getMessage()); 
            }
        }else{
            Mage::getSingleton('adminhtml/session')->addError('Not have data to save.'); 
        }
        $this->_redirect('*/*/edit',array('id'=>$ordersnotdelivered->getId()));
    }

}
