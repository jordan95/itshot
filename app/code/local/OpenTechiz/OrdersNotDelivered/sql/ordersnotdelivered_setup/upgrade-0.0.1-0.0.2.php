<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    ALTER TABLE `tsht_orders_not_delivered` CHANGE `claim_status` `claim_status` INT(11) NOT NULL DEFAULT '0' COMMENT 'Claim Status';
    UPDATE tsht_orders_not_delivered SET claim_status = 1 WHERE tsht_orders_not_delivered.claim_status = 'Reviewed';
	UPDATE tsht_orders_not_delivered SET claim_status = 2 WHERE tsht_orders_not_delivered.claim_status = 'Approved';
	UPDATE tsht_orders_not_delivered SET claim_status = 3 WHERE tsht_orders_not_delivered.claim_status = 'Denied';
");
$installer->endSetup();
