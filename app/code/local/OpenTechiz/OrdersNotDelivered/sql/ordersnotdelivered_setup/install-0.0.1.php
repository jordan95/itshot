<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('ordersnotdelivered/ordersnotdelivered');
$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
if (!$connection->isTableExists($tableName)) {
    $table = $connection->newTable($tableName)
            ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
                'identity' => true,
                'primary' => true,
                'unsigned' => true,
                'nullable' => false,
                    ], 'Id')
            ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
                'nullable' => false,
                    ], 'Order ID')
            ->addColumn('shipment_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
                'nullable' => false,
                    ], 'Shipment ID')
            ->addColumn('track_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
                'nullable' => true,
                    ], 'Track ID')
            ->addColumn('received_notification', Varien_Db_Ddl_Table::TYPE_DATETIME, null, [
                'nullable' => true,
                    ], 'Received Notification')
            ->addColumn('claim_filed', Varien_Db_Ddl_Table::TYPE_DATETIME, null, [
                'nullable' => true,
                    ], 'Claim Filed')
            ->addColumn('claim_number', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, [
                'nullable' => true,
                    ], 'Claim Number')
            ->addColumn('claim_status', Varien_Db_Ddl_Table::TYPE_VARCHAR, 25, [
                'nullable' => true,
                    ], 'Claim Status')
            ->addColumn('claim_paid', Varien_Db_Ddl_Table::TYPE_DECIMAL, null, [
                'nullable' => true,
                    ], 'Claim Paid')
            ->addColumn('notes', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
                'nullable' => true,
                    ], 'Notes')
            ->addIndex($installer->getIdxName('ordersnotdelivered/ordersnotdelivered', array('order_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX), array('order_id'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX))
            ->addIndex($installer->getIdxName('ordersnotdelivered/ordersnotdelivered', array('shipment_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX), array('shipment_id'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX))
            ->addIndex($installer->getIdxName('ordersnotdelivered/ordersnotdelivered', array('track_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX), array('track_id'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX))
            ->setComment('Orders Not Delivered')
            ->setOption('type', 'MyISAM')
            ->setOption('charset', 'utf8');

    $connection->createTable($table);
}

$installer->endSetup();
