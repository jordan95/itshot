<?php

class OpenTechiz_OrdersNotDelivered_Helper_Data extends Mage_Core_Helper_Abstract
{
    
    const XML_PATH_ORDERSNOTDELIVERED_RECIPIENT_EMAIL = 'ordersnotdelivered/general/recipient_email';
    const XML_PATH_ORDERSNOTDELIVERED_SUBJECT_TEMPLATE = 'ordersnotdelivered/general/subject_template';
    const XML_PATH_ORDERSNOTDELIVERED_RECIPIENT_NAME = 'ordersnotdelivered/general/recipient_name';
    const XML_PATH_ORDERSNOTDELIVERED_ENABLE = 'ordersnotdelivered/general/enable';

    const CLAIM_STATUS_REVIEWED = 1;
    const CLAIM_STATUS_APPROVED = 2;
    const CLAIM_STATUS_DENIED = 3;

    public function getClaimStatus($status = ''){
        $listStatus = array(
            self::CLAIM_STATUS_REVIEWED => $this->__('Reviewed'),
            self::CLAIM_STATUS_APPROVED => $this->__('Approved'),
            self::CLAIM_STATUS_DENIED => $this->__('Denied')
        );
        
        if($status != null && isset($listStatus[$status])){
            return $listStatus[$status];
        }
        return $listStatus;
    }

    public function isEnable()
    {
        return (bool) Mage::getStoreConfig(self::XML_PATH_ORDERSNOTDELIVERED_ENABLE);
    }

    public function getRecipientName()
    {
        return Mage::getStoreConfig(self::XML_PATH_ORDERSNOTDELIVERED_RECIPIENT_NAME);
    }
    
    public function getSubjectTemplate()
    {
        return Mage::getStoreConfig(self::XML_PATH_ORDERSNOTDELIVERED_SUBJECT_TEMPLATE);
    }

    public function getRecipientEmails()
    {
        $recipient_email = Mage::getStoreConfig(self::XML_PATH_ORDERSNOTDELIVERED_RECIPIENT_EMAIL);
        $emails = preg_split("/[\s,;]+/", $recipient_email);
        return $emails;
    }

    public function sendEmailOrdersNotDelivered($subject, $message)
    {
        if (!$this->isEnable()) {
            return;
        }
        $emails = $this->getRecipientEmails();
        $toEmail = current($emails);
        $ccEmails = array();
        foreach ($emails as $e) {
            if ($e != $toEmail) {
                $ccEmails[] = $e;
            }
        }
        $mail = Mage::getModel('core/email');
        $mail->setToName($this->getRecipientName());
        $mail->setToEmail($toEmail);
        $mail->setFromEmail("sales@itshot.com");
        $mail->setFromName("ItsHot.com");
        $mail->setType('html');
        $mail->setSubject($subject);
        $mail->setBody($message);
        try {
            $mail->send();
        } catch (Exception $e) {
            
        }
    }

}
