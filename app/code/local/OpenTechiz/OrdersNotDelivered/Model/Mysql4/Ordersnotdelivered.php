<?php

class OpenTechiz_OrdersNotDelivered_Model_Mysql4_Ordersnotdelivered extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('ordersnotdelivered/ordersnotdelivered', 'id');
    }

}
