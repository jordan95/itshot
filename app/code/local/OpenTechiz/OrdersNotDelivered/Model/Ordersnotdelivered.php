<?php

class OpenTechiz_OrdersNotDelivered_Model_Ordersnotdelivered extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('ordersnotdelivered/ordersnotdelivered');
    }

}
