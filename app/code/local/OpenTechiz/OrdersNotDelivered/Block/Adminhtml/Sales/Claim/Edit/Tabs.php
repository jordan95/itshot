<?php

class OpenTechiz_OrdersNotDelivered_Block_Adminhtml_Sales_Claim_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('ordersnotdelivered_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ordersnotdelivered')->__('Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('general', array(
            'label'     => Mage::helper('ordersnotdelivered')->__('General'),
            'title'     => Mage::helper('ordersnotdelivered')->__('General'),
            'content'   => $this->getLayout()->createBlock('ordersnotdelivered/adminhtml_sales_claim_edit_tab_form')->toHtml()
        ));
        return parent::_beforeToHtml();
    }
}