<?php

class OpenTechiz_OrdersNotDelivered_Block_Adminhtml_Sales_Claim_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $ordersnotdelivered = Mage::registry('ordersnotdelivered');
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('ordersnotdelivered_form', array('legend'=>Mage::helper('ordersnotdelivered')->__('General')));
        $fieldset->addField('received_notification', 'datetime', array(
            'label'     => Mage::helper('ordersnotdelivered')->__('Received Notification'),
            'name'      => 'received_notification',
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => "MM/dd/yyyy",
            'format'       => "MM/dd/yyyy",
            'time' => true,
            "value" => "textstart",
        ));

        $fieldset->addField('track_number', 'text', array(
            'label'     => Mage::helper('ordersnotdelivered')->__('Shipping and Tracking Info'),
            'name'      => 'track_number',
        ));

        $fieldset->addField('claim_filed', 'datetime', array(
            'label'     => Mage::helper('ordersnotdelivered')->__('Claim Filed'),
            'name'      => 'claim_filed',
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => "MM/dd/yyyy",
            'format'       => "MM/dd/yyyy",
            'time' => true,
            "value" => "textstart",
        ));

        $fieldset->addField('claim_number', 'text', array(
            'label'     => Mage::helper('ordersnotdelivered')->__('Claim #'),
            'name'      => 'claim_number',
        ));

        $fieldset->addField('claim_status', 'select', array(
            'label'     => Mage::helper('ordersnotdelivered')->__('Claim Status'),
            'name'      => 'claim_status',
            'options'   => array(
                0 => '',
                OpenTechiz_OrdersNotDelivered_Helper_Data::CLAIM_STATUS_REVIEWED => Mage::helper('ordersnotdelivered')->__('Reviewed'),
                OpenTechiz_OrdersNotDelivered_Helper_Data::CLAIM_STATUS_APPROVED => Mage::helper('ordersnotdelivered')->__('Approved'),
                OpenTechiz_OrdersNotDelivered_Helper_Data::CLAIM_STATUS_DENIED => Mage::helper('ordersnotdelivered')->__('Denied'),
            ),
        ));

        $fieldset->addField('claim_paid', 'text', array(
            'label'     => Mage::helper('ordersnotdelivered')->__('Claim Paid'),
            'name'      => 'claim_paid',
        ));

        $fieldset->addField('notes', 'textarea', array(
            'label'     => Mage::helper('ordersnotdelivered')->__('Notes'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'notes',
        ));

        $track = Mage::getModel('sales/order_shipment_track')->load($ordersnotdelivered->getTrackId());
        if ($track->getNumber()) {
            $ordersnotdelivered->setTrackNumber($track->getNumber());
        }

        $form->setValues($ordersnotdelivered->getData());
    
        return parent::_prepareForm();
    }
}