<?php

if (class_exists("OpenTechiz_Custom_Block_Adminhtml_Sales_Order_Shipment_View") && Mage::helper('core')->isModuleEnabled('OpenTechiz_Custom')) {

    class OpenTechiz_OrdersNotDelivered_Block_Adminhtml_Sales_Order_Shipment_View_Pure extends OpenTechiz_Custom_Block_Adminhtml_Sales_Order_Shipment_View
    {
        
    }

} else {

    class OpenTechiz_OrdersNotDelivered_Block_Adminhtml_Sales_Order_Shipment_View_Pure extends Mage_Adminhtml_Block_Sales_Order_Shipment_View
    {
        
    }

}

class OpenTechiz_OrdersNotDelivered_Block_Adminhtml_Sales_Order_Shipment_View extends OpenTechiz_OrdersNotDelivered_Block_Adminhtml_Sales_Order_Shipment_View_Pure
{

    public function __construct()
    {
        parent::__construct();
        $shipment = $this->getShipment();
        $_tracks = $shipment->getAllTracks();
        $ordersnotdelivered = Mage::getModel('ordersnotdelivered/ordersnotdelivered')->load($shipment->getId(), 'shipment_id');
        if (!$ordersnotdelivered->getId() && count($_tracks) > 0 && Mage::getSingleton('admin/session')->isAllowed('admin/sales/ordersnotdelivered')) {
            $this->_addButton('not_received_claim', array(
                'label' => Mage::helper('sales')->__('Not Received Claim'),
                'onclick' => 'javascript:openNotReceivedClaimPopup();'
                    )
            );
        }
    }

}
