<?php

class OpenTechiz_OrdersNotDelivered_Block_Adminhtml_Sales_Claim_Renderer_Claim extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $html = '';
        if ($row->getData('claim_paid')) {
            $html = Mage::helper('core')->currency($row->getData('claim_paid'), true, false);
        }
        return $html;
	}
}