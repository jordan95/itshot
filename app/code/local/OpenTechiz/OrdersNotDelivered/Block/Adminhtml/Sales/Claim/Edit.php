<?php

class OpenTechiz_OrdersNotDelivered_Block_Adminhtml_Sales_Claim_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        // $this->_controller = 'ordersnotdelivered';
        $this->_blockGroup = 'ordersnotdelivered';
        $this->_controller = 'adminhtml_sales_claim';
        parent::__construct();
        $this->_removeButton('delete');
        $id = (int)$this->getRequest()->getParam('id');
        $ordersnotdelivered = Mage::getModel('ordersnotdelivered/ordersnotdelivered')->load($id);
        if ($ordersnotdelivered->getClaimStatus() == OpenTechiz_OrdersNotDelivered_Helper_Data::CLAIM_STATUS_REVIEWED) {
            $this->_removeButton('save');
            $this->_removeButton('reset');
        }
    }

    public function getHeaderText()
    {
        return Mage::helper('ordersnotdelivered')->__('Edit Orders Not Delivered');
    }
}