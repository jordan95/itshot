<?php

class OpenTechiz_OrdersNotDelivered_Block_Adminhtml_Sales_Claim_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('orders_not_delivered_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('ordersnotdelivered/ordersnotdelivered')->getCollection();
        $collection->getSelect()->joinLeft(array('order' => 'tsht_sales_flat_order'), 'main_table.order_id = order.entity_id', array('increment_id'));
        $collection->getSelect()->joinLeft(array('shipment' => 'tsht_sales_flat_shipment'), 'order.entity_id = shipment.order_id', array('shipment_increment_id'=>'shipment.increment_id'));
        $collection->getSelect()->joinLeft(array('shipment_track' => 'tsht_sales_flat_shipment_track'), 'shipment.entity_id = shipment_track.parent_id', array('track_number'));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('ordersnotdelivered')->__('ID'),
            'index'     => 'id',
            'width'     => '50',
        ));

        $this->addColumn('increment_id', array(
            'header'    => Mage::helper('ordersnotdelivered')->__('Order #'),
            'index'     => 'increment_id',
            'filter_index'  => 'order.increment_id',
            'type'      => 'text',
            'renderer'  => 'OpenTechiz_SalesExtend_Block_Adminhtml_Renderer_OrderNumber'
        ));

        $this->addColumn('shipment_increment_id', array(
            'header'    => Mage::helper('ordersnotdelivered')->__('Shipment #'),
            'index'     => 'shipment_increment_id',
            'filter_index'  => 'shipment.increment_id',
        ));

        $this->addColumn('track_number', array(
            'header'    => Mage::helper('ordersnotdelivered')->__('Track Number'),
            'index'     => 'track_number',
        ));

        $this->addColumn('received_notification', array(
            'header'    => Mage::helper('ordersnotdelivered')->__('Received Notification'),
            'type'      => 'date',
            'index'     => 'received_notification',
        ));

        $this->addColumn('claim_filed', array(
            'header'    => Mage::helper('ordersnotdelivered')->__('Claim Filed'),
            'index'     => 'claim_filed',
            'type'      => 'date',
        ));

        $this->addColumn('claim_status', array(
            'header'    => Mage::helper('ordersnotdelivered')->__('Claim Status'),
            'index'     => 'claim_status',
            'type'      => 'options',
            'options'   => Mage::helper('ordersnotdelivered')->getClaimStatus(),
        ));

        $this->addColumn('claim_paid', array(
            'header'    => Mage::helper('ordersnotdelivered')->__('Claim Paid'),
            'index'     => 'claim_paid',
            'type'      => 'number',
            'renderer'  => 'OpenTechiz_OrdersNotDelivered_Block_Adminhtml_Sales_Claim_Renderer_Claim'
        ));

        $this->addColumn('notes', array(
            'header'    => Mage::helper('ordersnotdelivered')->__('Notes'),
            'index'     => 'notes',
            'filter'    => false,
            'sortable'  => false,
        ));

        $this->addColumn('action',array(
            'header' => Mage::helper('opentechiz_production')->__('Action'),
            'width' => '50',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('opentechiz_production')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id',
                    'data-column' => 'action',
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
            'align' => 'center'
        ));
        
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        parent::_prepareMassaction();
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('item_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('update_status', array(
            'label'=> Mage::helper('ordersnotdelivered')->__('Update Status'),
            'url'  => $this->getUrl('*/*/massUpdateStatus', array('massupdate'=>1,'permission_print'=>1)),
            'additional'    => array(
                'claim_status' => array(
                    'name'  => 'claim_status',
                    'type'  => 'select',  
                    'label' => Mage::helper('ordersnotdelivered')->__('Values'),
                    'values' => Mage::helper('ordersnotdelivered')->getClaimStatus()
                ),
            )
        ));

        return $this;
    }

    public function getRowUrl($row)
    {
        // if (Mage::getSingleton('admin/session')->isAllowed('admin/erp/purchase/supplier/suppliers/supplier_edit')) {
            return $this->getUrl('*/*/edit', array('id' => $row->getId()));
        // }
    }
}