<?php

class OpenTechiz_OrdersNotDelivered_Block_Adminhtml_Sales_Claim_Container extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_sales_claim';
        $this->_blockGroup = 'ordersnotdelivered';
        $this->_headerText = Mage::helper('ordersnotdelivered')->__('Orders Not Delivered');
        parent::__construct();
        $this->_removeButton('add');
    }
}