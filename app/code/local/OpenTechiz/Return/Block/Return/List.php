<?php




class OpenTechiz_Return_Block_Return_List extends Mage_Core_Block_Template
{
    /**
     * @return void
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle(Mage::helper('opentechiz_return')->__('My Returns'));
        }
        $pager = $this->getLayout()->createBlock('page/html_pager', 'sales.return.history.pager')
            ->setCollection($this->getRmaCollection());
        $this->setChild('pager', $pager);
        $this->getRmaCollection()->load();
        return $this;
    }
    protected function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    public function getOrderUrl($orderId)
    {
        return Mage::getUrl('sales/order/view', array('order_id' => $orderId));
    }
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    public function getRmaCollection()
    {
        if (!$this->_collection) {
            $page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
            $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 1;
    //get values of current limit
            $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 1;
            $this->_collection = Mage::getModel('opentechiz_return/return')->getCollection()
                ->addFieldToFilter('main_table.customer_id', $this->getCustomer()->getId())
                ->setOrder('created_at', 'desc')->setPageSize($pageSize)->setCurPage($page);
            
        }

        return $this->_collection;
    }
  
}
