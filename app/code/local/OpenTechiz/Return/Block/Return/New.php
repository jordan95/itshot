<?php




class OpenTechiz_Return_Block_Return_New extends Mage_Core_Block_Template
{
    /**
     * @return void
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle(Mage::helper('opentechiz_return')->__('New Return'));
        }
    }

    protected function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    public function getOrderItem()
    {
        $item_id = Mage::registry('order_item_id');
        
         $item = Mage::getResourceModel('sales/order_item_collection')
            ->join('order', 'order.entity_id=main_table.order_id') ->addFieldToFilter('main_table.item_id',array('in'=>$item_id));
        return $item;
    }
    public function getOption($option){
        return Mage::helper('opentechiz_return')->getOption($option);
    }
    public function getCountry(){
        return $countryList = Mage::getModel('directory/country')->getResourceCollection()->loadByStore()->toOptionArray(true);
    }
  
}
