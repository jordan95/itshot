<?php
class OpenTechiz_Return_Block_Return_View extends Mage_Core_Block_Template
{
   
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $rma = $this->getRma();
        $headBlock = $this->getLayout()->getBlock('head');
        if ($rma && $headBlock) {
            $headBlock->setTitle(Mage::helper('opentechiz_return')->__('Return #%s', $rma->getIncrementId()));
        }
    }

  

    /**
     * @return Mirasvit_Rma_Model_Rma
     */
    public function getRma()
    {
        return Mage::registry('current_rma');
    }

     protected function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    public function getOrderItem()
    {
        $item_id = $this->getRma()->getOrderItemId();
         $item = Mage::getResourceModel('sales/order_item_collection')
            ->join('order', 'order.entity_id=main_table.order_id') ->addFieldToFilter('main_table.item_id', $item_id)->getFirstItem();
        return $item;
    }
    public function getOption($option){
        return Mage::helper('opentechiz_return')->getOption($option);
    }
}
