<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_rma
 * @version   2.4.16
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */



class OpenTechiz_Return_Block_Return_New_Stepone extends Mage_Core_Block_Template
{

    protected function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    public function getAllowedOrderItems()
    {
         $collection = Mage::getResourceModel('sales/order_item_collection')
            ->join('order', 'order.entity_id=main_table.order_id') ->addFieldToFilter('customer_id', $this->getCustomer()->getId())->addAttributeToFilter('status', array('in' => array('complete')));;
            return $collection;
    }

    public function getOption($option){
        return Mage::helper('opentechiz_return')->getOption($option);
    }

    
}
