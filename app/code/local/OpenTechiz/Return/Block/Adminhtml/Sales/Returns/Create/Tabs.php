<?php

class OpenTechiz_Return_Block_Adminhtml_Sales_Returns_Create_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('return_create_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('opentechiz_return')->__('Detail Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('opentechiz_return')->__('Return Information'),
            'title'     => Mage::helper('opentechiz_return')->__('Return Information'),
            'content'   => $this->getLayout()->createBlock('opentechiz_return/adminhtml_sales_returns_create_tab_returninfo')->toHtml(),
            
        ));
       
        return parent::_beforeToHtml();
    }


  
}