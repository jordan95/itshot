<?php

class OpenTechiz_Return_Block_Adminhtml_Sales_Returns extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_sales_returns';
        $this->_blockGroup = 'opentechiz_return';
        $this->_headerText = Mage::helper('opentechiz_return')->__('Returns');
         
        parent::__construct();
   
    }
}