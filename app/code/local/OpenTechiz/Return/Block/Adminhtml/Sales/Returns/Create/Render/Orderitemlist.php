<?php 
class OpenTechiz_Return_Block_Adminhtml_Sales_Returns_Create_Render_Orderitemlist extends Mage_Adminhtml_Block_Abstract implements Varien_Data_Form_Element_Renderer_Interface
{

    public function render(Varien_Data_Form_Element_Abstract $element) 
    {
        $html = $this->getLayout()->createBlock('core/template')->setTemplate('rma/orderitems.phtml')->setData('order_id',$element->getValue())->toHtml();
      
        return $html;
    }
}