<?php

class Opentechiz_Return_Block_Adminhtml_Sales_Return_Create_OrderItemList extends Mage_Core_Block_Template
{
    /**
     * @return void
     */
 

    public function getOrderItem()
    {
        $order_id = $this->getOrderId();
        
         $item = Mage::getResourceModel('sales/order_item_collection')
            ->join('order', 'order.entity_id=main_table.order_id') ->addFieldToFilter('order.entity_id',array('eq'=>$order_id));
        return $item;
    }
    
  
}
