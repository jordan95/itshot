<?php

class OpenTechiz_Return_Block_Adminhtml_Sales_Returns_Create_Tab_Returninfo extends Mage_Adminhtml_Block_Widget_Form
{


    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $order_id = $this->getRequest()->getParam('order_id');
        $params = $this->getRequest()->getParams();
        $type = 0;
        if(isset($params['type'])){
            if($params['type'] == 'repair'){
                $type = 10;
            }elseif($params['type'] == 'exchange'){
                $type = 20;
            }
        }
        $order = Mage::getModel('sales/order')->load($order_id);
        $shippingAddress = $order->getBillingAddress();
        $general = $form->addFieldset('general', array('legend' => Mage::helper('opentechiz_return')->__('General')));

        $general->addField('order_id', 'hidden', array(
            'label' => 'order id:',
            'readonly' => true,
            'name'     => 'order_id',
            'value' => $order_id
        ));

        $general->addField('firstname', 'text', array(
            'label' => 'First Name:',
            'name'     => 'first_name',
            'value' => $shippingAddress->getData('firstname')
        ));
        $general->addField('is_new', 'hidden', array(
            'name'     => 'is_new_rma',
            'value' => 1
        ));
        $general->addField('customer_id', 'hidden', array(
            'name'     => 'customer_id',
            'value' => $order->getCustomerId()
        ));
        $general->addField('type', 'hidden', array(
            'name'     => 'type',
            'value' => $type
        ));
        $general->addField('lastname', 'text', array(
            'label' => 'Last Name:',
            'name'     => 'last_name',
            'value' =>  $shippingAddress->getData('lastname')
        ));
        $general->addField('email', 'text', array(
            'label' => 'Customer Email:',
            'name'     => 'email',
            'value' => $shippingAddress->getData('email')
        ));

        $general->addField('phone', 'text', array(
            'label' => 'Customer Phone:',
            'name'     => 'phone',
            'value' => $shippingAddress->getData('telephone')
        ));
        $general->addField('street', 'text', array(
            'label' => 'Street:',
            'name'     => 'street',
            'value' => $shippingAddress->getData('street')
        ));
        $general->addField('city', 'text', array(
            'label' => 'City:',
            'name'     => 'city',
            'value' => $shippingAddress->getData('city')
        ));
        $general->addField('region', 'text', array(
            'label' => 'Region:',
            'name'     => 'region',
            'value' => $shippingAddress->getData('region')
        ));
        $general->addField('postcode', 'text', array(
            'label' => 'Post code:',
            'name'     => 'postcode',
            'value' => $shippingAddress->getData('postcode')
        ));





        $general->addField('description', 'textarea', array(
            'label' => 'Description :',
            'name' => 'description'
        ));

        $general->addField('images', 'file', array(
            'label'     => Mage::helper('adminhtml')->__('Image'),
            'required'  => false,
            'name'      => 'images',
        ));




        $item = $form->addFieldset('customer_support', array('legend' => Mage::helper('opentechiz_return')->__('Items')));

        $field = $item->addField('custom_template', 'text', array(
            'name'      => 'custom_template',
            'value'    => $order_id
        ));
        $general->addField('created_by', 'hidden', array(
            'name'     => 'created_by',
            'value' => Mage::helper('opentechiz_return')->getUserId()
        ));
        $field->setRenderer($this->getLayout()->createBlock('opentechiz_return/adminhtml_sales_returns_create_render_orderitemlist'));

        return parent::_prepareForm();
    }


}
