<?php

class OpenTechiz_Return_Block_Adminhtml_Sales_Returns_Edit_Tab_Returninfo extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function getSteps($rma){
        $rma_id = $rma->getId();
        $step = Mage::getModel('opentechiz_return/repair')->load($rma_id,'return_id')->getRepairStep();
        if(strpos($step, ',"Finished"]') === true)    
            $step = str_replace(',"Finished"]', ']', $step);
        $step_ar = json_decode($step);
        $html = '<style>
        #step_repair_table,#step_repair_table th,#step_repair_table td {
          border: 1px solid black;
          border-collapse: collapse;
          text-align:center;
        }
        </style> <table id="step_repair_table" style="width:100%">
          <tr id="sort_step_0" >
            <th style="width:30%" >Sort Order</th>
            <th>Step Name</th> 
            ';
        $i=1;
        if(!is_array($step_ar)){
            $html .= '<th></th> 
          </tr></table>';
            return $html;
        }
        $html .=' 
          </tr>'; 
        foreach ($step_ar as $value) {
            if($value){
                $html .= '<tr id="sort_step_'.$i.'"><td>'.$i.'</td><td>'.$value.'</td></tr>';
                $i++;
             }
        }
        $html .= '</table>';
        return $html;
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $rma = Mage::registry('current_return');
        $specific_item =   Mage::helper('opentechiz_return')->getItembybarcode($rma->getItemBarcode())->getFirstItem();
        $orderItem = Mage::getModel('sales/order_item')->load($rma->getOrderItemId());
        $itemTotal = (($orderItem->getBaseRowTotal() + $orderItem->getBaseTaxAmount())- $orderItem->getBaseDiscountAmount()) / $orderItem->getQtyOrdered();
        $options = unserialize($specific_item->getOptions());
        if(isset($options['info_buyRequest']) && isset($options['info_buyRequest']['options'])
            && array_key_exists('quotation_product_id', $options['info_buyRequest']['options'])
            && !$rma->getSolution())
            $solutions = array(
                '' => ' ',
                OpenTechiz_Return_Helper_Data::REPAIRITEM => $this->__('Repair Item')
            );
        else {
            if(!$rma->getSolution()) {
                $solutions = Mage::helper('opentechiz_return')->getAllSolution();
            }else{
                $solutions = Mage::helper('opentechiz_return')->getAllSolutionToChange();
            }
        }
        $general = $form->addFieldset('general', array('legend' => Mage::helper('opentechiz_return')->__('General')));
        $disabled = $rma->getSolution()&&$rma->getStatus()!=46;
        

        $created_at = Mage::getModel('core/date')->timestamp($rma->getCreatedAt());
        $created_at = new Zend_Date($created_at);

        if($rma->getStatus()==46)
            unset($solutions[OpenTechiz_Return_Helper_Data::REPAIRITEM]);


         $general->addField('created_at', 'label', array(
            'label' => 'Created Date:',
            'value' =>$created_at
        ));
        $general->addField('customer_name', 'label', array(
            'label' => 'Customer Name:',
            'value' => $rma->getFirstName() .' '. $rma->getLastName()
        ));

        $general->addField('email', 'label', array(
            'label' => 'Customer Email:',
            'value' => $rma->getEmail()
        ));

        $general->addField('phone', 'label', array(
            'label' => 'Customer Phone:',
            'value' => $rma->getPhone()
        ));
         $general->addField('address', 'label', array(
            'label' => 'Address:',
            'value' => $rma->getStreet().', '.$rma->getCity().', '.$rma->getRegion().' '.$rma->getPostcode()
        ));
        $general->addField('return_type', 'label', array(
            'label' => 'Return Type:',
            'value' => Mage::helper('opentechiz_return')->getReturnType($rma->getReturnType())
        ));
        
        
        $general->addField('reason', 'label', array(
            'label' => 'Return Reason:',
            'value' => Mage::helper('opentechiz_return')->getReturnReason($rma->getReason())
        ));
        if(!empty($rma->getCreatedBy())){
            $general->addField('created_by', 'label', array(
                'label' => 'Created By:',
                'value' => Mage::helper("opentechiz_return")->getUserName($rma->getCreatedBy())
            ));
        }
        
        if(!is_null($rma->getStatusDenied()) && $rma->getStatusDenied() !=''){
            $general->addField('status_denied', 'label', array(
                'label' => 'Status Denied:',
                'value' => OpenTechiz_Return_Helper_Data::STATUS_DENIED[$rma->getStatusDenied()]
            ));
        }
          $general->addField('status','select', array(
            'label'    => Mage::helper('opentechiz_purchase')->__('Status'),
            'name'     => 'status',
            'type'      => 'options',
            'value' =>$rma->getStatus(),
            'options'   => Mage::helper('opentechiz_return')->getAllStatus(),
        ));
        $general->addField('solution','select', array(
            'label'    => Mage::helper('opentechiz_purchase')->__('Solution'),
            'name'     => 'solution',
            'type'      => 'options',
            'value' =>$rma->getSolution(),
            'options'   => $solutions,
            'onchange'  => 'onchangeSolution()',
//            'disabled'  =>  $disabled
        ))->setAfterElementHtml($this->getSolutionHtml());
        $order = Mage::helper('opentechiz_return')->getRmaOrder($rma);
        $order_item = Mage::helper('opentechiz_return')->getRmaOrderItem($rma);
        $general->addField('description', 'textarea', array(
            'label' => 'Description :',
            'value' => $rma->getDescription(),
            'disabled' => true
        ));
        $general->addField('return_image', 'link', array(        
            'label'     => Mage::helper('adminhtml')->__('Return Image'),
            'value' => $rma->getReturnImage(),
            'href' => Mage::getBaseUrl("media").'upload'.DS.$rma->getReturnImage(),
            'popup' => true,
            'target' => '_blank',
        ));
        
        $general->addField('order', 'link', array(
            'label' => 'Original order :',
            'value' => '#'.$order->getIncrementId(),
            'href' => Mage::getUrl('adminhtml/sales_order/view/',['order_id'=>$order->getId()])
        ));
        if(!$rma->getSolution() || $rma->getSolution()==15 || $rma->getSolution()==10 || $rma->getSolution()==20){
        //disable edit return fee in return if order is created, fee can be edited in order
        $disableReturnFee = false;
        if($rma->getReturnFeeOrderId()) {
            $disableReturnFee = true;
        }
        $general->addField('return_fee', 'text', array(
            'label'     => Mage::helper('opentechiz_return')->__('Return fee'),
            'name'      => 'return_fee',
            'class'     => 'validate-number',
            'value'     => $rma->getReturnFee(),
            'disabled'  => $disableReturnFee
         ));
        if(!$rma->getReturnFeeOrderId()) {
            $general->addField('create_return_order', 'select', array(
                'label' => Mage::helper('opentechiz_return')->__('Create Return Order'),
                'name' => 'create_return_order',
                'type' => 'options',
                'options' => array(1 => "Yes", 0 => "No"),
            ));
        }
        $general->addField('is_paid', 'select', array(
            'label'     => Mage::helper('opentechiz_return')->__('Return Fee Paid'),
            'name'     => 'step_list',
            'type'      => 'options',
            'options'   => array(1 => "Yes", 0=> "No"),
            'value'     => $rma->getIsPaid()
             ));
        if($rma->getSolution()!=20)
            $general->addField('production_note', 'textarea', array(
            'label'     => Mage::helper('opentechiz_return')->__('Production Note'),
            'name'      => 'production_note'
             ));
        }
        if($rma->getReturnFeeOrderId()) {
            $returnOrder = Mage::getModel('sales/order')->loadByIncrementId($rma->getReturnFeeOrderId());
            $general->addField('return_order', 'link', array(
                'label' => 'Return Order :',
                'value' => '#'.$rma->getReturnFeeOrderId(),
                'href' => Mage::getUrl('adminhtml/sales_order/view/',['order_id'=>$returnOrder->getId()])
            ));
        }
        if($rma->getSolution()==5 ){
            $general->addField('refund_amount', 'text', array(
                'label'     => Mage::helper('opentechiz_return')->__('Refund amount'),
                'name'      => 'refund_amount',
                'class'     => 'validate-number',
                'value'     => $rma->getRefundAmount()
            ));
        }elseif(!$rma->getSolution()){
            $general->addField('refund_amount', 'text', array(
                'label'     => Mage::helper('opentechiz_return')->__('Refund amount'),
                'name'      => 'refund_amount',
                'class'     => 'validate-number',
                'value'     => $itemTotal
            ));
        }

        if(!$rma->getSolution()){
             $general->addField('add_js', 'hidden', array(
                'name'      => 'add_js',
                'value' => ' '
            ))->setAfterElementHtml('<script>
                jQuery("#return_fee").parent().parent().hide();
                jQuery("#create_return_order").parent().parent().hide();
                jQuery("#is_paid").parent().parent().hide();
                jQuery("#production_note").parent().parent().hide();
                jQuery("#refund_amount").parent().parent().hide();</script>');
        }
        $item = $form->addFieldset('customer_support', array('legend' => Mage::helper('opentechiz_return')->__('Item')));

        $item->addField('item_barcode', 'label', array(
            'label' => 'Item Barcode:',
            'value' => $rma->getItemBarcode()
        ));

        $item->addField('item_name', 'label', array(
            'label' => 'Item Name:',
            'value' => $order_item->getName()
        ));
        
        
        $item->addField('item_sku', 'label', array(
            'label' => 'Item SKU:',
            'value' =>  Mage::helper('opentechiz_purchase')->getTagSku($specific_item->getSku())
        )); 

        $itemOption = strip_tags(Mage::helper('opentechiz_return')->getOption(unserialize($specific_item->getOptions()),$specific_item->getSku()));
        $option_info = $item->addField('item_option', 'label', array(
            'label' => 'Item Option:',
            'value' => $itemOption
        ));
        if($new_item_barcode =  $rma->getNewItemBarcode()){
            $new_return_item = Mage::helper('opentechiz_return')->getItembybarcode($new_item_barcode)->getFirstItem();
            $item->addField('new_item_barcode', 'label', array(
            'label' => 'New Item Barcode:',
            'value' => $rma->getNewItemBarcode()
            ));
            if($new_return_item->getSku() != $specific_item->getSku()){
                $item->addField('new_item_sku', 'label', array(
                    'label' => 'New Item SKU:',
                    'value' => $new_return_item->getSku()
                )); 
                 $item->addField('new_item_option', 'label', array(
                    'label' => 'New Item Option:',
                    'value' =>strip_tags( Mage::helper('opentechiz_return')->getOption(unserialize($new_return_item->getOptions()),$new_return_item->getSku())
                )));
            }
        }
        if($rma->getNewOption() && $rma->getSolution() &&
            $rma->getSolution() != 5 && !in_array($rma->getStatus(), [45,55,60])){
            $newOption = strip_tags(Mage::helper('opentechiz_return')->getOption(unserialize($rma->getNewOption())));
            if($newOption != $itemOption) {
                $item->addField('new__option', 'label', array(
                    'label' => 'New Option:',
                    'value' => $newOption
                ));
            }
        }
        if(!$rma->getSolution() || $rma->getSolution()==10){

            $repair_fieldset = $form->addFieldset('repair_fieldset', array('legend' => Mage::helper('opentechiz_quotation')->__('Repair Step')));
            $types = OpenTechiz_Production_Helper_Data::PROCESS_LIST;
            array_pop($types);
            $repair_fieldset->addField('repair_step', 'hidden', array(
                'name'      => 'repair_step',
                'value' => $rma->getRepairStep()
            ));
            $repair_fieldset->addField('repair_step_field', 'label', array(
                'label' => 'Repair Step:',
                'value' => ''
            ))->setAfterElementHtml($this->getSteps($rma));
                if(!$rma->getSolution()){
                $repair_fieldset->addField('step_list','select', array(
                    'label'    => Mage::helper('opentechiz_return')->__('Step'),
                    'name'     => 'step_list',
                    'type'      => 'options',
                    'options'   => $types
                ));
                $html = '';

                    $params = $this->getRequest()->getParams();
                    if(isset($params['type'])) {
                        if ($params['type'] != 'repair') {
                            $html = '<script>
                            jQuery("#repair_fieldset").hide();
                            jQuery("#repair_fieldset").prev().hide();</script>';
                        }
                    }
                $repair_fieldset->addField('step_list_n','label', array(
                    'label'    => Mage::helper('opentechiz_return')->__('Step Order'),
                    'value'    => '',

                ))->setAfterElementHtml('<input id="step_number_input" type="number" style="float:left; margin-left: 10px;" min="1" max="'.count($types).'"  ><a onclick="AddStep()" href="#!" class="service" style="float:right">Add Step</a>'.$html);
                $repair_fieldset->addField('request_stone','label', array(
                    'label'    => Mage::helper('opentechiz_return')->__('Stone Request'),
                    'value'    => '',

                ))->setAfterElementHtml('<div id="stone_request_div" ></div><a id="request_click" onclick="jsStoneRequest('.$order_item->getProductId().',\''.$specific_item->getSku().'\')" href="#!" class="service" style="float:right;margin:5px;" > Stone Request </a>');
            }
        }
        if(!$rma->getSolution() && strpos($specific_item->getSku(), 'quotation') === false){
        $after_html = '<a id="new-option-btn" onclick="jsGetProduct('.$order_item->getProductId().')" href="#" style="float:right">New Option</a><div id=\'product-info\'></div>';
        }else $after_html = '';
        $item_options = unserialize($specific_item->getOptions());
        if(!isset($item_options['options'])) $item_options['options'] = array();
        $option_info->setAfterElementHtml($after_html.'
        <script>
            jQuery("#repair_fieldset").hide();
            jQuery("#repair_fieldset").prev().hide();
           
            function AddStep() {
                jQuery("#loading-mask").show();
                var selected = jQuery("#step_list").children("option:selected");
                 var step_ar = [];
                jQuery("#step_repair_table").find(\'> tbody > tr\').each(function () {
                    jQuery(this).children(\'td:first\').text(jQuery(this).index());
                    jQuery(this).attr(\'id\',\'sort_step_\'+jQuery(this).index());
                    step_ar.push(jQuery(this).find("td:eq(1)").text());
                });
                if(jQuery.inArray(selected.text(),step_ar)!=-1){
                    console.log(jQuery.inArray(selected.text(),step_ar));
                    alert(\'Step \'+ selected.text()+\' already added\');
                    jQuery("#loading-mask").hide();
                }else{
                var html = \'<tr><td>3</td><td>\'+selected.text()+\'</td><td><a  href="#" class="remove_button" onclick="RemoveStep(event)">x</a></td></tr>\'
                var sort =  jQuery("#step_number_input").val() - 1; 
                jQuery("#sort_step_"+sort).after(html);
                 var step_ar = [];
                jQuery("#step_repair_table").find(\'> tbody > tr\').each(function () {
                    jQuery(this).children(\'td:first\').text(jQuery(this).index());
                    jQuery(this).attr(\'id\',\'sort_step_\'+jQuery(this).index());
                    step_ar.push(jQuery(this).find("td:eq(1)").text());
                });
                console.log(jQuery.inArray("Stone Setting", step_ar));
                if(jQuery.inArray("Stone Setting", step_ar) === -1){
                    jQuery("#stone_request_div").html("");
                     jQuery("#stone_request_div").parent().parent().hide();
                }else{
                    jQuery("#stone_request_div").parent().parent().show();
                }
                jQuery("#repair_step").val(JSON.stringify(step_ar));
                
                jQuery("#loading-mask").hide();
               }
            }
             function RemoveStep(event) {
                  event.preventDefault();
                  var row = event.target.closest("tr");
                  var step_ar = [];
                  row.remove();
                  
                  jQuery("#step_repair_table").find(\'> tbody > tr\').each(function () {
                    jQuery(this).children(\'td:first\').text(jQuery(this).index());
                    jQuery(this).attr(\'id\',\'sort_step_\'+jQuery(this).index());
                    step_ar.push(jQuery(this).find("td:eq(1)").text());
                });
                if(jQuery.inArray("Stone Setting", step_ar) === -1){
                    jQuery("#stone_request_div").html("");
                     jQuery("#stone_request_div").parent().parent().hide();
                }
                jQuery("#repair_step").val(JSON.stringify(step_ar));
             }
            function jsRemoveOption(productId) {
                jQuery("#stone_request_div").html("");
                 var productInfoDiv = document.getElementById(\'product-info\');
                 productInfoDiv.innerHTML = "";
            }
            function jsStoneRequest(productId,sku){
                 var option = {};
                 var option_id,option_value,is_new=1;

                 jQuery(\'.product-custom-option:checked\').each(function(){
                        option_id = jQuery(this).attr(\'name\').replace(/[\[\]]?/g, "").replace("options", "");
                        option_value = jQuery(this).val();
                        option[option_id] = option_value;
                    });
                  if(jQuery.isEmptyObject(option)) {
                    option = '.json_encode($item_options['options']).';
                    is_new = 0;
                  }
                  var url = "'.$this->getUrl('adminhtml/instockwaiting/getstonerequest').'";
                var data = {\'id\' : productId ,option : option,is_new : is_new,sku: sku};
                jQuery("#loading-mask").show();
                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(data)
                    {   
                        jQuery("#stone_request_div").html("");
                        jQuery("#stone_request_div").append(data);
                        jQuery("#loading-mask").hide();
                    }
                });
                
            } 
            function jsGetProduct(productId) {
                jQuery("#stone_request_div").html("");
                var productInfoDiv = document.getElementById(\'product-info\');
                var url = "'.$this->getUrl('adminhtml/sales_order_create/configureProductToAdd').'";

                var data = {\'id\' : productId };
                jQuery("#loading-mask").show();
                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(data)
                    {
                        productInfoDiv.innerHTML = data+\'<a onclick="jsRemoveOption('.$order_item->getProductId().')" href="#" style="float:right">Hide Option</a>\';
                        jQuery("#product_composite_configure_fields_qty").remove();
                        jQuery("#loading-mask").hide();
                    }
                });

            }
            </script>');
        return parent::_prepareForm();
    }

    public function getSolutionHtml(){
        $html = '
            <script>
            function onchangeSolution() {  
                if(jQuery("#solution").val()==10){
                    jQuery("#repair_fieldset").show();
                    jQuery("#repair_fieldset").prev().show();     
                    jQuery("#select_set").parent().parent().show();
                    jQuery("#return_fee").parent().parent().show();
                    jQuery("#create_return_order").parent().parent().show();
                    jQuery("#is_paid").parent().parent().show();
                    jQuery("#production_note").parent().parent().show();
                    jQuery("#refund_amount").parent().parent().hide();
                    jQuery("#stone_request_div").parent().parent().hide();
                    jQuery("#new-option-btn").hide();
                }else{
                    jQuery("#stone_request_div").html("");
                    jQuery("#repair_fieldset").hide();
                    jQuery("#repair_fieldset").prev().hide();
                    jQuery("#select_set").parent().parent().hide();
                    if(jQuery("#solution").val() != 15 && jQuery("#solution").val() != 20){
                        jQuery("#return_fee").parent().parent().hide();
                        jQuery("#create_return_order").parent().parent().hide();
                        jQuery("#is_paid").parent().parent().hide();
                        jQuery("#production_note").parent().parent().hide();
                        jQuery("#refund_amount").parent().parent().show();
                        jQuery("#new-option-btn").hide();
                    }else{
                        jQuery("#return_fee").parent().parent().show();
                        jQuery("#create_return_order").parent().parent().show();
                        jQuery("#is_paid").parent().parent().show();
                        jQuery("#new-option-btn").show();
                    if(jQuery("#solution").val()==15)
                        jQuery("#production_note").parent().parent().show();
                        jQuery("#refund_amount").parent().parent().hide();
                    }
                    if(jQuery("#solution").val()==""){
                        jQuery("#refund_amount").parent().parent().hide();
                    }
                }
            }
            </script>
        ';
        $params = $this->getRequest()->getParams();
        if(isset($params['type'])){
            $type = 0;
            if($params['type'] == 'repair'){
                $type = 10;
            }elseif($params['type'] == 'exchange'){
                $type = 20;
            }
            $html .= '<script>
                          jQuery("#solution").val('.$type.');
                          jQuery("#solution").change();
                          document.getElementById("solution").disabled = true; 
                      </script>
                      <input name="solution" type="hidden" value="'.$type.'"/>';
        }

        return $html;
    }
}
