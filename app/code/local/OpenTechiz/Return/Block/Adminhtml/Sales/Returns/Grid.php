<?php

class OpenTechiz_Return_Block_Adminhtml_Sales_Returns_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set grid params
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('returnGrid');
        $this->setDefaultSort('return_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $sales_order =  Mage::getSingleton('core/resource')->getTableName('sales/order');
        $sales_item =  Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $shipment =  Mage::getSingleton('core/resource')->getTableName('sales/shipment');
        $collection = Mage::getModel('opentechiz_return/return')->getCollection()->addFieldToFilter("main_table.status",array('nin'=>array(51,65,80)));
        $collection->getSelect()
                    ->joinLeft(array("sales_item"=>$sales_item),"main_table.order_item_id = sales_item.item_id",array('real_order_id'=>'sales_item.order_id'))
                    ->joinLeft(array("sales_order"=>$sales_order),"sales_item.order_id = sales_order.entity_id",array('order_increment_id'=>'sales_order.increment_id'))
                    ->joinLeft(array("shipment"=>$shipment),"shipment.order_id = sales_order.entity_id",array('created_at_shipment' => 'shipment.created_at'));
        //echo $collection->getSelect()->__toString();die('aa');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('return_id', array(
            'header' => Mage::helper('opentechiz_return')->__('ID #'),
            'width' => '50',
            'index' => 'return_id',
            'align' => 'center'
        ));

        $this->addColumn('increment_id', array(
            'header' => Mage::helper('opentechiz_return')->__('Increment ID'),
            'index' => 'increment_id',
            'filter_index'=>'main_table.increment_id',
            'width' => '50',
            'align' => 'center'
        ));

        $this->addColumn('return_type', array(
            'header' => Mage::helper('opentechiz_return')->__('Type'),
            'index' => 'return_type',
            'type' => 'options',
            'options' => Mage::helper('opentechiz_return')->getReturnTypes(),
            'align' => 'center',
        ));
        $this->addColumn('reason', array(
            'header' => Mage::helper('opentechiz_return')->__('Reason'),
            'index' => 'reason',
            'type' => 'options',
            'options' => Mage::helper('opentechiz_return')->getAllReasons(),
            'align' => 'center'
        ));
        $this->addColumn('description', array(
            'header' => Mage::helper('opentechiz_return')->__('Description'),
            'index' => 'description',
        ));

        $this->addColumn('customer_name', array(
            'header'    => Mage::helper('opentechiz_return')->__('Customer Name'),
            'index'        => array('first_name', 'last_name'),
            'type'         => 'concat',
            'separator'    => ' ',
            'filter_index' => new Zend_Db_Expr("CONCAT(first_name, ' ', last_name)"),
            'width'        => '140px',
        ));

        $this->addColumn('customer_email', array(
            'header' => Mage::helper('opentechiz_return')->__('Customer Email'),
            'index' => 'email',
        ));

        $this->addColumn('customer_phone', array(
            'header' => Mage::helper('opentechiz_return')->__('Customer Phone'),
            'index' => 'phone',
        ));

        $this->addColumn('street', array(
            'header' => Mage::helper('opentechiz_return')->__('Address'),
            'index' => array('street', 'city'),
            'type'         => 'concat',
            'separator'    => '<br>',
            'filter_index' => new Zend_Db_Expr("CONCAT(street, ' ', city)"),
            'width'        => '140px',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('opentechiz_return')->__('Created At'),
            'index' => 'created_at',
            'filter_index'=>'main_table.created_at',
            'type' => 'datetime'
        ));
        $this->addColumn('updated_at', array(
            'header' => Mage::helper('opentechiz_return')->__('Updated At'),
            'index' => 'updated_at',
            'filter_index'=>'main_table.updated_at',
            'type' => 'datetime'
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('opentechiz_return')->__('Status'),
            'width' => '120',
            'index' => 'status',
            'filter_index'=>'main_table.status',
            'type' => 'options',
            'options' => Mage::helper('opentechiz_return')->getAllStatus(),
            'align' => 'center',
            'renderer' => 'OpenTechiz_Return_Block_Adminhtml_Renderer_Status'
        ));
        $this->addColumn('is_paid', array(
            'header' => Mage::helper('opentechiz_return')->__('Paid'),
            'index' => 'is_paid',
            'type' => 'options',
            'options' => array(0 => 'No', 1 => 'Yes'),
            'align' => 'center'
        ));
        $this->addColumn('order_increment_id', array(
            'header' => Mage::helper('opentechiz_return')->__('Order #'),
            'width' => '50',
            'index' => 'order_increment_id',
            'filter_index' => 'sales_order.increment_id',
            'renderer' => 'OpenTechiz_Return_Block_Adminhtml_Renderer_RealOrderLink'
        ));
        $this->addColumn('shipping', array(
            'header' => Mage::helper('opentechiz_return')->__('Shipped At'),
            'width' => '50',
            'index' => 'created_at_shipment',
            'filter_index' => 'shipment.created_at',
            'type' => 'datetime'
        ));
        $this->addColumn('order_id', array(
            'header' => Mage::helper('opentechiz_return')->__('Return Fee Order Id'),
            'index' => 'return_fee_order_id',
            'type' => 'text',
            'renderer' => 'OpenTechiz_Return_Block_Adminhtml_Renderer_OrderLink'
        ));
        $this->addColumn('created_by', array(
            'header' => Mage::helper('opentechiz_return')->__('Created By'),
            'index' => 'created_by',
            'type' => 'text',
            'renderer' => 'OpenTechiz_Return_Block_Adminhtml_Renderer_CreatedByUser'
        ));
       
        $this->addColumn('action',
            array(
                'header' => Mage::helper('opentechiz_production')->__('Action'),
                'width' => '50',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('opentechiz_production')->__('View'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id',
                        'data-column' => 'action',
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
                'align' => 'center'
            ));
        
        return parent::_prepareColumns();
    }

 

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
    
    protected function _prepareMassaction()
    {
        if(Mage::getSingleton('admin/session')->isAllowed('sales/return/allow_decline_approved')){
            $this->setMassactionIdField('return_id');
            $this->getMassactionBlock()->setFormFieldName('item');
            $this->getMassactionBlock()->setUseSelectAll(false);

            $this->getMassactionBlock()->addItem('status', array(
                'label' => Mage::helper('opentechiz_material')->__('Update Status Denied'),
                'url' => $this->getUrl('*/*/massStatus'),
                'additional' => array(
                    'visibility' => array(
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => Mage::helper('opentechiz_material')->__('Status'),
                        'values' => [1 => 'Pending', 2 => 'Approved', 3 => 'Decline']
                    )
                )
            ));
            return $this;
        }else{
            return false;
        }
    }
}
