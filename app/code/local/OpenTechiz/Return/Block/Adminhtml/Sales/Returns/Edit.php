<?php

class OpenTechiz_Return_Block_Adminhtml_Sales_Returns_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_sales_returns';
        $this->_blockGroup = 'opentechiz_return';
        $this->_removeButton('reset');
        $this->_removeButton('delete');
        $this->_updateButton('save', 'label', Mage::helper('opentechiz_return')->__('Save'));
        if($this->getRequest()->getParam('id')) {
            $return = Mage::getModel('opentechiz_return/return')->load($this->getRequest()->getParam('id'));
            if($return->getSolution() && $return->getSolution() != OpenTechiz_Return_Helper_Data::REFUND) {
                if (!$return->getReturnFeeOrderId()) {
                    $this->_addButton('createOrder', array(
                        'label' => 'Create Order for Return',
                        'onclick' => 'setLocation(\'' . $this->getUrl('*/*/createOrder/id/' . $this->getRequest()->getParam('id')) . '\')',
                        'class' => 'save',
                    ));
                }
                $this->_addButton('sendMailProblem', array(
                    'label' => 'Send Mail report problems with received item',
                    'onclick' => 'setLocation(\'' . $this->getUrl('*/*/sendMailProblem/id/' . $this->getRequest()->getParam('id')) . '\')',
                    'class' => 'save',
                ));
            }
        }

        $this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('quote_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'quote_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'quote_content');
			}
		";
    }
    
    public function getHeaderText()
    {
        if (Mage::registry('current_return') && Mage::registry('current_return')->getId()) {
            return Mage::helper('opentechiz_return')->__("Detail for #%s", $this->htmlEscape(Mage::registry('current_return')->getIncrementId()));
        }
    }
}