<?php
class OpenTechiz_Return_Block_Adminhtml_Sales_Order extends Mage_Adminhtml_Block_Sales_Order
{
    public function __construct()
    {
//        $this->_controller = 'sales_order';
//        $this->_headerText = Mage::helper('sales')->__('Orders');
//        $this->_addButtonLabel = Mage::helper('sales')->__('Create New Standard Order');
//
//        $this->_addButton('createRepairOrder', array(
//            'label' => 'Create Order for Repair',
//            'onclick' => 'setLocation(\'' . $this->getUrl('adminhtml/sales_returns/new/type/repair') . '\')',
//            'class' => 'add',
//        ));
//        $this->_addButton('createExchangeOrder', array(
//            'label' => 'Create Order for Exchange',
//            'onclick' => 'setLocation(\'' . $this->getUrl('adminhtml/sales_returns/new/type/exchange') . '\')',
//            'class' => 'add',
//        ));
        parent::__construct();

//        $this->updateButton('add', 'label', Mage::helper('sales')->__('Create New Standard Order'));
        $this->setTemplate('opentechiz/sales_extend/order_grid.phtml');
    }

    public function getLayawayOrderCreateUrl(){
        return Mage::helper("adminhtml")->getUrl("adminhtml/sales_order_create/start", array("type" => "layaway"));
    }

    public function getStandardOrderCreateUrl(){
        return Mage::helper("adminhtml")->getUrl("adminhtml/sales_order_create/start", array("type" => "standard"));
    }

    public function getRepairOrderCreateUrl(){
        return Mage::helper("adminhtml")->getUrl("adminhtml/sales_returns/new",array("type"=>"repair"));
    }

    public function getExchangeOrderCreateUrl(){
        return Mage::helper("adminhtml")->getUrl("adminhtml/sales_returns/new",array("type"=>"exchange"));
    }
}