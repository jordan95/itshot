<?php

class OpenTechiz_Return_Block_Adminhtml_Sales_Returns_Create extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_sales_returns';
        $this->_blockGroup = 'opentechiz_return';
        $this->_removeButton('reset');
        $this->_removeButton('delete');
        $this->_updateButton('save', 'label', Mage::helper('opentechiz_return')->__('Next'));
        
        $this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('quote_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'quote_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'quote_content');
			}
		";
    }
    
    public function getHeaderText()
    {
        if (Mage::registry('current_return') && Mage::registry('current_return')->getId()) {
            return Mage::helper('opentechiz_return')->__("Detail for #%s", $this->htmlEscape(Mage::registry('current_return')->getIncrementId()));
        }
    }
   
}