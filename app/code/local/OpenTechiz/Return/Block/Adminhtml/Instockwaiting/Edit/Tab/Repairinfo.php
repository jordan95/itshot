<?php

class OpenTechiz_Return_Block_Adminhtml_Instockwaiting_Edit_Tab_Repairinfo extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function getSteps($step){
        
        $step_ar = json_decode($step);
        $html = '<style>
        table, th, td {
          border: 1px solid black;
          border-collapse: collapse;
          text-align:center;
        }
        </style> <table id="step_repair_table" style="width:100%">
          <tr id="sort_step_0" >
            <th style="width:30%" >Sort Order</th>
            <th>Step Name</th> 
            ';
        $i=1;
        if(!is_array($step_ar)){
            $html .= '<th></th> 
          </tr></table>';
            return $html;
        }
        $html .=' 
          </tr>'; 
        foreach ($step_ar as $value) {
            if($value){
                $html .= '<tr id="sort_step_'.$i.'"><td>'.$i.'</td><td>'.$value.'</td></tr>';
                $i++;
             }
        }
        $html .= '</table>';
        return $html;
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $item_repair = Mage::registry('current_item');
        $product_id = Mage::helper('opentechiz_return')->getProductByItem($item_repair);
        $repair_step = '';
        if($item_repair->getId()){
            $repair = Mage::helper('opentechiz_return')->getRepair($item_repair->getBarcode()); 
            if($repair)
              $repair_step = $repair->getRepairStep();
        }
        
        $item = $form->addFieldset('customer_support', array('legend' => Mage::helper('opentechiz_return')->__('Item')));

        $item->addField('item_barcode', 'label', array(
            'label' => 'Item Barcode:',
            'value' => $item_repair->getBarcode()
        ));

        $item->addField('item_name', 'label', array(
            'label' => 'Item Name:',
            'value' => $item_repair->getName()
        ));
        $item->addField('item_sku', 'label', array(
            'label' => 'Item SKU:',
            'value' => $item_repair->getSku()
        )); 
        $option_info = $item->addField('item_option', 'label', array(
            'label' => 'Item Option:',
            'value' =>strip_tags( Mage::helper('opentechiz_return')->getOption(unserialize($item_repair->getOptions()),$item_repair->getSku()))
        ));
        $item->addField('product_id', 'hidden', array(
                'name'      => 'product_id',
                'value' => $product_id
            ));
            $repair_fieldset = $form->addFieldset('repair_fieldset', array('legend' => Mage::helper('opentechiz_quotation')->__('Repair Step')));
            $types = OpenTechiz_Production_Helper_Data::PROCESS_LIST;
            array_pop($types);
            $repair_fieldset->addField('repair_step', 'hidden', array(
                'name'      => 'repair_step',
                'value' => $repair_step
            ));
            $repair_fieldset->addField('repair_step_field', 'label', array(
                'label' => 'Repair Step:',
                'value' => ''
            ))->setAfterElementHtml($this->getSteps($repair_step));
            $repair_fieldset->addField('step_list','select', array(
                'label'    => Mage::helper('opentechiz_return')->__('Step'),
                'name'     => 'step_list',
                'type'      => 'options',
                'options'   => $types
            ));
            
          
                
            $repair_fieldset->addField('step_list_n','label', array(
                'label'    => Mage::helper('opentechiz_return')->__('Step Order'),
                'value'    => '',
               
            ))->setAfterElementHtml('<input id="step_number_input" type="number" style="float:left; margin-left: 10px;" min="1" max="'.count($types).'"  ><a onclick="AddStep()" href="#" style="float:right">Add Step</a>');
            $repair_fieldset->addField('request_stone','label', array(
                'label'    => Mage::helper('opentechiz_return')->__('Stone Request'),
                'value'    => '',
               
            ))->setAfterElementHtml('<div id="stone_request_div" ></div><a onclick="jsStoneRequest('.$product_id.',\''.$item_repair->getSku().'\')" href="#" style="float:right;margin:5px"> Stone Request </a>');
         
             $afterhtml = '';
         $item_options = unserialize($item_repair->getOptions());

        $option_info->setAfterElementHtml($afterhtml.'<div id=\'product-info\'></div>
        <script>
            jQuery("#repair_fieldset").hide();
            jQuery("#repair_fieldset").prev().hide();
            function AddStep() {
                jQuery("#loading-mask").show();
                var selected = jQuery("#step_list").children("option:selected");
                var html = \'<tr><td>3</td><td>\'+selected.text()+\'</td><td><a  href="#" class="remove_button" onclick="RemoveStep(event)">x</a></td></tr>\'
                var sort =  jQuery("#step_number_input").val() - 1; 
                jQuery("#sort_step_"+sort).after(html);
                 var step_ar = [];
                jQuery("#step_repair_table").find(\'> tbody > tr\').each(function () {
                    jQuery(this).children(\'td:first\').text(jQuery(this).index());
                    jQuery(this).attr(\'id\',\'sort_step_\'+jQuery(this).index());
                    step_ar.push(jQuery(this).find("td:eq(1)").text());
                });
                
                jQuery("#repair_step").val(JSON.stringify(step_ar));
                
                jQuery("#loading-mask").hide();
            }
             function RemoveStep(event) {
                  event.preventDefault();
                  var row = event.target.closest("tr");
                  var step_ar = [];
                  row.remove();
                  
                  jQuery("#step_repair_table").find(\'> tbody > tr\').each(function () {
                    jQuery(this).children(\'td:first\').text(jQuery(this).index());
                    jQuery(this).attr(\'id\',\'sort_step_\'+jQuery(this).index());
                    step_ar.push(jQuery(this).find("td:eq(1)").text());
                });
                jQuery("#repair_step").val(JSON.stringify(step_ar));
             }
            function jsRemoveOption(productId) {
                 var productInfoDiv = document.getElementById(\'product-info\');
                 jQuery("#stone_request_div").html("");
                 productInfoDiv.innerHTML = "";
            }
            function jsStoneRequest(productId,sku){
                 var option = {};
                 var option_id,option_value,is_new=1;

                 jQuery(\'.product-custom-option:checked\').each(function(){
                        option_id = jQuery(this).attr(\'name\').replace(/[\[\]]?/g, "").replace("options", "");
                        option_value = jQuery(this).val();
                        option[option_id] = option_value;
                    });
                  if(jQuery.isEmptyObject(option)) {
                    option = '.json_encode($item_options['options']).';
                    is_new = 0;
                  }
                  var url = "'.$this->getUrl('adminhtml/instockwaiting/getstonerequest').'";
                var data = {\'id\' : productId ,option : option,is_new : is_new,sku: sku};
                jQuery("#loading-mask").show();
                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(data)
                    {   
                        jQuery("#stone_request_div").html("");
                        jQuery("#stone_request_div").append(data);
                        console.log(data);
                        jQuery("#loading-mask").hide();
                    }
                });
                
            }
            function jsGetProduct(productId) {
                var productInfoDiv = document.getElementById(\'product-info\');
                var url = "'.$this->getUrl('adminhtml/sales_order_create/configureProductToAdd').'";

                var data = {\'id\' : productId };
                jQuery("#loading-mask").show();
                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(data)
                    {
                        productInfoDiv.innerHTML = data+\'<a onclick="jsRemoveOption('.$product_id.')" href="#" style="float:right;margin:5px">Reset Option</a> \';
                        jQuery("#product_composite_configure_fields_qty").remove();
                        jQuery("#stone_request_div").html("");
                        jQuery("#loading-mask").hide();
                    }
                });

            }
            </script>');
        return parent::_prepareForm();
    }
}
