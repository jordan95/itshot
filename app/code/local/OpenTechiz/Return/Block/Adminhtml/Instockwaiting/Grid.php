<?php

class OpenTechiz_Return_Block_Adminhtml_Instockwaiting_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('waitingitemGrid');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_inventory/item')->getCollection()->addFieldtoFilter('state',array('eq'=>30));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    =>  Mage::helper('opentechiz_return')->__('ID'),
            'type'      => 'text',
            'index'     =>  'item_id',
            'width'     => '80',
            'align'     => 'center'
        ));
        $this->addColumn('image', array(
            'header'    => Mage::helper('opentechiz_return')->__('Gallery Image'),
            'index'     => 'image',
            'width'     => '125px',
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_Image'
        ));
        $this->addColumn('options', array(
            'header'    => Mage::helper('opentechiz_return')->__('Options Information'),
            'index'     => 'options',
            'width'     =>  '500',
            'renderer'  => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_CustomOptions'
        ));
        $this->addColumn('name', array(
            'header'    =>  Mage::helper('opentechiz_return')->__('Product Name'),
            'type'      => 'text',
            'index'     =>  'name',
        ));
        $this->addColumn('sku', array(
            'header'    => Mage::helper('opentechiz_return')->__('SKU'),
            'index'     => 'sku',
            'width'     =>  '100',
        ));
        $this->addColumn('barcode', array(
            'header'    => Mage::helper('opentechiz_return')->__('Barcode #'),
            'index'     => 'barcode',
            'width'     =>  '50'
        ));
        $this->addColumn('created_at', array(
            'header'    => Mage::helper('opentechiz_return')->__('Created At'),
            'index'     => 'created_at',
            'type'      => 'datetime',
        ));
        $this->addColumn('updated_at', array(
            'header'    => Mage::helper('opentechiz_return')->__('Updated At'),
            'index'     => 'updated_at',
            'type'      => 'datetime',
        ));
        $this->addColumn('type', array(
            'header'    => Mage::helper('opentechiz_return')->__('Type'),
            'width'     => '50',
            'index'     => 'type',
            'type'      => 'options',
            'options'   => OpenTechiz_Inventory_Helper_Data::ITEM_TYPE
        ));
        $this->addColumn('note', array(
            'header'    => Mage::helper('opentechiz_return')->__('Note'),
            'index'     => 'note',
            'width'     =>  '100'
        ));
        $this->addColumn('state', array(
            'header'    =>  Mage::helper('opentechiz_return')->__('Item State'),
            'index'     =>  'state',
            'type'      => 'options',
            'options'   => OpenTechiz_Inventory_Helper_Data::ITEM_STATE
        ));
        $this->addColumn('action',
            array(
                'header' => Mage::helper('opentechiz_return')->__('Action'),
                'width' => '50',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('opentechiz_material')->__('View'),
                        'url' => array('base' => '*/*/view'),
                        'field' => 'id',
                        'data-column' => 'action',
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
                'align' => 'center',
            ));
        return parent::_prepareColumns();
    }
    protected function _prepareMassaction()
   {
       $this->setMassactionIdField('item_id');
       $this->getMassactionBlock()->setFormFieldName('item');
        $this->getMassactionBlock()->setUseSelectAll(false);

        // MassDelete
       $this->getMassactionBlock()->addItem('instock_item', array(
            'label'=> Mage::helper('opentechiz_return')->__('In Stock'),
            'url'      => $this->getUrl('*/*/massInstock'),
            'confirm'  => Mage::helper('opentechiz_return')->__('Are you sure?')
       ));
       $this->getMassactionBlock()->addItem('markbroken_item', array(
            'label'=> Mage::helper('opentechiz_return')->__('Mark as broken'),
            'url'      => $this->getUrl('*/*/maskasbroken'),
            'confirm'  => Mage::helper('opentechiz_return')->__('Are you sure?')
       ));
       return $this;
    }
}