<?php

class OpenTechiz_Return_Block_Adminhtml_Instockwaiting_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('repair_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('opentechiz_return')->__('Detail Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('opentechiz_return')->__('Repair Information'),
            'title'     => Mage::helper('opentechiz_return')->__('Repair Information'),
            'content'   => $this->getLayout()->createBlock('opentechiz_return/adminhtml_instockwaiting_edit_tab_repairinfo')->toHtml(),
            
        ));
       
        return parent::_beforeToHtml();
    }


  
}