<?php

class OpenTechiz_Return_Block_Adminhtml_Renderer_OrderLink extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $orderIncrementId =  $row->getData('return_fee_order_id');
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        if($order->getId()) return '<a href="'.Mage::getUrl("adminhtml/sales_order/view", array("order_id" => $order->getId())).'">#'.$orderIncrementId.'</a>';
        else return '';
    }
}