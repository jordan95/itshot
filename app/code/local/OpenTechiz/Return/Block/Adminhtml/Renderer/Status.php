<?php

class OpenTechiz_Return_Block_Adminhtml_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {

        	$status_denied =  $row->getData('status_denied');
            $status =  $row->getData('status');
            if(!empty($status_denied) && ($status == OpenTechiz_Return_Helper_Data::DENIED || $status == OpenTechiz_Return_Helper_Data::STOCK_REQUEST_DECLINE)){
               if($status_denied ==1){
                return "<span style='background:orange;padding:2px 5px;margin-right:5px;color:#fff;'>P</span>".Mage::helper('opentechiz_return')->getAllStatus()[$status];
               }elseif ($status_denied == 2) {
                   return "<span style='background:green;padding:2px 5px;margin-right:5px;color:#fff;'>A</span>".Mage::helper('opentechiz_return')->getAllStatus()[$status];
               }else{
                    return "<span style='background:red;padding:2px 5px;margin-right:5px;color:#fff;'>D</span>".Mage::helper('opentechiz_return')->getAllStatus()[$status];
               }
            }else{
               return Mage::helper('opentechiz_return')->getAllStatus()[$status];
            }
            
        	

    }
}