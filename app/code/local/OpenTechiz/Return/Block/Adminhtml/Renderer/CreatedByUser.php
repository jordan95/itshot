<?php

class OpenTechiz_Return_Block_Adminhtml_Renderer_CreatedByUser extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $user_id =  $row->getData('created_by');
        $user_name = Mage::helper('opentechiz_return')->getUserName($user_id);
        return $user_name;
    }
}