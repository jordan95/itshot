<?php

class OpenTechiz_Return_Block_Adminhtml_Renderer_RealOrderLink extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {

        	$orderIncrementId =  $row->getData('order_increment_id');
            if($orderIncrementId !=''){
                $order_id =  $row->getData('real_order_id');
                return '<a target="__blank" href="'.Mage::getUrl("adminhtml/sales_order/view", array("order_id" => $order_id)).'">#'.$orderIncrementId.'</a>';
            }
            return '';
        	

    }
}