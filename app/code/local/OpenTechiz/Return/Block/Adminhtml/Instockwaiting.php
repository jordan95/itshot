<?php

class OpenTechiz_Return_Block_Adminhtml_Instockwaiting extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected $_controller = '';
    protected $_blockGroup = '';
    protected $_headerText = '';

    public function __construct()
    {
        $this->_controller = 'adminhtml_instockwaiting';
        $this->_blockGroup = 'opentechiz_return';
        $this->_headerText = Mage::helper('opentechiz_return')->__('InStock Waiting Items');
        parent::__construct();
        $this->_removeButton('add');
    }
}