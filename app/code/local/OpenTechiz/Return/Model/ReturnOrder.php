<?php

class OpenTechiz_Return_Model_ReturnOrder
{
    public function createReturnOrder($params){
        $error = [];
        $error['redirect'] = '';
        $error['message'] = '';

        $data =  $params;
        Mage::register('return_order_create', true);

        try{
            if(isset($data['id'])){
                $return = Mage::getModel('opentechiz_return/return')->load($data['id']);
                //error if already have order created
                if($return->getReturnFeeOrderId()){
                    $error['message'] = 'Return Order already created';
                    return $error;
                }
                $helper = Mage::helper('opentechiz_return');
                $websiteId = 1;
                $customerId = $return->getCustomerId();
                $customer = Mage::getModel('customer/customer')
                    ->setWebsiteId($websiteId)
                    ->load($customerId);

                $quote = Mage::getModel('sales/quote')->setStoreId($websiteId);
                $quote->assignCustomer($customer);
                $quote->setIsSuperMode(true);

                //add return fee product
                if($return->getReturnFee() > 0) {
                    $product = Mage::getModel('opentechiz_return/return')->getProduct();
                    $quoteItem = Mage::getModel('sales/quote_item')->setProduct($product)->setQty(1)
                        ->setCustomPrice($return->getReturnFee())->setOriginalCustomPrice($return->getReturnFee())
                        ->setDescription('Return fee for return #' . $return->getIncrementId());
                    $quote->addItem($quoteItem);
                }

                //add returned product
                $orderItemId = $return->getOrderItemId();
                $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);
                if(!$orderItem || !$orderItem->getId()){
                    throw new Exception($helper->__('Can\'t find order item to add to quote'));
                }

                $productId = $orderItem->getProductId();
                if(!$productId){
                    $sku = $orderItem->getSku();
                    $sku = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[0];
                    $returnedProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                    $productId = $returnedProduct->getId();
                }
                $returnedProduct = Mage::getModel('catalog/product')->load($productId);
                if(!$returnedProduct || !$returnedProduct->getId()){
                    throw new Exception($helper->__('Can\'t find product to add to quote'));
                }

                //for exchange order
                if($return->getSolution() == 20){
                    $options = [];
                    $options['partialpayment'] = 0;
                    if($return->getNewOption()){
                        $newOption = $return->getNewOption();
                        if(is_string($newOption)){
                            $newOption = unserialize($return->getNewOption());
                        }
                        if(isset($newOption['options'])){
                            foreach ($newOption['options'] as $option){
                                $options[$option['option_id']] = $option['option_value'];
                            }
                        }
                    }else{
                        $oldOption = $orderItem->getProductOptions();
                        if(is_string($oldOption)){
                            $oldOption = unserialize($oldOption);
                        }
                        if(isset($oldOption['options'])) {
                            foreach ($oldOption['options'] as $option) {
                                $options[$option['option_id']] = $option['option_value'];
                            }
                        }
                    }
                }
                //for repair order
                else{
                    $options = [];
                    $options['partialpayment'] = 0;
                    if(isset($orderItem->getProductOptions()['info_buyRequest']['options'])){
                        $optionData = $orderItem->getProductOptions()['info_buyRequest']['options'];
                        $optionData['partialpayment'] = 0;
                        $options = $optionData;
                    }else{
                        if(isset($orderItem->getProductOptions()['options'])){
                            foreach ($orderItem->getProductOptions()['options'] as $option){
                                $options[$option['option_id']] = $option['option_value'];
                            }
                        }
                    }
                }
                $buyRequest = array(
                    'qty' => 1,
                    'options' => $options
                );
                $resultItem = $quote->addProduct($returnedProduct, new Varien_Object($buyRequest));
                if(is_string($resultItem)){
                    $quoteItem = Mage::getModel('sales/quote_item')->setProduct($returnedProduct)->setQty(1);
                    $productOptions = $orderItem->getProductOptions();
                    if ($productOptions) {
                        if (is_string($productOptions)) {
                            $productOptions = unserialize($productOptions);
                        }
                        $quoteItem->setOptions($productOptions);
                    }
                    $quote->addItem($quoteItem);
                }

                $quote->save();

                $quoteItem = $quote->getItemByProduct($returnedProduct);
                if(!$quoteItem){
                    foreach ($quote->getAllItems() as $item){
                        if($item->getProduct()->getId() == $returnedProduct->getId()){
                            $quoteItem = $item;
                        }
                    }
                }

                if(!$return->getSolution() == 20){
                    $quoteItem->setSku($orderItem->getSku());
                }
                if($return->getSolution() != 20) {
                    $quoteItem->setOriginalCustomPrice(0);
                    $quoteItem->setCustomPrice(0);
                }
                $quoteItem->setDescription(OpenTechiz_Return_Helper_Data::RETURN_DESCRIPTION_STRING.' for return #'.$return->getIncrementId());
                $quoteItem->setIsSuperMode(true);
                $quoteItem->save();

                //remove partial payment option
                $additionalOptions = array(
                    'label' => 'allow_partial_payment',
                    'value' => 0
                );
                $total = 0;
                foreach ($quote->getAllVisibleItems() as $item) {
                    $item->addOption(array(
                        'code' => 'info_buyRequest',
                        'value' => serialize($additionalOptions)
                    ));
                    $total += $item->getProduct()->getSpecialPrice();
                }
                $quote->save();

                //set billing and shipping adrress
                if($customer->getPrimaryBillingAddress() && $customer->getPrimaryShippingAddress()) {
                    $quote->getBillingAddress()->addData($customer->getPrimaryBillingAddress()->getData());
                    $quote->getShippingAddress()->addData($customer->getPrimaryShippingAddress()->getData());
                }elseif($customer->getPrimaryShippingAddress()){
                    $quote->getBillingAddress()->addData($customer->getPrimaryShippingAddress()->getData());
                    $quote->getShippingAddress()->addData($customer->getPrimaryShippingAddress()->getData());
                }elseif ($customer->getPrimaryBillingAddress()){
                    $quote->getBillingAddress()->addData($customer->getPrimaryBillingAddress()->getData());
                    $quote->getShippingAddress()->addData($customer->getPrimaryBillingAddress()->getData());
                }else{
                    $order = Mage::getModel('sales/order')->load($orderItem->getOrderId());
                    $quote->getBillingAddress()->addData($order->getBillingAddress()->getData());
                    $quote->getShippingAddress()->addData($order->getShippingAddress()->getData());
                }
                $shippingDataFromReturn = [];
                $shippingDataFromReturn['firstname'] = $return->getFirstName();
                $shippingDataFromReturn['lastname'] = $return->getLastName();
                $shippingDataFromReturn['street'] = $return->getStreet();
                $shippingDataFromReturn['city'] = $return->getCity();
                $shippingDataFromReturn['region'] = $return->getRegion();
                $shippingDataFromReturn['postcode'] = $return->getPostcode();
                $shippingDataFromReturn['telephone'] = $return->getPhone();
                $shippingDataFromReturn['email'] = $return->getEmail();
                $quote->getBillingAddress()->addData($shippingDataFromReturn)->save();
                $quote->getShippingAddress()->addData($shippingDataFromReturn)->save();

                //add shipping method
                $address = $quote->getShippingAddress();
                $address->setCountryId('US')->setCollectShippingRates(true)->collectShippingRates();
                $rates = $address->getGroupedAllShippingRates();

                $shippingMethodCode = 'storepickup_storepickup';

                if($total >= 150){
                    $shippingAmount = 0;
                }

                if(isset($rates['matrixrate']) && $return->getSolution() == 20) { #1756 Shipping default for exchange orders
                    foreach ($rates['matrixrate'] as $rate) {
                        if ($rate->getMethodTitle() == 'Ground Shipping') {
                            $code = $rate->getCode();
                            $shippingMethodCode = $code;
                            if(isset($shippingAmount)) {
                                $rate->setPrice($shippingAmount)->save();
                                $address->setShippingMethod($shippingMethodCode)
                                    ->setShippingAmount($shippingAmount)
                                    ->setBaseShippingAmount($shippingAmount)->save();
                            }else{

                            }
                            break;
                        } else {
                            continue;
                        }
                    }
                }

                $address->setShippingMethod($shippingMethodCode)->save();
                $quote->save();

                //add payment method
                $paymentMethod = 'free';
                $quote->setPaymentMethod($paymentMethod);
                $quote->getShippingAddress()->setShouldIgnoreValidation(true);
                $quote->getBillingAddress()->setShouldIgnoreValidation(true);
                $quote->getPayment()->importData(array('method' => $paymentMethod));

                Mage::getSingleton('adminhtml/session_quote')->setCustomerId(-1);
                $quote->collectTotals()->save();

                //recalculate address total
                foreach ($quote->getAllAddresses() as $address) {
                    $address->setSubtotal(0);
                    $address->setBaseSubtotal(0);

                    $address->setGrandTotal(0);
                    $address->setBaseGrandTotal(0);

                    $address->collectTotals()->save();
                }

                // Add custom prefix order Increment Id type return (Fix issue #2950)
                // Don't use observer for this because rewards point will not set. Module TBT_Rewards will validate increment id
                // TBT_Rewards_Model_Observer_Sales_Order_Save_After_Create (_processCatalogTransfers)
                foreach ($quote->getAllItems() as $item){
                    $desctiption = $item->getDescription();
                    if(stripos($desctiption, '#') !== false){
                        $returnIncrementId = substr($desctiption, strpos($desctiption, '#')+1, 30);
                        $return = Mage::getModel('opentechiz_return/return')->load($returnIncrementId, 'increment_id');
                        if($return && $return->getId()){
                            $isRepairSurcharge = true;
                            $solution = $return->getSolution();
                            $orderType = OpenTechiz_Return_Helper_Data::SOLUTION_TO_ORDER_TYPE[$solution];
                            break;
                        }
                    }
                }

                if($isRepairSurcharge){
                    $quote->reserveOrderId();
                    $reserveOrderId = $quote->getReservedOrderId();
                    if($orderType == 2) {
                        $newReservedOrderId = 'R-' . $reserveOrderId;
                    }elseif($orderType == 3){
                        $newReservedOrderId = 'E-' . $reserveOrderId;
                    }
                    $quote->setReservedOrderId($newReservedOrderId);
                }
                // End add prefix order type return

                $service = Mage::getModel('sales/service_quote', $quote);
                $service->submitAll();

                //clear quote
                foreach($quote->getAllItems() as $item) {
                    $quote->removeItem($item->getId());
                }
                $quote->collectTotals()->save();

                //set return as paid
                $order = $service->getOrder();
                $return->setIsPaid(1)->setReturnFeeOrderId($order->getIncrementId())->save();
                $orderId = $order->getId();

                //update link to original order item
                try {
                    $orderItem->setChildOrderIncrementId(trim($orderItem->getChildOrderIncrementId() . ',' . $order->getIncrementId(), ','))->save();
                }catch (Exception $e){
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }

                if($order->getOrderType() == 3) { //#1619 Change workflow for exchange order
                    Mage::register('return_exchange_order_create', true);
                    //Auto-create credit memo for the original item
                    $origOrderId = $orderItem->getOrderId();
                    $origOrder = Mage::getModel('sales/order')->load($origOrderId);
                    $service = Mage::getModel('sales/service_order', $origOrder);
                    $data = array(
                        'qtys' => array(
                            $orderItem->getId() => 1
                        ),
                        'shipping_amount' => 0
                    );
                    $creditMemo = $service->prepareCreditmemo($data)->register()->save();
                    $transactionSave = Mage::getModel('core/resource_transaction')
                        ->addObject($creditMemo)
                        ->addObject($creditMemo->getOrder());
                    $transactionSave->save();
                    //payment record on credit memo
                    //get payment method from payment record in invoice
                    $randomRecord = Mage::getModel('cod/paymentgrid')->getCollection()->addFieldToFilter('order_id', $origOrderId)
                        ->addFieldToFilter('type', 'invoice')->getFirstItem();
                    //payment record
                    $customer_id = $origOrder->getCustomer_id();
                    $customer_row = Mage::getModel('customer/customer')->load($customer_id);
                    $received_date = Mage::getModel('core/date')->date('Y-m-d');
                    $data_arr = array(
                        'increment_id' => 0,
                        'invoice_id' => 0,
                        'payment_id' => $randomRecord->getPaymentId(),
                        'number' => 'Move payment to new exchange order #'.$order->getIncrementId(),
                        'total' => (-1) * (($orderItem->getBaseRowTotal() + $orderItem->getBaseTaxAmount())- $orderItem->getBaseDiscountAmount())/$orderItem->getQtyOrdered(),
                        'billing_name' => $origOrder->getBillingAddress()->getName(),
                        'email' => $customer_row->getEmail(),
                        'received_date' => $received_date,
                        'order_id' => $origOrder->getId(),
                        'credit_memo_id' => $creditMemo->getId(),
                        'type'   => 'creditmemo',
                        'created_time' =>now(),
                        'update_time' =>now(),
                        'user_id' => Mage::helper('opentechiz_payment')->getUserId()
                    );
                    Mage::getModel('cod/paymentgrid')->setData($data_arr)->save();
                    $creditMemo->save();
                    //invoice + payment record on new order from the original item
                    //invoice
                    $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                    $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                    $invoice->register();
                    $invoice->getOrder()->setIsInProcess(true);
                    $transactionSave = Mage::getModel('core/resource_transaction')
                        ->addObject($invoice)
                        ->addObject($invoice->getOrder());
                    $transactionSave->save();
                    //payment record
                    $data_arr = array(
                        'increment_id' => $invoice->getIncrementId(),
                        'invoice_id' => $invoice->getId(),
                        'payment_id' => Mage::getModel('opentechiz_return/rewrite_payment_methodForRecordReturn')->getPaymentRecord()->getId(),
                        'number' => 'Payment moved from order #'.$origOrder->getIncrementId(),
                        'total' => (($orderItem->getBaseRowTotal() + $orderItem->getBaseTaxAmount())- $orderItem->getBaseDiscountAmount())/$orderItem->getQtyOrdered(),
                        'billing_name' => $origOrder->getBillingAddress()->getName(),
                        'email' => $customer_row->getEmail(),
                        'received_date' => $received_date,
                        'credit_memo_id' => 0,
                        'order_id' => $order->getId(),
                        'type'   => 'invoice',
                        'created_time' =>now(),
                        'update_time' =>now(),
                        'user_id' => Mage::helper('opentechiz_payment')->getUserId()
                    );
                    Mage::getModel('cod/paymentgrid')->setData($data_arr)->save();

                    //add credit memo if price lower
                    $paid = $orderItem->getBaseRowTotalInclTax()/$orderItem->getQtyOrdered();
                    $newTotal = $order->getBaseGrandTotal();
                    if($newTotal < $paid){
                        $diff = abs($newTotal - $paid);
                        $data = array(
                            'qtys' => [],
                            'shipping_amount' => 0
                        );
                        foreach ($order->getAllItems() as $newOrderItem){
                            $data['qtys'][$newOrderItem->getId()] = 0;
                        }
                        $service = Mage::getModel('sales/service_order', $order);
                        $creditMemo = $service->prepareCreditmemo($data)->register()->save();
                        $creditMemo->setGrandTotal($diff)->setBaseGrandTotal($diff)->save();
                    }
                }
                $error['order_id'] = $orderId;
            }else{
                $error['message'] = Mage::helper('core')->__('Return not found');
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->setFormData($data);
            $error['message'] = Mage::helper('core')->__($e->getMessage());
        }

        return $error;
    }
}