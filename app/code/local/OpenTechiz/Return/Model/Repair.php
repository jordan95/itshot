<?php

class OpenTechiz_Return_Model_Repair extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_return/repair', 'id');
    }
}