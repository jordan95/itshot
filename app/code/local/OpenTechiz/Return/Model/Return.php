<?php

class OpenTechiz_Return_Model_Return extends Mage_Core_Model_Abstract
{
    const RETURN_FEE_PRODUCT_SKU = OpenTechiz_Return_Helper_Data::RETURN_FEE_ITEM_SKU;
    const RETURN_ITEM_DESCRIPTION = 'return_item';

    protected function _construct()
    {
        $this->_init('opentechiz_return/return', 'return_id');
    }

    public function generatedIncrementId($id){
        if($this->getSolution() || $this->getReturnType()){
            $admin = Mage::getSingleton('admin/session')->getUser();
            $name = $admin->getFirstname().' '.$admin->getLastname();
            $words = explode(" ", $name);
            $acronym = "";
            foreach ($words as $w) {
                $acronym .= $w[0];
            }
            $initials = strtoupper($acronym);

            $orderItemId = $this->getOrderItemId();
            $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);
            $order = Mage::getModel('sales/order')->load($orderItem->getOrderId());

            if($this->getSolution()) {
                $prefix = OpenTechiz_Return_Helper_Data::INCREMENT_ID_PREFIX_BY_SOLUTION[$this->getSolution()];
            } elseif($this->getReturnType()){
                $prefix = OpenTechiz_Return_Helper_Data::INCREMENT_ID_PREFIX_BY_TYPE[$this->getReturnType()];
            }
            $id = $prefix.'-'.$initials.'-'.$order->getIncrementId();
        } else {
            $id = 'RT' . $id;
        }

        //check if id exist and modify
        $returnWithSimilarId = Mage::getModel('opentechiz_return/return')->getCollection()
            ->addFieldToFilter('increment_id', array('like' => $id.'%'));
        $similarIds = [];
        foreach ($returnWithSimilarId as $return){
            $similarIds[] = $return->getIncrementId();
        }
        $count = 0;
        while(in_array($id, $similarIds)){
            $count++;
            $id .= '-'.$count;
        }

        return $id;
    }

    protected function _afterLoad()
    {
        parent::_afterLoad();
        $this->setOldData($this->getData());

    }

    protected function _afterSave(){
        parent::_afterSave();
        $old_data = $this->getOldData();
        $check_disable_send_mail = $this->disableEmailReturn();
        if($check_disable_send_mail){
            return;
        }
        if(isset($old_data['status']) && $old_data['status'] != $this->getData('status') ) {
            $customer = Mage::getModel('customer/customer')->load($this->getCustomerId());

            // Set sender information
            $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
            $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

            // Getting recipient E-Mail
            $emailTemplate = Mage::getModel('core/email_template');
            $emailTemplate->loadDefault('return_noty_email_tpl');
            $emailTemplate->setTemplateSubject(Mage::app()->getStore()->getName() . ': Return Status Notification');

            $emailTemplate->setSenderName($senderName);
            $emailTemplate->setSenderEmail($senderEmail);

            $emailTemplateVariables = array();
            $emailTemplateVariables['customer_name'] = $customer->getName();
            $emailTemplateVariables['increment_id'] = $this->getIncrementId();
            $emailTemplateVariables['status'] = Mage::helper('opentechiz_return')->getReturnStatus($this->getStatus());


            $emailTemplate->send($customer->getEmail(), 'Admin', $emailTemplateVariables);
        }
    }

    public function createNewQuote($returnId, $customer){
        $helper = Mage::helper('opentechiz_return');

        if (!$returnId) {
            throw new Exception($helper->__('Can not create quote for empty surcharge id'));
        }

        $checkoutSession = Mage::getSingleton('checkout/session');

        if (!$customer) {
            $customer = $checkoutSession->getCustomer();
        }

        $model = Mage::getModel('opentechiz_return/return');
        $return = $model->load($returnId);
        if(!$return->getId()){
            throw new Exception($helper->__('Invalid return object'));
        }

        $cart = Mage::getSingleton('checkout/cart');
        $cart->init();
        $cart->truncate();

//        $currentQuote = Mage::getSingleton('checkout/session')->getQuote();
//        $quoteItems = $currentQuote->getItemsCollection();
//        foreach( $quoteItems as $item ){
//            $cart->removeItem($item->getId());
//        }

        //add return fee virtual
        $product = $this->getProduct();
        $cart->addProduct($product->getId());

        //add returned product
        $orderItemId = $return->getOrderItemId();
        $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);
        if(!$orderItem || !$orderItem->getId()){
            throw new Exception($helper->__('Can\'t find order item to add to quote'));
        }

        $returnedProduct = Mage::getModel('catalog/product')->load($orderItem->getProductId());
        if(!$returnedProduct || !$returnedProduct->getId()){
            throw new Exception($helper->__('Can\'t find product to add to quote'));
        }

        if($return->getSolution() == 20){
            $options = [];
            $options['partialpayment'] = 0;
            if($return->getNewOption() && is_string($return->getNewOption())){
                $newOption = unserialize($return->getNewOption());
                if(isset($newOption['options'])){
                    foreach ($newOption['options'] as $option){
                        $options[$option['option_id']] = $option['option_value'];
                    }
                    $cart->addProduct($returnedProduct ,
                        array( 'product_id' => $returnedProduct->getId(),
                            'qty' => 1,
                            'options' => $options
                        ));
                }
            }
        }else {
            $options = $orderItem->getProductOptions();
            if ($options == '') {
                $cart->addProduct($returnedProduct->getId());
            } else {
                if (!is_array($options)) $options = unserialize($options);
                $productOptions = $options['info_buyRequest']['options'];
                $cart->addProduct($returnedProduct,
                    array('product_id' => $returnedProduct->getId(),
                        'qty' => 1,
                        'options' => $productOptions
                    ));
            }
        }

        $cart->save();
        $this->modifyPrice($product->getId(), $return->getReturnFee(), $product->getName() . ' for return #'.$return->getIncrementId());
        $this->modifyPrice($returnedProduct->getId(), 0, $product->getName());
        Mage::getSingleton('checkout/session')->setCartWasUpdated(true);

        return $cart->getQuote();
    }

    private function modifyPrice($product_id, $newPrice, $name)
    {
        //get quote
        $quote = Mage::getSingleton('checkout/session')->getQuote();

        foreach ($quote->getAllVisibleItems() as $item) {
            //for specific product
            if ($item->getProduct()->getId() != $product_id) continue;

            $item->setCustomPrice($newPrice)->setName($name)->setDescription($name);
            if($newPrice == 0) $item->setDescription(self::RETURN_ITEM_DESCRIPTION);
            $item->setOriginalCustomPrice($newPrice);
            $item->getProduct()->setIsSuperMode(true);
        }
        $quote->save();
    }

    public function sendNotiMail(){
        $check_disable_send_mail = $this->disableEmailReturn();
        if($check_disable_send_mail){
            return;
        }
        $customer = Mage::getModel('customer/customer')->load($this->getCustomerId());
        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('return_noty_pay_email_tpl');
        $emailTemplate->setTemplateSubject(Mage::app()->getStore()->getName() . ': Return Status Notification');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();
        $emailTemplateVariables['customer_name'] = $customer->getName();
        $emailTemplateVariables['increment_id'] = $this->getIncrementId();
        $emailTemplateVariables['payment_link'] = Mage::getStoreConfig(Mage_Core_Model_Url::XML_PATH_SECURE_URL).'returns/return/';

        $emailTemplate->send($customer->getEmail(), 'Admin', $emailTemplateVariables);
    }

    public function sendItemProblemMail(){
        $check_disable_send_mail = $this->disableEmailReturn();
        if($check_disable_send_mail){
            return;
        }
        $customer = Mage::getModel('customer/customer')->load($this->getCustomerId());
        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        // Getting recipient E-Mail
        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('return_item_problem_email_tpl');
        $emailTemplate->setTemplateSubject(Mage::app()->getStore()->getName() . ': Return Item State Problem');

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = array();
        $emailTemplateVariables['customer_name'] = $customer->getName();
        $emailTemplateVariables['increment_id'] = $this->getIncrementId();
        $emailTemplateVariables['payment_link'] = Mage::getStoreConfig(Mage_Core_Model_Url::XML_PATH_SECURE_URL).'returns/return/';

        $emailTemplate->send($customer->getEmail(), 'Admin', $emailTemplateVariables);
    }

    public function getProduct()
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product');
        $productId = $product->getIdBySku(self::RETURN_FEE_PRODUCT_SKU);
        if ($productId) {
            return $product->load($productId);
        }

        $websites = Mage::getModel('core/website')->getCollection()->toArray(array('website_id'));
        $websitesIds = array();
        if (!empty($websites['items'])) {
            foreach ($websites['items'] as $website) {
                $websitesIds[] = $website['website_id'];
            }
        } else {
            $websitesIds[] = 1;
        }

        try {
            $product
                ->setStoreId(0)
                ->setWebsiteIds($websitesIds)
                ->setAttributeSetId(Mage::getModel('catalog/product')->getDefaultAttributeSetId())
                ->setTypeId('virtual')
                ->setCreatedAt(strtotime('now'))
                ->setUpdatedAt(strtotime('now'))
                ->setSku(self::RETURN_FEE_PRODUCT_SKU)
                ->setName('Return Fee')
                ->setWeight(0)
                ->setStatus(1)
                ->setTaxClassId(0)
                ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE)
                ->setPrice(0)
                ->setCost(0)
                ->setDescription('Virtual Surcharge For Returned Product')
                ->setShortDescription('Virtual Surcharge For Returned Product')
                ->setStockData(array(
                        'use_config_manage_stock' => 0,
                        'manage_stock' => 0,
                        'min_sale_qty' => 1,
                        'max_sale_qty' => 1,
                        'is_in_stock' => 1,
                        'qty' => 999
                    )
                );
            $product->save();
        } catch (Exception $e) {
            Mage::log($e->getMessage());
            throw $e;
        }

        return $product;
    }
    protected function disableEmailReturn(){
        $check_disable_send_mail = Mage::getStoreConfig('opentechizreturn/general/enable');
        if($check_disable_send_mail){
            return true;
        }else{
            return false;
        }
    }
}