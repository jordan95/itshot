<?php

class OpenTechiz_Return_Model_Observer
{

    public function updateReturn($observer)
    {
        $order = $observer->getEvent()->getOrder();
        $quote = Mage::getModel('sales/quote')->load($order->getQuoteId());
        if ($quote->getId()) {
            foreach ($quote->getAllVisibleItems() as $item) {
                $description = $item->getDescription();
                if (strpos($description, '#') !== false) {
                    $returnId = substr($description, strpos($description, "#") + 1);
                    $return = Mage::getModel('opentechiz_return/return')->load($returnId, 'increment_id');
                    if ($return && $return->getId()) {
                        $return->setIsPaid(1)->save();

                        $resource = Mage::getSingleton('core/resource');
                        $writeConnection = $resource->getConnection('core_write');
                        $table = $resource->getTableName('opentechiz_return/return');
                        $query = "UPDATE {$table} SET return_fee_order_id = '" . $order->getIncrementId() . "' WHERE return_id = " . $return->getId();
                        $writeConnection->query($query);

                        //add repair surcharge
                        $orderItem = Mage::getModel('sales/order_item')->load($return->getOrderItemId());
                        if ($orderItem && $orderItem->getId()) {
                            $parentOrder = Mage::getModel('sales/order')->load($orderItem->getOrderId());
                            $this->addRepairSurchargeOrder($order, $parentOrder, $return);
                        }
                    }
                }
            }
        }
    }

    public function addRepairSurchargeOrder($currentOrder, $parentOrder, $return)
    {
        $surchargeOrder = Mage::getModel('mageworx_orderssurcharge/surcharge');
        $data = [];
        $data['store_id'] = Mage::app()->getStore()->getId();
        $data['customer_id'] = $return->getCustomerId();
        $data['customer_email'] = $return->getEmail();
        $data['customer_is_guest'] = 0;
        $data['parent_order_id'] = $parentOrder->getId();
        $data['parent_order_increment_id'] = $parentOrder->getIncrementId();
        $data['order_id'] = $currentOrder->getId();
        $data['order_increment_id'] = $currentOrder->getIncrementId();
        $data['status'] = 3;
        $data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $data['updated_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $data['base_total'] = $currentOrder->getGrandTotal();
        $data['base_total_due'] = 0;
        $surchargeOrder->setData($data)->save();
    }

    public function refundEarningPoints(Varien_Event_Observer $observer)
    {
        /* @var $creditmemoItem Mage_Sales_Model_Order_Creditmemo_Item */
        $creditmemoItem = $observer->getCreditmemoItem();
        if (!$creditmemoItem->isObjectNew()) {
            return;
        }
        /* @var $orderItem Mage_Sales_Model_Order_Item */
        $orderItem = $creditmemoItem->getOrderItem();
        $order = $orderItem->getOrder();
        if (!$order->hasPointsEarning()) {
            return;
        }
        $defaultCurrencyId = Mage::helper('rewards/currency')->getDefaultCurrencyId();
        // Earn points
        $earnedPointsOnItem = Mage::helper('rewards/transfer')->getEarnedPointsOnItem($orderItem);
        if (empty($earnedPointsOnItem[$defaultCurrencyId])) {
            return;
        }
        // Reward Points
        $this->_getAdjuster()
                ->setOrder($order);
        $message = [];
        $totalEarnedPoints = $order->getTotalEarnedPoints();
        $refundEarnedPoints = $earnedPointsOnItem[$defaultCurrencyId] / $orderItem->getQtyOrdered();
        $adjustedEarned = $totalEarnedPoints[$defaultCurrencyId] - floor($refundEarnedPoints * $creditmemoItem->getQty());
        $this->_getAdjuster()->setAdjustedEarned($adjustedEarned);
        $message[] = sprintf("%d earning points", $refundEarnedPoints);
        Mage::dispatchEvent('rewards_adjust_points_init_before', array('adjuster' => $this->_getAdjuster()));
        $this->_getAdjuster()->init();
        $this->_getAdjuster()
                ->setTransferComments(sprintf("Refund %s from qty: %d of order item: #%s", join($message, " and "), $creditmemoItem->getQty(), $orderItem->getSku()))
                ->execute();
    }

    public function refundStoreCredit(Varien_Event_Observer $observer)
    {
        /* @var $creditmemo Mage_Sales_Model_Order_Creditmemo */
        $creditmemo = $observer->getCreditmemo();
        $baseCreditAmount = $creditmemo->getBaseCreditAmount();
        if ($baseCreditAmount <= 0) {
            return;
        }
        $order = $creditmemo->getOrder();
        $email = $order->getCustomerEmail();
        if (!$order->getCustomerEmail()) {
            return;
        }
        $message = "Creditmemo #m|" . $creditmemo->getIncrementId();
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $select = $read->select()
                ->from($resource->getTableName('credit/transaction', array('transaction_id')));
        $select->where("message = ?", $message);
        $result = $read->fetchOne($select);
        if($result) {
            return;
        }
        $customer = Mage::getModel('customer/customer')->setWebsiteId(1)->loadByEmail($email);
        if (!$customer->getId()) {
            return;
        }
        $balance = Mage::getModel('credit/balance')->loadByCustomer($customer->getId());
        $balance->addTransaction(
                $baseCreditAmount, Mirasvit_Credit_Model_Transaction::ACTION_REFUNDED, $message
        );
    }

    /**
     * @return TBT_Rewards_Model_Sales_Order_Transfer_Adjuster
     */
    protected function _getAdjuster()
    {
        return Mage::getSingleton('rewards/sales_order_transfer_adjuster');
    }

}
