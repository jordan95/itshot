<?php

class OpenTechiz_Return_Model_Resource_Repair_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_return/repair');
    }
}