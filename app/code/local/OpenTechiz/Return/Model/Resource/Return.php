<?php

class OpenTechiz_Return_Model_Resource_Return extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_return/return', 'return_id');
    }
}