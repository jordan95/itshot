<?php

class OpenTechiz_Return_Model_Solution
{
    const REVERSE_PAYMENT_RECORD_NOTE = 'Reverse payment record to cancel return order';
    /**
     * @param $return
     * @param @newSolution
     *
     * @return string $error
     */
    public function changeReturnSolution($return, $newSolution){
        $error = '';
        $currentSolution = $return->getSolution();

        //skip if select return the first time
        if(!$currentSolution || $currentSolution == 0 || $currentSolution == $newSolution){
            $error = 'skipped';
        }

        //cannot change from refunded to repair/ exchange
        if($currentSolution == OpenTechiz_Return_Helper_Data::REFUND && $newSolution != OpenTechiz_Return_Helper_Data::REFUND){
            $error = 'Cannot change solution from Refunded to Repair/Exchange';
        }

        //change from repair to exchange
        if($currentSolution == OpenTechiz_Return_Helper_Data::REPAIRITEM && $newSolution == OpenTechiz_Return_Helper_Data::EXCHANGEFROMSTOCK){
            if($return->getReturnFeeOrderId()){
                $repairOrder = Mage::getModel('sales/order')->load($return->getReturnFeeOrderId(), 'increment_id');
                //cancel repair order
                $error = $this->cancelReturnOrder($repairOrder);
                if($error != '') return $error;
                //remove repair from repair order, keep the process, return to stock when done
                $barcode = $return->getItemBarcode();
                $repairProcess = Mage::getModel('opentechiz_production/product')->load($barcode, 'barcode');
                $data = [];
                $data['order_item_id'] = null;
                $data['production_type'] = 4;
                $repairProcess->addData($data)->save();
//                //delete repair return
//                $repair = Mage::getModel('opentechiz_return/repair')->load($return->getId(), 'return_id');
//                $repair->delete();
                //set return to exchange
                $return->setSolution(OpenTechiz_Return_Helper_Data::EXCHANGEFROMSTOCK);
                //set return type
                $return->setReturnType(OpenTechiz_Return_Helper_Data::EXCHANGE);
                //remove old return order id
                $return->setReturnFeeOrderId(null);
                //save
                $return->save();
                //set increment id to new solution
                $return->setIncrementId($return->generatedIncrementId($return->getId()));
                //save
                $return->save();
                //create new exchange order
                $data = [];
                $data['id'] = $return->getId();
                $resultCreateOrder = Mage::getModel('opentechiz_return/returnOrder')->createReturnOrder($data);
                if($resultCreateOrder['message']){
                    return $resultCreateOrder['message'];
                }
                //set new return fee order id to return
                $order = Mage::getModel('sales/order')->load($resultCreateOrder['order_id']);
                $return->setReturnFeeOrderId($order->getIncrementId());
                $return->save();
            }else{
                //remove repair from repair order, keep the process, return to stock when done
                $barcode = $return->getItemBarcode();
                $repairProcess = Mage::getModel('opentechiz_production/product')->load($barcode, 'barcode');
                $data = [];
                $data['order_item_id'] = null;
                $data['production_type'] = 4;
                $repairProcess->addData($data)->save();
                //change solution
                $return->setSolution(OpenTechiz_Return_Helper_Data::EXCHANGEFROMSTOCK);
                //set return type
                $return->setReturnType(OpenTechiz_Return_Helper_Data::EXCHANGE);
                //save
                $return->save();
                //set increment id to new solution
                $return->setIncrementId($return->generatedIncrementId($return->getId()));
                //save
                $return->save();
//                //delete repair return
//                $repair = Mage::getModel('opentechiz_return/repair')->load($return->getId(), 'return_id');
//                $repair->delete();
            }
        }

        //change from exchange to repair
        if($currentSolution == OpenTechiz_Return_Helper_Data::EXCHANGEFROMSTOCK && $newSolution == OpenTechiz_Return_Helper_Data::REPAIRITEM){
            $origOrderItem = Mage::getModel('sales/order_item')->load($return->getOrderItemId());
            if($origOrderItem){
                $inventoryItems = Mage::getModel('opentechiz_inventory/item')->getCollection()
                    ->addFieldToFilter('sku', $origOrderItem->getSku())
                    ->addFieldToFilter('state', 35);
                if(count($inventoryItems)){
                    $itemToBeRepair = $inventoryItems->getFirstItem();
                }else{
                    return 'No item in stock to create a new repair process.';
                }
            }else{
                return 'Original order item (id '. $return->getOrderItemId() .' ) not found';
            }
            if($return->getReturnFeeOrderId()) {
                //cancel exchange order
                $exchangeOrder = Mage::getModel('sales/order')->load($return->getReturnFeeOrderId(), 'increment_id');
                $itemIds = [];
                foreach ($exchangeOrder->getAllItems() as $item){
                    array_push($itemIds, $item->getId());
                }
                $error = $this->cancelReturnOrder($exchangeOrder);
                if($error != '') return $error;
                //return new item to stock
                $linkCollection = Mage::getModel('opentechiz_inventory/order')->getCollection()
                    ->addFieldToFilter('order_item_id', $itemIds);
                foreach ($linkCollection as $link){
                    $item = Mage::getModel('opentechiz_inventory/item')->load($link->getItemId());
                    $item->setState(30)->save();
                }
            }
            //set return to repair
            $return->setSolution(OpenTechiz_Return_Helper_Data::REPAIRITEM);
            //set return type
            $return->setReturnType(OpenTechiz_Return_Helper_Data::REPAIR);
            //remove old return order id
            $return->setReturnFeeOrderId(null);
            //save
            $return->save();
            //set increment id to new solution
            $return->setIncrementId($return->generatedIncrementId($return->getId()));
            //save
            $return->save();
            //create new repair order on stage fulfilment
            $data = [];
            $data['id'] = $return->getId();
            $resultCreateOrder = Mage::getModel('opentechiz_return/returnOrder')->createReturnOrder($data);
            if($resultCreateOrder['message']){
                return $resultCreateOrder['message'];
            }
            $return = Mage::getModel('opentechiz_return/return')->load($return->getId());
            $repairOrder = Mage::getModel('sales/order')->load($return->getReturnFeeOrderId(), 'increment_id');
            foreach ($repairOrder->getAllItems() as $repairOrderItem){
                if(strpos($repairOrderItem->getDescription(), OpenTechiz_Return_Helper_Data::RETURN_DESCRIPTION_STRING) !== false){
                    $repairOrderItemId = $repairOrderItem->getId();
                }
            }
            Mage::helper('opentechiz_salesextend')->updateStageAndStatus($repairOrder, 2, 17, true);

            //add item from stock to repair process with process: quality control, finished
            if(isset($itemToBeRepair) && isset($repairOrderItemId)){
                $production_data = array();
                $production_data['note'] = 'return '.$return->getIncrementId(). 'changed from exchange to repair';

                $production_data['item_state'] = 0;
                $production_data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');

                $production_data['barcode'] = $itemToBeRepair->getBarcode();
                $production_data['image'] = $itemToBeRepair->getImage();
                $production_data['production_type'] = 2;
                $production_data['order_item_id'] = $repairOrderItemId;
                $production_data['option'] = $itemToBeRepair->getOptions();

                $repairStep = '["","Quality Control","Finished"]';

                $step_ar = json_decode($repairStep,true);
                $status = array_search ($step_ar[1], OpenTechiz_Production_Helper_Data::PROCESS_LIST);
                $production_data['status'] = $status;

                $productRequest = Mage::getModel('opentechiz_production/product');
                $productRequest->setData($production_data)->save();

                $repair_data = ['item_barcode'=>$itemToBeRepair->getBarcode(),
                                'repair_step' => $repairStep,
                                'production_id'=>$productRequest->getId(),
                                'return_id' => $return->getId() ];


                //log stock
                $stock = Mage::getModel('opentechiz_inventory/instock')->load($itemToBeRepair->getSku(), 'sku');
                if($stock){
                    Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                        'qty' => '-1',
                        'on_hold' => 0,
                        'type' => 11,
                        'stock_id' => $stock->getId(),
                        'qty_before_movement' => $stock->getQty(),
                        'on_hold_before_movement' => $stock->getOnHold(),
                        'sku' => $stock->getSku(),
                        'order_id' => $repairOrder->getId(),
                    ));
                }

                $itemToBeRepair->setState(1)->save();

                $repairModel = Mage::getModel('opentechiz_return/repair');
                $repairModel->setData($repair_data)->save();

                //link item to new repair order
                $linkData = [];
                $linkData['order_item_id'] = $repairOrderItemId;
                $linkData['item_id'] = $itemToBeRepair->getId();
                Mage::getModel('opentechiz_inventory/order')->setData($linkData)->save();
            }
        }

        //change from repair to refunded
        if($currentSolution == OpenTechiz_Return_Helper_Data::REPAIRITEM && $newSolution == OpenTechiz_Return_Helper_Data::REFUND){
            if($return->getReturnFeeOrderId()){
                $repairOrder = Mage::getModel('sales/order')->load($return->getReturnFeeOrderId(), 'increment_id');
                //remove repair from repair order, keep the process, return to stock when done
                $barcode = $return->getItemBarcode();
                $repairProcess = Mage::getModel('opentechiz_production/product')->load($barcode, 'barcode');
                $repairProcess->setOrderItemId(null)->setProductionType(4)->save();
                //cancel repair order
                $error = $this->cancelReturnOrder($repairOrder);
                if($error != '') return $error;
//                //delete repair return
//                $repair = Mage::getModel('opentechiz_return/repair')->load($return->getId(), 'return_id');
//                $repair->delete();
            }
            //refund orig order
            $error = $this->refundOrigOrder($return);
            if($error != ''){
                return $error;
            }
            //remove repair from repair order, keep the process, return to stock when done
            $barcode = $return->getItemBarcode();
            $repairProcess = Mage::getModel('opentechiz_production/product')->load($barcode, 'barcode');
            $data = [];
            $data['order_item_id'] = null;
            $data['production_type'] = 4;
            $repairProcess->addData($data)->save();
            //set return to refunded
            $return->setSolution(OpenTechiz_Return_Helper_Data::REFUND);
            //set return type
            $return->setReturnType(OpenTechiz_Return_Helper_Data::WITHDRAWAL);
            $return->setReturnFeeOrderId(null);
            //save
            $return->save();
            //set increment id to new solution
            $return->setIncrementId($return->generatedIncrementId($return->getId()));
            //save
            $return->save();
        }


        //change from exchange to refunded
        if($currentSolution == OpenTechiz_Return_Helper_Data::EXCHANGEFROMSTOCK && $newSolution == OpenTechiz_Return_Helper_Data::REFUND){
            if($return->getReturnFeeOrderId()) {
                //return new item to stock
                $exchangeOrder = Mage::getModel('sales/order')->load($return->getReturnFeeOrderId(), 'increment_id');
                $itemIds = [];
                foreach ($exchangeOrder->getAllItems() as $item){
                    array_push($itemIds, $item->getId());
                }
                $linkCollection = Mage::getModel('opentechiz_inventory/order')->getCollection()
                    ->addFieldToFilter('order_item_id', $itemIds);
                foreach ($linkCollection as $link){
                    $item = Mage::getModel('opentechiz_inventory/item')->load($link->getItemId());
                    $item->setState(30)->save();
                }
                //cancel exchange order
                $error = $this->cancelReturnOrder($exchangeOrder);
                if($error != '') return $error;

            }

            //set return to refunded
            $return->setSolution(OpenTechiz_Return_Helper_Data::REFUND);
            //set return type
            $return->setReturnType(OpenTechiz_Return_Helper_Data::WITHDRAWAL);
            $return->setReturnFeeOrderId(null);
            //save
            $return->save();
            //set increment id to new solution
            $return->setIncrementId($return->generatedIncrementId($return->getId()));
            //save
            $return->save();
        }

        return $error;
    }

    public function removeAllPaymentRecord($order){
        $paymentRecordCollection = Mage::getModel('cod/paymentgrid')->getCollection()
            ->addFieldToFilter('order_id', $order->getId());

        foreach ($paymentRecordCollection as $paymentRecord){
            $total = $paymentRecord->getTotal();
            $reverseTotal = 0 - $total;
            $data = $paymentRecord->getData();
            unset($data['payment_grid_id']);
            Mage::getModel('cod/paymentgrid')->setData($data)
                ->setTotal($reverseTotal)
                ->setNumber(self::REVERSE_PAYMENT_RECORD_NOTE)->save();
        }
    }

    public function cancelReturnOrder($order){
        $error = '';
        $this->removeAllPaymentRecord($order);
        foreach ($order->getInvoiceCollection() as $invoice){
            $invoice->cancel();
        }
        try{
            $order->setState('canceled')->setStatus('canceled')->save();
        }catch (Exception $e){
            $error = $e;
            return $error;
        }
        return $error;
    }

    public function refundOrigOrder($return){
        $order_item = Mage::getModel('sales/order_item')->load($return->getOrderItemId());
        $order = Mage::getModel('sales/order')->load($order_item->getOrderId());

        $note = 'Refund from return '.$return->getIncrementId();
        if($order->canCreditmemo()){
            try {
                if($order_item) {
                    if($order_item->canRefund()){
                        $service = Mage::getModel('sales/service_order', $order);

                        //normal partial refund with custom amount
                        $order_item_amount = $order_item->getRowTotal() + $order_item->getTaxAmount() + $order_item->getHiddenTaxAmount() + Mage::helper('weee')->getRowWeeeAmountAfterDiscount($order_item) - $order_item->getDiscountAmount();
                        $shipping_amount = 0;
                        $adjustment_amount = $adjustment_negative = 0;
                        $baserefund = ($order_item_amount / $order_item->getQtyOrdered()) + $shipping_amount;
                        if($return->getRefundAmount() > $baserefund ){
                            $adjustment_amount = $return->getRefundAmount() - $baserefund;
                        }
                        if($return->getRefundAmount() <= $baserefund){
                            $adjustment_negative =  $baserefund - $return->getRefundAmount();
                        }
                        $data = array(
                            'qtys' =>array( $order_item->getId() => 1),
                            'adjustment_positive' => $adjustment_amount,
                            'shipping_amount' => $shipping_amount,
                            'adjustment_negative' => $adjustment_negative

                        );

                        $creditMemo = $service->prepareCreditmemo($data);
                        $creditMemo->register();
                        $creditMemo->save();

                        $transactionSave = Mage::getModel('core/resource_transaction')
                            ->addObject($creditMemo)
                            ->addObject($creditMemo->getOrder());
                        $transactionSave->save();

                        $item = Mage::getModel('opentechiz_inventory/item')->load($return->getItemBarcode(), 'barcode');

                        if($item && $item->getState()!=45) $item->setState(30)->setNote($note)->save();
                        $return->setStatus(10)->save();

                        return '';
                    }else {

                        return Mage::helper('core')->__('Order item cannot refunded!');
                    }
                } else {
                    return Mage::helper('core')->__('Order item not found to credit memo!');
                }
            } catch (Exception $e) {
                return Mage::helper('core')->__('Order cannot create credit memo!');
            }
        }else{
            return Mage::helper('core')->__('Order cannot create credit memo!');
        }
    }
}