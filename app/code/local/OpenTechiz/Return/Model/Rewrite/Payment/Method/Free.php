<?php
class OpenTechiz_Return_Model_Rewrite_Payment_Method_Free extends Mage_Payment_Model_Method_Free
{
    /**
     * Check whether method is available
     *
     * @param Mage_Sales_Model_Quote|null $quote
     * @return bool
     */
    public function isAvailable($quote = null)
    {
        $checkPrice = Mage::app()->getStore()->roundPrice($quote->getGrandTotal()) == 0;
        foreach ($quote->getAllVisibleItems()as $item){
            if(strpos($item->getDescription(), OpenTechiz_Return_Helper_Data::RETURN_DESCRIPTION_STRING) !== false) {
                $checkPrice = true;
                break;
            }
        }

        return Mage_Payment_Model_Method_Abstract::isAvailable($quote) && !empty($quote)
            && $checkPrice;
    }
}