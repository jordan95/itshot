<?php
class OpenTechiz_Return_Model_Rewrite_Payment_MethodForRecordReturn
{
    const payment_record_name = 'Offline Refund';
    public function getPaymentRecord(){
        $name = self::payment_record_name;
        $paymentMethodCollection = Mage::getModel('cod/payment')->getCollection()
            ->addFieldToFilter('name', $name);
        if (count($paymentMethodCollection) > 0) {
            $paymentMethod = $paymentMethodCollection->getFirstItem();
        } else {
            $data = [];
            $data['name'] = $name;
            $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $paymentMethod = Mage::getModel('cod/payment')->setData($data)->save();
        }
        return $paymentMethod;
    }
}