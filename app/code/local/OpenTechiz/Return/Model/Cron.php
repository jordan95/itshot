<?php
class OpenTechiz_Return_Model_Cron
{
    public function cancelExpiredExchangeOrder(){
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $currentTime = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $dateCheckFrom = date('Y-m-d H:i:s', strtotime('-45 day', strtotime($currentTime)));

        $exchangeOrderCollection = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('order_type', 3)
            ->addFieldToFilter('order_stage', 0)
            ->addFieldToFilter('created_at', array('to' => $dateCheckFrom))
            ->addFieldToFilter('status', array('nin' => OpenTechiz_ReportExtend_Helper_Data::EXCLUDED_ORDER_STATUS));

        foreach ($exchangeOrderCollection as $order){
            $orderIncrement = $order->getIncrementId();
            $return = Mage::getModel('opentechiz_return/return')->load($orderIncrement, 'return_fee_order_id');
            if($return && $return->getId()){
                $itemBarcode = $return->getItemBarcode();
                $returnedOrderItemId = $return->getOrderItemId();
                $returnedOrderItem = Mage::getModel('sales/order_item')->load($returnedOrderItemId);
                $returnedOrder = Mage::getModel('sales/order')->load($returnedOrderItem->getOrderId());

                //cancel exchange order
                $order->setState('canceled')->setStatus('canceled')->save();

                //remove refunded_qty on order item
                $qtyRefunded = $returnedOrderItem->getQtyRefunded() - 1;
                if($qtyRefunded < 0) $qtyRefunded = 0;
                $returnedOrderItem->setQtyRefunded($qtyRefunded)->save();

                //add (-) payment record to creditmemo to undo existing payment record for that order item
                $query = "Select * from tsht_sales_flat_creditmemo_item join tsht_sales_flat_creditmemo
                          on tsht_sales_flat_creditmemo_item.parent_id = tsht_sales_flat_creditmemo.entity_id
                          where tsht_sales_flat_creditmemo_item.order_item_id = ".$returnedOrderItem->getId();
                $results = $read->fetchAll($query);
                if(count($results)) {
                    $creditmemoData = $results[0];
                    $creditmemoId = $creditmemoData['parent_id'];
                    $creditmemoRowTotal = $creditmemoData['base_row_total'];
                    $originalOrderId = $returnedOrder->getId();

                    $refundRecord = Mage::getModel('cod/paymentgrid')->getCollection()
                        ->addFieldToFilter('order_id', $originalOrderId)
                        ->addFieldToFilter('number', array('like' => '%'.$order->getIncrementId().'%'))
                        ->getFirstItem();

                    $data = [];
                    $data['credit_memo_id'] = $creditmemoId;
                    $data['payment_id'] = $refundRecord->getPaymentId();
                    $data['total'] = (-1)*$refundRecord->getTotal();
                    $data['created_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                    $data['update_time'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                    $data['order_id'] = $originalOrderId;
                    $data['type'] = 'creditmemo';
                    $data['number'] = 'Cancel Expired Exchange Order'; //note
                    $data['received_date'] = Mage::getModel('core/date')->gmtDate('Y-m-d');
                    $data['email'] = $returnedOrder->getCustomerEmail();
                    $data['billing_name'] = $returnedOrder->getCustomerFirstname().' '.$returnedOrder->getCustomerLastname();

                    Mage::getModel('cod/paymentgrid')->setData($data)->save();

                    //cancel credit memo
                    $creditmemo = Mage::getModel('sales/order_creditmemo')->load($creditmemoId);
                    $creditmemo->setState(3)->save();
                }

                //change item state to shipped
                $item = Mage::getModel('opentechiz_inventory/item')->load($itemBarcode, 'barcode');
                $item->setState(15)->save();

                //change order-item link back to original order
                $link = Mage::getModel('opentechiz_inventory/order')->load($item->getId(), 'item_id');
                $link->setOrderItemId($returnedOrderItem->getId())->save();
            }else{
                Mage::log('cancelExpiredExchangeOrder: cannot find return for order '.$order->getIncrementId());
                continue;
            }
        }
    }
}