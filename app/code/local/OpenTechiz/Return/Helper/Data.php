<?php

class OpenTechiz_Return_Helper_Data extends Mage_Core_Helper_Abstract
{
    const RETURN_DESCRIPTION_STRING = 'Returned product';
    const RETURN_FEE_ITEM_SKU = 'return_fee';
    const PENDING = 5;
    const APPROVED = 10 ;
    const DENIED = 15 ;
    const RETURN_IN_REPAIRING = 40 ;
    const RETURN_FINISHED_REPAIRING_PROCESS = 45 ;
    const RETURN_REPAIRING_BROKEN = 46;
    const RETURN_IN_PRODUCTION = 50 ;
    const WAIT_FOR_STOCK_REQUEST = 51 ;
    const STOCK_REQUEST_DECLINE = 52 ;
    const RETURN_FINISHED_PRODUCTION_PROCESS = 55 ;
    const SHIP_OUT_WAITING = 60 ;
    const RESIZING_AND_ENGRAVING = 61 ;
    const SHIPPED_OUT = 65 ;
    const SHIPPED_TO_CUSTOMER = 80 ;
    const CLOSE = 85;

    const REPAIR = 20 ;
    const WITHDRAWAL = 15 ;
    const EXCHANGE = 25;
    
    const REFUND = 5;
    const REPAIRITEM = 10 ;
    const EXCHANGEFROMSTOCK = 20 ;

    const WRONGSIZE = 0;
    const REFUNDAFTERSHIPPED = 2;
    const DAMAGED = 5;
    const WRONGORDERED = 15 ;
    const OTHER = 20 ;
    const PAYMENT_STEP = ['1'=>'Waiting payment confirm','2'=>'Payment confirm'];
    const INCREMENT_ID_PREFIX_BY_SOLUTION = [
        self::REFUND => 'RT',
        self::REPAIRITEM => 'RP',
        self::EXCHANGEFROMSTOCK => 'EX'
    ];

    const INCREMENT_ID_PREFIX_BY_TYPE = [
        self::WITHDRAWAL => 'RT',
        self::REPAIR => 'RP',
        self::EXCHANGE => 'EX'
    ];

    const SOLUTION_TO_ORDER_TYPE = [
        10 => 2,
        15 => 2,
        20 => 3,
        5  => 2
    ];

    const ALL_SOLUTIONS = [
        self::REFUND,
        self::REPAIRITEM,
        self::EXCHANGEFROMSTOCK
    ];
    const STATUS_DENIED = [
        1 => "Decline Pending",
        2 => "Decline Approved",
        3 => "Decline Denied"
    ];
    public function getReturnOrderType(){
        return array(
            self::REPAIRITEM => 'repair',
            self::EXCHANGEFROMSTOCK => 'exchange'
        );
    }

    public function  getAllStatus()
    {
        $status = array(
            self::PENDING => $this->__('Pending'),
            self::APPROVED => $this->__('Approved Return'),
            self::DENIED => $this->__('Denied'),
            self::RETURN_IN_REPAIRING => $this->__('Return in repairing'),
            self::RETURN_FINISHED_REPAIRING_PROCESS => $this->__('Return finished repairing'),
            self::RETURN_REPAIRING_BROKEN => $this->__('Return repair broken'),
            self::RETURN_IN_PRODUCTION => $this->__('Return in production'),
            self::STOCK_REQUEST_DECLINE => $this->__('Stock request decline'),
            self::RETURN_FINISHED_PRODUCTION_PROCESS => $this->__('Return finished production'),
            self::SHIP_OUT_WAITING => $this->__('Ship out waiting'),
            self::RESIZING_AND_ENGRAVING => $this->__('Resizing or engraving'),
            self::CLOSE => $this->__('Closed'),
        );
        return $status;
    }
    public function getReturnTypes(){
        $types = array(
            self::REPAIR => $this->__('Repair Request'),
            self::EXCHANGE => $this->__('Exchange'),
            self::WITHDRAWAL => $this->__('Product Return'),
        );
        return $types;
    }
    public function getReturnReasons(){
        $reasons = array(
            self::WRONGSIZE => $this->__('Incorrect Size/Measurements'),
            self::DAMAGED => $this->__('Poor Quality/Broken'),
            self::WRONGORDERED => $this->__('Wrong Product Received'),
            self::OTHER => $this->__('Other')
        );
        return $reasons;
    }
    public function getAllReasons(){
        $reasons = array(
            self::WRONGSIZE => $this->__('Incorrect Size/Measurements'),
            self::REFUNDAFTERSHIPPED => $this->__('Refunded after shipped, auto create new return'),
            self::DAMAGED => $this->__('Poor Quality/Broken'),
            self::WRONGORDERED => $this->__('Wrong Product Received'),
            self::OTHER => $this->__('Other')
        );
        return $reasons;
    }

     public function getAllSolution(){
        $solutions = array(
            '' => ' ',
            self::REPAIRITEM => $this->__('Repair Item'),
            self::REFUND => $this->__('Refund Item'),
            self::EXCHANGEFROMSTOCK => $this->__('Exchange Item From Stock'),
        );
        return $solutions;
     }

     public function getAllSolutionToChange(){
         $solutions = array(
             self::REPAIRITEM => $this->__('Repair Item'),
             self::REFUND => $this->__('Refund Item'),
             self::EXCHANGEFROMSTOCK => $this->__('Exchange Item From Stock'),
         );
         return $solutions;
     }

    public function getReturnType($return_type){
        $return_types = $this->getReturnTypes();
        if(!isset($return_types[$return_type])){
            return '';
        }
        return $return_types[$return_type];
    }
    public function getReturnReason($id){
        $reasons = $this->getReturnReasons();
        if(!isset($reasons[$id])){
            return '';
        }
        return $reasons[$id];
    }

    public function getReturnStatus($status_id){
        $status = $this->getAllStatus();
        if(!isset($status[$status_id])){
            return '';
        }
        return $status[$status_id];
    }
    public function getOption($option,$sku=''){
        if(strpos($sku, 'quotation') === false){
            if(!isset($option['options']))
                return 'no option';
            $html = '';
            foreach ($option['options'] as $value) {
                # code...
                $html .= '<span> '.$value['label'].' : '.$value['value'].' </span></br>';
            }
        }else{
            $quotationProductId = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[1];
            $optionHtml = '';

            $optionCollection = Mage::getModel('opentechiz_quotation/productOption')->getCollection()->addFieldToFilter('quote_product_id', $quotationProductId);
            $productName = Mage::getModel('opentechiz_quotation/product')->load($quotationProductId)->getQpName();

            foreach ($optionCollection as $option){
                 if($option->getOptionType() == 'stone') {
                    $optionHtml .= '<dd>'. $option->getOptionName() . ' : ' . Mage::getModel('opentechiz_quotation/product')->getStoneName($option->getOptionValue()) . '</dd>';
                } elseif ($option->getOptionType() == 'gold'){
                    $optionHtml .= '<dd>' . $option->getOptionName() . ' : '. Mage::getModel('opentechiz_quotation/product')->getGoldName($option->getOptionValue()) . '</dd>';
                }  else{
                    $optionHtml .= '<dd>' . $option->getOptionName() . ' : ' . $option->getOptionValue() . '</dd>';
                }
            }
            $html = '<div class="item-container">
                    <div class="item-text">
                        
                        <dl class="item-options">
                            ' . $optionHtml . '
                        </dl>
                    </div>
                </div>';
        }
        return $html;
    }

    public function getReturnFee($fee){
        $html = '$';
        if(!$fee){
            $fee = 0;
        }
        $fee = number_format ( $fee , 2 , "." ,  "," );
        return $html.$fee;
    }

     public function getOrderItembybarcode($barcode,$order_item_ids){
        $items = $this->getItembybarcode($barcode);

        if($items->count()!=1)
            return false;
        $itemID = $items->getFirstItem()->getId();
        $order_items =  Mage::getModel('opentechiz_inventory/order')->getCollection()->AddFieldtoFilter('item_id',$itemID)->AddFieldtoFilter('order_item_id',array("in"=>$order_item_ids))->getFirstItem()->getOrderItemId();
        return $order_items;
    }

     public function getItembybarcode($barcode){
        $items =  Mage::getModel('opentechiz_inventory/item')->getCollection()->AddFieldtoFilter('barcode',$barcode);

        return $items;
    }
    public function getItemProduction($barcode){
         $productions =  Mage::getModel('opentechiz_production/product')->getCollection()->AddFieldtoFilter('barcode',$barcode)->setOrder('created_at', 'DESC');;

        return $productions;
    }
    public function checkItemReturned($barcode,$order_item_id){
       
        $order_items =  Mage::getModel('opentechiz_return/return')->getCollection()->AddFieldtoFilter('item_barcode',$barcode)->AddFieldtoFilter('order_item_id',array("eq"=>$order_item_id))->AddFieldtoFilter('status',array('nin'=>[15,85]));
        if(count($order_items)>0)
            return true;
        return false;

    }

    public function getReturnByBarcode($barcode){
       
        $returns =  Mage::getModel('opentechiz_return/return')->getCollection()->AddFieldtoFilter('item_barcode',$barcode)->AddFieldtoFilter('status',array('nin'=>[15,85]));
        if(count($returns)>1)
            return false;
        return $returns;

    }
    public function getReturnByProductionId($personalized_id){
        $return_id = Mage::getModel('opentechiz_return/repair')->load($personalized_id, 'production_id')->getReturnId();
        if($return_id){
           $return = Mage::getModel('opentechiz_return/return')->load($return_id);
            return $return;
        }
        $barcode = Mage::getModel('opentechiz_production/product')->load($personalized_id)->getBarcode();
        $return = Mage::getModel('opentechiz_return/return')->load($barcode,'new_item_barcode');
        if($return->getId())
           return $return; 
       return false;
    }
     public function getProductionStep($requestId){
        $repair =  Mage::getModel('opentechiz_return/repair')->getCollection()->AddFieldtoFilter('production_id',$requestId)->getFirstItem();
        return $repair->getRepairStep();
     }

     public function getRmaOrderItem($rma){
        $order_item = Mage::getModel('sales/order_item')->load($rma->getOrderItemId());
        return $order_item;
     }
    public function getRmaOrder($rma){
        $order_id = $this->getRmaOrderItem($rma)->getOrderId();
        $order = Mage::getModel('sales/order')->load($order_id);
        return $order;
    }
    public function getProductByItem($item){
        $sku = $item->getSku();
        $sku_ar = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
        if(is_array($sku_ar)){
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku',$sku_ar[0]);
            $product_id = $product->getId();
            return $product_id;
        }
        
        return ;
    }
    public function getRepair($barcode){
        $production = $this->getProductionByBarcode($barcode);
        $repair =  Mage::getModel('opentechiz_return/repair')->getCollection()->AddFieldtoFilter('production_id',$production->getId())->getFirstItem();

        return $repair;
    }

    public function getProductionByBarcode($barcode){
        $production =  Mage::getModel('opentechiz_production/product')->getCollection()->AddFieldtoFilter('barcode',$barcode)->AddFieldtoFilter('item_state',array('in'=>[0,4]))->getFirstItem();
        return $production;
    }

    public function getOptionStringFromarray($option_ar,$product){
        $optionsCollection = $product->getOptions();        
                    // if option enabled = no && hasOptions = 0
                    if (!$optionsCollection) $optionsCollection = $product->getProductOptionsCollection();
                    $option_data = array();
                    $ar_sku = array();
                     foreach ($optionsCollection as $option) {
                                    if($option->getType() == 'field' && $option_ar[$option->getId()]){
                                      
                                      $optionval['label'] = $option->getTitle();
                                        $optionval['value'] = $option_ar[$option->getId()];
                                        $optionval['option_id'] = $option->getId();
                                        $optionval['option_value'] = $option_ar[$option->getId()];
                                        $optionval['option_type'] = $option->getType();
                                        if($optionValue->getSku())
                                          $values[] = $optionValue->getSku();
                                      
                                        $option_data['options'][] = $optionval;
                                    }
                                    foreach ($option->getValues() as $optionValue){
                                    if($optionValue->getId() == $option_ar[$option->getId()]) {
                                        $ar_sku[] = $optionValue->getSku();
                                        $optionval['label'] = $option->getTitle();
                                        $optionval['value'] = $optionValue->getTitle();
                                        $optionval['option_id'] = $option->getId();
                                        $optionval['option_value'] = $optionValue->getId();
                                        $optionval['option_type'] = $option->getType();

                                        $option_data['options'][] = $optionval;
                                    }
                                }
                         }
                          $ar_sku = array_filter($ar_sku);  
                        $option_data['info_buyRequest'] = array('product' => $product->getId() );
                        $sku = $product->getSku().OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $ar_sku);
                        $data['option'] = $option_data;
                        $data['sku'] = $sku;
                        return $data;
    }

    public function getStoneRequestList($option_ar,$product,$is_new,$sku){
         $stoneOptionModel = Mage::getModel('personalizedproduct/price_stone');
         $result =[];
        if($is_new){
            $optionsCollection = $product->getOptions();       
            
             if (!$optionsCollection) $optionsCollection = $product->getProductOptionsCollection();
                        $option_data = array();
                        $ar_sku = array();
                         foreach ($optionsCollection as $option) {
                                    if($option->getType() == 'stone'){
                                        foreach ($option->getValues() as $optionValue){
                                                if($optionValue->getId()==$option_ar[$option->getId()]){
                                                    $stoneSku = $optionValue->getSku();
                                                    $quality = '';
                                                    if (strpos($stoneSku, 'diamond') !== false) {
                                                        $quality = str_replace('diamond', '', $stoneSku);
                                                        if(in_array($quality, OpenTechiz_PersonalizedProduct_Helper_Data::QUALITY)){
                                                            $stoneSku = 'diamond';
                                                        } else{
                                                            $quality = '';
                                                        }
                                                    }

                                                    $query  = 'stone.stone_id = main_table.stone_id and main_table.option_id = "'.$option->getId().'" and main_table.product_id = '.$product->getId().' and stone.stone_type = "'.$stoneSku.'"';
                                                    if($quality != ''){
                                                        $query .= 'and stone.quality = "'.$quality.'"';
                                                    }
                                                
                                                    $collection = $stoneOptionModel->getCollection()->join(array('stone'=> 'opentechiz_material/stone'), $query, '*');
                                                    foreach ($collection as $requiredStone){
                                                        $array = [];
                                                        $array['id'] = $requiredStone->getStoneId();
                                                        $array['name'] = $requiredStone->getStoneType();
                                                        $array['weight'] = $requiredStone->getWeight();
                                                        $array['shape'] = $requiredStone->getShape();
                                                        $array['diameter'] = $requiredStone->getDiameter();
                                                        $array['quality'] = $requiredStone->getQuality();
                                                        $array['qty'] = $requiredStone->getQuantity();

                                                        array_push($result, $array);
                                                    }
                                                }

                                        }  
                                    }    

            }
        }else{
            $has_stone_option = 0;
            foreach($option_ar as $option){
                
                if($option['option_type'] == 'stone'){
                        $has_stone_option = 1;
                                $optionData = [];
                                $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
                                for ($i = 1; $i < count($parts) - 2; $i++) {
                                    switch ($i){
                                        case 1:
                                            $key = 'Center Stone';
                                            break;
                                        case 2:
                                            $key = 'Stone 2';
                                            break;
                                        case 3:
                                            $key = 'Stone 3';
                                            break;
                                        case 4:
                                            $key = 'Stone 4';
                                            break;

                                    }
                                    $optionData[$key] = $parts[$i];
                                }

                            $option['value'] = $optionData[$option['label']];
                            $stoneSku = strtolower(str_replace(' ', '', $option['value']));
                            $quality = '';
                            if (strpos($stoneSku, 'diamond') !== false) {
                                $quality = str_replace('diamond', '', $stoneSku);
                                if(in_array($quality, OpenTechiz_PersonalizedProduct_Helper_Data::QUALITY)){
                                    $stoneSku = 'diamond';
                                } else{
                                    $quality = '';
                                }
                            }


                            $query  = 'stone.stone_id = main_table.stone_id and main_table.option_name = "'.$option['label'].'" and main_table.product_id = "'.$product->getId().'" and stone.stone_type = "'.$stoneSku.'"';
                        
                            if($quality != ''){
                                $query .= 'and stone.quality = "'.$quality.'"';
                            }
                            $collection = $stoneOptionModel->getCollection()->join(array('stone'=> 'opentechiz_material/stone'), $query, '*');

                            foreach ($collection as $requiredStone){
                                $array = [];

                                $array['id'] = $requiredStone->getStoneId();
                                $array['name'] = $requiredStone->getStoneType();
                                $array['weight'] = $requiredStone->getWeight();
                                $array['shape'] = $requiredStone->getShape();
                                $array['diameter'] = $requiredStone->getDiameter();
                                $array['quality'] = $requiredStone->getQuality();
                                $array['qty'] = $requiredStone->getQuantity();

                                array_push($result, $array);

                               
                            }
                        }
            }
            if($has_stone_option == 0){
                $productId = $product->getId();
                $query = 'stone.stone_id = main_table.stone_id';
                $collection = $stoneOptionModel->getCollection()
                    ->join(array('stone' => 'opentechiz_material/stone'), $query, '*')
                    ->addFieldToFilter('product_id', $productId);

                foreach ($collection as $requiredStone) {
                    $array = [];
                    $array['id'] = $requiredStone->getStoneId();
                    $array['name'] = $requiredStone->getStoneType();
                    $array['weight'] = $requiredStone->getWeight();
                    $array['shape'] = $requiredStone->getShape();
                    $array['diameter'] = $requiredStone->getDiameter();
                    $array['quality'] = $requiredStone->getQuality();
                    $array['qty'] = $requiredStone->getQuantity();

                    array_push($result, $array);
                }
            }
            
            
        

        }
        mage::log($result,null,'mylog.log',true);
        return $result;  
    }

    public function addStoneRequestQuotation($optionArray){

        $result = [];
        $stoneModel = Mage::getModel('opentechiz_material/stone');
        $admin = Mage::getSingleton('admin/session')->getUser();

        foreach ($optionArray as $requiredStone){
            $stone = $stoneModel->load($requiredStone->getData('option_value'));
            $array = [];
            $array['id'] = $stone->getId();
            $array['name'] = $stone->getStoneType();
            $array['weight'] = $stone->getWeight();
            $array['shape'] = $stone->getShape();
            $array['diameter'] = $stone->getDiameter();
            $array['quality'] = $stone->getQuality();
            $array['qty'] = $requiredStone->getData('qty');
            array_push($result, $array);

        }
       return $result;
    }

    public function getReturnableBarcode($order_item_id){
        $barcodes = array();
        $array = Mage::getModel('opentechiz_inventory/order')->getCollection()->AddFieldtoFilter('order_item_id',$order_item_id);
        foreach ($array as $value) {
            # code...
            $item = Mage::getModel('opentechiz_inventory/item')->load($value->getItemId());
            if(in_array($item->getState(), [5,6,10,15,20]))
                $barcodes[] = $item->getBarcode();
        }

        //logic remove if item already in a return
        $returns = Mage::getModel('opentechiz_return/return')->getCollection()
            ->AddFieldtoFilter('item_barcode', array('in' => $barcodes))
            ->AddFieldtoFilter('status', array('nin'=> array(self::CLOSE, self::DENIED)));

        foreach ($returns as $return) {
            //skip check if item not in any return
            $key = array_search($return->getItemBarcode(), $barcodes);
            if($key !== false){
                //skip if return have different item or order_item_id as current order
                if($return->getOrderItemId() != $order_item_id || $return->getItemBarcode() != $barcodes[$key]){
                    continue;
                }

                //skip if return order is already shipped
                if($return->getReturnFeeOrderId() != null){
                    $incrementId = $return->getReturnFeeOrderId();
                    $order = Mage::getModel('sales/order')->load($incrementId, 'increment_id');
                    if(in_array($order->getState(), (array)OpenTechiz_SalesExtend_Helper_Data::CANCEL_CLOSE_ORDER_STATE)){
                        continue;
                    }
                    if($order->getOrderStage() == 5){
                        continue;
                    }
                }
                unset($barcodes[$key]);
            }
        }

        return $barcodes;
    }

    public function removeSpace($data){
        $data_nospace = '';
        $data_nospace = trim($data);
        return $data_nospace;
    }
    public function getUserName($user_id){

        $user_data = Mage::getModel('admin/user')->load($user_id);
        if($user_data){
            return $user_data->getUsername();
        }
       
        return '';
    }
    public function getUserId(){
        if(Mage::getSingleton('admin/session')->isLoggedIn()){
            $user_id = Mage::getSingleton('admin/session')->getUser()->getId();
        }else{
            $user_id = 0;
        }
        return $user_id;
    }
}