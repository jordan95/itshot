<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_return/return')} 
    CHANGE `return_image` `return_image`
    VARCHAR(255)
    NULL
    COMMENT 'return image';
");
$installer->endSetup();
