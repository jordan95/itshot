<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('opentechiz_return/return')};
    CREATE TABLE {$this->getTable('opentechiz_return/return')} (
      `return_id`   INT(11) UNSIGNED         NOT NULL AUTO_INCREMENT,
      `increment_id`      VARCHAR(45)       NOT NULL,
      `customer_id`      INT(11)       NOT NULL,
      `order_item_id`      INT(11)       NOT NULL,
      `return_type`      INT(11)       NOT NULL,
      `description`       VARCHAR(255)      NOT NULL,
      `repair_step`       VARCHAR(255)      NOT NULL,
      `first_name`       VARCHAR(45)      NOT NULL,
      `last_name`       VARCHAR(45)      NOT NULL,
      `email`       VARCHAR(45)      NOT NULL,
      `phone`       VARCHAR(45)      NOT NULL,
      `street`       VARCHAR(45)      NOT NULL,
      `city`       VARCHAR(45)      NOT NULL,
      `region`       VARCHAR(45)      NOT NULL,
      `postcode`       VARCHAR(45)      NOT NULL,
      `status`      INT(11)           NOT NULL DEFAULT 5,
      `item_barcode`      INT(11)       NOT NULL,
      `new_item_barcode`      INT(11)       NOT NULL,
      `reason`       VARCHAR(255)      NOT NULL,
      `solution`       INT(11)      NOT NULL,
      `return_fee`    double(12,4)          NULL ,
      `is_paid`      INT(11)           NOT NULL DEFAULT 0,
      `refund_amount`    double(12,4)         NULL ,
      `created_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `updated_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`return_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);


$installer->endSetup();