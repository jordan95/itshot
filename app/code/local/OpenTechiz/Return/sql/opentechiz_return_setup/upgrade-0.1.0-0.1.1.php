<?php

$installer = $this;
$installer->startSetup();

$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_return/return')} 
    ADD `status_denied` 
    int(5)
    COMMENT 'status denied';
");

$installer->endSetup();