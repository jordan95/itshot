<?php

/* @var $this Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('opentechiz_return/return');
$installer->getConnection()->addIndex(
    $tableName,
    $installer->getIdxName($tableName, ['increment_id']),
    ['increment_id']
);
$installer->getConnection()->addIndex(
    $tableName,
    $installer->getIdxName($tableName, ['order_item_id']),
    ['order_item_id']
);

$installer->endSetup();
