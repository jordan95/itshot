<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_return/repair')} 
    ADD `return_id` 
    INT(11) UNSIGNED         NOT NULL
    COMMENT 'return id';
");
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_return/return')} 
    DROP `repair_step`;
");
$installer->endSetup();
