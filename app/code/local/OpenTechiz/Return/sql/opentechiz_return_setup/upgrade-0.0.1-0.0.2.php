<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    
    ALTER TABLE {$this->getTable('opentechiz_return/return')}
    CHANGE `item_barcode` `item_barcode`
    VARCHAR(45)
    NULL
    COMMENT 'Item return barcode';
    
    ALTER TABLE {$this->getTable('opentechiz_return/return')}
    CHANGE `new_item_barcode` `new_item_barcode`
    VARCHAR(45)
    NULL
    COMMENT 'New Item barcode';
");

$installer->endSetup();