<?php
$content = 'Return Policy : In order to be eligible for refund item(s) must be returned in original condition, unworn, with tag attached and not tempered with';


    $block = Mage::getModel('cms/block');
    $block->setTitle('Return Policy');
    $block->setIdentifier('return_policy');
    $block->setStores(array(0));
    $block->setIsActive(1);
    $block->setContent($content);
    $block->save();
