<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_return/return')} 
    ADD `return_image`
    VARCHAR(45)
    NULL
    COMMENT 'return image';
");
$installer->endSetup();
