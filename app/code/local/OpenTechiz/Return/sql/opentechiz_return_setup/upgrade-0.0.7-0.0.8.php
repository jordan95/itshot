<?php

$installer = $this;
$installer->startSetup();

$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_return/return')} 
    ADD `new_option` 
    text 
    COMMENT 'new option';
");

$installer->endSetup();