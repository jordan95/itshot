<?php

$installer = $this;
$installer->startSetup();

$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_return/return')} 
    ADD `created_by` 
    int(5)
    COMMENT 'created by';
");

$installer->endSetup();