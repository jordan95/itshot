<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('opentechiz_return/repair')};
    CREATE TABLE {$this->getTable('opentechiz_return/repair')} (
      `id`   INT(11) UNSIGNED         NOT NULL AUTO_INCREMENT,
      `item_barcode`      VARCHAR(45)       NOT NULL,
      `production_id`      INT(11)       NOT NULL,
      `repair_step`       VARCHAR(255)      NOT NULL,
      `created_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `updated_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);


$installer->endSetup();