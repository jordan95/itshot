<?php

$installer = $this;
$installer->startSetup();

$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_return/return')} 
    ADD `return_fee_order_id` 
    varchar(50)
    COMMENT 'order for return fee';
");

$installer->endSetup();