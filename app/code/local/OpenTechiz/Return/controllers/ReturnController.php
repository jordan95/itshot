<?php

class OpenTechiz_Return_ReturnController extends Mage_Core_Controller_Front_Action
{
    public function createAction()
    {
        $data = $this->getRequest()->getPost();
        $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $item_data = $data['order_item'];
        unset($data['order_item']);
        $returnable = 0;
        $returnable_barcodes =array();
        foreach ($item_data as $id => $item) {
            $barcodes = Mage::helper('opentechiz_return')->getReturnableBarcode($id);
            $returnable_barcodes[$id] = $barcodes;
            $returnable_qty = count($barcodes);
            if ($returnable_qty)
                $returnable = 1;
        }
        if($returnable==0){
            $this->_redirect('returns/return/stepone');
            $session = Mage::getSingleton('core/session');
            $session->addError('Something wrong. Order item not able to return');
            return;
        }
        if ($data)
        {
           
            if(isset($_FILES['images']['name']) and (file_exists($_FILES['images']['tmp_name']))) {
                $_logFile = $_FILES['images']['name'].'.log';
                try {
                    $uploader = new Varien_File_Uploader('images');
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);
                    $newDir = "upload";
                    $path = Mage::getBaseDir('media') . DS . $newDir;
                    $destFile = $path . $_FILES['images']['name'];
                    $filename = $uploader->getNewFileName($destFile);
                    $uploader->save($path, $filename);
                    $dataPath = $uploader->getUploadedFileName();
                    $data['return_image'] =  $dataPath;

                    $uploader->save($path, $filename);

                   
                    //$model->setReturnImage($dataPath);
                }catch(Exception $e) {
                    Mage::log($e->getMessage(), null, $_logFile, true);
                }
            }
            $data['created_at'] = now();
            $data['updated_at'] = now();
            $data['customer_id'] = $customerId;

            foreach ($item_data as $id => $item) {
                $data['return_type'] = $item['return_type'];
                $data['reason'] = $item['reason'];
                $data['order_item_id'] = $id;
                $barcodes = $returnable_barcodes[$id];

                if(isset($item['qty_return'])) {
                    for ($i = 0; $i < $item['qty_return']; $i++) {
                        # code...
                        $model = Mage::getModel('opentechiz_return/return');
                        $k = array_rand($barcodes);
                        $data['item_barcode'] = $barcodes[$k];
                        unset($barcodes[$k]);
                        try {
                            $model->addData($data);

                            // set data to model
                            /* Session errors */
                            $session = Mage::getSingleton('core/session');
                            // save data
                            $model->save();

                            if (!$model->getIncrementId()) {
                                $model->setIncrementId($model->generatedIncrementId($model->getId()))->save();
                            }
                            $customer = Mage::getSingleton('customer/session')->getCustomer();
                            $event_data_array = array('customer' => $customer);
                            $varien_object = new Varien_Object($event_data_array);
                            Mage::dispatchEvent('add_email_list_customer_create_return', array('event_data' => $varien_object));

                        } catch (Exception $error) {
                            $session->addError($error->getMessage());
                            return false;
                        }
                    }
                }
            }
            $this->_redirect('returns/return');
        }
    }

    public function saveAction()
    {
        $data = $this->getRequest()->getPost();
        $model = Mage::getModel('opentechiz_return/return');
        $rma_id = $this->getRequest()->getParam('id');
        
        $model->load($rma_id);
        if ($data)
        {
            if(isset($_FILES['images']['name']) and (file_exists($_FILES['images']['tmp_name']))) {
                $_logFile = $_FILES['images']['name'].'.log';
                try {
                    $uploader = new Varien_File_Uploader('images');
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);
                    $newDir = "upload";
                    $path = Mage::getBaseDir('media') . DS . $newDir;
                    $destFile = $path . $_FILES['images']['name'];
                    $filename = $uploader->getNewFileName($destFile);
                    $uploader->save($path, $filename);
                    $dataPath = $uploader->getUploadedFileName();
                    $data['return_image'] =  $dataPath;

                    $uploader->save($path, $filename);

                    //$model->setReturnImage($dataPath);
                }catch(Exception $e) {
                    Mage::log($e->getMessage(), null, $_logFile, true);
                }
            }
            $data['created_at'] = now();
            $data['updated_at'] = now();
            $model->addData($data);
            // set data to model
            /* Session errors */
            $session = Mage::getSingleton('core/session');
            // save data
            try{
                $model->save();
                if(!$model->getIncrementId()){
                    $model->setIncrementId($model->generatedIncrementId($model->getId()))->save();
                }
                $session->addSuccess('Submit Sucessfully!');
            }
            catch(Exception $error)
            {
                $session->addError($error->getMessage());
                return false;
            }
             $this->_redirect('returns/return');
        }
    }

    protected function newAction()
    {
        $email = $this->getRequest()->getParam('email');
        $order_number = $this->getRequest()->getParam('order_number');
        $order = Mage::getModel('sales/order')->getCollection()->AddFieldtoFilter('increment_id',$order_number)->getFirstItem();
         $session = Mage::getSingleton('core/session');

        if ($email&&$order->getCustomerEmail()!= $email) {
            $this->_redirect('returns/return/stepone');
            $session->addError('Something wrong. Please check your email');
            return;
        }

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        
       
        if($customer->getId()&&$order->getCustomerId()!=$customer->getId()){
            $this->_redirect('returns/return/stepone');
            
             $session->addError('Something wrong. Please check your order number.');
            return;
        }
        $returnable = 0;
        foreach ($order->getAllItems() as $item) {
            $order_item_ids[] = $item->getId();
            $barcodes = Mage::helper('opentechiz_return')->getReturnableBarcode($item->getId());

            $returnable_qty = count($barcodes);
            if ($returnable_qty)
                $returnable = 1;
        }

        if($returnable==0){
            $this->_redirect('returns/return/stepone');
            $session->addError('Something wrong. Order item cannot be returned');
            return;
        }
        
        Mage::register('order_item_id',$order_item_ids);

     
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    public function viewAction()
    {
        $rma_id = $this->getRequest()->getParam('id');
        $rma =  Mage::getModel('opentechiz_return/return')->load($rma_id);
        Mage::register('current_rma',$rma);
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    public function StepOneAction()
    {
        
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    public function payAction(){

        $helper = Mage::helper('opentechiz_return');
        $model = Mage::getModel('opentechiz_return/return');
        $coreSession = Mage::getSingleton('core/session');
        $session = Mage::getSingleton('customer/session');

        // Validate surcharge id in request
        $returnId = $this->getRequest()->getParam('id');
        if (!$returnId) {
            $coreSession->addError($helper->__('Return has empty id'));
            $this->_redirect('*/*');
            return;
        }

        $return = $model->load($returnId);

        // Validate surcharge
        if (!$return || !$return->getId()) {
            $coreSession->addError($helper->__('Empty return'));
            $this->_redirect('*/*');
            return;
        }

        // Validate customer
        $customerId = $session->getCustomerId();
        if (!$customerId) {
            $coreSession->addError($helper->__('Please, check your login information'));
            $this->_redirect('*/*');
            return;
        }

        $orderItem = Mage::getModel('sales/order_item')->load($return->getOrderItemId());
        if(!$orderItem || !$orderItem->getOrderId()){
            $coreSession->addError($helper->__('Something wrong with order item'));
            $this->_redirect('*/*');
            return;
        }
        $order = Mage::getModel('sales/order')->load($orderItem->getOrderId());

        // Validate parent order
        if (!$order->getId()) {
            $coreSession->addError($helper->__('Empty parent order'));
            $this->_redirect('*/*');
            return;
        }

        // Remove existing customer quote and add new quote with return payment
        $customer = $session->getCustomer();
        $quote = Mage::getModel('opentechiz_return/return')->createNewQuote($returnId, $customer, $return->getReturnFee());

        $session->setQuoteId($quote->getId());
        $quote->collectTotals();
        $quote->save();

        $coreSession->addSuccess($helper->__('Return fee quote was successfully created for order #%s', $order->getIncrementId()));
        $this->_redirect('checkout/cart');
    }
}