<?php

class OpenTechiz_Return_Adminhtml_InstockwaitingController extends Mage_Adminhtml_Controller_Action
{
    protected function _initModel($requestParam = 'id')
        {
            $model = Mage::registry('current_item');
            if ($model) {
               return $model;
            }
            $model = Mage::getModel('opentechiz_inventory/item');
            

            $id = $this->getRequest()->getParam($requestParam);
            if ($id) {
                $model->load($id);
                if (!$model->getId()) {
                    Mage::throwException(Mage::helper('opentechiz_production')->__('Wrong Item requested.'));
                }
            }
            Mage::register('current_item', $model);

            return $model;
        }
    protected function indexAction()
        {
            $this->loadLayout()
                ->_setActiveMenu('erp/inventory/')
                ->_title($this->__('ERP'))
                ->_title($this->__('Inventory'))
                ->_title($this->__('Instock Waiting Items'));
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('opentechiz_return/adminhtml_instockwaiting'));
            $this->renderLayout();
        }
    public function gridAction()
        {
            $this->loadLayout();
           
            $this->getResponse()->setBody(
                $this->getLayout()->createBlock('opentechiz_return/adminhtml_instockwaiting_grid')->toHtml()
            );
        }
    public function massInstockAction(){
        
        $Ids = $this->getRequest()->getParam('item');
        $return_id ='';
        $order_id ='';
        if (!is_array($Ids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($Ids as $id) {
                    $item = Mage::getModel('opentechiz_inventory/item')->load($id);
                    $inventory_order_item = Mage::getModel('opentechiz_inventory/order')->load($item->getItemId(), "item_id");
                    $instock = Mage::getModel('opentechiz_inventory/instock')->getCollection()->addFieldtoFilter('sku',$item->getSku())->getFirstItem();
                    $order_item = Mage::getModel('sales/order_item')->load($inventory_order_item->getOrderItemId());
                    
                    if($instock && $instock->getId()) {
                        if ($order_item->getId()) {
                            $order = $order_item->getOrder();
                            $return = Mage::getModel('opentechiz_return/return')->getCollection()->addFieldtoFilter('order_item_id',$order_item->getId())->addFieldtoFilter('item_barcode',$item->getBarcode())->getFirstItem();
                            if(isset($return)){
                                $return_id = $return->getId();
                            }
                            if(isset($order)){
                                $order_id = $order->getId();
                            }
                        }

                        Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                            'qty' => 1,
                            'on_hold' => '0',
                            'type' => 5,
                            'stock_id' => $instock->getId(),
                            'qty_before_movement' => $instock->getQty(),
                            'on_hold_before_movement' => $instock->getOnHold(),
                            'sku' => $instock->getSku(),
                            'order_id' => $order_id,
                            'return_id' => $return_id,

                        ));
                    }
                    $item->setState(35)->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully in stock', count($Ids)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/instockwaiting/index');
    }
    public function maskasbrokenAction(){
        
        $Ids = $this->getRequest()->getParam('item');
        if (!is_array($Ids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($Ids as $id) {
                    $item = Mage::getModel('opentechiz_inventory/item')->load($id);
                    $item->setState(45)->save();
                    
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully mark as broken', count($Ids)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/instockwaiting/index');
    }
    public function saveAction()
        {
            try {
                $data = $this->getRequest()->getPost();
                if(empty($data['repair_step'])){
                    Mage::getSingleton('core/session')->addError(Mage::helper('core')->__('Please select step repair!'));
                    $this->_redirect('*/*/view', array('id' => $this->getRequest()->getParam('id')));
                    return;
                }
                 
                $item_id = $this->getRequest()->getParam('id');
                
                $item = Mage::getModel('opentechiz_inventory/item')->load($item_id);
                $product = Mage::getModel('catalog/product')->load($data['product_id']);
                $admin = Mage::getSingleton('admin/session')->getUser();
                $production_data = array();
                
                $production_data['status'] = OpenTechiz_Production_Helper_Data::PROCESSES[0];
                $production_data['production_type'] = 4;
                $production_data['option'] = $item->getOptions();
                $production_data['barcode'] = $item->getBarcode();   
                $production_data['item_state'] = 4; 
                $production_data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                if($step_json = $data['repair_step']){
                    $step_ar = json_decode($step_json,true);
                    $status = array_search ($step_ar[1], OpenTechiz_Production_Helper_Data::PROCESS_LIST); 
                    $production_data['status'] = $status;
                              
                }
                $image = $item->imageUrlFromOption(unserialize($production_data['option']), $product);
                 if(isset($data['options'])){
                    $option_ar = $data['options'];
                    $product = Mage::getModel('catalog/product')->load($data['product_id']);
                    $result = Mage::helper('opentechiz_return')->getOptionStringFromarray($option_ar,$product);
                    $sku = $result['sku'];
                    $option = serialize($result['option']);
                    $image = $item->imageUrlFromOption($result['option'], $product);
                    $item->setImage($image)->setSku($sku)->setOptions($option)->setUpdatedAt(now())->save();

                    $production_data['option'] = $item->getOptions();
                }
                $item->setState(1)->save();
                $production_data['image'] = $image;
                
                $productRequest = Mage::getModel('opentechiz_production/product');
                $productRequest->addData($production_data)->save();
                $requeststone = [];
                if(isset($data['stonerequest'])){
                    foreach ($data['stonerequest'] as $key => $value) {
                        # code...
                        
                        $stoneModel = Mage::getModel('opentechiz_material/stone');
                        if(!isset($value['id']))
                            continue;

                        $stone = $stoneModel->load($value['id']);
                        $array = [];
                        $array['id'] = $stone->getId();
                        $array['name'] = $stone->getStoneType();
                        $array['weight'] = $stone->getWeight();
                        $array['shape'] = $stone->getShape();
                        $array['diameter'] = $stone->getDiameter();
                        $array['quality'] = $stone->getQuality();
                        $array['qty'] = $value['qty'];
                        array_push($requeststone, $array);

                        if($admin->getId()){
                            Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                                'user_id' => $admin->getId(),
                                'description' => 'Stone Request for repair item with personalized id #'.$productRequest->getId(),
                                'qty' => '0',
                                'on_hold' => '+'.$array['qty'],
                                'type' => 1,
                                'stock_id' => $array['id']
                            ));
                        }
                    }
                    if(count($requeststone)>0){
                        $requeststonedata = [];
                        $requeststonedata['personalized_id'] = $productRequest->getId();
                        $requeststonedata['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                        $requeststonedata['stones'] = serialize($requeststone);
                        Mage::getModel('opentechiz_material/stoneRequest')->setData($requeststonedata)->save();
                        Mage::getModel('opentechiz_material/stoneRequest')->putStoneOnHold(serialize($requeststone));
                    }
                }
                
                

                $repair = Mage::getModel('opentechiz_return/repair');
                if(strpos($data['repair_step'], ',"Finished"]') === false)
                        $data['repair_step'] = str_replace(']', ',"Finished"]', $data['repair_step']);
                $repair_data = ['item_barcode' => $item->getBarcode(),'repair_step'=>$data['repair_step'],'production_id'=>$productRequest->getId()];
                $repair->setData($repair_data)->save();
                 Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Item on repair production'
                    )
                );
                $this->_redirect('*/*/view', array('id' => $this->getRequest()->getParam('id')));
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                  Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                  Mage::getSingleton('adminhtml/session')->setFormData($data);
                  $this->_redirect('*/*/view', array('id' => $this->getRequest()->getParam('id')));
                  return;
            }
        }
    public function viewAction()
    {
         $this->_initModel();
         
         $this->loadLayout()
                ->_setActiveMenu('production');
        
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/jquery-1.10.2.min.js');
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/noconflict.js');
         $this->_addContent($this->getLayout()->createBlock('opentechiz_return/adminhtml_instockwaiting_edit'))
             ->_addLeft($this->getLayout()->createBlock('opentechiz_return/adminhtml_instockwaiting_edit_tabs'));
             
            $this->renderLayout();
    }
    public function GetStoneRequestAction(){
        $data = $this->getRequest()->getPost();
         $product = Mage::getModel('catalog/product')->load($data['id']);
        $option_ar = $data['option'];
        $sku = $data['sku'];
        
        if(strpos($sku, 'quotation') !== false){
            $quotationProductId = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku)[1];
            $stoneOptionCollection = Mage::getModel('opentechiz_quotation/productOption')->getCollection()
                ->addFieldToFilter('quote_product_id', $quotationProductId)
                ->addFieldToFilter('option_type', 'stone');
            $result = Mage::helper('opentechiz_return')->addStoneRequestQuotation($stoneOptionCollection);
        }else{
            $result = Mage::helper('opentechiz_return')->getStoneRequestList($option_ar,$product,$data['is_new'],$sku);
        }
        $html = 'no requested';
        if(count($result)>0){
            $html = '<table><tbody><tr class="">
            <th>Select</th>
            <th>Stone Type</th>
            <th>Weight</th>
            <th>Shape</th>
            <th>Diameter</th>
            <th>Quality</th>
            <th width="10%" >Quantity</th>
          </tr>';
          foreach ($result as  $value) {
            $html .= ' <tr > <td>'.'<input type="checkbox" name="stonerequest['.$value['id'].'][id]" value="'.$value['id'].'"> </td> <td>'.$value['name'].'</td> <td>'.$value['weight'].'</td> <td>'.$value['shape'].'</td> <td>'.$value['diameter'].'</td> <td> '.$value['quality'].'</td> <td><input type="number" min=0 name="stonerequest['.$value['id'].'][qty]" style="width:50px" value="'.$value['qty'].'" max="'.$value['qty'].'" ></td> </tr> ';
             
          }
        
        
            $html .= '</tbody></table>';
        }
        print_r($html);
        die('');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/inventory/instock');
    }
}