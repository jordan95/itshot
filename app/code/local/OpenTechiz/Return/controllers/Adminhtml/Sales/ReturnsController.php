<?php

class OpenTechiz_Return_Adminhtml_Sales_ReturnsController extends Mage_Adminhtml_Controller_Action
{
    protected function _initReturn($idFieldName = 'id')
    {
        $Id = (int)$this->getRequest()->getParam($idFieldName);
        $return = Mage::getModel('opentechiz_return/return');

        if ($Id) {
            $return->load($Id);
            if (!$return->getId()) {
                Mage::throwException(Mage::helper('opentechiz_return')->__('Wrong Return requested.'));
            }
        }

        Mage::register('current_return', $return);
        return $return;
    }

    public function editAction()
    {
        $return = $this->_initReturn();

        $this->_initAction();
        $this->_title($this->__('Detail'));
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/jquery-1.10.2.min.js');
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/noconflict.js');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_return/adminhtml_sales_returns_edit'))->_addLeft($this->getLayout()->createBlock('opentechiz_return/adminhtml_sales_returns_edit_tabs'));
        $this->renderLayout();

    }

    public function createAction()
    {
        $return = $this->_initReturn();
        $order_id = (int)$this->getRequest()->getParam('order_id');
        $type = $this->getRequest()->getParam('type');
        $order = Mage::getModel('sales/order')->load($order_id);
        if($order->getOrderType() == 2){
            if(isset($type)) $this->_redirect('*/*/new/type/'.$type);
            else $this->_redirect('*/*/new');
            $session = Mage::getSingleton('core/session');
            $session->addError('Repair order cannot be returned. Please use return function on original order');
            return;
        }
        $returnable = 0;
        foreach ($order->getAllItems() as $item) {
            if($item->getProductType() == 'virtual'){
                continue;
            }
            $order_item_ids[] = $item->getId();
            $barcodes = Mage::helper('opentechiz_return')->getReturnableBarcode($item->getId());
            
            $returnable_qty = count($barcodes);
            if ($returnable_qty)
                $returnable = 1;
        }

        if($returnable==0){
            if(isset($type)) $this->_redirect('*/*/new/type/'.$type);
            else $this->_redirect('*/*/new');
            $session = Mage::getSingleton('core/session');
            $session->addError('No Item is in available for return. Or Item is already being processed in another return.');
            return;
        }
        $this->_initAction();
        $this->_title($this->__('Detail'));
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/jquery-1.10.2.min.js');
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/noconflict.js');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_return/adminhtml_sales_returns_create'))->_addLeft($this->getLayout()->createBlock('opentechiz_return/adminhtml_sales_returns_create_tabs'));
        $this->renderLayout();

    }

    public function newAction()
    {
        $return = $this->_initReturn();

        $this->_initAction();
        $this->_title($this->__('Select order'));
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/jquery-1.10.2.min.js');
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/noconflict.js');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_return/adminhtml_sales_returns_ordergrid'));
        $this->renderLayout();

    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/returns')
            ->_title($this->__('Sales'))
            ->_title($this->__('Return'));
        return $this;
    }

    protected function addAction()
    {
        $data = $this->getRequest()->getPost();

        $type = null;
        if(isset($this->getRequest()->getParams()['type'])){
            if(in_array($this->getRequest()->getParams()['type'], array(10, 20))){
                $type = Mage::helper('opentechiz_return')->getReturnOrderType()[$this->getRequest()->getParams()['type']];
            }
        }
        $data['first_name'] = Mage::helper("opentechiz_return")->removeSpace($data['first_name']);
        $data['last_name'] = Mage::helper("opentechiz_return")->removeSpace($data['last_name']);
        $data['phone'] = Mage::helper("opentechiz_return")->removeSpace($data['phone']);
        $data['email'] = Mage::helper("opentechiz_return")->removeSpace($data['email']);
        $data['street'] = Mage::helper("opentechiz_return")->removeSpace($data['street']);
        $data['description'] = Mage::helper("opentechiz_return")->removeSpace($data['description']);
        
        $item_data = $data['order_item'];
        unset($data['order_item']);
        $returnable = 0;
        $returnable_barcodes = array();
        $qty_returns = 0;
        foreach ($item_data as $id => $item) {
            $barcodes = Mage::helper('opentechiz_return')->getReturnableBarcode($id);
            $returnable_barcodes[$id] = $barcodes;
            $returnable_qty = count($barcodes);
            if ($returnable_qty)
                $returnable = 1;
            if(isset($item['qty_return']) && is_numeric($item['qty_return']))
                $qty_returns += $item['qty_return'];
        }

        if($returnable==0){
            if($type && is_string($type)) $this->_redirect('*/*/create', array('order_id' => $data['order_id'], 'type' => $type));
            else $this->_redirect('*/*/create', array('order_id' => $data['order_id']));
            $session = Mage::getSingleton('core/session');
            $session->addError('No Item is in available for return. Or Item is already being processed in another return.');
            return;
        }
        if( $qty_returns==0){
            $this->_redirect('*/*/create', array('order_id' => $data['order_id']));
            $session = Mage::getSingleton('core/session');
            $session->addError('Something\'s wrong. Please select return qty ');
            return;
        }

        if ($data)
        {
            if(isset($_FILES['images']['name']) and (file_exists($_FILES['images']['tmp_name']))) {
                $_logFile = $_FILES['images']['name'].'.log';
                try {
                    $uploader = new Varien_File_Uploader('images');
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);
                    $newDir = "upload";
                    $path = Mage::getBaseDir('media') . DS . $newDir;
                    $destFile = $path . $_FILES['images']['name'];
                    $filename = $uploader->getNewFileName($destFile);
                    $uploader->save($path, $filename);
                    $dataPath = $uploader->getUploadedFileName();
                    $data['return_image'] =  $dataPath;

                    $uploader->save($path, $filename);


                    //$model->setReturnImage($dataPath);
                }catch(Exception $e) {
                    Mage::log($e->getMessage(), null, $_logFile, true);
                }
            }
            $data['created_at'] = now();
            $data['updated_at'] = now();

            foreach ($item_data as $id => $item) {
                # code...
                $data['return_type'] = $item['return_type'];
                $data['reason'] = $item['reason'];
                $data['order_item_id'] = $id;
                $barcodes = $returnable_barcodes[$id];
                if(!isset($item['qty_return'])) $item['qty_return'] = 0;
                for ($i=0; $i < $item['qty_return']; $i++) {
                    # code...
                    $model = Mage::getModel('opentechiz_return/return');
                    $k = array_rand($barcodes);
                    $data['item_barcode'] =  $barcodes[$k];
                    unset($barcodes[$k]);
                    try{
                        $model->addData($data);

                        // set data to model
                        /* Session errors */
                        $session = Mage::getSingleton('core/session');
                        // save data
                        $model->save();

                        if(!$model->getIncrementId()){
                            $model->setIncrementId($model->generatedIncrementId($model->getId()))->save();
                        }

                        $customer = Mage::getModel('customer/customer')->load($data['customer_id']);
                        $event_data_array = array('customer' => $customer);
                        $varien_object = new Varien_Object($event_data_array);
                        Mage::dispatchEvent('add_email_list_customer_create_return', array('event_data' => $varien_object));
                    }
                    catch(Exception $error)
                    {
                        $session->addError($error->getMessage());
                        return false;
                    }
                }
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('adminhtml')->__(
                    'Return were saved successfully '
                )
            );

            if(isset($model) && $model->getId()) {
                if($type && is_string($type)){
                    $this->_redirect('adminhtml/sales_returns/edit/id/' . $model->getId() . '/type/' .$type);
                }else {
                    $this->_redirect('adminhtml/sales_returns/edit/id/' . $model->getId());
                }
            }else{
                $this->_redirect('adminhtml/sales_returns');
            }
        }else{
            Mage::getSingleton('core/session')->addError(Mage::helper('core')->__('No input data'));
            $this->_redirect('adminhtml/sales_returns');
        }
    }

    public function saveAction()
    {
        try {
            $data =  $this->getRequest()->getPost();

            $createReturnOrder = 0;
            if(isset($data['create_return_order']) && $data['create_return_order'] == 1){
                $createReturnOrder = 1;
            }

            if(isset($data['order_item'])) {
                foreach ($data['order_item'] as $item_id => $itemReturned) {
                    if (isset($itemReturned['qty_return']) && is_numeric($itemReturned['qty_return'])) {
                        $inventoryData = Mage::getModel('opentechiz_inventory/order')->getCollection()
                            ->join(array('item' => 'opentechiz_inventory/item'), 'item.item_id = main_table.item_id', '*')
                            ->addFieldToFilter('order_item_id', $item_id);
                        $allData = $inventoryData->getData();
                        foreach ($allData as $itemData) {
                            if (!$itemData['barcode'] || $itemData['barcode'] == '') {
                                Mage::getSingleton('adminhtml/session')->addError('No Barcode found for returned item.');
                                $this->_redirect('*/*/create/order_id/' . $data['order_id']);
                                return;
                            }
                        }
                    }
                }
            }

            if(isset($data['is_new_rma'])){
                $this->addAction();
                return;
            }
            $return = $this->_initReturn();

            //check if return change solution
            $error = Mage::getModel('opentechiz_return/solution')->changeReturnSolution($return, $data['solution']);
            if($error && $error != 'skipped'){
                Mage::getSingleton('core/session')->addError($error);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }elseif(!$error){
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('core')->__('Solution updated'));
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

            if(!isset($data['solution']) || $data['solution']!=10){
                unset($data['repair_step']);
            }

            if(isset($data['solution']) && $data['solution'] == 10){
                //check if already setup repair steps
                $haveRepairSteps = false;
                if($return->getId()){
                    $repairProcess = Mage::getModel('opentechiz_return/repair')->load($return->getId(), 'return_id');
                    if($repairProcess && $repairProcess->getId()){
                        $haveRepairSteps = true;
                    }
                }

                //error if missing repair steps in return repair
                if(!$haveRepairSteps && (empty($data['repair_step']) || $data['repair_step'] == '[""]')){
                    Mage::getSingleton('core/session')->addError(Mage::helper('core')->__('Please select step repair!'));
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return;
                }
            }

            $order_item = Mage::getModel('sales/order_item')->load($return->getOrderItemId());
            $order = Mage::getModel('sales/order')->load($order_item->getOrderId());

            if(isset($data['solution']) && $data['solution'] == 5) {
                $baseAvailableRefund = $order->getBaseTotalPaid() - $order->getBaseTotalRefunded();
                if (!isset($data['refund_amount']) || !is_numeric($data['refund_amount'])) $data['refund_amount'] = 0;
                if ((floatval($data['refund_amount']) - floatval($return->getRefundAmount())) > $baseAvailableRefund) {

                    Mage::getSingleton('core/session')->addError(Mage::helper('core')->__('Maximum amount available to refund is %s', $order->formatBasePrice($baseAvailableRefund)));
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return;
                }
            }
            if(($data['status'] == 15 || $data['status'] == 52) && empty($return->getStatusDenied())){
                if(Mage::getSingleton('admin/session')->isAllowed('sales/return/allow_decline_approved')){
                    $data['status_denied'] = 2;
                }else{
                    $data['status_denied'] = 1;
                }
                
            }
            if( ($data['status'] != "15" && $data['status'] != "52") && !empty($return->getStatusDenied())){
                $data['status_denied'] = NULL;
                
            }
            $check = $return->getSolution();
            $status = $return->getStatus();
            $data['updated_at'] = now();

            //set increment id by solution
            $incrementId = $return->getIncrementId();
            if($check || (isset($data['solution']) && $data['solution'])){
                if($check){
                    $solution = $check;
                }else{
                    $solution = $data['solution'];
                }
                //get admin user initials
                $admin = Mage::getSingleton('admin/session')->getUser();
                $name = $admin->getFirstname().' '.$admin->getLastname();
                $words = explode(" ", $name);
                $acronym = "";
                foreach ($words as $w) {
                    $acronym .= $w[0];
                }
                $initials = strtoupper($acronym);

                //build id
                $orderItemId = $return->getOrderItemId();
                $orderItem = Mage::getModel('sales/order_item')->load($orderItemId);
                $order = Mage::getModel('sales/order')->load($orderItem->getOrderId());
                $orderIncrement = $order->getIncrementId();

                $prefix = OpenTechiz_Return_Helper_Data::INCREMENT_ID_PREFIX_BY_SOLUTION[$solution];
                $prefix = $prefix.'-'.$initials;

                if(strpos($incrementId, $orderIncrement) === false || strpos($incrementId, $prefix) === false){
                    //save return to generate new increment id
                    $return->addData($data);
                    $return->save();

                    $newIncrementId = $return->generatedIncrementId($return->getId());
                    $data['increment_id'] = $newIncrementId;

                    if($return->getIncrementId()){ //change return ID on order item as well
                        $orderItems = Mage::getModel('sales/order_item')->getCollection()
                            ->addFieldToFilter('description', array('like' => '%'.$return->getIncrementId().'%'));
                        foreach ($orderItems as $orderItemToChangeDesc){
                            $description = $orderItemToChangeDesc->getDescription();
                            $description = str_replace($return->getIncrementId(), $data['increment_id'], $description);
                            $orderItemToChangeDesc->setDescription($description)->save();
                        }
                    }
                }
            }

            $return->addData($data);
            $return->save();
            if(!$return->getIdPaid() && $return->getReturnFee() > 0) $return->sendNotiMail();

            if($check && $status != 46){
                Mage::getSingleton('adminhtml/session')->addSuccess('Return saved');
                if($createReturnOrder == 1){
                    //create return order
                    $this->createOrderAction();
                    return;
                }else {
                    $this->_redirect('*/*/');
                    return;
                }
            }

            $product_id = $order_item->getProductId();
            $product = Mage::getModel('catalog/product')->load($product_id);
            $barcode = $return->getItemBarcode();
            $item = Mage::helper('opentechiz_return')->getItembybarcode($barcode)->getFirstItem();

            if($data['solution']==15 || $data['solution']==10 || $data['solution']==20){
                $production_data = array();
                $production_data['note'] = $data['production_note'];
                $production_data['status'] = OpenTechiz_Production_Helper_Data::PROCESSES[0];
                $production_data['production_type'] = 3;

                $production_data['item_state'] = 0;
                $production_data['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');

                $note = 'Returned from Return #'.$return->getIncrementId();

                if(isset($data['options'])){
                    $option_ar = $data['options'];

                    $result = Mage::helper('opentechiz_return')->getOptionStringFromarray($option_ar,$product);
                    $sku = $result['sku'];

                    $production_data['option'] = serialize($result['option']);
                    $return->setNewOption( $production_data['option']);
                }else{
                    $sku = $order_item->getSku();
                    $production_data['option'] = $item->getOptions();
                    if($data['solution'] == 20){
                        $return->setNewOption($production_data['option']);
                    }

                }

                $itemModel = Mage::getModel('opentechiz_inventory/item');

                $product_option = unserialize($production_data['option']);
                $production_data['order_item_id'] = $return->getOrderItemId();
                $production_data['image'] = $itemModel->imageUrlFromOption($product_option, $product);
                if($data['solution']==10){
                    $production_data['barcode'] = $return->getItemBarcode();
                    $production_data['production_type'] = 2;
                    $return->setStatus(40);
                    $production_data['item_state'] = 4;
                    $item->setState(1)->save();
                }else{
                    if($data['solution']==20){
                        //just change returned item back to stock instead
                        $return->save();
                        if($item->getState()!=45) $item->setState(30)->setNote($note)->save();
                        Mage::getSingleton('core/session')->addSuccess(Mage::helper('core')->__('Exchange request was created'));
                    }
                    if($item->getState()!=45)
                        $item->setState(30)->setNote($note)->save();
                }

                $productRequest = Mage::getModel('opentechiz_production/product');

                $return->save();
                #add repair item
                if(isset($data['repair_step'])){
                    $productRequest->setData($production_data)->save();

                    if(strpos($data['repair_step'], ',"Finished"]') === false)
                        $data['repair_step'] = str_replace(']', ',"Finished"]', $data['repair_step']);
                    $repair_data = ['item_barcode'=>$barcode,'repair_step' => $data['repair_step'] ,'production_id'=>$productRequest->getId(),'return_id' => $return->getId() ];
                    if($step_json = $data['repair_step']){
                        $step_ar = json_decode($step_json,true);
                        $status = array_search ($step_ar[1], OpenTechiz_Production_Helper_Data::PROCESS_LIST);
                        $productRequest->setStatus($status)->save();

                    }
                    $repairModel = Mage::getModel('opentechiz_return/repair');
                    $repairModel->setData($repair_data)->save();
                }
                if(isset($data['stonerequest'])&&$data['solution']==10){
                    $admin = Mage::getSingleton('admin/session')->getUser();
                    $requeststone = [];
                    foreach ($data['stonerequest'] as $key => $value) {
                        # code...

                        $stoneModel = Mage::getModel('opentechiz_material/stone');
                        if(!isset($value['id']))
                            continue;

                        $stone = $stoneModel->load($value['id']);
                        $array = [];
                        $array['id'] = $stone->getId();
                        $array['name'] = $stone->getStoneType();
                        $array['weight'] = $stone->getWeight();
                        $array['shape'] = $stone->getShape();
                        $array['diameter'] = $stone->getDiameter();
                        $array['quality'] = $stone->getQuality();
                        $array['qty'] = $value['qty'];
                        array_push($requeststone, $array);

                        if($admin->getId()){
                            Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                                'user_id' => $admin->getId(),
                                'description' => 'Stone Request for repair item with personalized id #'.$productRequest->getId(),
                                'qty' => '0',
                                'on_hold' => '+'.$array['qty'],
                                'type' => 1,
                                'stock_id' => $array['id']
                            ));
                        }
                    }
                    if(count($requeststone)>0){
                        $requeststonedata = [];
                        $requeststonedata['personalized_id'] = $productRequest->getId();
                        $requeststonedata['created_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                        $requeststonedata['stones'] = serialize($requeststone);
                        Mage::getModel('opentechiz_material/stoneRequest')->setData($requeststonedata)->save();
                        Mage::getModel('opentechiz_material/stoneRequest')->putStoneOnHold(serialize($requeststone));
                    }
                }

                //create return order
                if($createReturnOrder == 1){
                    $this->createOrderAction();
                    return;
                }
                /*
                if(isset($data['options'])&&$data['solution']==10){
                    $itemModel->load($item->getId())->setImage($productRequest->getImage())->setSku($sku)->setOption($productRequest->getOption())->save();
                }*/
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('core')->__('Return Saved.'));
            }

            if($data['solution']==5){

                $order = Mage::getModel('sales/order')->load($order_item->getOrderId());
                $note = $item->getNote();
                $note .= ' Refund from return '.$return->getIncrementId();

                if($order->canCreditmemo()){
                    try {
                        if($order_item) {
                            if($order_item->canRefund()){
                                $service = Mage::getModel('sales/service_order', $order);
                                $data = array(
                                    'qtys' => array(
                                        $order_item->getId() => 1
                                    )
                                );

                                //normal partial refund with custom amount
                                $order_item_amount = $order_item->getRowTotal() + $order_item->getTaxAmount() + $order_item->getHiddenTaxAmount() + Mage::helper('weee')->getRowWeeeAmountAfterDiscount($order_item) - $order_item->getDiscountAmount();
                                $shipping_amount = 0;
                                $adjustment_amount = $adjustment_negative = 0;
                                $baserefund = ($order_item_amount / $order_item->getQtyOrdered()) + $shipping_amount;
                                if($return->getRefundAmount() > $baserefund ){
                                    $adjustment_amount = $return->getRefundAmount() - $baserefund;
                                }
                                if($return->getRefundAmount() <= $baserefund){
                                    $adjustment_negative =  $baserefund - $return->getRefundAmount();
                                }
                                $data = array(
                                    'qtys' =>array( $order_item->getId() => 1),
                                    'adjustment_positive' => $adjustment_amount,
                                    'shipping_amount' => $shipping_amount,
                                    'adjustment_negative' => $adjustment_negative

                                );

                                $creditMemo = $service->prepareCreditmemo($data);
                                $creditMemo->register();
                                $creditMemo->save();

                                $transactionSave = Mage::getModel('core/resource_transaction')
                                    ->addObject($creditMemo)
                                    ->addObject($creditMemo->getOrder());
                                $transactionSave->save();

                                if($item->getState()!=45)
                                    $item->setState(30)->setNote($note)->save();
                                $return->setStatus(10)->save();
                                Mage::getSingleton('core/session')->addSuccess(Mage::helper('core')->__('The credit memo has been created.'));
                            }else {
                                $return->setSolution(0);
                                $return->setRefundAmount(null);
                                $return->save();
                                Mage::getSingleton('core/session')->addError(Mage::helper('core')->__('Order item cannot refunded!'));
                            }
                        } else {
                            $return->setSolution(0);
                            $return->save();
                            Mage::getSingleton('core/session')->addError(Mage::helper('core')->__('Order item not found to credit memo!'));
                        }
                    } catch (Exception $e) {
                        $return->setSolution(0);
                        $return->save();
                        Mage::getSingleton('core/session')->addError(Mage::helper('core')->__('Order cannot create credit memo!'));
                        Mage::throwException(Mage::helper('core')->__($e->getMessage()));
                    }
                }else{
                    $return->setSolution(0);
                    $return->save();
                    Mage::getSingleton('core/session')->addError(Mage::helper('core')->__('Order cannot create credit memo!'));
                }
            }

            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            return;
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setFormData($data);
            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            return;
        }
    }

    public function createOrderAction(){
        $data =  $this->getRequest()->getParams();
        $error = Mage::getModel('opentechiz_return/returnOrder')->createReturnOrder($data);
        if($error['message']){
            Mage::getSingleton('adminhtml/session')->addError($error['message']);
            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            return;
        }else {
            Mage::getSingleton('core/session')->addSuccess(Mage::helper('core')->__('Please check and edit return order'));
            if(isset($error['order_id'])) {
                $this->_redirect('adminhtml/sales_order/view', array('order_id' => $error['order_id']));
            }else{
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
    }

    public function sendMailProblemAction(){
        $data =  $this->getRequest()->getParams();
        if(isset($data['id'])){
            $return = Mage::getModel('opentechiz_return/return')->load($data['id']);
            $return->sendItemProblemMail();
            Mage::getSingleton('adminhtml/session')->addSuccess('Email Sent to notify customer');
            $this->_redirect('*/*/edit', array('id' => $data['id']));
        }else{
            Mage::getSingleton('adminhtml/session')->adderror('No Return Id');
            $this->_redirect('*/*/index');
        }
        return;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/return');
    }
    public function massStatusAction()
    {
        $count = 0;
        $return_ids = $this->getRequest()->getParam('item');
        if (!$return_ids) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($return_ids as $return_id) {
                    $return = Mage::getModel('opentechiz_return/return')->load($return_id);
                    if(!empty($return->getStatusDenied())){
                        $status_denied = $this->getRequest()->getParam('status');
                        if( $return->getStatusDenied() != $status_denied){
                            $return->setStatusDenied($status_denied);
                            $return->save();
                            $count++;
                        }
                        
                    }
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', $count)
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

}