<?php

/**
 * Controller for the Log viewing operations
 *
 *
 * @author Ashley Schroder (aschroder.com)
 * @copyright  Copyright (c) 2014 Ashley Schroder
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

require_once Mage::getModuleDir('controllers', 'Aschroder_SMTPPro').DS.'Smtp'.DS.'LogController.php';
class OpenTechiz_MailLog_Smtp_LogController
	extends Aschroder_SMTPPro_Smtp_LogController {

	public function resendEmailAction(){
		$itemsId = $this->getRequest()->getPost('item_ids');
		if ($itemsId) {
			$collection = Mage::getModel('smtppro/email_log')->getCollection();
			$collection->addFieldToFilter('email_id', array('in'=> $itemsId));
			if ($collection->getSize()) {
				$item_success = array();
				$item_error = array();
				foreach ($collection as $mail_log) {
					if ($mail_log->getData('email_status')) {
						continue;
					}
					Mage::register('resend_smtp', $mail_log);
					$email_to 	= $mail_log->getData('email_to');
			        $subject 	= $mail_log->getData('subject');
			        $body 		= $mail_log->getData('email_body');
			        if ($mail_log->getData('email_from')) {
			        	$sender = $mail_log->getData('email_from');
			        }else{
			        	$sender = Mage::getStoreConfig('trans_email/ident_general/email');
			        }
			        $emailsLog 	= $mail_log->getData('email_to');
			        $emails = preg_split("/[\s,;]+/", $emailsLog);

			        $mail = Mage::getModel('core/email');
			        $mail->setToEmail($emails);
			        $mail->setFromEmail($sender);
			        $mail->setFromName("ItsHot.com");
			        $mail->setType('html');
			        $mail->setSubject($subject);
			        $mail->setBody($body);
			        
	                try {
	                	$mail->send();
	                	$exception = Mage::registry('resend_smtp')->getEmailMessageException();
	                	if (empty($exception)) {
	                		$mail_log->setData('email_status',1);
	                		$item_success[] = $mail_log->getData('email_id');
	                	}else{
	                		$mail_log->setData('email_message_exception',$exception);
	                		$item_error[] = $mail_log->getData('email_id');
	                	}
	                	$mail_log->save();
	                } catch (Exception $e) {
	                	Mage::logException($e);
	                }
	                Mage::unregister('resend_smtp');
				}
				if (count($item_success)) {
					$this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Re-send items %s success.',implode(',', $item_success)));
				}


				if (count($item_error)) {
					$this->_getSession()->addError(Mage::helper('adminhtml')->__('Re-send items %s fail.',implode(',', $item_error)));
				}
			}
		}
		$this->_redirectReferer();
		return;
	}
} 
