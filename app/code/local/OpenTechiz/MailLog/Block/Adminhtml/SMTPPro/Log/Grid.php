<?php

class OpenTechiz_MailLog_Block_Adminhtml_SMTPPro_Log_Grid extends Aschroder_SMTPPro_Block_Log_Grid
{

    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        
        $this->addColumn('email_from', array(
            'header' => $this->__('Sender'),
            'width' => '100',
            'align' => 'left',
            'index' => 'email_from',
        ));
        $this->addColumn('email_status', array(
            'header' => $this->__('Email Status'),
            'width' => '5',
            'align' => 'left',
            'index' => 'email_status',
            'type' => 'options',
            'options' => array(0 => $this->__('Error'), 1 => $this->__('Success')),
            'frame_callback' => array($this, 'decorateStatus')
        ));
        
        $this->addColumn('ip_address', array(
            'header' => $this->__('IP Address'),
            'width' => '10',
            'align' => 'left',
            'index' => 'ip_address',
        ));
        
        $this->addColumn('email_message_exception', array(
            'header' => $this->__('Email Message Exception'),
            'width' => '120',
            'align' => 'left',
            'index' => 'email_message_exception',
        ));
        return $this;
    }

    public function decorateStatus($value, $row, $column, $isExport)
    {
        if ($row->getEmailStatus()) {
            $cell = '<span class="grid-severity-notice"><span>' . $value . '</span></span>';
        } else {
            $cell = '<span class="grid-severity-critical"><span>' . $value . '</span></span>';
        }
        return $cell;
    }

        protected function _prepareMassaction()
    {
        $this->setMassactionIdField('email_id');
        $this->getMassactionBlock()->setFormFieldName('item_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

        $this->getMassactionBlock()->addItem('resend_email', array(
            'label' => $this->__('Re-send'),
            'url' => $this->getUrl('*/*/resendEmail')
        ));

        return $this;
    }

}
