<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('smtppro/email_log'),'ip_address', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => true,
    'length'    => 100,
    'comment'   => 'email status'
    ));   

$installer->endSetup();