<?php


$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('smtppro/email_log'),'email_from', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => false,
    'length'    => 500,
    'comment'   => 'email from',
    'before'     => 'email_to',
    ));

$installer->getConnection()
->addColumn($installer->getTable('smtppro/email_log'),'email_status', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'default'   =>1,
    'nullable'  => false,
    'length'    => 2,
    'comment'   => 'email status'
    ));   



$installer->getConnection()
->addColumn($installer->getTable('smtppro/email_log'),'email_message_exception', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => true,
    'length'    => 1000,
    'comment'   => 'email message exception'
    ));   
$installer->endSetup();