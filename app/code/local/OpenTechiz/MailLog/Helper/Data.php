<?php

/**
 * OnlineBiz Software Solution
 * 
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Joe Vu<joe@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2017 , OnlineBiz Software Solution
 * 
 * Create at: Aug 27, 2018 10:02:32 AM
 */
class OpenTechiz_MailLog_Helper_Data extends Mage_Core_Helper_Abstract
{
	const MARKETPLACE_CUSTOMER_GROUP = "5";
    public function sendMailToAdmin($subject, $message)
    {
        Mage::dispatchEvent('maillog_send_email_to_admin', array(
            'subject' => $subject,
            'message' => $message
                )
        );
    }
    public function canSendEmail($email)
    {
        if (substr($email, -18, 18) == 'noreply@ithost.com' || substr($email, -12, 12) == '@example.com' || $email == '') {//treat these emails as blocked
          return false;
        }

	$customer = Mage::getModel('customer/customer')->setWebsiteId(1)->loadByEmail($email);
        if($customer->getId()){
            if($customer->getGroupId() == self::MARKETPLACE_CUSTOMER_GROUP){ // not send email with customer group marketplace
                return false;
            }
        }

        return true;
    }
}
