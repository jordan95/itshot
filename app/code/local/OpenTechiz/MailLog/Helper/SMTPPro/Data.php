<?php

class OpenTechiz_MailLog_Helper_SMTPPro_Data extends Aschroder_SMTPPro_Helper_Data
{

    public function logEmailSent($to, $template, $subject, $email,  $isHtml, $from ="", $status = 1, $message_exception = "")
    {
        if ($this->isLogEnabled()) {
            if (Mage::registry('resend_smtp')) {
                $log = Mage::registry('resend_smtp');
                $log->setLogAt(now());
            }else{
                $log = Mage::getModel('smtppro/email_log');
            }

                $log->setEmailTo($to)
                    ->setTemplate($template)
                    ->setSubject($subject)
                    ->setEmailBody($isHtml ? $email : nl2br($email))
                    ->setEmailFrom($from)
                    ->setEmailStatus($status)
                    ->setEmailMessageException($message_exception);
            $clientIp = Mage::helper('ipsecurity/customcode')->getip_index();
            $log->setIpAddress($clientIp);
            $log->save();
        }
        return $this;
    }

}
