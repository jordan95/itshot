<?php

class OpenTechiz_MailLog_Model_SMTPPro_Email extends Aschroder_SMTPPro_Model_Email
{

    public function send()
    {

        $_helper = Mage::helper('smtppro');

        // If it's not enabled, just return the parent result.
        if (!$_helper->isEnabled()) {
            if (!(isset($_SERVER['HTTP_HOST']) && (strpos($_SERVER['HTTP_HOST'], 'dev') !== FALSE || strpos($_SERVER['HTTP_HOST'], 'test') !== FALSE || strpos($_SERVER['HTTP_HOST'], 'stage') !== FALSE || strpos($_SERVER['HTTP_HOST'], 'demo') !== FALSE || strpos($_SERVER['HTTP_HOST'], 'local') !== FALSE || strpos($_SERVER['HTTP_HOST'], '127.0.0.1') !== FALSE))) {
                return parent::send();
            }
            return $this;
        }

        if (Mage::getStoreConfigFlag('system/smtp/disable')) {
            return $this;
        }

        $mail = new Zend_Mail();
        if(!is_array($this->getToEmail())){
            if(!Mage::helper('maillog')->canSendEmail($this->getToEmail())){
                 return $this;
            }

        }
        if (strtolower($this->getType()) == 'html') {
            $mail->setBodyHtml($this->getBody());
        } else {
            $mail->setBodyText($this->getBody());
        }

        $mail->setFrom($this->getFromEmail(), $this->getFromName())
                ->addTo($this->getToEmail(), $this->getToName())
                ->setSubject($this->getSubject());

        $transport = new Varien_Object(); // for observers to set if required
        Mage::dispatchEvent('aschroder_smtppro_before_send', array(
            'mail' => $mail,
            'email' => $this,
            'transport' => $transport
        ));
        $success = true;
        if (!(isset($_SERVER['HTTP_HOST']) && (strpos($_SERVER['HTTP_HOST'], 'dev') !== FALSE || strpos($_SERVER['HTTP_HOST'], 'test') !== FALSE || strpos($_SERVER['HTTP_HOST'], 'stage') !== FALSE || strpos($_SERVER['HTTP_HOST'], 'demo') !== FALSE || strpos($_SERVER['HTTP_HOST'], 'local') !== FALSE || strpos($_SERVER['HTTP_HOST'], '127.0.0.1') !== FALSE))) {

            if ($transport->getTransport()) { // if set by an observer, use it
                $success = $mail->send($transport->getTransport());
            } else {
                $success = $mail->send();
            }
        }

        if ($success) {
            Mage::dispatchEvent('aschroder_smtppro_after_send', array(
                'to' => $this->getToName(),
                'subject' => $this->getSubject(),
                'template' => "n/a",
                'html' => (strtolower($this->getType()) == 'html'),
                'email_body' => $this->getBody(),
                'from' => $this->getFromEmail(),
                    )
            );
        }

        return $this;
    }

}
