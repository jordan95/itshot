<?php

/**
 * OnlineBiz Software Solution
 * 
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Joe Vu<joe@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2018, OnlineBiz Software Solution
 * 
 * Create at: Aug 27, 2018 2:27:27 PM
 */
class OpenTechiz_MailLog_Model_Observer
{

    const DAILY_REPORT_NAMES_WITH_DATE = [
        'DailySales Email',
        'DailySold Email',
        'DailyReturn Email',
        'Order Progress Manufacturing Report Email',
        'Order Progress Waiting on Vendor Report Email',
        'Stock Become More Than Zero Email',
        'Check/Cash Order Email',
        'Inventory ReOrder Report'
    ];
    const XML_PATH_MAILLOG_RECIPIENT_EMAIL = 'maillog/general/recipient_email';
    const XML_PATH_MAILLOG_RECIPIENT_NAME = 'maillog/general/recipient_name';
    const MAILLOG_CC_EMAIL_REGITRY = 'addMaillogCc';
    const VARIABLE_PRICE_ORDER = 'op_difference_price_order';
    const MARKETPLACE_CUSTOMER_GROUP = 5;

    public function sendEmailToAdmin(Varien_Event_Observer $observer)
    {
        $recipient_name = Mage::getStoreConfig(self::XML_PATH_MAILLOG_RECIPIENT_NAME);
        $recipient_email = Mage::getStoreConfig(self::XML_PATH_MAILLOG_RECIPIENT_EMAIL);
        $message = $observer->getMessage();
        $subject = $observer->getSubject();

        /* @var $mail Aschroder_SMTPPro_Model_Email */
        $emails = preg_split("/[\s,;]+/", $recipient_email);
        $toEmail = current($emails);
        if (!Mage::registry(self::MAILLOG_CC_EMAIL_REGITRY) && count($emails) > 1) {
            $ccEmails = array();
            foreach ($emails as $e) {
                if ($e != $toEmail) {
                    $ccEmails[] = $e;
                }
            }
            Mage::register(self::MAILLOG_CC_EMAIL_REGITRY, $ccEmails);
        }
        $mail = Mage::getModel('core/email');
        $mail->setToName($recipient_name);
        $mail->setToEmail($toEmail);
        $mail->setFromEmail("sales@itshot.com");
        $mail->setFromName("ItsHot.com");
        $mail->setType('html');
        $mail->setSubject($subject);
        $mail->setBody($message);

        try {
            $mail->send();
        } catch (Exception $e) {
            
        }
    }

    public function addMailCc(Varien_Event_Observer $observer)
    {
        if (!Mage::registry(self::MAILLOG_CC_EMAIL_REGITRY)) {
            return;
        }

        $mail = $observer->getMail();
        $emails = Mage::registry(self::MAILLOG_CC_EMAIL_REGITRY);
        $mail->addCc($emails);
    }

//    public function notifyCronRunningDaily()
//    {
//        $this->notifyAdmin(
//                "Cronjob is running " . date("d-m-Y H:i:s"), 
//                'ItsHot.com: Cronjob is running'
//        );
//    }

    public function notifyAdmin($message, $subject)
    {
        Mage::dispatchEvent('maillog_send_email_to_admin', array(
            'message' => $message,
            'subject' => $subject
                )
        );
    }

    public function cronjobMailCheckingDaily()
    {
        $timeChecking = date('Y-m-d H:i:s', time() - 60 * 60 * 24);

        $mail_subject = trim(Mage::getStoreConfig('maillog/mail_checking/mail_subject'));
        if (!Mage::getStoreConfig('maillog/mail_checking/enable') || !$mail_subject) {
            return;
        }

        $mail_subjects = preg_split('/(\s*\r*\n)+/', $mail_subject);

        $sqlString = [];
        foreach ($mail_subjects as $mail_subject) {
            $sqlString[] = array('like' => $mail_subject . '%');
        }

        $collection = Mage::getModel('smtppro/email_log')->getCollection();
        $collection->addFieldToFilter('subject', $sqlString);
        $collection->getSelect()->where('log_at > ?', $timeChecking);
        $collection->getSelect()->reset(Zend_Db_Select::COLUMNS);
        $collection->getSelect()->columns(array('subject'));
        $collection->getSelect()->group(array('subject'));

        $subjects = array();
        if ($collection->count() != 0) {
            foreach ($collection as $log) {
                $subjects[] = $log->getData('subject');
            }

            $sub = $mail_subjects;
            foreach ($sub as $key => $mail_subject) {
                foreach ($subjects as $subject) {
                    if (stripos($subject, $mail_subject) !== false) {
                        unset($sub[$key]);
                    }
                }
            }

            if (count($sub) > 0) {
                $this->notifyAdmin(
                        "The list cronjobs are stop working : <br/>" . join('<br/>', $sub) . '<br/> Date:' . $timeChecking . '<br/> Datetime: ' . date('Y-m-d H:i:s'), "Some cron jobs might be not working"
                );
            } else {
                $this->notifyAdmin(
                        "Cronjob is running " . date("d-m-Y H:i:s"), 'ItsHot.com: Cronjob is running'
                );
            }
        } else {
            $this->notifyAdmin(
                    "The list cronjobs are stop working : <br/>" . join('<br/>', $mail_subjects) . '<br/> Date:' . $timeChecking . '<br/> Datetime: ' . date('Y-m-d H:i:s'), "Some cron jobs might be not working"
            );
        }
        $this->checkFaildEmailSend($timeChecking);
    }

    public function cronjobCheckingOrderPrice()
    {
        $custom_variable_plain_value = '';
        $list_order_id = array();
        $list_increment_id = array();
        $storeId = Mage::app()->getStore()->getId();
        $custom_variable_plain_value = Mage::getModel('core/variable')->setStoreId($storeId)
                ->loadByCode(self::VARIABLE_PRICE_ORDER)
                ->getValue('plain');

        if ($custom_variable_plain_value != '') {
            $orders = Mage::getModel('sales/order')->getCollection()
                    ->setOrder('entity_id', 'DESC')
                    ->setPageSize(1)
                    ->getFirstItem();
            $collection_order_item = Mage::getModel('sales/order_item')->getCollection()->addAttributeToSelect('order_id')->addAttributeToFilter('original_price', array('neq' => new Zend_Db_Expr('price')))->addAttributeToFilter('order_id', array('gt' => $custom_variable_plain_value));
            if ($collection_order_item) {
                foreach ($collection_order_item->getData() as $key => $value) {
                    $list_order_id[] = (int) $value['order_id'];
                }
                if (!empty($list_order_id)) {

                    $collection_orders = Mage::getModel('sales/order')->getCollection()->addAttributeToSelect('increment_id')->addAttributeToSelect('customer_email')->addAttributeToFilter('entity_id', array('in' => $list_order_id))->addAttributeToFilter('order_type', array('nin' => array(2, 3)))->addAttributeToFilter('source', array('nin' => OpenTechiz_SalesExtend_Helper_Data::MARKETPLACE_ORDER_SOURCE));
                    if ($collection_orders) {
                        foreach ($collection_orders->getData() as $key => $value) {
                            $store = Mage::app()->getStore(1);
                            $websiteId = $store->getWebsite()->getId();
                            $customer = Mage::getModel('customer/customer')
                                    ->setWebsiteId($websiteId)
                                    ->loadByEmail($value['customer_email']);
                            if (!empty($customer)) {
                                if ($customer->getGroupId() == self::MARKETPLACE_CUSTOMER_GROUP) {
                                    continue;
                                }
                            }

                            $list_increment_id[] = $value['increment_id'];
                        }
                        if (!empty($list_increment_id)) {
                            $list_increment_id = join(',', $list_increment_id);
                            $this->notifyAdmin(
                                    "This is a list of orders have original_price different with price:<br/>  " . $list_increment_id .
                                    "<br/>  Datetime: " . date('Y-m-d H:i:s'), "List of orders with different prices"
                            );
                        }
                    }
                }
            }
            Mage::getModel('core/variable')
                    ->loadByCode(self::VARIABLE_PRICE_ORDER)
                    ->setPlainValue($orders->getId())
                    ->save();
        }
    }

    public function cronjobOrderBillingDaily()
    {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection('core_write');
        /* @var $select Zend_Db_Select */
        $select = $read->select();
        $select->from(array('main_table' => $resource->getTableName('sales/order')), array('entity_id'));
        $select->join(array('payment' => $resource->getTableName('sales/order_payment')), "main_table.entity_id = payment.parent_id AND payment.method = 'authorizenet'", array());
        $select->joinLeft(array('invoice' => $resource->getTableName('sales/invoice')), "main_table.entity_id = invoice.order_id", array());
        $select->where("main_table.created_at <= DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 5 DAY)");
        $select->where("main_table.notify_order_fulfillment = 0");
        $select->where("main_table.state NOT IN ('canceled', 'closed', 'holded')");
        $select->where("main_table.status != 'processing_manual'");
        $select->where("NOT (main_table.order_stage = 0 AND main_table.internal_status = 14)");
        $select->where("main_table.base_grand_total > 0");
        $select->where("main_table.base_total_paid = 0 OR main_table.base_total_paid IS NULL");
        $select->where("main_table.order_type = 0");
        $select->where("invoice.entity_id IS NULL");
        $select->limit(50);
        $ids = $read->fetchCol($select);
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $order = Mage::getModel('sales/order')->load($id);
                if (!$order->getId()) {
                    continue;
                }
                $subject = Mage::getStoreConfig("opentechiz_salesextend/notify_billing/subject");
                $subject = str_replace("{order_number}", $order->getIncrementId(), $subject);
                $days = 7;
                $expired_date = Mage::getModel('core/date')->date('Y-m-d', strtotime($order->getCreatedAt() . " + {$days} days"));
                $message = "Authorize Approval may expire soon. Charge order #{$order->getIncrementId()} before {$expired_date}.";
                $canSendMail = Mage::helper('odoo')->isEnabledSendEmail();

                try {
                    // Submit ticket to Odoo
                    Mage::getModel('odoo/api_odoo')->submitTicket([
                        "message" => $message,
                        "ticket_subject" => $subject,
                        "team_id" => 'Billing',
                    ]);
                } catch (Exception $exc) {
                    Mage::logException($exc);
                }

                if ($canSendMail) {
                    // Send email if submit ticket Odoo fail or Odoo disabled
                    Mage::helper("opentechiz_salesextend")->sendEmailOrderBilling($order, $days);
                }

                $write->query("UPDATE {$resource->getTableName('sales/order')} SET notify_order_fulfillment = 1 WHERE entity_id = " . $id);
            }
        }
    }
    public function checkFaildEmailSend($timeChecking){
        $subjects = array();
        $collection = Mage::getModel('smtppro/email_log')->getCollection();
        $collection->addFieldToFilter('email_status', array('eq'=>0));
        $collection->getSelect()->where('log_at > ?', $timeChecking);
        $collection->getSelect()->reset(Zend_Db_Select::COLUMNS);
        $collection->getSelect()->columns(array('subject'));
        $collection->getSelect()->group(array('subject'));
        if ($collection->count() > 0) {
            foreach ($collection as $log) {
                $subjects[] = $log->getData('subject');
            }

            if (!empty($subjects)) {
                $this->notifyAdmin(
                        "The list failed emails : <br/>" . join('<br/>', $subjects) . '<br/> Date:' . $timeChecking . '<br/> Datetime: ' . date('Y-m-d H:i:s'), "The list failed emails"
                );
            }
        }
        
    }
}
