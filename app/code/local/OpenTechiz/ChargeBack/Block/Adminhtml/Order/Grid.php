<?php

class OpenTechiz_Chargeback_Block_Adminhtml_Order_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('chargeBackGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);

    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_chargeback/order')->getCollection()
            ->join(array('order' => 'sales/order'), 'order.entity_id = main_table.order_id', '*')
            ->join(array('order_status' => 'sales/order_status'), 'order_status.status = order.status', '*');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Id'),
            'index' => 'id',
            'width' => '100',
            'type'  => 'hidden'
        ));
        $this->addColumn('order_increment_id', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Order #'),
            'index' => 'order_increment_id',
            'width' => '100'
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Order Status'),
            'width' => '100px',
            'index' => 'label',
        ));
        $this->addColumn('reason', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Reason'),
            'width' => '150px',
            'index' => 'reason',
            'type'  => 'options',
            'options'=> OpenTechiz_ChargeBack_Helper_Data::REASONS
        ));
        $this->addColumn('date', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Date Replied'),
            'width' => '80px',
            'index' => 'date',
            'type'  => 'date',
        ));
        $this->addColumn('outcome', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Outcome'),
            'width' => '100px',
            'index' => 'outcome',
            'type'  => 'options',
            'options' => array(
                'won' => 'Won',
                'lost'=> 'Lost'
            )
        ));
        $this->addColumn('sign_cover', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Sign Covered'),
            'width' => '100px',
            'index' => 'sign_cover',
            'type'  => 'options',
            'options' => array(
                1 => 'Yes',
                2 => 'No'
            )
        ));
        $this->addColumn('note', array(
            'header' => Mage::helper('opentechiz_inventory')->__('Note'),
            'width' => '200px',
            'index' => 'note',
        ));

        $this->addColumn('action',
            array(
                'header' => Mage::helper('opentechiz_chargeback')->__('Action'),
                'width' => '50',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('opentechiz_chargeback')->__('Edit'),
                        'url' => array('base' => '*/*/editChargeback'),
                        'field' => 'id',
                        'data-column' => 'action',
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
                'align' => 'center'
            ));
        return parent::_prepareColumns();
    }
}