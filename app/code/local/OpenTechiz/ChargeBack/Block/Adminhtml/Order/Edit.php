<?php

class OpenTechiz_Chargeback_Block_Adminhtml_Order_Edit extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_chargeback_order';
    protected $_blockGroup = 'opentechiz_chargeback';
    protected $_headerText = 'Edit Order Chargeback';

    protected function _prepareLayout()
    {
        $head = $this->getLayout()->getBlock('head');
        $head->setTitle('Edit Order Chargeback');

        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function _toHtml()
    {
        $this->setTemplate('opentechiz/charge_back/edit.phtml');

        return parent::_toHtml();
    }

    public function getCurrentChargeBackEntity(){
        $params = $this->getRequest()->getParams();
        $id = $params['id'];
        return Mage::getModel('opentechiz_chargeback/order')->load($id);
    }

    public function getFormAction(){
        return $this->getUrl('adminhtml/order/editChargeBackSave');
    }

    public function getHelper(){
        return Mage::helper('opentechiz_chargeback');
    }
    
}