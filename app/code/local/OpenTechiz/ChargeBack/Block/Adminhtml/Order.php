<?php

class OpenTechiz_ChargeBack_Block_Adminhtml_Order extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_order';
        $this->_blockGroup = 'opentechiz_chargeback';
        $this->_headerText = Mage::helper('opentechiz_chargeback')->__('Order Chargeback');
        parent::__construct();
        $this->_removeButton('add');
    }
}