<?php

class OpenTechiz_ChargeBack_Adminhtml_OrderController extends Mage_Adminhtml_Controller_Action
{
    protected $_comment = 'Chargeback Received. Reason: ';

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales')
            ->_title($this->__('Order Chargeback'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_chargeback/adminhtml_order'));
        return $this;
    }

    public function chargeBackIndexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_chargeback/adminhtml_order_grid')->toHtml()
        );
    }

    public function editChargebackAction(){
        $params = $this->getRequest()->getParams();
        if(!isset($params['id'])){
            $this->_getSession()->addError(Mage::helper('adminhtml')->__('Missing Id Param.'));
            $this->_redirect('*/*/chargeBackIndex');
            return;
        }
        $this->_title($this->__('Order Chargeback - Edit'));
        $this->loadLayout()
            ->_setActiveMenu('sales');
        $this->_addContent($this->getLayout()->createBlock('opentechiz_chargeback/adminhtml_order_edit'));
        $this->renderLayout();
    }

    public function editChargeBackSaveAction(){
        $params = $this->getRequest()->getParams();
        $charge = Mage::getModel('opentechiz_chargeback/order')->load($params['charge_id']);
        $data = [];
        $data['reason'] = $params['reason'];
        $data['date'] = $params['charge_back_date_replied'];
        $data['outcome'] = $params['outcome'];
        $data['sign_cover'] = $params['sign_cover'];
        $data['note'] = $params['note'];
        $charge->addData($data)->save();

        $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Saved successfully.'));
        $this->_redirect('*/*/chargeBackIndex');
    }

    public function saveChargeBackAction()
    {
        $params = $this->getRequest()->getPost();
        $orderId = $params['order_id'];

        $order = Mage::getModel('sales/order')->load($orderId);
        $chargeBackModel = Mage::getModel('opentechiz_chargeback/order');

        $data = [];
        $data['order_id'] = $orderId;
        $data['order_increment_id'] = $order->getIncrementId();
        $data['reason'] = $params['reason'];
        $data['date'] = $params['charge_back_date_replied'];
        $data['outcome'] = $params['outcome'];
        $data['sign_cover'] = (int)$params['sign_covered'];
        $data['note'] = $params['note'];

        $chargeBackExist = $chargeBackModel->load($orderId, 'order_id');
        if($chargeBackExist && $chargeBackExist->getId()){
            $chargeBackExist->addData($data)->save();
        }else{
            Mage::getModel('opentechiz_chargeback/order')->setData($data)->save();
        }

        //if reason is "unauthorized" when form is saved please make the customer as "Fraud"
        if($params['reason'] == 'unauthorized'){
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $query = "UPDATE tsht_customer_entity SET is_fraudster = 1 WHERE entity_id = '" .$order->getCustomerId()."'";
            $writeConnection->query($query);
        }

        //add a note to the order
        $reason = OpenTechiz_ChargeBack_Helper_Data::REASONS[$params['reason']];
        $order->addStatusHistoryComment($this->_comment . $reason);
        $order->save();

        $this->_redirect('*/sales_order/view/order_id/'.$order->getId());
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/sales/chargeback');
    }
}