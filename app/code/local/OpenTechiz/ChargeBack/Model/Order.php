<?php

class OpenTechiz_ChargeBack_Model_order extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('opentechiz_chargeback/order');
    }
}