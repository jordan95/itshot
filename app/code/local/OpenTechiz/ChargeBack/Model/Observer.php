<?php
class OpenTechiz_ChargeBack_Model_Observer
{
    public function adminhtmlWidgetContainerHtmlBefore($event)
    {
        $block = $event->getBlock();
        $isAllowed = Mage::getSingleton('admin/session')->isAllowed('admin/sales/chargeback');

        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View && $isAllowed) {
            $params = Mage::app()->getRequest()->getParams();
            $orderId = $params['order_id'];

            $isChargedBack = false;
            $chargeBackCollection = Mage::getModel('opentechiz_chargeback/order')->getCollection()
                ->addFieldToFilter('order_id', $orderId);
            if(count($chargeBackCollection)){
                $isChargedBack = true;
            }

            if(!$isChargedBack) {
                $block->addButton('charge_back', array(
                    'label' => Mage::helper('opentechiz_chargeback')->__('Chargeback'),
                    'onclick' => 'openChargeBackPopup('.$orderId.'); return false;'
                ),1,7);
            }

        }
    }
}