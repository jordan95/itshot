<?php
class OpenTechiz_ChargeBack_Helper_Data extends Mage_Core_Helper_Abstract
{
	const REASONS = [
	    'unauthorized'      => 'Unauthorized',
        'not received'      => 'Not Received',
        'received late'     => 'Received Late',
        'not as described'  => 'Not As Described',
        'other'             => 'Other'
    ];

	const SIGN_COVERED = [
	    1 => 'Yes',
        2 => 'No'
    ];

	const OUTCOME = [
        'won' => 'Won',
        'lost'=> 'Lost'
    ];

	public function dateFormat($date){
        $newDate = date("m/d/Y", strtotime($date));
        return $newDate;
    }
}
	 