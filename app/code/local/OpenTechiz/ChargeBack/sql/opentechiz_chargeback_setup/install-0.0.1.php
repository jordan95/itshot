<?php

$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('opentechiz_chargeback/order'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Id')
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'order Id')
    ->addColumn('order_increment_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false
    ), 'order increment id')
    ->addColumn('reason',Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => true
    ), 'reason')
    ->addColumn('date',Varien_Db_Ddl_Table::TYPE_DATE, null, array(
        'nullable'  => false,
    ), 'date')
    ->addColumn('outcome',Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
    ), 'outcome')
    ->addColumn('sign_cover',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
    ), 'sign covered')
    ->addColumn('note',Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        'nullable'  => false,
    ), 'note');
$installer->getConnection()->createTable($table);

$installer->endSetup();
