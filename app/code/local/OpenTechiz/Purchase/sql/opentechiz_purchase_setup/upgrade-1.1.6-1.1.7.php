<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_purchase/purchase')}
    ADD `notified_at` datetime 
    NULL
    DEFAULT NULL;
");
$installer->endSetup();