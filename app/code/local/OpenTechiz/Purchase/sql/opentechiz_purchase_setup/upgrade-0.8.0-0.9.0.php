<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('purchase_order')} 
    CHANGE `total_include_tax`  `total_include_tax`  DOUBLE(12,4) NOT NULL DEFAULT '0.0000';
");
$installer->endSetup();