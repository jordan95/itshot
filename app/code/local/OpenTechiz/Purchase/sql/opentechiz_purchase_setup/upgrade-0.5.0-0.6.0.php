<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    
    ALTER TABLE {$this->getTable('opentechiz_purchase/orderitem')} 
    CHANGE `option` `option`    TEXT NOT NULL ;
    
");
$installer->endSetup();
