<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('opentechiz_purchase/poitem')};
    CREATE TABLE {$this->getTable('opentechiz_purchase/poitem')} (
      `id`   INT(11) UNSIGNED         NOT NULL AUTO_INCREMENT,
      `po_id`      INT(11)       NOT NULL,
      `item_id`      INT(11)           NOT NULL,
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
