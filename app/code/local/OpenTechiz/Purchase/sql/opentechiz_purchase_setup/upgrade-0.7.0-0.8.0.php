<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_purchase/purchasegold')} 
    CHANGE `delivered_qty`  `delivered_qty`  DOUBLE(12,4) NOT NULL DEFAULT '0.0000';
     ALTER TABLE {$this->getTable('opentechiz_purchase/purchasegold')} 
    CHANGE `qty`  `qty`  DOUBLE(12,4) NOT NULL DEFAULT '0.0000'
");
$installer->endSetup();
