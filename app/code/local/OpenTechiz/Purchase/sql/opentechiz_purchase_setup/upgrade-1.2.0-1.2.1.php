<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `tsht_purchase_order` ADD `is_purchase_notified` INT(1) NOT NULL DEFAULT '0';");
$installer->run("UPDATE `tsht_purchase_order` SET `is_purchase_notified` = 1;");
$installer->endSetup();
