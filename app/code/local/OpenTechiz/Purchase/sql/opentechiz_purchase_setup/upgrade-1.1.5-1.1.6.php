<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_purchase/purchase')}
    ADD `is_supplier_notified` SMALLINT 
    NULL
    DEFAULT NULL;
");
$installer->endSetup();