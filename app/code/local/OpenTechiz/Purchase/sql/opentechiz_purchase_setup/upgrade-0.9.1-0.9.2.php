<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_purchase/purchase')} 
    ADD `sup_sales_rep`   text
    DEFAULT '';
");
$installer->endSetup();
