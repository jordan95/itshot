<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_purchase/purchase')}
    ADD `is_pending_po` int
    not NULL
    DEFAULT 0;
");
$installer->endSetup();