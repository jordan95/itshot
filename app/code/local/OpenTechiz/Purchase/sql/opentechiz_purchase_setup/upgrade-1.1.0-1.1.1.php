<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    ALTER TABLE `tsht_purchase_return` CHANGE `total` `total` FLOAT(11) NOT NULL;
    "
);
$installer->endSetup();