<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_purchase/purchase')}
    ADD UNIQUE (po_increment_id); 
");
$installer->endSetup();