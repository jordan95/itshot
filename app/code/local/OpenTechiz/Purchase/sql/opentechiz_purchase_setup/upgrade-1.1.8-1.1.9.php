<?php

$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `{$this->getTable('opentechiz_purchase/supplier')}` ADD `username` VARCHAR(250) NULL DEFAULT NULL AFTER `email`, "
        . "ADD `password` VARCHAR(250) NULL DEFAULT NULL AFTER `username`,"
        . "ADD `password_hash` VARCHAR(250) NULL DEFAULT NULL AFTER `password`;");
$installer->run("ALTER TABLE `{$this->getTable('opentechiz_purchase/purchase')}` "
. "ADD `confirmed` BOOLEAN NULL DEFAULT NULL AFTER `notified_at`, "
. "ADD `shipment_date` DATE NULL DEFAULT NULL AFTER `confirmed`;");
$installer->run("ALTER TABLE `{$this->getTable('opentechiz_purchase/orderitem')}` "
. "ADD `shipped_qty`  INT(11) UNSIGNED NULL DEFAULT NULL AFTER `qty`;");
$installer->endSetup();
