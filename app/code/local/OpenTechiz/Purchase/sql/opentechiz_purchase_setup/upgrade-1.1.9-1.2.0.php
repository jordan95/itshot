<?php
$installer = $this;
$installer->startSetup();
$installer->run("
		-- DROP TABLE IF EXISTS {$this->getTable('opentechiz_purchase/materialreceipt')};
		CREATE TABLE {$this->getTable('opentechiz_purchase/materialreceipt')} (
		`id` int(11) NOT NULL auto_increment,
		`supplier_id` int(11) NULL,
		`created_at` timestamp NULL,
		PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
		-- DROP TABLE IF EXISTS {$this->getTable('opentechiz_purchase/materialreceiptitem')};
		CREATE TABLE {$this->getTable('opentechiz_purchase/materialreceiptitem')} (
		`id` int(11) NOT NULL auto_increment,
		`material_receipt_id` int(11) NOT NULL,
		`product_supplier_id` int(11) NOT NULL,
		`po_item_id` int(11) default NULL,
		PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("ALTER TABLE `tsht_purchase_order_item` ADD `receipt_item_id` INT(11) NULL AFTER `shipment_reference`;");
$installer->run("ALTER TABLE `tsht_purchase_supplier` ADD `is_active_portal` INT(1) NOT NULL DEFAULT '0' COMMENT 'Is Active Portal' AFTER `email`;");
$installer->endSetup();