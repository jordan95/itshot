<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('purchase_stone_supplier')};
    CREATE TABLE {$this->getTable('purchase_stone_supplier')} (
      `purchase_stone_supplier_id`       INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `supplier_id`          INT           NOT NULL,
      `stone_id`             INT           NOT NULL,
      `price`                FLOAT         NOT NULL,
      PRIMARY KEY (`purchase_stone_supplier_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
      
      DROP TABLE IF EXISTS {$this->getTable('purchase_gold_supplier')};
    CREATE TABLE {$this->getTable('purchase_gold_supplier')} (
      `purchase_gold_supplier_id`       INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `supplier_id`          INT           NOT NULL,
      `gold_id`              INT           NOT NULL,
      `price`                FLOAT         NOT NULL,
      PRIMARY KEY (`purchase_gold_supplier_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
$installer->endSetup();