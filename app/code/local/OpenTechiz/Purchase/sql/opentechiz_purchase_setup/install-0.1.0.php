<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('purchase_supplier')};
    CREATE TABLE {$this->getTable('purchase_supplier')} (
      `supplier_id`   INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `name`          VARCHAR(45)       NOT NULL,
      `address`       VARCHAR(255)      NOT NULL,
      `zipcode`       INT(10)           NOT NULL,
      `country`       VARCHAR(45)       NOT NULL,
      `city`          VARCHAR(45)       NULL,
      `telephone`     varchar(45)           NOT NULL,
      `fax`           INT(20)           NULL,
      `email`         VARCHAR(255)       NOT NULL,
      `website`       VARCHAR(45)       NULL,
      `contact`       TEXT       NOT NULL,
      PRIMARY KEY (`supplier_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
$installer->endSetup();