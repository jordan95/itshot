<?php
$installer = $this;
$installer->startSetup();
$installer->run("
		-- DROP TABLE IF EXISTS {$this->getTable('opentechiz_purchase/itemdelivery')};
		CREATE TABLE {$this->getTable('opentechiz_purchase/itemdelivery')} (
		`id` int(11) NOT NULL auto_increment,
		`po_item_id` int(11) NULL,
		`old_shipped_qty` int(11) NULL,
		`new_shipped_qty` int(11) NULL,
		`change_qty` int(11) NULL,
		`created_at` timestamp NULL,
		PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");


$installer->run("ALTER TABLE `tsht_purchase_order_item` ADD `shipment_date` DATETIME NULL DEFAULT NULL AFTER `delivered_qty`;");
$installer->run("UPDATE `tsht_purchase_order_item` as main_table left join `tsht_purchase_order` as `po` ON `main_table`.`po_id` = `po`.`po_id` SET main_table.`shipment_date` = `po`.`shipment_date`;");
$installer->run("ALTER TABLE tsht_purchase_supplier DROP password;");
$installer->endSetup();
