<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

try {
    $connection = $installer->getConnection();
    $tableName = $installer->getTable('opentechiz_inventory/instock');
    $connection->addColumn(
        $tableName,
        'tag_sku',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 255,
            'comment' => 'Tag SKU',
        )
    );

    $connection->addIndex(
        $tableName, $installer->getIdxName('opentechiz_inventory/instock', array('tag_sku')),
        array('tag_sku')
    );
}catch (Exception $e){

}

$installer->endSetup();
