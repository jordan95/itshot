<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->run("ALTER TABLE `tsht_purchase_order` ADD `user_id` INT(11) NULL DEFAULT NULL;");

$installer->endSetup();
