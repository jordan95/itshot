<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_purchase/orderitem')} 
    ADD `delivered_qty`   INT(11)
    NOT NULL
    DEFAULT 0
    AFTER `qty`;
    ALTER TABLE {$this->getTable('opentechiz_purchase/purchasegold')} 
    ADD `delivered_qty`   INT(11)
    NOT NULL
    DEFAULT 0
    AFTER `qty`;

    ALTER TABLE {$this->getTable('opentechiz_purchase/purchasestone')} 
    ADD `delivered_qty`   INT(11)
    NOT NULL
    DEFAULT 0
    AFTER `qty`;
");
$installer->endSetup();
