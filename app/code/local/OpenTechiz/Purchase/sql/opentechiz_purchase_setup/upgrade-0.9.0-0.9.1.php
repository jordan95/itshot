<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_purchase/productsupplier')} 
    ADD `sup_sku`   VARCHAR(45)
    NOT NULL
    DEFAULT 0;
");
$installer->endSetup();
