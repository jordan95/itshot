<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('opentechiz_purchase/productsupplier')};
    CREATE TABLE {$this->getTable('opentechiz_purchase/productsupplier')} (
      `id`   INT(11) UNSIGNED         NOT NULL AUTO_INCREMENT,
      `product_id`      INT(11)       NOT NULL,
      `sup_id`      INT(11)           NOT NULL,
      `price`    double(12,4)         NOT NULL DEFAULT '0.0000',
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('opentechiz_purchase/orderitem')};
    CREATE TABLE {$this->getTable('opentechiz_purchase/orderitem')} (
      `id`   INT(11) UNSIGNED         NOT NULL AUTO_INCREMENT,
      `product_id`      INT(11)       NOT NULL,
      `po_id`      INT(11)       NOT NULL,
      `sku`       VARCHAR(255)      NOT NULL,
      `option`       VARCHAR(255)      NOT NULL,
      `qty`      INT(11)           NOT NULL,
      `price`    double(12,4)         NOT NULL DEFAULT '0.0000',
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
$installer->endSetup();