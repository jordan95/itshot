<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('purchase_order')};
    CREATE TABLE {$this->getTable('purchase_order')} (
      `po_id`                       INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `po_increment_id`             VARCHAR(45)       NOT NULL,
      `created_at`                  DATETIME          NOT NULL,
      `updated_at`                  DATETIME          NOT NULL,
      `supplier_id`                 INT(11)           NOT NULL,
      `total_include_tax`           INT(20)           NOT NULL,
      `tax_rate`                    FLOAT             NOT NULL,
      `shipping_cost`               FLOAT             NOT NULL,
      `status`                      INT               NOT NULL,
      `note`                        VARCHAR(200)      NULL,
      `delivery_date`               DATETIME          NOT NULL,
      `is_paid`                     INT               NOT NULL,
      `type`                        INT               NOT NULL,
      PRIMARY KEY (`po_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
      
    DROP TABLE IF EXISTS {$this->getTable('purchase_order_gold')};
    CREATE TABLE {$this->getTable('purchase_order_gold')} (
      `po_gold_id`           INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `po_id`                INT(11)       NOT NULL,
      `gold_id`              INT(11)       NOT NULL,
      `qty`                  INT           NOT NULL,
      `price`                FLOAT         NOT NULL,
      PRIMARY KEY (`po_gold_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
      
    DROP TABLE IF EXISTS {$this->getTable('purchase_order_stone')};
    CREATE TABLE {$this->getTable('purchase_order_stone')} (
      `po_stone_id`          INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `po_id`                INT(11)       NOT NULL,
      `stone_id`             INT(11)       NOT NULL,
      `qty`                  INT           NOT NULL,
      `price`                FLOAT         NOT NULL,
      PRIMARY KEY (`po_stone_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
$installer->endSetup();