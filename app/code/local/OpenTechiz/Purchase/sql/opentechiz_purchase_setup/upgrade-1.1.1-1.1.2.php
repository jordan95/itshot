<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_purchase/productsupplier')} 
    ADD `last_cost`   DECIMAL(12,4)
    NULL
    DEFAULT 0;
");
$installer->endSetup();