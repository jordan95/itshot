<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_purchase/productsupplier')} 
    ADD `last_cost_date`   DATETIME
    NULL
    DEFAULT NULL;
");
$installer->endSetup();