<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_purchase/orderitem')} 
    ADD `note`   TEXT
    DEFAULT '';
");
$installer->endSetup();
