<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE {$this->getTable('opentechiz_purchase/orderitem')} 
    ADD `shipment_reference` text
    NULL
    DEFAULT NULL;
");
$installer->endSetup();