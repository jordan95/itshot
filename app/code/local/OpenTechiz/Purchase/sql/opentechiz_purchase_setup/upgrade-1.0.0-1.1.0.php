<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('purchase_return')};
    CREATE TABLE {$this->getTable('purchase_return')} (
      `id`   INT(11) UNSIGNED  NOT NULL AUTO_INCREMENT,
      `po_id`   INT(11)        NOT NULL,
      `item`       text      NOT NULL,
      `total`       INT(11)           NOT NULL,
      `reason`       INT(11)       NOT NULL,
      `status`          INT(11)  default 0     NULL,
      `note`         VARCHAR(255)       NULL,
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
$installer->endSetup();