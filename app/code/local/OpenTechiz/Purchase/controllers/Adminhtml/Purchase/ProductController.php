<?php

class OpenTechiz_Purchase_Adminhtml_Purchase_ProductController extends Mage_Adminhtml_Controller_Action
{
    protected function _initPurchaseOrder($idFieldName = 'id')
    {
        $id = (int)$this->getRequest()->getParam($idFieldName);
        $purchaseOrder = Mage::getModel('opentechiz_purchase/purchase');

        if ($id) {
            $purchaseOrder->load($id);
            if (!$purchaseOrder->getId()) {
                Mage::throwException(Mage::helper('opentechiz_production')->__('Wrong purchase Order requested.'));
            }
        }

        Mage::register('purchaseOrder', $purchaseOrder);
        return $purchaseOrder;
    }


    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp')
            ->_title($this->__('Purchase Order'))
            ->_title($this->__('Product Purchase'));
        return $this;
    }

    protected function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('production');
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_product'));
        $this->getLayout()->getBlock('head')->setTitle($this->__('Product Purchase Grid'));
        $this->renderLayout();
    }
    public function gridAction()
    {
        $this->loadLayout();

        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_product_grid')->toHtml()
        );
    }

    /**
     * Edit action
     */
    public function editAction()
    {
        $purchaseOrder = $this->_initPurchaseOrder();


        $this->_initAction();

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/jquery-1.10.2.min.js');
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/noconflict.js');
        $this->getLayout()->getBlock('head')->addJs('opentechiz/adminhtml/product/composite/configure.js');
        $this->getLayout()->getBlock('head')->addJs('varien/configurable.js');

        $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_product_edit'))
            ->_addLeft($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_product_edit_tabs'));

        $this->renderLayout();

    }
    /**
     * New product action
     */
    public function createAction()
    {

        $this->_forward('edit');
    }
    /**
     * step one action
     */
    public function newAction()
    {
        $this->loadLayout();

        $this->_setActiveMenu('erp');
        $this->getLayout()->getBlock('head')->setTitle($this->__('New Purchase Order / Material Purchase'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_product_new/'));

        $this->renderLayout();
    }


    public function saveAction()
    {
        $purchase = $this->_initPurchaseOrder();
        $is_new = 0;
        $action = 'edit';
        if(!$purchase->getId()) {
            $is_new = 1;
            $action = 'create';
        }
        //add event log admin action
        if ($data = $this->getRequest()->getPost()) {
            if(isset($data['item'])) {
                try {
                    $item_datas = $data['item'];
                    unset($data['item']);
                    $newTotal = 0;

                    foreach ($item_datas as $item_data) {
                        if(isset($item_data['delete']) && $item_data['delete'] != '') continue;

                        $sku = $item_data['sku'];
                        $purchase_item = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()
                                ->addFieldToFilter('po_id', $purchase->getId())
                                ->addFieldToFilter('sku', $sku)->getFirstItem();
                                
                        if (isset($item_data['delivered_qty']) && isset($item_data['qty'])) {
                            if ($item_data['delivered_qty'] > $item_data['qty']) {
                                throw Mage::exception('Mage_Eav', Mage::helper('eav')->__("Delivered quantity can not be more than quantity"));
                            }

                            $sup = Mage::getModel('opentechiz_purchase/supplier')->load($purchase->getSupplierId());
                            if ($sup->getData('is_active_portal') == OpenTechiz_Purchase_Helper_Data::SUP_IS_ACTIVE_PORTAL) {
                                if ($purchase_item->getShippedQty() > 0) { 
                                    if ($item_data['delivered_qty'] > $purchase_item->getShippedQty()) {
                                        throw Mage::exception('Mage_Eav', Mage::helper('eav')->__("Can not save Delivered qty more than Shipped Qty PO."));
                                    }
                                }
                            }
                        }

                        if(!isset($item_data['qty'])) {
                            $itemQty = $purchase_item->getQty();
                        }else{
                            $itemQty = $item_data['qty'];
                        }
                        $newTotal += (float)$item_data['price'] * $itemQty;
                    }

                    $data['total_include_tax'] = $newTotal;
                    $data['type'] = OpenTechiz_Purchase_Helper_Data::PO_TYPE_PRODUCT;

                    if (!$purchase->getpo_increment_id())
                        $data['po_increment_id'] = $purchase->generateOrderNumber();
                    $purchase->addData($data);
                    $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

                    if($is_new) {
                        $created_at = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s");
                        $purchase->setCreatedAt($created_at);
                    }
                    if(isset($data['delivery_date'])) {
                        $delivery_date = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $data['delivery_date']);
                        $purchase->setDeliveryDate($delivery_date);
                    }
                    $purchase->setUpdatedAt(now());
                    $purchase->save();
                    foreach ($item_datas as $item_data) {
                        $purchase_item = Mage::getModel('opentechiz_purchase/orderitem');
                        if (isset($item_data['delete']) && $item_data['delete']) {
                            $id = $purchase_item->getCollection()->addFieldToFilter('sku', $item_data['delete'])->addFieldToFilter('po_id', $purchase->getId())->getFirstItem()->getId();
                            $purchase_item->load($id);
                            $purchase_item->delete();
                            continue;
                        }
                        if (isset($item_data['sku'])) {
                            $id = $purchase_item->getCollection()->addFieldToFilter('sku', $item_data['sku'])->addFieldToFilter('po_id', $purchase->getId())->getFirstItem()->getId();

                            $purchase_item->load($id);
                            if (isset($item_data['option'])) {
                                $option = json_decode($item_data['option'], true);
                                $option_data['info_buyRequest'] = array('product' => $item_data['product_id']);
                                $option_data['options'] = $option;
                                $item_data['option'] = serialize($option_data);
                            }

                            $priceChange = false;
                            if(round($purchase_item->getPrice(), 4) != round($item_data['price'], 4)){
                                $priceChange = true;
                            }

                            if (isset($item_data['purchase_item_id']))
                                $purchase_item->load($item_data['purchase_item_id']);
                            $item_data['po_id'] = $purchase->getId();
                            $item_data['price'] = str_replace(',', '', $item_data['price']);
                            $purchase_item->addData($item_data);

                            $purchase_item->save();


                            //change last cost and item cost if have permission
                            $allowEditPrice = Mage::getSingleton('admin/session')->isAllowed('admin/erp/purchase/product/edit_product_cost');
                            if($allowEditPrice && $priceChange){
                                $resource = Mage::getSingleton('core/resource');
                                $writeConnection = $resource->getConnection('core_write');


                                $cost = (float)$item_data['price'];
                                $po_id = $purchase->getId();
                                $query = "UPDATE `tsht_purchase_purchase_order_item` 
                                          join tsht_inventory_item on tsht_purchase_purchase_order_item.item_id = tsht_inventory_item.item_id 
                                          set tsht_inventory_item.cost = ".$cost."
                                          where po_id = ".$po_id." AND tsht_inventory_item.sku = '".$item_data['sku']."'";
                                $writeConnection->query($query);

                                if($purchase_item->getDeliveredQty() > 0){
                                    $currentTime = Mage::getModel('core/date')->gmtDate('Y-m-d');
                                    $query = "UPDATE `tsht_inventory_instock_product` 
                                              set last_cost = ".$cost.", last_cost_date = '".$currentTime."', last_po_id =".$po_id."
                                              where sku = '".$item_data['sku']."'";
                                    $writeConnection->query($query);
                                     //update last cost,last cost date to tsht_purchase_product_supplier
                                    $product_supplier = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()->addFieldToFilter('product_id', $purchase_item->getProductId())
                                                        ->addFieldToFilter('sup_id', $purchase->getSupplierId())->getFirstItem();
                                    $data['last_cost'] = $cost;
                                    $data['last_cost_date'] = $purchase->getUpdatedAt();
                                    $product_supplier->addData($data)->save(); 
                                }
                                
                            }
                        }
                    }
                    if(!$this->getRequest()->getParam('back')){
                        $data = [];
                        $data['is_supplier_notified'] = OpenTechiz_Purchase_Helper_Data::SUP_NOTIFIED;
                        $data['is_pending_po'] = OpenTechiz_Purchase_Helper_Data::PO_EMAIL_PROCESSED;
                        $data['notified_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                        $purchase->addData($data)->save();
                        if ($purchase->getStatus() != OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL) {
                            Mage::helper('opentechiz_purchase')->sendMail($purchase, $action);
                            $dataLog = array();
                            $dataLog['log_from'] = 'app/code/local/OpenTechiz/Purchase/controllers/Adminhtml/Purchase/ProductController.php';
                            $dataLog['po_id'] = $purchase->getId();
                            Mage::log($dataLog, null, 'cron_master_po.log', true);
                        }
                    }

                    if ($purchase) {
                        $purchase->validAndUpdateStatus();
                    }
                    $admin = Mage::getSingleton('admin/session')->getUser();
                    if ($is_new && $admin->getId()) {
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Add new purchase product #' . $purchase->getOrderIcrementId()));
                    }
                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('opentechiz_purchase')->__('Item was successfully saved'));
                    Mage::getSingleton('adminhtml/session')->setFormData(false);

                    if ($this->getRequest()->getParam('back')) {
                        $this->_redirect('*/*/edit', array('id' => $purchase->getId()));
                        return;
                    }
                    $this->_redirect('*/*/');
                    return;
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                    Mage::getSingleton('adminhtml/session')->setFormData($data);
                    if($this->getRequest()->getParam('id'))
                        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    else
                        $this->_redirect('*/*/');
                    return;
                }
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_purchase')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction() {
        if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $model = Mage::getModel('opentechiz_purchase/supplier');

                $model->setId($this->getRequest()->getParam('id'))->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }


    /**
     * GetProductFromOption action
     */
    public function GetProductFromOptionAction()
    {
        $post = $this->getRequest()->getPost();

        if(isset($post['option'])) $options = $post['option'];
        else $options = [];

        $option_data=array();
        foreach ($options as $data) {
            # code...
            if(array_key_exists($data['name'], $option_data))
                $option_data[$data['name']] = $option_data[$data['name']].','.$data['value'];
            else
                $option_data[$data['name']] = $data['value'];
        }

        $product = Mage::getModel('catalog/product')->load($post['product_id']);
        $optionsCollection = $product->getOptions();
        // if option enabled = no && hasOptions = 0
        if (!$optionsCollection) $optionsCollection = $product->getProductOptionsCollection();



        $values = array();
        $results = array();
        $option_products = array();
        $end = '}';
        $ar_json = '';
        $k=0;

        foreach ($option_data as $key => $value) {
            # code...
            if(!$value)
                continue;
            $k++;
            $value_ar = explode(',', $value);




            $key=  str_replace(']','',$key);
            $key = trim($key, 'options');
            $key = ltrim($key, '[');

            $ar_options = explode("[",$key);
            if($value){
                if($k>1)
                    $ar_json .=',';
                if(count($ar_options)==1){
                    if(count($value_ar)>1){
                        $value1 = json_encode(explode(',', $value),true);
                        $ar_json .='"'.$key.'":"'.$value1.'"';
                    }else{
                        $ar_json .='"'.$key.'":"'.$value.'"';
                    }
                    continue;
                }
                $i = 1;
                $json = '';

                foreach ($ar_options as $optionid) {
                    # code...

                    if($i==1)
                        $json .= '"'.$optionid.'":';
                    else{
                        if($optionid)
                            $json .='{"'.$optionid.'":';

                        if($i==count($ar_options)){


                            if(count($value_ar)<=1){
                                $json .= $value;
                                if($optionid)
                                    $json .= '}';
                            }else{
                                $value = json_encode($value_ar,true);
                                $json .= $value;
                            }
                            $ar_json .= $json;
                        }
                        else{
                            if($optionid)
                                $json .= '}';
                        }
                    }

                    $i++;
                }


            }
        }

        $ar_json = '{'.$ar_json;
        $ar_json.= '}';
        $option_ar = json_decode($ar_json,true);
        $option_data = array();
        $isPersonalized = Mage::helper('personalizedproduct')->isPersonalizedProduct($product);
        $price = 0;
        if(!$isPersonalized)
            $price = $product->getFinalPrice();
        foreach ($option_ar as $option_id => $option_type_id) {
            # code...
            $option_sku = Mage::getResourceModel('catalog/product_option_value_collection')
                ->addFieldToFilter('option_type_id', $option_type_id)->getFirstItem()->getSku();

            if($product->getOptionById($option_id)->getType() == 'stone' ){

                if(Mage::helper('custom')->isDiamond($option_sku)){

                    $qualityData = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStoneWithQualityPrice($option_id,$product->getId());
                    $quality = str_replace("diamond", "", $option_sku);
                    $price += number_format($qualityData[$quality], 2);

                }else{

                    $data = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStonePrice($option_id,$product->getId());
                    $price += number_format($data[$option_sku], 2);

                }


            }

            if($product->getOptionById($option_id)->getType() == 'gold' ){
                $data = Mage::helper('personalizedproduct/personalizedPriceHelper')->getAlloyPrice($product->getId());

                $price += $data[$option_sku];


            }

        }
        if($product->getAdditionalFee())
            $price += $product->getAdditionalFee();

        foreach ($optionsCollection as $option) {
            if($option->getIsRequire()&&!isset($option_ar[$option->getId()])){
                print_r('error');
                die();
            }


            if($option->getType() == 'field' && isset($option_ar[$option->getId()])){

                $optionval['label'] = $option->getTitle();
                $optionval['value'] = $option_ar[$option->getId()];
                $optionval['option_id'] = $option->getId();

                $optionval['option_value'] = $option_ar[$option->getId()];
                $optionval['option_type'] = $option->getType();
                if($option->getSku())
                    $values[] = $option->getSku();

                $option_data[] = $optionval;
            }
            foreach ($option->getValues() as $optionValue){
                if(isset($option_ar[$option->getId()]) && $optionValue->getId() == $option_ar[$option->getId()]) {
                    $optionval['label'] = str_replace("'","\"",$option->getTitle());
                    $optionval['value'] = str_replace("'","\"",$optionValue->getTitle());
                    $optionval['option_id'] = $option->getId();
                    $optionval['option_value'] = $optionValue->getId();
                    $optionval['option_type'] = $option->getType();
                    if($optionValue->getSku())
                        $values[] = $optionValue->getSku();
                    $option_data[] = $optionval;
                }
            }
        }

        $result['product_information']['config_option'] = $option_data;
        $result['product_information']['price'] = Mage::getModel('directory/currency')->format($price, array('display'=>Zend_Currency::NO_SYMBOL), false);
        if(count($values))
            $result['product_information']['product_option_sku'] = $product->getSku().OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $values);
        else
            $result['product_information']['product_option_sku'] = $product->getSku();
        //check if has sku option size

        // get item last cost
        $last_cost = Mage::helper('opentechiz_purchase')->getLastCostBySku($result['product_information']['product_option_sku']);
        $last_cost_date = Mage::helper('opentechiz_purchase')->getLastCostDate($result['product_information']['product_option_sku']);
        $result['product_information']['last_cost'] = $last_cost;
        $result['product_information']['last_cost_date'] = $last_cost_date;
        $result['product_information']['product_name'] = $product->getName();
        $sku_id = str_replace('.', '', $result['product_information']['product_option_sku']);
        $result['product_information']['item_id'] = $product->getId().'_'.$sku_id;
        $result['product_information']['product_option_tag_sku'] = Mage::helper('opentechiz_purchase')->getTagSku($result['product_information']['product_option_sku']);
        $result['product_information']['sup_sku'] = "";
        $Po_products = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()
                ->addFieldToFilter('product_id', $product->getId())
                ->addFieldToFilter('sup_id', $post['sup_id']);
        if($Po_products->count() > 0){
            $po_first = $Po_products->getFirstItem();
            $result['product_information']['sup_sku'] = $po_first->getData('sup_sku'); 
        }
        print_r( json_encode($result));
        die();

    }

    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            case 'edit':
                $aclResource = 'admin/erp/purchase/product/product_edit';
                break;
            default:
                $aclResource = 'admin/erp/purchase/product';
                break;
        }
        return Mage::getSingleton('admin/session')->isAllowed($aclResource);
    }

    public function gridproductaddAction()
    {
        if(!Mage::registry('resetGrid')){
            Mage::register('resetGrid', true);
        }
        $this->_initPurchaseOrder();
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_product_edit_tab_product')->setIsAjaxCall(1)->toHtml()
        );
    }

    /**
     *
     */
    public function addProductToMasterPOAction(){
        $post = $this->getRequest()->getPost();
        $model = Mage::getModel('opentechiz_purchase/purchase');
        $po_id = $model->addToMasterPO($post);
        print_r($po_id);
        die();
    }

    public function getPoIdToAddProductAction(){
        $post = $this->getRequest()->getPost();
        $result = ''; 
        if (isset($post['sup_id'])) {
            $collection = Mage::getModel('opentechiz_purchase/purchase')->getCollection()
                ->addFieldToFilter('supplier_id', $post['sup_id'])
                ->addFieldToFilter('is_pending_po', OpenTechiz_Purchase_Helper_Data::PO_EMAIL_PENDING)
                ->addFieldToFilter('status', array('nin' => array(OpenTechiz_Purchase_Helper_Data::PO_STATUS_COMPLETE, OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL)));
            if(count($collection)){
                $result = $collection->getFirstItem()->getPoIncrementId();
            }
        }
        $this->getResponse()->setBody($result);
    }
}