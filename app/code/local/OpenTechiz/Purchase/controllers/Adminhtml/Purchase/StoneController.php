<?php
require_once Mage::getModuleDir('controllers', "OpenTechiz_Purchase").DS."Adminhtml/MaterialController.php";

class OpenTechiz_Purchase_Adminhtml_Purchase_StoneController extends OpenTechiz_Purchase_Adminhtml_MaterialController
{
    /**
     * Edit action
     */
    public function editAction()
    {
        $this->_initMaterial();
        $material = Mage::registry('material_data');
        $materialID = $material->getId();
        if ($materialID || $materialID == $this->getRequest()->getParam('id')) {

            $this->_initAction();

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->getLayout()->getBlock('head')->addJs('lib/jquery/jquery-1.10.2.min.js');
            $this->getLayout()->getBlock('head')->addJs('lib/jquery/noconflict.js');
            $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_purchase_stone_edit'))
                ->_addLeft($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_purchase_stone_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_purchase')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * New action
     *
     */
    public function createAction()
    {
        $order = mage::helper('opentechiz_purchase')->createNewOrder();
        $admin = Mage::getSingleton('admin/session')->getUser();
            if($admin->getId()){
                          Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Add new purchase stone #'.$order->getOrderIcrementId()));
              }
        $supplier = $this->getRequest()->getParam('supplier');
        $this->_redirect('adminhtml/purchase_stone/edit', array('id' => $order->getId(), 'supplier' => $supplier));
    }

    /**
     * Save action
     *
     */
    public function saveAction()
    {
        $this->_initMaterial();
        if ($data = $this->getRequest()->getPost()) {
            //Purchase order id parameter
            $id = $this->getRequest()->getParam('id');
            $purchase = Mage::getModel('opentechiz_purchase/purchase');
            $purchaseStone = Mage::getModel('opentechiz_purchase/purchasestone');
            // Array golds appear when add product checkbox clicked
            $stones = $this->getRequest()->getPost('add');
           
            $addPrice = 0;
             try {
                 $delivered = true;
                if(isset($stones))
                {
                    foreach ($stones as $key => $value)
                    {
                        if($value['delivered_qty'] > $value['qty'])
                            throw Mage::exception('Mage_Eav', Mage::helper('eav')->__("Delivered quantity can not be more than quantity"));
                        $addPrice += $value['qty'] * $value['price'];
                    }
                }
                $totalPrice = $addPrice;
                if(isset($stones))
                {
                    if(isset($id))
                    {
                        $data['created_at'] = Mage::getSingleton('core/date')->gmtDate( "Y-m-d H:i:s", $data['created_at']);
                        $data['delivery_date'] = Mage::getSingleton('core/date')->gmtDate( "Y-m-d H:i:s", $data['delivery_date']);
                        $totalPrice = ($totalPrice*(100+$data['tax_rate']))/100 + $data['shipping_cost'];
                        $purchase->load($id)->addData($data)->setTotalIncludeTax($totalPrice)->setUpdatedAt(now());
                        $purchase->save();
                        foreach ($stones as $key => $value) {
                            $stoneData['po_id'] = $id;
                            $stoneData['qty'] = $value['qty'];
                            $stoneData['delivered_qty'] = $value['delivered_qty'];
                            $stoneData['price'] = $value['price'];
                            $stoneData['stone_id'] = $value['stone_id'];
                            $purchaseStone_id  = $purchaseStone->getCollection()->AddFieldtoFilter('stone_id',$key)->AddFieldtoFilter('po_id',$id)->GetFirstItem()->getId();
                            $purchaseStone = Mage::getModel('opentechiz_purchase/purchasestone');
                            if($purchaseStone_id){
                                $purchaseStone->load($purchaseStone_id);
                                if(isset($value['delete'])){
                                   $change = $purchaseStone->getDeliveredQty();
                                   $purchaseStone->delete();
                                   $stone =  Mage::getModel('opentechiz_material/stone')->load($purchaseStone->getStoneId());
                                    $current_qty =  $stone->getQty();
                                    $qty = $current_qty - $change;
                                    $stone->setQty($qty)->save();
                                   continue; 
                                }

                            }
                            $purchaseStone->addData($stoneData)->save();   
                        }
                    }
                    else
                    {
                        $totalPrice = ($totalPrice*(100+$data['tax_rate']))/100 + $data['shipping_cost'];
                        $purchase->setData($data)->setTotalIncludeTax($totalPrice)->save();
                        $stoneData = array();
                        foreach ($stones as $key => $value) {
                            $stoneData['po_id'] = $id;
                            $stoneData['stone_id'] = $value['stone_id'];
                            $stoneData['qty'] = $value['qty'];
                            $stoneData['price'] = $value['price'];
                            $purchaseStone->setData($stoneData)->save();
                        }
                    }
                }else{

                    if(isset($id))
                    {
                        $data['created_at'] = Mage::getSingleton('core/date')->gmtDate( "Y-m-d H:i:s", $data['created_at']);
                       
                        $data['delivery_date'] = Mage::getSingleton('core/date')->gmtDate( "Y-m-d H:i:s", $data['delivery_date']);
                        $totalPrice = ($totalPrice*(100+$data['tax_rate']))/100 + $data['shipping_cost'];
                        $purchase->load($id)->addData($data)->setTotalIncludeTax($totalPrice)->setUpdatedAt(now())->save();
                    }
                    else
                    {
                        $totalPrice = ($totalPrice*(100+$data['tax_rate']))/100 + $data['shipping_cost'];
                        $purchase->setData($data)->setTotalIncludeTax($totalPrice)->save();
                    }
                }
                if($purchase){
                    $purchase->validAndUpdateStatus();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('opentechiz_purchase')->__('Item was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $purchase->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id'),'supplier'=>$data['supplier_id']));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_purchase')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function gridstoneaddAction()
    {
         $this->_initMaterial();
        $this->loadLayout();
        

        $this->getResponse()->setBody($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_purchase_stone_edit_tab_add')->setIsAjaxCall(1)->toHtml());
    }

    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            case 'edit':
                $aclResource = 'admin/erp/purchase/material/stone/stone_edit';
                break;
            default:
                $aclResource = 'admin/erp/purchase/material/stone';
                break;
        }
        return Mage::getSingleton('admin/session')->isAllowed($aclResource);
    }
}