<?php
require_once Mage::getModuleDir('controllers', "OpenTechiz_Purchase").DS."Adminhtml/MaterialController.php";

class OpenTechiz_Purchase_Adminhtml_Purchase_GoldController extends OpenTechiz_Purchase_Adminhtml_MaterialController
{
    /**
     * Edit action
     */
    public function editAction()
    {
        $this->_initMaterial();
        $material = Mage::registry('material_data');
        $materialID = $material->getId();
        if ($materialID || $materialID == $this->getRequest()->getParam('id')) {

            $this->_initAction();

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->getLayout()->getBlock('head')->addJs('lib/jquery/jquery-1.10.2.min.js');
            $this->getLayout()->getBlock('head')->addJs('lib/jquery/noconflict.js');
            $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_purchase_gold_edit'))
                ->_addLeft($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_purchase_gold_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_purchase')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * New action
     *
     */
    public function createAction()
    {
        $order = mage::helper('opentechiz_purchase')->createNewOrder();
         $admin = Mage::getSingleton('admin/session')->getUser();
            if($admin->getId()){
                          Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Add new purchase gold #'.$order->getOrderIcrementId()));
              }
        $supplier = $this->getRequest()->getParam('supplier');
        $this->_redirect('adminhtml/purchase_gold/edit', array('id' => $order->getId(), 'supplier' => $supplier));
    }

    /**
     * Save action
     *
     */
    public function saveAction()
    {
        $this->_initMaterial();

        if ($data = $this->getRequest()->getPost()) {
            $purchase = Mage::getModel('opentechiz_purchase/purchase');
            $purchaseGold = Mage::getModel('opentechiz_purchase/purchasegold');
            $id = $this->getRequest()->getParam('id');
            
          
            

            // Array golds appear when add product checkbox clicked
            $golds = $this->getRequest()->getPost('add');
           
            $addPrice = 0;
            if(isset($golds))
            {
                foreach ($golds as $key => $value)
                {
                      if(isset($value['delete'])){
                         continue; 
                        }
                    $addPrice += $value['qty'] * $value['price'];
                }
            }

            $totalPrice = $addPrice;

            try {
                if(isset($golds))
                {
                    if(isset($id))
                    {
                        $data['created_at'] = Mage::getSingleton('core/date')->gmtDate( "Y-m-d H:i:s", $data['created_at']);
                       
                        $data['delivery_date'] = Mage::getSingleton('core/date')->gmtDate( "Y-m-d H:i:s", $data['delivery_date']);
                        $totalPrice = floatval($totalPrice*(100+$data['tax_rate'])/100 + $data['shipping_cost']);

                        $purchase->load($id)->addData($data)->setTotalIncludeTax($totalPrice)->setUpdatedAt(now())->save();

                        foreach ($golds as $key => $value) {
                            
                            $goldData['po_id'] = $purchase->load($id)->getId();
                            
                            $goldData['qty'] = $value['qty'];
                            
                            $goldData['price'] = $value['price'];
                            
                            $goldData['gold_id'] = $value['gold_id'];
                            
                            $purchasegold_id  = $purchaseGold->getCollection()->AddFieldtoFilter('gold_id',$key)->AddFieldtoFilter('po_id',$id)->GetFirstItem()->getId();
                           
                            $purchaseGold = Mage::getModel('opentechiz_purchase/purchasegold');
                            if($purchasegold_id){
                                $purchaseGold->load($purchasegold_id);
                                if(isset($value['delete'])){
                                   $purchaseGold->delete();
                                   continue; 
                                }

                            }
                           
                            $purchaseGold->addData($goldData)->save();   
                        }
                    }
                    else
                    {
                        $totalPrice = $totalPrice*(100+$data['tax_rate'])/100 + $data['shipping_cost'];
                        $purchase->setData($data)->setTotalIncludeTax($totalPrice)->save();
                        $goldData = array();
                        foreach ($golds as $key => $value) {
                            $goldData['po_id'] = $purchase->load($id)->getId();
                            $goldData['gold_id'] = $value['gold_id'];
                            $goldData['qty'] = $value['qty'];
                            $goldData['price'] = $value['price'];

                            $purchaseGold->setData($goldData)->save();
                        }
                    }
                }
                else
                {
                    if(isset($id))
                    {
                       $data['created_at'] = Mage::getSingleton('core/date')->gmtDate( "Y-m-d H:i:s", $data['created_at']);
                       
                        $data['delivery_date'] = Mage::getSingleton('core/date')->gmtDate( "Y-m-d H:i:s", $data['delivery_date']);
                        $totalPrice = $totalPrice*(100+$data['tax_rate'])/100 + $data['shipping_cost'];
                        $purchase->load($id)->addData($data)->setTotalIncludeTax($totalPrice)->setUpdatedAt(now())->save();
                    }
                    else
                    {
                        $totalPrice = $totalPrice*(100+$data['tax_rate'])/100 + $data['shipping_cost'];
                        $purchase->setData($data)->setTotalIncludeTax($totalPrice)->save();
                    }
                }
                
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('opentechiz_purchase')->__('Item was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $purchase->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id'),'supplier'=>$data['supplier_id']));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_purchase')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function gridgoldaddAction()
    {
        $this->_initMaterial();
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_purchase_gold_edit_tab_add')->setIsAjaxCall(1)->toHtml()
        );
    }

     protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            case 'edit':
                $aclResource = 'admin/erp/purchase/material/gold/gold_edit';
                break;
            default:
                $aclResource = 'admin/erp/purchase/material/gold';
                break;
        }
        return Mage::getSingleton('admin/session')->isAllowed($aclResource);
    }
}