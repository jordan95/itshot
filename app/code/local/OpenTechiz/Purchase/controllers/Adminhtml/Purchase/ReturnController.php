<?php

class OpenTechiz_Purchase_Adminhtml_Purchase_ReturnController extends Mage_Adminhtml_Controller_Action
{
    protected function _initPurchaseOrder($idFieldName = 'id')
    {
        $id = (int)$this->getRequest()->getParam($idFieldName);
        $purchaseOrder = Mage::getModel('opentechiz_purchase/purchase');

        if ($id) {
            $purchaseOrder->load($id);
            if (!$purchaseOrder->getId()) {
                Mage::throwException(Mage::helper('opentechiz_production')->__('Wrong purchase order requested.'));
            }
        }

        Mage::register('purchaseOrder', $purchaseOrder);
        return $purchaseOrder;
    }


    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp')
            ->_title($this->__('Purchase Order'))
            ->_title($this->__('Product Purchase'));
        return $this;
    }

    protected function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('production');
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_return'));
        $this->getLayout()->getBlock('head')->setTitle($this->__('PO Return Grid'));
        $this->renderLayout();
    }
    public function gridAction()
    {
        $this->loadLayout();

        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_return_grid')->toHtml()
        );
    }

    /**
     * Edit action
     */
    public function editAction()
    {
        $purchaseOrder = $this->_initPurchaseOrder();
        if(count(Mage::getModel('opentechiz_purchase/poitem')->getCollection()->addFieldToFilter('po_id', $purchaseOrder->getId())) == 0
            && !Mage::getModel('opentechiz_purchase/poreturn')->load($purchaseOrder->getId(), 'po_id')){
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_purchase')->__('Purchase Order doesn\'t have any item.'));
            if($this->getRequest()->getParam('id'))
                $this->_redirect('*/purchase_product/edit', array('id' => $this->getRequest()->getParam('id')));
            else
                $this->_redirect('*/purchase_product/');
            return;
        }

        $this->_initAction();

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/jquery-1.10.2.min.js');
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/noconflict.js');
        $this->getLayout()->getBlock('head')->addJs('opentechiz/adminhtml/product/composite/configure.js');
        $this->getLayout()->getBlock('head')->addJs('varien/configurable.js');

        $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_return_edit'))
            ->_addLeft($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_return_edit_tabs'));

        $this->renderLayout();

    }
    /**
     * New product action
     */
    public function createAction()
    {
        $this->_forward('edit');
    }
    /**
     * step one action
     */
    public function newAction()
    {
        $this->loadLayout();

        $this->_setActiveMenu('erp');
        $this->getLayout()->getBlock('head')->setTitle($this->__('New Purchase Order / Material Purchase'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_product_new/'));

        $this->renderLayout();
    }


    public function saveAction()
    {
        //add event log admin action
        if ($data = $this->getRequest()->getPost()) {
            try {
                $reduceInStock = [];
                //save to po return table
                $poReturnCollection = Mage::getModel('opentechiz_purchase/poreturn')->getCollection()
                    ->addFieldToFilter('po_id', $this->getRequest()->getParam('id'));

                if(count($poReturnCollection) > 0){ //edit
                    $currentReturn = $poReturnCollection->getFirstItem();
                    $id = $currentReturn->getId();

                    $oldItems = [];
                    foreach (unserialize($currentReturn->getItem()) as $oldItem){
                        $oldItems[$oldItem['sku']] = $oldItem['return_qty'];
                    }

                    foreach ($data['item'] as $key => $newItem){
                        $difference = $newItem['return_qty'] - $oldItems[$newItem['sku']];
                        if($difference < 0) {
                            $difference = 0;
                            $data['item'][$key]['return_qty'] = $oldItems[$newItem['sku']];  //prevent reduce qty returned
                            Mage::getSingleton('adminhtml/session')->addError('Cannot reduce qty returned with SKU '.$newItem['sku'].'. Items are already marked as Lost');
                        }
                        $reduceInStock[$newItem['sku']] = $difference;

                    }

                    foreach ($reduceInStock as $sku => $qty){
                        $collection = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()
                            ->addFieldToFilter('po_id', $this->getRequest()->getParam('id'))->addFieldToFilter('sku', $sku);
                        if(count($collection) > 0){
                            $POProduct = $collection->getFirstItem();
                            if($POProduct->getDeliveredQty() < $qty){
                                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_purchase')->__('Cannot return more than delivered qty '));
                                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                                return;
                            }
                        }
                    }
                    
                    $error = $this->checkReduceStock($reduceInStock);
                    
                    if($error != ''){
                        Mage::getSingleton('adminhtml/session')->addError($error);
                        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                        return;
                    }

                    $saveData = [];
                    $saveData['total'] = $data['total'];
                    $saveData['item'] = serialize($data['item']);
                    $saveData['reason'] = $data['reason'];
                    $saveData['note'] = $data['note'];

                    $currentReturn->setData($saveData)->setId($id)->save();
                    $this->reduceStock($reduceInStock,$id);
                }else{  //new
                    foreach ($data['item'] as $newItem){
                        $reduceInStock[$newItem['sku']] = $newItem['return_qty'];
                    }

                    foreach ($reduceInStock as $sku => $qty){
                        $collection = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()
                            ->addFieldToFilter('po_id', $this->getRequest()->getParam('id'))->addFieldToFilter('sku', $sku);
                        if(count($collection) > 0){
                            $POProduct = $collection->getFirstItem();
                            if($POProduct->getDeliveredQty() < $qty){
                                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_purchase')->__('Cannot return more than delivered qty '));
                                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                                return;
                            }
                        }
                    }
                    $error = $this->checkReduceStock($reduceInStock);
                    if($error){
                        Mage::getSingleton('adminhtml/session')->addError($error);
                        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                        return;
                    }

                    $saveData = [];
                    $saveData['po_id'] = $this->getRequest()->getParam('id');
                    $saveData['total'] = $data['total'];
                    $saveData['item'] = serialize($data['item']);
                    $saveData['reason'] = $data['reason'];
                    $saveData['note'] = $data['note'];
                    $po_return = Mage::getModel('opentechiz_purchase/poreturn');
                    $po_return->setData($saveData)->save();
                    $id = $po_return->getId();
                    $this->reduceStock($reduceInStock,$id);
                }

                $admin = Mage::getSingleton('admin/session')->getUser();
                if($admin->getId()){
                    Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Add/Edit return for purchase #'.$this->getRequest()->getParam('id')));
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('opentechiz_purchase')->__('PO Return was saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('id')) {
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                if($this->getRequest()->getParam('id'))
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                else
                    $this->_redirect('*/*/');
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_purchase')->__('Something went wrong when saving item'));
        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            case 'edit':
                $aclResource = 'admin/erp/purchase/product/product_edit';
                break;
            default:
                $aclResource = 'admin/erp/purchase/product';
                break;
        }
        return Mage::getSingleton('admin/session')->isAllowed($aclResource);
    }

    public function reduceStock($reduceInStock,$id){
        //reduce stock
       
        foreach ($reduceInStock as $sku => $qty){
            $stockCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()
                ->addFieldToFilter('sku', $sku);
            if(count($stockCollection) > 0){
                $stock = $stockCollection->getFirstItem();
                //delete item & delete in item-PO table
                $collection = Mage::getModel('opentechiz_purchase/poitem')->getCollection()->join(array('item' => 'opentechiz_inventory/item'), 'main_table.item_id = item.item_id')
                    ->addFieldToFilter('po_id', $this->getRequest()->getParam('id'))
                    ->addFieldToFilter('state', 35) //filter for item instock
                    ->addFieldToFilter('sku', $sku);
                $i = 0;
                foreach ($collection as $item){
                    if($i == $qty) break;
                    $item_id = $item->getItemId();
                    Mage::log($item_id);
                    Mage::getModel('opentechiz_purchase/poitem')->load($item_id, 'item_id')->delete();
                    Mage::getModel('opentechiz_inventory/item')->load($item_id)->setState(50)->save();
                    $i ++;
                }
                //reduce delivered qty
                $collection = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()
                    ->addFieldToFilter('po_id', $this->getRequest()->getParam('id'))->addFieldToFilter('sku', $sku);
                if(count($collection) > 0){
                    $POProductId = $collection->getFirstItem()->getId();
                    $POProduct = Mage::getModel('opentechiz_purchase/orderitem')->load($POProductId);
                    $POProduct->setDeliveredQty($POProduct->getDeliveredQty() - $qty)->save();
                }
                //add log movement stock when reduce stock
                Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                    'qty' => '-'.$qty,
                    'on_hold' => 0,
                    'type' => 22,
                    'stock_id' => $stock->getId(),
                    'qty_before_movement' => $stock->getQty(),
                    'on_hold_before_movement' => $stock->getOnHold(),
                    'sku' => $stock->getSku(),
                    'po_return_id' => $id,
                    'po_id' =>$this->getRequest()->getParam('id')
                ));
            }
        }
        return '';
    }
    public function checkReduceStock($reduceInStock){
        //reduce stock
        foreach ($reduceInStock as $sku => $qty){
            $stockCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()
                ->addFieldToFilter('sku', $sku);
            if(count($stockCollection) > 0){
                //delete item & delete in item-PO table
                $collection = Mage::getModel('opentechiz_purchase/poitem')->getCollection()->join(array('item' => 'opentechiz_inventory/item'), 'main_table.item_id = item.item_id')
                    ->addFieldToFilter('po_id', $this->getRequest()->getParam('id'))
                    ->addFieldToFilter('state', 35) //filter for item instock
                    ->addFieldToFilter('sku', $sku);

                if(count($collection) < $qty){
                    $error = 'SKU '.$sku.' doesn\'t have enough items left in stock';
                    return $error;
                    break;
                }
            }
        }
        return '';

    }
}