<?php

class OpenTechiz_Purchase_Adminhtml_Purchase_AllItemController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('erp')
            ->_title($this->__('All Items'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_allItem'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();
        $this->getLayout()->getBlock('head')->setTitle($this->__('All Purchase Order Items'));
        $this->renderLayout();
    }

    public function allItemGridAction(){
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_allItem_grid')->toHtml()
        );
    }

    public function saveAction(){
        $data = $this->getRequest()->getParams();
        foreach ($data as $key => $itemData) {
            //load po item, po
            $parts = explode('_', $key);
            $itemId = $parts[0];
            unset($parts[0]);
            $index = implode('_', $parts);
            $item = Mage::getModel('opentechiz_purchase/orderitem')->load($itemId);
            $po = Mage::getModel('opentechiz_purchase/purchase')->load($item->getPoId());

            //set shipment reference
            if($index == 'shipment_reference') {
                $item->setData('shipment_reference', $itemData);
            }
            $item->save();

            //check delivered qty
            if($index == 'delivered_qty'){
                $currentDelivered = $item->getDeliveredQty();
                $qty = $item->getQty();
                //error if < current
                if((int)$itemData < $currentDelivered){
                    $this->_getSession()->addError('Delivered QTY input lower than current delivered QTY');
                    $this->_redirect('*/*/');
                    $error = 1;
                    break;
                }
                if((int)$itemData > $qty){
                    $this->_getSession()->addError('Delivered QTY input cannot be higher than requested QTY');
                    $this->_redirect('*/*/');
                    $error = 1;
                    break;
                }

                //set delivered qty
                $item->setDeliveredQty((int)$itemData);
                $item->save();

                //check if po complete, set po status
                $allItems = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()
                    ->addFieldToFilter('po_id', $po->getId());
                $delivered = true;
                foreach ($allItems as $eachItem){
                    if($eachItem->getDeliveredQty() != $eachItem->getQty()){
                        $delivered = false;
                    }
                }
                if($delivered && $po->getStatus() != 1){
                    $po->setStatus(1);
                }
            }

            //save
            $po->save();
        }

        if(!isset($error)) {
            $this->_getSession()->addSuccess('Saved');
            $this->_redirect('*/*/');
        }
    }

    public function massUpdateShipmentRefAction(){
        $itemIds = $this->getRequest()->getPost('item_ids');
        $shipment_reference = $this->getRequest()->getPost('shipment_reference');
        $item_success =  array();
        $errors =  array();

        if (!empty($itemIds)) {
            $collection = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()->addFieldToFilter('id', array('in' => $itemIds));
            foreach ($collection as $item) {
                $po = Mage::getModel('opentechiz_purchase/purchase')->load($item->getPoId());
                $sup = Mage::getModel('opentechiz_purchase/supplier')->load($po->getSupplierId());
                // if active portal, we can not update the shipment ref at admin page.
                if ($sup->getIsActivePortal() == OpenTechiz_Purchase_Helper_Data::SUP_IS_ACTIVE_PORTAL) {
                    $errors['is_active_portal'][] = $item->getId();
                    continue;
                }

                // validate duplicate shipment ref
                $old_shipment_reference = array_map('trim', explode(',', $item->getShipmentReference()));
                if (in_array($shipment_reference, $old_shipment_reference)) {
                    $errors['duplicate'][] = $item->getId();
                    continue;
                }

                // validate if qty is 1, can not add multiple shipment ref
                if (!empty($item->getShipmentReference())) {
                    if ((int)$item->getQty() > 1) {
                        $shipment_reference = $item->getShipmentReference() . ',' . trim($shipment_reference);
                    }else{
                        $errors['multiple'][] = $item->getId();
                        continue;
                    }
                }

                $item->setData('shipment_reference', $shipment_reference);
                try {
                    $item->save();
                    $item_success[] = $item->getId();
                } catch (Exception $e) {
                    $errors['exception'] = $item->getId();
                }
            }

            if (count($errors)) {
                if (array_key_exists('is_active_portal', $errors)) {
                    $this->_getSession()->addError(Mage::helper('adminhtml')->__('Items are using Portal: %s.', implode(',',$errors['is_active_portal'])));
                }

                if (array_key_exists('duplicate', $errors)) {
                    $this->_getSession()->addError(Mage::helper('adminhtml')->__('Items duplicate Shipment Ref: %s.', implode(',',$errors['duplicate'])));
                }

                if (array_key_exists('multiple', $errors)) {
                    $this->_getSession()->addError(Mage::helper('adminhtml')->__('Items only allow one Shipment Ref: %s.', implode(',',$errors['multiple'])));
                }
            }

            if ($item_success) {
                $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Update items success: %s', implode(',',$item_success)));
            }
        }
        $this->_redirect('*/*/');
    }
}