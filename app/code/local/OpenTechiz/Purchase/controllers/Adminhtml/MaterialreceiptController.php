<?php

class OpenTechiz_Purchase_Adminhtml_MaterialReceiptController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            default:
                $aclResource = 'admin/erp/purchase/material_receipt';
                break;
        }
        return Mage::getSingleton('admin/session')->isAllowed($aclResource);
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp')
            ->_title($this->__('Manage Material Receipt'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();

        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_materialreceipt_grid')->toHtml()
        );
    }

    public function editAction()
    {
        $this->_initAction();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_materialreceipt_edit'))
            ->_addLeft($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_materialreceipt_edit_tabs'));

        $this->renderLayout();
    }

    public function newAction()
    {
        $this->loadLayout();

        $this->_setActiveMenu('erp');
        $this->getLayout()->getBlock('head')->setTitle($this->__('New Material Receipt'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_materialreceipt_new/'));

        $this->renderLayout();
    }

    public function createAction(){
        $this->_forward('edit');
    }

    public function isValidateBeforeSaveItem($data_post){
        
        foreach ($data_post['data'] as $key => $data) {
            if (empty($data['sku'])) {
                continue;
            }

            $barcode_item = explode('_', $data['sku']);
            $dataItem = $this->getDataItemMaterialReceipt($data_post['supplier_id'],$barcode_item);
            $delivered_qty = (int)$data['delivered_qty'];
            if ($dataItem->getData()) {
                $PO_item = Mage::getModel('opentechiz_purchase/orderitem')->load($dataItem->getPoItemId());
                if ($PO_item->getId()) {
                    if ($delivered_qty > $PO_item->getQty()) {
                        Mage::getSingleton('core/session')->addError(Mage::helper('opentechiz_purchase')->__('Can not save Delivered qty more than Qty PO.'));
                        return false;
                    }
                    // validate 
                    $sup = Mage::getModel('opentechiz_purchase/supplier')->load($data_post['supplier_id']);
                    if ($sup->getData() &&  Mage::helper('opentechiz_purchase')->isActivePortalSupplier($sup)) {
                        if ($delivered_qty > (int)$PO_item->getShippedQty()) {
                            Mage::getSingleton('core/session')->addError(Mage::helper('opentechiz_purchase')->__('Can not save Delivered qty more than Shipped Qty PO.'));
                            return false;
                        }
                        // else{
                        //     Mage::getSingleton('core/session')->addError(Mage::helper('opentechiz_purchase')->__('For Supplier active portal please update Shipped Qty before update Delivered Qty.'));
                        //     return false;
                        // }
                    }
                }
            }else{
                Mage::getSingleton('core/session')->addError(Mage::helper('opentechiz_purchase')->__('Can not get data from item.'));
                return false;
            }
        }
        return true;
    }

    public function saveAction(){
        $post = $this->getRequest()->getPost();
        $shipment_reference = $this->getRequest()->getParam('shipment_reference');
        if (isset($post['data']) && isset($post['supplier_id']) && count($post['data']) > 1) {
            $material_receipt = Mage::getModel('opentechiz_purchase/materialreceipt');
            $material_receipt->setSupplierId($post['supplier_id']);
            $material_receipt->setCreatedAt(now());
            $material_receipt->save();

            if (!$this->isValidateBeforeSaveItem($post)) {
                $material_receipt->delete();
                $this->_redirectUrl($this->getUrl('*/materialreceipt/create', array('_query'=>'supplier='.$post['supplier_id'].'&shipment_reference='.$post['shipment_reference'].'')));
                return;
            }
            
            $listItemValid = array();
            foreach ($post['data'] as $key => $data) {
                if ($data['sku']) {
                    if (in_array($data['sku'], $listItemValid)) {
                        continue;
                    }
                    $barcode_item = explode('_', $data['sku']);
                    $dataItem = $this->getDataItemMaterialReceipt($post['supplier_id'],$barcode_item);
                    if ($dataItem->getData()) {
                        try {
                            $material_receipt_item = Mage::getModel('opentechiz_purchase/materialreceiptitem');
                            $material_receipt_item->setMaterialReceiptId($material_receipt->getId());
                            $material_receipt_item->setProductSupplierId($dataItem->getId());
                            $material_receipt_item->setPoItemId($dataItem->getPoItemId());
                            $material_receipt_item->setCreatedAt(now());
                            $material_receipt_item->save();

                            $PO_item = Mage::getModel('opentechiz_purchase/orderitem')->load($dataItem->getPoItemId());
                            if ($PO_item->getId()) {
                                if ($data['cost']) {
                                    $PO_item->setPrice($data['cost']);
                                }

                                if ($shipment_reference) {
                                    if (!Mage::helper('opentechiz_purchase')->isActivePortalSupplier($dataItem)) {
                                        $PO_item->setShipmentReference($shipment_reference);
                                    }
                                }

                                $purchase_order = $PO_item->getPurchaseOrder();
                                if ($data['delivered_qty']) {
                                    $PO_item->setDeliveredQty($data['delivered_qty']);
                                }

                                $PO_item->setReceiptItemId($material_receipt_item->getId());
                                $PO_item->save();
                                // if qty = delivered qty mark the PO as complete
                                $purchase_order->validAndUpdateStatus();
                            }
                            
                            $listItemValid[] = $data['sku'];
                        } catch (Exception $e) {
                            Mage::getSingleton('core/session')->addError(
                                $e->getMessage()
                            );
                            $this->_redirect('*/*/');
                            return;
                        }
                    } 
                } 
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('opentechiz_purchase')->__('Material Receipt was successfully created'));
        }else{
            Mage::getSingleton('core/session')->addError(Mage::helper('opentechiz_purchase')->__('Missing data item or supplier.'));
        }
        
        $this->_redirect('*/*/');
        return;
    }

    public function loadDataItemReceiptAction(){
        $post = $this->getRequest()->getPost();
        if ($post['supplier_id'] && isset($post['barcode_item'])) {
            $supplier_id = (int)$this->getRequest()->getPost('supplier_id');
            $barcode_item = explode('_', $this->getRequest()->getPost('barcode_item'));
            $result = $this->getDataItemMaterialReceipt($supplier_id,$barcode_item);
            if (empty($result)) {
                $result['error'] = Mage::helper('opentechiz_purchase')->__('Can not found data for item purchase order.');
            }
            return $this->getResponse()->setBody(Zend_Json::encode($result));
        }
        return $this->getResponse()->setBody('');
    }

    public function getDataItemMaterialReceipt($supplier_id,$data){
        $po_item_id = $data[0];
        $product_id = $data[1];

        $collection = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection();
        $collection->getSelect()->joinLeft(array('supplier' => 'tsht_purchase_supplier'), 'main_table.sup_id = supplier.supplier_id', array('is_active_portal'));
        $collection->getSelect()->joinLeft(array('purchase_order' => 'tsht_purchase_order'), 'supplier.supplier_id = purchase_order.supplier_id', array('po_id','po_increment_id'));
        $collection->getSelect()->joinLeft(array('purchase_order_item' => 'tsht_purchase_order_item'), 'purchase_order.po_id = purchase_order_item.po_id', array('po_item_id'=>'purchase_order_item.id','shipped_qty','delivered_qty','sku','option','po_note'=>'purchase_order_item.note','po_id','shipment_date'));
        $collection->addFieldToFilter('main_table.sup_id',$supplier_id);
        $collection->addFieldToFilter('main_table.product_id',$product_id);
        $collection->addFieldToFilter('purchase_order_item.id',$po_item_id);

        $item = $collection->getFirstItem();
        if ($item->getData()) {
            $_product = Mage::getModel('catalog/product')->load($product_id);
            if ($_product->getData()) {
                $sales_price = Mage::helper('core')->currency($_product->getSpecialPrice(), true, false);
                $data_lowest = Mage::getModel('opentechiz_inventory/stockMovement')->getCollection()->getLowestCost($item->getSku());
                if (array_key_exists('lowest_cost', $data_lowest)) {
                    $item->setLowestCost($data_lowest['lowest_cost']);
                }
                $options = Mage::helper('opentechiz_purchase')->getOptionHtmlItem($item->getOption(), $item->getSku());
                $item->setProductName($_product->getName());
                $item->setSalesPrice($sales_price);
                $item->setAttribute($options);
            }
        }else{
            return null;
        }

        return $item;
    }

    public function checkIsPortalAction(){
        $post = $this->getRequest()->getPost();
        $result = array();
        if ($post['supplier_id']) {
            $sup = Mage::getModel('opentechiz_purchase/supplier')->load($post['supplier_id']);
            $result['is_active_portal'] = Mage::helper('opentechiz_purchase')->isActivePortalSupplier($sup);
            return $this->getResponse()->setBody(Zend_Json::encode($result));
        }
        return $this->getResponse()->setBody('');
    }
}