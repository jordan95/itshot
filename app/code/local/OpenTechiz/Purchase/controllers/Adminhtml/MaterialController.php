<?php

class OpenTechiz_Purchase_Adminhtml_MaterialController extends Mage_Adminhtml_Controller_Action
{
    protected function _initMaterial($idFieldName = 'id')
    {
        $materialID = (int)$this->getRequest()->getParam($idFieldName);
        $material = Mage::getModel('opentechiz_purchase/purchase');

        if ($materialID) {
            $material->load($materialID);
        }

        Mage::register('material_data', $material);
        return $this;
    }


    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp')
            ->_title($this->__('Purchase Order Material'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    /**
     * New action
     */
    public function newAction()
    {
        $this->loadLayout();

        $this->_setActiveMenu('erp');
        $this->getLayout()->getBlock('head')->setTitle($this->__('New Purchase Order / Material Purchase'));
        $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_purchase_gold_new/'));

        $this->renderLayout();
    }


    public function deleteAction() {
        if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $model = Mage::getModel('opentechiz_purchase/purchase');

                $model->setId($this->getRequest()->getParam('id'))->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * MassDelete action
     */
    public function massDeleteAction()
    {
        $matIDs = $this->getRequest()->getParam('item');
        if (!is_array($matIDs)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($matIDs as $matID) {
                    $sup = Mage::getModel('opentechiz_purchase/purchase')->load($matID);
                    $sup->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($matIDs)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

}