<?php

class OpenTechiz_Purchase_Adminhtml_SupplierController extends Mage_Adminhtml_Controller_Action
{
    protected function _initSupplier($idFieldName = 'id')
    {
        $supplierID = (int)$this->getRequest()->getParam($idFieldName);
        $supplier = Mage::getModel('opentechiz_purchase/supplier');

        if ($supplierID) {
            $supplier->load($supplierID);
        }

        Mage::register('supplier_data', $supplier);
        return $supplier;
    }


    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('erp')
            ->_title($this->__('Purchase Order'))
            ->_title($this->__('Supplier List'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }
    public function gridproductAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_supplier_edit_tab_product')->setIsAjaxCall(1)->toHtml()
        );
    }
    public function gridpurchaseAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_supplier_edit_tab_purchase')->toHtml()
        );
    }
    public function gridstoneAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_supplier_edit_tab_stone')->setIsAjaxCall(1)->toHtml()
        );
    }
    public function gridgoldAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_supplier_edit_tab_gold')->setIsAjaxCall(1)->toHtml()
        );
    }

    /**
     * Edit action
     */
    public function editAction()
    {
        $this->_initSupplier();
        $supplier = Mage::registry('supplier_data');
        $supplierID = $supplier->getId();
        if ($supplierID || $supplierID == $this->getRequest()->getParam('id')) {

            $this->_initAction();

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_supplier_edit'))
                ->_addLeft($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_supplier_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_purchase')->__('Supplier does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * New action
     */

    public function newAction() {
        $this->_forward('edit');
    }


    public function saveAction()
    {
        $supplier = $this->_initSupplier();
        if ($data = $this->getRequest()->getPost()) {
             $id = $this->getRequest()->getParam('id', false);
            //Validate current admin password
            $username = $this->getRequest()->getParam('username', null);
            $password = $this->getRequest()->getParam('password', null);
            if ($username || $password) {
                $currentPassword = $this->getRequest()->getParam('current_password', null);
                $this->getRequest()->setParam('current_password', null);
                unset($data['current_password']);
                $result = $this->_validateCurrentPassword($currentPassword);
                if (is_array($result)) {
                    foreach ($result as $error) {
                        $this->_getSession()->addError($error);
                    }
                    if ($id) {
                        $this->_redirect('*/*/edit', array('id' => $id));
                    } else {
                        $this->_redirect('*/*/new');
                    }
                    return;
                }
                $passwordConfirmation = $this->getRequest()->getParam('password_confirmation', null);
                if ($password != $passwordConfirmation) {
                    $this->_getSession()->addError(Mage::helper('adminhtml')->__('Password confirmation must be same as password.'));
                    if ($id) {
                        $this->_redirect('*/*/edit', array('id' => $id));
                    } else {
                        $this->_redirect('*/*/new');
                    }
                    return;
                }
            }

            $data = array_filter($data);
            if (isset($data['is_active_portal'])) {
                $data['is_active_portal'] = OpenTechiz_Purchase_Helper_Data::SUP_IS_ACTIVE_PORTAL;
            }else{
                $data['is_active_portal'] = OpenTechiz_Purchase_Helper_Data::SUP_NOT_ACTIVE_PORTAL;
            }

            $supplier->addData($data);
            if($supplier_id = $supplier->getId()){
                
                    $product_ids = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()->addFieldToFilter('sup_id',$supplier_id)->addFieldToSelect('id')->getData();
                     $product_ids = array_column($product_ids, 'product_id');
                     $new_id = array();
                     if(isset($data['products']))
                        $new_id = array_keys($data['products']);
                     $supplier_product = Mage::getModel('opentechiz_purchase/productsupplier');
                     foreach ($product_ids as $id) {
                         # code...
                        if(!in_array($id, $new_id)){
                            $supplier_product->load($id);
                            $supplier_product->delete();
                        }
                    }
                
                
             $stone_ids = Mage::getModel('opentechiz_purchase/stonesupplier')->getCollection()->addFieldToFilter('supplier_id',$supplier_id)->addFieldToSelect('purchase_stone_supplier_id')->getData();
                 $stone_ids = array_column($stone_ids, 'purchase_stone_supplier_id');
                 $new_id = array();
                 if(isset($data['stones']))
                 $new_id = array_keys($data['stones']);
                 $supplier_stone = Mage::getModel('opentechiz_purchase/stonesupplier');
                 foreach ($stone_ids as $id) {
                     # code...
                    if(!in_array($id, $new_id)){
                        $supplier_stone->load($id);
                        $supplier_stone->delete();
                    }
                }

                 $gold_ids = Mage::getModel('opentechiz_purchase/goldsupplier')->getCollection()->addFieldToFilter('supplier_id',$supplier_id)->addFieldToSelect('purchase_gold_supplier_id')->getData();
                 $gold_ids = array_column($gold_ids, 'purchase_gold_supplier_id');
                 $new_id = array();
                 if(isset($data['golds']))
                  $new_id = array_keys($data['golds']);
                 $supplier_gold = Mage::getModel('opentechiz_purchase/goldsupplier');
                 foreach ($gold_ids as $id) {
                     # code...
                    if(!in_array($id, $new_id)){
                        $supplier_gold->load($id);
                        $supplier_gold->delete();
                    }
                }
            }
            try {
                $supplier->save();

                if(isset($data['products']))
                foreach ($data['products'] as $key => $value) {
                    # code...
                    $value['sup_id'] = $supplier->getId();
                    $supplier_product = Mage::getModel('opentechiz_purchase/productsupplier');
                    $sup_product_collection = $supplier_product->getCollection()->addFieldToFilter('product_id',$value['product_id'])->addFieldToFilter('sup_id',$value['sup_id'])->getFirstItem();
                    if($sup_product_collection){
                        $supplier_product->load($sup_product_collection->getId());
                    }
                    if(!$sup_product_collection->getId()){

                        $value['last_cost'] = 0;
                    }

                    $supplier_product->addData($value);
                    $supplier_product->save();
                }
                 if(isset($data['golds']))
                foreach ($data['golds'] as $key => $value) {
                    # code...
                    $value['supplier_id'] = $supplier->getId();
                    $supplier_gold = Mage::getModel('opentechiz_purchase/goldsupplier');
                    $id =$supplier_gold->getCollection()->addFieldToFilter('gold_id',$value['gold_id'])->addFieldToFilter('supplier_id',$value['supplier_id'])->getFirstItem()->getId();
                    $supplier_gold->load($id);
                    $value['supplier_id'] = $supplier->getId();
                    
                    $supplier_gold->addData($value);
                    $supplier_gold->save();
                }
                if(isset($data['stones']))
                foreach ($data['stones'] as $key => $value) {
                    # code...
                    $value['supplier_id'] = $supplier->getId();
                    $supplier_stone = Mage::getModel('opentechiz_purchase/stonesupplier');
                    $id = $supplier_stone->getCollection()->addFieldToFilter('stone_id',$value['stone_id'])->addFieldToFilter('supplier_id',$value['supplier_id'])->getFirstItem()->getId();
                     $supplier_stone->load($id);
                    $value['supplier_id'] = $supplier->getId();
                    
                     $supplier_stone->addData($value);
                     $supplier_stone->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('opentechiz_purchase')->__('Item was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $supplier->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('opentechiz_purchase')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction() {
        if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $model = Mage::getModel('opentechiz_purchase/supplier');

                $model->setId($this->getRequest()->getParam('id'))->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }


    /**
     * MassDelete action
     */
    public function massDeleteAction()
    {
        $supIDs = $this->getRequest()->getParam('item');
        if (!is_array($supIDs)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($supIDs as $supID) {
                    $sup = Mage::getModel('opentechiz_purchase/supplier')->load($supID);
                    $sup->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($supIDs)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            case 'edit':
                $aclResource = 'admin/erp/purchase/supplier/suppliers/supplier_edit';
                break;
            case 'delete':
                $aclResource = 'admin/erp/purchase/supplier/suppliers/supplier_delete';
                break;
            default:
                $aclResource = 'admin/erp/purchase/supplier/suppliers';
                break;
        }
        return Mage::getSingleton('admin/session')->isAllowed($aclResource);
    }

    public function importAction(){
        if ($this->getRequest()->getPost()) {
            if (isset($_FILES['file']['name']) && (file_exists($_FILES['file']['tmp_name']))) {
                try {
                    $file = $_FILES['file']['name'];
                    $path = Mage::getBaseDir('var') . DS . 'import' . DS;
                    $uploader = new Varien_File_Uploader('file');
                    $uploader->setAllowedExtensions(array('csv', 'xml', 'xls'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);
                    $uploader->save($path, $file);
                    $file = Mage::app()->getConfig()->getTempVarDir() . '/import/' . $file;
                    $csv = new Varien_File_Csv();

                    $i = 0;
                    foreach ($csv->getData($file) as $line => $row) {
                        if($i==0){
                            $i++;
                            continue;
                        }
                        $phoneNumber = str_replace('-', '', str_replace(' ', '', $row[14]));
                        Mage::getModel('opentechiz_purchase/supplier')->setName($row[0])
                            ->setEmail($row[1])
                            ->setAddress($row[10])
                            ->setTelephone($phoneNumber)
                            ->setCity($row[15])
                            ->setZipcode($row[16])
                            ->setCountry($row[18])
                            ->setContact($row[4].' - '.$row[5].' - '.$row[6].' - '.$row[7])
                            ->save();
                    }

                    $admin = Mage::getSingleton('admin/session')->getUser();
                    if($admin->getId()){
                        Mage::dispatchEvent('opentechiz_admin_action_logger', array('admin_id' => $admin->getId(), 'action' => 'Import stone data'));
                    }

                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('The stone has been imported.'));
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
            $this->_redirect('*/*');
        }
    }
}