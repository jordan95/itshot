<?php

class OpenTechiz_Purchase_Adminhtml_ErpProductController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('erp')
            ->_title($this->__('ERP Products'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('opentechiz_purchase/adminhtml_erpProduct'));
        $this->getLayout()->getBlock('head')->setTitle($this->__('ERP Products'));
        
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->getLayout()->getBlock('head')->addJs('opentechiz/js/erp_product.js');
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/jquery-1.10.2.min.js');
        $this->getLayout()->getBlock('head')->addJs('lib/jquery/noconflict.js');
        $this->getLayout()->getBlock('head')->addJs('opentechiz/adminhtml/product/composite/configure.js');
        $this->getLayout()->getBlock('head')->addJs('varien/configurable.js');

        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();

        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_erpProduct_grid')->toHtml()
        );
    }

    public function getSupplierListAjaxAction(){
        $params = $this->getRequest()->getParams();
        $html = '';
        $html .= '<option value=""></option>';
        if(!isset($params['data'])){
            $html .= $this->getAllSupplierListHtml();
        }else {
            $params = $params['data'];
            if (count($params) == 1) {
                $productId = $params[0];
                $supProducts = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()
                    ->addFieldToFilter('product_id', $productId);
                $supIds = [];
                foreach ($supProducts as $supProduct) {
                    array_push($supIds, $supProduct->getSupId());
                }
                if(count($supIds) == 0){
                    $html .= $this->getAllSupplierListHtml();
                }else{
                    $suppliers = Mage::getModel('opentechiz_purchase/supplier')->getCollection()
                        ->addFieldToFilter('supplier_id', array('in' => $supIds))->setOrder('name', 'ASC');
                    foreach ($suppliers as $supplier) {
                        $html .= '<option value="' . $supplier->getId() . '">' . $supplier->getName() . '</option>';
                    }
                    $html .= '<option value="" disabled >-----------------------------------------------------------------------</option>';
                    $suppliers = Mage::getModel('opentechiz_purchase/supplier')->getCollection()
                        ->addFieldToFilter('supplier_id', array('nin' => $supIds))->setOrder('name', 'ASC');
                    foreach ($suppliers as $supplier) {
                        $html .= '<option value="' . $supplier->getId() . '">' . $supplier->getName() . '</option>';
                    }
                }
            } elseif (count($params) == 0) {
                $html .= $this->getAllSupplierListHtml();
            } else {
                $supProducts = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()
                    ->addFieldToFilter('product_id', array('in' => $params));
                $supIds = [];
                foreach ($supProducts as $supProduct) {
                    if (!in_array($supProduct->getSupId(), $supIds)) {
                        array_push($supIds, $supProduct->getSupId());
                    }
                }
                if(count($supIds) == 0){
                    $html .= $this->getAllSupplierListHtml();
                }else {
                    $suppliers = Mage::getModel('opentechiz_purchase/supplier')->getCollection()
                        ->addFieldToFilter('supplier_id', array('in' => $supIds))->setOrder('name', 'ASC');
                    foreach ($suppliers as $supplier) {
                        $html .= '<option value="' . $supplier->getId() . '">' . $supplier->getName() . '</option>';
                    }
                    $html .= '<option value="" disabled >-----------------------------------------------------------------------</option>';
                    $suppliers = Mage::getModel('opentechiz_purchase/supplier')->getCollection()
                        ->addFieldToFilter('supplier_id', array('nin' => $supIds))->setOrder('name', 'ASC');
                    foreach ($suppliers as $supplier) {
                        $html .= '<option value="' . $supplier->getId() . '">' . $supplier->getName() . '</option>';
                    }
                }
            }
        }
        $result = [];
        $result['html'] = $html;

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($result));
    }

    public function getAllSupplierListHtml(){
        $html = '';
        $html .= '<option value="" disabled >-----------------------------------------------------------------------</option>';
        $suppliers = Mage::getModel('opentechiz_purchase/supplier')->getCollection()->setOrder('name', 'ASC');
        foreach ($suppliers as $supplier) {
            $html .= '<option value="' . $supplier->getId() . '">' . $supplier->getName() . '</option>';
        }
        return $html;
    }

    public function addProductAndCreatePOAction(){
        $productIds = $this->getRequest()->getParam('product_id');
        $supplierId = $this->getRequest()->getParam('supplier');
        if(count($productIds) > 20){
            Mage::getSingleton('adminhtml/session')->addError($this->__('Number of Item(s) is limited to 20'));
            $this->_redirect('adminhtml/erpProduct/index');
            return;
        }
        
        $productCollection = Mage::getModel('catalog/product')->getCollection()
            ->addFieldToFilter('entity_id', array('in' => $productIds))
            ->addAttributeToSelect(array('price', 'special_price'));

        //add products to supplier
        foreach ($productCollection as $product){
            $POproductCollection =  Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()->addFieldToFilter('product_id', $product->getId())
                ->addFieldToFilter('sup_id', $supplierId);
            //skip if exist
            if(count($POproductCollection) > 0){
                continue;
            }

            //add product to supplier
            $data = [];
            $data['product_id'] = $product->getId();
            $data['sup_id'] = $supplierId;
            $data['last_cost'] = 0;
            $data['sup_sku'] = $product->getSku();

            Mage::getModel('opentechiz_purchase/productsupplier')->setData($data)->save();
        }

//        //create new PO
//        $newPO = Mage::helper('opentechiz_purchase')->createNewOrder();
//        $newPO->setSupplierId($supplierId)->save();
//
//        //add product to PO
//        foreach ($productCollection as $product){
//            $data = [];
//            $data['product_id'] = $product->getId();
//            $data['po_id'] = $newPO->getId();
//            $data['sku'] = $product->getSku();
//            $data['qty'] = 1;
//            $data['delivered_qty'] = 0;
//            $data['price'] = 0;
//            Mage::getModel('opentechiz_purchase/orderitem')->setData($data)->save();
//        }

        //redirect
        $productIdsString = implode(',', $productIds);
        Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Purchase Order Item(s) added'));
        $this->_redirect('adminhtml/purchase_product/create', array('supplier' => $supplierId, 'productIds' => $productIdsString));
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/purchase/supplier/erpproduct');
    }

    public function getSupplierPurchasedListRadioBoxAction(){
        $params = $this->getRequest()->getParams();
        $html = '<strong>Select supplier to add to master PO:</strong> <div>';
        $productId = $params['product_id'];
        $isUseQuote = isset($params['quote_item_id']);
        if($isUseQuote) {
            $quoteItemId = $params['quote_item_id'];
        }
        $supProducts = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()
            ->addFieldToFilter('product_id', $productId)->addFieldToFilter('last_cost', array('gt' => 0))
            ->addFieldToFilter('last_cost_date', array('notnull' => true));
        $supIds = [];
        foreach ($supProducts as $supProduct) {
            array_push($supIds, $supProduct->getSupId());
        }
        if(count($supIds) == 0){
            $html = 'No data';
        }else{
            $suppliers = Mage::getModel('opentechiz_purchase/supplier')->getCollection()
                ->addFieldToFilter('supplier_id', array('in' => $supIds))->setOrder('name', 'ASC');
            foreach ($suppliers as $supplier) {
                $name = $supplier->getName();
                $product_supplier = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()->addFieldToFilter('product_id', $productId)->addFieldToFilter('sup_id', $supplier->getId())->addFieldToFilter('last_cost_date',array(array('notnull'=>true),array('neq'=>'')));
                if($product_supplier){
                    foreach($product_supplier as $item){
                        $last_cost = '<strong> | Last Cost:</strong>'. Mage::getModel('directory/currency')->format(
                                $item->getLastCost(),
                                array('display'=>Zend_Currency::NO_SYMBOL),
                                false
                            );
                        if(!empty($item->getLastCostDate())){
                            $lastPurchaseDate = '<strong> | Last Purchased:</strong> ' . Mage::helper('core')->formatDate($item->getLastCostDate());
                        }else{
                            $lastPurchaseDate = "";
                        }
                        if(!empty($lastPurchaseDate)){
                            $name .= $lastPurchaseDate;
                        }
                        if($last_cost !=''){
                            $name .= $last_cost;
                        }
                    }
                }
                if($isUseQuote){
                    $html .= '<input product_id="'.$productId.'" quote_item_id="'.$quoteItemId.'" type="radio" name="supplier_id" value="'.$supplier->getId().'" onchange="ProductAdd(this);return false;">
                          <label>' . $name . '</label><br>';
                }else{
                    $html .= '<input product_id="'.$productId.'" type="radio" name="supplier_id" value="'.$supplier->getId().'" onchange="ProductAdd(this);return false;">
                          <label>' . $name . '</label><br>';
                }

            }
            $html .= '</div>';
        }

        $result = [];
        $result['html'] = $html;

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($result));
    }
}