<?php

class OpenTechiz_Purchase_SupplierController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        if (!$this->_getSession()->isLoggedIn()) {
            return $this->_redirect('*/*/login');
        }
        if (!$this->_validateFormKey()) {
            Mage::getSingleton('core/session')->addError("Form Key invalid.");
            return $this->_redirect('*/*/');
        }
        if ($this->getRequest()->isPost()) {
            $filter = $this->getRequest()->getPost('filter');
            $params = array();
            if ($filter) {
                $filter = base64_encode(http_build_query($filter));
                $params = array('filter' => $filter);
            }
            return $this->_redirect('*/*/', $params);
        } else {
            $filter = $this->getRequest()->getParam('filter');
            $filter = Mage::helper('adminhtml')->prepareFilterString($filter);
            $this->_getSession()->setFilterData($filter);
        }
        $page = (int) $this->getRequest()->getQuery('page', 1);
        $this->_getSession()->setPage($page);
        $limit = (int) $this->getRequest()->getQuery('limit', 10);
        $this->_getSession()->setLimit($limit);
        $orderBy = $this->getRequest()->getQuery('order_by', 'created_at');
        $this->_getSession()->setOrderBy($orderBy);
        $orderDir = $this->getRequest()->getQuery('order_dir', 'desc');
        $this->_getSession()->setOrderDir($orderDir);
        $showComplete = $this->getRequest()->getQuery('show_complete', true);
        $this->_getSession()->setShowComplete($showComplete);
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle("Supplier Portal");
        $this->renderLayout();
    }

    public function saveAction()
    {
        if (!$this->_getSession()->isLoggedIn()) {
            return $this->_redirect('*/*/login');
        }
        if (!$this->_validateFormKey()) {
            Mage::getSingleton('core/session')->addError("Form Key invalid.");
            return $this->_redirect('*/*/');
        }
        if (!$this->getRequest()->isPost()) {
            Mage::getSingleton('core/session')->addError("Only support method POST");
            return $this->_redirect('*/*/');
        }
        $shipment_ref_global =  $this->getRequest()->getPost('shipment_ref_global');
        $purchases = $this->getRequest()->getPost('purchases', array());
        try {
            foreach ($purchases as $po_id => $v) {
                $po = Mage::getModel('opentechiz_purchase/purchase')->load($po_id);
                if ($po->getSupplierId() != $this->_getSession()->getId()) {
                    Mage::throwException("Permission Denied to save PO" . $po->getPoIncrementId());
                }
                if (!$po->getConfirmed() && !empty($v['confirmed'])) {
                    $po->setStatus(OpenTechiz_Purchase_Helper_Data::PO_STATUS_WAITING_FOR_DELIVERY);
                    $po->setConfirmed(true);
                }

                if (!empty($v['items'])) {
                    $updated_all_item = true;
                    foreach ($v['items'] as $item_id => $item_v) {
                        $item = Mage::getModel('opentechiz_purchase/orderitem')->load($item_id);
                        if ($item->getPoId() != $po_id) {
                            Mage::throwException("Permission Denied to save Item");
                        }

                        if (!empty($item_v['shipped_qty'])) {
                            if ($this->isValidShipmentQty($item,$item_v['shipped_qty'])) {
                                $item->setShippedQty((int) $item_v['shipped_qty']);
                                $item->logDelivery();
                                // save shipment date
                                if (!empty($item_v['shipment_date'])) {
                                    $item->setShipmentDate(Mage::getModel('core/date')->gmtDate('Y/m/d',$item_v['shipment_date']));
                                }else{
                                    if ($item->getShipmentDate() == '') {
                                        $item->setShipmentDate(Mage::getModel('core/date')->gmtDate('Y/m/d'));
                                    }
                                }
                            }else{
                                Mage::throwException("Can not save the Shipped Qty.It is greater than Qty or less than old Shipped Qty.");
                            }
                        }

                        // save the shipment ref by customer input row on the grid
                        if (!empty($item_v['shipment_reference'])) {
                            if ($shipment_reference = $this->formatShipmentRef($item,$item_v['shipment_reference'])) {
                                $item->setShipmentReference($shipment_reference);
                            }else{
                                Mage::throwException("Can not save the Shipment Ref for " . $po->getPoIncrementId() . ".Please check shipped qty and the number of shipment.");
                            }
                        }
                        // save the shipment ref by the global input from top on the grid
                        if (!empty($item_v['item_selected']) && !empty($shipment_ref_global)) {
                            if (empty($item_v['shipped_qty'])) {
                                Mage::throwException("Please update the shipped qty: " . $po->getPoIncrementId());
                            }
                            
                            if (!in_array($po->getStatus(), array(OpenTechiz_Purchase_Helper_Data::PO_STATUS_COMPLETE,OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL))) {
                                if ($shipment_reference = $this->formatShipmentRef($item,$shipment_ref_global,true)) {
                                    $item->setShipmentReference($shipment_reference);
                                }else{
                                    Mage::throwException("Can not save the Shipment Ref for " . $po->getPoIncrementId() . ".Please check shipped qty and the number of shipment.");
                                }
                            }
                        }
                        
                        $item->save();
                        // #2859 Update status to Waiting on Delivery when supplier update the Shipped QTY for all items on the order
                        if ($item->getQty() != $item->getShippedQty()) {
                            $updated_all_item = false;
                        }
                    }

                    if ($updated_all_item) {
                        $po->setStatus(OpenTechiz_Purchase_Helper_Data::PO_STATUS_WAITING_FOR_DELIVERY);
                    }
                }
                $po->save();
            }
            Mage::getSingleton('core/session')->addSuccess("Your changes is saved.");
        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError($e->getMessage());
        }
        return $this->_redirect('*/*/');
    }

    public function isValidShipmentQty($item,$shipment_qty_post){
        if ((int)$shipment_qty_post > $item->getQty()) {
            return false;
        }

        if ((int)$shipment_qty_post < $item->getShippedQty()) {
            return false;
        }

        return true;
    }

    public function formatShipmentRef($item,$shipment_ref_post,$is_mass_action = false){
        if ($shipment_ref_post == $item->getShipmentReference()) {
            return $shipment_ref_post;
        }

        if ($is_mass_action) {
            $shipment_reference = array($item->getShipmentReference(),$shipment_ref_post);
            // return implode(',', array($item->getShipmentReference(),$shipment_reference));
        }else{
            $old_shipment_reference = array_map('trim', explode(',', $item->getShipmentReference()));
            $new_shipment_reference = array_map('trim', explode(',', $shipment_ref_post));
            if (count($new_shipment_reference) > count($old_shipment_reference)) {
                $shipment_reference = array_merge($old_shipment_reference, $new_shipment_reference); //merge old data in db and new data post
                $shipment_reference = array_unique($shipment_reference); // remove duplicate value from array
                $shipment_reference = array_filter($shipment_reference); //remove null value from array
            }else{
                return $shipment_ref_post;
            }
        }
        
        if (count($shipment_reference) > (int)$item->getData('shipped_qty')) {
            return false;
        }

        if(count($shipment_reference) > 1){
            return implode(',', $shipment_reference);
        }else{
            return $shipment_ref_post;
        }
    }

    public function loginAction()
    {
        if ($this->_getSession()->isLoggedIn()) {
            return $this->_redirect('*/*/');
        }
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle("Supplier Portal Login");
        $this->renderLayout();
    }

    public function logoutAction()
    {
        $session = $this->_getSession();
        $session->logout()->renewSession();
        return $this->_redirect('*/*/login');
    }

    public function loginPostAction()
    {
        if ($this->_getSession()->isLoggedIn()) {
            return $this->_redirect('*/*/');
        }

        if (!$this->_validateFormKey()) {
            Mage::getSingleton('core/session')->addError("Form Key invalid.");
            return $this->_redirect('*/*/login');
        }

        if (!$this->getRequest()->isPost()) {
            Mage::getSingleton('core/session')->addError("Only support method POST");
            return $this->_redirect('*/*/login');
        }

        $session = $this->_getSession();
        $login = $this->getRequest()->getPost('login');
        if (!empty($login['username']) && !empty($login['password'])) {
            try {
                $session->login($login['username'], $login['password']);
                return $this->_redirect('*/*/');
            } catch (Mage_Core_Exception $e) {
                Mage::log($e->getCode(), null, 'test.log');
                switch ($e->getCode()) {
                    case OpenTechiz_Purchase_Model_Supplier::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                        $message = $e->getMessage();
                        break;
                    case OpenTechiz_Purchase_Model_Supplier::EXCEPTION_INVALID_ACTIVE_PORTAL:
                        $message = $e->getMessage();
                        break;
                    default:
                        $message = $e->getMessage();
                }
                Mage::getSingleton('core/session')->addError($message);
                $session->setUsername($login['username']);
                return $this->_redirect('*/*/login');
            } catch (Exception $e) {
                
            }
        } else {
            $session->addError($this->__('Login and password are required.'));
        }
    }

    /**
     * 
     * @return OpenTechiz_Purchase_Model_Supplier_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('opentechiz_purchase/supplier_session');
    }

}
