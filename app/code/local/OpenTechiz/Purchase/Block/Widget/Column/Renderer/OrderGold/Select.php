<?php

class OpenTechiz_Purchase_Block_Widget_Column_Renderer_OrderGold_Select extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $html = '<input readonly="readonly" class="goldadd" type="checkbox" name="add['.$row->getGoldId().'][id]" value="'.$row->getGoldId().'" onchange="GoldAdd(this)" />';

        return $html;
    }

}