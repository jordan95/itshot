<?php

class OpenTechiz_Purchase_Block_Widget_Column_Renderer_Format_PoTotal extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $html = '';
      
        $id = $row->getId();
        $pos = Mage::getModel('opentechiz_purchase/purchase')->getCollection()->AddFieldtoFilter('supplier_id',$id);
        $total = 0;
        foreach ($pos as $po) {
            # code...
            $total += $po->getTotalIncludeTax();
        }
        $html =  Mage::getModel('directory/currency')->format($total, array('display'=>Zend_Currency::NO_SYMBOL), false);
        return $html;
    }
}