<?php

class OpenTechiz_Purchase_Block_Widget_Column_Renderer_OrderStone_Select extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $html = '<input readonly="readonly" type="checkbox" class="stoneadd" name="add['.$row->getStoneId().'][id]" value="'.$row->getStoneId().'" onchange="StoneAdd(this)" />';

        return $html;
    }

}