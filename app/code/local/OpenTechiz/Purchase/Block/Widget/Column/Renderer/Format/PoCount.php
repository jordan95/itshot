<?php

class OpenTechiz_Purchase_Block_Widget_Column_Renderer_Format_PoCount extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $html = '';
      
        $id = $row->getId();
        $pos = Mage::getModel('opentechiz_purchase/purchase')->getCollection()->AddFieldtoFilter('supplier_id',$id);
      
        $html = count($pos)?count($pos):'0';
        return $html;
    }
}