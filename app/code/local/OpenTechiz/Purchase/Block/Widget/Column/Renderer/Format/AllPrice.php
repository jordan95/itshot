<?php

class OpenTechiz_Purchase_Block_Widget_Column_Renderer_Format_AllPrice extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $html = '';
        $allPrice = $row->getData('price');
        $poPrice = $row->getData('po_total');
        $totalIncludeTax = $row->getData('total_include_tax');

        if(isset($allPrice))
        {
            $html = Mage::helper('core')->currency($allPrice, true, false);
        }
        else if (isset($poPrice))
        {
            $html = Mage::helper('core')->currency($poPrice, true, false);
        }
        else if (isset($totalIncludeTax))
        {
            $html = Mage::helper('core')->currency($totalIncludeTax, true, false);
        }

        return $html;
    }
}