<?php

class OpenTechiz_Purchase_Block_Widget_Column_Renderer_OrderStone_Price extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
		$price = Mage::getModel('directory/currency')->format($row->getPrice(), array('display'=>Zend_Currency::NO_SYMBOL), false);


        $html = $price ;

        return $html;
    }
}