<?php

class OpenTechiz_Purchase_Block_Adminhtml_Return extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_return';
        $this->_blockGroup = 'opentechiz_purchase';
        $this->_headerText = Mage::helper('opentechiz_purchase')->__('PO Return');

        parent::__construct();
        $this->_removeButton('add');
    }


}