<?php

class OpenTechiz_Purchase_Block_Adminhtml_ErpProduct extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_erpProduct';
        $this->_blockGroup = 'opentechiz_purchase';
        $this->_headerText = Mage::helper('opentechiz_purchase')->__('ERP Products');

//        $this->_addButton('supplierLink', array(
//            'label' => Mage::helper('opentechiz_material')->__('ERP Products'),
//            'onclick' => '',
//            'class' => 'add',
//            'after_html' => '<form id="uploadForm" method="POST" action="'.$this->getUrl('*/*/import', array()).'" enctype="multipart/form-data">
//            <input name="form_key" type="hidden" value="'.Mage::getSingleton('core/session')->getFormKey().'" />
//            <input type="file" name="file" style="display:none;" id="uploadTarget"/>
//        </form>
//        <script type="text/javascript">
//            document.getElementById(\'uploadTarget\').addEventListener(\'change\', function(){
//                document.getElementById(\'uploadForm\').submit();
//            }, false);
//        </script>',
//        ));

        parent::__construct();

        $this->_removeButton('add');


    }


}