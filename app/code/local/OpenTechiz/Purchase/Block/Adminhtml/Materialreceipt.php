<?php

class OpenTechiz_Purchase_Block_Adminhtml_Materialreceipt extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_materialreceipt';
        $this->_blockGroup = 'opentechiz_purchase';
        $this->_headerText = Mage::helper('opentechiz_purchase')->__('Material Receipt List');
        $this->_addButtonLabel = $this->__('Add New Material Received');
        parent::__construct();
    }
}