<?php

class OpenTechiz_Purchase_Block_Adminhtml_Renderer_DatabaseDate extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $date = date('Y-m-d H:i:s', strtotime($row->getData($this->getColumn()->getIndex()) . ' +5 hour'));
        $date = Mage::helper('core')->formatDate($date, 'medium', true);
        $dateArray = explode(' ', $date);
        unset($dateArray[count($dateArray)-1]);
        unset($dateArray[count($dateArray)-1]);

        $date = implode(' ', $dateArray);

        return $date;
    }
}