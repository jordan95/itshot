<?php

class OpenTechiz_Purchase_Block_Adminhtml_Renderer_InputData extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $index = $this->getColumn()->getIndex();
        $itemId = $row->getData('id');
        $currentData = $row->getData($index);
        $disable = '';
        if($index == 'delivered_qty'){
            $type = 'type="number"';
            //check for qty/delivered
            $qty = $row->getData('qty');
            if($currentData == $qty){
                $disable = 'disabled=true';
            }
        }else{
            $type = '';
        }
        $html = '<input id="'.$itemId.'_'.$index.'" '.$type.' '.$disable.' name="'.$itemId.'_'.$index.'" value="'.$currentData.'" onchange="addToForm(\''.$itemId.'_'.$index.'\')"/>';

        return $html;
    }
}