<?php

class OpenTechiz_Purchase_Block_Adminhtml_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $productId = $row->getData('product_id');
        $sku = $row->getData('sku');
        $product = Mage::getModel('catalog/product')->load($productId);
        $product->setSku($sku);

        $imageUrl = Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(120, 120)->__toString();

        return '<img src="'.$imageUrl.'"/>';
    }
}