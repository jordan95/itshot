<?php

class OpenTechiz_Purchase_Block_Adminhtml_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $status = $row->getData('status');
        if($status == 1){
            return 'Yes';
        }else{
            return 'No';
        }
    }
}