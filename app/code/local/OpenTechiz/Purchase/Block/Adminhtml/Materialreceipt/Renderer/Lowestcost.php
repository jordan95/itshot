<?php

class OpenTechiz_Purchase_Block_Adminhtml_Materialreceipt_Renderer_Lowestcost extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
    	$sku = $row->getSku();
		$data = Mage::getModel('opentechiz_inventory/stockMovement')->getCollection()->getLowestCost($sku);
		if (array_key_exists('lowest_cost', $data)) {
			return $data['lowest_cost'];
		}
		return null;
	}
}