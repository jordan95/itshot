<?php

class OpenTechiz_Purchase_Block_Adminhtml_Materialreceipt_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'opentechiz_purchase';
        $this->_controller = 'adminhtml_materialreceipt';
        parent::__construct();
        $this->_removeButton('save');
        $this->_removeButton('detele');
        $this->_updateButton('save', 'label', Mage::helper('opentechiz_purchase')->__('Save'));

        $this->addButton('save', array(
            'label' => Mage::helper('opentechiz_purchase')->__('Save'),
            'class' => 'save',
            'onclick' => "requestActionButton(this,'". $this->getUrl('*/materialreceipt/save') ."')"
        ));

        $forcusText = '<span><span><span>'.$this->helper('opentechiz_purchase')->__("Was pressed and disabled").'</span></span></span>';
        $this->_formScripts[] = 'function requestActionButton(ele, url){
            var confirmContent = "Are you sure ?";
            if(confirm(confirmContent)){
                ele.innerHTML = "'. $forcusText .'";
                ele.addClassName("disabled");
                ele.disabled = true;
                editForm.submit($("edit_form").action);
            }
        }';
    }

    public function getHeaderText()
    {
        return Mage::helper('opentechiz_purchase')->__('Add Material Receipt');
    }
}