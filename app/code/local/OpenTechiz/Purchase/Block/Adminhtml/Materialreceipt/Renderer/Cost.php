<?php

class OpenTechiz_Purchase_Block_Adminhtml_Materialreceipt_Renderer_Cost extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
    	$html = '';
        $price = $row->getPrice();
        $html = Mage::helper('core')->currency($price, true, false);
        return $html;
    	
	}
}