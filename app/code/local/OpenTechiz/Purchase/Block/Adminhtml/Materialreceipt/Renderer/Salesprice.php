<?php

class OpenTechiz_Purchase_Block_Adminhtml_Materialreceipt_Renderer_Salesprice extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $product_id = $row->getProductId();
        $_product = Mage::getModel('catalog/product')->load($product_id);
        if ($_product->getData()) {
            $html = Mage::helper('core')->currency($_product->getSpecialPrice(), true, false);
            return $html;
        }
        return null;
}
}