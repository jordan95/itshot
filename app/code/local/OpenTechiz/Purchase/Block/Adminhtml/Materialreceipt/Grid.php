<?php

class OpenTechiz_Purchase_Block_Adminhtml_Materialreceipt_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('materialreceipt_grid');
        $this->setDefaultSort('material_receipt_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_purchase/materialreceiptitem')->getCollection();
        $collection->getSelect()->joinLeft(array('receipt' => 'tsht_purchase_material_receipt'), 'main_table.material_receipt_id= receipt.id', array());
        $collection->getSelect()->joinLeft(array('po_item' => 'tsht_purchase_order_item'), 'main_table.po_item_id = po_item.id', array('sku','qty','shipped_qty','delivered_qty','shipment_reference','product_id','note','price'));
        $collection->getSelect()->joinLeft(array('po' => 'tsht_purchase_order'), 'po_item.po_id = po.po_id', array('po_increment_id','status'));
        $collection->getSelect()->joinLeft(array('product_supplier' => 'tsht_purchase_product_supplier'), 'main_table.product_supplier_id  = product_supplier.id', array('sup_sku'));
        $collection->getSelect()->joinLeft(array('supplier' => 'tsht_purchase_supplier'), 'product_supplier.sup_id = supplier.supplier_id', array('supplier_id'));
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('material_receipt_id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('MR ID'),
            'index'     => 'material_receipt_id',
            'align'     => 'center',
            'width'     => '50',
        ));

        $this->addColumn('supplier_name', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Supplier'),
            'index'     => 'supplier_id',
            'filter_index' => 'supplier.supplier_id',
            'align'     => 'center',
            'width'     => '50',
            'type'      =>'options',
            'options'   => Mage::helper('opentechiz_purchase')->getAllSupplier(),
        ));

        $this->addColumn('sup_sku', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Supplier SKU'),
            'index'     => 'sup_sku',
            'type'      =>'text',
        ));

        $this->addColumn('po_increment_id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('PO Increment #'),
            'index'     => 'po_increment_id',
            'type'      =>'text',
        ));

        $this->addColumn('sku', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('SKU'),
            'index'     => 'sku',
            'type'      =>'text',
        ));

        $this->addColumn('image', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Image'),
            'index'     => 'product_id',
            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_Renderer_Image',
            'filter' => false,
            'sortable' => false,
            'align'     => 'center',
            'width'     => '100',
        ));

        $this->addColumn('qty', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('PO Qty'),
            'index'     => 'qty',
            'type'      => 'number',
        ));

        $this->addColumn('shipped_qty', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Shipped Qty'),
            'index'     => 'shipped_qty',
            'type'      => 'number',
        ));

        $this->addColumn('delivered_qty', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Delivered Qty'),
            'index'     => 'delivered_qty',
            'type'      => 'number',
        ));

        $this->addColumn('price', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Cost'),
            'filter_index' => 'po_item.price',
            'index'     => 'price',
            'type'      => 'number',
            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_Materialreceipt_Renderer_Cost',
        ));

        $this->addColumn('sales_price', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Sales Price'),
            'index'     => 'sales_price',
            'type'      => 'number',
            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_Materialreceipt_Renderer_Salesprice',
            'filter_condition_callback' => array($this, '_salespriceFilter')
        ));

        $store = $this->_getStore();
        $this->addColumn('lowest_cost', array(
            'header'        => Mage::helper('opentechiz_purchase')->__('Lowest Cost'),
            'index'         => 'lowest_cost',
            'filter_index'  => 'po_item.price',
            'type'          => 'number',
            'filter'        => false,
            'sortable'      => false,
            'renderer'      => 'OpenTechiz_Purchase_Block_Adminhtml_Materialreceipt_Renderer_Lowestcost'
        ));

        $this->addColumn('lowest_cost_date', array(
            'header'        => Mage::helper('opentechiz_purchase')->__('Lowest Cost Date'),
            'type'          => 'date',
            'filter'        => false,
            'sortable'      => false,
            'renderer'      => 'OpenTechiz_Purchase_Block_Adminhtml_Materialreceipt_Renderer_Lowestcostdate'
        ));

        $this->addColumn('note', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('PO Notes'),
            'index'     => 'note',
            'type'      => 'text',
        ));

        $this->addColumn('shipment_reference', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Shipment Reference'),
            'index'     => 'shipment_reference',
            'type'      => 'text',
        ));

        $this->addColumn('order_references', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Order References'),
            'filter'    => false,
            'sortable'  => false,
            'index'     => 'sku',
            'renderer'  => 'OpenTechiz_Inventory_Block_Adminhtml_Renderer_OrderNumber'
        ));

        $this->addColumn('status', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('PO Status'),
            'index'     => 'status',
            'type'      =>'options',
            'options'   => Mage::helper('opentechiz_purchase')->getPurchaseOrderStatus(),
        ));

        return parent::_prepareColumns();
    }

    protected function _salespriceFilter($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        if(!$value){
            return $this;
        }
        $collection->getSelect()->joinLeft(array('product' => 'tsht_catalog_product_entity'), 'po_item.product_id = product.entity_id', array('entity_id'));
        $collection->getSelect()->joinLeft(array('decimal' => 'tsht_catalog_product_entity_decimal'), 'product.entity_id = decimal.entity_id', array('attribute_id','value'));
        $collection->getSelect()->joinLeft(array('eav' => 'tsht_eav_attribute'), 'decimal.attribute_id = eav.attribute_id', array('attribute_code'));
        $collection->getSelect()->where('eav.attribute_code LIKE "special_price"');
        if(isset($value['from']) && $value['from'])
        {
            $collection->addFieldToFilter('decimal.value', array('gteq' => $value['from']));
        }

        if(isset($value['to']) && $value['to'])
        {
            $collection->addFieldToFilter('decimal.value', array('lteq' => $value['to']));
        }

        return $this;
    }

    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }


}