<?php

class OpenTechiz_Purchase_Block_Adminhtml_Materialreceipt_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('opentechiz/purchase/material_receipt/new_item_receipt.phtml');
    }

    public function getAllSkuItem(){
        $supplier_id = (int)$this->getRequest()->getParam('supplier');

        $collection = Mage::getModel('opentechiz_purchase/orderitem')->getCollection();
        $collection->join(array('purchase' => 'opentechiz_purchase/purchase'), 'purchase.po_id = main_table.po_id', array('status' => 'purchase.status'));
        $collection->addFieldToFilter('supplier_id',$supplier_id);
        $collection->addFieldToFilter('status', array('in' => array(OpenTechiz_Purchase_Helper_Data::PO_STATUS_WAITING_FOR_SUPPLIER,OpenTechiz_Purchase_Helper_Data::PO_STATUS_WAITING_FOR_DELIVERY)));
        $collection->getSelect()->where('ifnull(main_table.delivered_qty,0) < ifnull(main_table.qty,0) AND main_table.receipt_item_id is null');
        $result = array();
        if ($collection->getSize()) {
            foreach ($collection as $po_item) {
                $barcode = $po_item->getId() . '_' . $po_item->getProductId();
                $result[$barcode] = $po_item->getSku();
            }
        }
        return $result;
    }

    public function getSupplierId() {
        return $this->getRequest()->getParam('supplier');
    }

    public function getShipmentReferenceParam() {
        return $this->getRequest()->getParam('shipment_reference');
    }

}