<?php

class OpenTechiz_Purchase_Block_Adminhtml_Materialreceipt_New extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();

    }

    public function getBackUrl()
    {
        return $this->getUrl('adminhtml/materialreceipt/index', array());
    }

    public function getNewSupplierUrl()
    {
        return $this->getUrl('adminhtml/supplier/new');
    }

    /**
     *
     */
    public function getSuppliersAsCombo($name='supplier')
    {
        $retour = '<select  id="'.$name.'" name="'.$name.'">';

        $collection = Mage::getModel('opentechiz_purchase/supplier')
            ->getCollection()
            ->setOrder('name', 'asc');
        foreach ($collection as $item)
        {
            $retour .= '<option value="'.$item->getId().'">'.$item->getname().'</option>';
        }

        $retour .= '</select>';
        return $retour;
    }
}