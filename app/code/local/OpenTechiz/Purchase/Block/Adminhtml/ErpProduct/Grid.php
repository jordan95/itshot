<?php

class OpenTechiz_Purchase_Block_Adminhtml_ErpProduct_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('opentechiz/purchase/erp_product.phtml');
        $this->setId('erpProductGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()->addFieldToFilter('type_id', array('nin' => array('virtual', 'quotation')));
        $query = '(SELECT *, GROUP_CONCAT(DISTINCT purchase.sup_id SEPARATOR \',\') as suppliers 
                  FROM tsht_purchase_product_supplier as purchase 
                  GROUP BY product_id)';
        $manu_table = new Zend_Db_Expr($query);

        $collection->getSelect()
            ->joinLeft(array('purchase_product' => $manu_table),
                'entity_id = purchase_product.product_id',
                array('suppliers' => 'purchase_product.suppliers'));
        $collection->addFilterToMap('suppliers', 'purchase_product.suppliers');

        $collection->addAttributeToSelect('name');
        $collection->addAttributeToSelect('price');
        $collection->addAttributeToSelect('special_price');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=> true));
    }

    protected function _prepareColumns()
    {
        $store = Mage::app()->getStore();
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Product #'),
            'index'     => 'entity_id',
            'align'     => 'center',
            'width'     => '50'
        ));
        $this->addColumn('product_name', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Product Name'),
            'index'     => 'name',
        ));
        $this->addColumn('sku', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('SKU'),
            'index'     => 'sku',
            'filter_condition_callback' => array($this, '_skuFilter')
        ));
        $this->addColumn('price', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Sale Price'),
            'index'     => 'special_price',
            'type'      => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));
        $this->addColumn('suppliers', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Suppliers'),
            'index'     => 'suppliers',
            'sortable'  => false,
//            'filter'    => false,
            'type'      =>'options',
            'options'   => Mage::helper('opentechiz_purchase')->getAllSupplier(),
            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_ErpProduct_Renderer_Suppliers',
            'filter_condition_callback' => array($this, '_supplierFilter')
        ));
//        $this->addColumn('action', array(
//            'header'    => Mage::helper('opentechiz_purchase')->__('Action'),
//            'width'     => '130',
//            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_ErpProduct_Renderer_SupplierListSelect',
//            'filter'    => false,
//            'sortable'  => false,
//            'is_system' => true,
//        ));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('product_id');
        $this->getMassactionBlock()->setUseSelectAll(false);

        foreach (Mage::helper('opentechiz_purchase')->getAllSupplier() as $id => $supplier){
            $this->getMassactionBlock()->addItem($id, array(
                'label' => Mage::helper('opentechiz_purchase')->__($supplier),
                'url' => $this->getUrl('*/*/addProductAndCreatePO/supplier/'.$id),
            ));
        }

//            'additional' => array(
//                'visibility' => array(
//                    'name' => 'supplier',
//                    'type' => 'select',
//                    'class' => 'required-entry',
//                    'label' => Mage::helper('opentechiz_purchase')->__('Choose Supplier'),
//                    'values' => Mage::helper('opentechiz_purchase')->getAllSupplier()
//                )
//            ),

        return $this;
    }

    protected function _supplierFilter($collection, $column)
    {
        $value = $column->getFilter()->getValue();

        $collection->getSelect()->where('purchase_product.suppliers = '.$value .' 
                                        OR purchase_product.suppliers like "%,'.$value .'" 
                                        OR purchase_product.suppliers like "%,'.$value .',%" 
                                        OR purchase_product.suppliers like "'.$value .',%"');

        return $this;
    }

    protected function _skuFilter($collection, $column)
    {
        $value = $column->getFilter()->getValue();

        $collection->getSelect()->where('sku = "'.$value.'"');

        return $this;
    }

    public function getSupplierHtml(){
        $suppliers = Mage::helper('opentechiz_purchase')->getAllSupplier();
        $html = '<div id="supplier_select" style="display: none"> Select supplier to add to master PO <select id="supplier_select_grid" onchange="ProductAdd(this);return false;">';
        foreach ($suppliers as $key => $value){
            $value = str_replace("'", "\'", $value);
            $html .= '<option value="'.$key.'">'.$value.'</option>';
        }
        $html .= '</select></div>';
        return $html;
    }
}