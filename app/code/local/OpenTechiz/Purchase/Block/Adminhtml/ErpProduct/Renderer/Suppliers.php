<?php

class OpenTechiz_Purchase_Block_Adminhtml_ErpProduct_Renderer_Suppliers extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $suppliers = $row->getData('suppliers');
        $productId = $row->getData('entity_id');
        $suppliers = explode(',', $suppliers);
        $data_cost = array();
        $last_cost = '';

        $html = '';
        $item_id = 0;
        foreach ($suppliers as $supplier){
            if($supplier !==""){
                
                $supplier = Mage::getModel('opentechiz_purchase/supplier')->load($supplier);

                    $product_supplier = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()->addFieldToFilter('product_id', $productId)->addFieldToFilter('sup_id', $supplier->getId())->addFieldToFilter('last_cost_date',array(array('notnull'=>true),array('neq'=>''))); 
                    if($product_supplier){
                        foreach($product_supplier as $item){
                            $last_cost = '<strong> | Last Cost:</strong>'. Mage::getModel('directory/currency')->format(
                                    $item->getLastCost(), 
                                    array('display'=>Zend_Currency::NO_SYMBOL), 
                                    false
                                    );
                            if(!empty($item->getLastCostDate())){
                                $lastPurchaseDate = '<strong> | Last Purchased:</strong> ' . Mage::helper('core')->formatDate($item->getLastCostDate());
                            }else{
                                $lastPurchaseDate = "";
                            }
                            $html .= $supplier->getName();
                            if(!empty($lastPurchaseDate)){
                                $html .= $lastPurchaseDate;
                                $lastPurchaseDate ='';
                            }  
                            if($last_cost !=''){
                                $html .= $last_cost;
                                $last_cost ='';
                            }
                            $html .= '<br>';
                        }
                    }
                

                
            }
            
        }

        
        return $html;
    }
}