<?php

class OpenTechiz_Purchase_Block_Adminhtml_ErpProduct_Renderer_SupplierListSelect extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $supplierCollection = Mage::getModel('opentechiz_purchase/supplier')->getCollection();

//        $html = '<select style="width: 100px;" id="'.$row->getData("entity_id").'" onchange="ProductAdd(this)">';
//        $html .= '<option value="">-- Select --</option>';
//        foreach ($supplierCollection as $supplier){
//            $html .= '<option value="'.$supplier->getId().'">'.$supplier->getName().'</option>';
//        }
//        $html .= '</select>';

        $html = '<a id="'.$row->getData("entity_id").'" href="#" onclick="ProductAdd(this);return false;">Add to master PO</a>';

        return $html;
    }
}