<?php

class OpenTechiz_Purchase_Block_Adminhtml_ErpProduct_Grid_Massaction extends Mage_Adminhtml_Block_Widget_Grid_Massaction
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('opentechiz/grid/massaction.phtml');
        $this->setErrorText(Mage::helper('catalog')->jsQuoteEscape(Mage::helper('catalog')->__('Please select items.')));
    }

    public function ajaxSupplierUrl(){
        return $this->getUrl('adminhtml/erpProduct/getSupplierListAjax/');
    }

}
