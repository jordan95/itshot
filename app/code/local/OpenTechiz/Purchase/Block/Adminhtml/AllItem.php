<?php

class OpenTechiz_Purchase_Block_Adminhtml_AllItem extends Mage_Core_Block_Template
{
    protected  $_controller = 'adminhtml_allItem';
    protected $_blockGroup = 'opentechiz_purchase';
    protected $_headerText = 'All Purchase Order Items';

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('opentechiz/purchase/all_items.phtml');
    }

    public function getFormAction(){
        return Mage::getUrl('adminhtml/purchase_allItem/save/');
    }
}