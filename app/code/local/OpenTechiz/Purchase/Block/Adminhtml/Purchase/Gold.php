<?php

class OpenTechiz_Purchase_Block_Adminhtml_Purchase_Gold extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_purchase_gold';
        $this->_blockGroup = 'opentechiz_purchase';
        $this->_headerText = Mage::helper('opentechiz_purchase')->__('Gold Purchase Orders ');
        parent::__construct();
    }

    public function getHeaderCssClass()
    {
        return 'icon-head head-gold-purchase-order';
    }
}