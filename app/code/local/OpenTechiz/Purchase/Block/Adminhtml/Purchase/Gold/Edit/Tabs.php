<?php

class OpenTechiz_Purchase_Block_Adminhtml_Purchase_Gold_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('gold_edit_tabs');
        $this->setDestElementId('edit_form');
    }

    protected function _beforeToHtml()
    {
        $this->addTab('general', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Summary'),
            'title'     => Mage::helper('opentechiz_purchase')->__('Summary'),
            'content'   => $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_purchase_gold_edit_tab_form')->toHtml()
        ));

        $this->addTab('add_gold_product', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Add Gold '),
            'title'     => Mage::helper('opentechiz_purchase')->__('Add Gold '),
            'content'   => $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_purchase_gold_edit_tab_add')->toHtml()
        ));

    

       /* if(Mage::registry('material_data')->getStatus() == '1')
        {
            $this->removeTab('add_gold_product');
        }*/
        return parent::_beforeToHtml();
    }
}