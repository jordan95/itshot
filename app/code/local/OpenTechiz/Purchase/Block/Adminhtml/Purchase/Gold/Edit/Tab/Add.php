<?php

class OpenTechiz_Purchase_Block_Adminhtml_Purchase_Gold_Edit_Tab_Add extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('goldAddGrid');
        $this->setDefaultSort('gold_id');
        $this->setDefaultDir('DESC');
        $this->setTemplate('opentechiz/purchase/goldgrid.phtml');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }
     public function getSupplierId() {
        if($sup_id = Mage::registry('material_data')->getSupplierId())
            return $sup_id;
        return $this->getRequest()->getParam('supplier');;
    }
    public function getPurchaseStatus(){
        if (Mage::registry('material_data')) {
            return  Mage::registry('material_data')->getStatus();
        }
        return 0;
    }
     public function getGoldOrder(){
        $html = '';
        if($this->getSupplierId()){

            $purchaseOrderID = $this->getRequest()->getParam('id');

            $collection = Mage::getModel('opentechiz_purchase/purchasegold')->getCollection()
            ->join(array('g' => 'opentechiz_material/gold'), 'g.gold_id=main_table.gold_id', array(
                'gold_id' => 'gold_id',
                'gold_price' => 'main_table.price',
                'gold_type' => 'gold_type',
                'weight' => 'weight',
                'gold_color' => 'gold_color'))
            ->addFieldToFilter('po_id', $purchaseOrderID);
            $po = Mage::getModel('opentechiz_purchase/purchase')->load($purchaseOrderID);
           foreach ($collection as $gold) {
                if($po->getStatus()!=OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL&&$po->getStatus()!=OpenTechiz_Purchase_Helper_Data::PO_STATUS_COMPLETE && $gold->getDeliveredQty()<$gold->getQty()){
                    $html .= '<tr id="addgold'.$gold->getGoldId().'" class="item_purchase"> <td class="a-center "><input class="required-entry " name="add['.$gold->getGoldId().'][price]" value="'.$gold->getData('gold_price').'"> <input type="hidden" name="add['.$gold->getGoldId().'][gold_id]" value="'.$gold->getGoldId().'"><td class="a-center "><input class="required-entry purchase_qty " name="add['.$gold->getGoldId().'][qty]" value="'.$gold->getData('qty').'"></td><td class="a-center "><input class="required-entry delivered_qty " name="add['.$gold->getGoldId().'][delivered qty]" value="'.$gold->getData('delivered_qty').'" disabled="disabled" ></td><td class="a-center ">'.$gold->getGoldId().'</td><td class=" ">'.$gold->getGoldType().'</td><td class=" ">'.$gold->getGoldColor().'</td><td class=" a-right ">'.$gold->getWeight().'</td><td class="first"><a href="#" onclick="hideGold('.$gold->getGoldId().')">Remove</a></td></tr><input type="checkbox" name="add['.$gold->getGoldId().'][delete]" style="display:none">';
                 }else{
                    $html .= '<tr id="addgold'.$gold->getGoldId().'" class="item_purchase" > <td class="a-center "><input name="add['.$gold->getGoldId().'][price]" value="'.$gold->getData('gold_price').'" readonly> <input type="hidden" name="add['.$gold->getGoldId().'][gold_id]" value="'.$gold->getGoldId().'"><td class="a-center "><input class="required-entry purchase_qty " name="add['.$gold->getGoldId().'][qty]" value="'.$gold->getData('qty').'" readonly></td><td class="a-center delivered_qty">'.$gold->getData('delivered_qty').' <td class="a-center ">'.$gold->getGoldId().'</td><td class=" ">'.$gold->getGoldType().'</td><td class=" ">'.$gold->getGoldColor().'</td><td class=" a-right ">'.$gold->getWeight().'</td><td class="first"></tr>';
                 }
               
           }
           
        }
        return $html;
        
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $po = Mage::registry('material_data');
        $po_gold  = Mage::getModel('opentechiz_purchase/purchasegold')->getCollection()->AddFieldtoFilter('po_id',$po->getId())->getData('gold_id');
        if(count($po_gold) >= 1)
          $added_gold = array_column($po_gold, 'gold_id');
        else 
          $added_gold = [0];
       
        $collection = Mage::getModel('opentechiz_purchase/goldsupplier')->getCollection()
            ->join('supplier', 'supplier.supplier_id = main_table.supplier_id AND main_table.supplier_id = '.$this->getSupplierId())
            ->join(array('g'=>'opentechiz_material/gold'), 'g.gold_id=main_table.gold_id', array(
                'gold_id'       =>  'gold_id',
                'price'         =>  'price',
                'gold_type'     =>  'gold_type',
                'weight'        =>  'weight',
                'gold_color'    =>  'gold_color'))->AddFieldtoFilter('main_table.gold_id',array('nin' => $added_gold));


        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('purchase_gold_supplier_id', array(
            'header_css_class' => 'a-center',
            'header' => Mage::helper('opentechiz_purchase')->__('Select'),
            'type'   => 'checkbox',
            'index'  => 'purchase_gold_supplier_id',
            'renderer'   => 'OpenTechiz_Purchase_Block_Widget_Column_Renderer_OrderGold_Select',
            'align'      => 'center',
            'sortable'   => false,
            'width'     => '80'
        ));

        
        $this->addColumn('price', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Price'),
            'index'     => 'price',
            'type'      => 'number',
            'align'     => 'center',
            'width'     => '200',
            'renderer'  => 'OpenTechiz_Purchase_Block_Widget_Column_Renderer_OrderGold_Price',
            'sortable'  => false,
            'filter'    => false,
        ));

        $this->addColumn('gold_id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('ID'),
            'align'     =>'center',
            'index'     => 'gold_id',
            'width'     => '80',
            'filter_index' => 'main_table.gold_id',
        ));

        $this->addColumn('gold_type', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Gold Type'),
            'index'     => 'gold_type',
            'type'      => 'options',
            'options'   =>  OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_TYPE,
            'width'     => '200'
        ));

        $this->addColumn('gold_color', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Gold Color'),
            'index'     => 'gold_color',
            'type'      => 'options',
            'options'   =>  OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_COLOR,
            'width'     => '200'
        ));

        $this->addColumn('weight', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Gold Weight (gr)'),
            'index'     => 'weight',
            'align'     => 'center',
            'width'     => '200'
        ));

        $this->addColumn('contact', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Contact'),
            'index'     => 'contact',
            'align'     => 'center'
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Suppliers'),
            'index'     => 'name',
        ));
        return parent::_prepareColumns();
    }
   
    public function getGridUrl()
    {
        return $this->getUrl('*/*/gridgoldadd', array('_current'=>true,'id'=>$this->getRequest()->getParam('id')));
    }
}