<?php

class OpenTechiz_Purchase_Block_Adminhtml_Purchase_Gold_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('purchaseGoldGrid');
        $this->setDefaultSort('po_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_purchase/purchase')->getCollection()->addFieldToFilter('type', 0)
            ->join(array('s' => 'supplier'), 'main_table.supplier_id = s.supplier_id', array('name'=>'name'));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('po_id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('PO #'),
            'index'     => 'po_id',
            'align'     => 'center',
            'width'     => '50'
        ));
        $this->addColumn('po_increment_id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('PO Increment #'),
            'index'     => 'po_increment_id',
        ));
        $this->addColumn('created_at', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Created At'),
            'index'     => 'created_at',
            'type'      => 'datetime'
        ));
        $this->addColumn('updated_at', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Updated At'),
            'index'     => 'updated_at',
            'type'      => 'datetime'
        ));
        $this->addColumn('delivery_date', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Estimated time of arrival'),
            'index'     => 'delivery_date',
            'type'      => 'datetime',
            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_Renderer_DatabaseDate'
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Supplier'),
            'index'     => 'name',
            'align'     => 'center',
        ));
        $this->addColumn('status', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Status'),
            'index'     => 'status',
            'type'      => 'options',
            'options'   => Mage::helper('opentechiz_purchase')->getPurchaseOrderStatus(),
            'align'     => 'center'
        ));
        $this->addColumn('total_include_tax', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Total (Incl. tax)'),
            'index'     => 'total_include_tax',
            'align'     => 'center',
            'renderer'  => 'OpenTechiz_Purchase_Block_Widget_Column_Renderer_Format_AllPrice',
        ));
        $this->addColumn('is_paid', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Is Paid'),
            'index'     => 'is_paid',
            'type'      => 'options',
            'align'     => 'center',
            'options'   => array( 0 => 'No', 1 => 'Yes')
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('admin/erp/purchase/material/gold/gold_edit')) {
            $this->addColumn('action',
                array(
                    'header' => Mage::helper('opentechiz_production')->__('Action'),
                    'width' => '50',
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('opentechiz_production')->__('Edit'),
                            'url' => array('base' => '*/*/edit'),
                            'field' => 'id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'is_system' => true,
                    'align' => 'center'
                ));
        }
        return parent::_prepareColumns();
    }

    
    public function getRowUrl($row)
    {
        if (Mage::getSingleton('admin/session')->isAllowed('admin/erp/purchase/material/gold/gold_edit')) {
            return $this->getUrl('*/*/edit', array('id' => $row->getId()));
        }
    }
}