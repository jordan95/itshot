<?php

class OpenTechiz_Purchase_Block_Adminhtml_Purchase_Stone_New extends OpenTechiz_Purchase_Block_Adminhtml_Purchase_Gold_New
{
    public function getBackUrl()
    {
        return $this->getUrl('adminhtml/purchase_stone/index', array());
    }
}