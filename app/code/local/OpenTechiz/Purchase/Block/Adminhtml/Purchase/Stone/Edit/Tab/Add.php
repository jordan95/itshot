<?php

class OpenTechiz_Purchase_Block_Adminhtml_Purchase_Stone_Edit_Tab_Add extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('stoneAddGrid');
        $this->setDefaultSort('stone_id');
        $this->setDefaultDir('DESC');
        $this->setTemplate('opentechiz/purchase/stonegrid.phtml');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    public function getSupplierId() {
        if($sup_id = Mage::registry('material_data')->getSupplierId())
            return $sup_id;
        return $this->getRequest()->getParam('supplier');
    }
    public function getPurchaseStatus(){
        if (Mage::registry('material_data')) {
            return  Mage::registry('material_data')->getStatus();
        }
        return 0;
    }
    public function getStoneOrder(){
        $html = '';
        if($this->getRequest()->getParam('id')){

        $purchaseOrderID = $this->getRequest()->getParam('id');

        $collection = Mage::getModel('opentechiz_purchase/purchasestone')->getCollection()
            ->join(array('s'=>'opentechiz_material/stone'), 's.stone_id = main_table.stone_id', array(
                'stone_id'      =>  'stone_id',
                'price'         =>  'main_table.price',
                'stone_type'    =>  'stone_type',
                'weight'        =>  'weight',
                'shape'         =>  'shape',
                'diameter'      =>  'diameter',
                'quality'       =>  'quality'))
            ->addFieldToFilter('po_id', $purchaseOrderID);
            $po = Mage::getModel('opentechiz_purchase/purchase')->load($purchaseOrderID);
           foreach ($collection as $stone) {
                if($po->getStatus()!=OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL&&$po->getStatus()!=OpenTechiz_Purchase_Helper_Data::PO_STATUS_COMPLETE && $stone->getDeliveredQty()<$stone->getQty()){
                    $html .= '<tr id="addstone'.$stone->getStoneId().'" class="item_purchase"> <td class="a-center "><input name="add['.$stone->getStoneId().'][price]" value="'.$stone->getPrice().'"></td><td class="a-center "><input class="required-entry purchase_qty" type="number" min="0" name="add['.$stone->getStoneId().'][qty]" value="'.$stone->getQty().'"></td><td class="a-center "><input class="required-entry delivered_qty" type="number" min="0" name="add['.$stone->getStoneId().'][delivered_qty]" value="'.$stone->getDeliveredQty().'"> <input type="hidden" name="add['.$stone->getStoneId().'][stone_id]" value="'.$stone->getStoneId().'"></td><td class="a-center ">'.$stone->getStoneId().'</td><td class="a-center">'.$stone->getStoneType().'</td><td class="a-center a-right ">'.$stone->getQuality().'</td><td class="a-center ">'.$stone->getWeight().'</td><td class="a-center">'.$stone->getShape().'</td><td class="a-center last">'.$stone->getDiameter().'</td><td class="first"><a href="#" onclick="hidestone('.$stone->getStoneId().')">Remove</a></td></tr> <input type="checkbox" name="add['.$stone->getStoneId().'][delete]" style="display:none">';
                }
                else{
                   $html .= '<tr id="addstone'.$stone->getStoneId().'" class="item_purchase"> <td class="a-center "><input name="add['.$stone->getStoneId().'][price]" value="'.$stone->getPrice().'" readonly></td><td class="a-center "><input class="required-entry purchase_qty" type="number" min="0" name="add['.$stone->getStoneId().'][qty]" value="'.$stone->getQty().'" readonly ></td><td class="a-center "><input class="required-entry delivered_qty" type="number" min="0" name="add['.$stone->getStoneId().'][delivered_qty]" value="'.$stone->getDeliveredQty().'" readonly> <input type="hidden" name="add['.$stone->getStoneId().'][stone_id]" value="'.$stone->getStoneId().'"></td><td class="a-center ">'.$stone->getStoneId().'</td><td class="a-center">'.$stone->getStoneType().'</td><td class="a-center a-right ">'.$stone->getQuality().'</td><td class="a-center ">'.$stone->getWeight().'</td><td class="a-center">'.$stone->getShape().'</td><td class="a-center last">'.$stone->getDiameter().'</td><td class="first"> </td></tr> ';
               }
            }
           
        }
        return $html;
        
    }
    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
       $po = Mage::registry('material_data');
        $po_stone  = Mage::getModel('opentechiz_purchase/purchasestone')->getCollection()->AddFieldtoFilter('po_id',$po->getId())->getData('stone_id');
          if(count($po_stone) >= 1)
          $added_stone = array_column($po_stone, 'stone_id');
        else 
          $added_stone = [0]; 
        $collection = Mage::getModel('opentechiz_purchase/stonesupplier')->getCollection()
            ->join(array('s'=>'opentechiz_material/stone'), 's.stone_id=main_table.stone_id AND main_table.supplier_id = '.$this->getSupplierId(), array(
                'stone_id'    => 'stone_id',
                'price' => 'main_table.price',
                'stone_type'  => 'stone_type',
                'weight'      => 'weight',
                'shape'       => 'shape',
                'diameter'    => 'diameter',
                'quality'     => 'quality'))->AddFieldtoFilter('main_table.stone_id',array('nin' => $added_stone));


        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('purchase_stone_supplier_id', array(
            'header_css_class' => 'a-center',
            'header' => Mage::helper('opentechiz_purchase')->__('Select'),
            'type'   => 'checkbox',
            'index'  => 'purchase_stone_supplier_id',
            'renderer'   => 'OpenTechiz_Purchase_Block_Widget_Column_Renderer_OrderStone_Select',
            'align'      => 'center',
            'sortable'   => false,
            'width'     => '80'
        ));

     
        $this->addColumn('price', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Price '),
            'index'     => 'price',
            'align'     => 'center',
            'width'     => '200',
            'renderer'  => 'OpenTechiz_Purchase_Block_Widget_Column_Renderer_OrderStone_Price',
            'sortable'  => false,
            'filter'    => false
        ));

        $this->addColumn('stone_id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('ID'),
            'align'     =>'center',
            'index'     => 'stone_id',
            'width'     => '80',
            'filter_index' => 'main_table.stone_id',
        ));

        $this->addColumn('stone_type', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Stone Type'),
            'index'     => 'stone_type',
            'type'      => 'options',
            'align'     => 'center',
            'options'   => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_TYPE,

        ));

        $this->addColumn('quality', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Quality'),
            'index'     => 'quality',
            'align'     => 'center',
            'type'      => 'options',
            'width'     => '100',
            'options'   => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_QUALITY,
        ));

        $this->addColumn('weight', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Size'),
            'index'     => 'weight',
            'align'     => 'center',
        ));

        $this->addColumn('shape', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Shape'),
            'index'     => 'shape',
            'align'     => 'left',
            'type'      => 'options',
            'options'   => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_SHAPE
        ));

        $this->addColumn('diameter', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Diameter'),
            'index'     => 'diameter',
            'align'     => 'center',
        ));

       
       
        return parent::_prepareColumns();
    }
    public function getGridUrl()
    {
        return $this->getUrl('*/*/gridstoneadd', array('_current'=>true,'id'=>$this->getRequest()->getParam('id')));
    }
}