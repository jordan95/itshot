<?php

class OpenTechiz_Purchase_Block_Adminhtml_Purchase_Gold_New extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getBackUrl()
    {
        return $this->getUrl('adminhtml/purchase_gold/index', array());
    }

    public function getNewSupplierUrl()
    {
        return $this->getUrl('adminhtml/supplier/new');
    }


    public function getSuppliersAsCombo($name='supplier')
    {
        $html = '<select  id="'.$name.'" name="'.$name.'" style="width:200px;height:25px;">';

        $collection = Mage::getModel('opentechiz_purchase/supplier')
            ->getCollection()
            ->setOrder('name', 'asc');
        foreach ($collection as $item)
        {
            $html .= '<option value="'.$item->getsupplier_id().'">'.$item->getname().'</option>';
        }

        $html .= '</select>';
        return $html;
    }
}