<?php

class OpenTechiz_Purchase_Block_Adminhtml_Purchase_Stone_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('stone_edit_tabs');
        $this->setDestElementId('edit_form');
    }

    protected function _beforeToHtml()
    {
        $this->addTab('general', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Summary'),
            'title'     => Mage::helper('opentechiz_purchase')->__('Summary'),
            'content'   => $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_purchase_stone_edit_tab_form')->toHtml()
        ));

        $this->addTab('add_stone_product', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Add Stone '),
            'title'     => Mage::helper('opentechiz_purchase')->__('Add Stone '),
            'content'   => $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_purchase_stone_edit_tab_add')->toHtml()
        ));

       /* $this->addTab('added_stone_product', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Added Stone Products'),
            'title'     => Mage::helper('opentechiz_purchase')->__('Added Stone Products'),
            'content'   => $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_purchase_stone_edit_tab_added')->toHtml(),
            'active'    => Mage::registry('material_data')->getId() ? false : true
        ));
*/
       /* if(Mage::registry('material_data')->getStatus() == '1')
        {
            $this->removeTab('add_stone_product');
        }*/
        return parent::_beforeToHtml();
    }
}