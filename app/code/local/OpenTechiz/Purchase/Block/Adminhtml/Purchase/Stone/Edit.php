<?php

class OpenTechiz_Purchase_Block_Adminhtml_Purchase_Stone_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    private $_order;
    private $_supplierName;

    public function __construct()
    {
        $this->_objectId = 'stone_id';
        $this->_blockGroup = 'opentechiz_purchase';
        $this->_controller = 'adminhtml_purchase_stone';
        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('opentechiz_purchase')->__('Save'));
        $this->_updateButton('save', 'onclick', 'return confirmFunction(\'save\');');
        $this->_removeButton('delete');

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' =>  'return confirmFunction(\'savencontinue\');',
            'class' => 'save',
        ), -100);
       
        $this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('purchase_stone_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'purchase_stone_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'purchase_stone_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
    }

    public function getHeaderText()
    {
        if( Mage::registry('material_data') && Mage::registry('material_data')->getId() ) {
            return $this->__('Purchase Order #') . $this->getOrder()->getpo_increment_id() . ' (' . $this->getSuppliers() . ')';
        } else {
            return Mage::helper('opentechiz_purchase')->__('Create new purchase order');
        }
    }

    public function getOrder() {
        if ($this->_order == null) {
            $this->_order = Mage::getModel('opentechiz_purchase/purchase')->load($this->getRequest()->getParam('id'));
        }
        return $this->_order;
    }

    public function getSuppliers()
    {
        $POId = $this->getRequest()->getParam('id');
        $supplierParam = $this->getRequest()->getParam('supplier');
        if(isset($supplierParam))
        {
            $this->_supplierName = Mage::getModel('opentechiz_purchase/supplier')->load($supplierParam)->getName();
        }
        else
        {
            $loadSupplier = Mage::getModel('opentechiz_purchase/purchase')->load($POId)->getSupplierId();
            $this->_supplierName = Mage::getModel('opentechiz_purchase/supplier')->load($loadSupplier)->getName();
        }

        return $this->_supplierName;
    }
}