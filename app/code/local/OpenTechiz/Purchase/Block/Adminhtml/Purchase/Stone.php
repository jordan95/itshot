<?php

class OpenTechiz_Purchase_Block_Adminhtml_Purchase_Stone extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_purchase_stone';
        $this->_blockGroup = 'opentechiz_purchase';
        $this->_headerText = Mage::helper('opentechiz_purchase')->__('Stone Purchase Orders ');
        parent::__construct();
    }

    public function getHeaderCssClass()
    {
        return 'icon-head head-stone-purchase-order';
    }
}