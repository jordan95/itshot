<?php

class OpenTechiz_Purchase_Block_Adminhtml_Purchase_Stone_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        //Purchase-Order Model
        $purchaseModel = Mage::getModel('opentechiz_purchase/purchase');

        //Purchase-Supplier Model
        $supplierModel = Mage::getModel('opentechiz_purchase/supplier');

        // PO Id parameter
        $getpo_id = $this->getRequest()->getParam('id');

        //Load supplier by supplier Id parameter
        $getSupplierID = $this->getRequest()->getParam('supplier');
        $disabled = (Mage::registry('material_data')->getStatus()==OpenTechiz_Purchase_Helper_Data::PO_STATUS_COMPLETE||Mage::registry('material_data')->getStatus()==OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL)?true:false;
        //Load supplier name
        $loadSupplier = $purchaseModel->load($getpo_id)->getSupplierId();
        $supplier = $supplierModel->load($loadSupplier);
        $loadSupplierName = $supplier->getName();

        //Load PO increment ID
        $loadpo_increment_id = $purchaseModel->load($getpo_id)->getpo_increment_id();

        $fieldset = $form->addFieldset('purchase_stone_form', array('legend'=>Mage::helper('opentechiz_purchase')->__('General')));

        $fieldset->addField('po_increment_id', 'note', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('PO Increment ID'),
            'text'      => Mage::registry('material_data')->getPoIncrementId(),
        ));

         $fieldset->addType('datetimezone', 'OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tab_Form_Element_Datetime');

        $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $fieldset->addField('created_at', 'datetime', array(
            'name'   => 'created_at',
            'label'  => Mage::helper('opentechiz_purchase')->__('Order Date'),
            'title'  => Mage::helper('opentechiz_purchase')->__('Order Date'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'required' => true,
            'disabled' =>$disabled,
            'input_format' => "MM/dd/yyyy",
            'format'       => "MM/dd/yyyy",
            'time' => true,
            "value" => "textstart", 

        ));
        $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $fieldset->addField('delivery_date', 'datetime', array(
            'name'   => 'delivery_date',
            'label'  => Mage::helper('opentechiz_purchase')->__('Estimate time to delivery'),
            'title'  => Mage::helper('opentechiz_purchase')->__('Estimate time to delivery'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => "MM/dd/yyyy",
            'format'       => "MM/dd/yyyy",
            'disabled' =>$disabled,
            'required' => true,
            'time' => true
        ));

        $supplierData = $loadSupplierName;

        if($supplier->getContact()) {
            $supplierData .= '<br>'.$supplier->getContact();
        }

        if($supplier->getTelephone() && $supplier->getEmail()) {
            $supplierData .= '<br>'.$supplier->getTelephone() . ' - ' . $supplier->getEmail();
        }elseif($supplier->getTelephone()){
            $supplierData .= '<br>'.$supplier->getTelephone();
        }elseif($supplier->getEmail()){
            $supplierData .= '<br>'.$supplier->getEmail();
        }

        if($supplier->getWebsite()) {
            $supplierData .= '<br>'.'<a href="'.$supplier->getWebsite().'">'. $supplier->getWebsite() .'</a>';
        }

        $fieldset->addField('supplier_name', 'note', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Supplier'),
            'text'      => $supplierData
        ));


        $fieldset->addField('supplier_id', 'hidden', array(
            'name'      => 'supplier_id',
        ));

        $fieldset->addField('sup_sales_rep', 'textarea', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Sales Representative'),
            'name'      => 'sup_sales_rep',
            'disabled'  => $disabled
        ));

        $fieldset->addField('note', 'textarea', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Note'),
            'name'      => 'note',
        ));
        $fieldset->addField('tax_rate', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Tax Rate'),
            'name'      => 'tax_rate',
            'disabled' =>$disabled,
        ));
        $fieldset->addField('total_include_tax', 'note', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Total (incl tax)'),
            'text'     => Mage::getModel('directory/currency')->format($purchaseModel->getTotalIncludeTax(), array('display'=>Zend_Currency::NO_SYMBOL), false),
        ));
        $fieldset->addField('shipping_cost', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Shipping Cost'),
            'name'      => 'shipping_cost',
            'disabled' =>$disabled,
        ));
        $fieldset->addField('is_paid', 'select', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Is Paid'),
            'name'      => 'is_paid',
            'type'      => 'options',
            'disabled' =>$disabled,
            'options'   => Mage::helper('opentechiz_purchase')->getPoPaidStatus()
        ));
        $fieldset->addField('status','select', array(
            'label'    => Mage::helper('opentechiz_purchase')->__('Status'),
            'name'     => 'status',
            'type'      => 'options',
            'disabled' =>$disabled,
            'options'   => Mage::helper('opentechiz_purchase')->getPurchaseOrderStatus(),
        ));

        $fieldset->addField('type', 'hidden', array(
            'name'      => 'type',
        ));

        if ( Mage::registry('material_data') ) {
            $form->setValues(Mage::registry('material_data')->getData());
            $form->addValues(array('supplier_name'=> $loadSupplierName));
            $form->addValues(array('po_increment_id'=> $loadpo_increment_id));
            $form->addValues(array('type'=> OpenTechiz_Purchase_Helper_Data::PO_TYPE_STONE));

            if(isset($getSupplierID))
            {
                $form->addValues(array('supplier_id'=> $getSupplierID));
            }
            else
            {
                $form->addValues(array('supplier_id'=> $loadSupplier ));
            }
            $data = Mage::registry('material_data')->getData();
            if(isset($data['total_include_tax']) && $data['total_include_tax'] == 0){
                $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
                $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $date);
                $newDateString = $myDateTime->format('m/d/Y H:i:s');
                $form->addValues(array('created_at' => $newDateString));
                $form->addValues(array('delivery_date' => ''));
            }else{
                $PO = $purchaseModel->load($getpo_id);
                $date = $PO->getCreatedAt();
                $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $date);
                $newDateString = $myDateTime->format('m/d/Y H:i:s');
                $form->addValues(array('created_at' => $newDateString));

                $date = $PO->getDeliveryDate();
                $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $date);
                $newDateString = $myDateTime->format('m/d/Y H:i:s');
                $form->addValues(array('delivery_date' => $newDateString));
            }
        }

        return parent::_prepareForm();
    }
}