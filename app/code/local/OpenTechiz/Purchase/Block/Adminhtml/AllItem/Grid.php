<?php

class OpenTechiz_Purchase_Block_Adminhtml_AllItem_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('allItemGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_purchase/orderitem')->getCollection();
        $collection->join(array('purchase' => 'opentechiz_purchase/purchase'), 'purchase.po_id = main_table.po_id', array(
            'note' => 'main_table.note',
            'po_increment_id' => 'purchase.po_increment_id',
            'status' => 'purchase.status',
            'confirmed',
            'user_id',
        ));
        $collection->join(array('supplier' => 'opentechiz_purchase/supplier'), 'purchase.supplier_id = supplier.supplier_id', '*');

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getUrl('adminhtml/purchase_allItem/allItemGrid', array('_current'=> true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('ID'),
            'index'     => 'id',
            'width'     => '50',
            'type'      => 'number',
            'column_css_class'=>'no-display',
            'header_css_class'=>'no-display',
        ));

        $this->addColumn('supplier_name', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Supplier'),
            'index'     => 'supplier_id',
            'filter_index' => 'supplier.supplier_id',
            'align'     => 'center',
            'width'     => '50',
            'type'      =>'options',
            'options'   => Mage::helper('opentechiz_purchase')->getAllSupplier(),
        ));

        $this->addColumn('po_increment_id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('PO #'),
            'index'     => 'po_increment_id',
        ));

        $this->addColumn('sku', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('SKU'),
            'index'     => 'sku'
        ));

        $this->addColumn('image', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Image'),
            'index'     => 'product_id',
            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_Renderer_Image',
            'filter' => false,
            'sortable' => false,
            'align'     => 'center',
            'width'     => '100',
        ));

        $this->addColumn('qty', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Qty'),
            'index'     => 'qty',
            'type'      => 'number',
        ));

        $this->addColumn('delivered_qty', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Delivered Qty'),
            'index'     => 'delivered_qty',
            'type'      => 'number',
            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_Renderer_InputData'
        ));

        $this->addColumn('shipped_qty', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Shipped QTY'),
            'index'     => 'shipped_qty',
            'type'      => 'number',
            'filter_index' => 'main_table.shipped_qty',
        ));

        $this->addColumn('shipment_date', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Ship Date'),
            'index'     => 'shipment_date',
            'type'      => 'date',
            'filter_index' => 'main_table.shipment_date',
        ));
        
        $this->addColumn('shipment_reference', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Shipment Reference'),
            'index'     => 'shipment_reference',
            'filter_index' => 'main_table.shipment_reference',
        ));

        $this->addColumn('confirmed', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Confirmed'),
            'index'     => 'confirmed',
            'type'      =>'options',
            'options'   => array('' => 'No', 1 => 'Yes'),
            'filter_index' => 'purchase.confirmed',
        ));

        $this->addColumn('status', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Completed PO'),
            'index'     => 'status',
            'type'      =>'options',
            'options'   => array(0 => 'No', 1 => 'Yes'),
            'renderer'  =>'OpenTechiz_Purchase_Block_Adminhtml_Renderer_Status',
            'filter_condition_callback' => array($this, '_statusFilter')
        ));

        $this->addColumn('note', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Note'),
            'index'     => 'note',
            'filter_index' => 'main_table.note',
        ));

        $this->addColumn('user_id', array(
            'header' => Mage::helper('opentechiz_purchase')->__('Create By'),
            'index' => 'user_id',
            'filter_index'=>'purchase.user_id',
            'type' => 'options',
            'multiple'=> true,
            'options' =>$this->helper('opentechiz_purchase')->getListUserActive(),
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('item_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

        $this->getMassactionBlock()->addItem('update_shipment_ref', array(
             'label'=> Mage::helper('sales')->__('Update Shipment Ref'),
             'url'  => $this->getUrl('*/*/massUpdateShipmentRef'),
             'additional'    => array(
                'shipment_reference' => array(
                    'name'  => 'shipment_reference',
                    'type'  => 'text',  
                    'label' => Mage::helper('opentechiz_purchase')->__('Value')
                ),
            )
        ));

        return $this;
    }

    protected function _statusFilter($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        if($value == 0){
            $collection->getSelect()->where('purchase.status in (0,2)');
        }else{
            $collection->getSelect()->where('purchase.status in (1)');
        }

        return $this;
    }

    public function getRowClass(Varien_Object $row) {
        $class = '';

        if ((int)$row->getDeliveredQty() != (int)$row->getShippedQty()) {
            $class = 'hilight-diffrent';
        }
    
        return $class;
    }

    protected function _toHtml()
    {
        $html = '<style>body .grid tr.hilight-diffrent{background-color:#FFFCBC;}</style>';
        $html .= parent::_toHtml();
        return $html;
    }
}