<?php

class OpenTechiz_Purchase_Block_Adminhtml_Product_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('purchaseProductGrid');
        $this->setDefaultSort('order_po_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /** @var OpenTechiz_Purchase_Model_Resource_Purchase_Collection $collection */
        $collection = Mage::getModel('opentechiz_purchase/purchase')->getCollection()
            ->addFieldToSelect([
                'order_po_id' => 'po_id',
                'po_increment_id',
                'created_at',
                'updated_at',
                'delivery_date',
                'status',
                'total_include_tax',
                'is_paid',
                'notified_at',
                'source',
                'user_id'
            ])
            ->addFieldToFilter('type', 2)

        ->join('supplier', 'supplier.supplier_id=main_table.supplier_id',
            array(
                'old_data_id' => 'main_table.old_data_id',
                'name'        => 'supplier.name'
            ));

        $collection->getSelect()
            ->joinLeft(array('orderitem' => $collection->getTable('opentechiz_purchase/orderitem')),
                'orderitem.po_id=main_table.po_id',
                array(
                )
            )->group('main_table.po_id');

        $collection->addExpressionFieldToSelect('items_count', 'SUM(orderitem.qty)', []);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('order_po_id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('PO #'),
            'index'     => 'order_po_id',
            'filter'    => false,
            'align'     => 'center',
            'width'     => '50'
        ));
        $this->addColumn('po_increment_id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('PO Increment #'),
            'index'     => 'po_increment_id'
        ));
        $this->addColumn('created_at', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Created At'),
            'index'     => 'created_at',
            'type'      => 'datetime'
        ));
        $this->addColumn('updated_at', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Updated At'),
            'index'     => 'updated_at',
            'type'      => 'datetime',
        ));
        $this->addColumn('delivery_date', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Estimated time of arrival'),
            'index'     => 'delivery_date',
            'type'      => 'datetime',
            'width'     => '150px',
            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_Renderer_DatabaseDate'
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Supplier'),
            'index'     => 'name',
            'align'     => 'center',
        ));
        $this->addColumn('status', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Status'),
            'index'     => 'status',
            'type'      => 'options',
            'options'   => Mage::helper('opentechiz_purchase')->getPurchaseOrderStatus(),
            'align'     => 'center'
        ));
        $this->addColumn('total_include_tax', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Total (Incl. tax)'),
            'index'     => 'total_include_tax',
            'align'     => 'center',
            'renderer'  => 'OpenTechiz_Purchase_Block_Widget_Column_Renderer_Format_AllPrice',
        ));
        $this->addColumn('items_count', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Total QTY'),
            'index'     => 'items_count',
            'filter'    => false,
            'align'     => 'center',
        ));
        $this->addColumn('is_paid', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Is Paid'),
            'index'     => 'is_paid',
            'type'      => 'options',
            'align'     => 'center',
            'options'   => array( 0 => 'No', 1 => 'Yes')
        ));
        $this->addColumn('notified_at', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Emailed to Supplier'),
            'index'     => 'notified_at',
            'type'      => 'datetime',
            'align'     => 'center',
        ));
        $this->addColumn('source', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Source'),
            'index'     => 'source',
            'type'      => 'options',
            'align'     => 'center',
            'options'   => Mage::getSingleton('opentechiz_purchase/source_product_source')->toOptionArray()
        ));

        $this->addColumn('user_id', array(
            'header' => Mage::helper('opentechiz_purchase')->__('Create By'),
            'index' => 'user_id',
            'filter_index'=>'main_table.user_id',
            'type' => 'options',
            'multiple'=> true,
            'options' =>$this->helper('opentechiz_purchase')->getListUserActive(),
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('admin/erp/purchase/product/product_edit')) {
            $this->addColumn('action',
                array(
                    'header' => Mage::helper('opentechiz_production')->__('Action'),
                    'width' => '50',
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('opentechiz_production')->__('Edit'),
                            'url' => array('base' => '*/*/edit'),
                            'field' => 'id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'is_system' => true,
                    'align' => 'center'
                ));
        }
        return parent::_prepareColumns();
    }

   

    public function getRowUrl($row)
    {
        if (Mage::getSingleton('admin/session')->isAllowed('admin/erp/purchase/product/product_edit')) {
            return $this->getUrl('*/*/edit', array('id' => $row->getId()));
        }
    }
}