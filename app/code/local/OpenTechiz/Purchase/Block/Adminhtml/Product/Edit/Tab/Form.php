<?php

class OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('opentechiz/purchase/form.phtml');
        $this->setDestElementId('edit_form');
        $this->setShowGlobalIcon(false);
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $PO = Mage::registry('purchaseOrder');
//        $disabled = ($PO->getStatus()==OpenTechiz_Purchase_Helper_Data::PO_STATUS_COMPLETE||$PO->getStatus()==OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL)?true:false;
        $disabled = false;
        $dateDisabled = Mage::helper('opentechiz_purchase')->isAllItemDelivered($PO->getId());

        if($po_id = $PO->getId()){
           $supplierId = $PO->getSupplierId(); 
            
        }else{
        //Load supplier by parameter
            $supplierId = $this->getRequest()->getParam('supplier');
        }
        
        $supplier = Mage::getModel('opentechiz_purchase/supplier')->load($supplierId);
        $supplierName = $supplier->getName();

         $fieldset = $form->addFieldset('purchase_gold_form', array('legend'=>Mage::helper('opentechiz_purchase')->__('General')));

        if($po_id){
            $fieldset->addField('po_increment_id', 'note', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('PO Increment ID'),
            'text'      => $PO->getPo_increment_id(),
             ));

            $fieldset->addField('created_at', 'note', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Order Date'),
                'text'      => Mage::getSingleton('core/date')->date('m/d/Y', $PO->getCreatedAt())
            ));
        }
        //Load purchase by parameter
        
        $fieldset->addType('datetimezone', 'OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tab_Form_Element_Datetime');

        $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $fieldset->addField('delivery_date', 'datetime', array(
            'name'   => 'delivery_date',
            'label'  => Mage::helper('opentechiz_purchase')->__('Estimate time to delivery'),
            'title'  => Mage::helper('opentechiz_purchase')->__('Estimate time to delivery'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => "MM/dd/yyyy",
            'format'       => "MM/dd/yyyy",
            'required' => true,
            'time' => true,
            "value" => "textstart",
            'disabled' =>$dateDisabled
        ));
        
        // $fieldset->addField('shipment_date', 'datetime', array(
        //     'name'   => 'shipment_date',
        //     'label'  => Mage::helper('opentechiz_purchase')->__('Shipment Date'),
        //     'title'  => Mage::helper('opentechiz_purchase')->__('Shipment Date'),
        //     'image'  => $this->getSkinUrl('images/grid-cal.gif'),
        //     'input_format' => "MM/dd/yyyy",
        //     'format'       => "MM/dd/yyyy",
        //     'required' => false,
        //     'time' => true,
        //     "value" => "textstart",
        // ));

        $supplierData = $supplierName;

        if($supplier->getContact()) {
            $supplierData .= '<br>'.$supplier->getContact();
        }

        if($supplier->getTelephone() && $supplier->getEmail()) {
            $supplierData .= '<br>'.$supplier->getTelephone() . ' - ' . $supplier->getEmail();
        }elseif($supplier->getTelephone()){
            $supplierData .= '<br>'.$supplier->getTelephone();
        }elseif($supplier->getEmail()){
            $supplierData .= '<br>'.$supplier->getEmail();
        }

        if($supplier->getWebsite()) {
            $supplierData .= '<br>'.'<a href="'.$supplier->getWebsite().'">'. $supplier->getWebsite() .'</a>';
        }

        $fieldset->addField('supplier_name', 'note', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Supplier'),
            'text'      => $supplierData
        ));

        $fieldset->addField('supplier_id', 'hidden', array(
            'name'      => 'supplier_id',
        ));

        $fieldset->addField('sup_sales_rep', 'textarea', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Sales Representative'),
            'name'      => 'sup_sales_rep',
            'disabled'  => $disabled
        ));

        $fieldset->addField('note', 'textarea', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Note'),
            'name'      => 'note',
        ));
        $fieldset->addField('tax_rate', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Tax Rate(%)'),
            'name'      => 'tax_rate',
            'onchange'  => 'updateTotalPrice()',
            'disabled' =>$disabled
        ));
          $symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
        $fieldset->addField('total_include_tax', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Total (incl tax)('.$symbol.')'),
            'name'      => 'total_include_tax',
            'readonly' => true,
            'disabled' =>$disabled
        ));
      
        $fieldset->addField('shipping_cost', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Shipping Cost ('.$symbol.')'),
            'onchange'  => 'updateTotalPrice()',
            'name'      => 'shipping_cost',
            'disabled' =>$disabled
        ));
        if(!$po_id){
            $fieldset->addField('is_paid', 'select', array(
                'label'     => Mage::helper('opentechiz_purchase')->__('Is Paid'),
                'name'      => 'is_paid',
                'type'      => 'options',
                'options'   => Mage::helper('opentechiz_purchase')->getPoPaidStatus()
            ));
        }else{
            if($PO->getIsPaid()){
                 $fieldset->addField('is_paid', 'select', array(
                    'label'     => Mage::helper('opentechiz_purchase')->__('Is Paid'),
                    'name'      => 'is_paid',
                    'type'      => 'options',
                    'disabled' =>$disabled,
                    'options'   => Mage::helper('opentechiz_purchase')->getPoPaidStatus()
                ));
             }else{
                   $fieldset->addField('is_paid', 'select', array(
                    'label'     => Mage::helper('opentechiz_purchase')->__('Is Paid'),
                    'name'      => 'is_paid',
                    'type'      => 'options',
                    
                    'options'   => Mage::helper('opentechiz_purchase')->getPoPaidStatus()
                )); 
             }
           
        }
        $excludeStatus = array(OpenTechiz_Purchase_Helper_Data::PO_STATUS_CLOSED,OpenTechiz_Purchase_Helper_Data::PO_STATUS_DRAFTED);
        $statusArray = Mage::helper('opentechiz_purchase')->getPurchaseOrderStatus(null,$excludeStatus);
        if($PO->getId() && !isset($statusArray[$PO->getStatus()])){
            $statusArray = Mage::helper('opentechiz_purchase')->getPurchaseOrderStatus();
        }
        $fieldset->addField('status','select', array(
            'label'    => Mage::helper('opentechiz_purchase')->__('Status'),
            'name'     => 'status',
            'type'      => 'options',
            'options'   => $statusArray,
            'disabled' =>$disabled
        ));

        $users = $this->helper('opentechiz_purchase')->getListUserActive();
        $user = '';
        if (array_key_exists($PO->getUserId(), $users)) {
            $user = $users[$PO->getUserId()];
        }
        $fieldset->addField('user_id', 'note', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Create By'),
            'text'      => $user
        ));

        parent::_prepareForm();

        if ( Mage::registry('purchaseOrder') ) {
            $data = Mage::registry('purchaseOrder')->getData();
            if(isset($data['total_include_tax'])) {
                $data['total_include_tax'] = Mage::getModel('directory/currency')->format($data['total_include_tax'], array('display' => Zend_Currency::NO_SYMBOL), false);
            }else{
                $data['total_include_tax'] = Mage::getModel('directory/currency')->format(0, array('display' => Zend_Currency::NO_SYMBOL), false);;
            }
            $form->setValues($data);

            if(isset($getSupplierID))
            {
                $form->addValues(array('supplier_id'=> $getSupplierID));
            }
            else
            {
                $form->addValues(array('supplier_id'=> $supplierId ));
            }

            if(!isset($data['delivery_date'])){
                $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
                $currentTime = strtotime($date);
                $secondsToAdd = 3 * 7 * 24 * (60 * 60); //3 weeks
                $newTime = $currentTime + $secondsToAdd;
                $newTime = date("m/d/Y H:i:s", $newTime);
                $form->addValues(array('delivery_date' => $newTime));
            }else{
                $date = $PO->getDeliveryDate();
                $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $date);
                $newDateString = $myDateTime->format('m/d/Y H:i:s');
                $form->addValues(array('delivery_date' => $newDateString));
            }
        }
        return $this;
    }
}