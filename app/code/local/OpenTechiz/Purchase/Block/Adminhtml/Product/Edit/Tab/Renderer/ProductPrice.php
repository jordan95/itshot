<?php

class OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tab_Renderer_ProductPrice extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $html = Mage::helper('core')->currency($row->getData($this->getColumn()->getIndex()), true, false);
        return $html;
    }
}