<?php

class OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tab_Product extends Mage_Adminhtml_Block_Widget_Grid {

    private $_supplier_id;



    /**
     * Get supplier
     *
     * @return integer $value
     */
    public function getSupplierId() {
        if($sup_id = Mage::registry('purchaseOrder')->getSupplierId())
            return $sup_id;
        return $this->getRequest()->getParam('supplier');
    }

    public function __construct() {
        parent::__construct();
        $this->setId('SupplierProductsGrid');
        $this->setTemplate('opentechiz/purchase/grid.phtml');
        $this->setEmptyText($this->__('No items'));
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
        $this->setVarNameFilter('supplier_products');
        $this->setDefaultSort('productsupplierprice', 'asc');
    }


    public function getOrderitem($po_id){
        $collection = mage::getModel('opentechiz_purchase/purchase_item')
                ->getCollection()->addFieldToFilter('po_id',$po_id);
    }
    public function getPurchaseStatus(){
        if (Mage::registry('purchaseOrder')) {
            return  Mage::registry('purchaseOrder')->getStatus();
        }
        return 0;
    }
    /**
     * Prepare collection
     *
     * @return object $collection
     */
    protected function _prepareCollection() {
        $collection = mage::getModel('catalog/product')
                ->getCollection()->addAttributeToSelect('name');
        $latestCostTable = new Zend_Db_expr("(SELECT product_id, last_cost, max(last_cost_date) as last_cost_date FROM `tsht_inventory_instock_product` GROUP BY product_id)");
        $productsupplier = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/productsupplier');
        $collection->getSelect()->join(array('productsupplier'=>$productsupplier), "e.entity_id = productsupplier.product_id AND productsupplier.sup_id =".$this->getSupplierId(), array('productsupplierprice'=>"productsupplier.last_cost",'sup_sku'=>'productsupplier.sup_sku'));
        if(Mage::helper('opentechiz_purchase')->isCreatedFromErpProduct()) { //put selected ids on top result
            $idArray = explode(',', $this->getRequest()->getParams()['productIds']);
            $idArrayString = '(';
            foreach ($idArray as $id){
                $idArrayString .= $id.',';
            }
            $idArrayString = trim($idArrayString, ',');
            $idArrayString .= ')';
            $collection->getSelect()->order(array('entity_id in '.$idArrayString.' DESC'));
        }
        $collection->getSelect()->join(array('lastCostTable' => $latestCostTable), 'productsupplier.product_id = lastCostTable.product_id', '*');

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Grid columns definition
     *
     * @return unknown
     */
    protected function _prepareColumns() {
         $PO = Mage::registry('purchaseOrder');
         if($PO->getStatus()!=1){
            $this->addColumn('product_id_check', array(
                'header_css_class' => 'a-center',
                'header' => Mage::helper('adminhtml')->__('Select'),
                'type' => 'checkbox',
                'index' => 'product_id',
                'align' => 'center',
                'renderer' => 'OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tab_Renderer_Id',
                 'filter' =>false
            ));
        }
        $this->addColumn('last_cost', array(
            'header' => Mage::helper('adminhtml')->__('Last Cost'),
            'index' => 'last_cost',
            'align' => 'center',
            'renderer' => 'OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tab_Renderer_ProductPrice',
            'width'=>'15px',
            'filter' =>false
        ));
        $this->addColumn('last_cost_date', array(
            'header' => Mage::helper('adminhtml')->__('Last Cost Date'),
            'index' => 'last_cost_date',
            'align' => 'center',
            'renderer' => 'OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tab_Renderer_DateConvert',
            'width'=>'15px',
            'filter' =>false
        ));
        $this->addColumn('product_price', array(
            'header' => Mage::helper('adminhtml')->__('Supplier\'s Price'),
            'index' => 'productsupplierprice',
            'align' => 'center',
            'renderer' => 'OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tab_Renderer_ProductPrice',
            'width'=>'15px',
            'filter' =>false
        ));
        $this->addColumn('sku', array(
            'header' => Mage::helper('catalog')->__('Sku'),
            'index' => 'sku',
            'filter_index' => 'sku'
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('catalog')->__('Name'),
            'index' => 'name',
            'filter_index' => 'name'
        ));

        $this->addColumn('sup_sku', array(
            'header' => Mage::helper('catalog')->__('Supplier Sku'),
            'index' => 'sup_sku',
            'filter_index' => 'productsupplier.sup_sku',
            'sort_index' => 'productsupplier.sup_sku',
            'sortable' => false,
            'filter' => false
        ));


        $this->addColumn('productsupplierprice', array(
            'header' => Mage::helper('catalog')->__('Price'),
            'index' => 'productsupplierprice',
            'column_css_class'=>'no-display',
            'header_css_class'=>'no-display',
        ));

        

        return parent::_prepareColumns();
    }

 
    public function getOrderItems()
    {
        $po_id = (int)$this->getRequest()->getParam('id');
        $html = '';
 

        if ($po_id) {
            $productsupplier = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/productsupplier');
            $orderItem = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()->addFieldToFilter('po_id',$po_id);
            $orderItem->getSelect()->joinLeft(array('productsupplier'=>$productsupplier), "main_table.product_id = productsupplier.product_id AND productsupplier.sup_id =".$this->getSupplierId(), array('productsupplierprice'=>"productsupplier.last_cost",'sup_sku'=>'productsupplier.sup_sku'));
            $sup = Mage::getModel('opentechiz_purchase/supplier')->load($this->getSupplierId());
            foreach ($orderItem as $item) {
                 # code...
                $option = $item->getOption();
                $sku = $item->getSku();
                $product_id = $item->getProductId();
                $product = Mage::getModel('catalog/product')->load($product_id);
                $option_ar = array();
                $name = $product->getName();
                $price = Mage::helper('custom')->convertPrice($item->getPrice());
                $option_item_ar = unserialize($option);
                if(isset($option_item_ar['options']))
                    $option_ar = $option_item_ar['options'];
                $last_cost = Mage::helper('custom')->convertPrice(Mage::helper('opentechiz_purchase')->getLastCostBySku($sku)); 
                $sale_price = Mage::helper('custom')->convertPrice($this->getSalePriceByOption($option_ar ,$product));
                $last_cost_date = Mage::helper('opentechiz_purchase')->getLastCostDate($sku);
                $qty = $item->getQty();
                $delivered_qty = $item->getDeliveredQty();
                $note = $item->getNote();
                $sku_id = str_replace('.', '', $sku);
                $id = $product_id.'_'.$sku_id;
                $total = Mage::helper('custom')->convertPrice($item->getPrice()*$qty); 
                $sup_sku = $item->getData('sup_sku');
                $po = Mage::getModel('opentechiz_purchase/purchase')->load($po_id);

                if (Mage::helper('opentechiz_purchase')->isActivePortalSupplier($sup)) {
                    if ((int)$item->getShippedQty() > 0) {
                        $allowed_delivered_input = '';
                    }else{
                        $allowed_delivered_input = 'disabled="disabled"';
                    }
                }else{
                    $allowed_delivered_input = '';
                }

                if ($item->getShipmentDate()) {
                    $shipment_date = date('m/d/Y', strtotime($item->getShipmentDate()));
                }else{
                    $shipment_date = '';
                }
                
                if($delivered_qty != $qty){
                     $html.="<input  type=\"hidden\" name=\"item[".$id."][delete]\"   ><tr id=\"product_".$id."\" class=\"even item_purchase\"><td class=\"first\"><h5 class=\"title\"><span id=\"order_item_".$id."_title\">".$name."</span><input  class=\"required-entry input-text\" type=\"hidden\" name=\"item[".$id."][product_id]\" value=".$product_id." >";
                     $html .= "</h5><div><strong>SKU:</strong>".Mage::helper('opentechiz_purchase')->getTagSku($sku)."<input  class=\"required-entry input-text sku_data\" type=\"hidden\" id=\"sku_".$id."\" name=\"item[".$id."][sku]\" value=\"".$sku."\" ></div>";
                     foreach ($option_ar as $option){
                         $html .= "<strong>".$option['label'].":</strong> ".$option['value'].'<br>';
                     }
                     $html .= '</td>';
                     $html .= "<td class=\"first\">".$sup_sku." </td> ";
                     $html .= "<td class=\"first\"><input  class=\"required-entry input-text price_data\" type=\"text\" id=\"price_".$id."\" name=\"item[".$id."][price]\" value=\"".$price."\" onchange=\"UpdateTotal('".$id."')\"> </td> ";
                     $html .= "<td class=\"first costData\" id=\"cost_".$id."\">".$last_cost." </td> ";
                     $html .= "<td class=\"first\">".$last_cost_date." </td> ";
                     $html .= "<td class=\"first\">".$sale_price." </td> ";
                     $html .= "<td class=\"first\"><input  class=\"required-entry input-text purchase_qty\" type=\"text\" id=\"qty_".$id."\" name=\"item[".$id."][qty]\" value=\"".$qty."\" onchange=\"UpdateTotal('".$id."')\"></td>";
                                
                     $html .= "<td class=\"first\"><span id='total_".$id."' class='total'>".$total."</span></td>";
                     $html .= "<td class=\"first\"><span id='shipped_qty_".$id."' class='total'>".$item->getShippedQty()."</span></td>";
                     $html .= "<td class=\"first\"><span id='shipped_date_".$id."' class='total'>".$shipment_date."</span></td>";
                     $html .= "<td class=\"first\"><input  class=\"required-entry delivered_qty\" type=\"number\" id=\"delivered_qty_".$id."\" name=\"item[".$id."][delivered_qty]\" value=\"".$delivered_qty."\" min =\"".$delivered_qty."\" max=\"".$qty."\"></td>";
                     $html .= "<td class=\"first\"><input  class=\"note\" type=\"text\" name=\"item[".$id."][note]\" value=\"".$note."\"  ></td><td class=\"first\"><a href=\"#\" onclick=\"hideItem('".$id."','".$sku."')\">Remove</a></td></tr>";
                }else{
                     $allowEditPrice = Mage::getSingleton('admin/session')->isAllowed('admin/erp/purchase/product/edit_product_cost');
                     $html.="<input  type=\"hidden\" name=\"item[".$id."][delete]\"   ><tr id=\"product_".$id."\" class=\"even item_purchase\"><td class=\"first\"><h5 class=\"title\"><span id=\"order_item_".$id."_title\">".$name."</span>";

                     $html .= "</h5><div><strong>SKU:</strong>".Mage::helper('opentechiz_purchase')->getTagSku($sku)."</div>";
                     foreach ($option_ar as $option){
                         $html .= "<strong>".$option['label'].":</strong> ".$option['value'].'<br>';
                     }
                     $html .= '</td>';
                     $html .= "<td class=\"first\">".$sup_sku." </td> ";
                     if($allowEditPrice){
                         $html .= "<td class=\"first\"><input  class=\"required-entry input-text\" type=\"text\" name=\"item[".$id."][price]\" value=\"".$price."\"> </td> ";
                         $html .= "<input  class=\"required-entry input-text\" type=\"hidden\" name=\"item[".$id."][sku]\" value=\"".$sku."\" >";
                     }else {
                         $html .= "<td class=\"first\">" . $price . " </td> ";
                     }
                     $html .= "<td class=\"first\">".$last_cost." </td> ";
                     $html .= "<td class=\"first\">".$last_cost_date." </td> ";
                     $html .= "<td class=\"first\">".$sale_price." </td> ";
                     $html .= "<td class=\"first purchase_qty\">".$qty."</td>";  
                     $html .= "<td class=\"first\"><span id='total_".$id."' class='total'>".$total."</span></td>" ;      
                     $html .= "<td class=\"first\"><span id='shipped_qty_".$id."' class='total'>".$item->getShippedQty()."</span></td>";
                     $html .= "<td class=\"first\"><span id='shipped_date_".$id."' class='total'>".$shipment_date."</span></td>";
                     $html .= "<td class=\"first delivered_qty\">".$delivered_qty."</td>";
                     $html .= "<td class=\"first note\">".$note."</td><td class=\"first\"></td></tr>";
                }
             } 
        }

        return $html;

    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/gridproductadd', array('id'=>$this->getRequest()->getParam('id'), 'supplier'=>$this->getRequest()->getParam('supplier')));
    }

    public function getSalePriceByOption($options ,$product){
        $price = 0;
        $isPersonalized = Mage::helper('personalizedproduct')->isPersonalizedProduct($product);
        if(!$isPersonalized) $price = $product->getFinalPrice();
        foreach ($options as $option) {
                # code...
                $option_sku = Mage::getResourceModel('catalog/product_option_value_collection')
                        ->addFieldToFilter('option_type_id', $option['option_value'])->getFirstItem()->getSku();
                
                 if($option['option_type'] == 'stone' ){

                    if(Mage::helper('custom')->isDiamond($option_sku)){
                        
                        $qualityData = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStoneWithQualityPrice($option['option_id'],$product->getId());
                        $quality = str_replace("diamond", "", $option_sku);
                        $price += number_format($qualityData[$quality], 2);
                        $stone_sku[] = "diamond";
                    }else{
                        
                      $data = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStonePrice($option['option_id'],$product->getId());
                      $price += number_format($data[$option_sku], 2);
                      $stone_sku[] = $option_sku;
                    }
                    
                 }

                 if($option['option_type'] == 'gold' ){
                    $data = Mage::helper('personalizedproduct/personalizedPriceHelper')->getAlloyPrice($product->getId());
                    $price += $data[$option_sku];
                 }
                       
            }
        if($product->getAdditionalFee())
              $price += $product->getAdditionalFee();

        return $price;
          
    }
    
    

}
