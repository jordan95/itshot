<?php

class OpenTechiz_Purchase_Block_Adminhtml_Product_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'opentechiz_purchase';
        $this->_controller = 'adminhtml_product';
        parent::__construct();

        $poId = $this->getRequest()->getParam('id');

        if($poId) {
            $po = Mage::getModel('opentechiz_purchase/purchase')->load($poId);
            if(count(Mage::getModel('opentechiz_purchase/poitem')->getCollection()->addFieldToFilter('po_id', $po->getId())) > 0) {
                $this->_addButton('poreturn', array(
                    'label' => $this->__('Add/Edit Return for purchase order'),
                    'onclick' => 'setLocation(\'' . $this->getNewPOReturnUrl($poId) . '\')'
                ));
            }
        }

        $this->_updateButton('save', 'label', Mage::helper('opentechiz_purchase')->__('Save and Email Supplier'));
        $this->_updateButton('save', 'onclick', 'return confirmFunction(\'save\');');

        $note = '';
        if(isset($po) && $po && $po->getIsSupplierNotified()) {
            $this->_removeButton('save');
            $note = '<br>Note: The Supplier will not be notified.';
        }

        $this->_removeButton('delete');

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' =>  'return confirmFunction(\'savencontinue\');',
            'class' => 'save',
            'after_html' => $note
        ), -100);

        $this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('supplier_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'supplier_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'supplier_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
    }

    public function getNewPOReturnUrl($poId){
        return $this->getUrl('*/purchase_return/edit', array('id' => $poId));
    }

    public function getHeaderText()
    {
        if( Mage::registry('purchaseOrder') && Mage::registry('purchaseOrder')->getId() ) {
            return Mage::helper('opentechiz_purchase')->__("Edit Purchase Order #%s", $this->htmlEscape(Mage::registry('purchaseOrder')->getPoIncrementId()));
        } else {
            return Mage::helper('opentechiz_purchase')->__('Add Purchase Order');
        }
    }
}