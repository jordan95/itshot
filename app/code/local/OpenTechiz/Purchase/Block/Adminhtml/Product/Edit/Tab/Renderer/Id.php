<?php

class OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tab_Renderer_Id extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value =  $row->getId();

        $html = '<input type="checkbox" id="'.$value.'" class="productadd" name="product['.$value.'][id]" value=\''.$value.'\' onchange="ProductAdd(this)">';
        return $html;
    }
}