<?php
class OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tab_Form_Element_Datetime extends Varien_Data_Form_Element_Datetime
{

    /**
     * Get date value as string.
     * Format can be specified, or it will be taken from $this->getFormat()
     *
     * @param string $format (compatible with Zend_Date)
     * @return string
     */
    public function getValue($format = null)
    {
        if (empty($this->_value)) {
            return '';
        }
        if (null === $format) {
            $format = $this->getFormat();
        }
      $value = $this->_value->toString($format);
        
        $format = "m/d/Y H:i:s";
        $date = DateTime::createFromFormat($format, $value)->format("d/m/y H:i:s");
       
        return  Mage::getModel('core/date')->date('m/d/Y H:i:s',  $date);
    }
}