<?php

class OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('supplier_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('opentechiz_purchase')->__('Product Purchase Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('general', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('General'),
            'title'     => Mage::helper('opentechiz_purchase')->__('General'),
            'content'   => $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_product_edit_tab_form')->toHtml(),
            'active'    =>  false
        ));
        $this->addTab('productlist', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Product'),
            'title'     => Mage::helper('opentechiz_purchase')->__('Product'),
            'content'   => $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_product_edit_tab_product')->toHtml(),
            'active'    =>  true
        ));
        return parent::_beforeToHtml();
    }
}