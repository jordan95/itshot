<?php

class OpenTechiz_Purchase_Block_Adminhtml_Product_New extends OpenTechiz_Purchase_Block_Adminhtml_Purchase_Gold_New
{
    public function __construct()
    {
        parent::__construct();

    }

    public function getBackUrl()
    {
        return $this->getUrl('adminhtml/purchase_product/index', array());
    }

    public function getNewSupplierUrl()
    {
        return $this->getUrl('adminhtml/supplier/new');
    }

    /**
     *
     */
    public function getSuppliersAsCombo($name='supplier')
    {
        $retour = '<select  id="'.$name.'" name="'.$name.'">';

        $collection = Mage::getModel('opentechiz_purchase/supplier')
            ->getCollection()
            ->setOrder('name', 'asc');
        foreach ($collection as $item)
        {
            $retour .= '<option value="'.$item->getId().'">'.$item->getname().'</option>';
        }

        $retour .= '</select>';
        return $retour;
    }
}