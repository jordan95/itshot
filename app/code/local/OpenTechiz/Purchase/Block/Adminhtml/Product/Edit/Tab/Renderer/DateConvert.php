<?php

class OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tab_Renderer_DateConvert extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        if(!$row->getData($this->getColumn()->getIndex()) || stripos($row->getData($this->getColumn()->getIndex()), 'null') !== false ){
            $html = '';
        }
        else{
            if ($this->getColumn()->getIndex() == 'last_cost_date') {
                $html = date_format(date_create($row->getData($this->getColumn()->getIndex())), "m/d/Y");
            } else {
                $html = Mage::helper('core')->formatDate($row->getData($this->getColumn()->getIndex()));
            }
        }

        return $html;
    }
}