<?php

class OpenTechiz_Purchase_Block_Adminhtml_Product_Edit_Tab_Price_Supplier
    extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Price_Group_Abstract
{
    /**
     * Initialize block
     */
    public function __construct()
    {
        $this->setTemplate('catalog/product/edit/price/supplier.phtml');
    }

    public function getValues()
    {
        $productId = $this->getProduct()->getId();
        /** @var OpenTechiz_Purchase_Model_Resource_Supplier_Collection $collection */
        $collection = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection();
        $collection->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('last_cost_date', [['notnull' => true], ['neq'=>'']]);
        $collection->join(
            ['supplier' => 'opentechiz_purchase/supplier'],
            'main_table.sup_id = supplier.supplier_id',
            ['name']
        );
        return $collection->getItems();
    }
}
