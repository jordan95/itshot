<?php

class OpenTechiz_Purchase_Block_Adminhtml_Supplier extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_supplier';
        $this->_blockGroup = 'opentechiz_purchase';
        $this->_headerText = Mage::helper('opentechiz_purchase')->__('Supplier List');
        parent::__construct();

        $this->_addButton('import', array(
            'label' => Mage::helper('opentechiz_material')->__('Import'),
            'onclick' => 'document.getElementById(\'uploadTarget\').click();',
            'class' => 'add',
            'after_html' => '<form id="uploadForm" method="POST" action="'.$this->getUrl('*/*/import', array()).'" enctype="multipart/form-data">
            <input name="form_key" type="hidden" value="'.Mage::getSingleton('core/session')->getFormKey().'" />
            <input type="file" name="file" style="display:none;" id="uploadTarget"/>
        </form>
        <script type="text/javascript">
            document.getElementById(\'uploadTarget\').addEventListener(\'change\', function(){
                document.getElementById(\'uploadForm\').submit();
            }, false);
        </script>',
        ));
    }

    public function getHeaderCssClass()
    {
        return 'icon-head head-warehouse-stone';
    }
}