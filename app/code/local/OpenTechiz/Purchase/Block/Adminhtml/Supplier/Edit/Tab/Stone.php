<?php
class OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit_Tab_Stone extends Mage_Adminhtml_Block_Widget_Grid
{
public function __construct()
    {
        parent::__construct();
        $this->setId('stoneGrid');
        $this->setDefaultSort('stone_id');
        $this->setTemplate('opentechiz/purchase/supplierstonegrid.phtml');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    public function getSupplierId() {
        return  (int)$this->getRequest()->getParam('id');
    }
    public function getStoneSupplier(){
        $html = '';
        if($this->getSupplierId()){

            $collection = mage::getModel('opentechiz_material/stone')
                ->getCollection();
        
       $stonesupplier = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/stonesupplier');
       $collection->getSelect()->join(array('stonesupplier'=>$stonesupplier), "main_table.stone_id = stonesupplier.stone_id AND stonesupplier.supplier_id = ".$this->getSupplierId(), array('stonesupplierprice'=>"stonesupplier.price",'supplier_id'=>"stonesupplier.supplier_id"));
           foreach ($collection as $stone) {
                $html .= '<tr id="addstone'.$stone->getId().'"> <td class="a-center "><input name="stones['.$stone->getId().'][price]" value="'.number_format($stone->getData('stonesupplierprice'),2).'"> <input type="hidden" name="stones['.$stone->getId().'][stone_id]" value="'.$stone->getId().'"></td> <td class="a-center ">'.$stone->getId().'</td><td class=" ">'.$stone->getStoneType().'</td><td class=" ">'.$stone->getWeight().'</td><td class=" ">'.$stone->getQuality().'</td><td class=" a-right ">'.$stone->getShape().'</td><td class=" a-right ">'.$stone->getDiameter().'</td><td class=" a-right ">'.number_format($stone->getPrice(),2).'</td><td class=" a-right ">'.$stone->getQty().'</td><td class=" a-right ">'.$stone->getOnHold().'</td><td class=" a-right ">'.$stone->getReorderPoint().'</td><td class=" a-right ">'.$stone->getIdeaStockLevel().'</td><td class=" a-right ">'.$stone->getShelfLocation().'</td><td class="first"><a href="#" onclick="hidestone('.$stone->getId().')">Remove</a></td></tr>';
               
           }
           
        }
        return $html;
        
    }
    protected function _prepareCollection()
    {
        $collection = mage::getModel('opentechiz_material/stone')
                ->getCollection();
        
       $stonesupplier = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/stonesupplier');
       $collection->getSelect()->joinLeft(array('stonesupplier'=>$stonesupplier), "main_table.stone_id = stonesupplier.stone_id AND stonesupplier.supplier_id = ".$this->getSupplierId(), array('stonesupplierprice'=>"stonesupplier.price",'supplier_id'=>"stonesupplier.supplier_id"));
        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('stone_id_check', array(
            'header_css_class' => 'a-center',
            'header'    => Mage::helper('adminhtml')->__('Select'),
            'index'     => 'stone_id',
            'align'     => 'center',
            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit_Tab_Renderer_Stone_Id',
            'filter'    => false,
            'sortable'  => false,
            'width'     => '80'

        ));
        
        $this->addColumn('stone_id', array(
            'header' => Mage::helper('opentechiz_material')->__('ID'),
            'width' => '80',
            'index' => 'stone_id',
            'filter_index' => 'main_table.stone_id',
            'align' => 'center'
        ));
        $this->addColumn('stone_type', array(
            'header' => Mage::helper('opentechiz_material')->__('Type'),
            'index' => 'stone_type',
            'width' => '150',
            'type'      => 'options',
            'options'   => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_TYPE,
        ));
        $this->addColumn('stone_size', array(
            'header' => Mage::helper('opentechiz_material')->__('Size'),
            'index' => 'weight',
            'width' => '100'
        ));
        $this->addColumn('quality', array(
            'header' => Mage::helper('opentechiz_material')->__('Quality'),
            'index' => 'quality',
            'type'      => 'options',
            'width'     => '100',
            'options'   => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_QUALITY,
        ));
        $this->addColumn('shape', array(
            'header' => Mage::helper('opentechiz_material')->__('Shape'),
            'index' => 'shape',
            'width' => '150',
            'type'      => 'options',
            'options'   => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_SHAPE,
        ));
        $this->addColumn('diameter', array(
            'header' => Mage::helper('opentechiz_material')->__('Diameter'),
            'index' => 'diameter',
            'width' => '50'
        ));
        $this->addColumn('price', array(
            'header' => Mage::helper('opentechiz_material')->__('Sale Price'),
            'index' => 'price',
            'width' => '50',
            'renderer'  => 'OpenTechiz_Purchase_Block_Widget_Column_Renderer_Format_AllPrice',
        ));
        $this->addColumn('current_qty', array(
            'header' => Mage::helper('opentechiz_material')->__('Current Quantity'),
            'index' => 'qty',
            'width' => '50',
            'type' => 'number'
        ));
        $this->addColumn('on_hold', array(
            'header' => Mage::helper('opentechiz_material')->__('On Hold'),
            'index' => 'on_hold',
            'width' => '50',
            'type' => 'number'
        ));
        $this->addColumn('reorder_point', array(
            'header' => Mage::helper('opentechiz_material')->__('Reorder Point'),
            'index' => 'reorder_point',
            'width' => '50',
            'type' => 'number',
        ));
        $this->addColumn('ideal_stock_level', array(
            'header' => Mage::helper('opentechiz_material')->__('Ideal Stock Level'),
            'index' => 'ideal_stock_level',
            'text' => 'text',
            'width' => '50',
            'type' => 'number'
        ));
        $this->addColumn('shelf_location', array(
            'header' => Mage::helper('opentechiz_material')->__('Shelf Location'),
            'index' => 'shelf_location',
            'text' => 'text',
            'width' => '50',
            'type' => 'text'
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('opentechiz_material')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                0 => 'Disable',
                2 => 'Reorder',
                3 => 'OK'
            ),
            'align' => 'center',
            'renderer' => 'OpenTechiz_Material_Block_Adminhtml_Material_Stone_Renderer_Status',
        ));
        

        return parent::_prepareColumns();

    }
    public function getGridUrl()
    {
        return $this->getUrl('*/*/gridstone', array('_current'=>true));
    }


}