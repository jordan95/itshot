<?php

class OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit_Tab_Renderer_Gold_Id extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value =  $row->getId();
        $sup_id = $row->getData('supplier_id');
        if($sup_id)
        	$html = '<input type="checkbox" class="suppliergold" name="golds['.$value.'][gold_id]" value=\''.$value.'\' checked="checked" onchange="Addgold(this)">';
        else
        	$html = '<input type="checkbox" class="suppliergold" name="golds['.$value.'][gold_id]" value=\''.$value.'\'  onchange="Addgold(this)" >';
        return $html;
    }
}