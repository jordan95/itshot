<?php

class  OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit_Tab_Purchase extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('purchaseGrid');
        $this->setDefaultSort('po_id');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }
    public function getSupplierId() {
        return  (int)$this->getRequest()->getParam('id');
    }
    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_purchase/purchase')->getCollection()

        ->join('supplier', 'supplier.supplier_id=main_table.supplier_id AND main_table.supplier_id = '.$this->getSupplierId());

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

     public function getGridUrl()
    {
        return $this->getUrl('*/*/gridpurchase', array('_current'=>true));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('po_id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('PO #'),
            'index'     => 'po_id',
            'align'     => 'center',
            'width'     => '50',
            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit_Tab_Renderer_POLink',
        ));
        $this->addColumn('po_increment_id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('PO Increment #'),
            'index'     => 'po_increment_id',
        ));
        $this->addColumn('created_at', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Created At'),
            'index'     => 'created_at',
            'type'      => 'datetime'
        ));
        $this->addColumn('updated_at', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Updated At'),
            'index'     => 'updated_at',
            'type'      => 'datetime'
        ));
        $this->addColumn('delivery_date', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Estimated time of arrival'),
            'index'     => 'delivery_date',
            'type'      => 'datetime'
        ));
    
        $this->addColumn('status', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Status'),
            'index'     => 'status',
            'type'      => 'options',
            'options'   => Mage::helper('opentechiz_purchase')->getPurchaseOrderStatus(),
            'align'     => 'center'
        ));
         $this->addColumn('type', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Type'),
            'index'     => 'type',
            'type'      => 'options',
            'options'   => Mage::helper('opentechiz_purchase')->getPurchaseOrderType(),
            'align'     => 'center'
        ));
        $this->addColumn('total_include_tax', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Total (Incl. tax)'),
            'index'     => 'total_include_tax',
            'align'     => 'center',
            'renderer'  => 'OpenTechiz_Purchase_Block_Widget_Column_Renderer_Format_AllPrice',
        ));
        $this->addColumn('is_paid', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Is Paid'),
            'index'     => 'is_paid',
            'type'      => 'options',
            'align'     => 'center',
            'options'   => array( 0 => 'No', 1 => 'Yes')
        ));

        return parent::_prepareColumns();
    }



    public function getRowUrl($row)
    {
        $po = Mage::getModel('opentechiz_purchase/purchase')->load($row->getId());
        $type = Mage::helper('opentechiz_purchase')->getPurchaseOrderType($po->getType());
        $type = strtolower($type);
        return $this->getUrl('adminhtml/purchase_'.$type.'/edit', array('id' => $row->getId()));
    }
}