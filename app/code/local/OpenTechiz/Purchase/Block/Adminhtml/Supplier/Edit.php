<?php

class OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'opentechiz_purchase';
        $this->_controller = 'adminhtml_supplier';
        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('opentechiz_purchase')->__('Save'));

        if (!Mage::getSingleton('admin/session')->isAllowed('admin/erp/supplier/supplier_delete')) {
            $this->removeButton('detele');
        } else {
            $this->_updateButton('delete', 'label', Mage::helper('opentechiz_purchase')->__('Delete'));
        }

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('supplier_content') == null)
                    tinyMCE.execCommand('mceAddControl', false, 'supplier_content');
                else
                    tinyMCE.execCommand('mceRemoveControl', false, 'supplier_content');
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }

            function updateActivePortal(element){
                if(element.checked){
                    element.value = 1;
                    $('current_password').up('tr').show();
                    $('username').up('tr').show();
                    $('password').up('tr').show();
                    $('password_confirmation').up('tr').show();

                    $('current_password').disabled = false;
                    $('current_password').addClassName('required-entry');
                    $('username').disabled = false;
                    $('password').disabled = false;
                    $('password_confirmation').disabled = false;
                }else{
                    element.value = 0;
                    $('current_password').up('tr').hide();
                    $('username').up('tr').hide();
                    $('password').up('tr').hide();
                    $('password_confirmation').up('tr').hide();

                    $('current_password').disabled = true;
                    $('current_password').removeClassName('required-entry');
                    $('username').disabled = true;
                    $('password').disabled = true;
                    $('password_confirmation').disabled = true;
                }
            }

            window.addEventListener('load', function(){
                updateActivePortal($('is_active_portal'));
            });
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('supplier_data') && Mage::registry('supplier_data')->getId() ) {
            return Mage::helper('opentechiz_purchase')->__("Edit Supplier %s", $this->htmlEscape(Mage::registry('supplier_data')->getName()));
        } else {
            return Mage::helper('opentechiz_purchase')->__('Add Supplier');
        }
    }
}