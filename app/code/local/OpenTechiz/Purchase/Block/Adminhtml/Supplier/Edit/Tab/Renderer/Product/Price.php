<?php

class OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit_Tab_Renderer_Product_Price extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value =  $row->getData('productsupplierprice');
        $id = $row->getId();
        if($value)
        	$html = '<input name="products['.$id.'][price]" value="'.Mage::getModel('directory/currency')->format($value, array('display'=>Zend_Currency::NO_SYMBOL), false).'" >';
        else
        	$html = '<input name="products['.$id.'][price]" disabled >';
        return $html;
    }
}