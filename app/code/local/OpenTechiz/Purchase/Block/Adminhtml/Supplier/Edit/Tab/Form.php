<?php

class OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('supplier_form', array('legend'=>Mage::helper('opentechiz_purchase')->__('Supplier information')));

        $fieldset->addField('name', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Name'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'name',
        ));

        $fieldset->addField('address', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Address'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'address',
        ));
        $fieldset->addField('city', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('City'),
            'required'  => false,
            'name'      => 'city',
        ));
        $fieldset->addField('zipcode', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('ZIP Code'),
            'class'     => 'required-entry validate-number',
            'required'  => true,
            'name'      => 'zipcode',
        ));
        $fieldset->addField('country', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Country'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'country',
        ));
        $fieldset->addField('telephone', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Telephone'),
            'class'     => 'required-entry validate-not-negative-number',
            'required'  => true,
            'name'      => 'telephone',
        ));
        $fieldset->addField('fax', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('FAX'),
            'required'  => false,
            'name'      => 'fax',
        ));
        $fieldset->addField('email', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Email'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'email',
        ));

        $checked = '';
        if (Mage::registry('supplier_data')->getIsActivePortal()) {
            $checked = 'checked';
        }

        $fieldset->addField('is_active_portal', 'checkbox', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Portal Access'),
            'name'      => 'is_active_portal',
            'checked'   => $checked,
            'onclick'  => 'updateActivePortal(this)',
        ));

        $fieldset->addField('username', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Username'),
            'name'      => 'username',
        ));
        $fieldset->addField('password', 'password', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('New Password'),
            'name'      => 'password',
        ));
        $fieldset->addField('password_confirmation', 'password', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Password Confirmation'),
            'name'      => 'password_confirmation',
        ));
        $fieldset->addField('current_password', 'password', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Current Admin Password'),
            'name'      => 'current_password',
            'required'  => true,
        ));
        $fieldset->addField('website', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Website'),
            'required'  => false,
            'name'      => 'website',
        ));
        $fieldset->addField('contact', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Contact'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'contact',
        ));


        if ( Mage::getSingleton('adminhtml/session')->getWebData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getWebData());
            Mage::getSingleton('adminhtml/session')->setWebData(null);
        } elseif ( Mage::registry('supplier_data') ) {
            $form->setValues(Mage::registry('supplier_data')->getData());
        }
        return parent::_prepareForm();
    }
}