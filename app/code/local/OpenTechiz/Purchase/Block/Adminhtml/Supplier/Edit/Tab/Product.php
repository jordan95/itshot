<?php

class OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit_Tab_Product extends Mage_Adminhtml_Block_Widget_Grid {

    private $_supplier_id;

    

   
    public function getSupplierId() {
        return  (int)$this->getRequest()->getParam('id');
    }

    public function __construct() {
        parent::__construct();
        $this->setId('SupplierProductsGrid');
        $this->setTemplate('opentechiz/purchase/supplierproductgrid.phtml');
        $this->setEmptyText($this->__('No items'));
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('supplier_products');
        $this->setDefaultSort('productsupplierprice', 'asc');
    }
    public function getProductSupplier(){
        $html = '';
        if($this->getSupplierId()){

            $collection = mage::getModel('catalog/product')
                ->getCollection()->addAttributeToSelect('name');
            $msa_eventType = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/productsupplier');
           $collection->getSelect()->join(array('productsupplier'=>$msa_eventType), "e.entity_id = productsupplier.product_id AND productsupplier.sup_id = ".$this->getSupplierId(), array('productsupplierprice'=>"productsupplier.last_cost",'supplier_id'=>"productsupplier.sup_id", 'sup_sku'=>"productsupplier.sup_sku"));
           foreach ($collection as $product) {
               # code...
                $html .= '<tr id="addproduct'.$product->getId().'"> <td class="a-center "><input name="products['.$product->getId().'][last_cost]" value="'.number_format($product->getData('productsupplierprice'),2).'" readonly> <input type="hidden" name="products['.$product->getId().'][product_id]" value="'.$product->getId().'" ></td> <td class=" "> '.$product->getId().' <td class=" "> '.$product->getData('sku').' <td class=""> '.$product->getName().'\'</td> <td> <input name="products['.$product->getId().'][sup_sku]" value="'.$product->getSupSku().'"</td> <td class="first"><a href="#" onclick="hideItem('.$product->getId().')">Remove</a></td></tr>';
               
           }
           
        }
        return $html;
        
    }


    public function getGridUrl()
    {
        return $this->getUrl('*/*/gridproduct', array('_current'=>true));
    }

    /**
     *  collection
     *
     * @return unknown
     */
    protected function _prepareCollection() {
  
        $collection = mage::getModel('catalog/product')
                ->getCollection()->addAttributeToSelect('name');
        if($this->getSupplierId()){
            $msa_eventType = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/productsupplier');
           $collection->getSelect()->joinLeft(array('productsupplier'=>$msa_eventType), "e.entity_id = productsupplier.product_id AND productsupplier.sup_id = ".$this->getSupplierId(), array('productsupplierprice'=>"productsupplier.last_cost",'supplier_id'=>"productsupplier.sup_id"));
        }
        
        /*->addFieldToFilter('pps_supplier_num', $this->_supplier_id);*/

        

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Grid columns definition
     *
     * @return unknown
     */
    protected function _prepareColumns() {

        $this->addColumn('product_id_check', array(
            'header_css_class' => 'a-center',
            'header'    => Mage::helper('adminhtml')->__('Select'),
            'index'     => 'product_id',
            'align'     => 'center',
            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit_Tab_Renderer_Product_Id',
            'filter'    => false,
            'sortable'  => false
        ));
        $this->addColumn('product_id', array(
            'header' => Mage::helper('catalog')->__('ID'),
            'index' => 'entity_id',
        ));
        $this->addColumn('sku', array(
            'header' => Mage::helper('catalog')->__('Sku'),
            'index' => 'sku',
        ));
        $this->addColumn('product_name', array(
            'header' => Mage::helper('catalog')->__('Name'),
            'index' => 'name',
        ));
     
    
        

        return parent::_prepareColumns();
    }

   


    

}
