<?php

class OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('supplier_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('opentechiz_purchase')->__('Supplier Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('general', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('General'),
            'title'     => Mage::helper('opentechiz_purchase')->__('General'),
            'content'   => $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_supplier_edit_tab_form')->toHtml()
        ));
        $this->addTab('goldlist', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Gold'),
            'title'     => Mage::helper('opentechiz_purchase')->__('Gold'),
            'content'   => $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_supplier_edit_tab_gold')->toHtml()
        ));
        $this->addTab('stonelist', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Stone'),
            'title'     => Mage::helper('opentechiz_purchase')->__('Stone'),
            'content'   => $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_supplier_edit_tab_stone')->toHtml()
        ));
        $this->addTab('productlist', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Product'),
            'title'     => Mage::helper('opentechiz_purchase')->__('Product'),
            'content'   => $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_supplier_edit_tab_product')->toHtml()
        ));
        $this->addTab('purchase', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Purchase'),
            'title'     => Mage::helper('opentechiz_purchase')->__('Purchase'),
            'content'   => $this->getLayout()->createBlock('opentechiz_purchase/adminhtml_supplier_edit_tab_purchase')->toHtml()
        ));
        return parent::_beforeToHtml();
    }
}