<?php
class OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit_Tab_Renderer_POLink extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $po_id =  $row->getData('po_id');

        return '<a href="'.$this->getUrl('adminhtml/purchase_product/edit', array('id'=> $po_id)).'">'.$po_id.'</a>';
    }
}