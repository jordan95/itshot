<?php
class OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit_Tab_Gold extends Mage_Adminhtml_Block_Widget_Grid
{
public function __construct()
    {
        parent::__construct();
        $this->setId('goldsupplierGrid');
        $this->setDefaultSort('gold_id');
        $this->setDefaultDir('DESC');
        $this->setTemplate('opentechiz/purchase/suppliergoldgrid.phtml');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }
    
    public function getSupplierId() {
        return  (int)$this->getRequest()->getParam('id');
    }
    public function getGoldSupplier(){
        $html = '';
        if($this->getSupplierId()){

            $collection = mage::getModel('opentechiz_material/gold')
                ->getCollection();
        
            $goldsupplier = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/goldsupplier');
            $collection->getSelect()->join(array('goldsupplier'=>$goldsupplier), "main_table.gold_id = goldsupplier.gold_id AND goldsupplier.supplier_id = ".$this->getSupplierId(), array('goldsupplierprice'=>"goldsupplier.price",'supplier_id'=>"goldsupplier.supplier_id"));
           foreach ($collection as $gold) {
                $html .= '<tr id="addgold'.$gold->getId().'"> <td class="a-center "><input name="golds['.$gold->getId().'][price]" value="'.number_format($gold->getData('goldsupplierprice'),2).'"> <input type="hidden" name="golds['.$gold->getId().'][gold_id]" value="'.$gold->getId().'"></td> <td class="a-center ">'.$gold->getId().'</td><td class=" ">'.$gold->getGoldType().'</td><td class=" ">'.$gold->getGoldColor().'</td><td class=" ">'.number_format($gold->getPrice(),2).'</td><td class=" a-right ">'.$gold->getWeight().'</td><td class=" a-right ">'.$gold->getReorderPoint().'</td><td class="first"><a href="#" onclick="hideGold('.$gold->getId().')">Remove</a></td></tr>';
               
           }
           
        }
        return $html;
        
    }
    protected function _prepareCollection()
    {
        
        $collection = mage::getModel('opentechiz_material/gold')
                ->getCollection();
        
       $goldsupplier = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/goldsupplier');
       $collection->getSelect()->joinLeft(array('goldsupplier'=>$goldsupplier), "main_table.gold_id = goldsupplier.gold_id AND goldsupplier.supplier_id = ".$this->getSupplierId(), array('goldsupplierprice'=>"goldsupplier.price",'supplier_id'=>"goldsupplier.supplier_id"));
        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
         $this->addColumn('gold_id_check', array(
         'header_css_class' => 'a-center',
         'header'    => Mage::helper('adminhtml')->__('Select'),
         'index'     => 'gold_id',
         'align'     => 'center',
         'width'     => '80',
         'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_Supplier_Edit_Tab_Renderer_Gold_Id',
         'filter'    => false,
         'sortable' => false,
         'type'     => 'checkbox'
        ));

       

        $this->addColumn('gold_id', array(
                            'header' => Mage::helper('opentechiz_material')->__('ID'),
                            'width' => '80',
                            'index' => 'gold_id',
                            'filter_index' => 'main_table.gold_id',
                            'align' => 'center'
                        ));

        $this->addColumn('gold_type', array(
            'header'    => Mage::helper('opentechiz_material')->__('Gold Type'),
            'index'     => 'gold_type',
            'type'      => 'options',
            'options'   => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_TYPE,
        ));
        $this->addColumn('gold_color', array(
            'header'    => Mage::helper('opentechiz_material')->__('Gold Color'),
            'index'     => 'gold_color',
            'type'      => 'options',
            'options'   => OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_GOLD_COLOR,
        ));
        $this->addColumn('price', array(
            'header' => Mage::helper('opentechiz_material')->__('Sale Price'),
            'index' => 'price',
            'renderer'  => 'OpenTechiz_Purchase_Block_Widget_Column_Renderer_Format_AllPrice',
            'width' => '150'
        ));
        $this->addColumn('weight', array(
            'header' => Mage::helper('opentechiz_material')->__('Weight'),
            'index' => 'weight',
            'width' => '150'
        ));
        $this->addColumn('reorder_point', array(
            'header' => Mage::helper('opentechiz_material')->__('Reorder Point'),
            'index' => 'reorder_point',
            'width' => '150'
        ));

        return parent::_prepareColumns();

    }
    public function getGridUrl()
    {
        return $this->getUrl('*/*/gridgold', array('_current'=>true));
    }


}