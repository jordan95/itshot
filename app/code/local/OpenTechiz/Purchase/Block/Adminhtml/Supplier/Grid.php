<?php

class OpenTechiz_Purchase_Block_Adminhtml_Supplier_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('supplierGrid');
        $this->setDefaultSort('supplier_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_purchase/supplier')->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('name', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Name'),
            'index'     => 'name',
            'align'     => 'center',
        ));
        $this->addColumn('contact', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Contact'),
            'index'     => 'contact',
        ));
        $this->addColumn('telephone', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Phone'),
            'index'     => 'telephone',
        ));
        $this->addColumn('website', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Website'),
            'index'     => 'website',
        ));
        $this->addColumn('email', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Mail'),
            'index'     => 'email',
        ));
        $this->addColumn('po_count', array(
            'header'    =>  Mage::helper('opentechiz_purchase')->__('PO Count'),
            'index'     =>  'po_count',
            'sortable'  => false,
            'align'     => 'center',
            'renderer'  => 'OpenTechiz_Purchase_Block_Widget_Column_Renderer_Format_PoCount',
        ));
        $this->addColumn('po_total', array(
            'header'    =>  Mage::helper('opentechiz_purchase')->__('PO Total'),
            'index'     =>  'po_total',
            'sortable'  => false,
            'align'     => 'center',
            'renderer'  => 'OpenTechiz_Purchase_Block_Widget_Column_Renderer_Format_PoTotal',
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('admin/erp/purchase/supplier/suppliers/supplier_edit')) {
            $this->addColumn('action',
                array(
                    'header' => Mage::helper('opentechiz_production')->__('Action'),
                    'width' => '50',
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('opentechiz_production')->__('Edit'),
                            'url' => array('base' => '*/*/edit'),
                            'field' => 'id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'is_system' => true,
                    'align' => 'center'
                ));
        }
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('supplier_id');
        $this->getMassactionBlock()->setFormFieldName('item');
        $this->getMassactionBlock()->setUseSelectAll(false);

        // MassDelete
        $this->getMassactionBlock()->addItem('delete_item', array(
            'label'=> Mage::helper('opentechiz_purchase')->__('Delete'),
            'url'      => $this->getUrl('*/*/massDelete'),
            'confirm'  => Mage::helper('opentechiz_purchase')->__('Are you sure?')
        ));
        return $this;
    }

    public function getRowUrl($row)
    {
        if (Mage::getSingleton('admin/session')->isAllowed('admin/erp/purchase/supplier/suppliers/supplier_edit')) {
            return $this->getUrl('*/*/edit', array('id' => $row->getId()));
        }
    }

}