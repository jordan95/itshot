<?php

class OpenTechiz_Purchase_Block_Adminhtml_Product extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_product';
        $this->_blockGroup = 'opentechiz_purchase';
        $this->_headerText = Mage::helper('opentechiz_purchase')->__('Purchase List');

        $url = $this->getUrl('adminhtml/erpProduct/index');

        $this->_addButton('productGrid', array(
            'label' => Mage::helper('opentechiz_material')->__('ERP Products'),
            'onclick' => 'window.location=\'' . $url . '\'',
//            'class' => 'add'
        ));

        parent::__construct();
    }

 
}