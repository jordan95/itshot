<?php
class OpenTechiz_Purchase_Block_Adminhtml_Return_Renderer_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action
{
    public function render(Varien_Object $row)
    {
        $id = $row->getData('po_id');
        if ($id && $id != 0)
            return '<a href="'.$this->getUrl("adminhtml/purchase_return/edit", array("id"=> $id)).'">Edit</a>';
        else
        return false;
    }
}
