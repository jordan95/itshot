<?php

class OpenTechiz_Purchase_Block_Adminhtml_Return_Renderer_PoId extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $po_id = $row->getData('po_id');
        if($po_id == 0 || !$po_id){
            return '(Adempier imported data)';
        }else{
            return $po_id;
        }
    }
}