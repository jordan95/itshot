<?php

class OpenTechiz_Purchase_Block_Adminhtml_Return_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'opentechiz_purchase';
        $this->_controller = 'adminhtml_return';
        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('opentechiz_purchase')->__('Save'));
        $this->_removeButton('delete');
    }

    public function getHeaderText()
    {
        Mage::helper('opentechiz_purchase')->__('Add/Edit PO Return');
    }
}