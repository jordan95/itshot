<?php

class OpenTechiz_Purchase_Block_Adminhtml_Return_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('purchaseReturnGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('opentechiz_purchase/poreturn')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('ID #'),
            'index'     => 'id',
            'align'     => 'center',
            'width'     => '10'
        ));

        $this->addColumn('po_id', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Purchase Order #'),
            'index'     => 'po_id',
            'align'     => 'center',
            'width'     => '50',
            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_Return_Renderer_PoId'
        ));

        $this->addColumn('item', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Returned Items'),
            'index'     => 'item',
            'width'     => '300',
            'renderer'  => 'OpenTechiz_Purchase_Block_Adminhtml_Return_Renderer_ReturnItem'
        ));

        $this->addColumn('total', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Total'),
            'index'     => 'total',
            'align'     => 'center',
            'width'     => '50'
        ));

        $this->addColumn('reason', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Reason'),
            'index'     => 'reason',
            'align'     => 'center',
            'width'     => '50',
            'type'      => 'options',
            'options'   => Mage::helper('opentechiz_purchase')->getPurchaseReturnReson()
        ));

        $this->addColumn('note', array(
            'header'    => Mage::helper('opentechiz_purchase')->__('Note'),
            'index'     => 'note',
            'width'     => '200'
        ));

        $this->addColumn('action',
            array(
                'header' => Mage::helper('opentechiz_production')->__('Action'),
                'width' => '50',
                'type' => 'action',
                'getter' => 'getPoId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('opentechiz_production')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id',
                        'data-column' => 'action',
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
                'align' => 'center',
                'renderer' => 'OpenTechiz_Purchase_Block_Adminhtml_Return_Renderer_Action'
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getPoId()));
    }
}