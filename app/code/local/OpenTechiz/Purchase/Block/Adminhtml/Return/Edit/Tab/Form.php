<?php

class OpenTechiz_Purchase_Block_Adminhtml_Return_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();

        $po_id = $this->getRequest()->getParam('id');
        $purchaseOrder = Mage::getModel('opentechiz_purchase/purchase')->load($po_id);
        $supplierId = $purchaseOrder->getSupplierId();

        $supplier = Mage::getModel('opentechiz_purchase/supplier')->load($supplierId);
        $supplierName = $supplier->getName();

        $fieldset = $form->addFieldset('purchase_return_form', array('legend'=>Mage::helper('opentechiz_purchase')->__('General')));

        $supplierData = $supplierName;

        if($supplier->getContact()) {
            $supplierData .= '<br>'.$supplier->getContact();
        }

        if($supplier->getTelephone() && $supplier->getEmail()) {
            $supplierData .= '<br>'.$supplier->getTelephone() . ' - ' . $supplier->getEmail();
        }elseif($supplier->getTelephone()){
            $supplierData .= '<br>'.$supplier->getTelephone();
        }elseif($supplier->getEmail()){
            $supplierData .= '<br>'.$supplier->getEmail();
        }

        if($supplier->getWebsite()) {
            $supplierData .= '<br>'.'<a href="'.$supplier->getWebsite().'">'. $supplier->getWebsite() .'</a>';
        }

        $fieldset->addField('supplier_name', 'note', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Supplier'),
            'text'      => $supplierData
        ));

        $fieldset->addField('supplier_id', 'hidden', array(
            'name'      => 'supplier_id',
        ));

        $fieldset->addField('total', 'text', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Total'),
            'name'      => 'total',
        ));

        $fieldset->addField('reason', 'select', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Return reason'),
            'name'      => 'reason',
            'type'      => 'options',
            'options'   => Mage::helper('opentechiz_purchase')->getPurchaseReturnReson(),
        ));

        $fieldset->addField('note', 'textarea', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Note'),
            'name'      => 'note',
        ));

        $fieldset->addField('item_info', 'note', array(
            'label'     => Mage::helper('opentechiz_purchase')->__('Items'),
            'text'      => $this->getLayout()->createBlock('adminhtml/template')->setTemplate('opentechiz/purchase/return_product.phtml')->toHtml()

        ));

        return parent::_prepareForm();
    }
}