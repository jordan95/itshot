<?php

class OpenTechiz_Purchase_Block_Adminhtml_Return_Renderer_ReturnItem extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $items = $row->getData('item');
        $po_id = $row->getData('po_id');
        $html = '';
        $html .= '<table>
                  <tr>
                    <th class="item"  style="text-align: center;width: 60%">SKU</th>
                    <th class="qty">Qty</th>
                    <th class="total">Total</th>
                  </tr>';

        $items = unserialize($items);
        foreach ($items as $item) {
            $sku = $item['sku'];
            $qty = $item['return_qty'];
            if(isset($item['total'])){
                $price = $item['total'];
            }else {
                $price = Mage::getModel('opentechiz_purchase/orderitem')->load($po_id, 'po_id')->getPrice();
            }
            $html .= '<tr>
                        <th class="item" style="width: 60%">' . Mage::helper('opentechiz_purchase')->getTagSku($sku) . '</th>
                        <th class="qty">' . $qty . '</th>
                        <th class="total">' . (int)$qty * (float)$price . '</th>
                      </tr>';
        }
        $html .= '</table>';

        $html .= '<style>
    .item {
    border-right: solid lightgrey 1px;
    border-bottom: solid lightgrey 1px;
    }
    .qty {
    border-right: solid lightgrey 1px;
    border-bottom: solid lightgrey 1px;
    text-align: center;
    }
    .total {
    border-bottom: solid lightgrey 1px;
    text-align: center;
    }
</style>';
        return $html;
    }
}