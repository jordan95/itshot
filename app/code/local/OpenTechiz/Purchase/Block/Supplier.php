<?php

class OpenTechiz_Purchase_Block_Supplier extends Mage_Core_Block_Template
{

    protected $_collection = null;
    protected $totalPage = null;

    public function getSupplierId()
    {
        return $this->getSupplier()->getId();
    }

    public function getCollection()
    {
        if ($this->_collection === null) {
            $filter = $this->_getSession()->getFilterData();
            $resource = Mage::getSingleton('core/resource');
            $orderitem = $resource->getTableName('opentechiz_purchase/orderitem');
            $instock = $resource->getTableName('opentechiz_inventory/instock');
            $productsupplier = $resource->getTableName('opentechiz_purchase/productsupplier');
            $po = $resource->getTableName('opentechiz_purchase/purchase');
            $collection = Mage::getModel('opentechiz_purchase/purchase')->getCollection();
            $collection->addFieldToFilter('supplier_id', $this->getSupplierId());
            $select = $collection->getSelect();
            if (!empty($filter['po_increment_id'])) {
                $collection->addFieldToFilter('po_increment_id', $filter['po_increment_id']);
            }
            if (!empty($filter['order_date_from'])) {
                $date = Mage::getModel('core/date')->gmtDate('Y-m-d',$filter['order_date_from']);
                $collection->addFieldToFilter('created_at', array('gteq' => $date));
            }
            if (!empty($filter['order_date_to'])) {
                $date = Mage::getModel('core/date')->gmtDate('Y-m-d',$filter['order_date_to']);
                $collection->addFieldToFilter('created_at', array('lteq' => $date .' 23:59:59'));
            }

            if (!empty($filter['delivery_date_from'])) {
                $date = Mage::getModel('core/date')->gmtDate('Y-m-d',$filter['delivery_date_from']);
                $collection->addFieldToFilter('delivery_date', array('gteq' => $date));
            }
            if (!empty($filter['delivery_date_to'])) {
                $date = Mage::getModel('core/date')->gmtDate('Y-m-d',$filter['delivery_date_to']);
                $collection->addFieldToFilter('delivery_date', array('lteq' => $date .' 23:59:59'));
            }

            $orderitemJoined = false;
            if (!empty($filter['supplier_sku'])) {
                $select->join(array('orderitem' => $orderitem), 'main_table.po_id = orderitem.po_id', array());
                $select->join(array('productsupplier' => $productsupplier), 'orderitem.product_id = productsupplier.product_id', array());
                $select->where("productsupplier.sup_sku LIKE ?", '%' .$filter['supplier_sku'] . '%');
                $select->group('main_table.po_id');
                $orderitemJoined = true;
            }
            
            if (!empty($filter['itshot_sku'])) {
                if (!$orderitemJoined) {
                    $select->join(array('orderitem' => $orderitem), 'main_table.po_id = orderitem.po_id', array());
                    $select->group('main_table.po_id');
                }
                $select->join(array('instock' => $instock), 'orderitem.sku = instock.sku', array());
                $select->where("instock.tag_sku LIKE ?", '%' .$filter['itshot_sku'] . '%');
            }
            if (isset($filter['status']) && $filter['status'] != "") {
                $collection->addFieldToFilter('status', $filter['status']);
            } else {
                $showComplete = $this->_getSession()->getShowComplete();
                if (!$showComplete) {
                    $collection->addFieldToFilter('status', array('neq' => 1));
                } else {
                    $collection->addFieldToFilter('status', array('eq' => 1));
                }
            }

            // print_r($collection->getSelect()->__toString());
            $orderBy = $this->_getSession()->getOrderBy();
            $orderDir = $this->_getSession()->getOrderDir();
            $select->order("main_table.{$orderBy} {$orderDir}");
            $select->limit($this->_getSession()->getLimit(), ($this->getCurrentPage() - 1) * $this->getLimit());
            $this->_collection = $collection;
        }

        return $this->_collection;
    }

    public function getFilterUrl($q = array())
    {
        $filter = $this->_getSession()->getFilterData();
        $params = array();
        if ($filter) {
            $filter = base64_encode(http_build_query($filter));
            $params = array('filter' => $filter);
        }
        $_query = array();
        $_query['page'] = $this->_getSession()->getPage();
        if (isset($q['page'])) {
            $_query['page'] = $q['page'];
        }
        $_query['limit'] = $this->_getSession()->getLimit();
        if (isset($q['limit'])) {
            $_query['limit'] = $q['limit'];
        }
        $_query['order_by'] = $this->_getSession()->getOrderBy();
        if (isset($q['order_by'])) {
            $_query['order_by'] = $q['order_by'];
        }
        $_query['order_dir'] = $this->_getSession()->getOrderDir();
        if (isset($q['order_dir'])) {
            $_query['order_dir'] = $q['order_dir'];
        }
        if (isset($q['show_complete'])) {
            $_query['show_complete'] = $q['show_complete'];
        }
        if ($_query) {
            $params['_query'] = $_query;
        }
        return $this->getUrl('*/*/', $params);
    }

    public function canShowCompleted()
    {
        $filter = $this->_getSession()->getFilterData();
        return !$filter || ($filter && empty($filter['status']));
    }

    public function IsShowCompleted()
    {
        return $this->_getSession()->getShowComplete();
    }

    public function getShowCompleteUrl()
    {
        return $this->getFilterUrl(array(
                    'show_complete' => !$this->IsShowCompleted()
        ));
    }

    public function getFilterData()
    {
        return $this->_getSession()->getFilterData();
    }

    public function getTotalPage()
    {
        if ($this->totalPage === null) {
            if ($this->getLimit() <= 0) {
                return false;
            }
            $this->totalPage = ceil($this->getTotalRowsFound() / $this->getLimit());
        }
        return $this->totalPage;
    }

    public function getTotalRowsFound()
    {
        return $this->getCollection()->getSize();
    }

    public function getLimit()
    {
        return $this->_getSession()->getLimit();
    }

    public function getCurrentPage()
    {
        return $this->_getSession()->getPage();
    }

    public function getName()
    {
        return $this->getSupplier()->getName();
    }

    public function getSupplier()
    {
        return $this->_getSession()->getSupplier();
    }

    /**
     * 
     * @return OpenTechiz_Purchase_Model_Supplier_Session
     */
    public function _getSession()
    {
        return Mage::getSingleton('opentechiz_purchase/supplier_session');
    }

}
