<?php

class OpenTechiz_Purchase_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * list status for purchase order
     */
    const PO_STATUS_WAITING_FOR_SUPPLIER = 0;
    const PO_STATUS_COMPLETE = 1;
    const PO_STATUS_WAITING_FOR_DELIVERY = 2;
    const PO_STATUS_CANCEL = 3;
    const PO_STATUS_CLOSED = 4;
    const PO_STATUS_DRAFTED = 5;
    /**
     * list type for purchase order
     */
    const PO_TYPE_GOLD = 0;
    const PO_TYPE_STONE = 1;
    const PO_TYPE_PRODUCT = 2;
    /**
     * list reason for purchase order return reason
     */
    const PO_RETURN_REASON_MISTAKE = 0;
    const PO_RETURN_REASON_DEFECTIVE_REPAIR = 1;
    const PO_RETURN_REASON_DEFECTIVE_REFUND = 2;
    const PO_RETURN_REASON_RETURN_TO_VENDOR = 3;
    const PO_RETURN_REASON_REFUND = 4;
    const PO_RETURN_REASON_EXCHANGE = 5;

    const PO_NOT_PAID = 0;
    const PO_IS_PAID = 1;

    const PO_EMAIL_PENDING = 1; // default value when add the master PO, create new PO
    const PO_EMAIL_PROCESSED = 0; //status when send email for pendding PO, it confirmed email send success

    const SUP_NOTIFIED = 1;

    const SUP_NOT_ACTIVE_PORTAL = 0;
    const SUP_IS_ACTIVE_PORTAL = 1;

    const PO_SOURCE_AUTO = 1;
    const PO_SOURCE_MANUAL = 2;

    protected $_tagSkuMappings;
    protected $_tagOptionOrders;
    protected $_productOptions = [];
    protected $_isMetalAdded;
    protected $_metalAttribute;

    public function getPoPaidStatus($key = ''){
        $listStatus = [
            self::PO_NOT_PAID => $this->__('No'),
            self::PO_IS_PAID => $this->__('Yes')
        ];

        if(isset($listStatus[$key]))
            return $listStatus[$key];
        return $listStatus;
    }

    public function getActivePortalSupplier(){
        return [
            self::SUP_NOT_ACTIVE_PORTAL => $this->__('No'),
            self::SUP_IS_ACTIVE_PORTAL => $this->__('Yes')
        ];
    }

    public function getPurchaseOrderStatus($key = '',$exclude = array()){
        $listStatus = [
            self::PO_STATUS_WAITING_FOR_SUPPLIER => $this->__('Waiting for Supplier Confirmation'),
            self::PO_STATUS_WAITING_FOR_DELIVERY => $this->__('Waiting for Delivery'),
            self::PO_STATUS_COMPLETE => $this->__('Complete'),
            self::PO_STATUS_CANCEL => $this->__('Cancelled'),
            self::PO_STATUS_CLOSED => $this->__('Closed'),
            self::PO_STATUS_DRAFTED => $this->__('Drafted')
        ];

        if (!empty($exclude)) {
            foreach($exclude as $value)
            {
                if(isset($listStatus[$value]))
                    unset($listStatus[$value]);
            }
        }

        if(isset($listStatus[$key]))
            return $listStatus[$key];
        return $listStatus;
    }

    public function getPurchaseOrderType($key = ''){
        $array = [
            self::PO_TYPE_GOLD => $this->__('Gold'),
            self::PO_TYPE_STONE => $this->__('Stone'),
            self::PO_TYPE_PRODUCT => $this->__('Product')
        ];

        if(isset($array[$key]))
            return $array[$key];
        return $array;
    }

    public function getPurchaseReturnReson($key = ''){
        $array = [
            self::PO_RETURN_REASON_MISTAKE => $this->__('Sent by mistake'),
            self::PO_RETURN_REASON_DEFECTIVE_REPAIR => $this->__('Is defective, Send back for repair'),
            self::PO_RETURN_REASON_DEFECTIVE_REFUND => $this->__('Is defective, Send back for refund'),
            self::PO_RETURN_REASON_RETURN_TO_VENDOR => $this->__('Return to Vendor'),
            self::PO_RETURN_REASON_REFUND => $this->__('Refund'),
            self::PO_RETURN_REASON_EXCHANGE => $this->__('Exchange')
        ];

        if(isset($array[$key]))
            return $array[$key];
        return $array;
    }

    public function PopulatePurchaseOrder($old_data,$po_item){

        # code...
        if(isset($old_data['delivered_qty']))
            $increaseQty = $po_item->getDeliveredQty() - $old_data['delivered_qty'];
        else
            $increaseQty = $po_item->getDeliveredQty();
        if($increaseQty > 0){
            $data = array();
            $data['sku'] = $po_item->getSku();
            $data['options'] = $po_item->getOption();
            $options = unserialize($data['options']);
            $product_id = $po_item->getProductId();;
            $product = Mage::getModel('catalog/product')->load($product_id);
            $instock_product = Mage::getModel('opentechiz_inventory/instock');
            $instock = $instock_product->getCollection()->addFieldtoFilter('sku',$po_item->getSku())->addFieldtoFilter('product_id',$product_id)->getFirstItem();

            $data['name'] = $product->getName();

            $data['type'] = 1;
            $data['state'] = 35;

            for ($i=0; $i < $increaseQty  ; $i++) {
                # code...
                $item_model = Mage::getModel('opentechiz_inventory/item');
                $product_data = $options;
                if(!isset($data['image']))
                    $data['image'] = $item_model->imageUrlFromOption($product_data, $product);
                $data['cost'] = $po_item->getPrice();
                $item_model->setData($data);
                $item_model->setPrice($po_item->getPrice());

                $item_model->setCreatedAt(now());
                $item_model->setUpdatedAt(now());
                $item_model->save();

                $item_model->setBarcode($item_model->generateBarcode($item_model->getId()))->save();

                $poitem_model = Mage::getModel('opentechiz_purchase/poitem');
                $poitem_data = array('po_id'=>$po_item->getPoId(),'item_id'=>$item_model->getId());
                $poitem_model->setData($poitem_data)->save();
            }

            //search for order(s) and change status back to item collection
            $sku = $po_item->getSku();
            $qty = $increaseQty;
            $orderCollection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('order_stage', 2)
                ->addFieldToFilter('internal_status', array('in', [3, 4]))->setOrder('created_at', 'ASC');
            if(count($orderCollection)){
                foreach ($orderCollection as $order){
                    $delivered = false;
                    $otherItemInstock = true;
                    foreach ($order->getAllItems() as $item){
                        $orderedQty = $item->getQtyOrdered() - $item->getQtyCanceled() - $item->getQtyRefunded();
                        //check if PO delivered have enough qty for order item
                        if(($item->getSku() == $sku || strpos($sku, $item->getSku()) !== false) && $orderedQty <= $qty){
                            $delivered = true;
                            $qty = $qty - $item->getQtyOrdered();
                        }else{ //check other order items if have enough in stock
                            $instockQty = 0;
                            $instockAvailableSizes = Mage::helper('opentechiz_inventory')->getAvailableSize($item->getSku());
                            foreach ($instockAvailableSizes as $instockAvailableSize){
                                $instockQty += (int)$instockAvailableSize['qty'];
                            }
                            if($instockQty < $orderedQty){
                                $otherItemInstock = false;
                            }
                        }
                    }

                    if($delivered && $otherItemInstock){
                        Mage::helper('opentechiz_salesextend')->updateStageAndStatus($order, null, 1);
                    }
                }
            }
            //add event log stock
            $qty_before_move = $instock->getQty();
            $on_hold_before_move = $instock->getOnHold();
            if(!$instock->getId()){
                $instock = $instock_product->getCollection()->addFieldtoFilter('sku',$po_item->getSku())->addFieldtoFilter('product_id',$product_id)->getFirstItem();
                $qty_before_move = 0;
                $on_hold_before_move = 0;
            }
            Mage::dispatchEvent('opentechiz_admin_stock_product_movement_logger', array(
                'qty' => (int)$increaseQty,
                'on_hold' => '0',
                'type' => 3,
                'stock_id' =>$instock->getId(),
                'qty_before_movement' => $qty_before_move,
                'on_hold_before_movement' => $on_hold_before_move,
                'sku' => $instock->getSku(),
                'po_id' => $po_item->getPoId()
            ));
            //populate last cost
            Mage::helper('opentechiz_inventory')->updateLastCostFromPO($sku, $po_item->getPoId());
        }
    }
    public function createNewOrder()
    {
        $model = mage::getModel('opentechiz_purchase/purchase');
        $order = $model
            ->setcreated_at(now())
            ->setdelivery_date(now())
            ->setpo_increment_id($model->generateOrderNumber())
            ->save();

        return $order;
    }
    public function  getLastCostBySku($sku){
        $last_cost = Mage::helper('opentechiz_inventory')->getLastCost($sku);
        return $last_cost;
    }
    public function  getLastCostDate($sku){
        $last_cost_date = Mage::helper('opentechiz_inventory')->getLastCostDate($sku);
        $last_cost_date = date("m/d/Y", strtotime($last_cost_date));
        return $last_cost_date;
    }

    public function getAllSupplier(){
        $results = [];
        $collection = Mage::getModel('opentechiz_purchase/supplier')->getCollection()->setOrder('name', 'ASC');
        foreach ($collection as $supplier){
            $results[$supplier->getId()] = $supplier->getName();
        }
        return $results;
    }

    public function getAllSupplierGrid(){
        $results = [];
        $collection = Mage::getModel('opentechiz_purchase/supplier')->getCollection();
        foreach ($collection as $supplier){
            array_push($results, array('label' => $supplier->getName(), 'value' => $supplier->getId()));
        }

        return $results;
    }

    public function sendMail($po, $action){
        if(Mage::registry('currentPOId')){
            Mage::unregister('currentPOId');
        }
        Mage::register('currentPOId', $po->getId());
        if($action == 'edit' && $po->getIsSupplierNotified()){
            $message = 'Purchase Order #'.$po->getPoIncrementId().' has been updated.';
        }else{
            $message = 'Purchase Order #'.$po->getPoIncrementId().' has been created.';
        }

        $senderName = Mage::getStoreConfig('opentechiz_purchase/general/sender_name');
        $senderEmail = Mage::getStoreConfig('opentechiz_purchase/general/sender_email');

        // Getting recipient E-Mail
        $supplier = Mage::getModel('opentechiz_purchase/supplier')->load($po->getSupplierId());
        $recipientEmail = $supplier->getEmail();
        $listEmail = array();
        $recipientEmail = preg_split("/[\s,;]+/", $recipientEmail);
        if(!empty($recipientEmail)){
            foreach($recipientEmail as $k => $val){
                if (strpos($val, '@') !== false) {
                    $listEmail[] = trim($val);
                }
            }
        }
        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('ot_po_notify_create');
        $subject  ="Purchase Order No - ".$po->getPoIncrementId();
        $emailTemplate->setTemplateSubject($subject);

        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $poItemCollection = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()->addFieldToFilter('po_id', $po->getId());
        $count = 0;
        foreach ($poItemCollection as $item){
            $count += $item->getQty();
        }
        $note = $po->getNote();
        if($note == ''){
            $note = 'None';
        }
        $emailTemplateVariables = [];
        $emailTemplateVariables['message'] = $message;
        $emailTemplateVariables['note'] = $note;
        $emailTemplateVariables['total'] = Mage::helper('core')->currency($po->getTotalIncludeTax(), true, false);
        $emailTemplateVariables['total_number_item'] = $count;
        $emailTemplateVariables['delivery_date'] = explode(' ', $po->getDeliveryDate())[0];
        $emailTemplateVariables['delivery_date'] = Mage::helper('core')->formatDate($emailTemplateVariables['delivery_date']);
        $emailTemplateVariables['status'] = $this->getPurchaseOrderStatus($po->getStatus());
        if(!empty($listEmail)){
            $emailTemplate->send($listEmail, 'Itshot Admin', $emailTemplateVariables);
        }
    }

    public function isAllItemDelivered($poId){
        $items = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()->addFieldToFilter('po_id', $poId);
        if(!count($items)){
            return false;
        }
        $delivered = true;
        foreach ($items as $item){
            if($item->getQty() > $item->getDeliveredQty()){
                $delivered = false;
                break;
            }
        }
        return $delivered;
    }

    public function isCreatedFromErpProduct(){
        $params = Mage::app()->getRequest()->getParams();
        if(array_key_exists('productIds',$params)){
            return true;
        }
        return false;
    }

    public function getPO(){
        $poId = Mage::registry('currentPOId');
        if($poId){
            $this->_currentPurchase = Mage::getModel('opentechiz_purchase/purchase')->load($poId);
            Mage::unregister('currentPOId');
            return $this->_currentPurchase;
        }else return null;
    }

    public function getOrderItems(){
        $po = $this->getPO();
        $po_id = $po->getId();
        $supplier_id = $po->getSupplierId();
        $html = '';
        $supplier_code = '';
        if ($po_id) {
            $orderItem = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()->addFieldToFilter('po_id',$po_id);
            foreach ($orderItem as $item) {
                # code...
                $option = $item->getOption();
                $sku = $item->getSku();
                $product_id = $item->getProductId();
                $product = Mage::getModel('catalog/product')->load($product_id);
                $option_ar = array();
                $name = $product->getName();
                $option_item_ar = unserialize($option);
                if(isset($option_item_ar['options']))
                    $option_ar = $option_item_ar['options'];
                $qty = $item->getQty();
                $delivered_qty = $item->getDeliveredQty();
                $note = $item->getNote();
                $sku_id = str_replace('.', '', $sku);
                $id = $product_id.'_'.$sku_id;
                $collection = Mage::getModel('opentechiz_purchase/productsupplier')->getCollection()->addFieldToFilter('product_id',$product_id)->addFieldToFilter('sup_id',$supplier_id);
                if (is_array($collection->getData()) || is_object($collection->getData())){
                    foreach($collection->getData() as $k => $val){
                        $supplier_code = $val['sup_sku'];
                    }
                }
                $product->setSku($sku);
                $sku = Mage::helper('opentechiz_purchase')->getTagSku($sku);
                $imageUrl = Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(120, 120);
                $html.="<tr id=\"product_".$id."\" class=\"even item_purchase\"><td class=\"cell-content\"><div class=\"title\"><strong class=\"strong\" id=\"order_item_".$id."_title\">".$name."</strong>";

                $html .= "</div><div><strong class=\"strong\">SKU: </strong>".$sku."</div>";
                foreach ($option_ar as $option){
                    $html .= "<strong class=\"strong\">".$option['label'].":</strong> ".$option['value'].'<br>';
                }
                $html .= '</td>';
                $html .= "<td class=\"first cell-content\">".$supplier_code."</td>";
                $html .= "<td class=\"first cell-content\">".$qty."</td>";
                $html .= "<td class=\"first cell-content\">".$delivered_qty."</td>";
                $html .= "<td class=\"first images\">".'<img src="'.$imageUrl.'" width="120px" height="120px">'."</td>";
                $html .= "<td class=\"first note cell-content\">".$note."</td><td class=\"first\"></td></tr>";
            }
        }
        return $html;
    }

    public function getOptionHtmlItem($option,$sku=''){
        $html = "";
        $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $sku);
        if(strpos($sku, OpenTechiz_Quotation_Helper_Data::QUOTATION_PRODUCT_SKU) === false) {
            if($option) {
                $option = unserialize($option);
                $optionHtml = '';
                if (array_key_exists('options', $option)) {
                    foreach ($option['options'] as $option) {
                        if ($option['option_type'] == 'stone') {
                            $optionHtml .= '<dt>' . $option['label'] . '</dt>';
                            $stoneType = strtolower(str_replace(' ', '', $option['value']));

                            $quality = '';
                            if (strpos($stoneType, 'diamond')) {
                                $quality = str_replace('diamond', '', $stoneType);
                                if (in_array($quality, self::DIAMOND_QUALITY)) {
                                    $quality = strtoupper($quality);
                                } else $quality = '';
                            }
                            $optionHtml .= '<dd>' . OpenTechiz_Material_Helper_Data::OPPENTECHIZ_MATERIAL_STONE_TYPE[$stoneType];
                            if ($quality != '') {
                                $optionHtml .= ' - ' . $quality;
                            }
                            $optionHtml .= '</dd>';
                        } elseif ($option['option_type'] == 'gold') {
                            $optionHtml .= '<dt>' . $option['label'] . '</dt>';
                            $optionHtml .= '<dd>' . OpenTechiz_Material_Helper_Data::GOLD_NAME_FRONT_END[strtolower(str_replace(' ', '', $option['value']))] . '</dd>';
                        } else {
                            $optionHtml .= '<dt>' . $option['label'] . '</dt>';
                            $optionHtml .= '<dd>' . $option['value'] . '</dd>';
                        }
                    }
                }
                $html .= '<dl class="item-options">
                            ' . $optionHtml . '
                        </dl>';
            }
            $html .='        </div>
                </div>';
        } else if ($sku && isset($parts[1])
            && strpos($sku, OpenTechiz_Quotation_Helper_Data::QUOTATION_PRODUCT_SKU) !== false){
            $quotationProductId = $parts[1];
            $optionHtml = '';

            $optionCollection = Mage::getModel('opentechiz_quotation/productOption')->getCollection()->addFieldToFilter('quote_product_id', $quotationProductId);
            foreach ($optionCollection as $option){
                if($option->getOptionType() == 'stone') {
                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
                    $optionHtml .= '<dd>' . Mage::getModel('opentechiz_quotation/product')->getStoneName($option->getOptionValue()) . '</dd>';
                } elseif ($option->getOptionType() == 'gold'){
                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
                    $optionHtml .= '<dd>' . Mage::getModel('opentechiz_quotation/product')->getGoldName($option->getOptionValue()) . '</dd>';
                } else{
                    $optionHtml .= '<dt>' . $option->getOptionName() . '</dt>';
                    $optionHtml .= '<dd>' . $option->getOptionValue() . '</dd>';
                }
            }
            $html = '<div class="item-container">
                    <div class="item-text">
                        <dl class="item-options">
                            ' . $optionHtml . '
                        </dl>
                    </div>
                </div>';
        }
        return $html;
    }

    public function isActivePortalSupplier($supplier){
        if ($supplier->getData()) {
            if ($supplier->getData('is_active_portal') == OpenTechiz_Purchase_Helper_Data::SUP_IS_ACTIVE_PORTAL) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function getTagSkuMappings()
    {
        if ($this->_tagSkuMappings == null) {
            $tagSkuMappings = [];
            if ($maps = preg_split("/\r\n|\n|\r/", Mage::getStoreConfig('opentechiz_purchase/tag_sku/map'))) {
                foreach ($maps as $map) {
                    $values = explode(',', trim($map));
                    if (count($values) != 2) {
                        continue;
                    }
                    $tagSkuMappings[strtolower(trim($values[0]))] = trim($values[1]);
                }
            }

            $this->_tagSkuMappings = $tagSkuMappings;
        }

        return $this->_tagSkuMappings;
    }

    /**
     * @return false|Mage_Catalog_Model_Resource_Eav_Attribute
     */
    public function getMetalAttribute()
    {
        if ($this->_metalAttribute !== null) {
            return $this->_metalAttribute;
        }
        if (!$attributeCode = Mage::getStoreConfig('opentechiz_purchase/tag_sku/metal_attribute')) {
            $this->_metalAttribute = false;
        }
        $attribute = Mage::getModel('catalog/product')->getResource()->getAttribute($attributeCode);
        if ($attribute) {
            $this->_metalAttribute = $attribute;
        } else {
            $this->_metalAttribute = false;
        }
        return $this->_metalAttribute;
    }

    /**
     * @return array
     */
    public function getTagOptionOrders()
    {
        if ($this->_tagOptionOrders == null) {
            $tagOptionOrders = [];
            if ($maps = preg_split("/\r\n|\n|\r/", Mage::getStoreConfig('opentechiz_purchase/tag_sku/option_order'))) {
                foreach ($maps as $map) {
                    $values = explode(',', trim($map));
                    if (count($values) != 2) {
                        continue;
                    }
                    $tagOptionOrders[strtolower(trim($values[0]))] = (int)trim($values[1]);
                }
            }

            $this->_tagOptionOrders = $tagOptionOrders;
        }

        return $this->_tagOptionOrders;
    }

    /**
     * @param string $sku
     * @param int $productId
     * @param string $productSku
     * @return string
     */
    public function getTagSkuBySku($sku, $productId, $productSku = null)
    {
        if (!$productSku) {
            /** @var Mage_Catalog_Model_Resource_Product $resource */
            $resource = Mage::getSingleton('catalog/product')->getResource();
            $sql = $resource->getReadConnection()->select()->from($resource->getEntityTable(), ['sku'])->where('entity_id = ?', $productId);
            $productSku = $resource->getReadConnection()->fetchOne($sql);
        }
        $this->_isMetalAdded = false;
        $tagSku = '';
        $optionSkus = substr($sku, strlen($productSku));
        if (strpos($optionSkus, '_') === 0) {
            $optionSkus = ltrim($optionSkus, '_');
            $skus = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $optionSkus);

            if (count($skus) > 1) {
                $tagSku = $this->getSortedTagSku($skus, $productId);
            } else {
                $sku = array_shift($skus);
                $tagSku = $this->prepareTagSku($sku);
            }
        }

        if (!$this->_isMetalAdded) {
            if ($metalTag = $this->getMetalTag($productId)) {
                $tagSku = $metalTag . $tagSku;
            }
        }

        return $tagSku ? $productSku . '/' . $tagSku : $productSku;
    }

    /**
     * @param int $productId
     * @return false|string
     */
    protected function getMetalTag($productId)
    {
        if ($metalAttribute = $this->getMetalAttribute()) {
            /** @var Mage_Catalog_Model_Resource_Product $resource */
            $resource = Mage::getSingleton('catalog/product')->getResource();
            $metalId = $resource->getAttributeRawValue($productId, $metalAttribute, 0);
            if (!$metalId || strpos($metalId, ',')) {
                return false;
            }
            if ($metal = strtolower($metalAttribute->getFrontend()->getOption($metalId))) {
                $tagSkuMappings = $this->getTagSkuMappings();
                return isset($tagSkuMappings[$metal]) ? $tagSkuMappings[$metal] : false;
            }
        }
        return false;
    }

    /**
     * @param string $sku
     * @return string
     */
    protected function prepareTagSku($sku)
    {
        $sku = strtolower($sku);
        $sku = str_replace('(custom)', '', $sku);
        $sku = str_replace(' ', '', $sku);
        $sku = preg_replace('/(\d+\.?\d*)(in|mm)\.?/', '$1', $sku);
        $tagSkuMappings = $this->getTagSkuMappings();
        $tagSku = isset($tagSkuMappings[$sku]) ? $tagSkuMappings[$sku] : $sku;
        if (preg_match('/\d+k/', $sku) || preg_match('/SS|ST|PL/', $tagSku)) {
            $this->_isMetalAdded = true;
        }
        return $tagSku;
    }

    /**
     * @param array $skus
     * @param int $productId
     * @return string
     */
    protected function getSortedTagSku($skus, $productId)
    {
        if (!array_key_exists($productId, $this->_productOptions)) {
            /** @var Mage_Catalog_Model_Resource_Product_Option_Collection $optionsCollection */
            $optionsCollection = Mage::getModel('catalog/product_option')->getCollection()
                ->addFieldToFilter('product_id', $productId)
                ->addTitleToResult(0)
                ->setOrder('sort_order', 'asc')
                ->setOrder('title', 'asc');

            $optionsCollection->addValuesToResult();
            $options = [];
            $noOrder = 21;
            foreach ($optionsCollection as $productOption) {
                $tagOptionOrders = $this->getTagOptionOrders();
                $productOptionTitle = strtolower($productOption->getTitle());
                $tagOptionOrder = isset($tagOptionOrders[$productOptionTitle]) ? $tagOptionOrders[$productOptionTitle] : $noOrder++;
                $option = new Varien_Object();
                $values = [];
                foreach ($productOption->getValues() as $productValue) {
                    $value = new Varien_Object();
                    $value->setData('sku', $productValue->getSku());
                    $values[] = $value;
                }
                $option->setData([
                    'title' => $productOptionTitle,
                    'values' => $values,
                    'tag_option_order' => $tagOptionOrder,
                ]);

                $options[] = $option;
            }
            $this->_productOptions[$productId] = $options;
        }
        $productOptions = $this->_productOptions[$productId];
        if (!$productOptions) {
            return implode('', $skus);
        }

        $tagSkus = [];
        $maxOrder = 30;
        foreach ($skus as $sku) {
            $tagSkuOrder = $maxOrder +1;
            foreach ($productOptions as $key => $option) {
                foreach ($option->getValues() as $value) {
                    if ($value->getSku() == $sku) {
                        $tagSkuOrder = $option->getData('tag_option_order');
                        unset($productOptions[$key]);
                        break 2;
                    }
                }
            }
            if ($tagSkuOrder >= $maxOrder) {
                $maxOrder = $tagSkuOrder;
            }
            $tagSku = $this->prepareTagSku($sku);
            $tagSkus[] = ['sku' => $tagSku, 'order' => $tagSkuOrder];
        }

        usort($tagSkus, function ($a, $b) {
            if ($a['order'] == $b['order']) {
                return 0;
            }
            return ($a['order'] < $b['order']) ? -1 : 1;
        });

        $result = '';
        $additionalDelimiterAdded = false;
        foreach ($tagSkus as $tagSku) {
            if (!$additionalDelimiterAdded && $tagSku['order'] > 10) {
                $result .= '&';
                $additionalDelimiterAdded = true;
            }
            $result .= $tagSku['sku'];
        }

        return $result;
    }


    public function getListUserActive(){
        $result = array();
        $result[0] = "System";
        $roles_users = Mage::getResourceModel('admin/roles_user_collection');
        foreach($roles_users as $roleuser){
           $user = Mage::getModel('admin/user')->load($roleuser->getUserId());
           $result[$user->getId()] = $user->getName();
        }
        return $result;
    }
    
    /**
     * @param string $sku
     * @return string
     */
    public function getTagSku($sku)
    {
        $result = '';

        if ($sku) {
            $instock = Mage::getSingleton('core/resource')->getTableName('opentechiz_inventory/instock');
            $sql = "SELECT tag_sku
                    FROM {$instock}
                    WHERE sku = '{$sku}'";
            $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
            $tagSku = $conn->fetchOne($sql);
            $result = $tagSku ?: $sku;
        }

        return $result;
    }

    /**
     * @param string $tagSku
     * @return string
     */
    public function getSkuByTagSku($tagSku)
    {
        $result = '';

        if ($tagSku) {
            $instock = Mage::getSingleton('core/resource')->getTableName('opentechiz_inventory/instock');
            $sql = "SELECT sku
                    FROM {$instock}
                    WHERE tag_sku = '{$tagSku}'";
            $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
            $sku = $conn->fetchOne($sql);
            $result = $sku ?: $tagSku;
        }

        return $result;
    }
}