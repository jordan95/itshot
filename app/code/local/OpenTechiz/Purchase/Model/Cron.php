<?php
class OpenTechiz_Purchase_Model_Cron
{
    public function sendMasterPOEmail(){
        $poCollection = Mage::getModel('opentechiz_purchase/purchase')->getCollection()
            ->addFieldToFilter('is_pending_po', OpenTechiz_Purchase_Helper_Data::PO_EMAIL_PENDING)
            ->addFieldToFilter('is_supplier_notified', array('null' => true))
            ->addFieldToFilter('status', array('neq' => OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL));
        foreach ($poCollection as $purchase) {
            Mage::helper('opentechiz_purchase')->sendMail($purchase, 'create');
            $data = [];
            $data['is_supplier_notified'] = OpenTechiz_Purchase_Helper_Data::SUP_NOTIFIED;
            $data['is_pending_po'] = OpenTechiz_Purchase_Helper_Data::PO_EMAIL_PROCESSED;
            $data['notified_at'] = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $purchase->addData($data)->save(); 

            $dataLog = array();
            $dataLog['log_from'] = 'app/code/local/OpenTechiz/Purchase/Model/Cron.php';
            $dataLog['po_id'] = $purchase->getId();
            Mage::log($dataLog, null, 'cron_master_po.log', true);
        }
    }

    public function cronSendSupplierEmailNotify(){
        $collection = Mage::getModel('opentechiz_purchase/purchase')->getCollection();
        $collection->getSelect()->joinLeft(array('supplier' => 'tsht_purchase_supplier'), 'main_table.supplier_id = supplier.supplier_id', array('is_active_portal'));
        $collection->addFieldToFilter('main_table.status', OpenTechiz_Purchase_Helper_Data::PO_STATUS_WAITING_FOR_SUPPLIER);
        $collection->addFieldToFilter('main_table.confirmed', array('null' => true));
        $collection->addFieldToFilter('supplier.is_active_portal', OpenTechiz_Purchase_Helper_Data::SUP_IS_ACTIVE_PORTAL);
        $collection->addFieldToFilter('main_table.is_purchase_notified', 0);
        $collection->getSelect()->where("(`main_table`.`notified_at` + INTERVAL 2 DAY) < " . "'" . now() . "'");
        $collection->getSelect()->limit(10);
        if ($collection->getSize()) {
            foreach ($collection as $item) {
                $notified_at = $item->getNotifiedAt();
                if (strtotime($notified_at . "+2 days") < strtotime(now())) {
                    $item->sendSupplierEmailNotify();
                    $item->setIsPurchaseNotified(1);
                    $item->save();
                }
            }
        }
        return "Send a notification to the supplier success.";
    }
}