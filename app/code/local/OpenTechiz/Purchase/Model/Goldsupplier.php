<?php

class OpenTechiz_Purchase_Model_Goldsupplier extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_purchase/goldsupplier');
    }
}