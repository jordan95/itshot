<?php

class OpenTechiz_Purchase_Model_Orderitem extends Mage_Core_Model_Abstract
{
    protected $runAfterSafe = true;

    protected function _construct()
    {
        $this->_init('opentechiz_purchase/orderitem');
    }

    protected function _afterLoad()
    {
        parent::_afterLoad();
        $this->setOldData($this->getData());
    }

    public function preventAfterSave(){
        $this->runAfterSafe = false;
        return $this;
    }

    protected function _afterSave(){
        if($this->runAfterSafe) {
            $old_data = $this->getOldData();
            Mage::helper('opentechiz_purchase')->PopulatePurchaseOrder($old_data, $this);
        }
        return parent::_afterSave();
    }

    public function getPurchaseOrder(){
        if(!$this->getData('purchase_order')){
            $purchaseOrder = Mage::getModel('opentechiz_purchase/purchase')->load($this->getPoId());
            $this->setPurchaseOrder($purchaseOrder);
        }
        return $this->getData('purchase_order');
    }

    public function logDelivery(){
        $oldData = $this->getOldData();
        $newData = $this->getData();
        $data = array();

        $change_qty = (int)$this->getData('shipped_qty') - (int)$this->getOldData('shipped_qty');
        if ($change_qty) {
            $data['po_item_id'] = $this->getId();
            $data['old_shipped_qty'] = $this->getOldData('shipped_qty');
            $data['new_shipped_qty'] = $this->getData('shipped_qty');
            $data['change_qty'] = $change_qty;
            $data['created_at'] = now();
            $itemDelivery = Mage::getModel('opentechiz_purchase/itemdelivery');
            $itemDelivery->addData($data);
            $itemDelivery->save();
        }
        
        return $this;
    }
}