<?php

class OpenTechiz_Purchase_Model_Purchase extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_purchase/purchase');
    }

    protected function _beforeSave(){
        $user_id = 0;
        if (Mage::getSingleton('admin/session')->getUser()) {
            $user_id = Mage::getSingleton('admin/session')->getUser()->getId();
        }

        if (empty($this->getUserId())) {
            $this->setUserId($user_id);
        }

        return parent::_beforeSave();
    }

    public function generateOrderNumber() {
        //prefix
        $new = Mage::getModel('core/date')->date('mdY') . 'PO';
        
        //get today's PO id list
        $collection = Mage::getModel('opentechiz_purchase/purchase')
            ->getCollection()
            ->addFieldToFilter('po_increment_id', array('like' => $new.'%'));
        $existingIDs = [];
        foreach ($collection as $po){
            array_push($existingIDs, $po->getPoIncrementId());
        }

        //find next with a limit off 3000 per day
        for ($i = 1; $i < 3000; $i++) {

            //define next number
            $current = $new . $i;

            //check if exist
            if(!in_array($current, $existingIDs)){
                return $current;
            }
        }
        return $new;
    }

    public function createMasterPO($supplierId){
        $model = Mage::getModel('opentechiz_purchase/purchase');
        $data = [];
        $data['po_increment_id'] = $this->generateOrderNumber();
        $data['created_at'] = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s");
        $data['updated_at'] = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s");
        $data['supplier_id'] = $supplierId;
        $data['type'] = 2;
        $data['is_pending_po'] = OpenTechiz_Purchase_Helper_Data::PO_EMAIL_PENDING;

        //add default delivery date
        $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $currentTime = strtotime($date);
        $secondsToAdd = 3 * 7 * 24 * (60 * 60); //3 weeks
        $newTime = $currentTime + $secondsToAdd;
        $newTime = date("m/d/Y H:i:s", $newTime);
        $data['delivery_date'] = $newTime;

        $model->setData($data)->save();
        return $data['po_increment_id'];
    }

    public function addToMasterPO($params){
        $supId = $params['sup_id'];
        $masterPOCollection = $this->getCollection()->addFieldToFilter('supplier_id', $supId)
            ->addFieldToFilter('is_pending_po', OpenTechiz_Purchase_Helper_Data::PO_EMAIL_PENDING)
            ->addFieldToFilter('status', array('nin' => array(OpenTechiz_Purchase_Helper_Data::PO_STATUS_COMPLETE, OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL))) //exclude completed and canceled
            ->addFieldToFilter('source', array('neq' => OpenTechiz_Purchase_Helper_Data::PO_SOURCE_AUTO)); //exclude auto created
        if(!count($masterPOCollection)){
            $newPOId = $this->createMasterPO($supId);
            $pendingPO = $this->load($newPOId, 'po_increment_id');
        }else{
            $pendingPO = $masterPOCollection->getFirstItem();
        }

        $po_id = $pendingPO->getId();
        $product_id = $params['product_id'];
        $qty = $params['qty'];
        $note = $params['note'];

        $option_data = $params['option'];
        $selectedOptions = [];
        $keys = [];
        $serializeOption = [];
        $serializeOption['info_buyRequest'] = [];
        $serializeOption['info_buyRequest']['product'] = $product_id;
        $serializeOption['options'] = [];

        foreach ($option_data as $option) {
            $key = $option['name'];
            $value = $option['value'];
            if (!$value) continue;

            $key = str_replace(']', '', $key);
            $key = trim($key, 'options');
            $key = ltrim($key, '[');

            $ar_options = explode("[", $key);
            $selectedOptions[$key] = $value;
            array_push($keys, $key);
        }

        $product = Mage::getModel('catalog/product')->load($product_id);
        $sku = $product->getSku();
        $optionsCollection = $product->getOptions();
        // if option enabled = no && hasOptions = 0
        if (!$optionsCollection) $optionsCollection = $product->getProductOptionsCollection();

        foreach ($optionsCollection as $option) {
            if(in_array($option->getId(), $keys)){
                if($option->getType() == Mage_Catalog_Model_Product_Option::OPTION_TYPE_FIELD){
                    if(array_key_exists($option->getId(), $selectedOptions)){
                        $serializedData = [];
                        $serializedData['label'] = $option->getTitle();
                        $serializedData['value'] = $selectedOptions[$option->getId()];
                        $serializedData['option_value'] = $selectedOptions[$option->getId()];
                        $serializedData['option_id'] = $option->getId();
                        $serializedData['option_type'] = $option->getType();
                        array_push($serializeOption['options'], $serializedData);
                    }
                }else {
                    foreach ($option->getValues() as $value) {
                        if ($value->getId() == $selectedOptions[$option->getId()]) {
                            //add to sku
                            if ($value->getSku()) {
                                $sku .= OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR . $value->getSku();
                            }

                            //add to serialized options
                            $serializedData = [];
                            $serializedData['label'] = $option->getTitle();
                            $serializedData['value'] = $value->getTitle();
                            $serializedData['option_id'] = $option->getId();
                            $serializedData['option_value'] = $value->getId();
                            $serializedData['option_type'] = $option->getType();
                            array_push($serializeOption['options'], $serializedData);
                        }
                    }
                }
            }
        }
        
        $serializeOption = serialize($serializeOption);
        $saveData = [];
        $saveData['product_id'] = $product_id;
        $saveData['po_id'] = $po_id;
        $saveData['sku'] = $sku;
        $saveData['option'] = $serializeOption;
        $saveData['qty'] = $qty;
        $instockItem = mage::getModel('opentechiz_inventory/instock')->load($sku, 'sku');
        if($instockItem) {
            $saveData['price'] = $instockItem->getLastCost();
        }
        $saveData['note'] = $note;

        //check if item with same sku already exist in PO
        $itemCollection = mage::getModel('opentechiz_purchase/orderitem')->getCollection()
            ->addFieldToFilter('sku', $sku)->addFieldToFilter('po_id', $po_id);
        if(count($itemCollection)){
            $item = $itemCollection->getFirstItem();
            //show error when user try to set qty < delivered qty
            if($item->getDeliveredQty() > $saveData['qty']){
                return 'Error: PO already have Delivered QTY = '.$item->getDeliveredQty().'. QTY cannot be '.$saveData['qty'];
            }
            $item->preventAfterSave();
            $item->addData($saveData)->save();
        }else{
            mage::getModel('opentechiz_purchase/orderitem')->setData($saveData)->save();
        }

        //update PO total
        $itemCollection = mage::getModel('opentechiz_purchase/orderitem')->getCollection()->addFieldToFilter('po_id', $po_id);
        $total = 0;
        foreach ($itemCollection as $item){
            $total += $item->getPrice()*$item->getQty();
        }
        $this->load($po_id)->setTotalIncludeTax($total)->save();

        //return PO id
        return $po_id;
    }

    public function getInstockQtyByProductId($productId){
        $stock = [];
        $inventoryCollection = Mage::getModel('opentechiz_inventory/instock')->getCollection()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('sku', array('nlike' => '\_%'));
        foreach ($inventoryCollection as $instock){
            if(isset($stock[$instock->getGroupSku()])){
                $stock[$instock->getGroupSku()] += $instock->getQty();
            }else{
                $stock[$instock->getGroupSku()] = $instock->getQty();
            }
        }

        return $stock;
    }

    public function getQtyInPOByProductId($productId){
        $results = [];
        $arrayStatusToExclude = [
            OpenTechiz_Purchase_Helper_Data::PO_STATUS_CANCEL,
            OpenTechiz_Purchase_Helper_Data::PO_STATUS_CLOSED,
            OpenTechiz_Purchase_Helper_Data::PO_STATUS_DRAFTED,
        ];
        $itemsCollection = Mage::getModel('opentechiz_purchase/orderitem')->getCollection()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('qty', array ('gt' => new Zend_Db_Expr('delivered_qty') ))
            ->join(array('po' => 'opentechiz_purchase/purchase'), 'main_table.po_id = po.po_id', '*')
            ->addFieldToFilter('status', array('nin' => $arrayStatusToExclude))
            ->join(array('supplier' => 'opentechiz_purchase/supplier'), 'po.supplier_id = supplier.supplier_id', '*');
        
        foreach ($itemsCollection as $item) {
            if (isset($results[$item->getSku()])){
                if(isset($results[$item->getSku()][$item->getPoIncrementId()])) {
                    $results[$item->getSku()][$item->getPoIncrementId()] .= '<br>'.  '('.$item->getDeliveredQty() . '/' . $item->getQty().')';
                }else{
                    $results[$item->getSku()][$item->getPoIncrementId()] =  '('.$item->getDeliveredQty() . '/' . $item->getQty().')';
                }
            }else{
                $results[$item->getSku()] = [];
                $results[$item->getSku()][$item->getPoIncrementId()] =  '('.$item->getDeliveredQty() . '/' . $item->getQty().')';
            }
        }

        return $results;
    }

    public function validAndUpdateStatus(){
        $allItemDelivered = Mage::helper('opentechiz_purchase')->isAllItemDelivered($this->getId());
        if ($allItemDelivered) {
            $this->setDeliveryDate(now());
            $this->setStatus(OpenTechiz_Purchase_Helper_Data::PO_STATUS_COMPLETE);
            $this->setUpdatedAt(now());
            $this->save();
        }
        return $this;
    }

    public function sendSupplierEmailNotify(){
        if(Mage::registry('currentPOId')){
            Mage::unregister('currentPOId');
        }
        Mage::register('currentPOId', $this->getId());

        $supplier = Mage::getModel('opentechiz_purchase/supplier')->load($this->getSupplierId());
        $po_increment_id = $this->getPoIncrementId();

        $senderName = Mage::getStoreConfig('opentechiz_purchase/general/sender_name');
        $senderEmail = Mage::getStoreConfig('opentechiz_purchase/general/sender_email');

        $subject = $supplier->getName() . " supplier didn't checked " .$po_increment_id. " within 48 hours";
        $message ='<p>The "Confirmed" checkmark for the item on PO '.$po_increment_id.' is not checked within 48 hours after the supplier was notified.</p>';

        $emailTemplate = Mage::getModel('core/email_template');
        $emailTemplate->loadDefault('ot_po_notify_confirm');
        $emailTemplate->setTemplateSubject($subject);
        $emailTemplate->setSenderName($senderName);
        $emailTemplate->setSenderEmail($senderEmail);

        $emailTemplateVariables = [];
        $emailTemplateVariables['message'] = $message;

        $listEmail = array();
        // get all email from Supplier
        $supplierEmails = preg_split("/[\s,;]+/", $supplier->getEmail());
        if(!empty($supplierEmails)){
            foreach($supplierEmails as $k => $val){
                if (strpos($val, '@') !== false) {
                    $listEmail[] = trim($val);
                }
            }
        }

        // get all email from Admin
        $recipientEmail = Mage::getStoreConfig('opentechiz_reportExtend/general/confirmed_not_check');
        $recipientEmail = preg_split("/[\s,;]+/", $recipientEmail);
        if(!empty($recipientEmail)){
            foreach($recipientEmail as $k => $val){
                if (strpos($val, '@') !== false) {
                    $listEmail[] = trim($val);
                }
            }
        }

        if(!empty($listEmail)){
            $emailTemplate->send($listEmail, 'Itshot Admin', $emailTemplateVariables);
        }
        return $this;
    }


}