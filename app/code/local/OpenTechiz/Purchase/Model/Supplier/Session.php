<?php

class OpenTechiz_Purchase_Model_Supplier_Session extends Mage_Core_Model_Session
{

    protected $_isSupplierIdChecked = null;
    
    public function __construct()
    {
        $this->init('supplier');
    }

    public function isLoggedIn()
    {  
         return (bool)$this->getId() && (bool)$this->checkSupplierId($this->getId());
    }
    
     public function checkSupplierId($supplierId)
    {
        if ($this->_isSupplierIdChecked === null) {
            $this->_isSupplierIdChecked = Mage::getResourceSingleton('opentechiz_purchase/supplier')->checkSupplierId($supplierId);
        }
        return $this->_isSupplierIdChecked;
    }
    
     public function logout()
    {
        if ($this->isLoggedIn()) {
            $this->_logout();
        }
        return $this;
    }
    
    protected function _logout()
    {
        $this->setId(null);
        $this->getCookie()->delete($this->getSessionName());
        Mage::getSingleton('core/session')->renewFormKey();
        return $this;
    }

    public function login($username, $password)
    {
        $supplier = Mage::getModel('opentechiz_purchase/supplier');

        if ($supplier->authenticate($username, $password)) {
            $this->setSupplierAsLoggedIn($supplier);
            return true;
        }
        return false;
    }

    public function setSupplierAsLoggedIn($supplier)
    {
        $this->setId($supplier->getId());
        $this->setSupplier($supplier);
        $this->renewSession();
        Mage::getSingleton('core/session')->renewFormKey();
        Mage::dispatchEvent('supplier_login', array('supplier' => $supplier));
        return $this;
    }

    public function renewSession()
    {
        parent::renewSession();
        Mage::getSingleton('core/session')->unsSessionHosts();

        return $this;
    }

}
