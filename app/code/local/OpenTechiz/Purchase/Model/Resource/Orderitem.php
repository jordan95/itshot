<?php

class OpenTechiz_Purchase_Model_Resource_Orderitem extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_purchase/orderitem', 'id');
    }
}