<?php

class OpenTechiz_Purchase_Model_Resource_Purchase_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_purchase/purchase');
    }
    public function getLatePurchasePerSupplier($supplier_id)
    {
    	$table_purchase_supplier = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/supplier');
        $currentDate = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $collection = $this->addFieldToFilter('main_table.delivery_date', array(
		        'to' => $currentDate
		    ))->addFieldToFilter('main_table.status', array(
		        'nin' => array(1,3,4)
		    ))->addFieldToFilter('supplier.supplier_id', array(
                'eq' => $supplier_id
            ));
            $collection->getSelect()->join(array('supplier' => $table_purchase_supplier), 'supplier.supplier_id=main_table.supplier_id',array("main_table.supplier_id","supplier.email"));
            $collection->getSelect()->columns('DATEDIFF(NOW(),main_table.delivery_date) as day_late')->order(array('day_late desc', 'main_table.po_id asc'));
        return $collection;
    }
    public function hasLatePurchasePerSupplier($supplier_id)
    {
    	$table_purchase_supplier = Mage::getSingleton('core/resource')->getTableName('opentechiz_purchase/supplier');
        $currentDate = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $collection = $this->addFieldToFilter('main_table.delivery_date', array(
		        'to' => $currentDate
		    ))->addFieldToFilter('main_table.status', array(
		        'nin' => array(1,3,4)
		    ))->addFieldToFilter('supplier.supplier_id', array(
                'eq' => $supplier_id
            ));
            $collection->getSelect()->join(array('supplier' => $table_purchase_supplier), 'supplier.supplier_id=main_table.supplier_id',array("main_table.supplier_id","supplier.email","supplier.name"));
            $collection->getSelect()->columns('DATEDIFF(NOW(),main_table.delivery_date) as day_late')->order(array('day_late desc', 'main_table.po_id asc'))->limit(1);
            //echo $collection->getSelect()->__toString();die('erer');
        return $collection;
    }

    public function getSelectCountSql()
    {
        $this->_renderFilters();

        $countSelect = clone $this->getSelect();
        $countSelect->reset(Zend_Db_Select::ORDER);
        $countSelect->reset(Zend_Db_Select::LIMIT_COUNT);
        $countSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
        $countSelect->reset(Zend_Db_Select::COLUMNS);

        if (count($this->getSelect()->getPart(Zend_Db_Select::GROUP)) > 0) {
            $countSelect->reset(Zend_Db_Select::GROUP);
            $countSelect->distinct(true);
            $group = $this->getSelect()->getPart(Zend_Db_Select::GROUP);
            $countSelect->columns("COUNT(DISTINCT ".implode(", ", $group).")");
        } else {
            $countSelect->columns('COUNT(*)');
        }
        return $countSelect;
    }
}