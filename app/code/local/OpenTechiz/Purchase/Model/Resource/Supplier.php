<?php

class OpenTechiz_Purchase_Model_Resource_Supplier extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('opentechiz_purchase/supplier', 'supplier_id');
    }

    public function loadSupplierIDByUsername($username)
    {
        $adapter = $this->_getReadAdapter();
        $bind = array('username' => $username);
        $select = $adapter->select()
                ->from($this->getTable('opentechiz_purchase/supplier'), array("*"))
                ->where('username = :username');
        return $adapter->fetchOne($select, $bind);
    }

    public function loadByUsername(OpenTechiz_Purchase_Model_Supplier $supplier, $username)
    {
        $supplierId = $this->loadSupplierIDByUsername($username);
        if ($supplierId) {
            $this->load($supplier, $supplierId);
        } else {
            $supplier->setData(array());
        }

        return $this;
    }

    public function checkSupplierId($supplierId)
    {
        $adapter = $this->_getReadAdapter();
        $bind = array('supplier_id' => (int) $supplierId);
        $select = $adapter->select()
                ->from($this->getTable('opentechiz_purchase/supplier'), 'supplier_id')
                ->where('supplier_id = :supplier_id')
                ->limit(1);

        $result = $adapter->fetchOne($select, $bind);
        if ($result) {
            return true;
        }
        return false;
    }

}
