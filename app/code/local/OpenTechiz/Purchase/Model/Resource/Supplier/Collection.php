<?php

class OpenTechiz_Purchase_Model_Resource_Supplier_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_purchase/supplier');
    }

    public function joinCountSupplier()
    {
        $select = $this->getSelect();
        $table = new Zend_Db_Expr("(select sum(weight_usage) as total_weight_usage, gold_casting_code from ". $this->getTable('opentechiz_production/goldusage') ." group by gold_casting_code)");
        $select->joinLeft(array('usage'=>$table), "main_table.gold_casting_code = usage.gold_casting_code", array('weight_usage'=>"usage.total_weight_usage"));

        return $this;
    }
}