<?php

class OpenTechiz_Purchase_Model_Resource_Poitem extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_purchase/poitem', 'id');
    }
}