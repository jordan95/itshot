<?php

class OpenTechiz_Purchase_Model_Resource_Materialreceiptitem_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_purchase/materialreceiptitem');
    }
}