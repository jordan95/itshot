<?php

class OpenTechiz_Purchase_Model_Purchasestone extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('opentechiz_purchase/purchasestone');
    }

    protected function _afterLoad()
    {
        parent::_afterLoad();
        $this->setOldData($this->getData());

    }
    protected function _afterSave(){
        parent::_afterSave();
        $old_data = $this->getOldData();
        if($old_data['delivered_qty'] != $this->getData('delivered_qty') ){
        	$change = $this->getData('delivered_qty') - $old_data['delivered_qty'];
        	$stone_id = $this->getData('stone_id');
        	$stone =  Mage::getModel('opentechiz_material/stone')->load($stone_id);
        	$current_qty =  $stone->getQty();
        	$qty = $current_qty + $change;
        	$stone->setQty($qty)->save();
            //add event log stock
              $admin = Mage::getSingleton('admin/session')->getUser();
                if($admin->getId()){
                        Mage::dispatchEvent('opentechiz_admin_stock_movement_logger', array(
                            'user_id' => $admin->getId(),
                            'description' => 'Add stone #'.$stone_id.'to stock',
                            'qty' => '+'.$change,
                            'on_hold' => '',
                            'type' => 1,
                            'stock_id' =>$stone_id
                        ));
                    }
        }
    }


}