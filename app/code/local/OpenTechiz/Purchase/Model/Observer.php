<?php

class OpenTechiz_Purchase_Model_Observer
{
    public function addSupplierInformation(Varien_Event_Observer $observer)
    {
        if (!Mage::getSingleton('admin/session')->isAllowed('admin/erp/purchase/supplier/erpproduct')) {
            return;
        }
        $form = $observer->getForm();
        /** @var Varien_Data_Form_Element_Fieldset $fieldset */
        foreach ($form->getElements() as $fieldset) {
            if ($fieldset->getType() != 'fieldset' || !$form->getElement('price')) {
                continue;
            }
            $fieldset->addField(
                'supplier_information',
                'text',
                ['label' => 'Suppliers']
            );
            $field = $form->getElement('supplier_information');
            $field->setRenderer(
                Mage::app()->getLayout()->createBlock('opentechiz_purchase/adminhtml_product_edit_tab_price_supplier')
            );
        }
    }

    public function updateTagSku(Varien_Event_Observer $observer)
    {
        $instock = $observer->getObject();
        if (!$instock->getSku()) {
            return;
        }
        $tagSku = Mage::helper('opentechiz_purchase')->getTagSkuBySku($instock->getSku(), $instock->getProductId());
        $instock->setTagSku($tagSku);
    }

    public function updateProductTagSku(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getProduct();
        if (!$product->getId()) {
            return;
        }
        /** @var OpenTechiz_Purchase_Helper_Data $helper */
        $helper = Mage::helper('opentechiz_purchase');
        $metalAttribute = $helper->getMetalAttribute();
        if (!$metalAttribute) {
            return;
        }
        if ($product->getOrigData($metalAttribute->getAttributeCode()) == $product->getData($metalAttribute->getAttributeCode())) {
            return;
        }
        /** @var OpenTechiz_Inventory_Model_Resource_Instock_Collection $collection */
        $collection = Mage::getModel('opentechiz_inventory/instock')->getCollection();
        $collection->addFieldToFilter('product_id', $product->getId());
        foreach ($collection as $instock) {
            $instock->save();
        }
    }
}
