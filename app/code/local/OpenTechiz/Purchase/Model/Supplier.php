<?php

class OpenTechiz_Purchase_Model_Supplier extends Mage_Core_Model_Abstract
{

    const EXCEPTION_INVALID_ACTIVE_PORTAL = 1;
    const EXCEPTION_INVALID_EMAIL_OR_PASSWORD = 2;

    protected function _construct()
    {
        $this->_init('opentechiz_purchase/supplier');
    }
    
    protected function _beforeSave()
    {
        if($this->getData('username')) {
            $supplierID = $this->_getResource()->loadSupplierIDByUsername($this->getData('username'));
            if($supplierID && $supplierID != $this->getId()) {
                Mage::throwException("Username is used for other supplier.");
            }
        }
        if($this->getData('password')) {
            $this->setPassword($this->getData('password'));
        }
        return parent::_beforeSave();
    }

    public function authenticate($login, $password)
    {
        $this->loadByUsername($login);
        if (!$this->validatePassword($password)) {
            throw Mage::exception(
                    'Mage_Core', Mage::helper('opentechiz_purchase')->__('Invalid login or password.'), self::EXCEPTION_INVALID_EMAIL_OR_PASSWORD
            );
        }

        if (!$this->getIsActivePortal()) {
            throw Mage::exception(
                    'Mage_Core', Mage::helper('opentechiz_purchase')->__('Your account is not active.'), self::EXCEPTION_INVALID_ACTIVE_PORTAL
            );
        }
        return true;
    }
    
    public function loadByUsername($username)
    {
        $this->_getResource()->loadByUsername($this, $username);
        return $this;
    }

    /**
     * Validate password with salted hash
     *
     * @param string $password
     * @return boolean
     */
    public function validatePassword($password)
    {
        $hash = $this->getPasswordHash();
        if (!$hash) {
            return false;
        }
        return Mage::helper('core')->validateHash($password, $hash);
    }

    /**
     * Set plain and hashed password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        // $this->setData('password', $password);
        $this->setPasswordHash($this->hashPassword($password));
        return $this;
    }

    /**
     * Hash customer password
     *
     * @param   string $password
     * @param   int    $salt
     * @return  string
     */
    public function hashPassword($password, $salt = null)
    {
        return $this->_getHelper('core')
                        ->getHash(trim($password), (bool) $salt ? $salt : Mage_Admin_Model_User::HASH_SALT_LENGTH);
    }

    /**
     * Get helper instance
     *
     * @param string $helperName
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelper($helperName)
    {
        return Mage::helper($helperName);
    }

    /**
     * Encrypt password
     *
     * @param   string $password
     * @return  string
     */
    public function encryptPassword($password)
    {
        return Mage::helper('core')->encrypt($password);
    }

    /**
     * Decrypt password
     *
     * @param   string $password
     * @return  string
     */
    public function decryptPassword($password)
    {
        return Mage::helper('core')->decrypt($password);
    }

}
