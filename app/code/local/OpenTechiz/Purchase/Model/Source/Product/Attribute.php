<?php

class OpenTechiz_Purchase_Model_Source_Product_Attribute
{
    public function toOptionArray()
    {
        $options = array();
        $collection = Mage::getResourceModel('catalog/product_attribute_collection');
        $options[] = array(
            'value' => '',
            'label' => ''
        );
        foreach ($collection as $attribute) {
            $options[] = array(
                'value' => $attribute->getAttributeCode(),
                'label' => ($attribute->getFrontendLabel() ? $attribute->getFrontendLabel()
                    : $attribute->getAttributeCode())
            );
        }
        return $options;
    }

}