<?php

class OpenTechiz_Purchase_Model_Source_Product_Source
{
    public function toOptionArray()
    {
        return [
            OpenTechiz_Purchase_Helper_Data::PO_SOURCE_AUTO => Mage::helper('adminhtml')->__('Auto'),
            OpenTechiz_Purchase_Helper_Data::PO_SOURCE_MANUAL => Mage::helper('adminhtml')->__('Manual')
        ];
    }

}