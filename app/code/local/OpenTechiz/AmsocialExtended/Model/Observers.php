<?php

class OpenTechiz_AmsocialExtended_Model_Observers {
     public function generateNewCoupon(Varien_Event_Observer $observer) {
         $resObj = $observer->getData('resObj');
         $code = $observer->getData('code');
         $currentPageUrl = $observer->getData('currentPageUrl');
         
         if($code === 204 && strpos($currentPageUrl, Mage::helper('cms/page')->getPageUrl('coupons'))== 0) {
             $resObj->setData('coupon_code', Mage::getModel('amsocial/coupon')->getCouponCode());
         }
     }
}