<?php

/**
 * OnlineBiz Software Solution
 * 
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Jordan Tran<jordan@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2020, OnlineBiz Software Solution
 * 
 * Create at: April 7, 2020 2:27:27 PM
 */
class OpenTechiz_ProductReviewEmail_Model_Cron
{

    public function removeProductReviewEmail()
    {   
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $collection = Mage::getModel('emailnotify/emailnotify')->getCollection()->getSelect()->where("main_table.update_time <= DATE_SUB(NOW(), INTERVAL 60 DAY) AND email_description like '%20%'");
        $listItem = $readConnection->fetchAll($collection);
        if(!empty($listItem)){
           foreach($listItem as $k =>$val){
                $emailModel = Mage::getModel('emailnotify/emailnotify')->load($val['id']);
                if($emailModel){
                    $emailDescription = $emailModel->getEmailDescription();
                    if(trim($emailDescription) =="20"){
                        $emailModel->delete();
                        continue;
                    }
                    if (strpos($emailDescription, ',20,') !== false) {
                        $description = str_replace(',20,',',',$emailDescription);
                        $update_time = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                        $emailModel->setEmailDescription($description)->setUpdateTime($update_time)->save();
                    }elseif((strpos($emailDescription, '20,') !== false)){
                        $description = trim(str_replace('20,','',$emailDescription));
                        $update_time = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                        $emailModel->setEmailDescription($description)->setUpdateTime($update_time)->save();
                    }elseif((strpos($emailDescription, ',20') !== false)){
                        $description = trim(str_replace(',20','',$emailDescription));
                        $update_time = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
                        $emailModel->setEmailDescription($description)->setUpdateTime($update_time)->save();
                    }

                }
           }
        }
        
    
        
           
    }

}
