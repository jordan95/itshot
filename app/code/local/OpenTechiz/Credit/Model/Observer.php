<?php

class OpenTechiz_Credit_Model_Observer
{

    protected function updateTotalPaid($invoice, $paymentgrid)
    {
        $order = $invoice->getOrder();
//        if ($paymentgrid->getTotal() <= 0) {
//            Mage::getSingleton('adminhtml/session')->addError("The value of Payment Received is invalid.");
//            Mage::app()->getResponse()
//                    ->setRedirect(Mage::getUrl('adminhtml/sales_invoice/view', array('invoice_id' => $paymentgrid->getData('invoice_id'))))
//                    ->sendResponse();
//            exit;
//        }
        $baseAmount = 0;
        if (!$paymentgrid->isObjectNew()) {
            $origPaymentgrid = $paymentgrid->getOrigData();
            if ($paymentgrid->getTotal() != $origPaymentgrid['total']) {
                $baseAmount = $paymentgrid->getTotal() - $origPaymentgrid['total'];
            }
        } else {
            $baseAmount = $paymentgrid->getTotal();
        }
        if ($baseAmount != 0) {
            $helper = $this->getHelper();
            $payment_id = $helper->getPaymentReceivedMethodsID();
            // Update Store Credit
            if ($helper->isEnablePaymentReceivedMethods() && $paymentgrid->getData('payment_id') == $payment_id && $baseAmount != 0) {
                $email = $paymentgrid->getEmail();
                $customer = Mage::getModel('customer/customer')->setWebsiteId(1)->loadByEmail($email);
                $customerId = $customer->getId();
                if ($customer->getId()) {
                    $balance_delta = 0;
                    $message = Mage::helper('credit')->__('Used in invoice #%s', $paymentgrid->getData('invoice_id'));
                    $balance = Mage::getModel('credit/balance')->loadByCustomer($customerId);
                    $currentTotalStoreCredit = $balance->getAmount();
                    if ($baseAmount > $currentTotalStoreCredit) {
                        Mage::getSingleton('adminhtml/session')->addError("The value of Store Credit is exceed total avaiable store credit.  Avaiable Store Credit is: " . Mage::helper('core')->currency($currentTotalStoreCredit, true, false));
                        Mage::app()->getResponse()->setRedirect(Mage::getUrl('adminhtml/sales_invoice/view', array('invoice_id' => $paymentgrid->getData('invoice_id'))))->sendResponse();
                        exit;
                    }
                    $action = $baseAmount > 0 ? Mirasvit_Credit_Model_Transaction::ACTION_USED : Mirasvit_Credit_Model_Transaction::ACTION_REFUNDED;
                    $balance_delta = (-1) * $baseAmount;
                    $balance->addTransaction(
                            $balance_delta, $action, $message
                    );
                }
            }
        }
    }

    protected function refundTotalPaid($invoice, $paymentgrid)
    {
        $order = $invoice->getOrder();
        $baseAmount = floatval($paymentgrid->getTotal());
        $amount = Mage::helper('directory')->currencyConvert($baseAmount, "USD", $order->getOrderCurrencyCode());
        if ($baseAmount != 0) {
            $helper = $this->getHelper();
            $payment_id = $helper->getPaymentReceivedMethodsID();

            // Update Store Credit
            if ($helper->isEnablePaymentReceivedMethods() && $paymentgrid->getData('payment_id') == $payment_id) {
                $email = $paymentgrid->getEmail();
                $customer = Mage::getModel('customer/customer')->setWebsiteId(1)->loadByEmail($email);
                $customerId = $customer->getId();
                $balance = Mage::getModel('credit/balance')->loadByCustomer($customerId);
                if (!$customer->getId()) {
                    return;
                }
                $message = Mage::helper('credit')->__('Refund invoice #%s', $paymentgrid->getData('invoice_id'));
                if ($baseAmount != 0) {
                    $balance->addTransaction(
                            $baseAmount, Mirasvit_Credit_Model_Transaction::ACTION_REFUNDED, $message
                    );
                }
            }
        }
    }

    public function updatePaidAmount(Varien_Event_Observer $observer)
    {
        $paymentgrid = $observer->getEvent()->getPaymentgrid();
        $order = null;
        if ($paymentgrid->getType() == 'invoice' || ($paymentgrid->isObjectNew() && $paymentgrid->getData('invoice_id'))) {
            $invoice = Mage::getModel("sales/order_invoice")->load($paymentgrid->getData('invoice_id'));
            if (!$invoice->getId()) {
                return;
            }
            $order = $invoice->getOrder();
            $this->updateTotalPaid($invoice, $paymentgrid);
        } elseif ($paymentgrid->getType() == 'creditmemo' || ($paymentgrid->isObjectNew() && $paymentgrid->getData('credit_memo_id'))) {
            $creditmemo = Mage::getModel("sales/order_creditmemo")->load($paymentgrid->getData('credit_memo_id'));
            if (!$creditmemo->getId()) {
                return;
            }
            $order = $creditmemo->getOrder();
        }
        if ($order) {
            Mage::helper('partialpaymentextended')->refreshOrderTotals($order);
        }
    }

    public function refundPaidAmount(Varien_Event_Observer $observer)
    {
        $paymentgrid = $observer->getEvent()->getPaymentgrid();
         $order = null;
        if ($paymentgrid->getType() == 'invoice' || ($paymentgrid->isObjectNew() && $paymentgrid->getData('invoice_id'))) {
            $invoice = Mage::getModel("sales/order_invoice")->load($paymentgrid->getData('invoice_id'));
            if (!$invoice->getId()) {
                return;
            }
             $order = $invoice->getOrder();
            $this->refundTotalPaid($invoice, $paymentgrid);
        } elseif ($paymentgrid->getType() == 'creditmemo' || ($paymentgrid->isObjectNew() && $paymentgrid->getData('credit_memo_id'))) {
            $creditmemo = Mage::getModel("sales/order_creditmemo")->load($paymentgrid->getData('credit_memo_id'));
            if (!$creditmemo->getId()) {
                return;
            }
            $order = $creditmemo->getOrder();
        }
        if ($order) {
            Mage::helper('partialpaymentextended')->refreshOrderTotals($order);
        }
    }

    public function getHelper()
    {
        return Mage::helper("opentechizcredit");
    }
    public function saveUserCreateStoreCredit($observer)
    {
         $transaction = $observer->getEvent()->getCreditTransaction();
        if(Mage::getSingleton('admin/session')->isLoggedIn()){
            $user_id = Mage::getSingleton('admin/session')->getUser()->getId();
        }else{
            $user_id = 0;
        }
        $transaction->setUserId($user_id);
    }
    
}
