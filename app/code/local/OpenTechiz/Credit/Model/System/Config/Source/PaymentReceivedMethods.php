<?php

class OpenTechiz_Credit_Model_System_Config_Source_PaymentReceivedMethods
{

    public function toOptionArray()
    {
        $collection = Mage::getModel('cod/payment')->getCollection();
        $data = array();
        if ($collection->count() > 0) {
            foreach ($collection as $item) {
                $data[] = array(
                    'value' => $item->getId(),
                    'label' => $item->getName()
                );
            }
        }
        return $data;
    }

}
