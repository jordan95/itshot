<?php

class OpenTechiz_Credit_Model_Observer_Invoice extends Mirasvit_Credit_Model_Observer_Invoice
{

    public function onSaveAfter($observer)
    {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();

        // is new invoice
        if ($invoice->getOrigData() === null && $invoice->getBaseCreditAmount() && Mage::registry('credit_invoiced') === null) {
            $order->setBaseCreditInvoiced($order->getBaseCreditInvoiced() + $invoice->getBaseCreditAmount())
                    ->setCreditInvoiced($order->getCreditInvoiced() + $invoice->getCreditAmount());
            Mage::register('credit_invoiced', true);
        }

        $order->getResource()->saveAttribute($order, 'base_credit_invoiced');
        $order->getResource()->saveAttribute($order, 'credit_invoiced');

        return $this;
    }

    public function revertStorecreditFromInvoice(Varien_Event_Observer $observer)
    {
        $invoice = $observer->getInvoice();
        if ($invoice->getCreditAmount() > 0) {
            $order = $invoice->getOrder();
            if ($order->getCreditInvoiced() > $invoice->getCreditAmount()) {
                $order->setCreditInvoiced($order->getCreditInvoiced() - $invoice->getCreditAmount());
                $order->setBaseCreditInvoiced($order->getBaseCreditInvoiced() - $invoice->getBaseCreditAmount());
            } else {
                $order->setCreditInvoiced(0);
                $order->setBaseCreditInvoiced(0);
            }
        }
    }

    public function removeAllPaymentRecord(Varien_Event_Observer $observer)
    {
        $invoice = $observer->getInvoice();
        $paymentRecords = Mage::getModel('cod/paymentgrid')->getCollection()
                ->addFieldToFilter('invoice_id', $invoice->getId());
        foreach ($paymentRecords as $record) {
            $record->delete();
        }
    }

}
