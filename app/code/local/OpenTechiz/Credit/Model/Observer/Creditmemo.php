<?php

class OpenTechiz_Credit_Model_Observer_Creditmemo extends Mirasvit_Credit_Model_Observer_Invoice
{

    public function removeAllPaymentRecord(Varien_Event_Observer $observer)
    {
        $creditmemo = $observer->getCreditmemo();
        $paymentRecords = Mage::getModel('cod/paymentgrid')->getCollection()
                ->addFieldToFilter('credit_memo_id', $creditmemo->getId());
        foreach ($paymentRecords as $record) {
            $record->delete();
        }
    }

}
