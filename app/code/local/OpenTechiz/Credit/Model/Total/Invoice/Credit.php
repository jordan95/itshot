<?php

class OpenTechiz_Credit_Model_Total_Invoice_Credit extends Mirasvit_Credit_Model_Total_Invoice_Credit
{

    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        $order = $invoice->getOrder();
        if ($order->getBaseCreditAmount() && floatval($order->getBaseCreditInvoiced()) == 0
        ) {
            $baseUsed = $order->getBaseCreditAmount();
            $used = $order->getCreditAmount();

            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() - $baseUsed)
                    ->setGrandTotal($invoice->getGrandTotal() - $used)
                    ->setBaseCreditAmount($baseUsed)
                    ->setCreditAmount($used);
        }

        return $this;
    }

}
