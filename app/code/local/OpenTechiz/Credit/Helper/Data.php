<?php

/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_credit
 * @version   1.0.16
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */
class OpenTechiz_Credit_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getRewardPoint()
    {
        return Mage::getModel('checkout/cart')->getQuote()->getRewardsPointsSpending();
    }

    public function isEnablePaymentReceivedMethods()
    {
        return Mage::getStoreConfig("credit/payment_received_methods/enable");
    }

    public function getPaymentReceivedMethodsID()
    {
        return Mage::getStoreConfig("credit/payment_received_methods/linking_to");
    }

}
