<?php

class OpenTechiz_Credit_Block_Adminhtml_Customer_Edit_Tab_Credit_Renderer_UserName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
    	$name ='';
        $user_id =  $row->getData($this->getColumn()->getIndex());
        if(!is_null($user_id)){
         $user = Mage::getModel('admin/user')->load($user_id);
	      if($user->getId()){
	        $name = $user->getUsername();
	        }else{
	            $name = 'system';
	       }
        }
       
        return $name;
    }
}