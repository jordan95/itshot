<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_credit
 * @version   1.0.16
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



class OpenTechiz_Credit_Block_Adminhtml_Customer_Edit_Tab_Credit_Grid extends Mirasvit_Credit_Block_Adminhtml_Customer_Edit_Tab_Credit_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('updated_at');
        $this->setDefaultDir('desc');

        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
        $this->setDefaultLimit(100);

        $this->setEmptyText(Mage::helper('credit')->__('No Transactions Found'));
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('credit/transaction')->getCollection()
            ->addFilterByCustomer(Mage::registry('current_customer')->getId());

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        $this->addColumn('user_id', array(
            'header'   => Mage::helper('credit')->__('User'),
            'index'    => 'user_id',
            'width'    => '50px',
            'renderer' => 'OpenTechiz_Credit_Block_Adminhtml_Customer_Edit_Tab_Credit_Renderer_UserName',
            'sortable' => false
        ));
        return $this;
    }
}
