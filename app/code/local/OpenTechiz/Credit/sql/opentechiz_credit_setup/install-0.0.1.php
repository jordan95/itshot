<?php

$installer = $this;
$installer->startSetup();
$installer->run("  
    ALTER TABLE tsht_m_credit_transaction
    ADD `user_id`
    INT(11)
    NULL
    COMMENT 'user id create credit';
");

$installer->endSetup();
