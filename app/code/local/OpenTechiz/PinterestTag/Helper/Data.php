<?php
class OpenTechiz_PinterestTag_Helper_Data extends Cadence_Pinterest_Helper_Data
{
	public function getOrderItemsJson()
    {
        $order = $this->_getOrder();

        $itemData = array();

        /** @var Mage_Sales_Model_Order_Item $item */
        foreach($order->getAllVisibleItems() as $item) {
            $qty = max(round($item->getQtyOrdered()), 1);
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
            $cats = $product->getCategoryIds();
            $category_name = "";
            if ( is_array( $cats ) || is_object( $cats ) ) {
                foreach ($cats as $category_id) {
                    $_cat = Mage::getModel('catalog/category')->load($category_id) ;
                    $category_name = $_cat->getName();
                    break;
                }
            }
            $itemData[] = array(
                "product_category" =>$category_name,
                "product_name" => $item->getName(),
                "product_id" => $item->getSku(),
                "product_price" => round($item->getPrice(),2),
                "product_quantity" => $qty
            );
        }

        return json_encode($itemData);
    }
}