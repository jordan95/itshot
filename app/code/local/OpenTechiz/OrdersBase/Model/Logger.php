<?php

/**

 * @category   Jordan
 * @package    OpenTechiz_OrdersBase
 * @copyright  Copyright (c) 2021
 */
class OpenTechiz_OrdersBase_Model_Logger extends MageWorx_OrdersBase_Model_Logger
{
    /**
     * Add comment and save order
     *
     * @param $text
     * @param Mage_Sales_Model_Order $order
     * @param int $notify (0 - no one; 1 - only admin; 2 - notify all)
     * @return $this
     * @throws Exception
     */
    public function log($text, Mage_Sales_Model_Order $order, $notify)
    {
        $order->addStatusHistoryComment($text, $order->getStatus())
            ->setIsVisibleOnFront(0)
            ->setIsCustomerNotified($notify > 1);

        if ($notify) {
            $order->sendOrderUpdateEmail($notify > 1, $text);
        }
        $order->save();

        return $this;
    }
}