<?php

class OpenTechiz_Migrateaff_Model_Service_AffService extends MW_Migrateaff_Model_Service_AffService
{
   
   

    public function saveCustomerToMagento($websiteId, $store, $data){
        $customer = Mage::getModel("customer/customer");
        $customer ->setWebsiteId($websiteId)
            ->setStore($store)
            ->setFirstname("")
            ->setLastname($data['Contact'])
            ->setEmail($data['Email'])
            ->setPassword($data['LoginPassword']);
        if($data['DateCreated'] !=""){
            $createdTime = $this->updateCorrectTime($data['DateCreated']);
            $customer->setCreatedAt($createdTime);
        }
        $customer->setForceConfirmed(true);
        $customer->save();
        $customer->setConfirmation(null);
        $customer->save();

        $customerId = $customer->getId();

        
        $customAddress = Mage::getModel('customer/address');
        $country_id = $this->getCountryId($data['Country']);
        $region_id = $this->getRegionId($data['State'],$country_id);
        $address = array(
            'customer_address_id' => '',
            'prefix' => '',
            'firstname' => '',
            'middlename' => '',
            'lastname' => $data['Contact'],
            'suffix' => '',
            'company' => $data['CompanyName'],
            'street' => array(
                '0' => $data['CompanyAddress'],
                '1' => ''
            ),
            'city' => $data['City'],
            'region_id' => $region_id,
            'region' => $data['State'],
            'country_id' => $country_id,
            'fax' => $data['Fax'],
            'postcode' => $data['Zip'],
            'telephone' => $data['Telephone'],
            'save_in_address_book' => 1
        );
        $customAddress->setData($address)
            ->setCustomerId($customerId)
            ->setIsDefaultBilling('1')
            ->setIsDefaultShipping('1')
            ->setSaveInAddressBook('1');
        $customAddress->save();

        return $customer;
    }


    public function saveCustomerToAffiliate($customerId,$data){
        $active = MW_Affiliate_Model_Statusactive::ACTIVE;
        $payment_gateway = '';
        $paypal_email = '';
        $auto_withdrawn = 1;
        $payment_release_level = 0;
        $reserve_level = 0;
        $bank_name = '';
        $name_account = '';
        $bank_country = '';
        $swift_bic = '';
        $account_number = '';
        $re_account_number = '';
        $referral_site = '';
        $invitation_type = MW_Affiliate_Model_Typeinvitation::NON_REFERRAL;

        if($data["PaymentMethodID"]!="" && $data["PaymentMethodID"]=="1"){
            $payment_gateway = "check";
        }
        if($data["PaymentMethodID"]!="" && $data["PaymentMethodID"]=="2"){
            $payment_gateway = "paypal";
        }
        $customerData = array(
            'customer_id'			=> $customerId,
            'active'				=> $active,
            'payment_gateway'		=> $payment_gateway,
            'payment_email'		    => $paypal_email,
            'auto_withdrawn'		=> $auto_withdrawn,
            'withdrawn_level'		=> $payment_release_level,
            'reserve_level'		=> $reserve_level,
            'bank_name'			=> $bank_name,
            'name_account'		=> $name_account,
            'bank_country'		=> $bank_country,
            'swift_bic'			=> $swift_bic,
            'account_number'		=> $account_number,
            're_account_number'	=> $re_account_number,
            'referral_site'		=> $referral_site,
            'total_commission'	=> 0,
            'total_paid'			=> 0,
            'referral_code' 		=> '',
            'status'				=> 1,
            'invitation_type'		=> $invitation_type,
            'customer_time' 		=> now(),
            'customer_invited'	=> 0
        );

        Mage::getModel('affiliate/affiliatecustomers')->saveCustomerAccount($customerData);
        // set lai referral code cho cac customer affiliate
        Mage::helper('affiliate')->setReferralCode($customerId);

        //$store_id = Mage::getModel('customer/customer')->load($customerId)->getStoreId();
        //Mage::helper('affiliate')->setMemberDefaultGroupAffiliate($customerId,$store_id);

        $data = array(
            'customer_id'	=> $customerId,
            'group_id'	=> $data['AffiliateTypeID']
        );
        Mage::getModel('affiliate/affiliategroupmember')->setData($data)->save();

        //set total member customer program
        Mage::helper('affiliate')->setTotalMemberProgram();

    }

    public function _prepareData($Collection){
        $websiteId = Mage::app()->getWebsite()->getId();
        $store = Mage::app()->getStore();
        try{
            foreach($Collection->getData() as $data){
                $customer = Mage::getModel("customer/customer")->setWebsiteId($websiteId)
                    ->loadByEmail($data['Email']);
                if(!$customer->getId()){
                    // save customer to magento
                    $customer = $this->saveCustomerToMagento($websiteId, $store, $data);
                }
                $customerId = $customer->getId();
                $this->_CUSTOMER_IDS[] = array(
                    "aff_id" => $data['AffiliateID'],
                    "customer_id" => $customerId,
                );
                $aff = Mage::getModel('affiliate/affiliatecustomers')->load($customerId);
                if(!$aff->getId()) {
                    // save customer to Affiliate
                    $this->saveCustomerToAffiliate($customerId, $data);
                    
                    $this->saveAffiliateWebsite($customerId, $data);
                    
                    // save customer transaction
                    $this->saveAffiliateTransactionAndCredit($customerId, $data);
                    // save affiliate intivation
                    $this->saveAffiliateIntivation($customerId, $data['AffiliateID']);
                }

                

            }
            return true;
        }

        catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'migrate-aff.log', true);
            return false;
        }
    }
    public function saveAffiliateWebsite($customerId,$data){
        Mage::getModel('affiliate/affiliatewebsitemember')->getCollection();
        if(!empty($customerId) && $data['WebSiteURL'] !=""){
            $domain_name = $data['WebSiteURL'];
            $verified_key = '';
            $is_verified = 1;
            $status = 1;
            $created_time = $data['DateCreated'];
            $affiliate_website = array(
                'customer_id'           => $customerId,
                'domain_name'           => $domain_name,
                'verified_key'          => $verified_key,
                'is_verified'           => $is_verified,
                'status'                => $status,
                'created_time'          => $created_time
            );

            Mage::getModel('affiliate/affiliatewebsitemember')->setData($affiliate_website)->save();
        }
    }
    public function getCountryId($countryName){
        if($countryName !=""){
            $countries = Mage::app()->getLocale()->getCountryTranslationList();;
            $countryCode = array_search($countryName, $countries);
            return $countryCode;
        }
        return "";
    }
    public function updateCorrectTime($datetime){
        // Convert datetime to Unix timestamp
        $timestamp = strtotime($datetime);

        // Subtract time from datetime
        $time = $timestamp - (7* 60 * 60);

        // Date and time after subtraction
        $datetime = date("Y-m-d H:i:s", $time);
        return $datetime;
    }
    public function getRegionId($region_code,$country_id){
        $state_id = '';
        if($region_code =="" || $country_id ==""){
            return '';
        }
        $region = Mage::getModel('directory/region')->getCollection()->addFieldToFilter('name',array("eq"=>$region_code));
        if(!empty($region->getData())){
            foreach($region->getData() as $val){
                $state_id = $val['region_id'];
                return $state_id;
            }
        }
        $region = Mage::getModel('directory/region')->loadByCode($region_code, $country_id);
        $state_id = $region->getId();
        return $state_id;
    }
    public function saveAffiliateTransactionAndCredit($customerId, $data){
        $affiliateId = $data['AffiliateID'];
        $allTransactionOfAffiliate = Mage::getResourceModel('migrateaff/oldaff_collection')
                                        ->getAllTransactionOfAffiliate($affiliateId);
        $dataToSaveTran = array();
        $dataToSaveHis = array();
        $dataIntivation = array();

        $count = 1;

        $totalCommission = 0;
        $totalPaid = 0;
        $credit = 0;

        foreach ($allTransactionOfAffiliate as $transaction){
            if($transaction['Paid'] == 1){
                $paid_status = MW_Affiliate_Model_Status::COMPLETE;
            }else{
                $paid_status = MW_Affiliate_Model_Status::PENDING;
            }
            $tran1 = array(
                'order_id'              => $transaction['SalesId'],
                'customer_id'           => $transaction['CUSTOMER_ID'],
                'total_commission'      => $transaction['Commission'],
                'total_discount'        => 0,
//                'grand_total'         => $transaction['GRAND_TOTAL'],
                'transaction_time'      => $transaction['ORDER_DATE'],
                'commission_type'       => MW_Mwcredit_Model_Transactiontype::BUY_PRODUCT,
                'show_customer_invited' => 0,
                'customer_invited'      => $customerId,
                'invitation_type'       => MW_Affiliate_Model_Typeinvitation::REFERRAL_LINK,
                'status'                => $paid_status,
            );
            $tran2 = array(
                'order_id'              => $transaction['SalesId'],
                'customer_id'           => $transaction['CUSTOMER_ID'],
                'total_commission'      => $transaction['Commission'],
                'total_discount'        => 0,
//                'grand_total'         => $transaction['GRAND_TOTAL'],
                'transaction_time'      => $transaction['ORDER_DATE'],
                'commission_type'       => MW_Mwcredit_Model_Transactiontype::BUY_PRODUCT,
                'show_customer_invited' => $customerId,
                'customer_invited'      => 0,
                'invitation_type'       => MW_Affiliate_Model_Typeinvitation::REFERRAL_LINK,
                'status'                => $paid_status,
            );

            $tranHis = array(
                'customer_id' => $transaction['CUSTOMER_ID'],
                'product_id' => 0,
                'program_id' => 0,
                'order_id' => $transaction['SalesId'],
                'total_amount' => $transaction['GRAND_TOTAL'],
                'history_commission' => $transaction['Commission'],
                'history_discount' => 0,
                'transaction_time' => $transaction['ORDER_DATE'],
                'customer_invited'      => $customerId,
                'invitation_type'       => MW_Affiliate_Model_Typeinvitation::REFERRAL_LINK,
                'status'                => $paid_status,
            );

            $intivation = array(
                'customer_id' => $customerId,
                'email' => '',
                'ip' => $transaction['IPAddress'],
                'count_click_link' => 0,
                'count_register' => 0,
                'count_purchase' => 1,
                'referral_from' => $transaction['Referrer'],
                'referral_from_domain' => '',
                'referral_to' => '',
                'order_id' => $transaction['SalesId'],
                'invitation_type' => 1,
                'invitation_time' => $transaction['ORDER_DATE'],
                'status' => MW_Affiliate_Model_Statusinvitation::PURCHASE,
                'commission' => $transaction['Commission'],
                'count_subscribe' => 0,
            );


            $totalCommission += $transaction['Commission'];
            if($transaction['Paid'] == 0){
                $credit += $transaction['Commission'];
            }else{
                $totalPaid += $transaction['Commission'];
            }

            $dataToSaveTran[] = $tran1;
            $dataToSaveTran[] = $tran2;
            $dataToSaveHis[] = $tranHis;
            $dataIntivation[] = $intivation;

            $count++;

            if($count >= 300){
                $success1 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_transaction', $dataToSaveTran);
                $success2 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_history', $dataToSaveHis);
                $success3 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_invitation', $dataIntivation);

                $this->catchingError($success1, $customerId, $affiliateId, 'transaction');
                $this->catchingError($success2, $customerId, $affiliateId, 'history');
                $this->catchingError($success3, $customerId, $affiliateId, 'invitation');

                $dataToSaveTran = array();
                $dataToSaveHis = array();
                $dataIntivation = array();
                $count = 1;
            }
        }
        if(count($dataToSaveTran) > 0){
            $success1 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_transaction', $dataToSaveTran);
            $success2 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_history', $dataToSaveHis);
            $success3 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_invitation', $dataIntivation);

            $this->catchingError($success1, $customerId, $affiliateId, 'transaction');
            $this->catchingError($success2, $customerId, $affiliateId, 'history');
            $this->catchingError($success3, $customerId, $affiliateId, 'invitation');
        }
        if($credit > 0){
            $creditcustomer = Mage::getModel('mwcredit/creditcustomer')->load($customerId);
            $creditcustomer->setCredit($credit)->save();
        }
        if($totalPaid > 0 || $totalCommission > 0){
            $aff = Mage::getModel('affiliate/affiliatecustomers')->load($customerId);
            $aff->setTotalCommission($totalCommission)
                ->setTotalPaid($totalPaid)
                ->save();
        }
        if($totalPaid > 0){
            $dataPayout =array();
            $pament_email = '';
            $payment_gateway ='';
            $status_complete = 2;
            if($data["PaymentMethodID"]!="" && $data["PaymentMethodID"]=="1"){
                $payment_gateway = "check";
            }
            if($data["PaymentMethodID"]!="" && $data["PaymentMethodID"]=="2"){
                $payment_gateway = "paypal";
            }
            if($data["PayPalEmail"] !=""){
                $pament_email  = $data["PayPalEmail"];
            }
            $payment_date = $this->getPaymentDate($affiliateId);
            if(empty($payment_date)){
                $payment_date = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            }
            $dataPayout = array(
                'customer_id' => $customerId,
                'payment_gateway' => $payment_gateway,
                'payment_email' => $pament_email,
                'bank_name' => '',
                'name_account' =>'',
                'bank_country' => '',
                'swift_bic' => '',
                'account_number' => '',
                're_account_number' => '',
                'withdrawn_amount' => $totalPaid,
                'fee' => 0,
                'amount_receive' => $totalPaid,
                'withdrawn_time' => $payment_date,
                'status' => $status_complete
            );
            $payout = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_withdrawn', $dataPayout);
        }
    }
    public function getPaymentDate($affId){
        $cr = Mage::getSingleton('core/resource');
        $connection = $cr->getConnection('core_write');
        $select = $connection->select()
            ->from(
                array('main_table' => Mage::getSingleton('core/resource')->getTableName('tblaff_PaymentHistory')),
                array( "PaymentDate" )
            )
        ;
        $select->where('main_table.AffiliateID = ?', $affId);
        $paymentDate = $connection->fetchOne($select);
        return $paymentDate;
    }
        
}