<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE `tsht_mw_affiliate_website_member` 
    DROP INDEX 	domain_name;
");

$installer->endSetup();