<?php

class OpenTechiz_Hotjar_Helper_Data extends Mage_Core_Helper_Abstract
{

    const MODULE_KEY_ROUTES = 'hotjar/general/page_view';
    const MODULE_KEY_ENABLE = 'hotjar/general/enable';

    public function isAllowed($route)
    {
        if (!$this->isModuleActive() || !$this->isEnabled()) {
            return false;
        }

        return $this->isEnabledRoute($route);
    }

    public function isEnabledRoute($route)
    {
        foreach ($this->getEnabledRoutes() as $enabledRoute) {
            if (false !== strpos($route, $enabledRoute)) {
                return true;
            }
        }

        return false;
    }

    public function isModuleActive()
    {
        return Mage::getConfig()
                        ->getModuleConfig("OpenTechiz_Hotjar")
                        ->is('active', 'true');
    }

    public function isEnabled()
    {
        return Mage::getStoreConfigFlag(self::MODULE_KEY_ENABLE);
    }

    public function getEnabledRoutes()
    {
        $routes = array_filter(explode(',', Mage::getStoreConfig(self::MODULE_KEY_ROUTES)));
        array_map('strtolower', $routes);

        return $routes;
    }

}
