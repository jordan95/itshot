<?php

class OpenTechiz_Hotjar_Model_Observer
{
    public function onPreDispatch(Varien_Event_Observer $observer) {
        if (! Mage::helper('hotjar')->isEnabled()) return;
        $controller = $observer->getEvent()->getControllerAction();
        $route = $controller->getFullActionName();
        if (! Mage::helper('hotjar')->isAllowed($route)) return;
        Mage::register('hotjar_active', true);
    }
    
    public function addHandle(Varien_Event_Observer $observer)
    {
        if(Mage::registry('hotjar_active')) {
            $observer->getEvent()->getLayout()->getUpdate()
            ->addHandle('hotjar_default');
        }
 
    }

}
