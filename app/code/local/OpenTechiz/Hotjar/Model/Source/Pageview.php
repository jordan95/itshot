<?php
class OpenTechiz_Hotjar_Model_Source_Pageview {
    public function toOptionArray()
    {
        return array(
            array('value' => 'checkout_cart_index', 'label' => Mage::helper('hotjar')->__('Shopping Cart')),
            array('value' => 'onestepcheckout_index_index', 'label' => Mage::helper('hotjar')->__('One Step Checkout Page')),
            array('value' => 'checkout_onepage_success', 'label'=> Mage::helper('hotjar')->__('Checkout Onepage Success'))
        );
    }
}