<?php

class OpenTechiz_Hotjar_Block_Trackingcode extends Mage_Core_Block_Abstract
{

    const PATH_HOTJAR_TRACKINGCODE = 'hotjar/general/tracking_code';

    protected function _toHtml()
    {
        return Mage::getStoreConfig(self::PATH_HOTJAR_TRACKINGCODE);
    }

}
