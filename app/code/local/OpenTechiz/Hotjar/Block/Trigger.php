<?php

class OpenTechiz_Hotjar_Block_Trigger extends Mage_Core_Block_Abstract
{

    protected $_name;

    public function setName($name)
    {
        $this->_name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->_name;
    }

    protected function _toHtml()
    {
        return sprintf("<script>hj('trigger', '%s');</script>", $this->getName());
    }

}
