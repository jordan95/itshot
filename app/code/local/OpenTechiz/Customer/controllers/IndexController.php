<?php

/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 06/07/2017
 * Time: 17:42
 */
require_once(Mage::getModuleDir('controllers', 'Medialounge_Customer') . DS . 'IndexController.php');

class OpenTechiz_Customer_IndexController extends Medialounge_Customer_IndexController
{

    const XML_PATH_REPAIR_EXCHANGE_DEPARTMENT = 'contacts/email/repair_exchange_email';
    const XML_PATH_BILLING_DEPARTMENT = 'contacts/email/billing_email';
    const XML_PATH_CUSTOMER_SERVICE_DEPARTMENT = 'contacts/email/customer_service_email';
    const XML_PATH_CUSTOM_ORDER_DEPARTMENT = 'contacts/email/custom_order_email';
    const XML_PATH_AFFILIATE = 'contacts/email/custom_affiliate_email';

    public function postAction()
    {
        $post = $this->getRequest()->getPost();
        if ($post) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $receipt_email = self::XML_PATH_EMAIL_RECIPIENT;
                if ($post['department'] == 'sales') {
                    $receipt_email = self::XML_PATH_EMAIL_RECIPIENT;
                } elseif ($post['department'] == 'repair') {
                    $receipt_email = self::XML_PATH_REPAIR_EXCHANGE_DEPARTMENT;
                } elseif ($post['department'] == 'billing') {
                    $receipt_email = self::XML_PATH_BILLING_DEPARTMENT;
                } elseif ($post['department'] == 'customer_service') {
                    $receipt_email = self::XML_PATH_CUSTOMER_SERVICE_DEPARTMENT;
                } elseif ($post['department'] == 'custom_orders') {
                    $receipt_email = self::XML_PATH_CUSTOM_ORDER_DEPARTMENT;
                }elseif ($post['department'] == 'affiliate') {
                    $receipt_email = self::XML_PATH_AFFILIATE;
                }
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['name']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['comment']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if ($error) {
                    throw new Exception();
                }
                $messageSuccess = Mage::helper('contacts')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.');
                $canSendMail = Mage::helper('odoo')->isEnabledSendEmail();

                $departments = array(
                    'affiliate' => "Affiliate",
                    'sales' => "Sales",
                    "repair" => "Repair / Exchange",
                    "billing" => "Billing",
                    "customer_service" => "Customer Service",
                    "custom_orders" => "Custom Orders"
                );
                if (!empty($departments[$post["department"]])) {
                    try {
                        // Submit ticket to Odoo
                        $this->submitTicket($post, $departments);
                    } catch (Exception $exc) {
                        $messageSuccess = Mage::helper('contacts')->__('There were a problem with our CRM system, your information will be sent to our admin email');
                    }
                }

                if($this->_getSession()->getFeedbackData()){
                    $this->_getSession()->setFeedbackData('');
                }

                if ($canSendMail) {
                    // Send email if submit ticket Odoo fail or Odoo disabled
                    $this->sendContactMail($receipt_email, $post, $postObject);
                }

                Mage::getSingleton('customer/session')->addSuccess($messageSuccess);
                $this->_redirectReferer();

                return;
            } catch (Exception $e) {
                $this->_getSession()->setFeedbackData($post);
                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
                $this->_redirectReferer();
                return;
            }
        } else {
            $this->_redirect('*/*/');
        }
    }

    protected function submitTicket($post, $departments)
    {
        Mage::getModel('odoo/api_odoo')->submitTicket([
            "customer_name" => $post["name"],
            "customer_phone" => $post["telephone"],
            "customer_email" => $post["email"],
            "message" => $post["comment"],
            "ticket_subject" => $post["subject"],
            "team_id" => $departments[$post["department"]],
        ]);
    }

    protected function sendContactMail($receipt_email, $post, $postObject)
    {
        $mailTemplate = Mage::getModel('core/email_template');
        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                ->setReplyTo($post['email'])
                ->sendTransactional(
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE), Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER), Mage::getStoreConfig($receipt_email), null, array('data' => $postObject)
        );

        if (!$mailTemplate->getSentSuccess()) {
            throw new Exception();
        }
    }

    private function validateRecaptcha($response)
    {
        if (!extension_loaded('curl')) {
            return null;
        }

        $curl = new Varien_Http_Adapter_Curl();
        $curl->setConfig(array(
            'timeout' => 2
        ));
        $post = ['secret' => $this->getHelper()->getRecaptchaSecretKey(),
            'response' => $response
        ];
        $str = '';
        foreach ($post as $key => $value) {
            if ($str != '')
                $str .= '&';
            $str .= sprintf('%s=%s', $key, urlencode($value));
        }

        $curl->write(Zend_Http_Client::POST, 'https://www.google.com/recaptcha/api/siteverify', '1.1', ['Content-Type: application/x-www-form-urlencoded; charset=utf-8', 'Content-Length: ' . strlen($str)], $str);
        $data = $curl->read();
        if ($data === false) {
            return false;
        }
        $data = preg_split('/^\r?$/m', $data, 2);
        $response = $this->getCoreHelper()->jsonDecode(trim($data[1]));
        $curl->close();

        if (isset($response['success']) && $response['success']) {
            return true;
        }

        return false;
    }

    /**
     * @return \Medialounge_Customer_Helper_Data
     */
    private function getHelper()
    {
        return Mage::helper('medialounge_customer');
    }

    /**
     * @return Mage_Core_Helper_Data
     */
    private function getCoreHelper()
    {
        return Mage::helper('core');
    }

    /**
     * Retrieve adminhtml session model object
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

}
