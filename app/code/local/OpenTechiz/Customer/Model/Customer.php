<?php

class OpenTechiz_Customer_Model_Customer extends Mage_Customer_Model_Customer
{
    /**
     * Send email with new account related information
     *
     * @param string $type
     * @param string $backUrl
     * @param string $storeId
     * @param string $password
     * @throws Mage_Core_Exception
     * @return $this
     */
    public function sendNewAccountEmail($type = 'registered', $backUrl = '', $storeId = '0', $password = null)
    {
        $types = array(
            'registered'   => self::XML_PATH_REGISTER_EMAIL_TEMPLATE, // welcome email, when confirmation is disabled
            'confirmed'    => self::XML_PATH_CONFIRMED_EMAIL_TEMPLATE, // welcome email, when confirmation is enabled
            'confirmation' => self::XML_PATH_CONFIRM_EMAIL_TEMPLATE, // email with confirmation link
        );
        if (!isset($types[$type])) {
            Mage::throwException(Mage::helper('customer')->__('Wrong transactional account email type'));
        }

        if (!$storeId) {
            $storeId = $this->_getWebsiteStoreId($this->getSendemailStoreId());
        }

        if (!is_null($password)) {
            $this->setPassword($password);
        }

        // custom send email in case registered
        if ($type == 'registered') {

            $this->_sendEmailTemplateRegistered(
                "customer_create_account_welcome_email_template",
                self::XML_PATH_REGISTER_EMAIL_IDENTITY,
                array('customer' => $this, 'back_url' => $backUrl),
                $storeId
            );

            $this->_sendEmailTemplateRegistered(
                "customer_create_account_pw_email_template",
                self::XML_PATH_REGISTER_EMAIL_IDENTITY,
                array('customer' => $this, 'back_url' => $backUrl),
                $storeId
            );
        }else{
            $this->_sendEmailTemplate(
                $types[$type],
                self::XML_PATH_REGISTER_EMAIL_IDENTITY,
                array('customer' => $this, 'back_url' => $backUrl),
                $storeId
            );
        }

        
        $this->cleanPasswordsValidationData();

        return $this;
    }

    /**
     * Send corresponding email template
     *
     * @param string $template configuration path of email template
     * @param string $sender configuration path of email identity
     * @param array $templateParams
     * @param int|null $storeId
     * @param string|null $customerEmail
     * @return $this
     */
    protected function _sendEmailTemplate($template, $sender, $templateParams = array(), $storeId = null, $customerEmail = null)
    {
        $customerEmail = ($customerEmail) ? $customerEmail : $this->getEmail();
        /** @var Mage_Core_Model_Email_Template_Mailer $mailer */
        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($customerEmail, $this->getName());
        $mailer->addEmailInfo($emailInfo);

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig($sender, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId(Mage::getStoreConfig($template, $storeId));
        $mailer->setTemplateParams($templateParams);
        $mailer->send();
        return $this;
    }

    protected function _sendEmailTemplateRegistered($template, $sender, $templateParams = array(), $storeId = null, $customerEmail = null)
    {
        $customerEmail = ($customerEmail) ? $customerEmail : $this->getEmail();
        /** @var Mage_Core_Model_Email_Template_Mailer $mailer */
        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($customerEmail, $this->getName());
        $mailer->addEmailInfo($emailInfo);

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig($sender, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($template);
        $mailer->setTemplateParams($templateParams);
        $mailer->send();
        return $this;
    }
}
