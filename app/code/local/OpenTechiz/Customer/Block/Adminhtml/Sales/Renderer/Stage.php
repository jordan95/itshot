<?php

class OpenTechiz_Customer_Block_Adminhtml_Sales_Renderer_Stage extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $order_stage = $row->getData('order_stage');
        $internal_status = $row->getData('internal_status');
        if(isset(OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE[$order_stage])){
            $order_stage = OpenTechiz_SalesExtend_Helper_Data::ORDER_STAGE[$order_stage];
        }else{
             $order_stage = '';
        }
        $status = OpenTechiz_SalesExtend_Helper_Data::ORDER_INTERNAL_STATUS;
        if($row->getData('order_type') == 1){
            $status = OpenTechiz_SalesExtend_Helper_Data::LAYAWAY_ORDER_INTERNAL_STATUS;
        }
        if($row->getData('order_type') == 2){
            $status = OpenTechiz_SalesExtend_Helper_Data::REPAIR_ORDER_INTERNAL_STATUS;
        }
        $internal_status = isset($status[$internal_status]) ? $status[$internal_status] : "";
        if($order_stage !='' || $internal_status !=''){
            return $order_stage." > ".$internal_status;
        }
        return '';
    }

}