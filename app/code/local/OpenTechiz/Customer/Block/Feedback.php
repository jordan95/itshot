<?php
/**
 * Created by jordan.

 */

class OpenTechiz_Customer_Block_Feedback extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $studioforty9_recaptcha = Mage::getSingleton('core/layout')->createBlock('studioforty9_recaptcha/explicit');
        $studioforty9_recaptcha->setTemplate('studioforty9/recaptcha/explicit.phtml');
        $this->setChild('studioforty9.recaptcha.explicit', $studioforty9_recaptcha);
    }

    public function getRecaptchaSiteKey()
    {
        return $this->getCustomerHelper()->getRecaptchaSiteKey();
    }

    public function getFormAction()
    {
        return Mage::getUrl('customerfeedback/index/post');
    }

    /**
     * @return \Medialounge_Customer_Helper_Data
     */
    private function getCustomerHelper()
    {
        return Mage::helper('medialounge_customer');
    }

    /**
     * Retrieve adminhtml session model object
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }
    public function getDataContactForm($field){
        $fieldValue = '';
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            return '';
        }
        $feedbackData = Mage::getSingleton('customer/session')->getFeedbackData();
        if (isset($feedbackData[$field])) {
            $fieldValue = $feedbackData[$field];
        }
        return $fieldValue;
    }
}