<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('customer_entity'),'has_multi_address', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Has Multi Address'
    ));

$installer->endSetup();