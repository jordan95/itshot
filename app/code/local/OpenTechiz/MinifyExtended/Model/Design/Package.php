<?php

class OpenTechiz_MinifyExtended_Model_Design_Package extends Mage_Core_Model_Design_Package
{
    protected $_minifiedDir;

    public function getMergedJsUrl($files)
    {
        $targetDir = $this->_initMergerDir('js');
        if (!$targetDir) {
            return '';
        }
        // merge into target file
        $targetFilename = $this->getOuputFileName($files,".js");
        $targetFile = $targetDir . DS . $targetFilename;
        $shouldMerge = $this->shouldMerge($files, $targetFile);
        $mergeFilesResult = $this->_mergeFiles($files, $targetFile, false, null, 'js');

        if ($mergeFilesResult) {
            // minify js
            if (Mage::getStoreConfig("dev/js/minify_js_files") && $shouldMerge) {
                $this->minifyJs($targetDir, $targetFilename);
            }

            // return URL path js file
            return Mage::getBaseUrl('media', Mage::app()->getRequest()->isSecure()) . 'js/' . $targetFilename . '?v=' . filemtime($targetFile);
        }
        return '';
    }

    public function getMergedCssUrl($files)
    {
        // secure or unsecure
        $isSecure = Mage::app()->getRequest()->isSecure();
        $mergerDir = $isSecure ? 'css_secure' : 'css';
        $targetDir = $this->_initMergerDir($mergerDir);
        if (!$targetDir) {
            return '';
        }

        // merge into target file
        $targetFilename = $this->getOuputFileName($files);
        $targetFile = $targetDir . DS . $targetFilename;
        $shouldMerge = $this->shouldMerge($files, $targetFile);
        $mergeFilesResult = $this->_mergeFiles(
                $files, $targetFile, false, array($this, 'beforeMergeCss'), 'css'
        );

        if ($mergeFilesResult) {
            // minify css
            if (Mage::getStoreConfig("dev/css/minify_css_files") && $shouldMerge) {
                $this->minifyCss($targetDir, $targetFilename);
            }

            // return URL path css file
            return Mage::getBaseUrl('media', $isSecure) . $mergerDir . '/' . $targetFilename . '?v=' . filemtime($targetFile);
        }
        return '';
    }

    protected function shouldMerge($srcFiles, $targetFile)
    {
        $shouldMerge = !$targetFile;
        if (!file_exists($targetFile)) {
            $shouldMerge = true;
        } else {
            $targetMtime = filemtime($targetFile);
            foreach ($srcFiles as $file) {
                if (!file_exists($file) || @filemtime($file) > $targetMtime) {
                    $shouldMerge = true;
                    break;
                }
            }
        }
        return $shouldMerge;
    }
    
    function getOuputFileName($files, $ext = ".css")
    {
        // secure or unsecure
        $isSecure = Mage::app()->getRequest()->isSecure();
        // base hostname & port
        $baseMediaUrl = Mage::getBaseUrl('media', $isSecure);
        $hostname = parse_url($baseMediaUrl, PHP_URL_HOST);
        $port = parse_url($baseMediaUrl, PHP_URL_PORT);
        if (false === $port) {
            $port = $isSecure ? 443 : 80;
        }
        $replace_basedir = Mage::getBaseDir();
        $nomralized_files = str_replace($replace_basedir, "", $files);
        // merge into target file
        return md5(implode(',', $nomralized_files) . "|{$hostname}|{$port}") . $ext;
    }

    protected function minifyJs($targetDir, $targetFilename)
    {
        $cssMergedFile = "js_merged.log";
        $filePath = $targetDir . DS . $targetFilename;
        /* @var $helper Apptrian_Minify_Helper_Data */
        Mage::log("================JS File: " . $targetFilename . "================", null, $cssMergedFile);
        $startTime = time();
        Mage::log("Begin time:" . date("Y-d-m H:i:s", $startTime), null, $cssMergedFile);
        $helper = Mage::helper('apptrian_minify');
        $unoptimized = file_get_contents($filePath);
        $optimized = $helper->minifyJs($unoptimized);
        file_put_contents($filePath, $optimized);
        $endTime = time();
        $duration = $endTime - $startTime;
        Mage::log("End time:" . date("Y-d-m H:i:s", $endTime), null, $cssMergedFile);
        Mage::log("Duration:" . date("H:i:s", $duration), null, $cssMergedFile);
        Mage::log("====================================================", null, $cssMergedFile);
    }

    protected function minifyCss($targetDir, $targetFilename)
    {
        $cssMergedFile = "css_merged.log";
        $filePath = $targetDir . DS . $targetFilename;
        /* @var $helper Apptrian_Minify_Helper_Data */
        Mage::log("================CSS File: " . $targetFilename . "================", null, $cssMergedFile);
        $startTime = time();
        Mage::log("Begin time:" . date("Y-d-m H:i:s", $startTime), null, $cssMergedFile);
        $helper = Mage::helper('apptrian_minify');
        $unoptimized = file_get_contents($filePath);
        $optimized = $helper->minifyCss($unoptimized);
        file_put_contents($filePath, $optimized);
        $endTime = time();
        $duration = $endTime - $startTime;
        Mage::log("End time:" . date("Y-d-m H:i:s", $endTime), null, $cssMergedFile);
        Mage::log("Duration:" . date("H:i:s", $duration), null, $cssMergedFile);
        Mage::log("====================================================", null, $cssMergedFile);
    }

    public function getVersions()
    {
        $key = OpenTechiz_MinifyExtended_Helper_Apptrian_Data::FLAG_MINIFY_VESRION;
        if (!Mage::registry($key)) {
            $flag = Mage::getModel('core/flag', array('flag_code' => $key))->loadSelf();
            if (!$flag->getId()) {
                $time = time();
                $flag->setState(1);
                $flag->save();
            } else {
                $time = strtotime($flag->getData('last_update'));
            }
            Mage::register($key, $time);
        }
        return Mage::registry($key);
    }

    /**
     * @param string $sourceFile
     * @param string $filename
     * @return bool
     */
    public function minify($sourceFile, $filename)
    {
        if (!file_exists($sourceFile) || stripos($sourceFile, '.min')) {
            return false;
        }
        if ($this->_minifiedDir === null) {
            $this->_minifiedDir = $this->_initMergerDir('minified');
        }
        if (!$this->_minifiedDir) {
            return false;
        }
        $targetFile = $this->_minifiedDir . DS . $filename;
        if (file_exists($targetFile) && filemtime($targetFile) >= filemtime($sourceFile)) {
            return true;
        }
        $removeComments = (bool)Mage::getStoreConfig('apptrian_minify/minify_css_js/remove_comments');
        try {
            /* @var $helper Apptrian_Minify_Helper_Data */
            $helper = Mage::helper('apptrian_minify');
            $ext = strtolower(
                pathinfo($sourceFile, PATHINFO_EXTENSION)
            );
            if ($ext == 'js') {
                $unoptimized = file_get_contents($sourceFile);
                $optimized = $helper->minifyJs($unoptimized, $removeComments);
            } else {
                return false;
            }
            $parts = explode('/', $filename);
            array_pop($parts);
            $dir = $this->_minifiedDir;
            foreach($parts as $part) {
                if (!is_dir($dir .= "/$part")) {
                    mkdir($dir);
                }
            }
            return (bool)file_put_contents($targetFile, $optimized);
        } catch (\Exception $e) {
            return false;
        }
    }
}
