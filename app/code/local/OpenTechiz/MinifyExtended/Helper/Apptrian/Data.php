<?php

class OpenTechiz_MinifyExtended_Helper_Apptrian_Data extends Apptrian_Minify_Helper_Data
{

    const FLAG_MINIFY_VESRION = 'minifycssjs_version';

    public function process()
    {
        // Get remove important comments option
        $removeComments = (bool) Mage::getConfig()->getNode(
                        'apptrian_minify/minify_css_js/remove_comments', 'default'
        );
        @file_get_contents(Mage::getBaseUrl(), false, stream_context_create());
        $flag = Mage::getModel('core/flag', array('flag_code' => self::FLAG_MINIFY_VESRION))->loadSelf();
        $flag->setState(1);
        $flag->save();

        foreach ($this->getPaths() as $path) {
            $iterator = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator(
                    $path, RecursiveDirectoryIterator::FOLLOW_SYMLINKS
                    )
            );

            foreach ($iterator as $filename => $file) {
                if ($file->isFile() && preg_match('/^.+\.(css|js)$/i', $file->getFilename())
                ) {
                    $filePath = $file->getRealPath();
                    if (!is_writable($filePath)) {
                        Mage::log(
                                'Minification failed for '
                                . $filePath . ' File is not writable.'
                        );
                        continue;
                    }

                    //This is available from php v5.3.6
                    //$ext = $file->getExtension();
                    // Using this for compatibility
                    $ext = strtolower(
                            pathinfo($filePath, PATHINFO_EXTENSION)
                    );
                    $optimized = '';
                    $unoptimized = Mage::helper('feedexport/io')->read($filePath);

                    // If it is 0 byte file or cannot be read
                    if (!$unoptimized) {
                        Mage::log('File ' . $filePath . ' cannot be read.');
                        continue;
                    }

                    // CSS files
                    if ($ext == 'css') {
                        $optimized = $this->minifyCss(
                                $unoptimized, $removeComments
                        );
                        // JS files
                    } else {
                        $optimized = $this->minifyJs(
                                $unoptimized, $removeComments
                        );
                    }

                    // If optimization failed
                    if (!$optimized) {
                        Mage::log('File ' . $filePath . ' was not minified.');
                        continue;
                    }
                    Mage::helper('feedexport/io')->write($filePath, $optimized);
                }
            }
        }
    }

}
