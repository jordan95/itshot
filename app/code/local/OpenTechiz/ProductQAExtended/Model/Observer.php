<?php

class OpenTechiz_ProductQAExtended_Model_Observer
{

    public function update(Varien_Event_Observer $observer){
        
        $faq = $observer->getEvent()->getObject();
        $product_id = $faq->getProductId();
        $product = Mage::getModel("catalog/product")->load($product_id);

        if (!empty($faq->getAnswer()) && $faq->getStatus()=="public") {
            if($product->getId()){
                $product->setUpdatedAt(date("Y-m-d H:i:s"));
                $product->save();
            }
            
        }
        

    }
}
