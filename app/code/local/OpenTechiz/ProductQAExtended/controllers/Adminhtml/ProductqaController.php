<?php
require_once(Mage::getModuleDir('controllers','Medialounge_ProductQA').DS.'Adminhtml'.DS.'ProductqaController.php');
class OpenTechiz_ProductQAExtended_Adminhtml_ProductqaController extends Medialounge_ProductQA_Adminhtml_ProductqaController
{
 
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/catalog/pws_productqa');
    }
    
}    
