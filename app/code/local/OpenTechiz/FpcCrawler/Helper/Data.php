<?php

class OpenTechiz_FpcCrawler_Helper_Data extends Mage_Core_Helper_Abstract
{

    // Crawler Status
    const STATUS_RESPONE_ERROR = 0;
    const STATUS_RESPONE_SUCCESS = 1;
    const STATUS_RESPONE_NOT_FOUND = 2;
    const STATUS_RESPONE_301_REDIRECT = 3;
    const STATUS_RESPONE_302_REDIRECT = 4;
    const STATUS_RESPONE_CACHE_ID_INVALID = 5;
    // Crawler Version
    const VERSION_NON_US = 0;
    const VERSION_US = 1;

    public function getStatusCode($content)
    {
        if (strpos($content, '404 Not Found') !== false) {
            $statusCode = self::STATUS_RESPONE_NOT_FOUND;
        } else if (strpos($content, '301 Moved Permanently') !== false) {
            $statusCode = self::STATUS_RESPONE_301_REDIRECT;
        } else if (strpos($content, 'HTTP/1.1 302 Found') !== false) {
            $statusCode = self::STATUS_RESPONE_302_REDIRECT;
        } elseif (strpos($content, 'HTTP/1.1 200 OK') !== false) {
            $statusCode = self::STATUS_RESPONE_SUCCESS;
        } else {
            $statusCode = self::STATUS_RESPONE_ERROR;
        }
        return $statusCode;
    }

    public function getStatusCodeAsText($id)
    {
        $data = $this->getAllStatusCode();
        return !empty($data[$id]) ? $data[$id] : '';
    }

    public function getAllStatusCode()
    {
        return array(
            self::STATUS_RESPONE_SUCCESS => Mage::helper('adminhtml')->__('Success'),
            self::STATUS_RESPONE_ERROR => Mage::helper('adminhtml')->__('Error'),
            self::STATUS_RESPONE_NOT_FOUND => Mage::helper('adminhtml')->__('404 Not Found'),
            self::STATUS_RESPONE_301_REDIRECT => Mage::helper('adminhtml')->__('301 Moved Permanently'),
            self::STATUS_RESPONE_302_REDIRECT => Mage::helper('adminhtml')->__('HTTP/1.1 302 Found'),
            self::STATUS_RESPONE_CACHE_ID_INVALID => Mage::helper('adminhtml')->__('FPC Cache ID Invalid'),
        );
    }

    public function getCurrentVersion()
    {
        if (empty($_SERVER['GEOIP_COUNTRY_CODE'])) {
            return self::VERSION_NON_US;
        } else {
            return $_SERVER['GEOIP_COUNTRY_CODE'] == 'US' ? self::VERSION_US : self::VERSION_NON_US;
        }
    }

    public function getVersionAsText($id)
    {
        $data = $this->getAllVersions();
        return !empty($data[$id]) ? $data[$id] : '';
    }

    public function getAllVersions()
    {
        return array(
            self::VERSION_US => Mage::helper('adminhtml')->__('US'),
            self::VERSION_NON_US => Mage::helper('adminhtml')->__('Non US')
        );
    }

    public function getFPCCrawlerUrlCollection($storeId = null)
    {
        $sortByPageTypeCount = count(Mage::getSingleton('fpccrawler/config')->getSortByPageType());
        $sortByProductAttributeCount = count(Mage::getSingleton('fpccrawler/config')->getSortByProductAttribute());
        $sortCrawlerUrls = Mage::getSingleton('fpccrawler/config')->getSortCrawlerUrls();
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        /* @var $select ClassName */
        $select = $read->select()
                ->from($resource->getTableName('fpccrawler/crawler_url', array('*')));
        $storeId = ($storeId && strpos($storeId, ',') !== false) ? explode(',', $storeId) : $storeId;
        if ($storeId) {
            $select->where('store_id IN(?)', $storeId);
        }

        $select->order('need_to_warm desc');
        if ($sortCrawlerUrls == 'custom_order' && $sortByPageTypeCount > 0 && $sortByProductAttributeCount > 0) {
            $select->order(Mage::helper('fpccrawler')->getOrderSql(true));
        } elseif ($sortCrawlerUrls == 'custom_order' && $sortByPageTypeCount > 0) {
            $select->order(Mage::helper('fpccrawler')->getOrderSql(false));
        } elseif ($sortCrawlerUrls == 'custom_order' && $sortByProductAttributeCount > 0) {
            $select->order(array('sort_by_product_attribute asc', 'rate desc'));
        } else {
            $select->order('rate desc');
        }
        $select->limit(200000);

        return $read->query($select);
    }

    public function purgeVarnish($singlePurgeUrl)
    {
        $helper = Mage::helper('mgt_varnish');
        if ($helper->isEnabled()) {
            $domainList = $helper->getStoreDomainList();
            extract(parse_url($singlePurgeUrl));
            if (!isset($host)) {
                throw new Mage_Core_Exception($helper->__('Invalid URL "%s".', $singlePurgeUrl));
            }
            if (!in_array($host, explode('|', $domainList))) {
                throw new Mage_Core_Exception($helper->__('Invalid domain "%s".', $host));
            }
            $uri = '';
            if (isset($path)) {
                $uri .= $path;
            }
            if (isset($query)) {
                $uri .= '\?';
                $uri .= $query;
            }
            if (isset($fragment)) {
                $uri .= '#';
                $uri .= $fragment;
            }
            $varnish = $this->_getVarnishModel();
            $varnish->purge($host, sprintf('^%s$', $uri));
        }
    }

    protected function _getVarnishModel()
    {
        return Mage::getModel('mgt_varnish/varnish');
    }

}
