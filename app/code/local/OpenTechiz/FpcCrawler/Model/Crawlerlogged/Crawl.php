<?php

/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_fpc
 * @version   1.0.87
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */
class OpenTechiz_FpcCrawler_Model_Crawlerlogged_Crawl extends Mirasvit_FpcCrawler_Model_Crawlerlogged_Crawl
{

    //multithreads
    public function requestUrls($urls, $verbose = true, $userAgent, $htaccessAuth = false, $verifyPeer)
    {
        $multiResult = array();

        if (function_exists('curl_multi_init')) {
            $adapter = new Varien_Http_Adapter_Curl();
            $options = array(
                CURLOPT_USERAGENT => $userAgent,
                CURLOPT_HEADER => true,
                CURLOPT_SSL_VERIFYPEER => $verifyPeer,
                CURLOPT_USERPWD => ($htaccessAuth) ? $htaccessAuth : null,
                CURLOPT_COOKIE => "MGT_NO_CACHE=1",
            );

            $multiResult = $adapter->multiRequest($urls, $options);
        } else {
            ini_set('user_agent', $userAgent);
            foreach ($urls as $urlId => $url) {
                $multiResult[$urlId] = implode(PHP_EOL, get_headers($url));
            }
        }

        foreach ($multiResult as $urlId => $content) {
            $urlModel = Mage::getModel('fpccrawler/crawlerlogged_url')->load($urlId);
            $this->_removeDublicates($urlModel);
            $matches = array();
            $statusCode = Mage::helper("opentechiz_fpccrawler")->getStatusCode($content);
            if ($statusCode == OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_SUCCESS) {
                preg_match('/Fpc-Cache-Id: (' . Mirasvit_Fpc_Model_Config::REQUEST_ID_PREFIX . '[a-z0-9]{32})/', $content, $matches);
                if (count($matches) == 2) {
                    $cacheId = $matches[1];
                    $cacheIdInvalid = false;
                    if ($urlModel->getCacheId() != $cacheId) {
                        $urlModel->setCacheId($cacheId);
                    }
                } else {
                    $statusCode = OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_CACHE_ID_INVALID;
                }
            }
            if ($statusCode != OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_SUCCESS) {
                $urlModel->setRate(0); //log URL problematic and set lower rate to avoid repeat crawler
            }
            if ($urlModel->getNeedToWarm()) {
                $urlModel->warmCache()
                        ->setNeedToWarm(0);
            }
            $urlModel->setStatusRespone($statusCode)
                    ->save();
        }

        return $this;
    }

}
