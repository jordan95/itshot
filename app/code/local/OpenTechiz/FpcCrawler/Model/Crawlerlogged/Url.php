<?php

/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_fpc
 * @version   1.0.87
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */
class OpenTechiz_FpcCrawler_Model_Crawlerlogged_Url extends Mirasvit_FpcCrawler_Model_Crawlerlogged_Url
{

    public function saveUrl($line, $rate = 1)
    {
        if (count($line) < 9) {
            return $this;
        }

        $url = $line[2];
        $cacheId = preg_replace('/\s+/', ' ', trim($line[3]));
        $customerGroupId = trim($line[6]);
        $storeId = trim($line[7]);
        $currency = trim($line[8]);
        $mobileGroup = trim($line[9]);
        $helper = Mage::helper("opentechiz_fpccrawler");
        
        $collection = $this->getCollection();
        $collection->getSelect()->where('url = ?', $url)
                ->where('customer_group_id = ?', $customerGroupId)
                ->where('store_id = ?', $storeId)
                 ->where('version = ?', $helper->getCurrentVersion())
                ->where('currency = ?', $currency)
                ->where('mobile_group = ?', $mobileGroup)
        ;
        $model = $collection->getFirstItem();
        
        try {
            if (trim($cacheId) != '') {
                $model->setCacheId($cacheId)
                        ->setUrl($url)
                        ->setRate(intval($model->getRate()) + $rate)
                        ->setSortByPageType(trim($line[4]))
                        ->setCustomerGroupId($customerGroupId)
                        ->setStoreId($storeId)
                        ->setVersion($helper->getCurrentVersion())
                        ->setCurrency($currency)
                        ->setMobileGroup($mobileGroup)
                ;
                if (isset($line[5])) {
                    $model->setSortByProductAttribute(trim($line[5]))
                            ->save();
                } else {
                    $model->save();
                }
            } elseif ($model->getId()) {
                $model->setRate(intval($model->getRate()) + $rate)
                        ->save();
            }
        } catch (Exception $e) {
            
        }

        return $this;
    }

    public function warmCache()
    {
        $storeCrawlerConfig = $this->_getStoreCrawlerConfig($this->getStoreId());
        if ($storeCrawlerConfig && $storeCrawlerConfig->getIsDeleteCrawlerUrls()) {
            $this->delete();

            return false;
        } elseif ($storeCrawlerConfig && !$storeCrawlerConfig->getIsCrawlerEnabled()) {
            return false;
        }
        $this->clearCache(); //we should always clear cache before warming cache
        $url = $this->getUrl();
        $customerGroupId = $this->getCustomerGroupId();
        if (!Mage::getSingleton('fpccrawler/config')->isAllowedGroup($customerGroupId)) {
            $this->delete();

            return $this;
        }
        $userAgent = Mage::helper('fpccrawler')->getUserAgent($customerGroupId, $this->getStoreId(), $this->getCurrency());
        $content = '';
        $helper = Mage::helper("opentechiz_fpccrawler");
        if (function_exists('curl_multi_init')) {
            $adapter = new Varien_Http_Adapter_Curl();
            $options = array(
                CURLOPT_USERAGENT => $userAgent,
                CURLOPT_HEADER => true,
                CURLOPT_SSL_VERIFYPEER => self::$_verifyPeer,
                CURLOPT_USERPWD => (self::$_htaccessAuth) ? self::$_htaccessAuth : null,
                CURLOPT_COOKIE => "MGT_NO_CACHE=1",
            );
            if($this->getVersion() == OpenTechiz_FpcCrawler_Helper_Data::VERSION_US) {
                $options[CURLOPT_COOKIE] = "MGT_NO_CACHE=1; version=US";
            }


            $content = $adapter->multiRequest(array($url), $options);
            $content = $content[0];
        } else {
            ini_set('user_agent', $userAgent);
            $content = implode(PHP_EOL, get_headers($url));
        }
        $statusCode = $helper->getStatusCode($content);
        if ($statusCode == OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_SUCCESS) {
            preg_match('/Fpc-Cache-Id: (' . Mirasvit_Fpc_Model_Config::REQUEST_ID_PREFIX . '[a-z0-9]{32})/', $content, $matches);
            if (count($matches) == 2) {
                $cacheId = $matches[1];
                if ($this->getCacheId() != $cacheId) {
                    $this->setCacheId($cacheId);
                }
            } else {
                $statusCode = OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_CACHE_ID_INVALID;
            }
        }
        if ($statusCode != OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_SUCCESS) {
            $this->setRate(0); //log URL problematic and set lower rate to avoid repeat crawler
        }
        $this->setNeedToWarm(0)
                ->setStatusRespone($statusCode)
                ->save();
        $this->warmMobileUrls($userAgent, $url);

        // purge cache by varnish
        try {
            $this->purgeVarnish($url);
        } catch (Exception $exc) {
            
        }

        return $this;
    }

    public function isCacheExist()
    {
        return $this->isCached() && !$this->getNeedToWarm();
    }

    public function isCached()
    {
        $cache = Mirasvit_Fpc_Model_Cache::getCacheInstance();
        $cacheId = $this->getCacheId();

        if (is_string($cacheId) && $cache->load($cacheId)) {
            return true;
        }

        return false;
    }

    protected function purgeVarnish($url)
    {
        return Mage::helper("opentechiz_fpccrawler")->purgeVarnish($url);
    }

}
