<?php

class OpenTechiz_FpcCrawler_Model_Cron
{

    public function needToWarmCrawler()
    {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $select = $read->select()
                ->from($resource->getTableName('fpccrawler/crawler_url', array('url_id')));
        $select->where("need_to_warm = 1");
        $select->order('rate desc');
        $select->limit(50);
        $urlIDs = $read->fetchCol($select);
        foreach ($urlIDs as $id) {
            $crawlerURL = Mage::getModel('fpccrawler/crawler_url')->load($id);
            if (!$crawlerURL->getId()) {
                continue;
            }
            $crawlerURL->warmCache();
        }
    }

    public function needToWarmCrawlerLogged()
    {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $select = $read->select()
                ->from($resource->getTableName('fpccrawler/crawlerlogged_url', array('url_id')));
        $select->where("need_to_warm = 1");
        $select->order('rate desc');
        $select->limit(50);
        $urlIDs = $read->fetchCol($select);
        foreach ($urlIDs as $id) {
            $crawlerURL = Mage::getModel('fpccrawler/crawlerlogged_url')->load($id);
            if (!$crawlerURL->getId()) {
                continue;
            }
            $crawlerURL->warmCache();
        }
    }

}
