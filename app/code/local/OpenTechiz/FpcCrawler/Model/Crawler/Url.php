<?php

/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_fpc
 * @version   1.0.87
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */
class OpenTechiz_FpcCrawler_Model_Crawler_Url extends Mirasvit_FpcCrawler_Model_Crawler_Url
{

    public function saveUrl($line, $rate = 1)
    {
        if (count($line) < 9) {
            return $this;
        }

        $url = $line[2];
        $cacheId = preg_replace('/\s+/', ' ', trim($line[3]));
        $storeId = trim($line[7]);
        $currency = trim($line[8]);
        $mobileGroup = trim($line[9]);
        $helper = Mage::helper("opentechiz_fpccrawler");

        $collection = $this->getCollection();
        $collection->getSelect()->where('url = ?', $url)
                ->where('store_id = ?', $storeId)
                ->where('currency = ?', $currency)
                ->where('version = ?', $helper->getCurrentVersion())
                ->where('mobile_group = ?', $mobileGroup)
        ;
        $model = $collection->getFirstItem();

        try {
            if (trim($cacheId) != '') {
                $model->setCacheId($cacheId)
                        ->setUrl($url)
                        ->setRate(intval($model->getRate()) + $rate)
                        ->setSortByPageType(trim($line[4]))
                        ->setStoreId($storeId)
                        ->setVersion($helper->getCurrentVersion())
                        ->setCurrency($currency)
                        ->setMobileGroup($mobileGroup)
                ;
                if (isset($line[5])) {
                    $model->setSortByProductAttribute(trim($line[5]))
                            ->save();
                } else {
                    $model->save();
                }
            } elseif ($model->getId()) {
                $model->setRate(intval($model->getRate()) + $rate)
                        ->save();
            }
        } catch (Exception $e) {
            
        }

        return $this;
    }

    public function warmCache($showError = false)
    {
        $storeCrawlerConfig = $this->_getStoreCrawlerConfig($this->getStoreId());
        if ($storeCrawlerConfig && $storeCrawlerConfig->getIsDeleteCrawlerUrls()) {
            $this->delete();

            return false;
        } elseif ($storeCrawlerConfig && !$storeCrawlerConfig->getIsCrawlerEnabled()) {
            return false;
        }
        $this->clearCache(); //we should always clear cache before warming cache
        $url = $this->getUrl();
        $userAgent = Mage::helper('fpccrawler')->getUserAgent(null, $this->getStoreId(), $this->getCurrency());
        $content = '';
        $helper = Mage::helper("opentechiz_fpccrawler");
        if (function_exists('curl_multi_init')) {
            $adapter = new Varien_Http_Adapter_Curl();
            $options = array(
                CURLOPT_USERAGENT => $userAgent,
                CURLOPT_HEADER => true,
                CURLOPT_SSL_VERIFYPEER => self::$_verifyPeer,
                CURLOPT_USERPWD => (self::$_htaccessAuth) ? self::$_htaccessAuth : null,
                CURLOPT_COOKIE => "MGT_NO_CACHE=1",
            );
            if($this->getVersion() == OpenTechiz_FpcCrawler_Helper_Data::VERSION_US) {
                $options[CURLOPT_COOKIE] = "MGT_NO_CACHE=1; version=US";
            }

            $content = $adapter->multiRequest(array($url), $options);
            $content = $content[0];
            if ($showError && !$content && strpos($url, 'https://') !== false && ($curlError = $this->_getCurlError($url))) {
                $this->setFpcCrawlerError($curlError); //show this error in  Mirasvit_FpcCrawler_Adminhtml_Fpccrawler_UrlController
            }
        } else {
            ini_set('user_agent', $userAgent);
            $content = implode(PHP_EOL, get_headers($url));
        }

        $statusCode = $helper->getStatusCode($content);
        if ($statusCode == OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_SUCCESS) {
            preg_match('/Fpc-Cache-Id: (' . Mirasvit_Fpc_Model_Config::REQUEST_ID_PREFIX . '[a-z0-9]{32})/', $content, $matches);
            if (count($matches) == 2) {
                $cacheId = $matches[1];
                if ($this->getCacheId() != $cacheId) {
                    $this->setCacheId($cacheId);
                }
            } else {
                $statusCode = OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_CACHE_ID_INVALID;
            }
        }
        if ($statusCode != OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_SUCCESS) {
            $this->setRate(0); //log URL problematic and set lower rate to avoid repeat crawler
        }
        if ($showError && $statusCode != OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_SUCCESS) {
            if ($statusCode != OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_CACHE_ID_INVALID) {
                $error = $this->_getCrawlerError($content);
                $this->setFpcCrawlerError($error);
            } else {
                $this->setFpcCrawlerError('Full Page Cache can\'t add "Fpc-Cache-Id" in page header.');
            }
        }
        $this->setNeedToWarm(0)
                ->setStatusRespone($statusCode)
                ->save();
        $this->warmMobileUrls($userAgent, $url);

        // purge cache by varnish
        try {
            $this->purgeVarnish($url);
        } catch (Exception $exc) {
            
        }

        return $this;
    }

    public function isCacheExist()
    {
        return $this->isCached() && !$this->getNeedToWarm();
    }

    public function isCached()
    {
        $isCached = false;
        $cache = Mirasvit_Fpc_Model_Cache::getCacheInstance();
        $cacheId = $this->getCacheId();

        if (is_string($cacheId) && $cache->load($cacheId)) {
            $isCached = true;
        }
        return $isCached;
    }

    protected function purgeVarnish($url)
    {
        return Mage::helper("opentechiz_fpccrawler")->purgeVarnish($url);
    }

}
