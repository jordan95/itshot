<?php

require_once(Mage::getModuleDir('controllers', 'Mirasvit_FpcCrawler') . DS . 'Adminhtml' . DS . 'Fpccrawlerlogged' . DS . 'UrlController.php');

class OpenTechiz_FpcCrawler_Adminhtml_Fpccrawlerlogged_UrlController extends Mirasvit_FpcCrawler_Adminhtml_Fpccrawlerlogged_UrlController
{

    public function needToWarmAction()
    {
        $ids = $this->getRequest()->getParam('url_id');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select urls(s)'));
        } else {
            try {
                $showError = (count($ids) == 1) ? true : false;
                foreach ($ids as $id) {
                    $model = Mage::getModel('fpccrawler/crawlerlogged_url')
                            ->load($id);
                    $model->setNeedToWarm(1);
                    $model->save();
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function rewarmAllAction()
    {
        $collection = Mage::helper("opentechiz_fpccrawler")->getFPCCrawlerUrlCollection();
        while ($row = $collection->fetch()) {
            $urlModel = Mage::getModel('fpccrawler/crawlerlogged_url')->load($row['url_id']);
            $urlModel->setNeedToWarm(1);
            $urlModel->save();
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Re-warm all is successfully'));
        $this->_redirect('*/*/index');
    }

}
