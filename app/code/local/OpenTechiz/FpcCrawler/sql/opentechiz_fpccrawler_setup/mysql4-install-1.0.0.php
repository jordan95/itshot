<?php


$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('fpccrawler/crawler_url'),'status_respone', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'default'   =>1,
    'nullable'  => false,
    'length'    => 2,
    'after'     => 'status', // column name to insert new column after
    'comment'   => 'status respone'
    ));   
$installer->endSetup();