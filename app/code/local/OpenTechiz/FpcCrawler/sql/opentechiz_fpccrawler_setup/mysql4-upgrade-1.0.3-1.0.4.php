<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
        ->addColumn($installer->getTable('fpccrawler/crawler_url'), 'version', array(
            'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'default' => 0,
            'nullable' => false,
            'length' => 2,
            'after' => 'status_respone', // column name to insert new column after
            'comment' => 'Version'
        ));
$installer->getConnection()
        ->addColumn($installer->getTable('fpccrawler/crawlerlogged_url'), 'version', array(
            'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'default' => 0,
            'nullable' => false,
            'length' => 2,
            'after' => 'status_respone', // column name to insert new column after
            'comment' => 'Version'
        ));
$installer->endSetup();
