<?php


$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('fpccrawler/crawlerlogged_url'),'need_to_warm', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'default'   => 0,
    'nullable'  => false,
    'length'    => 2,
    'after'     => 'status_respone', // column name to insert new column after
    'comment'   => 'need to warm'
    ));   
$installer->getConnection()->addIndex($installer->getTable('fpccrawler/crawlerlogged_url'), 'need_to_warm', array('need_to_warm'));
$installer->endSetup();
