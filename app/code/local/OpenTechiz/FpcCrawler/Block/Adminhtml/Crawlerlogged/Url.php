<?php
class OpenTechiz_FpcCrawler_Block_Adminhtml_Crawlerlogged_Url extends Mirasvit_FpcCrawler_Block_Adminhtml_Crawlerlogged_Url
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_crawlerlogged_url';
        $this->_blockGroup = 'fpccrawler';
        $this->_headerText = Mage::helper('fpccrawler')->__('Crawler URLs for logged in users');

        parent::__construct();
        $this->_addButton('rewarm_all', array(
            'label'     => Mage::helper('sales')->__('Re-warm All'),
            'onclick'   => "location.href='".$this->getUrl('*/*/rewarmAll')."'",
            'class'     => '',
        ));
        $this->_removeButton('add');
    }

}
