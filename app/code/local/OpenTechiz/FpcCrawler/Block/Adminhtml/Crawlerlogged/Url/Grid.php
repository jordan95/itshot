<?php

/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_fpc
 * @version   1.0.87
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */
class OpenTechiz_FpcCrawler_Block_Adminhtml_Crawlerlogged_Url_Grid extends Mirasvit_FpcCrawler_Block_Adminhtml_Crawlerlogged_Url_Grid
{

    protected function _prepareMassaction()
    {
        parent::_prepareMassaction();

        $this->getMassactionBlock()->addItem('need_to_warm', array(
            'label' => Mage::helper('fpccrawler')->__('Need to warm'),
            'url' => $this->getUrl('*/*/needToWarm'),
        ));

        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('url_id', array(
            'header' => Mage::helper('fpccrawler')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'url_id',
                )
        );

        $this->addColumn('url', array(
            'header' => Mage::helper('fpccrawler')->__('URL'),
            'index' => 'url',
            'renderer' => 'Mirasvit_FpcCrawler_Block_Adminhtml_Crawler_Url_Grid_Renderer_Url',
                )
        );

        $this->addColumn('cache_id', array(
            'header' => Mage::helper('fpccrawler')->__('Cache Id'),
            'index' => 'cache_id',
                )
        );

        $this->addColumn('rate', array(
            'header' => Mage::helper('fpccrawler')->__('Popularity (number of visits)'),
            'index' => 'rate',
            'align' => 'right',
            'width' => '100px',
            'type' => 'number',
                )
        );

        $this->addColumn('sort_by_page_type', array(
            'header' => Mage::helper('fpccrawler')->__('Sort by page type'),
            'index' => 'sort_by_page_type',
            'width' => '150px',
                )
        );

        $this->addColumn('sort_by_product_attribute', array(
            'header' => Mage::helper('fpccrawler')->__('Sort by product attribute'),
            'index' => 'sort_by_product_attribute',
            'align' => 'right',
            'width' => '100px',
                )
        );

        $this->addColumn('customer_group', array(
            'header' => Mage::helper('fpccrawler')->__('Customer group'),
            'renderer' => 'Mirasvit_FpcCrawler_Block_Adminhtml_Crawlerlogged_Url_Grid_Renderer_Group',
            'filter_condition_callback' => array($this, '_customerGroupFilter'),
        ));

        $this->addColumn('store_id', array(
            'header' => Mage::helper('fpccrawler')->__('Store id'),
            'index' => 'store_id',
            'align' => 'right',
            'width' => '100px',
            'filter_condition_callback' => array($this, '_storeFilter'),
                )
        );

        $this->addColumn('currency', array(
            'header' => Mage::helper('fpccrawler')->__('Currency'),
            'index' => 'currency',
            'align' => 'right',
            'width' => '100px',
                )
        );
        
        $this->addColumn('version', array(
            'header' => Mage::helper('fpccrawler')->__('Version'),
            'index' => 'version',
            'align' => 'center',
            'width' => '100px',
            'type' => 'options',
            'options' => Mage::helper("opentechiz_fpccrawler")->getAllVersions(),
            )
        );

        $this->addColumn('cache_status', array(
            'header' => Mage::helper('fpccrawler')->__('Cache Status'),
            'index' => 'url',
            'renderer' => 'OpenTechiz_FpcCrawler_Block_Adminhtml_Crawler_Url_Grid_Renderer_Respone_Cache',
            'filter' => false,
            'sortable' => false,
                )
        );

        $this->addColumn('need_to_warm', array(
            'header' => Mage::helper('fpccrawler')->__('Re-warm Queue'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'need_to_warm',
            'type' => 'options',
            'options' => array(
                1 => Mage::helper('core')->__('Yes'),
                0 => Mage::helper('core')->__('No')
            ),
            'frame_callback' => array($this, 'decorateStatus')
                )
        );

        $this->addColumn('status_respone', array(
            'header' => Mage::helper('fpccrawler')->__('Respone Status'),
            'align' => 'right',
            'width' => '50px',
            'renderer' => 'OpenTechiz_FpcCrawler_Block_Adminhtml_Crawlerlogged_Url_Grid_Renderer_Respone_Status',
            'index' => 'status_respone',
            'type' => 'options',
            'options' => Mage::helper("opentechiz_fpccrawler")->getAllStatusCode(),
                )
        );
        $this->sortColumnsByOrder();
        return $this;
    }

    public function decorateStatus($value, $row, $column, $isExport)
    {
        if ($row->getNeedToWarm()) {
            $cell = '<span class="grid-severity-major"><span>' . $value . '</span></span>';
        } else {
            $cell = '<span class="grid-severity-notice"><span>' . $value . '</span></span>';
        }
        return $cell;
    }

}
