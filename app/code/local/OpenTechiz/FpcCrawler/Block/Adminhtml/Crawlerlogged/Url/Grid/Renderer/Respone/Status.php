<?php

class OpenTechiz_FpcCrawler_Block_Adminhtml_Crawlerlogged_Url_Grid_Renderer_Respone_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $status_respone = $row->getStatusRespone();
        $id = $row->getId();
        if ($status_respone == 0 && $row->isCacheExist()) {
            $urlModel = Mage::getModel('fpccrawler/crawlerlogged_url')->load($id);
            $urlModel->setStatusRespone(OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_SUCCESS)->save();
            $status_respone = OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_SUCCESS;
        }
        $statusCodeText = Mage::helper("opentechiz_fpccrawler")->getStatusCodeAsText($status_respone);
        return $status_respone == OpenTechiz_FpcCrawler_Helper_Data::STATUS_RESPONE_SUCCESS ?
                '<span style="background:grey;border-radius:10px" class="grid-severity-notice"><span style="background:grey;border-radius:10px">OK</span></span>' : '<span  class="grid-severity-critical"><span>'.$statusCodeText.'</span></span>';
    }

}
