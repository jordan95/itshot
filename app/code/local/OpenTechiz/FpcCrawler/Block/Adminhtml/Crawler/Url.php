<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_fpc
 * @version   1.0.87
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */



class OpenTechiz_FpcCrawler_Block_Adminhtml_Crawler_Url extends Mirasvit_FpcCrawler_Block_Adminhtml_Crawler_Url
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_crawler_url';
        $this->_blockGroup = 'fpccrawler';
        $this->_headerText = Mage::helper('fpccrawler')->__('Crawler URLs');

        parent::__construct();
        $this->_addButton('rewarm_all', array(
        'label'     => Mage::helper('sales')->__('Re-warm All'),
        'onclick'   => "location.href='".$this->getUrl('*/*/rewarmAll')."'",
        'class'     => '',
    ));
        $this->_removeButton('add');
    }

}
