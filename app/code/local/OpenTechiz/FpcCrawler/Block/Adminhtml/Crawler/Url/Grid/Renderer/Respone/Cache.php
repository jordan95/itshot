<?php

class OpenTechiz_FpcCrawler_Block_Adminhtml_Crawler_Url_Grid_Renderer_Respone_Cache extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{
    public function render(Varien_Object $row)
    {
        return $row->isCached() ?
            '<span class="grid-severity-notice"><span>In Cache</span></span>'
            : '<span class="grid-severity-major"><span>Pending</span></span>';
    }
}