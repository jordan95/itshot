<?php

class OpenTechiz_BackendNotification_Block_Adminhtml_NotifyLiveSite extends Mage_Core_Block_Template
{
    const WARNING_TEXT = 'YOU ARE ON THE PRODUCTION ENVIRONMENT';

    public function getWarningText(){
        return self::WARNING_TEXT;
    }

    public function isOnLiveSite(){
        if (strpos($_SERVER['HTTP_HOST'], 'itshot.com') !== FALSE
                && strpos($_SERVER['HTTP_HOST'], 'dev') === FALSE
                && strpos($_SERVER['HTTP_HOST'], 'test') === FALSE
                && strpos($_SERVER['HTTP_HOST'], 'stage') === FALSE
                && strpos($_SERVER['HTTP_HOST'], 'demo') === FALSE
                && strpos($_SERVER['HTTP_HOST'], 'local') === FALSE
                && strpos($_SERVER['HTTP_HOST'], '127.0.0.1') === FALSE
        ) {
            return true;
        }

        return false;
    }
}