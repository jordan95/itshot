<?php

class OpenTechiz_RewardsExtended_Helper_Transfer extends TBT_Rewards_Helper_Transfer
{

    public function getCatalogRewardsRuleIdsForProduct($productId, $wId = null, $gId = null)
    {
        $p = Mage::getModel('rewards/catalog_product')->load($productId);
        if ($p->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_DISABLED) {
            $rules = array(1);
        } else {
            $rules = $p->getCatalogRewardsRuleIdsForProduct($wId, $gId);
        }
        return $rules;
    }

}
