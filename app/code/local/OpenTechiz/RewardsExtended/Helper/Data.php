<?php

class OpenTechiz_RewardsExtended_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getRuleName($ruleIds)
    {
        $collection = Mage::getModel('salesrule/rule')->getCollection()->getSelect();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $tsht_salesrule = $resource->getTableName('salesrule');
        $tsht_salesrule_coupon = $resource->getTableName('salesrule_coupon');
        $select = $readConnection->select();
        $select->from(array('main_table' => $tsht_salesrule), array("name"));
        $select->joinLeft(array('coupon' => $tsht_salesrule_coupon), "main_table.rule_id = coupon.rule_id", array("code"));
        $select->where("main_table.rule_id IN(?)", $ruleIds);
        $ruleNames = "";
        $result = $readConnection->fetchAll($select);
        foreach ($result as $item) {
            if ($item['name'] == "Redeem Points") {
                continue;
            }
            if(!empty($item['name'])) {
                $ruleNames .= $item['name'] . " + ";
            } else if(!empty ($item['code'])){
                $ruleNames .= $item['code'] . " + ";
            }
        }
        $ruleNames = rtrim($ruleNames, " + ");
        return $ruleNames;
    }

}
