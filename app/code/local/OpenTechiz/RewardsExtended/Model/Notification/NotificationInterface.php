<?php

interface OpenTechiz_RewardsExtended_Model_Notification_NotificationInterface{
    public function getCustomerCollection();
    public function getAdapter();
}