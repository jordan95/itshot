<?php

abstract class OpenTechiz_RewardsExtended_Model_Notification_AbstractNotification implements OpenTechiz_RewardsExtended_Model_Notification_NotificationInterface
{

    protected $adapter;
    protected $_customerCollection;

    public function setAdapter(OpenTechiz_RewardsExtended_Model_Notification_Adapter_AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        return $this;
    }

    /**
     * 
     * @return OpenTechiz_RewardsExtended_Model_Notification_Adapter_AdapterInterface
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    public function notify()
    {
        $customerCollection = $this->getCustomerCollection();
        $adapter = $this->getAdapter();
        $i = 0;
        $MessageMail = '';
        $expiry_notification_date = $this->getAdapter()->getExpiryDate();
        foreach ($customerCollection as $c) {
            $total_earned_points = (int) $c->getData('total_earned_points');
            $total_spent_points = $c->getData('total_spent_points');
            if ($total_earned_points <= abs($total_spent_points)) {
                continue;
            }
            $customer_points_usable = $c->getData('customer_points_usable');
            $customer_id = $c->getData('customer_id');
            $customer = Mage::getModel('rewards/customer')->load($customer_id);
            if (!$customer->getId()) {
                continue;
            }
            $collection = Mage::getModel('emailnotify/emailnotify')->getCollection()->addFieldToFilter('email_address', $customer->getEmail())
            ->addFieldToFilter('email_description', array('in' => array('rewards_expire_email_custom_template','all')))->addFieldToFilter('status', 1);
            if(count($collection) > 0){
                continue;
            }
            $points_balance = $total_earned_points + $total_spent_points;
            $_customer = Mage::getModel('customer/customer')->load($customer_id);
            $MessageMail .= '<br/>S. No.: ' . $i++ . ', User Id: ' . $customer_id . ', Email: '. $_customer->getEmail() .',Expiry Notification Date: '.$expiry_notification_date.', Total Points: ' . number_format($customer_points_usable) . ', Earned Points On Date: ' . number_format($total_earned_points) . ', Spent Points after Date: ' . number_format($total_spent_points) . "<br/>";
            Mage::dispatchEvent('reward_point_notify_to_customer', array(
                'customer' => $customer,
                'adapter' => $adapter,
                'points_balance' => (string) $points_balance
            ));
        }
        Mage::dispatchEvent('maillog_send_email_to_admin', array(
            'message' => $MessageMail. "<br/><br/>Script ends at: ".date("d-m-Y H:i:s"),
            'subject' => $adapter->getSubjectTitle()
                )
        );
    }

    public function getCustomerCollection()
    {
        if (!$this->_customerCollection) {
            $status = TBT_Rewards_Model_Transfer_Status::STATUS_APPROVED;
            $expiry_notification_date = $this->getAdapter()->getExpiryDate();
            $collection = Mage::getModel('rewards/customer_indexer_points')
                    ->getCollection()
                    ->addFieldToSelect(array('customer_id', 'customer_points_usable'))
                    ->addExpressionFieldToSelect('total_earned_points', new Zend_Db_Expr("(SELECT SUM(quantity) FROM tsht_rewards_transfer WHERE customer_id = main_table.customer_id AND status_id = {$status} AND updated_at LIKE '%{$expiry_notification_date}%')"), array())
                    ->addExpressionFieldToSelect('total_spent_points', new Zend_Db_Expr("(SELECT SUM(quantity) FROM tsht_rewards_transfer WHERE customer_id = main_table.customer_id AND status_id = {$status} AND updated_at > '{$expiry_notification_date} 23:59:59' AND quantity < '0')"), array())
                    ->addFieldToFilter('customer_points_usable', array('gt' => 0));
            $collection->getSelect()->having('total_earned_points is not null');
            $this->_customerCollection = $collection;
        }
        return $this->_customerCollection;
    }

}
