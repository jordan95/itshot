<?php

abstract class OpenTechiz_RewardsExtended_Model_Notification_Adapter_AbstractAdapter implements OpenTechiz_RewardsExtended_Model_Notification_Adapter_AdapterInterface
{

    protected $transaction_email_id;
    protected $expires_in_days;
    protected $expiry_date;
    protected $delay_days;

    public function getTransactionEmailId()
    {
        return $this->transaction_email_id;
    }

    public function getDelayDays()
    {
        if (!$this->delay_days) {
            $this->delay_days = Mage::helper('rewards/expiry')->getDelayDays($this->getStoreId());
        }
        return $this->delay_days;
    }

    public function getStoreId()
    {
        return Mage::app()->getStore()->getStoreId();
    }

    public function getExpiryDate()
    {
        if (!$this->expiry_date) {
            $days = $this->getDelayDays() - $this->getWarningDays();
            $e_time = time() - $days * 24 * 60 * 60;
            $this->expiry_date = date('Y-m-d', $e_time);
        }
        return $this->expiry_date;
    }

    public function getExpiresInDays()
    {
        if (!$this->expires_in_days) {
            $this->expires_in_days = $this->getWarningDays() . ' day(s)';
        }
        return $this->expires_in_days;
    }

    public function getRewardsExpiryHelper()
    {
        return Mage::helper('rewards/expiry');
    }
}
