<?php

class OpenTechiz_RewardsExtended_Model_Notification_Adapter_Warning1Days extends OpenTechiz_RewardsExtended_Model_Notification_Adapter_AbstractAdapter
{

    protected $transaction_email_id = 27;

    public function getWarningDays()
    { 
        return $this->getRewardsExpiryHelper()->getWarning1Days($this->getStoreId());
    }
    
    public function getSubjectTitle() {
        return 'ItsHot.com: Cronjob Rewards Point First Notification';
    }

}
