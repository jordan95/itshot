<?php

class OpenTechiz_RewardsExtended_Model_Notification_Adapter_FinalWarning extends OpenTechiz_RewardsExtended_Model_Notification_Adapter_AbstractAdapter
{

    protected $transaction_email_id = 28;

    public function getWarningDays()
    { 
        return 1;
    }
    public function getExpiresInDays()
    {
        if(!$this->expires_in_days) {
            $this->expires_in_days = '24 hour(s)';
        }
        return $this->expires_in_days;
    }
    public function getSubjectTitle() {
        return 'ItsHot.com: Cronjob Rewards Point Final Notification';
    }

}
