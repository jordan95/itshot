<?php

class OpenTechiz_RewardsExtended_Model_Notification_Adapter_Warning2Days extends OpenTechiz_RewardsExtended_Model_Notification_Adapter_AbstractAdapter
{

    protected $transaction_email_id = 27;
    protected $expires_in_days;
    
     public function getWarningDays()
    {
        return $this->getRewardsExpiryHelper()->getWarning2Days($this->getStoreId());
    }
    
     public function getSubjectTitle() {
        return 'ItsHot.com: Cronjob Rewards Point Second Notification';
    }
}
