<?php

interface OpenTechiz_RewardsExtended_Model_Notification_Adapter_AdapterInterface
{
    public function getTransactionEmailId();
    public function getExpiresInDays();
    public function getExpiryDate();
    public function getDelayDays();
    public function getStoreId();
    public function getSubjectTitle();
}
