<?php

class OpenTechiz_RewardsExtended_Model_Review_Wrapper extends TBT_Rewards_Model_Review_Wrapper
{
    public function approvePendingTransfers()
    {
        $pointsEarned = 0;
        $pointsBeforeReviews = $this->getPointsBalance();
        
        foreach ($this->getAssociatedTransfers() as $transfer) {
            if ($transfer->getStatusId() == TBT_Rewards_Model_Transfer_Status::STATUS_PENDING_EVENT) {
                //Move the transfer status from pending to approved, and save it!
                $transfer->setStatusId(
                    TBT_Rewards_Model_Transfer_Status::STATUS_PENDING_EVENT,
                    TBT_Rewards_Model_Transfer_Status::STATUS_APPROVED
                );

                $transfer->save();
                $pointsEarned += $transfer->getQuantity();
                
                $this->shouldSendReviewApprovalConfirmationEmail = true;
            } 
        }
        if($pointsEarned > 0){
            $this->sendReviewApprovalConfirmationEmail($pointsBeforeReviews, $pointsEarned);
        }
        return $this;
    }
}
