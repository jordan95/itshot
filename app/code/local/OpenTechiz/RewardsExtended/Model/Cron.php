<?php

class OpenTechiz_RewardsExtended_Model_Cron
{

    public function startRewardPoints()
    {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection('core_write');
        $select = $read->select();
        $main_table = $resource->getTableName('rewards/transfer');
        $select->from(array('main_table' => $main_table), array('rewards_transfer_id'));
        $select->where("main_table.status_id = 4");
        $select->where("main_table.start_applied_at IS NULL");
        $select->where("main_table.start_at < CURRENT_TIMESTAMP");
        $select->limit(100);
        $refIds = $read->fetchCol($select);
        if (count($refIds) > 0) {
            foreach ($refIds as $refId) {
                $write->query("UPDATE $main_table SET status_id='5', start_applied_at = CURRENT_TIMESTAMP WHERE rewards_transfer_id='" . $refId . "'");
            }
            $this->reindex();
        }
    }

    public function endRewardPoints()
    {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection('core_write');
        $select = $read->select();
        $main_table = $resource->getTableName('rewards/transfer');
        $select->from(array('main_table' => $main_table), array('rewards_transfer_id', 'customer_id', 'quantity', 'reason_id', 'start_applied_at'));
        $select->where("main_table.status_id = 5");
        $select->where("main_table.start_applied_at IS NOT NULL");
        $select->where("main_table.end_applied_at IS NULL");
        $select->where("main_table.end_at < CURRENT_TIMESTAMP");
        $select->where("main_table.quantity > 0");
        $select->limit(100);
        $result = $read->fetchAll($select);
        if (count($result) > 0) {
            foreach ($result as $row) {
                $select = $read->select();
                $select->from(array('main_table' => $main_table), array('sum_spend_points' => new Zend_Db_Expr("SUM(main_table.quantity)")));
                $select->where("main_table.created_at > ?", $row["start_applied_at"]);
                $select->where("main_table.quantity < 0");
                $select->where("main_table.status_id NOT IN (1)");
                $select->where("main_table.reason_id NOT IN (6)");
                $select->where("main_table.customer_id = ?", $row["customer_id"]);
                $sum_spend_points = (int) $read->fetchOne($select);
                if ($row['quantity'] >= abs($sum_spend_points)) {
                    $quantity = (-1) * ($row['quantity'] - abs($sum_spend_points));
                    $comments = "Points expired for transfer #ID: " . $row["rewards_transfer_id"];
                    $write->query("INSERT INTO $main_table(customer_id, quantity, comments, reason_id,  status_id, created_at) VALUES (?, ?, ?, ?, 5, CURRENT_TIMESTAMP)", array(
                        $row['customer_id'],
                        $quantity,
                        $comments,
                        $row['reason_id'],
                    ));
                }
                $write->query("UPDATE $main_table SET end_applied_at = CURRENT_TIMESTAMP WHERE rewards_transfer_id='" . $row["rewards_transfer_id"] . "'");
            }
            $this->reindex();
        }
    }

    public function reindex()
    {
        $process = Mage::getModel('index/indexer')->getProcessByCode('rewards_transfer');
        try {
            $process->reindexAll();
        } catch (Exception $exc) {
            
        }
    }

}
