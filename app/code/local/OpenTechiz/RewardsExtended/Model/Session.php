<?php

class OpenTechiz_RewardsExtended_Model_Session extends TBT_Rewards_Model_Session {

    public function getQuote()
    {
        /* @var $quote Mage_Sales_Model_Quote */
        if ($this->isAdminMode()) {
            if (!is_null(Mage::registry('ordersedit_quote'))) {
                $quote = Mage::registry('ordersedit_quote');
            } elseif ($this->getEditQuoteId()) {
                $quote = Mage::getModel("sales/quote")
                        ->setStoreId(1)
                        ->load($this->getEditQuoteId());
                $items = $quote->getAllItems();
                $quote->setIsSuperMode(true);
                $quote->setTotalsCollectedFlag(false)
                        ->collectTotals();
                $quote->save();
            } else {
                $quote = Mage::getSingleton('adminhtml/session_quote')->getQuote();
            }
            $quote = TBT_Rewards_Model_Sales_Quote::wrap($quote);
        } else {
            $quote = $this->getCheckoutSession()->getQuote();
        }
        return $quote;
    }

    public function isAdminMode()
    {
        return parent::isAdminMode() || Mage::getSingleton('admin/session')->isLoggedIn();
    }

    public function getTotalPointsEarnedOnCart($cart = null) {

        if ($cart == null)
            $cart = $this->getQuote ();

        if ($this->isAdminMode ()) {
            $cart = $this->getQuote ()->updateItemCatalogPoints ();
        }

        $points = array ();
        $points_exist = false;
        $total_cart_points = $this->updateShoppingCartPoints ( $cart );

        foreach ( $total_cart_points as $transfer ) {
            if (isset ( $points [$transfer ['currency']] )) {
                $points [$transfer ['currency']] += $transfer ['amount'];
            } else {
                $points_exist = true;
                $points [$transfer ['currency']] = $transfer ['amount'];
            }
        }

        foreach ( $cart->getAllItems () as $item ) {
            if ($item->getParentItem ())
                continue;

            $points_to_earn = Mage::helper ( 'rewards' )->unhashIt ( $item->getEarnedPointsHash () );
            if ($this->calculateAccumulatedPoints ( $points_to_earn, $points )) {
                $points_exist = true;
            }
        }

        if (! $points_exist) {
            $points = array ();

        //return '<i>'. $this->__('No points') .'</i>.';
        }
        if(isset($points[1]) && $points[1]) {
            $totals = $this->getQuote()->getTotals();
            $subtotal = $this->getQuote()->getSubtotal();
            if(isset($totals["discount"])) {
                $subtotal -= abs($totals["discount"]->getValue());
            }
            /*fix show wrong point on cart,checkout page*/
            $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
            $currentCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
            if ($baseCurrencyCode != $currentCurrencyCode) {
                $subtotal = $this->convertCurrencyAmount($subtotal);
                $subtotal = round($subtotal, 2);
            }  
            /*end fix show wrong point on cart,checkout page*/
            $subtotal_points = $subtotal * 5;
            $points[1] = $subtotal_points;
        }
        return $points;
    }
    private function calculateAccumulatedPoints($points_to_transfer, &$points) {
        $points_exist = false;

        if ($points_to_transfer) {
            foreach ( $points_to_transfer as $points_per_rule ) {
                if ($points_per_rule) {
                    $points_per_rule = ( array ) $points_per_rule;

                    $points_rule_currency = $points_per_rule [TBT_Rewards_Model_Catalogrule_Rule::POINTS_CURRENCY_ID];
                    $points_rule_amount = $points_per_rule [TBT_Rewards_Model_Catalogrule_Rule::POINTS_AMT];
                    $points_rule_applicable_qty = $points_per_rule [TBT_Rewards_Model_Catalogrule_Rule::POINTS_APPLICABLE_QTY];

                    // mixed value for qty int or array
                    $points_rule_applicable_qty = (array) $points_rule_applicable_qty;
                    $points_rule_applicable_qty = array_pop($points_rule_applicable_qty);
                    
                    if (isset ( $points [$points_rule_currency] )) {
                        $points [$points_rule_currency] += $points_rule_amount * $points_rule_applicable_qty;
                    } else {
                        $points_exist = true;
                        $points [$points_rule_currency] = $points_rule_amount * $points_rule_applicable_qty;
                    }
                }
            }
        }

        return $points_exist;
    }
    public function convertCurrencyAmount ($amount)
    {
        $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
        $currentCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        
        if ($baseCurrencyCode != $currentCurrencyCode) {
            $currencyRates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, $currentCurrencyCode);
            $currentCurrencyRate = $currencyRates[$currentCurrencyCode];
            $amount = $amount / $currentCurrencyRate;
        }
        if(empty($amount)){
            $amount = 0;
        }
        return $amount;
    }
    private function getActionsSingleton() {
        return Mage::getSingleton ( 'rewards/salesrule_actions' );
    }
    private function getCartRedemptionsMap($cart = null) {
        if ($cart == null)
            $cart = $this->getQuote ();
        $applicable_rules = Mage::getModel ( 'rewards/salesrule_list_valid_applicable' )->initQuote ( $cart );
        $cart_redemptions = $applicable_rules->getList ();

        $redem_map = array ();
        foreach ( $cart_redemptions as $redem_id ) {
            if (empty ( $redem_id ) && $redem_id != 0)
                continue;
            $redem_map [$redem_id] = true;
        }
        return $redem_map;
    }
    
    public function refreshSessionCustomer($customer_id = null)
    {
        if ($customer_id == null) {
            $customer_id = $this->getCustomerId ();
        } else  if (!is_null(Mage::registry('ordersedit_quote'))) {
            $quote = Mage::registry('ordersedit_quote');
            if($quote->getCustomer()){
                $customer_id = $quote->getCustomer()->getId();
            }
        }
        // if $customer_id is still NULL, check if this is a referral registering an account
        if ($customer_id == null && Mage::registry('rewards_referral_customer')) {
            $this->_customer = Mage::registry('rewards_referral_customer');
        } else {
            $this->_customer = Mage::getModel ( 'rewards/customer' )->load ( $customer_id );
        }

        return $this;
    }
}
