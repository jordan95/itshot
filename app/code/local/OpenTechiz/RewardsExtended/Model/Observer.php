<?php

class OpenTechiz_RewardsExtended_Model_Observer
{
    
    public function refundRewardPoints(Varien_Event_Observer $observer)
    {
        if(Mage::registry("refund_reward_points")) {
            return;
        }
        $order = $observer->getOrder();
        if (!in_array($order->getState(), ["canceled", "closed"])) {
            return;
        }
        if (!$order->hasPointsSpending() && !$order->hasPointsEarning()) {
            return;
        }
        $this->_getAdjuster()
                ->setOrder($order);
        $defaultCurrencyId = Mage::helper('rewards/currency')->getDefaultCurrencyId();
        $canCanceled = false;
        if($order->hasPointsEarning()) {
            $totalEarnedPoints = $order->getTotalEarnedPoints();
            if(!empty($totalEarnedPoints[$defaultCurrencyId]) && $totalEarnedPoints[$defaultCurrencyId] > 0) {
                 $this->_getAdjuster()->setAdjustedEarned(0);
                 $canCanceled = true;
            }
        }
        if($order->hasPointsSpending()) {
            $totalSpentPoints = $order->getTotalSpentPoints();
            if(!empty($totalSpentPoints[$defaultCurrencyId]) && $totalSpentPoints[$defaultCurrencyId] > 0) {
                 $this->_getAdjuster()->setAdjustedSpent(0);
                 $canCanceled = true;
            }
        }
        if (!$canCanceled) {
            return;
        }
        Mage::dispatchEvent('rewards_adjust_points_init_before', array('adjuster' => $this->_getAdjuster()));
        try {
            $this->_getAdjuster()->init();
            $this->_getAdjuster()
                    ->setTransferComments(sprintf("[Order: #%s - Status: %s ] Refund all reward points",  $order->getIncrementId(), $order->getStatus()))
                    ->execute();
        } catch (Exception $exc) {}
        Mage::register("refund_reward_points", true);
    }
    
    /**
     * @return TBT_Rewards_Model_Sales_Order_Transfer_Adjuster
     */
    protected function _getAdjuster()
    {
        return Mage::getSingleton('rewards/sales_order_transfer_adjuster');
    }

    public function returnPointsSpent($observer)
    {
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }

        $action = $event->getControllerAction();
        if (!$action) {
            return $this;
        }

        $params = $action->getRequest()->getParams();
        $adjustedSpent = $this->_getFromParams($params, 'adjustment_points_spent');
        $adjustedEarned = $this->_getFromParams($params, 'adjustment_points_earned');

        $adjuster = $observer->getAdjuster();

        if (!$adjustedSpent) {
            $adjuster->setAdjustedSpent(0);
        }

        if (!$adjustedEarned) {
            $adjuster->setAdjustedEarned(0);
        }
    }

    protected function _getFromParams($params, $key)
    {
        if (!isset($params['rewards'])) {
            return null;
        }

        if (!is_array($params['rewards'])) {
            return null;
        }

        if (!array_key_exists($key, $params['rewards'])) {
            return null;
        }

        return $params['rewards'][$key];
    }

    public function sentMailRewardPointNotifyToCustomer(Varien_Event_Observer $observer)
    {
        $c = $observer->getCustomer();
        $adapter = $observer->getAdapter();
        $points_balance = $observer->getData('points_balance');
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);
        $email = Mage::getModel('core/email_template');
        $sender = array(
            'name' => strip_tags(Mage::helper('rewards/expiry')->getSenderName($c->getStoreId())),
            'email' => strip_tags(Mage::helper('rewards/expiry')->getSenderEmail($c->getStoreId()))
        );
        $email->setDesignConfig(array(
            'area' => 'frontend',
            'store' => $c->getStoreId())
        );
        $product_recommended = $this->getProductRecommended($c);
        $vars = array(
            'customer_name' => $c->getName(),
            'customer_email' => $c->getEmail(),
            'store_name' => $c->getStore()->getName(),
            'days_left' => $adapter->getExpiresInDays(),
            'points_balance' => (string) $points_balance,
            'show_product_list' => $product_recommended
        );
        $email->sendTransactional($adapter->getTransactionEmailId(), $sender, $c->getEmail(), $c->getName(), $vars);
        $translate->setTranslateInline(true);
    }

    public function getProductRecommended($customer)
    {
        return Mage::getSingleton('core/layout')
                        ->createBlock('opentechiz_review/catalog_product_recommended')
                        ->setCustomer($customer)
                        ->toHtml();
    }

}
