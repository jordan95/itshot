<?php

class OpenTechiz_RewardsExtended_Model_Customer extends TBT_Rewards_Model_Customer
{
    protected $_eventPrefix = 'rewards_customer';

    public function hasUsablePoints()
    {
        foreach ($this->getUsablePoints() as $points) {
            if ($points > 0)
                return true;
        }
        $quote = Mage::registry('ordersedit_quote');
        if (!is_null($quote) && intval($quote->getPointsSpending()) > 0) {
            return true;
        }
        return false;
    }

}
