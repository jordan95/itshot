<?php

class OpenTechiz_RewardsExtended_Model_Sales_Aggregated_Cart extends TBT_Rewards_Model_Sales_Aggregated_Cart
{
    protected $_customer = false;

    public function getQuote()
    {
        if (Mage::app()->getStore()->isAdmin()) {
            if (!is_null(Mage::registry('ordersedit_quote'))) {
                return Mage::registry('ordersedit_quote');
            } elseif ($this->getEditQuoteId()) {
                $quote = Mage::getModel("sales/quote")
                        ->setStoreId(1)
                        ->load($this->getEditQuoteId());
                $quote->setIsSuperMode(true);
                $quote->setTotalsCollectedFlag(false)
                        ->collectTotals();
                $quote->save();
                return $quote;
            } else {
                return Mage::getSingleton('adminhtml/session_quote')->getQuote();
            }
        }

        return Mage::getSingleton('checkout/session')->getQuote();
    }

    /**
     * Getter for Current Customer in checkout process on frontend or admin order creation
     * @return TBT_Rewards_Model_Customer|null if null then the customer is a guest in frontend
     */
    public function getCustomer()
    {
        if ($this->_customer === null) {
            if (Mage::app()->getStore()->isAdmin()) {
                $customer = Mage::getSingleton('adminhtml/session_quote')
                    ->getCustomer();
            } else {
                $customerSession = Mage::getSingleton('customer/session');
                $customer = ($customerSession->isLoggedIn()) ? $customerSession->getCustomer() : null;
            }
            $this->_customer = ($customer && $customer->getId()) ?
                Mage::getModel('rewards/customer')->getRewardsCustomer($customer) : null;
        }

        return $this->_customer;
    }
}
