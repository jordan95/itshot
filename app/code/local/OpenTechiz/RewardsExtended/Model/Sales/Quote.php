<?php

class OpenTechiz_RewardsExtended_Model_Sales_Quote extends TBT_Rewards_Model_Sales_Quote {

    public function collectQuoteToOrderTransfers($reserveIncrementId = true) {

        if ($this->_getRewardsSession ()->isAdminMode ()) {
            $this->updateItemCatalogPoints ();
        }

        $this->validateQuoteToOrderTransfers();

        $order_items = $this->getAllItems ();

        $spent_points = array();
        $catalog_redemptions = array();
        $catalog_transfers = $this->_getCatalogTransfersSingleton ();
        foreach ( $order_items as $item ) {
            $this->_tallyCatalogRedemptions($item, $spent_points, $catalog_redemptions);

            /* Start applying catalog distributions */
            $earned_point_totals = $this->_getRH ()->unhashIt ( $item->getEarnedPointsHash () );
            //          Mage::helper('rewards')->notice("Customer earned the following catalog points for item #{$item->getId()} named '{$item->getName()}': ". base64_decode($item->getEarnedPointsHash()));
            if (! empty ( $earned_point_totals )) {
                if ($this->_getRewardsSession()->isCustomerLoggedIn() || $this->_getRewardsSession()->isRecurringOrderBeingPlaced() || Mage::getStoreConfig('rewards/checkout/associate_guest_checkouts_with_account')) {
                    $catalog_transfers->addEarnedPoints ( $earned_point_totals );
                } elseif ($this->_getRewardsSession ()->isAdminMode ()) {    //TODO:Fix for bug 108, will be moved for abstraction in the rewards session
                    $catalog_transfers->addEarnedPoints ( $earned_point_totals );
                } else {
                    // TODO: do we not support earning points on an API-placed order?
                    // TODO Not customer, not admin so possible via API or something, so no points and no message.
                }
            }
            /* End applying catalog distributions */
        }
        
        $all_earned_points = $catalog_transfers->getAllEarnedPoints();
        if (count($all_earned_points) > 0) {
            $catalog_transfers->clearEarnedPoints();
            $totals = $this->getTotals();
            $subtotal = $this->getSubtotal();
            if (isset($totals['discount'])) {
                $subtotal -= abs($totals["discount"]->getValue());
            }
            /*fix show wrong point on cart,checkout page*/
            $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
            $currentCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
            
            if ($baseCurrencyCode != $currentCurrencyCode) {
                $subtotal = $this->convertCurrencyAmount($subtotal);
                $subtotal = round($subtotal, 2);
            }   
            /* end fix show wrong point on cart,checkout page*/
            $total_earned_points = $subtotal * 5;
            if ($total_earned_points > 0) {
                $catalog_transfers->addEarnedPoints(array(
                    array(
                        'points_currency_id' => 1,
                        'points_amt' => $total_earned_points,
                        'rule_id' => 1,
                        'applicable_qty' => 1
                    )
                ));
            }
        }

        $this->_tallyCartRedemptions($spent_points);

        // cleaning the spent_points array by removing any instances of 0 points
        foreach ($spent_points as $key => $value) {
            if ($value == 0) {
                unset($spent_points[$key]);
            }
        }

        if (!empty($spent_points) && !$this->_getRewardsSession()->isCustomerLoggedIn() && !$this->_getRewardsSession()->isAdminMode()) {
            throw new Mage_Core_Exception ( $this->_getRH ()->__ ( 'You must be logged in to spend points.  Please return to your cart and remove the applied point redemptions.' ) );
        }

        /* Start checking ALL points spent against customer balance */
        foreach ($spent_points as $currency_id => $points_amount) {
            $checkoutMethod = $this->getCheckoutMethod(true);

            if ($checkoutMethod == Mage_Sales_Model_Quote::CHECKOUT_METHOD_REGISTER) {
                Mage::getSingleton("rewards/session")->clear();
                //Customer is auto logged in so set the checkout method to logged in
                $this->setCheckoutMethod(Mage_Sales_Model_Quote::CHECKOUT_METHOD_LOGIN_IN);
            }
        }
        /* End checking points against balance */

        /* Start checking if any cart EARNINGS are NEGATIVE (safeguard against bugs) */
        foreach ($this->_getRewardsSession()->updateShoppingCartPoints() as $points) {
            if (!is_array($points)) {
                continue;
            }

            if (isset($points['amount']) && $points['amount'] < 0) {
                $customer_id = 0;
                if ($this->getCustomer()) {
                    $customer_id = $this->getCustomer()->getId();
                }

                Mage::helper('rewards/debug')->log("Order failed due to an EARNING rule attempting to DEDUCT points from the customer.  Please contact MageRewards Support immediately.   Quote ID: [{$this->getId()}], Customer ID: [{$customer_id}], Point Earnings: [{$points['amount']}]");

                // log out the customer if they're registering at checkout, to ensure they can't place order again
                $checkoutMethod = Mage::helper ( 'rewards' )->isBaseMageVersionAtLeast ( '1.4.0.0' ) ?
                    $this->getCheckoutMethod(true) :
                    $this->getCheckoutMethod();
                if ($checkoutMethod == Mage_Sales_Model_Quote::CHECKOUT_METHOD_REGISTER) {
                    Mage::getSingleton("customer/session")->clear();
                    Mage::getSingleton("rewards/session")->clear();
                    //Customer is auto logged in so set the new customer id to empty
                    $this->getCustomer()->setId(null);
                }

                throw new Mage_Core_Exception ( Mage::helper ( 'rewards' )->__ ( 'Points earned could not be processed on this order.  This error has been logged; please contact the store owner immediately.' ) );
            }
        }
        /* End checking if any cart earnings are negative */

        /* Start applying all redemptions */
        $applied = Mage::getModel ( 'rewards/salesrule_list_valid_applied' )->initQuote ( $this );
        $cart_redemptions = $this->_getCartTransfersSingleton ();
        $cart_redemptions->setRedemptionRuleIds ( $applied->getList () );
        foreach ($catalog_redemptions as $redemption) {
            $catalog_transfers->addRedeemedPoints($redemption);
        }
        /* End applying all redemptions */

        /* Start preparing cart points strings (redemptions & distributions) */
        if ($this->_getRewardsSession ()->getCustomerId ()) {
            $points_earning = $this->_getRewardsSession ()->getTotalPointsEarningAsString ();
            $points_spending = $this->_getRewardsSession ()->getTotalPointsSpendingAsString ();
            $cart_redemptions->setEarnedPointsString ( $points_earning );
            $cart_redemptions->setRedeemedPointsString ( $points_spending );
        }
        /* End preparing cart points strings */

        if ($reserveIncrementId) {
            $this->reserveOrderId();
            $catalog_transfers->setIncrementId($this->getRealIncrementId());
        }
    }
    public function convertCurrencyAmount ($amount)
    {
        $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
        $currentCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        
        if ($baseCurrencyCode != $currentCurrencyCode) {
            $currencyRates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, $currentCurrencyCode);
            $currentCurrencyRate = $currencyRates[$currentCurrencyCode];
            $amount = $amount / $currentCurrencyRate;
        }
        if(empty($amount)){
            $amount = 0;
        }
        return $amount;
    }
   public function getMaxSpendablePoints() {
        $this->_calculateMaxPointsUsage();
        $maxUsablePoints = $this->getData('max_spendable_points');
        $quote = Mage::registry('ordersedit_quote');
        if(!is_null($quote)) {
             return max($maxUsablePoints, (int)$quote->getPointsSpending());
        } else {
            return max($maxUsablePoints, 0);
        }
    }
}
