<?php

/**
 * Sweet Tooth
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Sweet Tooth SWEET TOOTH POINTS AND REWARDS
 * License, which extends the Open Software License (OSL 3.0).
 * The Sweet Tooth License is available at this URL:
 *      https://www.sweettoothrewards.com/terms-of-service
 * The Open Software License is available at this URL:
 *      http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * By adding to, editing, or in any way modifying this code, Sweet Tooth is
 * not held liable for any inconsistencies or abnormalities in the
 * behaviour of this code.
 * By adding to, editing, or in any way modifying this code, the Licensee
 * terminates any agreement of support offered by Sweet Tooth, outlined in the
 * provided Sweet Tooth License.
 * Upon discovery of modified code in the process of support, the Licensee
 * is still held accountable for any and all billable time Sweet Tooth spent
 * during the support process.
 * Sweet Tooth does not guarantee compatibility with any other framework extension.
 * Sweet Tooth is not responsbile for any inconsistencies or abnormalities in the
 * behaviour of this code if caused by other framework extension.
 * If you did not receive a copy of the license, please send an email to
 * support@sweettoothrewards.com or call 1.855.699.9322, so we can send you a copy
 * immediately.
 *
 * @category   [TBT]
 * @package    [TBT_Rewards]
 * @copyright  Copyright (c) 2014 Sweet Tooth Inc. (http://www.sweettoothrewards.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Customer Controller
 *
 * @category   TBT
 * @package    TBT_Rewards
 * * @author     Sweet Tooth Inc. <support@sweettoothrewards.com>
 */
require_once(Mage::getModuleDir('controllers','TBT_RewardsReferral').DS.'CustomerController.php');
class OpenTechiz_RewardsExtended_CustomerController extends TBT_RewardsReferral_CustomerController {

    /**
     * Sends a multi-invite using data specified from PLAXO.
     */
    public function invitesAction() {
        // || !$this->getRequest()->getPost('contacts')
        if (!$this->getRequest()->isPost()) {
            $this->_redirect('*/*/');
            return $this;
        }

        $session = Mage::getSingleton('core/session');
        $post_contacts = $this->getRequest()->getPost('contacts');

        $contacts = $this->_getContactsFromPost($post_contacts);

        if (count($contacts) > 10) {
            $session->addError(
                Mage::helper('rewardsref')->__('Limit max 10 invitation emails.')
            );
            $this->_redirect('*/*/');
            return $this;
        }

        $subject = trim((string) strip_tags($this->getRequest()->getPost('subject', "")));
        $message = trim((string) strip_tags($this->getRequest()->getPost('message', "")));

        // Validate data
        try {
            $this->_validateContactsData($contacts);
        } catch (Exception $e) {
            $session->addError($e->getMessage());
            $this->_redirect('*/*/');
            return $this;
        }

        $invitationService = Mage::getModel('rewardsref/service_invite');

        foreach ($contacts as $contact) {
            try {
                $name = $contact[0];
                $email = $contact[1];

                $invitationService->clearInstance()
                    ->setName($name)
                    ->setEmail($email)
                    ->setInvitationMessage($message);

                $invitationService->sendInvitation();
                $session->addSuccess($this->__('Your referral e-mail to %s has been sent.', $name));
            } catch (Mage_Core_Exception $e) {
                $session->addException($e, $this->__('%s', $e->getMessage()));
            } catch (Exception $e) {
                $session->addException($e, $this->__('There was a problem with the invitation.'));
                Mage::logException($e);
            }
        }

        $this->_redirect('*/*/');
        return $this;
    }
}

