<?php


$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('rewards/transfer'),'start_at', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    'nullable'  => true,
    'comment'   => "start time",
    "after" => "created_at",
    ));
$installer->getConnection()
->addColumn($installer->getTable('rewards/transfer'),'start_applied_at', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    'nullable'  => true,
    'comment'   => "start applied time",
    "after" => "start_at",
    ));
$installer->getConnection()
->addColumn($installer->getTable('rewards/transfer'),'end_at', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    'nullable'  => true,
    'comment'   => "end time",
    "after" => "start_applied_at",
    ));
$installer->getConnection()
->addColumn($installer->getTable('rewards/transfer'),'end_applied_at', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    'nullable'  => true,
    'comment'   => "end applied time",
    "after" => "end_at",
    ));
$installer->endSetup();