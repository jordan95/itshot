<?php
class OpenTechiz_GuestCheckoutAdmin_Model_Rewrite_Order_Create extends OpenTechiz_SalesExtend_Model_Rewrite_Order_Create
{

    public function importPostData($data)
    {
        if (is_array($data)) {
            $this->addData($data);
        } else {
            return $this;
        }

       if (isset($data['account'])) {
           $this->setAccountData($data['account']);
           if (isset($data['account']['customer_is_guest']) && $data['account']['customer_is_guest'] ==1) {
                $this->getQuote()->setCustomerIsGuest(true);
           }
       }

        if (isset($data['comment'])) {
            $this->getQuote()->addData($data['comment']);
            if (empty($data['comment']['customer_note_notify'])) {
                $this->getQuote()->setCustomerNoteNotify(false);
            } else {
                $this->getQuote()->setCustomerNoteNotify(true);
            }
        }
        
        if (isset($data['billing_address'])) {
            $this->setBillingAddress($data['billing_address']);
        }

        if (isset($data['shipping_address'])) {
            $this->setShippingAddress($data['shipping_address']);
        }

        if (isset($data['shipping_method'])) {
            $this->setShippingMethod($data['shipping_method']);
        }

        if (isset($data['payment_method'])) {
            $this->setPaymentMethod($data['payment_method']);
        }

        if (isset($data['coupon']['code'])) {
            $this->applyCoupon($data['coupon']['code']);
        }
        return $this;
    }
    /**
     * Retrieve new customer email
     *
     * @param   Mage_Customer_Model_Customer $customer
     * @return  string
     */
    protected function _getNewCustomerEmail($customer)
    {
        $email = $this->getData('account/email');
        if (empty($email)) {
            //start change customer with marketplace customer
            $source_order = $this->getData('source');
            $group_id = $this->getData('account/group_id');
            if(in_array($source_order,OpenTechiz_SalesExtend_Helper_Data::MARKETPLACE_ORDER_SOURCE) && $group_id =="5"){
                
                
                    $email = "ovs-" .time()."@itshot.com";
                    $account = $this->getData('account');
                    $account['email'] = $email;
                    $this->setData('account', $account);
                    return $email;
                
            }
            //end change customer with marketplace customer
            $host = $this->getSession()
                ->getStore()
                ->getConfig(Mage_Customer_Model_Customer::XML_PATH_DEFAULT_EMAIL_DOMAIN);
            $account = $customer->getIncrementId() ? $customer->getIncrementId() : time();
            $email = $account.'@'. $host;
            $account = $this->getData('account');
            $account['email'] = $email;
            $this->setData('account', $account);
        }
        return $email;
    }
}