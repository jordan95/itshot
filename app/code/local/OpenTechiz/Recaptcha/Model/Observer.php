<?php

class OpenTechiz_Recaptcha_Model_Observer {

    public function addRouterToCheck(Varien_Event_Observer $observer) {
        $routes = $observer->getRoutes();
        // selected default option for enable Routes
        $routes->add('quotes_index_post', Mage::helper('studioforty9_recaptcha')->__('Custom Watches Request'));
        $routes->add('quotes_index_post1', Mage::helper('studioforty9_recaptcha')->__('Custom Chains Request'));
        $routes->add('quotes_index_post2', Mage::helper('studioforty9_recaptcha')->__('Custom Earrings Request'));
        $routes->add('quotes_index_post3', Mage::helper('studioforty9_recaptcha')->__('Custom Rings Request'));
        $routes->add('quotes_index_post4', Mage::helper('studioforty9_recaptcha')->__('Custom Jewelry Request'));
        $routes->add('quotes_index_post5', Mage::helper('studioforty9_recaptcha')->__('Custom Pendants Request'));
        $routes->add('quotes_index_post6', Mage::helper('studioforty9_recaptcha')->__('Custom Bracelets Request'));
        $routes->add('quotes_index_post7', Mage::helper('studioforty9_recaptcha')->__('Sell Gold'));
        $routes->add('customerfeedback_index_post', Mage::helper('studioforty9_recaptcha')->__('Contact Us'));
        $routes->add('rewardsref_customer_invites', Mage::helper('studioforty9_recaptcha')->__('Customer Send Invitations'));
        $routes->add('productqa_index_addQuestion', Mage::helper('studioforty9_recaptcha')->__('Product QA'));
        $routes->add('contacts_index_post', Mage::helper('studioforty9_recaptcha')->__('Give us feedback'));
    }

}
