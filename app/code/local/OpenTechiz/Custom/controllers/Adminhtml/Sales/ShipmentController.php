<?php

class OpenTechiz_Custom_Adminhtml_Sales_ShipmentController extends Mage_Adminhtml_Controller_Sales_Shipment
{
    public function printAction()
    {
        $invoice_id = $this->getRequest()->getParam('invoice_id');
        if(!isset($invoice_id)){
            $invoice_id ='';
        }

        if ($invoice_id !='') {
            $invoice = Mage::getModel('sales/order_invoice')->load($invoice_id);
            $order = $invoice->getOrder();
            if ($order) {
                    $shipments = Mage::getResourceModel('sales/order_shipment_collection')
                        ->setOrderFilter($order->getId())
                        ->load();
                    if ($shipments->getSize()) {
                       $pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf($shipments);
                        return $this->_prepareDownloadResponse('packingslip.pdf', $pdf->render(), 'application/pdf');
                    }

            }
            $order = $invoice->getOrder();
            $data = [];

            $data['order_id']    = $order->getIncrementId();
            $data['order_date']  = date_format(date_create($order->getCreatedAt()),"M d, Y");
            $data['invoice_items'] = $invoice->getAllItems();
            $data['customer_comment'] = '';
            if ($order->getOnestepcheckoutCustomercomment()) {
                $data['customer_comment'] = Mage::helper('sales')->__('Customer Comment: ') . $order->getOnestepcheckoutCustomercomment();
            }

            try{
                require_once(Mage::getBaseDir('lib') . DS . 'mpdf/vendor/autoload.php');
                $block = $this->getLayout()->createBlock('core/template');

                $block->setItemData($data);
                $block->setTemplate('opentechiz/sales_extend/pdf/packingslip.phtml');

                $mpdf = new \Mpdf\Mpdf(array('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0));
                $mpdf->showImageErrors = true;
                $mpdf->SetDisplayMode('fullpage');
                $mpdf->AddPage('', // L - landscape, P - portrait
                    '', '', '', '',
                    8, // margin_left
                    8, // margin right
                    15, // margin top
                    20, // margin bottom
                    0, // margin header
                    0); // margin footer
                // $mpdf->list_indent_first_level = 0;
                $mpdf->WriteHTML($block->toHtml());
                $mpdf->Output('packingslip.pdf','D');
                exit;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/sales_shipment/');
            }
        }else{
            $this->_getSession()->addWarning("The packaging slip has not been created so can not print it.");
            $this->_redirect('*/sales_shipment/');
        }
    }

    public function pdfshipmentsAction(){

        $shipmentIds = $this->getRequest()->getPost('shipment_ids');
        if (!empty($shipmentIds)) {
            $shipments = Mage::getResourceModel('sales/order_shipment_collection')
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('entity_id', array('in' => $shipmentIds))
                ->load();
            if (!isset($pdf)){
                $pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf($shipments);
            } else {
                $pages = Mage::getModel('sales/order_pdf_shipment')->getPdf($shipments);
                $pdf->pages = array_merge ($pdf->pages, $pages->pages);
            }

            return $this->_prepareDownloadResponse('packingslip.pdf', $pdf->render(), 'application/pdf');
        }
        $this->_redirect('*/*/');
    }
    public function pdffrominvoiceAction(){

        $orderId = $this->getRequest()->getParam('order_id');
        if(!isset($orderId)){
            $orderId ='';
        }
        if ($orderId !='') {
            $flag =false;
                 $shipments = Mage::getResourceModel('sales/order_shipment_collection')
                    ->setOrderFilter($orderId)
                    ->load();
                if ($shipments->getSize()){
                    $flag = true;
                    if (!isset($pdf)){
                        $pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf($shipments);
                    } else {
                        $pages = Mage::getModel('sales/order_pdf_shipment')->getPdf($shipments);
                        $pdf->pages = array_merge ($pdf->pages, $pages->pages);
                    }
                }
        
        }
        if ($flag) {
            return $this->_prepareDownloadResponse(
                'packingslip.pdf',
                $pdf->render(), 'application/pdf'
            );
        } else {
            $this->_getSession()->addWarning($this->__('There packaging slip has not been created so can not print it.'));
            $this->_redirect('*/sales_invoice/');
        }
        

    }
}