<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is released under commercial license by Lemonline.
 * ----------------------------------------------------------------------------
 * 
 * ----------------------------------------------------------------------------
 *
 * @category   Custom
 * @package    Onlinebiz_Custom
 * @copyright  Copyright (c) 2018 Onlinebiz 
 */

require_once Mage::getModuleDir('controllers', 'MDN_PointOfSales') . DS . 'Adminhtml' . DS .'PointOfSales'. DS .'PointOfSalesController.php';
class OpenTechiz_Custom_Adminhtml_PointOfSales_PointOfSalesController extends MDN_PointOfSales_Adminhtml_PointOfSales_PointOfSalesController
{
        /**
     * First step to create an order
     *
     */
    public function GetProductFromOptionAction()
    {
        try {
          
            $price_incl_tax = 0;
            $params = $this->getRequest()->getParams();
            $options = $params['option'];
            
            $option_data=array();
            foreach ($options as $data) {
                # code...
                if(array_key_exists($data['name'], $option_data))
                    $option_data[$data['name']] = $option_data[$data['name']].','.$data['value'];
                else
                    $option_data[$data['name']] = $data['value'];
            }
            
            $product = Mage::getModel('catalog/product')->load($params['id']);
            $taxRate = Mage::helper('custom/productInfo')->getTaxRate($product)/100;

           
            $optionsCollection = $product->getOptions();        
        // if option enabled = no && hasOptions = 0
            if (!$optionsCollection) $optionsCollection = $product->getProductOptionsCollection();

            

            $values = array();
            $results = array();
            $option_products = array();
            $end = '}';
            $ar_json = '';
            $k=0;

            foreach ($option_data as $key => $value) {
                # code...
                 $k++;
                 $value_ar = explode(',', $value);
                 if(count($value_ar)>1){
                    $option_value = Mage::getResourceModel('catalog/product_option_value_collection')
                    ->addFieldToFilter('option_type_id', array("in" => $value_ar));
                    foreach ($option_value as $data) {
                        # code...
                        if($sku = $data->getData('sku')){
                           
                           $values[] = $sku;
                         }
                    }
                      
                 }else{
                    $option_value = Mage::getResourceModel('catalog/product_option_value_collection')
                        ->addFieldToFilter('option_type_id', $value)->getFirstItem();
                         if($sku = $option_value->getData('sku')){
                            
                           $values[] = $sku;
                         }
                }

               
               $key=  str_replace(']','',$key);
                 $key = trim($key, 'options');
                 $key = ltrim($key, '[');
                 
                 $ar_options = explode("[",$key);
                if($value){
                    if($k>1)
                       $ar_json .=','; 
                    if(count($ar_options)==1){
                        if(count($value_ar)>1){
                          $value1 = json_encode(explode(',', $value),true);  
                          $ar_json .='"'.$key.'":"'.$value1.'"';
                        }else{
                          $ar_json .='"'.$key.'":"'.$value.'"';  
                        }
                       continue;
                    }
                    $i = 1;
                    $json = '';
                    
                    foreach ($ar_options as $optionid) {
                        # code...
                        
                        if($i==1)
                          $json .= '"'.$optionid.'":';
                        else{
                            if($optionid)
                                $json .='{"'.$optionid.'":';

                          if($i==count($ar_options)){

                             
                             if(count($value_ar)<=1){
                                 $json .= $value;
                                 if($optionid)
                                    $json .= '}';
                             }else{
                                $value = json_encode($value_ar,true);
                                $json .= $value;
                             }
                             $ar_json .= $json;
                          }
                          else{
                            if($optionid)
                              $json .= '}';
                          }
                        }
                        
                        $i++;
                    }

                    
                    }
            }

                $ar_json = '{'.$ar_json;
             $ar_json.= '}';
           
            
            

            $product = Mage::getModel('catalog/product')->load($params['id']);

            $result = mage::helper('PointOfSales/ProductInfo')->getProductToAdd($product);
            
          
            
            $isPersonalized = Mage::helper('personalizedproduct')->isPersonalizedProduct($product);
          $price = 0;
          if(!$isPersonalized)
              $price = $product->getFinalPrice();
            $option = json_decode($ar_json);
            $stone_sku = array();
            foreach ($option as $option_id => $option_type_id) {
                # code...
                $option_sku = Mage::getResourceModel('catalog/product_option_value_collection')
                        ->addFieldToFilter('option_type_id', $option_type_id)->getFirstItem()->getSku();

                 if($product->getOptionById($option_id)->getType() == 'stone' ){

                    if(Mage::helper('custom')->isDiamond($option_sku)){
                        
                        $qualityData = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStoneWithQualityPrice($option_id,$product->getId());
                        $quality = str_replace("diamond", "", $option_sku);
                        $price += number_format($qualityData[$quality], 2);
                        $stone_sku[] = "diamond";
                    }else{
                        
                      $data = Mage::helper('personalizedproduct/personalizedPriceHelper')->getStonePrice($option_id,$product->getId());
                      $price += number_format($data[$option_sku], 2);
                      $stone_sku[] = $option_sku;
                    }
                    
                    
                 }

                 if($product->getOptionById($option_id)->getType() == 'gold' ){
                    $data = Mage::helper('personalizedproduct/personalizedPriceHelper')->getAlloyPrice($product->getId());
                   
                    $price += $data[$option_sku];

                    $gold_sku = $option_sku;
                 }
                       
            }
          
            if(count($value)>0)
               $product_sku = $result['product_information']['product_sku'].OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR.implode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $values);
            else
                $product_sku = $result['product_information']['product_sku'];

             if (count(explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $product_sku)) > 3 && !empty($gold_sku)) {
                $url = Mage::getBaseUrl().'media/personalized/product/';
                $url = str_replace('index.php/', '', $url);
                $parts = explode(OpenTechiz_SalesExtend_Helper_Sku::SKU_SEPARATOR, $product_sku);
                $imgName = $parts[0];
                $imgName .= '-'.$parts[count($parts)-2];
                for($i = 1; $i < count($parts) - 2; $i ++){
                    $stoneName = $parts[$i];
                    if(in_array(str_replace('diamond', '', $stoneName), OpenTechiz_PersonalizedProduct_Helper_Data::QUALITY)){
                        $stoneName = 'diamond';
                    }
                    $imgName .= '-'.$stoneName;
                }
                $imgName.='-1.jpg';
                $url .= $imgName;
            } else {
                $url = $product->getImageUrl();
            }
            $result['product_information']['small_image'] = $url;
            $result['product_information']['price_incl_tax'] = $price + ($price*$taxRate);
            $result['product_information']['config_option'] = json_decode($ar_json);
            $result['product_information']['product_option_sku'] =  $product_sku;
            $result['product_information']['product_option_id'] = '_'.implode('_', $values);
            $results[] = $result;
            print_r(json_encode($results));
            return;
        } catch (Exception $ex) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('An error occured : %s', $ex->getMessage()));
            $this->_redirect('adminhtml/dashboard');
        }
    }
        /**
     * load datas
     *
     */
    private function loadDatas()
    {
        $dataRaw = $this->getRequest()->getPost('rawdata');
        $this->_OrderCreationData = mage::helper('PointOfSales/Serialization')->unserializeObject($dataRaw);
    }
     /**
     * Create order and return result with ajax
     *
     */
    public function CreateOrderAction()
    {
        //init vars
        $error = false;
        $message = '';
        $stacktrace = '';

        try {

            //load and init datas & create order
            $this->loadDatas();

            $this->initDatas();

            //$this->checkStocks();
            if (Mage::helper('PointOfSales')->checkRewardPointModule()):
                $this->_OrderCreationData["pos_credits_applied"] = $this->getRequest()->getPost('pos_credits_applied')=="on"?1:0;
                $this->_OrderCreationData["pos_credits_credits_used"] = $this->getRequest()->getPost('pos_credits_credits_used');
            endif;
            //If new customer selected, create customer
            if ($this->_OrderCreationData['customer_mode'] == 'new') {
                $customer = mage::helper('PointOfSales/Customer')->createCustomer($this->_OrderCreationData);
                $address = mage::helper('PointOfSales/Customer')->createAddress($this->_OrderCreationData, $customer->getId());

                $this->_OrderCreationData['customer_id'] = $customer->getId();
            } else {
                Mage::helper('PointOfSales/Customer')->updateInformation($this->_OrderCreationData);
            }

            //newsletter subscription
            if ($this->getRequest()->getPost('newsletter')) {
                Mage::helper('PointOfSales/Customer')->newsletterSubscription($this->_OrderCreationData['customer_id']);
            }
            $order = mage::helper('PointOfSales')->createOrder($this->_OrderCreationData);
            $order->setData('onestepcheckout_customerfeedback', $this->_OrderCreationData['onestepcheckout_customerfeedback']);
            $order->setData('source', $this->_OrderCreationData['source']);
            if(!empty($this->_OrderCreationData['increment_id'])) {
                $resource = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                $tsht_sales_flat_order = $resource->getTableName('sales_flat_order');
                $orderId = (int) $readConnection->fetchOne("SELECT entity_id FROM $tsht_sales_flat_order WHERE increment_id = ? LIMIT 1", array($this->_OrderCreationData['increment_id']));
                if($orderId){
                    Mage::throwException("Order Number: ". $this->_OrderCreationData['increment_id'] . " is existed.");
                }
                $order->setData('increment_id', $this->_OrderCreationData['increment_id']);
            }
            // identify order was created by user
            Mage::dispatchEvent('opentechiz_create_order_by_user', array(
                'order'=>$order
            ));
            $order->save();
            $dataEvent = [];
            $dataEvent["order"] = $order;
            mage::helper('PointOfSales')->createPayments($order, $this->_OrderCreationData);

            //create order / shipment
            if ($this->_OrderCreationData['is_paid']) {
                $invoice = mage::helper('PointOfSales')->createInvoice($order, $this->_OrderCreationData['invoice_comments']);
                $dataEvent["invoice"] = $invoice;
            }
            mage::helper('PointOfSales')->createShipment($order, $this->_OrderCreationData);

            Mage::dispatchEvent("pos_save_order_after", $dataEvent);
            /* Printing is now handled at the client level with PDF download

            //print invoice (is required)
            if (mage::getStoreConfig('pointofsales/configuration/print_invoice')) {
                $this->printInvoice($order);
            }

            //print shipment (is required)
            if (mage::getStoreConfig('pointofsales/configuration/print_shipment')) {
                $this->printShipment($order);
            }

            if (Mage::getStoreConfig('pointofsales/configuration/print_receipt')) {
                $this->printReceipt($order);
            }
            */

            // notify guest customer if congiguration allow it
            if (Mage::getStoreConfig('pointofsales/notification/enable_new_order_email')) {
                $order->sendNewOrderEmail();
            }

            //return url
            //$url = $this->getUrl('PointOfSales/PointOfSales/Confirm', array('order_id' => $order->getId()));
            $order->setData('order_view_url', $this->getUrl('adminhtml/PointOfSales_PointOfSales/OrderView', array('order_id' => $order->getId(), 'from' => 'created', 'print_receipt' => Mage::getStoreConfig('pointofsales/configuration/print_receipt'), 'print_invoice' => Mage::getStoreConfig('pointofsales/configuration/print_invoice'))));
            $message = $order->getData();
        } catch (Exception $e) {
            $error = true;
            $exceptionText = $e->getMessage();

            //improve error msg
            $quoteSession = Mage::getSingleton('adminhtml/session_quote');
            $messages = $quoteSession->getMessages(true);
            if ($messages->count() > 0)
                $exceptionText = $messages->getLastAddedMessage()->toString();

            if ($exceptionText == '')
                $exceptionText = $this->__('Undefined exception');

            $stacktrace = $e->getTraceAsString();

            $message = $this->__($exceptionText);
        }

        //return ajax result
        $response = array();
        $response['error'] = $error;
        $response['message'] = $message;
        $response['stacktrace'] = $stacktrace;
        $response = Zend_Json::encode($response);
        $this->getResponse()->setBody($response);
    }

   

     /**
     * init with post data
     *
     */
    private function initDatas()
    {
       
        //add products
        if ($this->getRequest()->getPost('product_ids') != '') {
            
            $this->_OrderCreationData['products'] = array();
            $products = explode(';', $this->getRequest()->getPost('product_ids'));

            foreach ($products as $item) {
                $t = explode('-', $item);
                if (count($t) == 7) {
                    $id = explode('_', $t[0]) ;
                    $product = mage::getModel('catalog/product')->load($id[0]);
                    $qty = $t[1];
                    $priceExclTax = $t[2];
                    $priceInclTax = $t[3];
                    $discount = $t[4];
                    $isShipped = $t[5];
                    $config = $t[6];
                    $newProduct = array('product_id' => $id[0],
                        'qty' => $qty,
                        'priceExclTax' => $priceExclTax,
                        'priceInclTax' => $priceInclTax,
                        'discount' => $discount,
                        'config' =>  $config,
                        'shipped' => $isShipped);
                    $this->_OrderCreationData['products'][] = $newProduct;
       
                }
                    if (count($t) == 6) {
                    $id = $t[0];
                    $product = mage::getModel('catalog/product')->load($id);
                    $qty = $t[1];
                    $priceExclTax = $t[2];
                    $priceInclTax = $t[3];
                    $discount = $t[4];
                    $isShipped = $t[5];
                    $newProduct = array('product_id' => $id,
                        'qty' => $qty,
                        'priceExclTax' => $priceExclTax,
                        'priceInclTax' => $priceInclTax,
                        'discount' => $discount,
                        'shipped' => $isShipped);
                    $this->_OrderCreationData['products'][] = $newProduct;
                }
            }
        }
        
        $this->_OrderCreationData['source'] = $this->getRequest()->getPost('onestepcheckout-ordersource');
        $increment_id = $this->getRequest()->getPost('onestepcheckout-ordernumber', false);
        $increment_id = str_replace(array("    ", "   ", "  ", " "), "", $increment_id);
        if($increment_id) {
            $this->_OrderCreationData['increment_id'] = trim($increment_id);
        }
        
        if($this->getRequest()->getPost('onestepcheckout-feedback') && $this->getRequest()->getPost('onestepcheckout-feedback')!='freetext'){
            $feedbackValues = unserialize(Mage::getStoreConfig('onestepcheckout/feedback/feedback_values'));
           
            $this->_OrderCreationData['onestepcheckout_customerfeedback'] = $feedbackValues[$this->getRequest()->getPost('onestepcheckout-feedback')]['value'];
        }
        else
            $this->_OrderCreationData['onestepcheckout_customerfeedback'] = $this->getRequest()->getPost('onestepcheckout-feedback-freetext');
        $this->_OrderCreationData['total_discount'] = $this->getRequest()->getPost('input_total_discount');
        $this->_OrderCreationData['pos_user_id'] = $this->getRequest()->getPost('user_id');
        $this->_OrderCreationData['store_id'] = $this->getRequest()->getPost('store_id');

        //payment & shipping methods
        $this->_OrderCreationData['payment_method'] = $this->getRequest()->getPost('paymentmethod');
        $this->_OrderCreationData['shipping_method'] = $this->getRequest()->getPost('shippingmethod');

        //throw new Exception(print_r($this->getRequest()->getPost(), true));

        $this->_OrderCreationData['payments'] = array();
        if ($this->getRequest()->getPost('payments') != '') {
            $this->_OrderCreationData['payments'] = json_decode($this->getRequest()->getPost('payments'), true);
        }


        if ($this->getRequest()->getPost('is_paid') != '')
            $this->_OrderCreationData['is_paid'] = $this->getRequest()->getPost('is_paid');

        if ($this->getRequest()->getPost('is_shipped') != '')
            $this->_OrderCreationData['is_shipped'] = $this->getRequest()->getPost('is_shipped');

        //customer
        $this->_OrderCreationData['customer_mode'] = $this->getRequest()->getPost('customer_mode');

        $this->_OrderCreationData['comments'] = $this->getRequest()->getPost('comments');
        $this->_OrderCreationData['invoice_comments'] = $this->getRequest()->getPost('invoice_comments');

        //define information depending of customer mode
        $emptyString = mage::getStoreConfig('pointofsales/configuration/empty_string');
        switch ($this->_OrderCreationData['customer_mode']) {
            case 'guest':

                $this->_OrderCreationData['customer_firstname'] = $emptyString;
                $this->_OrderCreationData['customer_lastname'] = $emptyString;
                $this->_OrderCreationData['customer_email'] = Mage::getStoreConfig('pointofsales/notification/guest_account_email');

                //address
                $this->_OrderCreationData['customer_company'] = $emptyString;
                $this->_OrderCreationData['address'] = $emptyString;

                $defaultCity = mage::helper('PointOfSales/User')->getDefaultCity();
                if (!$defaultCity)
                    $this->_OrderCreationData['city'] = $emptyString;
                else
                    $this->_OrderCreationData['city'] = $defaultCity;

                $defaultZip = mage::helper('PointOfSales/User')->getDefaultZip();
                if (!$defaultZip)
                    $this->_OrderCreationData['zip'] = $emptyString;
                else
                    $this->_OrderCreationData['zip'] = $defaultZip;

                $this->_OrderCreationData['country'] = $this->getRequest()->getPost('country');
                $this->_OrderCreationData['region'] = $this->getRequest()->getPost('region');
                $this->_OrderCreationData['mobile'] = $emptyString;
                $this->_OrderCreationData['phone'] = $emptyString;
                $this->_OrderCreationData['fax'] = $emptyString;
                $this->_OrderCreationData['customer_phone'] = $emptyString;

                break;
            default: //new, existing

                $this->_OrderCreationData['customer_id'] = $this->getRequest()->getPost('customer_id');
                $this->_OrderCreationData['customer_firstname'] = $this->getRequest()->getPost('customer_firstname');
                $this->_OrderCreationData['customer_lastname'] = $this->getRequest()->getPost('customer_lastname');
                $this->_OrderCreationData['customer_email'] = $this->getRequest()->getPost('customer_email');

                //address
                $this->_OrderCreationData['customer_company'] = $this->getRequest()->getPost('customer_company');
                $this->_OrderCreationData['address'] = $this->getRequest()->getPost('address');
                $this->_OrderCreationData['city'] = $this->getRequest()->getPost('city');
                $this->_OrderCreationData['zip'] = $this->getRequest()->getPost('zip');
                $this->_OrderCreationData['country'] = $this->getRequest()->getPost('country');
                $this->_OrderCreationData['phone'] = $this->getRequest()->getPost('phone');
                //$this->_OrderCreationData['fax'] = $this->getRequest()->getPost('fax');
                //$this->_OrderCreationData['customer_phone'] = $this->getRequest()->getPost('customer_phone');
                $this->_OrderCreationData['region'] = $this->getRequest()->getPost('region');
                $this->_OrderCreationData['group'] = $this->getRequest()->getPost('group');

                //fill required address information with empty string if not set
                $fields = array('address', 'city', 'zip', 'phone');
                foreach ($fields as $field) {
                    if ($this->_OrderCreationData[$field] == '')
                        $this->_OrderCreationData[$field] = $emptyString;
                }

                break;
        }
    }

      public function SearchProductsAction()
    {
        $term = $this->getRequest()->getParam('term');
        $return = array();

        if ($term) {
            $collection = mage::helper('PointOfSales/Search')->searchProducts($term);

            foreach ($collection as $product) {
                $suffix = mage::helper('PointOfSales/ProductInfo')->getProductAttributeLabelsAsText($product);
                $result = mage::helper('PointOfSales/ProductInfo')->getProductToAdd($product, $suffix);
                $result['product_information']['has_option'] = $product->getHasOptions();
                $return[] = $result['product_information'];
            }
        }
        $this->getResponse()->setBody(json_encode($return));
    }
     public function ShippingMethodAction()
    {
        //init vars
        $error = false;
        $message = '';
        $shippingRates = '';

        try {

            //load and init datas & create order
            $this->loadDatas();
            $this->initDatas();

            $shippingRates = mage::helper('PointOfSales')->getShippingRates($this->_OrderCreationData);
        } catch (Exception $e) {
            $error = true;
            $message = $this->__('An error occured : %s', $e->getMessage());
        }

        //return ajax result
        $response = array();
        $response['error'] = $error;
        $response['message'] = $message;
        $response['shippingRates'] = $shippingRates;
        $response = Zend_Json::encode($response);
        $this->getResponse()->setBody($response);
    }

}
