<?php

class OpenTechiz_Custom_RecentlyViewedController extends Mage_Core_Controller_Front_Action
{
    public function ajaxGetProductsAction()
    {
        if ($this->getRequest()->isAjax()) {
            $productId = $this->getRequest()->getParam('productId');
            if ($productId) {
                $product = Mage::getModel('catalog/product')->load($productId);
                if ($product->getId()) {
                    Mage::getModel('reports/product_index_viewed')
                        ->setProductId($productId)
                        ->save()
                        ->calculate();
                }
            }
            $response = new Varien_Object();
            $html = $this->getLayout()
                ->createBlock('reports/product_viewed')
                ->setTemplate('catalog/product/view/product_viewed.phtml')
                ->toHtml();
            $response->setData('html', $html);
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody($response->toJson());
        }
    }
}