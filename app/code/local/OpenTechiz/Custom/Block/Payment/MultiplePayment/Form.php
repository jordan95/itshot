<?php

class OpenTechiz_Custom_Block_Payment_MultiplePayment_Form extends MDN_PaymentTracker_Block_Payment_MultiplePayment_Form
{
    public function getPaymentRecords()
    {
        $paymentData = $this->getRequest()->getParam('payment');

        if (isset($paymentData['method']) && $paymentData['method'] == 'MultiplePayment' &&
            count($paymentData['MultiplePayment'])) {
            $payments = array();
            $methods = $paymentData['MultiplePayment']['method'];
            $amounts = $paymentData['MultiplePayment']['amount'];

            foreach ($methods as $key => $value) {
                if ($value) {
                    $payments[$key]['ptp_method'] = $value;
                    $payments[$key]['ptp_amount'] = $amounts[$key];
                }
            }
            return $payments;
        }
        if ($this->getRequest()->getParam('order_id')) {
            $collection = Mage::getModel('PaymentTracker/Payment')
                ->getCollection()
                ->addFieldToFilter('ptp_order_id', $this->getRequest()->getParam('order_id'));
            return $collection->getData();
        }

        return array();
        
    }

    public function getMethodsDropDownMenu($i)
    {
        $payments = $this->getPaymentRecords();
        $html = '<select name="payment['.$this->getMethodCode().'][method]['.$i.']" style="max-width:500px">';
        $html .= '<option></option>';
        foreach(Mage::helper('PaymentTracker')->getPaymentMethods() as $k => $v)
        {
            $v = strip_tags($v);
            if(isset($payments[$i]['ptp_method'])&&$payments[$i]['ptp_method']==$v)
               $html .= '<option value="'.$v.'" selected >'.$v.'</option>';
            else
                $html .= '<option value="'.$v.'" >'.$v.'</option>';
        }

        $html .= '</select>';

        return $html;
    }

}