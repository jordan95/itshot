<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Opentechiz_Custom_Helper_Shortcuts extends MDN_PointOfSales_Helper_Shortcuts
{
    public function getShorcutHtmlAttributes($product)
    {
        if ($product->gettype_id() == 'configurable') {
            return ' data-product-type="configurable" data-product-id="' . $product->getId() . '"';
        }
        if ($product->getHasOptions()) {
            return ' data-product-type="has_option" data-product-id="' . $product->getId() . '"';
        }
        $product = mage::getModel('catalog/product')->setStoreId(Mage::helper('PointOfSales/User')->getCurrentStoreId())->load($product->getId());

        $productToAdd = mage::helper('PointOfSales/ProductInfo')->getProductToAdd($product);

        if (!isset($productToAdd['product_information'])) {
            return 'style="color: red;"';
        }

        $tmp = $productToAdd['product_information'];
     
        $html = '';
        $html .= ' data-product-id="' . $product->getId() . '"';
        $html .= ' data-product-name="' . $tmp['product_name'] . '"';
        $html .= ' data-price-excl-tax="' . $tmp['price_excl_tax'] . '"';
        $html .= ' data-tax-rate="' . $tmp['tax_rate'] . '"';
        $html .= ' data-price-incl-tax="' . $tmp['price_incl_tax'] . '"';
        $html .= ' data-small-image="' . $tmp['small_image'] . '"';
        $html .= ' data-currency-symbol="' . $tmp['currency_symbol'] . '"';
        $html .= ' data-stock="' . $tmp['stock'] . '"';
        $html .= ' data-shelf-location="' . $tmp['shelf_location'] . '"';
       
        return $html;
    }
}