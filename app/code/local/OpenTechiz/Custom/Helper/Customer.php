<?php
class OpenTechiz_Custom_Helper_Customer extends MDN_PointOfSales_Helper_Customer {
	public function getCustomerInfo($customerId) {

        $customer = mage::getModel('customer/customer')->load($customerId);

        $infos = array();
        $infos['id'] = $customer->getId();
        $infos['email'] = $customer->getData('email');
        $infos['firstname'] = $customer->getData('firstname');
        $infos['lastname'] = $customer->getData('lastname');

        $address = Mage::getModel('customer/address')->load($customer->getDefaultBilling());
        $infos['street'] = $address->getstreet();
        $infos['city'] = $address->getcity()?$address->getcity():Mage::helper('PointOfSales/User')->getDefaultCity();
        $infos['zip'] = $address->getpostcode()?$address->getpostcode():Mage::helper('PointOfSales/User')->getDefaultZip();
        $infos['country_id'] = $address->getcountry_id()?$address->getcountry_id():Mage::helper('PointOfSales/User')->getDefaultCountryId();

        $infos['region_id'] = $address->getregion_id()?$address->getregion_id():0;
        $infos['phone'] = $address->gettelephone();
        $infos['group'] = $customer->getGroupId();
        if (Mage::helper('PointOfSales')->checkRewardPointModule()):
            $infos['pos_reward_points'] = $customer->storeCreditBalance();
            $infos['pos_reward_credits'] = Mage::helper('core')->currency(Mage::helper("posrewardpoints")->creditsToMoney($infos['pos_reward_points']), true, false);
        endif;

        return $infos;
    }
}