<?php
class OpenTechiz_Custom_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function isDiamond($stoneName){
        $arrayQuality=['si', 'i', 'vs'];
        $quality = str_replace("diamond", "", $stoneName);
        return in_array($quality, $arrayQuality);
    }

    public function checkPaymentTrackerModule()
    {
        $moduleName = 'MDN_PaymentTracker';
        $installed = Mage::getConfig()->getModuleConfig($moduleName)->is('active', 'true');
        $enabled = Mage::helper('core')->isModuleEnabled($moduleName);
        return ($installed && $enabled);
    }

    public function checkExportPermission(){
        return Mage::getSingleton('admin/session')->isAllowed('export');
    }

    public function getAllPaymentMethods()
    {
        if (Mage::registry('payment_methods')) return Mage::registry('payment_methods');
        $payments = Mage::getSingleton('payment/config')->getAllMethods();
        $methods = array();
        foreach ($payments as $paymentCode => $paymentModel) {
            $methods[$paymentCode] = Mage::getStoreConfig('payment/' . $paymentCode . '/title');
        }
        Mage::register('payment_methods', $methods);
        return $methods;
    }
    public function convertPrice($price)
    {
        $format_price = Mage::getModel('directory/currency')->format($price, array('display'=>Zend_Currency::NO_SYMBOL), false);
        return $format_price;
    }
    public function getUserId(){
        if(Mage::getSingleton('admin/session')->isLoggedIn()){
            $user_id = Mage::getSingleton('admin/session')->getUser()->getId();
        }else{
            $user_id = 0;
        }
        return $user_id;
    }
    public function isCookieVersionExisting(){
        $cookie_version = Mage::getModel('core/cookie')->get("version");
        if($cookie_version){
            return true;
        }else{
            return false;
        }
       
    }
    public function isFromUsCountry(){
        if(isset($_SERVER['GEOIP_COUNTRY_CODE']) && $_SERVER['GEOIP_COUNTRY_CODE']=="US"){
            return true;
        }else{
            return false;
        }
    }
}
	 