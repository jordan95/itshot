<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Opentechiz_Custom_Helper_ProductInfo extends MDN_PointOfSales_Helper_ProductInfo {

        public function getProductToAdd($product, $productNameSuffix = '')
    {

        try {
            if (($product == null) || (!$product->getId()))
                throw new Exception($this->__('Cant find product'));

            //define product information
            $storeId = mage::helper('PointOfSales/User')->getCurrentStoreId();
            $price = $this->getProductPrice($product);
            $taxRate = $this->getTaxRate($product);

            //define prices
            if (Mage::getStoreConfig('tax/calculation/price_includes_tax', $storeId) == 1) {
                $priceInclTax = $price;
                $priceExclTax = $priceInclTax / (1 + ($taxRate / 100));
            } else {
                $priceExclTax = $price;
                $priceInclTax = $price * (1 + ($taxRate / 100));
            }

            //format prices
            $priceExclTax = number_format($priceExclTax, 2, '.', '');
            $taxRate = number_format($taxRate, 4, '.', '');
            $priceInclTax = number_format($priceInclTax, 2, '.', '');
            $productName = $this->cleanTxtForJson($product->getname()) . $productNameSuffix;

            // convert price
            $priceExclTax = Mage::helper('PointOfSales/Currency')->convert($priceExclTax);
            $priceInclTax = Mage::helper('PointOfSales/Currency')->convert($priceInclTax);

            $log = 'Tax rate : '.$taxRate.' , Price Excluding Tax : '.$priceExclTax.' , Price Including tax : '.$priceInclTax;
            Mage::helper('PointOfSales')->addLog($log);


            //return product information
            $result['product_information'] = array();
            $result['product_information']['product_name'] = htmlentities(utf8_decode($productName), ENT_QUOTES);
            $result['product_information']['product_id'] = $product->getId();
            $result['product_information']['skin_url'] = Mage::getDesign()->getSkinUrl('images/OrderWizardCreation/');
            $result['product_information']['price_excl_tax'] = $priceExclTax;
            $result['product_information']['price_incl_tax'] = $priceInclTax;
            $result['product_information']['tax_rate'] = $taxRate;
            $result['product_information']['currency_symbol'] = mage::helper('PointOfSales/User')->getCurrency()->getData('currency_code');

            $result['product_information']['product_sku'] = $product->getSku();
            $result['product_information']['product_type'] = $product->gettype_id();
            $result['product_information']['price_includes_tax'] = Mage::getStoreConfig('tax/calculation/price_includes_tax');

            //additionnal for autocomplete
            $websiteId = mage::helper('PointOfSales/User')->getWebsiteId();
            $result['product_information']['stock'] = mage::helper('PointOfSales/Stock')->getAvailableQuantityForSale($product,$websiteId)."";

            $shelfLocation = '';
            if(Mage::helper('PointOfSales/Stock')->erpIsInstalled()) {
                $productStock = mage::getResourceModel('AdvancedStock/MassStockEditor_Collection')
                    ->addFieldToFilter('sku',$product->getSku())
                    ->getFirstItem();
                if($productStock->getId()){
                    $shelfLocation = $productStock->getShelfLocation();
                }
            }
            $result['product_information']['shelf_location'] = $shelfLocation;
            try{

                $parentProduct = $this->getConfigurableProduct($product);
                if($parentProduct === null)
                    $result['product_information']['small_image'] = (string) mage::helper('catalog/image')->init($product, 'small_image')->resize(48, 48);
                else{
                    $result['product_information']['small_image'] = (string) mage::helper('catalog/image')->init($parentProduct, 'small_image')->resize(48, 48);
                }
            }
            catch(Exception $e)
            {
                $result['product_information']['small_image'] = '';
            }

            $result['error'] = 0;
            $result['message'] = $this->__('%s added', $productName);
        } catch (Exception $ex) {
            $result['error'] = 1;
            $result['message'] = $ex->getMessage();
        }

        return $result;
    }

}