<?php

class OpenTechiz_Custom_Model_Observer
{
    public function checkCategoryIds(Varien_Event_Observer $observer){
        $product = $observer->getEvent()->getProduct();
        $default_category_id = Mage::app()->getStore("default")->getRootCategoryId();
        $db = Mage::getSingleton('core/resource')->getConnection('core_read');
        if(sizeof($product->getCategoryIds()) == 0) {
            $sql = "INSERT INTO tsht_catalog_category_product (category_id, product_id, position) VALUES (?,?,1)";
            $db->query($sql, array($default_category_id, $product->getId()));
        }
    }

    public function stopCollectShippingRates($observer){
        $urlParams = Mage::app()->getRequest()->getParams();
        if(array_key_exists('id', $urlParams)){
            $ppId = $urlParams['id'];
            $partialPayment = Mage::getModel('partialpayment/partialpayment')->load($ppId);
            if($partialPayment){
                $orderId = $partialPayment->getOrderId();
                $session = $observer->getSessionQuote();
                if (!$session->getReordered()) {
                    $currentOrderId = $session->getOrderId();
                } else {
                    $currentOrderId = $session->getReordered();
                }
                if($currentOrderId == $orderId){
                    $session->setUseOldShippingMethod(false);
                }
            }
        }
    }
}