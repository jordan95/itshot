<?php

/**
 * OnlineBiz Software Solution
 * 
 * @project autoviihde.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Joe Vu<joe@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2017 , OnlineBiz Software Solution
 * 
 * Create at: Jan 26, 2018 10:55:47 AM
 */
class Onlinebiz_Custom_Model_PointOfSales_Pdf_Receipt extends MDN_PointOfSales_Model_Pdf_Receipt {

    /**
     * generate PDF
     *
     * @param array $orders
     * @return Zend_Pdf
     */
    public function getPdf($orders = array()) {

        // save order in class attribute, used for check pdf height
        $this->orders = &$orders;

        $this->_beforeGetPdf();
        $storeId = Mage::helper('PointOfSales/User')->getCurrentStoreId();

        // init
        $this->initSizes(Mage::getStoreConfig('pointofsales/receipt/width', $storeId), Mage::getStoreConfig('pointofsales/receipt/unit', $storeId), $orders);

        if ($this->pdf == null)
            $this->pdf = new Zend_Pdf();

        $style = new Zend_Pdf_Style();
        $style->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD), $this->_fontSize);

        // create new page
        $settings['title'] = Mage::getStoreConfig('pointofsales/receipt/header', $storeId);
        $settings['store_id'] = Mage::helper('PointOfSales/User')->getCurrentStoreId();
        $page = $this->NewPage($settings); // add header
        // get first order
        $order = $orders[0];


        //Affiche les lignes produit
        foreach ($order->getAllItems() as $item) {

            //Si c'est un sous produit, on affiche pas
            if ($item->getParentItem()) {
                continue;
            }
            //Pour les produits "standards"
            $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), $this->_fontSize);

            // [1]
            // add qty
            $qty_txt = (int) $item->getqty_ordered();
            $page->drawText($qty_txt . 'x', 5 + $this->margin_left, $this->y, 'UTF-8');
            // [1]
            // [2]
            // build line : ajout de retour a la ligne afin que le texte rentre dans la largeur passee en parametre
            $caption_width = 40 * $this->width / 100;
            $caption = $this->WrapTextToWidth($page, $item->getname(), $caption_width);
            // add caption (product name and options)
            $shelfLocation = Mage::helper('PointOfSales/ShelfLocation')->getShelfLocationForProduct($item->getProduct());
            
            // return height of caption block
            $offset = $this->DrawMultilineText($page, $caption.PHP_EOL.$shelfLocation , 25 + $this->margin_left, $this->y, $this->_fontSize, 0, $this->_lineHeight, false);
            // [2]
            // [3]
            // add price
            $price_width = 20 * $this->width / 100;
            $price_pos = $this->width * 70 / 100;
            $this->drawTextInBlock($page, $order->formatPriceTxt(round($item->getrow_total_incl_tax(), 2)), $price_pos, $this->y, $price_width, 20, 'r');
            // [3]

            $this->y -= $offset;
        }

        $this->y -= 5;

        //totals
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD), $this->_fontSize);
        $txt_pos = $price_pos - $price_width - 50;
        $page->drawText(Mage::Helper('PointOfSales')->__('Sub Total'), $txt_pos, $this->y, 'UTF-8');
        $this->drawTextInBlock($page, $order->formatPriceTxt(round($order->getsubtotal(), 2)), $price_pos, $this->y, $price_width, 20, 'r');

        if ($order->getdiscount_amount() <> 0) {
            $this->y -= 10;
            $page->drawText(Mage::Helper('PointOfSales')->__('Discount'), $txt_pos, $this->y, 'UTF-8');
            $this->drawTextInBlock(
                    $page, $order->formatPriceTxt(round($order->getdiscount_amount(), 2)), $price_pos, $this->y, $price_width, 20, 'r'
            );
        }

        $this->y -= 10;

        $shipping_amount = $order->getbase_shipping_amount();
        if ($shipping_amount > 0) {

            $page->drawText(Mage::Helper('PointOfSales')->__('Shipping'), $txt_pos, $this->y, 'UTF-8');
            $this->drawTextInBlock($page, $order->formatPriceTxt(round($order->getbase_shipping_amount(), 2)), $price_pos, $this->y, $price_width, 20, 'r');

            $this->y -= 10;
        }
        if (Mage::helper('PointOfSales')->checkRewardPointModule()) {
            if ($order->getPosCreditsApplied() && $order->getPosCreditsCreditsUsed()) {

                $page->drawText(Mage::Helper('PointOfSales')->__('Reward Credits'), $txt_pos, $this->y, 'UTF-8');
                $this->drawTextInBlock($page, $order->formatPriceTxt($order->getPosCreditsMoneyUsed()), $price_pos, $this->y, $price_width, $this->_lineHeight, 'r');
                $this->y -= 10;
            }
        }
        // check taxes
        $page->drawText(Mage::Helper('PointOfSales')->__('Tax'), $txt_pos, $this->y, 'UTF-8');

        $this->drawTextInBlock($page, $order->formatPriceTxt(round($order->getbase_tax_amount() + $order->getbase_shipping_tax(), 2)), $price_pos, $this->y, $price_width, $this->_lineHeight, 'r');

        $this->y -= 10;

        // grand total
        $page->drawText(Mage::Helper('PointOfSales')->__('Total'), $txt_pos, $this->y, 'UTF-8');
        $this->drawTextInBlock($page, $order->formatPriceTxt($order->getgrand_total()), $price_pos, $this->y, $price_width, $this->_lineHeight, 'r');

        $this->y -= 10;

        //payment list
        if ($order->getPayment()) {
            if ($order->getPayment()->getMethodInstance()->getcode() == 'MultiplePayment') {
                $payments = mage::helper('PointOfSales/Payment')->getMultiplePayments($order);

                if (count($payments) > 0) {
                    $page->drawLine(5, $this->y, $this->_BLOC_ENTETE_LARGEUR, $this->y);
                    $this->y -= 15;
                    $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), $this->_fontSize);

                    foreach ($payments as $payment) {
                        $paymentDate = Mage::helper('core')->formatDate($payment->getptp_date(), 'short', false);
                        $page->drawText($paymentDate, 5 + $this->margin_left, $this->y, 'UTF-8');

                        $caption_width = 40 * $this->width / 100;
                        $caption = $this->WrapTextToWidth($page, $payment->getptp_method(), $caption_width);
                        $offset = $this->DrawMultilineText($page, $caption, 50 + $this->margin_left, $this->y, $this->_fontSize, 0, $this->_lineHeight, false);
                        $this->drawTextInBlock($page, $order->formatPrice($payment->getptp_amount()), 243 + $this->margin_left, $this->y, $price_width, 20, 'r');
                        $this->y -= $offset;
                    }
                }
            } else {
                $page->drawLine(5, $this->y, $this->_BLOC_ENTETE_LARGEUR, $this->y);
                $this->y -= 15;
                $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), $this->_fontSize);

                $page->drawText(Mage::Helper('PointOfSales')->__('Payment method : ') . $order->getPayment()->getMethodInstance()->gettitle(), 5 + $this->margin_left, $this->y, 'UTF-8');

                $this->y -= 15;
            }
        }

        //add date & order id
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), $this->_fontSize);
        $page->drawLine(5, $this->y, $this->_BLOC_ENTETE_LARGEUR, $this->y);
        $this->y -= 15;
        $infos = Mage::helper('PointOfSales')->__('Order') . ' #' . $order->getincrement_id();
        $infos .= "\nVendor : " . Mage::helper('PointOfSales/User')->getUsernameForUserId($order->getPosUserId());
        $infos .= "\nDate : " . Mage::helper('core')->formatDate($order->getData('created_at'), 'medium', true);
        $offset = $this->DrawMultilineText($page, $infos, 5 + $this->margin_left, $this->y, $this->_fontSize, 0, $this->_lineHeight, false);
        $this->y -= $offset;

        $page->drawLine(5, $this->y, $this->_BLOC_ENTETE_LARGEUR, $this->y);
        $this->y -= 10;

        if (Mage::helper('PointOfSales')->hasToPrintBarcodeOnReceipt()) {
            $this->drawBarcode($page, $order->getincrement_id(), $this->width / 2 - 75, $this->y - 40, $this->width / 2 + 75, $this->y);
            $this->y -= 40;
        }

        if (Mage::helper('PointOfSales')->checkRewardPointModule()) {
            if (Mage::helper("posrewardpoints")->isAllowReceiptPrint()) {
                $total = $order->getGrandTotal();
                if (!Mage::helper("posrewardpoints")->isIncTax())
                    $total = $order->getGrandTotal() - $order->getTaxAmount();
                $points = Mage::helper("posrewardpoints")->getOrderRewardPoint($total);
                if ($points) {
                    $page->drawLine(5, $this->y, $this->_BLOC_ENTETE_LARGEUR, $this->y);
                    $this->y -= 10;
                    $storeCreditBalance = Mage::helper("posrewardpoints")->storeCreditBalance($order->getCustomerId());
                    $infos = sprintf(__("You earned %s credits with this order, \nyour credit balance is now %s \n %s"), $points, $storeCreditBalance, Mage::helper("posrewardpoints")->getFreeText());
                    $offset = $this->DrawMultilineText($page, $infos, 5 + $this->margin_left, $this->y, $this->_fontSize, 0, $this->_lineHeight, false);
                    $this->y -= $offset;
                }
            }
        }

        // add footer
        $this->drawFooter($page, $storeId);

        $this->_afterGetPdf();

        // check pdf height, if to long, build another one according to content
        $this->checkHeight();

        return $this->pdf;
    }

}
