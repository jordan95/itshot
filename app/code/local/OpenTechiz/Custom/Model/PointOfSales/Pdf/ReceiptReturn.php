<?php

/**
 * OnlineBiz Software Solution
 * 
 * @project autoviihde.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Joe Vu<joe@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2017 , OnlineBiz Software Solution
 * 
 * Create at: Jan 26, 2018 10:56:07 AM
 */
class Onlinebiz_Custom_Model_PointOfSales_Pdf_ReceiptReturn extends MDN_PointOfSales_Model_Pdf_ReceiptReturn {
    /**
     * generate PDF
     *
     * @param array $creditMemos
     * @return Zend_Pdf
     */
    public function getPdf($creditMemos = array()) {

        // save order in class attribute, used for check pdf height
        $this->orders = &$creditMemos;

        $this->_beforeGetPdf();
        $storeId = Mage::helper('PointOfSales/User')->getCurrentStoreId();

        // init
        $this->initSizes(Mage::getStoreConfig('pointofsales/receipt/width', $storeId), Mage::getStoreConfig('pointofsales/receipt/unit', $storeId), $creditMemos);

        if ($this->pdf == null)
            $this->pdf = new Zend_Pdf();

        $style = new Zend_Pdf_Style();
        $style->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD), $this->_fontSize);

        // create new page
        $settings['title'] = Mage::getStoreConfig('pointofsales/receipt/header', $storeId);

        $settings['store_id'] = $storeId;
        $page = $this->NewPage($settings); // add header
        // get first order
        $creditMemo = $creditMemos[0];

        //Affiche les lignes produit
        foreach ($creditMemo->getAllItems() as $item) {

            //Si c'est un sous produit, on affiche pas
            if ($item->getParentItem()) {
                continue;
            }

            //Pour les produits "standards"
            $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), $this->_fontSize);

            // [1]
            // add qty
            $qty_txt = (int) $item->getQty();
            $page->drawText($qty_txt.'x', 5 + $this->margin_left, $this->y, 'UTF-8');
            // [1]
            // [2]
            // build line : ajout de retour a la ligne afin que le texte rentre dans la largeur passee en parametre
            $caption_width = 40 * $this->width / 100;
            $caption = $this->WrapTextToWidth($page, $item->getName(), $caption_width);

            // add caption (product name and options)
            // return height of caption block
            $shelfLocation = Mage::helper('PointOfSales/ShelfLocation')->getShelfLocationForProduct($item->getProduct());
            
            // return height of caption block
            $offset = $this->DrawMultilineText($page, $caption.PHP_EOL.$shelfLocation , 25 + $this->margin_left, $this->y, $this->_fontSize, 0, $this->_lineHeight, false);
            // [2]
            // [3]
            // add price
            $price_width = 20 * $this->width / 100;
            $price_pos = $this->width * 70 / 100;
            $this->drawTextInBlock($page, $creditMemo->getOrder()->formatPriceTxt(round(-$item->getrow_total_incl_tax(), 2)), $price_pos, $this->y, $price_width, 20, 'r');
            // [3]

            $this->y -= $offset;
        }

        $this->y -= 5;

        //totals
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD), $this->_fontSize);
        $txt_pos = $price_pos - $price_width - 50;
        $page->drawText(Mage::Helper('PointOfSales')->__('Sub Total'), $txt_pos, $this->y, 'UTF-8');
        $this->drawTextInBlock($page, $creditMemo->getOrder()->formatPriceTxt(round(-$creditMemo->getsubtotal(), 2)), $price_pos, $this->y, $price_width, 20, 'r');

        if($creditMemo->getdiscount_amount() > 0) {
            $this->y -= 10;
            $page->drawText(Mage::Helper('PointOfSales')->__('Discount'), $txt_pos, $this->y, 'UTF-8');
            $this->drawTextInBlock(
                $page,
                $creditMemo->getOrder()->formatPriceTxt(round(-$creditMemo->getdiscount_amount(), 2)),
                $price_pos,
                $this->y,
                $price_width,
                20,
                'r'
            );
        }

        $this->y -= 10;

        $shipping_amount = $creditMemo->getbase_shipping_amount();
        if ($shipping_amount > 0) {

            $page->drawText(Mage::Helper('PointOfSales')->__('Shipping'), $txt_pos, $this->y, 'UTF-8');
            $this->drawTextInBlock($page, $creditMemo->getOrder()->formatPriceTxt(round(-$creditMemo->getbase_shipping_amount(), 2)), $price_pos, $this->y, $price_width, 20, 'r');

            $this->y -= 10;
        }

        // check taxes
        $page->drawText(Mage::Helper('PointOfSales')->__('Tax'), $txt_pos, $this->y, 'UTF-8');

        $this->drawTextInBlock($page, $creditMemo->getOrder()->formatPriceTxt(round(-$creditMemo->getbase_tax_amount() - $creditMemo->getbase_shipping_tax_amount(), 2)), $price_pos, $this->y, $price_width, $this->_lineHeight, 'r');

        $this->y -= 10;

        // grand total
        $page->drawText(Mage::Helper('PointOfSales')->__('Total'), $txt_pos, $this->y, 'UTF-8');
        $this->drawTextInBlock($page, $creditMemo->getOrder()->formatPriceTxt(-$creditMemo->getgrand_total()), $price_pos, $this->y, $price_width, $this->_lineHeight, 'r');

        $this->y -= 10;

        //payment list
        $payments = Mage::getModel('PaymentTracker/Payment')->getCollection()
            ->addFieldToFilter('ptp_order_id', array('eq' => $creditMemo->getOrder()->getId()))
            ->addFieldToFilter('ptp_amount', array('lt' => 0));

        if(count($payments)>0)
        {
            $page->drawLine(5, $this->y, $this->_BLOC_ENTETE_LARGEUR, $this->y);
            $this->y -= 15;
            $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), $this->_fontSize);

            foreach($payments as $payment)
            {
                $paymentDate = Mage::helper('core')->formatDate($payment->getptp_date(), 'short', false);
                $page->drawText($paymentDate, 5 + $this->margin_left, $this->y, 'UTF-8');

                $caption_width = 40 * $this->width / 100;
                $caption = $this->WrapTextToWidth($page, $payment->getptp_method(), $caption_width);
                $offset = $this->DrawMultilineText($page, $caption, 50 + $this->margin_left, $this->y, $this->_fontSize, 0, $this->_lineHeight, false);
                $this->drawTextInBlock($page, $creditMemo->getOrder()->formatPrice($payment->getptp_amount()), 243 + $this->margin_left, $this->y, $price_width, 20, 'r');
                $this->y -= $offset;
            }
        }
        else
        {
            $page->drawLine(5, $this->y, $this->_BLOC_ENTETE_LARGEUR, $this->y);
            $this->y -= 15;
            $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), $this->_fontSize);

            $page->drawText(Mage::Helper('PointOfSales')->__('Payment method : ').$creditMemo->getOrder()->getPayment()->getMethodInstance()->gettitle(), 5 + $this->margin_left, $this->y, 'UTF-8');

            $this->y -= 15;
        }



        //add date & order id
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), $this->_fontSize);
        $page->drawLine(5, $this->y, $this->_BLOC_ENTETE_LARGEUR, $this->y);
        $this->y -= 15;
        $infos = Mage::helper('PointOfSales')->__('Credit Memo') . ' #' . $creditMemo->getincrement_id();
        $infos .= "\nVendor : ".Mage::helper('PointOfSales/User')->getUsernameForUserId($creditMemo->getOrder()->getPosUserId());
        $infos .= "\nDate : " . Mage::helper('core')->formatDate($creditMemo->getData('created_at'), 'medium', true);
        $offset = $this->DrawMultilineText($page, $infos, 5 + $this->margin_left, $this->y, $this->_fontSize, 0, $this->_lineHeight, false);
        $this->y -= $offset;

        if(Mage::helper('PointOfSales')->hasToPrintBarcodeOnReceipt())
            $this->drawBarcode($page, $creditMemo->getincrement_id(), $this->width / 2 - 75,  10 , $this->width / 2 + 75, 50);

        // add footer
        $this->drawFooter($page);

        $this->_afterGetPdf();

        // check pdf height, if to long, build another one according to content
        $this->checkHeight();

        return $this->pdf;
    }
}
