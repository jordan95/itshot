<?php

class OpenTechiz_Custom_Model_FeedExport_Config extends Mirasvit_FeedExport_Model_Config
{
    public function getBasePath()
    {
        $dir = Mage::getBaseDir('base').'/sitemaps';
        if (!file_exists($dir)) {
            mkdir($dir);
        }

        return $dir;
    }
}