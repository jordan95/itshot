<?php

class OpenTechiz_Custom_Model_FeedExport_Feed extends Mirasvit_FeedExport_Model_Feed
{
    public function getUrl()
    {
        $feedUrl = false;
        if (file_exists(Mage::getModel('feedexport/config')->getBasePath().DS.$this->getFilenameWithExt())) {
            $feedUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'sitemaps'.DS.$this->getFilenameWithExt();
            if (Mage::getBaseUrl() !== substr($feedUrl, 0, strlen(Mage::getBaseUrl()))) {
                $feedUrl = str_replace(Mage::getModel('core/url')->parseUrl($feedUrl)->getHost(), Mage::app()->getRequest()->getHttpHost(), $feedUrl);
            }
        }

        return $feedUrl;
    }
    

    public function getArchiveUrl()
    {
        if ($this->getArchivation() &&
            file_exists(Mage::getModel('feedexport/config')->getBasePath().DS.$this->getFilenameWithExt().'.'.$this->getArchivation())
        ) {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'sitemaps'.DS.$this->getFilenameWithExt().'.'.$this->getArchivation();
        }

        return false;
    }
}