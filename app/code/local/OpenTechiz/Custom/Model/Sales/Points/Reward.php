<?php

class OpenTechiz_Custom_Model_Sales_Points_Reward extends Mage_Sales_Model_Order_Pdf_Total_Default
{
    /**
     * Get array of arrays with totals information for display in PDF
     * array(
     *  $index => array(
     *      'amount'   => $amount,
     *      'label'    => $label,
     *      'font_size'=> $font_size
     *  )
     * )
     * @return array
     */
    public function getTotalsForDisplay()
    {
        $order = $this->getOrder();
        
        $ar = array();
        $k=0;
        foreach ( $order->getTotalEarnedPoints() as $pt_entry ) {
            if ($pt_entry > 0) {
                $k = 1;
                break;
            }
        }
        if($k == 1)
            $ar[] = array(
                'amount' => $order->getTotalEarnedPointsAsString(),
                'label' => Mage::helper('rewards')->__("Point Earned "),
                'font_size' => $this->getFontSize() ? $this->getFontSize() : 7
            );

        if($order->getHasSpentPoints()){
            $ar[] = array(
                'amount' => $order->getTotalSpentPointsAsString(),
                'label' => Mage::helper('rewards')->__("Point Spend "),
                'font_size' => $this->getFontSize() ? $this->getFontSize() : 7
            );
        }
        return $ar;
    }
}
