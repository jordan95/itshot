<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('opentechiz_review/ekomifeedback');
$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
if ($connection->isTableExists($tableName)) {
    $connection->dropTable($tableName);
}

$table = $connection->newTable($tableName)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'identity' => true,
        'primary'  => true,
        'unsigned' => true,
        'nullable' => false,
    ], 'Id')
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'nullable' => true,
        
    ], 'Order ID')
    ->addColumn('rate', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'nullable' => true,
    ], 'Rate')
    ->addColumn('point', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'nullable' => true,
    ], 'Point')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, [
        'nullable' => false,
    ], 'Created at')
    ->addIndex($installer->getIdxName('opentechiz_review/ekomifeedback', array('order_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE), array('order_id'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
    ->setComment('Order Ekomi Feedback');

$connection->createTable($table);
$installer->endSetup();
