<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('testimonials/testimonials'),'order_id', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'default'   => null,
    'nullable'  => true,
    'length'    => 10,
    'after'     => 'status', // column name to insert new column after
    'comment'   => 'Order ID'
    ));   
$installer->endSetup();