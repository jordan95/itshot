<?php

class OpenTechiz_Review_Model_Rewrite_Review extends Mage_Review_Model_Review
{

    public function getRatingPercent()
    {
        $ratingPercent = 0;
        $summary = Mage::getModel('review/review_summary')->load($this->getentity_pk_value());
        if ($summary->getId()) {
            $ratingPercent = $summary->getRatingSummary();
        }

        $_votes = $this->getRatingVotes();
        foreach ($_votes as $_vote):
            if ($_vote->getRatingCode() == "Overall Value" && $ratingPercent < $_vote->getPercent()) {
                $ratingPercent = $_vote->getPercent();
                break;
            }
        endforeach;
        return $ratingPercent;
    }

}
