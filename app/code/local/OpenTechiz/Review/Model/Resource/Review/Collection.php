<?php

class OpenTechiz_Review_Model_Resource_Review_Collection extends Mage_Review_Model_Resource_Review_Collection{
        protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()
            ->join(
                array('det' => $this->_reviewDetailTable),
                'main_table.review_id = det.review_id',
                array('det.detail_id', 'det.title', 'det.detail', 'det.nickname', 'det.photo', 'det.customer_id')
            );
        return $this;
    }
}