<?php

class OpenTechiz_Review_Model_Ekomifeedback extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('opentechiz_review/ekomifeedback');
    }

}
