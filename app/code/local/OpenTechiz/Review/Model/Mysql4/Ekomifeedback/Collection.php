<?php

class OpenTechiz_Review_Model_Mysql4_Ekomifeedback_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    public function _construct()
    {
        $this->_init('opentechiz_review/ekomifeedback');
    }

}
