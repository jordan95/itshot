<?php

class OpenTechiz_Review_Model_Feed extends Mage_Core_Block_Template
{

    public function generate($filename = null)
    {
        if (!$filename) {
            $filename = Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . 'sitemaps' . DIRECTORY_SEPARATOR . 'product_review_feeds.xml';
        }
        $this->saveXml($filename);
    }

    protected function saveXml($filename)
    {
        Mage::helper('feedexport/io')->write($filename, $this->toXml());
    }

    public function getReviewCollection()
    {
        $resource = Mage::getSingleton('core/resource');
        $url_rewrite = $resource->getTableName('core_url_rewrite');
        $catalog_product_entity = $resource->getTableName('catalog_product_entity');
        $catalog_product_entity_int = $resource->getTableName('catalog_product_entity_int');
        $catalog_product_entity_varchar = $resource->getTableName('catalog_product_entity_varchar');
        $eav_attribute_option_value = $resource->getTableName('eav_attribute_option_value');
        $storeId = Mage::app()->getStore()->getId();
        $collection = Mage::getModel('review/review')->getCollection()
                ->addStoreFilter($storeId)
                ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
                ->addFieldToFilter('main_table.entity_id', Mage_Review_Model_Review::ENTITY_PRODUCT)
                ->setDateOrder()
                ->addExpressionFieldToSelect('product_url', new Zend_Db_Expr("(select request_path from {$url_rewrite} where id_path = CONCAT('product/', main_table.entity_pk_value) limit 1)"), array())
                ->addExpressionFieldToSelect('product_sku', new Zend_Db_Expr("(select value from {$catalog_product_entity_varchar} where `entity_id` = main_table.`entity_pk_value` and `attribute_id` = 2058 AND `entity_type_id` = '4' limit 1)"), array())
                ->addExpressionFieldToSelect('product_sku2', new Zend_Db_Expr("(select value from {$catalog_product_entity_varchar} where `entity_id` = main_table.`entity_pk_value` and `attribute_id` = 2032 AND `entity_type_id` = '4' limit 1)"), array())
                ->addExpressionFieldToSelect('product_sku_default', new Zend_Db_Expr("(select sku from {$catalog_product_entity} where entity_id = main_table.entity_pk_value limit 1)"), array())
                ->addExpressionFieldToSelect('product_brand', new Zend_Db_Expr("(select value from {$eav_attribute_option_value} where option_id = (select value from {$catalog_product_entity_int} where entity_id = main_table.entity_pk_value and attribute_id=2050 limit 1) limit 1)"), array());
        $collection->getSelect()->where('main_table.entity_pk_value in ?', new Zend_Db_Expr("(SELECT `entity_id` FROM `tsht_catalog_product_entity_int` WHERE `attribute_id` = '96' AND `value` = '1' AND `entity_type_id` = '4')"));
        return $collection;
    }

    private function getReviewXml()
    {
        $xml = '';
        $reviews = $this->getReviewCollection();
        if ($reviews->count() > 0) {
            $reviews->addRateVotes();
            foreach ($reviews as $_review) {
                $content = $this->getReviewContent($_review);
                $customer_name = $this->getReviewCustomerName($_review);
                $product_url = $this->getReviewProductUrl($_review);
                $product_sku = $this->getProductSku($_review);
                $product_brand = $_review->getData('product_brand');
                $review_url = $_review->getReviewUrl();
                $rating_value = $this->getReviewRatingValue($_review);
                $review_timestamp = $this->getReviewTimestamp($_review);
                if (!$this->nonEmptyStringType($product_sku) || !$this->nonEmptyStringType($customer_name) || !$this->nonEmptyStringType($product_brand)) {
                    continue;
                }
                $xml .= $this->renderReviewItemXml($_review->getId(), $content, $rating_value, $product_url, $product_sku, $product_brand, $review_url, $customer_name, $review_timestamp);
            }
        }
        return $xml;
    }

    protected function nonEmptyStringType($value)
    {
        return preg_match('/\s*\S+[\s\S]*/', $value);
    }

    private function getReviewTimestamp($_review)
    {
        return gmDate("Y-m-d\TH:i:s\Z", strtotime($_review->getCreatedAt()));
    }
    
    private function getProductSku($_review)
    {
        if($_review->getData('product_sku')){
            return $_review->getData('product_sku');
        }
        if($_review->getData('product_sku2')){
            return $_review->getData('product_sku2');
        }
        return $_review->getData('product_sku_default');
    }

    private function getReviewUrl($_review)
    {
        return Mage::getUrl('review/product/view', array('id' => $_review->getId()));
    }

    private function getReviewRatingValue($_review)
    {
        $_votes = $_review->getRatingVotes();
        if (count($_votes))
            $_voteCount = 0;
        foreach ($_votes as $_vote) {
            $_voteCount += $_vote->getValue();
        }
        return number_format($_voteCount / count($_votes), 1);
    }

    private function getReviewCustomerName($_review)
    {
        return html_entity_decode(explode(' ', $_review->getNickname(), 2)[0]);
    }

    private function getReviewProductUrl($_review)
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $_review->getData('product_url');
    }

    private function getReviewContent($_review)
    {
        $content = nl2br($this->escapeHtml(html_entity_decode($_review->getDetail(), ENT_COMPAT | ENT_HTML401, "UTF-8")));
        $content = strip_tags($content);
        $content = htmlspecialchars_decode($content);
        $content = trim($content, "& \t\n\r\0\x0B");
        return preg_replace('/[^[:print:]]/', '', $content);
    }

    private function renderReviewItemXml($review_id, $content, $rating_value, $product_url, $product_sku, $product_brand, $review_url, $customer_name, $review_timestamp)
    {
        return <<<EOD
<review>
    <review_id>{$review_id}</review_id>
    <reviewer>
        <name><![CDATA[{$customer_name}]]></name>
    </reviewer>
    <review_timestamp>{$review_timestamp}</review_timestamp>
    <content><![CDATA[{$content}]]></content>
    <review_url type="singleton">{$review_url}</review_url>
    <ratings>
        <overall min="1" max="5">{$rating_value}</overall>
    </ratings>
    <products>
        <product>
            <product_ids>
                <skus>
                    <sku><![CDATA[{$product_sku}]]></sku>
                </skus>
                <brands>
                    <brand><![CDATA[{$product_brand}]]></brand>
                </brands>
            </product_ids>
            <product_url>{$product_url}</product_url>
        </product>
    </products>
</review>
EOD;
    }

    private function getPublisher()
    {
        return Mage::app()->getStore()->getFrontendName();
    }

    private function getFavicon()
    {
        return Mage::getDesign()->getSkinUrl('favicon.ico');
    }

    public function toXml()
    {
        $publisher = $this->getPublisher();
        $favicon = $this->getFavicon();
        $reviewXml = $this->getReviewXml();
        return <<<EOD
<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns:vc="http://www.w3.org/2007/XMLSchema-versioning" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation=
 "http://www.google.com/shopping/reviews/schema/product/2.2/product_reviews.xsd">
    <version>2.2</version>
    <publisher>
        <name>{$publisher}</name>
        <favicon>{$favicon}</favicon>
    </publisher>
    <reviews>
        {$reviewXml}
    </reviews>
</feed>
EOD;
    }

}
