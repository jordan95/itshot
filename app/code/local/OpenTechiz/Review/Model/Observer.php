<?php

class OpenTechiz_Review_Model_Observer
{

    public function update(Varien_Event_Observer $observer){
        
        $review = $observer->getEvent()->getObject();
        $product_id = $review->getEntityPkValue();
        $product = Mage::getModel("catalog/product")->load($product_id);

        if ($review->getOrigData('status_id') != Mage_Review_Model_Review::STATUS_APPROVED && $review->getData('status_id') == Mage_Review_Model_Review::STATUS_APPROVED) {
            if($product->getId()){
                $list_categories = $product->getCategoryIds();
                if(!empty($list_categories)){
                    foreach ($list_categories as $cat_id) {
                         $cate = Mage::getModel('catalog/category')->load($cat_id);
                         $cate->setUpdatedAt(date("Y-m-d H:i:s"));
                         $cate->save();
                    } 
                    
                }
                $product->setUpdatedAt(date("Y-m-d H:i:s"));
                $product->save();
            }
            
        }
        

    }
}
