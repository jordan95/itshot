<?php

class OpenTechiz_Review_Adminhtml_Review_FeedController extends Mage_Adminhtml_Controller_Action
{

    public function generateAction()
    {
        try {
            Mage::getModel('opentechiz_review/feed')->generate();
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Product Review Feed is generated successfully'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        } 
        return $this->_redirect('*/catalog_product_review/index');
    }

    protected function _isAllowed()
    {
        return true;
    }

}
