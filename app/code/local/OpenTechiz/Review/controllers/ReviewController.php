<?php

class OpenTechiz_Review_ReviewController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $ruleCollection = Mage::getSingleton('rewards/special_validator')->getApplicableRulesOnReview();
        foreach ($ruleCollection as $rule) {
            $message = 'You will receive %s upon approval of this review';
            Mage::getSingleton('core/session')->addSuccess(
                    Mage::helper('rewards')->__(
                            $message, (string) Mage::getModel('rewards/points')->set($rule)
                    )
            );
        }
        $this->_redirect('/');
    }

}
