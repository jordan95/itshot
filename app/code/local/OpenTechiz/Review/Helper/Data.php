<?php

class OpenTechiz_Review_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getStarRatingHtml($ratingSummary, $iconSize = 13, $highlightClass = 'rating-hightlight', $defaultClass = 'rating-default')
    {
        $width = $ratingSummary ? "width:$ratingSummary%;"  : '' ;
        return <<<EOD
            <div class="start-rating-wapper">
                <div class="star-rating $defaultClass" style="font-size:{$iconSize}px">★★★★★</div>
                <div class="star-rating $highlightClass" style="$width font-size:{$iconSize}px" >★★★★★</div>
            </div>
EOD;
    }
    
    public function getPointsOnReview($current_id = 1){
        $sumary_point = 0;
        $ruleCollection = Mage::getSingleton('rewards/special_validator')->getApplicableRulesOnReview();
        foreach ($ruleCollection as $rule){
            $points = Mage::getModel('rewards/points')->set($rule)->getPoints();
            $sumary_point += $points[$current_id];
        }
        return $sumary_point;
    }

}
