<?php

class OpenTechiz_Review_Block_Testimonials extends Jextn_Testimonials_Block_Testimonials
{

    public function getAllItems($orderID)
    {
        $productIDs = $this->getAllProductIDsByOrder($orderID);
        return count($productIDs) > 0 ? $this->getProductCollection($productIDs) : false;
    }

    public function getProductCollection($productIDs)
    {
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        return Mage::getResourceModel('catalog/product_collection')
                        ->addFieldToFilter('entity_id', array('in' => $productIDs))
                        ->addAttributeToSelect($attributes)
                        ->addUrlRewrite();
    }

    public function getAllProductIDsByOrder($orderId)
    {
        /* @var $order Mage_Sales_Model_Order */
        $order = Mage::getModel('sales/order')->load($orderId);
        if (!$order->getId()) {
            return false;
        }
        $items = $order->getAllItems();
        $_PIDs = [];
        foreach ($items as $item) {
            if($item->getProductType() == 'virtual') continue; //exclude virtual products
            $_PIDs[] = $item->getProductId();
        }
        return $_PIDs;
    }

}
