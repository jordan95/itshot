<?php

class OpenTechiz_Review_Block_Catalog_Product_Recommended extends Mage_Core_Block_Template
{

    const ATTRIBUTE_DISP_CUSTOM_STOCK_MSG_CODE = 'c2c_disp_custom_stock_msg';

    protected $_productCollection;
    protected $_customer;

    protected function _construct()
    {
        $this->setTemplate('opentechiz/review/catalog/product/recommended.phtml');
    }

    public function setCustomer($customer)
    {
        $this->_customer = $customer;
        return $this;
    }

    public function getCustomer()
    {
        return $this->_customer;
    }

    public function getBestsellingProducts()
    {
        $productIds = [];
        $itemsCollection = Mage::getResourceModel('sales/order_item_collection')
                ->join('order', 'order_id=entity_id')
                ->addFieldToFilter('main_table.store_id', array('eq' => 1))
                ->setOrder('main_table.created_at', 'desc');
        $select = $itemsCollection->getSelect();
        $select->limit(8);
        $select->columns(array('item_created' => new Zend_Db_Expr('max(main_table.created_at)')));
        $select->order('item_created DESC');
        $select->group(array('main_table.product_id'));
        if (sizeof($itemsCollection) > 0) {
            foreach ($itemsCollection as $item) {
                $productIds[] = $item->getProductId();
            }
        }

        return $productIds;
    }

    public function getRecommendProduct($customer_id = null)
    {
//        if(is_numeric($customer_id)){
//            $customer = Mage::getModel('rewards/customer')->load($customer_id);
//        } elseif($customer_id instanceof  Mage_Customer_Model_Customer){
//            $customer = $customer_id;
//        }
//        if(!$customer->getId()){
//           return;
//        }
//        $product_ids = array();
//        $this->getRecommendProductFromQuote($product_ids, $customer);
//        $count_1 = count($product_ids);
//        if ($count_1 < 8) {
//            $this->getRecommendProductFromWishlist($product_ids, $customer, 8 - $count_1);
//        }
//        $count_2 = count($product_ids);
//        if ($count_2 < 8) {
//            $this->getRecommendProductFromBestseller($product_ids, 8 - $count_2);
//        }
        return $this->getBestsellingProducts();
    }

    protected function getRecommendProductFromQuote(&$product_ids, $customer, $limit = 8)
    {
        $cartItems = Mage::getModel('sales/quote')
                ->loadByCustomer($customer)
                ->getItemsCollection()
                ->addFieldToFilter('parent_item_id', array('null' => true));
        $cartItems->getSelect()->limit($limit);
        if ($cartItems->getSize()) {
            foreach ($cartItems as $cartItem) {
                $product_ids[] = $cartItem->getProductId();
            }
        }
    }

    protected function getRecommendProductFromWishlist(&$product_ids, $customer, $limit = 8)
    {
        $wishLists = Mage::getModel('wishlist/wishlist')->loadByCustomer($customer);
        $collection = $wishLists->getItemCollection();
        $collection->getSelect()->limit($limit);
        if ($collection->count()) {
            foreach ($collection as $wishList) {
                $product_ids[] = $wishList->getProductId();
            }
        }
    }

    protected function getRecommendProductFromBestseller(&$product_ids, $limit = 8)
    {
        $storeId = Mage::app()->getStore()->getId();
        $products = Mage::getResourceModel('reports/product_collection')
                ->addOrderedQty()
                ->addAttributeToSelect(array('name')) //edit to suit tastes
                ->setStoreId($storeId)
                ->addStoreFilter($storeId)
                ->setOrder('ordered_qty', 'desc'); //best sellers on top
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
        $products->setPageSize($limit)->setCurPage(1);
        $bestsellers = $products->getData();

        foreach ($bestsellers as $bestseller) {
            $product_ids[] = $bestseller["entity_id"];
        }
    }

    public function addStockFilter($collection)
    {
        $collection->addAttributeToFilter(self::ATTRIBUTE_DISP_CUSTOM_STOCK_MSG_CODE, array(
            'or' => array(
                array('eq' => 0),
                array('null' => true))
                ), 'left');
        return $collection;
    }

    public function getProductCollection()
    {
        if (!$this->_productCollection) {
            $customer = $this->getCustomer();
            $product_ids = $this->getRecommendProduct($customer);
            $collection = Mage::getResourceModel('catalog/product_collection')
                    ->addAttributeToSelect('*')
                    ->setStoreId(Mage::app()->getDefaultStoreView()->getId())
                    ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                    ->addFieldToFilter('entity_id', array('in' => $product_ids));
            $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
            $this->addStockFilter($collection);
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
            $this->_productCollection = $collection;
        }
        return $this->_productCollection;
    }

}
