<?php

class OpenTechiz_Review_Block_Adminhtml_Grid
    extends Medialounge_Review_Block_Adminhtml_Grid
{

    public function _prepareColumns()
    {
        $this->addColumnAfter('customer_id', array(
            'header'    => $this->__('Customer Email'),
            'width'     => '100',
            'type'      => 'text',
            'renderer'  => 'OpenTechiz_Review_Block_Adminhtml_Renderer_Email',
            'filter_condition_callback' => array($this, '_EmailFilter')
        ),'title');
        $this->addColumnAfter('photo', array(
            'header'    => $this->__('Customer Image'),
            'filter' => false,
            'sortable' => false,
            'width'     => '100',
            'renderer'  => 'OpenTechiz_Review_Block_Adminhtml_Renderer_Image'
        ),'sku');
         $this->addColumnAfter('entity_id', array(
            'header'    => $this->__('Product Image'),
            'filter' => false,
            'sortable' => false,
            'width'     => '100',
            'renderer'  => 'OpenTechiz_Review_Block_Adminhtml_Renderer_ProductImage'
        ),'sku');
        parent::_prepareColumns();
        return $this;
    }
    protected function _EmailFilter($collection, $column)
    {
       
        $value = $column->getFilter()->getValue();

        $collection->getSelect()->join(array('customer'=>'tsht_customer_entity'), 'customer.entity_id = rdt.customer_id AND customer.email LIKE '.'\'%'.$value.'%\'' , 'customer.email');
        return $this;
    }
    public function setCollection($collection)
    {

        $collection->getSelect()
            ->columns(['rdt.citystate', 'rdt.country', 'rdt.photo']);
        $this->_collection = $collection;
        return $this;
    }
}