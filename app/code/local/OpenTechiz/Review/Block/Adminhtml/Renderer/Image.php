<?php

class OpenTechiz_Review_Block_Adminhtml_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $path_img =  $row->getData('photo');
        $html ="";
        if($path_img !=""){
        	$imageUrl = Mage::getBaseUrl('media') .DS.$path_img;
        	$html = '<img src="'.$imageUrl.'" width="120px" height="120px">';
        }
       
        return $html;
    }
}