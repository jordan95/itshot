<?php

class OpenTechiz_Review_Block_Adminhtml_Renderer_Email extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $customer_id =  $row->getData('customer_id');
        $customer = Mage::getModel('customer/customer')->load($customer_id);
        if($customer){
            return $customer->getEmail();
        }
        return "";
    }
}