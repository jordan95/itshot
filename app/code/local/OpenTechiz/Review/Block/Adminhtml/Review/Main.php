<?php

class OpenTechiz_Review_Block_Adminhtml_Review_Main extends Mage_Adminhtml_Block_Review_Main
{

    public function __construct()
    {
        parent::__construct();
        $this->addButton('generate_feed', array(
            'label' => Mage::helper('reports')->__('Generate Feed'),
            'onclick' => "setLocation('{$this->getUrl('*/review_feed/generate')}')",
        ));
    }
}
