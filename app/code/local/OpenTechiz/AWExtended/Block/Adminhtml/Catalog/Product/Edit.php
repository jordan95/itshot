<?php

class OpenTechiz_AWExtended_Block_Adminhtml_Catalog_Product_Edit extends AW_Productupdates_Block_Adminhtml_Catalog
{

    protected function _prepareLayout()
    {
        $product = $this->getProduct();
        $name = '';
        if (!$product->getMetaTitle()) {
            $name = $product->getName();
        } else {
            $name = $product->getMetaTitle();
        }
        $urlKey = Mage::getResourceModel('catalog/url')->getProductModel()->formatUrlKey($name);
        $this->setChild('generate_url_key', $this->getLayout()->createBlock('adminhtml/widget_button')
                        ->setData(array(
                            'label' => Mage::helper('catalog')->__('Generate URL Key'),
                            'onclick' => "$('url_key').value='" . $urlKey . "'"
                        ))
        );

        return parent::_prepareLayout();
    }

    public function getSaveButtonHtml()
    {
        $saveButtonHtml = parent::getSaveButtonHtml();
        return $saveButtonHtml . $this->getChildHtml('generate_url_key');
    }

}
