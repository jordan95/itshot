<?php
class OpenTechiz_AWExtended_Model_Observer
{
    public function checkoutCartProductAddBefore(){
        $request = Mage::app()->getRequest();
        $productId = (int) $request->getParam('product');
        if ($productId) {
            $productStatus = Mage::getModel('catalog/product')->load($productId)->getStatus();
            if ($productStatus == 2) {
                Mage::getSingleton('core/session')->setIsProductDisabled(true);
            } else {
                Mage::getSingleton('core/session')->setIsProductDisabled(false);
            }
        }else{
            Mage::getSingleton('core/session')->setIsProductDisabled(false);
        }
    }
}