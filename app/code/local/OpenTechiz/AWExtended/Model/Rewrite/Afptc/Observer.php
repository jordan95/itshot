<?php
class OpenTechiz_AWExtended_Model_Rewrite_Afptc_Observer extends AW_Afptc_Model_Observer
{
    public function checkoutCartSaveAfter($observer){
        if (Mage::helper('awafptc')->extensionDisabled()) {
            return $this;
        }
        
        $cart = $observer->getCart();
        $store = Mage::app()->getStore();
        $rulesCollection = Mage::getResourceModel('awafptc/rule')->getActiveRulesCollection($store);
        $isRuleApplied = false;
        foreach ($rulesCollection as $ruleModel) {
            /** @var AW_Afptc_Model_Rule $ruleModel */
            $ruleModel->load($ruleModel->getId());

            if (!$ruleModel->validate($cart)) {
                continue;
            }

            if (isset($_stopFlag)) {
                break;
            }

            if ($ruleModel->getStopRulesProcessing()) {
                $_stopFlag = true;
            }

            if ($ruleModel->getShowPopup()) {
                continue;
            }

            if (
                (
                    Mage::helper('awafptc')->getDeclineRuleCookie($ruleModel->getId())
                    || Mage::registry('rule_decline') == $ruleModel->getId()
                )
                && !Mage::registry('ignore_decline')
            ) {
                continue;
            }

            try {
                $ruleModel->apply($cart, null);
                $isRuleApplied = true;
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }

        if($isRuleApplied) {
            $cart->getQuote()->unsTotalsCollectedFlag()->collectTotals()->save();
        }

        return $this;
    }
}