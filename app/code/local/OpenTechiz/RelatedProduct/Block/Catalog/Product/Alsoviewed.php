<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Product View block
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @module     Catalog
 */
class OpenTechiz_RelatedProduct_Block_Catalog_Product_Alsoviewed extends Mage_Catalog_Block_Product_Abstract
{

    const ATTRIBUTE_ID_STATUS = 96;
    const ATTRIBUTE_ID_GENDER = 2051;
    const ATTRIBUTE_ID_META_KEYWORD = 83;
    const ATTRIBUTE_DISP_CUSTOM_STOCK_MSG_CODE = 'c2c_disp_custom_stock_msg';

    protected function _construct()
    {
        $currency_code = Mage::app()->getStore()->getCurrentCurrencyCode();
        $this->addData(array(
            'cache_lifetime' => Mage::getStoreConfig('relatedproduct/general/cache_lifetime'),
            'cache_tags' => array(Mage_Catalog_Model_Product::CACHE_TAG),
            'cache_key' => $currency_code . '-related_products-' . $this->getProduct()->getId(),
        ));
    }

    public function getMetaKeyword()
    {
        $_product = $this->getProduct();
        $meta_keyword = $_product->getData('meta_keyword');
        return $this->getWordsByLength($meta_keyword);
    }

    /**
     * Get product ids same meta keywords
     * 
     * @return []int Product Ids
     */
    public function getProductIdsByMetaKeyword()
    {
        if (Mage::registry('product_ids_meta_keyword') !== null) {
            return Mage::registry('product_ids_meta_keyword');
        }
        $_product = $this->getProduct();
        $words = $this->getMetaKeyword();
        $productIds = array();
        $sql = '';
        if (count($words) > 0) {
            $countWords = count($words);
            $sql = 'SELECT DISTINCT (icpet.entity_id)';
            $sql .= ' FROM ' . $this->getTableName('catalog_product_entity_text') . ' AS icpet';
            $sql .= ' JOIN ' . $this->getTableName('catalog_product_entity_int') . ' AS icpei ON icpei.entity_id = icpet.entity_id';
            $sql .= ' JOIN ' . $this->getTableName('cataloginventory_stock_item') . ' AS stock_items ON icpei.entity_id = stock_items.product_id';
            $sql .= ' WHERE icpei.attribute_id = ' . self::ATTRIBUTE_ID_STATUS . ' AND icpei.value = 1'; //check status
            $sql .= ' AND stock_items.is_in_stock = 1';
            $sql .= ' AND icpet.attribute_id = ' . self::ATTRIBUTE_ID_META_KEYWORD . ' AND (';
            for ($i = 0; $i < $countWords; $i++) {
                $word = $this->quoteString("[[:<:]]$words[$i][[:>:]]");
                if ($i === 0) {
                    $sql .= ' icpet.value RLIKE ' . $word;
                } else {
                    $sql .= ' OR icpet.value RLIKE ' . $word;
                }
            }
            $sql .= ' )';
            $sql .= ' AND icpet.entity_id <>' . $_product->getId();
            $sql .= ' ';
//            $limit = $this->getConfigLimitMatching();
//            if ($limit) {
//                $sql .= ' LIMIT ' . $limit;
//            }
            $productIds = $this->queryIds($sql);
        }
        Mage::register('product_ids_meta_keyword', $productIds);
        $this->showDebug(__FUNCTION__, $sql);
        return $productIds;
    }

    /**
     * Get product ids same categories( child categories)
     * 
     * @param []int $excludeIds Exclude product ids
     * @return []int
     */
    public function getProductIdsByCategoryIds($excludeIds = array())
    {
        $_product = $this->getProduct();
        $categoryIds = $this->getAllCategoryIds();
        $productIds = array();
        $sql = '';
        if (count($categoryIds) > 0) {
            $sql = 'SELECT DISTINCT(a.product_id)';
            $sql .= ' FROM ' . $this->getTableName('catalog_category_product') . ' AS a';
            $sql .= ' JOIN ' . $this->getTableName('catalog_product_entity_int') . ' AS icpei ON icpei.entity_id = a.product_id';
            $sql .= ' JOIN ' . $this->getTableName('cataloginventory_stock_item') . ' AS stock_items ON a.product_id = stock_items.product_id';
            $sql .= ' WHERE icpei.attribute_id=' . self::ATTRIBUTE_ID_STATUS . ' AND icpei.value=1'; //check status
            $sql .= ' AND a.category_id IN(' . join(',', $categoryIds) . ')';
            if (count($excludeIds) > 0) {
                $sql .= ' AND a.product_id NOT IN(' . join(',', $excludeIds) . ')';
            }
            $sql .= ' AND stock_items.is_in_stock = 1';
            $sql .= ' AND a.product_id <>' . $_product->getId();
            $limit = $this->getConfigLimitMatching();
            if ($limit) {
                $sql .= ' LIMIT ' . $limit;
            }
            $productIds = $this->queryIds($sql);
        }
        $this->showDebug(__FUNCTION__, $sql);
        return $productIds;
    }

    /**
     * Add relevance to collection when get products same keywords
     * 
     * @param Mage_Catalog_Model_Product_Collection $collection
     * @return Mage_Catalog_Model_Product_Collection
     */
    public function addRelevance($collection)
    {
        $words = $this->getMetaKeyword();
        $countWords = count($words);
        if ($countWords > 0) {
            $scoreExpressionField = '(';
            for ($i = 0; $i < $countWords; $i++) {
                $score = $i == 0 ? $countWords * 5 : 5;
                $word = $this->quoteString("[[:<:]]$words[$i][[:>:]]");
                $scoreExpressionField .= ' IF(icpet.value RLIKE ' . $word . ', ' . $score . ', 0)';
                if ($i != $countWords - 1) {
                    $scoreExpressionField .= ' + ';
                }
            }
            $scoreExpressionField .= ')';
            $collection->getSelect()->columns(array('score' => new Zend_Db_Expr($scoreExpressionField)));
            $collection->getSelect()->joinLeft(array('icpet' => $this->getTableName('catalog_product_entity_text')), 'icpet.entity_id = e.entity_id AND icpet.store_id = ' . Mage::app()->getStore()->getId() . ' AND icpet.attribute_id = ' . self::ATTRIBUTE_ID_META_KEYWORD, array());
            $collection->getSelect()->order('score DESC');
        }
        return $collection;
    }

    /**
     * Add filter stock to collection
     * 
     * @param Mage_Catalog_Model_Product_Collection $collection
     * @return Mage_Catalog_Model_Product_Collection
     */
    public function addStockFilter($collection)
    {
        $collection->addAttributeToFilter(self::ATTRIBUTE_DISP_CUSTOM_STOCK_MSG_CODE, array(
            'or' => array(
                array('eq' => 0),
                array('null' => true))
                ), 'left');
        return $collection;
    }

    /**
     * Add filter range price to collection
     * 
     * @param Mage_Catalog_Model_Product_Collection $collection
     * @return Mage_Catalog_Model_Product_Collection
     */
    public function addRangePrice($collection)
    {
        $_product = $this->getProduct();
        $special_price = $_product->getSpecialPrice();
        $percent = (int) Mage::getStoreConfig('relatedproduct/general/percent_maximum_price_allowed');
        if (!$percent) {
            $percent = 20;
        }
        $max_price = $special_price * (1 + $percent / 100);
        $collection->addAttributeToFilter('special_price', array('from' => $special_price, 'to' => $max_price));
        return $collection;
    }

    /**
     * Add filter gender to collection
     * 
     * @param Mage_Catalog_Model_Product_Collection $collection
     * @return Mage_Catalog_Model_Product_Collection
     */
    public function addGender($collection)
    {
        $_product = $this->getProduct();
        if ($_product->getData('c2c_gender_for_filter')) {
            $collection->addAttributeToFilter('c2c_gender_for_filter', $_product->getData('c2c_gender_for_filter'));
        }
        return $collection;
    }

    public function getProductCollection($includeIds = array(), $limit = 3, $useRelevance = false)
    {
        $_product = $this->getProduct();
        $collection = Mage::getModel('catalog/product')->getCollection();
        if (count($includeIds) > 0) {
            $collection->addFieldToFilter('entity_id', array('in' => $includeIds));
        } else {
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
            $collection->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                    ->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        }
        $this->addRangePrice($collection);
        $this->addGender($collection);
        if ($useRelevance) {
            $this->addRelevance($collection);
        }
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collection->addAttributeToSelect($attributes);
        $collection->addUrlRewrite();
        $this->addStockFilter($collection);
        $collection->getSelect()->limit($limit);
        $products = array();
        $ids = array();
        if ($collection->count() > 0) {
            foreach ($collection as $p) {
                $products[] = $p;
                $ids[] = $p->getId();
            }
        }
        $c1 = array();

        //still not enough products for related products
        if ($collection->count() < $limit) {
            $limit_1 = $limit - $collection->count();
            $includeIds_1 = array_diff($includeIds, $ids);
            if (count($includeIds_1) > 0) {
                if ($useRelevance) {
                    $c1 = $this->getProductBestRelevance($includeIds_1, $limit_1);
                } else {
                    $c1 = $this->getProductPriceNearest($includeIds_1, $limit_1);
                }
            }
        }
        if (count($c1) > 0) {
            foreach ($c1 as $p1) {
                $products[] = $p1;
            }
        }
        $this->showDebug(__FUNCTION__ . '(' . count($products) . ')');

        return $products;
    }

    /**
     * Get product best relevance if keywords are not matched
     * 
     * @param []int $includeIds
     * @return []Mage_Catalog_Model_Product
     */
    public function getProductBestRelevance($includeIds, $limit = 3)
    {
        $products = array();
        $_product = $this->getProduct();
        $collection = Mage::getResourceModel('catalog/product_collection');
        $percent = (int) Mage::getStoreConfig('relatedproduct/general/percent_minimum_price_allowed');
        if (!$percent) {
            $percent = 5;
        }
        $special_price = $_product->getSpecialPrice();
        $min_price = $special_price * (1 - $percent / 100);
        $collection->addAttributeToFilter('special_price', array('gteq' => $min_price));
        $collection->addFieldToFilter('entity_id', array('in' => $includeIds));
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collection->addAttributeToSelect($attributes);
        $collection->addUrlRewrite();
        $collection->getSelect()->limit($limit);
        $this->addRelevance($collection);
        $this->addStockFilter($collection);
        if ($collection->count() > 0) {
            foreach ($collection as $p) {
                $products[] = $p;
            }
        }
        $this->showDebug(__FUNCTION__ . '(' . count($products) . ')', $collection->getSelect());
        return $products;
    }

    /**
     * Get products have price nearest with current product
     * 
     * @param []int $includeIds
     * @return []Mage_Catalog_Model_Product
     */
    public function getProductPriceNearest($includeIds, $limit = 3)
    {
        $products = array();
        $c1 = $this->getProductMaxPriceNearest($includeIds, $limit);
        $c2 = null;
        if ($c1->count() < $limit) {
            $limit_1 = $limit - $c1->count();
            $c2 = $this->getProductMinPriceNearest($includeIds, $limit_1);
        }

        if ($c1->count() > 0) {
            foreach ($c1 as $p1) {
                $products[] = $p1;
            }
        }
        if ($c2 && $c2->count() > 0) {
            foreach ($c2 as $p2) {
                $products[] = $p2;
            }
        }
        return $products;
    }

    /**
     * Get products have price nearest(sort price asc) with current product
     * 
     * @param []int $includeIds
     * @return []Mage_Catalog_Model_Product
     */
    public function getProductMaxPriceNearest($includeIds = array(), $limit = 3)
    {
        $_product = $this->getProduct();
        $special_price = $_product->getSpecialPrice();
        $collection = Mage::getResourceModel('catalog/product_collection');
        if (count($includeIds) > 0) {
            $collection->addFieldToFilter('entity_id', array('in' => $includeIds));
        } else {
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
            $collection->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                    ->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        }
        $collection->addAttributeToSort('special_price', 'ASC')
                ->addAttributeToFilter('special_price', array('gteq' => $special_price));
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collection->addAttributeToSelect($attributes);
        $collection->addUrlRewrite();
        $collection->getSelect()->limit($limit);
        $this->addStockFilter($collection);
        $this->showDebug(__FUNCTION__ . '(' . $collection->count() . ')', $collection->getSelect());
        return $collection;
    }
    
    /**
     * Get products have price nearest(sort price desc) with current product
     * 
     * @param []int $includeIds
     * @return []Mage_Catalog_Model_Product
     */
    public function getProductMinPriceNearest($includeIds = array(), $limit = 3)
    {
        $_product = $this->getProduct();
        $special_price = $_product->getSpecialPrice();
        $collection = Mage::getResourceModel('catalog/product_collection');
        if (count($includeIds) > 0) {
            $collection->addFieldToFilter('entity_id', array('in' => $includeIds));
        } else {
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
            $collection->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                    ->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        }
        $collection->addAttributeToSort('special_price', 'DESC')
                ->addAttributeToFilter('special_price', array('lteq' => $special_price));
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collection->addAttributeToSelect($attributes);
        $collection->addUrlRewrite();
        $collection->getSelect()->limit($limit);
        $this->addStockFilter($collection);
        $this->showDebug(__FUNCTION__ . '(' . $collection->count() . ')', $collection->getSelect());
        return $collection;
    }

    public function getAlsoViewedProduct($limit = 3)
    {
        $products = array();
        //get all ids according meta keyword matching
        $incl_1 = array_diff($this->getProductIdsByMetaKeyword(), $this->getAllProductExcluded());
        $c1 = array();
        if (count($incl_1) > 0) {
            $c1 = $this->getProductCollection($incl_1, $limit, true);
        }
        $c2 = array();
        $ids = array();

        //push product ids to an arrange $ids and $products
        if (count($c1) > 0) {
            foreach ($c1 as $p1) {
                $products[] = $p1;
                $ids[] = $p1->getId();
            }
        }
        if (count($c1) < $limit) { //just when related products is not enough
            $limit_1 = $limit - count($c1);

            //try to find more products matching category excluding products in $ids
            $incl_2 = array_diff($this->getProductIdsByCategoryIds($ids), $this->getAllProductExcluded());
            $c2 = $this->getProductCollection($incl_2, $limit_1);
        }

        //return related products immediately if enough
        if (count($c2) > 0) {
            foreach ($c2 as $p2) {
                $products[] = $p2;
            }
        }
        return $products;
    }

    public function getAllCategoryIds($product = null)
    {
        if (!$product) {
            $product = $this->getProduct();
        }
        $excludeIds = $this->getAllExcludeCategoryIds();
        return array_diff($this->_filterCategoryIds($product->getCategoryIds()), $excludeIds);
    }

    public function _filterCategoryIds(array $ids)
    {
        $filteredCategoryIds = array();
        foreach ($ids as $categoryId) {

            if ($categoryId == Mage::app()->getStore()->getRootCategoryId()) {
                continue;
            }
            $_category = Mage::getModel('catalog/category')->load($categoryId);

            if ($_category->getIsAnchor()) {
                $categoryChildren = $_category->getAllChildren(true);

                $key = array_search($categoryId, $categoryChildren);
                if ($key == false) {
                    unset($categoryChildren[$key]);
                }

                $intersect = array_intersect($ids, $categoryChildren);
                if (count($intersect)) {
                    continue;
                }
            }
            $filteredCategoryIds[] = $categoryId;
        }
        return $filteredCategoryIds;
    }

    public function getWordsByLength($subject, $length = 2)
    {
        $words = array();
        $words_arr = explode(',', $subject);
        foreach ($words_arr as $w) {
            if (str_word_count($w) > $length) {
                $words[] = trim($w);
            }
        }
        return $words;
    }

    public function getTableName($table)
    {
        return $this->getResource()->getTableName($table);
    }

    public function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    public function getConnection()
    {
        return $this->getResource()->getConnection('core_read');
    }

    public function queryIds($sql)
    {
        return $this->getConnection()->fetchCol($sql);
    }

    public function showDebug($message, $sql = '')
    {
        if (isset($_GET['debug_related']) && $_GET['debug_related'] == 'opentechiz') {
            echo $message;
        }
        if (isset($_GET['debug_sqlx']) && $_GET['debug_sqlx'] == 1 && $sql) {
            echo '[sql]' . $sql . '[/sql]';
        }
        if (isset($_GET['debug_related']) && $_GET['debug_related'] == 'opentechiz') {
            echo '=>';
        }
    }

    public function quoteString($subject)
    {
        return $this->getConnection()->quote($subject);
    }

    public function getConfigExcludeProductIds()
    {
        if (Mage::getStoreConfig('relatedproduct/general/exclude_productids')) {
            return preg_split('/[\s,]+/', Mage::getStoreConfig('relatedproduct/general/exclude_productids'));
        }
        return array();
    }

    public function getConfigExcludeCategoryIds()
    {
        return Mage::helper('relatedproduct')->getConfigExcludeCategoryIds();
    }

    public function getAllExcludeCategoryIds()
    {
        if (Mage::registry('exclude_catids') !== null) {
            return Mage::registry('exclude_catids');
        }
        $catids = $this->_filterCategoryIds($this->getConfigExcludeCategoryIds());
        Mage::register('exclude_catids', $catids);
        return $catids;
    }

    public function getAllProductExcluded()
    {
        if (Mage::registry('all_product_excluded') !== null) {
            return Mage::registry('all_product_excluded');
        }
        $categoryIds = $this->getAllExcludeCategoryIds();
        $excludeIds = array();
        if (count($categoryIds) > 0) {
            $sql = 'SELECT DISTINCT(a.product_id)';
            $sql .= ' FROM ' . $this->getTableName('catalog_category_product') . ' AS a';
            $sql .= ' JOIN ' . $this->getTableName('catalog_product_entity_int') . ' AS icpei ON icpei.entity_id = a.product_id';
            $sql .= ' JOIN ' . $this->getTableName('cataloginventory_stock_item') . ' AS stock_items ON a.product_id = stock_items.product_id';
            $sql .= ' WHERE icpei.attribute_id=' . self::ATTRIBUTE_ID_STATUS . ' AND icpei.value=1'; //check status
            $sql .= ' AND a.category_id IN(' . join(',', $categoryIds) . ')';
            $excludeIds = $this->queryIds($sql);
        }

        if (count($excludeIds) > 0) {
            $sqlSkipExcluded = 'SELECT DISTINCT(a.product_id)';
            $sqlSkipExcluded .= ' FROM ' . $this->getTableName('catalog_category_product') . ' AS a';
            $sqlSkipExcluded .= ' WHERE a.product_id IN(' . join(',', $excludeIds) . ') AND a.category_id NOT IN (' . join(',', $categoryIds) . ')';
            $excludeIdsSkip = $this->queryIds($sqlSkipExcluded);
            $excludeIds = array_diff($excludeIds, $excludeIdsSkip);
        }

        if (count($excludeIds) > 0) {
            $productIds = $this->getConfigExcludeProductIds();
            $excludeIds = array_unique(array_merge($excludeIds, $productIds));
        } else {
            $excludeIds = $this->getConfigExcludeProductIds();
        }

        Mage::register('all_product_excluded', $excludeIds);
        return $excludeIds;
    }

    public function getConfigLimitMatching()
    {
        return (int) Mage::getStoreConfig('relatedproduct/general/limit_matching');
    }

    public function repeatWords($words, $length = 2)
    {
        $results = array();
        $indexs = array();
        foreach ($words as $word) {
            $ws = $this->extractWords($word, $length);
            foreach ($ws as $w) {
                $indexs[$w] = !isset($indexs[$w]) ? 1 : $indexs[$w] + 1;
            }
        }
        foreach ($indexs as $k => $v) {
            if ($v > 1 && !in_array($k, $target)) {
                $results[] = $k;
            }
        }
        return $results;
    }

    public function extractWords($s, $length = 2)
    {
        $output = array();
        $a = preg_split('/[\s,]+/', trim($s));
        $len = count($a);
        for ($i = 0; $i < $len - 1; $i++) {
            $temp = array();
            for ($j = 0; $j < $length; $j++) {
                if (!isset($a[$i + $j])) {
                    continue;
                }
                $temp[] = $a[$i + $j];
            }
            $output[] = trim(join(' ', $temp));
        }
        return $output;
    }

}
