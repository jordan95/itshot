<?php

class OpenTechiz_RelatedProduct_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getConfigExcludeCategoryIds()
    {
        if (Mage::getStoreConfig('relatedproduct/general/exclude_catids')) {
            return preg_split('/[\s,]+/', Mage::getStoreConfig('relatedproduct/general/exclude_catids'));
        }
        return array();
    }

}
