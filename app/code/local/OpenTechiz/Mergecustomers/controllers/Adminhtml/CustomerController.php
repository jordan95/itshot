<?php
/**
 * @author Jordan Tran
 * @package OpenTechiz_Mergecustomers
 */


class OpenTechiz_Mergecustomers_Adminhtml_CustomerController extends Mage_Adminhtml_Controller_Action
{
    const ENABLE_STATUS = 1;
    const DISABLE_STATUS = 2;
    public function mergeCustomersAction()
    {
        $customersIds = $this->getRequest()->getParam('customer');


        if (count($customersIds) < 2) {
            Mage::getSingleton('adminhtml/session')
                ->addError(Mage::helper('mergecustomers')->__('Please select minimum 2 customers.'));
        }elseif(count($customersIds) > 2){
            Mage::getSingleton('adminhtml/session')
                ->addError(Mage::helper('mergecustomers')->__('Please select maximum 2 customers.'));
        } else {
            try {
                $mainCustomerId = (int)$this->getRequest()->getParam('main_customer');
                $mainCustomer = Mage::getModel('customer/customer')->load($mainCustomerId);

                foreach ($customersIds as $customerId) {
                    if ($customerId == $mainCustomerId) {
                        continue;
                    }

                    $this->mergeSalesOrderInfo($customerId, $mainCustomer)
                         ->mergeAddresses($customerId, $mainCustomer)
                         ->mergePoints($customerId, $mainCustomer)
                         ->mergeCredit($customerId, $mainCustomer)
                         ->disableCustomer($customerId, $mainCustomer)
                         ->mergeCustomerNote($customerId, $mainCustomer)
                         ->mergeProductReviews($customerId, $mainCustomer)
                         ->mergeNewsletter($customerId, $mainCustomer)
                         ->mergeAffiliate($customerId, $mainCustomer);
                         
                                   
                }
            } catch (Exception $e) {
                Mage::log($e->getMessage(), null, 'mergecustomers.log', true);
                Mage::getSingleton('adminhtml/session')
                    ->addError(Mage::helper('mergecustomers')->__('Error occurred, view log file.'));
            }

            Mage::getSingleton('adminhtml/session')
                ->addSuccess(
                    count($customersIds) - 1 . Mage::helper('mergecustomers')->__(' customers have been merged.')
                );
        }

        $this->_redirect('*/*/index');
    }

    /**
     * @param $customerId
     * @param $mainCustomer
     * @return $this
     */
    protected function mergeSalesOrderInfo($customerId, $mainCustomer)
    {
        $ordersCustomer = Mage::getModel('sales/order')
            ->getCollection()
            ->addFieldToFilter('customer_id', $customerId);

        foreach ($ordersCustomer as $order) {
            $order->addData(array(
                'customer_id' => $mainCustomer->getId(),
                'customer_firstname' => $mainCustomer->getFirstname(),
                'customer_lastname' => $mainCustomer->getLastname(),
                'customer_email' => $mainCustomer->getEmail()
            ));
        }

        $ordersCustomer->save();

        return $this;
    }

    /**
     * @param $customerId
     * @param $mainCustomer
     * @return $this
     */
    protected function mergeAddresses($customerId, $mainCustomer)
    {
        $addresses = Mage::getModel('customer/address')
            ->getCollection()
            ->addFieldToFilter('parent_id', $customerId);

        foreach ($addresses as $address) {
            $address->setParentId($mainCustomer->getId());
        }

        $addresses->save();

        return $this;
    }
    protected function mergePoints($customerId, $mainCustomer)
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $readConnection = $resource->getConnection('core_read');

        $process_reindex_id = OpenTechiz_Mergecustomers_Helper_Data::NUMBER_REINDEX_REWARD_TRANSFER;
        $table_reward_transfer =  $resource->getTableName('rewards/transfer');
        
        $data_reward = $readConnection->fetchCol('SELECT rewards_transfer_id FROM ' . $table_reward_transfer.' WHERE customer_id ='.$customerId );

        if(!empty($data_reward)){
            $query = "UPDATE {$table_reward_transfer} SET customer_id = '{$mainCustomer->getId()}' WHERE customer_id = ". (int)$customerId;
            $writeConnection->query($query);

            $process = Mage::getModel('index/process')->load($process_reindex_id);
            $process->reindexAll();
        }
    
        return $this;
    }
    protected function mergeCredit($customerId, $mainCustomer)
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $readConnection = $resource->getConnection('core_read');

        $table_credit_balance =  $resource->getTableName('credit/balance');
        $table_credit_transaction =  $resource->getTableName('credit/transaction');
        
        $credit_unessential_customer = $readConnection->fetchAll('SELECT amount,balance_id FROM ' . $table_credit_balance.' WHERE customer_id ='.$customerId );
        $credit_main_customer = $readConnection->fetchAll('SELECT amount,balance_id FROM ' . $table_credit_balance.' WHERE customer_id ='.$mainCustomer->getId() );
        if(!empty($credit_unessential_customer[0]) && $credit_unessential_customer[0]['amount'] != "0"){
            if(!empty($credit_main_customer[0])){
                $amount_unessential_customer = (double)$credit_unessential_customer[0]['amount'];
                $amount_credit_merge = (double)$credit_main_customer[0]['amount'] + $amount_unessential_customer;
                $message_main_customer = "move credit amount from customer id: {$customerId}";
                $message_unessential_customer = "move credit amount to customer id: {$mainCustomer->getId()}";
                //update credit amount for main customer
                Mage::helper("mergecustomers")->updateCreditBalance($writeConnection,$table_credit_balance,$amount_credit_merge,$mainCustomer->getId());
                //update credit amount for unessential customer
                Mage::helper("mergecustomers")->updateCreditBalance($writeConnection,$table_credit_balance,0,$customerId);
                //add transaction  for main customer
                Mage::helper("mergecustomers")->addCreditTransaction($writeConnection,$table_credit_transaction,$amount_credit_merge,$amount_unessential_customer,$credit_main_customer[0]['balance_id'],$customerId,$message_main_customer);
                 //add transaction  for unessential customer
                Mage::helper("mergecustomers")->addCreditTransaction($writeConnection,$table_credit_transaction,0,-$amount_unessential_customer,$credit_unessential_customer[0]['balance_id'],$customerId,$message_unessential_customer);
                   
            }
           
        }
    
        return $this;
    }
    protected function disableCustomer($customerId, $mainCustomer)
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $readConnection = $resource->getConnection('core_read');
        $table_customer_entity =  $resource->getTableName('customer/entity');
        if($customerId !=""){
            $query = "UPDATE {$table_customer_entity} SET is_active = 0 WHERE entity_id = ". (int)$customerId;
            $writeConnection->query($query);
        }
        return $this;
    }
    protected function mergeCustomerNote($customerId, $mainCustomer)
    {
        $customerNote = Mage::getModel('opentechiz_customernotes/note')->getCollection()->addFieldToFilter('customer_id',array('eq'=>$customerId));
        if( $customerNote->count() > 0){
            foreach($customerNote as $note){
                $note->setCustomerId($mainCustomer->getId());
            } 

            $customerNote->save();
        }
        

        return $this;
    }
    protected function mergeProductReviews($customerId, $mainCustomer)
    {
        $reviews = Mage::getModel('mergecustomers/detail')
            ->getCollection()
            ->addFieldToFilter('customer_id', $customerId);
        if($reviews->count() > 0){
            foreach ($reviews as $review) {
                $review->addData(array(
                    'customer_id' => $mainCustomer->getId(),
                    'nickname' => $mainCustomer->getFirstname()
                ));
            }

            $reviews->save();
        }
        

        return $this;
    }
    protected function mergeNewsletter($customerId, $mainCustomer)
    {
        $newsletter_mainCustomer = Mage::getModel('newsletter/subscriber')
                    ->getCollection()
                    ->addFieldToFilter('customer_id', $mainCustomer->getId());
        $newsletter = Mage::getModel('newsletter/subscriber')
                    ->getCollection()
                    ->addFieldToFilter('customer_id', $customerId);
        if($newsletter_mainCustomer->count() <= 0){

            if($newsletter->count() > 0){
                foreach ($newsletter as $news) {
                    $news->addData(array(
                        'customer_id' => $mainCustomer->getId(),
                        'subscriber_email' => $mainCustomer->getEmail()
                    ));
                }
                $newsletter->save();
            }
        }else{
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $readConnection = $resource->getConnection('core_read');
            $table_newsletter_link = $resource->getTableName('newsletter/queue_link');
            if($newsletter->count() > 0){
                foreach ($newsletter as $news) {
                    $extra_subscriber_id = $news->getSubscriberId();
                    break;
                }
                foreach ($newsletter_mainCustomer as $data) {
                    $main_subscriber_id = $data->getSubscriberId();
                    break;
                }
                $query_newletter_link = "SELECT queue_id FROM {$table_newsletter_link} WHERE subscriber_id = " .(int)$main_subscriber_id;
                $results = $readConnection->fetchAll($query_newletter_link);
                $list_queue_id =[];
                if(!empty($results)){
                    foreach($results as $data){
                            $list_queue_id[] = $data['queue_id'];
                    }
                    $list_id = implode(',',$list_queue_id);
                    $query = "UPDATE {$table_newsletter_link} SET subscriber_id = {$main_subscriber_id} WHERE subscriber_id = ". (int)$extra_subscriber_id." AND queue_id NOT IN(".$list_id.")";
                    $writeConnection->query($query);
                }
            }
            
            
        }

        

        return $this;
    }
    protected function mergeAffiliate($customerId, $mainCustomer)
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $readConnection = $resource->getConnection('core_read');
        $isTableExist = $readConnection->isTableExists('tsht_mw_affiliate_customers');
        if($isTableExist){
            $table_credit_customer =  $resource->getTableName('mwcredit/creditcustomer');
            $table_affiliate_customer =  $resource->getTableName('affiliate/affiliatecustomers');
            $extra_affiliate_customer = Mage::getModel('affiliate/affiliatecustomers')->load($customerId);
            $main_affiliate_customer  = Mage::getModel('affiliate/affiliatecustomers')->load($mainCustomer->getId());
            $affiliate_group_customer  = Mage::getModel('affiliate/affiliategroupmember')->getCollection()->addFieldToFilter("customer_id",array('eq'=>array($customerId)));
            $affiliate_history_customer  = Mage::getModel('affiliate/affiliatehistory')->getCollection()->addFieldToFilter("customer_id",array('eq'=>array($customerId)));
            $affiliate_invitation_customer  = Mage::getModel('affiliate/affiliateinvitation')->getCollection()->addFieldToFilter("customer_id",array('eq'=>array($customerId)));
            $affiliate_transaction_customer  = Mage::getModel('affiliate/affiliatetransaction')->getCollection()->addFieldToFilter("customer_invited",array('eq'=>array($customerId)));
            $affiliate_credit_customer_history  = Mage::getModel('mwcredit/credithistory')->getCollection()->addFieldToFilter("customer_id",array('eq'=>array($customerId)));
            $affiliate_extra_credit_customer  = Mage::getModel('mwcredit/creditcustomer')->load($customerId);
            $affiliate_main_credit_customer  = Mage::getModel('mwcredit/creditcustomer')->load($mainCustomer->getId());
            if($extra_affiliate_customer->getCustomerId()){
                if(!$main_affiliate_customer->getCustomerId()){
                    $query = "UPDATE {$table_affiliate_customer} SET customer_id = {$mainCustomer->getId()} WHERE customer_id = ". (int)$customerId;
                    $writeConnection->query($query);
                    $extra_affiliate_customer->setCustomerId($mainCustomer->getId());
                    $extra_affiliate_customer->save();
                    if($affiliate_group_customer->count() > 0){
                        foreach($affiliate_group_customer as $group_customer){
                            $group_customer->setCustomerId($mainCustomer->getId()); 
                        }
                        $affiliate_group_customer->save();
                    }

                }else if($main_affiliate_customer->getCustomerId()){
                    $extra_affiliate_customer->setStatus(self::DISABLE_STATUS);
                    $extra_affiliate_customer->save();
                }
                
                if($affiliate_history_customer->count() > 0){
                    foreach($affiliate_history_customer as $history_customer){
                        $history_customer->setCustomerId($mainCustomer->getId());
                    }
                    $affiliate_history_customer->save();
                }

                if($affiliate_invitation_customer->count() > 0){
                    foreach($affiliate_invitation_customer as $invitation_customer){
                        $invitation_customer->setCustomerId($mainCustomer->getId());
                    }
                    $affiliate_invitation_customer->save();
                }
                if($affiliate_transaction_customer->count() > 0){
                    foreach($affiliate_transaction_customer as $transaction_customer){
                        $transaction_customer->setCustomerId($mainCustomer->getId());
                        $transaction_customer->setCustomerInvited($mainCustomer->getId());
                    }
                    $affiliate_transaction_customer->save();
                }
                if($affiliate_credit_customer_history->count() > 0){
                    foreach($affiliate_credit_customer_history as $credit_customer_history){
                        $credit_customer_history->setCustomerId($mainCustomer->getId());
                    }
                    $affiliate_credit_customer_history->save();
                }
                if($affiliate_extra_credit_customer->getCustomerId()){
                    if(!$affiliate_main_credit_customer->getCustomerId()){
                        $query = "UPDATE {$table_credit_customer} SET customer_id = {$mainCustomer->getId()} WHERE customer_id = ". (int)$customerId;
                        $writeConnection->query($query);
                    }else if($affiliate_main_credit_customer->getCustomerId()){
                        $credit = (float)$affiliate_extra_credit_customer->getCredit() + (float)$affiliate_main_credit_customer->getCredit();
                        $affiliate_main_credit_customer->setCredit($credit);
                        $affiliate_main_credit_customer->save();
                        $affiliate_extra_credit_customer->delete();
                    }


                }
            }
        }
        
       
        
        return $this;
    }
    

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('customer/manage/mergecustomers');
    }
}
