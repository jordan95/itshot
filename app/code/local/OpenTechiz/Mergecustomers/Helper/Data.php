<?php
/**
 * @author Jordan Tran
 * @package OpenTechiz_Mergecustomers
 */


class OpenTechiz_Mergecustomers_Helper_Data extends Mage_Core_Helper_Abstract
{
	const NUMBER_REINDEX_REWARD_TRANSFER  = 12;
	public function updateCreditBalance($writeConnection,$table_credit_balance,$amount,$customer_id){

        $query = "UPDATE {$table_credit_balance} SET amount = '{$amount}' WHERE customer_id = ". (int)$customer_id;
        $writeConnection->query($query);
    }
    public function addCreditTransaction($writeConnection,$table_credit_transaction,$balance_amount,$balance_delta,$balance_id,$customer_id,$message){

    	$user_id = Mage::helper("custom")->getUserId();
    	$created_at =$updated_at  = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $query = "INSERT INTO  {$table_credit_transaction}(balance_id,balance_amount,balance_delta,action,message,is_notified,created_at,updated_at,user_id) VALUES ({$balance_id}, {$balance_amount}, {$balance_delta},\"manual\",\"{$message}\",0,\"{$created_at}\",\"{$updated_at}\",{$user_id})";
        $writeConnection->query($query);
    }
}
