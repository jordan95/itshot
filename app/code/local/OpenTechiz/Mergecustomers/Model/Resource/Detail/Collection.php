<?php
/**
 * @author jordan
 * @package OpenTechiz_Mergecustomers
 */


class OpenTechiz_Mergecustomers_Model_Resource_Detail_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('mergecustomers/detail');
    }
}
