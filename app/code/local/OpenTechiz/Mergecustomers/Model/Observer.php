<?php
/**
 * @author Jordan Tran
 * @package OpenTechiz_Mergecustomers
 */



class OpenTechiz_Mergecustomers_Model_Observer
{
    /**
     * @param $observer
     */
    public function onPrepareMassactionBefore($observer)
    {
        $block = $observer->getEvent()->getBlock();
        
        if ($block instanceof Mage_Adminhtml_Block_Customer_Grid
            && Mage::getSingleton('admin/session')->isAllowed('customer/manage/mergecustomers')
        ) {
            $this->setMergeCustomersMenuInGrid($block);
        }
    }

    /**
     * @param $block
     */
    protected function setMergeCustomersMenuInGrid($block)
    {
        $block->getMassactionBlock()->addItem(
            'merge_customers',
            array(
                'label' => Mage::helper('mergecustomers')->__('Merge Customers'),
                'url' => $block->getUrl('*/*/mergeCustomers'),
                'confirm' => Mage::helper('mergecustomers')->__(
                    'Are you sure want to merge customer?'
                ),
                'additional'   => array(
                    'visibility'    => array(
                        'name'     => 'main_customer',
                        'type'     => 'select',
                        'class'    => 'required-entry',
                        'label'    => Mage::helper('mergecustomers')->__('Choose Main Account'),
                        'values'   => '',
                        'customerId' => ''
                    )
                )
            )
        );
    }
    public function customerAuth($observer)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $observer->getData('model');
        $isActive = $customer->getData('is_active');
        if (!$isActive) {
            throw Mage::throwException(Mage::helper('mergecustomers')->__('This account is not active'));
        }
    }
}
