<?php
/**
 * @author jordan
 * @package OpenTechiz_Mergecustomers
 */


class OpenTechiz_Mergecustomers_Model_Detail extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('mergecustomers/detail');
    }
}
