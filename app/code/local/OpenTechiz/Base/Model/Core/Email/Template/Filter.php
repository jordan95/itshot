<?php

if (Mage::helper('core')->isModuleEnabled('TBT_Rewards')) {

    class OpenTechiz_Base_Model_Core_Email_Template_Filter_Pure extends TBT_Rewards_Model_Core_Email_Template_Filter
    {
        
    }

} else {

    class OpenTechiz_Base_Model_Core_Email_Template_Filter_Pure extends Mage_Core_Model_Email_Template_Filter
    {
        
    }

}

class OpenTechiz_Base_Model_Core_Email_Template_Filter extends OpenTechiz_Base_Model_Core_Email_Template_Filter_Pure
{

    public function dateDirective($construction)
    {
        $params = $this->_getIncludeParameters($construction[2]);
        $format = isset($params['format']) ? $params['format'] : 'Y-m-d H:i:s';
        return Mage::getModel('core/date')->date($format);
    }

}
