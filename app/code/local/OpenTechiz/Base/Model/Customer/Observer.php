<?php

class OpenTechiz_Base_Model_Customer_Observer
{

    public function stripScriptInFieldAddress(Varien_Event_Observer $observer)
    {
        $customer_address = $observer->getCustomerAddress();
        $data = $customer_address->getData();
	array_walk_recursive( $data, function(&$value, $key) { if(is_string($value)){ $value = strip_tags($value);} }); 
        $customer_address->setData($data);
    }

    public function stripScriptInFieldCustomer(Varien_Event_Observer $observer)
    {
        $customer = $observer->getCustomer();
        $data = $customer->getData();
	array_walk_recursive( $data, function(&$value, $key) { if(is_string($value)){ $value = strip_tags($value);} }); 
        $customer->setData($data);
    }

    public function stripScriptInFieldOrderCustomer(Varien_Event_Observer $observer)
    {
        $order = $observer->getOrder();
        $order->setCustomerName(strip_tags($observer->getCustomerName()));
        $shipping_address = $order->getShippingAddress();
        if ($shipping_address instanceof Mage_Sales_Model_Order_Address) {
            $data = $shipping_address->getData();
            array_walk_recursive( $data, function(&$value, $key) { if(is_string($value)){ $value = strip_tags($value);} }); 
            $shipping_address->setData($data);
        }
        
        $billing_address = $order->getBillingAddress();
        if ($billing_address instanceof Mage_Sales_Model_Order_Address) {
            $data = $billing_address->getData();       
            array_walk_recursive( $data, function(&$value, $key) { if(is_string($value)){ $value = strip_tags($value);} }); 
            $billing_address->setData($data);
        }
    }

}
