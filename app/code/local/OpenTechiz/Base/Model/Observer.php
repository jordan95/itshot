<?php

class OpenTechiz_Base_Model_Observer
{

    public function redirectPrefixJewelryWatches($observer)
    {
        $matches = array();
        if (preg_match('/^\/(jewelry|watches)\/(.+)$/i', $_SERVER['REQUEST_URI'], $matches) === 1) {
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: /{$matches[2]}");
            exit();
        }
    }

    public function singleResultRedirect($observer)
    {
        $actionName = Mage::app()->getFrontController()->getAction()->getFullActionName();
        if ($actionName === 'catalogsearch_result_index') {
            $collection = $observer->getCollection();
            if (Mage::getSingleton('searchsphinx/config')->isSingleResultRedictEnabled() && $collection->count() == 1) {
                $product = $collection->getFirstItem();
                header('Location: ' . $product->getProductUrl());
                exit;
            }
        }
    }
}
