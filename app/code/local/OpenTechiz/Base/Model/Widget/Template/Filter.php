<?php

class OpenTechiz_Base_Model_Widget_Template_Filter extends Mage_Widget_Model_Template_Filter
{

    public function dateDirective($construction)
    {
        $params = $this->_getIncludeParameters($construction[2]);
        $format = isset($params['format']) ? $params['format'] : 'Y-m-d H:i:s';
        return Mage::getModel('core/date')->date($format);
    }

}
