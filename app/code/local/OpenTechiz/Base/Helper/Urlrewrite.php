<?php

class OpenTechiz_Base_Helper_Urlrewrite extends Medialounge_Catalog_Helper_Urlrewrite
{

    protected function createUrlPermanentRedirectToFirstCategory($store, $product, $firstcategory)
    {
        $targetPath = $this->getTargetPath($product, $firstcategory);
        $urlRewrite = $this->getCoreUrlRewriteModel();
        $request_path = $product->getData('request_path');
        if (preg_match('/^(jewelry|watches)\/(.+)$/i', $request_path, $matches) === 1) {
            $request_path = $matches[2];
        }
        $urlRewrite->setData('store_id', $store->getId());
        $urlRewrite->setData('id_path', sprintf('product/%s', $product->getId()));
        $urlRewrite->setData('request_path', $request_path);
        $urlRewrite->setData('target_path', $targetPath);
        $urlRewrite->setData('is_system', 0);
        $urlRewrite->setData('options', 'RP');
        $urlRewrite->setData('description', sprintf('disabled sku: %s to now redirect to its first category: %s', $product->getSku(), $firstcategory->getName()));
        $urlRewrite->setData('product_id', $product->getId());
        $urlRewrite->save();
        return $urlRewrite;
    }

    public function doRedirects1($requestUrl)
    {
        
        if (file_exists(Mage::getBaseDir() . '/optimiseweb/redirects/default/ItsHot-Redirects_Denis.csv')) {
            $redirectLines = file(Mage::getBaseDir() . '/optimiseweb/redirects/default/ItsHot-Redirects_Denis.csv');
            foreach ($redirectLines AS $redirectLine) {
                $sourceDestination = explode(Mage::getStoreConfig('optimisewebredirects/redirects1/delimiter'), $redirectLine);
                if (count($sourceDestination) == 3) {
                    $sourceUrl = rtrim(trim($sourceDestination[0]), '/');
                    $destinationUrl = trim($sourceDestination[1]);
                    $redirectCode = (int) trim($sourceDestination[2]);
                    $doRedirect = FALSE;
                    if ($sourceUrl == $requestUrl) {
                        $doRedirect = TRUE;
                    } elseif (strpos($sourceUrl, Mage::getStoreConfig('optimisewebredirects/redirects1/wildcardcharacter'))) {
                        $sourceUrl = str_replace(Mage::getStoreConfig('optimisewebredirects/redirects1/wildcardcharacter'), '', $sourceUrl);
                        if (strpos($requestUrl, $sourceUrl) === 0) {
                            $doRedirect = TRUE;
                        }
                    }
                    if ($doRedirect) {
                        return $destinationUrl;
                    }
                    continue;
                }
            }
        }
        return false;
    }

    protected function getTargetPath($product, $category)
    {
        $targetPath = null;
        if (Mage::helper('core')->isModuleEnabled('Optimiseweb_Redirects') && (bool) Mage::getStoreConfig('optimisewebredirects/disabled_products/enabled')) {
            $requestUrl = rtrim(Mage::getUrl() . $product->getUrlPath(), '/');
            $requestUrl = str_replace('/index.php', '', $requestUrl);
            $targetPath = $this->doRedirects1($requestUrl);
        } 
        if(!$targetPath){
            $targetPath = $this->getCategoryUrl($category);
        }
        return $targetPath;
    }

}
