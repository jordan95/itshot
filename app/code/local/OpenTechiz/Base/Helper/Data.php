<?php

/**
 * OnlineBiz Software Solution
 * 
 * @project itshot.com
 * @version 0.0.1
 * @encoding UTF-8
 * @author Joe Vu<joe@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2017 , OnlineBiz Software Solution
 * 
 * Create at: Aug 27, 2018 10:02:32 AM
 */
class OpenTechiz_Base_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function generateCouponCode()
    {
        $couponCode = strtoupper(substr(md5(rand(0, 1000000)), 0, 6));
        $coupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');
        if ($coupon->getId()) {
            return $this->generateCouponCode();
        }
        return $couponCode;
    }

    public function createCouponCode($rulename, $amount, $action_type = 'by_percent')
    {
        $couponCode = $this->generateCouponCode();
        $fromdate = date('Y-m-d h:i:s');
        $fromexpire = date('Y-m-d h:i:s', strtotime('2 days'));
        
        $coupon = Mage::getModel('salesrule/rule');
        $coupon->setName($rulename)
                ->setDescription($rulename)
                ->setFromDate($fromdate)
                ->setCouponCode($couponCode)
                ->setToDate($fromexpire)
                ->setCouponType(2)
                ->setUsesPerCoupon('')
                ->setUsesPerCustomer('')
                ->setCustomerGroupIds(array(0, 1, 2, 3))//an array of customer grou pids
                ->setIsActive(1)
                ->setActionsSerialized('a:6:{s:4:"type";s:40:"salesrule/rule_condition_product_combine";s:9:"attribute";N;s:8:"operator";N;s:5:"value";s:1:"1";s:18:"is_value_processed";N;s:10:"aggregator";s:3:"all";}')
                ->setStopRulesProcessing(0)
                ->setIsAdvanced(1)
                ->setProductIds('')
                ->setSortOrder(0)
                ->setSimpleAction($action_type)
                ->setDiscountAmount($amount)
                ->setDiscountQty(null)
                ->setDiscountStep('0')
                ->setSimpleFreeShipping(0)
                ->setApplyToShipping(0)
                ->setIsRss(0)
                ->setWebsiteIds(array(1));
        
        $conditions = array();
        $conditions[1] = array(
            'type' => 'salesrule/rule_condition_combine',
            'aggregator' => 'all',
            'value' => 1,
            'new_child' => ''
        );
        
        $conditions['1--1'] = array(
            'type' => 'salesrule/rule_condition_address',
            'attribute' => 'base_subtotal',
            'operator' => '>',
            'value' => 1,
        );

        $coupon->setData('conditions', $conditions);
        $coupon->loadPost($coupon->getData());
        try {
            $coupon->save();
            return $couponCode;
        } catch (Exception $e) {
            return false;
        }
    }

    public function formatCamelPrice($price)
    {
        if (Mage::registry('current_product')) {
            return Mage::helper('core')->formatPrice($price, false);
        }
        $symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
        $fraction = $price * 100 % 100;
        if ($fraction < 10) {
            $fraction = sprintf('0%s', $fraction);
        }
        $price = number_format($price);
        return "$symbol$price<sup>$fraction</sup>";
    }

    public function feedExportSize($entity_id)
    {
        $product = Mage::getModel('catalog/product')->load($entity_id);
        $output = array();
        foreach ($product->getOptions() as $_option) {
            if (!preg_match('/size/i', $_option->getTitle())) {
                continue;
            }
            $values = $_option->getValues();
            foreach ($values as $v) {
                $output[] = $v->getTitle();
            }
        }
        return join('/', $output);
    }

    public function formatCCnumber($ccNumber)
    {
        $pattern = '/^(\d{4})(\d+)(\d{4})$/i';
        $replacement = '$1********$3';
        return preg_replace($pattern, $replacement, $ccNumber);
    }

}
