<?php

class MW_Mwcredit_Model_Mysql4_Creditcustomer extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        $this->_init('mwcredit/creditcustomer', 'customer_id');
    }
}