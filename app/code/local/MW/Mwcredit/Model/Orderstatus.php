<?php
class MW_Mwcredit_Model_Orderstatus extends Varien_Object
{
	const PENDING				= 1;		//haven't change points yet
    const COMPLETE				= 2;
    const CANCELED			    = 3;
    const CLOSED				= 4;
    const HOLDING				= 5;

    static public function getOptionArray()
    {
        return array(
            self::PENDING    	=> Mage::helper('mwcredit')->__('Pending'),
            self::COMPLETE	    => Mage::helper('mwcredit')->__('Complete'),
            self::CANCELED  	=> Mage::helper('mwcredit')->__('Canceled'),
            self::CLOSED  		=> Mage::helper('mwcredit')->__('Closed'),
        	self::HOLDING		=> Mage::helper('mwcredit')->__('Holding')
        );
    }
    
    static public function getLabel($type)
    {
    	$options = self::getOptionArray();
    	return $options[$type];
    }
}