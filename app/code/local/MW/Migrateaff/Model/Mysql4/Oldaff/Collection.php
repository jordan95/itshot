<?php

class MW_Migrateaff_Model_Mysql4_Oldaff_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('migrateaff/oldaff');
    }

    public function getAllAffWhichNotSyncCollection(){
        $collection = $this;
//        $collection->getSelect()
//            ->joinLeft(array('custmer_has' => Mage::getSingleton('core/resource')->getTableName('aff_customer')),
//                'custmer_has.aff_id = main_table.AffiliateID',
//                array('customer_id' => 'IFNULL(custmer_has.customer_id, 0)')
//            )
//            ->where('custmer_has.customer_id IS NULL')
//        ;


        $listIdsnotSync = $this->getAllIdsOfAffWhichNotSync();
        Mage::getModel('admin/session')->setData('session_list_aff_ids_not_sync', $listIdsnotSync);
        if(empty($listIdsnotSync)){
            $listIdsnotSync[] = 0;
        }

        $collection->getSelect()->where('main_table.AffiliateID IN(?)', $listIdsnotSync);
        return $collection;
    }


    public function getAllIdsOfAffWhichNotSync(){
        $cr = Mage::getSingleton('core/resource');
        $connection = $cr->getConnection('core_write');
        $select = $connection->select()
            ->from(
                array('main_table' => $this->getTable('migrateaff/oldaff')),
                array('AffiliateID')
            )
        ;
        $select->joinLeft(array('customer_has' => Mage::getSingleton('core/resource')->getTableName('aff_customer')),
            'customer_has.aff_id = main_table.AffiliateID',
            array('customer_id' => 'IFNULL(customer_has.customer_id, 0)')
        );
        $queryAdapter = Mage::getResourceModel('migrateaff/affcustomer');

        $temTable  = Mage::getSingleton('core/resource')->getTableName('old_aff_have_not_sync_yet_temp');
        $queryAdapter->dropTable($temTable, true);
        $queryAdapter->createTable($temTable, $select, true);

        $query = $connection->select()
            ->from(array('main_table' => $temTable),
                array('AffiliateID')
            )->where('customer_id = 0')
        ;
        $listIds =  $connection->fetchCol($query);
        return $listIds;
    }

    public function getprepareCollectionToSync($ids){
        $Collection = $this;
        $Collection->addFieldToFilter('main_table.AffiliateID',array('in'=>$ids))
            ->setOrder('AffiliateID','DESC');
        return $Collection;
    }

    public function getAllOldGroup(){
        $cr = Mage::getSingleton('core/resource');
        $connection = $cr->getConnection('core_write');
        $select = $connection->select()
            ->from(
                array('main_table' => Mage::getSingleton('core/resource')->getTableName('tblaff_AffiliateTypes')),
                array( "group_id" => 'main_table.AffiliateTypeID',
                       "group_name" => 'main_table.Description')
            )
        ;
        $listGroup = $connection->fetchAll($select);
        return $listGroup;
    }

    public function getAllTransactionOfAffiliate($affId){
        $cr = Mage::getSingleton('core/resource');
        $connection = $cr->getConnection('core_write');
        $select = $connection->select()
            ->from(
                array('main_table' => Mage::getSingleton('core/resource')->getTableName('tblaff_Sales_Commissions')),
                array( "ID", "SalesId", "AffiliateId", "Commission", "Paid" )
            )
        ;
        $select->joinLeft(array('sales_temp' => Mage::getSingleton('core/resource')->getTableName('tblaff_Sales')),
            'main_table.SalesId = sales_temp.SalesId',
            array(
                'CUSTOMER_ID' => 'sales_temp.CUSTOMER_ID',
                'ORDER_DATE' => 'sales_temp.ORDER_DATE',
                'GRAND_TOTAL' => 'sales_temp.GRAND_TOTAL',
                'IPAddress' => 'sales_temp.IPAddress',
                'Valid' => 'sales_temp.Valid',
                'AffiliateSaleAccepted' => 'sales_temp.AffiliateSaleAccepted',
                'Referrer' => 'sales_temp.Referrer',
            )
        );
        $select->where('main_table.AffiliateId = ?', $affId);
        $listGroup = $connection->fetchAll($select);
        return $listGroup;
    }

    public function getAllIntivationOfAffiliate($affId){
        $cr = Mage::getSingleton('core/resource');
        $connection = $cr->getConnection('core_write');
        $select = $connection->select()
            ->from(
                array('main_table' => Mage::getSingleton('core/resource')->getTableName('tblaff_ClicksGeneric')),
                array( "ID", "AffiliateId", "ClickIP", "ClickDateTime", "Referrer" )
            )
        ;
        $select->where('main_table.AffiliateId = ?', $affId);
        $listIntivation = $connection->fetchAll($select);
        return $listIntivation;
    }
}