<?php

class MW_Migrateaff_Model_Mysql4_Affcustomer_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('migrateaff/affcustomer');
    }

    public function loadByAffId($affId){
        return $this->addFieldToFilter('aff_id', $affId)->getFirstItem();
    }

    public function loadByCustomerId($customerId){
        return $this->addFieldToFilter('customer_id', $customerId)->getFirstItem();
    }
}