<?php

class MW_Migrateaff_Model_Mysql4_Affrecorded_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('migrateaff/affrecorded');
    }
}