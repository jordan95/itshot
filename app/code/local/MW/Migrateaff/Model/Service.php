<?php

class MW_Migrateaff_Model_Service
{

    public static function syncService()
    {
        return self::getService('migrateaff/service_syncService');
    }

    public static function affService(){
        return self::getService('migrateaff/service_affService');
    }

    /**
     * get service model
     *
     * @param string $servicePath
     * @throws Exception
     */
    public static function getService( $servicePath )
    {
        $service = Mage::getSingleton($servicePath);
        if ( $service == false ) {
            throw new Exception('There is no available service: ' . $servicePath);
        }
        return $service;
    }

}