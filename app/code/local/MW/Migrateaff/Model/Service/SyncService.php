<?php


class MW_Migrateaff_Model_Service_SyncService
    extends MW_Migrateaff_Model_Service_Modify_Grid
{
    /**
     * Return collection with aff information which not sync
     * @return MW_Migrateaff_Model_Mysql4_Oldaff_Collection
     */
    public function getAllAffiliates()
    {
        return Mage::getResourceModel('migrateaff/oldaff_collection')->getAllAffWhichNotSyncCollection();
    }

    /**
     * @param $grid
     * @param $type
     */
    public function modifiColumns($grid,$type){
        return $this->Columns($grid,$type);
    }
}