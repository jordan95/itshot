<?php

class MW_Migrateaff_Model_Service_AffService
{
    const SIZE = 5;

    protected $_REMAIN_ID ;
    protected $_CUSTOMER_IDS = array();

    public function syncGroupFirst(){

        $allOldGroups = Mage::getResourceModel('migrateaff/oldaff_collection')->getAllOldGroup();
        $collectionFilter = Mage::getModel('affiliate/affiliategroup')->getCollection();
        if($collectionFilter->getSize() < count($allOldGroups)){

            foreach ($allOldGroups as $group){
                $newgroup = array(
                    "group_name" => $group['group_name'],
                    "limit_day" => 0,
                    "limit_order" => 0,
                    "limit_commission" => 0
                );
                $groupxx = Mage::getModel('affiliate/affiliategroup')->getCollection()
                    ->addFieldToFilter('group_id', $group['group_id']);
                if($groupxx->getData()){
                    $newgroup['group_id'] = $group['group_id'];
                }else{
                    /*NOT DO ANYTHING */
                }
                Mage::getModel('affiliate/affiliategroup')->setData($newgroup)->save();
            }
        }
    }

    /**
     * @param $product
     * @return mixed
     */
    public function syncAff($product){
        $this->syncGroupFirst();
        $product = Mage::helper('migrateaff')->base64Decode($product);
        $product = explode(',',$product);
        if (sizeof($product) > self::SIZE){
            $ids_need_correct = array_slice($product, 0, self::SIZE, true);
            $remain_ids = array_diff($product,$ids_need_correct);
            $this->_REMAIN_ID = $remain_ids;
            return $this->correctbyId($ids_need_correct);
        }else{
            return $this->correctbyId($product);
        }
    }

    public function correctbyId($ids){
        $Collection = Mage::getResourceModel('migrateaff/oldaff_collection')->getprepareCollectionToSync($ids);
        $result = $this->_prepareData($Collection);

        if($result){
            $products = $this->_REMAIN_ID;
            $remain_size = sizeof($products);
            $products = ($products) ? implode(',',$products) : 0;
            $products = Mage::helper('migrateaff')->base64Encode($products);

            $listCustomerHasCreated = $this->_CUSTOMER_IDS;
            Mage::getResourceModel('migrateaff/affcustomer')->insertData('aff_customer', $listCustomerHasCreated);

            if($remain_size == 0){
                $products = 0;
            }
            $data = array(
                'product_remain'=>$products,
                'remain_size' => $remain_size
            );
            return json_encode($data);
        }else{
            $data =  array(
                'product_remain'=>0,
                'remain_size' => 0
            );
            return json_encode($data);
        }
    }


    public function saveCustomerToMagento($websiteId, $store, $data){
        $customer = Mage::getModel("customer/customer");
        $customer ->setWebsiteId($websiteId)
            ->setStore($store)
            ->setFirstname("")
            ->setLastname($data['Contact'])
            ->setEmail($data['Email'])
            ->setPassword($data['LoginPassword']);
        $customer->setForceConfirmed(true);
        $customer->save();
        $customer->setConfirmation(null);
        $customer->save();

        $customerId = $customer->getId();

        $customAddress = Mage::getModel('customer/address');
        $address = array(
            'customer_address_id' => '',
            'prefix' => '',
            'firstname' => '',
            'middlename' => '',
            'lastname' => $data['Contact'],
            'suffix' => '',
            'company' => '',
            'street' => array(
                '0' => $data['CompanyAddress'],
                '1' => $data['CompanyName']
            ),
            'city' => $data['City'],
            'postcode' => $data['Zip'],
            'telephone' => $data['Telephone'],
            'save_in_address_book' => 1
        );
        $customAddress->setData($address)
            ->setCustomerId($customerId)
            ->setIsDefaultBilling('1')
            ->setIsDefaultShipping('1')
            ->setSaveInAddressBook('1');
        $customAddress->save();

        return $customer;
    }


    public function saveCustomerToAffiliate($customerId,$data){
        $active = MW_Affiliate_Model_Statusactive::ACTIVE;
        $payment_gateway = '';
        $paypal_email = '';
        $auto_withdrawn = 0;
        $payment_release_level = 0;
        $reserve_level = 0;
        $bank_name = '';
        $name_account = '';
        $bank_country = '';
        $swift_bic = '';
        $account_number = '';
        $re_account_number = '';
        $referral_site = '';
        $invitation_type = MW_Affiliate_Model_Typeinvitation::NON_REFERRAL;

        $customerData = array(
            'customer_id'			=> $customerId,
            'active'				=> $active,
            'payment_gateway'		=> $payment_gateway,
            'payment_email'		    => $paypal_email,
            'auto_withdrawn'		=> $auto_withdrawn,
            'withdrawn_level'		=> $payment_release_level,
            'reserve_level'		=> $reserve_level,
            'bank_name'			=> $bank_name,
            'name_account'		=> $name_account,
            'bank_country'		=> $bank_country,
            'swift_bic'			=> $swift_bic,
            'account_number'		=> $account_number,
            're_account_number'	=> $re_account_number,
            'referral_site'		=> $referral_site,
            'total_commission'	=> 0,
            'total_paid'			=> 0,
            'referral_code' 		=> '',
            'status'				=> 1,
            'invitation_type'		=> $invitation_type,
            'customer_time' 		=> now(),
            'customer_invited'	=> 0
        );

        Mage::getModel('affiliate/affiliatecustomers')->saveCustomerAccount($customerData);
        // set lai referral code cho cac customer affiliate
        Mage::helper('affiliate')->setReferralCode($customerId);

        //$store_id = Mage::getModel('customer/customer')->load($customerId)->getStoreId();
        //Mage::helper('affiliate')->setMemberDefaultGroupAffiliate($customerId,$store_id);

        $data = array(
            'customer_id'	=> $customerId,
            'group_id'	=> $data['AffiliateTypeID']
        );
        Mage::getModel('affiliate/affiliategroupmember')->setData($data)->save();

        //set total member customer program
        Mage::helper('affiliate')->setTotalMemberProgram();

    }

    /**
     * @param $customerId  // new aff id
     * @param $affiliateId // old aff id
     */
    public function saveAffiliateTransactionAndCredit($customerId, $affiliateId){
        $allTransactionOfAffiliate = Mage::getResourceModel('migrateaff/oldaff_collection')
                                        ->getAllTransactionOfAffiliate($affiliateId);
        $dataToSaveTran = array();
        $dataToSaveHis = array();
        $dataIntivation = array();

        $count = 1;

        $totalCommission = 0;
        $totalPaid = 0;
        $credit = 0;

        foreach ($allTransactionOfAffiliate as $transaction){
            $tran1 = array(
                'order_id'				=> $transaction['SalesId'],
                'customer_id'           => $transaction['CUSTOMER_ID'],
                'total_commission'		=> $transaction['Commission'],
                'total_discount'		=> 0,
//                'grand_total'		    => $transaction['GRAND_TOTAL'],
                'transaction_time'		=> $transaction['ORDER_DATE'],
                'commission_type'	    => MW_Mwcredit_Model_Transactiontype::BUY_PRODUCT,
                'show_customer_invited' => 0,
                'customer_invited'		=> $customerId,
                'invitation_type'		=> MW_Affiliate_Model_Typeinvitation::REFERRAL_LINK,
                'status'				=> MW_Affiliate_Model_Status::COMPLETE,
            );
            $tran2 = array(
                'order_id'				=> $transaction['SalesId'],
                'customer_id'           => $transaction['CUSTOMER_ID'],
                'total_commission'		=> $transaction['Commission'],
                'total_discount'		=> 0,
//                'grand_total'		    => $transaction['GRAND_TOTAL'],
                'transaction_time'		=> $transaction['ORDER_DATE'],
                'commission_type'	    => MW_Mwcredit_Model_Transactiontype::BUY_PRODUCT,
                'show_customer_invited' => $customerId,
                'customer_invited'		=> 0,
                'invitation_type'		=> MW_Affiliate_Model_Typeinvitation::REFERRAL_LINK,
                'status'				=> MW_Affiliate_Model_Status::COMPLETE,
            );

            $tranHis = array(
                'customer_id' => $transaction['CUSTOMER_ID'],
                'product_id' => 0,
                'program_id' => 0,
                'order_id' => $transaction['SalesId'],
                'total_amount' => $transaction['GRAND_TOTAL'],
                'history_commission' => $transaction['Commission'],
                'history_discount' => 0,
                'transaction_time' => $transaction['ORDER_DATE'],
                'customer_invited'		=> $customerId,
                'invitation_type'		=> MW_Affiliate_Model_Typeinvitation::REFERRAL_LINK,
                'status'				=> MW_Affiliate_Model_Status::COMPLETE,
            );

            $intivation = array(
                'customer_id' => $customerId,
                'email' => '',
                'ip' => $transaction['IPAddress'],
                'count_click_link' => 0,
                'count_register' => 0,
                'count_purchase' => 1,
                'referral_from' => $transaction['Referrer'],
                'referral_from_domain' => '',
                'referral_to' => '',
                'order_id' => $transaction['SalesId'],
                'invitation_type' => 1,
                'invitation_time' => $transaction['ORDER_DATE'],
                'status' => MW_Affiliate_Model_Statusinvitation::PURCHASE,
                'commission' => $transaction['Commission'],
                'count_subscribe' => 0,
            );


            $totalCommission += $transaction['Commission'];
            if($transaction['Paid'] == 0){
                $credit += $transaction['Commission'];
            }else{
                $totalPaid += $transaction['Commission'];
            }

            $dataToSaveTran[] = $tran1;
            $dataToSaveTran[] = $tran2;
            $dataToSaveHis[] = $tranHis;
            $dataIntivation[] = $intivation;

            $count++;

            if($count >= 300){
                $success1 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_transaction', $dataToSaveTran);
                $success2 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_history', $dataToSaveHis);
                $success3 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_invitation', $dataIntivation);

                $this->catchingError($success1, $customerId, $affiliateId, 'transaction');
                $this->catchingError($success2, $customerId, $affiliateId, 'history');
                $this->catchingError($success3, $customerId, $affiliateId, 'invitation');

                $dataToSaveTran = array();
                $dataToSaveHis = array();
                $dataIntivation = array();
                $count = 1;
            }
        }
        if(count($dataToSaveTran) > 0){
            $success1 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_transaction', $dataToSaveTran);
            $success2 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_history', $dataToSaveHis);
            $success3 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_invitation', $dataIntivation);

            $this->catchingError($success1, $customerId, $affiliateId, 'transaction');
            $this->catchingError($success2, $customerId, $affiliateId, 'history');
            $this->catchingError($success3, $customerId, $affiliateId, 'invitation');
        }
        if($credit > 0){
            $creditcustomer = Mage::getModel('mwcredit/creditcustomer')->load($customerId);
            $creditcustomer->setCredit($credit)->save();
        }
        if($totalPaid > 0 || $totalCommission > 0){
            $aff = Mage::getModel('affiliate/affiliatecustomers')->load($customerId);
            $aff->setTotalCommission($totalCommission)
                ->setTotalPaid($totalPaid)
                ->save();
        }
    }

    public function  saveAffiliateIntivation($customerId, $affiliateId){
        $allIntivation = Mage::getResourceModel('migrateaff/oldaff_collection')
            ->getAllIntivationOfAffiliate($affiliateId);
        $dataToSave = array();
        $count = 1;
        foreach($allIntivation as $invitation){
            $in = array(
                'customer_id' => $customerId,
                'email' => '',
                'ip' => $invitation['ClickIP'],
                'count_click_link' => 1,
                'count_register' => 0,
                'count_purchase' => 0,
                'referral_from' => $invitation['Referrer'],
                'referral_from_domain' => '',
                'referral_to' => '',
                'order_id' => '',
                'invitation_type' => 1,
                'invitation_time' => $invitation['ClickDateTime'],
                'status' => MW_Affiliate_Model_Statusinvitation::CLICKLINK,
                'commission' => 0,
                'count_subscribe' => 0,
            );

            $dataToSave[] = $in;
            $count++;

            if($count >= 300){
                $success1 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_invitation', $dataToSave);
                $this->catchingError($success1, $customerId, $affiliateId, 'invitation-click');
                $dataToSave = array();
                $count = 1;
            }
        }
        if(count($dataToSave) > 0){
            $success1 = Mage::getResourceModel('migrateaff/affcustomer')->insertData('mw_affiliate_invitation', $dataToSave);
            $this->catchingError($success1, $customerId, $affiliateId, 'invitation-click');
        }
    }

    public function catchingError($success, $customerId, $affiliateId, $key){
        if(!$success){
            Mage::log( "{$key} error at customerId {$customerId} and affId {$affiliateId}", null, 'migrate-aff.log', true);
        }
    }

    /**
     * @param $Collection
     * @return array
     */
    public function _prepareData($Collection){
        $websiteId = Mage::app()->getWebsite()->getId();
        $store = Mage::app()->getStore();
        try{
            foreach($Collection->getData() as $data){
                $customer = Mage::getModel("customer/customer")->setWebsiteId($websiteId)
                    ->loadByEmail($data['Email']);
                if(!$customer->getId()){
                    // save customer to magento
                    $customer = $this->saveCustomerToMagento($websiteId, $store, $data);
                }
                $customerId = $customer->getId();
                $this->_CUSTOMER_IDS[] = array(
                    "aff_id" => $data['AffiliateID'],
                    "customer_id" => $customerId,
                );
                $aff = Mage::getModel('affiliate/affiliatecustomers')->load($customerId);
                if(!$aff->getId()) {
                    // save customer to Affiliate
                    $this->saveCustomerToAffiliate($customerId, $data);
                }

                // save customer transaction
                $this->saveAffiliateTransactionAndCredit($customerId, $data['AffiliateID']);
                // save affiliate intivation
                $this->saveAffiliateIntivation($customerId, $data['AffiliateID']);

            }
            return true;
        }

        catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'migrate-aff.log', true);
            return false;
        }
    }

}