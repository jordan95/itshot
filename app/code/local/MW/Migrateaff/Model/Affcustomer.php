<?php

class MW_Migrateaff_Model_Affcustomer extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('migrateaff/affcustomer');
    }
    
	private function _getCustomer()
	{
		return Mage::getSingleton('customer/session')->getCustomer();
	}
    
}