<?php

class MW_Migrateaff_Block_Adminhtml_Sync_Customer_Customer
    extends Mage_Core_Block_Template
{

    protected $_template = 'migrateaff/customer.phtml';

    protected $blockGrid;

    public function getBlockGrid()
    {
        if (null === $this->blockGrid) {
            $this->blockGrid = $this->getLayout()->createBlock(
                'migrateaff/adminhtml_sync_customer_grid',
                'sync_customerGrids.grid'
            );
        }
        return $this->blockGrid;
    }
    /**
     * Return HTML of grid block
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getBlockGrid()->toHtml();
    }

    public function syncAffUrl(){
        return  Mage::helper('adminhtml')->getUrl('adminhtml/sync/syncAff');
    }

}