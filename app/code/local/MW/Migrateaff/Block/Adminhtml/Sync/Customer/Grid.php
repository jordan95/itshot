<?php

use MW_Migrateaff_Helper_Data as Data;
class MW_Migrateaff_Block_Adminhtml_Sync_Customer_Grid extends
    MW_Migrateaff_Block_Adminhtml_AbstractGrid {

    protected $ALLOW_CORRECT = true;

    protected $_massactionIdField = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->setId('customerGrid');
        $this->setDefaultSort('AffiliateID');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
    }

    /**
     * @param $collection
     * @return mixed
     */
    public function modifyCollection($collection)
    {
        return $collection;
    }

    /**
     *
     */
    public function modifyColumns()
    {
        return $this->service()->modifiColumns($this, Data::LIST_AFF_NOT_SYNC);
    }

    /**
     * @return mixed
     */
    public function getGridUrl() {
        return $this->getUrl('*/*/customergrid', array(
            '_current' => true
        ));
    }

    /**
     * @param $row
     * @return bool
     */
    public function getRowUrl($row) {
        return false;
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('AffiliateID');
        $this->getMassactionBlock()->setFormFieldName('list_Aff_need_sync');

        $this->getMassactionBlock()->addItem('correct_by_customer',
            array(
            'label'=> Mage::helper('migrateaff')->__('Collect Customer'),
            'url'  => $this->getUrl('*/*/customer', array('_current'=>true)),
        ));

       // Mage::dispatchEvent('adminhtml_catalog_product_grid_prepare_massaction', array('block' => $this));
        return $this;
    }

}
