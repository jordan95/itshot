<?php

class MW_Migrateaff_Adminhtml_SyncController extends Mage_Adminhtml_Controller_Action {


    protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('migrateaff/customer_not_sync')
            ->_addBreadcrumb(
                Mage::helper('adminhtml')->__('Migrate Aff'), Mage::helper('adminhtml')->__('Migrate Aff')
            ) ->_title($this->__('Migrate Aff'))
            ->_title($this->__('Migrate Aff'));
        $this->_title($this->__('Migrate Aff'));
        return $this;
    }

    protected function _isAllowed()
    {
        return true ;// Mage::getSingleton('admin/session')->isAllowed('admin/inventoryreport_historics/');
    }


    public function customerAction(){
        //https://inchoo.net/magento/programming-magento/programmaticaly-adding-new-customers-to-the-magento-store/
        //https://devdocs.magento.com/guides/m1x/api/soap/customer/customer.create.html
        if($listAffNeedSync = $this->getRequest()->getParam('list_Aff_need_sync')){
                $ids_not_sync =  Mage::getResourceModel('migrateaff/oldaff_collection')->getAllIdsOfAffWhichNotSync();
                if(!$ids_not_sync){
                    return $this->runPage();
                }else{
                    $check =  array_intersect ($ids_not_sync,$listAffNeedSync);
                    if(!$check){
                        return $this->runPage();
                    }
                }
             $total_size = sizeof($listAffNeedSync);
             $productString = implode(',',$listAffNeedSync);
             $product_encode = Mage::helper('migrateaff')->base64Encode($productString);
                    $this->loadLayout()
                        ->_setActiveMenu('migrateaff/customer_not_sync')
                        ->_addBreadcrumb(
                            Mage::helper('adminhtml')->__('Migrate Aff'), Mage::helper('adminhtml')->__('Migrate Aff')
                        ) ->_title($this->__('Migrate Aff'))
                        ->_title($this->__('Migrate Aff'));
                    $this->_title($this->__('Migrate Aff'));
                    $this->getLayout()->getBlock('sync_customerGrids')
                        ->setProductsEncode($product_encode)
                        ->setTotalSize($total_size);
                    $this->renderLayout();
                    return;
        }
        return $this->runPage();
    }

    /**
     *
     */
    public function runPage(){
        $this->_initAction();
        $this->renderLayout();
    }

    /**
     *
     */
    public function customergridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function syncAffAction(){
        $product = $this->getRequest()->getParam('product');
        $body = $this->service()->syncAff($product);
        return $this->getResponse()->setBody($body);
    }

    public function service(){
        return MW_Migrateaff_Model_Service::affService();
    }

}
