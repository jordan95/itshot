<?php

use \Hayageek\OAuth2\Client\Provider\Yahoo as YahooOauth2;

class Magestore_Sociallogin_Model_Yalogin extends Mage_Core_Model_Abstract
{

    protected $provider;

    public function getProvider(){
        if(!$this->provider){
            $provider = new YahooOauth2([
                'clientId'     => $this->getConsumerKey(),
                'clientSecret' => $this->getConsumerSecret(),
                'redirectUri'  => $this->getRedirectUri(),
            ]);
            $this->provider = $provider;
        }
        return $this->provider;
    }

    public function hasSession()
    {
        $consumerKey = $this->getConsumerKey();
        $consumerSecret = $this->getConsumerSecret();
        $appId = $this->getAppId();
        return YahooSession::hasSession($consumerKey, $consumerSecret, $appId);
    }

    public function getAuthUrl()
    {
        $consumerKey = $this->getConsumerKey();
        $consumerSecret = $this->getConsumerSecret();
        $callback = YahooUtil::current_url() . '?in_popup';
        return YahooSession::createAuthorizationUrl($consumerKey, $consumerSecret, $callback);
    }

    public function getSession()
    {
        $consumerKey = $this->getConsumerKey();
        $consumerSecret = $this->getConsumerSecret();
        $appId = $this->getAppId();
        return YahooSession::requireSession($consumerKey, $consumerSecret, $appId);
    }
    
    public function getRedirectUri(){
        return Mage::getUrl('sociallogin/yalogin/login', array('_secure'=>true));
    }

    public function getConsumerKey()
    {
        return trim(Mage::getStoreConfig('sociallogin/yalogin/consumer_key'));
    }

    public function getConsumerSecret()
    {
        return trim(Mage::getStoreConfig('sociallogin/yalogin/consumer_secret'));
    }

    public function getAppId()
    {
        return trim(Mage::getStoreConfig('sociallogin/yalogin/app_id'));
    }

}
