<?php

class Magestore_Sociallogin_Block_Linkedlogin extends Mage_Core_Block_Template
{

    public function getLoginUrl()
    {
        $redirect_url = $this->getRedirectUrl();
        $params = [];
        if ($redirect_url) {
            $params = [
                '_query' => [
                    'redirect_url' => $redirect_url
                ]
            ];
        }
        return $this->getUrl('sociallogin/linkedlogin/login', $params);
    }

    /* public function getLoginUrl(){
      return Mage::getModel('sociallogin/linkedlogin')->getLinkedLoginUrl();
      } */

    protected function _beforeToHtml()
    {
        if (!Mage::helper('magenotification')->checkLicenseKey('Sociallogin')) {
            $this->setTemplate(null);
        }
        return parent::_beforeToHtml();
    }

}
