<?php

class Magestore_Sociallogin_Block_Aollogin extends Mage_Core_Block_Template
{

    public function getLoginUrl()
    {
        $redirect_url = $this->getRedirectUrl();
        $params = [];
        if ($redirect_url) {
            $params = [
                '_query' => [
                    'redirect_url' => $redirect_url
                ]
            ];
        }
        return $this->getUrl('sociallogin/allogin/login', $params);
    }

    public function getAlLoginUrl()
    {
        return $this->getUrl('sociallogin/allogin/setScreenName');
    }

    public function getEnterName()
    {
        return 'ENTER SCREEN NAME';
    }

    public function getName()
    {
        return 'Name';
    }

    public function getCheckName()
    {
        return $this->getUrl('sociallogin/allogin/setBlock');
    }

    protected function _beforeToHtml()
    {
        if (!Mage::helper('magenotification')->checkLicenseKey('Sociallogin')) {
            $this->setTemplate(null);
        }
        return parent::_beforeToHtml();
    }

}
