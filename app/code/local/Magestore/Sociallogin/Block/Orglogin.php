<?php

class Magestore_Sociallogin_Block_Orglogin extends Mage_Core_Block_Template
{

    public function getLoginUrl()
    {
        $redirect_url = $this->getRedirectUrl();
        $params = [];
        if ($redirect_url) {
            $params = [
                '_query' => [
                    'redirect_url' => $redirect_url
                ]
            ];
        }
        return $this->getUrl('sociallogin/orglogin/login', $params);
    }

    public function getAlLoginUrl()
    {
        return Mage::getModel('sociallogin/orglogin')->getOrgLoginUrl();
    }

}
