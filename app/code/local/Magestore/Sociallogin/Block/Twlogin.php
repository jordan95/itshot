<?php

class Magestore_Sociallogin_Block_Twlogin extends Mage_Core_Block_Template
{

    public function getLoginUrl()
    {
        $redirect_url = $this->getRedirectUrl();
        $params = [];
        if ($redirect_url) {
            $params = [
                '_query' => [
                    'redirect_url' => $redirect_url
                ]
            ];
        }
        return $this->getUrl('sociallogin/twlogin/login', $params);
    }

    public function setBackUrl()
    {
        $currentUrl = Mage::helper('core/url')->getCurrentUrl();
        Mage::getSingleton('core/session')->setBackUrl($currentUrl);
        //Zend_debug::dump($currentUrl);
        return $currentUrl;
    }

    protected function _beforeToHtml()
    {
        if (!Mage::helper('magenotification')->checkLicenseKey('Sociallogin')) {
            $this->setTemplate(null);
        }
        return parent::_beforeToHtml();
    }

}
