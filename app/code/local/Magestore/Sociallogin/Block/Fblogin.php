<?php

class Magestore_Sociallogin_Block_Fblogin extends Mage_Core_Block_Template
{

    public function getLoginUrl()
    {
        $redirect_url = $this->getRedirectUrl();
        $params = [];
        if ($redirect_url) {
            $params = [
                '_query' => [
                    'redirect_url' => $redirect_url
                ]
            ];
        }
        return $this->getUrl('sociallogin/fblogin/login', $params);
    }

    public function getFbUser()
    {
        return Mage::getModel('sociallogin/fblogin')->getFbUser();
    }

    public function getFbLoginUrl()
    {
        return Mage::getModel('sociallogin/fblogin')->getFbLoginUrl();
    }

    public function getDirectLoginUrl()
    {
        return Mage::helper('sociallogin')->getDirectLoginUrl();
    }

    protected function _beforeToHtml()
    {
        if (!Mage::helper('magenotification')->checkLicenseKey('Sociallogin')) {
            $this->setTemplate(null);
        }
        return parent::_beforeToHtml();
    }

}
