<?php

class Magestore_Sociallogin_Block_Checkout_Buttons extends Magestore_Sociallogin_Block_Sociallogin
{

    public function getInsButton()
    {
        return $this->getLayout()->createBlock('sociallogin/inslogin')
                        ->setRedirectUrl(Mage::helper('checkout/url')->getCheckoutUrl())
                        ->setTemplate('sociallogin/bt_inslogin.phtml')->toHtml();
    }

    public function getYahooButton()
    {
        return $this->getLayout()->createBlock('sociallogin/yalogin')
                        ->setRedirectUrl(Mage::helper('checkout/url')->getCheckoutUrl())
                        ->setTemplate('sociallogin/bt_yalogin.phtml')->toHtml();
    }

    public function getAmazonButton()
    {
        return $this->getLayout()->createBlock('sociallogin/amazon')
                        ->setRedirectUrl(Mage::helper('checkout/url')->getCheckoutUrl())
                        ->setTemplate('sociallogin/bt_amazonlogin.phtml')->toHtml();
    }

    public function getGmailButton()
    {
        return $this->getLayout()->createBlock('sociallogin/gologin')
                        ->setRedirectUrl(Mage::helper('checkout/url')->getCheckoutUrl())
                        ->setTemplate('sociallogin/bt_gologin.phtml')->toHtml();
    }

    public function getFacebookButton()
    {
        return $this->getLayout()->createBlock('sociallogin/fblogin')
                        ->setRedirectUrl(Mage::helper('checkout/url')->getCheckoutUrl())
                        ->setTemplate('sociallogin/bt_fblogin.phtml')->toHtml();
    }

    public function getAolButton()
    {
        return $this->getLayout()->createBlock('sociallogin/aollogin')
                        ->setRedirectUrl(Mage::helper('checkout/url')->getCheckoutUrl())
                        ->setTemplate('sociallogin/bt_aollogin.phtml')->toHtml();
    }

    public function getWpButton()
    {
        return $this->getLayout()->createBlock('sociallogin/wplogin')
                        ->setRedirectUrl(Mage::helper('checkout/url')->getCheckoutUrl())
                        ->setTemplate('sociallogin/bt_wplogin.phtml')->toHtml();
    }

    public function getCalButton()
    {
        return $this->getLayout()->createBlock('sociallogin/callogin')
                        ->setRedirectUrl(Mage::helper('checkout/url')->getCheckoutUrl())
                        ->setTemplate('sociallogin/bt_callogin.phtml')->toHtml();
    }

    public function getOrgButton()
    {
        return $this->getLayout()->createBlock('sociallogin/orglogin')
                        ->setRedirectUrl(Mage::helper('checkout/url')->getCheckoutUrl())
                        ->setTemplate('sociallogin/bt_orglogin.phtml')->toHtml();
    }

    public function getFqButton()
    {
        return $this->getLayout()->createBlock('sociallogin/fqlogin')
                        ->setRedirectUrl(Mage::helper('checkout/url')->getCheckoutUrl())
                        ->setTemplate('sociallogin/bt_fqlogin.phtml')->toHtml();
    }

    public function getLiveButton()
    {
        return $this->getLayout()->createBlock('sociallogin/livelogin')
                        ->setRedirectUrl(Mage::helper('checkout/url')->getCheckoutUrl())
                        ->setTemplate('sociallogin/bt_livelogin.phtml')->toHtml();
    }

    public function getVkButton()
    {
        return $this->getLayout()->createBlock('sociallogin/vklogin')
                        ->setRedirectUrl(Mage::helper('checkout/url')->getCheckoutUrl())
                        ->setTemplate('sociallogin/bt_vklogin.phtml')->toHtml();
    }

    public function getTwitterButton()
    {
        return $this->getLayout()->createBlock('sociallogin/twlogin')
                        ->setRedirectUrl(Mage::helper('checkout/url')->getCheckoutUrl())
                        ->setTemplate('sociallogin/bt_twlogin.phtml')->toHtml();
    }

}
