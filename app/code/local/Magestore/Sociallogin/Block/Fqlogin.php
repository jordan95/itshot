<?php

class Magestore_Sociallogin_Block_Fqlogin extends Mage_Core_Block_Template
{

    public function getLoginUrl()
    {
        $redirect_url = $this->getRedirectUrl();
        $params = [];
        if ($redirect_url) {
            $params = [
                '_query' => [
                    'redirect_url' => $redirect_url
                ]
            ];
        }
        return $this->getUrl('sociallogin/fqlogin/login', $params);
    }

    public function getFqUser()
    {
        return Mage::getModel('sociallogin/fqlogin')->getFqUser();
    }

    public function getFqLoginUrl()
    {
        return Mage::getModel('sociallogin/fqlogin')->getFqLoginUrl();
    }

    public function getDirectLoginUrl()
    {
        return Mage::helper('sociallogin')->getDirectLoginUrl();
    }

    protected function _beforeToHtml()
    {
        if (!Mage::helper('magenotification')->checkLicenseKey('Sociallogin')) {
            $this->setTemplate(null);
        }
        return parent::_beforeToHtml();
    }

}
