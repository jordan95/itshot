<?php

class Magestore_Sociallogin_Block_Aol extends Mage_Core_Block_Template
{

    public function getLoginUrl()
    {
        $redirect_url = $this->getRedirectUrl();
        $params = [];
        if ($redirect_url) {
            $params = [
                '_query' => [
                    'redirect_url' => $redirect_url
                ]
            ];
        }
        return $this->getUrl('sociallogin/allogin/login', $params);
    }

    public function getAlLoginUrl()
    {
        return Mage::getModel('sociallogin/allogin')->getAlLoginUrl();
    }

}
