<?php

class Magestore_Sociallogin_Block_Wplogin extends Mage_Core_Block_Template
{

    public function getLoginUrl()
    {
        $redirect_url = $this->getRedirectUrl();
        $params = [];
        if ($redirect_url) {
            $params = [
                '_query' => [
                    'redirect_url' => $redirect_url
                ]
            ];
        }
        return $this->getUrl('sociallogin/wplogin/login', $redirect_url);
    }

    public function getAlLoginUrl()
    {
        return $this->getUrl('sociallogin/wplogin/setBlogName');
    }

    public function getCheckName()
    {
        return $this->getUrl('sociallogin/wplogin/setBlock');
    }

    public function getEnterName()
    {
        return 'ENTER YOUR BLOG NAME';
    }

    public function getName()
    {
        return 'Name';
    }

}
