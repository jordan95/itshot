<?php

class Magestore_Sociallogin_YaloginController extends Mage_Core_Controller_Front_Action
{

    // url to login
    public function loginAction()
    {
        if (!Mage::helper('magenotification')->checkLicenseKeyFrontController($this)) {
            return;
        }
        $yalogin = Mage::getModel('sociallogin/yalogin');
        $provider = $yalogin->getProvider();
        $request = $this->getRequest();
        if($redirect_url = $request->getQuery('redirect_url', false)){
            $this->getSession()->setRedirectUrlAfterLogin(urldecode($redirect_url));
        }
        if ($error = $request->getQuery('error', false)) {
            return $this->getResponse()->setBody('Got error: ' . $error);
        }

        if (!$code = $request->getQuery('code', false)) {
            $authUrl = $provider->getAuthorizationUrl();
            $this->getSession()->setOauth2State($provider->getState());
            return $this->_redirectUrl($authUrl);
        }

        if (!$state = $request->getQuery('state', false) || $state != $this->getSession()->getOauth2State()) {
            $this->getSession()->unsOauth2State();
            return $this->getResponse()->setBody('Invalid state');
        }

        $token = $provider->getAccessToken('authorization_code', [
            'code' => $code
        ]);
        try {
            $ownerDetails = $provider->getResourceOwner($token);
            $user = array();
            $user['email'] = $ownerDetails->getEmail();
            $user['firstname'] = $ownerDetails->getFirstName();
            $user['lastname'] = $ownerDetails->getLastName();

            //get website_id and sote_id of each stores
            $store_id = Mage::app()->getStore()->getStoreId();
            $website_id = Mage::app()->getStore()->getWebsiteId();

            $customer = Mage::helper('sociallogin')->getCustomerByEmail($user['email'], $website_id);
            if (!$customer || !$customer->getId()) {
                //Login multisite
                $customer = Mage::helper('sociallogin')->createCustomerMultiWebsite($user, $website_id, $store_id);
                if (Mage::getStoreConfig(('sociallogin/general/send_newemail'), Mage::app()->getStore()->getId()))
                    $customer->sendNewAccountEmail('registered', '', Mage::app()->getStore()->getId());
                if (Mage::getStoreConfig('sociallogin/yalogin/is_send_password_to_customer')) {
                    $customer->sendPasswordReminderEmail();
                }
            }
            // fix confirmation
            if ($customer->getConfirmation()) {
                try {
                    $customer->setConfirmation(null);
                    $customer->save();
                } catch (Exception $e) {}
            }
            Mage::getSingleton('customer/session')->setCustomerAsLoggedIn($customer);
            $post_redirect  = $this->_loginPostRedirect(); 
            die("<script type=\"text/javascript\">if(navigator.userAgent.match('CriOS')){window.location.href=\"" . $post_redirect . "\";}else{try{window.opener.location.href=\"" . $post_redirect . "\";}catch(e){window.opener.location.reload(true);} window.close();}</script>");
        } catch (Exception $e) {
            // Failed to get user details
            exit('Something went wrong: ' . $e->getMessage());
        }
    }

    public function getSession()
    {
        return Mage::getSingleton('core/session');
    }

    protected function _loginPostRedirect()
    {
        if($redirect_url = $this->getSession()->getRedirectUrlAfterLogin()){
            $this->getSession()->unsRedirectUrlAfterLogin();
            return $redirect_url;
        }
        
        $selecturl = Mage::getStoreConfig(('sociallogin/general/select_url'), Mage::app()->getStore()->getId());
        if ($selecturl == 0)
            return Mage::getUrl('customer/account');
        if ($selecturl == 2)
            return Mage::getUrl();
        if ($selecturl == 3)
            return Mage::getSingleton('core/session')->getSocialCurrentpage();
        if ($selecturl == 4)
            return Mage::getStoreConfig(('sociallogin/general/custom_page'), Mage::app()->getStore()->getId());
        if ($selecturl == 1 && Mage::helper('checkout/cart')->getItemsCount() != 0)
            return Mage::getUrl('checkout/cart');
        else
            return Mage::getUrl();
    }

}
