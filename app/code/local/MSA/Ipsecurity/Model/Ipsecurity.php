<?php
class MSA_Ipsecurity_Model_Ipsecurity extends Mage_Core_Model_Abstract
{
	protected $_ipaddress = array();
	
    public function _construct()
    {
        parent::_construct();
        $this->_init('ipsecurity/ipsecurity');
    }
    
	public function prepareData()
    {
        
    }
    
	public function load($id, $field=null)
    {
        $this->_validated = false;
        $this->_isValid = false;

        parent::load($id);

        $this->prepareData();

        return $this;
    }
    
    public function checkUserIPAdd($ip)
    {
		$found = false;
		$objModel = Mage::getModel("ipsecurity/ipsecurity");
		$ipData = $objModel->getCollection()->addFieldToFilter("ip_address",$ip)->addFieldToFilter("status",1);
		$dataArr = $ipData->getData();
		//print_r($dataArr);
		if(isset($dataArr[0]["id"]) && $dataArr[0]["id"]>0)
		{
			$found = true;
			$id = $dataArr[0]["id"];
		}
		//echo "Id=>".$id;
		return $found;
	}
	
	public function checkUserEmail($email)
    {
		$found = false;
		$objModel = Mage::getModel("ipsecurity/ipsecurity");
		$ipData = $objModel->getCollection()->addFieldToFilter("ip_address",$email)->addFieldToFilter("status",1);
		$dataArr = $ipData->getData();
		if(isset($dataArr[0]["id"]) && $dataArr[0]["id"]>0)
		{
			$found = true;
			$id = $dataArr[0]["id"];
		}
		return $found;
	}	
}
