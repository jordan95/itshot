<?php
class MSA_Ipsecurity_Model_Mysql4_Ipsecurity_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('ipsecurity/ipsecurity');
    }
}
