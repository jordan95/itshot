<?php
class MSA_Ipsecurity_Model_Observer 
{
	public function CheckBlockedIp($observer) {
		//$getData = $observer->getData();		 
		//$quote = $observer->getQuote()->getData();
		//$quote = $observer->getEvent()->getQuote()->getData();
		
		//get post data and get customer email
		$email = "";
		$postData = Mage::app()->getRequest()->getPost();
		//print_r($postData);
		if(isset($postData["billing"]["email"]) && $postData["billing"]["email"]!="")
		{
			$email = $postData["billing"]["email"];
		}
		if($email=="")
		{
			//check cutomer session and get email
			$customer = Mage::getSingleton('customer/session')->getCustomer();
			$email = $customer->getEmail();
		}
		//echo "Email==>".$email;
		
		//get client IP address
		$clientIp = Mage::helper('ipsecurity/customcode')->getip_index();
		$objModel = Mage::getModel("ipsecurity/ipsecurity");
		
		//Check if IP or Email Address is blocked and display message to user
		$chekIPAdd = $objModel->checkUserIPAdd($clientIp);
		$chekEmail = $objModel->checkUserEmail($email);
		$blocked =  false;
		$blockType = "";
		if($chekIPAdd==true)
		{
			$blocked =  true;
			$blockType = "IP";
		}
		else if($chekEmail==true)
		{			
			$blocked =  true;
			$blockType = "Email";
		}
		else
		{
			$blocked =  false;
		}
		
		if($blocked)
		{
			//Mage::log($logText, null, 'ip_blocked_order.txt');
			//$redirectUrl = Mage::getUrl('ipsecurity/error');
			
			//create error log.
			$logText = "\n Email=>".$email." ### IP=>".$clientIp." Date=>".date("Y-m-d H:i:s")." Users {$blockType} is blocked";
			file_put_contents("/var/www/ItsHot/releases/feeds/ip_blocked_order.txt", $logText, FILE_APPEND);
			
			$erroMsg = "Sorry for inconvience it seems like some issue with information you have provided.";
			$erroMsg .= "\nPlease check your Shiping, Billing Address and Payment Method.";
			$erroMsg .= "\n\t\t\t\t\t\t\t\t OR \n Please email sales@ItsHot.com with the issue and we will assist you ASAP.";
			Mage::throwException(Mage::helper('ipsecurity')->__($erroMsg));
			exit;
		}
	}
	
	public function CheckDiamondSearchProduct($observer)
	{
		$items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
		foreach ($items as $item)
		{
			$id = $item->getId();
			$sku = $item->getSku();
			//$price = $item->getPrice();
			$price =$item->getRowTotal();
			$pos = strpos($sku, "loose_diamond");
			
			if($pos==0 && $price==0)
			{
				//echo "<br>Id=>".$id." ## SKU=>".$sku." ## Price=>".$price;
				$cart = Mage::getSingleton('checkout/session')->getQuote();
				$cart->removeItem($id)->save();
			}
		}
	}
	
	/**
	 * @Added By : Raviraj(MS)
	 * @Added On : 6-Nov-2014
	 * @Description: Check cart items and remove Gift Certificate if applied
	 **/ 
	public function controller_action_predispatch_checkout_cart_index($observer)
	{
		//get gift certificate applied to cart
		$gcs = "";
		$gcs = Mage::getSingleton('checkout/session')->getQuote()->getGiftcertCode();
		
		if($gcs!="")
		{
			//get cart items
			$cart_items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
			$category_model = Mage::getModel('catalog/category'); 
			$_categoryObj = $category_model->load(59);
			$filter_cats = $category_model->getResource()->getAllChildren($_categoryObj);
			
			//Check item's category.
			$found_cat = false;
			foreach($cart_items as $temp_item) 
			{
				$categories_array = $temp_item->getProduct()->getCategoryIds();
				foreach($categories_array as $cat)
				{
					if(in_array($cat,$filter_cats))
					{
						$found_cat = true;
						break;
					}
				}
			}//end foreach
			
			//If item belongs to Luxury Wrist Watches or it's subcategory then remove the Gift Certificate
			if($found_cat)
			{
				//echo "You can't add this cert.";
				
				//Remove Gift Certificate
				$gcsArr = array();//set blank
				
				Mage::getSingleton('checkout/session')->getQuote()->setGiftcertCode(join(', ', $gcsArr))->save();	
				Mage::getSingleton('checkout/session')->addError(Mage::helper('ugiftcert')->__("Gift certificate '%s' can't be applied to your order.", $gcs));
			}
		}//end if
	}
}
