<?php
class MSA_Ipsecurity_Block_Adminhtml_Ipsecurity_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('ipsecurityGrid');
      $this->setDefaultSort('id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('ipsecurity/ipsecurity')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('id', array(
          'header'    => Mage::helper('ipsecurity')->__('Id'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'id',
      ));

      $this->addColumn('ip_address', array(
          'header'    => Mage::helper('ipsecurity')->__('IP/Email Address'),
          'align'     =>'left',
          'index'     => 'ip_address',
      ));
	  $this->addColumn('description', array(
          'header'    => Mage::helper('ipsecurity')->__('Description'),
          'align'     =>'left',
          'index'     => 'description',
      ));

	  $this->addColumn('status', array(
          'header'    => Mage::helper('ipsecurity')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
      
      $this->addColumn('created_time', array(
            'header'    => Mage::helper('cms')->__('Date Created'),
            'index'     => 'created_time',
            'width'     => '150px',
            'type'      => 'datetime',
        ));

        $this->addColumn('update_time', array(
            'header'    => Mage::helper('cms')->__('Last Modified'),
            'index'     => 'update_time',
            'width'     => '150px',
            'type'      => 'datetime',
        ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('ipsecurity')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('ipsecurity')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('ipsecurity')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('ipsecurity')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ipsecurity');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('ipsecurity')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('ipsecurity')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('ipsecurity/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('ipsecurity')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('ipsecurity')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}
