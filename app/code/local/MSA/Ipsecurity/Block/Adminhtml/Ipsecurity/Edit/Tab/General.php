<?php
class MSA_Ipsecurity_Block_Adminhtml_Ipsecurity_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm() 
    {
        $data = Mage::registry('ipsecurity_data');

        if (is_object($data)) $data = $data->getData();

        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('general', array('legend' => $this->__('IP/Email Address')));

        $fieldset->addField('ip_address', 'text', array(
                'label'    => $this->__('IP/Email Address'),
                'name'     => 'ip_address',
                'required' => true,
            ));

       $fieldset->addField('description', 'text', array(
                'label'    => $this->__('Decription'),
                'name'     => 'description',
                'required' => false,
            ));
	    $fieldset->addField('status', 'select', array(
                'label'  => $this->__('Status'),
                'name'   => 'status',
                'values' => MSA_Ipsecurity_Model_Status::getOptionArray(),
            ));
        $form->setValues($data);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
