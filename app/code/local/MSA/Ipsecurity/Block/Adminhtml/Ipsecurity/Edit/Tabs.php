<?php
class MSA_Ipsecurity_Block_Adminhtml_Ipsecurity_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('ipsecurity_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('ipsecurity')->__('IP/Email Address Information'));
  }

  protected function _beforeToHtml()
  {
     $this->addTab('general', array(
                'label'   => $this->__('General'),
                'title'   => $this->__('General'),
                'content' => $this->getLayout()->createBlock('ipsecurity/adminhtml_ipsecurity_edit_tab_general')->toHtml()
            )
        );
     
      return parent::_beforeToHtml();
 
  }
  
}
