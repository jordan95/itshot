<?php
class MSA_Ipsecurity_Block_Adminhtml_Ipsecurity_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'ipsecurity';
        $this->_controller = 'adminhtml_ipsecurity';
        
        $this->_updateButton('save', 'label', Mage::helper('ipsecurity')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('ipsecurity')->__('Delete'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('ipsecurity_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'ipsecurity_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'ipsecurity_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('ipsecurity_data') && Mage::registry('ipsecurity_data')->getId() ) {
		
            return Mage::helper('ipsecurity')->__("Edit IP/Email Address '%s'", $this->htmlEscape(Mage::registry('ipsecurity_data')->getIpAddress()));
        } else {
            return Mage::helper('ipsecurity')->__('Add IP/Email Address');
        }
    }
	
	
	
}
