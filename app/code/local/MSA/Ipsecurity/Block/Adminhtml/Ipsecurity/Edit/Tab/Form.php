<?php
class MSA_Ipsecurity_Block_Adminhtml_Ipsecurity_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	public function __construct()
    {
        parent::__construct();
        $this->setTemplate('ipsecurity/ipsecurity.phtml');
    }
	
	protected function _getDefaultStoreId()
    {
        return Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID;
    }   
    
	public function getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store');
        return Mage::app()->getStore($storeId);
    }   
    
	public function getIpAddress()
	{ 
	    return $this->htmlEscape(Mage::registry('ipsecurity_data')->getIpAddress());
	}
	
	public function getDescription()
	{
		    return $this->htmlEscape(Mage::registry('ipsecurity_data')->getDescription());
	}
	
	public function getStatus()
	{
		    return $this->htmlEscape(Mage::registry('ipsecurity_data')->getStatus());
	}
}
