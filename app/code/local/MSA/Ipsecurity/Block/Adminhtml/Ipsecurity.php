<?php
class MSA_Ipsecurity_Block_Adminhtml_Ipsecurity extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_ipsecurity';
    $this->_blockGroup = 'ipsecurity';
    $this->_headerText = Mage::helper('ipsecurity')->__('Manage IP/Email Address');
    $this->_addButtonLabel = Mage::helper('ipsecurity')->__('Add New IP/Email Address');
    parent::__construct();
  }
}
