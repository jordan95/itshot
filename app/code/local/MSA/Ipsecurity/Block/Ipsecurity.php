<?php
class MSA_Ipsecurity_Block_Ipsecurity extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getIpsecurity()     
     { 
        if (!$this->hasData('ipsecurity')) {
            $this->setData('ipsecurity', Mage::registry('ipsecurity'));
        }
        return $this->getData('ipsecurity');        
    }
	
}
