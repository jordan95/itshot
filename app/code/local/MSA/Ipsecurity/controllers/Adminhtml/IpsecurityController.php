<?php
class MSA_Ipsecurity_Adminhtml_IpsecurityController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('ipsecurity/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Manage IP/Email Address List'), Mage::helper('adminhtml')->__('Manage IP/Email Address List'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('ipsecurity/ipsecurity')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('ipsecurity_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('ipsecurity/items');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Manage IP'), Mage::helper('adminhtml')->__('Manage IP/Email'));
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock('ipsecurity/adminhtml_ipsecurity_edit'))
				->_addLeft($this->getLayout()->createBlock('ipsecurity/adminhtml_ipsecurity_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ipsecurity')->__('IP/Email does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction()
	{
		if ($data = $this->getRequest()->getPost()) 
		{
			$model = Mage::getModel('ipsecurity/ipsecurity');	
			
			if(isset($data['ipsecurity']))
			{
				$data['ipsecurity'] = array_filter(array_map('trim',explode(',',$data['ipsecurity'])));
				$data['ipsecurity'] = implode(',', $data['ipsecurity']);
			}
			
			$model->setData($data)->setId($this->getRequest()->getParam('id'));
			
			try
			{
				if($model->getId()!="")
				{
					$model->setUpdateTime(now());
				}
				else
				{
					$model->setCreatedTime(now())->setUpdateTime(now());
				}
				/*
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())->setUpdateTime(now());
				}
				else
				{
					$model->setUpdateTime(now());
				}*/	
				
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('ipsecurity')->__('IP/Email was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            }
            catch (Exception $e)
            {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ipsecurity')->__('Unable to find IP/Email to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction()
	{
		if( $this->getRequest()->getParam('id') > 0 )
		{
			try
			{
				$model = Mage::getModel('ipsecurity/ipsecurity');				 
				$model->setId($this->getRequest()->getParam('id'))->delete();					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('IP/Email was successfully deleted'));
				$this->_redirect('*/*/');
			}
			catch (Exception $e)
			{
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction()
    {
        $keywordsIds = $this->getRequest()->getParam('ipsecurity');
        if(!is_array($keywordsIds))
        {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        }
        else
        {
            try
            {
                foreach ($keywordsIds as $keywordsId)
                {
                    $keywords = Mage::getModel('ipsecurity/ipsecurity')->load($keywordsId);
                    $keywords->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($keywordsIds)
                    )
                );
            }
            catch (Exception $e)
            {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
        $keywordsIds = $this->getRequest()->getParam('ipsecurity');
        if(!is_array($keywordsIds))
        {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        }
        else
        {
            try
            {
                foreach ($keywordsIds as $keywordsId)
                {
                    $keywords = Mage::getSingleton('ipsecurity/ipsecurity')
                        ->load($keywordsId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->setUpdateTime(now())
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($keywordsIds))
                );
            }
            catch (Exception $e)
            {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'ipsecurity.csv';
        $content    = $this->getLayout()->createBlock('ipsecurity/adminhtml_ipsecurity_grid')->getCsv();
        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'ipsecurity.xml';
        $content    = $this->getLayout()->createBlock('ipsecurity/adminhtml_ipsecurity_grid')->getXml();
        $this->_sendUploadResponse($fileName, $content);
    }
   
    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
	
}
