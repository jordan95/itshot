<?php
class MSA_Emailnotify_Model_Emailnotify extends Mage_Core_Model_Abstract {
	protected $_emailaddress = array();
	
	public function _construct() {
		parent::_construct();
		$this->_init('emailnotify/emailnotify');
	}
	
	public function prepareData() {
	}
	
	public function load($id,$field=null) {
		$this->_validated = false;
		$this->_isValid = false;
		parent::load($id);
		$this->prepareData();
		return $this;
	}
	
	public function checkUserEmailAdd($templateId,$email) {
		$found = false;
		if($templateId!='20') {
			$objModel = Mage::getModel("emailnotify/emailnotify");
			$emailData = $objModel->getCollection()->addFieldToFilter("email_address",$email)->addFieldToFilter("status",1);
			$dataArr = $emailData->getData();
			if(isset($dataArr[0]["id"]) && $dataArr[0]["id"]>0) {
				$tname_array = explode(',',$dataArr[0]["email_description"]);
				if(in_array($templateId,$tname_array) || in_array('all',$tname_array)) {
					$found = true;
					$id = $dataArr[0]["id"];
				}
			}
		}
		return $found;
	}
	
	public function checkReviewEmailNotify($email,$order_date) {
		$found = false;
		$objModel = Mage::getModel("emailnotify/emailnotify");
		$emailData = $objModel->getCollection()->addFieldToFilter("email_address",$email)->addFieldToFilter("status",1);
		$dataArr = $emailData->getData();
		if(count($dataArr)>0) {
			foreach($dataArr as $dataVal) {
				$tname_array = explode(',',$dataVal["email_description"]);
				if(in_array('20',$tname_array) && $dataVal["created_time"]>$order_date) {
					$found = true;
				}
			}
		}
		return $found;
	}
}
