<?php
class MSA_Emailnotify_Model_Templatesname extends Varien_Object {
	static public function getOptionArray() {
		return array(
			'22'=>array('label'=>'Newsletter Popup Signup Coupon','value'=>'22'),
			'20'=>array('label'=>'Product Review Email','value'=>'20'),
			'14'=>array('label'=>'Past Customer Coupon','value'=>'14'),
			'rewards_expire_email_custom_template'=>array('label'=>'Rewards Expiry Notification','value'=>'rewards_expire_email_custom_template'),
			'all'=>array('label'=>'Block All Emails From Website','value'=>'all')
		);
	}
}
