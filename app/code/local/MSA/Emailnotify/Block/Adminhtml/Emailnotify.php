<?php
class MSA_Emailnotify_Block_Adminhtml_Emailnotify extends Mage_Adminhtml_Block_Widget_Grid_Container {
	public function __construct() {
		$this->_controller = 'adminhtml_emailnotify';
		$this->_blockGroup = 'emailnotify';
		$this->_headerText = Mage::helper('emailnotify')->__('Manage Email Notification');
		$this->_addButtonLabel = Mage::helper('emailnotify')->__('Add New Email Address To Stop Receiving Email');
		parent::__construct();
	}
}
