<?php
class MSA_Emailnotify_Block_Adminhtml_Emailnotify_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {
	public function __construct() {
		parent::__construct();
		$this->setId('emailnotify_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('emailnotify')->__('Email Address Information'));
	}
	
	protected function _beforeToHtml() {
		$this->addTab('general',array(
			'label'=>$this->__('General'),
			'title'=>$this->__('General'),
			'content'=>$this->getLayout()->createBlock('emailnotify/adminhtml_emailnotify_edit_tab_general')->toHtml()
		));
		return parent::_beforeToHtml();
	}
}
