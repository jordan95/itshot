<?php
class MSA_Emailnotify_Block_Adminhtml_Emailnotify_Grid extends Mage_Adminhtml_Block_Widget_Grid {
	public function __construct() {
		parent::__construct();
		$this->setId('emailnotifyGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}
	
	protected function _prepareCollection() {
		$collection = Mage::getModel('emailnotify/emailnotify')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}
	
	protected function _prepareColumns() {
		$this->addColumn('id',array(
			'header'=>Mage::helper('emailnotify')->__('Id'),
			'align'=>'right',
			'width'=>'50px',
			'index'=>'id',
		));
		$this->addColumn('email_address',array(
			'header'=>Mage::helper('emailnotify')->__('Email Address'),
			'align'=>'left',
			'index'=>'email_address',
		));
		$this->addColumn('email_description',array(
			'header'=>Mage::helper('emailnotify')->__('Email Description'),
			'align'=>'left',
			'index'=>'email_description',
			'renderer'=>'MSA_Emailnotify_Block_Adminhtml_Emailnotify_Renderer_View',
		));
		$this->addColumn('status',array(
			'header'=>Mage::helper('emailnotify')->__('Status'),
			'align'=>'left',
			'width'=>'80px',
			'index'=>'status',
			'type'=>'options',
			'options'=>array(
				1=>'Enabled',
				2=>'Disabled',
			),
		));
		$this->addColumn('created_time',array(
			'header'=>Mage::helper('emailnotify')->__('Date Created'),
			'index'=>'created_time',
			'width'=>'150px',
			'type'=>'datetime',
		));
		$this->addColumn('update_time',array(
			'header'=>Mage::helper('emailnotify')->__('Last Modified'),
			'index'=>'update_time',
			'width'=>'150px',
			'type'=>'datetime',
		));
		$this->addColumn('action',array(
			'header'=>Mage::helper('emailnotify')->__('Action'),
			'width'=>'100',
			'type'=>'action',
			'getter'=>'getId',
			'actions'=>array(
				array(
					'caption'=>Mage::helper('emailnotify')->__('Edit'),
					'url'=>array('base'=>'*/*/edit'),
					'field'=>'id'
				)
			),
			'filter'=>false,
			'sortable'=>false,
			'index'=>'stores',
			'is_system'=>true,
		));
		$this->addExportType('*/*/exportCsv',Mage::helper('emailnotify')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('emailnotify')->__('XML'));
		return parent::_prepareColumns();
	}
	
	protected function _prepareMassaction() {
		$this->setMassactionIdField('id');
		$this->getMassactionBlock()->setFormFieldName('emailnotify');
		
		$this->getMassactionBlock()->addItem('delete',array(
			'label'=>Mage::helper('emailnotify')->__('Delete'),
			'url'=>$this->getUrl('*/*/massDelete'),
			'confirm'=>Mage::helper('emailnotify')->__('Are you sure?')
		));
		
		$statuses = Mage::getSingleton('emailnotify/status')->getOptionArray();
		
		array_unshift($statuses,array('label'=>'','value'=>''));
		
		$this->getMassactionBlock()->addItem('status',array(
			'label'=>Mage::helper('emailnotify')->__('Change status'),
			'url'=>$this->getUrl('*/*/massStatus',array('_current'=>true)),
			'additional'=>array(
				'visibility'=>array(
					'name'=>'status',
					'type'=>'select',
					'class'=>'required-entry',
					'label'=>Mage::helper('emailnotify')->__('Status'),
					'values'=>$statuses
				)
			)
		));
		return $this;
	}
	
	public function getRowUrl($row) {
		return $this->getUrl('*/*/edit',array('id'=>$row->getId()));
	}
}
