<?php
class MSA_Emailnotify_Block_Adminhtml_Emailnotify_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {
	public function __construct() {
		parent::__construct();
		
		$this->_objectId = 'id';
		$this->_blockGroup = 'emailnotify';
		$this->_controller = 'adminhtml_emailnotify';
		
		$this->_updateButton('save','label',Mage::helper('emailnotify')->__('Save'));
		$this->_updateButton('delete','label',Mage::helper('emailnotify')->__('Delete'));
		
		$this->_addButton('saveandcontinue',array(
			'label'=>Mage::helper('emailnotify')->__('Save And Continue Edit'),
			'onclick'=>'saveAndContinueEdit()',
			'class'=>'save',
		),-100);
		
		$this->_formScripts[] = "
			function toggleEditor() {
				if(tinyMCE.getInstanceById('emailnotify_content') == null) {
					tinyMCE.execCommand('mceAddControl',false,'emailnotify_content');
				}
				else {
					tinyMCE.execCommand('mceRemoveControl',false,'emailnotify_content');
				}
			}
			
			function saveAndContinueEdit() {
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
	}
	
	public function getHeaderText() {
		if(Mage::registry('emailnotify_data') && Mage::registry('emailnotify_data')->getId()) {
			return Mage::helper('emailnotify')->__("Edit Email Address '%s' To Stop Receiving Email",$this->htmlEscape(Mage::registry('emailnotify_data')->getEmailAddress()));
		}
		else {
			return Mage::helper('emailnotify')->__('Add Email Address To Stop Receiving Email');
		}
	}
}
