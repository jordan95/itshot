<?php
class MSA_Emailnotify_Block_Adminhtml_Emailnotify_Renderer_View extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
	public function render(Varien_Object $row) {
		$values = $row->getData($this->getColumn()->getIndex());
		$values_array = explode(',',$values);
                $value = '';
		foreach($values_array as $val) {
			if($val == '22') {
				$value .= 'Newsletter Popup Signup Coupon,';
			}
			else if($val == '20') {
				$value .= 'Product Review Email,';
			}
			else if($val == '14') {
				$value .= 'Past Customer Coupon,';
			}
			else if($val == 'rewards_expire_email_custom_template') {
				$value .= 'Rewards Expiry Notification,';
			}
			else if($val == 'all') {
				$value .= 'Block All Emails From Website,';
			}
		}
		return substr_replace($value,"",-1);
	}
}
?>
