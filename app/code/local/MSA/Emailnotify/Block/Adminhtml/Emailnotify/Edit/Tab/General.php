<?php
class MSA_Emailnotify_Block_Adminhtml_Emailnotify_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form {
	protected function _prepareForm() {
		$data = Mage::registry('emailnotify_data');
		
		if(is_object($data)) $data = $data->getData();
		
		$form = new Varien_Data_Form();
		$fieldset = $form->addFieldset('general',array('legend'=>$this->__('Email Address')));
		
		$fieldset->addField('email_address','text',array(
			'label'=>$this->__('Email Address'),
			'name'=>'email_address',
			'required'=>true,
		));
		$fieldset->addField('email_description','multiselect',array(
			'label'=>$this->__('Email Decription'),
			'name'=>'email_description[]',
			'values'=>MSA_Emailnotify_Model_Templatesname::getOptionArray(),
			'required'=>true,
		));
		$fieldset->addField('status','select',array(
			'label'=>$this->__('Status'),
			'name'=>'status',
			'values'=>MSA_Emailnotify_Model_Status::getOptionArray(),
			'required'=>true,
		));
		$form->setValues($data);
		$this->setForm($form);
		
		return parent::_prepareForm();
	}
}
