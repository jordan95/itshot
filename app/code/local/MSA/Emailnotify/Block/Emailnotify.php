<?php
class MSA_Emailnotify_Block_Emailnotify extends Mage_Core_Block_Template {
	public function _prepareLayout() {
		return parent::_prepareLayout();
	}
	
	public function getEmailnotify() {
		if(!$this->hasData('emailnotify')) {
			$this->setData('emailnotify',Mage::registry('emailnotify'));
		}
		return $this->getData('emailnotify');
	}
}
