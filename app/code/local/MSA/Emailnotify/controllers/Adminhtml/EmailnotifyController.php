<?php
class MSA_Emailnotify_Adminhtml_EmailnotifyController extends Mage_Adminhtml_Controller_action {
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('emailnotify/items')
			->_addBreadcrumb(Mage::helper('emailnotify')->__('Manage Email Address List'),Mage::helper('emailnotify')->__('Manage Email Address List'));
		return $this;
	}
	
	public function indexAction() {
		$this->_initAction()->renderLayout();
	}
	
	public function editAction() {
		$id = $this->getRequest()->getParam('id');
		$model = Mage::getModel('emailnotify/emailnotify')->load($id);
		
		if($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if(!empty($data)) {
				$model->setData($data);
			}
			
			Mage::register('emailnotify_data',$model);
			
			$this->loadLayout();
			$this->_setActiveMenu('emailnotify/items');
			$this->_addBreadcrumb(Mage::helper('emailnotify')->__('Manage Email Address'),Mage::helper('adminhtml')->__('Manage Email Address'));
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock('emailnotify/adminhtml_emailnotify_edit'))
				 ->_addLeft($this->getLayout()->createBlock('emailnotify/adminhtml_emailnotify_edit_tabs'));
			$this->renderLayout();
		}
		else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('emailnotify')->__('Email does not exist'));
			$this->_redirect('*/*/');
		}
	}
	
	public function newAction() {
		$this->_forward('edit');
	}
	
	public function saveAction() {
		if($data = $this->getRequest()->getPost()) {
			$model = Mage::getModel('emailnotify/emailnotify');
			
			if(isset($data['emailnotify'])) {
				$data['emailnotify'] = array_filter(array_map('trim',explode(',',$data['emailnotify'])));
				$data['emailnotify'] = implode(',',$data['emailnotify']);
			}
			
			$ed = "";
			if($data['email_description']) {
				foreach($data['email_description'] as $a) {
					$ed .= $a.",";
				}
				$data['email_description'] = substr_replace($ed,"",-1);
			}
			
			$model->setData($data)->setId($this->getRequest()->getParam('id'));
			
			try {
				if($model->getId()!="") {
					$model->setUpdateTime(now());
				}
				else {
					$model->setCreatedTime(now())->setUpdateTime(now());
				}
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('emailnotify')->__('Email was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);
				
				if($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit',array('id'=>$model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit',array('id'=>$this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('emailnotify')->__('Unable to find Email to save'));
		$this->_redirect('*/*/');
	}
	
	public function deleteAction() {
		if($this->getRequest()->getParam('id') > 0) {
			try {
				$model = Mage::getModel('emailnotify/emailnotify');
				$model->setId($this->getRequest()->getParam('id'))->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('emailnotify')->__('Email was successfully deleted'));
				$this->_redirect('*/*/');
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit',array('id'=>$this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}
	
	public function massDeleteAction() {
		$keywordsIds = $this->getRequest()->getParam('emailnotify');
		if(!is_array($keywordsIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('emailnotify')->__('Please select item(s)'));
		}
		else {
			try {
				foreach($keywordsIds as $keywordsId) {
					$keywords = Mage::getModel('emailnotify/emailnotify')->load($keywordsId);
					$keywords->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(
					Mage::helper('adminhtml')->__(
						'Total of %d record(s) were successfully deleted',count($keywordsIds)
					)
				);
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
	
	public function massStatusAction() {
		$keywordsIds = $this->getRequest()->getParam('emailnotify');
		if(!is_array($keywordsIds)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
		}
		else {
			try {
				foreach($keywordsIds as $keywordsId) {
					$keywords = Mage::getSingleton('emailnotify/emailnotify')
						->load($keywordsId)
						->setStatus($this->getRequest()->getParam('status'))
						->setIsMassupdate(true)
						->setUpdateTime(now())
						->save();
				}
				$this->_getSession()->addSuccess(
					$this->__('Total of %d record(s) were successfully updated',count($keywordsIds))
				);
			}
			catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
	
	public function exportCsvAction() {
		$fileName = 'emailnotify.csv';
		$content = $this->getLayout()->createBlock('emailnotify/adminhtml_emailnotify_grid')->getCsv();
		$this->_sendUploadResponse($fileName,$content);
	}
	
	public function exportXmlAction() {
		$fileName = 'emailnotify.xml';
		$content = $this->getLayout()->createBlock('emailnotify/adminhtml_emailnotify_grid')->getXml();
		$this->_sendUploadResponse($fileName,$content);
	}
	
	protected function _sendUploadResponse($fileName,$content,$contentType='application/octet-stream') {
		$response = $this->getResponse();
		$response->setHeader('HTTP/1.1 200 OK','');
		$response->setHeader('Pragma','public',true);
		$response->setHeader('Cache-Control','must-revalidate, post-check=0, pre-check=0',true);
		$response->setHeader('Content-Disposition','attachment; filename='.$fileName);
		$response->setHeader('Last-Modified',date('r'));
		$response->setHeader('Accept-Ranges','bytes');
		$response->setHeader('Content-Length',strlen($content));
		$response->setHeader('Content-type',$contentType);
		$response->setBody($content);
		$response->sendResponse();
		die;
	}

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/customer/emailnotify');
    }
}
