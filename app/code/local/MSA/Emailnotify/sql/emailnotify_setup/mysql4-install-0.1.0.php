<?php
$installer = $this;
$installer->startSetup();

$installer->run("
CREATE TABLE IF NOT EXISTS {$this->getTable('emailnotify')} (
	`id` int(11) unsigned NOT NULL auto_increment,
	`email_address` varchar(255) NOT NULL default '',
	`email_description` varchar(255) NOT NULL default '',
	`status` smallint(6) NOT NULL default '0',
	`created_time` datetime NULL,
	`update_time` datetime NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

$installer->endSetup();
