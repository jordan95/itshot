<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @package    Mage_Questions
 * @copyright  Copyright (c) 2004-2007 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Questions index controller
 *
 * @category   Mage
 * @package    Mage_Questions
 * @author      Magento Core Team <core@magentocommerce.com>
 */

require_once(Mage::getBaseDir('lib'). DS . 'EmailSES/ses.php');


class Mage_Quotes_IndexController extends Mage_Core_Controller_Front_Action
{

    const XML_PATH_EMAIL_RECIPIENT  = 'quotes/email/recipient_email';
    const XML_PATH_EMAIL_SENDER     = 'quotes/email/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE   = 'quotes/email/email_template';
    const XML_PATH_ENABLED          = 'quotes/quotes/enabled';

	const XML_PATH_SES_AWSACCESSKEY		= 'system/sessettings/aws_access_key';
	const XML_PATH_SES_AWSPRIVATEKEY	= 'system/sessettings/aws_private_key';
	//const XML_PATH_SENDER_EMAIL			= 'store/ident_custom2/email';

	//groups[email][fields][recipient_email][value]
	//groups[ident_custom2][fields][email][value]

    public function preDispatch()
    {
        parent::preDispatch();

        if( !Mage::getStoreConfigFlag(self::XML_PATH_ENABLED) ) {
            $this->norouteAction();
        }
    }

//    public function indexAction()
//    {
//        $this->loadLayout();
//        $this->getLayout()->getBlock('quoteForm')
//            ->setFormAction( Mage::getUrl('*/*/post') );
//
//        $this->_initLayoutMessages('customer/session');
//        $this->renderLayout();
//    }

	//get file MIME type
	protected function get_mime_type($file)
	{
		// our list of mime types
		$mime_types = array(
					"pdf"=>"application/pdf"
					,"exe"=>"application/octet-stream"
					,"zip"=>"application/zip"
					,"docx"=>"application/msword"
					,"doc"=>"application/msword"
					,"xls"=>"application/vnd.ms-excel"
					,"ppt"=>"application/vnd.ms-powerpoint"
					,"gif"=>"image/gif"
					,"png"=>"image/png"
					,"jpeg"=>"image/jpeg"
					,"jpg"=>"image/jpg"
					,"mp3"=>"audio/mpeg"
					,"wav"=>"audio/x-wav"
					,"mpeg"=>"video/mpeg"
					,"mpg"=>"video/mpeg"
					,"mpe"=>"video/mpeg"
					,"mov"=>"video/quicktime"
					,"avi"=>"video/x-msvideo"
					,"3gp"=>"video/3gpp"
					,"css"=>"text/css"
					,"jsc"=>"application/javascript"
					,"js"=>"application/javascript"
					,"php"=>"text/html"
					,"htm"=>"text/html"
					,"html"=>"text/html"
			);
                                $files = explode('.',$file);
		$extension = strtolower(end($files));
		return $mime_types[$extension];
	}

	public function check_upload_file($fieldname,$file)
	{
		//print_r($_FILES[$fieldname]);
		if(is_readable($_FILES[$fieldname]['tmp_name']))
		{
			$allowedExts = array("jpg", "jpeg", "gif", "png");
			$allowedType = array("image/gif", "image/jpeg", "image/png", "image/jpg");
			$image_mime = image_type_to_mime_type(exif_imagetype($_FILES[$fieldname]['tmp_name']));
			$size = $_FILES[$fieldname]["size"];
			//$extension = end(explode(".", $file));
                                                $exts = explode(".", $file);
			$extension = strtolower(end($exts)); //Added strtolower on date 17-08-2015

			//echo "image_mime=>".$image_mime." && Size==>".$size." && Extension=>".$extension;
			if (in_array($image_mime, $allowedType) && $size < 2097152 && in_array($extension, $allowedExts))
			{
				return 1;
			}
			else
			{
				return 0;
			}
	     }
	     else
	     {
			 return 0;
		 }
	}

	//check data and filter for email to ignore
	protected function _isIgnored($post)
	{
		$ignore = false;
		if($post['phone']!="" && $post['phone']=="123456")
		{
			$ignore = true;
		}
		return $ignore;
	}

	//Send custom jewelery requests emails
    protected function _sendmail($post, $customImage1, $customImage2)
    {
        //ignore spam email
        if ($this->_isIgnored($post)) {
            return true;
        }
        $sender_email = Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER);
        $senderEmail = Mage::getStoreConfig('trans_email/ident_' . $sender_email . '/email');
        $receiverEmail = Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT);
        $clientIp = Mage::helper('ipsecurity/customcode')->getip_index();
        
        $bodymail = '<body style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
                                                        <div style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
                                                        <table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
                                                        <tr>
                                                        <td align="center" valign="top" style="padding:20px 0 20px 0">
                                                        <table bgcolor="FFFFFF" cellspacing="0" cellpadding="10" border="0" width="60%" style="border:1px solid #E0E0E0;">';

        $bodymail .= '<tr><td valign="top" width="20%"><b>Name:</b></td><td valign="top" width="80%">' . $post['name'] . '</td></tr>';
        $bodymail .= '<tr><td valign="top"><b>Email:</b></td><td valign="top">' . $post['email'] . '</td></tr>';
        $bodymail .= '<tr><td valign="top"><b>Phone:</b></td><td valign="top">' . $post['phone'] . '</td></tr>';

        $bodymail .= '<tr><td valign="top"><b>Minimum Price Range:</b></td><td valign="top">' . $post['minpricerange'] . '</td></tr>';
        $bodymail .= '<tr><td valign="top"><b>Maximum Price Range:</b></td><td valign="top">' . $post['maxpricerange'] . '</td></tr>';

        if ((isset($post['metal']) && $post['metal'] != "0") && $post['metal'] != "None") {
            $bodymail .= '<tr><td valign="top"><b>Metal:</b></td><td valign="top">' . $post['metal'] . '</td></tr>';
        }
        if (isset($post['stones']) && $post['stones'] != "None") {
            $bodymail .= '<tr><td valign="top"><b>Stones:</b></td><td valign="top">' . $post['stones'] . '</td></tr>';
        }
        if ((isset($post['width']) && $post['width'] != "") && (isset($post['height']) && $post['height'] != "")) {
            $bodymail .= '<tr><td valign="top"><b>Size:</b></td><td valign="top">' . $post['width'] . ' X ' . $post['height'] . '</td></tr>';
        }
        if (isset($post['notes']) && $post['notes'] != "") {
            $bodymail .= '<tr><td valign="top"><b>Notes:</b></td><td valign="top">' . $post['notes'] . '</td></tr>';
        }

        $bodymail .= '</table>
                                                        </td>
                                                        </tr>
                                                        </table>
                                                        </div>
                                                        <p>Client IP: '.$clientIp.'</p>
                                                        </body>';
//		$rootDir = Mage::getBaseDir();
//		include($rootDir."/EmailSES/ses.php");

        /* @var $mailTemplate OpenTechiz_MailLog_Model_SMTPPro_Email_Template */
        $mailTemplate = Mage::getModel('core/email_template');
        $mailTemplate->setSenderName('ItsHot.com');
        $mailTemplate->setSenderEmail($senderEmail);
        $mailTemplate->setReplyTo($post['email']);
        $mailTemplate->setTemplateSubject("ItsHot.com- " . $post['title']);
        $mailTemplate->setTemplateText($bodymail);
        try {

            // add attachment
            if ($customImage1 != "") {
                $mailTemplate->getMail()->createAttachment(
                        file_get_contents(Mage::getBaseDir('media') . '/upload/' . Varien_File_Uploader::getCorrectFileName($customImage1)), //location of file
                        Zend_Mime::TYPE_OCTETSTREAM, Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, $customImage1
                );
            }
            if ($customImage2 != "") {
                $mailTemplate->getMail()->createAttachment(
                        file_get_contents(Mage::getBaseDir('media') . '/upload/' . Varien_File_Uploader::getCorrectFileName($customImage2)), //location of file
                        Zend_Mime::TYPE_OCTETSTREAM, Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, $customImage2
                );
            }

            $mailTemplate->send($receiverEmail, "ItsHot.com- " . $post['title']);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    //Custom Watch request
    public function postAction()
    {
        $post = $this->getRequest()->getPost();

        if ($post) {
            try {
                $customImage1 = "";
                $customImage2 = "";
                if (isset($_FILES['customImage1']['name']) && $_FILES['customImage1']['name'] != '') {
                    $customImage1 = $_FILES['customImage1']['name'];
                    $fieldname1 = 'customImage1';
                    if ($this->check_upload_file($fieldname1, $customImage1)) {
                        $uploader = new Varien_File_Uploader('customImage1');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage1']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage1 = $_FILES['customImage1']['name'];
                    } else {
                        $_SESSION["custom-watches"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }
                if (isset($_FILES['customImage2']['name']) && $_FILES['customImage2']['name'] != '') {
                    $customImage2 = $_FILES['customImage2']['name'];
                    $fieldname2 = 'customImage2';
                    //echo$contentType1 = $this->check_upload_file($fieldname,$customImage1);exit;
                    if ($this->check_upload_file($fieldname2, $customImage2)) {
                        $uploader = new Varien_File_Uploader('customImage2');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        //$path = Mage::getBaseDir('media') . DS;
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage2']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage2 = $_FILES['customImage2']['name'];
                    } else {
                        $_SESSION["custom-watches"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }
                $messageSuccess = $this->getMessageSuccess();
                $this->sendRequest($post, $customImage1, $customImage2, $messageSuccess, 'custom-watches');
                Mage::getSingleton('core/session')->addSuccess($messageSuccess);
                $this->_redirectReferer();
                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Unable to submit your request. Please, try again later.'));
                $this->_redirectReferer();
                return;
            }
        } else {
            $this->_redirectReferer();
        }
    }

    //custom chain request
	public function post1Action()
    {
        $post = $this->getRequest()->getPost();

        if ($post) {
            try {
                $customImage1 = "";
                $customImage2 = "";

                if (isset($_FILES['customImage1']['name']) && $_FILES['customImage1']['name'] != '') {
                    $customImage1 = $_FILES['customImage1']['name'];
                    $fieldname1 = 'customImage1';
                    if ($this->check_upload_file($fieldname1, $customImage1)) {
                        $uploader = new Varien_File_Uploader('customImage1');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage1']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage1 = $_FILES['customImage1']['name'];
                    } else {
                        $_SESSION["custom-chains"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }

                if (isset($_FILES['customImage2']['name']) && $_FILES['customImage2']['name'] != '') {
                    $customImage2 = $_FILES['customImage2']['name'];
                    $fieldname2 = 'customImage2';
                    if ($this->check_upload_file($fieldname2, $customImage2)) {
                        $uploader = new Varien_File_Uploader('customImage2');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage2']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage2 = $_FILES['customImage2']['name'];
                    } else {
                        $_SESSION["custom-chains"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }
                $messageSuccess = $this->getMessageSuccess();
                $this->sendRequest($post, $customImage1, $customImage2, $messageSuccess, 'custom-chains');
                Mage::getSingleton('core/session')->addSuccess($messageSuccess);
                $this->_redirectReferer();
                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Unable to submit your request. Please, try again later.'));
                $this->_redirectReferer();
                return;
            }
        } else {
            $this->_redirectReferer();
        }
    }

	public function post2Action()
        {
        $post = $this->getRequest()->getPost();
        if ($post) {
            try {
                $customImage1 = "";
                $customImage2 = "";
                if (isset($_FILES['customImage1']['name']) && $_FILES['customImage1']['name'] != '') {
                    $customImage1 = $_FILES['customImage1']['name'];
                    $fieldname1 = 'customImage1';
                    if ($this->check_upload_file($fieldname1, $customImage1)) {
                        $uploader = new Varien_File_Uploader('customImage1');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage1']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage1 = $_FILES['customImage1']['name'];
                    } else {
                        $_SESSION["custom-earrings"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }
                if (isset($_FILES['customImage2']['name']) && $_FILES['customImage2']['name'] != '') {
                    $customImage2 = $_FILES['customImage2']['name'];
                    $fieldname2 = 'customImage2';
                    if ($this->check_upload_file($fieldname2, $customImage2)) {
                        $uploader = new Varien_File_Uploader('customImage2');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage2']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage2 = $_FILES['customImage2']['name'];
                    } else {
                        $_SESSION["custom-earrings"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }
                $messageSuccess = $this->getMessageSuccess();
                $this->sendRequest($post, $customImage1, $customImage2, $messageSuccess, 'custom-earrings');
                Mage::getSingleton('core/session')->addSuccess($messageSuccess);
                $this->_redirectReferer();
                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Unable to submit your request. Please, try again later.'));
                $this->_redirectReferer();
                return;
            }
        } else {
            $this->_redirectReferer();
        }
    }

    //Custom Rings Request
	public function post3Action()
        {
        $post = $this->getRequest()->getPost();
        if ($post) {
            try {
                $customImage1 = "";
                $customImage2 = "";
                if (isset($_FILES['customImage1']['name']) && $_FILES['customImage1']['name'] != '') {
                    $customImage1 = $_FILES['customImage1']['name'];
                    $fieldname1 = 'customImage1';
                    if ($this->check_upload_file($fieldname1, $customImage1)) {
                        $uploader = new Varien_File_Uploader('customImage1');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage1']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage1 = $_FILES['customImage1']['name'];
                    } else {
                        $_SESSION["custom-rings"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }
                if (isset($_FILES['customImage2']['name']) && $_FILES['customImage2']['name'] != '') {
                    $customImage2 = $_FILES['customImage2']['name'];
                    $fieldname2 = 'customImage2';
                    if ($this->check_upload_file($fieldname2, $customImage2)) {
                        $uploader = new Varien_File_Uploader('customImage2');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage2']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage2 = $_FILES['customImage2']['name'];
                    } else {
                        $_SESSION["custom-rings"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }
                $messageSuccess = $this->getMessageSuccess();
                $this->sendRequest($post, $customImage1, $customImage2, $messageSuccess, 'custom-rings');
                Mage::getSingleton('core/session')->addSuccess($messageSuccess);
                $this->_redirectReferer();
                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Unable to submit your request. Please, try again later.'));
                $this->_redirectReferer();
                return;
            }
        } else {
            $this->_redirectReferer();
        }
    }

    //Custom Jewelry Request
	public function post4Action()
    {
        $post = $this->getRequest()->getPost();
        if ($post) {
            try {
                $customImage1 = "";
                $customImage2 = "";
                if (isset($_FILES['customImage1']['name']) && $_FILES['customImage1']['name'] != '') {
                    $customImage1 = $_FILES['customImage1']['name'];
                    $fieldname1 = 'customImage1';
                    if ($this->check_upload_file($fieldname1, $customImage1)) {
                        $uploader = new Varien_File_Uploader('customImage1');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage1']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage1 = $_FILES['customImage1']['name'];
                    } else {
                        $_SESSION["custom-jewelry"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }
                if (isset($_FILES['customImage2']['name']) && $_FILES['customImage2']['name'] != '') {
                    $customImage2 = $_FILES['customImage2']['name'];
                    $fieldname2 = 'customImage2';
                    if ($this->check_upload_file($fieldname2, $customImage2)) {
                        $uploader = new Varien_File_Uploader('customImage2');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage2']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage2 = $_FILES['customImage2']['name'];
                    } else {
                        $_SESSION["custom-jewelry"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }
                $messageSuccess = $this->getMessageSuccess();
                $this->sendRequest($post, $customImage1, $customImage2, $messageSuccess);
                Mage::getSingleton('core/session')->addSuccess($messageSuccess);
                $this->_redirectReferer();
                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Unable to submit your request. Please, try again later.'));
                Mage::log('#1' . $e->getMessage(), null, 'quotesErrors.log');
                $this->_redirectReferer();
                return;
            }
        } else {
            $this->_redirectReferer();
        }
    }
    
    protected function getMessageSuccess() {
        return Mage::helper('quotes')->__('Thank you! We have received your request and one of our salesmen will contact you within 1 business day.');
    }

    protected function sendRequest($post, $customImage1, $customImage2, &$messageSuccess, $sessionKey = 'custom-jewelry')
    {

        $canSendMail = Mage::helper('odoo')->isEnabledSendEmail();

        try {
            // Submit ticket to Odoo
            $this->submitTicket($post, $customImage1, $customImage2);
        } catch (Exception $exc) {
            $messageSuccess = Mage::helper('quotes')->__('There were a problem with our CRM system, your information will be sent to our admin email');
        }

        if ($canSendMail) {
            // Send email if submit ticket Odoo fail or Odoo disabled
            $res = $this->_sendmail($post, $customImage1, $customImage2);
            if ($res) {
                if(isset($_SESSION[$sessionKey])) {
                    unset($_SESSION[$sessionKey]);
                }
            } else {
                Mage::throwException('Unable to submit your request. Please, try again later.');
            }
        }
    }

    protected function submitTicket($post, $customImage1, $customImage2)
    {
        $ticketData = [
            "customer_name" => $post["name"],
            "customer_phone" => $post["phone"],
            "customer_email" => $post["email"],
            "message" => trim($post["notes"]),
            "ticket_subject" => "ItsHot.com- " . $post['title'],
            "ticket_minprice" => floatval($post["minpricerange"]),
            "ticket_maxprice" => floatval($post["maxpricerange"]),
            "ticket_metal" => $post["metal"],
            "team_id" => "Custom Orders",
        ];
        if(isset($post["stones"])) {
            $ticketData['ticket_stone'] = trim($post['stones']);
        }
        if(isset($post["height"])) {
            $ticketData['ticket_height'] = floatval($post["height"]);
            $ticketData['size_height'] = floatval($post["height"]);
        }
        if(isset($post["width"])) {
            $ticketData['ticket_width'] = floatval($post["width"]);
            $ticketData['size_width'] = floatval($post["width"]);
        }

        Mage::getModel('odoo/api_odoo')->submitTicket($ticketData,$customImage1, $customImage2);
    }

    public function post50Action()
    {
        $post = $this->getRequest()->getPost();

        if ( $post ) {
            try {
				$mailTemplate = Mage::getModel('core/email_template') ->loadByCode('custom-product-request');
				//$a=$mailTemplate->loadByCode('Watch Template');
			/*	if (isset($_FILES['customImage1']['name']) && $_FILES['customImage1']['name'] != '') {

					$uploader = new Varien_File_Uploader('customImage1');
					$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					$uploader->setFilesDispersion(false);
					$path = Mage::getBaseDir('media') . DS . 'upload' . DS;
					$logoName = $_FILES['customImage1']['name'];
					$uploader->save($path, $logoName);
					$logo = $path.$logoName;


				$fileContents = file_get_contents(Mage::getBaseDir('media').'/upload/'.$_FILES['customImage1']['name']);
		// echo get_class($mailTemplate->getMail());
//exit;

				$attachment = $mailTemplate->getMail()->createAttachment($fileContents);
	$attachment->type        = 'image/jpeg';
				$attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
				$attachment->encoding    = Zend_Mime::ENCODING_BASE64;
				$attachment->filename = $_FILES['customImage1']['name'];

			  }*/
if (isset($_FILES['customImage1'])) {
            $file=$_FILES['customImage1']['tmp_name'];
            $name=$_FILES['customImage1']['name'];

            $myImage = fopen($file, 'r');

            $at = $mailTemplate->getMail()->createAttachment($myImage);
            $at->type        = 'image/jpeg';
            $at->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
            $at->encoding    = Zend_Mime::ENCODING_BASE64;
            $at->filename    = $name;

        }
			  if (isset($_FILES['customImage2']['name']) && $_FILES['customImage2']['name'] != '') {

					$uploader = new Varien_File_Uploader('customImage2');
					$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					$uploader->setFilesDispersion(false);
					//$path = Mage::getBaseDir('media') . DS;
					$path = Mage::getBaseDir('media') . DS . 'upload' . DS;
					$logoName = $_FILES['customImage2']['name'];
					$uploader->save($path, $logoName);
					$logo = $path.$logoName;

//				$fileContents2 = file_get_contents(Mage::getBaseDir('media').'/upload/'.$_FILES['customImage2']['name']); /*(Here put the filename with full path of file, which have to be send)*/
//
//				$attachment2 = $mailTemplate->getMail()->createAttachment($fileContents2);
//				$attachment2->filename = $_FILES['customImage2']['name'];

			  }
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->sendTransactional(
                        $mailTemplate->getTemplateId(),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
						//$_POST['email'],
						Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
                        null,
                        array('data' => $postObject)
                    );
				Mage::getSingleton('core/session')->addSuccess(Mage::helper('quotes')->__('Thank you. We received your request and one of our salesmen will contact you within 1 business day.'));
                $this->_redirect('custom-jewelry.aspx');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Unable to submit your request. Please, try again later.'));
                $this->_redirect('custom-jewelry.aspx');
                return;
            }
        } else {
            $this->_redirect('custom-jewelry.aspx');
        }
    }


	//custom Pendents request
	public function post5Action()
    {
        $post = $this->getRequest()->getPost();
        if ($post) {
            try {
                $customImage1 = "";
                $customImage2 = "";
                if (isset($_FILES['customImage1']['name']) && $_FILES['customImage1']['name'] != '') {
                    $customImage1 = $_FILES['customImage1']['name'];
                    $fieldname1 = 'customImage1';
                    if ($this->check_upload_file($fieldname1, $customImage1)) {
                        $uploader = new Varien_File_Uploader('customImage1');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        //$path = Mage::getBaseDir('media') . DS;
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage1']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage1 = $_FILES['customImage1']['name'];
                    } else {
                        $_SESSION["custom-pendants"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }

                if (isset($_FILES['customImage2']['name']) && $_FILES['customImage2']['name'] != '') {
                    $customImage2 = $_FILES['customImage2']['name'];
                    $fieldname2 = 'customImage2';
                    if ($this->check_upload_file($fieldname2, $customImage2)) {
                        $uploader = new Varien_File_Uploader('customImage2');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        //$path = Mage::getBaseDir('media') . DS;
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage2']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage2 = $_FILES['customImage2']['name'];
                    } else {
                        $_SESSION["custom-pendants"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }
                $messageSuccess = $this->getMessageSuccess();
                $this->sendRequest($post, $customImage1, $customImage2, $messageSuccess, 'custom-pendants');
                Mage::getSingleton('core/session')->addSuccess($messageSuccess);
                $this->_redirectReferer();
                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Unable to submit your request. Please, try again later.'));
                $this->_redirectReferer();
                return;
            }
        } else {
            $this->_redirectReferer();
        }
    }

    //Custom Bracelets Request
	public function post6Action()
        {
        $post = $this->getRequest()->getPost();
        if ($post) {
            try {
                $customImage1 = "";
                $customImage2 = "";
                if (isset($_FILES['customImage1']['name']) && $_FILES['customImage1']['name'] != '') {
                    $customImage1 = $_FILES['customImage1']['name'];
                    $fieldname1 = 'customImage1';
                    if ($this->check_upload_file($fieldname1, $customImage1)) {
                        $uploader = new Varien_File_Uploader('customImage1');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage1']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage1 = $_FILES['customImage1']['name'];
                    } else {
                        $_SESSION["custom-bracelets"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }

                if (isset($_FILES['customImage2']['name']) && $_FILES['customImage2']['name'] != '') {
                    $customImage2 = $_FILES['customImage2']['name'];
                    $fieldname2 = 'customImage2';
                    if ($this->check_upload_file($fieldname2, $customImage2)) {
                        $uploader = new Varien_File_Uploader('customImage2');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'upload' . DS;
                        $logoName = $_FILES['customImage2']['name'];
                        $uploader->save($path, $logoName);
                        $logo = $path . $logoName;
                        $customImage2 = $_FILES['customImage2']['name'];
                    } else {
                        $_SESSION["custom-bracelets"] = $post;
                        Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Invalid file type. Please check the file Size & Type(2MB file size of JPG/JPEG, PNG, GIF format are supported) and try again.'));
                        $this->_redirectReferer();
                        return;
                    }
                }
                $messageSuccess = $this->getMessageSuccess();
                $this->sendRequest($post, $customImage1, $customImage2, $messageSuccess, 'custom-bracelets');
                Mage::getSingleton('core/session')->addSuccess($messageSuccess);
                $this->_redirectReferer();
                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Unable to submit your request. Please, try again later.'));
                $this->_redirectReferer();
                return;
            }
        } else {
            $this->_redirectReferer();
        }
    }

    //sell Gold request
	public function post7Action()
    {
        $formRedirect = 'jewelry/sell-your-gold';

        $post = $this->getRequest()->getPost();

        if ( $post )
		{
            try
			{

				if(count($post)>0)
				{
                                                                                $error = 0;
					if($post['firstname']=='' || $post['lastname']=='' || $post['address1']=='' || $post['city']=='' || $post['state']=='' || $post['zip']=='' || $post['phone']=='' || $post['email']=='')
					{
						Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Please enter mandatory fields.'));
						$error = 1;


					}
					elseif($post['email']!='' && !filter_var($post['email'], FILTER_VALIDATE_EMAIL))
					{
						Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Please enter mandatory fields.'));
						$error = 1;
					}
					if($error==1)
					{
						//keep post data in session and redirect user
						$_SESSION["SELL_GOLD_POST_DATA"] = $post;
						$this->_redirect($formRedirect);
						return;
				   }
				}


				$mailTemplate = Mage::getModel('core/email_template') ->loadByCode('sells-gold');

				$postObject = new Varien_Object();
                $postObject->setData($post);

                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->sendTransactional(
                        $mailTemplate->getTemplateId(),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                        //$_POST['email'],
						Mage::getStoreConfig("trans_email/ident_custom3/email"),
                        null,
                        array('data' => $postObject)
                    );
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('quotes')->__('Thank you. We received your request and one of our salesmen will contact you within 1 business day.'));
                $this->_redirect($formRedirect);
                return;
            }
			catch (Exception $e)
			{
                Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Unable to submit your request. Please, try again later.'));
                $this->_redirect($formRedirect);
                return;
            }
        }
		else
		{
            $this->_redirect($formRedirect);
        }
    }

	//Sell Gold- What We Buy request
	public function post8Action()
    {
        $post = $this->getRequest()->getPost();

        if ( $post ) {
            try {
				 $mailTemplate = Mage::getModel('core/email_template') ->loadByCode('sells-gold');

				 //$a=$mailTemplate->loadByCode('Watch Template');
				// print_r($a);
				// exit;


                $postObject = new Varien_Object();
                $postObject->setData($post);


                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->sendTransactional(
                        $mailTemplate->getTemplateId(),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                        //Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
						Mage::getStoreConfig("trans_email/ident_custom3/email"),
                        null,
                        array('data' => $postObject)
                    );

             Mage::getSingleton('core/session')->addSuccess(Mage::helper('quotes')->__('Thank you. We received your request and one of our salesmen will contact you within 1 business day.'));
                $this->_redirect('selling-gold/what-we-buy.aspx');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Unable to submit your request. Please, try again later.'));
                $this->_redirect('selling-gold/what-we-buy.aspx');
                return;
            }
        } else {
            $this->_redirect('selling-gold/what-we-buy.aspx');
        }
    }

	//Sell Gold- What We pay request
	public function post9Action()
    {
        $post = $this->getRequest()->getPost();

        if ( $post ) {
            try {
				 $mailTemplate = Mage::getModel('core/email_template') ->loadByCode('sells-gold');

				$postObject = new Varien_Object();
                $postObject->setData($post);


                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->sendTransactional(
                        $mailTemplate->getTemplateId(),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                        //Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
						Mage::getStoreConfig("trans_email/ident_custom3/email"),
                        null,
                        array('data' => $postObject)
                    );

                 Mage::getSingleton('core/session')->addSuccess(Mage::helper('quotes')->__('Thank you. We received your request and one of our salesmen will contact you within 1 business day.'));
                $this->_redirect('selling-gold/what-we-pay.aspx');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Unable to submit your request. Please, try again later.'));
                $this->_redirect('selling-gold/what-we-pay.aspx');
                return;
            }
        } else {
            $this->_redirect('selling-gold/what-we-pay.aspx');
        }
    }

	//Sell Gold: FAQs request
	public function post10Action()
    {
        $post = $this->getRequest()->getPost();

        if ( $post ) {
            try {
				 $mailTemplate = Mage::getModel('core/email_template') ->loadByCode('sells-gold');

				$postObject = new Varien_Object();
                $postObject->setData($post);


                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->sendTransactional(
                        $mailTemplate->getTemplateId(),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                        //Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
						Mage::getStoreConfig("trans_email/ident_custom3/email"),
                        null,
                        array('data' => $postObject)
                    );

                  Mage::getSingleton('core/session')->addSuccess(Mage::helper('quotes')->__('Thank you. We received your request and one of our salesmen will contact you within 1 business day.'));
                $this->_redirect('selling-gold/faq.aspx');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('quotes')->__('Unable to submit your request. Please, try again later.'));
                $this->_redirect('selling-gold/faq.aspx');
                return;
            }
        } else {
            $this->_redirect('selling-gold/faq.aspx');
        }
	}

	private function validateRecaptcha($response)
    {
        if (!extension_loaded('curl')) {
            return null;
        }

        $curl = new Varien_Http_Adapter_Curl();
        $curl->setConfig(array(
            'timeout'   => 2
        ));
        $post = ['secret' => Mage::helper('medialounge_customer')->getRecaptchaSecretKey(),
            'response' => $response
        ];
        $str = '';
        foreach ($post as $key=>$value) {
            if ($str !='') $str .= '&';
            $str .= sprintf('%s=%s', $key, urlencode($value));
        }

        $curl->write(Zend_Http_Client::POST, 'https://www.google.com/recaptcha/api/siteverify', '1.1', ['Content-Type: application/x-www-form-urlencoded; charset=utf-8', 'Content-Length: ' . strlen($str)], $str);
        $data = $curl->read();
        if ($data === false) {
            return false;
        }
        $data = preg_split('/^\r?$/m', $data, 2);
        $response = Mage::helper('core')->jsonDecode(trim($data[1]));
        $curl->close();

        if (isset($response['success']) && $response['success']) {
            return true;
        }

        return false;
    }
}
