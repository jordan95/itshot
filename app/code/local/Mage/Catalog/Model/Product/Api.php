<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog product api
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Model_Product_Api extends Mage_Catalog_Model_Api_Resource
{
    protected $_filtersMap = array(
        'product_id' => 'entity_id',
        'set'        => 'attribute_set_id',
        'type'       => 'type_id'
    );

    protected $_defaultProductAttributeList = array(
        'type_id',
        'category_ids',
        'website_ids',
        'name',
        'description',
        'short_description',
        'sku',
        'weight',
        'status',
        'url_key',
        'url_path',
        'visibility',
        'has_options',
        'gift_message_available',
        'price',
        'special_price',
        'special_from_date',
        'special_to_date',
        'tax_class_id',
        'tier_price',
        'meta_title',
        'meta_keyword',
        'meta_description',
        'custom_design',
        'custom_layout_update',
        'options_container',
        'image_label',
        'small_image_label',
        'thumbnail_label',
        'created_at',
        'updated_at'
    );

    public function __construct()
    {
        $this->_storeIdSessionField = 'product_store_id';
        $this->_ignoredAttributeTypes[] = 'gallery';
        $this->_ignoredAttributeTypes[] = 'media_image';
    }

    /**
     * Retrieve list of products with basic info (id, sku, type, set, name)
     *
     * @param null|object|array $filters
     * @param string|int $store
     * @return array
     */
    public function items($filters = null, $store = null, $identifier = null)
    {
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		
		/* New Added by Ravinesh(Onsis) ---------------------------------------------- */		
        if($identifier != null)
        {
			$result = array();
			if($identifier=="all")
			{
				$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
				$sSql = "SELECT entity_id, sku FROM ".$tablePrefix."catalog_product_entity ORDER BY entity_id DESC";
				$sSqlRes = $conn->fetchAll($sSql);
				foreach($sSqlRes AS $productArr)
				{
					$result[] = array("product_id"=>$productArr["entity_id"],"sku"=>$productArr["sku"]);
				}
			}
			return $result;
		}//---------------------------------------------------------------------//
		
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addStoreFilter($this->_getStoreId($store))
            ->addAttributeToSelect('name');

        /** @var $apiHelper Mage_Api_Helper_Data */
        $apiHelper = Mage::helper('api');
        $filters = $apiHelper->parseFilters($filters, $this->_filtersMap);
        //New Added by Ravinesh(Onsis) : If condition:-----------------// 
        if (is_array($filters)) {
			try {
				foreach ($filters as $field => $value) {
					//New Added by Ravinesh(Onsis) : If condition:-----------------// 
					if (isset($this->_filtersMap[$field])) {
                        $field = $this->_filtersMap[$field];
                    }
					$collection->addFieldToFilter($field, $value);
				}
			} catch (Mage_Core_Exception $e) {
				$this->_fault('filters_invalid', $e->getMessage());
			}
		}
		
        $result = array();
        
        //New Added by Ravinesh(Onsis) : If condition:-----------------//        
        if($identifier != null)
        {
			if($identifier=="id")
			{
				foreach ($collection as $product)
				{
					//$result[] = array('product_id' => $product->getId());
					$result[] = $product->getId();
				}
			}
			else if($identifier=="sku")
			{
				foreach ($collection as $product)
				{
					//$result[] = array('sku' => $product->getSku());
					$result[] = $product->getSku();
				}	
			}
			return $result;
		}//----------------------------------------------------//
		
		
        foreach ($collection as $product) {
            $result[] = array(
                'product_id' => $product->getId(),
                'sku'        => $product->getSku(),
                'name'       => $product->getName(),
                'set'        => $product->getAttributeSetId(),
                'type'       => $product->getTypeId(),
                'category_ids' => $product->getCategoryIds(),
                'website_ids'  => $product->getWebsiteIds()
            );
        }
        return $result;
    }

    /**
     * Retrieve product info
     *
     * @param int|string $productId
     * @param string|int $store
     * @param array      $attributes
     * @param string     $identifierType
     * @return array
     */
    public function info($productId, $store = null, $attributes = null, $identifierType = null, $exists=null)
    {
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
        // make sku flag case-insensitive
        if (!empty($identifierType)) {
            $identifierType = strtolower($identifierType);
        }
			$existsRes = array();
			/* New Added by Ravinesh(Onsis) ------------------------------------------------------ */
			//add exists param and check product existance check
			if($exists!=null && ($exists=="exists" || $exists=="url_key"))
			{
				$existsRes[] = $this->_getCustomInfo($productId, $identifierType, $exists);
					$textmsg = "Date=>".date("Y-m-d@H:i:s")."/info/ existsRes=>".implode('|',$existsRes)." "."\n";
					file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);
				return $existsRes;
			}
			else if($exists!=null && ($exists=="exists" || $exists=="custom_url_key"))
			{
				$existsRes[] = $this->_getCustomInfo($productId, $identifierType, $exists);
					$textmsg = "Date=>".date("Y-m-d@H:i:s")."/info/ custom_url_key=>".implode('|',$existsRes)." "."\n";
					file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);
				return $existsRes;
			}
			else if($exists!=null && $exists=="upload_video")
			{
				$existsRes[] = $this->_uploadVideoFile($productId, $identifierType);
					$textmsg = "Date=>".date("Y-m-d@H:i:s")."/info/ upload_video=>".implode('|',$existsRes)." "."\n";
					file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);
				return $existsRes;
			}
			else if($exists!=null && $exists=="price")
			{
				$priceRes[] = $this->_getCustomInfo($productId, $identifierType, $exists);
					$textmsg = "Date=>".date("Y-m-d@H:i:s")."/info/ price=>".implode('|',$existsRes)." "."\n";
					file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);
				return $priceRes;
			}//-----------------------------------------------------------------------------------//
		
        $product = $this->_getProduct($productId, $store, $identifierType);
		
		//New Added by Ravinesh(Onsis)--------------------//
		if (!$product->getId())
		{
            $result = '';
        }
		else
		{
			//Made some changes here
			$result = array( // Basic product data
				'product_id' => $product->getId(),
				'sku'        => $product->getSku(),
				'set'        => $product->getAttributeSetId(),
				'type'       => $product->getTypeId(),
				'categories' => $product->getCategoryIds(),
				'websites'   => $product->getWebsiteIds(),
					'producturl' 	=>   Mage::getBaseUrl().$product->getUrlPath(),			 
					'imageurl'      => $product->getImageUrl(),
					'smallimageurl' => $product->getSmallImageUrl(),
					'thumburl'      => $product->getThumbnailUrl()
			
			);	/* Last 4 Fields added here.	*/
		
			/**			
			 * @New Added by Ravinesh(Onsis):----------------------------------------------//
			 * @Description: Get product video file name and add in return result array.
			 **/
			/* this is commented by Ankush at 13 March 2018		
			$video_link 		= "";
			//$videoProductObj	= Mage::getModel('catalog/product_video');
			$resVideo			= $this->getvideo($product->getId());	//array: 'video_url'
			$textmsg = "Date=>".date("Y-m-d@H:i:s")."/info/ getvideo=>".implode('|',$resVideo)." "."\n";
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);
				
			if(isset($resVideo['video_url']) && $resVideo['video_url']!='')
			{
				$video_link	= $resVideo['video_url'];
			}
			$result['video_link'] = $video_link;
			//-------------------------------------------//
		    */
			foreach ($product->getTypeInstance(true)->getEditableAttributes($product) as $attribute) {
				if ($this->_isAllowedAttribute($attribute, $attributes)) {
					$result[$attribute->getAttributeCode()] = $product->getData(
																	$attribute->getAttributeCode());
				}
			}
			//New Added by Ravinesh(Onsis):----------------------------------------------//
			$optionArray = array();
			$options = $product->getOptions();
				$textmsg = "Date=>".date("Y-m-d@H:i:s")."/info/ count-getOptions=>".count($options)." "."\n";
				file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);
			foreach($options as $optionKey => $option)
			{
				// add the option data to the array
				$optionArray[$optionKey] = $option->getData();
					$textmsg = "Date=>".date("Y-m-d@H:i:s")."/info/ optionKey=> ".$optionKey." option-getData=>".implode('|',$option->getData())." "."\n";
					file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);
				$optionArray[$optionKey]['values'] = array();

				// remove the option and product ids since these
				// can/will change on import
				//unset($optionArray[$optionKey]['option_id']);
				//unset($optionArray[$optionKey]['product_id']);

				// loop over any values for the option
				foreach($option->getValues() as $valueKey => $value)
				{
					// add the value to the option
					$optionArray[$optionKey]['values'][$valueKey] = $value->getData();

					// remove ids from the array
					//unset($optionArray[$optionKey]['values'][$valueKey]['option_type_id']);
					//unset($optionArray[$optionKey]['values'][$valueKey]['option_id']);
				}
			}
			$result['optiontable']= json_encode($optionArray);
			//---------------------------------------------------------------------//
		}
		
        return $result;
    }

    /**
     * Create new product.
     *
     * @param string $type
     * @param int $set
     * @param string $sku
     * @param array $productData
     * @param string $store
     * @return array
     */
    public function create($type, $set, $sku, $productData, $store = null)
    {				
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		
        if (!$type || !$set || !$sku) {
            $this->_fault('data_invalid');
        }

        $this->_checkProductTypeExists($type);
        $this->_checkProductAttributeSet($set);

        /** @var $product Mage_Catalog_Model_Product */
        $product = Mage::getModel('catalog/product');
        $product->setStoreId($this->_getStoreId($store))
            ->setAttributeSetId($set)
            ->setTypeId($type)
            ->setSku($sku);
		
		/* New Added by Ravinesh(Onsis) */
		if (isset($productData['website_ids']) && is_array($productData['website_ids'])) {
            $product->setWebsiteIds($productData['website_ids']);
        }
        
        if (!isset($productData['stock_data']) || !is_array($productData['stock_data'])) {
            //Set default stock_data if not exist in product data
            $product->setStockData(array('use_config_manage_stock' => 0));
        }

        foreach ($product->getMediaAttributes() as $mediaAttribute) {
            $mediaAttrCode = $mediaAttribute->getAttributeCode();
            $product->setData($mediaAttrCode, 'no_selection');
        }
        
		/* New Added by Ravinesh(Onsis) */
		foreach ($product->getTypeInstance(true)->getEditableAttributes($product) as $attribute) {
            if ($this->_isAllowedAttribute($attribute)
                && isset($productData[$attribute->getAttributeCode()])) {
                $product->setData(
                    $attribute->getAttributeCode(),
                    $productData[$attribute->getAttributeCode()]
                );
            }
        }
        
        $this->_prepareDataForSave($product, $productData);
		$arrayOption = array();		/* New Added by Ravinesh(Onsis) */
		
		/* New Added by Ravinesh(Onsis) */
		/**
		 * For Creating dropdown,select,multiselect,radio type of custom option
		 */
                if (isset($productData['product_options']) && isset($productData['product_options']['value'])){
                    foreach($productData['product_options']['value'] as $key=>$value) {
			$firtsval=$productData['product_options']['childval'][$value];
			$secondval=$value;
			$thirdval=$productData['product_options']['type'][$key];
			$fourthval=$productData['product_options']['isrequire'][$key];
			$fifthval=$productData['product_options']['sort'][$key];
			$arrayOption[] = $this->setCustomOption($firtsval, $secondval, $thirdval,$fourthval,$fifthval);
                    }
                }
		
        try {
            /**
             * @todo implement full validation process with errors returning which are ignoring now
             * @todo see Mage_Catalog_Model_Product::validate()
             */
            if (is_array($errors = $product->validate())) {
                $strErrors = array();
                foreach($errors as $code => $error) {
                    if ($error === true) {
                        $error = Mage::helper('catalog')->__('Attribute "%s" is invalid.', $code);
                    }
                    $strErrors[] = $error;
                }
                $this->_fault('data_invalid', implode("\n", $strErrors));
            }
            
            /* New Added by Ravinesh(Onsis) ------------------------------ */
            //Set default special from date for price. 
			//$specialFromDate = date("Y-m-d 00:00:00", (time()-86400));//05/8/12
			$specialFromDate = "1999-11-30 00:00:00";
			$product->setSpecialFromDate($specialFromDate);
			
			//Save product brand
			if(isset($productData["c2c_brand"]) && $productData["c2c_brand"]!="")
			{
				$brand =  $this->_selectBrand($productData["c2c_brand"]);
				if($brand!=0)
				{
					$product->setBrand($brand);
				}
			}
			//add actual stock and available stock fields
			$product->setPopularity(0);
			$product->setActualStockAvailable(0);
			/*	----------------------------------------------	*/
			
            $product->save();
            
            /* New Added by Ravinesh(Onsis) --------------------------- */
            $insertid=$product->getId();
				$textmsg = "Date=>".date("Y-m-d@H:i:s")."/create/ insertid=>".$insertid." "."\n";
				file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);
			//save product video information					 
			/* if($productData['video_link']!='')
			{
				//Not wokring Now
				
				$videoproductobj = Mage::getModel('catalog/product_video');
				$videoproductobj->setProductId($insertid)
								->setVideoUrl($productData['video_link'])
								->save();
				
				$status = $this->savevideo($insertid, $productData['video_link']);
				
				$textmsg = "Date=>".date("Y-m-d@H:i:s")."/create/ savevideo=>".$status." "."\n";
				file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);
			}
			*/
			/*
			 * Create Date : 18-08-2017
			 * -------------:Create Google Feeds Label Values: -------[START]-------------
			 * */
						
				// Get Products Google Feed Data from Products Attributes
				$currProdID = $insertid;
				$custom_label_0 = " ";
				$custom_label_1 = " ";
				$custom_label_2 = " ";
				$custom_label_3 = " ";
				$custom_label_4 = " ";				
				
				$customAttributeLabel_322 = $this->getAttributeId('c2c_gemstoneclarity1');
				$customAttributeLabel_323 = $this->getAttributeId('c2c_gemstonecolor1');
				$customAttributeLabel_324 = $this->getAttributeId('c2c_numberofstonesingemstone1');
				$customAttributeLabel_325 = $this->getAttributeId('c2c_gemstonesettingtype1');
				$customAttributeLabel_326 = $this->getAttributeId('c2c_gemstoneshape1');
				
					
				$custom_label_0 = $this->attributevarchar($customAttributeLabel_322, $currProdID);
				$custom_label_1 = $this->attributevarchar($customAttributeLabel_323, $currProdID);
				$custom_label_2 = $this->attributevarchar($customAttributeLabel_324, $currProdID);
				$custom_label_3 = $this->attributevarchar($customAttributeLabel_325, $currProdID);
				$custom_label_4 = $this->attributevarchar($customAttributeLabel_326, $currProdID);
									
				// Check If any value is not updated in Product, then only get the Google Data from custom Table
				if(	$custom_label_0=='' && $custom_label_1=='' && $custom_label_2=='' && $custom_label_3=='' && $custom_label_4=='')
				{
					
					/*
					 * Closed : 18-08-2017
					 * Description : Denis suggested not to use the Table, as this is already updated for it's all Items to Products data.
					 * */
					
					//added 2 new fields(requested by SEO team on 24-Jul-2014)
					/*
					$custom_label_0 = " ";
					$custom_label_1 = " ";
					$custom_label_4 = " ";
					$feeds_type = "";
					$feedsTypeArr = $this->selectGoogleFeedsAds($products['sku']);  
					if(isset($feedsTypeArr["custom_label_1"]) && $feedsTypeArr["custom_label_1"] != "")
					{
						$custom_label_0 = "TopProducts";
						$custom_label_1 = $feedsTypeArr["custom_label_1"];
						$custom_label_4 = $feedsTypeArr["custom_label_4"];
					}
					*/
					
					//get product categories and set in custom label(requested by SEO team on 6-Feb-2015)
					$custom_label_2 = " ";
					$custom_label_3 = " ";
					
					$prdCatArr 		= $this->categorieslist($currProdID);
					$parentCatArr 	= $this->getParentCategories($prdCatArr);
									
					if(isset($parentCatArr[0]))
					{
						$custom_label_2 = $parentCatArr[0];
					}
					if(isset($parentCatArr[1]))
					{
						$custom_label_3 = $parentCatArr[1];
					}
										
					
					//Insert Google Feed Data into Product Field 					
					$this->insertgooglefeedsproductcustomdata($customAttributeLabel_322, $currProdID, $custom_label_0);
					$this->insertgooglefeedsproductcustomdata($customAttributeLabel_323, $currProdID, $custom_label_1);
					$this->insertgooglefeedsproductcustomdata($customAttributeLabel_324, $currProdID, $custom_label_2);
					$this->insertgooglefeedsproductcustomdata($customAttributeLabel_325, $currProdID, $custom_label_3);
					$this->insertgooglefeedsproductcustomdata($customAttributeLabel_326, $currProdID, $custom_label_4);
					
					$textmsg = "Date=>".date("Y-m-d@H:i:s").
								" currProdID =>".$currProdID.
								" custom_label_0 =>".$custom_label_0.
								" custom_label_1 =>".$custom_label_1.
								" custom_label_2 =>".$custom_label_2.
								" custom_label_3 =>".$custom_label_3.
								" custom_label_4 =>".$custom_label_4." ".
								"\n";
					file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/google-feed-items-label-log.txt",$textmsg,FILE_APPEND);
					
					$textmsg = "Date=>".date("Y-m-d@H:i:s")."/create/insertgooglefeedsproductcustomdata=>".
								" currProdID =>".$currProdID.
								" custom_label_0 =>".$custom_label_0.
								" custom_label_1 =>".$custom_label_1.
								" custom_label_2 =>".$custom_label_2.
								" custom_label_3 =>".$custom_label_3.
								" custom_label_4 =>".$custom_label_4." "."\n";
					file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);
					
				}
			 
			 /*
			  * ----------------(google-feed-changes)-------[END]--------------------- 
			  * */			

			if(count($arrayOption) > 0){ 
				
				$textmsg = "Date=>".date("Y-m-d@H:i:s")."/create/ count-arrayOption=>".count($arrayOption)." "."\n";
				file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);			
				
				/*			 
				 * No need of Delete statement, in Create Api, Surely that will be nothing found in where clause. 			  
				 * */
				$product = Mage::getModel("catalog/product")->load($insertid);	
							
				foreach ($arrayOption as $options)
				{
					foreach ($options as $option)
					{
						Mage::log("Query cusatom option: ".print_r($option, true),null,'customoption.log');
						
						/* 'Engraving' This is Implemented recently */
						if($option['title'] == 'Engraving'){

							$write = Mage::getSingleton('core/resource')->getConnection('core_write');
							$write->query("insert into ".$tablePrefix."catalog_product_option (product_id,type,is_require,max_characters,image_size_x,image_size_y, sort_order) values($insertid,'field',0,30,0,0,4)");
							$insertidoptionId = $write->lastInsertId();
							$insertidoptionId2  = $write->fetchOne('SELECT last_insert_id()'); 

							$write->query("insert into ".$tablePrefix."catalog_product_option_title (option_id,store_id,title) values($insertidoptionId,0,'Engraving Text')");
							
							$write->query("insert into ".$tablePrefix."catalog_product_option_price (option_id,store_id,price_type) values($insertidoptionId,0,'fixed')");
											   
							Mage::log("Query cusatom option title: ".$option['title'],null,'customoption2.log');
							Mage::log("Query cusatom option title: ".$sql1.$sql2.$sql3.$sql4,null,'customoption3.log');

						}
						$opt = Mage::getModel('catalog/product_option');
						$opt->setProduct($product);
						$opt->addOption($option);
						$opt->saveOptions();
					}
				}
			}
			/*	------------------------------------------------ */
            
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }
		
		$pID[] = $insertid;	//settype($insertid, "string");
        return $pID;		//$product->getId();
    }

    /**
     * Update product data
     *
     * @param int|string $productId
     * @param array $productData
     * @param string|int $store
     * @return array
     */
    public function update($productId, $productData, $store = null, $identifierType = null)
    {
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		
        $product = $this->_getProduct($productId, $store, $identifierType);

        // save data attribute_set_id when product have change. Fix the issue data can not save when update attribute  (Task #3126)
        $oldAttributeSetId = '';
        if ($product->getId() && !empty($productData["attribute_set_id"]) && $productData["attribute_set_id"] != $product->getAttributeSetId()) {
        	$oldAttributeSetId = $product->getAttributeSetId();
            $product->setAttributeSetId($productData["attribute_set_id"])
                    ->setIsMassupdate(true);
            $product->save();
        }

        $this->_prepareDataForSave($product, $productData);
		
		/* New Added by Ravinesh(Onsis) ---------------------------------------------- */
		if (!$product->getId())
		{
            $result='';
        }
		else
		{
			if (isset($productData['website_ids']) && is_array($productData['website_ids'])) {
				$product->setWebsiteIds($productData['website_ids']);
			}

			foreach ($product->getTypeInstance(true)->getEditableAttributes($product) as $attribute) {
				if ($this->_isAllowedAttribute($attribute) && isset($productData[$attribute->getAttributeCode()])) {
						$product->setData(
						$attribute->getAttributeCode(),
						$productData[$attribute->getAttributeCode()]
					);
				}
			}
			
			$this->_prepareDataForSave($product, $productData);
			$arrayOption = array();
			/**
			* For Creating dropdown,select,multiselect,radio type of custom option
			*/
                        if (isset($productData['product_options']) && isset($productData['product_options']['value'])){
                            foreach($productData['product_options']['value'] as $key=>$value)
                            {
                                    $firtsval=$productData['product_options']['childval'][$value];
                                    $secondval=$value;
                                    $thirdval=$productData['product_options']['type'][$key];
                                    $fourthval=$productData['product_options']['isrequire'][$key];
                                    $fifthval=$productData['product_options']['sort'][$key];
                                    $arrayOption[] = $this->setCustomOption($firtsval, $secondval, $thirdval,$fourthval,$fifthval);
                            }//------------------------------------------------------------------------//

                        }
			try {
				/**
				 * @todo implement full validation process with errors returning which are ignoring now
				 * @todo see Mage_Catalog_Model_Product::validate()
				 */
				if (is_array($errors = $product->validate())) {
					$strErrors = array();
					foreach($errors as $code => $error) {
						if ($error === true) {
							$error = Mage::helper('catalog')->__('Value for "%s" is invalid.', $code);
						} else {
							$error = Mage::helper('catalog')->__('Value for "%s" is invalid: %s', $code, $error);
						}
						$strErrors[] = $error;
					}
					$this->_fault('data_invalid', implode("\n", $strErrors));
				}
				/* New Added by Ravinesh(Onsis) ------------- */
				//Set default special from date for price.
				//$specialFromDate = date("Y-m-d 00:00:00", (time()-86400));//05/8/12
				$specialFromDate = "1999-11-30 00:00:00";
				$product->setSpecialFromDate($specialFromDate);
			
				//Save product brand
				if(isset($productData["c2c_brand"]) && $productData["c2c_brand"]!="")
				{
					$brand =  $this->_selectBrand($productData["c2c_brand"]);
					if($brand!=0)
					{
						$product->setBrand($brand);
					}
				}//------------------------------------------//
				$product->save();
				// Update request path in rewrite url
                if(isset($productData['status']) && $productData['status'] == Mage_Catalog_Model_Product_Status::STATUS_ENABLED){
                    $this->updateRewriteURL($product);
                }
				/* New Added by Ravinesh(Onsis) ------------------------------------- */
				//save product video information
				/*
				if($productData['video_link']!='')
				{					
					//Not working this section for now.
					//$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
					$results = $conn->fetchAll("SELECT * FROM ".$tablePrefix."catalog_product_video where product_id='".$product->getId()."'");
					$editvideid=$results[0]['video_id'];
					if($editvideid!='')
					{
						$videoproductobj = Mage::getModel('catalog/product_video')->load($editvideid);
					}
					else
					{
						$videoproductobj = Mage::getModel('catalog/product_video');
					}					
					$videoproductobj->setProductId($product->getId())
											->setVideoUrl($productData['video_link'])									
											->save();
					
					//$status = $this->savevideo($product->getId(), $productData['video_link']);
					
				}
				*/
				if(count($arrayOption)>0)
				{ 
					$write = Mage::getSingleton('core/resource')->getConnection('core_write');
					$write->query("delete from ".$tablePrefix."catalog_product_option where product_id='".$product->getId()."'");
					$insertid=$product->getId();
					$product = Mage::getModel("catalog/product")->load($insertid);
					
					foreach ($arrayOption as $options){

						foreach ($options as $option) {
							
							if(!in_array("Engraving",$option))
							{
								$results = $write->fetchAll("SELECT option_id FROM ".$tablePrefix."catalog_product_option where product_id='".$product->getId()."' and type='field'");
								$del_option_id = $results[0]['option_id'];
								if($del_option_id !='')
								{							
								   $write->query("delete from ".$tablePrefix."catalog_product_option where product_id='".$product->getId()."' and type='field'");
								}
												
								$sql1= "SELECT option_id FROM ".$tablePrefix."catalog_product_option where product_id='".$product->getId()."' and type='field' <br>";
								
								$sql2= "delete from ".$tablePrefix."catalog_product_option where product_id='".$product->getId()."' and type='field' <br>";
							   
								Mage::log("Query cusatom option title: ".$option['title'],null,'customoption4.log');
								Mage::log("Query cusatom option query: ".$sql1.$sql2,null,'customoption5.log');
							
							}else{
								
								if($option['title'] == 'Engraving')	{						
									
									$results2 = $write->fetchAll("SELECT opt.option_id FROM ".$tablePrefix."catalog_product_option as opt INNER JOIN ".$tablePrefix."catalog_product_option_title as tit on tit.option_id= opt.option_id WHERE opt.product_id= '".$product->getId()."' AND opt.type='field'");
									$select_option_id = $results2[0]['option_id'];

									if($select_option_id =='')
									{											
										$write = Mage::getSingleton('core/resource')->getConnection('core_write');
										$write->query("insert into ".$tablePrefix."catalog_product_option (product_id,type,is_require,max_characters,image_size_x,image_size_y, sort_order) values('".$product->getId()."','field',0,30,0,0,4)");
										$insertidoptionId = $write->lastInsertId();
										$insertidoptionId2  = $write->fetchOne('SELECT last_insert_id()'); 

										$write->query("insert into ".$tablePrefix."catalog_product_option_title (option_id,store_id,title) values($insertidoptionId,0,'Engraving Text')");

										$write->query("insert into ".$tablePrefix."catalog_product_option_price (option_id,store_id,price_type) values($insertidoptionId,0,'fixed')"); 
									}
								 
								}
							}
							$this->_saveProductCustomOption($product, $option);
						}
					}
				}//------------------------------------------------------------------//
				
			} catch (Mage_Core_Exception $e) {
				$this->_fault('data_invalid', $e->getMessage());
				// revert save attrinute set id when update product get error
				if ($oldAttributeSetId) {
					$product->setAttributeSetId($oldAttributeSetId)->save();
				}
			}
        }//-----------------------------------------------------------------//
        $res[] = true;
        return $res;
    }

    public function updateRewriteURL($product)
    {
        $urlKey = Mage::getModel('catalog/product_url')->formatUrlKey($product->getName());
        $request_path = "";
        if(Mage::helper("opentechizcatalog")->canAppendPrefixCategoryUrl()) {
            $prefix = $this->_getPrefixByProduct($product);
            if($prefix) {
                $request_path = $prefix . "/" . $urlKey;
            }
        } else {
            $request_path = $urlKey;
        }

        $sku = strtolower($product->getSku());
        if($sku) {
            $request_path .=  "-" . $sku;
        }
        if ($urlKey != $product->getUrlKey()) {
            $product->setUrlKey($urlKey);
            $product->save();
        }
        $rewrite = $this->_getRequestPath("product/" . $product->getId());
        Mage::getResourceModel('catalog/url')->saveRewrite(array(
            'store_id' => 1,
            'category_id' => null,
            'product_id' => $product->getId(),
            'id_path' => "product/" . $product->getId(),
            'request_path' => $request_path,
            'target_path' => "catalog/product/view/id/" . $product->getId(),
            'is_system' => 1
                ), $rewrite);
    }

    protected function _getRequestPath($id_path){
        $url = Mage::getModel('core/url_rewrite')->load($id_path, "id_path");
        if($url->getId()){
            return $url;
        }
        return false;
    }

    protected function _getPrefixByProduct($product) {
        $category_id = '';
        $excluded_cats = Mage::helper('opentechizcatalog')->getConfigExcludeCategoryIds();
        $categoryIdss = array_diff($product->getCategoryIds(), $excluded_cats);
        if (count($categoryIdss)) {
            foreach ($categoryIdss as $data) {
                if ($data != '') {
                    $category_id = $data;
                    break;
                }
            }
        }
        if (!$category_id) {
            return "";
        }
        $conn = Mage::getSingleton('core/resource')->getConnection('core_write');
        $tablePrefix = (string) Mage::getConfig()->getTablePrefix();
        $nameSql = "SELECT path FROM " . $tablePrefix . "catalog_category_entity WHERE entity_id=" . $category_id;
        $sqlNameRes = $conn->fetchRow($nameSql);
        $path = $sqlNameRes["path"];
        $ids = explode('/', $path);
        if (isset($ids[2])) {
            $topParent = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getId())->load($ids[2]);
        } else {
            $topParent = null;
        }
        $prefix = "";
        if ($topParent){
          $prefix  = $topParent->getUrlKey();
        }
        return $prefix;
    }


    protected function _saveProductCustomOption($product, $data)
    {
        foreach ($data as $key => $value) {
            if (is_string($value)) {
                $data[$key] = Mage::helper('catalog')->stripTags($value);
            }
        }

        try {
            if (!$product->getOptionsReadonly()) {
                $product
                    ->getOptionInstance()
                    ->setOptions(array($data));

                $product->setHasOptions(true);

                // an empty request can be set as event parameter
                // because it is not used for options changing in observers
                Mage::dispatchEvent(
                    'catalog_product_prepare_save',
                    array('product' => $product, 'request' => new Mage_Core_Controller_Request_Http())
                );

                $product->save();
            }
        } catch (Exception $e) {
            $this->_fault('save_option_error', $e->getMessage());
        }
    }

    /**
     *  Set additional data before product saved
     *
     *  @param    Mage_Catalog_Model_Product $product
     *  @param    array $productData
     *  @return   object
     */
    protected function _prepareDataForSave($product, $productData)
    {
        if (isset($productData['website_ids']) && is_array($productData['website_ids'])) {
            $product->setWebsiteIds($productData['website_ids']);
        }

        foreach ($product->getTypeInstance(true)->getEditableAttributes($product) as $attribute) {
            //Unset data if object attribute has no value in current store
            if (Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID !== (int) $product->getStoreId()
                && !$product->getExistsStoreValueFlag($attribute->getAttributeCode())
                && !$attribute->isScopeGlobal()
            ) {
                $product->setData($attribute->getAttributeCode(), false);
            }

            if ($this->_isAllowedAttribute($attribute)) {
                if (isset($productData[$attribute->getAttributeCode()])) {
                    $product->setData(
                        $attribute->getAttributeCode(),
                        $productData[$attribute->getAttributeCode()]
                    );
                } elseif (isset($productData['additional_attributes']['single_data'][$attribute->getAttributeCode()])) {
                    $product->setData(
                        $attribute->getAttributeCode(),
                        $productData['additional_attributes']['single_data'][$attribute->getAttributeCode()]
                    );
                } elseif (isset($productData['additional_attributes']['multi_data'][$attribute->getAttributeCode()])) {
                    $product->setData(
                        $attribute->getAttributeCode(),
                        $productData['additional_attributes']['multi_data'][$attribute->getAttributeCode()]
                    );
                }
            }
        }

        if (isset($productData['categories']) && is_array($productData['categories'])) {
            $product->setCategoryIds($productData['categories']);
        }

        if (isset($productData['websites']) && is_array($productData['websites'])) {
            foreach ($productData['websites'] as &$website) {
                if (is_string($website)) {
                    try {
                        $website = Mage::app()->getWebsite($website)->getId();
                    } catch (Exception $e) { }
                }
            }
            $product->setWebsiteIds($productData['websites']);
        }

        if (Mage::app()->isSingleStoreMode()) {
            $product->setWebsiteIds(array(Mage::app()->getStore(true)->getWebsite()->getId()));
        }

        if (isset($productData['stock_data']) && is_array($productData['stock_data'])) {
            $product->setStockData($productData['stock_data']);
        }

        if (isset($productData['tier_price']) && is_array($productData['tier_price'])) {
             $tierPrices = Mage::getModel('catalog/product_attribute_tierprice_api')
                 ->prepareTierPrices($product, $productData['tier_price']);
             $product->setData(Mage_Catalog_Model_Product_Attribute_Tierprice_Api::ATTRIBUTE_CODE, $tierPrices);
        }
    }

    /**
     * Update product special price
     *
     * @param int|string $productId
     * @param float $specialPrice
     * @param string $fromDate
     * @param string $toDate
     * @param string|int $store
     * @return boolean
     */
    public function setSpecialPrice($productId, $specialPrice = null, $fromDate = null, $toDate = null, $store = null)
    {
        return $this->update($productId, array(
            'special_price'     => $specialPrice,
            'special_from_date' => $fromDate,
            'special_to_date'   => $toDate
        ), $store);
    }

    /**
     * Retrieve product special price
     *
     * @param int|string $productId
     * @param string|int $store
     * @return array
     */
    public function getSpecialPrice($productId, $store = null)
    {
        $product = $this->_getProduct($productId, $store);

        $result = array(
            'special_price'     => $product->getSpecialPrice(),
            'special_from_date' => $product->getSpecialFromDate(),
            'special_to_date'   => $product->getSpecialToDate()
        );

        return $result;
    }

    /**
     * Delete product
     *
     * @param int|string $productId
     * @return boolean
     */
    public function delete($productId, $identifierType = null, $deleteOnlyVideo = null)
    {
        $product = $this->_getProduct($productId, null, $identifierType);
		
		/* New Added by Ravinesh(Onsis) ---------------------------------- */
		if (!$product->getId()) {
			return false;
        }
		
		/* //Delete only product video
		if($deleteOnlyVideo!=null && $deleteOnlyVideo=="delete_video")
		{
			$deleteRes = $this->_deleteProductVideo($product->getId());
			return $deleteRes;
		}
		*/
		/*	-----------------------------------------------------	*/
		
        try {
            $product->delete();
            //$this->_deleteProductVideo($product->getId());	/* New Added by Ravinesh(Onsis) */

        } catch (Mage_Core_Exception $e) {
            $this->_fault('not_deleted', $e->getMessage());
        }

		return true;
    }

   /**
    * Get list of additional attributes which are not in default create/update list
    *
    * @param  $productType
    * @param  $attributeSetId
    * @return array
    */
    public function getAdditionalAttributes($productType, $attributeSetId)
    {
        $this->_checkProductTypeExists($productType);
        $this->_checkProductAttributeSet($attributeSetId);

        /** @var $product Mage_Catalog_Model_Product */
        $productAttributes = Mage::getModel('catalog/product')
            ->setAttributeSetId($attributeSetId)
            ->setTypeId($productType)
            ->getTypeInstance(false)
            ->getEditableAttributes();

        $result = array();
        foreach ($productAttributes as $attribute) {
            /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
            if ($attribute->isInSet($attributeSetId) && $this->_isAllowedAttribute($attribute)
                && !in_array($attribute->getAttributeCode(), $this->_defaultProductAttributeList)) {

                if ($attribute->isScopeGlobal()) {
                    $scope = 'global';
                } elseif ($attribute->isScopeWebsite()) {
                    $scope = 'website';
                } else {
                    $scope = 'store';
                }

                $result[] = array(
                    'attribute_id' => $attribute->getId(),
                    'code' => $attribute->getAttributeCode(),
                    'type' => $attribute->getFrontendInput(),
                    'required' => $attribute->getIsRequired(),
                    'scope' => $scope
                );
            }
        }

        return $result;
    }

    /**
     * Check if product type exists
     *
     * @param  $productType
     * @throw Mage_Api_Exception
     * @return void
     */
    protected function _checkProductTypeExists($productType)
    {
        if (!in_array($productType, array_keys(Mage::getModel('catalog/product_type')->getOptionArray()))) {
            $this->_fault('product_type_not_exists');
        }
    }

    /**
     * Check if attributeSet is exits and in catalog_product entity group type
     *
     * @param  $attributeSetId
     * @throw Mage_Api_Exception
     * @return void
     */
    protected function _checkProductAttributeSet($attributeSetId)
    {
        $attributeSet = Mage::getModel('eav/entity_attribute_set')->load($attributeSetId);
        if (is_null($attributeSet->getId())) {
            $this->_fault('product_attribute_set_not_exists');
        }
        if (Mage::getModel('catalog/product')->getResource()->getTypeId() != $attributeSet->getEntityTypeId()) {
            $this->_fault('product_attribute_set_not_valid');
        }
    }
    
    
    /**
	 * @Created By 	: Ravinesh(Onsis)
	 * @Created On 	: 27-Jul-2017
	 * @Description	: Delete product video from DB and unlink Video file from media dir and S3 bucket
	 * @param		: int
	 * @return		: boolean
	 **/
	protected function _deleteProductVideo($productId)
	{
		$conn		= Mage::getSingleton('core/resource')->getConnection('core_write');
		$videoSql	= "SELECT video_id, video_url FROM ".$tablePrefix."catalog_product_video where product_id=".$productId;
		$results	= $conn->fetchRow($videoSql);
		$fileName	= "";
		if(isset($results['video_id']) && $results['video_id']>0)
		{
			//$videoproductobj = Mage::getModel('catalog/product_video')->load($results['video_id']);
			//$videoproductobj->delete();
			
			if(isset($results['video_url']) && $results['video_url']!="")
			{
				$fileName	= $results['video_url'];
				//unlink video file from dir
				$path 		= Mage::getBaseDir('media').DS."catalog".DS."product".DS."video";
				$delFilePath= $path."/".$fileName;
				@unlink($delFilePath);
			
				// Delete from cdn S3 bucket
				$cdnPath = '/usr/bin/s3cmd del -c /var/www/ItsHot/bucket/.s3cfg s3://itshot/media/catalog/product/video/'.$fileName;
				shell_exec($cdnPath);
			}	
			return true;
		}
		else
		{
			return "No record exists in database for this product.";
		}
		return true;	
	}//end function
	
	/**
	 * @Created By : Ravinesh(Onsis)
	 * @Created On : 27-Jul-2017
	 * @Description: Get product custom info and check the existance of products
	 * 
	 * @param int|string|array $productId
     * @param int|string|array $identifierType
     * @param int|string|array $exists
     * @return boolean
	 **/
	protected function _getCustomInfo($productId, $identifierType="sku", $exists="exists")
	{
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
					
		if(isset($productId) && $productId!=null)
		{
			$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
			$newProductId = 0;
			
			//get the product information based on SKU and Id
			if($identifierType=="sku")
			{
				$sSQL = "SELECT entity_id FROM ".$tablePrefix."catalog_product_entity WHERE sku='".$productId."'";
				//$result["sql"] = $sSQL;
				$productSkuArr	= $conn->fetchRow($sSQL);
				if(isset($productSkuArr['entity_id']) && $productSkuArr['entity_id']>0)
				{
					$newProductId = $productSkuArr['entity_id'];
				}
			}
			else
			{
				$sSQL = "SELECT entity_id FROM ".$tablePrefix."catalog_product_entity WHERE entity_id=".$productId;
				//$result["sql"] = $sSQL;
				$productSkuArr	= $conn->fetchRow($sSQL);
				if(isset($productSkuArr['entity_id']) && $productSkuArr['entity_id']>0)
				{
					$newProductId = $productSkuArr['entity_id'];
				}
			}
			
			$textmsg = "Date=>".date("Y-m-d@H:i:s").
									" productId===>".$productId.
									" identifierType===>".$identifierType.
									" newProductId===>".$newProductId.
									" exists===>".$exists.
									" "."\n";
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);
		
			//Return true if products exists else return false
			if($exists!=null && $exists=="exists")
			{
				if($newProductId>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}			
			//Return array if products exists else return empty string
			else if($exists!=null && $exists=="url_key")
			{
				$result = "";
				if($newProductId>0)
				{	
					//url_key | attribute_id=> 97
					$keySQL = "SELECT value FROM ".$tablePrefix."catalog_product_entity_varchar WHERE attribute_id=97 AND entity_id=".$newProductId;
					//$result["sql"] = $keySQL;
					$productKeyArr	= $conn->fetchRow($keySQL);
					if(isset($productKeyArr['value']) && $productKeyArr['value']!="")
					{
						//$result["id"] = $newProductId;
						$result = $productKeyArr['value'];
					}
				}
				return $result;
			}
			else if($exists!=null && $exists=="custom_url_key")
			{
				//For Custom Image name upadte from adempiere side.
				$result = "";
				if($newProductId>0)
				{
					$keySQL = "SELECT request_path FROM ".$tablePrefix."core_url_rewrite WHERE product_id='".$newProductId."' AND id_path='product/".$newProductId."'";
					$productKeyArr	= $conn->fetchRow($keySQL);
					if(isset($productKeyArr['request_path']) && $productKeyArr['request_path']!="")
					{
						//$resultArray = explode(".",$productKeyArr['request_path']);
						//$result = $resultArray[0];
						//Code commneted at 12-07-2018 by Ankush
						$resultArray = str_replace("/","-",$productKeyArr['request_path']);
						$result = $resultArray;
					}
				}
				
				/*$finalResult = " >>ProductID: ".$newProductId." URL: ".$result;
				file_put_contents($_SERVER['DOCUMENT_ROOT'].'feeds/API/log/product_image_url_key-log.txt',"======>>Date: ".date('d-m-Y H:i:s').$finalResult,FILE_APPEND); */
				return $result; 
			} 
			//get product price and return
			else if($exists!=null && $exists=="price")
			{
				$result = array("price"=>"0.00", "special_price"=>"0.00", "cost"=>"0.00");
				if($newProductId>0)
				{	
					//price	attribute_id=>75;	special_price attribute_id=>76;	cost attribute_id=>79
					//get Price
					$priceSQL = "SELECT value FROM ".$tablePrefix."catalog_product_entity_decimal WHERE attribute_id=75 AND entity_id=".$newProductId;
					$priceArr	= $conn->fetchRow($priceSQL);
					if(isset($priceArr['value']) && $priceArr['value']!="")
					{
						$result["price"] = $priceArr['value'];
					}
					//get Special Price
					$sPriceSQL = "SELECT value FROM ".$tablePrefix."catalog_product_entity_decimal WHERE attribute_id=76 AND entity_id=".$newProductId;
					$sPriceArr	= $conn->fetchRow($sPriceSQL);
					if(isset($sPriceArr['value']) && $sPriceArr['value']!="")
					{
						$result["special_price"] = $sPriceArr['value'];
					}
					//get cost
					$costSQL = "SELECT value FROM ".$tablePrefix."catalog_product_entity_decimal WHERE attribute_id=79 AND entity_id=".$newProductId;
					$costArr	= $conn->fetchRow($costSQL);
					if(isset($costArr['value']) && $costArr['value']!="")
					{
						$result["cost"] = $costArr['value'];
					}
				}
				return $result;
			}
		}
		else
		{
			return false;
		}
	}

	/**
	 * @Created By : Ravinesh(Onsis)
	 * @Created On : 27-Jul-2017
	 * @Description	: Select brand option Id from DB if already exists, else insert new brand and return new insert id
	 * @param		: string
	 * @return		: int
	 **/
	protected function _selectBrand($brandname)
	{
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
		
		$option_id 		= 0;
		$brandname		= addslashes(trim($brandname));
		$selectbrand	= "SELECT DISTINCT(option_id) AS option_id FROM ".$tablePrefix."eav_attribute_option_value WHERE value='".$brandname."'";
		$resBrand		= $conn->fetchRow($selectbrand);
		$id				= $resBrand['option_id'];
		if($id>0)
		{
			$option_id = $id;
		}
		else
		{
			$option_id = 0;
			//brand	attribute_id=>2050
			$insSQL = "INSERT INTO ".$tablePrefix."eav_attribute_option(attribute_id, sort_order) values(2050, 0)";
			$insRes = $conn->query($insSQL);
			$option_id = $conn->lastInsertId();
			if($option_id>0)
			{
				$insSQL1 = "INSERT INTO ".$tablePrefix."eav_attribute_option_value(option_id, store_id, value) values({$option_id}, 0, '{$brandname}')";
				$insRes1 = $conn->query($insSQL1);
				
				$insSQL2 = "INSERT INTO ".$tablePrefix."eav_attribute_option_value(option_id, store_id, value) values({$option_id}, 1, '{$brandname}')";
				$insRes2 = $conn->query($insSQL2);
			}
		}
		return 	$option_id;	
	}//end function

	/**
	 * @Created By : Ravinesh(Onsis)
	 * @Created On : 27-Jul-2017
	 * @Description: Move uploaded file to Amazon S3
	 * 
	 * @param		: int|string|array $productId
	 * @param		: int|string|array $identifierType
	 * @return		: string
	 **/
	protected function _uploadVideoFile($productId, $identifierType="sku")
	{
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		
		if(isset($productId) && $productId!=null)
		{
			$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
			$newProductId = 0;
			
			//get the product information based on SKU and Id
			if($identifierType=="sku")
			{
				$sSQL = "SELECT entity_id FROM ".$tablePrefix."catalog_product_entity WHERE sku='".$productId."'";
				$productSkuArr	= $conn->fetchRow($sSQL);
				if(isset($productSkuArr['entity_id']) && $productSkuArr['entity_id']>0)
				{
					$newProductId = $productSkuArr['entity_id'];
				}
			}
			else
			{
				$sSQL = "SELECT entity_id FROM ".$tablePrefix."catalog_product_entity WHERE entity_id=".$productId;
				$productSkuArr	= $conn->fetchRow($sSQL);
				if(isset($productSkuArr['entity_id']) && $productSkuArr['entity_id']>0)
				{
					$newProductId = $productSkuArr['entity_id'];
				}
			}
			
			if($newProductId>0)
			{
				//get product URL Key value		//url_key | attribute_id=> 97
				$urlKeySql = "SELECT value FROM ".$tablePrefix."catalog_product_entity_varchar WHERE attribute_id=97 AND entity_id=".$newProductId;
				$productUrlKeyArr	= $conn->fetchRow($urlKeySql);
				if(isset($productUrlKeyArr["value"]) && $productUrlKeyArr["value"]!="")
				{
					$path = Mage::getBaseDir('media').DS."catalog".DS."product".DS."video";
					$fileName = $productUrlKeyArr["value"].".flv";
					if(file_exists($path."/".$fileName))
					{
						//update video name in DB
						$updateSQL = "UPDATE ".$tablePrefix."catalog_product_video SET video_url='{$fileName}' WHERE product_id=".$newProductId;
						$conn->query($updateSQL);
						
						$exe_image = '/usr/bin/s3cmd put --acl-public --add-header=\'Cache-Control:no-cache\' -c /var/www/ItsHot/bucket/.s3cfg ' .$path.'/'.$fileName.' s3://itshot/media/catalog/product/video/'.$fileName;
						$output = shell_exec($exe_image);
						if($output)
						{
							return 'true';
							//@unlink($path."/".$fileName);
						}
						else
						{
							return "Could not upload file to S3.";
						}
						return 'false';
					}
					else
					{
						return "{$fileName} does not exists in media dir.";
					}
				}
				else
				{
					return "URL key does not exists for product id ".$newProductId;
				}
			}
			else
			{
				return "Product does not exists in database.";
			}
		}
		else
		{
			return 'false';
		}
	}

	/**
	 * @Created By : Ravinesh(Onsis)
	 * @Created On : 27-Jul-2017
	 * @Description: Move uploaded file to Amazon S3
	 **/
	public function setCustomOption ($value, $title, $type,$isrequire,$sort, $noOption = false)
	{
		$custom_options = array();
		if ($type && $value != "" && $value) {
			//$values = explode(',', $value);
			if ($value['value']) {
				/**If the custom option has options*/
				if (! $noOption) {
					$is_required = 0;
					$sort_order = 0;
					$custom_options[] = array(
						'is_delete' => 0 , 'title' => $title , 'previous_group' => '' , 'previous_type' => '' , 'type' => $type , 'is_require' =>$isrequire , 'sort_order' =>$sort , 'values' => array()
					);
					foreach ($value['value'] as $k=>$v) {
						$titleopt = ucfirst(trim($v));
						$price=$value['price'][$k];
						$pricetype=$value['pricetype'][$k];
						$sku=$value['sku'][$k];
						$sortorder=$value['sortorder'][$k];
						switch ($type) {
							case 'drop_down':
							case 'radio':
							case 'checkbox':
							case 'multiple':
							default:
								$title = ucfirst(trim($k));
								$custom_options[count($custom_options) - 1]['values'][] = array(
									'is_delete' => 0 , 'title' => $titleopt , 'option_type_id' => - 1 , 'price_type' => $pricetype , 'price' => $price , 'sku' => '' , 'sort_order' => $sortorder
								);
							break;
						}
					}
					return $custom_options;
				}
				/**If the custom option doesn't have options | Case: area and field*/
				else {
					$is_required = 0;
					$sort_order = '';
					$custom_options[] = array(
						"is_delete" => 0 , "title" => $title , "previous_group" => "text" , "price_type" => 'fixed' , "price" => '' , "type" => $type , "is_required" => 1
					);
					return $custom_options;
				}
			}
		}
		return false;
	}


	/**
	 * @Created By : Ravinesh(Onsis)
	 * @Created On : 27-Jul-2017
	 * Description : Retrieve product URL
     *
     * @param int|string|array $productId
     * @param string $identifier
     * @return array
     */
    public function url($productId, $identifier='sku')
    {
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
        $result = array();
        $conn = Mage::getSingleton('core/resource')->getConnection('core_write');
        $baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        
        if(is_array($productId))
        {
			foreach($productId AS $sku)
			{
				//get the product id from product SKU
				$productId = 0;
				$productUrl = "";
				$sSQL = "SELECT entity_id FROM ".$tablePrefix."catalog_product_entity WHERE sku='".$sku."'";
				$productSkuArr	= $conn->fetchRow($sSQL);
				if(isset($productSkuArr['entity_id']) && $productSkuArr['entity_id']>0)
				{
					$productId = $productSkuArr['entity_id'];
				}
				if($productId>0)
				{
					//get product URL
					$sqlUrl = "SELECT request_path FROM ".$tablePrefix."core_url_rewrite";
					$sqlUrl .= " WHERE product_id=".$productId." AND id_path='product/".$productId."'";
					$sqlUrlRes = $conn->fetchRow($sqlUrl);
					if($sqlUrlRes["request_path"]!="")
					{
						$productUrl = $baseUrl.$sqlUrlRes["request_path"];
					}
				}
				else
				{
					$productUrl = "Item Does Not Exists in Magento.";
				}
				$result[$sku] = $productUrl;
			}
		}
		return $result;
    }
    
    /**
	 * @Created By : Ravinesh(Onsis)
	 * @Created On : 26-08-2017
	 * Description : Retrieve product video
     *
     * @param int|string|array $productId
     * @return array
     */
    public function getvideo($productIds)
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $tablePrefix = (string)Mage::getConfig()->getTablePrefix();
        
		$results = array();
		if($productIds>0)
		{
			$sSQL = "SELECT video_url FROM ".$tablePrefix."catalog_product_video where product_id=".$productIds;
			
			echo $textmsg = "Date=>".date("Y-m-d@H:i:s")." /getvideo/ sSQL===>".$sSQL." "."\n";
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);			
			
			$results = $conn->fetchRow($sSQL);		
		}
		return $results;
    }
    
    /**
	 * @Created By : Ravinesh(Onsis)
	 * @Created On : 28-08-2017
	 * Description : Retrieve product video
     *
     * @param int|string|array $productId
     * @param int|string|array $videoUrl
     * @return boolean
     */
    public function savevideo($productId, $videoUrl="")
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $tablePrefix = (string)Mage::getConfig()->getTablePrefix();
        
		$results = array();
		if(trim($productId)!="" && trim($videoUrl)!="")		
		{
			//get existing record if exists
			$value_id = 0;
			$sSql = "SELECT video_id FROM ".$tablePrefix."catalog_product_video WHERE product_id='".$productId."' LIMIT 1";		
			$sqlUrlRes	= $conn->fetchRow($sSql);
									
			if(isset($sqlUrlRes['video_id']) && $sqlUrlRes['video_id']!=""){
				$video_id = $sqlUrlRes["video_id"];
			}
			
			if($video_id>0){
				//Update the Existing Value
				$uSQL = "UPDATE ".$tablePrefix."catalog_product_video SET video_url='".$videoUrl."' WHERE video_id=".$video_id;
				
			}else{
				//Insert the New Value
				$uSQL = "INSERT INTO ".$tablePrefix."catalog_product_video SET product_id='".$productId."', video_url='".$videoUrl."' ";
			}
			
			$textmsg = "Date=>".date("Y-m-d@H:i:s")."/savevideo/ uSQL=>".$uSQL." "."\n";
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);

			$conn->query($uSQL);
			
		}
		return true;
    }
    
    /*
     * Create Date : 18-08-2017
     * 
     * @param int|string|array $attrCode
     * @return string
     * */
    public function getAttributeId($attrCode)
	{
		$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		
		$sql 		 =	"SELECT attribute_id FROM ".$tablePrefix."eav_attribute"; 
		$sql 		.=	" WHERE attribute_code='".$attrCode."'"; 			
		$sqlUrlRes  = $conn->fetchRow($sql);
		
		if(isset($sqlUrlRes['attribute_id']) && $sqlUrlRes['attribute_id']!=""){			
			$attributeId = $sqlUrlRes['attribute_id'];			
			return $attributeId;		
		}else{
			return '';
		}		
	}
	
	
    /*
     * Create Date : 18-08-2017
     * 
     * @param int|string|array $attr_id
     * @param int|string|array $entity_id
     * @return string
     * */
    public function attributevarchar($attr_id,$entity_id)
	{
		$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		
		$sql 		=	"SELECT value FROM ".$tablePrefix."catalog_product_entity_varchar"; 
		$sql 		.=	" WHERE attribute_id='".$attr_id."'"; 
		$sql 		.=	" AND entity_id='".$entity_id."'";			
		$sqlUrlRes 	= $conn->fetchRow($sql);
		
		if(isset($sqlUrlRes['value']) && $sqlUrlRes['value']!=""){			
			$value = $sqlUrlRes['value'];			
			return $value;		
		}else{
			return '';
		}		
	}
	
	/*
     * Create Date : 18-08-2017
     * 
     * @param int|string|array $sku     
     * @return array
     * */
	public function selectGoogleFeedsAds($sku)
	{
		$product_type = array();
		$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		
		$sSql		= "SELECT custom_label_1, custom_label_4 FROM ".$tablePrefix."google_feeds_ads_product WHERE sku='{$sku}'";
		$sqlUrlRes	= $conn->fetchRow($sSql);
		
		if(!empty($sqlUrlRes))
		{			
			$product_type["custom_label_1"] = $sqlUrlRes["custom_label_1"];
			$product_type["custom_label_4"] = $sqlUrlRes["custom_label_4"];
		}	
		return $product_type;
	}
	
	/*
     * Create Date : 18-08-2017
     * 
     * @param int|string|array $entity_id     
     * @return array
     * */
	public function categorieslist($entity_id)
	{
		$category_array = array();
		$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		
		$sqlcat		= "SELECT category_id FROM ".$tablePrefix."catalog_category_product WHERE product_id='".$entity_id."'";		
		$sqlUrlRes	= $conn->fetchAll($sqlcat);
		
		if(!empty($sqlUrlRes)){
			
			foreach($sqlUrlRes AS $productArr)
			{
				$category_array[] = $productArr["category_id"];
			}
				
			return $category_array;
		
		}else{
			return $category_array;
		}
	}
	
	/*
     * Create Date : 18-08-2017
     * 
     * @param int|string|array $attr_id
     * @param int|string|array $entity_id
     * @return string
     * */
	public function categoryname($attr_id,$entity_id)
	{
		$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		
		$sql = "SELECT value FROM ".$tablePrefix."catalog_category_entity_varchar WHERE attribute_id='".$attr_id."' AND entity_id='".$entity_id."'";		
		$sqlUrlRes = $conn->fetchRow($sql);
				
		if(isset($sqlUrlRes['value']) && $sqlUrlRes['value']!=""){
			
			return $sqlUrlRes['value'];
		}else{
			return '';
		}
	}

	/*
     * Create Date : 18-08-2017
     * 
     * @param int|string|array $cat     
     * @return array
     * */
	public function getParentCategories($cat)
	{
		$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		
		$catId = 0;
		$parentCatNameArr = array();
		$catId = array_pop($cat); //get the last element of array
			
		//echo "<br>sss catID=>".$catId;
		if($catId>0)
		{
			//get category path
			$catPath = "";
			$sSql	= "SELECT path FROM ".$tablePrefix."catalog_category_entity WHERE entity_id=".$catId;			
			$sqlUrlRes	= $conn->fetchRow($sSql);
			
			
					
			if(isset($sqlUrlRes['path']) && $sqlUrlRes['path']!=""){
			
				$catPath = $sqlUrlRes["path"];
				$catPathArr = explode("/",$catPath);
				$catPathArr = array_slice($catPathArr,2);
				
				if(count($catPathArr)>1)
				{
					//get category name
					foreach($catPathArr as $parentCatId)
					{
						if($parentCatId!=$catId)
						{				
							$parentCatNameArr[] = $this->categoryname(41,$parentCatId);
						}
					}
					return $parentCatNameArr;
				}			
			}
		}
		return $parentCatNameArr;
	}
	
	
	/*
     * Create Date : 18-08-2017
     * Description: //Insert Google Feed Custom Data into Product main Table
     * 
     * @param int|string|array $attribute_id
     * @param int|string|array $entity_id
     * @param int|string|array $value
     * @return boolean
     * */
	public function insertgooglefeedsproductcustomdata($attribute_id="", $entity_id="", $value="")
	{		

		$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		
		//Image, Sitemap, SWI, TextFeed, Video
		if(trim($attribute_id)!="" && trim($entity_id)!="" && trim($value)!="")
		{
			$value = addslashes($value);
							
			//get existing record if exists
			$value_id = 0;
			$sSql = "SELECT value_id FROM ".$tablePrefix."catalog_product_entity_varchar WHERE entity_type_id='4' AND store_id='0' AND attribute_id='".$attribute_id."' AND entity_id='".$entity_id."' LIMIT 1";		
			$sqlUrlRes	= $conn->fetchRow($sSql);
			
			$textmsg = "Date=>".date("Y-m-d@H:i:s")." sSql=>".$sSql." "."\n";
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/google-feed-items-label-log.txt",$textmsg,FILE_APPEND);
		
			
			if(isset($sqlUrlRes['value_id']) && $sqlUrlRes['value_id']!=""){
				$value_id = $sqlUrlRes["value_id"];
			}
			
			if($value_id>0){
				//Update the Existing Value
				$uSQL = "UPDATE ".$tablePrefix."catalog_product_entity_varchar SET value='{$value}' WHERE value_id=".$value_id;
				
			}else{
				//Insert the New Value
				$uSQL = "INSERT INTO ".$tablePrefix."catalog_product_entity_varchar SET entity_type_id='4', store_id='0', attribute_id='".$attribute_id."', entity_id='".$entity_id."', value='".$value."' ";
			}	
			
			$textmsg = "Date=>".date("Y-m-d@H:i:s")." uSQL=>".$uSQL." "."\n";
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/google-feed-items-label-log.txt",$textmsg,FILE_APPEND);
				
			$conn->query($uSQL);
		}
		return true;
	}
	

} // Class Mage_Catalog_Model_Product_Api End
