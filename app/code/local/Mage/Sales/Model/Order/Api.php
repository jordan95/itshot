<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Order API
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Model_Order_Api extends Mage_Sales_Model_Api_Resource
{
    /**
     * Initialize attributes map
     */
    public function __construct()
    {
        $this->_attributesMap = array(
            'order' => array('order_id' => 'entity_id', 'order_type' => 'order_type', 'order_stage' => 'order_stage'),
            'order_address' => array('address_id' => 'entity_id'),
            'order_payment' => array('payment_id' => 'entity_id')
        );
    }

    /**
     * Initialize basic order model
     *
     * @param mixed $orderIncrementId
     * @return Mage_Sales_Model_Order
     */
    protected function _initOrder($orderIncrementId)
    {
        $order = Mage::getModel('sales/order');

        /* @var $order Mage_Sales_Model_Order */

        $order->loadByIncrementId($orderIncrementId);

        if (!$order->getId()) {
            $this->_fault('not_exists');
        }

        return $order;
    }

    /**
     * Retrieve list of orders. Filtration could be applied
     *
     * @param null|object|array $filters
     * @return array
     */
    public function items($filters = null, $fields=null)
    {
		/*$filters = array();
		$filters['status'] = array("IN"=>array("processing","pending","pending_payment","complete","payment_review"));
		$filters['created_at'] = array('gt'=>'2017-07-18 04:58:26');
		*/
		//$filters =json_decode('{"status":{"IN":["processing","pending","pending_payment","complete","payment_review"]},
		//							"created_at":{"gt":"2017-08-21 13:10:13.472"}}');

        //TODO: add full name logic
        $billingAliasName = 'billing_o_a';
        $shippingAliasName = 'shipping_o_a';

		$orderCollection = Mage::getModel("sales/order")->getCollection();

		//Return limited fields if $fields parameter is not null
		if($fields=="custom_fields")
		{
			$orderCollection->addAttributeToSelect("entity_id");
			//$collection->addAttributeToSelect("customer_id");
			$orderCollection->addAttributeToSelect("customer_email");
			$orderCollection->addAttributeToSelect("increment_id");
			$orderCollection->addAttributeToSelect("created_at");
			$orderCollection->addAttributeToSelect("status");
		}
		else
		{
			$orderCollection->addAttributeToSelect('*');
		}

		$orderCollection->addAddressFields();

        /** @var $orderCollection Mage_Sales_Model_Mysql4_Order_Collection */
        //$orderCollection = Mage::getModel("sales/order")->getCollection();
        if($fields==null)
		{
			$billingFirstnameField = "$billingAliasName.firstname";
			$billingLastnameField = "$billingAliasName.lastname";
			$shippingFirstnameField = "$shippingAliasName.firstname";
			$shippingLastnameField = "$shippingAliasName.lastname";
			$orderCollection->addExpressionFieldToSelect('billing_firstname', "{{billing_firstname}}",
					array('billing_firstname' => $billingFirstnameField))
				->addExpressionFieldToSelect('billing_lastname', "{{billing_lastname}}",
					array('billing_lastname' => $billingLastnameField))
				->addExpressionFieldToSelect('shipping_firstname', "{{shipping_firstname}}",
					array('shipping_firstname' => $shippingFirstnameField))
				->addExpressionFieldToSelect('shipping_lastname', "{{shipping_lastname}}",
					array('shipping_lastname' => $shippingLastnameField))
				->addExpressionFieldToSelect('billing_name', "CONCAT({{billing_firstname}}, ' ', {{billing_lastname}})",
					array('billing_firstname' => $billingFirstnameField, 'billing_lastname' => $billingLastnameField))
				->addExpressionFieldToSelect('shipping_name', 'CONCAT({{shipping_firstname}}, " ", {{shipping_lastname}})',
					array('shipping_firstname' => $shippingFirstnameField, 'shipping_lastname' => $shippingLastnameField)
			);
		}

        /** @var $apiHelper Mage_Api_Helper_Data */
        /*
         * Commented by Ravinesh: 29-08-2017 :----------//
        $apiHelper = Mage::helper('api');
        $filters = $apiHelper->parseFilters($filters, $this->_attributesMap['order']);
        try {
            foreach ($filters as $field => $value) {
                $orderCollection->addFieldToFilter($field, $value);
            }
        } catch (Mage_Core_Exception $e) {
            $this->_fault('filters_invalid', $e->getMessage());
        }
        * */

        $textmsg = "Date=>".date("Y-m-d@H:i:s").
								" orderCollection=>".json_encode($orderCollection).
								" orderCollection-toString=>".$orderCollection->getSelect().
								" "."\n";
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/sales/create-sales-log.txt",$textmsg,FILE_APPEND);

		//filter is object => return all orders => exceed memory limit
        if(is_object($filters)){
            $apiHelper = Mage::helper('api');
            $filters = $apiHelper->parseFilters($filters, $this->_attributesMap['order']);
        }

        //Changed by Ravinesh: 29-08-2017 : -------------------------//
        if (is_array($filters)) {
            try {
                foreach ($filters as $field => $value) {
                    if (isset($this->_attributesMap['order'][$field])) {
                        $field = $this->_attributesMap['order'][$field];
                    }

                    $orderCollection->addFieldToFilter($field, $value);
                }
            } catch (Mage_Core_Exception $e) {
                $this->_fault('filters_invalid', $e->getMessage());
            }
        }//----------------------------------------------------------//

        /*
         * Commented by Ravinesh: 24-08-2017 :----------//
        foreach ($orderCollection as $order) {
            $orders[] = $this->_getAttributes($order, 'order');
        }
        return $orders;
        */

        $textmsg = "Date=>".date("Y-m-d@H:i:s").
								" orderCollection=>".json_encode($orderCollection).
								" orderCollection-toString=>".$orderCollection->getSelect().
								" "."\n";
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/sales/create-sales-log.txt",$textmsg,FILE_APPEND);


        //Changed by Ravinesh: 24-08-2017 : --------------------------//
        $result = array();
        foreach ($orderCollection as $order) {
			/**
			 * @Description: Commented above code and added below conditon
			 * 				 to skip Orders which are already Sync to Adempiere
			 **/
			$orderIn      = $this->_getAttributes($order, 'order');
			$increment_id = $orderIn['increment_id'];

			//$objModel = Mage::getModel("syncorder/syncorder");
			//$checkOrder = $objModel->checkOrder($orderIn);  //incrementId
			//---------------------------------------------------//
				$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
				$tablePrefix = (string)Mage::getConfig()->getTablePrefix();

				$sql = "SELECT id FROM ".$tablePrefix."adempiere_sync_orders  WHERE increment_id='".$increment_id."' AND status='1'";
				$sqlUrlRes = $conn->fetchRow($sql);
				if(isset($sqlUrlRes['id']) && $sqlUrlRes['id']!=""){
					$checkOrder = true;
				}else{
					$checkOrder = false;
				}
			//---------------------------------------------------//


			if(!$checkOrder)
			{
				$result[] = $this->_getAttributes($order, 'order');
			}
        }
        //$result['50']="sssssssssssssssssssss";

        $textmsg = "Date=>".date("Y-m-d@H:i:s").
								" filters=>".json_encode($filters).
								" fields=>".json_encode($fields).
								" "."\n";
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/sales/create-sales-log.txt",$textmsg,FILE_APPEND);

		$res = $result;
		$textmsg = "Date=>".date("Y-m-d@H:i:s").
								" result=>".json_encode($res).
								" "."\n";
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/sales/create-sales-log.txt",$textmsg,FILE_APPEND);
        return $result;
        //------------------------------------------------------------//
    }

    /**
     * Retrieve full order information
     *
     * @param string $orderIncrementId
     * @return array
     */
    public function info($orderIncrementId, $infoType="")
    {

		/*
			$array = array(
					"foo" => "bar",
					42    => 24,
					"multi" => array(
						 "dimensional" => array(
							 "array" => "foo"
						 )
					)
				);
			return $array;
		 */


		$textmsg = "Date=>".date("Y-m-d@H:i:s").
								" orderIncrementId=>".json_encode($orderIncrementId).
								" infoType=>".json_encode($infoType).
								" "."\n";
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/var/log/create-sales-log.txt",$textmsg,FILE_APPEND);

		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();

		//Changed by Ravinesh: 24-08-2017 : --------------------------//
        //Check order already exists,
		if($infoType!="" && $infoType=="info")
		{
			$order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
			if ($order->getId())
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}//------------------------------------------------------------//

        $order = $this->_initOrder($orderIncrementId);

        if ($order->getGiftMessageId() > 0) {
            $order->setGiftMessage(
                Mage::getSingleton('giftmessage/message')->load($order->getGiftMessageId())->getMessage()
            );
        }

        $result = $this->_getAttributes($order, 'order');


		//Changed by Ravinesh: 24-08-2017 : --------------------------//
		//Get only payment information of Order and return,
		if($infoType!="" && $infoType=="payment")
		{
			$resultPayment = "";

			//$result = $this->_getAttributes($order, 'order');
			$resultPayment['customer_email'] = $result['customer_email'];
			$resultPayment['remote_ip'] = $result['remote_ip'];

			$k2 = $this->_getAttributes($order->getPayment(), 'order_payment');
			unset($k2['method_instance']);
			$resultPayment['payment'] = $k2;
			$resultPayment['payment']['cc_number_enc'] = $this->decrypt($resultPayment['payment']['cc_number_enc']);

			$resultPayment['billing_address']  = $this->_getAttributes($order->getBillingAddress(), 'order_address');

			return $resultPayment;
		}//-----------------------------------------------------------//

        $result['shipping_address'] = $this->_getAttributes($order->getShippingAddress(), 'order_address');
        $result['billing_address']  = $this->_getAttributes($order->getBillingAddress(), 'order_address');
        //$result['items'] = array();

		//Changed by Ravinesh: 24-08-2017 : -------------------------------//
		$conn = Mage::getSingleton('core/resource')->getConnection('core_read');
                $_conn = $conn->getConnection();
		// Add custom code for get the address id in magento
		if($result['shipping_address']['quote_address_id']=='')
		{
			$orderid=$result['order_id'];
			if($orderid!='')
			{
				// For balank email address
				$custid=$conn->fetchRow("SELECT customer_id, billing_address_id FROM ".$tablePrefix."sales_flat_order  where entity_id='".$orderid."' ");
				//print_r($custid);die;

				// for balank email address
				$billingaddressid = $custid['billing_address_id'];

				$billadress=$conn->fetchRow("SELECT customer_id, customer_address_id FROM ".$tablePrefix."sales_flat_order_address  where entity_id='".$billingaddressid."' ");
				//print_r($custid);die;
				$custom_billing_customer_id         = 	$billadress['customer_id'];
				$custom_billing_customer_address_id = 	$billadress['customer_address_id'];

				$custid=$custid['customer_id'];

				$data=Mage::getModel('customer/customer')->load($custid);
				$defaultbillingid=$data->getData('default_billing');
				$defaultshippingid=$data->getData('default_shipping');
				$result['shipping_address']['customer_address_id']=$defaultshippingid;
				$result['shipping_address']['customer_id']=$custid;
				$result['billing_address']['customer_address_id']=$defaultbillingid;
				$result['billing_address']['customer_id']=$custid;
				// For balank email adreess
				$result['billing_address']['custom_customer_id']=$custom_billing_customer_id;
				$result['billing_address']['custom_customer_address_id']=$custom_billing_customer_address_id;
			}
		}// End Code :-----------------------------------------------------//

		//$order = Mage::getModel('sales/order');
		//$order->loadByIncrementId($orderIncrementId);
        $result['items'] = array();
		$i=0;	//New Added by Ravinesh:--------//
        foreach ($order->getAllItems() as $item) {
            if ($item->getGiftMessageId() > 0) {
                $item->setGiftMessage(
                    Mage::getSingleton('giftmessage/message')->load($item->getGiftMessageId())->getMessage()
                );
            }

            //$result['items'][$i] = $this->_getAttributes($item, 'order_item');		//Commented by Ravinesh

            //Changed by Ravinesh: 24-08-2017 : -------------------------------//
            $result['items'][$i] = $this->_getAttributes($item, 'order_item');
			$_product = Mage::getModel('catalog/product')->load($result['items'][$i]['product_id']);
			$dist=$_product->getDistributor();
			$result['items'][$i]['distributor'] = $dist;
			$dist=='';
			$i++;
			//------------------------------------------------------------------//
        }

        //Changed by Ravinesh: 24-08-2017 : ------------------------------------//
        //Product custom options
		$result['custom_option'] = array();
		$looseDiamondArr = array();
		foreach($result['items'] as $key=>$value)
		{
			$result['custom_option'][$value['product_id']][] = unserialize($value['product_options']);

			//get product custom options for Loose Diamond product in concat in string, added by MS on 19-Dec-2013
			$productOptions = unserialize($value['product_options']);
			$productOptionsArr = isset($productOptions["options"]) ? $productOptions["options"] : array();
			$product_options_str = "";
			$product_options_arr = array();
			foreach($productOptionsArr AS $optionsArr)
			{
				$product_options_arr[] = $optionsArr["label"].": ".$optionsArr["value"];
			}
			$product_options_str =  implode(", ",$product_options_arr);
			$looseDiamondArr["item_description"][$value['item_id']] = $product_options_str;
		}
		//return $looseDiamondArr;

		//Concat Loose Diamond product options in product Name, added by MS on 19-Dec-2013
		$j=0;
        foreach ($order->getAllItems() as $item)
		{
			$subStrSku = substr($result['items'][$j]['sku'], 0, 13);
			if($subStrSku=="loose_diamond")
			{
				$item_id = $result['items'][$j]['item_id'];
				$newProductName = $result['items'][$j]['name']."(".$looseDiamondArr["item_description"][$item_id].")";
				$result['items'][$j]['name'] = $newProductName;
			}
			$j++;
        }

		/***
		 *  For geting the customer password
		 *  based on the customer email and store in to array and
		 *  return into api array**
		 **/
		// now $write is an instance of Zend_Db_Adapter_Abstract
		$sSQLPass  = " SELECT custvar.value FROM ".$tablePrefix."customer_entity AS cstent";
		$sSQLPass .= " INNER JOIN ".$tablePrefix."customer_entity_varchar AS custvar ON custvar.entity_id=cstent.entity_id";
		$sSQLPass .= " WHERE cstent.email=" .$_conn->quote($result['customer_email']). " AND custvar.attribute_id=12";
		$password = $conn->fetchRow($sSQLPass);
		$result['password'] = $password['value'];
        //----------------------------------------------------------------------//

        //$result['payment'] = $this->_getAttributes($order->getPayment(), 'order_payment');
		/*
		 * Commented by Ravinesh: 24-08-2017 :
		 * For API error in paymeny
		 * */

		//Changed by Ravinesh: 24-08-2017 : ------------------------------------//
		$k = $this->_getAttributes($order->getPayment(), 'order_payment');
		//Mage::log(print_r($k,1), null, 'logfile.log', true);
		Mage::log(get_class($this), null, 'create-sales-log.log', true);
		unset($k['method_instance']);
		/*
		// This is updated by Ankush at 04-04-2018
		$result['payment2'] = array_pop($k['additional_information']['authorize_cards']);
		//$result['payment2'] = array_pop($k['additional_information']);
		//$result['payment3'] = array_pop($result['payment2']);
		$k['last_trans_id'] = $result['payment2']['last_trans_id'];
		$k['cc_trans_id']   = $result['payment2']['last_trans_id'];
		$k['cc_type']       = $result['payment2']['cc_type'];
		$k['cc_last4']      = $result['payment2']['cc_last4'];
		$k['cc_exp_month']  = $result['payment2']['cc_exp_month'];
		$k['cc_exp_year']   = $result['payment2']['cc_exp_year'];

		*/

		$result['payment'] = $k;
		$result['payment']['cc_number_enc'] = $this->decrypt($result['payment']['cc_number_enc']);
		//-----------------------------------------------------------------------//

        $result['status_history'] = array();

        foreach ($order->getAllStatusHistory() as $history) {
            $result['status_history'][] = $this->_getAttributes($history, 'order_status_history');
        }

         //Mage::log(print_r($result['status_history'],1), null, 'logfile.log', true);
		//Changed by Ravinesh: 24-08-2017 : ---------------------------//
        //Check order info and get the CC Number from Quote table if does not exists in main order table
        if(($result["payment"]["method"]=="verisign" || $result["payment"]["method"]=="authorizenet") && ($result["payment"]["cc_number_enc"]==""))
        {
			$quote_id = 0;
			$quote_id = $result["quote_id"];
			if($quote_id>0)
			{
				$sqlQuote = "SELECT cc_number_enc FROM ".$tablePrefix."sales_flat_quote_payment WHERE quote_id=".$quote_id;
				$ccNumberRes = $conn->fetchRow($sqlQuote);
				if($ccNumberRes)
				{
					$result["payment"]["cc_number_enc"] = $this->decrypt($ccNumberRes['cc_number_enc']);
					$result["payment"]["cc_number_enc_q"] = $this->decrypt($ccNumberRes['cc_number_enc']);
				}
			}
		}
		//Get customer name from Billing Address if does not exists in Customer info
		if($result["customer_firstname"]=="" || $result["customer_lastname"]=="")
		{
			$result["customer_firstname"]	= $result["billing_address"]["firstname"];
			$result["customer_lastname"]	= $result["billing_address"]["lastname"];
		}
		//Remove CC type form if method is CheckMO and Order is Pending
		if($result["payment"]["method"]=="checkmo" && $result["status"]=="pending")
		{
			$result["payment"]["cc_type"] = "";
			$result["payment"]["cc_last4"] = "";
		}
		else if($result["payment"]["method"]=="paypal_standard" || $result["payment"]["method"]=="paypal_express")
		{
			$result["payment"]["cc_type"] = "";
			$result["payment"]["cc_last4"] = "";
		}
		//get CC Order Score
		$score = "";
		if($result["payment"]["method"]=="authorizenet" && $result["payment"]["cc_type"]!="")
		{
			$orderNumber = $result['increment_id'];
			$score = 'n/a';
		}
		$result['signifyd_score'] = $score;

		//Get Expected Ship Date, Estimated Delivery Date of amazon orders
		if($result["payment"]["method"]=="m2epropayment" || $result["payment"]["method"]=="amazon")
		{
			$amazonInfo = $this->__getAmazonOrderInfo($orderIncrementId);
			$result['amazon_info'] = $amazonInfo;
		}//--------------------------------------------------------------//
		//Mage::log(print_r($result,1), null, 'logfile.log', true);
        //Mage::log(print_r($result['status_history'],1), null, 'logfile.log', true);
//        if(isset($result['order_type'])) {
//            $result['order_type'] = Mage::helper('opentechiz_salesextend')->getOrderTypeAsString($result['order_type']);
//        }
        if(isset($result['order_stage'])) {
            $result['order_stage'] = Mage::helper('opentechiz_salesextend')->getOrderStageAsString($result['order_stage']);
        }
        return $result;
    }

    /**
     * Add comment to order
     *
     * @param string $orderIncrementId
     * @param string $status
     * @param string $comment
     * @param boolean $notify
     * @return boolean
     */
    public function addComment($orderIncrementId, $status, $comment = '', $notify = false)
    {
        $order = $this->_initOrder($orderIncrementId);

        $historyItem = $order->addStatusHistoryComment($comment, $status);
        $historyItem->setIsCustomerNotified($notify)->save();


        try {
            if ($notify && $comment) {
                $oldStore = Mage::getDesign()->getStore();
                $oldArea = Mage::getDesign()->getArea();
                Mage::getDesign()->setStore($order->getStoreId());
                Mage::getDesign()->setArea('frontend');
            }

            $order->save();
            $order->sendOrderUpdateEmail($notify, $comment);
            if ($notify && $comment) {
                Mage::getDesign()->setStore($oldStore);
                Mage::getDesign()->setArea($oldArea);
            }

        } catch (Mage_Core_Exception $e) {
            $this->_fault('status_not_changed', $e->getMessage());
        }

        return true;
    }

    /**
     * Hold order
     *
     * @param string $orderIncrementId
     * @return boolean
     */
    public function hold($orderIncrementId)
    {
        $order = $this->_initOrder($orderIncrementId);

        try {
            $order->hold();
            $order->save();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('status_not_changed', $e->getMessage());
        }

        return true;
    }

    /**
     * Unhold order
     *
     * @param string $orderIncrementId
     * @return boolean
     */
    public function unhold($orderIncrementId)
    {
        $order = $this->_initOrder($orderIncrementId);

        try {
            $order->unhold();
            $order->save();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('status_not_changed', $e->getMessage());
        }

        return true;
    }

    /**
     * Cancel order
     *
     * @param string $orderIncrementId
     * @return boolean
     */
    public function cancel($orderIncrementId)
    {
        $order = $this->_initOrder($orderIncrementId);

        if (Mage_Sales_Model_Order::STATE_CANCELED == $order->getState()) {
            $this->_fault('status_not_changed');
        }
        try {
            $order->cancel();
            $order->save();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('status_not_changed', $e->getMessage());
        }
        if (Mage_Sales_Model_Order::STATE_CANCELED != $order->getState()) {
            $this->_fault('status_not_changed');
        }
        return true;
    }

    /**
     * @Created By : Raviraj
     * @Created On : 08-08-2017
     * @Description: Get Expected Ship Date, Estimated Delivery Date of amazon orders using Amazon API.
     *
     * @Param: String
     * @Return: Array
     *
     **/
    protected function __getAmazonOrderInfo($orderIncrementId)
    {
		$amazonInfoArr = array();
		$orderIncrementId = str_replace("A-","",$orderIncrementId);//remove prefix A-
		$amazonInfoArr["order_increment_id"] = $orderIncrementId;

		//call API using CURL and then process the XML response
		$baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
		$request_url = $baseUrl."feeds/MarketplaceWebServiceOrders/Samples/GetOrderSample.php?orderIncrementId=".$orderIncrementId;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $request_url);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close($ch);

		$jsonResp = json_decode($response);
		if($jsonResp->{'Error'})
		{
			$amazonInfoArr["Error"] = $jsonResp->{'Error'};
		}
		else
		{
			$amazonInfoArr["EarliestShipDate"] = $jsonResp->{'EarliestShipDate'};
			$amazonInfoArr["LatestShipDate"] = $jsonResp->{'LatestShipDate'};
			$amazonInfoArr["EarliestDeliveryDate"] = $jsonResp->{'EarliestDeliveryDate'};
			$amazonInfoArr["LatestDeliveryDate"] = $jsonResp->{'LatestDeliveryDate'};
		}
		return $amazonInfoArr;
	}//end function

	/**
     * @Created By : Raviraj
     * @Created On : 24-08-2017
     * @Description: Decrypt data
     *
     * @param   string $data
     * @return  string
     */
    public function decrypt($data)
    {
        if ($data) {
            return Mage::helper('core')->decrypt($data);
        }
        return $data;
    }

    /**
     * @Created By : Raviraj
     * @Created On : 24-08-2017
     * @Description: Decrypt password
     *
     */
    public function decryptpassword($password)
    {
       return Mage::helper('core')->decrypt($password);
    }

    /**
     * @Created By : Raviraj
     * @Created On : 24-08-2017
     * @Description: SignifydScore
     *
     */
    public function getSignifydScore($order)
    {
		/* Your API key */
		$key = "hgZYEX4eF3wI1Y7nlI7Lcgpu1";
		$score = 0;
		if($order!="")
		{
			/* URL Endpoint for case information */
			$url = "https://api.signifyd.com/v2/orders/".$order."/case";

			$response = array();

			/* Get case data via the REST API */
			$response = $this->signifydRequest($url, $key, 'application/json');

			/* Decode the JSON */
			$raw_response = array_filter(json_decode($response["raw_response"], true));

			/**
			 * Parse the response to extract the score
			 */
			//$score = "Scoring in Progress";
			if (!empty($raw_response) && isset($raw_response["orderAmount"]))
			{
				//$score = round($raw_response["adjustedScore"]);
				$response['response'] = $response;
				$response['orderAmount'] = $raw_response["orderAmount"];
			}
		}
		return $response;
	}//end function

	public function getSignifydCaseId($caseId)
    {
		/* Your API key */
		$key = "hgZYEX4eF3wI1Y7nlI7Lcgpu1";
		$score = 0;

		if($caseId!="")
		{
			/* URL Endpoint for case information */
			$url = "https://api.signifyd.com/v2/cases/".$caseId;

			$response = array();

			/* Get case data via the REST API */
			$response = $this->signifydRequest($url, $key, 'application/json');

			/* Decode the JSON */
			$raw_response = array_filter(json_decode($response["raw_response"], true));

			/**
			 * Parse the response to extract the score
			 */
			//$score = "Scoring in Progress";
			if (!empty($raw_response))
			{
				$response['response'] = $response;
			}
		}
		return $response;
	}//end function


	/**
     * @Created By: Ankush
     * @Created On: 22-Jun-2018
     * Send product order details to the signifyd site and get responce
     * from Adempiere to Website
     *
     * @param int|string $orderId
     * @param string|json $data
     * @param string|int $store
     * @return array
     */
    public function signifydpost($orderId, $data, $store = null, $identifierType = null)
    {
		$contenttypeJason = 'application/json';

			$response = 'Stop Signifyd integration';
			//Log writting here
			$textmsg = "Date=>".date("Y-m-d@H:i:s")." |".
									" /score-signifyd-log/ ".
									" OrderId: =>".$orderId.
									" ".
									" => responseResult: =>". json_encode($response).
									" ".
									"\n";
			return $response;

	}  //end function

	/**
     * @Created By : Raviraj
     * @Created On : 24-08-2017
     * @Description: Get Customer's Info with Email
     *
     */
	function getcustomers($email) {

		/* Get customer model, run a query */
		$customer = Mage::getModel('customer/customer');
		$result=$customer->loadByEmail($email);

		return $result;
	}

    /**
     * @Created By : Raviraj
     * @Created On : 24-08-2017
     * @Description: Retrieve selected Field order information
     *
     * @param string $orderIncrementId
     * @return array
     */
    public function infoselected($orderIncrementId)
    {
		$textmsg = "Date=>".date("Y-m-d@H:i:s")." orderIncrementId=>".$orderIncrementId." "."\n";
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/feeds/newproduct/createProduct-log.txt",$textmsg,FILE_APPEND);

		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		$return = array();
		$conn = Mage::getSingleton('core/resource')->getConnection('core_read');

		$order = $this->_initOrder($orderIncrementId);

        $result = $this->_getAttributes($order, 'order');

        $result['shipping_address'] = $this->_getAttributes($order->getShippingAddress(), 'order_address');
        $result['billing_address']  = $this->_getAttributes($order->getBillingAddress(), 'order_address');
        //-----------------------------------------------------------------------------//
        if($result['shipping_address']['quote_address_id']=='')
		{
			$orderid=$result['order_id'];
			if($orderid!='')
			{
				$custid=$conn->fetchAll("SELECT customer_id FROM ".$tablePrefix."sales_flat_order  where entity_id='".$orderid."' ");
				$custid=$custid['0']['customer_id'];
				$data=Mage::getModel('customer/customer')->load($custid);
				$defaultbillingid=$data->getData('default_billing');
				$defaultshippingid=$data->getData('default_shipping');
				$result['shipping_address']['customer_address_id']=$defaultshippingid;
				$result['shipping_address']['customer_id']=$custid;
				$result['billing_address']['customer_address_id']=$defaultbillingid;
				$result['billing_address']['customer_id']=$custid;
			}
		} //-----------------------------------------------------------------------------//

        $return['customer_id'] = $result['customer_id'];
        $return['customer_email'] = $result['customer_email'];
        $return['shipping_address']['customer_address_id'] = $result['shipping_address']['customer_address_id'];
        $return['billing_address']['customer_address_id'] = $result['billing_address']['customer_address_id'];

        return $return;

    }

    /**
     *
     * @Created By : Raviraj
     * @Created On : 24-08-2017
     * @Description: Retrieve full order information except payment
     *
     * @param string $orderIncrementId
     * @return array
     */
    public function newinfo($orderIncrementId, $infoType="")
    {
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		//Check order already exists,
		if($infoType!="" && $infoType=="info")
		{
			$order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
			if ($order->getId())
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}

        $order = $this->_initOrder($orderIncrementId);

		//Get only payment information of Order and return,
		if($infoType!="" && $infoType=="payment")
		{
			$resultPayment = "";

			$result = $this->_getAttributes($order, 'order');
			$resultPayment['customer_email'] = $result['customer_email'];
			$resultPayment['remote_ip'] = $result['remote_ip'];

			$k2 = $this->_getAttributes($order->getPayment(), 'order_payment');
			unset($k2['method_instance']);
			$resultPayment['payment'] = $k2;
			$resultPayment['payment']['cc_number_enc'] = $this->decrypt($resultPayment['payment']['cc_number_enc']);

			$resultPayment['billing_address']  = $this->_getAttributes($order->getBillingAddress(), 'order_address');

			return $resultPayment;
		}

		$result = $this->_getAttributes($order, 'order');

		$result['shipping_address'] = $this->_getAttributes($order->getShippingAddress(), 'order_address');
        $result['billing_address']  = $this->_getAttributes($order->getBillingAddress(), 'order_address');
        $result['items'] = array();
		$conn = Mage::getSingleton('core/resource')->getConnection('core_read');
                $_conn = $conn->getConnection();
		// Add custom code for get the address id in magento
		if($result['shipping_address']['quote_address_id']=='')
		{
			$orderid=$result['order_id'];
			if($orderid!='')
			{
				$custid=$conn->fetchAll("SELECT customer_id FROM ".$tablePrefix."sales_flat_order  where entity_id='".$orderid."' ");
				$custid=$custid['0']['customer_id'];
				$data=Mage::getModel('customer/customer')->load($custid);
				$defaultbillingid=$data->getData('default_billing');
				$defaultshippingid=$data->getData('default_shipping');
				$result['shipping_address']['customer_address_id']=$defaultshippingid;
				$result['shipping_address']['customer_id']=$custid;
				$result['billing_address']['customer_address_id']=$defaultbillingid;
				$result['billing_address']['customer_id']=$custid;
			}
		}
		// End Code

		$i=0;
        foreach ($order->getAllItems() as $item)
		{
			$result['items'][$i] = $this->_getAttributes($item, 'order_item');
			$_product = Mage::getModel('catalog/product')->load($result['items'][$i]['product_id']);
			$dist=$_product->getDistributor();
			$result['items'][$i]['distributor'] = $dist;
			$dist=='';
			$i++;
        }

		//Product custom options
		$result['custom_option'] = array();
		$looseDiamondArr = array();
		foreach($result['items'] as $key=>$value)
		{
			$result['custom_option'][$value['product_id']][] = unserialize($value['product_options']);

			//get product custom options for Loose Diamond product in concat in string,
			$productOptions = unserialize($value['product_options']);
			$productOptionsArr = $productOptions["options"];
			$product_options_str = "";
			$product_options_arr = array();
			foreach($productOptionsArr AS $optionsArr)
			{
				$product_options_arr[] = $optionsArr["label"].": ".$optionsArr["value"];
			}
			$product_options_str =  implode(", ",$product_options_arr);
			$looseDiamondArr["item_description"][$value['item_id']] = $product_options_str;
		}
		//return $looseDiamondArr;

		//Concat Loose Diamond product options in product Name,
		$j=0;
        foreach ($order->getAllItems() as $item)
		{
			$subStrSku = substr($result['items'][$j]['sku'], 0, 13);
			if($subStrSku=="loose_diamond")
			{
				$item_id = $result['items'][$j]['item_id'];
				$newProductName = $result['items'][$j]['name']."(".$looseDiamondArr["item_description"][$item_id].")";
				$result['items'][$j]['name'] = $newProductName;
			}
			$j++;
        }

		/*$result['items_new'] = array();
		foreach($result['items'] as $key=>$value){
			$result['items_new'][$key] = json_encode($value);
		}*/


		/***
		 *  For geting the customer password
		 *  based on the customer email and store in to array and
		 *  return into api array**
		 **/
		// now $write is an instance of Zend_Db_Adapter_Abstract
		$sSQLPass  = " SELECT custvar.value FROM ".$tablePrefix."customer_entity AS cstent";
		$sSQLPass .= " INNER JOIN ".$tablePrefix."customer_entity_varchar AS custvar ON custvar.entity_id=cstent.entity_id";
		$sSQLPass .= " WHERE cstent.email=" .$_conn->quote($result['customer_email']). " AND custvar.attribute_id=12";
                $password = $conn->fetchRow($sSQLPass);
		$result['password'] = $password['value'];

        $result['status_history'] = array();

        foreach ($order->getAllStatusHistory() as $history)
		{
            $result['status_history'][] = $this->_getAttributes($history, 'order_status_history');
        }

        //Check order info and get the CC Number from Quote table if does not exists in main order table
        if(($result["payment"]["method"]=="verisign" || $result["payment"]["method"]=="authorizenet") && ($result["payment"]["cc_number_enc"]==""))
        {
			$quote_id = 0;
			$quote_id = $result["quote_id"];
			if($quote_id>0)
			{
				$sqlQuote = "SELECT cc_number_enc FROM ".$tablePrefix."sales_flat_quote_payment WHERE quote_id=".$quote_id;
				$ccNumberRes = $conn->fetchRow($sqlQuote);
				if($ccNumberRes)
				{
					$result["payment"]["cc_number_enc"] = $this->decrypt($ccNumberRes['cc_number_enc']);
					$result["payment"]["cc_number_enc_q"] = $this->decrypt($ccNumberRes['cc_number_enc']);
				}
			}
		}
		//Get customer name from Billing Address if does not exists in Customer info
		if($result["customer_firstname"]=="" || $result["customer_lastname"]=="")
		{
			$result["customer_firstname"]	= $result["billing_address"]["firstname"];
			$result["customer_lastname"]	= $result["billing_address"]["lastname"];
		}
		//Remove CC type form if method is CheckMO and Order is Pending
		if($result["payment"]["method"]=="checkmo" && $result["status"]=="pending")
		{
			$result["payment"]["cc_type"] = "";
			$result["payment"]["cc_last4"] = "";
		}
		else if($result["payment"]["method"]=="paypal_standard" || $result["payment"]["method"]=="paypal_express")
		{
			$result["payment"]["cc_type"] = "";
			$result["payment"]["cc_last4"] = "";
		}
		//get CC Order Score
		$score = "";
		if($result["payment"]["method"]=="authorizenet" && $result["payment"]["cc_type"]!="")
		{
			$orderNumber = $result['increment_id'];
			$score = 'n/a';
		}
		$result['signifyd_score'] = $score;

		//Get Expected Ship Date, Estimated Delivery Date of amazon orders
		if($result["payment"]["method"]=="m2epropayment" || $result["payment"]["method"]=="amazon")
		{
			$amazonInfo = $this->__getAmazonOrderInfo($orderIncrementId);
			$result['amazon_info'] = $amazonInfo;
		}

        return $result;
    }

    /**
     * @Created By : Raviraj
     * @Created On : 24-08-2017
     * @Description: send CURL request to signifyd API to get Credit Card Order score
     *
     */
    public function signifydRequest($url, $auth=null, $contenttype="application/json")
    {
		$debug = false;
		if ($debug)
		{
            echo ("Request:\nURL: $url \nAuth: $auth");
        }
        $curl = curl_init();
        $response = array();
        curl_setopt($curl, CURLOPT_URL, $url);

        if (stripos($url, 'https://') === 0)
        {
			curl_setopt($curl, CURLOPT_PORT, 443);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        if ($auth)
        {
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $auth.":");
        }

        $raw_response = curl_exec($curl);
        $response["raw_response"] = $raw_response;

        $response_data = curl_getinfo($curl);
        $response["response_data"] = $response_data;

		if ($debug)
		{
            echo ("Response ($url):\n " . json_encode($response));
        }

        if ($raw_response === false || curl_errno($curl))
        {
			$error = curl_error($curl);
			if ($debug)
			{
				echo ("ERROR ($url):\n$error");
            }
            $response["error"]= $error;
		}
		curl_close($curl);
		return $response;
    }

    /**
     *
     * @Created By : Raviraj
     * @Created On : 24-08-2017
     * @Description: Submit order info to FraudShield system
     *
     * @param string $orderIncrementId
     * @return sting
     */
    public function submitToFS($orderIncrementId)
    {
		$order = $this->info($orderIncrementId);

		if($order["payment"]["cc_type"]=="")
		{
			return "Order is not a credit card order.";
		}

		//get Credit Cart info
		$cardnum = $order["payment"]["cc_number_enc"];
		$cc_exp_month = $order["payment"]["cc_exp_month"];
		$cc_exp_year = $order["payment"]["cc_exp_year"];
		$CardExp = str_pad($cc_exp_month,2,'0',STR_PAD_LEFT) . substr($cc_exp_year,-2);

		$gatewaySucc = '';
		$gatewayErr = '';
		if($order["payment"]["cc_status"] == 1)
		{
			$gatewaySucc = $order["payment"]["cc_status_description"];
			$gatewayErr = '';
		}
		else
		{
			$gatewaySucc = '';
			$gatewayErr = $order["payment"]["cc_status_description"];
		}

		//Test API for Test credit card number
		/*$cardnum = "4111111111111111";
		$CardExp = "0116";
		$order["payment"]["cc_status"] = 1;
		$gatewaySucc = "This transaction has been approved.";
		$order["payment"]["last_trans_id"] = "1234567890";
		$order["payment"]["cc_avs_status"] = "Z";
		$order["payment"]["cc_cid_status"] = "M";*/

		//get order items
		$OrderItems = array();
		foreach($order["items"] AS $item)
		{
			$OrderItems[] = array(
				'name'=>$item["name"],
				'unitPrice'=>$item["base_price"],
				'sku'=>$item["sku"],
				'qty'=>$item["qty_ordered"]
			);
		}

		//Prepare post data to submit to Fraud Shields
		$postdata = array(
			"order_id"=>$order["increment_id"],
			"customername"=>$order["customer_firstname"]." ".$order["customer_lastname"],//Customer Name
			"customeremail"=>$order["customer_email"],//Customer Email
			"shippingmethod"=>$order["shipping_method"],//Shipping Method
			"shippingamount"=>$order["base_shipping_amount"],//ShippingAmount
			"grandtotal"=>$order["base_grand_total"],//Grand Total
			"domain"=>"www.itshot.com",//Domain name
			"shipping_firstname"=>$order["shipping_address"]["firstname"],
			"shipping_lastname"=>$order["shipping_address"]["lastname"],
			"shipping_company"=>$order["shipping_address"]["company"],
			"shipping_street"=>$order["shipping_address"]["street"],
			"shipping_region"=>$order["shipping_address"]["region"],
			"shipping_city"=>$order["shipping_address"]["city"],
			"shipping_postcode"=>$order["shipping_address"]["postcode"],
			"shipping_telephone"=>$order["shipping_address"]["telephone"],
			"shipping_country_id"=>$order["shipping_address"]["country_id"],
			"billing_firstname"=>$order["billing_address"]["firstname"],
			"billing_lastname"=>$order["billing_address"]["lastname"],
			"billing_company"=>$order["billing_address"]["company"],
			"billing_street"=>$order["billing_address"]["street"],
			"billing_region"=>$order["billing_address"]["region"],
			"billing_city"=>$order["billing_address"]["city"],
			"billing_postcode"=>$order["billing_address"]["postcode"],
			"billing_telephone"=>$order["billing_address"]["telephone"],
			"billing_country_id"=>$order["billing_address"]["country_id"],
			"ip"=>$order["remote_ip"],
			"cardnum"=>$cardnum,
			"exp"=>$CardExp,
			"customer_notes"=>$order["customer_comment"],
			"key"=>"8bfe220e83ec40c5ab7c5f21ca3e8643ItsHot",
			"gift_message"=>$order["gift_message_id"],
			"version"=>"1.2",
			"GatewayRefNum"=>$order["payment"]["last_trans_id"],
			"GatewayResult"=>$gatewaySucc,
			"GatewayError"=>$gatewayErr,
			"GatewayAVS"=>$order["payment"]["cc_avs_status"],
			"GatewayCVV"=>$order["payment"]["cc_cid_status"],
			"OrderItems"=>$OrderItems
		);
		//return $postdata;

		//submit order info to FraudShield
		try
		{
			$json = json_encode($postdata);
			$curl = curl_init('https://x1.fidelipay.com/fs');//Server url where json data send
			curl_setopt($curl, CURLOPT_FAILONERROR, true);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt ($curl, CURLOPT_POSTFIELDS,$json);
			$result = curl_exec($curl);
			return $result;
		}
		catch(Mage_Core_Exception $e)
		{
			$this->_fault('fraudshield_not_submitted', $e->getMessage());
		}
    }//end function



} // Class Mage_Sales_Model_Order_Api End
