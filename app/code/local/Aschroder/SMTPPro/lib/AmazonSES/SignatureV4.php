<?php
/**
 * Class App_Mail_Transport_AmazoneSES_SignatureV4
 */
class App_Mail_Transport_AmazoneSES_SignatureV4 extends Zend_Http_Client
{
    
        const HASH_ALGORITHM = 'sha256';
        public static $SESAlgorithms = [
            self::HASH_ALGORITHM => 'AWS4-HMAC-SHA256',
        ];

    /**
     * Returns header string containing encoded authentication key needed for signature version 4 as described in https://docs.aws.amazon.com/general/latest/gr/signature-version-4.html
     * 
         * @param DateTime $date
         * @param string $region
         * @param string $service
         * @param string $accessKey
         * @param string $privateKey
         * @return  string
         *
     */
    public function buildAuthKey(DateTime $date, $region, $service, $accessKey, $privateKey){
            //Mage::log(__METHOD__);
            $longDate = $date->format('Ymd\THis\Z');
            $shortDate = $date->format('Ymd');

            // Add minimal headers
            $this->setHeaders([
                'Host' => $this->uri->getHost(),
              'X-Amz-Date' => $longDate,
            ]);

            // Task 1: Create a canonical request for Signature Version 4
            // 1. Start with the HTTP request method (GET, PUT, POST, etc.), followed by a newline character.
            $method = $this->method . "\n";

            // 2. Add the canonical URI parameter, followed by a newline character
            $canonicalUri = $this->pathEncode($this->uri->getPath()) . "\n";

            // 3. Add the canonical query string, followed by a newline character. 
            $canonicalQuery = $this->getQuery() . "\n";

            // 4. Add the canonical headers, followed by a newline character. 
            $canonicalHeaders = "";
            $headers = $this->headers;
            ksort($headers, SORT_STRING);
            foreach ($headers as $k => $v) {
                $canonicalHeaders .= $k . ':' . $this->trimAllSpaces($v[1]) . "\n";
            }
            $canonicalHeaders .= "\n";

            // 5. Add the signed headers, followed by a newline character.
            $signedHeaders = implode(';', array_keys($headers)) . "\n";

            // 6. Use a hash (digest) function like SHA256 to create a hashed value from the payload in the body of the HTTP or HTTPS request. 
            $hashedPayload = $this->hash($this->_prepareBody());

            // 7. To construct the finished canonical request, combine all the components from each step as a single string.
            $canonicalRequest = $method . $canonicalUri . $canonicalQuery . $canonicalHeaders . $signedHeaders . $hashedPayload;

            //Mage::log('canonicalRequest:');
            //Mage::log("#####\n" . $canonicalRequest . "\n#####");

            // 8. Create a digest (hash) of the canonical request with the same algorithm that you used to hash the payload.
            $hashedCanonicalRequest = $this->hash($canonicalRequest);

            // Task 2: 
            // 1. Start with the algorithm designation, followed by a newline character.
            $algorithm = self::$SESAlgorithms[self::HASH_ALGORITHM] . "\n";

            // 2. Append the request date value, followed by a newline character.
            $requestDateTime = $longDate . "\n";

            // 3. Append the credential scope value, followed by a newline character. 
            $credentialScope = $shortDate . '/' .$region. '/' .$service. '/aws4_request' . "\n";

            // 4. Append the hash of the canonical request that you created in Task 1: Create a canonical request for Signature Version 4. 
            $stringToSign = $algorithm . $requestDateTime . $credentialScope . $hashedCanonicalRequest;
            
            //Mage::log('stringToSign:');
            //Mage::log("#####\n" . $stringToSign . "\n#####");

            // Task 3: Calculate the signature for AWS Signature Version 4
            // 1. Derive your signing key. 
            $dateKey = hash_hmac(self::HASH_ALGORITHM, $shortDate, 'AWS4' . $privateKey, true);
            $regionKey = hash_hmac(self::HASH_ALGORITHM, $region, $dateKey, true);
            $serviceKey = hash_hmac(self::HASH_ALGORITHM, $service, $regionKey, true);
            $signingKey = hash_hmac(self::HASH_ALGORITHM, 'aws4_request', $serviceKey, true);

            // 2. Calculate the signature.
            $signature = hash_hmac(self::HASH_ALGORITHM, $stringToSign, $signingKey);

            // Task 4: Add the signature to the HTTP request
            // Return string for HTTP Authorization header
            return trim($algorithm, "\n") . ' Credential=' . $accessKey . '/' . trim($credentialScope, "\n") . ', SignedHeaders=' . trim($signedHeaders, "\n") . ', Signature=' . $signature;
    }

        protected function pathEncode($path) {
            $encoded = [];
            foreach (explode('/', $path) as $k => $v) {
                $encoded[] = rawurlencode(rawurlencode($v));
            }
            return implode('/', $encoded);
        }

        protected function trimAllSpaces($text) {
            return trim(preg_replace('| +|', ' ', $text), ' ');
        }

        protected function hash($text) {
            return hash(self::HASH_ALGORITHM, $text);
        }

        protected function getQuery() {
            // From Zend_Http_Client:L946
            // Clone the URI and add the additional GET parameters to it
            $uri = clone $this->uri;
            if (! empty($this->paramsGet)) {
                $query = $uri->getQuery();
                if (! empty($query)) {
                    $query .= '&';
                }
                $query .= http_build_query($this->paramsGet, null, '&');
                if ($this->config['rfc3986_strict']) {
                    $query = str_replace('+', '%20', $query);
                }

                $uri->setQuery($query);
            }
            return $uri->getQuery();
        }
}
