<?php
// magento\app\code\local\ShipRush\OrderButton\controllers

// { $Revision: #5 $ }
// { $Date: 2016/02/18 $ }
// { $File: //depot/main/ShipRush/Current/WebApplications/ShoppingCartsIntegration/Cart_UI_Extensions/Magento/magento-root/app/code/local/ShipRush/OrderButton/controllers/IndexController.php $ }

#################################################################################
# 	
#  (c) 2016 Z-Firm LLC, ALL RIGHTS RESERVED.
#
#  This file is protected by U.S. and international copyright laws. Technologies and techniques herein are
#  the proprietary methods of Z-Firm LLC. 
#         
#         IMPORTANT
#         =========
#         THIS FILE IS RESTRICTED FOR USE IN CONNECTION WITH SHIPRUSH, MY.SHIPRUSH AND OTHER SOFTWARE 
#         PRODUCTS OWNED BY Z-FIRM LLC.  UNLESS EXPRESSLY PERMITTED BY Z-FIRM, ANY USE IS STRICTLY PROHIBITED.
#
#         THIS FILE, AND ALL PARTS OF THIS ZIP/MODULE  
#         ARE GOVERNED BY THE MY.SHIPRUSH TERMS OF SERVICE & END USER LICENSE AGREEMENT.
#         
#         The ShipRush License Agreement can be read here: http://www.zfirm.com/SHIPRUSH-EULA
#         
#         If you do not agree with these terms, this file and related files must be deleted immediately.
#
#         Thank you for using ShipRush!
# 	
################################################################################

require_once 'Mage/Adminhtml/controllers/Sales/OrderController.php';

class ShipRush_OrderButton_IndexController extends Mage_Adminhtml_Sales_OrderController
{

    public function indexAction()
    {
        $magento_logic = false;
        
        $tracking_number = $this->getRequest()->getParam('tnum');
        $order_id = $this->getRequest()->getParam('id');
        $carrier_code = $this->getRequest()->getParam('carrier');
        $service = $this->getRequest()->getParam('stn');
        $carrier = $this->getRequest()->getParam('ctn');
        $order = Mage::getModel('sales/order'); //also add status history, setData
        $order->loadByIncrementId($order_id);
		
		//format carrier/service as appropriate
		$formatted_service = $service;
		$formatted_service_with_space = $service;
				
        if($order->canShip() and $magento_logic == true)
        {
            $itemQtys = array();
            foreach ($order->getAllItems() as $orderItem) {
                if ($orderItem->getQtyToShip() && !$orderItem->getIsVirtual()) {
                    $itemQtys[$orderItem->getId()] = $orderItem->getQtyToShip();
                }
            }
            
            
            $order->save();
            $shipment = Mage::getModel('sales/service_order', $order)
                                ->prepareShipment($itemQtys);
            $shipment = new Mage_Sales_Model_Order_Shipment_Api();
            $shipmentId = $shipment->create($order->getIncrementId()); 
			
			
			
			
				 $trackmodel = Mage::getModel('sales/order_shipment_api')
            ->addTrack($shipmentId, $carrier_code, $formatted_service_with_space, $tracking_number);//ups,  fedex, dhl, flatrate?X, freeshipping?, pickup?, tablerate?
			
                
        }
        if(($order->getState() == 'processing' || $order->getState() == 'pending'  || $order->getState() == 'new') and $magento_logic == false) {
            
			
			if($order->getState() == 'pending' || $order->getState() == 'new')
			{
				$order->setData('state', "processing");
				$order->setStatus("processing");  
				$order->addStatusToHistory('processing', 'Intermediate state change');
				$order->save();
			}
            $order->setData('state', "complete");
            $order->setStatus("complete");  
			
           $history_item=$order->addStatusHistoryComment( 'Shipped on ' . date("m/d/Y") . ' via ' . $formatted_service. ', Tracking number ' . $tracking_number,'complete');
			$history_item->setIsCustomerNotified(1)->save();
			$order->sendOrderUpdateEmail(true, 'Shipped on ' . date("m/d/Y") . ' via ' . $formatted_service . ', Tracking number ' . $tracking_number);
            $order->save();
            $itemQtys = array();
            foreach ($order->getAllItems() as $orderItem) {
                if ($orderItem->getQtyToShip() && !$orderItem->getIsVirtual()) {
                    $itemQtys[$orderItem->getId()] = $orderItem->getQtyToShip();
                }
            }
            $shipment = $order->getShipmentsCollection()->getFirstItem();
			$shipmentId = $shipment->getIncrementId();
					
			if(!isset($shipmentId))
			{
				$shipment = Mage::getModel('sales/service_order', $order)
									->prepareShipment($itemQtys);
				$shipment = new Mage_Sales_Model_Order_Shipment_Api();
				$shipmentId = $shipment->create($order->getIncrementId(), $itemQtys ,'Shipped via ' . $formatted_service . ', Tracking number ' . $tracking_number , false, 1);  //, $itemQtys ,'Shipped via ' . $carrier . '[' . $service . ']' . ', Tracking number ' . $tracking_number , false, 1           
			}         
            $trackmodel = Mage::getModel('sales/order_shipment_api')->addTrack($shipmentId, $carrier_code, $formatted_service_with_space, $tracking_number);
        }
        if ($trackmodel){
            $track = Mage::getModel('sales/order_shipment_track')->load($trackmodel);
            if($track->getId()){
                $track->setNotifyEmail(true)
                        ->save();
            }
        }
        $this->_redirect('adminhtml/sales_order/view', array('order_id' => $order->getEntityId(), 'tNum' => $tracking_number, 'state' => $order->getState()));
    }
}
?>	