<?php
/**
 * magento\app\code\local\ShipRush\OrderButton\Model
 */

// { $Revision: #5 $ }
// { $Change: 98272 $    }
// { $Date: 2016/12/23 $ }
// { $File: //depot/main/ShipRush/Current/WebApplications/ShoppingCartsIntegration/Cart_UI_Extensions/Magento/magento-root/app/code/local/ShipRush/OrderButton/Model/Observer.php $ }

#################################################################################
# 	
#  (c) 2015 Z-Firm LLC, ALL RIGHTS RESERVED.
#
#  This file is protected by U.S. and international copyright laws. Technologies and techniques herein are
#  the proprietary methods of Z-Firm LLC. 
#         
#         IMPORTANT
#         =========
#         THIS FILE IS RESTRICTED FOR USE IN CONNECTION WITH SHIPRUSH, MY.SHIPRUSH AND OTHER SOFTWARE 
#         PRODUCTS OWNED BY Z-FIRM LLC.  UNLESS EXPRESSLY PERMITTED BY Z-FIRM, ANY USE IS STRICTLY PROHIBITED.
#
#         THIS FILE, AND ALL PARTS OF THIS ZIP/MODULE  
#         ARE GOVERNED BY THE MY.SHIPRUSH TERMS OF SERVICE & END USER LICENSE AGREEMENT.
#         
#         The ShipRush License Agreement can be read here: http://www.zfirm.com/SHIPRUSH-EULA
#         
#         If you do not agree with these terms, this file and related files must be deleted immediately.
#
#         Thank you for using ShipRush!
# 	
################################################################################
 
class ShipRush_OrderButton_Model_Observer
{
    /**
     * Magento passes a Varien_Event_Observer object as
     * the first parameter of dispatched events.
     */
    public function adminhtmlWidgetContainerHtmlBefore($event) {
        $block = $event->getBlock();
        $mageordermodel = Mage::getModel('sales/order');
        $mageshipmodel = Mage::getModel('sales/order_address');

        $order = $mageordermodel->loadByIncrementId(100000001); //or load(1)

        $x = $order->getItemscollection()->getFirstItem()->sku;
        $y = 'qwerty';
        $params = Mage::app()->getRequest()->getParams();
        $onclick = "(function(e){invoke_connect()})(event)";

        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {

            $block->addButton('do_something_crazy', array(
                'label'     => 'Ship w ShipRush',
                'onclick'   => $onclick,
                'class'     => 'myshiprush_button'
            ));
        }
    }
}