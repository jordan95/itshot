<?php
// Keep this file -- do not delete -- may be needed later for button layout purposes

// magento\app\code\local\ShipRush\OrderButton\Block\Sales\Order\View

// { $Revision: #1 $ }
// { $Change: 82808 $    }
// { $Date: 2015/09/18 $ }
// { $File: //depot/main/ShipRush/Current/WebApplications/ShoppingCartsIntegration/Cart_UI_Extensions/Magento/magento-root/app/code/local/ShipRush/OrderButton/Block/Sales/Order/View/info.php $ }

#################################################################################
# 	
#  (c) 2015 Z-Firm LLC, ALL RIGHTS RESERVED.
#
#  This file is protected by U.S. and international copyright laws. Technologies and techniques herein are
#  the proprietary methods of Z-Firm LLC. 
#         
#         IMPORTANT
#         =========
#         THIS FILE IS RESTRICTED FOR USE IN CONNECTION WITH SHIPRUSH, MY.SHIPRUSH AND OTHER SOFTWARE 
#         PRODUCTS OWNED BY Z-FIRM LLC.  UNLESS EXPRESSLY PERMITTED BY Z-FIRM, ANY USE IS STRICTLY PROHIBITED.
#
#         THIS FILE, AND ALL PARTS OF THIS ZIP/MODULE  
#         ARE GOVERNED BY THE MY.SHIPRUSH TERMS OF SERVICE & END USER LICENSE AGREEMENT.
#         
#         The ShipRush License Agreement can be read here: http://www.zfirm.com/SHIPRUSH-EULA
#         
#         If you do not agree with these terms, this file and related files must be deleted immediately.
#
#         Thank you for using ShipRush!
# 	
################################################################################


// class ShipRush_OrderButton_Block_Sales_Order_View_Info extends Mage_Adminhtml_Block_Sales_Order_View_Info
// {
    // protected function _construct()
    // {
       // $this->setTemplate('shiprush/sales/order/view/view.phtml');
    // }
// }
// ?>