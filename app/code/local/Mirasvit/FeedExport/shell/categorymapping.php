<?php


$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (! file_exists($path . $shell) && ! file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Mirasvit_FeedExport_CategoryMapping extends Mage_Shell_Abstract
{
    public function run()
    {
        Mage::setIsDeveloperMode(true);
        $file = 'livecategoryexport.csv';
        $file = Mage::app()->getConfig()->getTempVarDir().'/export/'.$file;
        $csv = new Varien_File_Csv();
        echo "Parsing file $file\n";

        $skus = false;
        $skuIndex = false;
        foreach ($csv->getData($file) as $line => $row) {
            if (0 == $line) {
                foreach ($row as $i=>$rowHeader) {
                    $headerRow[$i] = $rowHeader;
                }
                continue; // skip header row
            }
        }

        foreach ($csv->getData($file) as $line => $row) {
            if (0 == $line) {
                foreach ($row as $i => $rowHeader) {
                    $headerRow[$i] = $rowHeader;
                }
                continue; // skip header row
            }

            $categoryName = $row[1];
            $googleCategoryName = $row[2];

            $categoryCollection = Mage::getModel('catalog/category')->getCollection()->addFieldToFilter('name', $categoryName);
            if ($categoryCollection->count()>0) {
                $categoryId = $categoryCollection->getFirstItem()->getId();
            }

            if ($categoryId) {
                $mapping[$categoryId] = $googleCategoryName;
            }
        }

        $model = Mage::getModel('feedexport/dynamic_category');
        $model->load(1);

        $model->setData('mapping', $mapping);
        $model->save();

        echo "done\n";
    }

}

$shell = new Mirasvit_FeedExport_CategoryMapping();
$shell->run();