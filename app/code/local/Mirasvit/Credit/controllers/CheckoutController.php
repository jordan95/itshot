<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_credit
 * @version   1.0.16
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */


// @codingStandardsIgnoreFile
require_once Mage::getBaseDir('code') . '/core/Mage/Checkout/controllers/CartController.php';

class Mirasvit_Credit_CheckoutController extends Mage_Checkout_CartController
{
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    public function applyPostAction()
    {
        $this->_processPost();

        if ($this->getRequest()->isXmlHttpRequest()) {
            $response = [
                'success' => 1
            ];
            $this->getResponse()->setBody(Zend_Json::encode($response));
            return;
        }
        $this->_goBack();
    }

    protected function _processPost()
    {
        $isUseCredit = true;

        if ($this->getRequest()->getParam('remove-credit') == 1) {
            $isUseCredit = false;
        }

        $quote = Mage::getModel('checkout/cart')->getQuote();
        $quote->setUseCredit($isUseCredit)
            ->collectTotals()
            ->save();
//        if (Mage::helper('core')->isModuleEnabled('Mirasvit_Rewards')) {
//            /** @var Mirasvit_Rewards_Model_Purchase $purchase */
//            $purchase = Mage::helper('rewards/purchase')->getByQuote($quote);
//            if ($purchase) {
//                $purchase->refreshPointsNumber(true)
//                    ->save();
//            }
//        }
    }
}
