<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_credit
 * @version   1.0.16
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



class Mirasvit_Credit_Model_Total_Invoice_Credit extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        $order = $invoice->getOrder();

        #credit amount AND credit amount not set for invoice
        if ($order->getBaseCreditAmount()
            && floatval($order->getBaseCreditInvoiced()) == 0
        ) {
            $baseUsed = $order->getBaseCreditAmount();
            $used = $order->getCreditAmount();

            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal())
                ->setGrandTotal($invoice->getGrandTotal())
                ->setBaseDiscountAmount($invoice->getBaseDiscountAmount() + $baseUsed) #credit already in discount
                ->setDiscountAmount($invoice->getDiscountAmount() + $used) #credit already in discount
                ->setBaseCreditAmount($baseUsed)
                ->setCreditAmount($used);
        }

        return $this;
    }
}
