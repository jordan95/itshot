<?php

/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_credit
 * @version   1.0.16
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */
class Mirasvit_Credit_Model_Total_Creditmemo_Credit extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{

    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
        $creditmemo->setBaseCreditTotalRefunded(0)
                ->setCreditTotalRefunded(0)
                ->setBaseCreditReturnMax(0)
                ->setCreditReturnMax(0);

        $order = $creditmemo->getOrder();

        $left = 0;
        $grandTotal = 0;
        $creditAmount = 0;
        $baseCreditAmount = 0;
        $creditItemsQty = $this->getItemsQty($creditmemo);
        if (floatval($order->getBaseCreditAmount()) > 0 && floatval($order->getBaseCreditInvoiced()) > 0) {
            $baseDiscountAmount = 0;
            foreach ($order->getAllItems() as $_orderItem) {
                $baseDiscountAmount += $_orderItem->getBaseDiscountAmount();
            }
            $percent_credit_discount = $order->getBaseCreditAmount() / $baseDiscountAmount;
            $left = $order->getBaseCreditInvoiced() - $order->getBaseCreditRefunded();

            // Magento does not include fully discounted shipping in discount
            $grandTotal = $creditmemo->getBaseGrandTotal();
            if ($creditmemo->getBaseShippingAmount() && $creditmemo->getBaseShippingAmount() == $grandTotal) {
                $grandTotal = 0;
            }

            if ($left >= $grandTotal) {
                $baseUsed = $left + $grandTotal;
                $used = $left + $grandTotal;
            } else {
                $baseUsed = $order->getBaseCreditInvoiced() - $order->getBaseCreditRefunded();
                $used = $order->getCreditInvoiced() - $order->getCreditRefunded();
            }

            /** @var Mage_Sales_Model_Order_Creditmemo_Item $item */
            foreach ($creditmemo->getAllItems() as $item) {
                $orderItem = $item->getOrderItem();
                $creditAmount += $item->getQty() * ($percent_credit_discount * $orderItem->getDiscountAmount() / $orderItem->getQtyOrdered());
                $baseCreditAmount += $item->getQty() * ($percent_credit_discount * $orderItem->getBaseDiscountAmount() / $orderItem->getQtyOrdered());
            }
            $creditmemo->setCreditAmount($creditAmount);
            $creditmemo->setBaseCreditAmount($baseCreditAmount);
            $creditmemo->setAllowZeroGrandTotal(true);
        }
        $creditmemo->setDiscountAmount($creditmemo->getDiscountAmount() + $creditAmount);
        $creditmemo->setBaseDiscountAmount($creditmemo->getBaseDiscountAmount() + $baseCreditAmount);

        $creditmemo->setBaseCreditReturnMax($grandTotal + $left);

        $creditmemo->setCreditReturnMax($creditmemo->getBaseCreditReturnMax());

        if (!$creditmemo->getCreditAmount() || !$creditmemo->getBaseGrandTotal()) {
            $creditmemo->setAllowZeroGrandTotal(true);
        }

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     * @return int
     */
    private function getItemsQty($creditmemo)
    {
        $qty = 0;
        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($creditmemo->getOrder()->getItemsCollection() as $item) {
            $qty += $item->getQtyInvoiced() - $item->getQtyRefunded();
        }

        return $qty;
    }

}
