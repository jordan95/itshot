<?php

/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_credit
 * @version   1.0.16
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */
if (Mage::helper('core')->isModuleEnabled('TBT_Rewards')) {

    class Mirasvit_Credit_Model_Rewrite_Order_Status_Pure extends TBT_Rewards_Model_Sales_Order
    {
        
    }

} else {

    class Mirasvit_Credit_Model_Rewrite_Order_Status_Pure extends Mage_Sales_Model_Order
    {
        
    }

}

class Mirasvit_Credit_Model_Rewrite_Order_Status extends Mirasvit_Credit_Model_Rewrite_Order_Status_Pure
{

    /**
     * {@inheritdoc}
     */
    protected function _checkState()
    {
        parent::_checkState();

        if (
                0 == $this->getBaseGrandTotal() &&
                floatval($this->getCreditInvoiced()) > 0 &&
                $this->getCreditInvoiced() == $this->getCreditRefunded() &&
                $this->getState() === self::STATE_COMPLETE &&
                $this->hasCreditmemos()
        ) {
            $userNotification = $this->hasCustomerNoteNotify() ? $this->getCustomerNoteNotify() : null;

            $this->_setState(self::STATE_CLOSED, true, '', $userNotification);
        }

        return $this;
    }

}
