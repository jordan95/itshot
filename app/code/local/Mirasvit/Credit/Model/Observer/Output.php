<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_credit
 * @version   1.0.16
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



class Mirasvit_Credit_Model_Observer_Output
{
    public function afterOutput($obj)
    {
        $block = $obj->getEvent()->getBlock();
        $transport = $obj->getEvent()->getTransport();

        if (empty($transport)) {
            return $this;
        }

        $this->appendCartCreditBlock($block, $transport);

        if (Mage::app()->getRequest()->getModuleName() == 'onestepcheckout') {
            $this->appendOnestepcheckoutCreditBlock($block, $transport);
        }

        return $this;
    }

    /**
     * @param Mage_Core_Block_Abstract $block
     * @param Varien_Object            $transport
     * @return $this
     */
    public function appendOnestepcheckoutCreditBlock($block, $transport)
    {
        if ($block->getBlockAlias() == 'choose-payment-method') {
            $b = $block->getLayout()
                ->createBlock('credit/checkout_cart_credit', 'credit')
                ->setTemplate('mst_credit/checkout/credit.phtml');

            $html = $transport->getHtml();
            $ourhtml = strip_tags($b->toHtml(), '<div><span><input><button><p>');
            if (!$this->isBlockInserted) {
                $html = $html . $ourhtml;
                $this->isBlockInserted = true;
            }
            $transport->setHtml($html);
        }
    }

    protected $isBlockInserted = false;

    /**
     * @param Mage_Core_Block_Abstract $block
     * @param Varien_Object            $transport
     * @return $this
     */
    public function appendCartCreditBlock($block, $transport)
    {
        if (Mage::app()->getRequest()->getControllerName() != 'cart'
            && Mage::app()->getRequest()->getControllerName() != 'checkout_cart'
            && Mage::app()->getRequest()->getModuleName() != 'ajaxcoupon') {
            return $this;
        }
        
        if ($this->isAllowed($block)) {
            $b = $block->getLayout()
                ->createBlock('credit/checkout_cart_credit', 'credit')
                ->setTemplate('mst_credit/checkout/cart/credit.phtml');

            $html = $transport->getHtml();
            $ourhtml = $b->toHtml();
            if (!$this->isBlockInserted) {
                $html = $ourhtml . $html;
                $this->isBlockInserted = true;
            }
            $transport->setHtml($html);
        }

        return $this;
    }

    /**
     * @param Mage_Core_Block_Abstract $block
     * @return bool
     */
    protected function isAllowed($block)
    {
        if (Mage::getModel('credit/config')->getCreditBlock()) {
            if (
                $block->getTemplateFile() == Mage::getModel('credit/config')->getCreditBlock() ||
                $block->getTemplate() == Mage::getModel('credit/config')->getCreditBlock()
            ) {
                return true;
            }
        } elseif ($block->getBlockAlias() == 'coupon' || $block->getBlockAlias() == 'crosssell') {
            return true;
        }

        return false;
    }
}
