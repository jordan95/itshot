<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_credit
 * @version   1.0.16
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



class Mirasvit_Credit_Model_Config
{
    /**
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return bool
     */
    public function getNotificationBalanceUpdateEnabled($store = null)
    {
        return Mage::getStoreConfig('credit/notification/send_balance_update', $store)
            ? true
            : false;
    }

    /**
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return mixed|string
     */
    public function getNotificationBalanceUpdateTemplate($store = null)
    {
        return Mage::getStoreConfig('credit/notification/balance_update_template', $store)
            ? Mage::getStoreConfig('credit/notification/balance_update_template', $store)
            : 'credit_balance_update';
    }

    /**
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return mixed
     */
    public function isSendToFriendEnabled($store = null)
    {
        return Mage::getStoreConfig('credit/general/send_to_friend_enabled', $store);
    }

    /**
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return mixed
     */
    public function isAutoRefundEnabled($store = null)
    {
        return Mage::getStoreConfig('credit/general/auto_refund_enabled', $store);
    }

    /**
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return mixed
     */
    public function isApplyCreditToTax($store = null)
    {
        return Mage::getStoreConfig('credit/general/appy_credits_to_tax', $store);
    }

    /**
     * @param null|string|bool|int|Mage_Core_Model_Store $store
     * @return mixed
     */
    public function getCreditBlock($store = null)
    {
        return Mage::getStoreConfig('credit/general/assign_credit_to_block', $store);
    }
}
