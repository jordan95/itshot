<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_credit
 * @version   1.0.16
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



class Mirasvit_Credit_Block_Checkout_Cart_Credit extends Mage_Core_Block_Template
{
    /**
     * @return Mage_Customer_Model_Customer
     */
    protected function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    /**
     * @return float|bool
     */
    public function getBalance()
    {
        if ($this->getCustomer()->getId() > 0) {
            return Mage::getModel('credit/balance')->loadByCustomer($this->getCustomer());
        }

        return false;
    }

    /**
     * @return float|int
     */
    public function getAmountToUse()
    {
        $toUse = $this->getQuoteAmountToUse();

        if ($toUse > $this->getBalance()->getAmount()) {
            $toUse = $this->getBalance()->getAmount();
        }

        return $toUse;
    }

    /**
     * @return float|int
     */
    public function getUsedAmount()
    {
        return Mage::getModel('checkout/cart')->getQuote()->getCreditAmountUsed();
    }

    /**
     * @return float|int
     */
    private function getQuoteAmountToUse()
    {
        $toUse = 0;
        /** @var Mirasvit_Credit_Helper_Item $itemHelpder */
        $itemHelpder = Mage::helper('credit/item');

        /** @var Mage_Sales_Model_Quote $quote */
        $quote = Mage::getModel('checkout/cart')->getQuote();

        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach ($quote->getAllItems() as $item) {
            $toUse += $itemHelpder->calcItemPrice($item);
        }

        /** @var Mage_Sales_Model_Quote_Address $shipping */
        foreach ($quote->getAllShippingAddresses() as $shipping) {
            $toUse += $shipping->getBaseShippingAmount() - $shipping->getBaseShippingDiscountAmount();
            if (Mage::getModel('credit/config')->isApplyCreditToTax()) {
                $toUse += $shipping->getBaseShippingTaxAmount();
            }
        }

        return $toUse;
    }
}
