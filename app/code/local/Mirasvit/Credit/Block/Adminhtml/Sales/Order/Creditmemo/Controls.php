<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_credit
 * @version   1.0.16
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



class Mirasvit_Credit_Block_Adminhtml_Sales_Order_Creditmemo_Controls extends Mage_Core_Block_Template
{
    /**
     * @return bool
     */
    public function canRefundToCredit()
    {
        $order = Mage::registry('current_creditmemo')->getOrder();
        if ($order->getCustomerIsGuest()) {

            // Also check, whether customer with the same email was registered after placing of order
            $customer = Mage::getModel("customer/customer");
            $customer->setWebsiteId($order->getStore()->getWebsiteId());
            $customer->loadByEmail($order->getCustomerEmail());
            if ($customer->getId()) {
                return true;
            }

            return false;
        }

        return true;
    }

    /**
     * @return int
     */
    public function getReturnValue()
    {
        $max = Mage::registry('current_creditmemo')->getCreditReturnMax();

        if ($max) {
            return $max;
        }

        return 0;
    }
}
