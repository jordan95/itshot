<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_credit
 * @version   1.0.16
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



class Mirasvit_Credit_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @return bool
     */
    public function isAutoRefundEnabled()
    {
        return Mage::getSingleton('credit/config')->isAutoRefundEnabled();
    }

    /**
     * @param array $array
     * @return string
     */
    public function createTransactionMessage($array)
    {
        $arMessage = array();

        if (isset($array['order'])) {
            $order = $array['order'];

            $arMessage[] = Mage::helper('credit')->__('Order #%s', 'o|' . $order->getIncrementId());
        }

        if (isset($array['creditmemo'])) {
            $memo = $array['creditmemo'];

            $arMessage[] = Mage::helper('credit')->__('Creditmemo #%s', 'm|' . $memo->getIncrementId());
        }

        return implode(', ', $arMessage);
    }

    /**
     * @param string $message
     * @return string
     */
    public function getBackendTransactionMessage($message)
    {
        return $this->getPreparedTransactionMessage($message, 'adminhtml');
    }

    /**
     * @param string $message
     * @return string
     */
    public function getFrontendTransactionMessage($message)
    {
        return $this->getPreparedTransactionMessage($message, 'frontend');
    }

    /**
     * @param string $message
     * @return string
     */
    public function getEmailTransactionMessage($message)
    {
        return $this->getPreparedTransactionMessage($message, 'email');
    }

    /**
     * @param string $message
     * @param string $type
     * @return string
     */
    public function getPreparedTransactionMessage($message, $type)
    {
        $message = $this->highlightOrdersInMessage($message, $type);
        $message = $this->highlightMemosInMessage($message, $type);
        $message = $this->highlightRmasInMessage($message, $type);

        return $message;
    }

    /**
     * @param string $message
     * @param string $type
     * @return string
     */
    protected function highlightOrdersInMessage($message, $type)
    {
        $orderMatches = array();
        preg_match_all('/#o\|([0-9]*)/is', $message, $orderMatches);

        if (count($orderMatches) && isset($orderMatches[1])) {
            foreach ($orderMatches[1] as $key => $incrementId) {
                $order = Mage::getModel('sales/order')->getCollection()
                    ->addFieldToFilter('main_table.increment_id', $incrementId)
                    ->getFirstItem();

                $url = false;
                if ($type == 'adminhtml') {
                    $url = Mage::helper('adminhtml')->getUrl(
                        'adminhtml/sales_order/view',
                        array('order_id' => $order->getId())
                    );
                } elseif ($type == 'frontend') {
                    $url = Mage::getUrl('sales/order/view', array('order_id' => $order->getId()));
                }

                if ($url) {
                    $replace = "<a href='$url' target='_blank'>#$incrementId</a>";
                } else {
                    $replace = "#$incrementId";
                }

                $message = str_replace($orderMatches[0][$key], $replace, $message);
            }
        }

        return $message;
    }

    /**
     * @param string $message
     * @param string $type
     * @return string
     */
    protected function highlightMemosInMessage($message, $type)
    {
        $memoMatches = array();
        preg_match_all('/#m\|([0-9]*)/is', $message, $memoMatches);

        if (count($memoMatches) && isset($memoMatches[1])) {
            foreach ($memoMatches[1] as $key => $incrementId) {
                $memo = Mage::getModel('sales/order_creditmemo')->getCollection()
                    ->addFieldToFilter('main_table.increment_id', $incrementId)
                    ->getFirstItem();

                $url = false;
                if ($type == 'adminhtml') {
                    $url = Mage::helper('adminhtml')->getUrl(
                        'adminhtml/sales_creditmemo/view',
                        array('creditmemo_id' => $memo->getId())
                    );
                } elseif ($type == 'frontend') {
                    $url = Mage::getUrl('sales/order/creditmemo', array('order_id' => $memo->getOrderId()));
                }

                if ($url) {
                    $replace = "<a href='$url' target='_blank'>#$incrementId</a>";
                } else {
                    $replace = "#$incrementId";
                }

                $message = str_replace($memoMatches[0][$key], $replace, $message);
            }
        }

        return $message;
    }

    /**
     * @param string $message
     * @param string $type
     * @return string
     */
    protected function highlightRmasInMessage($message, $type)
    {
        $rmaMatches = array();
        preg_match_all('/#r\|([0-9]*)/is', $message, $rmaMatches);

        if (count($rmaMatches) && isset($rmaMatches[1])) {
            foreach ($rmaMatches[1] as $key => $incrementId) {
                $rma = Mage::getModel('rma/rma')->getCollection()
                    ->addFieldToFilter('increment_id', $incrementId)
                    ->getFirstItem();

                $url = false;
                if ($type == 'adminhtml') {
                    $url = Mage::helper('adminhtml')->getUrl(
                        'adminhtml/rma_rma/edit',
                        array('id' => $rma->getId())
                    );
                } elseif ($type == 'frontend') {
                    $url = Mage::getUrl('returns/guest/view', array('guest_id' => $rma->getGuestId()));
                }

                if ($url) {
                    $replace = "<a href='$url' target='_blank'>#$incrementId</a>";
                } else {
                    $replace = "#$incrementId";
                }

                $message = str_replace($rmaMatches[0][$key], $replace, $message);
            }
        }

        return $message;
    }
}
