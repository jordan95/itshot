<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/extension_credit
 * @version   1.0.16
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



class Mirasvit_Credit_Helper_Item
{
    /**
     * @return Mirasvit_Credit_Model_Config
     */
    protected function getConfig()
    {
        return Mage::getSingleton('credit/config');
    }

    /**
     * @param Mage_Sales_Model_Quote_Item $item
     * @return float
     */
    public function calcItemBasePrice($item)
    {
        if (!($itemBasePrice = $this->calcPrice($item))) {
            return 0;
        }
        if ($this->getConfig()->isApplyCreditToTax()) {
//            if (!Mage::getModel('tax/config')->applyTaxAfterDiscount()) {
                $itemBasePrice = $item->getBaseRowTotalInclTax() ?: $itemBasePrice + (float)$item->getData('base_tax_amount');
//            }
            $itemBasePrice += $item->getBaseWeeeTaxAppliedRowAmount();
        }

        return $itemBasePrice - $item->getDiscountAmount();
    }

    /**
     * @param Mage_Sales_Model_Quote_Item $item
     * @return float
     */
    public function calcItemPrice($item)
    {
        if (!($itemPrice = $this->calcPrice($item))) {
            return 0;
        }
        if ($this->getConfig()->isApplyCreditToTax()) {
//            if (!Mage::getModel('tax/config')->applyTaxAfterDiscount()) {
                $itemPrice = $item->getRowTotalInclTax() ?: $itemPrice + (float)$item->getData('tax_amount');
//            }
            $itemPrice += $item->getWeeeTaxAppliedRowAmount();
        }

        return $itemPrice - $item->getDiscountAmount();
    }

    /**
     * @param Mage_Sales_Model_Quote_Item $item
     * @return float
     */
    protected function calcPrice($item)
    {
        $itemPrice = 0;
        $parentItem = null;
        if ($item->getParentItemId()) {
            $parentItem = Mage::getModel('sales/quote_item')->load($item->getParentItemId());
            $parentProduct = Mage::getModel('catalog/product')->load($parentItem->getProductId());
            if ($parentProduct->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
                return $itemPrice;
            }
        }
        $priceIncludesTax = Mage::helper('tax')->priceIncludesTax();
        if ($parentItem) {
            $quantity = $parentItem->getQty();
        } else {
            $quantity = $item->getQty();
        }
        if ($priceIncludesTax) {
            $itemPrice = $item->getBasePriceInclTax() * $quantity;
        } else {
            $itemPrice = $item->getBasePrice() * $quantity;
        }

        return $itemPrice;
    }
}
