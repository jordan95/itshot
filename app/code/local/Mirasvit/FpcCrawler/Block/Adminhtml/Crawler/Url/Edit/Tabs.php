<?php 
class Mirasvit_FpcCrawler_Block_Adminhtml_Crawler_Url_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('crawler_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('fpccrawler')->__('Crawler tabs'));
	}
	protected function _beforeToHtml() {

            $this->addTab('item_fpccrawler_details', array(
                'label' => Mage::helper('fpccrawler')->__('Item fpccrawler'),
                'title' => Mage::helper('fpccrawler')->__('Item fpccrawler'),
                'content' => $this->getLayout()->createBlock('fpccrawler/adminhtml_crawler_url_edit_tabs_form')->toHtml()
            ));

        return parent::_beforeToHtml();
    }
}
 ?>