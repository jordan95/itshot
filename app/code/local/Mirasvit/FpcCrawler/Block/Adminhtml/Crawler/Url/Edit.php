<?php 
class Mirasvit_FpcCrawler_Block_Adminhtml_Crawler_Url_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'fpccrawler';
        $this->_controller = 'adminhtml_crawler_url';
        $this->_headerText = Mage::helper('fpccrawler')->__('Crawler URLs');
    }
}
 ?>