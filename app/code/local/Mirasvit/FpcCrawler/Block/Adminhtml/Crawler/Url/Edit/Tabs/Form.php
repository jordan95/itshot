<?php 
class Mirasvit_FpcCrawler_Block_Adminhtml_Crawler_Url_Edit_Tabs_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form(array(
				'id' => 'edit_form',
				'action' => $this->getUrl('*/*/save', array('massupdate'=>$this->getRequest()->getParam('massupdate'))),
				'method' => 'post',
				'enctype' => 'multipart/form-data'
					)
				);
		$item = Mage::registry('fpccrawler');
		$this->setForm($form);
		$fieldset = $form->addFieldset('detail_form',array('legend' => Mage::helper('shipout')->__('Item fpccrawler')));

		$fieldset->addField('url', 'text', array(
            'label'     => Mage::helper('fpccrawler')->__('Url'),
            'name' => 'url',
            'required' => true,
        ));
		$fieldset->addField('store_id', 'text', array(
            'label'     => Mage::helper('fpccrawler')->__('Store id'),
            'name' => 'store_id',
            'required' => true
        ));
		return parent::_prepareForm();
	}
	protected function getOptionStores()
	{
		$stores = Mage::app()->getStores();
		$data = array(''=>$this->__('---Select---'));
		foreach($stores as $store)
		{
			$data[$store->getId()] = $store->getName();
		}
		return $data;
	}
}
 ?>