<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Boughttogether
 * @version    1.1.13
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


/**
 * Frequently Bought Together Data Helper
 */
class AW_Boughttogether_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Retrives List html code
     * @return string
     */
	public function getHtml()
    {
		return  Mage::getSingleton('core/layout')
            ->createBlock('boughttogether/list')
            ->setTemplate('boughttogether/list.phtml')
            ->toHtml()
        ;
	}

	/*
	 * Compare param $version with magento version
	 */
	public function checkVersion($version)
	{
		return version_compare(Mage::getVersion(), $version, '>=');
	}

    /**
     * Retrives Extansion state
     * @return boolean
     */
    public function isDisabled()
    {
        return !!Mage::getStoreConfig('advanced/modules_disable_output/AW_Boughttogether');
    }
}