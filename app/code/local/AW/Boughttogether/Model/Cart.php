<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Boughttogether
 * @version    1.1.13
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


/**
 * Frequently Bought Together Cart Model
 */
class AW_Boughttogether_Model_Cart extends Mage_Checkout_Model_Cart
{
    /**
     * Check array of products for Composite Product Type
     * @param array $productIds array of product's id
     * @return boolean
     */
    protected function _isSomebodyComposite( $productIds )
    {
        foreach ( $productIds as $productId ) {
            $product = $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);
            if ( $product && ($product->isComposite() || $this->_hasRequiredOptions($product)) ) {
                return true;
            }
        }
        return false;
    }

    protected function _hasRequiredOptions(Mage_Catalog_Model_Product $product)
    {
        if ($options = $product->getProductOptionsCollection()) {
            foreach ($options as $option) {
                if ($option->getIsRequire()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get array of product ids.
     * If product not need option, then it will go to cart with success mmessage.
     * If product need to option's selection print message about it.
     *
     * @param array|int  $productIds
     * @return AW_Boughttogether_Model_Cart
     */
    public function addProductsByIds($productIds)
    {
        if (!$productIds || !is_array($productIds)) {
            return $this;
        }

        if (!$this->_isSomebodyComposite($productIds)) {
            return parent::addProductsByIds($productIds);
        }

        try {
            foreach ($productIds as $productId) {
                $product = Mage::getModel('catalog/product')
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->load($productId)
                ;

                if (null === $product->getId() || !$product->isSaleable()) {
                    continue;
                }

                $message = Mage::helper('checkout')->__(
                    '%s was successfully added to your shopping cart.', $product->getName()
                );

                if (!$product->isComposite() && !$this->_hasRequiredOptions($product)) {
                    $this->addProduct($product);
                    $this->getCheckoutSession()->addSuccess($message);
                } else if ( $this->_hasRequiredOptions($product) && $options = Mage::app()->getRequest()->getParam('related_options') ) {
                    $this->addProduct($product, ['options' => $options]);
                    $this->getCheckoutSession()->addSuccess($message);
                } else {
                    $link = '<a href="' . Mage::getUrl('catalog/product/view', array('id' => $productId)) . '">'
                        . $product->getName() . '</a>'
                    ;
                    $error = Mage::helper('boughttogether')->__(
                        'Product %s can\'t be added to cart. Please specify the product option(s) first.', $link
                    );
                    $this->getCheckoutSession()->addError($error);
                }
            }
        } catch (Exception $e) {
            $this->getCheckoutSession()->addException($e, $e->getMessage());
        }
        return $this;
    }

    public function addProduct($productInfo, $requestInfo=null)
    {
        if (isset($requestInfo['isBt']) && isset($requestInfo['main_options'])) {
            foreach ($requestInfo['main_options'] as $key => $value) {
                $requestInfo['options'][$key] = $value;
            }
        }
        parent::addProduct($productInfo, $requestInfo);
    }

}