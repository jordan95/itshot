<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Boughttogether
 * @version    1.1.13
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


/**
 * Frequently Bought Together List
 */
class AW_Boughttogether_Block_List extends Mage_Catalog_Block_Product_List_Related
{
    /**
     * Path to config
     */
    const CONFIG_LIMIT_PATH = 'boughttogether/general/limit';

    /**
     * Path to empty template
     */
    const TEMPLATE_EMPTY = 'boughttogether/empty.phtml';

    /**
     * Current ptoduct
     *
     * @var Mage_Catalog_Model_Product
     */
    protected $_thisProduct;

    /**
     * Array with optional products
     *
     * @var array
     */
    protected $_optionalProducts;

    public function disableRelated()
    {
        if (Mage::helper('boughttogether')->checkVersion('1.4.1.0') || Mage::helper('boughttogether')->isDisabled()) {
            return $this;
        }
        $block = $this->getLayout()->getBlock('catalog.product.related');
        if ($block) {
            $block->setTemplate(self::TEMPLATE_EMPTY);
        }
        return $this;
    }

    /**
     * Prepare data to show
     *
     * @return AW_Boughttogether_Block_List
     */
    protected function _prepareData()
    {
        parent::_prepareData();
        $this->_thisProduct = $this->getProduct();
        $this->_optionalProducts = array();
        $configLimit = intval(Mage::getStoreConfig(self::CONFIG_LIMIT_PATH));
        $itemsTotal = 0;
        foreach ($this->_itemCollection as $_key => $_item) {
            if (!$_item->isSaleable()) {
                $this->_itemCollection->removeItemByKey($_key);
                continue;
            }
            if ($configLimit) {
                $itemsTotal += 1;
                if ($itemsTotal >= $configLimit) {
                    $this->_itemCollection->removeItemByKey($_key);
                }
            }
        }
        return $this;
    }

    /**
     * Retrives block html code
     *
     * @return string
     */
    protected function _beforeToHtml()
    {
        $this->_prepareData();
        $this->disableRelated();
        return parent::_beforeToHtml();
    }
}