<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Store model
 *
 * @method Mage_Core_Model_Resource_Store _getResource()
 * @method Mage_Core_Model_Resource_Store getResource()
 * @method Mage_Core_Model_Store setCode(string $value)
 * @method Mage_Core_Model_Store setWebsiteId(int $value)
 * @method Mage_Core_Model_Store setGroupId(int $value)
 * @method Mage_Core_Model_Store setName(string $value)
 * @method int getSortOrder()
 * @method Mage_Core_Model_Store setSortOrder(int $value)
 * @method Mage_Core_Model_Store setIsActive(int $value)
 *
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Medialounge_Core_Model_Core_Store extends Mage_Core_Model_Store
{
      private $_currency = false;
    /**
     * Get current store currency code
     *
     * @return string
     */
     public function setCurrentCurrencyCode($code)
     {
         $code = strtoupper($code);
         if (in_array($code, $this->getAvailableCurrencyCodes())) {
             $this->_getSession()->setCurrencyCode($code);
             $this->_currency = $code;//for setting currency within the request scope
             if ($code == $this->getDefaultCurrency()) {
                 Mage::app()->getCookie()->delete(self::COOKIE_CURRENCY, $code);
             } else {
                 Mage::app()->getCookie()->set(self::COOKIE_CURRENCY, $code);
             }
         }
         return $this;
     }
    public function getCurrentCurrencyCode()
    {
        if ($this->_currency) {//because in the case of varnish, we skip currency from session so we need this to get curreny within request scope
          return $this->_currency;
        }

        // try to get currently set code among allowed
        if( Mage::getStoreConfig('mgt_varnish/mgt_varnish/enabled') ) {
            $code = Mage::app()->getCookie()->get('currency');
        } else {
            $code = $this->_getSession()->getCurrencyCode();
        }

        if (empty($code)) {
            $code = $this->getDefaultCurrencyCode();
        }
        if (in_array($code, $this->getAvailableCurrencyCodes(true))) {
            return $code;
        }

        // take first one of allowed codes
        $codes = array_values($this->getAvailableCurrencyCodes(true));
        if (empty($codes)) {
            // return default code, if no codes specified at all
            return $this->getDefaultCurrencyCode();
        }
        return array_shift($codes);
    }

}
