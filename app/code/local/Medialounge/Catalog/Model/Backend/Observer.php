<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Catalog_Model_Backend_Observer
{
    /**
     * @param $observer
     */
    public function productSaveAfter($observer)
    {
        $product = $observer->getEvent()->getProduct();
        if (!is_null($product->getSku())) {
            /** @var Mage_Catalog_Model_Product $product */
            if (($product->getOrigData('status') == Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                and ($product->getData('status') == Mage_Catalog_Model_Product_Status::STATUS_DISABLED)) {
                $this->getCatalogUrlRewriteHelper()->createUrlRewrite($product);
            }

            if (($product->getOrigData('status') == Mage_Catalog_Model_Product_Status::STATUS_DISABLED)
                and ($product->getData('status') == Mage_Catalog_Model_Product_Status::STATUS_ENABLED)) {
                $this->getCatalogUrlRewriteHelper()->deleteCustomUrlRewrite($product);
            }
        }
    }

    /**
     * @param $observer
     */
    public function productBatchSaveAfter($observer)
    {
        if (is_array($observer->getEvent()->getProductIds()))
        {
            $productIds = $observer->getEvent()->getProductIds();

            $this->getCatalogUrlRewriteHelper()->addUrlRewriteToDisabledProduct($productIds);
            $this->getCatalogUrlRewriteHelper()->clearCustomUrlRewriteToEnabledProduct($productIds);
        }
    }

    /**
     * @return Medialounge_Catalog_Helper_Urlrewrite
     */
    private function getCatalogUrlRewriteHelper()
    {
        return Mage::helper('medialounge_catalog/urlrewrite');
    }

}