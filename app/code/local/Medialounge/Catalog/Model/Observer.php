<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Catalog_Model_Observer
{
    public function productViewBefore(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Controller_Front_Action $action */
        $action         = $observer->getData('controller_action');
        /** @var Mage_Core_Controller_Request_Http $request */
        $request        = $action->getRequest();

        if ($this->isProductDisabled($request->getParam('id'))) {
            if ($urlRewrite = $this->getCatalogUrlRewriteHelper()->getCustomPermanentUrlRewrite($request->getParam('id'),Mage::app()->getStore()->getId())) {
                $action->getResponse()->setRedirect($urlRewrite->getData('target_path'), 301);
            } else {
                $product = $this->getProductModel();
                $product->load($request->getParam('id'));
                $this->getCatalogUrlRewriteHelper()->createUrlRewriteForStore($product, Mage::app()->getStore());
                $category = $this->getCatalogUrlRewriteHelper()->getFirstCategory(Mage::app()->getStore(),$product);
                $action->getResponse()->setRedirect($this->getCatalogUrlRewriteHelper()->getCategoryUrl($category), 301);
            }

            $action->getRequest()->setDispatched(true);
            return;
        }
    }

    /**
     * @param $productId
     * @return bool
     */
    private function isProductDisabled($productId)
    {
        // if the product is disabled but has no permanent custom rewrite rule, we will define the permanent redirect now
        $statusAttribute = $this->getEavConfigModel()->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'status');

        $resource = $this->getResource();
        $connection = $resource->getConnection('core_read');

        $select = $connection->select();
        $select->from($statusAttribute->getBackendTable())->where('entity_id=?', $productId);
        $select->reset(Varien_Db_Select::COLUMNS)->columns(array('status' => 'value'));
        $select->where('store_id in (?)',array(Mage::app()->getStore()->getId(), Mage_Core_Model_App::ADMIN_STORE_ID));
        $select->where('attribute_id=?', $statusAttribute->getId());
        $select->where('value=?', Mage_Catalog_Model_Product_Status::STATUS_DISABLED);

        $result = $connection->fetchOne($select);

        return $result == Mage_Catalog_Model_Product_Status::STATUS_DISABLED;
    }

    /**
     * @return Medialounge_Catalog_Helper_Urlrewrite
     */
    private function getCatalogUrlRewriteHelper()
    {
        return Mage::helper('medialounge_catalog/urlrewrite');
    }

    /**
     * @return Mage_Catalog_Model_Product
     */
    private function getProductModel()
    {
        return Mage::getModel('catalog/product');
    }

    /**
     * @return Mage_Core_Model_Resource
     */
    private function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    /**
     * @return Mage_Eav_Model_Config
     */
    private function getEavConfigModel()
    {
        return Mage::getSingleton('eav/config');
    }
}