<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog category helper
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Medialounge_Catalog_Helper_Category extends Mage_Catalog_Helper_Category
{
    /**
     * Check if a category can be shown
     *
     * @param  Mage_Catalog_Model_Category|int $category
     * @return boolean
     */
    public function canShow($category)
    {
        /* Don't show if any of parents hidden */
        if ($this->hasHiddenParentCat($category)) {
            return false;
        }
//        if(!Mage::helper('opentechizcatalog')->_hasProducts($category)){
//            return false;
//        }

        return parent::canShow($category);
    }

    public function hasHiddenParentCat($category) {
        $pathIds = array_reverse(explode(',', $category->getPathInStore()));
        $hiddenParentCats = Mage::getResourceModel('catalog/category_collection')
            ->setStore(Mage::app()->getStore())
            ->addAttributeToSelect('name')
            ->addFieldToFilter('entity_id', array('in' => $pathIds))
            ->addFieldToFilter('is_active', 0);
        return $hiddenParentCats->getSize() > 0;
    }

    public function isCategoryReferer() {
        $refererUrl = parse_url(Mage::helper('core/http')->getHttpReferer());
        if(!isset($refererUrl['path'])) {
            return false;
        }
//        $refererCategory = Mage::getModel('catalog/category')->getCollection()
//            ->addNameToResult()
//            ->addUrlRewriteToResult()
//            ->addAttributeToFilter('url_key',ltrim($refererUrl['path'], '/') )
//            ->getFirstItem();

        $refererCategory = Mage::getModel('catalog/category')->loadByAttribute('url_key', ltrim($refererUrl['path'], '/'));

        return $refererCategory ? true : false;
    }
}
