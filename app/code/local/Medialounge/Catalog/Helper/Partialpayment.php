<?php

/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 24/07/2017
 * Time: 15:00
 */
class Medialounge_Catalog_Helper_Partialpayment extends Mage_Core_Helper_Abstract
{

    public function getProductInitialPayment(Mage_Catalog_Model_Product $_product)
    {
        $initialPaymentPercentage = Mage::getStoreConfig('partialpayment/general_settings/partialpayment_first');

        if ($_product->getFinalPrice()) {
            return number_format($_product->getFinalPrice() * ($initialPaymentPercentage / 100), 2, '.', ',');
        }
    }
}
