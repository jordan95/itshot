<?php

class Medialounge_Catalog_Helper_Varnish extends Mage_Core_Helper_Abstract
{
    public function purgeUrl(){
        if(!isset($_SERVER['REQUEST_URI'])) {
            return;
        }
        $host = 'www.itshot.com';
        $varnish = Mage::getModel('mgt_varnish/varnish');
        $varnish->purge($host, sprintf('^%s$', $_SERVER['REQUEST_URI']));
//        $command = sprintf('varnishadm -T 172.26.7.129:6082 "ban req.url ~ %s"', $url);
//        system($command);

    }
}