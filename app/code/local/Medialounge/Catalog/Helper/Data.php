<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Catalog_Helper_Data extends Mage_Core_Helper_Abstract
{
    private static $numberRelatedProduct = 3;


    public function getOrderRelatedProducts(Mage_Sales_Model_Order $order)
    {
        $items = $order->getAllItems();
        $storeId = $order->getData('store_id');

        $ids = array();
        foreach ($items as $item) {
            /** @var Mage_Sales_Model_Order_Item $item */
            $ids[] = $item->getData('product_id');
        }

        if (count($ids)) {
            $orderRelatedInfo = $this->getRelatedProductAPIModel()->getRelatedProductsFor($ids, $storeId);
            return $this->getMedialoungeCatalogHelper()->getProductCollectionFromAheadRelatedAPIInfo($orderRelatedInfo);
        }

        return false;
    }

    /**
     * @param $orderRelatedInfo
     * @return bool|Mage_Catalog_Model_Resource_Product_Collection
     */
    private function getProductCollectionFromAheadRelatedAPIInfo($orderRelatedInfo)
    {
        $relatedIds = array();
        foreach ($orderRelatedInfo as $productId => $rank) {
            $relatedIds[$rank] = $productId;
        }
        krsort($relatedIds);

        $i = 0;
        $list = array();
        foreach ($relatedIds as $productId) {
            $list[] = $productId;
            $i++;
            if ($i > self::$numberRelatedProduct) break;
        }

        if ($list) {
            $collection = $this->getProductModel()->getCollection();
            $collection->addIdFilter($list);
            $collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
            $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
            return $collection;
        }

        return false;
    }

    /**
     * @return Mage_Catalog_Model_Product
     */
    private function getProductModel()
    {
        return Mage::getModel('catalog/product');
    }

    /**
     * @return AW_Relatedproducts_Model_Api
     */
    private function getRelatedProductAPIModel()
    {
        return Mage::getModel('relatedproducts/api');
    }

    /**
     * @return \Medialounge_Catalog_Helper_Data
     */
    private function getMedialoungeCatalogHelper()
    {
        return Mage::helper('medialounge_catalog');
    }

    public function getCurrentOrder()
    {
        if (!$order = Mage::registry('current_order')) {
            $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
            if ($orderId) {
                $order = Mage::getModel('sales/order')->load($orderId);
                if ($order->getId()) {
                    $isVisible = !in_array($order->getState(),
                        Mage::getSingleton('sales/order_config')->getInvisibleOnFrontStates());

                    if ($isVisible) {
                        Mage::register('current_order', $order);
                    }
                }
            }
        }

        return $order;
    }

    public function canPurchaseProduct($product)
    {
        if (!$product->getData('c2c_disp_custom_stock_msg')) {
            $saleable = true;
        } else {
            $saleable = false;
        }
        
        // return $saleable;
        
        return true;
    }
}