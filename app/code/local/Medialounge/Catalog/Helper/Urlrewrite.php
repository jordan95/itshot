<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Catalog_Helper_Urlrewrite extends Mage_Core_Helper_Abstract
{
    /**
     * @param $product
     */
    public function createUrlRewrite(Mage_Catalog_Model_Product $product)
    {
        if ($stores = $product->getData('store_ids')) {
            foreach ($stores as $storeId) {
                $store = Mage::app()->getStore($storeId);

                $this->createUrlRewriteForStore($product, $store);
            }
        }
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     */
    public function deleteCustomUrlRewrite(Mage_Catalog_Model_Product $product)
    {
        $resource = $this->getResource();
        $connection = $resource->getConnection('core_write');

        $cond[] = $connection->quoteInto('product_id=?',$product->getId());
        $cond[] = $connection->quoteInto('is_system=?',0);
        $cond[] = $connection->quoteInto('options=?','RP');
        $cond[] = "description like 'disabled sku: ".$product->getSku()." to now redirect to its first category:%'";

        $connection->delete($resource->getTableName('core/url_rewrite'),$cond);
        $this->getCatalogUrlModel()->refreshProductRewrite($product->getId());
    }

    public function getFirstCategory(Mage_Core_Model_Store $store,Mage_Catalog_Model_Product $product)
    {
        $resource = $this->getResource();
        $connection = $resource->getConnection('core_read');

        $select = $connection->select()->from($resource->getTableName('catalog/category_product'))
            ->where('product_id=?', $product->getId())
            ->reset(Varien_Db_Select::COLUMNS)
            ->columns('category_id')
            ->limit(1);

        if ($categoryId = $connection->fetchOne($select)) {
            /** @var Mage_Catalog_Model_Category $category */
            $category = Mage::getModel('catalog/category');
            $category->load($categoryId);
            $category->setStoreId($store->getId());
            return $category;
        }

        return false;
    }

    /**
     * @param $firstcategory
     * @return string
     */
    public function getCategoryUrl($firstcategory)
    {
        return $this->getCatalogCategoryUrlModel()->getCategoryUrl($firstcategory);
    }

    /**
     * @param Mage_Core_Model_Store $store
     * @param string $requestPath
     * @return Mage_Core_Model_Url_Rewrite
     */
    private function getProductUrlRewrite(Mage_Core_Model_Store $store, $requestPath)
    {
        $urlRewrite = $this->getCoreUrlRewriteModel();
        $urlRewrite->setStoreId($store->getId());
        $urlRewrite->loadByRequestPath($requestPath);
        return $urlRewrite;
    }

    /**
     * @return Mage_Catalog_Model_Product
     */
    private function getProductModel()
    {
        return Mage::getModel('catalog/product');
    }

    /**
     * @return Mage_Core_Model_Resource
     */
    private function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    /**
     * @param $store
     * @param $product
     * @param $requestPath
     * @param $firstcategory
     * @return Mage_Core_Model_Url_Rewrite
     */
    protected function createUrlPermanentRedirectToFirstCategory($store, $product, $firstcategory)
    {
        $urlRewrite = $this->getCoreUrlRewriteModel();
        $urlRewrite->setData('store_id', $store->getId());
        $urlRewrite->setData('id_path', sprintf('product/%s', $product->getId()));
        $urlRewrite->setData('request_path', $product->getData('request_path'));
        $urlRewrite->setData('target_path', $this->getCategoryUrl($firstcategory));
        $urlRewrite->setData('is_system', 0);
        $urlRewrite->setData('options', 'RP');
        $urlRewrite->setData('description',
            sprintf('disabled sku: %s to now redirect to its first category: %s', $product->getSku(),
                $firstcategory->getName()));
        $urlRewrite->setData('product_id', $product->getId());
        $urlRewrite->save();
        return $urlRewrite;
    }

    /**
     * @param $productIds
     */
    public function addUrlRewriteToDisabledProduct($productIds)
    {
        $resource = $this->getResource();
        $connection = $resource->getConnection('core_read');

        $cond = array();
        $cond[] = sprintf('core_url_rewrite.product_id=%s.entity_id', Mage_Catalog_Model_Resource_Product_Collection::MAIN_TABLE_ALIAS);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = $this->getProductModel()->getCollection();
        $collection->addAttributeToSelect('url_key');
        $collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
        $collection->addIdFilter($productIds);

        foreach ($collection as $product) {
            /** @var Mage_Catalog_Model_Product $product */
            $store = Mage::app()->getStore($product->getData('store_id'));

            $this->createUrlRewriteForStore($product,$store);
        }
    }

    /**
     * @param $productIds
     */
    public function clearCustomUrlRewriteToEnabledProduct($productIds)
    {
        $collection = $this->getCustomPermanentUrlRewriteCollection($productIds);

        if ($collection->count()>0) {
            $this->deleteUrlRewrite($collection->getColumnValues('url_rewrite_id'));
        }
    }

    /**
     * @param $resource
     * @param $collection
     */
    private function deleteUrlRewrite($urlRewriteIds)
    {
        $resource = $this->getResource();
        $connection = $resource->getConnection('core_write');

        $cond = array();
        $cond[] = $connection->quoteInto('url_rewrite_id in (?)',$urlRewriteIds);
        $cond[] = $connection->quoteInto('is_system=?', 0);
        $cond[] = $connection->quoteInto('options=?', 'RP');
        $cond[] = "description like '% to now redirect to its first category%'";

        $connection->delete($resource->getTableName('core/url_rewrite'), $cond);
    }

    /**
     * @param $productIds
     * @return object
     */
    public function getCustomPermanentUrlRewriteCollection($productIds)
    {
        $collection = $this->getCoreUrlRewriteModel()->getCollection();
        $collection->addFieldToFilter('is_system', 0);
        $collection->addFieldToFilter('options', 'RP');
        $collection->addFieldToFilter('description', array('like'=>'% to now redirect to its first category%'));
        $collection->addFieldToFilter('product_id', $productIds);

        return $collection;
    }

    /**
     * @param $productIds
     * @return object
     */
    public function getCustomPermanentUrlRewrite($productId,$storeId)
    {
        $resource = $this->getResource();
        $connection = $resource->getConnection('core_read');

        $cond = array();
        $cond[] = $connection->quoteInto('is_system=?', 0);
        $cond[] = $connection->quoteInto('options=?', 'RP');
        $cond[] = $connection->quoteInto('store_id=?', $storeId);
        $cond[] = $connection->quoteInto('id_path=?', "product/$productId");
        $cond[] = "description like '% to now redirect to its first category%'";

        $select = $connection->select();
        $select->from(array('core_url_rewrite' => $resource->getTableName('core/url_rewrite')));
        $select->where(implode(' AND ',$cond));

        $row = $connection->fetchRow($select);

        $result =  new Varien_Object();
        if (isset($row['url_rewrite_id'])) {
            $result->setData($row);
        }

        return $result;
    }

    /**
     * @param $productIds
     * @return object
     */
    public function getMainProductUrlRewrite(Mage_Catalog_Model_Product $product,Mage_Core_Model_Store $store)
    {
        $resource = $this->getResource();
        $connection = $resource->getConnection('core_read');

        $cond = array();
        $cond[] = $connection->quoteInto('is_system=?', 1);
        $cond[] = $connection->quoteInto('options=?', '');
        $cond[] = $connection->quoteInto('store_id=?', $store->getId());
        $cond[] = $connection->quoteInto('id_path=?', "product/{$product->getId()}");

        $select = $connection->select();
        $select->from(array('core_url_rewrite' => $resource->getTableName('core/url_rewrite')));
        $select->where(implode(' AND ',$cond));

        $row = $connection->fetchRow($select);

        $result =  new Varien_Object();
        if (isset($row['url_rewrite_id'])) {
            $result->setData($row);
        }

        return $result;
    }


    /**
     * @param Mage_Core_Model_Store $store
     * @return Mage_Core_Model_Abstract
     */
    private function getRootCategory(Mage_Core_Model_Store $store)
    {
        $rootCategoryId = $store->getRootCategoryId();
        return Mage::getModel('catalog/category')->load($rootCategoryId);
    }

    /**
     * @return Mage_Catalog_Model_Category_Url
     */
    private function getCatalogCategoryUrlModel()
    {
        return Mage::getModel('catalog/category_url');
    }

    /**
     * @return Mage_Catalog_Model_Url
     */
    private function getCatalogUrlModel()
    {
        return Mage::getModel('catalog/url');
    }

    /**
     * @return Mage_Core_Model_Url_Rewrite
     */
    protected function getCoreUrlRewriteModel()
    {
        return Mage::getModel('core/url_rewrite');
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param $store
     */
    public function createUrlRewriteForStore(Mage_Catalog_Model_Product $product,Mage_Core_Model_Store $store)
    {
        if ($firstcategory = $this->getFirstCategory($store, $product)) {
            $requestPath = $this->getCatalogUrlModel()->getProductRequestPath($product, $this->getRootCategory($store));
            $urlRewrite = $this->getProductUrlRewrite($store, $requestPath);
            if ($urlRewrite->getId()) {
                $urlRewrite->delete();
            }
            $product->setData('request_path', $requestPath);
            $urlRewrite = $this->createUrlPermanentRedirectToFirstCategory($store, $product, $firstcategory);
        }
    }


}