<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */

$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (!file_exists($path . $shell) && !file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Catalog_Product_Item extends Mage_Shell_Abstract
{
    public function run()
    {
        $sku = $this->getArg('sku');

        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addAttributeToSelect('c2c_disp_custom_stock_msg');

        if ($sku) {
            $collection->addAttributeToFilter('sku', $sku);
        }

        foreach ($collection as $product) {
            $stockItem  = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            if (($stockItem->getIsInStock()==false) and (!$product->getData('c2c_disp_custom_stock_msg'))) {
                echo 'repaid in stock flag';
                $stockItem->setData('is_in_stock', 1);
            }

            $stockItem->setData('use_config_backorders', 1);
            $stockItem->save();
        }

        echo 'complete';
    }


}

$test = new Catalog_Product_Item;
$test->run();