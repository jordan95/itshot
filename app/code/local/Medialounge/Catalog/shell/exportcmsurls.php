<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */

$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (!file_exists($path . $shell) && !file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Export_CmsUrls extends Mage_Shell_Abstract
{
    public function run()
    {
        $filename = 'cmspageurls.csv';
        $newFile = new Varien_Io_File();
        $newFile->cd(Mage::app()->getConfig()->getTempVarDir().'/export/');
        $newFile->streamOpen($filename,'w');

        $csvHeader = array();

        $csvHeader[] ='cms title';
        $csvHeader[] ='page url';
        $newFile->streamWriteCsv($csvHeader);

        $identifier = $this->getArg('identifier');

        $collection = $this->getCmsPageModel()->getCollection();
        $collection->addFieldToSelect('title');
        $collection->addFieldToSelect('identifier');

        if ($identifier) {
            $collection->addFieldToFilter('identifier',$identifier);
        }

        foreach ($collection as $cmspage) {
            /** @var Mage_Cms_Block_Page $cmspage */
            $csvRow['title'] = $cmspage->getData('title');
            $csvRow['identifier'] = $cmspage->getData('identifier');
            $newFile->streamWriteCsv($csvRow);
        }

        $newFile->streamClose();
        echo 'complete';

    }

    /**
     * @return Mage_Cms_Model_Page
     */
    private function getCmsPageModel()
    {
        return Mage::getModel('cms/page');
    }
}

$test = new Export_CmsUrls;
$test->run();