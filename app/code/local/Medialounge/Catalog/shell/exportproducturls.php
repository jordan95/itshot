<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */

$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (!file_exists($path . $shell) && !file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Export_ProductUrls extends Mage_Shell_Abstract
{
    public function run()
    {
        $filename = 'producturls.csv';
        $newFile = new Varien_Io_File();
        $newFile->cd(Mage::app()->getConfig()->getTempVarDir().'/export/');
        $newFile->streamOpen($filename,'w');

        $csvHeader = array();

        $csvHeader[] ='product sku';
        $csvHeader[] ='product url';
        $newFile->streamWriteCsv($csvHeader);

        $sku = $this->getArg('sku');

        $collection = $this->getCatalogProductModel()->getCollection();
        $collection->addAttributeToSelect('sku');
        $collection->addAttributeToSelect('url_key');

        if ($sku) {
            $collection->addFieldToFilter('sku',$sku);
        }

        foreach ($collection as $product) {
            /** @var Mage_Catalog_Model_Product $product */
            $csvRow['sku'] = $product->getData('sku');
            $csvRow['url_key'] = $product->getData('url_key');
            $newFile->streamWriteCsv($csvRow);
        }

        $newFile->streamClose();
        echo 'complete';

    }

    /**
     * @return Mage_Catalog_Model_Product
     */
    private function getCatalogProductModel()
    {
        return Mage::getModel('catalog/product');
    }
}

$test = new Export_ProductUrls;
$test->run();