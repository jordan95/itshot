<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */

$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (!file_exists($path . $shell) && !file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Reconcile_QA extends Mage_Shell_Abstract
{
    public function run()
    {
        /** @var Mage_Core_Model_Resource $resource */
        /*$resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_read');

        $select = $connection->select()->from($resource->getTableName('oldcatalog_product_entity'))
            ->reset(Varien_Db_Select::COLUMNS)
            ->columns(['entity_id', 'sku']);

        $skus = $connection->fetchPairs($select);*/

        //print_r($skus);

        $collectionQA = Mage::getModel('pws_productqa/productqa')
            ->getCollection();

        echo 'count '.$collectionQA->count()."\n";
        foreach ($collectionQA as $qa) {
            $productQuestionProductId = $qa->getData('product_id');
            $sku = $qa->getData('sku');
            $currentProductId = Mage::getModel('catalog/product')->getIdBySku($sku);

            if ($currentProductId != $productQuestionProductId) {
                echo sprintf('question id: %s, sku: %s, product id: %s and should be %s'."\n", $qa->getId(), $sku, $productQuestionProductId, $currentProductId);
                $qa->setData('product_id', $currentProductId);
                $qa->save();
            }
            /*if (isset($skus[$productQuestionProductId])) {
                $sku = $skus[$productQuestionProductId];
                $currentProductId = Mage::getModel('catalog/product')->getIdBySku($sku);
                if ($currentProductId != $productQuestionProductId) {
                    print_r($qa->getData());
                    die();
                }
            }*/
        }

        echo 'complete';
    }
}

$test = new Reconcile_QA;
$test->run();
