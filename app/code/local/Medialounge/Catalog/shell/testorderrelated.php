<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */

$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (!file_exists($path . $shell) && !file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Print_Order_Related extends Mage_Shell_Abstract
{
    public function run()
    {
        $order = Mage::getModel('sales/order')->load(6);

        if ($order instanceof Mage_Sales_Model_Order) {
            $items = $order->getAllItems();
            $storeId = $order->getData('store_id');

            $ids = array();
            foreach ($items as $item) {
                $ids[] = $item->getData('product_id');
            }

            $orderRelatedInfo = $this->getRelatedProductAPIModel()->getRelatedProductsFor($ids,$storeId);

        }

        echo 'complete';
    }

    /**
     * @return AW_Relatedproducts_Model_Api
     */
    private function getRelatedProductAPIModel()
    {
        return Mage::getModel('relatedproducts/api');
    }
}

$test = new Print_Order_Related;
$test->run();