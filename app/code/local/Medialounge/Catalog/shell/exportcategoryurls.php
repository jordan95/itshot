<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */

$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (!file_exists($path . $shell) && !file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Export_CategoryUrls extends Mage_Shell_Abstract
{
    public function run()
    {
        $filename = 'categoryurls.csv';
        $newFile = new Varien_Io_File();
        $newFile->cd(Mage::app()->getConfig()->getTempVarDir().'/export/');
        $newFile->streamOpen($filename,'w');

        $csvHeader = array();

        $csvHeader[] ='category name';
        $csvHeader[] ='category url';
        $newFile->streamWriteCsv($csvHeader);

        $categoryName = $this->getArg('category_name');

        $collection = $this->getCatalogCategoryModel()->getCollection();
        $collection->addAttributeToSelect('name');
        $collection->addAttributeToSelect('url_key');

        if ($categoryName) {
            $collection->addFieldToFilter('name',$categoryName);
        }

        foreach ($collection as $category) {
            /** @var Mage_Catalog_Model_Category $category */
            $csvRow['name'] = $category->getData('name');
            $csvRow['url_key'] = $category->getData('url_key');
            $newFile->streamWriteCsv($csvRow);
        }

        $newFile->streamClose();
        echo 'complete';

    }

    /**
     * @return Mage_Catalog_Model_Category
     */
    private function getCatalogCategoryModel()
    {
        return Mage::getModel('catalog/category');
    }
}

$test = new Export_CategoryUrls;
$test->run();