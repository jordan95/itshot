<?php


$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (! file_exists($path . $shell) && ! file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Medialounge_DiamondSearch extends Mage_Shell_Abstract
{
    public function run()
    {
        $cmsPage = Mage::getModel('cms/page');
        $cmsPage->setTitle('Loose Diamonds Search');
        $cmsPage->setIdentifier('loose-diamonds-search.aspx');
        $cmsPage->setRootTemplate('one_column');
        $cmsPage->setContent('<p>Top content {{block type="medialounge_catalog/diamond_search" name="catalog.diamond.search" template="medialounge/search/header.phtml" }} Footer content</p>');
        $cmsPage->save();

        $blockPermission = Mage::getModel('admin/block');
        $blockPermission->setBlockName('medialounge_catalog/diamond_search');
        $blockPermission->setIsAllowed(1);
        $blockPermission->save();

        echo "done\n";
    }

}

$shell = new Medialounge_DiamondSearch();
$shell->run();