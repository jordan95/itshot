<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 11/05/2017
 * Time: 17:16
 */
class Medialounge_Catalog_Block_Product extends Mage_Catalog_Block_Product_View
{
    private function getAttributeSet()
    {
        return Mage::getModel('eav/entity_attribute_set')->load($this->getProduct()->getAttributeSetId());
    }

    public function getProductWarranty()
    {
        switch ($this->getAttributeSet()->getAttributeSetName()) {
            case 'Watch':
                $value = $this->getProduct()->getData('c2c_waranty');
                break;
            default:
                $value = '1 year warranty from ItsHot.com';
        }
        return $value;
    }

    public function getProductSize()
    {
        switch ($this->getAttributeSet()->getAttributeSetName()) {
            case 'Watch':
                $value = $this->getProduct()->getData('c2c_size');
                break;
            default:
                $width = $this->getProduct()->getData('c2c_width_1');
                $height = $this->getProduct()->getData('c2c_height_in');
                $length = $this->getProduct()->getData('c2c_length_2');

                $value = '';

                if ($width) {
                    if ($value) $value .= ', ';
                    $value .= sprintf('Width: %s', $width);
                }

                if ($length) {
                    if ($value) $value .= ', ';
                    $value .= sprintf('Length: %s', $length);
                }

                if ($height) {
                    if ($value) $value .= ', ';
                    $value .= sprintf('Height: %s', $height);
                }
        }
        return $value;
    }

    public function getProductGiftBox()
    {
        switch ($this->getAttributeSet()->getAttributeSetName()) {
            case 'Watch':
                $value = 'No, watches come in original boxes.';
                break;
            default:
                $value = 'Yes, gift box/pouch supplied.';

        }
        return $value;
    }

    public function canPurchaseProduct()
    {
        $product = $this->getProduct();

        if (!$product->getData('c2c_disp_custom_stock_msg'))
        {
            $saleable = true;
        } else {
            $saleable = false;
        }
        
        // return $saleable;
        
        return true;
    }
}