<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Catalog_Block_Subcategory extends Mage_Catalog_Block_Product_Abstract
{
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $this->getLayout()->createBlock('catalog/breadcrumbs');

        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $category = $this->getCurrentCategory();
            if ($title = $category->getMetaTitle()) {
                $headBlock->setTitle($title);
            }
            if ($description = $category->getMetaDescription()) {
                $headBlock->setDescription($description);
            }
            if ($keywords = $category->getMetaKeywords()) {
                $headBlock->setKeywords($keywords);
            }
            if ($this->helper('catalog/category')->canUseCanonicalTag()) {
                $headBlock->addLinkRel('canonical', $category->getUrl());
            }
            /*
            want to show rss feed in the url
            */
            if ($this->IsRssCatalogEnable() && $this->IsTopCategory()) {
                $title = $this->helper('rss')->__('%s RSS Feed',$this->getCurrentCategory()->getName());
                $headBlock->addItem('rss', $this->getRssLink(), 'title="'.$title.'"');
            }
        }

        return $this;
    }

    /**
     * Retrieve current category model object
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getCurrentCategory()
    {
        if (!$this->hasData('current_category')) {
            $this->setData('current_category', Mage::registry('current_category'));
        }
        return $this->getData('current_category');
    }

    public function IsRssCatalogEnable()
    {
        return Mage::getStoreConfig('rss/catalog/category');
    }

    public function IsTopCategory()
    {
        return $this->getCurrentCategory()->getLevel()==2;
    }

    public function getRssLink()
    {
        return Mage::getUrl('rss/catalog/category',
            array(
                'cid' => $this->getCurrentCategory()->getId(),
                'store_id' => Mage::app()->getStore()->getId()
            )
        );
    }

    /**
     * @return bool|Varien_Data_Collection
     */
    public function getSubCategoryList()
    {
        if (!$this->hasData('sub_category_list')) {
            if ($this->getCurrentCategory()) {
                /** @var $currentCategory Mage_Catalog_Model_Category */
                /** @var Mage_Catalog_Model_Resource_Category_Collection $categories */
                $categories = $this->getCurrentCategory()->getCollection();
                $categories->addAttributeToSelect('url_key')
                    ->addAttributeToSelect('thumbnail')
                    ->addAttributeToSelect('description')
                    ->addAttributeToSelect('ml_featured_subcategory')
                    ->addAttributeToSelect('name')
                    ->addAttributeToSelect('all_children')
                    ->addAttributeToSelect('is_anchor')
                    ->addAttributeToSelect('intro_text')
                    ->setOrder('position', Varien_Db_Select::SQL_ASC)
                    ->joinUrlRewrite()
                    ->addAttributeToFilter('is_active', 1)
                    ->addIdFilter($this->getCurrentCategory()->getChildren());

            } else {
                $categories = new Varien_Data_Collection();
            }

            $featuredCategories = new Varien_Data_Collection();
            foreach ($categories as $category) {
                if ($category->getData('ml_featured_subcategory')) {
                    $featuredCategories->addItem($category);
                }
            }

            foreach ($featuredCategories as $featuredCategory) {
                $categories->removeItemByKey($featuredCategory->getId());
            }

            $this->setData('sub_category_list',$categories);
            $this->setData('sub_featured_category_list',$featuredCategories);
        }

        return $this->getData('sub_category_list');
    }

    public function getFeaturedSubCategories()
    {
        $this->getSubCategoryList();

        return $this->getData('sub_featured_category_list');
    }

    public function getCategoryImageUrl($url)
    {
        return Mage::getBaseUrl('media').'catalog/category/'.$url;
    }

    /**
     * @param $firstcategory
     * @return string
     */
    public function getCategoryUrl($category)
    {
        return $this->getCatalogCategoryUrlModel()->getCategoryUrl($category);
    }

    /**
     * @return Mage_Core_Model_Resource
     */
    private function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    /**
     * @return Mage_Catalog_Model_Category_Url
     */
    private function getCatalogCategoryUrlModel()
    {
        return Mage::getModel('catalog/category_url');
    }
}