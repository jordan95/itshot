<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 04/04/2017
 * Time: 10:55
 */

class Medialounge_Catalog_Block_Layer_Filter_Category extends Mage_Catalog_Block_Layer_Filter_Category
{
    public function getHtml()
    {
        if (Mage::registry('current_category')) {
            return parent::getHtml();
        }
    }

    public function getItemsCount()
    {
        if (!Mage::registry('current_category')) {
            return 0;
        }

        return parent::getItemsCount();
    }
}