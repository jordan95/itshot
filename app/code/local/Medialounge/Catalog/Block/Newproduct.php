<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Catalog_Block_Newproduct extends Mage_Catalog_Block_Product_New
{
    protected function _construct()
    {
        parent::_construct();
        $this->unsetData();
    }

    /**
     * Prepare and return product collection
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection|Object|Varien_Data_Collection
     */
    protected function _getProductCollection()
    {
        $categoryIds= [];
        $currentCategory  =  Mage::registry('current_category');
        
        if($currentCategory) {
            $categoryIds = $currentCategory->getAllChildren(true);
        }
        
        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
            ->addAttributeToSelect('*');
        if(count($categoryIds) > 0) {
            $collection->getSelect()->where('at_category_id.category_id IN (?)', $categoryIds);
        }

        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());


        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToSort('created_at', 'desc')
            ->setPageSize(15)
            ->setCurPage(1);

        // avoiding product duplication
        $collection->getSelect()->group('e.entity_id');

        return $collection;
    }
}