<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 03/08/2017
 * Time: 12:31
 */
class Medialounge_Catalog_Block_Tracking_Success extends Mage_Core_Block_Template
{
    protected function _toHtml()
    {
        if (!$this->getOrder()) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * Retrieve current order model instance
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if (!$this->hasData('order')) {
            $this->setData('order', Mage::helper('medialounge_catalog')->getCurrentOrder());
        }

        return $this->getData('order');
    }

    public function getProductIdsInCart()
    {
        $products = $this->getOrder()->getAllItems();
        $ids = [];
        foreach ($products as $product) {
            $ids[] = $product->getSku();
        }
        return sprintf('["%s"]', implode('","', $ids));
    }

    public function getCartTotal()
    {
        return number_format($this->getOrder()->getGrandTotal(), 2, '.', '');
    }

    public function getCartCurrency()
    {
        return $this->getOrder()->getOrderCurrencyCode();
    }
}