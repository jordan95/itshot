<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 03/08/2017
 * Time: 12:27
 */
class Medialounge_Catalog_Block_Tracking_Facebook extends Mage_Core_Block_Template
{
    public function getCurrentUserEmail()
    {
        $user = $this->getCustomerSessionModel();
        if ($user->isLoggedIn()) {
            return $user->getCustomer()->getEmail();
        }
    }

    /**
     * @return Mage_Customer_Model_Session
     */
    private function getCustomerSessionModel()
    {
        return Mage::getModel('customer/session');
    }
}