<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 03/08/2017
 * Time: 12:31
 */
class Medialounge_Catalog_Block_Tracking_Cart extends Mage_Core_Block_Template
{
    public function getProductIdsInCart()
    {
        $products = $this->getCheckoutCartModel()->getItems();
        $ids = [];
        foreach ($products as $product) {
            $ids[] = $product->getSku();
        }
        return sprintf('["%s"]', implode('","', $ids));
    }

    public function getCartTotal()
    {
        return number_format($this->getCheckoutCartModel()->getQuote()->getGrandTotal(), 2 , '.', '');
    }

    public function getCartCurrency()
    {
        return $this->getCheckoutCartModel()->getQuote()->getQuoteCurrencyCode();
    }

    /**
     * @return Mage_Checkout_Model_Cart
     */
    private function getCheckoutCartModel()
    {
        return Mage::getModel('checkout/cart');
    }
}