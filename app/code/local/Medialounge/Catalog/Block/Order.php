<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Catalog_Block_Order extends Mage_Catalog_Block_Product_Abstract
{
    protected function _toHtml()
    {
        if ($relatedProducts = $this->getOrderRelatedProducts()) {
            $this->setData('related_product',$relatedProducts);
        }

        return parent::_toHtml();
    }

    private function getCurrentOrder()
    {
        $this->setData('order', Mage::helper('medialounge_catalog')->getCurrentOrder());

        return $this->getData('order');
    }

    /**
     * @return AW_Relatedproducts_Model_Api
     */
    private function getRelatedProductAPIModel()
    {
        return Mage::getModel('relatedproducts/api');
    }

    private function getOrderRelatedProducts()
    {
        $order = $this->getCurrentOrder();

        if ($order instanceof Mage_Sales_Model_Order) {
            if ($collection = $this->getMedialoungeCatalogHelper()->getOrderRelatedProducts($order)) {
                $this->_addProductAttributesAndPrices($collection);
                return $collection;
            }
        }

        return false;
    }

    /**
     * @return \Medialounge_Catalog_Helper_Data
     */
    private function getMedialoungeCatalogHelper()
    {
        return Mage::helper('medialounge_catalog');
    }
}