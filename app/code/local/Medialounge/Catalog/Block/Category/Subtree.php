<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Catalog_Block_Category_Subtree extends Mage_Catalog_Block_Navigation
{
    public function __construct(array $args)
    {
        parent::__construct($args);
    }


    public function getCurrentSubCategoryList()
    {
        $currentCategory = $this->getCurrentCategory();
        if ($currentCategory) {
            $categories = $this->getSubCategoryList($currentCategory);
            return $categories;
        }
    }

    /**
     * @return bool|Varien_Data_Collection
     */
    public function getSubCategoryList(Mage_Catalog_Model_Category $category)
    {
        // get all children
        // If Flat Data enabled then use it but only on frontend
        $flatHelper = Mage::helper('catalog/category_flat');
        if ($flatHelper->isAvailable() && $flatHelper->isBuilt(true) && !Mage::app()->getStore()->isAdmin()) {
            $current_category = Mage::getModel('catalog/category')->load($category->getId());
            return $current_category;
        } else {
            $children = $category->getChildrenCategories();
            $childrenCount = $children->count();
        }
        $hasChildren = ($children && $childrenCount);
        
        // select active children
        $activeChildren = array();
        foreach ($children as $child) {
            if ($child->getIsActive() && Mage::helper('opentechizcatalog')->_hasProducts($child->entity_id)) {
                $activeChildren[] = $child;
            }
        }
        return $activeChildren;
    }
    
    /**
     * Enter description here...
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getCurrentCategory()
    {
        if (Mage::registry('current_category')) {
            return parent::getCurrentCategory();
        }

        return $this->getStoreRootCategory();
    }

    /**
     * @return Mage_Catalog_Model_Category
     */
    private function getStoreRootCategory()
    {
        $rootCategoryId = Mage::app()->getStore()->getRootCategoryId();

        return Mage::getModel('catalog/category')->load($rootCategoryId);
    }
}