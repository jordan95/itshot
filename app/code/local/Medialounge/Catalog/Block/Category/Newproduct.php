<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Catalog_Block_Category_Newproduct extends Medialounge_Catalog_Block_Newproduct
{
    public function getCacheKeyInfo()
    {
        $categoryId = $this->getCurrentCategory() ?  $this->getCurrentCategory()->getId() : '';

        return array('NEW_PRODUCT', $categoryId);
    }

    protected function _prepareLayout()
    {
        if (!$this->getCurrentCategory()) {
            return false;
        }

        return parent::_prepareLayout();
    }

    /**
     * Prepare and return product collection
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection|Object|Varien_Data_Collection
     */
    protected function _getProductCollection()
    {
        $collection = parent::_getProductCollection();

        if ($this->getCurrentCategory()) {
            $collection->addCategoryFilter($this->getCurrentCategory());
        }

        return $collection;
    }

    /**
     * @return bool|Mage_Catalog_Model_Category
     */
    public function getCurrentCategory()
    {
        if ($category = Mage::registry('current_category')) {
            return $category;
        }

        return false;
    }
}