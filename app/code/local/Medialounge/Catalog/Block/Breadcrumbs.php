<?php

class Medialounge_Catalog_Block_Breadcrumbs extends Mage_Page_Block_Html_Breadcrumbs
{

    protected function _toHtml()
    {
        if ($this->needCategoryCrumb()) {
            $newCrumbs[] = $this->prepareFirstCrumb();
            foreach ($this->getBreadcrumbCategories() as $category) {
                $newCrumbs[] = [
                    'label' => $category->getName(),
                    'title' => $category->getName(),
                    'link' => $category->getUrl()
                ];
            }

            $newCrumbs[] = $this->prepareLastCrumb();

            $this->assign('crumbs', $newCrumbs);
            return \Mage_Core_Block_Template::_toHtml();
        }

        return parent::_toHtml();
    }

    private function isProductpage()
    {
        return $this->getCurrentProduct();
    }

    private function getCurrentProduct()
    {
        return Mage::registry('current_product');
    }

    private function needCategoryCrumb()
    {
        if (!$this->isProductpage()) {
            return false;
        }

        return true;
    }

    /**
     * @return mixed
     */
    private function prepareFirstCrumb()
    {
        $first = array();
        if (is_array($this->_crumbs)) {
            reset($this->_crumbs);
            $first = current($this->_crumbs);
            $first['first'] = true;
        }

        return $first;
    }

    /**
     * @return mixed
     */
    private function prepareLastCrumb()
    {
        $last = array();
        if (is_array($this->_crumbs)) {
            $last = end($this->_crumbs);
            $last['last'] = true;
            return $last;
        }
    }

    protected function getCategoriesByBreadcumbPath()
    {
        $product = $this->getCurrentProduct();
        $categories = array();
        if ($product->getBreadcrumbPath()) {
            $catIds = array_diff(explode('>', $product->getBreadcrumbPath()), $this->getConfigExcludeCategoryIds());
            if (count($catIds) > 0) {
                foreach ($catIds as $id) {
                    $categories [] = Mage::getModel('catalog/category')->load($id);
                }
            }
        }
        return $categories;
    }

    protected function getConfigExcludeCategoryIds()
    {
        return Mage::helper('opentechizcatalog')->getConfigExcludeCategoryIds();
    }

    protected function getDefaultBreadcrumbCategories()
    {
        $key = 'last_category';
        if (Mage::registry($key) !== null) {
            return Mage::registry($key);
        }
        $level = 0;
        $lastCat = array();
        $categoryIds = array_diff($this->getCurrentProduct()->getCategoryIds(), $this->getConfigExcludeCategoryIds());
        if (count($categoryIds) == 0) {
            Mage::register($key, array());
            return array();
        }

        $productCats = Mage::getModel('catalog/category')->getCollection()
                ->addNameToResult()
                ->addUrlRewriteToResult()
                ->addIsActiveFilter()
                ->addAttributeToFilter('include_in_menu', 1)
                ->addIdFilter($categoryIds);

        if (
                !Mage::helper('catalog/category_flat')->isEnabled() &&
                $productCats instanceof Mage_Catalog_Model_Resource_Category_Flat_Collection
        ) {
            $productCats->addStoreFilter();
        }

        foreach ($productCats as $category) {
            if ($category->getLevel() > $level) {
                $level = $category->getLevel();
                $lastCat = $category;
            }
        }
        $cate = ($lastCat) ? $this->getParentCategories($lastCat) : $lastCat;
        Mage::register($key, $cate);
        return $cate;
    }

    /**
     * @return mixed
     */
    private function getBreadcrumbCategories()
    {
        $categories = $this->getCategoriesByBreadcumbPath();
        return count($categories) > 0 ? $categories : $this->getDefaultBreadcrumbCategories();
    }

    /**
     * Return parent categories of category
     *
     * @param Mage_Catalog_Model_Category $category
     * @return array
     */
    public function getParentCategories($category)
    {
        $exclude_ids = $this->getConfigExcludeCategoryIds();
        $catIds = array_diff(explode(',', $category->getPathInStore()), $exclude_ids);
        $catIds = $catIds ? $catIds : explode(',', $category->getPathInStore());
        $pathIds = array_reverse($catIds);

        $categories = Mage::getResourceModel('catalog/category_collection')
                ->setStore(Mage::app()->getStore())
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('url_key')
                ->addFieldToFilter('entity_id', array('in' => $pathIds))
                ->addFieldToFilter('is_active', 1)
                ->setOrder('level', 'asc')
                ->load();
        return $categories;
    }

}
