<?php

/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Catalog_Block_Subcategorywithproduct extends Mage_Catalog_Block_Product_Abstract
{

    private static $numberProductPerCategory = 30;
    protected $category_id;
    protected $pageSize = 2;
    protected $currentPage = 1;

    /**
     * Initialize product collection:
     * note: this function was initially taken from Mage_Catalog_Model_Layer::prepareProductCollection function
     *
     * @param Mage_Catalog_Model_Category
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
        return $this;
    }

    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;
        return $this;
    }

    public function prepareProductCollection($category, $collection)
    {
        $collection
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addUrlRewrite($category->getId());

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
    }

    /**
     * @return bool|Varien_Data_Collection
     */
    public function getSubCategoryList()
    {
        $currentCategory = $this->getCurrentCategory();
        if ($currentCategory) {
            /** @var $currentCategory Mage_Catalog_Model_Category */
            $categories = $currentCategory
                    ->getCollection()
                    ->addAttributeToSelect('url_key')
                    ->addAttributeToSelect('thumbnail')
                    ->addAttributeToSelect('ml_image')
                    ->addAttributeToSelect('description')
                    ->addAttributeToSelect('ml_featured_subcategory')
                    ->addAttributeToSelect('name')
                    ->addAttributeToSelect('all_children')
                    ->addAttributeToSelect('is_anchor')
                    ->addAttributeToSelect('intro_text')
                    ->setOrder('position', Varien_Db_Select::SQL_ASC)
                    ->joinUrlRewrite()
                    ->addAttributeToFilter('is_active', 1)
                    ->addIdFilter($currentCategory->getChildren());
            $offset = ($this->currentPage - 1) * $this->pageSize;
            $categories->getSelect()->limit($this->pageSize, $offset);
        } else {
            $categories = new Varien_Data_Collection();
        }

        return $categories;
    }

    public function getCurrentCategory()
    {
        if (!Mage::registry('current_category')) {
            $category = Mage::getModel('catalog/category')->load($this->category_id);
            if ($category->getId()) {
                Mage::register('current_category', $category);
            }
        }
        return Mage::registry('current_category');
    }

    public function getProductListForCategory(Mage_Catalog_Model_Category $category)
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = $category->getProductCollection();
        $collection->addAttributeToSort('c2c_popularity', 'desc');
        $this->prepareProductCollection($category, $collection);
        $collection->setPageSize(self::$numberProductPerCategory);
        Mage::dispatchEvent('subcategory_product_list_for_category', array('collection' => $collection));
        return $collection;
    }

    /**
     * @return Mage_Catalog_Model_Product
     */
    private function getProductModel()
    {
        return Mage::getModel('catalog/product');
    }

    /**
     * @return Mage_Core_Model_Resource
     */
    private function getResource()
    {
        return Mage::getSingleton('core/resource');
    }

}
