<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 24/07/2017
 * Time: 16:25
 */
if ('true' == (string) Mage::getConfig()->getNode('modules/Amasty_Shopby/active')) {

    class Medialounge_Catalog_Block_Product_View_Attributes_Pure extends Amasty_Shopby_Block_Product_View_Attributes {
        
    }

} else {

    class Medialounge_Catalog_Block_Product_View_Attributes_Pure extends Mage_Catalog_Block_Product_View_Attributes {
        
    }

}

class Medialounge_Catalog_Block_Product_View_Attributes extends Medialounge_Catalog_Block_Product_View_Attributes_Pure
{
    private static $groups = [
        'Item Information',
        'Diamond Information',
        'Watch Information',
        'Gemstone Information',
        'Eyewear Information',
    ];

    public function getAdditionalGroupAttributes()
    {
        $product = $this->getProduct();
        $setId = $product->getAttributeSetId();

        $list = [];

        if ($setId) {
            $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
                ->setAttributeSetFilter($setId)
                ->addFieldToFilter('attribute_group_name', ['in'=> self::$groups])
                ->setSortOrder()
                ->load();

            foreach ($groupCollection as $group) {
                $data = [];
                $attributes = $product->getAttributes($group->getId(), true);

                foreach ($attributes as $attribute) {
                    if ($attribute->getIsVisibleOnFront()) {
                        $value = $attribute->getFrontend()->getValue($product);

                        if (!$product->hasData($attribute->getAttributeCode())) {
                            $value = Mage::helper('catalog')->__('N/A');
                        } elseif ((string)$value == '') {
                            $value = Mage::helper('catalog')->__('No');
                        } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                            $value = Mage::app()->getStore()->convertPrice($value, true);
                        }

                        if (is_string($value) && strlen($value)) {
                            $data[$attribute->getAttributeCode()] = array(
                                'label' => $attribute->getStoreLabel(),
                                'value' => $value,
                                'code'  => $attribute->getAttributeCode()
                            );
                        }
                    }
                }

                $list[$group->getAttributeGroupName()] =  $data;
            }
        }

        return $list;
    }
}