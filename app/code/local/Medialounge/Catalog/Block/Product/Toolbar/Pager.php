<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 09/03/2017
 * Time: 16:38
 */

class Medialounge_Catalog_Block_Product_Toolbar_Pager extends Mage_Page_Block_Html_Pager
{
    protected function _toHtml()
    {
        $this->setTemplate('medialounge/catalog/pager.phtml');
        return parent::_toHtml();
    }
}