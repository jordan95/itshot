<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 09/03/2017
 * Time: 16:40
 */

class Medialounge_Catalog_Block_Product_Toolbar extends Amasty_Sorting_Block_Catalog_Product_List_Toolbar
{
    protected function _prepareLayout()
    {
        Mage::getConfig()->setNode('global/blocks/page/rewrite/html_pager', 'Medialounge_Catalog_Block_Product_Toolbar_Pager');
        return parent::_prepareLayout();
    }
    
    public function isDisableSortAjax(){
        return !Mage::getStoreConfigFlag('amshopby/general/ajax');
    }

    protected function _toHtml()
    {
        $this->setTemplate('medialounge/catalog/toolbar.phtml');
        return parent::_toHtml();
    }

    /**
     * Getter for alternative text for Previous link in pagination frame
     *
     * @return string
     */
    public function getAnchorTextForPrevious()
    {
        return Mage::getStoreConfig('design/pagination/anchor_text_for_previous');
    }

    /**
     * Getter for alternative text for Next link in pagination frame
     *
     * @return string
     */
    public function getAnchorTextForNext()
    {
        return Mage::getStoreConfig('design/pagination/anchor_text_for_next');
    }

    public function isFirstPage()
    {
        return $this->getCollection()->getCurPage() == 1;
    }

    public function isLastPage()
    {
        return $this->getCollection()->getCurPage() >= $this->getLastPageNum();
    }

    public function getFirstPageUrl()
    {
        return $this->getPageUrl(1);
    }

    public function getPreviousPageUrl()
    {
        return $this->getPageUrl($this->getCollection()->getCurPage(-1));
    }

    public function getNextPageUrl()
    {
        return $this->getPageUrl($this->getCollection()->getCurPage(+1));
    }

    public function getLastPageUrl()
    {
        return $this->getPageUrl($this->getCollection()->getLastPageNumber());
    }

    public function getPageUrl($page)
    {
        return $this->getPagerUrl(array($this->getPageVarName()=>$page));
    }

    public function getLimitUrl($limit)
    {
        return $this->getPagerUrl(array($this->getLimitVarName()=>$limit));
    }

    public function getPagerUrl($params=array())
    {
        $urlParams = array();
        $urlParams['_current']  = true;
        $urlParams['_escape']   = true;
        $urlParams['_use_rewrite']   = true;
        $urlParams['_query']    = $params;
        return $this->getUrl('*/*/*', $urlParams);
    }

    public function getAvailableOrders()
    {
        $orders = parent::getAvailableOrders();
        foreach ($orders as $key => $order) {
            if ($key == 'price') {
                $orders[$key] = 'Lowest Price';
            }
        }

        return $orders;
    }

    /**
     * Compare defined order field vith current order field
     *
     * @param string $order
     * @return bool
     */
    public function isOrderCurrent($order, $direction = 'asc')
    {
        if (($order == 'price')
            and ($this->getCurrentDirection() != $direction)) {
            return false;
        }
        return ($order == $this->getCurrentOrder());
    }

}