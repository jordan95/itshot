<?php
/**
 * Media block widget
 *
 * @category   Medialounge
 * @package    Medialounge_MediaBlock
 * @author     Javier Villanueva <javier@medialounge.co.uk>
 */
class Medialounge_MediaBlock_Block_Widget_Media
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{
    /**
     * Return absolute url to the bg image file.
     *
     * @return string
     */
    public function getBgImageUrl()
    {
        return Mage::getBaseUrl('media') . $this->getBgImage();
    }

    /**
     * Return absolute url to the image file.
     *
     * @return string
     */
    public function getImageUrl()
    {
        return Mage::getBaseUrl('media') . $this->getImage();
    }
    
    public function isEnableLazyImage(){
        return (bool)$this->getEnableLazyImage();
    }
}
