<?php
class Medialounge_ProductQA_IndexController extends Mage_Core_Controller_Front_Action
{

   const CONFIG_SEND_NOTIFICATION_EMAIL = 'pws_productqa/general/send_notification';
   const CONFIG_SEND_NOTIFICATION_EMAIL_TO = 'pws_productqa/general/notification_email';

   const XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY  = 'default/pws_productqa/emails/email_identity';
   const XML_PATH_EMAIL_PRODUCT_NOTIFICATION_TEMPLATE  = 'pws_productqa/general/notification_template';

   public function addQuestionAction()
   {
        $post = $this->getRequest()->getPost();
        if ($post) {

            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['question']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

//                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
//                    $error = true;
//                }
//
//                if (!$this->validateRecaptcha($post['g-recaptcha-response'])) {
//                    $error = true;
//                }

                if ($error) {
                    throw new Exception();
                }

                $post['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $productqa = Mage::getModel('pws_productqa/productqa');
                $productqa->setData($post);
                $productqa->save();
                $product = Mage::getModel('catalog/product')->load($productqa->getProductId());
                $messageSuccess = Mage::helper('pws_productqa')->__('Thank You! We\'ll reply to your question as soon as possible ');

                $canSendMail = Mage::helper('odoo')->isEnabledSendEmail();

                try {
                    // Submit ticket to Odoo
                    $this->submitTicket($post, $product);
                } catch (Exception $ex) {
                    $messageSuccess = Mage::helper('pws_productqa')->__('There were a problem with our CRM system, your information will be sent to our admin email');
                }
                // Send email if submit ticket Odoo fail or Odoo disabled
                if ($canSendMail) {
                    $this->sendQAMail($productqa, $product);
                }
                Mage::getSingleton('catalog/session')->addSuccess($messageSuccess);
                return $this->_redirectReferer();
            } catch (Exception $e) {

                Mage::getSingleton('catalog/session')->addError(Mage::helper('pws_productqa')->__('Unable to submit your question. Please, try again later' . $e->getMessage()));

                return $this->_redirectReferer();
            }
        } else {
            return $this->_redirectReferer();
        }
    }

    protected function sendQAMail($productqa, $product)
    {
        $sendNotificationEmail = Mage::getStoreConfig(self::CONFIG_SEND_NOTIFICATION_EMAIL);
        if ($sendNotificationEmail) {
            $emailData = array();
            $emailData['to_email'] = Mage::getStoreConfig(self::CONFIG_SEND_NOTIFICATION_EMAIL_TO);
            $emailData['to_name'] = Mage::getConfig()->getNode(self::XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY);
            $emailData['email'] = array(
                'item_code' => $product->getSku(),
                'product_name' => $product->getName(),
                'question_id' => $productqa->getData('productqa_id'),
                'product_url' => $product->getProductUrl(),
                'product_image' => (string) Mage::helper('catalog/image')->init($product, 'image'),
                'customer_name' => $productqa->getName(),
                'customer_email' => $productqa->getEmail(),
                'customer_phone' => $productqa->getPhone(),
                'store_id' => $productqa->getStoreId(),
                'question' => $productqa->getQuestion(),
                'date_posted' => Mage::helper('core')->formatDate($productqa->getCreatedOn(), 'long'),
            );
            $result = $this->sendEmail($emailData);
            if (!$result) {
                Mage::throwException($this->__('Cannot send email'));
            }
        }
    }

    protected function submitTicket($post, $product)
    {
        Mage::getModel('odoo/api_odoo')->submitTicket([
            "customer_name" => $post["name"],
            "customer_phone" => $post["phone"],
            "customer_email" => $post["email"],
            "message" => $post['question'],
            "ticket_subject" => "ItsHot.com - Question and Answer",
            "ticket_itemcode" => $product->getSku(),
            "ticket_producturl" => $product->getProductUrl(),
            "team_id" => "Sales",
        ]);
    }

    private function sendEmail($data)
	{

		$storeID = $data['email']['store_id'];

		$translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);

        /* @var $result Mage_Core_Model_Email_Template */
        $result = Mage::getModel('core/email_template')
            ->setDesignConfig(array('area' => 'frontend', 'store' => $storeID));

        $result->sendTransactional(
                Mage::getStoreConfig(self::XML_PATH_EMAIL_PRODUCT_NOTIFICATION_TEMPLATE),
                Mage::getConfig()->getNode(self::XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY),
                $data['to_email'],
                $data['to_name'],
                $data['email'], //data
                $storeID
               );
        //echo $result->getProcessedTemplate($data);

        $translate->setTranslateInline(true);

        return $result;
    }

    private function validateRecaptcha($response)
    {
        if (!extension_loaded('curl')) {
            return null;
        }

        $curl = new Varien_Http_Adapter_Curl();
        $curl->setConfig(array(
            'timeout'   => 2
        ));
        $post = ['secret' => Mage::helper('medialounge_customer')->getRecaptchaSecretKey(),
            'response' => $response
        ];
        $str = '';
        foreach ($post as $key=>$value) {
            if ($str !='') $str .= '&';
            $str .= sprintf('%s=%s', $key, urlencode($value));
        }

        $curl->write(Zend_Http_Client::POST, 'https://www.google.com/recaptcha/api/siteverify', '1.1', ['Content-Type: application/x-www-form-urlencoded; charset=utf-8', 'Content-Length: ' . strlen($str)], $str);
        $data = $curl->read();
        if ($data === false) {
            return false;
        }
        $data = preg_split('/^\r?$/m', $data, 2);
        $response = Mage::helper('core')->jsonDecode(trim($data[1]));
        $curl->close();

        if (isset($response['success']) && $response['success']) {
            return true;
        }

        return false;
    }

}
