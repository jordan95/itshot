<?php

class Medialounge_RewardsTransfer_Block_Manage_Customer_Edit_Tab_Orderincrementid extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $reference_id = $row->getData($this->getColumn()->getIndex());
        $reason_id = $row->getData('reason_id');
        if($reason_id == 10) {
            $parent_transfer = Mage::getModel('rewards/transfer')->load($reference_id);
            return Mage::getModel('sales/order')->load($parent_transfer->getData('reference_id'))->getIncrementId();
        } elseif($reason_id == 1) {
            return Mage::getModel('sales/order')->load($reference_id)->getIncrementId();
        } else {
            return "";
        }
    }

}

?>
