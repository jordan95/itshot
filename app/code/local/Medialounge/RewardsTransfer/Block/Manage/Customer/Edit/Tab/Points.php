<?php

class Medialounge_RewardsTransfer_Block_Manage_Customer_Edit_Tab_Points
    extends TBT_Rewards_Block_Manage_Customer_Edit_Tab_Points
{

    protected function _prepareColumns()
    {
        $this->addColumnAfter('reference_id', array(
            'header' => Mage::helper('rewards')->__('Order ID'),
            'width' => '40px',
            'index' => 'reference_id',
            'renderer' => 'Medialounge_RewardsTransfer_Block_Manage_Customer_Edit_Tab_Orderincrementid'
        ), 'transfer_id');

        return parent::_prepareColumns();
    }


}