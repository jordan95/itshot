<?php


$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (! file_exists($path . $shell) && ! file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Medialounge_Transfer_Points extends Mage_Shell_Abstract
{
    public function run()
    {
        $store = $this->getArg('store');

        if ($store) {
            $store = Mage::app()->getStore($store);
        }

        echo sprintf('approve transfer over %s days', $this->getWaitingDaysBeforeApproval())."\n";

        $pointsToTransfer = Mage::getModel('rewards/transfer')->getCollection();
        $pointsToTransfer->addStoreFilter($store);
        $this->selectOrdersOlderThan45Days($pointsToTransfer);
        $this->selectPendingTransfer($pointsToTransfer);

        foreach ($pointsToTransfer as $pointToTransfer) {
            echo sprintf('transfer: %s: %s'."\n", $pointToTransfer->getId(), $pointToTransfer->getStatusId());
            $transfer = Mage::getSingleton('rewards/transfer')->load($pointToTransfer->getId());
            if ($transfer->setStatusId($transfer->getStatusId(), \TBT_Rewards_Model_Transfer_Status::STATUS_APPROVED)) {
                echo 'success'."\n";
            } else {
                echo 'failed'."\n";
            }
            $transfer->setIsMassupdate(true)->save();
        }

        echo "done\n";
    }

    private function getWaitingDaysBeforeApproval()
    {
        $daysToWaitBeforeTransferApproval = Mage::getStoreConfig('rewards/transferComments/waitingDaysTransfer');
        if ($daysToWaitBeforeTransferApproval == '') $daysToWaitBeforeTransferApproval = 45;

        return $daysToWaitBeforeTransferApproval;
    }

    private function selectOrdersOlderThan45Days($pointsToTransfer)
    {
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getModel('core/resource');
        $connection = $resource->getConnection('core_read');
        $orderReasonId = Mage::helper('rewards/transfer_reason')->getReasonId('order');

        // join sales order history table
        $cond = [];
        $cond[] = $connection->quoteInto('main_table.reason_id=?', $orderReasonId);
        $cond[] = 'main_table.reference_id = order_status_history.parent_id';
        $cond[] = $connection->quoteInto('order_status_history.status=?', Mage_Sales_Model_Order::STATE_PROCESSING);
        $dateTimeStamp = new Zend_date(Mage::getModel('core/date')->timestamp());
        $dateTimeStempCompare = $dateTimeStamp->subDay($this->getWaitingDaysBeforeApproval());
        $cond[] = $connection->quoteInto('order_status_history.created_at<?', $dateTimeStempCompare->toString(Varien_Date::DATETIME_INTERNAL_FORMAT));

        $pointsToTransfer->getSelect()->join(
            ['order_status_history' => Mage::getSingleton('core/resource')->getTableName('sales/order_status_history')],
            implode(' AND ', $cond),
            []
        );

        // join order table with complete orders
        $cond = [];
        $cond[] = 'main_table.reference_id = order.entity_id';
        $cond[] = $connection->quoteInto('order.status=?', Mage_Sales_Model_Order::STATE_COMPLETE);

        $pointsToTransfer->getSelect()->join(
            ['order' => Mage::getSingleton('core/resource')->getTableName('sales/order')],
            implode(' AND ', $cond),
            []
        );
    }

    private function selectPendingTransfer($pointsToTransfer)
    {
        $pointsToTransfer->getSelect()->where('status_id in (?)', [\TBT_Rewards_Model_Transfer_Status::STATUS_PENDING_APPROVAL,
            \TBT_Rewards_Model_Transfer_Status::STATUS_PENDING_EVENT,
            \TBT_Rewards_Model_Transfer_Status::STATUS_PENDING_TIME
        ]);

        return $pointsToTransfer;
    }

}

$shell = new Medialounge_Transfer_Points();
$shell->run();