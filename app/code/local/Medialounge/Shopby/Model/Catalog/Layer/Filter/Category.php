<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Shopby
 */

/**
 * @method array getCategories()
 * @method setCategories(array $ids)
 */
class Medialounge_Shopby_Model_Catalog_Layer_Filter_Category extends Amasty_Shopby_Model_Catalog_Layer_Filter_Category
{
    public function getAdvancedCollection()
    {
        if (is_null($this->_advancedCollection)) {
            $category = $this->_getDataHelper()->getCurrentCategory();
            $startFrom = Mage::getStoreConfig('amshopby/category_filter/start_category');
            switch ($startFrom) {
                case Amasty_Shopby_Model_Source_Category_Start::START_CHILDREN:
                    if (!$category->getChildren()) {
                        $parent = $category->getParentCategory();
                        $category = $parent;
                    }
                    break;
                case Amasty_Shopby_Model_Source_Category_Start::START_CURRENT:
                    $parent = $category->getParentCategory();
                    if ($parent) {
                        $category = $parent;
                    }
                    break;
                case Amasty_Shopby_Model_Source_Category_Start::START_ROOT:
                default:
                    $category = Mage::getModel('catalog/category')->load(Mage::app()->getStore()->getRootCategoryId());
            }
            $excludeIds = preg_replace('/[^\d,]+/', '', Mage::getStoreConfig('amshopby/category_filter/exclude_cat'));
            $excludeIds = $excludeIds ? explode(',', $excludeIds) : array();
            $includeIds = preg_replace('/[^\d,]+/', '', Mage::getStoreConfig('amshopby/category_filter/include_cat'));
            $includeIds = $includeIds ? explode(',', $includeIds) : array();

            $cats = $this->_getCategoryCollection()->addIdFilter($category->getChildren());
            if (count($excludeIds) > 0) {
                $cats->addFieldToFilter('entity_id', array('nin' => $excludeIds));
            }

            if (count($includeIds) > 0) {
                $cats->addFieldToFilter('entity_id', array('in' => $includeIds));
            }
            $this->addCounts($cats);

            foreach ($cats as $c) {
                if ($c->getProductCount()) {
                    $this->_advancedCollection = $cats;
                    return $this->_advancedCollection;
                }
            }

            $this->_advancedCollection = array();
        }

        return $this->_advancedCollection;
    }
}
