<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Shopby
 */


class Medialounge_Shopby_Model_Catalog_Layer_Filter_Decimal extends Amasty_Shopby_Model_Catalog_Layer_Filter_Decimal
{

    /**
     * Apply decimal range filter to product collection
     *
     * @param Zend_Controller_Request_Abstract $request
     * @param Mage_Catalog_Block_Layer_Filter_Decimal $filterBlock
     * @return Mage_Catalog_Model_Layer_Filter_Decimal
     */
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
//        if (!$this->calculateRanges()){
//            $this->_items = array($this->_createItem('', 0, 0));
//        }

        $filterBlock->setValueFrom(Mage::helper('amshopby')->__('From'));
        $filterBlock->setValueTo(Mage::helper('amshopby')->__('To'));

        $input = $request->getParam($this->getRequestVar());
        $fromTo = $this->_parseRequestedValue($input);
        if (is_null($fromTo)) {
            return $this;
        }
        list($from, $to) = $fromTo;

        $attributeCode = $this->getAttributeModel()->getAttributeCode();
        /** @var Amasty_Shopby_Helper_Attributes $attributeHelper */
        $attributeHelper = Mage::helper('amshopby/attributes');
        if ($attributeHelper->lockApplyFilter($attributeCode, 'attr')) {
            $this->_getResource()->applyFilterToCollection($this, $from, $to);

            $this->getLayer()->getState()->addFilter(
                $this->_createItem($this->_renderItemLabel($from, $to, true), $input)
            );
        }

        $filterBlock->setValueFrom(($from > 0) ? $from : '0');
        $filterBlock->setValueTo(($to > 0) ? $to : '');

        if ($this->hideAfterSelection()){
            $this->_items = array();
        }
//        elseif ($this->calculateRanges()){
//            $this->_items = array($this->_createItem('', 0, 0));
//        }

        if (!$this->calculateRanges()) {
            /** @var Amasty_Shopby_Helper_Layer_Cache $cache */
            $cache = Mage::helper('amshopby/layer_cache');
            $cache->limitLifetime(Amasty_Shopby_Helper_Layer_Cache::LIFETIME_SESSION);
        }

        return $this;
        
    }

}
