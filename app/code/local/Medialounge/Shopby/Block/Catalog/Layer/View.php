<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Shopby
 */

class Medialounge_Shopby_Block_Catalog_Layer_View extends Amasty_Shopby_Block_Catalog_Layer_View
{

    public function getFilters()
    {
        if (!is_null($this->_filterBlocks)){
            return $this->_filterBlocks;
        }

        if ($this->_isCurrentUserAgentExcluded()) {
            return array();
        }

        $filters = parent::getFilters();

        $filters = $this->_excludeCurrentLandingFilters($filters);

        // append stock filter
        $f = $this->getChild('stock_filter');
    	if ($f && !$this->_notInBlock(Mage::getStoreConfig('amshopby/block/stock_filter_pos'))) {
        	$filters[] = $f;
        }

        /** @var Amasty_Shopby_Block_Catalog_Layer_Filter_Rating $f */
        $f = $this->getChild('rating_filter');
        if ($f && !Mage::helper('amshopby')->useSolr() && !$this->_notInBlock(Mage::getStoreConfig('amshopby/block/rating_filter_pos'))) {
            $filters[] = $f;
        }

        // remove some filters from the home page
        $exclude = Mage::getStoreConfig('amshopby/general/exclude');
        if ('/' == Mage::app()->getRequest()->getRequestString() && $exclude){
            $exclude = explode(',', preg_replace('/[^a-zA-Z0-9_\-,]+/','', $exclude));
            $filters = $this->excludeFilters($filters, $exclude);
        } else {
            $exclude = array();
        }

        $this->computeAttributeOptionsData($filters);


        $filtersPositions = Mage::helper('amshopby/attributes')->getPositionsAttributes();

        // update filters with new properties
        $allSelected = array();
        foreach ($filters as $f){
            $strategy = $this->_getFilterStrategy($f);

            if (is_object($strategy)) {
                // initiate all filter-specific logic
                $strategy->prepare();
                $f->setIsExcluded($strategy->getIsExcluded());

                // remember selected options for dependent excluding
                if ($strategy instanceof Amasty_Shopby_Helper_Layer_View_Strategy_Attribute) {
                    $selectedValues = $strategy->getSelectedValues();
                    if ($selectedValues){
                        $allSelected = array_merge($allSelected, $selectedValues);
                    }
                }
            }

            if(is_object($f->getAttributeModel()) && isset($filtersPositions[$f->getAttributeModel()->getAttributeCode()])){
                $f->setPosition($filtersPositions[$f->getAttributeModel()->getAttributeCode()]);
            }
            if ($f instanceof Mage_Catalog_Block_Layer_Filter_Category || $f instanceof Enterprise_Search_Block_Catalog_Layer_Filter_Category) {
                $f->setPosition($filtersPositions['ama_category_filter']);
            }
            if ($f instanceof Amasty_Shopby_Block_Catalog_Layer_Filter_Rating) {
                $f->setPosition($filtersPositions['ama_rating_filter']);
            }
            if ($f instanceof Amasty_Shopby_Block_Catalog_Layer_Filter_Stock) {
                $f->setPosition($filtersPositions['ama_stock_filter']);
            }

            // Move Price filter top of list
            if ($f->getType() == 'catalog/layer_filter_price' && $priceFilterPosition = Mage::getStoreConfig('amshopby/general/price_filter_pos')) {
                $f->setPosition($priceFilterPosition);
            }
        }

        //exclude dependant, since 1.4.7
        foreach ($filters as $f){
            $parentAttributes = trim(str_replace(' ', '', $f->getDependOnAttribute()));

            if (!$parentAttributes){
                continue;
            }

            if (!empty($parentAttributes)) {
                $attributePresent = false;
                $parentAttributes = explode(',', $parentAttributes);
                foreach ($parentAttributes as $parentAttribute) {
                    if (Mage::app()->getRequest()->getParam($parentAttribute)) {
                        $attributePresent = true;
                        break;
                    }
                }
                if (!$attributePresent) {
                    $exclude[] = $f->getAttributeModel()->getAttributeCode();
                }
            }
        }

        // 1.2.7 exclude some filters from the selected categories
        $filters = $this->excludeFilters($filters, $exclude);

        usort($filters, array(Mage::helper('amshopby/attributes'), 'sortFiltersByOrder'));

        $this->_filterBlocks = $filters;
        return $filters;
    }
    
    protected function _afterToHtml($html)
    {
        Mage::helper("fpc/response")->updateIgnoredUrlParams($html);
        return parent::_afterToHtml($html);
    }

}
