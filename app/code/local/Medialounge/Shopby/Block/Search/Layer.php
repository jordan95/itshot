<?php

class Medialounge_Shopby_Block_Search_Layer extends Amasty_Shopby_Block_Search_Layer
{

    protected function _afterToHtml($html)
    {
        Mage::helper("fpc/response")->updateIgnoredUrlParams($html);
        return parent::_afterToHtml($html);
    }

}
