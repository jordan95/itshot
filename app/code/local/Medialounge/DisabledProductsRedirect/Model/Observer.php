<?php

/**
 * DisabledProductsRedirect Observer
 *
 * @category    Medialounge
 * @package     Medialounge_DisabledProductsRedirect
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Medialounge_DisabledProductsRedirect_Model_Observer
{

    private static $permanentRedirectCode = 301;

    /**
     * Process redirect to category page for products with no route
     *
     * @param Varien_Event_Observer  $observer
     *
     * @return Medialounge_DisabledProductsRedirect_Model_Observer
     *
     */
    public function redirectIfProductIsDisabled(Varien_Event_Observer $observer)
    {
        $controller = $observer->getData('controller_action');
        $productId = (int) $controller->getRequest()->getParam('id');

        // don't produce any action if functionality is disabled or we can't get current product id
        if (!$productId || !$this->isUrlRewriteForDisabledProductActive()) {
            return $this;
        }

        // load only necessary attributes for better performance
        /** @var Mage_Catalog_Model_Product $product */
        if (!Mage::helper('catalog/product_flat')->isEnabled()) {
            $product = Mage::getModel('catalog/product')->getCollection()
                    ->setStoreId($this->getStoreId())
                    ->addAttributeToSelect(array('status', 'visibility'))
                    ->addAttributeToFilter('entity_id', $productId)
                    ->getFirstItem();
        } else {
            $product = Mage::getModel('catalog/product')->load($productId);
        }

        if (!$product->isVisibleInCatalog()) {
            $redirectUrl = $this->getRedirectUrlToFirstCategory($product, $observer);
            if ($redirectUrl) {
                $this->addNotice();
                $controller->getResponse()
                        ->setRedirect($redirectUrl, self::$permanentRedirectCode);
            } else {
                $controller->getResponse()->setRedirect(Mage::getBaseUrl())->sendResponse();
                exit;
            }
        }

        return $this;
    }

    /**
     * Get redirect url for current product
     *
     * @param Mage_Catalog_Model_Product $product
     * @param Varien_Event_Observer $observer
     *
     * @return string
     */
    protected function getRedirectUrlToFirstCategory($product, $observer)
    {
        // trying to get the product category form request params
        $categoryId = $observer->getParams() != null ? $observer->getParams()->getCategoryId() : false;

        if ($categoryId) {
            $productCategories = array($categoryId);
        } else {
            $productCategories = $product->getCategoryIds();
        }

        if (!count($productCategories)) {
            return Mage::getUrl();
        }

        // load only necessary attributes for better performance, also get only available categories
        $categories = Mage::getModel('catalog/category')->getCollection()
                ->addNameToResult()
                ->addUrlRewriteToResult()
                ->addIsActiveFilter()
                ->addAttributeToFilter('include_in_menu', 1)
                ->addIdFilter($productCategories);
        if (
                !Mage::helper('catalog/category_flat')->isEnabled() &&
                $categories instanceof Mage_Catalog_Model_Resource_Category_Flat_Collection
        ) {
            $categories->addStoreFilter();
        }
        $category = $categories->getFirstItem();

        return $category->getId() ? $category->getUrl() : '';
    }

    /**
     * check out the product visibility via core API
     *
     * @param $product
     *
     * @return bool
     */
    protected function canAvoidNoRoute($product)
    {
        return !Mage::helper('catalog/product')->canShow($product);
    }

    protected function addNotice()
    {
        Mage::getSingleton('core/session')->addNotice($this->getDisabledProductUrlRewriteNotice());
    }

    protected function getStoreId()
    {
        return Mage::app()->getStore()->getId();
    }

    private function getDisabledProductUrlRewriteNotice()
    {
        return Mage::getStoreConfig('disabled_redirect/settings/notice', Mage::app()->getStore());
    }

    private function isUrlRewriteForDisabledProductActive()
    {
        return Mage::getStoreConfig('disabled_redirect/settings/active', Mage::app()->getStore());
    }

    public function updateCustomUrl($observer)
    {
        if ($observer->getObject()->getResourceName() !== 'core/url_rewrite') {
            return;
        }
        // remove index.php from url
        $targetPath = str_replace("/index.php", "", $observer->getObject()->getData('target_path'));

        $observer->getObject()->setData('target_path', $targetPath);
    }

}
