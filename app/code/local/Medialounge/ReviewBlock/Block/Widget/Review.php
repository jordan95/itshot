<?php

/**
 * Widget block description
 *
 * @category   Medialounge
 * @package    Medialounge_ReviewBlock
 * @author     Javier Villanueva <javier@medialounge.co.uk>
 */
class Medialounge_ReviewBlock_Block_Widget_Review extends Mage_Cms_Block_Widget_Block
{

    protected $testimonial;
    protected $_limit = 10;
    protected $_jsSlider = true;

    public function __construct(array $args = array())
    {
        parent::__construct($args);

        //$this->testimonial = $this->getRandTestimonial();
    }

    public function getCustomerRate($code)
    {
        return Mage::getModel('core/variable')
                        ->setStoreId(Mage::app()->getStore()->getId())
                        ->loadByCode($code)
                        ->getValue('text');
    }

    public function getTestimonials()
    {
        $collection = Mage::getModel('testimonials/testimonials')
                ->getCollection()
                ->addFieldToFilter('rating', array('4', '5'));
        $collection->getSelect()
                ->where('status = 1')
                ->order('update_time DESC')
                ->limit($this->getLimit());
        return $collection;
    }

    protected function getRandTestimonial()
    {
        $testimonial = $this->getRandTestimonialLastest(30);
        if (!$testimonial) {
            $testimonial = $this->getRandTestimonialLastest();
        }
        return $testimonial;
    }

    protected function getRandTestimonialLastest($day = false)
    {
        $collection = Mage::getModel('testimonials/testimonials')
                ->getCollection()
                ->addFieldToFilter('rating', '5');
        if ($day) {
            $collection->getSelect()->where('update_time > ?', array(new Zend_Db_Expr('DATE_SUB(NOW(), INTERVAL ' . $day . ' day)')));
        }
        $collection->getSelect()
                ->where('status = 1')
                ->order('rand()')
                ->limit(1);
        return current($collection->getItems());
    }

    public function getTestimonial()
    {
        return $this->testimonial;
    }

    public function getLimit()
    {
        return $this->_limit;
    }

    public function setLimit($limit)
    {
        $this->_limit = $limit;
        return $this;
    }

    public function getJsSlider()
    {
        return $this->_jsSlider;
    }

    public function setJsSlider($jsslider)
    {
        $this->_jsSlider = $jsslider;
        return $this;
    }

}
