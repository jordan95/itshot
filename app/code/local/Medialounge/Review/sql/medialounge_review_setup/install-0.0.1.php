<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE {$this->getTable('review_detail')}
ADD COLUMN `citystate` VARCHAR(255) NULL after `nickname`,
ADD COLUMN `country` VARCHAR(255) NULL after `citystate`;

");

$installer->endSetup();