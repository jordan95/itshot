<?php

class Medialounge_Review_Block_Adminhtml_Grid
    extends Mage_Adminhtml_Block_Review_Grid
{
    public function _prepareCollection()
    {
        parent::_prepareCollection();

        return $this;
    }


    public function _prepareColumns()
    {
        $this->addColumnAfter('citystate', [
            'header'       => Mage::helper('review')->__('City & State'),
            'align'        => 'left',
            'width'        => '100px',
            'filter_index' => 'rdt.citystate',
            'index'        => 'citystate',
            'type'         => 'text',
            'truncate'     => 50,
            'escape'       => true,
        ], 'nickname');

        $this->addColumnAfter('country', [
            'header'       => Mage::helper('review')->__('Country'),
            'align'        => 'left',
            'width'        => '100px',
            'filter_index' => 'rdt.country',
            'index'        => 'country',
            'type'         => 'text',
            'truncate'     => 50,
            'escape'       => true,

        ], 'citystate');

        parent::_prepareColumns();

        $this->getColumn('detail')
//            ->setData('width', '200px')
            ->unsetData('truncate')
            ->unsetData('type');

        $this->addColumnAfter('review_id', [
            'header'         => Mage::helper('review')->__('Rating'),
            'align'          => 'left',
            'width'          => '150px',
            'index'          => 'review_id',
            'filter_index'   => 'rt.review_id',
            'frame_callback' => array($this, 'getRate')

        ], 'country');

        $this->sortColumnsByOrder();

        return $this;
    }

    public function getRate($ratingId)
    {
        $html = '';

        $coreResource = Mage::getSingleton('core/resource');
        $ratingTableName = $coreResource->getTableName('rating');
        $ratingOptionVote = $coreResource->getTableName('rating_option_vote');

        $select = Mage::getResourceModel('core/config')->getReadConnection()
            ->select()
            ->from(['r' => $ratingTableName])
            ->join(['v' => $ratingOptionVote],
                '`r`.`rating_id` = `v`.`rating_id`')
            ->where('`v`.`review_id` = ?', $ratingId);
//            ->order('`r`.`position`');

        $result = Mage::getResourceModel('core/config')->getReadConnection()
            ->fetchAll($select);
        $html = '';
        $html .= '<strong>Review Id:</strong> ' . $ratingId;
        $html .= "<br/>";
        for ($i = 0; $i < count($result); $i++) {
            $html .= '<strong>' . $result[$i]['rating_code'] . ':</strong> '
                . $result[$i]['value'];
            $html .= "<br/>";
        }


        return $html;
    }

    public function setCollection($collection)
    {
//        $countryId = Mage::getModel('eav/entity_attribute')
//            ->loadByCode('customer_address', 'country_id');
//        $regionId = Mage::getModel('eav/entity_attribute')
//            ->loadByCode('customer_address', 'region');
//        $addressTable = Mage::getSingleton('core/resource')
//            ->getTableName('customer_address_entity_varchar');

        $collection->getSelect()
            ->columns(['rdt.citystate', 'rdt.country']);
//            ->joinLeft(['r' => $addressTable],
//                'r.entity_id = rdt.customer_id AND '
//                . sprintf("r.attribute_id = %s", $regionId->getId()),
//                ['region' => 'r.value'])
//            ->joinLeft(['c' => $addressTable],
//                'c.entity_id = rdt.customer_id AND '
//                . sprintf("c.attribute_id = %s", $countryId->getId()),
//                ['country' => 'c.value']);

        $this->_collection = $collection;
    }

}