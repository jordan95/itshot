<?php

if (class_exists('OpenTechiz_Review_Block_Adminhtml_Review_Edit_Form') && Mage::helper('core')->isModuleEnabled('OpenTechiz_Review')) {

    class Medialounge_Review_Block_Adminhtml_Edit_Form_Pure extends OpenTechiz_Review_Block_Adminhtml_Review_Edit_Form
    {
        
    }

} else {

    class Medialounge_Review_Block_Adminhtml_Edit_Form_Pure extends Mage_Adminhtml_Block_Review_Edit_Form
    {
        
    }

}

class Medialounge_Review_Block_Adminhtml_Edit_Form extends Medialounge_Review_Block_Adminhtml_Edit_Form_Pure
{

    protected function _prepareForm()
    {
        parent::_prepareForm();

        $form = $this->getForm();

        $form->getElement('title')->setData('required', false);

        $fieldSet = $form->getElement('review_details');

        $fieldSet->addField('citystate', 'text', [
            'name' => 'citystate',
            'title' => Mage::helper('review')->__('citystate'),
            'label' => Mage::helper('review')->__('City & State'),
                ], 'nickname');

        $fieldSet->addField('country', 'text', [
            'name' => 'country',
            'title' => Mage::helper('review')->__('country'),
            'label' => Mage::helper('review')->__('Country'),
                ], 'citystate');

        $review = Mage::registry('review_data');
        $form->setValues($review->getData());

        return $this;
    }

}
