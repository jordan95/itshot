<?php

class Medialounge_Review_Model_Observer
{

    protected $writeAdapter;

    protected $reviewDetailTable;


    public function __construct()
    {
        $this->writeAdapter = Mage::getSingleton('core/resource')
            ->getConnection('core_write');
        $this->reviewDetailTable = Mage::getSingleton('core/resource')
            ->getTableName('review_detail');

    }

    public function saveAddress($observer)
    {

        $review = $observer->getObject();

        $address = [
            'citystate' => $review->getData('citystate'),
            'country'   => $review->getData('country')
        ];

        $select = $this->writeAdapter->select()
            ->from($this->reviewDetailTable, 'detail_id')
            ->where('review_id = :review_id');

        $detailId = $this->writeAdapter->fetchOne($select,
            array(':review_id' => $review->getId()));

        $condition = array("detail_id = ?" => $detailId);

        $this->writeAdapter->update($this->reviewDetailTable, $address,
            $condition);
    }

}