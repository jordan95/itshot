<?php

/**
 * Created by PhpStorm.
 * User: Medialounge_dev1
 * Date: 21/02/2017
 * Time: 17:38
 */
class Medialounge_Ekomi_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getAPIId()
    {
        return Mage::getStoreConfig('medialounge_ekomi/settings/api_id');
    }

    public function getAPIPassword()
    {
        return Mage::getStoreConfig('medialounge_ekomi/settings/api_password');
    }

    public function getRangeTime()
    {
        return Mage::getStoreConfig('medialounge_ekomi/settings/range_time');
    }

    public function isActive()
    {
        return (bool) Mage::getStoreConfig('medialounge_ekomi/settings/active');
    }

}
