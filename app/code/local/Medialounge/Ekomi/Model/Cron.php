<?php

/**
 * Created by PhpStorm.
 * User: Medialounge_dev1
 * Date: 21/02/2017
 * Time: 17:22
 */
use \Ekomi\Api;
use \Ekomi\Request\GetProductFeedback;

class Medialounge_Ekomi_Model_Cron
{

    const XML_EKOMI_PRODUCT_FEEDBACK_LAST_UPDATED = 'medialounge_ekomi/settings/last_updated';

    private $api;
    protected $ratingCollection;

    public function getHelper()
    {
        return Mage::helper('medialounge_ekomi');
    }

    public function getAPIID()
    {
        return $this->getHelper()->getAPIId();
    }

    public function getAPIPassword()
    {
        return $this->getHelper()->getAPIPassword();
    }

    public function getApi()
    {
        if (!$this->api) {
            $this->api = new Api($this->getAPIID(), $this->getAPIPassword());
        }
        return $this->api;
    }

    public function getProductFeedback($last_updated = '')
    {
        $apiCall = new GetProductFeedback();
        $apiCall->setRange($this->getHelper()->getRangeTime());
        $result = $this->getApi()->exec($apiCall);
        if ($last_updated) {
            $data = [];
            foreach ($result as $item) {
                if ($item->submitted < $last_updated) {
                    continue;
                }
                $data[] = $item;
            }
            return $data;
        }
        return $result;
    }

    public function getLastUpdated()
    {
        $last_updated = Mage::getStoreConfig(self::XML_EKOMI_PRODUCT_FEEDBACK_LAST_UPDATED);
        return $last_updated ?: time();
    }

    public function saveLastUpdated($time)
    {
        Mage::getConfig()->saveConfig(self::XML_EKOMI_PRODUCT_FEEDBACK_LAST_UPDATED, $time);
    }

    public function importProductReview($item)
    {
        if ($item->order_id < 30000) {
            return false;
        }

        $order = Mage::getModel('sales/order')->load($item->order_id, 'increment_id');
        if (!$order->getId() || !$order->getCustomerId()) {
            return false;
        }

        if ($this->isReviewAlreadyRecorded($order->getCustomerId(), $item->product_id)) {
            return false;
        }

        $product = Mage::getModel('catalog/product')->load($item->product_id);
        if (!$product->getId()) {
            return false;
        }

        $form_data = $this->getAnswers($item->product_id, $item->order_id);
        if ($form_data->form_type != Mage_Review_Model_Review::ENTITY_PRODUCT_CODE) {
            return false;
        }

        $ratingCollection = $this->getRatingCollection();
        $ratings = [];
        foreach ($ratingCollection as $_rating) {
            $ratingValue = $this->getRatingValueByLabel($form_data, $_rating->getData('rating_code'));
            if (!$ratingValue) {
                $ratingValue = $item->rating;
            }

            foreach ($_rating->getOptions() as $option) {
                if ($option->getValue() == $ratingValue) {
                    $rdata['ratings'][$_rating->getId()] = $option->getOptionId();
                }
            }
        }
        /* @var $review Mage_Review_Model_Review */
        $review = Mage::getModel('review/review')->setData($rdata);
        $review->setEntityId($review->getEntityIdByCode(Mage_Review_Model_Review::ENTITY_PRODUCT_CODE));
        $review->setEntityPkValue($item->product_id);
        $review->setStatusId(Mage_Review_Model_Review::STATUS_PENDING);
        $review->setNickname($order->getBillingAddress()->getFirstname() . ' ' . $order->getBillingAddress()->getLastname());
        $review->setData('citystate', $order->getBillingAddress()->getRegionCode());
        $review->setData('country', $order->getBillingAddress()->getCountryId());
        $review->setCustomerId($order->getCustomerId());
        $review->setTitle('');
        $review->setDetail($item->review);
        $review->setStoreId($order->getStoreId());
        $review->setStores(array($order->getStoreId()));
        $pictures = $this->loadAllPicturesFormAnswers($form_data);
        if(count($pictures) > 0) {
            $review->setPhoto($pictures[0]);
        }
        $review->save();
        foreach ($rdata['ratings'] as $ratingId => $optionId) {
            $ratingModel = Mage::getModel('rating/rating');
            $ratingModel->setId($ratingId);
            $ratingModel->setReviewId($review->getId());
            $ratingModel->setCustomerId($order->getCustomerId());
            $ratingModel->addOptionVote($optionId, $review->getEntityPkValue());
        }
        $review->aggregate();
    }

    public function loadAllPicturesFormAnswers($form_data)
    {
        $pictures = [];
        foreach ($form_data->questions as $question) {
            if ($question->question_type != 'picture') {
                continue;
            }
            $this->crawlerPicturesInQuestion($question, $pictures);
        }
        return $pictures;
    }

    public function crawlerPicturesInQuestion($question, &$pictures)
    {
        foreach ($question->answer_value as $url) {
            $h = get_headers($url, 1);
            if ($h['Content-Type'] != 'file' || empty($h['Content-disposition'])) {
                continue;
            }
            $filename = str_replace("attachment; filename=", "", $h['Content-disposition']);
            $filename = trim($filename, '"');
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $body = curl_exec($ch);
            $mediaDir = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA);
            $pathDir = "upload/review". DS . date('Y/m');
            $targetDir = $mediaDir . DS . $pathDir;
            if (!is_dir($targetDir)) {
                mkdir($targetDir, 0777, true);
            }
            file_put_contents($targetDir . DS . $filename, $body);
            $pictures[] = $pathDir . DS . $filename;
            curl_close($ch);
        }
    }

    public function getRatingValueByLabel($form_data, $label)
    {
        $questions = $form_data->questions;
        foreach ($questions as $q) {
            if ($q->question_type != 'ranking_scale' || $q->question_label != $label) {
                continue;
            }
            return $q->answer_value->options[0]->option_id;
        }
        return false;
    }

    public function getAnswers($product_id, $order_id)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://smartforms.ekomi.com/api/v3/answers?product_id={$product_id}&order_ids[]={$order_id}");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            'interface-id: ' . $this->getAPIID(),
            'interface-password: ' . $this->getAPIPassword(),
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (!$result = curl_exec($ch)) {
            return false;
        }
        $result = json_decode($result);
        if (!$result || isset($result->error) || $result->status_code != 200 || $result->data->status != 'success') {
            return false;
        }
        curl_close($ch);
        return $result->data->result[0]->forms[0];
    }

    public function getRatingCollection()
    {
        if (!$this->ratingCollection) {
            $this->ratingCollection = Mage::getModel('rating/rating')
                    ->getResourceCollection()
                    ->addEntityFilter('product')
                    ->setPositionOrder()
                    ->addRatingPerStoreName(Mage::app()->getStore()->getId())
                    ->setStoreFilter(Mage::app()->getStore()->getId())
                    ->load()
                    ->addOptionToItems();
        }
        return $this->ratingCollection;
    }

    public function importFeedback()
    {
        if (!$this->getHelper()->isActive()) {
            return;
        }
        $last_updated = $this->getLastUpdated();
        $data = $this->getProductFeedback($last_updated);
        if (count($data) > 0) {
            foreach ($data as $item) {
                $this->importProductReview($item);
                $last_updated = $item->submitted;
            }
            $this->saveLastUpdated($last_updated);
        }

        Mage::dispatchEvent('maillog_send_email_to_admin', array(
            'message' => "Ekomi Import Feedback Completed.<br/><br/>Script ends at: " . date("d-m-Y H:i:s"),
            'subject' => 'ItsHot.com: Cronjob Ekomi Import Feedback Completed'
                )
        );
    }

    /**
     * @param int $customerId
     * @param int $productId
     * @return bool
     */
    private function isReviewAlreadyRecorded($customerId, $productId)
    {
        if (($customerId == '')
                or ( $productId == '')) {
            return 1;
        }
        /** @var Mage_Review_Model_Resource_Review_Product_Collection $collection */
        $collection = Mage::getModel('review/review')->getCollection();
        $collection->addEntityFilter(null, $productId);
        $collection->addCustomerFilter($customerId);

        return $collection->count();
    }

}
