<?php


$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (!file_exists($path . $shell) && !file_exists($path . 'htdocs/' . $shell)
    && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Medialounge_Ekomi_UpdateAddress extends Mage_Shell_Abstract
{
    public function run()
    {
        $model = Mage::getModel('review/review');
        $collection = $model->getProductCollection();
        $collection->addStoreData();


        $countryId = Mage::getModel('eav/entity_attribute')
            ->loadByCode('customer_address', 'country_id');
        $regionId = Mage::getModel('eav/entity_attribute')
            ->loadByCode('customer_address', 'region');
        $addressTable = Mage::getSingleton('core/resource')
            ->getTableName('customer_address_entity_varchar');

        $collection->getSelect()
            ->columns(['rdt.citystate', 'rdt.country'])
            ->joinLeft(['r' => $addressTable],
                'r.entity_id = rdt.customer_id AND '
                . sprintf("r.attribute_id = %s", $regionId->getId()),
                ['c.region' => 'r.value'])
            ->joinLeft(['c' => $addressTable],
                'c.entity_id = rdt.customer_id AND '
                . sprintf("c.attribute_id = %s", $countryId->getId()),
                ['c.country' => 'c.value']);

        foreach ($collection as $item)
        {
            if ($item->getData('c.country') || $item->getData('c.region')) {
                $review = $model->load($item->getData('review_id'));
                $review->setData('country', $item->getData('c.country'));
                $review->setData('citystate', $item->getData('c.region'));
                $review->save();
            }
        }

        echo "done\n";
    }

}

$shell = new Medialounge_Ekomi_UpdateAddress();
$shell->run();