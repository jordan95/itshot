<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 04/07/2017
 * Time: 14:37
 */

$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (! file_exists($path . $shell) && ! file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Medialounge_Customer extends Mage_Shell_Abstract
{
    public function run()
    {
        $cmsPage = Mage::getModel('cms/page');
        $cmsPage->setTitle('Category Sitemap');
        $cmsPage->setIdentifier('category-sitemap');
        $cmsPage->setRootTemplate('one_column');
        $cmsPage->setContent('<p>{{block type="medialounge_catalog/category_sitemap" name="categoty.sitemap" template="medialounge/sitemap/categories.phtml" }}</p>');
        $cmsPage->save();

        $blockPermission = Mage::getModel('admin/block');
        $blockPermission->setBlockName('medialounge_catalog/category_sitemap');
        $blockPermission->setIsAllowed(1);
        $blockPermission->save();

        echo "done\n";
    }

}

$shell = new Medialounge_Customer();
$shell->run();