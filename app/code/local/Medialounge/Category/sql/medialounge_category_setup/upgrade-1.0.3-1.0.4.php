<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_category', 'intro_text', array(
    'type' => 'varchar',
    'label' => 'Intro Text (Alternartive Cat)',
    'input' => 'textarea',
    'default' => '',
    'required' => false,
    'wysiwyg_enabled' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group' => 'General Information',
));

$installer->endSetup();