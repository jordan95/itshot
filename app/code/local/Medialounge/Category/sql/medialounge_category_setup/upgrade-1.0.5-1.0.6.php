<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_category', 'summary', array(
    'type' => 'varchar',
    'label' => 'Summary',
    'sort_order' => 4,
    'input' => 'textarea',
    'default' => '',
    'required' => false,
    'wysiwyg_enabled' => true,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group' => 'General Information',
));

$installer->addAttribute('catalog_category', 'sub_heading', array(
    'type' => 'varchar',
    'label' => 'Sub Heading',
    'sort_order' => 4,
    'input' => 'text',
    'default' => '',
    'required' => false,
    'wysiwyg_enabled' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group' => 'General Information',
));

$installer->endSetup();