<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_category', 'ml_force_productlisting', array(
    'type'     => 'int',
    'group'    => 'General Information',
    'input'    => 'select',
    'label'    => 'Force Show Products',
    'required' => false,
    'source'   => 'eav/entity_attribute_source_boolean',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
));

$installer->endSetup();