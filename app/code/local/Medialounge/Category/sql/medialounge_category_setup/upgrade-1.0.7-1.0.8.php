<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();
$installer->updateAttribute('catalog_category', 'intro_text', 'frontend_label', 'Short Description');
$installer->endSetup();