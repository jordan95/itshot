<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();
$installer->updateAttribute('catalog_category', 'h2_tag_name', 'frontend_label', 'H1 Custom Heading');
$installer->endSetup();