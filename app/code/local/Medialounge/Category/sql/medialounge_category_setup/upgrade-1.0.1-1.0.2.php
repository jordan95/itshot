<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$codes['ml_featured_subcategory'] = 'Featured Category (In Sub Category Grid)';
$codes['ml_layoutsubcategory'] = 'Alternative Layout';
$codes['ml_hover'] = 'Hover Text (HP Top Slider Bar)';
$codes['ml_featuredcategory'] = 'Featured Category (HP Top Slider Bar)';

foreach ($codes as $code=>$label) {
    $attributeId = $installer->getAttributeId('catalog_category', $code);

    if ($attributeId) {
        $installer->updateAttribute('catalog_category', $attributeId, 'frontend_label', $label);
    }
}



$installer->endSetup();