<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_category', 'h2_tag_name', array(
    'type' => 'varchar',
    'label' => 'H2 Tag Name',
    'sort_order' => 3,
    'input' => 'text',
    'default' => '',
    'required' => false,
    'wysiwyg_enabled' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group' => 'General Information',
));

$installer->endSetup();