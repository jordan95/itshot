<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();
$installer->updateAttribute('catalog_category', 'summary', 'backend_type', 'text');
$installer->endSetup();