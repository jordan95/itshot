<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Category
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Category_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function hasSubCategory(Mage_Catalog_Model_Category $category)
    {
        /** @var Mage_Catalog_Model_Resource_Category_Collection $collection */
        //$collection = $category->getChildrenCategories();
        if (Mage::helper('catalog/category_flat')->isEnabled() && $category->getResource() instanceof  OpenTechiz_Catalog_Model_Resource_Category_Flat) {
            $children = (array)$category->getChildrenCategories();
            $childrenCount = count($children);         
        } else {
            $children = $category->getChildrenCategories();
            if(is_object($children)){
                $childrenCount = $children->count();
            }elseif(is_array($children)){
                $childrenCount = count($children);     
            }else{
                $childrenCount = 0;
           }
        }
        return $childrenCount > 1;
    }

    /**
     * @return Mage_Catalog_Model_Category
     */
    private function getCatalogCategoryModel()
    {
        return Mage::getModel('catalog/category');
    }
}