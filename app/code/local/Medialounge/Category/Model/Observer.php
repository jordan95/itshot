<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Category
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Category_Model_Observer
{
    public function addCategoryLayoutHandle(Varien_Event_Observer $observer)
    {
        $category = Mage::registry('current_category');

        if (!($category instanceof Mage_Catalog_Model_Category) || 'PAGE' == $category->getDisplayMode()) {
            return;
        }

        $takeOverLayout = '';
        $show_category_rel_link = true;
        if ($category->getData('ml_layoutsubcategory')) {
            $takeOverLayout = 'catalog_category_feature';
            $show_category_rel_link = false;
        } elseif (!$category->getData('ml_force_productlisting') && $this->getMediaLoungeCategoryHelper()->hasSubCategory($category)) {
            $takeOverLayout = 'catalog_category_sublisting';
            $show_category_rel_link = false;
        }
        if(Mage::registry('show_category_rel_link') === null){
            Mage::register('show_category_rel_link', $show_category_rel_link);
        }

        if ($takeOverLayout) {
            /** @var Mage_Core_Model_Layout_Update $update */
            $update = $observer->getEvent()->getLayout()->getUpdate();
            $handles = $update->getHandles();
            $update->resetHandles();

            foreach ($handles as $handle) {
                $update->addHandle($handle);
                if (($handle == 'catalog_category_view')
                    or ($handle == 'catalog_category_default')
                    or ($handle == 'catalog_category_layered')
                ) {
                    $update->removeHandle($handle);
                    $update->addHandle($takeOverLayout);
                }
            }
        }
    }

    /**
     * @return Medialounge_Category_Helper_Data
     */
    private function getMediaLoungeCategoryHelper()
    {
        return Mage::helper('medialounge_category');
    }
}