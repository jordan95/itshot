<?php
/**
 * Widget block description
 *
 * @category   Medialounge
 * @package    Medialounge_BlogBlocks
 * @author     Javier Villanueva <javier@medialounge.co.uk>
 */
class Medialounge_BlogBlocks_Block_Widget_Block
    extends Mage_Cms_Block_Widget_Block
{
    /**
     * Prepare blog posts from external xml feed
     *
     * @return Medialounge_BlogBlocks_Block_Widget_Block
     */
    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();
        $blogs = null;
        if(Mage::getStoreConfig('rss/blog/enable')){
            $xml = simplexml_load_file(Mage::getStoreConfig('rss/blog/url_feed'));
            if ($xml) {
                $blogs = $xml->channel->item;
            }
        }
        
        $this->setBlogPosts($blogs);
        return $this;
    }

    /**
     * Get first image from post content
     *
     * @param  string $content
     * @return string
     */
    public function getPostImage($content)
    {
        $regex = '/https?\:\/\/[^\" ]+/i';
        preg_match($regex, $content, $matches);

        return str_replace('http:', 'https:', $matches[0]);
    }

    /**
     * Limit number of words in a string
     *
     * @param  string $words
     * @param  int    $limit
     * @param  string $append
     * @return string
     */
    public function truncateWords($words, $limit, $append = '&hellip;')
    {
        $limit = $limit + 1;
        $words = explode(' ', $words, $limit);
        array_pop($words);
        $words = implode(' ', $words) . $append;

        return $words;
    }
}
