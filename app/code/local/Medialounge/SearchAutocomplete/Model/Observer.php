<?php

class Medialounge_SearchAutocomplete_Model_Observer
{
    public function beforeSaveCatalogSearch($observer)
    {
        $query = $observer->getData('catalogsearch_query');
        if($query->getId()) {
            return;
        }
        $query->setData('display_in_terms', 0);
    }
}