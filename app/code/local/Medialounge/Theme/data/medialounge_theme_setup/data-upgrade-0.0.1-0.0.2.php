<?php

/** @var \Mage_Admin_Model_Variable $model */
$model = Mage::getModel('admin/variable');
$model->setData(
    array('variable_name'=>'general/store_information/non_us_phone',
            'is_allowed'=>1
    ));
$model->save();