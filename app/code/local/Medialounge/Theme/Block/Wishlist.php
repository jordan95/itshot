<?php

class Medialounge_Theme_Block_Wishlist
    extends Mage_Wishlist_Block_Customer_Sidebar
{
    protected $_product_ids = [];

    public function _construct()
    {
        $this->_product_ids = $this->getWishlistProductIds();
        parent::_construct();
    }

    public function getWishlistItems()
    {
        if ($this->hasWishlistItems()) {
            return Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter(
                    'entity_id', array('in' => [$this->_product_ids])
                );
        }

        return [];
    }

    public function getWishlistProductIds()
    {
        $ids = [];
        foreach (
            Mage::helper('medialounge_theme')->getFavoritesItems() as $item
        ) {
            $ids[] = $item->getProductId();
        }
        return $ids;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getTemplate()) {
            return '';
        }
        $html = $this->renderView();
        return $html;
    }

    public function hasWishlistItems()
    {
        return $this->getItemCount() > 0;
    }

    public function getItemCount()
    {
        return count($this->_product_ids);
    }
}