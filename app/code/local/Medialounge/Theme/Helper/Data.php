<?php
class Medialounge_Theme_Helper_Data extends Mage_Core_Block_Abstract
{
    protected $lastListId;

    public function _construct()
    {
        $current_customer = Mage::getSingleton('customer/session')->getCustomer();
        $this->lastListId = Mage::getModel('amlist/list')->getLastListId($current_customer->getId());
    }

    public function getFavoritesItems() {
        return Mage::getResourceModel('amlist/item_collection')
            ->addFieldToFilter('list_id', $this->getLastListId())
            ->addFieldToSelect('product_id')
            ->setOrder('item_id')
            ->load();
    }

    public function getLastListId() {
        return $this->lastListId;
    }

    public function getFeedbackLink() {
        $path  = Mage::getModel('core/variable')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->loadByCode('feedback_link')
            ->getValue('text');
        return Mage::getUrl($path);
    }
}