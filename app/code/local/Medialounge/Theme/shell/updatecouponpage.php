<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */

$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (!file_exists($path . $shell) && !file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Coupon_Page_Init extends Mage_Shell_Abstract
{
    public function run()
    {
        $cmsCollection = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', 'diamond-jewelry-coupons-promotions.aspx');
        if ($cmsCollection->count()) {
            $cms = $cmsCollection->getFirstItem();
            $cms->setData('stores', [0]);
            $cms->setData('layout_update_xml', '<reference name="content">
            <block type="page/html" name="social_popup" template="amsocial/page.phtml">
                <block type="page/html_head" name="head" as="head">
                    <action method="addJs"><script>prototype/prototype.js</script></action>
                    <action method="addJs" ifconfig="dev/js/deprecation"><script>prototype/deprecation.js</script></action>
                    <action method="addJs"><script>lib/ccard.js</script></action>
                    <action method="addJs"><script>prototype/validation.js</script></action>
                    <action method="addJs"><script>scriptaculous/builder.js</script></action>
                    <action method="addJs"><script>scriptaculous/effects.js</script></action>
                    <action method="addJs"><script>scriptaculous/dragdrop.js</script></action>
                    <action method="addJs"><script>scriptaculous/controls.js</script></action>
                    <action method="addJs"><script>scriptaculous/slider.js</script></action>
                    <action method="addJs"><script>varien/js.js</script></action>
                    <action method="addJs"><script>varien/form.js</script></action>
                    <action method="addJs"><script>varien/menu.js</script></action>
                    <action method="addJs"><script>mage/translate.js</script></action>
                    <action method="addJs"><script>mage/cookies.js</script></action>

                    <action method="addCss"><stylesheet>css/styles.css</stylesheet></action>
                    <action method="addItem"><type>skin_css</type><name>css/styles-ie.css</name><params/><if>lt IE 8</if></action>
                    <action method="addCss"><stylesheet>css/widgets.css</stylesheet></action>
                    
                    <action method="addItem"><type>js</type><name>lib/ds-sleight.js</name><params/><if>lt IE 7</if></action>
                    <action method="addItem"><type>skin_js</type><name>js/ie6.js</name><params/><if>lt IE 7</if></action>

                    <action method="addItem"><type>js</type><name>amasty/amsocial/social.js</name><params/></action>
                    <action method="addItem"><type>skin_css</type><name>css/amasty/amsocial/popup.css</name><params/></action>
                </block>
                <block type="core/text_list" name="content" as="content" translate="label">
                    <block type="amsocial/popup" name="amsocial.popup" />
                </block>
            </block>
        </reference>');
            $cms->save();
        }
    }
}

$test = new Coupon_Page_Init;
$test->run();
