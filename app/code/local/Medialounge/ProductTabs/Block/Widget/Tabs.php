<?php
/**
 * Widget block description
 *
 * @category   Medialounge
 * @package    Medialounge_ProductTabs
 * @author     Javier Villanueva <javier@medialounge.co.uk>
 */
class Medialounge_ProductTabs_Block_Widget_Tabs
    extends Mage_Catalog_Block_Product_Abstract
    implements Mage_Widget_Block_Interface
{
    const ATTRIBUTE_POPULARITY_CODE = 'c2c_popularity';
    const ATTRIBUTE_DISP_CUSTOM_STOCK_MSG_CODE = 'c2c_disp_custom_stock_msg';
    
    public function addStockFilter($collection){
        $collection->addAttributeToFilter(self::ATTRIBUTE_DISP_CUSTOM_STOCK_MSG_CODE, array(
            'or' => array(
                array('eq' => 0), 
                array('null' => true))
            ), 'left');
        return $collection;
    }
    
    public function getBestsellingProducts()
    {
        $products = [];
        $storeId = Mage::app()->getStore()->getId();

        $itemsCollection = Mage::getResourceModel('sales/order_item_collection')
            ->join('order', 'order_id=entity_id')
            ->addFieldToFilter('main_table.store_id', array('eq' => $storeId))
            ->setOrder('main_table.created_at', 'desc')
            ->setPageSize($this->getLimit());
        $select = $itemsCollection->getSelect();
        $select->limit($this->getLimit());
        $select->columns( array( 'item_created' => new Zend_Db_Expr("max(main_table.created_at)")));
        $select->order('item_created DESC');
        $select->group(array('main_table.product_id'));
        $collection = Mage::getModel('catalog/product')->getCollection();
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collection->addAttributeToSelect($attributes);
        if ($itemsCollection->count() > 0) {
            $pids = array();
            foreach ($itemsCollection as $item) {
                $pids[] = $item->getProductId();
            }
            $collection->addAttributeToFilter('entity_id', array('in' => $pids));
        }
        $collection->addFinalPrice()
            ->addStoreFilter()
            ->addAttributeToFilter('status', 1)
            ->addUrlRewrite();
        $this->addStockFilter($collection);
        $collection->getSelect()->limit($this->getLimit());

        return $collection;
    }
    
    public function getFeaturedProducts() {
        $storeId   = Mage::app()->getStore()->getId();
        $dealProducts = Mage::helper('opentechiz_deals')->getDealProducts(OpenTechiz_Deals_Model_Deal::TYPE_WEEKLY);
        $productIds = [];
        /** @var OpenTechiz_Deals_Model_Deal $dealProduct */
        foreach ($dealProducts as $dealProduct) {
            $productIds[] = $dealProduct->getProductId();
        }
        $collection = Mage::getModel('catalog/product')->getCollection();
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collection->setStoreId($storeId)
            ->addAttributeToSelect($attributes)
            ->addFinalPrice()
            ->addStoreFilter()
            ->setPageSize($this->getLimit())
            ->addAttributeToFilter('entity_id', array('in' => $productIds))
            ->addUrlRewrite();
        $select = $collection->getSelect();
        $this->addStockFilter($collection);
        $select->limit($this->getLimit());
        return $collection;
    }

    public function getSaleProducts()
    {
        $todayDate = strftime("%Y-%m-%d", Mage::app()->getLocale()->storeTimeStamp(Mage::app()->getStore()->getId()));
        $storeId   = Mage::app()->getStore()->getId();
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect($attributes)
            ->addAttributeToFilter('special_price', array('gt' => 0), 'left')
            ->addAttributeToFilter('special_from_date', array('date' => true, 'to' => $todayDate))
            ->addAttributeToFilter('special_to_date', array('or'=> array(
                0 => array('date' => true, 'from' => $todayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToSort('created_at', 'desc')
            ->addFinalPrice()
            ->addStoreFilter()
            ->setPageSize($this->getLimit())
            ->addAttributeToFilter('status', 1)
            ->addUrlRewrite();
        $this->addStockFilter($products);

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInSearchFilterToCollection($products);

        return $products;
    }
}
