<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_category', 'ml_featuredcategory', array(
    'type'     => 'int',
    'group'    => 'General Information',
    'input'    => 'select',
    'label'    => 'Featured Category',
    'required' => false,
    'source'   => 'eav/entity_attribute_source_boolean',
));

$installer->addAttribute('catalog_category', 'ml_hover', array(
    'type'     => 'text',
    'group'    => 'General Information',
    'input'    => 'textarea',
    'label'    => 'Hover Text',
    'required' => false,
));

$installer->addAttribute('catalog_category', 'ml_image', array(
    'type'     => 'varchar',
    'group'    => 'General Information',
    'input'    => 'image',
    'backend'  => 'catalog/category_attribute_backend_image',
    'label'    => 'Secondary Image',
    'required' => false,
));

$installer->endSetup();