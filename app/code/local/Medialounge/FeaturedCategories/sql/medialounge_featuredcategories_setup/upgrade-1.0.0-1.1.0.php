<?php
$installer = $this;

$installer->startSetup();

$installer->updateAttribute(
    'catalog_category',
    'ml_image',
    'frontend_label',
    'HP Featured Category Image'
);

$installer->endSetup();
