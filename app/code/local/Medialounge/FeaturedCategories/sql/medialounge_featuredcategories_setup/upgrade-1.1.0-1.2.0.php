<?php
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_category', 'ml_sort_order', array(
    'type'     => 'varchar',
    'group'    => 'General Information',
    'input'    => 'text',
    'label'    => 'HP Featured Category Position',
    'required' => false,
    'sort_order' => 82,
));

$installer->endSetup();
