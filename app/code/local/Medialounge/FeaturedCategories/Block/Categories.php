<?php
class Medialounge_FeaturedCategories_Block_Categories extends Mage_Core_Block_Template
{
     const CACHE_TAG = 'featured_categories';
     
    protected function _construct()
    {
        $this->addData(array(
            'cache_lifetime' => 0,
            'cache_tags' => array(self::CACHE_TAG),
            'cache_key' => self::CACHE_TAG,
        ));
    }
    /**
     * Get all featured categories
     *
     * @return Mage_Catalog_Model_Resource_Category_Collection Featured categories collection
     */
    public function getFeaturedCategories()
    {
        $categories = Mage::getModel('catalog/category')
                        ->getCollection()
                        ->addAttributeToSelect('*')
                        ->addAttributeToSort('ml_sort_order', 'asc')
                        ->addAttributeToFilter('ml_featuredcategory', 1);

        return $categories;
    }

    public function getSecondaryImage($category)
    {
        $image = $category->getMlImage();

        if ($image) {
            return Mage::getBaseUrl('media') . 'catalog/category/' . $image;
        }

        return false;
    }
}