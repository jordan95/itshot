<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 06/07/2017
 * Time: 17:42
 */

class Medialounge_Customer_IndexController extends Mage_Core_Controller_Front_Action
{
    const XML_PATH_EMAIL_RECIPIENT  = 'contacts/email/recipient_email';
    const XML_PATH_EMAIL_SENDER     = 'contacts/email/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE   = 'contacts/email/email_template';

    public function postAction()
    {
        $post = $this->getRequest()->getPost();
        if ( $post ) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['comment']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

//                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
//                    $error = true;
//                }
//
//                if (isset($post['g-recaptcha-response']) && !$this->validateRecaptcha($post['g-recaptcha-response'])) {
//                    $error = true;
//                }

                if ($error) {
                    throw new Exception();
                }
                $mailTemplate = Mage::getModel('core/email_template');
                /* @var $mailTemplate Mage_Core_Model_Email_Template */
                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->setReplyTo($post['email'])
                    ->sendTransactional(
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
                        null,
                        array('data' => $postObject)
                    );

                if (!$mailTemplate->getSentSuccess()) {
                    throw new Exception();
                }

                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addSuccess(Mage::helper('contacts')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                $this->_redirectReferer();
                return;
            } catch (Exception $e) {
                $this->_getSession()->setFeedbackData($post);
                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
                $this->_redirectReferer();
                return;
            }

        } else {
            $this->_redirect('*/*/');
        }
    }

    private function validateRecaptcha($response)
    {
        if (!extension_loaded('curl')) {
            return null;
        }

        $curl = new Varien_Http_Adapter_Curl();
        $curl->setConfig(array(
            'timeout'   => 2
        ));
        $post = ['secret' => $this->getHelper()->getRecaptchaSecretKey(),
            'response' => $response
        ];
        $str = '';
        foreach ($post as $key=>$value) {
            if ($str !='') $str .= '&';
            $str .= sprintf('%s=%s', $key, urlencode($value));
        }

        $curl->write(Zend_Http_Client::POST, 'https://www.google.com/recaptcha/api/siteverify', '1.1', ['Content-Type: application/x-www-form-urlencoded; charset=utf-8', 'Content-Length: ' . strlen($str)], $str);
        $data = $curl->read();
        if ($data === false) {
            return false;
        }
        $data = preg_split('/^\r?$/m', $data, 2);
        $response = $this->getCoreHelper()->jsonDecode(trim($data[1]));
        $curl->close();

        if (isset($response['success']) && $response['success']) {
            return true;
        }

        return false;
    }

    /**
     * @return \Medialounge_Customer_Helper_Data
     */
    private function getHelper()
    {
        return Mage::helper('medialounge_customer');
    }

    /**
     * @return Mage_Core_Helper_Data
     */
    private function getCoreHelper()
    {
        return Mage::helper('core');
    }

    /**
     * Retrieve adminhtml session model object
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }
}