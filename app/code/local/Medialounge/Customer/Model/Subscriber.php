<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 05/09/2017
 * Time: 11:08
 */

class Medialounge_Customer_Model_Subscriber extends Mage_Newsletter_Model_Subscriber
{
    public function __construct()
    {
        parent::__construct();
        $this->setImportMode(true); // this disable the email for SUbscription to be sent
    }
}