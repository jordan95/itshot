<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 04/07/2017
 * Time: 11:44
 */


$cmsPage = Mage::getModel('cms/page');
$cmsPage->setTitle('Referral Program');
$cmsPage->setIdentifier('referral-program');
$cmsPage->setRootTemplate('one_column');
$cmsPage->setContent('<p>Top content {{block type="customer/form_login" name="customer_form_login" template="customer/form/login.phtml" }} Footer content</p>');
$cmsPage->save();

$blockPermission = Mage::getModel('admin/block');
$blockPermission->setBlockName('customer/form_login');
$blockPermission->setIsAllowed(1);
$blockPermission->save();