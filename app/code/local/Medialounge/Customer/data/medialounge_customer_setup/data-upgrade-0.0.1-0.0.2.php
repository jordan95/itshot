<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 04/07/2017
 * Time: 11:44
 */


$cmsPage = Mage::getModel('cms/page');
$cmsPage->setTitle('Feedback Form');
$cmsPage->setIdentifier('feedback-form');
$cmsPage->setRootTemplate('one_column');
$cmsPage->setContent('<p>Top content {{block type="medialounge_customer/feedback" name="contactForm" template="medialounge/feedback/form.phtml" }} Footer content</p>');
$cmsPage->save();

$blockPermission = Mage::getModel('admin/block');
$blockPermission->setBlockName('medialounge_customer/feedback');
$blockPermission->setIsAllowed(1);
$blockPermission->save();