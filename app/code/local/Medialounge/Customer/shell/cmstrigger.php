<?php


$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (! file_exists($path . $shell) && ! file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Medialounge_Customer extends Mage_Shell_Abstract
{
    public function run()
    {
        $cmsPage = Mage::getModel('cms/page');
        $cmsPage->setTitle('Referral Program');
        $cmsPage->setIdentifier('referral-program');
        $cmsPage->setRootTemplate('one_column');
        $cmsPage->setContent('<p>Top content {{block type="customer/form_login" name="customer_form_login" template="customer/form/login.phtml" }} Footer content</p>');
        $cmsPage->save();

        $blockPermission = Mage::getModel('admin/block');
        $blockPermission->setBlockName('customer/form_login');
        $blockPermission->setIsAllowed(1);
        $blockPermission->save();

        echo "done\n";
    }

}

$shell = new Medialounge_Customer();
$shell->run();