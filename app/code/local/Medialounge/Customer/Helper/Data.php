<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 20/07/2017
 * Time: 15:49
 */

class Medialounge_Customer_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getRecaptchaSiteKey()
    {
        return Mage::getStoreConfig('customer/create_account/recaptcha_site_key');
    }

    public function getRecaptchaSecretKey()
    {
        return Mage::getStoreConfig('customer/create_account/recaptcha_server_key');
    }
}