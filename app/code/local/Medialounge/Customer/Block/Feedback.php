<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 04/07/2017
 * Time: 17:01
 */

class Medialounge_Customer_Block_Feedback extends Mage_Core_Block_Template
{
    
     protected function _construct()
    {
        parent::_construct();
        $studioforty9_recaptcha = Mage::getSingleton('core/layout')->createBlock('studioforty9_recaptcha/explicit');
        $studioforty9_recaptcha->setTemplate('studioforty9/recaptcha/explicit.phtml');
        $studioforty9_recaptcha->setAllow(true);
        $this->setChild('studioforty9.recaptcha.explicit', $studioforty9_recaptcha);
    }

    public function getRecaptchaSiteKey()
    {
        return $this->getCustomerHelper()->getRecaptchaSiteKey();
    }

    public function getFormAction()
    {
        return Mage::getUrl('customerfeedback/index/post');
    }

    /**
     * @return \Medialounge_Customer_Helper_Data
     */
    private function getCustomerHelper()
    {
        return Mage::helper('medialounge_customer');
    }

    public function getData($field = '', $index = null)
    {
        $fieldValue = '';

        if ($field == 'name') {
            $fieldValue = $this->helper('contacts')->getUserName();
        }

        if ($field == 'email') {
            $fieldValue = $this->helper('contacts')->getUserEmail();
        }

        if (!$fieldValue) {
            $feedbackData = $this->_getSession()->getFeedbackData();
            if (isset($feedbackData[$field])) {
                $fieldValue = $feedbackData[$field];
            }
        }

        return $fieldValue;
    }

    /**
     * Retrieve adminhtml session model object
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }
}