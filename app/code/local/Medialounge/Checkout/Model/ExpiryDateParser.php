<?php
/**
 * Created by PhpStorm.
 * User: herve
 * Date: 17/04/2018
 * Time: 09:44
 */

class Medialounge_Checkout_Model_ExpiryDateParser
{
    private $expiryDate;

    public function __construct($date)
    {
        $this->expiryDate = $date;
    }

    public function getExpMonth()
    {
        $data = $this->parseExpiryDate();
        return $data[0];
    }

    public function getExpYear()
    {
        $data = $this->parseExpiryDate();
        return $data[1];
    }

    /**
     * @return mixed
     */
    private function parseExpiryDate()
    {
        $expMonth = $expYear = '';

        if (strpos($this->expiryDate, '-') !== false) {
            $parts = explode('-', $this->expiryDate);
            $expMonth = $parts[0];
            $expYear = $parts[1];
        }
        return [
            $expMonth,
            $expYear
        ];
    }
}