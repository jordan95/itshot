<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 11/07/2017
 * Time: 16:11
 */

class Medialounge_Checkout_Model_System_Config_Source_Paymentstatus
{
    public function toOptionArray()
    {
        $options[] = array(
            'value' => '',
            'label' => 'Normal Payment Flow',
        );

        $options[] = array(
            'value' => 'failed',
            'label' => 'Failed Payment',
        );

        $options[] = array(
            'value' => 'timeout',
            'label' => 'Payment Timeout',
        );

        $options[] = array(
            'value' => 'declined',
            'label' => 'Declined Payment',
        );

        return $options;
    }
}