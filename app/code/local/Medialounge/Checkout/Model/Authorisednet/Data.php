<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 23/05/2017
 * Time: 16:08
 */

class Medialounge_Checkout_Model_Authorisednet_Data extends Mage_Core_Model_Abstract
{
    const PAYMENT_RECORD_PENDING = 'pending';

    const PAYMENT_RECORD_READ = 'read';

    public function removeInvalidTransaction($order)
    {
        /** @var  $collection */
        $collection = Mage::getResourceModel('sales/order_payment_transaction_collection');
        $collection->addOrderIdFilter($order->getId());

        foreach ($collection as $transaction) {
            /** @var Mage_Sales_Model_Order_Status_History $statusHistory */
            foreach ($order->getAllStatusHistory() as $statusHistory) {
                if (!$statusHistory->getIsNew()) {
                    continue;
                }
                if (stripos($statusHistory->getComment(), sprintf('Authorize.Net Transaction ID %s.', $transaction->getTxnId())) ||
                    stripos($statusHistory->getComment(), 'amount of')) {
                    $statusHistory->delete();
                    continue;
                }
            }
            $transaction->delete();
        }
    }

    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('medialounge_checkout/authorisednet_data');
    }

    public function addRecord($quoteId, $customerId, $info, $exception)
    {
        $this->getResource()->addRecord($quoteId, $customerId, $info, $exception);
    }

    public function updatePaymentQuoteRecord($quoteId, $request, $result)
    {
        $this->getResource()->addPaymentQuoteRecord($quoteId, $request, $result);
    }

    public function transferPaymentQuoteRecordToOrder($quoteId, $orderId)
    {
        $this->getResource()->transferPaymentQuoteRecordToOrder($quoteId, $orderId);
    }

    /**
     * @param int $quoteId
     * @return []
     */
    public function getRecordInfoByQuoteId($quoteId)
    {
        return $this->getResource()->getRecordInfoByQuoteId($quoteId);
    }

    public function delete()
    {
        $this->getResource()->deleteRecord($this->getId());
    }

    public function reconcileOrder($quoteId, $orderId)
    {
        $this->getResource()->updateOrderId($quoteId, $orderId);
    }
}