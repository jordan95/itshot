<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 01/06/2017
 * Time: 09:05
 */
class Medialounge_Checkout_Model_Authorisednet_Cron
{
    public function clearOldInfo()
    {
        $date = new Zend_date(Mage::getModel('core/date')->timestamp());
        $date->subDay(30);

        $collection = Mage::getModel('medialounge_checkout/authorisednet_data')->getCollection();
        $collection->addFieldToFilter('updated_at', ['to' => $date->toString(Varien_Date::DATE_INTERNAL_FORMAT)]);

        foreach ($collection as $authorisednetData) {
            $authorisednetData->delete();
        }
    }
}