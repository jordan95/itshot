<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 30/05/2017
 * Time: 11:46
 */

class Medialounge_Checkout_Model_Authorisednet_Paygate extends Mage_Paygate_Model_Authorizenet
{
    private static $timeout = 30;
    private static $timeoutStatus = 'timeout';
    private static $declinedPaymentStatus = 'declined';

    protected $_canSaveCc   = true;

    /**
     * Post request to gateway and return responce
     *
     * @param Mage_Paygate_Model_Authorizenet_Request $request)
     * @return Mage_Paygate_Model_Authorizenet_Result
     */
    protected function _postRequest(Varien_Object $request)
    {
        $debugData = array('request' => $request->getData());

        $result = Mage::getModel('paygate/authorizenet_result');

        $client = new Varien_Http_Client();

        $uri = $this->getConfigData('cgi_url');
        $client->setUri($uri ? $uri : self::CGI_URL);
        $client->setConfig(array(
            'maxredirects' => 0,
            'timeout' => self::$timeout,
            'verifyhost' => 2,
            'verifypeer' => true,
            //'ssltransport' => 'tcp',
        ));
        //add log to trace back when case request failed
        Mage::log('Request:', Zend_Log::ERR, 'authorizenet.log');
        Mage::log($request->getData(), Zend_Log::ERR, 'authorizenet.log');
        foreach ($request->getData() as $key => $value) {
            $request->setData($key, str_replace(self::RESPONSE_DELIM_CHAR, '', $value));
        }
        $request->setXDelimChar(self::RESPONSE_DELIM_CHAR);

        $client->setParameterPost($request->getData());
        $client->setMethod(Zend_Http_Client::POST);
        
        $paymentStatus = '';
        try {
            $timeInit = time();
            $response = $client->request();
            //add log to trace back when case request failed
            Mage::log('Respond:', Zend_Log::ERR, 'authorizenet.log');
            $array = [];
            $array['Headers'] = $response->getHeaders();
            $array['Body'] = $response->getBody();
            Mage::log($array, Zend_Log::ERR, 'authorizenet.log');

            //update authorizednet data after order edit payment (#3143)
            if (isset($response)) {
                $responseBody = $response->getBody();
                $r = explode(self::RESPONSE_DELIM_CHAR, $responseBody);
                $respondCode = $r[0];
                if($respondCode != Mage_Paygate_Model_Authorizenet::RESPONSE_CODE_APPROVED){
                    $errorObject = new Varien_Object();
                    $errorObject->setCode($respondCode);
                    $errorObject->setMessage('payment was not approved');
                    $params = Mage::app()->getRequest()->getParams();
                    if(isset($params['order_id']) && $params['order_id']){
                        $order = Mage::getModel('sales/order')->load($params['order_id']);
                        $this->savePaymentDetailsToHandlePaymentError($request, $errorObject, $order);
                    }
                }else{
                    //remove when cc is approved after order edit payment
                    $params = Mage::app()->getRequest()->getParams();
                    if(isset($params['order_id']) && $params['order_id']) {
                        $order = Mage::getModel('sales/order')->load($params['order_id']);
                        Mage::helper('opentechiz_salesextend/authorizednet_data')->deleteExistingAuthorizedDataErrorRecord($order);
                    }
                }
            }

            $this->handleResponseNotApproved($response);
        } catch (Exception $e) {
            $error_checker = new Varien_Object();
            $error_checker->setShouldBeCatchException(false);
            Mage::dispatchEvent('credit_card_failure', array(
                'error_checker' => $error_checker
            ));
            if($error_checker->getShouldBeCatchException()){
                Mage::register('payment_declined', true);
                Mage::log("\n" . $e->__toString(), Zend_Log::ERR, 'authorizenet.log');
                Mage::throwException($error_checker->getMessage());
            }
            $result->setResponseCode(-1)
                ->setResponseReasonCode($e->getCode())
                ->setResponseReasonText($e->getMessage());

            $debugData['result'] = $result->getData();
            $this->_debug($debugData);

            switch ($e->getCode()) {
                case 2:
                case 3:
                case 4:
                    $paymentStatus = 'manual';
                    Mage::log("\n" . $e->__toString(), Zend_Log::ERR, 'authorizenet.log');
                    break;
                default:
                    $timeLapse = time() - $timeInit; // verify timelapse is close to timeout or in fact bigger as well
                    if ((abs($timeLapse - self::$timeout) < 2) or ($timeLapse > self::$timeout)) {
                        $paymentStatus = 'manual';
                        Mage::log("\n" . $e->__toString(), Zend_Log::ERR, 'authorizenet.log');
                    } else {
                        $paymentStatus = 'failed';
                    }
            }

            if ($paymentStatus == 'failed') {
                Mage::log("\n" . $e->__toString(), Zend_Log::ERR, 'authorizenet.log');
            }

            $this->savePaymentDetailsToHandlePaymentError($request, $e);
        }

        if (isset($response)) {
            $responseBody = $response->getBody();

            $r = explode(self::RESPONSE_DELIM_CHAR, $responseBody);

            if ($r) {
                $result->setResponseCode((int)str_replace('"','',$r[0]))
                    ->setResponseSubcode((int)str_replace('"','',$r[1]))
                    ->setResponseReasonCode((int)str_replace('"','',$r[2]))
                    ->setResponseReasonText($r[3])
                    ->setApprovalCode($r[4])
                    ->setAvsResultCode($r[5])
                    ->setTransactionId($r[6])
                    ->setInvoiceNumber($r[7])
                    ->setDescription($r[8])
                    ->setAmount($r[9])
                    ->setMethod($r[10])
                    ->setTransactionType($r[11])
                    ->setCustomerId($r[12])
                    ->setMd5Hash($r[37])
                    ->setCardCodeResponseCode($r[38])
                    ->setCAVVResponseCode( (isset($r[39])) ? $r[39] : null)
                    ->setSplitTenderId($r[52])
                    ->setAccNumber($r[50])
                    ->setCardType($r[51])
                    ->setRequestedAmount($r[53])
                    ->setBalanceOnCard($r[54])
                ;
            }
        }

        if ($this->isApprovedTransaction($paymentStatus)) {
            $result->setResponseCode(\Mage_Paygate_Model_Authorizenet::RESPONSE_CODE_APPROVED);

        } elseif ($paymentStatus == 'failed') {
            Mage::throwException(
                Mage::helper('paygate')->__('Error in payment gateway.')
            );
        }

        $debugData['result'] = $result->getData();
        $this->_debug($debugData);

        /** @var Medialounge_Checkout_Model_Authorisednet_Data $model */
        $model = Mage::getModel('medialounge_checkout/authorisednet_data');
        $model->updatePaymentQuoteRecord($this->getQuote()->getId(), $request, $result);

        return $result;
    }

    /**
     * Prepare info instance for save
     *
     * @return Mage_Payment_Model_Abstract
     */
    public function prepareSave()
    {
        $info = $this->getInfoInstance();
        if ($this->_canSaveCc) {
            $info->setCcNumberEnc($info->encrypt($info->getCcNumber()));
            $info->setCcStatus($info->getCcStatus());

        }
        //$info->setCcCidEnc($info->encrypt($info->getCcCid()));
        $info->setCcNumber(null)
            ->setCcCid(null);
        return $this;
    }

    protected function savePaymentDetailsToHandlePaymentError($request, $exception = false, $order = null)
    {
        /** @var Medialounge_Checkout_Model_Authorisednet_Data $model */
        $model = Mage::getModel('medialounge_checkout/authorisednet_data');
        // the authorised net is not recording the second line of the address, we'll add it now:
        $shippingDetail = $this->getQuote()->getShippingAddress();
        $request->setXShipToAddress2($shippingDetail->getStreet2());
        $billingDetail = $this->getQuote()->getBillingAddress();
        $request->setXAddress2($billingDetail->getStreet2());
        if($order && $order->getId()){
            $model->addRecord($order->getQuoteId(), $order->getCustomerId(), $request, $exception);
        }else {
            $model->addRecord($this->getQuote()->getId(), $this->getQuote()->getCustomerId(), $request, $exception);
        }
    }

    /**
     * @return Mage_Checkout_Model_Session
     */
    private function getCheckoutSessionModel()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * @return Mage_Sales_Model_Quote
     */
    private function getQuote()
    {
        return $this->getCheckoutSessionModel()->getQuote();
    }

    /**
     * @return mixed
     */
    private function getPaymentResponseStatus()
    {
        return Mage::getStoreConfig('checkout/monitor_payment/test_failed');
    }

    /**
     * @return bool
     */
    private function evaluateTestPaymentStatus()
    {
        $test = false;

        if (($this->getPaymentResponseStatus() == 'timeout')
            or ($this->getPaymentResponseStatus() == 'declined')
        ) {
            $test = true;
        }

        return $test;
    }

    private function getPaymentErrorMessage($paymentStatus)
    {
        if ($paymentStatus == self::$timeoutStatus) {
            $result = 'Authorised.net gateway is down';
        }

        if ($paymentStatus == self::$declinedPaymentStatus) {
            $result = 'Authorised.net has declined the payment';
        }

        if (!isset($result)) {
            $result = 'unknown error';
        }

        return $result;
    }

    protected function _buildRequest(Varien_Object $payment)
    {
        $request = parent::_buildRequest($payment);
        $request->setXCcBankNumber(Mage::registry('cc_bank_number'));

        return $request;
    }

    /**
     * Assign data to info model instance
     *
     * @param   mixed $data
     * @return  Mage_Payment_Model_Info
     */
    public function assignData($data)
    {
        parent::assignData($data);

        $info = $this->getInfoInstance();
        $this->setCcBankNumber($data->getCcBankNumber());

        return $this;
    }

    private function setCcBankNumber($bankNumber)
    {
        if (!Mage::registry('cc_bank_number')) {
            Mage::register('cc_bank_number', $bankNumber);
        }
    }

    private function handleResponseNotApproved($response)
    {
        if ($this->getPaymentResponseStatus() == 'failed') {
            Mage::log(print_r($response, true), null, 'responseauthorisednet.log');
            throw new \Exception('this payment was failed', 0);
        }

        $responseBody = $response->getBody();

        $r = explode(self::RESPONSE_DELIM_CHAR, $responseBody);
        if (isset($r[0]) && isset($r[2])) {
            $responseCode = (int) str_replace('"', '', $r[0]);
            $responseReasonCode = (int) str_replace('"', '', $r[2]);
            switch ($responseCode) {
                case 2:
                    $message = 'the payment was declined';
                    break;
                case 3:
                    $message = 'an error occured';
                    break;
                case 4:
                    $message = 'the transaction is under review';
                    break;
            }

            if ($responseCode != 1) {
                throw new \Exception($message, $responseCode);
            }
            if ($responseReasonCode != 1 && isset($r[3])) {
                throw new \Exception($r[3], $responseReasonCode);
            }
        }

        return false;
    }

    /**
     * @param $paymentStatus
     * @return bool
     */
    protected function isApprovedTransaction($paymentStatus)
    {
        $result = false;

        if ($paymentStatus == 'manual') {
             $result = true;
        }

        if (!$paymentStatus and $this->evaluateTestPaymentStatus()) {
            $result = true;
        }

        return $result;
    }
}