<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 23/05/2017
 * Time: 16:08
 */
class Medialounge_Checkout_Model_Resource_Authorisednet_Data extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('medialounge_checkout/authorisednet_data', 'id');
    }

    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if (!$object->getId()) {
            $object->setData('status', \Medialounge_Checkout_Model_Authorisednet_Data::PAYMENT_RECORD_PENDING);
            $this->setData('created_at', now());
        }
        $this->setData('updated_at', now());
        return parent::_beforeSave($object);
    }

    /**
     * Temporary resolving collection compatibility
     *
     * @return Varien_Db_Adapter_Interface
     */
    public function getReadConnection()
    {
        return $this->getDedicatedReadConnection();
    }

    /**
     * @return Varien_Db_Adapter_Interface
     */
    private function getDedicatedReadConnection()
    {
        $conn = Mage::helper('medialounge_checkout/payment')->getSecondConnection('read');
        return $conn;
    }

    private function getDedicatedWriteConnection()
    {
        $conn = Mage::helper('medialounge_checkout/payment')->getSecondConnection('write');
        return $conn;
    }

    private function getTableName($table)
    {
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getModel('core/resource');
        return  $resource->getTableName($table);
    }

    public function addRecord($quoteId, $customerId, $info, $exception)
    {
        if ($recordId = $this->isQuoteStarted($quoteId)) {
            $connection =  $this->getDedicatedWriteConnection();
            $connection->update($this->getTableName('medialounge_checkout/authorisednet_data'), [
                'quote_id' => $quoteId,
                'customer_id' => $customerId,
                'customer_firstname' => $info->getXFirstName(),
                'customer_lastname' => $info->getXLastName(),
                'info_trail' => $this->getEncodedCardInfo($info, $exception),
                'status' => \Medialounge_Checkout_Model_Authorisednet_Data::PAYMENT_RECORD_PENDING,
                'updated_at' => now()
            ], ['id=?' => $recordId]);
        } else {
            $connection =  $this->getDedicatedWriteConnection();
            $connection->insert($this->getTableName('medialounge_checkout/authorisednet_data'), [
                'quote_id' => $quoteId,
                'customer_id' => $customerId,
                'customer_firstname' => $info->getXFirstName(),
                'customer_lastname' => $info->getXLastName(),
                'info_trail' => $this->getEncodedCardInfo($info, $exception),
                'status' => \Medialounge_Checkout_Model_Authorisednet_Data::PAYMENT_RECORD_PENDING,
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }

    public function deleteRecord($entryId)
    {
        $connection =  $this->getDedicatedWriteConnection();
        $connection->delete($this->getTableName('medialounge_checkout/authorisednet_data'), ['id=?' => $entryId]);
    }

    private function getEncodedCardInfo($request, $exception)
    {
        $requestData = ['customer_ip' =>  $request->getXCustomerIp(),
            'type' => $request->getXType(),
            'amount' => $request->getXAmount(),
            'currency' => $request->getXCurrencyCode(),
            'card_num' => $request->getXCardNum(),
            'card_cvc' => $request->getXCardCode(),
            'card_expdate' => $request->getXExpDate(),
            'transaction_id' => $request->getXTransId(),
            'authorisation_code' => $request->getXAuthCode(),
            'bank_number' => $request->getXCcBankNumber(),
            'billing_firstname' =>  $request->getXFirstName(),
            'billing_lastname' =>  $request->getXLastName(),
            'billing_company' =>  $request->getXCompany(),
            'billing_street' =>  $request->getXAddress(),
            'billing_street2' =>  $request->getXAddress2(),
            'billing_city' =>  $request->getXCity(),
            'billing_postcode' =>  $request->getXZip(),
            'billing_country' =>  $request->getXCountry(),
            'billing_phone' =>  $request->getXPhone(),
            'billing_email' =>  $request->getXEmail(),
            'shipping_firstname' =>  $request->getXShipToFirstName(),
            'shipping_lastname' =>  $request->getXShipToLastName(),
            'shipping_company' =>  $request->getXShipToCompany(),
            'shipping_street' =>  $request->getXShipToAddress(),
            'shipping_street2' =>  $request->getXShipToAddress2(),
            'shipping_city' =>  $request->getXShipToCity(),
            'shipping_postcode' =>  $request->getXShipToZip(),
            'shipping_country' =>  $request->getXShipToCountry(),
            'shipping_phone' =>  $request->getXShipToPhone(),
            'shipping_email' =>  $request->getXEmail(),
            'error_code' => $exception->getCode(),
            'error_message' => $exception->getMessage()
        ];

        return Mage::getModel('core/encryption')->encrypt(serialize($requestData));
    }

    /**
     * @param int $quoteId
     * @return []
     */
    public function getRecordInfoByQuoteId($quoteId)
    {
        $connection = $this->getDedicatedReadConnection();
        $select = $connection->select();
        $select->from($this->getTableName('medialounge_checkout/authorisednet_data'));
        $select->where('quote_id=?', $quoteId);
        $record = $connection->fetchRow($select);

        $loadedInfo = [];

        if (isset($record['id'])) {
            $info = Mage::getModel('core/encryption')->decrypt($record['info_trail']);
            $info = unserialize($info);

            $loadedInfo['id'] = $record['id'];
            $loadedInfo['quote_id'] = $record['quote_id'];
            $loadedInfo['error_code'] = $info['error_code'];
            $loadedInfo['error_message'] = $info['error_message'];
        }

        return $loadedInfo;
    }

    /**
     * @param int $quoteId
     * @return array
     */
    private function isQuoteStarted($quoteId)
    {
        $connection = $this->getDedicatedReadConnection();
        $select = $connection->select();
        $select->from($this->getTableName('medialounge_checkout/authorisednet_data'));
        $select->reset(Varien_Db_Select::COLUMNS)->columns(['id']);
        $select->where('quote_id=?', $quoteId);
        $recordId = $connection->fetchOne($select);

        return $recordId;
    }

    /**
     * @param int $quoteId
     * @return array
     */
    private function isQuotePaymentStarted($quoteId)
    {
        $connection = $this->getDedicatedReadConnection();
        $select = $connection->select();
        $select->from($this->getTableName('medialounge_checkout/quote_payment'));
        $select->reset(Varien_Db_Select::COLUMNS)->columns(['quote_id']);
        $select->where('quote_id=?', $quoteId);
        $recordId = $connection->fetchOne($select);

        return $recordId;
    }

    public function updateOrderId($quoteId, $orderId)
    {
        $connection =  $this->getDedicatedWriteConnection();
        $connection->update($this->getTableName('medialounge_checkout/authorisednet_data'), [
            'order_id' => $orderId,
        ], ['quote_id=?' => $quoteId]);

        $connection = $this->getDedicatedReadConnection();
        $select = $connection->select();
        $select->from($this->getTableName('medialounge_checkout/authorisednet_data'));
        $select->reset(Varien_Db_Select::COLUMNS)->columns(['info_trail']);
        $select->where('quote_id=?', $quoteId);
        $infoTrail = $connection->fetchOne($select);

        $str = Mage::getModel('core/encryption')->decrypt($infoTrail);
        $info = unserialize($str);

        if (is_array($info)) {
            foreach ($info as $key=>$value) {
                $found = false;

                if (strpos($key, 'billing_')!==false) {
                    $found = true;
                }

                if (strpos($key, 'shipping_')!==false) {
                    $found = true;
                }

                if (strpos($key, 'error_')!==false) {
                    $found = true;
                }

                if (!$found) {
                    $transactionInfo[$key] = $value;
                }
            }
        }

        $connection =  $this->_getWriteAdapter();
        $connection->update($this->getTableName('sales/order_payment'), [
            'cc_exp_month' => ''
        ], ['parent_id=?' => $orderId]);
    }

    public function addPaymentQuoteRecord($quoteId, $request, $result)
    {
        /** @var \Medialounge_Checkout_Model_ExpiryDateParser $expiryDateParser */
        $expiryDateParser = Mage::getModel('medialounge_checkout/expiryDateParser', $request->getXExpDate());
        $connection = $this->getDedicatedWriteConnection();

        if ($recordId = $this->isQuotePaymentStarted($quoteId)) {
            $connection->update($this->getTableName('medialounge_checkout/quote_payment'), [
                'anet_trans_method' => $result->getMethod(),
                'cc_trans_id' => $result->getTransactionId(),
                'cc_status' => $result->getResponseCode(),
                'cc_type' => ($result->getCardType()=='MasterCard')?'MC':'VI',
                'cc_status_description' => $result->getResponseReasonText(),
                'cc_exp_month' => $expiryDateParser->getExpMonth(),
                'cc_exp_year' => $expiryDateParser->getExpYear(),
                'cc_avs_status' => $result->getAvsResultCode(),
                'cc_cid_status' => $result->getCardCodeResponseCode(),
                'cc_phnumber' => $request->getXCcBankNumber(),
                'cc_number_enc' => Mage::helper('core')->encrypt($request->getXCardNum()),
            ], ['quote_id=?' => $quoteId]);
        } else {
            $connection->insert($this->getTableName('medialounge_checkout/quote_payment'), [
                'quote_id' => $quoteId,
                'anet_trans_method' => $result->getMethod(),
                'cc_trans_id' => $result->getTransactionId(),
                'cc_status' => $result->getResponseCode(),
                'cc_type' => ($result->getCardType()=='MasterCard')?'MC':'VI',
                'cc_status_description' => $result->getResponseReasonText(),
                'cc_exp_month' => $expiryDateParser->getExpMonth(),
                'cc_exp_year' => $expiryDateParser->getExpYear(),
                'cc_avs_status' => $result->getAvsResultCode(),
                'cc_cid_status' => $result->getCardCodeResponseCode(),
                'cc_phnumber' => $request->getXCcBankNumber(),
                'cc_number_enc' => Mage::helper('core')->encrypt($request->getXCardNum())
            ]);
        }
    }

    public function transferPaymentQuoteRecordToOrder($quoteId, $orderId)
    {
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getModel('core/resource');

        $connection = $this->getReadConnection();
        $select = $connection->select()->from($resource->getTableName('medialounge_checkout/quote_payment'))
            ->where('quote_id=?', $quoteId);
        $row = $connection->fetchRow($select);
        $connection = $resource->getConnection('default_write');
        $connection->update($this->getTableName('sales/order_payment'), [
            'anet_trans_method' => $row['anet_trans_method'],
            'cc_trans_id' => $row['cc_trans_id'],
            'cc_status' => $row['cc_status'],
            'cc_type' => $row['cc_type'],
            'cc_status_description' => $row['cc_status_description'],
            'cc_exp_month' => $row['cc_exp_month'],
            'cc_exp_year' => $row['cc_exp_year'],
            'cc_avs_status' => $row['cc_avs_status'],
            'cc_cid_status' => $row['cc_cid_status'],
            'cc_phnumber' => $row['cc_phnumber'],
            'cc_number_enc' => $row['cc_number_enc']
        ], ['parent_id=?' => $orderId]);
    }

}