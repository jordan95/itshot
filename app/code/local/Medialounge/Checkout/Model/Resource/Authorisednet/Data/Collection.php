<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 23/05/2017
 * Time: 16:09
 */

class Medialounge_Checkout_Model_Resource_Authorisednet_Data_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    /**
     * Define resource model
     *
     */
    protected function _construct()
    {
        $this->_init('medialounge_checkout/authorisednet_data', 'medialounge_checkout/authorisednet_data');
    }
}