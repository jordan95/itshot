<?php

/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 14/07/2017
 * Time: 09:51
 */
class Medialounge_Checkout_Model_Observer
{

    const OPTION_ID_ENGRAVING = '38790';
    const OPTION_ID_ENGRAVING_TEXT = '38791';

    public function placeOrderAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();
        if (empty($order)) {
            throw new Exception('Invalid order submitted');
        }

        $authorisedNetInfo = $this->getAuthorisednetDataModel()->getRecordInfoByQuoteId($order->getQuoteId());

        if (Mage::registry('cc_bank_number')) {
            $order->setData('payment_bank_number', Mage::registry('cc_bank_number'));
        }

        if (count($authorisedNetInfo)) {
            $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, \Medialounge_Checkout_Helper_Data::MANUAL_PROCESSING_STATUS, $authorisedNetInfo['error_message'], false)
                    ->save();
            $this->getAuthorisednetDataModel()->reconcileOrder($order->getQuoteId(), $order->getId());
            $this->getAuthorisednetDataModel()->removeInvalidTransaction($order);

            $this->emailFailedOrderNotification($order);
        }
    }

    public function updatePaymentDetails(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $event = $observer->getEvent();
        $orderIds = $event->getOrderIds();

        $orderId = $orderIds[0];
        if (!$orderId) {
            throw new Exception('Invalid order submitted');
        }
        $order = Mage::getModel('sales/order')->load($orderId);

        $this->getAuthorisednetDataModel()->transferPaymentQuoteRecordToOrder($order->getQuoteId(), $order->getId());
    }

    /**
     * @return \Medialounge_Checkout_Model_Authorisednet_Data
     */
    private function getAuthorisednetDataModel()
    {
        return Mage::getModel('medialounge_checkout/authorisednet_data');
    }

    private function emailFailedOrderNotification(Mage_Sales_Model_Order $order)
    {
        if (!$this->saleHelper()->canSendNewOrderEmail($order->getStore()->getId())) {
            return $this;
        }

        $sendTo = $this->_getEmails('checkout/monitor_payment/notification_recipient', $order->getStoreId());
        if (!$sendTo) {
            return $this;
        }

        $translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);

        $mailTemplate = Mage::getModel('core/email_template');
        $template = Mage::getStoreConfig('checkout/monitor_payment/template', $order->getStoreId());

        $to = [];
        foreach ($sendTo as $email) {
            $to[] = array(
                'email' => $email,
                'name' => null
            );
        }

        foreach ($to as $recipient) {
            $mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $order->getStoreId()))
                    ->sendTransactional(
                            $template, Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_IDENTITY, $order->getStoreId()), $recipient['email'], $recipient['name'], array(
                        'order' => $order
                            )
            );
        }
        $order->setEmailSent(true);
        //Mage::getModel('sales/order')->_getResource()->saveAttribute($order, 'email_sent');
        $translate->setTranslateInline(true);

        // Mage::log('Email Failure Notification done for order: '.$order->getIncrementId());

        return true;
    }

    protected function _getEmails($configPath, $storeId)
    {
        $data = Mage::getStoreConfig($configPath, $storeId);
        if (!empty($data)) {
            return explode(',', $data);
        }
        return false;
    }

    /**
     * @return Mage_Sales_Helper_Data
     */
    private function saleHelper()
    {
        return Mage::helper('sales');
    }

    public function addBankNumberColumn(Varien_Event_Observer $observer)
    {
        try {
            $event = $observer->getEvent();
            if ($event->getBlock() instanceof
                    Mage_Adminhtml_Block_Sales_Order_Grid
            ) {
                $grid = $event->getBlock();

                $columnData = array(
                    'header' => 'Order Bank Number',
                    'index' => 'payment_bank_number',
                    'type' => 'text',
                );

                $grid->addColumnAfter('payment_bank_number', $columnData, 'grand_total');
            }
        } catch (Exception $e) {
            mage::logException($e);
        }

        return $this;
    }

    public function updateItemOptions($observer)
    {
        $params = Mage::app()->getRequest()->getParams();
        if (!isset($params['update_item']) || !$params['update_item']) {
            return;
        }
        $cart = $observer->getCart();
        Mage::app()->getRequest()->setParam('awacp', false);

        $item = $cart->getQuote()->getItemById($params['update_item']);
        if($item){
            $options = $item->getOptions();
            $noText = false;
            $product = $item->getProduct();
            $engravingOptionId = 0;
            $engravingTextOptionId = 0;
            $checkEngraving = false;
            $checkengravingText ="not empty";

            foreach ($options as $option) {
                $optionCode = str_replace('option_', '', $option->getCode());
                if (isset($params['options'][$item->getId()][$optionCode])) {
                    $value = (array) $params['options'][$item->getId()][$optionCode];
                    $optionValue = reset($value);
                    $opt = $product->getOptionById($optionCode);
                    if($opt->getTitle() =='Engraving'){
                         $engravingOptionId = $optionValue;
                         foreach($opt->getValues() as $value) {
                             if($value->getTitle() =="Don't Engrave" && $engravingOptionId == $value->getId()){
                                 $noText = true;
                             }
                             if($value->getTitle() !="Don't Engrave" && $engravingOptionId == $value->getId()){
                                 $checkEngraving = true;
                             }
                         }
                    }
                    if($opt->getTitle() =='Engraving Text'){
                        $engravingTextOptionId = $opt->getId();
                        $checkengravingText = $params['options'][$item->getId()][$engravingTextOptionId];
                    }
                }
            }
            if(!$engravingTextOptionId){
                foreach ($product->getOptions() as $o) {
                    if($o->getTitle()=='Engraving Text'){
                        $engravingTextOptionId = $o->getId();
                        $checkengravingText = $params['options'][$item->getId()][$engravingTextOptionId];
                        break;
                    }
                }
            }
            if(trim($checkengravingText) == '' && $checkEngraving===true ){
                Mage::app()->getResponse()->setRedirect(Mage::getUrl("checkout/cart"));
                Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml('Please enter engraving text.'));
                return;
            }
            foreach ($options as $option) {
                $optionCode = str_replace('option_', '', $option->getCode());
                if (isset($params['options'][$item->getId()][$optionCode])) {
                    $value = (array) $params['options'][$item->getId()][$optionCode];
                    $optionValue = reset($value);
                    $option->setValue($optionValue);
                }
            }
            
            if ($engravingOptionId && $engravingTextOptionId) {
                $buyRequest = $item->getBuyRequest();
                $optionIds = $item->getOptionByCode('option_ids');
                if (!$noText) {
                    $item->addOption(array(
                        'item' => $item,
                        'product' => $product,
                        'code' => 'option_' . $engravingTextOptionId,
                        'value' => $params['options'][$item->getId()][$engravingTextOptionId]
                    ));
                    $optionIds->setValue(join(',', array_unique(array_merge(explode(',', $optionIds->getValue()), array($engravingTextOptionId)))));
                } else {
                    $item->removeOption('option_' . $engravingTextOptionId);
                    $optionIds->setValue(join(',', array_diff(explode(',', $optionIds->getValue()), array($engravingTextOptionId))));
                    unset($params['options'][$item->getId()][$engravingTextOptionId]);
                }
                $buyRequest->setOptions($params['options'][$item->getId()]);
                $item->setDataChanges(true);
            }
            $cart->save();
            Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
            $helper = Mage::helper('core');
            $message = $helper->__('Your chosen options have been updated successfully.');

            Mage::getSingleton('checkout/session')->addSuccess($message);
        }else{
            $helper = Mage::helper('core');
            $message = $helper->__('The product that you updated is no longer in your cart.');
            Mage::getSingleton('checkout/session')->addError($message);
        }
        
    }

}
