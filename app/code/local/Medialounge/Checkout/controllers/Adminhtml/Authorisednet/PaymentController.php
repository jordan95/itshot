<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 23/05/2017
 * Time: 17:26
 */

class Medialounge_Checkout_Adminhtml_Authorisednet_PaymentController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/payment_trail')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Payment Entries'), Mage::helper('adminhtml')->__('Payment Entries'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('medialounge_checkout/adminhtml_entry'));
        $this->renderLayout();
    }

    public function deleteAction()
    {
        if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $entryModel = Mage::getModel('medialounge_checkout/authorisednet_data');

                $entryModel->setId($this->getRequest()->getParam('id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Entry was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/index', array('entry_id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/sales/payment_trail');
    }
}