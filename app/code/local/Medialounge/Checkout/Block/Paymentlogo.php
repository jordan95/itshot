<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Checkout_Block_Paymentlogo extends Mage_Core_Block_Template
{
    const CMS_BLOCK_ID = 'payment_logo';

    protected function _toHtml()
    {
        return $this->getLayout()->createBlock('cms/block')->setBlockId(self::CMS_BLOCK_ID)->toHtml();
    }
}