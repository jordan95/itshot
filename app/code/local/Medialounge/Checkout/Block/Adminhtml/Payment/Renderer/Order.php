<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 21/07/2017
 * Time: 11:03
 */

class Medialounge_Checkout_Block_Adminhtml_Payment_Renderer_Order extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $orderid = $row->getData('order_id');

        return sprintf('<a href="%s" target="_blank">%s</a>', Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view', array('order_id' => $orderid)), $this->getOrderIncrementId($orderid));
    }

    private function getOrderIncrementId($orderId)
    {
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getModel('core/resource');

        $connection = $resource->getConnection('core_read');
        $select = $connection->select()->from($resource->getTableName('sales/order'), ['increment_id']);
        $select->where('entity_id=?', $orderId);

        return $connection->fetchOne($select);
    }

}