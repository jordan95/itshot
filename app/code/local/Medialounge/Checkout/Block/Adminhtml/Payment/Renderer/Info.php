<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 30/05/2017
 * Time: 12:29
 */

class Medialounge_Checkout_Block_Adminhtml_Payment_Renderer_Info extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $result = '';
        $str = $row->getData('info_trail');
        if ($str) {
            try {
                $info = $this->extractInfo($str);
                if (count($info)) {
                    $result .= '<div class="transaction_info">';
                    foreach ($info as $group=>$data) {
                        $result .= sprintf('<div class="transaction_detail %s"><h3>%s</h3>', $group, $this->humanize($group));
                        foreach ($data as $key=>$value) {
                            $result .= sprintf('<b>%s</b>: %s<br>', $key, $value);
                        }
                        $result .= '</div>';
                    }
                    $result .= $this->addQuoteDetail($row->getData('quote_id'));
                    $result .= $this->addQuoteShipmentMethodDetail($row->getData('quote_id'));

                    $result .= '</div>';
                }
            } catch (\Exception $e) {
                $result = 'error with row '.$row->getId();
            }
        }

        return $result;
    }

    /**
     * @param $str
     * @return mixed
     */
    private function extractInfo($str)
    {
        $str = Mage::getModel('core/encryption')->decrypt($str);
        $info = unserialize($str);

        if (is_array($info)) {
            $billingInfo = $shippingInfo = $errorInfo = $transactionInfo = [];
            foreach ($info as $key=>$value) {
                $found = false;

                if (strpos($key, 'billing_')!==false) {
                    $displayKey = substr($key, strlen('billing_'));
                    $billingInfo[$displayKey] = $value;
                    $found = true;
                }

                if (strpos($key, 'shipping_')!==false) {
                    $displayKey = substr($key, strlen('shipping_'));
                    $shippingInfo[$displayKey] = $value;
                    $found = true;
                }

                if (strpos($key, 'error_')!==false) {
                    $displayKey = substr($key, strlen('error_'));
                    $errorInfo[$displayKey] = $value;
                    $found = true;
                }

                if (!$found) {
                    $transactionInfo[$key] = $value;
                }
            }

            $parsedInfo['transaction_info'] = $transactionInfo;
            $parsedInfo['error_info'] = $errorInfo;
            $parsedInfo['billing_info'] = $billingInfo;
            $parsedInfo['shipping_info'] = $shippingInfo;
        } else {
            $parsedInfo = [];
        }

        return $parsedInfo;
    }

    private function humanize($group)
    {
        $str = str_replace('_', ' ', $group);
        return ucwords($str);
    }

    private function addQuoteDetail($quoteId)
    {
        $quote = Mage::getModel('sales/quote')->setStoreId(Mage::app()->getDefaultStoreView()->getId())->load($quoteId);

        $items = $quote->getAllItems();

        $cartItems = [];
        foreach ($items as $item) {
            $itemData = [];
            $itemData['sku'] = $item->getSku();
            $itemData['price'] = $item->getPrice();
            $itemData['qty'] = $item->getQty();
            $itemData['options'] = $this->getItemOptions($item);

            $cartItems[] = $itemData;
        }

        $result = sprintf('<div class="transaction_detail %s"><h3>%s</h3>', 'quote_detail', 'Quote Detail');
        $result .= '<div class="quote-header-detail">';
        $result .= sprintf('<b>created at</b>: %s<br>', $quote->getData('created_at'));
        $result .= sprintf('<b>total</b>: %s<br>', $this->renderPrice($quote->getData('grand_total'), $quote));
        $result .= '</div>';

        $i = 1;
        if ($quote->getData('coupon_code')) {
            $result .= sprintf('<div class="quote-detail-coupon"><b>Coupon Applied: </b>%s</div>', $quote->getData('coupon_code'));
        }

        if ($quote->getData('onestepcheckout_customercomment')) {
            $result .= sprintf('<div class="quote-detail-comment"><b>Comment: </b>%s</div>', $quote->getData('onestepcheckout_customercomment'));
        }

        foreach ($cartItems as $cartItem) {
            $result .= sprintf('item %s: %s x %s (%s)<br>', $i, $cartItem['qty'], $cartItem['sku'], $this->renderPrice($cartItem['price'], $quote));
            if (isset($cartItem['options']) and count($cartItem['options'])) {
                $result .= '<div class="quote-detail-cart"><b>Custom Options</b>:<br>';
                foreach ($cartItem['options'] as $key=>$value) {
                    $result .= sprintf('%s: %s<br>', $key, $value);
                }
                $result .= '</div>';
            }
        }
        $result .= '</div>';

        return $result;
    }

    private function addQuoteShipmentMethodDetail($quoteId)
    {
        $collection = Mage::getModel('sales/quote_address')->getCollection();
        $collection->addFieldToFilter('quote_id', $quoteId);
        $collection->addFieldToFilter('address_type', 'shipping');

        $shippingMethodTitle = '';
        if ($collection->count()) {
            $shippingAddress = $collection->getFirstItem();
            if ($rate = $this->getRateFromShippingMethod($shippingAddress)) {
                $shippingMethodTitle = $rate->getMethodTitle();
            }
        }
        if (!$shippingMethodTitle) {
            $shippingMethodTitle = 'unknown';
        }

        $result = sprintf('<div class="transaction_detail %s"><h3>%s</h3>', 'quote_detail', 'Shipping Method Detail');
        $result .= sprintf('%s<br>', $shippingMethodTitle);
        $result .= '</div>';

        return $result;
    }

    private function renderPrice($price, Mage_Sales_Model_Quote $quote)
    {
        $_currency = Mage::getModel('directory/currency')->load($quote->getQuoteCurrencyCode());
        return $_currency->formatPrecision($price, 2);
    }

    /**
     * @param $result
     * @param $item
     * @return array
     */
    private function getItemOptions(Mage_Sales_Model_Quote_Item $item)
    {
        $result = $parsedResult = [];

        $product = $item->getProduct();
        $option = $item->getOptionByCode('info_buyRequest');
        $buyRequest = new Varien_Object($option ? unserialize($option->getValue()) : null);

        $productOptions = [];
        if (count($product->getOptions())) {
            foreach($buyRequest['options'] as $key => $value) {
                foreach ($product->getOptions() as $option) {
                    $optionKey = $option->getId();
                    $optionLabel = $option->getTitle();

                    if ($key == $optionKey) {
                        $valueLabel = $this->findOptionValueLabel($option->getValues(), $value);
                        $productOptions[$optionLabel] = $valueLabel;
                    }
                }
            }
        }

        return $productOptions;
    }

    private function findOptionValueLabel($values, $value)
    {
        if (isset($values[$value])) {
            return $values[$value]->getDefaultTitle();
        }
    }

    /**
     * @param $shippingAddress
     * @return mixed
     */
    private function getRateFromShippingMethod($shippingAddress)
    {
        $shippingMethod = $shippingAddress->getData('shipping_method');
        $rateCollection = Mage::getModel('sales/quote_address_rate')->getCollection();
        $rateCollection->addFieldToFilter('code', $shippingMethod);

        if ($rateCollection->count()) {
            return $rateCollection->getFirstItem();
        }
    }
}