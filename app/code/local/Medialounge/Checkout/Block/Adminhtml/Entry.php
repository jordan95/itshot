<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 23/05/2017
 * Time: 17:29
 */

class Medialounge_Checkout_Block_Adminhtml_Entry extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_entry';
        $this->_blockGroup = 'medialounge_checkout';
        $this->_headerText = Mage::helper('medialounge_checkout')->__('Payment Entry Manager');

        $this->_removeButton('add');

        parent::__construct();
    }
}