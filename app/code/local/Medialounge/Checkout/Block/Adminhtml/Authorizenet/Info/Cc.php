<?php
/**
 * Created by PhpStorm.
 * User: herve
 * Date: 12/02/2018
 * Time: 14:40
 */

class Medialounge_Checkout_Block_Adminhtml_Authorizenet_Info_Cc extends Mage_Paygate_Block_Authorizenet_Info_Cc
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('medialounge/paygate/cc.phtml');
    }

    public function getPaymentBankNumber()
    {
        return $this->getOrder()->getData('payment_bank_number');
    }

    private function getOrder()
    {
        $order = $this->getInfo()->getOrder();
        if(!$order || !$order->getId()){
            $order = Mage::getModel('sales/order')->load($this->getInfo()->getParentId());
        }
        return $order;
    }
}