<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 23/05/2017
 * Time: 17:31
 */

class Medialounge_Checkout_Block_Adminhtml_Entry_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('medialounge_checkoutGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('medialounge_checkout/authorisednet_data')->getCollection();
        $collection->setOrder('created_at', 'desc');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', [
            'header' => Mage::helper('medialounge_checkout')->__('Identifier'),
            'align' => 'left',
            'index' => 'id',
            'width' => '80px',
        ]);

        $this->addColumn('quote_id', [
            'header' => Mage::helper('medialounge_checkout')->__('Quote Identifier'),
            'align' => 'left',
            'index' => 'quote_id',
            'width' => '80px',
        ]);

        $this->addColumn('order_id', [
            'header' => Mage::helper('medialounge_checkout')->__('Order Identifier'),
            'align' => 'left',
            'index' => 'order_id',
            'width' => '80px',
            'renderer'  => 'medialounge_checkout/adminhtml_payment_renderer_order',
            'filter_condition_callback' => array($this, '_filterOrder'),
        ]);

        $this->addColumn('customer_firstname', [
            'header' => Mage::helper('medialounge_checkout')->__('Customer Firstname'),
            'align' => 'left',
            'index' => 'customer_firstname',
            'width' => '80px',
        ]);

        $this->addColumn('customer_lastname', [
            'header' => Mage::helper('medialounge_checkout')->__('Customer Lastname'),
            'align' => 'left',
            'index' => 'customer_lastname',
            'width' => '80px',
        ]);

        $this->addColumn('info_trail', [
            'header' => Mage::helper('medialounge_checkout')->__('Payment Info'),
            'align' => 'left',
            'index' => 'info_trail',
            'renderer'  => 'medialounge_checkout/adminhtml_payment_renderer_info'
        ]);

        $this->addColumn('created_at', [
            'header' => Mage::helper('medialounge_checkout')->__('Created At'),
            'align' => 'left',
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ]);

        $this->addColumn('status', [
            'header' => Mage::helper('medialounge_checkout')->__('Status'),
            'align' => 'left',
            'index' => 'status',
            'width' => '80px',
        ]);

        $this->addColumn('action',
                array(
                    'header'    => Mage::helper('medialounge_checkout')->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'     => 'getId',
                    'actions'   => array(
                        array(
                            'caption' => Mage::helper('sales')->__('Delete'),
                            'url'     => array('base'=>'*/*/delete'),
                            'field'   => 'id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => true,
                ));
    }

    protected function _filterOrder($collection, $column)
    {
        if (!$incrementIdFilter = $column->getFilter()->getValue()) {
            return $this;
        }

        $this->getCollection()->getSelect()->where("order_id in (?)", $this->getOrderIdsFromIncrementFilter($incrementIdFilter));

        return $this;
    }

    private function getOrderIdsFromIncrementFilter($incrementIdFilter)
    {
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getModel('core/resource');

        $connection = $resource->getConnection('core_read');
        $select = $connection->select()->from($resource->getTableName('sales/order'), ['entity_id']);
        $select->where('increment_id like ?', '%'.$incrementIdFilter.'%');

        return $connection->fetchCol($select);
    }
}