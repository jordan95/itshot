<?php
/**
 *
 * @category   Medialounge
 * @package    Medialounge_Catalog
 * @author     Herve Tribouilloy <herve@medialounge.co.uk>
 */
class Medialounge_Checkout_Block_Success extends Mage_Checkout_Block_Onepage_Success
{
    protected function _toHtml()
    {
        if (!$this->getOrder()) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * Retrieve current order model instance
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if (!$this->hasData('order')) {
            $this->setData('order', Mage::helper('medialounge_catalog')->getCurrentOrder());
        }

        return $this->getData('order');
    }

    public function getBilling()
    {
        return $this->getOrder()->getBillingAddress();
    }

    public function getShipping()
    {
        return $this->getOrder()->getShippingAddress();
    }

    public function getPaymentHtml()
    {
        /** @var Mage_Payment_Helper_Data $helper */
        $helper = $this->helper('payment');
        if ($block = $helper->getInfoBlock($this->getOrder()->getPayment())) {
            return $block->toHtml();
        }
    }

    public function getShippingMethod()
    {
        return $this->getOrder()->getShippingDescription();
    }

    public function getProductIdsInCart()
    {
        $products = $this->getOrder()->getAllVisibleItems();
        $ids = [];
        foreach ($products as $product) {
            $ids[] = $product->getSku();
        }
        return sprintf('["%s"]', implode('","', $ids));
    }

    public function getGrandTotal()
    {
        return number_format($this->getOrder()->getGrandTotal(), 2, '.', '');
    }
}