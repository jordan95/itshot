<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('medialounge_checkout/authorisednet_data');

$connection = Mage::helper('medialounge_checkout/payment')->getSecondConnection('write');

$connection->addColumn($tableName, 'customer_firstname', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 50,
    'nullable' => false,
    'comment' => 'Customer Firstname'
]);

$connection->addColumn($tableName, 'customer_lastname', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 50,
    'nullable' => false,
    'comment' => 'Customer Lastname'
]);

$connection->addColumn($tableName, 'created_at', [
    'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
    'comment' => 'Payment Failure Date'
]);

$installer->endSetup();
