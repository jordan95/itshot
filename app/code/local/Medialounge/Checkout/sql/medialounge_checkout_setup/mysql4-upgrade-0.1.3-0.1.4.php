<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('medialounge_checkout/authorisednet_data');

$connection = Mage::helper('medialounge_checkout/payment')->getSecondConnection('write');

$connection->addColumn($tableName, 'order_id', [
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'unsigned' => true,
    'nullable' => true,
    'comment' => 'Order Id'
]);

$installer->endSetup();
