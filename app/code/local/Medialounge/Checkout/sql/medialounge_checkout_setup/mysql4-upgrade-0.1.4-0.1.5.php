<?php

$installer = $this;
$installer->startSetup();

/* @var $installer Mage_Sales_Model_Entity_Setup */
$setup = new Mage_Eav_Model_Entity_Setup('sales_setup');

$setup->addAttribute('order', 'payment_bank_number', ['type'=> Varien_Db_Ddl_Table::TYPE_VARCHAR]);

$installer->startSetup();

$salesSetup = new Mage_Eav_Model_Entity_Setup('sales_setup');

$this->run("
	ALTER TABLE {$this->getTable('sales/order')} ADD COLUMN  payment_bank_number varchar(255);
");

$this->run("
	ALTER TABLE {$this->getTable('sales/order_grid')} ADD COLUMN  payment_bank_number varchar(255);
");

$salesSetup->addAttribute('order', 'payment_bank_number', array('grid'=>true));
$installer->run('UPDATE ' . $this->getTable('sales/order') . ' AS o, ' . $this->getTable('sales/order_grid') . ' AS g
                 SET g.payment_bank_number=o.payment_bank_number
                 WHERE o.entity_id = g.entity_id');

$installer->endSetup();
