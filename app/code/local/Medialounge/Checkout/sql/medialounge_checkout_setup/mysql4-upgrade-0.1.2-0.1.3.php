<?php

$installer = $this;
$installer->startSetup();

// Required tables
$statusTable = $installer->getTable('sales/order_status');
$statusStateTable = $installer->getTable('sales/order_status_state');

// Insert statuses
$installer->getConnection()->insertArray(
    $statusTable,
    array(
        'status',
        'label'
    ),
    array(
        array('status' => \Medialounge_Checkout_Helper_Data::MANUAL_PROCESSING_STATUS, 'label' => 'Procesing (Manual)')
    )
);

// Insert states and mapping of statuses to states
$installer->getConnection()->insertArray(
    $statusStateTable,
    array(
        'status',
        'state',
        'is_default'
    ),
    array(
        array(
            'status' => \Medialounge_Checkout_Helper_Data::MANUAL_PROCESSING_STATUS,
            'state' => 'processing',
            'is_default' => 0
        )
    )
);

$installer->endSetup();
