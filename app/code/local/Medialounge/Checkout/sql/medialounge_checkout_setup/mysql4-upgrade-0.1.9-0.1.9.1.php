<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('medialounge_checkout/quote_payment');

$connection = $installer->getConnection();

// Make re-installs of this module possible, even if the db wasn't cleaned up completely
if ($connection->isTableExists($tableName)) {
    $connection->dropTable($tableName);
}

$table = $connection->newTable($tableName)
    ->addColumn('payment_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'identity' => true,
        'primary'  => true,
        'unsigned' => true,
        'nullable' => false,
    ], 'Authorised.net Payment Id')
    ->addColumn('quote_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'nullable' => true,
    ], 'Quote Id')
    ->addColumn('method', Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'nullable' => true,
    ], 'Method')
    ->addColumn('cc_type', Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
    'nullable' => true,
    ], 'cc_type')
    ->addColumn('cc_number_enc', Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'nullable' => true,
    ], 'cc_number_enc')
    ->addColumn('cc_last4', Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'nullable' => true,
    ], 'cc_last4')
    ->addColumn('cc_cid_enc', Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'nullable' => true,
    ], 'cc_cid_enc')
    ->addColumn('cc_owner', Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'nullable' => true,
    ], 'cc_owner')
    ->addColumn('cc_exp_month', Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'nullable' => true,
    ], 'cc_exp_month')
    ->addColumn('cc_exp_year', Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'nullable' => true,
    ], 'cc_exp_year')
    ->addColumn('anet_trans_method', Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'nullable' => true,
    ], 'anet_trans_method')
    ->setComment('Authorised.net Data Table');

$connection->createTable($table);

$connection->query("
	ALTER TABLE $tableName ADD COLUMN  cc_trans_id varchar(255);
	ALTER TABLE $tableName ADD COLUMN  cc_status varchar(255);
	ALTER TABLE $tableName ADD COLUMN  cc_status_description varchar(255);
	ALTER TABLE $tableName ADD COLUMN  cc_avs_status varchar(255);
	ALTER TABLE $tableName ADD COLUMN  cc_cid_status varchar(255);
	ALTER TABLE $tableName ADD COLUMN  cc_phnumber varchar(255);
	ALTER TABLE $tableName ADD COLUMN  cc_phnumber varchar(255);
");

$installer->endSetup();
