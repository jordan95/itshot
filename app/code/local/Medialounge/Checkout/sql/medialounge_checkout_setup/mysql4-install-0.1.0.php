<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('medialounge_checkout/authorisednet_data');

// Make re-installs of this module possible, even if the db wasn't cleaned up completely
if ($installer->getConnection()->isTableExists($tableName)) {
    $installer->getConnection()->dropTable($tableName);
}

$table = $installer->getConnection()->newTable($tableName)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'identity' => true,
        'primary'  => true,
        'unsigned' => true,
        'nullable' => false,
    ], 'Authorised.net Payment Id')
    ->addColumn('quote_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'nullable' => true,
    ], 'Quote Id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'nullable' => true,
    ], 'Customer Id')
    ->addColumn('info_trail', Varien_Db_Ddl_Table::TYPE_TEXT, '1K', [
        'nullable' => false,
    ], 'Payment Data')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TEXT, 30, [
        'nullable' => false,
    ], 'Authorised.net Data Status')
    ->setComment('Authorised.net Data Table');

$installer->getConnection()->createTable($table);
$installer->endSetup();
