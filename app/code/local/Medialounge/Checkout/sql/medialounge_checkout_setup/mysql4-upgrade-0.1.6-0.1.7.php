<?php

$installer = $this;
$installer->startSetup();

$this->run("
	ALTER TABLE {$this->getTable('sales/quote_payment')} ADD COLUMN  anet_trans_method varchar(255);
");

$installer->endSetup();
