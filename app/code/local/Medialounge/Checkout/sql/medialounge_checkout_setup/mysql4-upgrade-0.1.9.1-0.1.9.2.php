<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('sales/order_payment');

$connection = $installer->getConnection();

$connection->query("
	ALTER TABLE $tableName ADD COLUMN  cc_phnumber varchar(255) default '';
");

$installer->endSetup();
