<?php


$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (! file_exists($path . $shell) && ! file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Medialounge_Checkout_Test extends Mage_Shell_Abstract
{
    public function run()
    {
        $model = Mage::getModel('medialounge_checkout/authorisednet_data');
        echo get_class($model);

        $model->setData('quote_id', 1);
        $model->setData('customer_id', 1);
        $model->setData('info_trail', 'this is a test');
        $model->save();

        /*$model->setData('quote_id', 3);
        $model->setData('customer_id', 3);
        $model->setData('info_trail', 'this is a test 3');
        $model->save();*/

        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        $connection =  $resource->getConnection('core_write');
        $connection->insert($resource->getTableName('medialounge_checkout/authorisednet_data'), ['quote_id' => 7,
            'customer_id' => 8,
            'info_trail' => 'this is a test again',
            'status' => \Medialounge_Checkout_Model_Authorisednet_Data::PAYMENT_RECORD_PENDING
        ]);

        $collection = $model->getCollection();

        foreach ($collection as $item) {
            print_r($item->getData());
        }


        echo "done\n";
    }

}

$shell = new Medialounge_Checkout_Test();
$shell->run();
