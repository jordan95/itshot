<?php


$path = __DIR__ . '/../';
$shell = 'shell/abstract.php';
$i = 0;
while (! file_exists($path . $shell) && ! file_exists($path . 'htdocs/' . $shell) && $i++ < 15) {
    $path .= '../';
}
chdir(file_exists($path . $shell) ? $path : $path . 'htdocs');

require_once $shell;

class Medialounge_Checkout_Cron extends Mage_Shell_Abstract
{
    public function run()
    {
        $model = Mage::getModel('medialounge_checkout/authorisednet_cron');
        $model->clearOldInfo();

        echo "done\n";
    }

}

$shell = new Medialounge_Checkout_Cron();
$shell->run();