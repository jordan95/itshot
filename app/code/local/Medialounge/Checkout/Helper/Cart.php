<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 07/07/2017
 * Time: 16:34
 */
class Medialounge_Checkout_Helper_Cart extends Mage_Core_Helper_Abstract
{
    public function getAddToCartUrl($_product, $partialOption = false)
    {
        $additional['options[partialpayment]'] = $partialOption;
        return Mage::helper('checkout/cart')->getAddUrl($_product, $additional);
    }
}