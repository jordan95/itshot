<?php
/**
 * Created by PhpStorm.
 * User: medialounge_dev1
 * Date: 23/05/2017
 * Time: 12:11
 */

class Medialounge_Checkout_Helper_Payment extends Mage_Core_Helper_Abstract
{
    private static $paymentFailureDataId = 'checkout_payment_failure';

    private $paymentFailureData = null;

    private $paymentTimeDeactivationDelay = null;

    /**
     * we record a new payment transaction failure
     */
    public function setPaymentMethodInDisabledStatus()
    {
        $date = new Zend_date();
        $this->commitPaymentFailureData(['method'=> $this->getPaymentMethodDisableCode(),
                                        'time'=> $date->toString(Varien_Date::DATETIME_INTERNAL_FORMAT),
                                        'occurences' => $this->readNumberPaymentFailures() + 1
        ]);
        $this->loadPaymentFailureData();
    }

    public function isFailedPaymentMethod($paymentMethodName)
    {
        $paymentFailureData = $this->loadPaymentFailureData();
        if (isset($paymentFailureData['method'])
            and ($paymentFailureData['method'] == $paymentMethodName)
            and $this->validateFailureDate($paymentFailureData)
        ) {
            return true;
        }

        return false;
    }

    public function getPaymentMethodFailureMessage($paymentMethodName)
    {
        if (!$this->isFailedPaymentMethod($paymentMethodName)) {
            return false;
        }

        $paymentFailureData = $this->loadPaymentFailureData();

        if (count($paymentFailureData)) {
            $timeFailure = Mage::getModel('core/date')->timestamp($paymentFailureData['time']);
            $timestamp = Mage::getModel('core/date')->timestamp();
            $this->paymentTimeDeactivationDelay = $this->getPaymentMethodDisableDuration($this->getNumberOfMinuteToRemainDeactivated($paymentFailureData['occurences'])) - ($timestamp - $timeFailure);

            return $this->__('Due to a payment error, this payment option will be enabled automatically in <span id="payment-flag-activation">%s</span>', $this->paymentTimeDeactivationDelay);
        }
    }

    /**
     * @return Mage_Checkout_Model_Session
     */
    private function getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    private function loadPaymentFailureData()
    {
        if (is_null($this->paymentFailureData)) {
            $this->paymentFailureData = $this->getSession()->getData(self::$paymentFailureDataId);
            if ($this->paymentFailureData) {
                $this->paymentFailureData = unserialize($this->paymentFailureData);
            }

            if (!is_array($this->paymentFailureData) or !$this->validateFailureDate($this->paymentFailureData)) {
                $this->paymentFailureData = [];
            }
        }

        return $this->paymentFailureData;
    }

    /**
     * @param $paymentFailureData
     */
    private function commitPaymentFailureData($paymentFailureData)
    {
        $this->getSession()->setData(self::$paymentFailureDataId, serialize($paymentFailureData));
    }

    private function validateFailureDate($paymentFailureData)
    {
        return $this->checkFailureIsOlderThan($paymentFailureData['time'], $this->getNumberOfMinuteToRemainDeactivated($this->getNumberPaymentFailures()));
    }

    private function checkFailureIsOlderThan($failureTime, $minuteDeactivation)
    {
        $timeFailure = Mage::getModel('core/date')->timestamp($failureTime);
        $timestamp = Mage::getModel('core/date')->timestamp();

        if (($timestamp - $timeFailure) > $this->getPaymentMethodDisableDuration($minuteDeactivation)) {
            return false;
        }

        return true;
    }

    public function getPaymentMethodDisableDuration($minutes)
    {
        return $minutes * 60;
    }

    public function getPaymentMethodDisableCode()
    {
        return Mage::getStoreConfig('checkout/monitor_payment/payment_code');
    }

    public function getPaymentMethodDisableEnabled()
    {
        return Mage::getStoreConfig('checkout/monitor_payment/enable');
    }

    public function getSecondConnection($read)
    {
        if ($read == 'read') {
            $name = 'second_read';
        } else {
            $name = 'second_write';
        }
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getModel('core/resource');
        $conn = $resource->getConnection($name);
        return $conn;
    }

    public function getPaymentMethodExpirySeconds()
    {
        if (!is_null($this->paymentTimeDeactivationDelay)) {
            return $this->paymentTimeDeactivationDelay;
        }
    }

    public function getCountTimerJs()
    {
        return Mage::getDesign()->getSkinUrl('js/authorisednet.time.js');
    }

    private function getNumberPaymentFailures()
    {
        $paymentFailureData = $this->loadPaymentFailureData();

        if (count($paymentFailureData)) {
            $numberFailures = $paymentFailureData['occurences'];

            return $numberFailures;
        }

        return 0;
    }

    private function readNumberPaymentFailures()
    {
        $paymentFailureData = $this->getSession()->getData(self::$paymentFailureDataId);
        if ($paymentFailureData) {
            $paymentFailureData = unserialize($paymentFailureData);
        }
        if (isset($paymentFailureData['occurences'])) {
            return $paymentFailureData['occurences'];
        }

        return 0;
    }

    /**
     * @return int
     */
    private function getNumberOfMinuteToRemainDeactivated($occurence)
    {
        switch ($occurence) {
            case 1:
                $minute = 0;
                break;
            case 2:
                $minute = 1;
                break;
            case 3:
                $minute = 3;
                break;
            case 4:
                $minute = 5;
                break;
            default:
                $minute = 10;
                break;
        }

        return $minute;
    }


}