<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Newsletter
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Customers newsletter subscription controller
 *
 * @category   Mage
 * @package    Mage_Newsletter
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Medialounge_Newsletter_ManageController extends Mage_Core_Controller_Front_Action
{
    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        parent::preDispatch();
        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
        }
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        if ($block = $this->getLayout()->getBlock('customer_newsletter')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->getLayout()->getBlock('head')->setTitle($this->__('Newsletter Subscription'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('customer/account/');
        }
        
        //added by Ravinesh on 29-Apr-2014
        /**************** E-Goi Code STARTS Here **********************/
        $is_subscribe = 0;
        $subArr = $this->getRequest()->getParam('is_subscribed', false);
        $extra_1 = "";
        if($subArr && (is_array($subArr) || $subArr instanceof Countable) && count($subArr)>0)
        {
			foreach($subArr AS $key=>$value)
			{
				$extra_1 .= "'$value'".";";
			}
			$is_subscribe = 1;
		}
		$extra_1 = rtrim($extra_1, ";");
		//if no options selected then make "general" option checked
		if($extra_1=="")
		{
			$extra_1 = "1";
		}		
        $customer = Mage::getSingleton('customer/session')->getCustomer();
		$email = $customer->getEmail();
		$firstName = $customer->getFirstname();
		$lastName = $customer->getLastname();
		$apikey	= Mage::getStoreConfig("fidelitas/config/api_key");
		
		//param for new subscriber
		$params = array(
			'apikey' => $apikey,
			'listID' => 17,
			'email' => $email,
			'subscriber' => $email,
			'extra_1' => $extra_1,
			'status' => 1,
			'first_name' => $firstName,
			'last_name' => $lastName
		);
		//check already exists
		$paramsCheck = array( 
					'apikey' => $apikey,
					'listID' => '17',
					'email' => $email,
					'subscriber' => $email
				 );	
				 
		require_once 'Zend/XmlRpc/Client.php';
		$client = new Zend_XmlRpc_Client('http://api.e-goi.com/v2/xmlrpc.php');
		
		$checkSub = $client->call('editSubscriber', array($paramsCheck));
		if (isset($checkSub['ERROR']) && $checkSub['ERROR'] == 'SUBSCRIBER_NOT_FOUND')
		{
			$mode = 'addSubscriber';
		}
		else if (isset($checkSub['ERROR']) && $checkSub['ERROR'] == 'SUBSCRIBER_DATA_CANNOT_BE_EDITED')
		{
			$mode = 'addSubscriber';
		}
		else
		{
			$mode = 'editSubscriber';
		}
		$resultGOICheck = $client->call($mode, array($params));
		//set info in cookies for e-goi tracking
		if (isset($resultGOICheck['UID']))
		{
			$objCookies = Mage::getModel("core/cookie");
			$cookiesPeriod = 86400*60;//for 2 months
			$objCookies->set("ItsHot-Egoi-Uid", $resultGOICheck['UID'], $cookiesPeriod);
			$objCookies->set("ItsHot-Egoi-List", $resultGOICheck['LIST'], $cookiesPeriod);
			$objCookies->set("ItsHot-Egoi-Email", $resultGOICheck['EMAIL'], $cookiesPeriod);
		}
		/**************** E-Goi Code END Here **********************/
        
        try {
            Mage::getSingleton('customer/session')->getCustomer()
            ->setStoreId(Mage::app()->getStore()->getId())
            //->setIsSubscribed((boolean)$this->getRequest()->getParam('is_subscribed', false))
            ->setIsSubscribed((boolean)$is_subscribe)
            ->save();
            //if ((boolean)$this->getRequest()->getParam('is_subscribed', false)) {
			if ((boolean)$is_subscribe) {
                Mage::getSingleton('customer/session')->addSuccess($this->__('The subscription has been saved.'));
            } else {
                Mage::getSingleton('customer/session')->addSuccess($this->__('The subscription has been removed.'));
            }
        }
        catch (Exception $e) {
            Mage::getSingleton('customer/session')->addError($this->__('An error occurred while saving your subscription.'));
        }
        $this->_redirect('customer/account/');
    }
}
