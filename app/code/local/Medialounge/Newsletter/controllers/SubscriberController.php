<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Newsletter
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Newsletter subscribe controller
 *
 * @category    Mage
 * @package     Mage_Newsletter
 * @author      Magento Core Team <core@magentocommerce.com>
 */
require_once Mage::getModuleDir('controllers', 'Mage_Newsletter').DS.'SubscriberController.php';

class Medialounge_Newsletter_SubscriberController extends Mage_Newsletter_SubscriberController {
	/**
      * New subscription action
      */
	public function newAction() {
		//get post values
                                if(!isset($_POST['data'])) {
                                    parent::newAction();
                                    return;
                                }
		parse_str($_POST['data'],$post);
		$email = $post['email_email_Email'];
		
		$session = Mage::getSingleton('core/session');
		$customerSession = Mage::getSingleton('customer/session');
		try {
			if(!Zend_Validate::is($email,'EmailAddress')) {
				Mage::throwException($this->__('Please enter a valid email address.'));
			}
			if(Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 && !$customerSession->isLoggedIn()) {
				Mage::throwException($this->__('Sorry, but administrator denied subscription for guests. Please <a href="%s">register</a>.', Mage::getUrl('customer/account/create/')));
			}
			$ownerId = Mage::getModel('customer/customer')
					   ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
					   ->loadByEmail($email)
					   ->getId();
			if($ownerId !== null && $ownerId != $customerSession->getId()) {
				Mage::throwException($this->__('Sorry, but your can not subscribe email adress assigned to another user.'));
				echo 0;
			}
			$status = Mage::getModel('newsletter/subscriber')->subscribe($email);
			
			//Afetr subscription create copun and send request to e-goe (8 July 2014)
			$couponCode = "";
			$couponCode = $this->createCouponCode();
			
			//*Ego request
			$apikey = Mage::getStoreConfig("fidelitas/config/api_key");
			$firstName = trim($post['firstName']);
			$lastName = trim($post['lastName']);
			$dateOfBirth = $post['birthDay']."-".$post['birthMonth']."-".$post['birthYear'];
			$mobile = trim($post['mobile']);
			$name = $firstName." ".$lastName;
			$this->sendEmail($couponCode,$name,$email);
			
			require_once 'Zend/XmlRpc/Client.php';
			$client = new Zend_XmlRpc_Client('http://api.e-goi.com/v2/xmlrpc.php');
			
			if(isset($post['interests'])) {
				$extra_1 = "";
				$subArr = $post['interests'];
				if(count($subArr) > 0) {
					foreach($subArr AS $key=>$value) {
						$extra_1 .= "'$value'".";";
					}
					$is_subscribe = 1;
				}
				$extra_1 = rtrim($extra_1,";");
				
				//param for new subscriber
				$params = array(
					'apikey'=>$apikey,
					'listID'=>17,
					'email'=>$email,
					'subscriber'=>$email,
					'extra_1'=>$extra_1,
					'status'=>1,
					'first_name'=>$firstName,
					'last_name'=>$lastName,
					'birth_date'=>$dateOfBirth,
					'cellphone'=>$mobile,
					'telephone'=>$mobile,
					//'validate_email'=>0
				);
				
				//check already exists
				$paramsCheck = array(
					'apikey'=>$apikey,
					'listID'=>17,
					'email'=>$email,
					'subscriber'=>$email
				);
				
				$checkSub = $client->call('editSubscriber',array($paramsCheck));
				if(isset($checkSub['ERROR']) && $checkSub['ERROR'] == 'SUBSCRIBER_NOT_FOUND') {
					$mode = 'addSubscriber';
				}
				else if(isset($checkSub['ERROR']) && $checkSub['ERROR'] == 'SUBSCRIBER_DATA_CANNOT_BE_EDITED') {
					$mode = 'addSubscriber';
				}
				else {
					$mode = 'editSubscriber';
				}
				
				$resultGOICheck = $client->call($mode,array($params));
				if(isset($resultGOICheck['ERROR'])) {
					if($resultGOICheck['ERROR'] == "EMAIL_ALREADY_EXISTS") {
						echo "This email address already subscribed.";
						exit;
					}
					else if($resultGOICheck['ERROR'] == "EMAIL_ADDRESS_INVALID") {
						echo "Given email address is invalid.";
						exit;
					}
					else if($resultGOICheck['ERROR'] == "EMAIL_ADDRESS_INVALID_DOESNT_EXIST") {
						echo "Given email deos not exist";
						exit;
					}
					else if($resultGOICheck['ERROR'] == "EMAIL_ADDRESS_INVALID_MX_ERROR") {
						echo "Given email address is invalid.";
						exit;
					}
					else {
						echo $resultGOICheck['ERROR'];
						exit;
					}
				}
				//Added by Raviraj On 20-Nov-2014
				//set info in cookies for e-goi tracking
				if(isset($resultGOICheck['UID']))
				{
					$objCookies = Mage::getModel("core/cookie");
					$cookiesPeriod = 86400*60;//for 2 months
					$objCookies->set("ItsHot-Egoi-Uid", $resultGOICheck['UID'], $cookiesPeriod);
					$objCookies->set("ItsHot-Egoi-List", $resultGOICheck['LIST'], $cookiesPeriod);
					$objCookies->set("ItsHot-Egoi-Email", $resultGOICheck['EMAIL'], $cookiesPeriod);
				}				
			}
			
			//End  copuon and egoi
			if($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
				$session->addSuccess($this->__('Confirmation request has been sent.'));
				echo 1;
			}
			else {
				echo 0;
			}
		}
		catch (Mage_Core_Exception $e) {
			$session->addException($e, $this->__('There was a problem with the subscription: %s',$e->getMessage()));
		}
	}
	
	/*
    Added By: Sujit kumar pandey
    Date : 27-06-14
    */
    public function addsignupAction() {
		//get post values
		parse_str($_POST['data'],$post);
		$customer_email = $post['email_email_Email'];
		$email = $post['email_email_Email'];
		$firstName = $post['firstName'];
		$lastName = $post['lastName'];
		
		//create user
		if($customer_email!='') {
			$session = Mage::getSingleton('core/session');
			$customerSession = Mage::getSingleton('customer/session');
			$password = $this->randomPassword(6);
			$customer = Mage::getModel('customer/customer');
			$customer->setWebsiteId(Mage::app()->getWebsite()->getId());
			$customer->loadByEmail($customer_email);
			try {
				/*
				 * Check if the email exist on the system.
				 * If YES,  it will not create a user account.
				 */
				if(!$customer->getId()) {
					//setting data such as email, firstname, lastname, and password
					if($firstName=="" || $firstName==NULL || empty($firstName)) {
						$firstName = 'Customer';
					}
					$firstName = trim($firstName);
					$lastName = trim($lastName);
					$customer->setFirstname($firstName);
					$customer->setLastname($lastName);
					$customer->setEmail($customer_email);
					$customer->setPassword($password);
					//the save the data and send the new account email.
					$customer->save();
					$customer->setConfirmation(1);
					//$customer->save();
					$customer->sendNewAccountEmail();
					
					//login customer
					$session = Mage::getSingleton('customer/session')->setCustomerAsLoggedIn($customer);
					$session->login($customer_email,$password);
					echo 1;
				}
				else {
					echo 2;
				}
			}
			catch(Exception $ex) {
				$session->addException($ex, $this->__('There was a problem with the subscription.'));
			}
		}
	}//end function
	
	public function olduserAction() {
		//get post values
		parse_str($_POST['data'],$post);
		$email = $post['email_email_Email'];
		$checkEmail = $this->addnewAction($email);
		if($checkEmail === "Success") {
			$couponCode = "";
			$couponCode = $this->createCouponCode();
			$firstName = trim($post['firstName']);
			$lastName = trim($post['lastName']);
			$dateOfBirth = $post['birthDay']."-".$post['birthMonth']."-".$post['birthYear'];
			$mobile = trim($post['mobile']);
			$name = $firstName." ".$lastName ;
			$this->sendEmail($couponCode,$name,$email);
			
			$apikey = Mage::getStoreConfig("fidelitas/config/api_key");
			require_once 'Zend/XmlRpc/Client.php';
			$client = new Zend_XmlRpc_Client('http://api.e-goi.com/v2/xmlrpc.php');
			if(isset($post['interests'])) {
				$extra_1 = "";
				$subArr = $post['interests'];
				if(count($subArr) > 0) {
					foreach($subArr AS $key => $value) {
						$extra_1 .= "'$value'".";";
					}
					$is_subscribe = 1;
				}
				$extra_1 = rtrim($extra_1,";");
				
				//param for new subscriber
				$params = array(
					'apikey'=>$apikey,
					'listID'=>17,
					'email'=>$email,
					'subscriber'=>$email,
					'extra_1'=>$extra_1,
					'status'=>1,
					'first_name'=>$firstName,
					'last_name'=>$lastName,
					'birth_date'=>$dateOfBirth,
					'cellphone'=>$mobile,
					'telephone'=>$mobile,
					'validate_email'=>2
				);
				
				//check already exists
				$paramsCheck = array(
					'apikey'=>$apikey,
					'listID'=>17,
					'email'=>$email,
					'subscriber'=>$email
				);
				
				$checkSub = $client->call('editSubscriber',array($paramsCheck));
				if(isset($checkSub['ERROR']) && $checkSub['ERROR'] == 'SUBSCRIBER_NOT_FOUND') {
					$mode = 'addSubscriber';
				}
				else if(isset($checkSub['ERROR']) && $checkSub['ERROR'] == 'SUBSCRIBER_DATA_CANNOT_BE_EDITED') {
					$mode = 'addSubscriber';
				}
				else {
					$mode = 'editSubscriber';
				}
				
				$resultGOICheck = $client->call($mode, array($params));
				
				if(isset($resultGOICheck['ERROR'])) {
					if($resultGOICheck['ERROR'] == "EMAIL_ALREADY_EXISTS") {
						echo "This email address already subscribed.";
					}
					else if($resultGOICheck['ERROR'] == "EMAIL_ADDRESS_INVALID_MX_ERROR") {
						echo "Given email address is invalid.";
					}
					else {
						echo $resultGOICheck['ERROR'];
					}
				}
				elseif(isset($resultGOICheck['UID'])) {
					echo "0";
					//Added by Raviraj On 20-Nov-2014
					//set info in cookies for e-goi tracking
					$objCookies = Mage::getModel("core/cookie");
					$cookiesPeriod = 86400*365*2;//for 2 years
					$objCookies->set("ItsHot-Egoi-Uid", $resultGOICheck['UID'], $cookiesPeriod);
					$objCookies->set("ItsHot-Egoi-List", $resultGOICheck['LIST'], $cookiesPeriod);
					$objCookies->set("ItsHot-Egoi-Email", $resultGOICheck['EMAIL'], $cookiesPeriod);
				}
			}
			else {
				echo "Please select you interest to subscribe.";
			}
		}
		else {
			echo $checkEmail;
		}
		exit;
	}
	
	public function createCouponCode() {
		$couponCode = strtoupper(substr(md5(rand(0,1000000)),0,6));
		$fromdate = date('Y-m-d h:i:s',Mage::getModel('core/date')->timestamp(time()));
		$fromexpire = date('Y-m-d h:i:s',Mage::getModel('core/date')->timestamp(time()+(30*24*60*60)));
		$simpleAction = 'by_percent';
		$amount = 5;
		$description = 'Newsletter coupon code';
		$couponId = $couponCode;
		$minimumAmt = 1;
		
		$coupon = Mage::getModel('salesrule/rule');
		$coupon->setName($description)
			   ->setDescription($description)
			   ->setFromDate($fromdate)
			   ->setCouponCode($couponCode)
			   ->setToDate($fromexpire)
			   ->setCouponType(2)
			   ->setUsesPerCoupon('')
			   ->setUsesPerCustomer('')
			   ->setCustomerGroupIds(array(0,1,2,3)) //an array of customer grou pids
			   ->setIsActive(1)
			   ->setActionsSerialized('a:6:{s:4:"type";s:40:"salesrule/rule_condition_product_combine";s:9:"attribute";N;s:8:"operator";N;s:5:"value";s:1:"1";s:18:"is_value_processed";N;s:10:"aggregator";s:3:"all";}')
			   ->setStopRulesProcessing(0)
			   ->setIsAdvanced(1)
			   ->setProductIds('')
			   ->setSortOrder(0)
			   ->setSimpleAction($simpleAction)
			   ->setDiscountAmount($amount)
			   ->setDiscountQty(null)
			   ->setDiscountStep('0')
			   ->setSimpleFreeShipping(0)
			   ->setApplyToShipping(0)
			   ->setIsRss(0)
			   ->setWebsiteIds(array(1));
		$conditions = array();
		$conditions[1] = array('type'=>'salesrule/rule_condition_combine','aggregator'=>'all','value'=>1,'new_child'=>'');
		$conditions['1--1'] = array('type'=>'salesrule/rule_condition_address','attribute'=>'base_subtotal','operator'=>'>','value'=>$minimumAmt);
		$coupon->setData('conditions',$conditions);
		$coupon->loadPost($coupon->getData());
		try {
			$coupon->save();
			return $couponCode;
		}
		catch(Exception $e) {
			$fileN = 'log_coupon_'.date('Y-m-d_h:i:s').".txt";
			$host  = '<a href="http://'.$_SERVER['HTTP_HOST']."/feeds/".$fileN.'">'.$_SERVER['HTTP_HOST']."/feeds/".$fileN.'</a>';
			
			$content = 'There is an error ocuurs during create a coupon code: '.$e->getMessage()."\n";
			$fp = fopen($_SERVER['DOCUMENT_ROOT']."/feeds/".$fileN,"w");
			fwrite($fp,$content);
			fclose($fp);
			return;
		}
	}
	
	/********** start send email ************/
	public function sendEmail($code, $names, $email) {
		$translate = Mage::getSingleton('core/translate');
		
		/* @var $translate Mage_Core_Model_Translate */
		$translate->setTranslateInline(false);
		$mailTemplate = Mage::getModel('core/email_template');
		/* @var $mailTemplate Mage_Core_Model_Email_Template */
		
		$templateId = 22;//template for sending customer data
		$template_collection = $mailTemplate->load($templateId);
		$template_data = $template_collection->getData();
		if(!empty($template_data)) {
			$i = 1;
			$message = '';
			$collection = Mage::getModel('core/app')->getBestSellingItem(8);
			if(count($collection) > 0) {
				$message = '<table align="center" cellpadding="1" cellspacing="1" height="195" width="599">';
				$message .= '<tr><td colspan="4" style="padding-top:10px; font-weight:bold;"><p>Recommended products for you:</p></td></tr>';
				$message .= '<tr>';
				foreach($collection as $item) {
					$image = Mage::helper('catalog/image')->init($item,'thumbnail')->resize(144,144);
					$name = '';
					$name_cnt = 1;
					$name_array = explode(' ',$item->getName());
					foreach($name_array as $name_split) {
						$name .= $name_split.' ';
						if($name_cnt==5) {
							$name .= '..';
							break;
						}
						$name_cnt++;
					}
					$message .= '<td valign="top" style="text-align:center;">';
					$message .= '<span style="font-size:12px;">';
					$message .= '<span style="font-family:arial,helvetica,sans-serif;">';
					$message .= '<a href="'.$item->getProductUrl().'" target="_blank">';
					$message .= '<img src="'.$image.'" height="144" width="144" style="border-width:1px; border-style:solid; margin-top:1px; margin-bottom:1px;" />';
					$message .= '</a>';
					$message .= '<br />';
					$message .= trim($name);
					$message .= '<br />';
					$message .= '<span style="color:#FF0000; font-weight:bold;">';
					$message .= Mage::helper('core')->currency($item->getSpecialPrice(),true,false);
					$message .= '</span>';
					$message .= '</span>';
					$message .= '</span>';
					$message .= '</td>';
					if((($i)%4) == 0) {
						$message .= '</tr><tr>';
					}
					$i++;
				}
				$message .= '</tr>';
				$message .= '</table>';
			}
			
			$templateId = $template_data['template_id'];
			$mailSubject = $template_data['template_subject'];
			$from_email = Mage::getStoreConfig('trans_email/ident_sales/email');//fetch sender email
			$from_name = Mage::getStoreConfig('trans_email/ident_sales/name');//fetch sender name
			$sender = array('name'=>$from_name,'email'=>$from_email);
			$names = trim($names);
			if($names=="" || $names==NULL || empty($names)) {
				$names = 'Customer';
				$names = trim($names);
			}
			
			/* Get the Unsubscription Link: Added on Date: 29-09-2015: */
			$UnsubscriptionLink = Mage::getModel('newsletter/subscriber')->loadByEmail($email)->getUnsubscriptionLink();
			
			//$vars = array('full_name'=>$names,'coupon'=>$code,'show_product_list'=>$message);//for replacing the variables in email with data
			$vars = array('full_name'=>$names,'coupon'=>$code,'show_product_list'=>$message, 'unsubscribe' =>$UnsubscriptionLink);
			
			/*This is optional*/
			$storeId = Mage::app()->getStore()->getId();
			$model = $mailTemplate->setReplyTo($sender['email'])->setTemplateSubject($mailSubject);
			//$email = Mage::getStoreConfig('contacts/email/recipient_email');
			if($names=='Customer') {
				$names = $email;
			}
			$model->sendTransactional($templateId,$sender,$email,$names,$vars,$storeId);
			if(!$mailTemplate->getSentSuccess()) {
				//throw new Exception();
				return false;
			}
			$translate->setTranslateInline(true);
			return true;
		}
	}
	/********** end send email ************/
	
	/*
	Added By: Sujit kumar pandey
	Date : 26-06-14
	*/
	public function signupsubscribeAction() {
		$this->loadLayout();
		$this->renderLayout();
	}
	
	/**
	 * Subscription confirm action
	 */
	public function confirmAction() {
		$id = (int) $this->getRequest()->getParam('id');
		$code = (string) $this->getRequest()->getParam('code');
		
		if($id && $code) {
			$subscriber = Mage::getModel('newsletter/subscriber')->load($id);
			$session = Mage::getSingleton('core/session');
			if($subscriber->getId() && $subscriber->getCode()) {
				if($subscriber->confirm($code)) {
					$session->addSuccess($this->__('Your subscription has been confirmed.'));
				}
				else {
					$session->addError($this->__('Invalid subscription confirmation code.'));
				}
			}
			else {
				$session->addError($this->__('Invalid subscription ID.'));
			}
		}
		$this->_redirectUrl(Mage::getBaseUrl());
	}
	
	/**
     * Unsubscribe newsletter
     */
	public function unsubscribeAction() {
		$id = (int) $this->getRequest()->getParam('id');
		$code = (string) $this->getRequest()->getParam('code');
		
		if($id && $code) {
			$session = Mage::getSingleton('core/session');
			try {
				Mage::getModel('newsletter/subscriber')->load($id)->setCheckCode($code)->unsubscribe();
				$session->addSuccess($this->__('You have been unsubscribed.'));
				
				/*------- Unsubscribe user from Egoi Emailr : Added on date 28-09-2015: ------------*/
				$apikey = Mage::getStoreConfig("fidelitas/config/api_key");
				require_once 'Zend/XmlRpc/Client.php';
				$client = new Zend_XmlRpc_Client('http://api.e-goi.com/v2/xmlrpc.php');
				
				$subscriber = Mage::getModel('newsletter/subscriber')->load($id);
				$email = $subscriber->getEmail();
				
				$params = array(	'apikey'     => $apikey,
                    'listID'     => 17,
                    'subscriber' => $email);
                $result = $client->call('removeSubscriber', array($params));
				//var_dump($result);
				/*----------------------------------------------------------------------------------*/
				
			}
			catch (Mage_Core_Exception $e) {
				$session->addException($e,$e->getMessage());
			}
			catch (Exception $e) {
				$session->addException($e, $this->__('There was a problem with the un-subscription.'));
			}
		}
		$this->_redirectReferer();
	}
	
	/**
     * View product action
     */
	public function subscribeAction() {
		$email = $this->getRequest()->getParam('val');
		Mage::getSingleton('core/session')->setSessionEmail($email);// In the Controller
		$this->loadLayout('subscriber');
		$this->renderLayout();
	}
	
	/**
	 * @Added On 6 Feb 2014
	 * @Added By: Pramod
	 * @Description: Send subscriber information to E-goi when user subscribed from Bottom or Left Newsletter field
	 **/
	public function goiAction()
	{
		//get post values
		parse_str($_POST['data'],$post);
		$email = $post['email_email_Email'];
		$checkEmail = $this->addnewAction($email);
		
		if($checkEmail === "Success")
		{
			$firstName = trim($post['firstName']);
			$lastName = trim($post['lastName']);
			$dateOfBirth = $post['birthDay']."-".$post['birthMonth']."-".$post['birthYear'];
			$dateOfAnniversary = $post['anniversaryDay']."-".$post['anniversaryMonth']."-".$post['anniversaryYear'];
			$mobile = trim($post['mobile']);
			
			$apikey = Mage::getStoreConfig("fidelitas/config/api_key");
			require_once 'Zend/XmlRpc/Client.php';
			$client = new Zend_XmlRpc_Client('http://api.e-goi.com/v2/xmlrpc.php');
			
			if(isset($post['interests']))
			{
				$extra_1 = "";
				$subArr = $post['interests'];
				if(count($subArr) > 0)
				{
					foreach($subArr AS $key=>$value)
					{
						$extra_1 .= "'$value'".";";
					}
					$is_subscribe = 1;
				}
				$extra_1 = rtrim($extra_1,";");
				//Added code for anniversary at 11 Sep 2017 by Ankush 
				//param for new subscriber
				$params = array(
					'apikey'=>$apikey,
					'listID'=>17,
					'email'=>$email,
					'subscriber'=>$email,
					'status'=>1,
					'first_name'=>$firstName,
					'last_name'=>$lastName,
					'birth_date'=>$dateOfBirth,
					'anniversary'=>$dateOfAnniversary,
					'extra_7'=>$dateOfAnniversary,
					'EXTRA_FIELD_7'=>$dateOfAnniversary,	
					'cellphone'=>$mobile,
					'telephone'=>$mobile,
					'validate_email'=>2
				);
				
				//get Extra field name and Id
				$paramsListInfo = array(
					'apikey'=>$apikey,
					'listID'=>17
				);
				$extraFieldId = 0;
				$listInfoRes = $client->call('getLists',array($paramsListInfo));
				
                                                                if (count($listInfoRes) > 0) {
                                                                    $extra_fields = $listInfoRes[0]["extra_fields"];
                                                                    if (count($extra_fields) > 0) {
                                                                        $extraFieldId = $listInfoRes[0]["extra_fields"][0]["id"];
                                                                        if ($extraFieldId > 0) {
                                                                            if ($extraFieldId == 1) {
                                                                                $params['extra_1'] = $extra_1;
                                                                            } else if ($extraFieldId == 2) {
                                                                                $params['extra_2'] = $extra_1;
                                                                            }
                                                                        }
                                                                    }
                                                                }

                //check already exists
				$paramsCheck = array(
					'apikey'=>$apikey,
					'listID'=>17,
					'email'=>$email,
					'subscriber'=>$email
				);
				
				$checkSub = $client->call('editSubscriber',array($paramsCheck));
				if(isset($checkSub['ERROR']) && $checkSub['ERROR'] == 'SUBSCRIBER_NOT_FOUND')
				{
					$mode = 'addSubscriber';
				}
				else if(isset($checkSub['ERROR']) && $checkSub['ERROR'] == 'SUBSCRIBER_DATA_CANNOT_BE_EDITED')
				{
					$mode = 'addSubscriber';
				}
				else
				{
					$mode = 'editSubscriber';
				}
				
				$resultGOICheck = $client->call($mode,array($params));
				
				if(isset($resultGOICheck['ERROR']))
				{
					if($resultGOICheck['ERROR'] == "EMAIL_ALREADY_EXISTS")
					{
						echo "This email address already subscribed.";
					}
					else if($resultGOICheck['ERROR'] == "EMAIL_ADDRESS_INVALID_MX_ERROR" || $resultGOICheck['ERROR'] == "EMAIL_ADDRESS_INVALID")
					{
						echo "Given email address is invalid.";
					}
					else
					{
						echo $resultGOICheck['ERROR'];
					}
				}
				elseif (isset($resultGOICheck['UID']))
				{
					echo "1";
					//set info in cookies for e-goi tracking
					$objCookies = Mage::getModel("core/cookie");
					$cookiesPeriod = 86400*365*2;//for 2 years
					$objCookies->set("ItsHot-Egoi-Uid", $resultGOICheck['UID'], $cookiesPeriod);
					$objCookies->set("ItsHot-Egoi-List", $resultGOICheck['LIST'], $cookiesPeriod);
					$objCookies->set("ItsHot-Egoi-Email", $resultGOICheck['EMAIL'], $cookiesPeriod);
					//echo "1#".$resultGOICheck['UID']."#".$resultGOICheck['LIST']."#".$resultGOICheck['EMAIL'];
				}
			}
			else
			{
				echo "Please select your interest to subscribe.";
			}
		}
		else
		{
			echo $checkEmail;
		}
		exit;
	}
	
	public function goisubscribeAction() {
		$this->loadLayout();
		$this->renderLayout();
	}
	
	public function olduserpageAction() {
		$this->loadLayout();
		$this->renderLayout();
	}
	
	//Created By: Ravinesh(MS) on 5-May-2014
	public function addnewAction($email)
	{
		if($email) {
			$session = Mage::getSingleton('core/session');
			$customerSession = Mage::getSingleton('customer/session');
			try {
				if(!Zend_Validate::is($email,'EmailAddress')) {
					return "Please enter a valid email address.";
				}
				if(Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 && !$customerSession->isLoggedIn()) {
					return 'Sorry, but administrator denied subscription for guests.';
				}
				$ownerId = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($email)->getId();
				if($ownerId !== null && $ownerId != $customerSession->getId()) {
					return 'Success';
				}
				$status = Mage::getModel('newsletter/subscriber')->subscribe($email);
				if($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
					return 'Success';
				}
				else {
					return "Success";
				}
			}
			catch(Mage_Core_Exception $e) {
				return "Success";
			}
			catch(Exception $e) {
				return "Success";
				if($e->getMessage()=="EMAIL_ALREADY_EXISTS") {
					return "Success";
				}
			}
		}
	}//end function
	
	function randomPassword($length) {
		$password = '';
		$passwords = array_merge(range(0,9),range('a','z'));
		for($i = 0; $i < $length; $i++) {
			$password .= $passwords[array_rand($passwords)];
		}
		return $password;
	}
	
	//Added by Ankush Garg on 10 Nov 2014 for newsletter subscriber of mobile theme
	public function mobilenewAction() {
		//get post values
		$email = $_POST['email'];
		$checkEmail = $this->addnewAction($email);
		
		/* Date: 20-01-2017
		 * If new user email added Success, then show the below message only.
		 * */
		if($checkEmail === "Success") {
			Mage::getSingleton('core/session')->addSuccess($this->__('You have been successfully subscribed.'));
		}
		
		/* Date: 20-01-2017
		 * No need to send emails using Egoi, Its managed by ConvertCart next onwards.
		 */
		/*
		 * if($checkEmail === "Success_") {
			$firstName = '';
			$lastName = '';
			$birthDay = -1;
			$birthMonth = -1;
			$birthYear = -1;
			$dateOfBirth = $birthDay."-".$birthMonth."-".$birthYear;
			$mobile = '';
			
			$apikey = Mage::getStoreConfig("fidelitas/config/api_key");
			require_once 'Zend/XmlRpc/Client.php';
			$client = new Zend_XmlRpc_Client('http://api.e-goi.com/v2/xmlrpc.php');
			
			$extra_1 = "";
			$subArr = array(1);
			if(count($subArr) > 0) {
				foreach($subArr AS $key=>$value) {
					$extra_1 .= "'$value'".";";
				}
				$is_subscribe = 1;
			}
			$extra_1 = rtrim($extra_1,";");
			
			//param for new subscriber
			$params = array(
				'apikey'=>$apikey,
				'listID'=>17,
				'email'=>$email,
				'subscriber'=>$email,
				'extra_1'=>$extra_1,
				'status'=>1,
				'first_name'=>$firstName,
				'last_name'=>$lastName,
				'birth_date'=>$dateOfBirth,
				'cellphone'=>$mobile,
				'telephone'=>$mobile,
				'validate_email'=>2
			);
			
			//check already exists
			$paramsCheck = array(
				'apikey'=>$apikey,
				'listID'=>17,
				'email'=>$email,
				'subscriber'=>$email
			);
			
			$checkSub = $client->call('editSubscriber',array($paramsCheck));
			if(isset($checkSub['ERROR']) && $checkSub['ERROR'] == 'SUBSCRIBER_NOT_FOUND') {
				$mode = 'addSubscriber';
			}
			else if(isset($checkSub['ERROR']) && $checkSub['ERROR'] == 'SUBSCRIBER_DATA_CANNOT_BE_EDITED') {
				$mode = 'addSubscriber';
			}
			else {
				$mode = 'editSubscriber';
			}
			
			$resultGOICheck = $client->call($mode,array($params));
			if(isset($resultGOICheck['ERROR'])) {
				if($resultGOICheck['ERROR'] == "EMAIL_ALREADY_EXISTS") {
					Mage::getSingleton('core/session')->addError($this->__("This email address already subscribed."));
				}
				else if($resultGOICheck['ERROR'] == "EMAIL_ADDRESS_INVALID_MX_ERROR" || $resultGOICheck['ERROR'] == "EMAIL_ADDRESS_INVALID") {
					Mage::getSingleton('core/session')->addError($this->__("Given email address is invalid."));
				}
				else {
					Mage::getSingleton('core/session')->addError($this->__($resultGOICheck['ERROR']));
				}
			}
			elseif(isset($resultGOICheck['UID'])) {
				//Added by Raviraj(MS) On 20-Nov-2014
				//set info in cookies for e-goi tracking
				$objCookies = Mage::getModel("core/cookie");
				$cookiesPeriod = 86400*365*2;//for 2 years
				$objCookies->set("ItsHot-Egoi-Uid", $resultGOICheck['UID'], $cookiesPeriod);
				$objCookies->set("ItsHot-Egoi-List", $resultGOICheck['LIST'], $cookiesPeriod);
				$objCookies->set("ItsHot-Egoi-Email", $resultGOICheck['EMAIL'], $cookiesPeriod);
				Mage::getSingleton('core/session')->addSuccess($this->__('You have been successfully subscribed.'));
			}
		}
		else {
			Mage::getSingleton('core/session')->addError($this->__($checkEmail));
		}
		* */
		$this->_redirectReferer();
	}
	//End
}
