<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('menuadminproitem'), 'is_en_add_cms_block',
              [
                  'type'     => Varien_Db_Ddl_Table::TYPE_SMALLINT,
                  'nullable' => false,
                  'comment'  => 'Enable Additional Cms Block'
              ]
          );
$installer->getConnection()->addColumn($installer->getTable('menuadminproitem'), 'add_cms_block',
              [
                  'type'     => Varien_Db_Ddl_Table::TYPE_TEXT,
                  'nullable' => false,
                  'length'   => 255,
                  'comment'  => 'Additional CMS Block'
              ]
          );

$installer->endSetup();