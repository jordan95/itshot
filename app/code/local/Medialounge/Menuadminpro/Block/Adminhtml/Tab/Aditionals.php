<?php
class Medialounge_Menuadminpro_Block_Adminhtml_Tab_Aditionals extends Mgtoxp_Menuadminpro_Block_Adminhtml_Menuadminpro_Tab_Aditionals
{
	
	public function _prepareLayout()
	{
        Varien_Data_Form::setElementRenderer(
            $this->getLayout()->createBlock('adminhtml/widget_form_renderer_element')
        );
        Varien_Data_Form::setFieldsetRenderer(
            $this->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset')
        );
        Varien_Data_Form::setFieldsetElementRenderer(
            $this->getLayout()->createBlock('adminhtml/catalog_form_renderer_fieldset_element')
        );

		$form = new Varien_Data_Form();
		$form->setDataObject($this->getItem());
		
		$fieldset = $form->addFieldset('dropdown_fieldset', array('legend'=>Mage::helper('catalog')->__('Dropdown settings')));

		
		$fieldset->addField('itemsperrowsub', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Items per Row'),
				'class'     => '',
				'required'  => false,
				'name'      => 'itemsperrowsub',
		));
		
		$fieldset->addField('distributeitemssub', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Distribute items in columns'),
				'name'      => 'distributeitemssub',
				'values'    => array(
						1    => Mage::helper('menuadminpro')->__('Yes'),
						0   => Mage::helper('menuadminpro')->__('No')
				),
		));

        $fieldset->addField('is_en_add_cms_block', 'select',
            [
                'label'  => Mage::helper('menuadminpro')->__('Enable Additional CMS BLock'),
                'name'   => 'is_en_add_cms_block',
                'values' => array(
                    1 => Mage::helper('menuadminpro')->__('Yes'),
                    0 => Mage::helper('menuadminpro')->__('No')
                ),
            ]
        );
	
		$fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('catalog')->__('Header/Footer dropdown')));
		
		$fieldset->addField('header', 'editor', array(
				'label'     => Mage::helper('menuadminpro')->__('Header'),
				'required'  => false,
				'name'      => 'header',
				'style'     => 'width:700px;height:12em;',
		));
		
		$fieldset->addField('footer', 'textarea', array(
				'label'     => Mage::helper('menuadminpro')->__('Footer'),
				'required'  => false,
				'name'      => 'footer',
				'style'     => 'width:700px;height:12em;'
		));
		
		$fieldset = $form->addFieldset('products_fieldset', array('legend'=>Mage::helper('catalog')->__('Products')));
	
		$fieldset->addField('productstotal', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Qty Products'),
				'class'     => '',
				'required'  => false,
				'name'      => 'productstotal',
		));
		
		$fieldset->addField('productsrow', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Products per Row'),
				'class'     => '',
				'required'  => false,
				'name'      => 'productsrow',
		));
		
		$fieldset->addField('productblockposition', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Products Block Position'),
				'name'      => 'productblockposition',
				'class'     => '',
				'required'  => false,
				'values'    => Mage::getSingleton('menuadminpro/productblocks')->getOptionArray()
		));
		
		$fieldset->addField('productblockpercent', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Percentage of total width'),
				'class'     => '',
				'required'  => false,
				'name'      => 'productblockpercent',
				'value'		=> '30'
		));
		
		
		$fieldset = $form->addFieldset('cms_fieldset', array('legend'=>Mage::helper('catalog')->__('CMS Block')));
		
		$fieldset->addField('cmsblock', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('CMS Block'),
				'name'      => 'cmsblock',
				'class'     => '',
				'required'  => false,
				'values'    => Mage::getSingleton('menuadminpro/cmsblocks')->getCmsBlocks()
		));
		
		$cmsblockposition = $fieldset->addField('cmsblockposition', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('CMS Block Position'),
				'name'      => 'cmsblockposition',
				'class'     => '',
				'required'  => false,
				'values'    => Mage::getSingleton('menuadminpro/cmsblocks')->getOptionArray()
		));
		
		$cmsblockpercent = $fieldset->addField('cmsblockpercent', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Percentage of total width'),
				'class'     => '',
				'required'  => false,
				'name'      => 'cmsblockpercent',
				'value'		=> '30'
		));

        $fieldset = $form->addFieldset('cms_extended_fieldset',
            array('legend' => Mage::helper('catalog')->__('CMS Block Additional')));

        $fieldset->addField('add_cms_block', 'select', array(
            'label'    => Mage::helper('menuadminpro')->__('CMS Block Additional'),
            'name'     => 'add_cms_block',
            'class'    => '',
            'required' => false,
            'values'   => Mage::getSingleton('menuadminpro/cmsblocks')->getCmsBlocks()
        ));
		
		$this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
				->addFieldMap($cmsblockposition->getHtmlId(), $cmsblockposition->getName())
				->addFieldMap($cmsblockpercent->getHtmlId(), $cmsblockpercent->getName())
				->addFieldDependence(
						$cmsblockpercent->getName(),
						$cmsblockposition->getName(),
						array('right', 'left')
				)
		);

		$form->addValues($this->getItem()->getData());
		$form->setFieldNameSuffix('general');
		$this->setForm($form);
	}
	

}