<?php

class Medialounge_Menuadminpro_Model_Observer
{

    const MENU_ACTION_NAME = 'adminhtml_menuadminpro_edit';
    const BLOCK_TYPE = 'menuadminpro/adminhtml_menuadminpro_tab_aditionals';
    const DROPDOWN_SETTINGS_FIELDSET = 'dropdown_fieldset';

    public function addMenuItemsSettings(Varien_Event_Observer $observer)
    {
        if (self::MENU_ACTION_NAME === Mage::app()->getFrontController()->getAction()->getFullActionName()) {
            $block = $observer->getEvent()->getBlock();
            if ( ! isset($block)) {
                return $this;
            }
            if ($block->getType() == self::BLOCK_TYPE) {
                $form     = $block->getForm();
                $fieldset = $form->addFieldset('cms_extended_fieldset',
                    array('legend' => Mage::helper('catalog')->__('CMS Block Additional')));

                $fieldset->addField('add_cms_block', 'select', array(
                    'label'    => Mage::helper('menuadminpro')->__('CMS Block Additional'),
                    'name'     => 'add_cms_block',
                    'class'    => '',
                    'required' => false,
                    'values'   => Mage::getSingleton('menuadminpro/cmsblocks')->getCmsBlocks()
                ));

                $dropdown_settings = $form->getElement(self::DROPDOWN_SETTINGS_FIELDSET);
                $dropdown_settings->addField('is_en_add_cms_block', 'select',
                    [
                        'label'  => Mage::helper('menuadminpro')->__('Enable Additional CMS BLock'),
                        'name'   => 'is_en_add_cms_block',
                        'values' => array(
                            1 => Mage::helper('menuadminpro')->__('Yes'),
                            0 => Mage::helper('menuadminpro')->__('No')
                        ),
                    ]
                );
            }
        }
    }
}