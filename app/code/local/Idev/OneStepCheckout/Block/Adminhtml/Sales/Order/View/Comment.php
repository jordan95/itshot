<?php
class Idev_OneStepCheckout_Block_Adminhtml_Sales_Order_View_Comment extends Mage_Adminhtml_Block_Sales_Order_View_Items
{
    public function _toHtml(){
        $html = parent::_toHtml();
        $comment = $this->getCommentHtml();
        return $html.$comment;
    }

    /**
     * get comment from order and return as html formatted string
     *
     *@return string
     */
    public function getCommentHtml(){
        $comment = $this->getOrder()->getOnestepcheckoutCustomercomment();
        $user = Mage::getModel('admin/user')->load($this->getOrder()->getOnestepcheckoutCustomercommentBy());
        $comment_by = $user->getName();
        $comment_at = $this->getOrder()->getOnestepcheckoutCustomercommentAt();
        $comment_at = Mage::helper('core')->formatDate($comment_at, 'medium', true);
        $feedback = $this->getOrder()->getOnestepcheckoutCustomerfeedback();
        $html = '';
        $url = Mage::getUrl('adminhtml/comment/updateform/', array('order_id'=>$this->getOrder()->getId()));
        if ($this->isShowCustomerCommentEnabled()){
            if(Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/customer_comment') || !is_null($comment)){
                $html .= '<div id="customer_comment" class="giftmessage-whole-order-container"><div class="entry-edit">';
                $html .= '<div class="entry-edit-head"><h4>'.$this->helper('onestepcheckout')->__('Customer Comment').'</h4></div>';
                $html .= '<fieldset id="block-edit-comment"><span style="color:red;">'.$comment.'</span>';
                if (!empty($comment) && !empty(trim($comment_by))) {
                    $html .= '<br>';
                    $html .= '<span>Last edited by <strong>'.$comment_by.'</strong> at '.$comment_at.'</span>';
                }
                
                if(Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/customer_comment')){
                    $html .= '<button class="btn-edit-comment" onclick="updateFormComment(\''.$url.'\'); return false;">Edit</button>';
                }
                $html .= '</fieldset></div></div>';
            }
        }

        if($this->isShowCustomerFeedbackEnabled()){
            $html .= '<div id="customer_feedback" class="giftmessage-whole-order-container"><div class="entry-edit">';
            $html .= '<div class="entry-edit-head"><h4>'.$this->helper('onestepcheckout')->__('Customer Feedback').'</h4></div>';
            $html .= '<fieldset>'.nl2br(Mage::helper('core')->escapeHtml($feedback)).'</fieldset>';
            $html .= '</div></div>';
        }

        return $html;
    }

    public function isShowCustomerCommentEnabled(){
        return Mage::getStoreConfig('onestepcheckout/exclude_fields/enable_comments', $this->getOrder()->getStore());
    }

    public function isShowCustomerFeedbackEnabled(){
        return Mage::getStoreConfig('onestepcheckout/feedback/enable_feedback', $this->getOrder()->getStore());
    }
}
