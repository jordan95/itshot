<?php

/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Block_Adminhtml_Partialpayment_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
  public function getOrder()
    {
        if ($this->hasOrder()) {
            return $this->getData('order');
        }
        if (Mage::registry('current_order')) {
            return Mage::registry('current_order');
        }
        if (Mage::registry('order')) {
            return Mage::registry('order');
        }
        Mage::throwException(Mage::helper('sales')->__('Cannot get the order instance.'));
    }

  public function __construct()
  {
      parent::__construct();
      $this->setId('partialpayment_order_view_tabs');
      $this->setDestElementId('sales_order_view');
      $this->setTitle(Mage::helper('partialpayment')->__('Partialpayment Order Information'));
  }
  
  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('partialpayment')->__('Pay Installment'),
          'title'     => Mage::helper('partialpayment')->__('Pay Installment'),
          'content'   => $this->getLayout()->createBlock('partialpayment/adminhtml_partialpayment_edit_tab_installments')->setTemplate('partialpayment/sales/order/payinstallment.phtml')->toHtml(),
      ));
      return parent::_beforeToHtml();
  }

  //protected function _beforeToHtml()
  //{
  //    $this->addTab('form_section', array(
  //        'label'     => Mage::helper('partialpayment')->__('Partialpayment Information'),
  //        'title'     => Mage::helper('partialpayment')->__('Partialpayment Information'),
  //        'content'   => $this->getLayout()->createBlock('partialpayment/adminhtml_partialpayment_edit_tab_partialpayment')->setTemplate('partialpayment/sales/order/detail.phtml')->toHtml(),
  //    ));
  //    
  //    
  //   
  //    return parent::_beforeToHtml();
  //}
}