<?php

/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Block_Adminhtml_Partialpayment_View extends Mage_Adminhtml_Block_Widget_Form_Container
{
     
	
    public function __construct()
    {
        $this->_objectId = 'order_id';
        $this->_blockGroup = 'partialpayment';
        $this->_controller = 'adminhtml_partialpayment';
	
        parent::__construct();
	$this->setId('sales_order_view');
	$this->_removeButton('save');
	
            $this->_addButton('view', array(
                'label'     => Mage::helper('sales')->__('View'),
                'onclick'   => 'setLocation(\'' . $this->getViewUrl() . '\')',
            ));
        
       
    }
    public function getViewUrl()
    {
	if( Mage::registry('partialpayment_data') && Mage::registry('partialpayment_data')->getId() ) {
	    return $this->getUrl('adminhtml/sales_order/view', array('order_id'=>Mage::registry('partialpayment_data')->getId()));
	} 
    }
    public function getHeaderText()
    {
        if( Mage::registry('partialpayment_data') && Mage::registry('partialpayment_data')->getRealOrderId() ) {
            return Mage::helper('partialpayment')->__("Partialpayment Order '%s'", $this->htmlEscape(Mage::registry('partialpayment_data')->getRealOrderId()));
        } 
    }
}