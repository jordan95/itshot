<?php

/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Block_Adminhtml_Partialpayment extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_partialpayment';
    $this->_blockGroup = 'partialpayment';
    $this->_headerText = Mage::helper('partialpayment')->__('Partialpayment Orders');
    parent::__construct();$this->_removeButton('add');
  }
}