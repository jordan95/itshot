<?php

/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Block_Adminhtml_Sales_Order_Totals_Partialpayment extends Mage_Adminhtml_Block_Sales_Order_Totals_Item {

    public function getStrong() {
        return true;
    }

}