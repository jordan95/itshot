<?php

/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Block_Checkout_Partialpaymentfee extends Mage_Checkout_Block_Total_Default
{
    protected $_template = 'partialpayment/partialpayment_totals.phtml';

    public function getIncludeTaxLabel()
    { 	
        return $this->helper('partialpayment')->__('Partialpayment Fee (Incl. Tax)');
    }

    public function getExcludeTaxLabel()
    {
        return $this->helper('partialpayment')->__('Partialpayment Fee (Excl. Tax)');
    }
}
