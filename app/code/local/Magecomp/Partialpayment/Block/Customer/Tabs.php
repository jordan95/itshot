<?php

/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Block_Customer_Tabs extends Mage_Adminhtml_Block_Customer_Edit_Tabs
{

    protected function _prepareLayout()
    {
        $ret = parent::_prepareLayout();
        if(Mage::helper('partialpayment')->getStoredDatafor('enable')){
	    $this->addTabAfter('magecomp_partialpayment_orders', 'partialpayment/customer_edit_orders','orders');
	    $this->addTabAfter('magecomp_partialpayment_installments', 'partialpayment/customer_edit_installments','orders');
	   
	}
        return $ret;
    } 
}
