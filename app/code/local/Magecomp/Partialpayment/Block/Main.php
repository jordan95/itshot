<?php 

/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Block_Main extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
	    ->addFieldToFilter('is_partialpayment',1)
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            //->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
            ->setOrder('created_at', 'desc')
        ;
        $this->setOrders($orders);
    }
    
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'partialpayment.order.history.pager')
            ->setCollection($this->getOrders());
        $this->setChild('pager', $pager);
        $this->getOrders()->load();
        return $this;
    }
    public function getViewUrl($order)
    {
        return $this->getUrl('*/*/order', array('order_id' => $order->getId()));
    }
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }
}
?>