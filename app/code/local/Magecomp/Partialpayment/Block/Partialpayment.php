<?php

/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Block_Partialpayment extends Mage_Catalog_Block_Product_view
{
    protected function getPartialpaymentData($productid = null)
	{
		
		return Mage::helper('partialpayment')->IsPartialpaymentEnabled($productid);
    }
    
   
}
?>