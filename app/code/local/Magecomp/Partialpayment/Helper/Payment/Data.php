<?php 

/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Helper_Payment_Data extends Mage_Payment_Helper_Data
{
	
	public function getStoreMethods($store=null, $quote=null)
	{
		$methods = parent::getStoreMethods($store, $quote);
		
		$check = Mage::helper('partialpayment')->getStoredDatafor('enable') && $quote->getIsPartialpayment();
		$allowedmethods = explode(',',Mage::helper('partialpayment')->getStoredDatafor('allowedpaymethods'));
		if (!Mage::app()->getStore()->isAdmin() && $check && count($allowedmethods))
		{
			$tmp = array();
			
			foreach ($methods as $method)
			{
				if (!in_array($method->getCode(),$allowedmethods))
				{
					continue;
				}
				$tmp[] = $method;
			}
			$methods = $tmp;
		}
		return $methods;
	}
}