<?php

/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment
**/

class Magecomp_Partialpayment_Helper_Data extends Mage_Core_Helper_Abstract
{

    function getheading()
	{
		return $this->getStoredDatafor('heading');
    }

    function getStoredDatafor($data)
	{
		return Mage::getStoreConfig('partialpayment/general/' . $data);
    }

    function getGlobalStoredDatafor($data)
	{
		return Mage::getStoreConfig('partialpayment/global/' . $data);
    }

    function getTable($table)
    {
        if($table)return Mage::getSingleton('core/resource')->getTableName('partialpayment/' . $table);
       		return;
    }

    public function IsPartialpaymentEnabled($productid = null)
	{

		try
		{
	    	if(Mage::registry('current_product'))
			{
				$productid = Mage::registry('current_product')->getId();
	    	}
	    	if(!$productid)
			{
				return 0;
	    	}
	    	$collection  = $this->getPartialpaymentProductData($productid);
	    	$return = $this->IsEnabled($collection);

	    	return $return;
		}
		catch (Exception $e)
		{
	    	throw $e;
		}
    }

    public function getPartialpaymentProductData($id)
	{
		return Mage::getModel('partialpayment/products')->getCollection()
				->addFieldToFilter('product_id',$id)->getData();
				//->addFieldToFilter('partialpayment_status',1)->getData();
    }

    public function IsEnabled($collection)
	{
		try
		{

	    	$status = $this->getStoredDatafor('enablefor');

			if($status == 0 && count($collection) > 0) // Partialpament for For Specific Products
			{
				if($collection[0]['config_partialpayment_status'] == 0)
				{
			   		return $collection[0]['partialpayment_status'];
				}
				elseif($collection[0]['config_partialpayment_status'] == 1)
				{
		    		return $status;
				}
				else
				{
		    		return 0;
				}
	    	}
			else // Partialpament for All Product and WholeCart
			{
				return $status;
	    	}

		}
		catch (Exception $e)
		{
	    	throw $e;
		}
    }

    public function getPartialpaymentDuration($collection)
	{
		$period = $this->getPartialpaymentData('partialpayment_period' , $collection);
		$frequency = $this->getPartialpaymentData('partialpayment_period_frequency' , $collection);
		$detail = Mage::getModel('partialpayment/period')->toOptionsArray();
		if($period)
		{
	    	return $frequency . " " . $detail[$period];
		}
		return $detail[0];
    }

    public function getPartialpaymentTimeLeft($collection, $startdate , $lastdate = null)
	{

		$period = $this->getPartialpaymentData('partialpayment_period' , $collection);
		$frequency = $this->getPartialpaymentData('partialpayment_period_frequency' , $collection);
		$detail = Mage::getModel('partialpayment/period')->toOptionsArray();

		if($period)
		{
	    	$date = new Zend_Date($startdate);
	    	if($period==1)
			{
				$date->add($frequency, Zend_Date::DAY);
	    	}
	    	elseif($period==2)
			{
				$date->add($frequency, Zend_Date::WEEK);
	    	}
			elseif($period==3)
			{
				$date->add($frequency, Zend_Date::MONTH);
			}
			elseif($period==4)
			{
				$date->add($frequency, Zend_Date::YEAR);
			}
	    	return $date->get();
		}
		return;
    }

    public function getOutstandingamount($order)
	{
		$appcurrency = $order->getBaseCurrencyCode() == $order->getOrderCurrencyCode()?$order->formatPrice($order->getBasePartialpaymentRemaining()): $order->formatPrice($order->getPartialpaymentRemaining()) ." or ". Mage::app()->getLocale()->currency($order->getBaseCurrencyCode())->getSymbol() . $order->getBasePartialpaymentRemaining();

		return $appcurrency;
    }

    public function getOrderProduct($order)
	{
		$items = $order->getAllVisibleItems();
		if(count($items)==1)
		{
			foreach($items as $item)
			{
				return $item;
			}
		}
		return;
    }

    public function getPartialpaymentFee($field,$collection)
	{
		$ret = $this->getPartialpaymentData($field,$collection);
		return $ret;
    }

    public function getPartialpaymentDetails($field , $id = 0)
	{
		try
		{
	    	if($id)
			{
				$collection = $this->getPartialpaymentProductData($id);
				return $this->getPartialpaymentData($field,$collection);
	    	}
		}
		catch (Exception $e)
		{
	    	throw $e;
		}
    }

    public function getPartialpaymentData($field,$collection)
	{
		try
		{
			if($field =='')
			{
				return 0;
			}
			$configfield = 'config_'.$field;
			if($this->IsEnabled($collection) && count($collection) > 0)
			{
				if($collection[0][$configfield] == 1)
				{
					return $this->getGlobalStoredDatafor($field);
				}
				else if($collection[0][$configfield] == 0)
				{
					return $collection[0][$field];
				}
				else
				{
					return 0;
				}

			}
			elseif($this->getStoredDatafor('enablefor'))
			{
				return $this->getGlobalStoredDatafor($field);
			}
			return 0;
		}
		catch (Exception $e)
		{
	    	throw $e;
		}
    }

    public function getApprovedInstallments($_order)
    {
        if($_order)
		{
            $paid_orders = Mage::getModel('sales/order')->getCollection()
                		 ->addFieldToFilter('partialpayment_order',$_order)
						 ->addFieldToFilter('partialpayment_status','approved')
                		 ->getData();
            return count($paid_orders);
        }
        return 0;
    }


    public function getStoreMethods($code,$store = null)
    {
		try
		{
	    	$pamenthelper = Mage::helper('payment');
            $prefix = 'payment/' . $code . '/';
            $model = Mage::getStoreConfig($prefix . 'model', $store);

            $methodInstance = Mage::getModel($model);
            $methodInstance->setStore($store);

            $sortOrder = (int)$methodInstance->getConfigData('sort_order', $store);
            $methodInstance->setSortOrder($sortOrder);
            return $methodInstance;
		}
		catch (Exception $e)
		{
			throw $e;
		}
    }

    public function getPayMethods()
    {
		try
		{
			$payments = Mage::getSingleton('payment/config')->getActiveMethods();
			$methods = array();
			foreach ($payments as $paymentCode=>$paymentModel)
			{
				$paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
				$methods[$paymentCode] = array(
					'label'   => $paymentTitle,
					'value' => $paymentCode
				);
			}
			return $methods;
		}
		catch (Exception $e)
		{
			throw $e;
		}
    }

    public function getPartialpaymentProduct()
    {
		try
		{
			$product = Mage::getModel('catalog/product');
			$pid = $product->getIdBySku('partialpayment_installment');

			if($pid > 0)
			{
				return $pid;
			}
			else
			{
				$product->setSku("partialpayment_installment1");
				$product->setAttributeSetId(4);
				$product->setTypeId('virtual');
				$product->setName('Installment');
				$product->setWebsiteIDs(array(Mage::app()->getStore()->getWebsiteId()));
				$product->setDescription("Please don't delete or update this product.");
				$product->setShortDescription("Please don't delete or update this product.");
				$product->setUrlKey("please_dont_access_this_product");
				$product->setPrice(0.00);
				$product->setWeight(4.0000);
				$product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE);
				$product->setStatus(1);
				$product->setTaxClassId(0); # My default tax class
				$product->setStockData(array(
					'manage_stock' => 0,
					'use_config_manage_stock' => 0
				));
				$product->setCreatedAt(strtotime('now'));

				try
				{
					$product->save();
				}
				catch (Exception $ex)
				{
					//Handle the error
				}
			}
		}
		catch (Exception $e)
		{
			throw $e;
		}
    }

	public function IsWholeCart($data)
	{
		if(Mage::getStoreConfig('partialpayment/general/' . $data) == 2)
			return 1;
		else
			return 0;

	}

	public function getPartialLabels($fieldid)
	{
		return Mage::getStoreConfig('partialpayment/partiallabels/' . $fieldid);
	}
}

