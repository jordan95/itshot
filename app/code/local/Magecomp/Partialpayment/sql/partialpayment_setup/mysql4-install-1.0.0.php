<?php
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('partialpayment/products')};
CREATE TABLE {$this->getTable('partialpayment/products')}(
    `partialpayment_id` INT( 11 ) NOT NULL AUTO_INCREMENT,
    `product_id` INT( 11 ) NOT NULL ,
    `partialpayment_status` TINYINT( 3 )  NOT NULL ,
    `partialpayment_fees` FLOAT( 11 )  NULL ,
    `partialpayment_first` FLOAT( 11 )  NULL ,
    `partialpayment_maxinstallments` INT( 11 )  NULL ,
    `partialpayment_period` INT( 11 )  NULL ,
    `partialpayment_period_frequency` INT( 11 )  NULL ,
    `config_partialpayment_period_frequency` SMALLINT( 1 )  NULL ,
    `config_partialpayment_period` SMALLINT( 1 )  NULL ,
    `config_partialpayment_status` SMALLINT( 1 )  NULL ,
    `config_partialpayment_fees` SMALLINT( 1 )  NULL ,
    `config_partialpayment_first` SMALLINT( 1 )  NULL ,
    `config_partialpayment_maxinstallments` SMALLINT( 1 )  NULL ,
    PRIMARY KEY ( `partialpayment_id` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ALTER TABLE {$this->getTable('sales/quote')}
        ADD `is_partialpayment` TINYINT( 3 ) NULL DEFAULT '0',
        ADD `partialpayment_order` INT( 11 ) NULL DEFAULT '0',
        ADD `partialpayment_fee` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.0',
        ADD `base_partialpayment_fee` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.0',
        ADD `partialpayment_remaining` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.0',
        ADD `base_partialpayment_remaining` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.0';
    ALTER TABLE  {$this->getTable('sales/order')}
        ADD  `is_partialpayment` TINYINT( 3 ) NULL DEFAULT '0',
        ADD `partialpayment_order` INT( 11 ) NULL DEFAULT '0',
        ADD  `partialpayment_fee` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.0',
        ADD  `base_partialpayment_fee` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.0',
        ADD  `partialpayment_remaining` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.0',
        ADD  `base_partialpayment_remaining` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.0';
    ALTER TABLE {$this->getTable('sales/quote_item')}
      ADD `is_partialpayment` TINYINT( 3 ) NULL DEFAULT '0';
    ALTER TABLE  {$this->getTable('sales/quote_address')}
        ADD `is_partialpayment` TINYINT( 3 ) NULL DEFAULT '0',
        ADD  `partialpayment_fee` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.0',
        ADD  `base_partialpayment_fee` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.0',
        ADD  `partialpayment_remaining` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.0',
        ADD  `base_partialpayment_remaining` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.0';

   
");
$statusTable        = $installer->getTable('sales/order_status');
$statusStateTable   = $installer->getTable('sales/order_status_state');
//$statusLabelTable   = $installer->getTable('sales/order_status_label');

$data = array(
    array('status' => 'partialpayment_pending', 'label' => 'Partialpayment Pending'),
    array('status' => 'partialpayment_complete', 'label' => 'Partialpayment Complete')
);
$installer->getConnection()->insertArray($statusTable, array('status', 'label'), $data);

$data = array(
    array('status' => 'partialpayment_pending', 'state' => 'partialpayment_pending', 'is_default' => 1),
    array('status' => 'partialpayment_complete', 'state' => 'partialpayment_complete', 'is_default' => 1)
);
$installer->getConnection()->insertArray($statusStateTable, array('status', 'state', 'is_default'), $data);
$installer->endSetup(); 