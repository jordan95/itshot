<?php 

/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Model_Partialpaymentoption 
{
 	public function toOptionArray()
    {
        return array(
            array('value' => '0','label' => 'For Specific Products'),
            array('value' => '1','label' => 'All Products'),
			array('value' => '2','label' => 'Whole Cart'),
        );
    }
} 