<?php
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Model_Invoice_Partialpaymentremaining extends Mage_Sales_Model_Order_Invoice_Total_Abstract {
    public function collect(Mage_Sales_Model_Order_Invoice $invoice) {
        $order = $invoice->getOrder();
        if ($order->getIsPartialpayment()) {
	    $invoice->setGrandTotal($invoice->getGrandTotal() - $order->getPartialpaymentRemaining());
	    $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() - $order->getBasePartialpaymentRemaining());
        }
        return $this;
    }
    
}
