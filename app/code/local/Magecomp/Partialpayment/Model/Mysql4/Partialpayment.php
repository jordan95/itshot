<?php 
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/
class Magecomp_Partialpayment_Model_Mysql4_Partialpayment extends Mage_Core_Model_Mysql4_Abstract
{

    public function _construct()
    {    
        // Note that the manufacturers_id refers to the key field in your database table.
        $this->_init('partialpayment/partialpayment', 'partialpayment_id');
    }
    
} 