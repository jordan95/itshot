<?php
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Model_Mysql4_Partialpayment_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('partialpayment/partialpayment');
    }
	
	/**
     * Add Filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     * @return Mage_Cms_Model_Mysql4_News_News_Collection
     */
    public function addStoreFilter($store)
    {
        if ($store instanceof Mage_Core_Model_Store) {
            $store = array($store->getId());
        }

        $this->getSelect()->join(
            array('store_table' => $this->getTable('partialpayment_store')),
            'main_table.partialpayment_id = store_table.partialpayment_id',
            array()
        )
        ->where('store_table.store_id in (?)', array(0, $store));

        return $this;
    }
	
	public function prepareSummary()
	{
			$partialpaymentTable = Mage::getSingleton('core/resource')->getTableName('partialpayment');
			$this->setConnection($this->getResource()->getReadConnection());
			$this->getSelect()
				->from(array('main_table'=>$partialpaymentTable),'*')
				->where('status = ?', 1)
				->order('created_time','asc');;
			return $this;
	}
	
	public function getPartialpayment()
	{
			$partialpaymentTable = Mage::getSingleton('core/resource')->getTableName('partialpayment');
			$this->setConnection($this->getResource()->getReadConnection());
			$this->getSelect()
				->from(array('main_table'=>$partialpaymentTable),'*')
				->where('status = ?', 1)
				->order('created_time','asc');
			return $this;
	}
	
	public function getDetail($partialpayment_id)
	{
		$partialpaymentTable = Mage::getSingleton('core/resource')->getTableName('partialpayment');
		$this->setConnection($this->getResource()->getReadConnection());
		$this->getSelect()
			->from(array('main_table'=>$partialpaymentTable),'*')
			->where('partialpayment_id = ?', $partialpayment_id);
		return $this;
	}
	
	public function getPartialpaymentBlock()
	{
			$partialpaymentTable = Mage::getSingleton('core/resource')->getTableName('partialpayment');
			$this->setConnection($this->getResource()->getReadConnection());
			$this->getSelect()
				->from(array('main_table'=>$partialpaymentTable),'*')
				->where('status = ?', 1)
				->order('created_time DESC')
				->limit(5);
			return $this;
	}
	
    public function toOptionArray()
    {
        return $this->_toOptionArray('partialpayment_id', 'title');
    }
	
    public function addPartialpaymentFilter($partialpayment)
    {
        if (is_array($partialpayment)) {
            $condition = $this->getConnection()->quoteInto('main_table.partialpayment_id IN(?)', $partialpayment);
        }
        else {
            $condition = $this->getConnection()->quoteInto('main_table.partialpayment_id=?', $partialpayment);
        }
        return $this->addFilter('partialpayment_id', $condition, 'string');
    }
}