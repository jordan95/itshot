<?php 
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Model_Products extends Mage_Core_Model_Abstract
{
 public function _construct()
 {
  parent::_construct();
  $this->_init('partialpayment/products');
 }

} 