<?php 
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/


class Magecomp_Partialpayment_Model_Period
{
    public function toOptionArray()
    {
        return array(
            array('value' => '0','label' => Mage::helper('partialpayment')->__('No limit')),
            array('value' => '1','label' => Mage::helper('partialpayment')->__('Day')),
            array('value' => '2','label' => Mage::helper('partialpayment')->__('Week')),
            array('value' => '3','label' => Mage::helper('partialpayment')->__('Month')),
            array('value' => '4','label' => Mage::helper('partialpayment')->__('Year'))
        );
    }
    public function toOptionsArray()
    {
        return array(
                '0' => Mage::helper('partialpayment')->__('No limit'),
                '1' => Mage::helper('partialpayment')->__('Day'),
                '2' => Mage::helper('partialpayment')->__('Week'),
                '3' => Mage::helper('partialpayment')->__('Month'),
                '4' => Mage::helper('partialpayment')->__('Year')
        );
    }
}
