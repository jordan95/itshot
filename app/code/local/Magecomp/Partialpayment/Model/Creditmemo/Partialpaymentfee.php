<?php
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Model_Creditmemo_Partialpaymentfee extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract {
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo) {
        $order = $creditmemo->getOrder();
        if ($order->getIsPartialpayment()) {
	    $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $order->getPartialpaymentFee());
	    $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $order->getBasePartialpaymentFee());
        }
        return $this;
    }
    
}
