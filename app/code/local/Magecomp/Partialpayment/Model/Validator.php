<?php 

/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/


class Magecomp_Partialpayment_Model_Validator extends Mage_SalesRule_Model_Validator
{
    
    /**
     * Quote item discount calculation process
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     * @return  Mage_SalesRule_Model_Validator
     */
    public function process(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        if($item->getSku()=='partialpayment_installment'){
            return $this;
        }
        return parent::process($item);
    }

    /**
     * Apply discounts to shipping amount
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_SalesRule_Model_Validator
     */
    public function processShippingAmount(Mage_Sales_Model_Quote_Address $address)
    {
        
        $quote              = $address->getQuote();
        $items = $address->getQuote()->getAllItems();
        
        foreach($items as $item){
            if($item->getSku()=='partialpayment_installment'){
                return $this;
            }
        }
        return parent::processShippingAmount($address);
    }

    public function initTotals($items, Mage_Sales_Model_Quote_Address $address)
    {
        if (!$items) {
            return $this;
        }
        foreach ($items as $item) {
            if($item->getSku()=='partialpayment_installment'){
                return $this;
            }
        }
        return parent::initTotals($items, $address);
    }
}
