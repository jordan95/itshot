<?php
 
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Model_Paymethods
{
 public function toOptionArray()
 {
  return Mage::helper('partialpayment')->getPayMethods();
 }
  
} 