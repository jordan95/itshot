<?php
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Model_Quote_Admintotal2 extends Mage_Sales_Model_Quote_Address_Total_Abstract {
    protected $_code = 'partialpayment_remaining';
       
	public function collect(Mage_Sales_Model_Quote_Address $address)
	{
		parent::collect($address);

		$this->_setAmount(0);
		$this->_setBaseAmount(0);

		$items = $this->_getAddressItems($address);
		if (!count($items)) {
			return $this; //to eliminate the billing address
		}
                if (count($items) > 1) {
			return $this; //to stop if more than one product in cart
		}


		$quote = $address->getQuote();
                if($quote->getIsPartialpayment()){
                    $go = true;$partialpaymentremaining=0;
                    foreach($items as $item){
                        $data = Mage::helper('partialpayment')->getPartialpaymentProductData($item->getProductId());
                        if(!Mage::helper('partialpayment')->IsEnabled($data)){
                            $go = false;
                        }else{
                            $partialpaymentremaining += ($item->getPrice() - ($item->getPrice() * Mage::helper('partialpayment')->getPartialpaymentFee('partialpayment_first',$data)/100))* $item->getQty();
                        }
                    }
                   
                    if($go){
                        $currentcurrency = Mage::app()->getStore()->convertPrice($partialpaymentremaining);
                        $address->setPartialpaymentRemaining($currentcurrency);
                        $address->setBasePartialpaymentRemaining($partialpaymentremaining);
			$quote->setPartialpaymentRemaining($currentcurrency);
			$session = Mage::getSingleton('checkout/session');
                        $quote1 = $session->getQuote();
                        $quote1->setData('partialpayment_remaining',$currentcurrency);
                        $quote1->setData('base_partialpayment_remaining',$partialpaymentremaining);
                        $quote1->save();
			$address->setGrandTotal($address->getGrandTotal() - $address->getPartialpaymentRemaining());
                        $address->setBaseGrandTotal($address->getBaseGrandTotal() - $address->getPartialpaymentRemaining());
                    }
                }
	}

	public function fetch(Mage_Sales_Model_Quote_Address $address)
	{
		$remainfeelabel = Mage::helper('partialpayment')->getPartialLabels('partialremaintext');
		
		$address->addTotal(array(
				'code'=>$this->getCode(),
				'title' => Mage::helper('partialpayment')->__($remainfeelabel),
				'value'=> -($address->getPartialpaymentRemaining())
		));
		return $this;
	}
    
}
