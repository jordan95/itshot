<?php
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Model_Quote_Admintotal extends Mage_Sales_Model_Quote_Address_Total_Abstract {
    protected $_code = 'partialpayment_fee';
       
	public function collect(Mage_Sales_Model_Quote_Address $address)
	{
		parent::collect($address);

		$this->_setAmount(0);
		$this->_setBaseAmount(0);

		$items = $this->_getAddressItems($address);
		if (!count($items)) {
			return $this; //to eliminate the billing address
		}
                if (count($items) > 1) {
			return $this; //to stop if more than one product in cart
		}


		$quote = $address->getQuote();
                if($quote->getIsPartialpayment()){
                    $go = true;$partialpaymentfee = 0;
                    foreach($items as $item){
                        $data = Mage::helper('partialpayment')->getPartialpaymentProductData($item->getProductId());
                        if(!Mage::helper('partialpayment')->IsEnabled($data)){
                            $go = false;
                        }else{
                            $partialpaymentfee += Mage::helper('partialpayment')->getPartialpaymentFee('partialpayment_fees',$data) * $item->getQty();
                        }
                    }
                   
                    if($go){
			$currentcurrency = Mage::app()->getStore()->convertPrice($partialpaymentfee);
                        $session = Mage::getSingleton('checkout/session');
                        $quote1 = $session->getQuote();
                        $quote1->setData('partialpayment_fee',$currentcurrency);
                        $quote1->setData('base_partialpayment_fee',$partialpaymentfee);
                        $quote1->save();
                        $address->setPartialpaymentFee($currentcurrency);
                        $address->setBasePartialpaymentFee($partialpaymentfee);
                        $quote->setPartialpaymentFee($currentcurrency);
                        
                        $address->setGrandTotal($address->getGrandTotal() + $address->getPartialpaymentFee());
                        $address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBasePartialpaymentFee());
                    }
                }
	}

	public function fetch(Mage_Sales_Model_Quote_Address $address)
	{
		$feelabel = Mage::helper('partialpayment')->getPartialLabels('partialfeetext');
		$address->addTotal(array(
				'code'=>$this->getCode(),
				'title' => Mage::helper('partialpayment')->__($feelabel),
				'value'=> $address->getPartialpaymentFee()
		));
		return $this;
	}
    
}
