<?php
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Model_Quote_Partialpaymentremaining extends Mage_Sales_Model_Quote_Address_Total_Abstract 
{
    protected $_code = 'partialpayment_remaining';
       
	public function collect(Mage_Sales_Model_Quote_Address $address)
	{
	    parent::collect($address);
	    try
		{
			if(Mage::helper('partialpayment')->getStoredDatafor('enable'))
			{
				$this->_setAmount(0);
				$this->_setBaseAmount(0);
				$items = $this->_getAddressItems($address);
				if (!count($items)) 
				{
					return $this; //to eliminate the billing address
				}
				$quote = $address->getQuote();
				
				if($quote->getIsPartialpayment())
				{
					$go = true;
					$partialpaymentremaining = 0;
					foreach($items as $item)
					{
						$data = Mage::helper('partialpayment')->getPartialpaymentProductData($item->getProductId());
						if(!Mage::helper('partialpayment')->IsEnabled($data) || !$item->getIsPartialpayment())						{
							//$go = false;
						}
						else
						{
							if($item->getSku() != "partialpayment_installment")
				             {		
							$partialpaymentremaining += ($item->getBasePrice() - ($item->getBasePrice() * Mage::helper('partialpayment')->getPartialpaymentFee('partialpayment_first',$data)/100))* $item->getQty();
							 }
							
						}
					}
					if($go)
					{
						$session = Mage::getSingleton('checkout/session');
						$partialpaymentremaining = round($partialpaymentremaining,2);
						$currentcurrency = Mage::app()->getStore()->convertPrice($partialpaymentremaining);
						$currentcurrency = round($currentcurrency,2);
					   
						$address->setPartialpaymentRemaining($currentcurrency);
						$address->setBasePartialpaymentRemaining($partialpaymentremaining);
						$quote->setPartialpaymentRemaining($currentcurrency);
						$quote->setBasePartialpaymentRemaining($partialpaymentremaining);
						$address->setIsPartialpayment(true);
						$quote->setIsPartialpayment(true);
						$address->setGrandTotal($address->getGrandTotal() - $address->getPartialpaymentRemaining());
						$address->setBaseGrandTotal($address->getBaseGrandTotal() - $address->getBasePartialpaymentRemaining());
					}
		    	}
			}
	    }
	    catch (Exception $e) 
		{
			throw $e;
	    }
	}

	public function fetch(Mage_Sales_Model_Quote_Address $address)
	{
	    try
		{
			$quote = $address->getQuote();
			if($quote->getIsPartialpayment())
			{
		    	$items = $this->_getAddressItems($address);
				if (!count($items)) 
				{
					return $this; //to eliminate the billing address
				}
				$items = $quote->getAllVisibleItems();
				
				/*if (count($items) > 1) 
				{
					return $this; // stop if more than one product in cart
				}*/
				if ($address->getPartialpaymentRemaining() < 1) 
				{
					return $this;
				}
				if(Mage::helper('partialpayment')->getStoredDatafor('enable'))
				{
					$remainfeelabel = Mage::helper('partialpayment')->getPartialLabels('partialremaintext');
					
					$address->addTotal(array(
						'code'=>$this->getCode(),
						'title' => Mage::helper('partialpayment')->__($remainfeelabel),
						'value'=> -($address->getPartialpaymentRemaining())
					));
					return $this;
				}
			}
	    }
	    catch (Exception $e) 
		{
			throw $e;
	    }
	}
}
