<?php
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Model_Quote_Partialpaymentfee extends Mage_Sales_Model_Quote_Address_Total_Abstract 
{
    protected $_code = 'partialpayment_fee';
       
	public function collect(Mage_Sales_Model_Quote_Address $address)
	{
        parent::collect($address);
        try
		{
			if(Mage::helper('partialpayment')->getStoredDatafor('enable'))
			{
				$this->_setAmount(0);
				$this->_setBaseAmount(0);

				$items = $this->_getAddressItems($address);
				
				if (!count($items)) 
				{
					return $this; //to eliminate the billing address
				}
				$quote = $address->getQuote();
						
				//$quote = $address->getQuote();
				if($quote->getIsPartialpayment())
				{
					$go = true;
					$partialpaymentfee = 0;
					foreach($items as $item)
					{
						$data = Mage::helper('partialpayment')->getPartialpaymentProductData($item->getProductId());
						if(!Mage::helper('partialpayment')->IsEnabled($data) || !$item->getIsPartialpayment())						{
							//$go = false;
						}
						else
						{
							/*$partialpaymentfee += Mage::helper('partialpayment')->getPartialpaymentFee('partialpayment_fees',$data) * $item->getQty();*/
							$partialpaymentfee = Mage::helper('partialpayment')->getPartialpaymentFee('partialpayment_fees',$data) * $item->getQty();

						}
					}
		     		
					if($go)
					{
						$session = Mage::getSingleton('checkout/session');
						$partialpaymentfee = round($partialpaymentfee,2);
						$currentcurrency = Mage::app()->getStore()->convertPrice($partialpaymentfee);
						$currentcurrency = round($currentcurrency,2);
						$address->setPartialpaymentFee($currentcurrency);
						$address->setBasePartialpaymentFee($partialpaymentfee);
						$address->setIsPartialpayment(true);
						$quote->setPartialpaymentFee($currentcurrency);
						$quote->setBasePartialpaymentFee($partialpaymentfee);
						$quote->setIsPartialpayment(true);
						$address->setGrandTotal($address->getGrandTotal() + $address->getPartialpaymentFee());
						$address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBasePartialpaymentFee());
					}
		    	}
			}
        }
	    catch (Exception $e) 
		{
			throw $e;
	    }
    }

	public function fetch(Mage_Sales_Model_Quote_Address $address)
	{
	    try
		{
			$quote = $address->getQuote();
		
			if($quote->getIsPartialpayment())
			{
		    	$items = $this->_getAddressItems($address);
		   
				if (!count($items)) 
				{
					return $this; //to eliminate the billing address
				}
				$items = $quote->getAllVisibleItems();
				
				/*if (count($items) > 1) 
				{
					return $this; // stop if more than one product in cart
				}*/
				
				if ($address->getPartialpaymentFee() < 1) 
				{
					return $this;
				}
				if(Mage::helper('partialpayment')->getStoredDatafor('enable'))
				{
					$feelabel = Mage::helper('partialpayment')->getPartialLabels('partialfeetext');
					
					$address->addTotal(array(
						'code'=>$this->getCode(),
						'title' => Mage::helper('partialpayment')->__($feelabel),
						'value'=> $address->getPartialpaymentFee()
					));
					return $this;
				}
			}
	    }
	    catch (Exception $e) 
		{
			throw $e;
	    }
	}
    
}
