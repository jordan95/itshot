<?php
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/
class Magecomp_Partialpayment_Model_Observer_Salesobserver
{
	public function CreateOrderProcessDataBefore($observer)
	{
		try
		{
			
			$isapplypartialpayment = 0;
			
			$postData = Mage::app()->getRequest()->getPost();
			
			if(!isset($postData['item'])) 
			{
				return;
			}
		
			if(isset($postData['update_items']) && $postData['update_items']) 
			{
				
				foreach ($postData['item'] as $item) 
				{
					$isapplypartialpayment = $item['partialpaymentapply'];		
				}
				
				Mage::getSingleton('core/session')->setMcpp($isapplypartialpayment);
				
				
			
				if($isapplypartialpayment == 1)
				{
					$quote = Mage::getSingleton('adminhtml/session_quote')->getQuote();
					
					$quote->setData('is_partialpayment',1);
					$quote->setData('partialpayment_order',0);
					$items = $quote->getAllVisibleItems();
					
					$partialpaymentfee = 0;
					$partialpaymentremaining = 0;
				
					if(count($items))
					{
						foreach($items as $item)
						{
									
							$data = Mage::helper('partialpayment')->getPartialpaymentProductData($item->getProductId());
							$item->setData('is_partialpayment',1);
							
							$partialpaymentfee = Mage::helper('partialpayment')->getPartialpaymentFee('partialpayment_fees',$data) * $item->getQty();
							
							$partialpaymentremaining += ($item->getBasePrice() - ($item->getBasePrice() * Mage::helper('partialpayment')->getPartialpaymentFee('partialpayment_first',$data)/100))* $item->getQty();
							
						}
					
						$quote->setData('partialpayment_fee',$partialpaymentfee);
						$quote->setData('base_partialpayment_fee',$partialpaymentfee);
						$quote->setData('partialpayment_remaining',$partialpaymentremaining);
						$quote->setData('base_partialpayment_remaining',$partialpaymentremaining);
						$quote->setGrandTotal($quote->getGrandTotal() + $quote->getPartialpaymentFee());
						$quote->setBaseGrandTotal($quote->getBaseGrandTotal() + $quote->getBasePartialpaymentFee());
		    		}
		    		$quote->save();
					
				}
				else
				{
					$quote->setData('is_partialpayment',0);
					$quote->setData('partialpayment_order',0);
					$quote->setData('partialpayment_fee',0.0);
					$quote->setData('base_partialpayment_fee',0.0);
					$quote->setData('partialpayment_remaining',0.0);
					$quote->setData('base_partialpayment_remaining',0.0);
					
					$items = $quote->getAllVisibleItems();
					if(count($items))
					{
						foreach($items as $item)
						{			
							$item->setData('is_partialpayment',0);
						}
					}
					$quote->save();
				}	
			}	
		}
		catch(Exception $e)
		{
			Mage::log($e->getMessage());
			Mage::getSingleton('core/session')->setMcpp(0);	
		}
		
	}
}