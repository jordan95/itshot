<?php
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Model_Observer_Product extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the partialpayment_id refers to the key field in your database table.
        $this->_init('partialpayment/product', 'partialpayment_id');
    }
	
	/**
	 * Inject one tab into the product edit page in the Magento admin
	 *
	 * @param Varien_Event_Observer $observer
	 */
	public function injectTabs(Varien_Event_Observer $observer)
	{
		
		$block = $observer->getEvent()->getBlock();
		
		if ($block instanceof Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs) 
		{
			$adminlabel = Mage::helper('partialpayment')->getPartialLabels('admintabtext');
			
			if ($this->_getRequest()->getActionName() == 'edit' || $this->_getRequest()->getParam('type')) {
				$block->addTab('partialpayment', array(
				    'before'=>'inventory',
					'label'     => Mage::helper('partialpayment')->__($adminlabel),
					'content'   => $block->getLayout()->createBlock('adminhtml/template', 'partialpayment-tab-content', array('template' => 'partialpayment/content.phtml'))->toHtml(),
				));
			}
		}
	}

	/**
	 * This method will run when the product is saved
	 * Use this function to update the product model and save
	 *
	 * @param Varien_Event_Observer $observer
	 */
	public function saveTabData(Varien_Event_Observer $observer)
	{
		
	    if ($post = $this->_getRequest()->getPost()) {
		try {
		    // Load the current product model
		    if($product = Mage::registry('product')){
		       // print_r($post);exit;
			if($post["partialpayment_id"] <= 0){
			    $productsArray = array();
			    $productsArray['product_id'] = $product["entity_id"];
			    $productsArray['partialpayment_status'] = isset($post["partialpayment_status"])?$post["partialpayment_status"]:$post["default_partialpayment_status"];
			    $productsArray['partialpayment_fees'] = isset($post["partialpayment_fees"])?$post["partialpayment_fees"]:$post["default_partialpayment_fees"];
			    $productsArray['partialpayment_first'] = isset($post["partialpayment_first"])?$post["partialpayment_first"]:$post["default_partialpayment_first"];
			    $productsArray['partialpayment_maxinstallments'] = isset($post["partialpayment_maxinstallments"])?$post["partialpayment_maxinstallments"]:$post["default_partialpayment_maxinstallments"];
			    $productsArray['config_partialpayment_status'] = isset($post["config_partialpayment_status"])?$post["config_partialpayment_status"]:0;
			    $productsArray['config_partialpayment_fees'] = isset($post["config_partialpayment_fees"])?$post["config_partialpayment_fees"]:0;
			    $productsArray['config_partialpayment_first'] = isset($post["config_partialpayment_first"])?$post["config_partialpayment_first"]:0;
			    $productsArray['config_partialpayment_maxinstallments'] = isset($post["config_partialpayment_maxinstallments"])?$post["config_partialpayment_maxinstallments"]:0;
			    $productsArray['partialpayment_period'] = isset($post["partialpayment_period"])?$post["partialpayment_period"]:$post["default_partialpayment_period"];
			    $productsArray['config_partialpayment_period'] = isset($post["config_partialpayment_period"])?$post["config_partialpayment_period"]:0;
			    $productsArray['partialpayment_period_frequency'] = isset($post["partialpayment_period_frequency"])?$post["partialpayment_period_frequency"]:$post["default_partialpayment_period_frequency"];
			    $productsArray['config_partialpayment_period_frequency'] = isset($post["config_partialpayment_period_frequency"])?$post["config_partialpayment_period_frequency"]:0;
			    
			    $this->_getWriteAdapter()->insert($this->getTable('partialpayment/products'), $productsArray);
			}else{
			    $this->_getWriteAdapter()->update($this->getTable('partialpayment/products'),
				    array(
					    'partialpayment_status' => isset($post["partialpayment_status"])?$post["partialpayment_status"]:$post["default_partialpayment_status"],
					    'partialpayment_fees' =>isset($post["partialpayment_fees"])?$post["partialpayment_fees"]:$post["default_partialpayment_fees"],
					    'partialpayment_first'=>isset($post["partialpayment_first"])?$post["partialpayment_first"]:$post["default_partialpayment_first"],
					    'partialpayment_maxinstallments'=>isset($post["partialpayment_maxinstallments"])?$post["partialpayment_maxinstallments"]:$post["default_partialpayment_maxinstallments"],
					    'config_partialpayment_status' =>isset($post["config_partialpayment_status"])?$post["config_partialpayment_status"]:0,
					    'config_partialpayment_fees' =>isset($post["config_partialpayment_fees"])?$post["config_partialpayment_fees"]:0,
					    'config_partialpayment_first'=>isset($post["config_partialpayment_first"])?$post["config_partialpayment_first"]:0,
					    'config_partialpayment_maxinstallments'=>isset($post["config_partialpayment_maxinstallments"])?$post["config_partialpayment_maxinstallments"]:0,
					    'partialpayment_period'=>isset($post["partialpayment_period"])?$post["partialpayment_period"]:$post["default_partialpayment_period"],
					    'config_partialpayment_period'=>isset($post["config_partialpayment_period"])?$post["config_partialpayment_period"]:0,
					    'partialpayment_period_frequency'=>isset($post["partialpayment_period_frequency"])?$post["partialpayment_period_frequency"]:$post["default_partialpayment_period_frequency"],
					    'config_partialpayment_period_frequency'=>isset($post["config_partialpayment_period_frequency"])?$post["config_partialpayment_period_frequency"]:0
					),
				    array("partialpayment_id =?" => $post["partialpayment_id"])
				);
			}
		      
			    
		    }
			
						
		}
		 catch (Exception $e) {
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		}
	    }
	}
	
	/**
	 * Shortcut to getRequest
	 */
	protected function _getRequest()
	{
		
		return Mage::app()->getRequest();
	}
}
