<?php
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment
**/
class Magecomp_Partialpayment_Model_Observer_Observer extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        // Note that the partialpayment_id refers to the key field in your database table.
        $this->_init('partialpayment/partialpayment', 'partialpayment_id');
    }

    private function isEnabledFully()
    {
        return Mage::getStoreConfigFlag('partialpayment/general/enablecheckout');
    }

    public function OnRemoveCart($observer)
    {
        if (!$this->isEnabledFully()) {
            return false;
        }

		try
		{
	    	if(Mage::helper('partialpayment')->getStoredDatafor('enable'))
			{
				$quote = $this->getQuote();
				if($quote->getIsPartialpayment())
				{
					$quote->setData('is_partialpayment',0);
					$quote->setData('partialpayment_order',0);
					$quote->setData('partialpayment_fee',0.0);
					$quote->setData('base_partialpayment_fee',0.0);
					$quote->setData('partialpayment_remaining',0.0);
					$quote->setData('base_partialpayment_remaining',0.0);
					$items = $quote->getAllVisibleItems();
					if(count($items))
					{
						foreach($items as $item)
						{
							$item->setData('is_partialpayment',0);
						}
					}
				}
				else if($quote->getPartialpaymentOrder() > 0)
				{
					Mage::getSingleton('checkout/session')->unsPartialpaymentProductName();
				}
				$quote->setData('partialpayment_order',0);
				$quote->save();
			}
		}
		catch (Exception $e)
		{
	    	throw $e;
		}
    }

    public function OnConfigureCart($observer)
    {
        if (!$this->isEnabledFully()) {
            return false;
        }

		try
		{
	    	if(Mage::helper('partialpayment')->getStoredDatafor('enable'))
			{
				$request = $this->_getRequest()->getPost();
				$quote = $this->getQuote();
				$iswithpartialpayment = isset($request['options']['partialpayment']) && $request['options']['partialpayment']==1;
				if($iswithpartialpayment)
				{

		    		$quote->setData('is_partialpayment',1);
		    		$quote->setData('partialpayment_order',0);
		    		$items = $quote->getAllVisibleItems();
		    		if(count($items))
					{
						foreach($items as $item)
						{
			    			$item->setData('is_partialpayment',1);
						}
		    		}
					$quote->save();
				}
				else
				{
		    		$quote->setData('is_partialpayment',0);
					$quote->setData('partialpayment_order',0);
					$quote->setData('partialpayment_fee',0.0);
					$quote->setData('base_partialpayment_fee',0.0);
					$quote->setData('partialpayment_remaining',0.0);
					$quote->setData('base_partialpayment_remaining',0.0);
					$items = $quote->getAllVisibleItems();
		    		if(count($items))
					{
						foreach($items as $item)
						{
			    			$item->setData('is_partialpayment',0);
						}
		    		}
					$quote->save();
				}
	    	}
		}
		catch (Exception $e) {
			throw $e;
		}

    }

    public function OnUpdateCart($observer)
    {
        if (!$this->isEnabledFully()) {
            return false;
        }

		try
		{
	    	if(Mage::helper('partialpayment')->getStoredDatafor('enable'))
			{
				$data = $this->_getRequest()->getPost('cart');
				$quote = $this->getQuote();
				$items = $quote->getAllVisibleItems();

				if($quote->getPartialpaymentOrder() > 0)
				{
		    		if(count($items))
					{
						foreach($items as $item)
						{
			    			if($data[$item->getId()]['qty']>1)
							{
								$item->setQty(1);
								Mage::getSingleton('checkout/session')->addError(Mage::helper('partialpayment')->__('You Should not update installment from cart, this may charge you incorrect amount.'));
			    			}
						}
		    		}
		    		$quote->save();
				}
	    	}
		}
		catch (Exception $e)
		{
	    	throw $e;
		}
    }


    public function getQuote()
    {
		$session = Mage::getSingleton('checkout/session');
		return $session->getQuote();
    }

    public function validateLoginStatus()
    {

		$request = $this->_getRequest()->getParams();
		$iswithpartialpayment = isset($request['options']['partialpayment']) && $request['options']['partialpayment']==1;
		$session = Mage::getSingleton('checkout/session');
		$customersession = Mage::getSingleton('customer/session');
		if(!$customersession->isLoggedIn() && $iswithpartialpayment ==1)
		{
	    	Mage::throwException(Mage::helper('partialpayment')->__('Please login or register before purchasing the product on Partialpayment.'));
	    	$session->setRedirectUrl(Mage::getUrl('customer/login'));
	    	if($session->getUseNotice() === null)
			{
		    	$session->setUseNotice(true);
			}
			return;
		}
    }

    public function AddToCartAfter($observer)
    {
        if (!$this->isEnabledFully()) {
            return false;
        }

		try
		{
	    	if(Mage::helper('partialpayment')->getStoredDatafor('enable'))
			{
			 	$request = $this->_getRequest()->getParams();
	    		 $session = Mage::getSingleton('checkout/session');
				$customersession = Mage::getSingleton('customer/session');
				$quote = $this->getQuote();
				$items = $quote->getAllVisibleItems();


				/*if($quote->getPartialpaymentOrder() > 0 && count($items)>1)
				{
		    		Mage::throwException(Mage::helper('partialpayment')->__('Please finish with the selected Installment First.'));
		    		return;
				}*/

				if(!isset($request['product']) && Mage::getSingleton('checkout/session')->getDoNotCheckout())
				{
		    		Mage::getSingleton('checkout/cart')->removeItem(Mage::getSingleton('checkout/session')->getDoNotCheckout())->save();
		    		Mage::getSingleton('checkout/session')->unsDoNotCheckout();
		    		Mage::throwException(Mage::helper('partialpayment')->__('Sorry, you cannot proceed.'));
		    		return;
				}



				$partialpayment_status = isset($request['product'])?Mage::helper('partialpayment')->IsPartialpaymentEnabled($request['product']):false;

				/*$iswithpartialpayment = isset($request['options']['partialpayment']) && $request['options']['partialpayment']==1;*/

				$iswithpartialpayment = isset($request['options']['partialpayment'])?$request['options']['partialpayment']:0;

				$installment = isset($request['options']['partialpayment_order']) && $request['options']['partialpayment_order']>0;



				if($partialpayment_status != 0 && !isset($request['options']['partialpayment']) && !$installment)
				{
		    		$product = Mage::getModel('catalog/product')->load($request['product']);
		    		$session->setRedirectUrl($product->getProductUrl());
		    		if($session->getUseNotice() === null)
					{
						$session->setUseNotice(true);
		    		}


					if(!Mage::helper('partialpayment')->getStoredDatafor('enableguestch') && !$customersession->isLoggedIn())
					{
						Mage::getSingleton('customer/session')->addNotice(Mage::helper('partialpayment')->__('Registered customers can only purchase the product on Partialpayment.'));
		    		}

		    		Mage::throwException(Mage::helper('partialpayment')->__('Do you want to checkout with Partialpayment.'));
		    		return;
				}

				if(!Mage::helper('partialpayment')->getStoredDatafor('enableguestch') && !$customersession->isLoggedIn() && $iswithpartialpayment == 1)
				{
					$product = Mage::getModel('catalog/product')->load($request['product']);
					$customersession->setBeforeAuthUrl(Mage::getUrl('checkout/cart/add',array('_query'=>http_build_query($request))));
					$session->setRedirectUrl(Mage::getUrl('customer/account/login'));
					if($session->getUseNotice() === null)
					{
						$session->setUseNotice(true);
					}
					Mage::throwException(Mage::helper('partialpayment')->__('Only registered customers can buy on partialpayment.'));
					return;exit;
				}

				$bundle = isset($request['bundle_option']);
				$configurable = isset($request['super_attribute']);


				$quote->setData('is_partialpayment',1);
		    	$items = $quote->getAllVisibleItems();
				$partialpaymentfee = 0;
				$partialpaymentremaining = 0;

				foreach($items as $item)
				{
			    	$data = Mage::helper('partialpayment')->getPartialpaymentProductData($item->getProductId());

					//$item->setData('is_partialpayment',1);

				   if($partialpayment_status == 2) // Partial Payment For WholeCart
				   {
						$item->setData('is_partialpayment',1);
				   }
				   else
				   {
						if(sizeof($data) == 1 && $iswithpartialpayment == 1)
						{
						 	$item->setData('is_partialpayment',sizeof($data));
						}
				   }

			    	$partialpaymentfee += Mage::helper('partialpayment')->getPartialpaymentFee('partialpayment_fees',$data) * $item->getQty();
			    	$partialpaymentremaining += ($item->getPrice() - ($item->getPrice() * Mage::helper('partialpayment')->getPartialpaymentFee('partialpayment_first',$data)/100))* $item->getQty();
				}

				$quote->setData('partialpayment_fee',$partialpaymentfee);
				$quote->setData('base_partialpayment_fee',$partialpaymentfee);
				$quote->setData('partialpayment_remaining',$partialpaymentremaining);
				$quote->setData('base_partialpayment_remaining',$partialpaymentremaining);

				$quote->save();

				if($installment) //elseif($installment)
				{
		    		$quote->setData('partialpayment_order',$request['options']['partialpayment_order']);
		    		$quote->save();
				}

		  }
		}
		catch (Exception $e)
		{
	    	throw $e;
		}
    }

	protected function _getRequest()
	{
		return Mage::app()->getRequest();
	}

	public function modifywithPartialpayment($event)
	{
	    try
		{
			$feelabel = Mage::helper('partialpayment')->getPartialLabels('partialfeetext');
			$remainfeelabel = Mage::helper('partialpayment')->getPartialLabels('partialremaintext');

			$version = str_replace('.','',Mage::getVersion());
			if(Mage::helper('partialpayment')->getStoredDatafor('enable') && $version >= 1420)
			{
		    	$paypalcart = $event->getPaypalCart();
		    	if($paypalcart->getSalesEntity()->getIsPartialpayment() && $paypalcart->getSalesEntity()->getPayment()->getMethod() =='paypal_standard')
				{
					$paypalcart->addItem(Mage::helper('paypal')->__($feelabel), 1, 1.00 * $paypalcart->getSalesEntity()->getPartialpaymentFee(),'');

					$paypalcart->updateTotal(Mage_Paypal_Model_Cart::TOTAL_DISCOUNT,$paypalcart->getSalesEntity()->getPartialpaymentRemaining());

		    	}
		    	elseif($paypalcart->getSalesEntity()->getIsPartialpayment() && $paypalcart->getSalesEntity()->getPayment()->getMethod() !='paypal_standard')
				{
					$paypalcart->addItem(Mage::helper('paypal')->__($feelabel), 1, 1.00 * $paypalcart->getSalesEntity()->getPartialpaymentFee(),'');
					$paypalcart->addItem(Mage::helper('paypal')->__($remainfeelabel), 1, -1.00 * $paypalcart->getSalesEntity()->getPartialpaymentRemaining(),'');
		    	}
			}
	    }
	    catch (Exception $e)
		{
			throw $e;
	    }
	}

	public function updatePrice($observer)
	{
        if (!$this->isEnabledFully()) {
            return false;
        }

	    try
		{
			$params = $this->_getRequest()->getParams();
			$event = $observer->getEvent();
			$quote_item = $event->getQuoteItem();
			Mage::getSingleton('checkout/session')->setDoNotCheckout(false);
			if($quote_item->getSku() =='partialpayment_installment')
			{
		    	$new_price = isset($params['amount'])?$params['amount']:0;
		    	if($new_price<=0)
				{
					Mage::getSingleton('checkout/session')->setDoNotCheckout($quote_item->getProductId());
					return;
		    	}
		    	$quote_item->setOriginalCustomPrice($new_price);
			}
	    }
	    catch (Exception $e)
		{
			throw $e;
	    }
	}

	public function updateOrder($observer)
	{
        if (!$this->isEnabledFully()) {
            return false;
        }

	    try
		{
			$event = $observer->getEvent();
			$quote = $event->getQuote();
			$order = $event->getOrder();
			if($quote->getPartialpaymentOrder() && $quote->getPartialpaymentOrder()>0)
			{
				$order->setPartialpaymentOrder($quote->getPartialpaymentOrder());
			}
	    }
	    catch (Exception $e)
		{
			throw $e;
	    }
	}

	public function UpdateInstallments($observer)
	{
	    try
		{
			$order = new Mage_Sales_Model_Order();
			$incrementId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
			$order->loadByIncrementId($incrementId);
			if($order->getPartialpaymentOrder() && $order->getPartialpaymentOrder()>0)
			{
				foreach($order->getAllVisibleItems() as $item)
				{
					if($item->getSku()=='partialpayment_installment' && Mage::getSingleton('checkout/session')->getPartialpaymentProductName())
					{
						$item->setName(Mage::getSingleton('checkout/session')->getPartialpaymentProductName());
					}
				}
				$order->save();
			}
	    }
	    catch (Exception $e)
		{
			throw $e;
	    }
	}

	public function CartSavebefore(Varien_Event_Observer $observer)
	{
	    $cart = $observer->getCart();
	    $items = $cart->getItems();

	    foreach($items as $item)
		{
			if($item->getSku()=='partialpayment_installment' && Mage::getSingleton('checkout/session')->getPartialpaymentProductName())
			{
		    	$item->setName(Mage::getSingleton('checkout/session')->getPartialpaymentProductName());
			}
	    }
	}

	public function validateInstallment($observer)
	{
        if (!$this->isEnabledFully()) {
            return false;
        }

	    $quoteItem = $observer->getEvent()->getItem();
	    /* @var $quoteItem Mage_Sales_Model_Quote_Item */
	    if ($quoteItem->getSku() == 'partialpayment_installment')
		{

			if($quoteItem->getQty()>1 && $quoteItem->getPrice()>0)
			{

		    	$quoteItem->setQty(1);
		    	return $quoteItem;
			}
			else if(($quoteItem->getQty()>=1 && $quoteItem->getPrice()<=0) || $quoteItem->getName() == 'Installment')
			{

		    	Mage::getSingleton('checkout/cart')->removeItem($quoteItem->getId())->save();
			}
	    }
	}

	public function updateInstallment($observer)
	{
        if (!$this->isEnabledFully()) {
            return false;
        }

	    $product = $observer->getProduct();
	    $quoteItem = $observer->getQuoteItem();

	    if ($quoteItem->getSku() == 'partialpayment_installment')
		{

			if(Mage::getSingleton('checkout/session')->getPartialpaymentProductName())
			{

		    	$product->setName(Mage::getSingleton('checkout/session')->getPartialpaymentProductName());
		    	$quoteItem->setName(Mage::getSingleton('checkout/session')->getPartialpaymentProductName());
			}
	    }
	}

	public function creditmemoSaveAfter($observer)
	{
	    $creditmemo = $observer->getEvent()->getCreditmemo();
		if($creditmemo->getId())
		{
			$creditmemo = Mage::getModel('sales/order_creditmemo')->load($creditmemo->getId());
			$order = $creditmemo->getOrder();
			$items = $creditmemo->getAllItems();
			if($order->getIsPartialpayment())
			{
				$order->setBaseGrandTotal($order->getBaseGrandTotal() - $order->getBasePartialpaymentFee());
				$order->setGrandTotal($order->getGrandTotal() - $order->getPartialpaymentFee());
				$order->save();
			}
			elseif(count($items)==1)
			{
		    	foreach($items as $item)
				{
					if($item->getSku() =='partialpayment_installment')
					{
			    		$remaining = 0;$baseremaining=0;$amount = 0;$baseamount=0;
			    		//Mage::getModel('sales/order')->load($creditmemo->getOrderId());
			    		$total = $creditmemo->getGrandTotal();
						if($order->getPartialpaymentOrder() >0)
						{
							$main_order = Mage::getModel('sales/order')->load($order->getPartialpaymentOrder());
							$rate = $main_order->getStoreToOrderRate();
							if($order->getOrderCurrencyCode() == $main_order->getOrderCurrencyCode() )
							{
								$amount = round($total,2);
								//$baseamount = round(($amount/$rate),2);
							}
							elseif($order->getOrderCurrencyCode() != $main_order->getOrderCurrencyCode())
							{
								$amount = round(($rate * $total),2);
								//$baseamount = round($amount,2);
							}
							if($order->getOrderCurrencyCode() == $main_order->getBaseCurrencyCode() )
							{
								//$amount = round($amount,2);
							    $baseamount = round($total,2);
							}
							elseif($order->getOrderCurrencyCode()!= $main_order->getBaseCurrencyCode())
							{
							   // $amount = round(($rate * $amount),2);
								 $baseamount = round(($total/$rate),2);
							}
							$remaining =$main_order->getPartialpaymentRemaining() + $amount;
							$baseremaining = $main_order->getBasePartialpaymentRemaining() + $baseamount;
							$remaining = $remaining < 0?0.00:$remaining;
							$baseremaining = $baseremaining < 0?0.00:$baseremaining;
							$main_order->setBasePartialpaymentRemaining($baseremaining);$main_order->setPartialpaymentRemaining($remaining);
							$main_order->setBaseGrandTotal($main_order->getBaseGrandTotal() - $baseamount);$main_order->setGrandTotal($main_order->getGrandTotal() - $amount);
							$main_order->save();
			    		}
					}
		    	}
			}
	    }
	}

	public function invoiceSaveAfter($observer)
	{
	    $invoice = $observer->getEvent()->getInvoice();
	    if($invoice->getId())
		{
			$invoice = Mage::getModel('sales/order_invoice')->load($invoice->getId());
			$items = $invoice->getAllItems();
			if(count($items)==1)
			{
		    	foreach($items as $item)
				{
					if($item->getSku() =='partialpayment_installment')
					{
			    		$remaining = 0;$baseremaining=0;
						$amount = 0;$baseamount=0;
						$order = Mage::getModel('sales/order')->load($invoice->getOrderId());
						$total = $invoice->getGrandTotal();
						if($order->getPartialpaymentOrder() >0)
						{
							$main_order = Mage::getModel('sales/order')->load($order->getPartialpaymentOrder());
							$rate = $main_order->getStoreToOrderRate();
							if($order->getOrderCurrencyCode() == $main_order->getOrderCurrencyCode() )
							{
								$amount = round($total,2);
								//$baseamount = round(($amount/$rate),2);
							}
							elseif($order->getOrderCurrencyCode() != $main_order->getOrderCurrencyCode())
							{
								$amount = round(($rate * $total),2);
								//$baseamount = round($amount,2);
							}
							if($order->getOrderCurrencyCode() == $main_order->getBaseCurrencyCode() )
							{
								//$amount = round($amount,2);
						   		$baseamount = round($total,2);
							}
							elseif($order->getOrderCurrencyCode()!= $main_order->getBaseCurrencyCode())
							{
						   		// $amount = round(($rate * $amount),2);
								$baseamount = round(($total/$rate),2);
							}
							$remaining =$main_order->getPartialpaymentRemaining() - $amount;
							$baseremaining = $main_order->getBasePartialpaymentRemaining() - $baseamount;
							$remaining = $remaining < 0?0.00:$remaining;
							$baseremaining = $baseremaining < 0?0.00:$baseremaining;
							$main_order->setBasePartialpaymentRemaining($baseremaining);$main_order->setPartialpaymentRemaining($remaining);
							$main_order->setBaseGrandTotal($baseamount + $main_order->getBaseGrandTotal());$main_order->setGrandTotal($amount + $main_order->getGrandTotal());
							$main_order->save();
			    		}
					}
		    	}
			}
	    }
	}

	public function validateStatus($observer)
	{
	    $order = $observer->getEvent()->getOrder();
	    if($order->getIsPartialpayment())
		{
			$remaining = $order->getPartialpaymentRemaining();
			if($remaining > 0 && $order->getStatus()=='complete')
			{
		    	$order->setStatus('partialpayment_pending');
		    	$order->save();
			}
	    }
	}
}