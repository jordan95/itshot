<?php 

/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/

class Magecomp_Partialpayment_Model_Partialpayment extends Mage_Core_Model_Abstract
{
 public function _construct()
    {
        parent::_construct();
        $this->_init('partialpayment/partialpayment');
    }
	
   
    /**
     * Retrieve related products
     *
     * @return array
     */
    public function getPartialpaymentRelatedProducts($partialpaymentId)
    {
	$read = Mage::getSingleton('core/resource')->getConnection('core_read');
	$select = $read->select()
            ->from(Mage::helper('partialpayment')->getFMTable('products'))
            ->where('attribute_id = ?', $partialpaymentId);

        if ($datas = $read->fetchAll($select)) {
            $locationArray = array();
            foreach ($datas as $row) {
                $locationArray[] = $row['products_id'];
            }
	  
         return $locationArray;
        }
	return;

    }
	
} 