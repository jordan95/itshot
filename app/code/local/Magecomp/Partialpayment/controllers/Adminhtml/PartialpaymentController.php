<?php
/**
 * Magento Partialpayment Extension
 *
 * @category   Magecomp
 * @package    Magecomp_Partialpayment
 * @author     Magecomp Partialpayment 
**/
class Magecomp_Partialpayment_Adminhtml_PartialpaymentController extends Mage_Adminhtml_Controller_action
{

     protected function _isAllowed()
    {
        return true;
    }
	protected function _initAction() {
		$this->_title($this->__('Partialpayment'))->_title($this->__('Orders'));
		$this->loadLayout()
			->_setActiveMenu('partialpayment/partialpayment')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Partialpayment Manager'), Mage::helper('adminhtml')->__('Partialpayment Manager'));
		$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}
	
	
	public function viewAction() {
		
		
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('sales/order')->load($id);
		
		if ($model->getEntityId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			Mage::register('partialpayment_data', $model);
			Mage::register('current_order', $model);
			Mage::register('sales_order', $model);
			
			$this->loadLayout();
			$this->_setActiveMenu('magecomp');
			$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
			$this->_title($this->__('Partialpayment'))->_title($this->__('Orders'))->_title($this->__('#') . $model->getIncrementId());
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Partialpayment Manager'), Mage::helper('adminhtml')->__('Partialpayment Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Partialpayment Partialpayment'), Mage::helper('adminhtml')->__('Partialpayment Partialpayment'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__('Partialpayment Order does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	//public function newAction() {
	//	$this->_forward('view');
	//}
 
	public function partialpaymentAction() {
		$orderdetail = $this->getRequest()->getPost();
		
		if($orderdetail && $orderdetail['payKey']!=''){
		    $return = Mage::getModel('partialpayment/partialpayment')->requestPartialpayment($orderdetail);
		    //print_r($return);exit;
		    if(strtolower($return['responseEnvelope.ack']) != "success"){
			$result['success'] = false;
			$result['error'] = true;
			$result['messages'] = Mage::helper('partialpayment')->__($return['error(0).message']);
			$this->getResponse()->setBody(Zend_Json::encode($result));
			return;
		    }else{
			$partialpaymentmessages = Mage::helper('partialpayment')->getPartialpaymentResponseMessage();
			$result['success'] = true;
			$result['error'] = false;
			$result['messages'] = Mage::helper('partialpayment')->__($partialpaymentmessages[$return['partialpaymentInfoList.partialpaymentInfo(0).partialpaymentStatus']]);
			$this->getResponse()->setBody(Zend_Json::encode($result));
			return;
		    }
		    
		}
		$result['success'] = false;
		$result['error'] = true;
		$result['messages'] = Mage::helper('partialpayment')->__('This order wasn\'t placed using Adaptive Paypal');
		$this->getResponse()->setBody(Zend_Json::encode($result));
		return;
	}
 
  
    public function exportCsvAction()
    {
        $fileName   = 'partialpayment.csv';
        $content    = $this->getLayout()->createBlock('partialpayment/adminhtml_partialpayment_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'partialpayment.xml';
        $content    = $this->getLayout()->createBlock('partialpayment/adminhtml_partialpayment_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
	
	
   
    public function gridAction()
	{
		 
	    $this->getResponse()->setBody(
            $this->getLayout()->createBlock('partialpayment/adminhtml_partialpayment_grid')->toHtml()
        );
	
	}

	public function adminpayinstallmentAction()
	{
		try
		{
			$data = $this->getRequest()->getPost();
			
			$order = Mage::getModel('sales/order')->load($data['order']);
			
			
			$currentcurrency = Mage::app()->getStore()->getCurrentCurrencyCode();
			
			$tobepaid = $order->getBaseCurrencyCode() == $currentcurrency?$order->getBasePartialpaymentRemaining():$order->getPartialpaymentRemaining();
			
			if($tobepaid < $data['amount'])
			{
				echo Mage::helper('partialpayment')->__('Your entered amount is exceeding the outstanding amount.');
				return;	
			}
			
			if(isset($data['remaining_installments']) && $data['remaining_installments']<2 && $tobepaid > $data['amount'])
			{
				echo Mage::helper('partialpayment')->__('You have reached the maximum installments limit, please pay full outstanding amount in %s now.',$currentcurrency);
				return;
			}
			
			/* Programetically Create Order (Start) */
			Mage::getSingleton('adminhtml/session_quote')->clear();
			
			$quote = Mage::getModel('sales/quote')->setStoreId($order->getStoreId());
			
			if($data['custid'] != '' && $data['custid'] > 0)
			{
				$customer = Mage::getModel('customer/customer')->load($data['custid']);
				$quote->assignCustomer($customer);
			}
			else
			{
				$quote->setCustomerEmail($data['custemail']);	
			}
			
			$quote->setCurrency($currentcurrency);
			
			//set Partial Payment Data
			$quote->setData('is_partialpayment',1);
			$quote->setData('partialpayment_order',$data['order']);
			$quote->setData('partialpayment_fee',0.0);
			$quote->setData('base_partialpayment_fee',0.0);
			$quote->setData('partialpayment_remaining',0.0);
			$quote->setData('base_partialpayment_remaining',0.0);
					
			//Add Product To Quote
			$product = Mage::getModel('catalog/product')->loadByAttribute('sku','partialpayment_installment');
			$product = Mage::getModel('catalog/product')->load($product->getEntityId());
			
			$newbuyInfo = array(
    				'product' => $product->getEntityId(),
					'qty' => 1,
					'price' => $data['amount']
			);
			$quote->addProduct($product,new Varien_Object($newbuyInfo))->setOriginalCustomPrice($data['amount']);
			
			$streetaddress = '';
			foreach($order->getShippingAddress()->getStreet() as $add)
			{
				$streetaddress = $add;	
			}
			
			$addressData = array(
				'firstname' => $order->getShippingAddress()->getFirstname(),
				'lastname' => $order->getShippingAddress()->getLastname(),
				'street' => $streetaddress,
				'city' => $order->getShippingAddress()->getCity(),
				'postcode' => $order->getShippingAddress()->getPostcode(),
				'telephone' => $order->getShippingAddress()->getTelephone(),
				'country_id' => $order->getShippingAddress()->getCountryId(),
				'region_id' => $order->getShippingAddress()->getRegionId(),
			);
			
			//Set Address   
			$billingAddress = $quote->getBillingAddress()->addData($addressData);
			$shippingAddress = $quote->getShippingAddress()->addData($addressData);
			
			
			$quote->getShippingAddress()->setCollectShippingRates(true);
            $quote->getShippingAddress()->collectShippingRates();
            $quote->getShippingAddress()->setShippingMethod($order->getShippingMethod());
            $quote->getShippingAddress()->setPaymentMethod($order->getPayment()->getMethodInstance()->getCode());
			
			// Set Sales Order Payment
 			$quote->getPayment()->importData(array('method' => $order->getPayment()->getMethodInstance()->getCode()));
 
 			// Collect Totals & Save Quote
 			$quote->collectTotals()->save();
			
			 // Create Order From Quote
 			$service = Mage::getModel('sales/service_quote', $quote);
 			$service->submitAll();
 			//$increment_id = $service->getOrder()->getRealOrderId();
 
 			// Resource Clean-Up
 			$quote = $customer = $service = null;
 
 			// Finished
 			echo 1;
			return;
			
			/* Programetically Create Order (End) */
			
		}
		catch(Exception $e)
		{
			echo $e->getMessage();	
		}
	}

 
}