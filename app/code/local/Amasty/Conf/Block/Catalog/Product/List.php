<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Conf
 */
if ('true' == (string) Mage::getConfig()->getNode('modules/Affirm_Affirm/active')) {

    class Amasty_Conf_Block_Catalog_Product_List_Pure extends Affirm_Affirm_Block_Product_List
    {
        
    }

} else {

    class Amasty_Conf_Block_Catalog_Product_List_Pure extends Mage_Catalog_Block_Product_List
    {
        
    }

}

class Amasty_Conf_Block_Catalog_Product_List extends Amasty_Conf_Block_Catalog_Product_List_Pure
{

    public function getPriceHtml($product, $displayMinimalPrice = false, $idSuffix = '')
    {
        $html = parent::getPriceHtml($product, $displayMinimalPrice, $idSuffix);
        if (Mage::getStoreConfig('amconf/list/enable_list') == 1 && $product->isConfigurable()) {
            $html .= Mage::helper('amconf')->getHtmlBlock($product, '');
        }

        return $html;
    }

}
