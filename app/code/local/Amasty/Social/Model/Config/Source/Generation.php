<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Social
 */
class Amasty_Social_Model_Config_Source_Generation extends Varien_Object
{
    public function toOptionArray()
    {
        $vals = array(
            'static' => Mage::helper('salesrule')->__('Static'),
            'dynamic'   => Mage::helper('salesrule')->__('Dynamic'),
        );

        $options = array();
        foreach ($vals as $k => $v)
            $options[] = array(
                    'value' => $k,
                    'label' => $v
            );
        
        return $options;
    }
}