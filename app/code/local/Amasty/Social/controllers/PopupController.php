<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Social
 */
class Amasty_Social_PopupController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    
    public function saveAction()
    {
        $save = Mage::app()->getRequest()->getParam('save');
        if($save) {
            Mage::app()->getCookie()->set('amsocial_is_first', 1, 3600 * 24 * 30); // set cookie for 30 days
        }
    }
    
    public function couponAction()
    {
        $platform = Mage::app()->getRequest()->getParam('platform');
        if (!in_array($platform, array('platform_twitter', 'platform_fb', 'platform_gplus', 'platform_pin')))
        {
            $this->getResponse()->setBody('Coupon do not supported platform.');
        } else {
            $currentCookie =  Mage::getModel('core/cookie')->get('amsocial_code');
            if (!$currentCookie)
            {
                if (Mage::getStoreConfig('amsocial/coupon/generation') == 'static')
                {        
                    $result = Mage::helper('amsocial')->getCouponCode();
                } else {
                    $result = Mage::getModel('amsocial/coupon')->getCouponCode();
                }
                Mage::app()->getCookie()->set('amsocial_code', $result, 3600 * 48); // set cookie for 48 hours
               
            }  else {
                $result = $currentCookie;
            }
            $this->getResponse()->setBody($result);             

        }
    }
}