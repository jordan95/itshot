<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Social
 */
class Amasty_Social_Block_Adminhtml_Callslide extends Mage_Adminhtml_Block_Abstract
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('amasty/amsocial/callslide.phtml');
    }
}

