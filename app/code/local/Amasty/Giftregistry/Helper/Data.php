<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Giftregistry
 */
class Amasty_GiftRegistry_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ORDER_STATES = 'amgiftreg/general/registry_order_status';

    const ERROR_MESSAGE = 'If there is the following text it means that Amasty_Base is not updated to the latest 
                             version.<p>In order to fix the error, please, download and install the latest version of 
                             the Amasty_Base, which is included in all our extensions.
                        <p>If some assistance is needed, please submit a support ticket with us at: 
                        <a href="https://amasty.com/contacts/" target="_blank">https://amasty.com/contacts/</a>';

    public function getAddUrl($product)
    {
        $url = '';
        if (Mage::getStoreConfig('amgiftreg/general/active'))
            $url =  $this->_getUrl('amgiftreg/event/addItem', array('product'=>$product->getId()));
             
        return $url;
    }

    public function getAddUrlById($productId, $eventId = null)
    {
        $url = '';
        if (Mage::getStoreConfig('amgiftreg/general/active'))
            $url =  $this->_getUrl('amgiftreg/event/addItem', array('product'=>$productId, 'event'=>$eventId));

        return $url;
    }

    public function getRegistryUrl($registryId)
    {
        $url = '';
        if (Mage::getStoreConfig('amgiftreg/general/active'))
            $url =  $this->_getUrl('amgiftreg/gift/view', array('id'=>$registryId));

        return $url;
    }


    public function getRegistryShareText($registry_id)
    {
        $shareUrl = $this->getRegistryUrl($registry_id);
        $shareText = $this->__("Hello Guys, I have a Gift Registry at %s. Thank you for the gifts.", $shareUrl);

        return $shareText;
    }

    public function getListEvents()
    {
        return Mage::getResourceModel('amgiftreg/event_collection')
            ->addCustomerFilter(Mage::getSingleton('customer/session')->getCustomerId())
            ->load();
    }

    public function getListItemPriority()
    {

        return array(
            10 => $this->__('Low'),
            20 => $this->__('Medium'),
            30 => $this->__('High'),
        );
    }

    public function getPriorityById($priorityId)
    {
        $listPriority = $this->getListItemPriority();
        return isset($listPriority[$priorityId]) ? $listPriority[$priorityId] : null;
    }

    public function escapeLike($value)
    {
        $escapeChars = array(
            "%" => "\\%",
            "_" => "\\_",
        );

        return str_replace(array_keys($escapeChars), array_values($escapeChars), $value);
    }

    public function getOrderStatusesForOrderedItems()
    {
        $orderStatesStr = Mage::getStoreConfig(self::XML_PATH_ORDER_STATES);
        $orderStates = explode(",", $orderStatesStr);
        return $orderStates;
    }

    public function getCalendarDateFormat()
    {
        return Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
    }

    /**
     * @param string $string
     *
     * @return array|null
     */
    public function unserialize($string)
    {
        if (!@class_exists('Amasty_Base_Helper_String')) {
            Mage::logException(new Exception(self::ERROR_MESSAGE));

            if (Mage::app()->getStore()->isAdmin()) {
                Mage::helper('ambase/utils')->_exit(self::ERROR_MESSAGE);
            } else {
                Mage::throwException($this->__('Sorry, something went wrong. Please contact us or try again later.'));
            }
        }

        return \Amasty_Base_Helper_String::unserialize($string);
    }
}
