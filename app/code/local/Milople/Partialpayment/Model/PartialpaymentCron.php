<?php

/**
 * 
 * Do not edit or add to this file if you wish to upgrade the module to newer 
 * versions in the future. If you wish to customize the module for your 
 * needs please contact us to https://www.milople.com/contact-us.html
 * 
 * @category		Ecommerce 
 * @package		Milople_Partialpayment
 * @extension	Partial Payment and Layaway Auto 
 * @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
 * @url			https://www.milople.com/magento-extensions/partial-payment.html
 * 
 * */
class Milople_Partialpayment_Model_PartialpaymentCron extends Mage_Core_Model_Abstract
{

    static $partial_payment_helper;
    static $partial_payment_emailsender;
    static $calculation_model;

    public function _construct()
    {
        parent::_construct();
        $this->_init('partialpayment/PartialpaymentCron');
        $this->partial_payment_helper = Mage::helper('partialpayment/partialpayment');
        $this->partial_payment_emailsender = Mage::helper('partialpayment/emailsender');
        $this->calculation_model = Mage::getModel("partialpayment/calculation");
    }

    protected function _debug($message)
    {
        Mage::log($message, null, 'partialpayment-cron.log', true);
    }

    public function cronFunctionsCall()
    {
        $this->_debug('*********************************************************');
        $this->_debug('Start cronjob: Auto capture payment for installments');
        $this->sentInstallmentReminderMails();
        $this->autoCapturePayment();
        $this->_debug('End cronjob: Auto capture payment for installments');
        $this->_debug('*********************************************************');
    }

    public function sentInstallmentReminderMails()
    {
        $this->_debug('Start sent installment reminder mails');
        $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
        $now = new Zend_Date($currentTimestamp);
        $date = new DateTime($now->addDay(2)->get('YYYY-MM-dd HH:mm:ss'));

        //get collection of installment which next installment date is after 2 days
        $installments = mage::getModel("partialpayment/installment")->getCollection()->addFieldToFilter('installment_due_date', $date->format('Y-m-d'))->addFieldToFilter('installment_status', 'Remaining')->addFieldToFilter('installment_reminder_email_sent', 0);
        foreach ($installments as $installment) {
            if ($installment->getInstallmentAmount() <= "0.0001") {
                continue;
            }
            $partialpaymentCollection = Mage::getModel('partialpayment/partialpayment')->load($installment->getPartialPaymentId());
            $salesOrderCollection = Mage::getModel('sales/order')->load($partialpaymentCollection->getOrderId());
            if (in_array($salesOrderCollection->getStatus(), array('closed', 'canceled', 'holded', 'complete'))) {
                continue;
            }
            $currency_code = $salesOrderCollection->getOrderCurrency()->getCurrencyCode();
            $is_auto_capture = false;
            $payment_method = $salesOrderCollection->getPayment()->getMethodInstance()->getCode();
            //function to send mails
            if (($payment_method == "authorizenet" || $payment_method == "sagepaydirectpro" || $payment_method == "sagepayserver") && ($this->partial_payment_helper->autocapture())) {
                $is_auto_capture = true;
            }

            $this->partial_payment_emailsender->sendIntallmentReminderMail($is_auto_capture, $salesOrderCollection->getCustomerFirstname(), $salesOrderCollection->getCustomerEmail(), $salesOrderCollection->getIncrementId(), $installment->getPartialPaymentId(), $date->format('m-d-Y'), $currency_code);
            $installmetCollection = Mage::getModel("partialpayment/installment")->load($installment->getInstallmentId());
            // set installment reminder email sent to 1 to stop again mail for same installment
            $installmetCollection->setInstallmentReminderEmailSent(1)->save();
        }
        $this->_debug('End sent installment reminder mails');
    }

    public function autoCapturePayment()
    {
        $logData = new Varien_Object();
        $logData->setStartTime(date('Y-d-m H:i:s'));
        $this->_debug('Start run auto capture payment');
        $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
        $now = new Zend_Date($currentTimestamp);
        $todayDate = new DateTime($now->get('YYYY-MM-dd HH:mm:ss'));
        $next_installment_date1 = new DateTime($now->addDay(-2)->get('YYYY-MM-dd HH:mm:ss'));
        $next_installment_date2 = new DateTime($now->addDay(-2)->get('YYYY-MM-dd HH:mm:ss'));
        $next_installment_date3 = new DateTime($now->addDay(-2)->get('YYYY-MM-dd HH:mm:ss'));
        $dates = array($todayDate->format('Y-m-d'), $next_installment_date1->format('Y-m-d'), $next_installment_date2->format('Y-m-d'), $next_installment_date3->format('Y-m-d'));
        $installments = mage::getModel("partialpayment/installment")->getCollection()->addFieldToFilter('installment_due_date', array('in' => $dates))->addFieldToFilter('installment_status', array('in' => array('Remaining', 'Failed')));
        $logData->setDumpSql((string) $installments->getSelect());
        $logData->setNumRow($installments->count());
        // Process each installments
        $logInstallments = array();
        foreach ($installments as $installment) {
            if ($installment->getInstallmentAmount() <= "0.0001") {
                continue;
            }
            $logInstallment = array();
            $partialpaymentCollection = Mage::getModel('partialpayment/partialpayment')->load($installment->getPartialPaymentId());
            $salesOrderCollection = Mage::getModel('sales/order')->load($partialpaymentCollection->getOrderId()); // Get collection of partial payment order
            if (in_array($salesOrderCollection->getStatus(), array('closed', 'canceled', 'holded', 'complete'))) {
                continue;
            }
            $this->_debug('*******************Installment #' . $installment->getId() . '*******************');

            $logInstallment = array(
                'partialpayment_id' => $installment->getPartialPaymentId(),
                'order_id' => $partialpaymentCollection->getOrderId(),
                'order_increment_id' => $salesOrderCollection->getIncrementId(),
                'customer_email' => $salesOrderCollection->getCustomerEmail(),
                'order_status' => $salesOrderCollection->getStatus()
            );
            $this->_debug('Order Status: OK');
            $currency_code = $salesOrderCollection->getOrderCurrency()->getCurrencyCode();
            $partial_payment_id = $partialpaymentCollection->getPartialPaymentId();
            $payment_method = $salesOrderCollection->getPayment()->getMethodInstance()->getCode(); // Get payment method
            $instament_due_date = new DateTime($installment->getInstallmentDueDate());
            $interval = $todayDate->diff($instament_due_date);
            $noOfDueMailSent = $installment->getInstallmentOverDueNoticeEmailSent();
            $installment_amount = $installment->getInstallmentAmount();
            $installment_failed = false;
            // logging
            $logInstallment['currency_code'] = $currency_code;
            $logInstallment['payment_method'] = $payment_method;
            $logInstallment['installment_id'] = $installment->getId();
            $logInstallment['installment_amount'] = $installment_amount;
            $logInstallment['autocapture'] = $this->partial_payment_helper->autocapture();
            $logInstallment['auto_capture_profile_id'] = $partialpaymentCollection->getAutoCaptureProfileId();
            $logInstallment['auto_capture_payment_profile_id'] = $partialpaymentCollection->getAutoCapturePaymentProfileId();

            if ($this->partial_payment_helper->autocapture()) {
                $this->_debug('Auto Capture: OK');
                $this->_debug('Payment Method: ' . $payment_method);
                if ($payment_method == "ewayrapid_ewayone") {
                    $post['Token'] = $partialpaymentCollection->getAutoCaptureProfileId();
                    $response = Mage::getModel('ewayrapid/request_direct')->ewayrapidDirectConnection($post, $salesOrderCollection, $installment_amount);
                    if ($response->isSuccess()) { // If installment successfull paid with auto capture
                        $this->_debug('Set Installment Success Data: OK');
                        Mage::getModel('partialpayment/calculation')->setInstallmentSuccessData($installment->getId(), $partialpaymentCollection->getOrderId(), $partialpaymentCollection->getId(), $payment_method, $response->getData()['TransactionID']);
                    } else {
                        $this->_debug('Set Installment Success Data: Failed');
                        $installment_failed = true;
                    }
                }
                #autocapture for paypal express
                if ($payment_method == "paypal_express") {
                    $billing_agreement_data = Mage::getModel('sales/billing_agreement')->getCollection()->addFieldToFilter('customer_id', $salesOrderCollection->getCustomerId())->addFieldToFilter('status', 'active');
                    if (!empty($billing_agreement_data)) {
                        $billing_id = $billing_agreement_data->getFirstItem()->getReferenceId();
                        if (!empty($billing_id)) {
                            Mage::register('add_installment_amount_payment_grid', $installment_amount);
                            $now = Mage::getModel('core/date')->date('m') . Mage::getModel('core/date')->date('d') . Mage::getModel('core/date')->date('H') . Mage::getModel('core/date')->date('i') . Mage::getModel('core/date')->date('s'); // Addded month, day, hour, minute, second in invoice to capture multiple installment at once
//                                $invId = $salesOrderCollection->getIncrementId() . "-" . $installment->getId() . "-" . $now; // Create Order(invoice) id to capture installment, it includes order id as well as installmnet id and date and time
                            $current_magento_time = Mage::getModel('core/date')->date('m/d/Y~H:i:s');
                            $invId = $salesOrderCollection->getIncrementId() . "-" . $current_magento_time;
                            $response = Mage::getModel("partialpayment/api_nvp")->callInstallmentCapture($partialpaymentCollection->getOrderId(), round($installment_amount, 2), $billing_id, $invId);
                            $logInstallment['api_request'] = array(
                                'order_id' => $partialpaymentCollection->getOrderId(),
                                'amount' => round($installment_amount, 2),
                                'billing_id' => $billing_id,
                                'invoice_id' => $invId
                            );
                            $logInstallment['api_response'] = $response;
                            if ($response['ACK'] == 'Success') { // If installment successfull paid with auto capture
                                $this->_debug('Set Installment Success Data: OK');
                                Mage::getModel('partialpayment/calculation')->setInstallmentSuccessData($installment->getId(), $partialpaymentCollection->getOrderId(), $partialpaymentCollection->getId(), $payment_method, $response['TRANSACTIONID']);
                            } else {
                                $this->_debug('Set Installment Success Data: Failed');
                                $installment_failed = true;
                            }
                            Mage::unregister('add_installment_amount_payment_grid');
                        } else {
                            $this->_debug('Set Installment Success Data: Failed');
                            $installment_failed = true;
                        }
                    }
                }
                // If payment method is paypal pro (paypal_direct) and have atuo capture profile id (refernece trasacation id) is set
                if ($payment_method == "paypal_direct" && $partialpaymentCollection->getAutoCaptureProfileId() != '') {
                    $now = Mage::getModel('core/date')->date('m') . Mage::getModel('core/date')->date('d') . Mage::getModel('core/date')->date('H') . Mage::getModel('core/date')->date('i') . Mage::getModel('core/date')->date('s'); // Addded month, day, hour, minute, second in invoice to capture multiple installment at once
                    $invId = $salesOrderCollection->getIncrementId() . "-" . $installment->getId() . "-" . $now; // Create Order(invoice) id to capture installment, it includes order id as well as installmnet id and date and time

                    $response = Mage::getModel("partialpayment/api_nvp")->callInstallmentCapture($partialpaymentCollection->getOrderId(), round($installment_amount, 2), $partialpaymentCollection->getAutoCaptureProfileId(), $invId);
                    $logInstallment['api_request'] = array(
                        'order_id' => $partialpaymentCollection->getOrderId(),
                        'amount' => round($installment_amount, 2),
                        'auto_capture_profile_id' => $partialpaymentCollection->getAutoCaptureProfileId(),
                        'invoice_id' => $invId
                    );
                    $logInstallment['api_response'] = $response;
                    if ($response['ACK'] == 'Success') { // If installment successfull paid with auto capture
                        $this->_debug('Set Installment Success Data: OK');
                        Mage::getModel('partialpayment/calculation')->setInstallmentSuccessData($installment->getId(), $partialpaymentCollection->getOrderId(), $partialpaymentCollection->getId(), $payment_method, $response['TRANSACTIONID']);
                    } else {
                        $this->_debug('Set Installment Success Data: Failed');
                        $installment_failed = true;
                    }
                } else if ($payment_method == "verisign" && $partialpaymentCollection->getAutoCaptureProfileId() != '') {
                    $response = Mage::getModel("partialpayment/payflowpro")->captureInstallment($partialpaymentCollection->getAutoCaptureProfileId(), round($installment_amount, 2), $salesOrderCollection->getIncrementId(), $installment->getId());
                    $logInstallment['api_request'] = array(
                        'order_id' => $partialpaymentCollection->getOrderId(),
                        'amount' => round($installment_amount, 2),
                        'order_increment_id' => $salesOrderCollection->getIncrementId(),
                        'installment_id' => $installment->getId()
                    );
                    $logInstallment['api_response'] = $response;
                    if ($response['pnref']) { // If installment successfully paid with auto capture
                        $this->_debug('Set Installment Success Data: OK');
                        $partialpaymentCollection->setAutoCaptureProfileId($response['pnref'])->save();
                        Mage::getModel('partialpayment/calculation')->setInstallmentSuccessData($installment->getId(), $partialpaymentCollection->getOrderId(), $partialpaymentCollection->getId(), $payment_method, $response['pnref']);
                    } else {
                        $this->_debug('Set Installment Success Data: Failed');
                        $installment_failed = true;
                    }
                } else if ((($payment_method == "sagepaydirectpro" && Mage::getStoreConfig('payment/sagepaydirectpro/active')) || ($payment_method == "sagepayserver" && Mage::getStoreConfig('payment/sagepayserver/active'))) && $this->partial_payment_helper->autocapture()) { // Capture process for sage pay
                    $sagepaymodel = Mage::getModel('partialpaymentsagepay/partialpaymentsagepay');
                    $billing_id = $salesOrderCollection->getBillingAddressId();
                    $shipping_id = $salesOrderCollection->getShippingAddressId();
                    $response = Milople_Partialpaymentsagepay_Model_Partialpaymentsagepay::repeatWithSagePay($salesOrderCollection->getIncrementId(), $billing_id, $shipping_id, $installment->getInstallmentId(), $installment_amount, $payment_method, $partialpaymentCollection->getAutoCaptureProfileId());
                    $logInstallment['api_request'] = array(
                        'order_increment_id' => $salesOrderCollection->getIncrementId(),
                        'billing_id' => $billing_id,
                        'shipping_id' => $shipping_id,
                        'installment_id' => $installment->getInstallmentId(),
                        'amount' => $installment_amount,
                        'payment_method' => $payment_method,
                        'auto_capture_profile_id' => $partialpaymentCollection->getAutoCaptureProfileId()
                    );
                    $logInstallment['api_response'] = $response;
                    if ($response['VPSTxId']) { // If installment successfull paid with auto capture
                        $this->_debug('Set Installment Success Data: OK');
                        Mage::getModel('partialpayment/calculation')->setInstallmentSuccessData($installment->getId(), $partialpaymentCollection->getOrderId(), $partialpaymentCollection->getId(), $payment_method, $response['VPSTxId']);
                    } else {
                        $this->_debug('Set Installment Success Data: Failed');
                        $installment_failed = true;
                    }
                } // Cron for Sagepay Ends
                else if (($payment_method == "authorizenet" ) && $partialpaymentCollection->getAutoCaptureProfileId() != "" && $partialpaymentCollection->getAutoCapturePaymentProfileId() != "" && $this->partial_payment_helper->autocapture() && Mage::getStoreConfig('payment/authorizenet/active')) { // Capture process for autorizenet
                    Mage::register('installmentId', $installment->getId());
                    Mage::register('add_installment_amount_payment_grid', $installment_amount);
                    try {
                        $paymentData = Mage::getModel('partialpayment/subscription');
                        $paymentData->processPayment($salesOrderCollection);
                    } catch (Exception $exc) {
                        $this->_debug($exc->getMessage());
                    }

                    if ($paymentData->getOrder()->getPayment()->getCcTransId()) { // If installment successfull paid with auto capture
                        $this->_debug('CC Trans ID: OK');
                        $this->_debug('Set Installment Success Data: OK');
                        Mage::getModel('partialpayment/calculation')->setInstallmentSuccessData($installment->getId(), $partialpaymentCollection->getOrderId(), $partialpaymentCollection->getId(), $payment_method, $paymentData->getOrder()->getPayment()->getCcTransId());
                    } else {
                        $this->_debug('Set Installment Success Data: Failed');
                        $installment_failed = true;
                    }
                    Mage::unregister('installmentId');
                    Mage::unregister('add_installment_amount_payment_grid');
                }
                // Capture process for autorizenet End
                // If not paid successfully then send failure mail
                if ($installment_failed) {
                    $this->_debug('Send Installment Auto Capture Failure Mail: OK');
                    if ($interval->format('%a') != 6) { // If failure after 2, 4 days
                        $this->partial_payment_emailsender->sendInstallmentAutoCaptureFailureMail($salesOrderCollection->getCustomerFirstname(), $salesOrderCollection->getCustomerEmail(), $salesOrderCollection->getIncrementId(), $installment_amount, $partial_payment_id, $currency_code, false);
                    } else { // Else failure in last attempt of after 6 days
                        $this->partial_payment_emailsender->sendInstallmentAutoCaptureFailureMail($salesOrderCollection->getCustomerFirstname(), $salesOrderCollection->getCustomerEmail(), $salesOrderCollection->getIncrementId(), $installment_amount, $partial_payment_id, $currency_code, true);
                    }
                    $installment->setInstallmentStatus('Failed');
                    $this->_debug('Set Installment Status: Failed');
                } else {
                    $this->_debug('Set Installment Status: Paid');
                    $installment->setInstallmentStatus('Paid');
                }
            }

            if (($interval->format('%a') != 0) && (($interval->format('%a') - 2) == $noOfDueMailSent) && ($installment->getInstallmentStatus() == 'Remaining' || $installment->getInstallmentStatus() == 'Failed')) {
                $this->_debug('Send Over Due Mail: OK');
                $this->partial_payment_emailsender->sendOverDueMail($salesOrderCollection->getCustomerFirstname(), $salesOrderCollection->getCustomerEmail(), $salesOrderCollection->getIncrementId(), $instament_due_date->format('m-d-Y'), $installment_amount, $partial_payment_id, $currency_code);

                // set installment over due notice mail sent to 2,4 or 6 as per day interval to stop again mail for same installment
                $installment->setInstallmentOverDueNoticeEmailSent(($noOfDueMailSent + 2));
            }
            $logInstallment['installment_saved'] = false;

            if ($installment_failed || (($interval->format('%a') != 0) && (($interval->format('%a') - 2) == $noOfDueMailSent) && ($installment->getInstallmentStatus() == 'Remaining' || $installment->getInstallmentStatus() == 'Failed'))) {
                $installment->save();
                $this->_debug('Installment Saved: OK');
                $logInstallment['installment_saved'] = true;
            }
            $logInstallments[$installment->getId()] = $logInstallment;
            $this->_debug('End run auto capture payment for Installment #' . $installment->getId());
        }
        $logData->setInstallments($logInstallments);
        $logData->setEndTime(date('Y-d-m H:i:s'));
        $this->_debug($logData->toArray());
        $this->_debug('End run auto capture payment');
    }

}

?>
