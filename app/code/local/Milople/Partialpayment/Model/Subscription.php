<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Subscription extends Mage_Core_Model_Abstract
{
	const INTERNAL_DATE_FORMAT = 'yyyy-MM-dd'; // DON'T use Y(upper case here)
    const DB_DATE_FORMAT = 'yyyy-MM-dd'; // DON'T use Y(upper case here)
    const DB_DATETIME_FORMAT = 'yyyy-MM-dd H:m:s'; // DON'T use Y(upper case here)

    const ITERATE_STATUS_REGISTRY_NAME = 'IND_SARP_PAYMENT_STATUS';
    const ITERATE_STATUS_RUNNING = 2;
    const ITERATE_STATUS_FINISHED = 12;

	public function processPayment($Order)
    {
        try 
		{
			$storeId = $Order->getStoreId();
			$this->setOrder($Order);
            $pm_code = $Order->getPayment()->getMethod();
            $PaymentInstance = $this->_getMethodInstance($pm_code);			
			$subscription = new Varien_Object();
			$subscription->setStoreId($storeId);
			$service = $PaymentInstance->getWebService();
			$service->setSubscription($subscription);
            $PaymentInstance
                    ->processOrder($this->getOrder(), $Order);
        } catch (Exception $e) {
            throw new Mage_Core_Exception("Payment message: #{$this->getId()}: {$e->getMessage()}");
        }     
	    return $this;
    }
	
	public function getMethodInstance($method)
    {
        return $this->_getMethodInstance($method);
    }
	
	protected function _getMethodInstance($method = null)
    {		
        if (!$method && $this->getOrder()) {
            try {
                $method = $this->getOrder()->getPayment()->getMethod();
            } catch (Exception $e) {
            }
        }
        if ($model = Mage::getModel('partialpayment/payment_method_' . $method)) {
            return $model->setSubscription($this);
        } else {
            throw new Mage_Core_Exception(Mage::helper('partialpayment')->__("Can't find implementation of payment method $method"));
        }
    }
	public static function isIterating()
    {
        return Mage::registry(self::ITERATE_STATUS_REGISTRY_NAME) == self::ITERATE_STATUS_RUNNING;
    }
}