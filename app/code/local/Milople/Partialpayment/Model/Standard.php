<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Standard extends Mage_Paypal_Model_Standard
{
	private function _getAggregatedCartSummary()
    {
        if ($this->_config->lineItemsSummary) {
            return $this->_config->lineItemsSummary;
        }
        return Mage::app()->getStore($this->getStore())->getFrontendName();
    }

	public function getStandardCheckoutFormFields()
    {
		$calculationHelper = Mage::helper('partialpayment/partialpayment');
        $orderIncrementId = $this->getCheckout()->getLastRealOrderId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        $api = Mage::getModel('paypal/api_standard')->setConfigObject($this->getConfig());
        $api->setOrderId($orderIncrementId)
            ->setCurrencyCode($order->getBaseCurrencyCode())
            ->setNotifyUrl(Mage::getUrl('paypal/ipn/'))
            ->setReturnUrl(Mage::getUrl('paypal/standard/success'))
            ->setCancelUrl(Mage::getUrl('paypal/standard/cancel'));

        // export address
        $isOrderVirtual = $order->getIsVirtual();
        $address = $isOrderVirtual ? $order->getBillingAddress() : $order->getShippingAddress();
        if ($isOrderVirtual) {
            $api->setNoShipping(true);
		}elseif ($address->getEmail()) {
		    $api->setAddress($address);
        }

		$api->setPaypalCart(Mage::getModel('paypal/cart', array($order)))
            ->setIsLineItemsEnabled($this->_config->lineItemsEnabled);

		if(!$this->_config->lineItemsEnabled){
			$api->setCartSummary($this->_getAggregatedCartSummary());
		}

		$result = $api->getStandardCheckoutRequest();

		// Start: Update Total when Order was placed with Partial Payment 
		$orderId = $order->getEntityId();
		$partialpaymentOrder = Mage::getModel('partialpayment/partialpayment')->getCollection()
			->addFieldToFilter('order_id',$orderId)
			->load();
		foreach($partialpaymentOrder as $partialpaymentModel)
		{
			if($order->getPaidAmount())
			{
				$amount = $order->getPaidAmount();
				$amount = $calculationHelper->convertCurrencyAmount($amount);  
				if(Mage::getStoreConfig("payment/paypal_standard/line_items_enabled")==1)
				{
					$items = $order->getAllVisibleItems();
					$i = 1;
					foreach ($items as $item) {
						if($i == 1)
						{
							$result['item_number_'.$i] = '';
							$result['item_name_'.$i] = $order->getIncrementId();
							$result['quantity_'.$i] = '';
							$result['amount_'.$i] = number_format($amount, 2);
							$result['tax_cart'] = 0 ;
							$result['tax']= 0 ;
						}
						else
						{
							$result['item_number_'.$i] = '';
							$result['item_name_'.$i] = '';
							$result['quantity_'.$i] = '';
							$result['amount_'.$i] = '';
						}
						$i++;
					}
					if (isset($result['item_name_'.$i]) && $result['item_name_'.$i] == 'Shipping') {
						unset($result['item_number_'.$i]);
						unset($result['item_name_'.$i]);
						unset($result['quantity_'.$i]);
						unset($result['amount_'.$i]);
					}
				}
				if(Mage::getStoreConfig("payment/paypal_standard/line_items_enabled") == 0) 
				{
					$result['tax'] = 0;
					$result['amount'] = number_format($amount, 2);
					if (isset($result['shipping'])) {
						unset($result['shipping']);
					}
				}
			}
		}
		return $result;
    }
}
