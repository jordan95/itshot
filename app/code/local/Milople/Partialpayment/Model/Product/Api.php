<?php
// app/code/local/Envato/Customapimodule/Model/Product/Api.php
class Milople_Partialpayment_Model_Product_Api extends Mage_Api_Model_Resource_Abstract
{
	public function items($order)
	{
		$response = NULL;
		$ord = Mage::getModel('sales/order')->loadByIncrementId($order);
		if($ord)
		{
			$partialpayment = Mage::getModel('partialpayment/partialpayment')->getCollection()->addFieldToFilter('order_id',$ord->getId());
			$installments = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id',$partialpayment->getFirstItem()->getPartialPaymentId());
			$response = json_encode($installments->getData());
		}
		return $response;
	}
}