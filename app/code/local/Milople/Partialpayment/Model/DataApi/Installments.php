<?php

class Milople_Partialpayment_Model_DataApi_Installments extends Mage_Api_Model_Resource_Abstract
{
	public function installmentsData($order)
	{
		$response = NULL;
		$ord = Mage::getModel('sales/order')->loadByIncrementId($order);
		if($ord)
		{
			$partialpayment = Mage::getModel('partialpayment/partialpayment')->getCollection()->addFieldToFilter('order_id',$ord->getId());
			$installments = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id',$partialpayment->getFirstItem()->getPartialPaymentId());
			$response = json_encode($installments->getData());
		}
		return $response;
	}
}