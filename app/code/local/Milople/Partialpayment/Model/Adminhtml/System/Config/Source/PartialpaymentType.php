<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Adminhtml_System_Config_Source_PartialpaymentType extends Varien_Object
{
    static public function toOptionArray()
    {
        return array(
            0 => Mage::helper('partialpayment')->__('Fixed Installment Payment'),
            1 => Mage::helper('partialpayment')->__('Flexy Layaway Plan')
        );
    }
}