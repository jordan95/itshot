<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_InstallmentSummaryPaymentMethods
{
    public function toOptionArray()
    {
        $payments = Mage::getSingleton('payment/config')->getActiveMethods();
		
		$methods = array();
		foreach ($payments as $paymentCode=>$paymentModel) 
		{
			$paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
			if($paymentCode == 'cashondelivery' || $paymentCode == 'checkmo')
			{
				$methods["$paymentCode"]= $paymentTitle;
			}
		}
		return $methods;
    }
}