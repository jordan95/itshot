<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/magento-extensions/contacts/
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2015 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Installments extends Varien_Object
{
    static public function getAllOptions()
    {
        return array(
			0    => Mage::helper('partialpayment')->__('Please Select'),
            1    => Mage::helper('partialpayment')->__('Fixed'),
            2    => Mage::helper('partialpayment')->__('Percentage'),
			
        );
    }
}