<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Validation extends Mage_Core_Model_Config_Data 
{
	public static $totalInstallments;
	public static $minimumOrderAmount;
	public static $calculateInstallmentsOn;
	public static $downPayment;
	public static $totalNumberOfDays;
	public static $surchargeValue;
	public static $surchargeValues;
	public static $calculatePreOrderDiscountOn;
	public static $preOrderDiscount;
	public static $defaultCreditAmount;
	public static $remindBeforeDays;

    public function save()
    {
		$data = $this->_getData('fieldset_data');

		if (isset($data['serial_key'])) {
			if($data['serial_key'] != '')
			{
				if(Mage::helper('partialpayment/data')->canRun($data['serial_key']))
				{
					return parent::save();
				}
				else
				{
					Mage::throwException(Mage::helper('partialpayment/data')->getLicenseViolationMassage ());
				}
			}
			else
			{
				Mage::throwException(Mage::helper('partialpayment/data')->getLicenseViolationMassage ());
			}
		}

		if(isset($data['surcharge_installment_calculation_type']) && ($data['surcharge_installment_calculation_type']==2 &&($data['surcharge_value'] < 0 || $data['surcharge_value'] > 100))) 															{
			Mage::throwException('The  Surcharge  must be between 1 to 100');
		}
		
		if(isset($data['calculate_downpayment_on']) && ($data['calculate_downpayment_on']==2 && $data['down_payment'] > 99))
		{
			Mage::throwException('The  Down Payment  must be between 1 to 100');
		}
		
		if(isset($data['default_credit_amount']) && ($data['default_credit_amount'] <0))
		{
			Mage::throwException('The credit Amount Cannot be Negative');
		}
		if(isset($data['partial_payment_options']) && $data['partial_payment_options'] == '2_installments') {
			self::$totalInstallments = 2;
		}

		if(isset($data['no_of_installments'])) {
			self::$totalInstallments = $data['no_of_installments'];
			if (self::$totalInstallments < 2) {
				Mage::throwException('Total Installments value must be at least 2.');
			}
		}

		if(isset($data['minimum_order_amount'])) {
			self::$minimumOrderAmount = $data['minimum_order_amount'];
			if (self::$minimumOrderAmount < 0) {
				Mage::throwException('Minimum Order Amount value must be at least 0.');
			}
		}

		if(isset($data['installment_calculation_type'])) 
		{
			self::$calculateInstallmentsOn = $data['installment_calculation_type'];

			if(isset($data['first_installment_amount'])) {
				self::$downPayment = $data['first_installment_amount'];

				if (self::$calculateInstallmentsOn == 1 && self::$downPayment <= 0) {
					Mage::throwException('Down Payment value must be greater than 0.');
				}
				elseif (self::$calculateInstallmentsOn == 2 && (self::$downPayment <= 0 || self::$downPayment >= 100)) {
					Mage::throwException('Down Payment value must be between 0 and 100.');
				}
			}
		}
		if(isset($data['surcharge_options'])) {
			if(($data['surcharge_options'])==1) {
				self::$surchargeValue = $data['surcharge_value'];
				
				if (self::$calculateInstallmentsOn == 1 && self::$surchargeValue < 0) {
					Mage::throwException('Surcharge Value must be at least 0.');
				}
				elseif (self::$calculateInstallmentsOn == 2 && (self::$surchargeValue < 0 || self::$surchargeValue > 100)) {
					Mage::throwException('Surcharge Value must be between 0 and 100.');
				}
			}
			if(($data['surcharge_options'])==1) {
				self::$surchargeValues = $data['surcharge_value'];
				if(!is_numeric(self::$surchargeValues))
				{
					Mage::throwException('Surcharge Value must be numeric.');
				}
			}
			if(($data['surcharge_options'])==2) {
				self::$surchargeValues = $data['surcharge_value'];
				$multiple_surcharge_values = explode(',', self::$surchargeValues);

				if (self::$totalInstallments != count($multiple_surcharge_values)) {
					Mage::throwException('Please, enter ' . self::$totalInstallments . ' surcharge values.');
				}

				for ($i=0;$i<count($multiple_surcharge_values);$i++) {
					if ($i == 0 && $multiple_surcharge_values[$i] != 0) {
						Mage::throwException('First surcharge value must be 0.');
					}
					elseif (self::$calculateInstallmentsOn == 1 && $multiple_surcharge_values[$i] < 0) {
						Mage::throwException('Surcharge Value must be at least 0.');
					}
					elseif (self::$calculateInstallmentsOn == 2 && ($multiple_surcharge_values[$i] < 0 || $multiple_surcharge_values[$i] > 100)) {
						Mage::throwException('Surcharge Value must be between 0 and 100.');
					}
				}
			}
		}

		if(isset($data['outofstock_discount_calculation_type'])) {
			self::$calculatePreOrderDiscountOn = $data['outofstock_discount_calculation_type'];

			if(isset($data['outofstock_discount_amount'])) {
				self::$preOrderDiscount = $data['outofstock_discount_amount'];

				if (self::$calculatePreOrderDiscountOn == 1 && self::$preOrderDiscount < 0) {
					Mage::throwException('Pre Order Discount value must be at least 0.');
				}
				elseif (self::$calculatePreOrderDiscountOn == 2 && (self::$preOrderDiscount < 0 || self::$preOrderDiscount > 100)) {
					Mage::throwException('Pre Order Discount value must be between 0 and 100.');
				}
			}
		}

		if(isset($data['default_credit_amount'])) {
			self::$defaultCreditAmount = $data['default_credit_amount'];
			if (self::$defaultCreditAmount < 0) {
				Mage::throwException('Default Credit Amount value must be at least 0.');
			}
		}

		if(isset($data['remind_before_days'])) {
			self::$remindBeforeDays = $data['remind_before_days'];

			if (self::$remindBeforeDays < 1) {
				Mage::throwException('Remind before days must be greater than 0.');
			}
		}

        return parent::save();
    }
}