<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Express extends Mage_Paypal_Model_Express
{
	protected function _placeOrder(Mage_Sales_Model_Order_Payment $payment, $amount)
    {
        $order = $payment->getOrder();

        // prepare api call
        $token = $payment->getAdditionalInformation(Mage_Paypal_Model_Express_Checkout::PAYMENT_INFO_TRANSPORT_TOKEN);
		if($order->getBasePaidAmount() > 0)
			$amount = $order->getBasePaidAmount();
        $api = $this->_pro->getApi()
            ->setToken($token)
            ->setPayerId($payment->
                getAdditionalInformation(Mage_Paypal_Model_Express_Checkout::PAYMENT_INFO_TRANSPORT_PAYER_ID))
            ->setAmount($amount)
            ->setPaymentAction($this->_pro->getConfig()->paymentAction)
            ->setNotifyUrl(Mage::getUrl('paypal/ipn/'))
            ->setInvNum($order->getIncrementId())
            ->setCurrencyCode($order->getBaseCurrencyCode())
            ->setPaypalCart(Mage::getModel('paypal/cart', array($order)))
            ->setIsLineItemsEnabled($this->_pro->getConfig()->lineItemsEnabled);
		
        // call api and get details from it
        $api->callDoExpressCheckoutPayment();

        $this->_importToPayment($api, $payment);
        return $this;
    }
}
