<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Api_Nvp extends Mage_Paypal_Model_Api_Nvp 
{
	protected $token;
	
	#Process a credit card payment
    public function callDoDirectPayment()
    {
        $request = $this->_exportToRequest($this->_doDirectPaymentRequest);
        $this->_exportLineItems($request);
        if ($this->getAddress()) {
            $request = $this->_importAddresses($request);
        }

        $request['ITEMAMT'] = 0;
		$request['TAXAMT']= 0 ;
		$request['SHIPPINGAMT'] = 0;

        $response = $this->call(self::DO_DIRECT_PAYMENT, $request);
        $this->_importFromResponse($this->_doDirectPaymentResponse, $response);
    }
	
	# autocapture with paypal pro and paypal express
	public function callInstallmentCapture($orderId,$amount,$referenceId,$invId)
    {
		$params = array(0 => 'paypal_direct');
		$configData = Mage::getModel('paypal/config',$params);
		$this->setConfigObject($configData);
		$request = $this->_exportToRequest($this->_doReferenceTransactionRequest);//set payment action
		try
		{
        	$response = $this->callInstallmnetPayment('DoReferenceTransaction',$orderId,$amount,$referenceId,$invId,$request);//call do installmnet payment method for send request
			return $response;
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
        $this->_importFromResponse($this->_doReferenceTransactionResponse, $response);
    }
	
	public function callInstallmnetPayment($methodName, $orderId, $amount,$referenceId,$invId, array $request)
	{
		$request = $this->_addMethodToRequest($methodName, $request);
        $eachCallRequest = $this->_prepareEachCallRequest($methodName);
        if ($this->getUseCertAuthentication()) {
            if ($key = array_search('SIGNATURE', $eachCallRequest)) {
                unset($eachCallRequest[$key]);
            }
        }
        $request = $this->_exportToRequest($eachCallRequest, $request);
		//Prepare request for capture payment
		$request['REFERENCEID'] = $referenceId;
		$request['AMT'] = $amount;
		$request['INVNUM'] = $invId;
		$request['ITEMAMT'] = $amount;
		$request['TAXAMT'] = '0.00';
		$request['SHIPPINGAMT'] = '0.00';
		
        $debugData = array('url' => $this->getApiEndpoint(), $methodName => $request);
        try {
			$http = new Varien_Http_Adapter_Curl();
            $config = array(
                'timeout'    => 100,
                'verifypeer' => $this->_config->verifyPeer
            );
            if ($this->getUseProxy()) {
                $config['proxy'] = $this->getProxyHost(). ':' . $this->getProxyPort();
            }
            if ($this->getUseCertAuthentication()) {
                $config['ssl_cert'] = $this->getApiCertificate();
            }
            $http->setConfig($config);
			
            $http->write(Zend_Http_Client::POST, $this->getApiEndpoint(), '1.1', array(), $this->_buildQuery($request));
            $response = $http->read();
			
        } catch (Exception $e) {
            $debugData['http_error'] = array('error' => $e->getMessage(), 'code' => $e->getCode());
            $this->_debug($debugData);
            throw $e;
        }

        $response = preg_split('/^\r?$/m', $response, 2);
		
        $response = trim($response[1]);
		
        $response = $this->_deformatNVP($response);
	    $debugData['response'] = $response;
        $this->_debug($debugData);

        // handle transport error
        if ($http->getErrno()) {
            Mage::logException(new Exception(
                sprintf('PayPal NVP CURL connection error #%s: %s', $http->getErrno(), $http->getError())
            ));
            $http->close();

            Mage::throwException(Mage::helper('paypal')->__('Unable to communicate with the PayPal gateway.'));
        }

        // cUrl resource must be closed after checking it for errors
        $http->close();

        if (!$this->_validateResponse($methodName, $response)) {
            Mage::logException(new Exception(
                Mage::helper('paypal')->__("PayPal response hasn't required fields.")
            ));
            Mage::throwException(Mage::helper('paypal')->__('There was an error processing your order. Please contact us or try again later.'));
        }

        $this->_callErrors = array();
		
        if ($this->_isCallSuccessful($response)) 
		{
		    if ($this->_rawResponseNeeded) 
			{
				$this->setRawSuccessResponseData($response);
			}
            return $response;
        }
        $this->_handleCallErrors($response);
		
        return $response;
	}
	
	#Express Checkout
	public function callSetExpressCheckout()//Pay downpayment with paypal exress
    {
		$this->_prepareExpressCheckoutCallRequest($this->_setExpressCheckoutRequest);
        $request = $this->_exportToRequest($this->_setExpressCheckoutRequest);
		
        $this->_exportLineItems($request);
		$calculationModel = Mage::getModel('partialpayment/calculation');
		foreach($calculationModel->getQuote()->getAllAddresses() as $address)
		{
			if($address->getAddressType() == $calculationModel->getShippingOrBilling())
			{
				if($address->getPaidAmount() > 0 && $address->getRemainingAmount() > 0)
				{
					$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
					$request['AMT'] = $partialpaymentHelper->setNumberFormat($address->getPaidAmount());
					$request['ITEMAMT'] = $partialpaymentHelper->setNumberFormat($address->getPaidAmount());
					if($partialpaymentHelper->isShippingAndTaxOnDownPayment())
					{
						$request['AMT'] += $request['TAXAMT'];
						$request['AMT'] += $request['SHIPPINGAMT'];
					}
					else
					{
						$request['TAXAMT'] = 0;
						$request['SHIPPINGAMT'] = 0;
					}
				}
			}
		}
		
        // import/suppress shipping address, if any
        $options = $this->getShippingOptions();
        if ($this->getAddress()) {
            $request = $this->_importAddresses($request);
            $request['ADDROVERRIDE'] = 1;
        } elseif ($options && (count($options) <= 10)) { // doesn't support more than 10 shipping options
            $request['CALLBACK'] = $this->getShippingOptionsCallbackUrl();
            $request['CALLBACKTIMEOUT'] = 6; // max value
            $request['MAXAMT'] = $request['AMT'] + 999.00; // it is impossible to calculate max amount
            $this->_exportShippingOptions($request);
        }

        // add recurring profiles information
        $i = 0;
        foreach ($this->_recurringPaymentProfiles as $profile) {
            $request["L_BILLINGTYPE{$i}"] = 'RecurringPayments';
            $request["L_BILLINGAGREEMENTDESCRIPTION{$i}"] = $profile->getScheduleDescription();
            $i++;
        }
        $response = $this->call(self::SET_EXPRESS_CHECKOUT, $request);
        $this->_importFromResponse($this->_setExpressCheckoutResponse, $response);
    }
	public function callDoExpressCheckoutPayment()//Pay downpayment with paypal exress
    {
        $this->_prepareExpressCheckoutCallRequest($this->_doExpressCheckoutPaymentRequest);
        $request = $this->_exportToRequest($this->_doExpressCheckoutPaymentRequest);
        $this->_exportLineItems($request);
		
		$calculationModel = Mage::getModel('partialpayment/calculation');
		foreach($calculationModel->getQuote()->getAllAddresses() as $address)
		{
			if($address->getAddressType() == $calculationModel->getShippingOrBilling())
			{
				if($address->getPaidAmount() > 0 && $address->getRemainingAmount() > 0)
				{
					$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
					
					$request['AMT'] = $partialpaymentHelper->setNumberFormat($address->getPaidAmount());
					$request['ITEMAMT'] = $partialpaymentHelper->setNumberFormat($address->getPaidAmount());
					if($partialpaymentHelper->isShippingAndTaxOnDownPayment())
					{
						$request['AMT'] += $request['TAXAMT'];
						$request['AMT'] += $request['SHIPPINGAMT'];
					}
					else
					{
						$request['TAXAMT'] = 0;
						$request['SHIPPINGAMT'] = 0;
					}
				}
			}
		}
		
        $response = $this->call(self::DO_EXPRESS_CHECKOUT_PAYMENT, $request);
        $this->_importFromResponse($this->_paymentInformationResponse, $response);
        $this->_importFromResponse($this->_doExpressCheckoutPaymentResponse, $response);
        $this->_importFromResponse($this->_createBillingAgreementResponse, $response);
    }
	public function callDoExpressInstallmentPayment($installmentId,$incrementId)//Pay installment with paypal exress
	{		
		$params = array(0 => 'paypal_express');
		$configData = Mage::getModel('paypal/config',$params);
		$this->setConfigObject($configData);
		$request = $this->_exportToRequest($this->_doExpressCheckoutPaymentRequest);//set payment action
		
		try
		{
        	$response = $this->callExpressInstallmentPayment(self::SET_EXPRESS_CHECKOUT,$request,$installmentId,$incrementId);//call do installmnet payment method for send request
			$this->token = $response['TOKEN'];
			return $response;
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
			return 'error';
		}
	}
	function callExpressInstallmentPayment($methodName, array $request, $installmentIds,$incrementId)//Pay installment with paypal exress
	{
		$order = Mage::getModel('sales/order')->load($incrementId);
		$billingAddress = $order->getBillingAddress();
		$billingAddressData = $billingAddress->getData();
		/* print_r($billingAddressData);
		exit; */
		$request = $this->_addMethodToRequest($methodName, $request);
        $eachCallRequest = $this->_prepareEachCallRequest($methodName);
        if ($this->getUseCertAuthentication()) {
            if ($key = array_search('SIGNATURE', $eachCallRequest)) {
                unset($eachCallRequest[$key]);
            }
        }
        $request = $this->_exportToRequest($eachCallRequest, $request);
		$amount = 0;
		$installmentids = array();
		$orderCurrency = NULL;
		foreach ($installmentIds as $instllmentid) {
			$installmentModel = Mage::getModel('partialpayment/installment')->load($instllmentid);
			$amount += $installmentModel->getInstallmentAmount();
			$installmentids[] = $instllmentid;
			if($orderCurrency == NULL)
			{
				$orderCurrency = Mage::getModel('sales/order')->load(Mage::getModel('partialpayment/partialpayment')->load($installmentModel->getPartialPaymentId())->getOrderId())->getBaseCurrencyCode();
			}
		}
		$current_magento_time = Mage::getModel('core/date')->date('m/d/Y~H:i:s');
		
		
		//Prepare request for capture payment
		$request['AMT'] = $amount;
		$request['INVNUM'] = $order->getIncrementId()."-".$current_magento_time;
		$request['ITEMAMT'] = '0.00';
		$request['TAXAMT'] = '0.00';
		$request['SHIPPINGAMT'] = '0.00';
		
        $request['CURRENCYCODE'] = $orderCurrency;
		$request['RETURNURL'] = Mage::getUrl('paypal/express/installmentReturn/', array('installmentId' => implode('~',$installmentids)));
		$request['CANCELURL'] = Mage::getUrl('paypal/express/installmentCancel/', array('installmentId' => implode('~',$installmentids)));
		$request['SOLUTIONTYPE'] = 'Mark';
		$request['GIROPAYCANCELURL'] = Mage::getUrl('paypal/express/installmentCancel/', array('installmentId' => implode('~',$installmentids)));
		$request['GIROPAYSUCCESSURL'] = Mage::getUrl('paypal/express/installmentSuccess/', array('installmentId' => implode('~',$installmentids)));
		$request['BANKTXNPENDINGURL'] = Mage::getUrl('paypal/express/installmentSuccess/', array('installmentId' => implode('~',$installmentids)));
		$request['LOCALECODE'] = 'en_US';
		$request['BILLINGTYPE'] = 'MerchantInitiatedBilling';
		
		if($billingAddressData['email']){
			$request['EMAIL'] = $billingAddressData['email'];
		}	
		if($billingAddressData['firstname']){
			$request['FIRSTNAME'] = $billingAddressData['firstname'];
		}
		if($billingAddressData['lastname']){
			$request['LASTNAME'] = $billingAddressData['lastname'];
		}
		if($billingAddressData['middlename']){
			$request['MIDDLENAME'] = $billingAddressData['middlename'];
		}	
		if($billingAddressData['suffix']){
			$request['SUFFIX'] = $billingAddressData['suffix'];
		}
		if($billingAddressData['country_id']){
			$request['COUNTRYCODE'] = $billingAddressData['country_id'];
		}
		if($billingAddressData['region_id']){
			$region = Mage::getModel('directory/region')->load($billingAddressData['region_id']);
			if ($region->getId()) {
				$request['STATE'] = $region->getCode();
			}
		}
		if($billingAddressData['city']){
			$request['CITY'] = $billingAddressData['city'];
		}
		if($billingAddressData['street']){
			$request['STREET'] = $billingAddressData['street'];
		}
		if($billingAddressData['postcode']){
			$request['ZIP'] = $billingAddressData['postcode'];
		}
		if($billingAddressData['telephone']){
			$request['PHONENUM'] = $billingAddressData['telephone'];
		}
		
		return $this->paypalCall($methodName,$request);
        
	}
	
	public function callGetExpressCheckoutDetailsForInstallment($token,$payerID)//Pay installment with paypal exress
    {
		$params = array(0 => 'paypal_express');
		$configData = Mage::getModel('paypal/config',$params);
		$this->setConfigObject($configData);
        $this->_prepareExpressCheckoutCallRequest($this->_getExpressCheckoutDetailsRequest);
		$request = $this->_exportToRequest($this->_getExpressCheckoutDetailsRequest);
		
		$response = $this->callGetExpressCheckoutDetailsForInstallmentPayment(self::GET_EXPRESS_CHECKOUT_DETAILS, $request,$token);
		return $response;
	}
	private function callGetExpressCheckoutDetailsForInstallmentPayment($methodName, array $request,$token)//Pay installment with paypal exress
	{
		$request = $this->_addMethodToRequest($methodName, $request);
        $eachCallRequest = $this->_prepareEachCallRequest($methodName);
        if ($this->getUseCertAuthentication()) {
            if ($key = array_search('SIGNATURE', $eachCallRequest)) {
                unset($eachCallRequest[$key]);
            }
        }
        $request = $this->_exportToRequest($eachCallRequest, $request);
		
		//Prepare request for capture payment
		$request['TOKEN'] = $token;
		try
		{
			return $this->paypalCall($methodName,$request);
		}
		catch(Exception $e)
		{
			Mage::log($e->getMessage());
		}
	}
	
	public function callDoExpressCheckoutPaymentForInstallment($token,$payerID, $installmentIds)//Pay installment with paypal exress
    {
		$params = array(0 => 'paypal_express');
		$configData = Mage::getModel('paypal/config',$params);
		$this->setConfigObject($configData);
        $this->_prepareExpressCheckoutCallRequest($this->_doExpressCheckoutPaymentRequest);
		$request = $this->_exportToRequest($this->_doExpressCheckoutPaymentRequest);
		$response = $this->callDoExpressCheckoutPaymentForInstallmentPayment(self::DO_EXPRESS_CHECKOUT_PAYMENT, $request, $token, $payerID, $installmentIds);
		return $response;
	}
	private function callDoExpressCheckoutPaymentForInstallmentPayment($methodName, array $request,$token,$payerID,$installmentIds)//Pay installment with paypal exress
	{
		$this->_prepareExpressCheckoutCallRequest($this->_doExpressCheckoutPaymentRequest);
        $request = $this->_exportToRequest($this->_doExpressCheckoutPaymentRequest);
		
		$request = $this->_addMethodToRequest($methodName, $request);
        $eachCallRequest = $this->_prepareEachCallRequest($methodName);
        if ($this->getUseCertAuthentication()) {
            if ($key = array_search('SIGNATURE', $eachCallRequest)) {
                unset($eachCallRequest[$key]);
            }
        }
		$amount = 0;
		$orderCurrency = NULL;
		foreach ($installmentIds as $instllmentid) {
			$installmentModel = Mage::getModel('partialpayment/installment')->load($instllmentid);
			$amount += $installmentModel->getInstallmentAmount();
			if($orderCurrency == NULL)
			{
				$orderCurrency = Mage::getModel('sales/order')->load(Mage::getModel('partialpayment/partialpayment')->load($installmentModel->getPartialPaymentId())->getOrderId())->getBaseCurrencyCode();
			}
		}
		
        $request = $this->_exportToRequest($eachCallRequest, $request);
		$request['TOKEN'] = $token;
		$request['PAYERID'] = $payerID;
		$request['AMT'] = $amount;
		$request['CURRENCYCODE'] = $orderCurrency;
		$request['NOTIFYURL'] = Mage::getUrl('paypal/ipn/');
		$request['ITEMAMT'] = $amount;
		$request['TAXAMT'] = '0.00';
		$request['SHIPPINGAMT'] = '0.00';
		
		try
		{
			return $this->paypalCall($methodName,$request);
		}
		catch(Exception $e)
		{
			Mage::log($e->getMessage());
		}
        
	}
	
	// custom call to pay installment
	private function paypalCall($methodName, $request)
	{
		$debugData = array('url' => $this->getApiEndpoint(), $methodName => $request);
        try {
			$http = new Varien_Http_Adapter_Curl();
            $config = array(
                'timeout'    => 100,
                'verifypeer' => $this->_config->verifyPeer
            );
            if ($this->getUseProxy()) {
                $config['proxy'] = $this->getProxyHost(). ':' . $this->getProxyPort();
            }
            if ($this->getUseCertAuthentication()) {
                $config['ssl_cert'] = $this->getApiCertificate();
            }
            $http->setConfig($config);
			
            $http->write(Zend_Http_Client::POST, $this->getApiEndpoint(), '1.1', array(), $this->_buildQuery($request));
            $response = $http->read();
        } catch (Exception $e) {
            $debugData['http_error'] = array('error' => $e->getMessage(), 'code' => $e->getCode());
            $this->_debug($debugData);
            throw $e;
        }

        $response = preg_split('/^\r?$/m', $response, 2);
		
        $response = trim($response[1]);
		
        $response = $this->_deformatNVP($response);
	    $debugData['response'] = $response;
        $this->_debug($debugData);

        // handle transport error
        if ($http->getErrno()) {
            Mage::logException(new Exception(
                sprintf('PayPal NVP CURL connection error #%s: %s', $http->getErrno(), $http->getError())
            ));
            $http->close();

            Mage::throwException(Mage::helper('paypal')->__('Unable to communicate with the PayPal gateway.'));
        }

        // cUrl resource must be closed after checking it for errors
        $http->close();

        if (!$this->_validateResponse($methodName, $response)) {
            Mage::logException(new Exception(
                Mage::helper('paypal')->__("PayPal response hasn't required fields.")
            ));
            Mage::throwException(Mage::helper('paypal')->__('There was an error processing your order. Please contact us or try again later.'));
        }

        $this->_callErrors = array();
		
        if ($this->_isCallSuccessful($response)) 
		{
		    if ($this->_rawResponseNeeded) 
			{
				$this->setRawSuccessResponseData($response);
			}
            return $response;
        }
        $this->_handleCallErrors($response);
		
        return $response;
	}
}
