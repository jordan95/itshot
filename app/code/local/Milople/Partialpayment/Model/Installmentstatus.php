<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/magento-extensions/contacts/
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2015 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Installmentstatus
{
    public function toOptionArray()
    {
        return array(
			'Paid' 		=> Mage::helper('partialpayment')->__('Paid'),
            'Remaining' => Mage::helper('partialpayment')->__('Remaining'),
            'Canceled'	=> Mage::helper('partialpayment')->__('Canceled'),
            'Failed' 	=> Mage::helper('partialpayment')->__('Failed')
        );
    }
}