<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Observer extends Mage_Core_Model_Config_Data
{
	static $partial_payment_helper;
	static $calculationModel;
	public static $profileId;
	public static $realId;
	
	public function beforeAddToCart ($observer)
    {
		$request = Mage::app()->getFrontController()->getRequest();
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		$productId = $request->getParam('product');
		$product = Mage::getModel('catalog/product')->load($productId);
		
		if(!isset($_POST['allow_partial_payment']))
		{
			/*$enable = $partialpaymentHelper->isEnabled();
			if ($enable)
			{
				if ($partialpaymentHelper->isPartialPaymentForAllProduct())
				{
					if ($partialpaymentHelper->isAllowFullPayment())
					{
						$url = $product->getProductUrl()."?options=cart";
						Mage::app()->getFrontController()->getResponse()->setRedirect($url);
						Mage::app()->getFrontController()->getResponse()->sendResponse(); 
						exit;	
					}
					else
					{
						if($enable)
						$request->setParam('allow_partial_payment', 1);
						else
						$request->setParam('surcharge_installments', 0);
						
					}
				}
				else if ($partialpaymentHelper->isPartialPaymentForSpecificProduct() && $product->getApplyPartialPayment())
				{
					if ($partialpaymentHelper->isAllowFullPayment($product)) 
					{
							$url = $product->getProductUrl()."?options=cart";
							Mage::app()->getFrontController()->getResponse()->setRedirect($url);
							Mage::app()->getFrontController()->getResponse()->sendResponse(); 
							exit;						
					}
					else
					{
						if($enable)
						$request->setParam('allow_partial_payment', 1);
						else
						$request->setParam('surcharge_installments', 0);
					}
				}
			}*/
		}
		else
		{
			if($partialpaymentHelper->isPartialPaymentForSpecificProduct() && !$product->getApplyPartialPayment())
			{
				if($product->getTypeId()=='grouped')
				{
					$grouped_product_model = Mage::getModel('catalog/product_type_grouped');
					$groupedParentId = $grouped_product_model->getParentIdsByChild($product->getId());
					$_associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);
					
					foreach($_associatedProducts as $_associatedProduct) 
					{
						if($_associatedProduct->getApplyPartialPayment() && $_associatedProduct->isVisibleInSiteVisibility())
						{
							$url = $_associatedProduct->getProductUrl()."?options=cart";
							Mage::app()->getFrontController()->getResponse()->setRedirect($url);
							Mage::app()->getFrontController()->getResponse()->sendResponse(); 
							exit;
						}
					}
				}
			}
		}
	}
	
	public function AddToCartAfter(Varien_Event_Observer $observer)
	{
		$postdata = Mage::app()->getRequest()->getPost(); // get Post data
		try
		{
			if (isset($postdata['super_group'])) 
			{
				if (isset($postdata['allow_partial_payment'])){
					$session = Mage::getSingleton("core/session",  array("name"=>"frontend"));
					$session->setData($postdata['product'], $postdata['allow_partial_payment']);
				}
			}
			if(isset($postdata['allow_partial_payment']) && $postdata['allow_partial_payment']==1) //check partial payment is selected or not in frontend
			{
				$product = Mage::getModel('catalog/product')->load($postdata['product']);
				$product->addCustomOption('allow_partial_payment', $postdata['allow_partial_payment']);//set value of partial payment allow in infoByRequest
			}
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function setWholeCartSession($observer)
	{
		$_POST = Mage::app()->getRequest()->getPost();
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		$optional=$partialpaymentHelper->isAllowFullPayment();
		$isFlexyAllow = $partialpaymentHelper->isAllowFlexyPayment();
		if($partialpaymentHelper->isPartialPaymentForWholecart())
		{
			if(isset($_POST['allow_partial_payment']))
			{
			  Mage::getSingleton('core/session')->setAllowPartialPayment($_POST['allow_partial_payment']);
			}
			elseif (!$optional && !$isFlexyAllow)
			{
			  Mage::getSingleton('core/session')->setAllowPartialPayment(1);
			}
		}
	}
	
	public function removeWholeCartSession($observer)
	{
		$quote = $observer->getEvent()->getQuote();
		if($quote->getItemsCount() == 0) {
			Mage::getSingleton('core/session')->setAllowPartialPayment(0);
			Mage::getSingleton('core/session')->unsetAllowPartialPayment();
		}
	}
	
	// to correct grand total 
	public function setCorrectTax($observer)
                {
                    $calculationModel = Mage::getModel("partialpayment/calculation");
                    $partial_payment_helper = Mage::helper('partialpayment/partialpayment');
                    $valid = $partial_payment_helper->isValidCustomer();
                    try {
                        $quote = $observer->getQuote();
                        foreach ($quote->getAllAddresses() as $address) {
                            if (($partial_payment_helper->isEnabled() || $partial_payment_helper->isAdminEnabled()) && $address->getAddressType() == $calculationModel->getShippingOrBilling($quote)) {
                                $surcharge = $calculationModel->getTotalSurchargeAmount($quote);
                                $outofstockdiscount = $calculationModel->getTotalOutofstockdiscountAmount($quote);
                                $extraBaseAmounts = $address->getBaseDiscountAmount() + $address->getBaseTaxAmount() + $address->getBaseShippingAmount() + $surcharge + $outofstockdiscount;
                                if (($partial_payment_helper->isPartialPaymentForWholecart() && $partial_payment_helper->getMinimumWholecartOrderAmount() > $address->getGrandTotal()) || $valid != 1) {
                                    $extraAmounts = $address->getDiscountAmount() + $address->getTaxAmount() + $address->getShippingAmount();
                                    $baseExtraAmounts = $address->getBaseDiscountAmount() + $address->getBaseTaxAmount() + $address->getBaseShippingAmount();
                                    if($address->getCreditAmount()){
                                        $extraAmounts -= $address->getCreditAmount();
                                    }
                                    if($address->getBaseCreditAmount()){
                                        $baseExtraAmounts -= $address->getBaseCreditAmount();
                                    }
                                    if ($partial_payment_helper->isShippingIncludingTax()) {
                                        $grandTotal = $address->getSubtotal() + ($extraAmounts - $address->getShippingTaxAmount());
                                        $baseGrandTotal = $address->getBaseSubtotal() + ($baseExtraAmounts - $address->getBaseShippingTaxAmount());
                                    } else {
                                        $grandTotal = $address->getSubtotal() + $extraAmounts;
                                        $baseGrandTotal = $address->getBaseSubtotal() + $baseExtraAmounts;
                                    }

                                    $address->setGrandTotal($grandTotal);
                                    $address->setBaseGrandTotal($baseGrandTotal);
                                } else {
                                    $extraAmounts = $address->getDiscountAmount() + $address->getTaxAmount() + $address->getShippingAmount() + $partial_payment_helper->convertToCurrentCurrencyAmount($surcharge + $outofstockdiscount);
                                    if($address->getCreditAmount()){
                                        $extraAmounts -= $address->getCreditAmount();
                                    }
                                    if($address->getBaseCreditAmount()){
                                        $extraBaseAmounts -= $address->getBaseCreditAmount();
                                    }
                                    if ($partial_payment_helper->isShippingIncludingTax()) {
                                        $baseGrandTotal = $address->getBaseSubtotal() + ($extraBaseAmounts - $address->getBaseShippingTaxAmount());
                                        $grandTotal = $address->getSubtotal() + ($extraAmounts - $address->getShippingTaxAmount());
                                    } else {
                                        $baseGrandTotal = $address->getBaseSubtotal() + $extraBaseAmounts;
                                        $grandTotal = $address->getSubtotal() + $extraAmounts;
                                    }

                                    $address->setBaseGrandTotal($baseGrandTotal);
                                    $address->setGrandTotal($grandTotal);
                                }
                                $paid_amount = $calculationModel->getPayingNowAmount($quote);
                                if ($paid_amount == $address->getBaseSubtotal()) {
                                    $paid_amount = 0;
                                }
                                // set paid amount
                                $allow = 0;
                                if ($partial_payment_helper->isPartialPaymentForWholecart()) {
                                    if (Mage::getSingleton('core/session')->getAllowPartialPayment() == "") {
                                        $allow = 1;
                                    } else if (Mage::getSingleton('core/session')->getAllowPartialPayment() == 0) {
                                        $allow = 1;
                                    }
                                }

                                if (($partial_payment_helper->isShippingAndTaxOnDownPayment() || $allow) && $paid_amount > 0) {
                                    $paid_amount += $extraBaseAmounts - $address->getShippingTaxAmount() - $address->getDiscountAmount();
                                }
                                if (($partial_payment_helper->isDiscountOnDownPayment() || $allow) && $paid_amount > 0) {
                                    $paid_amount += $address->getDiscountAmount();
                                }
                                $order = Mage::registry('ordersedit_order');
                                if (!$order) {
                                    $address->setPaidAmount($partial_payment_helper->convertToCurrentCurrencyAmount($paid_amount));
                                }else {
                                    $address->setPaidAmount($order->getPaidAmount());
                                    $address->setBasePaidAmount($order->getBasePaidAmount());
                                }
                                // set remaining amount
                                if ($address->getBaseSubtotal() != 0 && (($address->getGrandTotal() - $extraAmounts) != $address->getPaidAmount())) {
                                    $address->setSurchargeAmount($partial_payment_helper->convertToCurrentCurrencyAmount($surcharge));
                                    $address->setOutofstockdiscountAmount($partial_payment_helper->convertToCurrentCurrencyAmount($outofstockdiscount));
                                    $address->setRemainingAmount($address->getGrandTotal() - $address->getPaidAmount());
                                }
                            } else {
                                $extraAmounts = $address->getDiscountAmount() + $address->getTaxAmount() + $address->getShippingAmount();
                                $baseExtraAmounts = $address->getBaseDiscountAmount() + $address->getBaseTaxAmount() + $address->getBaseShippingAmount();
                                
                                if($address->getCreditAmount()){
                                    $extraAmounts -= $address->getCreditAmount();
                                }
                                if($address->getBaseCreditAmount()){
                                    $baseExtraAmounts -= $address->getBaseCreditAmount();
                                }
                                $grandTotal = $address->getSubtotal() + ($extraAmounts - $address->getShippingTaxAmount());
                                $baseGrandTotal = $address->getBaseSubtotal() + ($baseExtraAmounts - $address->getBaseShippingTaxAmount());                                
                                $address->setGrandTotal($grandTotal);
                                $address->setBaseGrandTotal($baseExtraAmounts);
                            }
                        }
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }
                }

    public function isCreditLimitExceeded($observer)
	{
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		$order = $observer->getEvent()->getOrder();
		$currency_code = $order->getOrderCurrency()->getCurrencyCode(); // fetch order currency
		$defaultCreditAmount = $partialpaymentHelper->getDefaultCreditAmount();  //default credit
		$orderId = $order->getId();

		$currentBaseRemainingAmount = $partialpaymentHelper->convertOrderCurrencyAmount($order->getRemainingAmount(),$currency_code);    //used to fetch fee amount
		$currentBasePaidAmount = $partialpaymentHelper->convertOrderCurrencyAmount($order->getPaidAmount(),$currency_code);    //used to fetch fee amount

		if($orderId!=NULL || !$partialpaymentHelper->isValidCustomer())// first condition checks that is it installment pay or second for avoid check limit for invalid customer
		{
			return true;
		}

		if($currentBaseRemainingAmount > 0 && $currentBasePaidAmount > 0)//if current order has remaining amount
		{
			$customer_id = $order->getCustomerId();
			if ($customer_id)//if customer is login
			{
				$credit_limit= 0;
				$customer = Mage::getModel('customer/customer')->load($customer_id);
				$credit_amount = $customer->getCreditAmount();  //get particular customer's credit_amt
				if ($credit_amount >= 0  && $credit_amount != '')//if customers amount is set
				{
					 $credit_limit = $credit_amount;
				}
				elseif ($defaultCreditAmount >= 0 && $defaultCreditAmount != '')//else if customer creadit not set then check default creadit
				{
					$credit_limit = $defaultCreditAmount;
				}
				else 
				{
					return true;
				}

				$collection = Mage::getModel('sales/order')->getCollection()
						  ->addFieldToFilter('customer_id',$customer->getId())
						  ->addFieldToFilter('status',array('nin' => array('canceled','closed')));			  

				$orders_total = $currentBaseRemainingAmount;
				foreach($collection as $order)
				{
					$orders_total += $order->getBaseRemainingAmount();
				}

				if($credit_limit > $orders_total)
				{
					return true ;
				}
				else
				{	
					Mage::throwException($partialpaymentHelper->getCreditLimitSurpassMessage($partialpaymentHelper->convertToCurrentCurrencyAmount($credit_limit), $currency_code));
				}
			}
			else
			{
				if($defaultCreditAmount != '')
				{ 
					if($currentBaseRemainingAmount > $defaultCreditAmount)
					{
						Mage::getSingleton('core/session', array('name' => 'adminhtml')); 
						$session = Mage::getSingleton('admin/session');
						if (!$session->isLoggedIn())
						{
							Mage::throwException($preorderHelper->getCreditLimitSurpassMessage($partialpaymentHelper->convertToCurrentCurrencyAmount($defaultCreditAmount), $currency_code));
						}
					}
				}
				else
				{
					return true;
				}
			}
		}
		else 
		{
			return true ;
		}
	}


	public function salesOrderSaveBefore ($observer)
	{
		$this->isCreditLimitExceeded($observer);
		try
		{
			$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
			$calculationModel = Mage::getModel("partialpayment/calculation");
			$order = $observer->getEvent()->getOrder();
			$getOrderCurrency = $order->getOrderCurrencyCode();
			
			$order->setBaseSubtotal($partialpaymentHelper->convertOrderCurrencyAmount($order->getSubtotal(),$getOrderCurrency));
			$order->setBaseSubtotalInclTax($partialpaymentHelper->convertOrderCurrencyAmount($order->getSubtotalInclTax(),$getOrderCurrency));
			$order->setBaseGrandTotal($partialpaymentHelper->convertOrderCurrencyAmount($order->getGrandTotal(),$getOrderCurrency));
			$order->setBasePaidAmount($partialpaymentHelper->convertOrderCurrencyAmount($order->getPaidAmount(),$getOrderCurrency));
			$order->setBaseRemainingAmount($partialpaymentHelper->convertOrderCurrencyAmount($order->getRemainingAmount(),$getOrderCurrency));
			$order->setBaseSurchargeAmount($partialpaymentHelper->convertOrderCurrencyAmount($order->getSurchargeAmount(),$getOrderCurrency));	
			
			$order->setBaseOutofstockdiscountAmount($partialpaymentHelper->convertOrderCurrencyAmount($order->getOutofstockdiscountAmount(),$getOrderCurrency));
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}
	 public function onepageCheckoutSaveOrderBefore($observer)
    {
        $quote = $observer->getQuote();
        $order = $observer->getOrder();
        $havePPItems = false;

        if (Mage::helper('partialpayment/partialpayment')->isPartialPaymentForWholecart()) {
            if (Mage::getSingleton('core/session')->getAllowPartialPayment()) {
                $havePPItems = true;
            }
        } else {
            foreach ($quote->getAllItems() as $item) {
                $infoBuyRequest = $item->getOptionByCode('info_buyRequest');
                $buyRequest = new Varien_Object(unserialize($infoBuyRequest->getValue()));

                if ($buyRequest->getAllowPartialPayment()) {
                    $havePPItems = true;
                    break;
                }
            }
        }
        if ($havePPItems) {
            switch ($order->getPayment()->getMethod()) {
                case Milople_Partialpayment_Model_Payment_Method_Authorizenet::PAYMENT_METHOD_CODE:
                    $paymentModel = Mage::getModel('partialpayment/payment_method_authorizenet');
                    $service = $paymentModel->getWebService();
                    $service->setPayment($quote->getPayment());
                    $subscription = new Varien_Object();
                    $subscription->setStoreId($order->getStoreId());
                    $service->setSubscription($subscription);
                    try {
                        $data = $service->createCIMAccount();
                        self::$profileId = $data->customerProfileId;
                        self::$realId = $data->customerPaymentProfileId;
                    } catch (Exception $e) {
                        Mage::log($e->getMessage(), null, 'log_partial_error.log');
                        throw new Mage_Core_Exception($e->getMessage());
                    }
                    break;
                default:
            }
        }
    }

    public function salesOrderPlaced($observer)//it will store data in partial payment table
	{	
		$calculationModel = Mage::getModel("partialpayment/calculation");
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		$order = $observer->getEvent()->getOrder();
		$partial_payment_id=0;
		$payment_method=NULL;
		
		try
		{
			if($calculationModel->getAmountToBePaidLater() > 0)
			{
				$paid_amount = $order->getBasePaidAmount();
				$remaining_amount = $order->getBaseRemainingAmount();
						
				$order_id = $order->getEntityId();
				$total_amount = $order->getBaseGrandTotal();
				$payment_method = $order->getPayment()->getMethod();
						
				$paid_installments = 1;
				$total_installments = 0;
				$items = $order->getQuote()->getAllVisibleItems();
				$installmentValue = 0;
				$total_price = 0;
				$profId=NULL;
				$rId=NULL;
				if($partialpaymentHelper->isPartialPaymentForWholecart())
				{
					if($partialpaymentHelper->isAllowFlexyPayment())
					{
						$total_installments = Mage::getSingleton('core/session')->getAllowPartialPayment();
					}
					else
					{
						$total_installments = $partialpaymentHelper->getTotalIinstallments();
					}
					$total_price += $order->getBaseSubtotal();
				}
				else
				{
					foreach ($items as $item) 
					{
						$product = Mage::getModel('catalog/product')->load($item->getProductId());
						if($partialpaymentHelper->getAllowPartialPaymentFromInfo($item))
						{
							if($partialpaymentHelper->isAllowFlexyPayment($product) && $partialpaymentHelper->getTotalIinstallments($product) > $total_installments)
							{
								$total_installments = $partialpaymentHelper->getAllowPartialPaymentFromInfo($item);
							}
							else if($partialpaymentHelper->getTotalIinstallments($product) > $total_installments)
							{
								$total_installments = $partialpaymentHelper->getTotalIinstallments($product);
							}
						}
					}
				}
				$remaining_installments = $total_installments - $paid_installments;
				
				$payment_method = $order->getPayment()->getMethod();

				if($payment_method != 'paypal_standard' || $payment_method != 'ccavenuepay' || $payment_method != 'payucheckout_shared')
				{
					$partial_payment_status = 'Processing';
				}
				else 
				{
					$partial_payment_status = 'Pending';
				}
								
				if($payment_method == 'paypal_standard' || $payment_method == 'ccavenuepay' || $payment_method == 'payucheckout_shared')
				{
					$remaining_amount = $total_amount;
					$paid_amount = 0;
					$remaining_installments = $total_installments;
					$paid_installments = 0;
				}
				
				$installmentValue = 0;
				if($partialpaymentHelper->isPartialPaymentForOutOfStockProducts())
				{
					$is_preordered = 1;
				}
				else
				{
					$is_preordered = 0;
				}
				$partialpaymentModelData = array(
				'order_id' => $order_id,
				'is_preordered' => $is_preordered,
				'total_installments' =>$total_installments,
				'paid_installments' => $paid_installments,
				'remaining_installments' =>$remaining_installments,
				'total_amount' => $partialpaymentHelper->setNumberFormat($total_amount),
				'paid_amount' => $partialpaymentHelper->setNumberFormat($paid_amount),
				'remaining_amount' => $partialpaymentHelper->setNumberFormat($remaining_amount),
				'auto_capture_profile_id' => self::$profileId,
				'auto_capture_payment_profile_id' => self::$realId
				);
				$partialpaymentModel = Mage::getModel('partialpayment/partialpayment')->setData($partialpaymentModelData)->save();
				
				$partial_payment_id = $partialpaymentModel->getId();
				foreach ($items as $item) 
				{
					$item_id = $item->getId();
					$product_total_installments = 1;
					$product_paid_installments = 1;
					$product_remaining_installments = 0;
					$_product = Mage::getModel('catalog/product')->load($item->getProductId());
					$product_total_amount = Mage::helper('tax')->getPrice($_product, $item->getPrice(), false );
					$product_paid_amount = $item->getPrice();//for non partial payment product
					$product_remaining_amount = 0;//for non partial payment product
					if(($partialpaymentHelper->isPartialPaymentForSpecificProduct() ||$partialpaymentHelper->isPartialPaymentForAllProduct() || $partialpaymentHelper->isPartialPaymentForOutOfStockProducts()) && $partialpaymentHelper->getAllowPartialPaymentFromInfo($item))//if partial payment selected for product 
					{
						if($partialpaymentHelper->isAllowFlexyPayment($_product))
						{
							$product_total_installments = $partialpaymentHelper->getAllowPartialPaymentFromInfo($item);
							$product_paid_amount = $calculationModel->getDownPaymentAmount($product_total_amount,$_product,$product_total_installments);
						}
						else
						{
							$product_total_installments = $partialpaymentHelper->getTotalIinstallments($_product);
							$product_paid_amount = $calculationModel->getDownPaymentAmount($product_total_amount,$_product);
						}
						$product_remaining_installments = $product_total_installments - $product_paid_installments;
						$product_remaining_amount = $product_total_amount - $product_paid_amount;
					}
					else if($partialpaymentHelper->isPartialPaymentForWholecart())//if whole cart
					{
						if($partialpaymentHelper->isAllowFlexyPayment($_product))
						{
							$product_total_installments = $total_installments;
							$product_remaining_installments = $product_total_installments - $product_paid_installments;
							$product_paid_amount =  $calculationModel->getDownPaymentAmount($product_total_amount,null,$product_total_installments);
							$product_remaining_amount = $product_total_amount - $product_paid_amount;
						}
						else
						{ 
							$product_total_installments = $total_installments;
							$product_remaining_installments = $product_total_installments - $product_paid_installments;
							$product_paid_amount =  $calculationModel->getDownPaymentAmount($product_total_amount,null,$product_total_installments);
							$product_remaining_amount = $product_total_amount - $product_paid_amount;
						}
					}
					$product_total_amount *= $item->getQty();
					$product_paid_amount *= $item->getQty();
					$product_remaining_amount *= $item->getQty();
					if($payment_method == 'paypal_standard' || $payment_method == 'ccavenuepay' || $payment_method == 'payucheckout_shared')
					{
					$productModelData = array(
						'partial_payment_id'=>$partial_payment_id,
						'sales_flat_order_item_id'=> $item_id,
						'downpayment'=> $partialpaymentHelper->setNumberFormat($product_paid_amount),
						'total_installments'=>$product_total_installments,
						'paid_installments'=>0,
						'remaining_installments'=>$product_total_installments,
						'total_amount'=>$partialpaymentHelper->setNumberFormat($product_total_amount),
						'paid_amount'=>0,
						'remaining_amount'=>$partialpaymentHelper->setNumberFormat($product_total_amount)
						);
					}
					else
					{
						$productModelData = array(
						'partial_payment_id'=>$partial_payment_id,
						'sales_flat_order_item_id'=> $item_id,
						'downpayment'=> $partialpaymentHelper->setNumberFormat($product_paid_amount),
						'total_installments'=>$product_total_installments,
						'paid_installments'=>$product_paid_installments,
						'remaining_installments'=>$product_remaining_installments,
						'total_amount'=>$partialpaymentHelper->setNumberFormat($product_total_amount),
						'paid_amount'=>$partialpaymentHelper->setNumberFormat($product_paid_amount),
						'remaining_amount'=>$partialpaymentHelper->setNumberFormat($product_remaining_amount)
						);
					}
					$productModel = Mage::getModel('partialpayment/product')->setData($productModelData)->save();
				}
				/* set installments */
				$transactionId = NULL;
				$currentTimestamp = Mage::getModel('core/date')->timestamp(time());
				$installment_due_date = new Zend_Date($currentTimestamp);
				
				$tax = $order->getTaxAmount();
				$shipping_tax = 0;

				if($tax > 0) {
					$shipping_tax = (float) $order->getShippingAmount() + $tax;
				}
				else {
					$shipping_tax = (float) $order->getShippingAmount();
				}
				$discount = abs($order->getDiscountAmount());
				$shipping_tax += $order->getSurchageAmount();
				$totlaremainngamount = 0;
				$grand_total = $order->getBaseGrandTotal();
				for ($i=1;$i<=$total_installments;$i++) 
				{
					if ($i == 1)
					{
						$installmentAmount = $partialpaymentHelper->setNumberFormat($order->getBasePaidAmount());
						if($payment_method == 'paypal_standard'||$payment_method == 'ccavenuepay' || $payment_method == 'payucheckout_shared')
						{
							$installmentModelData = array('partial_payment_id' => $partial_payment_id, 'installment_amount' => $installmentAmount, 'installment_due_date' => $installment_due_date->toString('yyyy-MM-dd'), 'installment_status' => 'Remaining');
						}
						else
						{ 
							$installmentModelData = array('partial_payment_id' => $partial_payment_id, 'installment_amount' => $installmentAmount, 'installment_due_date' => $installment_due_date->toString('yyyy-MM-dd'), 'installment_paid_date' => $installment_due_date->toString('yyyy-MM-dd'), 'installment_status' => 'Paid', 'payment_method' => $payment_method, 'txn_id' => $transactionId);
						}
						$totlaremainngamount += $installmentAmount;
					}
					else 
					{
						$installmentAmount = 0;
						if($partialpaymentHelper->isPartialPaymentForWholecart())
						{
							if($i <= $partialpaymentHelper->getTotalIinstallments())
							{
								$installmentAmount = $partialpaymentHelper->setNumberFormat($order->getBaseRemainingAmount()/($total_installments-1));
							}
						}
						else
						{
							foreach ($items as $item) 
							{
								$_product = Mage::getModel('catalog/product')->load($item->getProductId());
								$price = $item->getRowTotal();
								if($partialpaymentHelper->getAllowPartialPaymentFromInfo($item))//if partial payment selected for product 
								{
									if($partialpaymentHelper->isAllowFlexyPayment($_product))
									{
										if($i <= $partialpaymentHelper->getAllowPartialPaymentFromInfo($item))
										{
											$installmentAmount += $partialpaymentHelper->setNumberFormat($calculationModel->getInstallments($price,$_product,$partialpaymentHelper->getAllowPartialPaymentFromInfo($item)));
										}
									}
									else if($i <= $partialpaymentHelper->getTotalIinstallments($_product))
									{
										$installmentAmount += $partialpaymentHelper->setNumberFormat($calculationModel->getInstallments($price,$_product));
									}
								}
							}						
							if(!$partialpaymentHelper->isShippingAndTaxOnDownPayment())
							{
								$installmentAmount += $partialpaymentHelper->setNumberFormat($shipping_tax/($total_installments-1));
							}
							if(!$partialpaymentHelper->isDiscountOnDownPayment())
							{
								$installmentAmount -= $partialpaymentHelper->setNumberFormat($discount/($total_installments-1));
							}
						}
						
						if($i==$total_installments)
						{
							$installmentAmount = $grand_total - $totlaremainngamount;
						}
						else
						{
							$totlaremainngamount += $installmentAmount;
						}
						// next installment date calculation
						$currentTimestamp = Mage::getModel('core/date')->timestamp(time());
						$date = new Zend_Date($currentTimestamp);
						
						if($partialpaymentHelper->getPaymentplan()==1) 
						{						
							$date->addMonth($i-1);
						} 
						else if ($partialpaymentHelper->getPaymentplan()==2) 
						{
							$date->addWeek($i-1);
						} 
						else if ($partialpaymentHelper->getPaymentplan()==3) 
						{
							$days = $partialpaymentHelper->getNoOfDays() * ($i-1); 			
							$date->addDay($days);
						}
						
						$installmentModelData = array('partial_payment_id' => $partial_payment_id, 'installment_amount' => $installmentAmount, 'installment_due_date' => $date->toString('yyyy-MM-dd'),'installment_status' => 'Remaining');
					}
					$installmentModel = Mage::getModel('partialpayment/installment')->setData($installmentModelData)->save();
				}
				
				$currency_code = $order->getOrderCurrency()->getCurrencyCode();
				
				if($payment_method != 'paypal_standard' && $payment_method != 'ccavenuepay' && $payment_method != 'payucheckout_shared')
				{
					if($order->getCustomerId())
					{
						$customer_first_name = $order->getCustomerFirstname();
						$customer_email = $order->getCustomerEmail();
					}
					else
					{
						$customer_first_name = $order->getBillingAddress()->getFirstname();
						$customer_email = $order->getBillingAddress()->getEmail();
					}
					$partial_payment_emailsender=Mage::helper('partialpayment/emailsender');
					$partial_payment_emailsender->sendEmailSuccess($customer_first_name, $customer_email, $order->getIncrementId(), $partial_payment_id, $currency_code);
				}
			}
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
		if($partialpaymentHelper->isPartialPaymentForWholecart())
			Mage::getSingleton('core/session')->unsAllowPartialPayment();
	}

	public function stockAvailibityMailSender(Varien_Event_Observer $observer)
	{
		$partial_payment_id_with_preorder = Mage::getModel("partialpayment/partialpayment")->getCollection()->addFieldToFilter("is_preordered",1)->getData();
		$product_id = $observer->getProduct()->getId();
		$product_name = $observer->getProduct()->getName();
		$stock = $observer->getProduct()->isSaleable();
		$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($observer->getProduct());

		if($stock->getQty()>0)
		{
			foreach($partial_payment_id_with_preorder as $data)
			{
				$order = Mage::getModel('sales/order')->load($data['order_id']);
				$currency_code = $order->getOrderCurrency()->getCurrencyCode();
				$product_datas = Mage::getModel("partialpayment/product")->getCollection()->addFieldToFilter("partial_payment_id",$data['partial_payment_id'])->getData();
				foreach($product_datas as $product_data)
				{
					$quote_item = Mage::getModel('sales/order_item')->load($product_data['sales_flat_order_item_id'],'quote_item_id');
					$due_date='';
					if($product_data['remaining_installments'] > 0)
					{
						$product_installments = Mage::getModel("partialpayment/installment")->getCollection()->addFieldToFilter("partial_payment_id",$data['partial_payment_id'])->addFieldToFilter('installment_paid_date',array('null' => true))->getData();
						$due_date = $product_installments[0]['installment_due_date'];
					}

					if($product_id == $quote_item->getProductId() && $product_data['product_instock_email_sent'] == 0)
					{
						$partial_payment_emailsender = Mage::helper('partialpayment/emailsender');
						$partial_payment_emailsender->sendProductInstockMail($order->getCustomerFirstname(), $order->getCustomerEmail(), $order->getIncrementId(), $data['partial_payment_id'], $product_name, $due_date, $currency_code);
						$partial_payment_product = Mage::getModel("partialpayment/product")->load($product_data['partial_payment_order_product_id']);
						$partial_payment_product->setProductInstockEmailSent(1)->save();
					}
				}
			}
		}
	}
	
	public function CreateProcessDataBefore($observer)
	{
		$postData = Mage::app()->getRequest()->getPost();

		if (!isset($postData['item'])) {
			return;
		}

		if (isset($postData['update_items']) && $postData['update_items']) {
			$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
			$items = $postData['item'];
			$quote = Mage::getSingleton('adminhtml/session_quote')->getQuote();

			if ($partialpaymentHelper->isPartialPaymentForWholecart()) 
			{
				foreach ($quote->getAllVisibleItems() as $id => $item) 
				{
					if (isset($items[$item->getId()]['allow_partial_payment'])) 
					{
						if (empty($items[$item->getId()]['allow_partial_payment']) || $items[$item->getId()]['allow_partial_payment'] == 0) {
							Mage::getSingleton('core/session')->setAllowPartialPayment(0);
							return;
						}
					}
					else {
						Mage::getSingleton('core/session')->setAllowPartialPayment(0);
						return;
					}
				}
                                
				Mage::getSingleton('core/session')->setAllowPartialPayment($items[$item->getId()]['allow_partial_payment']);
			}
			else {
				foreach ($quote->getAllItems() as $id => $item) {
					if (isset($items[$item->getId()]['allow_partial_payment'])) {
						$options = $item->getOptions();
						foreach ($options as $option) {
							if($option->getCode() == 'info_buyRequest')
							{
								$unserialized = unserialize($option->getValue());
								$unserialized['allow_partial_payment'] = $items[$item->getId()]['allow_partial_payment'];
								$option->setValue(serialize($unserialized));
							}
						}
						$item->setOptions($options)->save();
					}
				}
			}
		}
	}
	public function SystemConfiganalyzer()
	{
		$serial= Mage::getStoreConfig('partialpayment/license_status_group/serial_key');
		$domain=$_SERVER['SERVER_NAME'];
		$time=date('Y-m-d H:i:s');
		$module_status = Mage::getStoreConfig('partialpayment/license_status_group/status')?"Enable" : "Disable";
		$partial_payment_brand_label = Mage::getStoreConfig('partialpayment/general_settings/brand_label');
		
		$partial_payment_available_to = Mage::getStoreConfig('partialpayment/general_settings/available_to');
		if($partial_payment_available_to == 1){
			$partial_payment_available_to = "All Customers Including Guest";
		}elseif($partial_payment_available_to == 2){
			$partial_payment_available_to = "Registered Customers Only";
		}else{
			$partial_payment_available_to = "Specific Customer Groups Only";
		}
		
		$customer_groups = "";
		$customer_groups_id = Mage::getStoreConfig('partialpayment/general_settings/customer_groups');		
		$customer_groups_id_arry = explode(",",$customer_groups_id);		
		if(count($customer_groups_id_arry) <= 1){			
			$customer_groups = Mage::getModel('customer/group')->load($customer_groups_id_arry[0])->getCustomerGroupCode();
		}else{			
			$cats = Mage::getModel('customer/group')->getCollection()->addFieldToFilter('customer_group_id', array('in'=>$customer_groups_id_arry));
			foreach($cats as $cat) {				
				$customer_groups .= $cat->getCustomerGroupCode().",";				
				
			}
			$customer_groups = rtrim($customer_groups, ",");		
		}
		$apply_partial_payment_to = Mage::getStoreConfig('partialpayment/general_settings/apply_partialpayment_to');
		if($apply_partial_payment_to == 1){
			$apply_partial_payment_to = "All Products";
		}elseif($apply_partial_payment_to == 2){
			$apply_partial_payment_to = "Whole cart";
		}else{
			$apply_partial_payment_to = "Selected Products Only";
		}
		
		$minimum_order_amount = Mage::getStoreConfig('partialpayment/general_settings/minimum_wholecart_order_amount_limit');
		
		$number_of_installments = Mage::getStoreConfig('partialpayment/general_settings/no_of_installments');
		
		$allow_full_payment = Mage::getStoreConfig('partialpayment/general_settings/allow_fullpayment')? "Yes" : "No";
		
		$capture_installments_automatically = Mage::getStoreConfig('partialpayment/general_settings/capture_installments_automatically')? "Yes" : "No";
		
		$shipping_tax_and_surcharge_calculation_options = Mage::getStoreConfig('partialpayment/general_settings/shipping_and_tax_calculation_options_for_wholecart')? "To be Added in Down Payment or First Installment Amount" : "To be added Equally in Remaining Installment(s) Amount";
		
		$discount_calculation_options = Mage::getStoreConfig('partialpayment/general_settings/discount_calculation_options_for_wholecart')? "To be Deducted from Down Payment or First Installment Amount" : "To be Deducted Equally from Remaining Installment(s) Amount";
		
		$allow_flexy_layaway_payments = Mage::getStoreConfig('partialpayment/payment_calculation_settings/allow_flexy_payment')? "Yes" : "No";
		
		$calculate_down_payment_on = Mage::getStoreConfig('partialpayment/payment_calculation_settings/calculate_downpayment_on') == 1? "Fixed Amount" : "Percentage of Product Price";
		
		$down_payment = Mage::getStoreConfig('partialpayment/payment_calculation_settings/down_payment');
		
		$payment_plan = Mage::getStoreConfig('partialpayment/payment_calculation_settings/payment_plan');
		if($payment_plan == 1){
			$payment_plan = "Monthly";
		}elseif($payment_plan == 2){
			$payment_plan = "Weekly";
		}else{
			$payment_plan = "Based on Days";
		}
		
		$number_of_days = Mage::getStoreConfig('partialpayment/payment_calculation_settings/total_no_days');
		
		$surcharge_options = Mage::getStoreConfig('partialpayment/payment_calculation_settings/surcharge_options');
		if($surcharge_options == 1){
			$surcharge_options = "Single Surcharge";
		}elseif($surcharge_options == 2){
			$surcharge_options = "Multiple Surcharge";
		}else{
			$surcharge_options = "No Surcharge";
		}
		
		$calculate_surcharge_on = Mage::getStoreConfig('partialpayment/payment_calculation_settings/surcharge_installment_calculation_type') == 1? "Fixed Amount" : "Percentage of Product Price";
		
		$surcharge =  Mage::getStoreConfig('partialpayment/payment_calculation_settings/surcharge_value');
		
		$credit_amount =  Mage::getStoreConfig('partialpayment/partialpayment_credit_setting/default_credit_amount');
		
		$credit_limit_surpass_message =  Mage::getStoreConfig('partialpayment/partialpayment_credit_setting/credit_limit_surpass_message');

		$send_partially_paid_orders_confirmation_email = Mage::getStoreConfig('partialpayment/email_notification/send_partially_paid_orders_confirmation_email');		
		if($send_partially_paid_orders_confirmation_email == 1){
			$send_partially_paid_orders_confirmation_email = "To Customers";
		}elseif($send_partially_paid_orders_confirmation_email == 2){
			$send_partially_paid_orders_confirmation_email = "To Customers and CC Both";
		}else{
			$send_partially_paid_orders_confirmation_email = "None";
		}
		
		$send_auto_capture_reminder_email = Mage::getStoreConfig('partialpayment/email_notification/send_auto_capture_reminder_email');
		if($send_auto_capture_reminder_email == 1){
			$send_auto_capture_reminder_email = "To Customers";
		}elseif($send_auto_capture_reminder_email == 2){
			$send_auto_capture_reminder_email = "To Customers and CC Both";
		}else{
			$send_auto_capture_reminder_email = "None";
		}
		
		$send_installment_reminder_email = Mage::getStoreConfig('partialpayment/email_notification/send_installment_reminder_email');
		if($send_installment_reminder_email == 1){
			$send_installment_reminder_email = "To Customers";
		}elseif($send_installment_reminder_email == 2){
			$send_installment_reminder_email = "To Customers and CC Both";
		}else{
			$send_installment_reminder_email = "None";
		}
		
		$send_installment_payment_confirmation_email = Mage::getStoreConfig('partialpayment/email_notification/send_installment_payment_confirmation_email');
		if($send_installment_payment_confirmation_email == 1){
			$send_installment_payment_confirmation_email = "To Customers";
		}elseif($send_installment_payment_confirmation_email == 2){
			$send_installment_payment_confirmation_email = "To Customers and CC Both";
		}else{
			$send_installment_payment_confirmation_email = "None";
		}
		
		$send_installment_auto_capture_failure_email = Mage::getStoreConfig('partialpayment/email_notification/send_installment_auto_capture_failure_email');
		if($send_installment_auto_capture_failure_email == 1){
			$send_installment_auto_capture_failure_email = "To Customers";
		}elseif($send_installment_auto_capture_failure_email == 2){
			$send_installment_auto_capture_failure_email = "To Customers and CC Both";
		}else{
			$send_installment_auto_capture_failure_email = "None";
		}
		
		$send_installment_failure_email = Mage::getStoreConfig('partialpayment/email_notification/send_installment_failure_email');
		if($send_installment_failure_email == 1){
			$send_installment_failure_email = "To Customers";
		}elseif($send_installment_failure_email == 2){
			$send_installment_failure_email = "To Customers and CC Both";
		}else{
			$send_installment_failure_email = "None";
		}
		
		$send_installment_over_due_notice_email = Mage::getStoreConfig('partialpayment/email_notification/send_installment_over_due_notice_email');
		if($send_installment_over_due_notice_email == 1){
			$send_installment_over_due_notice_email = "To Customers";
		}elseif($send_installment_over_due_notice_email == 2){
			$send_installment_over_due_notice_email = "To Customers and CC Both";
		}else{
			$send_installment_over_due_notice_email = "None";
		}
		$fields=array(
					'module_id' => 4,
					'serial_key'=>$serial,
					'domain'=>$domain,
					'table'=>'partialpayment_system_configuration',
					'data'=>json_encode(array(											
						'domain'=>$domain,
						'Config_save_time' => $time,
						'module_status' => $module_status,
						'partial_payment_brand_label' => $partial_payment_brand_label,
						'partial_payment_available_to' => $partial_payment_available_to,
						'customer_groups' => $customer_groups,
						'apply_partial_payment_to' => $apply_partial_payment_to,
						'minimum_order_amount' => $minimum_order_amount,
						'number_of_installments' => $number_of_installments,
						'allow_full_payment' => $allow_full_payment,
						'capture_installments_automatically' => $capture_installments_automatically,
						'shipping_tax_and_surcharge_calculation_options' => $shipping_tax_and_surcharge_calculation_options,
						'discount_calculation_options' => $discount_calculation_options,
						'allow_flexy_layaway_payments' => $allow_flexy_layaway_payments,
						'calculate_down_payment_on' => $calculate_down_payment_on,
						'down_payment' => $down_payment,
						'payment_plan' => $payment_plan,
						'number_of_days' => $number_of_days,
						'surcharge_options' => $surcharge_options,
						'calculate_surcharge_on' => $calculate_surcharge_on,
						'surcharge' => $surcharge,
						'credit_amount' => $credit_amount,
						'credit_limit_surpass_message' => $credit_limit_surpass_message,
						'send_partially_paid_orders_confirmation_email' => $send_partially_paid_orders_confirmation_email,
						'send_auto_capture_reminder_email' => $send_auto_capture_reminder_email,
						'send_installment_reminder_email' => $send_installment_reminder_email,
						'send_installment_payment_confirmation_email' => $send_installment_payment_confirmation_email,
						'send_installment_auto_capture_failure_email' => $send_installment_auto_capture_failure_email,
						'send_installment_failure_email' => $send_installment_failure_email,
						'send_installment_over_due_notice_email' => $send_installment_over_due_notice_email
					))
		);
		$url='http://www.milople.com/ModulesAnalyticsSystem/PartialPayment.php';
		$fields_string="";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_exec($ch);
		curl_close($ch);
			
	}
	public function SystemProductConfiganAlyzer($observer)	
	{
		$product = $observer->getProduct();
		$old_product = Mage::getModel('catalog/product')->load($product->getId());
		if(!(
		$product->getApplyPartialPayment() == $old_product->getApplyPartialPayment() && 
		$product->getNoOfInstallments() == $old_product->getNoOfInstallments() &&
		$product->getAllowFlexyPayment() == $old_product->getAllowFlexyPayment() &&
		$product->getAllowFullPayment() == $old_product->getAllowFullPayment() &&
		$product->getDownPaymentCalculation() == $old_product->getDownPaymentCalculation()&&
		$product->getDownpayment() == $old_product->getDownpayment()
		)){
			$serial= Mage::getStoreConfig('partialpayment/license_status_group/serial_key');
			$domain=$_SERVER['SERVER_NAME'];
			
			$product_id = $product->getId();
			$product_name = $product->getName();
			$product_sku = $product->getSku();
			$apply_partial_payment = $product->getApplyPartialPayment()?"Yes":"No";
			$number_of_installments = $product->getNoOfInstallments();
			$allow_flexy_layaway_payments = $product->getAllowFlexyPayment();
			if($allow_flexy_layaway_payments == 1){
				$allow_flexy_layaway_payments = "Yes";
			}elseif($allow_flexy_layaway_payments == 2){
				$allow_flexy_layaway_payments = "No";
			}else{
				$allow_flexy_layaway_payments = "None";
			}
			$allow_full_payment = $product->getAllowFullPayment();
			if($allow_full_payment == 1){
				$allow_full_payment = "Yes";
			}elseif($allow_full_payment == 2){
				$allow_full_payment = "No";
			}else{
				$allow_full_payment = "None";
			}
			$calculate_down_payment_on = $product->getDownPaymentCalculation();
			if($calculate_down_payment_on == 1){
				$calculate_down_payment_on = "Fixed Amount";
			}elseif($calculate_down_payment_on == 2){
				$calculate_down_payment_on = "Percentage of Product Price";
			}else{
				$calculate_down_payment_on = "None";
			}
			$down_payment = $product->getDownpayment();
			
			
			$fields=array(
						'module_id' => 4,
						'serial_key'=>$serial,
						'domain'=>$domain,
						'table'=>'partialpayment_product_wise_configuration',
						'data'=>json_encode(array(											
							'domain'=>$domain,
							'product_id'=>$product_id,
							'product_name'=>$product_name,
							'product_sku'=>$product_sku,
							'apply_partial_payment'=>$apply_partial_payment,
							'number_of_installments'=>$number_of_installments,
							'allow_flexy_layaway_payments'=>$allow_flexy_layaway_payments,
							'allow_full_payment'=>$allow_full_payment,
							'calculate_down_payment_on'=>$calculate_down_payment_on,
							'down_payment'=>$down_payment,
						))
				);			
			$url='http://www.milople.com/ModulesAnalyticsSystem/PartialPayment.php';
			
			$fields_string="";
			foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			rtrim($fields_string, '&');
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, count($fields));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_exec($ch);
			curl_close($ch);
		}
	}
	public function MwOrderEdit($observer)
    {
        $order = $observer->getEvent()->getOrder();

        $partialpaymentModel = Mage::getModel('partialpayment/partialpayment')->load($order->getId(), 'order_id');
        if ($partialpaymentModel->getId()) {
            $origOrder = $observer->getEvent()->getOrigOrder();
            if ($order->getGrandTotal() != $origOrder->getGrandTotal()) {
                if ($origOrder->getGrandTotal() > $order->getGrandTotal()) {
                    $diff = $origOrder->getGrandTotal() - $order->getGrandTotal();
                    $partialpaymentModel->setTotalAmount($order->getGrandTotal());
                    $partialpaymentModel->setRemainingAmount($partialpaymentModel->getRemainingAmount() - $diff);
                    $partialpaymentModel->setRemainingInstallments($partialpaymentModel->getRemainingInstallments() - $diff);
                    while ($diff > 0) {
                        $partial_payment_installments = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id', array('eq' => $partialpaymentModel->getId()));
                        $lastInstallment = $partial_payment_installments->getLastItem();
                        if ($diff >= $lastInstallment->getInstallmentAmount()) {
                            $partialpaymentModel->setTotalInstallments($partialpaymentModel->getTotalInstallments() - 1);
                            $lastInstallment->delete();
                            $remainingCount = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id', $partialpaymentModel->getId())->addFieldToFIlter('installment_status', 'Remaining')->getSize();
                            if (!$remainingCount) {
                                $diff = 0;
                            } else {
                                $diff = $diff - $lastInstallment->getInstallmentAmount();
                            }
                        } else {
                            $newAmount = $lastInstallment->getInstallmentAmount() - $diff;
                            $lastInstallment->setInstallmentAmount($newAmount)->save();
                            $diff = 0;
                        }
                    }
                } else {
                    $diff = $order->getGrandTotal() - $origOrder->getGrandTotal();
                    $partialpaymentModel->setTotalAmount($order->getGrandTotal());
                    $partialpaymentModel->setRemainingAmount($partialpaymentModel->getRemainingAmount() + $diff);
                    $partialpaymentModel->setRemainingInstallments($partialpaymentModel->getRemainingInstallments() + $diff);
                    $remainingCount = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id', $partialpaymentModel->getId())->addFieldToFIlter('installment_status', 'Remaining')->getSize();
                    $partial_payment_installments = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id', array('eq' => $partialpaymentModel->getId()));
                    $lastInstallment = $partial_payment_installments->getLastItem();
                    if (!$remainingCount) {
                        if ($lastInstallment->getInstallmentDueDate()) {
                            $installment_due_date = strtotime($lastInstallment->getInstallmentDueDate() . "+1 months");
                        }
                        if ($installment_due_date < time()) {
                            $installment_due_date = strtotime("+1 months");
                        }
                        $installment_due_date = date("Y-m-d", $installment_due_date);
                        $data = array('partial_payment_id' => $partialpaymentModel->getId(), 'installment_amount' => $diff, 'installment_due_date' => $installment_due_date, 'installment_paid_date' => '', 'installment_status' => 'Remaining');
                        $partial_payment_installments = Mage::getModel('partialpayment/installment')->setData($data)->save();
                        $partialpaymentModel->setTotalInstallments($partialpaymentModel->getTotalInstallments() + 1);
                    } else {
                        $newAmount = $lastInstallment->getInstallmentAmount() + $diff;
                        $lastInstallment->setInstallmentAmount($newAmount)->save();
                    }
                }
                if ($partialpaymentModel->getTotalAmount() <= $partialpaymentModel->getPaidAmount() && $partialpaymentModel->getRemainingAmount() > 0) {
                    $partialpaymentModel->setPaidAmount($partialpaymentModel->getTotalAmount() - $partialpaymentModel->getRemainingAmount());
                }
                $order->setPaidAmount($partialpaymentModel->getPaidAmount());
                $order->setRemainingAmount($partialpaymentModel->getRemainingAmount());
                $partialpaymentModel->save();
            }
            $remainingInstallmentCount = Mage::getModel('partialpayment/installment')->getCollection()
                            ->addFieldToFilter('partial_payment_id', $partialpaymentModel->getId())
                            ->addFieldToFIlter('installment_status', 'Remaining')->getSize();
            $paidInstallmentCount = Mage::getModel('partialpayment/installment')->getCollection()
                            ->addFieldToFilter('partial_payment_id', $partialpaymentModel->getId())
                            ->addFieldToFIlter('installment_status', 'Paid')->getSize();
            $partialpaymentModel->setPaidInstallments($paidInstallmentCount)->setRemainingInstallments($remainingInstallmentCount)->save();
        }
    }

}

?>
