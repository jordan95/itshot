<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

if (Mage::helper('core')->isModuleEnabled('MageWorx_OrdersEdit')){
    class Milople_Partialpayment_Model_Payflowpro_Pure extends MageWorx_OrdersEdit_Model_Paypal_Payflowpro{
        
    }
}else {
    class Milople_Partialpayment_Model_Payflowpro_Pure extends Mage_Paypal_Model_Payflowpro{
        
    }
}
class Milople_Partialpayment_Model_Payflowpro extends Milople_Partialpayment_Model_Payflowpro_Pure
{
    /**
     * Post request to gateway and return response
     *
     * @param Varien_Object $request
     * @return Varien_Object
     */
    protected function _postRequest(Varien_Object $request)
    {
		$calculationModel = Mage::getModel('partialpayment/calculation');
		if ($calculationModel->getQuote()->getIsActive()) {
			if ($calculationModel->getShippingOrBilling() == 'shipping') {
				$quoteAddress = $calculationModel->getQuote()->getShippingAddress();
			}
			else {
				$quoteAddress = $calculationModel->getQuote()->getBillingAddress();
			}
			if ($quoteAddress->getPaidAmount() > 0) {
				$request->setAmt(Mage::helper('partialpayment/partialpayment')->setNumberFormat($quoteAddress->getPaidAmount()));
			}
		}

        $debugData = array('request' => $request->getData());

        $client = new Varien_Http_Client();
        $result = new Varien_Object();

        $_config = array(
            'maxredirects' => 5,
            'timeout'    => 30,
            'verifypeer' => $this->getConfigData('verify_peer')
        );

        $_isProxy = $this->getConfigData('use_proxy', false);
        if ($_isProxy) {
            $_config['proxy'] = $this->getConfigData('proxy_host')
                . ':'
                . $this->getConfigData('proxy_port');//http://proxy.shr.secureserver.net:3128',
            $_config['httpproxytunnel'] = true;
            $_config['proxytype'] = CURLPROXY_HTTP;
        }

        $client->setUri($this->_getTransactionUrl())
            ->setConfig($_config)
            ->setMethod(Zend_Http_Client::POST)
            ->setParameterPost($request->getData())
            ->setHeaders('X-VPS-VIT-CLIENT-CERTIFICATION-ID: 33baf5893fc2123d8b191d2d011b7fdc')
            ->setHeaders('X-VPS-Request-ID: ' . $request->getRequestId())
            ->setHeaders('X-VPS-CLIENT-TIMEOUT: ' . $this->_clientTimeout);

        try {
           /**
            * we are sending request to payflow pro without url encoding
            * so we set up _urlEncodeBody flag to false
            */
            $response = $client->setUrlEncodeBody(false)->request();
        }
        catch (Exception $e) {
            $result->setResponseCode(-1)
                ->setResponseReasonCode($e->getCode())
                ->setResponseReasonText($e->getMessage());

            $debugData['result'] = $result->getData();
            $this->_debug($debugData);
            throw $e;
        }



        $response = strstr($response->getBody(), 'RESULT');
        $valArray = explode('&', $response);

        foreach($valArray as $val) {
                $valArray2 = explode('=', $val);
                $result->setData(strtolower($valArray2[0]), $valArray2[1]);
        }

        $result->setResultCode($result->getResult())
                ->setRespmsg($result->getRespmsg());

		if ($result->getResultCode() == self::RESPONSE_CODE_APPROVED || $result->getResultCode() == self::RESPONSE_CODE_FRAUDSERVICE_FILTER) {
			Milople_Partialpayment_Model_Observer::$profileId = $result->getPnref();
		}

        $debugData['result'] = $result->getData();
        $this->_debug($debugData);

        return $result;
    }

    public function captureInstallment ($origid, $amt, $order_increment_id, $installment_id) {
		$client = new Varien_Http_Client();

		$request = array(
			'TRXTYPE' => ($this->getConfigData('payment_action') == Mage_Paypal_Model_Config::PAYMENT_ACTION_AUTH ? self::TRXTYPE_AUTH_ONLY : self::TRXTYPE_SALE),
			'TENDER' => self::TENDER_CC,
			'PWD' => $this->getConfigData('pwd'),
			'PARTNER' => $this->getConfigData('partner'),
			'VENDOR' => $this->getConfigData('vendor'),
			'USER' => $this->getConfigData('user'),
			'ORIGID' => $origid,
			'AMT' => $amt,
			'COMMENT1' => $order_increment_id . ' - ' . $installment_id
		);

        $_config = array(
            'maxredirects' => 5,
            'timeout'    => 30,
            'verifypeer' => $this->getConfigData('verify_peer')
        );

        if ($this->getConfigData('use_proxy', false)) {
            $_config['proxy'] = $this->getConfigData('proxy_host')
                . ':'
                . $this->getConfigData('proxy_port');//http://proxy.shr.secureserver.net:3128',
            $_config['httpproxytunnel'] = true;
            $_config['proxytype'] = CURLPROXY_HTTP;
        }

        $client->setUri($this->_getTransactionUrl())
            ->setConfig($_config)
            ->setMethod(Zend_Http_Client::POST)
            ->setParameterPost($request)
            ->setHeaders('X-VPS-VIT-CLIENT-CERTIFICATION-ID: 33baf5893fc2123d8b191d2d011b7fdc')
            ->setHeaders('X-VPS-Request-ID: ' . $this->_generateRequestId())
            ->setHeaders('X-VPS-CLIENT-TIMEOUT: ' . $this->_clientTimeout);

		try {
           /**
            * we are sending request to payflow pro without url encoding
            * so we set up _urlEncodeBody flag to false
            */
            $response = $client->setUrlEncodeBody(false)->request();
            $response = strstr($response->getBody(), 'RESULT');
			$valArray = explode('&', $response);
			$result = array();
			foreach($valArray as $val) {
					$valArray2 = explode('=', $val);
					$result[strtolower($valArray2[0])] = $valArray2[1];
			}
			if ($result['result'] == self::RESPONSE_CODE_APPROVED || $result['result'] == self::RESPONSE_CODE_FRAUDSERVICE_FILTER) {
				return $result;
			}
        }
        catch (Exception $e) {
        }
        return array('pnref' => 0);
	}
}
