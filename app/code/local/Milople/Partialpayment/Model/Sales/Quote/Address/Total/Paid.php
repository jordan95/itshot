<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Sales_Quote_Address_Total_Paid extends Mage_Sales_Model_Quote_Address_Total_Abstract{
	protected $_code = 'Paid';

	public function fetch(Mage_Sales_Model_Quote_Address $address)
	{
		$calculationModel = Mage::getModel("partialpayment/calculation");
		$partial_payment_helper = Mage::helper('partialpayment/partialpayment');
		
		$address->setShippingAmount($address->getShippingInclTax()-$address->getShippingTaxAmount());
		$paid_amount = $address->getPaidAmount();
                                $order = Mage::registry('ordersedit_order');
		if((!$order || $order->getOrderType() == 1) && $address->getAddressType() == $calculationModel->getShippingOrBilling($address->getQuote()) && ($paid_amount > 0) && (round($paid_amount,2) < round($address->getGrandTotal(),2)) && ($paid_amount != $address->getSubtotal()))
		{
			$address->addTotal(array(
							'code'=>$this->getCode(),
							'strong' => true,
							'title'=> $partial_payment_helper->getDownpaymentLabel(),
							'value'=> $paid_amount,
							'area' => 'footer'
					), 'grand_total');
		}
	}
}