<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

if (Mage::helper('core')->isModuleEnabled('Braintree_Payments')) {

    class Milople_Partialpayment_Model_Sales_Order_Payment_Pure extends Braintree_Payments_Model_Rewrite_Sales_Order_Payment
    {
        
    }

} else {

    class Milople_Partialpayment_Model_Sales_Order_Payment_Pure extends Mage_Sales_Model_Order_Payment
    {
        
    }

}

class Milople_Partialpayment_Model_Sales_Order_Payment extends Milople_Partialpayment_Model_Sales_Order_Payment_Pure
{
	protected function _isCaptureFinal($amountToCapture)
    {
        $amountToCapture = $this->_formatAmount($amountToCapture, true);
        $orderGrandTotal = $this->_formatAmount($this->getOrder()->getBaseGrandTotal(), true);
        if ($orderGrandTotal == $this->_formatAmount($this->getBaseAmountPaid(), true) + $amountToCapture) {
            if (false !== $this->getShouldCloseParentTransaction()) {
                $this->setShouldCloseParentTransaction(true);
            }
            return true;
        }
		return true;
    }

    public function capture($invoice)
    {
		try {
			if (is_null($invoice)) {
				$invoice = $this->_invoice();
				$this->setCreatedInvoice($invoice);
				return $this; // @see Mage_Sales_Model_Order_Invoice::capture()
			}

			$order = $this->getOrder();
			$incrementId = $order->getEntityId();

			$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
			$partialpaymentOrder = Mage::getModel('partialpayment/partialpayment')->getCollection()
				->addFieldToFilter('order_id',$incrementId)
				->load();

			if(sizeof($partialpaymentOrder->getData()) == 0)
			{	
				if(($invoice->getBasePaidAmount() > 0) && ($invoice->getBaseRemainingAmount() > 0)&& ($invoice->getSubtotalInclTax() >= $partialpaymentHelper->getMinimumWholecartOrderAmount()) && ($invoice->getGrandTotal() > $invoice->getBaseRemainingAmount()) && ($invoice->getBasePaidAmount() > $invoice->getTaxAmount()))
				{
					$capture_amount = $invoice->getBasePaidAmount();
					$amountToCapture = $this->_formatAmount($capture_amount);
				}else{
					$amountToCapture = $this->_formatAmount($invoice->getBaseGrandTotal());
				}
			}
			else
			{
				foreach($partialpaymentOrder as $partialpaymentModel)
				{
					$partialpaymentId = $partialpaymentModel->getPartialPaymentId();
					$installmentModel = Mage::getModel('partialpayment/installment')->getCollection()
						->addFieldToFilter('partial_payment_id',$partialpaymentId)
						->getFirstItem() ;								
					$capture_amount = $installmentModel->getInstallmentAmount();
					$amountToCapture = $this->_formatAmount($capture_amount);
				}
			}

			// prepare parent transaction and its amount
			$paidWorkaround = 0;
			if (!$invoice->wasPayCalled()) {
				$paidWorkaround = (float)$amountToCapture;
			}
			$this->_isCaptureFinal($paidWorkaround);

			$this->_generateTransactionId(
				Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE,
				$this->getAuthorizationTransaction()
			);

			Mage::dispatchEvent('sales_order_payment_capture', array('payment' => $this, 'invoice' => $invoice));

			/**
			 * Fetch an update about existing transaction. It can determine whether the transaction can be paid
			 * Capture attempt will happen only when invoice is not yet paid and the transaction can be paid
			 */
			if ($invoice->getTransactionId()) {
				$this->getMethodInstance()
					->setStore($order->getStoreId())
					->fetchTransactionInfo($this, $invoice->getTransactionId());
			}
			$status = true;
			if (!$invoice->getIsPaid() && !$this->getIsTransactionPending()) {
				// attempt to capture: this can trigger "is_transaction_pending"
				$this->getMethodInstance()->setStore($order->getStoreId())->capture($this, $amountToCapture);

				$transaction = $this->_addTransaction(
					Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE,
					$invoice,
					true
				);

				if ($this->getIsTransactionPending()) {
					$message = Mage::helper('sales')->__('Capturing amount of %s is pending approval on gateway.', $this->_formatPrice($amountToCapture));
					$state = Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW;
					if ($this->getIsFraudDetected()) {
						$status = Mage_Sales_Model_Order::STATUS_FRAUD;
					}
					$invoice->setIsPaid(false);
				} else { // normal online capture: invoice is marked as "paid"
					$message = Mage::helper('sales')->__('Captured amount of %s online.', $this->_formatPrice($amountToCapture));
					$state = Mage_Sales_Model_Order::STATE_PROCESSING;

					$invoice->setIsPaid(true);

					$this->_updateTotals(array('base_amount_paid_online' => $amountToCapture));
				}
				if ($order->isNominal()) {
					$message = $this->_prependMessage(Mage::helper('sales')->__('Nominal order registered.'));
				} else {
					$message = $this->_prependMessage($message);
					$message = $this->_appendTransactionToMessage($transaction, $message);
				}
				$order->setState($state, $status, $message);
				$this->getMethodInstance()->processInvoice($invoice, $this); // should be deprecated
				return $this;
			}
			Mage::throwException(
				Mage::helper('sales')->__('The transaction "%s" cannot be captured yet.', $invoice->getTransactionId())
			);
		}
		catch(Exception $e)
		{
			throw new Mage_Core_Exception($e->getMessage());
		}
    }

	public function place()
    {
        Mage::dispatchEvent('sales_order_payment_place_start', array('payment' => $this));
        $order = $this->getOrder();

        $this->setAmountOrdered($order->getTotalDue());
        $this->setBaseAmountOrdered($order->getBaseTotalDue());
        $this->setShippingAmount($order->getShippingAmount());
        $this->setBaseShippingAmount($order->getBaseShippingAmount());

        $methodInstance = $this->getMethodInstance();
        $methodInstance->setStore($order->getStoreId());
        $orderState = Mage_Sales_Model_Order::STATE_NEW;
        $stateObject = new Varien_Object();
		$payment_method_code = $order->getPayment()->getMethodInstance()->getCode();
        /**
         * Do order payment validation on payment method level
         */
        $methodInstance->validate();
        $action = $methodInstance->getConfigPaymentAction();
        if ($action) {
            if ($methodInstance->isInitializeNeeded()) {
                /**
                 * For method initialization we have to use original config value for payment action
                 */
                $methodInstance->initialize($methodInstance->getConfigData('payment_action'), $stateObject);
            } else {
                $orderState = Mage_Sales_Model_Order::STATE_PROCESSING;
                switch ($action) {
                    case Mage_Payment_Model_Method_Abstract::ACTION_ORDER:
                        $this->_order($order->getBaseTotalDue());
                        break;
                    case Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE:
						if(($order->getBasePaidAmount() > 0) && ($order->getBaseRemainingAmount() > 0) && $payment_method_code == 'authorizenet')
						{
							$this->setAmountAuthorized($order->getTotalDue());
							$this->setBaseAmountAuthorized($order->getBaseTotalDue());
							$this->capture(null);
							break;
						}else{
							$this->_authorize(true, $order->getBaseTotalDue()); // base amount will be set inside
							$this->setAmountAuthorized($order->getTotalDue());
							break;
						}
                    case Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE:
                        $this->setAmountAuthorized($order->getTotalDue());
                        $this->setBaseAmountAuthorized($order->getBaseTotalDue());
                        $this->capture(null);
                        break;
                    default:
                        break;
                }
            }
        }

        $this->_createBillingAgreement();

        $orderIsNotified = null;
        if ($stateObject->getState() && $stateObject->getStatus()) {
            $orderState      = $stateObject->getState();
            $orderStatus     = $stateObject->getStatus();
            $orderIsNotified = $stateObject->getIsNotified();
        } else {
            $orderStatus = $methodInstance->getConfigData('order_status');
            if (!$orderStatus) {
                $orderStatus = $order->getConfig()->getStateDefaultStatus($orderState);
            } else {
                // check if $orderStatus has assigned a state
                $states = $order->getConfig()->getStatusStates($orderStatus);
                if (count($states) == 0) {
                    $orderStatus = $order->getConfig()->getStateDefaultStatus($orderState);
                }
            }
        }
        $isCustomerNotified = (null !== $orderIsNotified) ? $orderIsNotified : $order->getCustomerNoteNotify();
        $message = $order->getCustomerNote();

        // add message if order was put into review during authorization or capture
        if ($order->getState() == Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW) {
            if ($message) {
                $order->addStatusToHistory($order->getStatus(), $message, $isCustomerNotified);
            }
        } elseif ($order->getState() && ($orderStatus !== $order->getStatus() || $message)) {
            // add message to history if order state already declared
            $order->setState($orderState, $orderStatus, $message, $isCustomerNotified);
        } elseif (($order->getState() != $orderState) || ($order->getStatus() != $orderStatus) || $message) {
            // set order state
            $order->setState($orderState, $orderStatus, $message, $isCustomerNotified);
        }

        Mage::dispatchEvent('sales_order_payment_place_end', array('payment' => $this));

        return $this;
    }
	
	protected function _authorize($isOnline, $amount = 0)
	{
		try {
			// update totals
			// do authorization
			$order  = $this->getOrder();
			$incrementId = $order->getEntityId();
			$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
			$partialpaymentOrder = Mage::getModel('partialpayment/partialpayment')->getCollection()
				->addFieldToFilter('order_id',$incrementId)
				->load();

			if(sizeof($partialpaymentOrder->getData()) == 0)
			{
				if(($order->getBasePaidAmount() > 0) && ($order->getBaseRemainingAmount() > 0))
				{
					$amount =  $this->_formatAmount($order->getBasePaidAmount(), true);
					
				}else{
			
					$amount =  $this->_formatAmount($amount, true);
				}
			}
			else
			{
				foreach($partialpaymentOrder as $partialpaymentModel)
				{
					$partialpaymentId = $partialpaymentModel->getPartialPaymentId();
					$installmentModel = Mage::getModel('partialpayment/installment')->getCollection()
						->addFieldToFilter('partial_payment_id',$partialpaymentId)
						->load();

					foreach($installmentModel as $installments)
					{
						if($installments->getInstallmentStatus() == 'Remain')
						{
							$amount = $this->_formatAmount($installments->getInstallmentAmount(), true);
							
							break;
						}
					}
				}
			}
			$amount = $this->_formatAmount($amount);
			$this->setBaseAmountAuthorized($amount);

			$state  = Mage_Sales_Model_Order::STATE_PROCESSING;
			$status = true;
			if ($isOnline) {
				// invoke authorization on gateway
				$this->getMethodInstance()->setStore($order->getStoreId())->authorize($this, $amount);
			}

			// similar logic of "payment review" order as in capturing
			if ($this->getIsTransactionPending()) {
				$message = Mage::helper('sales')->__('Authorizing amount of %s is pending approval on gateway.', $this->_formatPrice($amount));
				$state = Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW;
				if ($this->getIsFraudDetected()) {
					$status = Mage_Sales_Model_Order::STATUS_FRAUD;
				}
			} else {
				$message = Mage::helper('sales')->__('Authorized amount of %s.', $this->_formatPrice($amount));
			}

			// update transactions, order state and add comments
			$transaction = $this->_addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH);
			if ($order->isNominal()) {
				$message = $this->_prependMessage(Mage::helper('sales')->__('Nominal order registered.'));
			} else {
				$message = $this->_prependMessage($message);
				$message = $this->_appendTransactionToMessage($transaction, $message);
			}
			$order->setState($state, $status, $message);
			return $this;
		}
		catch(Exception $e)
		{
			throw new Mage_Core_Exception($e->getMessage());
		}
	}
	public function refund($creditmemo)
    {
        $order = $this->getOrder();
		$partialpaymentOrder = Mage::getModel('partialpayment/partialpayment')->load($order->getId(),'order_id');
		if(sizeof($partialpaymentOrder->getData()) != 0)
		{
			$baseAmountToRefund = $this->_formatAmount(Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id',$partialpaymentOrder->getId())->getFirstItem()->getInstallmentAmount());
		}
		else
		{
			$baseAmountToRefund = $this->_formatAmount($creditmemo->getBaseGrandTotal());
		}
		
        $this->_generateTransactionId(Mage_Sales_Model_Order_Payment_Transaction::TYPE_REFUND);

        // call refund from gateway if required
        $isOnline = false;
        $gateway = $this->getMethodInstance();
        $invoice = null;
        if ($gateway->canRefund() && $creditmemo->getDoTransaction()) {
            $this->setCreditmemo($creditmemo);
            $invoice = $creditmemo->getInvoice();
            if ($invoice) {
                $isOnline = true;
                $captureTxn = $this->_lookupTransaction($invoice->getTransactionId());
                if ($captureTxn) {
                    $this->setParentTransactionId($captureTxn->getTxnId());
                }
                $this->setShouldCloseParentTransaction(true); // TODO: implement multiple refunds per capture
                try {
                    $gateway->setStore($this->getOrder()->getStoreId())
                        ->processBeforeRefund($invoice, $this)
                        ->refund($this, $baseAmountToRefund)
                        ->processCreditmemo($creditmemo, $this)
                    ;
                } catch (Mage_Core_Exception $e) {
                    if (!$captureTxn) {
                        $e->setMessage(' ' . Mage::helper('sales')->__('If the invoice was created offline, try creating an offline creditmemo.'), true);
                    }
                    throw $e;
                }
            }
        }

        // update self totals from creditmemo
        $this->_updateTotals(array(
            'amount_refunded' => $creditmemo->getGrandTotal(),
            'base_amount_refunded' => $baseAmountToRefund,
            'base_amount_refunded_online' => $isOnline ? $baseAmountToRefund : null,
            'shipping_refunded' => $creditmemo->getShippingAmount(),
            'base_shipping_refunded' => $creditmemo->getBaseShippingAmount(),
        ));

        // update transactions and order state
        $transaction = $this->_addTransaction(
            Mage_Sales_Model_Order_Payment_Transaction::TYPE_REFUND,
            $creditmemo,
            $isOnline
        );
        if ($invoice) {
            $message = Mage::helper('sales')->__('Refunded amount of %s online.', $this->_formatPrice($baseAmountToRefund));
        } else {
            $message = $this->hasMessage() ? $this->getMessage()
                : Mage::helper('sales')->__('Need to refund %s offline.', $this->_formatPrice($baseAmountToRefund));
        }
        $message = $message = $this->_prependMessage($message);
        $message = $this->_appendTransactionToMessage($transaction, $message);
        $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true, $message);

        Mage::dispatchEvent('sales_order_payment_refund', array('payment' => $this, 'creditmemo' => $creditmemo));
        return $this;
    }
}