<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Sales_Order_Total_Creditmemo_Totals extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
	{
		$calculationHelper = Mage::helper('partialpayment/partialpayment');
		$order = $creditmemo->getOrder();

		$remainingAmountLeft = $order->getRemainingAmount();
		$baseRemainingAmountLeft = $order->getBaseRemainingAmount();
		$paidAmount = $order->getPaidAmount();
		$basePaidAmount = $order->getBasePaidAmount();

		$creditmemo->setRemainingAmount($remainingAmountLeft);
		$creditmemo->setBaseRemainingAmount($baseRemainingAmountLeft);

		$creditmemo->setPaidAmount($paidAmount);
		$creditmemo->setBasePaidAmount($basePaidAmount);

		$creditmemo->setSurchargeAmount($order->getSurchargeAmount());
		$creditmemo->setBaseSurchargeAmount($order->getBaseSurchargeAmount());

		$creditmemo->getOutofstockdiscountAmount($order->getOutofstockdiscountAmount());
		$creditmemo->getBasaeOutofstockdiscountAmount($order->getBasaeOutofstockdiscountAmount());

		$creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal()+$order->getBaseSurchargeAmount()+$order->getBaseOutofstockdiscountAmount());
		$creditmemo->setGrandTotal($creditmemo->getGrandTotal()+$order->getSurchargeAmount()+$order->getOutofstockdiscountAmount());

		return $this;
	}
}
