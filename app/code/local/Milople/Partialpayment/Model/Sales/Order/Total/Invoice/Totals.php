<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Sales_Order_Total_Invoice_Totals extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{	
	public function collect(Mage_Sales_Model_Order_Invoice $invoice)
	{
		$order = $invoice->getOrder();
		$remainingAmountLeft = $order->getRemainingAmount() - $order->getRemainingAmountInvoiced();
		$baseRemainingAmountLeft = $order->getBaseRemainingAmount() - $order->getBaseRemainingAmountInvoiced();
		$baseRemainingAmountLeft = $order->getBaseRemainingAmount() - $order->getBaseRemainingAmountInvoiced();
		$paidAmount = $order->getPaidAmount();
		$basePaidAmount = $order->getBasePaidAmount();

		$invoice->setRemainingAmount($remainingAmountLeft);
		$invoice->setBaseRemainingAmount($baseRemainingAmountLeft);

		$invoice->setPaidAmount($paidAmount);
		$invoice->setBasePaidAmount($basePaidAmount);

		$invoice->setSurchargeAmount($order->getSurchargeAmount());
		$invoice->setBaseSurchargeAmount($order->getBaseSurchargeAmount());

		$invoice->getOutofstockdiscountAmount($order->getOutofstockdiscountAmount());
		$invoice->getBasaeOutofstockdiscountAmount($order->getBasaeOutofstockdiscountAmount());

		$invoice->setBaseGrandTotal($invoice->getBaseGrandTotal()+$order->getBaseSurchargeAmount()+$order->getBaseOutofstockdiscountAmount());
		$invoice->setGrandTotal($invoice->getGrandTotal()+$order->getSurchargeAmount()+$order->getOutofstockdiscountAmount());
		return $this;
	}
}
