<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

if(Mage::helper('core')->isModuleEnabled('MageWorx_OrdersEdit')){
    class Milople_Partialpayment_Model_Direct_Pure extends MageWorx_OrdersEdit_Model_Paypal_Direct{
        
    }
}else {
    class Milople_Partialpayment_Model_Direct_Pure extends Mage_Paypal_Model_Direct{
        
    }
}

class Milople_Partialpayment_Model_Direct extends Milople_Partialpayment_Model_Direct_Pure
{ 
	protected function _placeOrder(Mage_Sales_Model_Order_Payment $payment, $amount)
    {
		$calculationModel=Mage::getModel("partialpayment/calculation");
	
        $remaining = $calculationModel->getAmountToBePaidLater();
		$amt = $amount  ;

		if ($remaining > 0) {
			$amt = number_format($amount, 2, '.', '');	
		}

		$order = $payment->getOrder();
		if(!isset($_REQUEST['installment_id']))
		{
			$api = $this->_pro->getApi()
				->setPaymentAction($this->_pro->getConfig()->paymentAction)
				->setIpAddress(Mage::app()->getRequest()->getClientIp(false))
				->setAmount($amt)
				->setCurrencyCode($order->getBaseCurrencyCode())
				->setInvNum($order->getIncrementId().'-'.'1'.'-'.date("dmyHis"))
				->setEmail($order->getCustomerEmail())
				->setNotifyUrl(Mage::getUrl('paypal/ipn/'))
				->setCreditCardType($payment->getCcType())
				->setCreditCardNumber($payment->getCcNumber())
				->setCreditCardExpirationDate($this->_getFormattedCcExpirationDate($payment->getCcExpMonth(), $payment->getCcExpYear()))
				->setCreditCardCvv2($payment->getCcCid())
				->setMaestroSoloIssueNumber($payment->getCcSsIssue());
		}
		else     //when pay for remaining installments
		{
			$partial_payment = Mage::getModel('partialpayment/partialpayment')->getCollection();
			$partial_payment->addFieldToFilter('partial_payment_id', $_REQUEST['partial_payment_id']);
			$partial_payment->getData();
			
				foreach ($partial_payment as $partial)
				{
					$newOrderId =$partial->getPaidInstallment() + 1;
				}
				
				 $api = $this->_pro->getApi()
				->setPaymentAction($this->_pro->getConfig()->paymentAction)
				->setIpAddress(Mage::app()->getRequest()->getClientIp(false))
				->setAmount($amt)
				->setCurrencyCode($order->getBaseCurrencyCode())
				->setInvNum($order->getIncrementId().'-'.$newOrderId.'-' .date("dmyHis"))
				->setEmail($order->getCustomerEmail())
				->setNotifyUrl(Mage::getUrl('paypal/ipn/'))
				->setCreditCardType($payment->getCcType())
				->setCreditCardNumber($payment->getCcNumber())
				->setCreditCardExpirationDate($this->_getFormattedCcExpirationDate($payment->getCcExpMonth(), $payment->getCcExpYear()))
				->setCreditCardCvv2($payment->getCcCid())
				->setMaestroSoloIssueNumber($payment->getCcSsIssue());
		
		}

        if ($payment->getCcSsStartMonth() && $payment->getCcSsStartYear()) {
            $year = sprintf('%02d', substr($payment->getCcSsStartYear(), -2, 2));
            $api->setMaestroSoloIssueDate($this->_getFormattedCcExpirationDate($payment->getCcSsStartMonth(), $year));
        }
        if ($this->getIsCentinelValidationEnabled()) {
            $this->getCentinelValidator()->exportCmpiData($api);
        }
    
        if ($order->getIsVirtual()) {
            $api->setAddress($order->getBillingAddress())->setSuppressShipping(true);
        } else {
            $api->setAddress($order->getShippingAddress());
            $api->setBillingAddress($order->getBillingAddress());
        }
     
        $api->setPaypalCart(Mage::getModel('paypal/cart', array($order)))
            ->setIsLineItemsEnabled($this->_pro->getConfig()->lineItemsEnabled);
        $api->callDoDirectPayment();
        $this->_importResultToPayment($api, $payment);

        try {
            $api->callGetTransactionDetails();
        } catch (Mage_Core_Exception $e) {
            
            $payment->setIsTransactionPending(true);
        }
        $this->_importResultToPayment($api, $payment);
        return $this;
   } 
}
