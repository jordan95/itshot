<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Payment_Method_Authorizenet extends Varien_Object
{
    const PAYMENT_METHOD_CODE = 'authorizenet';
    const XML_PATH_AUTHORIZENET_API_LOGIN_ID = 'payment/authorizenet/login';
    const XML_PATH_AUTHORIZENET_TEST_MODE = 'payment/authorizenet/test';
    const XML_PATH_AUTHORIZENET_DEBUG = 'payment/authorizenet/debug';
    const XML_PATH_AUTHORIZENET_TRANSACTION_KEY = 'payment/authorizenet/trans_key';
    const XML_PATH_AUTHORIZENET_PAYMENT_ACTION = 'payment/authorizenet/payment_action';
    const XML_PATH_AUTHORIZENET_ORDER_STATUS = 'payment/authorizenet/order_status';
    const XML_PATH_AUTHORIZENET_SOAP_TEST = 'payment/authorizenet/soap_test';
    const WEB_SERVICE_MODEL = 'partialpayment/web_service_client_authorizenet';

    public function __construct()
    {
        $this->_initWebService();
    }

    /**
     * Initializes web service instance
     * @return Milople_Partialpayment_Model_Payment_Method_Authorizenet
     */
    protected function _initWebService()
    {
        $service = Mage::getModel(self::WEB_SERVICE_MODEL);
        $this->setWebService($service);
        return $this;
    }

    /**
     * Processes payment for specified order
     * @param Mage_Sales_Model_Order $Order
     * @return
     */
    public function processOrder(Mage_Sales_Model_Order $PrimaryOrder, Mage_Sales_Model_Order $Order = null)
    {
        if ($Order->getBaseGrandTotal() > 0) {
            $result = $this->getWebService()
                    ->setOrder($Order)
                    ->createTransaction();
            $ccTransId = @$result->transactionId;
            $Order->getPayment()->setCcTransId($ccTransId);
			return;
        }
    }
}
