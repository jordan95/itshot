<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Model_Calculation extends Mage_Core_Model_Abstract
{
	static $partial_payment_helper;
    public function _construct()
    {
        parent::_construct();
        $this->_init('partialpayment/Calculation');
		$this->partial_payment_helper=Mage::helper('partialpayment/partialpayment');
    }
	
	public function getDownPaymentAmount($price, $product=NULL, $installments=NULL)//to get down payment product wise
	{
		$down_payment=0;
		try
		{
			if($installments!=NULL)
			{
				return ($price / $installments);
			}
			$down_payment = $this->partial_payment_helper->getDownPaymentValue($product);
			if($this->partial_payment_helper->getCalculateDownpaymentOn($product)==2)// if type of calculation is percentage
			{
				$down_payment = ($price*$down_payment)/100;
			}
			return $down_payment;
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		
		}
	}
	public function getInstallments($price, $product=NULL, $installments=NULL)//it gives one installment value
	{
		try
		{
			if($installments!=NULL)
			{
				return ($price / $installments);
			}
			$down_payment =$this->getDownPaymentAmount($price,$product);//get down payment amount
			$installment = ($price-$down_payment)/($this->partial_payment_helper->getTotalIinstallments($product)-1);//calculate installment
			return $installment;
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function getRemainingAmountByProduct($price, $product=NULL, $installments=NULL)
	{
		return $price-$this->getDownPaymentAmount($price,$product,$installments);
	}
		
	public function getSurchargeAmount($value,$installments=NULL)
	{
		try
		{
			$surcharge = 0;
			
			if($this->partial_payment_helper->isSurcharge()==1)
			{
				if($this->partial_payment_helper->getCalculateSurchargeOn()==1) // surcharge value in percentage
				{
					$surcharge = $this->partial_payment_helper->getSurchargeValue();					
				}
				else
				{
					$surcharge = ((double)$value * $this->partial_payment_helper->getSurchargeValue())/100;
				}
				
			}
			else if($this->partial_payment_helper->isSurcharge()==2)//if multiple surcharge
			{
				$surchargeValues = explode(",",$this->partial_payment_helper->getSurchargeValue());
				
				if($this->partial_payment_helper->getCalculateSurchargeOn()==1) // surcharge value in percentage
				{
					$surcharge += $surchargeValues[$installments-1];
				}
				else
				{
					$surcharge = ((double)$value * $surchargeValues[$installments-1])/100;
				}	
				
			}
			return $surcharge;
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function getTotalSurchargeAmount($quote=null)
	{
		if($quote==null)
			$quote = $this->getQuote();
		$items = $quote->getAllVisibleItems();
		$surcharge=0;
		foreach($items as $item) 
		{
			$price = $item->getPrice();
			$product = Mage::getModel('catalog/product')->load($item->getProductId());
			if($this->partial_payment_helper->isPartialPaymentForWholecart() && Mage::getSingleton('core/session')->getAllowPartialPayment())
			{
				if($this->partial_payment_helper->isAllowFlexyPayment())
				{
					$surcharge += $this->getSurchargeAmount($price,Mage::getSingleton('core/session')->getAllowPartialPayment());
				}
				else
				{
					$surcharge += $this->getSurchargeAmount($price, $this->partial_payment_helper->getTotalIinstallments($product));
				}
			}
			else if($this->partial_payment_helper->getAllowPartialPaymentFromInfo($item))
			{
				if($this->partial_payment_helper->isAllowFlexyPayment($product))
				{
					$surcharge += ($this->getSurchargeAmount($price,$this->partial_payment_helper->getAllowPartialPaymentFromInfo($item))*$item->getQty());
				}
				else
				{
					$surcharge += ($this->getSurchargeAmount($price,$this->partial_payment_helper->getTotalIinstallments($product))*$item->getQty());
				}
			}
		}
		return $surcharge;
	}
	
	public function getOutofstockdiscountAmount($value)
	{
		try
		{
			$outofstockdiscount = $this->partial_payment_helper->getOutofstockdiscountValue();
			if($outofstockdiscount)
			{
				if($this->partial_payment_helper->getCalculateOutofstockdiscountOn()==1) // outofstockdiscount value fix
				{
					return -$outofstockdiscount;					
				}
				else
				{
					return -(((double)$value * $outofstockdiscount)/100);
				}
			}
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
		return 0;
	}
	
	public function getTotalOutofstockdiscountAmount($quote=null)
	{
		if($quote==null)
			$quote = $this->getQuote();
		$items = $quote->getAllVisibleItems();
		$outofstockdiscount=0;
		foreach($items as $item) 
		{
			$price = $item->getPrice();
			$product = Mage::getModel('catalog/product')->load($item->getProductId());
			if($this->partial_payment_helper->isPartialPaymentForOutOfStockProducts() && $this->partial_payment_helper->isOutOfStockProduct($item->getProductId()) && $this->partial_payment_helper->getAllowPartialPaymentFromInfo($item))
			{
				$outofstockdiscount += ($this->getOutofstockdiscountAmount($price)*$item->getQty());
			}
		}
		return $outofstockdiscount;
	}
	
	public function getQuote()
	{		
		$modules = Mage::getConfig()->getNode('modules')->children();
		$modulesArray = (array)$modules;
		if(isset($modulesArray['Milople_Partialpayment'])) 
		{
			$session = Mage::getSingleton('admin/session');
			if ($session->isLoggedIn()) 
			{
                $quote = Mage::getSingleton('adminhtml/session_quote')->getQuote();
			}
			else
			{
				$quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
				$quote = Mage::getModel('sales/quote')->load($quoteId);
			}
		}
		else
		{
			$quote = Mage::getSingleton('checkout/cart')->getQuote();
		}

		if(!$quote->getId())
		{ 
			$quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
			$quote = Mage::getModel('sales/quote')->load($quoteId);
		}

		return $quote;
	}
	
	public function getShippingOrBilling($quote=null)
	{
		if($quote==null)
			$quote = $this->getQuote();
		$items = $quote->getAllVisibleItems();
		foreach($items as $item)
		{
			if($item->getProductType()!='virtual' && $item->getProductType()!='downloadable')
			{
				//this doesnt return when called, even still can var_dump here
				return 'shipping';
			}
		}
		return 'billing';
	}
	
	public function getAddressData($quote=null)
	{
		if($quote==null)
			$quote = $this->getQuote();
		if($this->getShippingOrBilling()=='shipping')
			return $quote->getShippingAddress();
		else
			return $quote->getBillingAddress();
	}
	
	public function getPayingNowAmount($quote=null)
	{
		if($quote==null)
			$quote = $this->getQuote();
		$items = $quote->getAllVisibleItems();
		$payingNow = 0;
		$partialpaymenthelper = Mage::helper('partialpayment/partialpayment');
		if( $partialpaymenthelper->isPartialPaymentForWholecart() && Mage::getSingleton('core/session')->getAllowPartialPayment())
		{
			if( $partialpaymenthelper->isAllowFlexyPayment())
			{
				$payingNow = $this->getDownPaymentAmount($quote->getBaseSubtotal(),null,Mage::getSingleton('core/session')->getAllowPartialPayment());
			}
			else
			{
				$payingNow = $this->getDownPaymentAmount($quote->getBaseSubtotal());
			}
		}
		else
		{
			foreach($items as $item)
			{
				$product = Mage::getModel('catalog/product')->load($item->getProductId());
				$price = $item->getPrice();
				
				if( $partialpaymenthelper->getAllowPartialPaymentFromInfo($item))
				{
					if(($partialpaymenthelper->isPartialPaymentForSpecificProduct() && $product->getApplyPartialPayment()) || ($partialpaymenthelper->isPartialPaymentForOutOfStockProducts() && $partialpaymenthelper->isOutOfStockProduct($item->getProductId())) || ($partialpaymenthelper->isPartialPaymentForAllProduct()))
					{
						if( $partialpaymenthelper->isAllowFlexyPayment($product))
							$payingNow += ($this->getDownPaymentAmount($price,$product, $partialpaymenthelper->getAllowPartialPaymentFromInfo($item))*$item->getQty());
						else
							$payingNow += ($this->getDownPaymentAmount($price,$product)*$item->getQty());
					}
					else
					{
						$payingNow += ($price*$item->getQty());
					}
				}
				else
				{
					$payingNow += ($price*$item->getQty());
				}
			}
		}
		return $payingNow;
	}
	
	public function getAmountToBePaidLater($quote = null)
	{
		if(!$quote) $quote = $this->getQuote();
		$items = $quote->getAllVisibleItems();
		$price=0;
		foreach($items as $item) 
		{
			$price += ($item->getPrice()*$item->getQty());
		}
		return ($price-$this->getPayingNowAmount());
	}
	
	public function getSubtotal($quote)
	{
		$billing_address_id = $quote->getBillingAddress()->getSubtotalInclTax();
		$shipping_address_id = $quote->getShippingAddress()->getSubtotalInclTax();
		if($billing_address_id > 0) {
			$subTotal = $quote->getBillingAddress()->getSubtotal();
		}
		else {
			$subTotal = $quote->getShippingAddress()->getSubtotal();
		}
		return $subTotal;
	}
	
	public function calculateInstallmentDates($date,$i)
	{  
		$calculated_date = new DateTime($date);
		if($this->partial_payment_helper->getPaymentplan()==1) // add months
		{						
			$calculated_date->add(new DateInterval('P'.$i.'M'));
		} 
		else if ($this->partial_payment_helper->getPaymentplan()==2) // add weeks
		{
			$calculated_date->add(new DateInterval('P'.($i*7).'D'));
		} 
		else if ($this->partial_payment_helper->getPaymentplan()==3) // add days
		{
			$days = $this->partial_payment_helper->getNoOfDays() * ($i); 			
			$calculated_date->add(new DateInterval('P'.$days.'D'));
		}
		return $calculated_date->format('m-d-Y');
	}
	
	public function setCanceledOrderData($partial_paymentId)
	{
		$installmentDatas = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id',$partial_paymentId);
		
		foreach($installmentDatas as $installmentData)
		{
			$installmentData->setInstallmentStatus('Canceled')
					->setInstallmentPaidDate('')
					->setPaymentMethod('')
					->save();
		}
	}
	
	public function setOrderSuccessData($orderId)
	{
		$partial_payment = Mage::getModel('partialpayment/partialpayment')->getCollection();
		$partial_payment->addFieldToFilter('order_id',$orderId);
		$calculationModel = Mage::getModel("partialpayment/calculation");
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		$order = Mage::getModel('sales/order')->load($orderId);
		
		foreach ($partial_payment as $partial_item)
		{
			$totalAmount = $partial_item->getTotalAmount();
			$totalInstallment = $partial_item->getTotalInstallments();			
			$installmentData = Mage::getModel('partialpayment/installment')->getCollection()
							->addFieldToFilter('partial_payment_id', $partial_item->getPartialPaymentId())
							->getData();
			
			$amount = 0;
			if(isset($installmentData[0]['installment_id'])) 
			{
				$installmentModel = Mage::getModel('partialpayment/installment')->load($installmentData[0]['installment_id']);
				$amount = $installmentModel->getInstallmentAmount();
				$partial_payment_save = Mage::getModel('partialpayment/partialpayment')
									->setPaidAmount($amount)
									->setPaidInstallments(1)
									->setRemainingAmount($totalAmount - $amount)
									->setRemainingInstallments($totalInstallment-1)								
									->setPartialPaymentId($partial_item->getPartialPaymentId())
									->save();
				$installmentModel->setInstallmentPaidDate(date('Y-m-d'))
					->setInstallmentStatus('Paid')
					->setPaymentMethod($order->getPayment()->getMethodInstance()->getCode())
					->save();
			}
			
			foreach ($order->getAllVisibleItems() as $item) 
			{
				$partialPaymentProducts = Mage::getModel('partialpayment/product')->getCollection()->addFieldToFilter('sales_flat_order_item_id',$item->getQuoteItemId());
				$_product = Mage::getModel('catalog/product')->load($item->getProductId());
				
				foreach($partialPaymentProducts as $partialPaymentProduct)
				{ 
				
				if($partialpaymentHelper->isAllowFlexyPayment($_product))
				{
					$paid= $partialpaymentHelper->setNumberFormat($calculationModel->getDownPaymentAmount($item->getPrice(),$_product,$totalInstallment));
				}
				else
				{
					$paid= $partialpaymentHelper->setNumberFormat($calculationModel->getDownPaymentAmount($item->getPrice(),$_product));
				}
				$paid *= $item->getQtyOrdered();
				
				$paidInstallment=Mage::helper('partialpayment/partialpayment')->convertCurrencyAmount($paid);
				$remainingAmount =$partialPaymentProduct->getTotalAmount()-$paidInstallment;
									
				$partialPaymentProduct->setPaidInstallments(1)
										->setRemainingInstallments($partialPaymentProduct->getTotalInstallments()-1)
										->setPaidAmount($paidInstallment)
										->setRemainingAmount($remainingAmount)
										->save();
				}
			}
			$currency_code = $order->getOrderCurrency()->getCurrencyCode();
			Mage::helper('partialpayment/emailsender')->sendEmailSuccess($order->getCustomerFirstname(), $order->getCustomerEmail(), $order->getIncrementId(), $partial_item->getPartialPaymentId(),$currency_code);
		}
	}

	public function setInstallmentSuccessData ($installment_id, $order_id, $partial_payment_id, $payment_method, $transaction_id = '')
	{
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		$installmentData = Mage::getModel('partialpayment/installment')->load($installment_id);
		$instalmentamt = $installmentData->getInstallmentAmount();
		$partial_payment = Mage::getModel('partialpayment/partialpayment')->load($partial_payment_id);
		$installment_total_amount = 0;
		if(($partial_payment->getTotalAmount() > $partial_payment->getPaidAmount()) && ($partial_payment->getRemainingAmount() > 0) && (round($partial_payment->getRemainingAmount(),2) >= round($instalmentamt,2)))
		{	
			$partial_payment->setPaidAmount($partial_payment->getPaidAmount() + $instalmentamt);
			$partial_payment->setRemainingAmount($partial_payment->getRemainingAmount() - $instalmentamt);
			$partial_payment->setPaidInstallments($partial_payment->getPaidInstallments() + 1);
			$partial_payment->setRemainingInstallments($partial_payment->getRemainingInstallments() - 1);
			$partial_payment->save();		

			$productModel = Mage::getModel('partialpayment/product')->getCollection()
							->addFieldToFilter('partial_payment_id', $partial_payment_id);

			foreach($productModel as $product)
			{	
				if($product->getRemainingInstallments()>0)
				{
					$remain = $product->getRemainingAmount()/$product->getRemainingInstallments();
					$product->setRemainingInstallments($product->getRemainingInstallments() - 1);
					$product->setRemainingAmount($product->getRemainingAmount() - $remain);
					$product->setPaidInstallments($product->getPaidInstallments() + 1);
					$product->setPaidAmount($product->getPaidAmount() + $remain);
					$product->save();
				}
			}
			if($partial_payment->getTotalInstallments() == $partial_payment->getPaidInstallments())
			{	
				$partial_payment->setRemainingAmount(0);
				$partial_payment->setPaidAmount($partial_payment->getTotalAmount());
				$partial_payment->save();
			}
			$installmentData->setInstallmentPaidDate(date('Y-m-d'));
			$installmentData->setInstallmentStatus('Paid');
			$installmentData->setPaymentMethod($payment_method);
			$installmentData->setTransactionId($transaction_id);
			$installmentData->save();
		}

		$salesOrderCollection = Mage::getModel('sales/order')->load($order_id);
		$currency_code = $salesOrderCollection->getOrderCurrencyCode();
		$paid_amount = $salesOrderCollection->getPaidAmount()+$partialpaymentHelper->convertToOrderCurrencyAmount($instalmentamt,$currency_code);
		$base_paid_amount = $salesOrderCollection->getBasePaidAmount()+$instalmentamt;
		$remaining_amount = $salesOrderCollection->getRemainingAmount()-$partialpaymentHelper->convertToOrderCurrencyAmount($instalmentamt,$currency_code);
		$base_remaining_amount = $salesOrderCollection->getBaseRemainingAmount()-$instalmentamt;

		$salesOrderCollection->setPaidAmount($paid_amount);
		$salesOrderCollection->setRemainingAmount($remaining_amount);
		$salesOrderCollection->setBasePaidAmount($base_paid_amount);
		$salesOrderCollection->setBaseRemainingAmount($base_remaining_amount);
		$salesOrderCollection->save();

		# to update invoice and credit memo 
		$invoiceCollection = Mage::getModel('sales/order_invoice')->load($order_id,'order_id');
		$creditMemoCollection = Mage::getModel('sales/order_creditmemo')->load($order_id,'order_id');

		$invoice_data = $invoiceCollection->getData();
		$credit_memo_data = $creditMemoCollection->getData();
		if(!empty($invoice_data))
		{
			$invoiceCollection->setPaidAmount($paid_amount);
			$invoiceCollection->setRemainingAmount($remaining_amount);
			$invoiceCollection->setBasePaidAmount($base_paid_amount);
			$invoiceCollection->setBaseRemainingAmount($base_remaining_amount);
			$invoiceCollection->save();
		}
		if(!empty($credit_memo_data))
		{
			$creditMemoCollection->setPaidAmount($paid_amount);
			$creditMemoCollection->setRemainingAmount($remaining_amount);
			$creditMemoCollection->setBasePaidAmount($base_paid_amount);
			$creditMemoCollection->setBaseRemainingAmount($base_remaining_amount);
			$creditMemoCollection->save();
		}
		Mage::helper('partialpayment/emailsender')->sendInstallmentConfirmationMail($salesOrderCollection->getCustomerFirstname(),$salesOrderCollection->getCustomerEmail(),$salesOrderCollection->getIncrementId(),$instalmentamt,$partial_payment_id,$currency_code);
	}

	public function setInstallmentAsUnpaid ($installment, $partial_payment, $installment_status, $installment_due_date)
	{
		$partial_payment->setPaidAmount($partial_payment->getPaidAmount() - $installment->getInstallmentAmount());
		$partial_payment->setRemainingAmount($partial_payment->getRemainingAmount() + $installment->getInstallmentAmount());
		$partial_payment->setPaidInstallments($partial_payment->getPaidInstallments() - 1);
		$partial_payment->setRemainingInstallments($partial_payment->getRemainingInstallments() + 1);
		$partial_payment->save();		

		$productModel = Mage::getModel('partialpayment/product')->getCollection()
						->addFieldToFilter('partial_payment_id', $partial_payment->getPartialPaymentId());

		foreach($productModel as $product)
		{	
			$average_installment_amount = $product->getTotalAmount() / $product->getTotalInstallments();
			$product->setRemainingInstallments($product->getRemainingInstallments() + 1);
			$product->setRemainingAmount($product->getRemainingAmount() + $average_installment_amount);
			$product->setPaidInstallments($product->getPaidInstallments() - 1);
			$product->setPaidAmount($product->getPaidAmount() - $average_installment_amount);
			$product->save();
		}

		if($partial_payment->getPaidInstallments() == 0)
		{
			$partial_payment->setRemainingAmount($partial_payment->getTotalAmount());
			$partial_payment->setPaidAmount(0);
			$partial_payment->setRemainingInstallments($partial_payment->getTotalInstallments());
			$partial_payment->save();
		}

		$installment_due_date = explode('-', $installment_due_date);
		$installment->setInstallmentDueDate($installment_due_date[2] . '-' . $installment_due_date[0] . '-' . $installment_due_date[1]);
		$installment->setInstallmentStatus($installment_status);
        if($installment_status == 'Remaining'){
            $installment->setPaymentMethod('');
        }
		$installment->save();

		$salesOrderCollection = Mage::getModel('sales/order')->load($partial_payment->getOrderId());
		$paid_amount = $salesOrderCollection->getPaidAmount() - Mage::helper('partialpayment/partialpayment')->convertToOrderCurrencyAmount($installment->getInstallmentAmount(), $salesOrderCollection->getOrderCurrencyCode());
		$remaining_amount = $salesOrderCollection->getRemainingAmount() + Mage::helper('partialpayment/partialpayment')->convertToOrderCurrencyAmount($installment->getInstallmentAmount(), $salesOrderCollection->getOrderCurrencyCode());
		$base_paid_amount = $salesOrderCollection->getBasePaidAmount() - $installment->getInstallmentAmount();
		$base_remaining_amount = $salesOrderCollection->getBasePaidAmount() + $installment->getInstallmentAmount();

		$salesOrderCollection->setPaidAmount($paid_amount);
		$salesOrderCollection->setRemainingAmount($remaining_amount);
		$salesOrderCollection->setBasePaidAmount($base_paid_amount);
		$salesOrderCollection->setBaseRemainingAmount($base_remaining_amount);
		$salesOrderCollection->save();

		# to update invoice and credit memo 
		$invoiceCollection = Mage::getModel('sales/order_invoice')->load($partial_payment->getOrderId(), 'order_id');
		$creditMemoCollection = Mage::getModel('sales/order_creditmemo')->load($partial_payment->getOrderId(), 'order_id');

		$invoice_data = $invoiceCollection->getData();
		$credit_memo_data = $creditMemoCollection->getData();

		if(!empty($invoice_data))
		{
			$invoiceCollection->setPaidAmount($paid_amount);
			$invoiceCollection->setRemainingAmount($remaining_amount);
			$invoiceCollection->setBasePaidAmount($base_paid_amount);
			$invoiceCollection->setBaseRemainingAmount($base_remaining_amount);
			$invoiceCollection->save();
		}

		if(!empty($credit_memo_data))
		{
			$creditMemoCollection->setPaidAmount($paid_amount);
			$creditMemoCollection->setRemainingAmount($remaining_amount);
			$creditMemoCollection->setBasePaidAmount($base_paid_amount);
			$creditMemoCollection->setBaseRemainingAmount($base_remaining_amount);
			$creditMemoCollection->save();
		}

		if($installment_status == 'Canceled' || $installment_status == 'Failed') // Send installment fail or cancelation mail
		{
			Mage::helper('partialpayment/emailsender')->sendInstallmentFailureMail($installment->getInstallmentId());
		}
	}

	public function setInstallmentCancelData($installment_id)
	{
		$installment_save = Mage::getModel('partialpayment/installment')->load($installment_id);
		$installment_save->setInstallmentStatus('Canceled')->save();
		Mage::helper('partialpayment/emailsender')->sendInstallmentFailureMail($installment_id);
	}
}
?>
