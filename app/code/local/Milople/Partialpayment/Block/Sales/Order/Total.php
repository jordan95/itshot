<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Block_Sales_Order_Total extends Mage_Core_Block_Template
{
    /**
     * Get label cell tag properties
     *
     * @return string
     */
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }

    /**
     * Get order store object
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return $this->getParentBlock()->getOrder();
    }

    /**
     * Get totals source object
     *
     * @return Mage_Sales_Model_Order
     */
    public function getSource()
    {
        return $this->getParentBlock()->getSource();
    }

    /**
     * Get value cell tag properties
     *
     * @return string
     */
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }

    /**
     * Initialize reward points totals
     *
     * @return Enterprise_Reward_Block_Sales_Order_Total
     */
    public function initTotals()
    {
		
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		
		if($partialpaymentHelper->isPartialPaymentForWholecart())
				$condition = (((float) $this->getOrder()->getRemainingAmount() > 0) && ($this->getOrder()->getPaidAmount() > 0) && ($this->getOrder()->getSubtotalInclTax() >= $partialpaymentHelper->getMinimumWholecartOrderAmount() && $partialpaymentHelper->isPartialPaymentForWholecart() ) && ($this->getOrder()->getGrandTotal() > $this->getOrder()->getRemainingAmount()));
			else
				$condition = (((float) $this->getOrder()->getRemainingAmount() > 0) && ($this->getOrder()->getPaidAmount() > 0) && ($this->getOrder()->getGrandTotal() > $this->getOrder()->getRemainingAmount()));
		
        if ($condition) {
			
           $source = $this->getSource();
            			
			$value  = $source->getRemainingAmount();
			 $this->getParentBlock()->addTotal(new Varien_Object(array(
                'code'   => 'remaining',
                'strong' => true,
                'label'  => $partialpaymentHelper->getAmountToBePaidLater(),
                'value'  => $source instanceof Mage_Sales_Model_Order_Creditmemo ? - $value : $value,
				'area' => 'footer'
            )), 'grand_total');
			
			$value  = $source->getPaidAmount();
            $this->getParentBlock()->addTotal(new Varien_Object(array(
                'code'   => 'paid',
                'strong' => true,
                'label'  => $partialpaymentHelper->getDownpaymentLabel(1),
                'value'  => $source instanceof Mage_Sales_Model_Order_Creditmemo ? - $value : $value,
				'area' => 'footer'
            )), 'grand_total');
			
			$value  = $source->getOutofstockdiscountAmount();
			if($value < 0)
			{
				$this->getParentBlock()->addTotal(new Varien_Object(array(
					'code'   => 'outofdiscount',
					'strong' => true,
					'label'  => $partialpaymentHelper->getDiscountLabel(),
					'value'  => $source instanceof Mage_Sales_Model_Order_Creditmemo ? - $value : $value,
					'area' => 'footer'
				)), 'grand_total');
			} 
			
			$value  = $source->getSurchargeAmount();
			if($value > 0)
			{
				$this->getParentBlock()->addTotal(new Varien_Object(array(
					'code'   => 'surcharge',
					'strong' => true,
					'label'  => $partialpaymentHelper->getSurchargeLabel(),
					'value'  => $source instanceof Mage_Sales_Model_Order_Creditmemo ? - $value : $value,
					'area' => 'footer'
				)), 'grand_total');
			} 
        }
        return $this;
    }
}
