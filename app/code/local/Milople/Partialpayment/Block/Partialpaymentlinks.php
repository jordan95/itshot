<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Block_Partialpaymentlinks extends Mage_Core_Block_Abstract
{
	public function addLinkToParentBlock()
	{
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		// add partially paid orders grid on when partail payment is enabled
		if($partialpaymentHelper->isEnabled())
		{
			$parent = $this->getParentBlock();
			$parent->addLink($this->__('Partially Paid Orders'),'partialpayment/index/index',$this->__('Partially Paid Orders'));
		}
	}
}