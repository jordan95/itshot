<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Block_Adminhtml_Partialpayment_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{	
		parent::__construct();
		$this->setId('partialpaymentGrid');
		$this->setDefaultSort('partial_payment_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		//get detaful table name with prefix, if prefix is there in database
		$sales_flat_order = Mage::getSingleton('core/resource')->getTableName('sales_flat_order');
		$collection = Mage::getModel('partialpayment/partialpayment')->getCollection();
		$collection->getSelect()->joinLeft($sales_flat_order, 'main_table.order_id = '.$sales_flat_order.'.entity_id',array('increment_id','customer_firstname','customer_lastname', 'customer_email','order_currency_code','base_currency_code','created_at','status',));
		$collection->setOrder('partial_payment_id','DESC');
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{	
		$this->addColumn('order_id', array(
          'header'    => Mage::helper('partialpayment')->__('Order ID'),
          'align'     =>'center',
          'index'     => 'increment_id',
		));
		$this->addColumn('customer_first_name', array(
          'header'    => Mage::helper('partialpayment')->__('First Name'),
          'align'     =>'left',
          'index'     => 'customer_firstname',
		));
		$this->addColumn('customer_last_name', array(
          'header'    => Mage::helper('partialpayment')->__('Last Name'),
          'align'     =>'left',
          'index'     => 'customer_lastname',
		));
		$this->addColumn('customer_email', array(
          'header'    => Mage::helper('partialpayment')->__('Email Address'),
          'align'     =>'left',
          'index'     => 'customer_email',
		));
		$this->addColumn('paid_installment', array(
          'header'    => Mage::helper('partialpayment')->__('Paid Installments'),
          'align'     =>'center',
          'index'     => 'paid_installments',
		));
		$this->addColumn('remaining_installment', array(
          'header'    => Mage::helper('partialpayment')->__('Remaining Installments'),
          'align'     =>'center',
          'index'     => 'remaining_installments',
		));
		$this->addColumn('paid_amount', array(
          'header'    => Mage::helper('partialpayment')->__('Paid Amount'),
          'align'     =>'right',
		  'type'  => 'currency',
          'currency' => 'base_currency_code',
		  'index'     => 'paid_amount',
		));
		$this->addColumn('remaining_amount', array(
          'header'    => Mage::helper('partialpayment')->__('Remaining Amount'),
          'align'     =>'right',
		  'type'  => 'currency',
          'currency' => 'base_currency_code',
		  'index'     => 'remaining_amount',
		));
		$this->addColumn('created_at', array(
          'header'    => Mage::helper('partialpayment')->__('Order Date'),
          'align'     =>'center',
		  'type'	=>'datetime',
		  'format'    => 'MM-d-Y',
          'index'     => 'created_at',
		));
		$this->addColumn('partial_payment_status', array(
            'header'    => Mage::helper('partialpayment')->__('Status'),
            'align'     => 'center',
            'width'     => '80px',
            'index'     => 'status',
			'type'	  => 'options',
			'options' =>  array(
							'pending' => 'Pending',
							'processing' => 'Processing',
							'canceled' => 'Canceled',
							'complete' => 'Complete'
							),
		));
		$this->addColumn('action',
            array(
                'header'    =>  Mage::helper('partialpayment')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('partialpayment')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
	    
		$this->addExportType('*/*/exportCsv', Mage::helper('partialpayment')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('partialpayment')->__('XML'));
	  
     	return parent::_prepareColumns();
	}

    public function getRowUrl($row)
	{			
	  	return $this->getUrl('*/*/edit', array('id' => $row->getPartialPaymentId()));
	}

}