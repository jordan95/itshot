<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Block_Adminhtml_Partialpayment_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'partialpayment';
        $this->_controller = 'adminhtml_partialpayment';
        $this->_updateButton('save', 'label', Mage::helper('partialpayment')->__('Save Item'));
        $this->_removeButton('delete');
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinue()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('partialpayment_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'partialpayment_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'partialpayment_content');
                }
            }
		function saveAndContinue(){
                editForm.submit('" . $this->getUrl('adminhtml/partialpayment/save', array('action' => 'saveandcontinue')) . "');
            }";
    }

    public function getHeaderText()
    {
        if( Mage::registry('partialpayment_data') && Mage::registry('partialpayment_data')->getId() ) 
		{
            return Mage::helper('partialpayment')->__("Edit Partial Payment Order's Information", $this->htmlEscape(Mage::registry('partialpayment_data')->getTitle()));
        } 
		else 
		{
            return Mage::helper('partialpayment')->__('Edit Partial Payment Order Information');
        }
    }
}