<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Mini 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Block_Adminhtml_Partialpaymentrevenue extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{	
		$this->_controller = 'adminhtml_Partialpaymentrevenue';
		$this->_blockGroup = 'partialpayment';
		$this->_headerText = Mage::helper('partialpayment')->__('Revenue Generation Report');
		$this->_addButtonLabel = Mage::helper('partialpayment')->__('Add Item');
		parent::__construct();
		$this->_removeButton('add');
		$this->addButton('filter_form_submit', array(
            'label'     => Mage::helper('partialpayment')->__('Show Report'),
            'onclick'   => 'filterFormSubmit()'
        ));
	}
}