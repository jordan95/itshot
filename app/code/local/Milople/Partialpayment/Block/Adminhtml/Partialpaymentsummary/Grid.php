<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Block_Adminhtml_Partialpaymentsummary_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{	
		parent::__construct();
	 
		$this->setId('partialpaymentsummaryGrid');
		$this->setDefaultSort('partial_payment_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{		
		//get detaful table name with prefix, if prefix is there in database
		$sales_flat_order = Mage::getSingleton('core/resource')->getTableName('sales_flat_order');
		$partial_payment_orders = Mage::getSingleton('core/resource')->getTableName('partial_payment_orders');
		$collection = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToSelect(array('installment_due_date','installment_paid_date','installment_amount','installment_status','installment_id'));
		$collection->getSelect()->joinLeft($partial_payment_orders, 'main_table.partial_payment_id = '.$partial_payment_orders.'.partial_payment_id',array('order_id'));
		$collection->getSelect()->joinLeft(
		$sales_flat_order,$partial_payment_orders.'.order_id = '.$sales_flat_order.'.entity_id',array('customer_firstname','customer_lastname','customer_email','base_currency_code','increment_id'));
		$collection->setOrder('increment_id','DESC');
	
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()       
	{
		$this->addColumn('order_id', array(
          'header'    => Mage::helper('partialpayment')->__('Order ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'increment_id',
		));
		$this->addColumn('customer_name', array(
	  	  'header'    => Mage::helper('partialpayment')->__('First Name'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'customer_firstname',
		));
		$this->addColumn('customer_lastname', array(
	  	  'header'    => Mage::helper('partialpayment')->__('Last Name'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'customer_lastname',
		));
		$this->addColumn('customer_emial', array(
          'header'    => Mage::helper('partialpayment')->__('Email Address'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'customer_email',
		));
		$baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
		$this->addColumn('installment_amount', array(
          'header'    => Mage::helper('partialpayment')->__('Installment Amount'),
          'align'     =>'right',
          'width'     => '50px',
          'type'  => 'currency',
		  'currency_code' => $baseCurrencyCode,
		  'index'     => 'installment_amount',
		));
		$this->addColumn('installment_due_date', array(
          'header'    => Mage::helper('partialpayment')->__('Installment Due Date'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'installment_due_date',
		  'format'    => 'MM/dd/Y',
		  'type'      => 'date',
		));
		$this->addColumn('installment_paid_date', array(
          'header'    => Mage::helper('partialpayment')->__('Installment Paid Date'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'installment_paid_date',
		  'format'    => 'MM/dd/Y',
		  'type'      => 'date',
		));
		$this->addColumn('installment_status', array(
          'header'    => Mage::helper('partialpayment')->__('Installment Status'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'installment_status',
		  'type'	  => 'options',
		   'options' =>  array(
							'Paid' => 'Paid',
							'Remaining' => 'Remaining',
							'Canceled' => 'Canceled',
							'Failed' => 'Failed',
							),
		  
		));

		$this->addExportType('*/*/exportCsv', Mage::helper('partialpayment')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('partialpayment')->__('XML'));
	  
		return parent::_prepareColumns();
	}
	protected function _prepareMassaction() 
	{
		$this->getMassactionBlock()->setErrorText(
			Mage::helper('core')->jsQuoteEscape(
				Mage::helper('partialpayment')->__('Please select an installment')
			)
		);
		$this->setMassactionIdField('id');
		$this->getMassactionBlock()->setFormFieldName('partialpayment');
		$options = Mage::helper('payment')->getPaymentMethodList(true, false, false);
		$this->getMassactionBlock()->addItem('Remind', array(
			'label' => Mage::helper('partialpayment')->__('Send Installment Reminder Email'),
			'url' => $this->getUrl('*/*/massRemind'),
			));
			$this->getMassactionBlock()->addItem('changestatus', array(
			'label' => Mage::helper('partialpayment')->__('Send Installment Overdue Email'),
			'url' => $this->getUrl('*/*/massStatus'),
			));
			
		$methods = Mage::getModel('partialpayment/installmentSummaryPaymentMethods')->toOptionArray();
		
		if(!empty($methods))
		{
			$this->getMassactionBlock()->addItem('pay', array(
				'label' => Mage::helper('partialpayment')->__('Make Payment for selected Installments'),
				'url' => $this->getUrl('*/*/massPay', array('_current' => true)),
				'additional' => array(
					'visibility' => array(
						'name' => 'pay',
						'type' => 'select',
						'class' => 'required-entry',
						'label' => Mage::helper('partialpayment')->__('Select Payment Method'),
						'options' =>  $methods
					)
				)
				
			));
		}
		return $this;
	}
}