<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Block_Adminhtml_Partialpaymentreport_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{	
		parent::__construct();
		$this->setId('partialpaymentreportGrid');
		$this->setSaveParametersInSession(true);
		$this->setFilterVisibility(false);
		$this->setPagerVisibility(false);
	}

	protected function _prepareCollection()
	{
		//Custom code to jon both tables
		$collection2 = Mage::getModel('sales/order') ->getCollection()->addFieldToSelect('created_at');
                                $resource = Mage::getSingleton('core/resource');
                                $partial_payment_orders = $resource->getTableName('partial_payment_orders');
		$collection2->getSelect()->joinLeft(array('partial_payment_orders' => $partial_payment_orders), 'main_table.entity_id = partial_payment_orders.order_id',array());
		$collection2->getSelect()
                ->columns('count(order_id) as total_no_of_orders_with_partial_payment, sum(total_amount) as total_amount_of_partial_payment_orders, count(entity_id)-count(order_id) as total_no_of_orders_without_partial_payment, (case when (count(order_id) = 0) then sum(base_grand_total) else sum(base_grand_total)-sum(total_amount) end) as total_amount_of_non_partial_payment_orders')
                ->group('date(created_at)');
				
		$filter = $this->getRequest()->get('filter');	
		
		if (is_null($filter)) 
		{
            $filter = $this->_defaultFilter;
			$collection2->addFieldToFilter('created_at' , array('from'=>'0/0/0','to'=>'0/0/0','date' => true) );
        }
        if (is_string($filter)) 
		{
            $data = array();
            $filter = base64_decode($filter);
            parse_str(urldecode($filter), $data);
			$fromdate = Mage::app()->getLocale()->date($data['date_from'], Zend_Date::DATE_SHORT, null, false);
            $from = $fromdate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
            $todate = Mage::app()->getLocale()->date($data['date_to'],Zend_Date::DATE_SHORT, null, false);
            $todate->addDay('1');
			$to = $todate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
			# convert date in server time zone to get proper data
			$from = Mage::getSingleton('core/date')->gmtDate(null,$from);
			$to = Mage::getSingleton('core/date')->gmtDate(null,$to);
			$collection2->addFieldToFilter('created_at',array('from'=>$from,'to'=>$to));
		}
		$collection2->setOrder('created_at','DESC');
		foreach($collection2 as $data)
		{
			//$fromdate = Mage::app()->getLocale()->date($data->getCreatedAt(), Zend_Date::DATE_SHORT, null, false);
			//$from = $fromdate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
			$newDate = Mage::getSingleton('core/date')->gmtDate(null,$data->getCreatedAt());
			$data->setCreatedAt('2016-06-15 13:26:15');
		}
		Mage::log($collection2->getData(),null,'bh.log');
		$this->setCollection($collection2);
	}

	protected function _prepareColumns()       
	{
		$this->addColumn('created_at', array(
          'header'    => Mage::helper('partialpayment')->__('Date'),
          'align'     =>'center',
          'width'     => '50px',
          'index'     => 'created_at',
		  'type'	=>'datetime',
		  'format'    => 'MM-dd-Y',
		   'filter_index' => 'created_at',
		    ));
		$this->addColumn('partialpayment_order', array(
          'header'    => Mage::helper('partialpayment')->__('Total No Of Partial Payment Orders'),
          'align'     =>'center',
          'width'     => '50px',
          'index'     => 'total_no_of_orders_with_partial_payment',
		));
		$this->addColumn('partialpayment_amount', array(
          'header'    => Mage::helper('partialpayment')->__('Grand Total of Partial Payment Orders'),
          'align'     =>'right',
          'width'     => '50px',
		  'type'  => 'currency',
          'index'     => 'created_at',
		  'filter'  => false,
		  'type'    =>  'action',
          'renderer' => new Milople_Partialpayment_Block_Adminhtml_Partialpaymentreport_Revenue(),   
		));
		$this->addColumn('order_total', array(
          'header'    => Mage::helper('partialpayment')->__('Total No of Non Partial Payment Orders'),
          'align'     =>'center',
          'width'     => '50px',
          'index'     => 'total_no_of_orders_without_partial_payment',
		));
		$this->addColumn('order_amount', array(
           'header'    => Mage::helper('partialpayment')->__('Grand Total of Non Partial Payment Orders'),
          'align'     =>'right',
          'width'     => '50px',
		  'index'     => 'created_at',
		  'filter'  => false,
		  'type'    =>  'action',
          'renderer' => new Milople_Partialpayment_Block_Adminhtml_Partialpaymentreport_Count(),
		));
		$this->addExportType('*/*/exportCsv', Mage::helper('partialpayment')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('partialpayment')->__('XML'));
	}  
}