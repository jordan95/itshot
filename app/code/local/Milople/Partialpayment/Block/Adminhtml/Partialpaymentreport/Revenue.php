<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class  Milople_Partialpayment_Block_Adminhtml_Partialpaymentreport_Revenue extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract 
{
	public function render(Varien_Object $row)
    {
		$filter = $this->getRequest()->get('filter');

		if (is_string($filter)) 
		{
			$data = array();
			$filter = base64_decode($filter);
            parse_str(urldecode($filter), $data);	
		}
		else 
		{
			return 0;
		}
		$value =  $row->getData($this->getColumn()->getIndex());
		$collection2 = Mage::getModel('sales/order')->getCollection()->addFieldToSelect('created_at');
		$collection2->getSelect()->join('partial_payment_orders', 'main_table.entity_id = partial_payment_orders.order_id', array());
		$collection2->getSelect()->columns('sum((IFNULL(base_total_invoiced, 0) - IFNULL(base_tax_invoiced, 0) - IFNULL(base_shipping_invoiced, 0) - (IFNULL(base_total_refunded, 0) - IFNULL(base_tax_refunded, 0) - IFNULL(base_shipping_refunded, 0))) * IFNULL(base_to_global_rate, 0)) as total_amount_of_non_partial_payment_orders')
				->group('date(created_at)');
		if($value=="")
		{	
			$fromdate = Mage::app()->getLocale()->date($data['date_from'], Zend_Date::DATE_SHORT, null, false);
            $from = $fromdate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
		
			$todate = Mage::app()->getLocale()->date($data['date_to'],Zend_Date::DATE_SHORT, null, false);
            $todate->addDay('1');
			$to = $todate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
		
			$collection2->addFieldToFilter('created_at',array('from'=>$from,'to'=>$to));
			$amount = 0;
			foreach($collection2->getData() as $data)
			{
				$amount += $data['total_amount_of_non_partial_payment_orders'];
			}
			$currency_code=Mage::app()->getStore()->getBaseCurrencyCode();
			$ccode = Mage::app()->getLocale()->currency( $currency_code )->getSymbol();
			return $ccode.number_format((float)$amount, 2, '.', '');			
		}
		else
		{
			$date = Mage::helper('core')->formatDate($value);
			$fromdate = Mage::app()->getLocale()->date($date, Zend_Date::DATE_SHORT, null, false);
			$from = $fromdate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
			$todate = Mage::app()->getLocale()->date($date,Zend_Date::DATE_SHORT, null, false);
			$todate->addDay('1');
			$to = $todate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
			$currency_code = Mage::app()->getStore()->getBaseCurrencyCode();
			$ccode = Mage::app()->getLocale()->currency( $currency_code )->getSymbol();
			$collection2->addFieldToFilter('created_at',array('from'=>$from,'to'=>$to));
			$collectiondata2 = $collection2->getData('total_amount_of_non_partial_payment_orders');
			if (count($collectiondata2)) {
				return $ccode . number_format((float)$collectiondata2[0]['total_amount_of_non_partial_payment_orders'], 2, '.', '');
			}
			else {
				return $ccode . number_format((float)0, 2, '.', '');
			}
		}
    }
}
