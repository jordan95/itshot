<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Mini 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Block_Adminhtml_Partialpaymentrevenue_Filter_Form_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
  	{
		$filter = $this->getRequest()->get('filter');
		if (is_string($filter)) 
		{
            $data = array();
            $filter = base64_decode($filter);
            parse_str(urldecode($filter), $data);			
		}
		$form = new Varien_Data_Form(array(
               'id' => 'partialpaymentrevenue_reportdetail',
               'action' => $this->getUrl('*/*/index'),
               'method' => 'post',
               'enctype' => 'multipart/form-data'
               ));
		$form->setUseContainer(true);
		$this->setForm($form);
		$fieldset = $form->addFieldset('partialpaymentrevenue_reportdetail', array('legend'=>Mage::helper('partialpayment')->__('Filter')));
		$yrdata= strtotime('$data["date_to"]');
		if(!empty($data['date_to']))
		{
			$todate=date("d/m/Y", strtotime($data["date_to"]));
			$fromdate=date("d/m/Y", strtotime($data["date_from"]));
		}
		else
		{
			$todate="";
			$fromdate="";
		}
     
		$fieldset->addField('date_from', 'date', array(
          'label'     => Mage::helper('partialpayment')->__('Date From'),
          'tabindex' => 1,
		  'name' => 'date_from',
		  'index' => 'date_from',
		  'class'     => 'required-entry',
		  'required'  => true,
          'image' => $this->getSkinUrl('images/grid-cal.gif'),
		  'value' => $fromdate,
          'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
        ));
		
		 $fieldset->addField('date_to', 'date', array(
          'label'     => Mage::helper('partialpayment')->__('Date to'),
          'tabindex' => 1,
		  'name' => 'date_to',
		  'index' => 'date_from',
		  'class'     => 'required-entry',
		  'required'  => true,
		  'value' => $todate,
          'image' => $this->getSkinUrl('images/grid-cal.gif'),
          'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
        ));
     
		if ( Mage::getSingleton('adminhtml/session')->getPartialpaymentData() )
		{
			$form->setValues(Mage::getSingleton('adminhtml/session')->getPartialpaymentData());
			Mage::getSingleton('adminhtml/session')->getPartialpaymentData(null);
		} 
		elseif ( Mage::registry('partialpayment_data') ) 
		{
          $form->setValues(Mage::registry('partialpayment_data')->getData());
		}
		return parent::_prepareForm();
  	}
}
