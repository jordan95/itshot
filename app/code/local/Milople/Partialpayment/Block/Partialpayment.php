<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Block_Partialpayment extends Mage_Core_Block_Template
{
	protected function _prepareLayout()
    {	
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(10=>10,15=>15,20=>20,'all'=>'all'));
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    }
	
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
	public function __construct()
    {
        parent::__construct();
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		if($partialpaymentHelper->isEnabled())
		{
			$customer_id = Mage::getSingleton('customer/session')->getId();
					
			$collection = Mage::getModel('sales/order')->getCollection()
						  ->addFieldToFilter('status',array('nin' => array('canceled','closed')));
			
//			$arrInstallment=array();
//			if(sizeof($collection)) 
//			{
//				foreach ($collection as $order) 
//				{
//					$arrInstallment[]=$order->getIncrementId();
//				}
//			}
//			if(sizeof($arrInstallment)==0)
//			{
//				$arrInstallment[0]=0;
//			}
			//get detaful table name with prefix, if prefix is there in database
			$sales_flat_order = Mage::getSingleton('core/resource')->getTableName('sales_flat_order');
			$collection = Mage::getModel('partialpayment/partialpayment')->getCollection();
			$collection->getSelect()->joinLeft($sales_flat_order, 'main_table.order_id = '.$sales_flat_order.'.entity_id',array('customer_firstname','customer_lastname','customer_email','status','created_at','increment_id','customer_id','order_currency_code'));
				$collection->addFieldToFilter('customer_id', $customer_id)
				->setOrder('partial_payment_id','DESC');

			$this->setCollection($collection);
	    }
		else
		{
			//$this->_redirect("customer/account/index");	
	   	}
	   
	}
    public function getPartialpayment()     
    { 
        if (!$this->hasData('partialpayment')) 
		{
            $this->setData('partialpayment', Mage::registry('partialpayment'));
        }
        return $this->getData('partialpayment');
        
    }
}