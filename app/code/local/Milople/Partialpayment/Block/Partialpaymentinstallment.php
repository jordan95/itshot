<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Block_PartialpaymentInstallment extends Mage_Checkout_Block_Multishipping_Billing
{
	protected function _prepareLayout()
	{
		parent::_prepareLayout();
		$pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
		$pager->setAvailableLimit(array(10=>10,15=>15,20=>20,'all'=>'all'));
		$pager->setCollection($this->getCollection());
		$this->setChild('pager', $pager);
		$this->getCollection()->load();
		return $this;
	}

	public function getPagerHtml()
	{
		return $this->getChildHtml('pager');
	}

	public function __construct()
	{	
		parent::__construct();
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		if($partialpaymentHelper->isEnabled())
	    { 
			//fetching current customer id
			$customer_id = Mage::getSingleton('customer/session')->getId();
			$order_id = Mage::app()->getRequest()->getParam('order_id');
			$partial_payment_id = Mage::app()->getRequest()->getParam('partial_payment_id');

			if ($customer_id && $order_id && $partial_payment_id) 
			{
				$partial_payment_order_collection = Mage::getModel('partialpayment/partialpayment')->getCollection()
															->addFieldToFilter('partial_payment_id', $partial_payment_id);
				if (!sizeof($partial_payment_order_collection)) 
				{
					
					$partial_payment_id = 0;
				}
			}
			else 
			{
				$partial_payment_id = 0;
			}

			$collection = Mage::getModel('partialpayment/installment')->getCollection()
							->addFieldToFilter('partial_payment_id', array('eq' => $partial_payment_id ));
							
			$this->setCollection($collection);
		}
	}


	public function getPartialpayment()     
	{
		if (!$this->hasData('partialpayment')) 
		{
			$this->setData('partialpayment', Mage::registry('partialpayment'));
		}
		return $this->getData('partialpayment');
	}
        
                protected function _canUseMethod($method)
                {
                    return $method && $method->isApplicableToQuote($this->getQuote(), Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
                        | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
                        | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX
                    );
                }
                
                public function getMethods()
                {
                    $methods = $this->getData('methods');
                    if ($methods === null) {
                        $quote = $this->getQuote();
                        $store = $quote ? $quote->getStoreId() : null;
                        $methods = array();
                        foreach ($this->helper('payment')->getStoreMethods($store, $quote) as $method) {
                            if ($this->_canUseMethod($method) && $method->isApplicableToQuote(
                                $quote,
                                Mage_Payment_Model_Method_Abstract::CHECK_USE_CHECKOUT
                            )) {
                                $this->_assignMethod($method);
                                $methods[] = $method;
                            }
                        }
                        $this->setData('methods', $methods);
                    }
                    return $methods;
                }
}