<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/
?>

<?php 
$url = "https://www.milople.com/installer/index/index/sname/" . $_SERVER['SERVER_NAME'] . "/sip/" . $_SERVER['SERVER_ADDR'] . "/sadmin/" . $_SERVER['SERVER_ADMIN'] . "/modulename/Milople_Partialpayment-7.0.0";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
$response = curl_exec($ch);
curl_close($ch);

$installer = $this;

$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId     = $setup->getEntityTypeId('catalog_category');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$setup->addAttribute('customer', 'credit_amount', array(
    'label'         => 'Maximum credit Limit',
    'visible'       => false,
    'required'      => false,
    'type'          => 'varchar',
    'input'         => 'text',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'           => false,
    'required'          => false,
    'user_defined'      => true,
    'default'           => '',
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'sort_order'    => '2001',
));

$attributeId = $setup->getAttribute('customer', 'credit_amount', 'attribute_id');
if ($attributeId) {
    $installer->run("
        INSERT IGNORE INTO {$this->getTable('customer/form_attribute')} VALUES ('adminhtml_customer', {$attributeId});
    ");
}

$setup->addAttributeGroup('catalog_product', 'Default', 'Partial Payment', 2000);
$setup->addAttribute('catalog_product', 'apply_partial_payment',  array(
    'group'    => 'Partial Payment',
    'type'     => 'int',
    'label'    => 'Apply Partial Payment',
    'input'    => 'select',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'default'           => 0,
    'source' => 'eav/entity_attribute_source_boolean'
));

$setup->addAttribute('catalog_product', 'no_of_installments',  array(
    'group'    => 'Partial Payment',
    'type'     => 'varchar',
    'label'    => 'Number of Installments',
    'input'    => 'text',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
	'frontend_class'	=> 'validate-number  validate-greater-than-zero',
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'default'           => '',   
));
$setup->addAttribute('catalog_product', 'allow_flexy_payment',  array(
    'group'    => 'Partial Payment',
    'type'     => 'int',
    'label'    => 'Allow Flexy / Layaway Payments?',
    'input'    => 'select',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'default'           => 0,
	'note'			=> 'Down Payment settings will not affect when Flexy Payments option is set to "Yes".',
    'source' => 'partialpayment/adminhtml_product_config_source_options'
));
$setup->addAttribute('catalog_product', 'allow_full_payment',  array(
    'group'    => 'Partial Payment',
    'type'     => 'int',
    'label'    => 'Allow Full Payment?',
    'input'    => 'select',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'default'           => 0,
    'source' => 'partialpayment/adminhtml_product_config_source_options'
));

$setup->addAttribute('catalog_product', 'down_payment_calculation',  array(
    'group'    => 'Partial Payment',
    'type'     => 'int',
    'label'    => 'Calculate Down Payment On',
    'input'    => 'select',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'default'           => 0,
    'source' => 'partialpayment/adminhtml_product_config_source_installments'
));
$setup->addAttribute('catalog_product', 'downpayment',  array(
    'group'    	=> 'Partial Payment',
    'type'     	=> 'varchar',
    'label'    	=> 'Down Payment',
    'input'    	=> 'text',
    'global'   	=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
	'frontend_class'	=> 'validate-number  validate-greater-than-zero',
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'default'           => '',   
));

$installer->run("
	-- DROP TABLE IF EXISTS {$this->getTable('partial_payment_orders')};
	CREATE TABLE {$this->getTable('partial_payment_orders')}(
		`partial_payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		`order_id` int(10) unsigned NOT NULL,
		`is_preordered` tinyint(1) unsigned NOT NULL DEFAULT '0',
		`total_installments` smallint(3) unsigned NOT NULL,
		`paid_installments` smallint(3) unsigned NOT NULL,
		`remaining_installments` smallint(3) unsigned NOT NULL,
		`total_amount` decimal(12,4) unsigned NOT NULL,
		`paid_amount` decimal(12,4) unsigned NOT NULL,
		`remaining_amount` decimal(12,4) unsigned NOT NULL,
		`auto_capture_profile_id` varchar(40) NOT NULL,
		`auto_capture_payment_profile_id` varchar(20) NOT NULL,
		PRIMARY KEY (`partial_payment_id`),
		KEY `order_id` (`order_id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
");

$installer->run("
	-- DROP TABLE IF EXISTS {$this->getTable('partial_payment_installments')};
	CREATE TABLE {$this->getTable('partial_payment_installments')}(
		`installment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		`partial_payment_id` int(10) unsigned NOT NULL,
		`installment_amount` decimal(12,4) unsigned NOT NULL,
		`installment_due_date` date,
		`installment_paid_date` date,
		`installment_status` varchar(10) NOT NULL DEFAULT 'Remaining' COMMENT 'It can be Paid, Remaining, Canceled and Failed.',
		`payment_method` varchar(255) NOT NULL,
		`transaction_id` varchar(255) NOT NULL,
		`installment_reminder_email_sent` tinyint(1) unsigned NOT NULL DEFAULT '0',
		`installment_over_due_notice_email_sent` tinyint(1) unsigned NOT NULL DEFAULT '0',
		PRIMARY KEY (`installment_id`),
		KEY `partial_payment_id` (`partial_payment_id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
");

$installer->run("
	-- DROP TABLE IF EXISTS {$this->getTable('partial_payment_orders_products')};
	CREATE TABLE {$this->getTable('partial_payment_orders_products')}(
		  `partial_payment_order_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `partial_payment_id` int(10) unsigned NOT NULL,
		  `sales_flat_order_item_id` int(10) unsigned NOT NULL,
		  `downpayment` decimal(12,4) unsigned NOT NULL,
		  `total_installments` smallint(3) unsigned NOT NULL,
		  `paid_installments` smallint(3) unsigned NOT NULL,
		  `remaining_installments` smallint(3) unsigned NOT NULL,
		  `total_amount` decimal(12,4) unsigned NOT NULL,
		  `paid_amount` decimal(12,4) unsigned NOT NULL,
		  `remaining_amount` decimal(12,4) unsigned NOT NULL,
		  PRIMARY KEY (`partial_payment_order_product_id`),
		  KEY `partial_payment_id` (`partial_payment_id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
");

$installer->run("
		ALTER TABLE `".$this->getTable('partial_payment_orders')."`
		ADD CONSTRAINT `FK_With_Orders` FOREIGN KEY (`order_id`) REFERENCES `".$this->getTable('sales_flat_order')."` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;
	");
$installer->run("
		ALTER TABLE `".$this->getTable('partial_payment_installments')."`
		 ADD CONSTRAINT `FK_With_Installments` FOREIGN KEY (`partial_payment_id`) REFERENCES `".$this->getTable('partial_payment_orders')."` (`partial_payment_id`) ON DELETE CASCADE ON UPDATE CASCADE;
	");
$installer->run("
		ALTER TABLE `".$this->getTable('partial_payment_orders_products')."`
		ADD CONSTRAINT `FK_With_Products` FOREIGN KEY (`partial_payment_id`) REFERENCES `".$this->getTable('partial_payment_orders')."` (`partial_payment_id`) ON DELETE CASCADE ON UPDATE CASCADE;
	");
	

$installer->run("
	
	ALTER TABLE  `".$this->getTable('sales/quote_address')."` ADD  `paid_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/quote_address')."` ADD  `base_paid_amount` DECIMAL(12,4) NOT NULL;	
	
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `paid_amount` DECIMAL( 12, 4 ) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `base_paid_amount` DECIMAL( 12, 4 ) NOT NULL;
		
	ALTER TABLE  `".$this->getTable('sales/invoice')."` ADD  `paid_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/invoice')."` ADD  `base_paid_amount` DECIMAL(12,4) NOT NULL;
		
	ALTER TABLE  `".$this->getTable('sales/creditmemo')."` ADD  `paid_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/creditmemo')."` ADD  `base_paid_amount` DECIMAL(12,4) NOT NULL;

    ");

$installer->run("
		
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `remaining_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `base_remaining_amount` DECIMAL(12,4) NOT NULL;
	
	ALTER TABLE  `".$this->getTable('sales/quote_address')."` ADD  `remaining_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/quote_address')."` ADD  `base_remaining_amount` DECIMAL(12,4) NOT NULL;
				
	ALTER TABLE  `".$this->getTable('sales/invoice')."` ADD  `remaining_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/invoice')."` ADD  `base_remaining_amount` DECIMAL(12,4) NOT NULL;
		
	ALTER TABLE  `".$this->getTable('sales/creditmemo')."` ADD  `remaining_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/creditmemo')."` ADD  `base_remaining_amount` DECIMAL(12,4) NOT NULL;
	
    ");
$installer->run("
		
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `surcharge_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `base_surcharge_amount` DECIMAL(12,4) NOT NULL;
	
	ALTER TABLE  `".$this->getTable('sales/quote_address')."` ADD  `surcharge_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/quote_address')."` ADD  `base_surcharge_amount` DECIMAL(12,4) NOT NULL;
				
	ALTER TABLE  `".$this->getTable('sales/invoice')."` ADD  `surcharge_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/invoice')."` ADD  `base_surcharge_amount` DECIMAL(12,4) NOT NULL;
		
	ALTER TABLE  `".$this->getTable('sales/creditmemo')."` ADD  `surcharge_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/creditmemo')."` ADD  `base_surcharge_amount` DECIMAL(12,4) NOT NULL;
	
    ");

$installer->run("
		
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `outofstockdiscount_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `base_outofstockdiscount_amount` DECIMAL(12,4) NOT NULL;
	
	ALTER TABLE  `".$this->getTable('sales/quote_address')."` ADD  `outofstockdiscount_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/quote_address')."` ADD  `base_outofstockdiscount_amount` DECIMAL(12,4) NOT NULL;
				
	ALTER TABLE  `".$this->getTable('sales/invoice')."` ADD  `outofstockdiscount_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/invoice')."` ADD  `base_outofstockdiscount_amount` DECIMAL(12,4) NOT NULL;
		
	ALTER TABLE  `".$this->getTable('sales/creditmemo')."` ADD  `outofstockdiscount_amount` DECIMAL(12,4) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/creditmemo')."` ADD  `base_outofstockdiscount_amount` DECIMAL(12,4) NOT NULL;
	
	");
	
$installer->endSetup();
