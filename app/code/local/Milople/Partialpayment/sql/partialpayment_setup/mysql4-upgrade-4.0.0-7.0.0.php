<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/
?>

<?php 
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn(
    $installer->getTable('partial_payment_orders_products'), 'product_instock_email_sent', 'int(1) NOT NULL', '', array('default' => '0')
);

$installer->endSetup();
