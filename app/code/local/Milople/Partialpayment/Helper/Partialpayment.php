<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Helper_Partialpayment extends Mage_Core_Helper_Abstract
{
	public function getDownpaymentLabel($num=0)
	{
		if($num==1)
		{
			return Mage::helper('partialpayment')->__('Paid Amount');
		}
		else
		{
			return Mage::helper('partialpayment')->__('Down Payment');
		}
	}
	public function getAmountToBePaidLater($num=0)
	{
		return Mage::helper('partialpayment')->__('Amount To Be Paid Later');
	}
	public function getSurchargeLabel($num=0)
	{
		return Mage::helper('partialpayment')->__('Surcharge');
	}
	public function getDiscountLabel($num=0)
	{
		return Mage::helper('partialpayment')->__('Out Of Stock Discount');
	}
	public function isEnabled ()
	{
		if(Mage::getStoreConfig ('partialpayment/license_status_group/status', Mage::app()->getStore()) == '1')
		{
			return true;
		}
		return false;
	}
	public function getSerialKey ()
	{
		return(Mage::getStoreConfig ('partialpayment/license_status_group/serial_key', Mage::app()->getStore()));
	}
	public function isEnableAnalysticsAndReports() //Enable Analytics & Reports
	{
		return(Mage::getStoreConfig ('partialpayment/license_status_group/analytics_reports', Mage::app()->getStore()));
	}
	
	# General Settings Configuration Values
	public function isAdminEnabled ()//Partial Payment Apply Only In Order which are placed from admin
	{
		$session = Mage::getSingleton('admin/session');
		if($this->isEnabled() || (Mage::getStoreConfig ('partialpayment/license_status_group/status', Mage::app()->getStore()) == '2' && $session->isLoggedIn()))
			return true;
		return false;
	}
	public function getBrandLabel ()//Partial Payment Brand Label
	{
		return(Mage::getStoreConfig ('partialpayment/general_settings/brand_label', Mage::app()->getStore()));
	}
	public function  getAvailableTo ()//Partial Payment Available To
	{ 
		return(Mage::getStoreConfig ('partialpayment/general_settings/available_to', Mage::app()->getStore()));
	}	
	public function applyPartialPayment ()//Apply Partial Payment to
	{
		return(Mage::getStoreConfig ('partialpayment/general_settings/apply_partialpayment_to', Mage::app()->getStore()));
	}
	public function isPartialPaymentForSpecificProduct()//for All product
	{
		if($this->applyPartialPayment()==0)
			return true;
		else
			return false;
	}
	public function isPartialPaymentForOutOfStockProducts()//for All product
	{
		if($this->applyPartialPayment()==3)
			return true;
		else
			return false;
	}
	public function isPartialPaymentForAllProduct()//for specific product
	{
		if($this->applyPartialPayment()==1)
			return true;
		else
			return false;
	}
	public function isPartialPaymentForWholecart()//for wholecart
	{
		if($this->applyPartialPayment()==2)
			return true;
		else
			return false;
	}
	public function getAddToCartButtonLabel()
	{	
		if(Mage::getStoreConfig ('partialpayment/general_settings/addtocart_button_label', Mage::app()->getStore()))
		{
			return (Mage::getStoreConfig ('partialpayment/general_settings/addtocart_button_label', Mage::app()->getStore()));
		}
		return $this->__("Add To Cart");
	}
	public function getMinimumWholecartOrderAmount()// Minimum Order Amount
	{
		if($this->isPartialPaymentForWholecart())
		{
			if(Mage::getStoreConfig ('partialpayment/general_settings/minimum_wholecart_order_amount_limit', Mage::app()->getStore()))
			{
				return (Mage::getStoreConfig ('partialpayment/general_settings/minimum_wholecart_order_amount_limit', Mage::app()->getStore()));
			}
			return 0;
		}
	}
	
	public function getTotalIinstallments ($product=NULL)//Number of Installments
	{
		if($product && $product->getApplyPartialPayment() && $this->isPartialPaymentForSpecificProduct())//if selected product
		{
			if($product->getNoOfInstallments())
			{
				return $product->getNoOfInstallments();// returns number of installment value from specific product only
			}
		}
		//returns number of installments value from global config
		return(Mage::getStoreConfig ('partialpayment/general_settings/no_of_installments', Mage::app()->getStore()));
	}
	
	//to check apply partial payment automatically or not
	public function isAllowFullPayment ($product=NULL)//Allow Full Payment
	{
		if($product && $product->getApplyPartialPayment() && $this->isPartialPaymentForSpecificProduct())//if selected product
		{
			if($product->getAllowFullPayment())
			{
				if($product->getAllowFullPayment()==2)//to set No option value as 0
				{
					return 0;
				}
				else 
				{
					return $product->getAllowFullPayment();// returns number of installment value from specific product only
				}
			}
		}
		//returns number of installments value from globle config
		return(Mage::getStoreConfig ('partialpayment/general_settings/allow_fullpayment', Mage::app()->getStore()));
	}
	public function autocapture ()//Capture Installments Automatically
	{
		return(Mage::getStoreConfig ('partialpayment/general_settings/capture_installments_automatically', Mage::app()->getStore()));
	}
	public function isShippingTaxInAllProduct ()//Shipping and Tax Calculation Options
	{
		if(!$this->isAllowFullPayment())
		{
			return (Mage::getStoreConfig ('partialpayment/general_settings/shipping_and_tax_calculation_options_for_all_products', Mage::app()->getStore()));
		}
		return 1;
	}
	public function isDiscountInAllProduct ()//Discount Calculation Options
	{
		if(!$this->isAllowFullPayment())
		{
			return(Mage::getStoreConfig ('partialpayment/general_settings/discount_calculation_options_for_all_products', Mage::app()->getStore()));
		}
		return 1;
	}
	public function isShippingTaxInWholecart ()//Shipping and Tax Calculation Options 
	{
		return(Mage::getStoreConfig('partialpayment/general_settings/shipping_and_tax_calculation_options_for_wholecart', Mage::app()->getStore()));
	}
	public function isDiscountInWholecart ()//Discount Calculation Options
	{
		return(Mage::getStoreConfig ('partialpayment/general_settings/discount_calculation_options_for_wholecart', Mage::app()->getStore()));
	}
	public function isShippingAndTaxOnDownPayment()//if tax apply on downpayment then return true
	{
		if($this->isPartialPaymentForAllProduct())
		{
			return $this->isShippingTaxInAllProduct();
		}
		if($this->isPartialPaymentForWholecart())
		{
			return $this->isShippingTaxInWholecart();
		}
		return true;
	}
	public function isDiscountOnDownPayment()// if discount apply on downpayment then return true
	{
		if($this->isPartialPaymentForAllProduct())
		{
			return $this->isDiscountInAllProduct();
		}
		if($this->isPartialPaymentForWholecart())
		{
			return $this->isDiscountInWholecart();
		}
		return true;
	}
		
	
	# Payment Calculation Settings Values
	public function isAllowFlexyPayment($product=NULL)
	{
		if($product && $product->getApplyPartialPayment() && $this->isPartialPaymentForSpecificProduct())//if selected product
		{
			if($product->getAllowFlexyPayment()==1)
			{
				return 1;// returns 1 if flexy payment is yes
			}
			else if($product->getAllowFlexyPayment()==2)
			{
				return 0;// if flexy no from product tab
			}
		}
		//returns true if flexy payment is yes in global configuration
		return(Mage::getStoreConfig ('partialpayment/payment_calculation_settings/allow_flexy_payment', Mage::app()->getStore()));
	}
	public function getCalculateDownpaymentOn($product=NULL)//Calculate Down Payment On
	{
		if($product && $product->getApplyPartialPayment() && $this->isPartialPaymentForSpecificProduct())//if selected product
		{
			if($product->getDownPaymentCalculation())
			{
				return $product->getDownPaymentCalculation();// returns type of calculation for installment from specific product only
			}
		} 
		//returns type of calculation for installment from globle config
		return(Mage::getStoreConfig ('partialpayment/payment_calculation_settings/calculate_downpayment_on', Mage::app()->getStore()));
	}
	public function getDownPaymentValue ($product=NULL)//Down Payment
	{
		if($product && $product->getApplyPartialPayment() && $this->isPartialPaymentForSpecificProduct())//if selected product
		{
			if($product->getDownpayment())
			{
				return $product->getDownpayment();// returns type of calculation for installment from specific product only
			}
		}
		//returns type of calculation for installment from globle config
		return(Mage::getStoreConfig ('partialpayment/payment_calculation_settings/down_payment', Mage::app()->getStore()));
	}
	public function  getPaymentplan ()//Payment Plan
	{
		return(Mage::getStoreConfig ('partialpayment/payment_calculation_settings/payment_plan', Mage::app()->getStore()));
	}
	public function  getNoOfDays ()//Number of Days
	{
		return(Mage::getStoreConfig ('partialpayment/payment_calculation_settings/total_no_days', Mage::app()->getStore()));
	}
	public function isSurcharge()
	{
		return(Mage::getStoreConfig ('partialpayment/payment_calculation_settings/surcharge_options', Mage::app()->getStore()));
	}
	public function  getCalculateSurchargeOn ()//Calculate Surcharge On
	{ 
		return(Mage::getStoreConfig ('partialpayment/payment_calculation_settings/surcharge_installment_calculation_type', Mage::app()->getStore()));
	}		
	public function  getSurchargeValue ()//Surcharge
	{
		return(Mage::getStoreConfig ('partialpayment/payment_calculation_settings/surcharge_value', Mage::app()->getStore()));
	}
	public function  getCalculateOutofstockdiscountOn ()//Calculate Discount On
	{ 
		return(Mage::getStoreConfig ('partialpayment/payment_calculation_settings/outofstock_discount_calculation_type', Mage::app()->getStore()));
	}		
	public function  getOutofstockdiscountValue ()//Discount
	{
		return(Mage::getStoreConfig ('partialpayment/payment_calculation_settings/outofstock_discount_value', Mage::app()->getStore()));
	}
	
	
	# Allow Partial Payment to Specific Customer Groups
	public function  getDefaultCreditAmount ()//Credit Amount
	{ 
		return(Mage::getStoreConfig ('partialpayment/partialpayment_credit_setting/default_credit_amount', Mage::app()->getStore()));
	}	
	public function  getCreditLimitSurpassMessage ($limit, $currency_code)//Credit Limit Surpass Message
	{ 
		$limit = $this->getCurrencySymbol($currency_code).$limit;
		$msg = Mage::getStoreConfig ('partialpayment/partialpayment_credit_setting/credit_limit_surpass_message', Mage::app()->getStore());
		return str_replace("{credit_limit}",$limit,$msg);
	}

	# Emails and Notifications
	public function  emailsender()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/email_sender', Mage::app()->getStore()));
	}
	public function  emailcc()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/email_cc', Mage::app()->getStore()));
	}
	
	public function  orderConfimrationEmail()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/send_partially_paid_orders_confirmation_email', Mage::app()->getStore()));
	}
	public function  orderConfimrationEmailTemplate()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/partially_paid_orders_confirmation_email_template', Mage::app()->getStore()));
	}
	
	public function  autoCaptureReminderEmail()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/send_auto_capture_reminder_email', Mage::app()->getStore()));
	}
	public function  autoCaptureReminderEmailTempalte()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/auto_capture_reminder_email_template', Mage::app()->getStore()));
	}
	
	public function  installmentReminderEmail()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/send_installment_reminder_email', Mage::app()->getStore()));
	}
	public function  installmentReminderEmailTempalte()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/installment_reminder_email_template', Mage::app()->getStore()));
	}
	
	public function installmentPaymentConfirmationEmail()
	{
		return(Mage::getStoreConfig ('partialpayment/email_notification/send_installment_payment_confirmation_email', Mage::app()->getStore()));
	}
	public function installmentPaymentConfirmationEmailTempalte()
	{
		return(Mage::getStoreConfig ('partialpayment/email_notification/installment_payment_confirmation_email_template', Mage::app()->getStore()));
	}
	
	public function  installmentAutoCaptureFailureEmail()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/send_installment_auto_capture_failure_email', Mage::app()->getStore()));
	}
	public function  installmentAutoCaptureFailureEmailTempalte()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/installment_auto_capture_failure_email_template', Mage::app()->getStore()));
	}
	
	public function  installmentFailureEmail()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/send_installment_failure_email', Mage::app()->getStore()));
	}
	public function  installmentFailureEmailTempalte()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/installment_failure_email_template', Mage::app()->getStore()));
	}
	
	public function  installmentOverDueNoticeEmail()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/send_installment_over_due_notice_email', Mage::app()->getStore()));
	}
	public function  installmentOverDueNoticeEmailTempalte()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/installment_over_due_notice_email_template', Mage::app()->getStore()));
	}
	
	//instock mail for preorder
	public function  productInstockEmail()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/stock_availability_email_sender', Mage::app()->getStore()));
	}
	public function  productInstockEmailTempalte()
	{ 
		return(Mage::getStoreConfig ('partialpayment/email_notification/stock_availability_email_template', Mage::app()->getStore()));
	}
	
	
	public function isShippingIncludingTax()
	{
		return(Mage::getStoreConfig('tax/calculation/shipping_includes_tax', Mage::app()->getStore()));
	}
	public function isApplyPartialPayment($product)
	{
		if($this->isPartialPaymentForSpecificProduct())
			return $product->getApplyPartialPayment();
		return false;
	}
	
	//get partial payment is allow by product on cart page
	public function getAllowPartialPaymentFromInfo($product)
	{		
		$infoBuyRequest = $product->getOptionByCode('info_buyRequest');
		$buyRequest = new Varien_Object(unserialize($infoBuyRequest->getValue()));

		if($buyRequest['super_product_config']['product_type']=="grouped")
		{
			$session = Mage::getSingleton('admin/session');
			if ($session->isLoggedIn()) 
			{
				$buyRequest->setAllowPartialPayment($buyRequest['allow_partial_payment']);
			}
			else
			{
				$buyRequest->setAllowPartialPayment(Mage::getSingleton('core/session')->getData($buyRequest['super_product_config']['product_id']));
			}
		}
		return $buyRequest->getAllowPartialPayment();
	}
	// to check is product out of stock or not
	public function isOutOfStockProduct($product_id)
	{
		$product = Mage::getModel('catalog/product')->load($product_id);
		if ($product->getStockItem()->getStockQty()) {
			return false;
		}
		return true;
	}
	
	public function addOrdinalNumberSuffix($num) 
	{
		if (!in_array(($num % 100),array(11,12,13)))
		{
			switch ($num % 10) 
			{
				// Handle 1st, 2nd, 3rd
				case 1:  return $num.'<sup>st</sup>';
				case 2:  return $num.'<sup>nd</sup>';
				case 3:  return $num.'<sup>rd</sup>';
			}
		}
		return $num.'<sup>th</sup>';
	}
	# For checking customer groups
	public function isGuest()
	{
		if(Mage::getSingleton('customer/session')->isLoggedIn()) 
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	# Check whether the customer group is valid
	public function getCustomerGroup()
	{
		$group = explode(',', Mage::getStoreConfig('partialpayment/general_settings/customer_groups'));
		return $group;
	}
	
	public function isValidCustomerGroup()
	{
		$postData = Mage::app()->getRequest()->getPost();

		if (isset($postData['customer_id']))
	    {
			if ($postData['customer_id']) 
			{
				Mage::getSingleton('core/session')->setAdminhtmlCustomerId($postData['customer_id']);
				$customer = Mage::getModel('customer/customer')->load($postData['customer_id']);
			}
			else 
			{
				return false;
			}
		}
		elseif (Mage::getSingleton('core/session')->getAdminhtmlCustomerId()) {
			$customer = Mage::getModel('customer/customer')->load(Mage::getSingleton('core/session')->getAdminhtmlCustomerId());
		}
		else
		{
			$customer = Mage::getSingleton('customer/session')->getCustomer();
		}

		$customer_group = "";
		$customer_groups = $this->getCustomerGroup() ;
		
		if ($customer->getId())
		{
			$customer_group = $customer->getGroupId();
		
		}
		if($customer_group)
		{
			if(in_array($customer_group, $customer_groups)) 
			{	
				return true;
			}
		}
		return false;
	}

	public function isValidCustomer ()
	{
		$admin_selected_cust = Mage::getSingleton('core/session')->getData('cust_id');
		
		if ($this->getAvailableTo()==1)
		{   
			return true;
		}
		elseif ($this->getAvailableTo()==2)
		{
			if(isset($admin_selected_cust) && $admin_selected_cust > 0 )
			{
				return true;
			}
			elseif(!$this->isGuest())
			{
				return true;
			}
		}
		elseif($this->getAvailableTo()==3)
		{
			if ($this->isValidCustomerGroup())
			{	
				return true;
			}
		}
		return false;
	}
	public function setNumberFormat($number)
	{
		return number_format((float)$number, 2, '.', '');
	}
	public function getActivPaymentMethods($order_id)
    {
		$payment_method = new Mage_Checkout_Block_Onepage_Payment_Methods();
		$payment = Mage::helper('payment/data');
		$paymentMethods = $payment_method->getMethods();
		$methods = array();


		foreach ($paymentMethods as $paymentMethod) {
			$paymentCode = $paymentMethod->getCode();
			if($paymentMethod->canUseCheckout() == 1):
				$paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
				# And followed by your code
				$methods[$paymentCode] = array(
					'label' => $paymentTitle,
					'value' => $paymentCode,
				);
			endif;
		}

		return $methods;
    }
	public function convertOrderCurrencyAmount($price,$currentCurrency ='')
	{
		$baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
		if ($baseCurrencyCode != $currentCurrency && $currentCurrency!="") {
			$getCurrencyRates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, $currentCurrency);
			$getcurrentCurrencyRate = $getCurrencyRates[$currentCurrency];
			$price = str_replace( ',', '', $price );
			$price = $price/$getcurrentCurrencyRate;
		}
		$price = str_replace( ',', '', $price );
		return $price ;
	}
	public function validateWholecartMinAmount($price)
	{
		if($this->getMinimumWholecartOrderAmount() > 0 && $this->getMinimumWholecartOrderAmount() > $price)
		{
			return false;// if minimum wholecart amount create than subtotal return false not allow partial payment
		}
		return true;
	}
	public function convertCurrencyAmount ($price,$currentCurrency ='')
	{
		$baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
		if($currentCurrency != '')
		{
			$currentCurrencyCode = $currentCurrency ;
		}
		else
		{
			$currentCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
		}
		if ($baseCurrencyCode != $currentCurrencyCode) {
			$currencyRates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, $currentCurrencyCode);
			$currentCurrencyRate = $currencyRates[$currentCurrencyCode];
			$price = $price / $currentCurrencyRate;
		}
		return $price;
	}

	// To convert currency from base to order currency
	public function convertToOrderCurrencyAmount ($price, $currentCurrencyCode = '')
	{	
		$baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
		$session = Mage::getSingleton('admin/session');

		if ($baseCurrencyCode != $currentCurrencyCode) {
			$currencyRates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, $currentCurrencyCode);
			$currentCurrencyRate = $currencyRates[$currentCurrencyCode];
			$price = $price * $currentCurrencyRate;
		}

		return $price;
	}

	// To convert currency from base to current currency
	public function convertToCurrentCurrencyAmount ($price)
	{	
		$baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
		$session = Mage::getSingleton('admin/session');
		if ($session->isLoggedIn())
		 {
			$quote = Mage::getSingleton('adminhtml/session_quote')->getQuote();
			$currentCurrencyCode = $quote->getQuoteCurrencyCode() ;
			
		}
		else
		{
			$currentCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
		}
		
		if ($currentCurrencyCode && $baseCurrencyCode != $currentCurrencyCode) {
			$currencyRates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, $currentCurrencyCode);
                                                if(!isset($currencyRates[$currentCurrencyCode])){
                                                   Mage::throwException(sprintf('Sorry! We are not support your currency: %s', $currentCurrencyCode));
                                                }
                                                $currentCurrencyRate = $currencyRates[$currentCurrencyCode];
			$price = $price * $currentCurrencyRate;
		}

		return $price;
	}
	public function getCurrencySymbol($currency_code=NULL)
	{
		if($currency_code!=NULL)
		{
			$currency_symbol = Mage::app()->getLocale()->currency($currency_code)->getSymbol();
		}
		else
		{
			$currency_symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
		}
		if($currency_symbol==NULL)
			$currency_symbol = Mage::app()->getStore()->getCurrentCurrencyCode();
		return $currency_symbol;
	}
}
