<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2017 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Helper_Data extends Mage_Core_Helper_Abstract
{
	private $temp;
	
	public function convertCurrencyAmount ()
	{
		$baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
		if($currentCurrency != '')
		{
			$currentCurrencyCode = $currentCurrency ;
		}
		else
		{
			$currentCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
		}
		
		
		if ($baseCurrencyCode != $currentCurrencyCode) {
			$currencyRates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, $currentCurrencyCode);
			$currentCurrencyRate = $currencyRates[$currentCurrencyCode];
			$price = $price / $currentCurrencyRate;
		}

		return $price;
	}
	
	function getDomainName($url)
	{
		$custom_domain = preg_replace('#^https?://#', '', $url);
		$matchFound = preg_match('@^(?:http://)?([^/]+)@i', $custom_domain,$matches);
		if($matchFound)
		{
			$custom_domain = $matches[1];
		}
        $cmpstr = substr($custom_domain,0, 4);
		if($cmpstr == "www.")
		{
			$custom_domain = str_replace('www.',"",$custom_domain);       
		}
				
		return strtolower($custom_domain);
	}
	public function getDomain ()
    {
        $domain = $this->getDomainName(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB));
        $temp = explode('.', $domain);
        $exceptions = array('co.uk', 'com.au', 'com.hk', 'co.nz', 'co.in', 'com.sg');

		$count = count($temp);
		$last = $temp[($count-2)] . '.' . $temp[($count-1)];

		if(in_array($last, $exceptions)) {
			$new_domain = $temp[($count-3)] . '.' . $temp[($count-2)] . '.' . $temp[($count-1)];
		}
		else {
			$new_domain = $temp[($count-2)] . '.' . $temp[($count-1)];
		}
		return $new_domain;
    }


    public function checkEntry ($domain, $serial)
    {
        $key = sha1(base64_decode('UGFydGlhbFBheW1lbnQ='));
        if(sha1($key.$domain) == $serial)
		{
            return true;
        }
        return false;
    }


    public function canRun ($temp = '')
    {
        if($_SERVER['SERVER_NAME'] == "localhost" || $_SERVER['SERVER_NAME'] == "127.0.0.1") {
			return true;
		}
		//remove check for just4demo
		if(strpos($_SERVER['SERVER_NAME'],'just4demo') !== false){
            return true;
        }

		if ($temp == '') {
        	$temp = Mage::getStoreConfig('partialpayment/license_status_group/serial_key', Mage::app()->getStore());
		}

		$url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
		$parsedUrl = parse_url($url);
		$host = explode('.', $parsedUrl['host']);
		$subdomains = array_slice($host, 0, count($host) - 2 );

		if(sizeof($subdomains) && ($subdomains[0] == 'test' || $subdomains[0] == 'demo' || $subdomains[0] == 'dev')){
			return true;
		}

		$original = $this->checkEntry($_SERVER['SERVER_NAME'], $temp);
        $wildcard = $this->checkEntry($this->getDomain(), $temp);

        if(!$original && !$wildcard) {
            return false;
        }
        return true;
    }
   
      public function getCssBasedOnConfig()
     {
          $magentoVersion = Mage::getVersion();
		  
          if(version_compare($magentoVersion, '1.9', '<='))
          {
             return 'partialpayment/css/partialpayment_1.8.css';
		  }
     }

	public function getLicenseViolationMassage ()
	{
		return $this->__(base64_decode('PGRpdj5MaWNlbnNlIG9mIDxiPk1pbG9wbGUgUGFydGlhbCBQYXltZW50PC9iPiBleHRlbnNpb24gaGFzIGJlZW4gdmlvbGF0ZWQuIFRvIGdldCBzZXJpYWwga2V5IHBsZWFzZSBjb250YWN0IHVzIG9uIDxiPmh0dHBzOi8vd3d3Lm1pbG9wbGUuY29tL21hZ2VudG8tZXh0ZW5zaW9ucy9jb250YWN0cy88L2I+PC9kaXY+'));
	}
}