<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Helper_Emailsender extends Mage_Core_Helper_Abstract
{
                public function getStoreId(){
                    $storeId = Mage::app()->getStore()->getId();
                    return $storeId != Mage_Core_Model_App::ADMIN_STORE_ID ? $storeId: Mage::app()->getWebsite(true)->getDefaultGroup()->getDefaultStoreId();
                }

	public function sendEmailSuccess($customerName, $customer_email, $incrementId, $partial_payment_id, $currency_code)
	{
		$partial_payment_helper = Mage::helper('partialpayment/partialpayment');
		try
		{
			if(!$partial_payment_helper->orderConfimrationEmail())
			{
				return;
			}
			$storeId = $this->getStoreId();

			// Mail Data
			if ($partial_payment_id) 
			{
				$partialpayment_installment_grid = $this->getPartialPaymentGrid($partial_payment_id, $currency_code);
			}
			$data = array();
			$data['brand_label'] = $partial_payment_helper->getBrandLabel();
			$data['customer_name'] = $customerName;
			$data['order_id'] = $incrementId;
			$data['store_url'] = Mage::getBaseUrl();
			$data['user_partial_payment'] = Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id");
			$data['partialpayment_installment_grid'] = $partialpayment_installment_grid;
			$data['logo_src'] = $this->getLogoSrc();
			$data['logo_alt'] = $this->getLogoAlt();

			// Email Template 
			$translate = Mage::getSingleton('core/translate');
			$translate->setTranslateInline(false);
			$mailTemplate = Mage::getModel('core/email_template');
			$copyTo = $partial_payment_helper->emailcc();
			$emailInfo = Mage::getModel('core/email_info');
			$emailInfo->addTo($customer_email, $customerName);

			$sender = $partial_payment_helper->emailsender();
			$template = $partial_payment_helper->orderConfimrationEmailTemplate();
			if ($copyTo && $partial_payment_helper->orderConfimrationEmail()==2) 
			{
				// Add bcc to customer email
				$ccEmails = explode(",",$copyTo);
				foreach($ccEmails as $ccEmail)
				{
					$emailInfo->addBcc($ccEmail);
				}
			}
			$mailTemplate->addBcc($emailInfo->getBccEmails());
			$mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
				->sendTransactional(
					$template,
					$sender,
					$emailInfo->getToEmails(),
					$emailInfo->getToNames(),
					$data,
					$storeId
				 );

			$translate->setTranslateInline(true);
			if (!$mailTemplate->getSentSuccess()) {
				throw new Exception();
			}
			$translate->setTranslateInline(true);
			return;
		}
		catch(Exception $e) {
			return;
		}
	}


	public function sendIntallmentReminderMail($is_auto_capture,$customerName, $customer_email, $incrementId, $partial_payment_id, $next_installment_date, $currency_code)
	{
		$partial_payment_helper = Mage::helper('partialpayment/partialpayment');
		try
		{
			if($is_auto_capture && !$partial_payment_helper->autoCaptureReminderEmail())
			{
				return;
			}
			if(!$is_auto_capture && !$partial_payment_helper->installmentReminderEmail())
			{
				return;
			}
			$storeId = $this->getStoreId();

			// Mail Data
			$partialpayment_installment_grid = '';
			if ($partial_payment_id) 
			{
				$partialpayment_installment_grid = $this->getPartialPaymentGrid($partial_payment_id, $currency_code);
			}
			$data = array();
			$data['brand_label'] = $partial_payment_helper->getBrandLabel();
			$data['customer_name'] = $customerName;
			$data['order_id'] = $incrementId;
			$data['store_url'] = Mage::getBaseUrl();
			$data['user_partial_payment'] = Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id");
			$data['logo_src'] = $this->getLogoSrc();
			$data['logo_alt'] = $this->getLogoAlt();
			$data['partialpayment_installment_grid'] = $partialpayment_installment_grid;
			$data['next_installment_date'] = $next_installment_date;
			// Email Template 
			$translate = Mage::getSingleton('core/translate');
			$translate->setTranslateInline(false);
			$mailTemplate = Mage::getModel('core/email_template');
			$copyTo = $partial_payment_helper->emailcc();
			$emailInfo = Mage::getModel('core/email_info');
			$emailInfo->addTo($customer_email, $customerName);

			$sender = $partial_payment_helper->emailsender();
			$template = "";
			if($is_auto_capture)
			{
				$template = $partial_payment_helper->autoCaptureReminderEmailTempalte();
				if($copyTo && $partial_payment_helper->autoCaptureReminderEmail()==2) 
				{
					// Add bcc to customer email
					$ccEmails = explode(",",$copyTo);
					foreach($ccEmails as $ccEmail)
					{
						$emailInfo->addBcc($ccEmail);
					}
				}
			}
			else
			{
				$template = $partial_payment_helper->installmentReminderEmailTempalte();
				if($copyTo && $partial_payment_helper->installmentReminderEmail()==2) 
				{
					// Add bcc to customer email
					$ccEmails = explode(",",$copyTo);
					foreach($ccEmails as $ccEmail)
					{
						$emailInfo->addBcc($ccEmail);
					}
				}
			}

			$mailTemplate->addBcc($emailInfo->getBccEmails());
			$mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
				->sendTransactional(
					$template,
					$sender,
					$emailInfo->getToEmails(),
					$emailInfo->getToNames(),
					$data,
					$storeId
				 );

			$translate->setTranslateInline(true);
			if (!$mailTemplate->getSentSuccess()) {
				throw new Exception();
			}
			$translate->setTranslateInline(true);
			return;
		}
		catch(Exception $e){
			Mage::log("Exception" . $e);
			return;
		}
	}


	public function sendOverDueMail($customerName, $customer_email, $incrementId, $due_date, $installment_amount,$partial_payment_id, $currency_code)
	{
		$partial_payment_helper = Mage::helper('partialpayment/partialpayment');
		try
		{
			$storeId = $this->getStoreId();
			if(!$partial_payment_helper->installmentOverDueNoticeEmail())
			{
				return;
			}
			// Mail Data
			$data = array();
			$data['brand_label'] = $partial_payment_helper->getBrandLabel();
			$data['customer_name'] = $customerName;
			$data['order_id'] = $incrementId;
			$data['store_url'] = Mage::getBaseUrl();
			$data['user_partial_payment'] = Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id");
			$data['logo_src'] = $this->getLogoSrc();
			$data['logo_alt'] = $this->getLogoAlt();
			$data['due_date'] = $due_date;
			$data['installment_amount'] = $partial_payment_helper->setNumberFormat($installment_amount);

			// Email Template 
			$translate = Mage::getSingleton('core/translate');
			$translate->setTranslateInline(false);
			$mailTemplate = Mage::getModel('core/email_template');
			$copyTo = $partial_payment_helper->emailcc();

			$emailInfo = Mage::getModel('core/email_info');
			$emailInfo->addTo($customer_email, $customerName);

			$sender = $partial_payment_helper->emailsender();
			$template = $partial_payment_helper->installmentOverDueNoticeEmailTempalte();
			if ($copyTo && $partial_payment_helper->installmentOverDueNoticeEmail()==2) 
			{
				// Add bcc to customer email
				$ccEmails = explode(",",$copyTo);
				foreach($ccEmails as $ccEmail)
				{
					$emailInfo->addBcc($ccEmail);
				}
			}
			$mailTemplate->addBcc($emailInfo->getBccEmails());
			$mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
				->sendTransactional(
					$template,
					$sender,
					$emailInfo->getToEmails(),
					$emailInfo->getToNames(),
					$data,
					$storeId
				 );

			 $translate->setTranslateInline(true);
			if (!$mailTemplate->getSentSuccess()) {
				throw new Exception();
			}
			$translate->setTranslateInline(true); 
			return;
		}
		catch(Exception $e){
			Mage::log("Exception" . $e);
			
			return;
		}
	}


	public function sendInstallmentConfirmationMail($customerName, $customer_email, $incrementId, $installment_amount, $partial_payment_id,$currency_code)
	{
		echo $partial_payment_id;
		echo "<br>";
		$partial_payment_helper = Mage::helper('partialpayment/partialpayment');
		try
		{
			$orderId = Mage::getModel('partialpayment/partialpayment')->load($partial_payment_id)->getOrderId();
			$invoice = Mage::getModel('sales/order_invoice')->load($orderId,'order_id');
			if($invoice->getId())//if invoice is created then send partial payment total updated email
			{
				$invoice->sendEmail('1');
			}
			if(!$partial_payment_helper->installmentPaymentConfirmationEmail())
			{
				return;
			}
			$storeId = $this->getStoreId();

			// Mail Data
			$partialpayment_installment_grid = '';
			if ($partial_payment_id) 
			{
				$partialpayment_installment_grid = $this->getPartialPaymentGrid($partial_payment_id,$currency_code);
			}
			$data = array();
			$data['brand_label'] = $partial_payment_helper->getBrandLabel();
			$data['customer_name'] = $customerName;
			$data['order_id'] = $incrementId;
			$data['store_url'] = Mage::getBaseUrl();
			$data['user_partial_payment'] = Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id");
			$data['logo_src'] = $this->getLogoSrc();
			$data['logo_alt'] = $this->getLogoAlt();
			$data['installment_amount'] = $partial_payment_helper->getCurrencySymbol($currency_code) . $partial_payment_helper->setNumberFormat($partial_payment_helper->convertToOrderCurrencyAmount($installment_amount, $currency_code));
			$data['partialpayment_installment_grid'] = $partialpayment_installment_grid;

			// Email Template 
			$translate = Mage::getSingleton('core/translate');
			$translate->setTranslateInline(false);
			$mailTemplate = Mage::getModel('core/email_template');
			$copyTo = $partial_payment_helper->emailcc();
			$emailInfo = Mage::getModel('core/email_info');
			$emailInfo->addTo($customer_email, $customerName);

			$sender = $partial_payment_helper->emailsender();
			$template = $partial_payment_helper->installmentPaymentConfirmationEmailTempalte();
			if ($copyTo && $partial_payment_helper->installmentPaymentConfirmationEmail()==2)
			{
				// Add bcc to customer email
				$ccEmails = explode(",",$copyTo);
				foreach($ccEmails as $ccEmail)
				{
					$emailInfo->addBcc($ccEmail);
				}
			}
			$mailTemplate->addBcc($emailInfo->getBccEmails());
			$mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
				->sendTransactional(
					$template,
					$sender,
					$emailInfo->getToEmails(),
					$emailInfo->getToNames(),
					$data,
					$storeId
				 );

			 $translate->setTranslateInline(true);
			if (!$mailTemplate->getSentSuccess()) {
				throw new Exception();
			}
			$translate->setTranslateInline(true); 
			return;
		}
		catch(Exception $e){
			Mage::log("Exception" . $e);
			return;
		}
	}


	public function sendInstallmentFailureMail($installment_id) //$customerName, $customer_email, $incrementId, $installment_amount, $partial_payment_id
	{
		$partial_payment_helper = Mage::helper('partialpayment/partialpayment');
		try
		{
			if(!$partial_payment_helper->installmentFailureEmail())
			{
				return;
			}
			$installment_data    = Mage::getModel('partialpayment/installment')->load($installment_id);
			$partialpayment_data = Mage::getModel('partialpayment/partialpayment')->load($installment_data->getPartialPaymentId());
			$order_data			 = Mage::getModel('sales/order')->load($partialpayment_data->getOrderId());
			$customerName 		 = $order_data->getCustomerFirstname();
			$customer_email 	 = $order_data->getCustomerEmail();
			$incrementId 		 = $order_data->getIncrementId();
			$currency_code		 = $order_data->getOrderCurrency()->getCurrencyCode();
			$installment_amount  = $partial_payment_helper->setNumberFormat($installment_data->getInstallmentAmount());
			$partial_payment_id  = $installment_data->getPartialPaymentId();

			$storeId = $this->getStoreId();

			// Mail Data
			$data = array();
			$data['brand_label'] = $partial_payment_helper->getBrandLabel();
			$data['customer_name'] = $customerName;
			$data['order_id'] = $incrementId;
			$data['store_url'] = Mage::getBaseUrl();
			$data['user_partial_payment'] = Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id");
			$data['logo_src'] = $this->getLogoSrc();
			$data['logo_alt'] = $this->getLogoAlt();
			$data['installment_amount'] = $partial_payment_helper->getCurrencySymbol($currency_code).$installment_amount;
			
			// Email Template 
			$translate = Mage::getSingleton('core/translate');
			$translate->setTranslateInline(false);
			$mailTemplate = Mage::getModel('core/email_template');
			$copyTo = $partial_payment_helper->emailcc();
			$emailInfo = Mage::getModel('core/email_info');
			$emailInfo->addTo($customer_email, $customerName);
			$sender = $partial_payment_helper->emailsender();
			$template = $partial_payment_helper->installmentFailureEmailTempalte();
			if($copyTo && $partial_payment_helper->installmentFailureEmail()==2)
			{
				// Add bcc to customer email
				$ccEmails = explode(",",$copyTo);
				foreach($ccEmails as $ccEmail)
				{
					$emailInfo->addBcc($ccEmail);
				}
			}
			$mailTemplate->addBcc($emailInfo->getBccEmails());
			$mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
				->sendTransactional(
					$template,
					$sender,
					$emailInfo->getToEmails(),
					$emailInfo->getToNames(),
					$data,
					$storeId
				 );
			
			 $translate->setTranslateInline(true);
			if(!$mailTemplate->getSentSuccess()) 
			{
				throw new Exception();
			}
			$translate->setTranslateInline(true); 
			return;
		}
		catch(Exception $e){
			return;
		}
	}


	public function sendInstallmentAutoCaptureFailureMail($customerName, $customer_email, $incrementId, $installment_amount, $partial_payment_id, $currency_code, $is_last_try)
	{
		$partial_payment_helper = Mage::helper('partialpayment/partialpayment');
		try
		{
			if(!$partial_payment_helper->installmentAutoCaptureFailureEmail())
			{
				return;
			}
			$storeId = $this->getStoreId();

			// Mail Data
			$msg=NULL;

			if(!$is_last_try)
			{
                                                        $msg = '<p style="color:#000000; font-size:12px; margin-bottom: 20px;">Maintain sufficient balance in your credit card or check for the credit card expiry. <strong> <a href= "'.Mage::getBaseUrl().'">Contact us</a></strong> if you find any problem.</p><p style="color:#000000; font-size:12px; margin-bottom: 20px;">We will re-attempt installment payment capture after two days. OR <strong><a href= "'.Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id").'">logging into your account</a></strong> and pay installment now.</p>';
			}
			else
			{
                                                        $msg = '<p style="color:#000000; font-size:12px; margin-bottom: 20px;">Please make sure you make a manual payment for your instalment to avoid cancellation of this order by logging into your account <a href= "'.Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id").'">here</a>.</p>';
			}
			$data = array();
			$data['brand_label'] = $partial_payment_helper->getBrandLabel();
			$data['customer_name'] = $customerName;
			$data['order_id'] = $incrementId;
			$data['store_url'] = Mage::getBaseUrl();
			$data['user_partial_payment'] = Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id");
			$data['logo_src'] = $this->getLogoSrc();
			$data['logo_alt'] = $this->getLogoAlt();
			$data['installment_amount'] = $partial_payment_helper->getCurrencySymbol($currency_code).$partial_payment_helper->setNumberFormat($installment_amount);
			$data['message'] = $msg;

			// Email Template 
			$translate = Mage::getSingleton('core/translate');
			$translate->setTranslateInline(false);
			$mailTemplate = Mage::getModel('core/email_template');
			$copyTo = $partial_payment_helper->emailcc();
			$emailInfo = Mage::getModel('core/email_info');
			$emailInfo->addTo($customer_email, $customerName);

			$sender = $partial_payment_helper->emailsender();
			$template = $partial_payment_helper->installmentAutoCaptureFailureEmailTempalte();
			if ($copyTo && $partial_payment_helper->installmentAutoCaptureFailureEmail()==2) {
				// Add bcc to customer email
				$ccEmails = explode(",",$copyTo);
				foreach($ccEmails as $ccEmail)
				{
					$emailInfo->addBcc($ccEmail);
				}
			}
			$mailTemplate->addBcc($emailInfo->getBccEmails());
			$mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
				->sendTransactional(
					$template,
					$sender,
					$emailInfo->getToEmails(),
					$emailInfo->getToNames(),
					$data,
					$storeId
				 );
			
			 $translate->setTranslateInline(true);
			if (!$mailTemplate->getSentSuccess()) {
				throw new Exception();
			}
			$translate->setTranslateInline(true); 
			return;
		}
		catch(Exception $e){
			Mage::log("Exception" . $e);
			return;
		}
	}


	public function sendProductInstockMail($customerName, $customer_email, $incrementId, $partial_payment_id,$product_name,$due_date,$currency_code)
	{
		$partial_payment_helper = Mage::helper('partialpayment/partialpayment');
		try
		{
			if(!$partial_payment_helper->productInstockEmail())
			{
				return;
			}
			$storeId = $this->getStoreId();
			if ($partial_payment_id) 
			{
				$partialpayment_installment_grid = $this->getPartialPaymentGrid($partial_payment_id, $currency_code);
			}

			$msg = '';;
			if($due_date!='')
			{
				$due_date = new DateTime($due_date);
				$due_date_formated = $due_date->format('m-d-Y');
				$msg = "This email is to notify that the product <strong>$product_name</strong> you have ordered is now available in stock.You may <a href='".Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id")."' target='_blank'> pay the next installment</a> before <strong>$due_date_formated</strong>.";
			}

			// Mail Data
			$data = array();
			$data['brand_label'] = $partial_payment_helper->getBrandLabel();
			$data['customer_name'] = $customerName;
			$data['order_id'] = $incrementId;
			$data['store_url'] = Mage::getBaseUrl();
			$data['user_partial_payment'] = Mage::getUrl("partialpayment/index/installments/order_id/$incrementId/partial_payment_id/$partial_payment_id");
			$data['logo_src'] = $this->getLogoSrc();
			$data['logo_alt'] = $this->getLogoAlt();
			$data['partialpayment_installment_grid'] = $partialpayment_installment_grid;
			$data['msg']=$msg;

			// Email Template 
			$translate = Mage::getSingleton('core/translate');
			$translate->setTranslateInline(false);
			$mailTemplate = Mage::getModel('core/email_template');
			$copyTo = $partial_payment_helper->emailcc();
			$emailInfo = Mage::getModel('core/email_info');
			$emailInfo->addTo($customer_email, $customerName);

			$sender = $partial_payment_helper->emailsender();
			$template = $partial_payment_helper->productInstockEmailTempalte();
			if ($copyTo && $partial_payment_helper->orderConfimrationEmail()==2) 
			{
				// Add bcc to customer email
				$ccEmails = explode(",",$copyTo);
				foreach($ccEmails as $ccEmail)
				{
					$emailInfo->addBcc($ccEmail);
				}
			}
			$mailTemplate->addBcc($emailInfo->getBccEmails());
			$mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
				->sendTransactional(
					$template,
					$sender,
					$emailInfo->getToEmails(),
					$emailInfo->getToNames(),
					$data,
					$storeId
				 );
			
			$translate->setTranslateInline(true);
			if (!$mailTemplate->getSentSuccess()) {
				throw new Exception();
			}
			$translate->setTranslateInline(true);
			
			return;
		}
		catch(Exception $e){
			Mage::log("Exception" . $e);
			return;
		}
	}


	public function getPartialPaymentGrid($partial_payment_id, $currency_code)
	{
		$partial_payment_helper = Mage::helper('partialpayment/partialpayment');

		$partialpayment_installments_collection = Mage::getModel('partialpayment/installment')->getCollection()
						->addFieldToFilter('partial_payment_id', array('eq' => $partial_payment_id));
		$partialpayment_installments_collection = $partialpayment_installments_collection->getData();
		$partialpayment_installment_grid="";
		if (sizeof($partialpayment_installments_collection)) 
		{
			$partialpayment_installment_grid .= '<table style="width:100%; border-collapse: collapse; margin-bottom:20px" align="center" cellspacing="0" cellpadding="1" border="0">';
			$partialpayment_installment_grid .= '<tr>';
			$partialpayment_installment_grid .= '<th style ="border: 1px solid; padding: 10px 5px;">Installment</th>';
			$partialpayment_installment_grid .= '<th style ="border: 1px solid; padding: 10px 5px;">Installment Amount</th>';
			$partialpayment_installment_grid .= '<th style ="border: 1px solid; padding: 10px 5px;">Due Date</th>';
			$partialpayment_installment_grid .= '<th style ="border: 1px solid; padding: 10px 5px;">Paid Date</th>';
			$partialpayment_installment_grid .= '<th style ="border: 1px solid; padding: 10px 5px;">Installment Status</th>';
			$partialpayment_installment_grid .= '</tr>';

			$i = 0;
			$j = 1;
			for($i=0;$i<sizeof($partialpayment_installments_collection);$i++) 
			{
				$date = new DateTime($partialpayment_installments_collection[$i]['installment_due_date']);
				$due_date =  $date->format('m-d-Y');
				$paid_date=NULL;
				if($partialpayment_installments_collection[$i]['installment_paid_date']!="")
				{
					$install_date = new DateTime($partialpayment_installments_collection[$i]['installment_paid_date']);
					$paid_date = $install_date->format('m-d-Y');
				}
				$partialpayment_installment_grid .= '<tr>';
				$partialpayment_installment_grid .= '<td style ="border: 1px solid;" align="center">' . $partial_payment_helper->addOrdinalNumberSuffix($j) . '</td>';
				$j = $j + 1;
				$partialpayment_installment_grid .= '<td style ="border: 1px solid;" align="center">' . $partial_payment_helper->getCurrencySymbol($currency_code) . $partial_payment_helper->setNumberFormat($partial_payment_helper->convertToOrderCurrencyAmount($partialpayment_installments_collection[$i]['installment_amount'], $currency_code)) . '</td>';
				$partialpayment_installment_grid .= '<td style ="border: 1px solid;" align="center">' . $due_date . '</td>';
				$partialpayment_installment_grid .= '<td style ="border: 1px solid;" align="center">' . $paid_date . '</td>';
				$partialpayment_installment_grid .= '<td style ="border: 1px solid;" align="center">' . $partialpayment_installments_collection[$i]['installment_status'] . '</td>';
				$partialpayment_installment_grid .= '</tr>';
			}
			$partialpayment_installment_grid .= '</table>';
		}
		return $partialpayment_installment_grid;
	}


	public function getLogoSrc()
    {
        return Mage::getDesign()->getSkinUrl(Mage::getStoreConfig('design/header/logo_src'));
    }


    public function getLogoAlt()
    {
        return Mage::getStoreConfig('design/header/logo_alt');;
    }
}
