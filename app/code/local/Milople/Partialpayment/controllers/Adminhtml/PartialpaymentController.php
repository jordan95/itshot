<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Adminhtml_PartialpaymentController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('partialpayment/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		return $this;
	}   

	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	// this function added to solve access dined issue of new patch of matento
	protected function _isAllowed()	{
		return true;
	}

	public function editAction() {
		$id = $this->getRequest()->getParam('id');
		$model = Mage::getModel('partialpayment/partialpayment')->load($id);
		$orderId = $model->getOrderId(); // existing order to duplicate
		$session = Mage::getSingleton('adminhtml/session_quote');
		$session->clear();
		$ordercreatemodel = Mage::getSingleton('adminhtml/sales_order_create');
		$order = Mage::getModel('sales/order')->load($orderId);

		if ($order->getId()) {
			try	{
				$order->setReordered(true);
				$session->setUseOldShippingMethod(true);
				$ordercreatemodel->initFromOrder($order);
			}
			catch(Exception $e)	{
				Mage::log($e->getMessage());
			}
		}
		else{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__('Partially paid order is not available.'));
		}

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			Mage::register('partialpayment_data', $model);
			$this->loadLayout();
			$this->_setActiveMenu('partialpayment/items');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock('partialpayment/adminhtml_partialpayment_edit'))
				->_addLeft($this->getLayout()->createBlock('partialpayment/adminhtml_partialpayment_edit_tabs'));
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}

	public function newAction() {
		$this->_forward('edit');
	}

	public function saveAction() {
		$postData = $this->getRequest()->getPost();
		$action = Mage::app()->getRequest()->getParam('action');
		$total_installment_paid = 0;
		$total_installment_updated = 0;
		$error_message = '';
		$installments_to_pay = array();
		$installments_to_pay_amount = 0;

		if (!isset($postData['partial_payment_id'])) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__('Unable to edit partially paid order\'s information.'));
			$this->_redirect('adminhtml/partialpayment/index');
		}
		elseif(!isset($postData['installment_id']))
		{	
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__('Please select an installment.'));
			$this->_redirect('*/*/edit', array('id' => $postData['partial_payment_id']));
		}
		else
		{
			$partial_payment_data = Mage::getModel('partialpayment/partialpayment')->load($postData['partial_payment_id']);
			$order = Mage::getModel('sales/order')->load($partial_payment_data->getOrderId());
			foreach($postData['installment_id'] as $installment)
			{
				$partial_payment_installment_data = Mage::getModel('partialpayment/installment')->load($installment);
				if($partial_payment_installment_data->getInstallmentStatus() != 'Paid') 
				{
					if (isset($postData['payment']['method']) && $postData['payment']['method'] != '') 
					{
						array_push($installments_to_pay, array('installment_id' => $installment));
						$installments_to_pay_amount += $partial_payment_installment_data->getInstallmentAmount();
						$total_installment_paid++;
					}
					elseif ($postData['installment'][$installment]['installment_status'] == 'Paid') 
					{
						if ($error_message == '') 
						{
							$error_message = 'Please select a payment method to pay installment.';
						}
						else 
						{
							$error_message = 'Please select a payment method to pay installments.';
						}
					}
					else 
					{
						$installment_due_date = explode('/', $postData['installment'][$installment]['installment_due_date']);
						$partial_payment_installment_data->setInstallmentDueDate($installment_due_date[2] . '-' . $installment_due_date[0] . '-' . $installment_due_date[1]);
						$partial_payment_installment_data->setInstallmentStatus($postData['installment'][$installment]['installment_status']);
						$partial_payment_installment_data->save();
						$total_installment_updated++;
					}
				}
				elseif ($partial_payment_installment_data->getInstallmentStatus() == 'Paid' && $postData['installment'][$installment]['installment_status'] != 'Paid') 
				{
					Mage::getModel('partialpayment/calculation')->setInstallmentAsUnpaid($partial_payment_installment_data, $partial_payment_data, $postData['installment'][$installment]['installment_status'], $postData['installment'][$installment]['installment_due_date']);
					$total_installment_updated++;
				}
				else 
				{
					$installment_due_date = explode('/', $postData['installment'][$installment]['installment_due_date']);
					$partial_payment_installment_data->setInstallmentDueDate($installment_due_date[2] . '-' . $installment_due_date[0] . '-' . $installment_due_date[1]);
					$partial_payment_installment_data->setInstallmentStatus($postData['installment'][$installment]['installment_status']);
					$partial_payment_installment_data->save();
					$total_installment_updated++;
				}

			}
			if(isset($postData['save_for_future']) && $postData['save_for_future']==1)
			{
				$orderPayment = Mage::getModel('sales/order_payment')->load($order->getId(),'parent_id');
				if(isset($postData['payment'])){
					if($postData['payment']['method']==='authorizenet'){
						if($orderPayment){
							$orderPayment->setCcType($postData['payment']['cc_type']);
							$orderPayment->setCcExpMonth($postData['payment']['cc_exp_month']);
							$orderPayment->setCcExpYear($postData['payment']['cc_exp_year']);
							$orderPayment->setCcNumberEnc($orderPayment->encrypt($postData['payment']['cc_number']));
							$orderPayment->save();
						}
					}
				}
			}
			if ($error_message != '') 
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__($error_message));
			}

			if($total_installment_updated == 1)
			{
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('partialpayment')->__('%s installment of order #%s has been updated successfully.', $total_installment_updated, $order->getIncrementId()));
				
			}
			elseif ($total_installment_updated > 1)
			{
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('partialpayment')->__('%s installments of order #%s have been updated successfully.', $total_installment_updated, $order->getIncrementId()));
			}

			if (count($installments_to_pay)) 
			{
				if (!$this->_capturePayment($installments_to_pay, $order, $installments_to_pay_amount, $postData['payment'], $postData['partial_payment_id'], $postData)) 
				{
					$total_installment_paid = 0;
				}
			}

			if($total_installment_paid == 1)
			{
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('partialpayment')->__('%s installment of order #%s has been paid successfully.', $total_installment_paid, $order->getIncrementId()));
			}
			elseif ($total_installment_paid > 1)
			{
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('partialpayment')->__('%s installments of order #%s have been paid successfully.', $total_installment_paid, $order->getIncrementId()));
			}

			if ($action == 'saveandcontinue') 
			{
				$this->_redirect('adminhtml/partialpayment/edit', array('id' => $postData['partial_payment_id']));
			}
			else 
			{
				$this->_redirect('adminhtml/partialpayment/index');
			}
		}
	}

	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('partialpayment/partialpayment');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $partialpaymentIds = $this->getRequest()->getParam('partialpayment');
        if(!is_array($partialpaymentIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($partialpaymentIds as $partialpaymentId) {
                    $partialpayment = Mage::getModel('partialpayment/partialpayment')->load($partialpaymentId);
                    $partialpayment->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($partialpaymentIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction() {
        $partialpaymentIds = $this->getRequest()->getParam('partialpayment');
        if(!is_array($partialpaymentIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($partialpaymentIds as $partialpaymentId) {
                    $partialpayment = Mage::getSingleton('partialpayment/partialpayment')
									  ->load($partialpaymentId)
									  ->setPartialPaymentStatus($this->getRequest()->getParam('partial_payment_status'))
									  ->setIsMassupdate(true)
									  ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($partialpaymentIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction() {
        $fileName   = 'partialpayment.csv';
        $content    = $this->getLayout()->createBlock('partialpayment/adminhtml_partialpayment_grid')
            ->getCsv();
        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction() {
        $fileName   = 'partialpayment.xml';
        $content    = $this->getLayout()->createBlock('partialpayment/adminhtml_partialpayment_grid')
            ->getXml();
        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream') {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

	protected function _capturePayment($installments_to_pay, $order, $installment_amount, $paymentInfo, $partial_payment_id, $post) {
                                $action = Mage::app()->getRequest()->getParam('action');
		$invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
		$invoice->setState(Mage_Sales_Model_Order_Invoice::STATE_OPEN);

		try
		{
			if(is_array($paymentInfo) && count($paymentInfo) && isset($paymentInfo['method']))
			{
				$invoice->setBaseGrandTotal($installment_amount);
				$invoice->setGrandTotal($installment_amount);
				$invoice->getOrder()->setTotalDue($installment_amount);
				$capturePayment = Mage::getModel('partialpayment/sales_order_capture_payment');
				$capturePayment->setOrder($invoice->getOrder());
				$capturePayment->importData($paymentInfo);	
				$capturePayment->setAmountOrdered($installment_amount);
				$capturePayment->setBaseAmountOrdered($installment_amount);
				$capturePayment->setShippingAmount(0);
				$capturePayment->setBaseShippingAmount(0);
				$capturePayment->setAmountAuthorized($installment_amount);
				$capturePayment->setBaseAmountAuthorized($installment_amount);
				$clonedInvoice = clone $invoice;
				$invoice->getOrder()->addRelatedObject($capturePayment);
				
				if($paymentInfo['method'] == 'ewayrapid_ewayone')
				{
					$action = strtolower(Mage::getStoreConfig('payment/ewayrapid_general/payment_action'));
				}
				
				if($paymentInfo['method'] == 'authorizenet')
				{
					if(isset($post['save_for_future']) && $post['save_for_future']==1)
					{
						$paymentModel = Mage::getModel('partialpayment/payment_method_authorizenet');
						$service = $paymentModel->getWebService();
						//$service->setPayment($capturePayment->getPayment());
						$subscription = new Varien_Object();
						$subscription->setStoreId($order->getStoreId());
						$service->setSubscription($subscription);
						$data = json_decode(json_encode($service->createCIMAccountOnInstallmentPayment($order,$paymentInfo)),true);
						
						if(isset($data['CreateCustomerPaymentProfileResult'])){
							$profileId = $data['CreateCustomerPaymentProfileResult']['customerProfileId'];
						}
						if(isset($data['profile'])){
							$profileId = $data['profile']['customerProfileId'];
						}
						$paymentProfiles = array();
						
						
						if(isset($data['profile']) && sizeof($data['profile']['paymentProfiles']['CustomerPaymentProfileMaskedType']) > 1)
						{
							$total_paymentProfiles = $data['profile']['paymentProfiles']['CustomerPaymentProfileMaskedType'];
							for($i=1;$i<= sizeof($total_paymentProfiles);$i++)
							{  
								if($i == sizeof($total_paymentProfiles) )
								{
									$no = $i - 1 ;
									if(isset($data['profile']['paymentProfiles']['CustomerPaymentProfileMaskedType']['customerPaymentProfileId'])){
										$paymentProfiles = $data['profile']['paymentProfiles']['CustomerPaymentProfileMaskedType']['customerPaymentProfileId'];
									}

									if(isset($data['profile']['paymentProfiles']['CustomerPaymentProfileMaskedType'][$no]['customerPaymentProfileId'])){
										$paymentProfiles = $data['profile']['paymentProfiles']['CustomerPaymentProfileMaskedType'][$no]['customerPaymentProfileId'];
									}
								}
								else
								{
									continue ;  
								}
							}
							$realId = $paymentProfiles;
						}
						else
						{
							if(isset($data['CreateCustomerPaymentProfileResult'])){
								$realId = $data['CreateCustomerPaymentProfileResult']['customerPaymentProfileId'];
							}
							if(isset($data['profile'])){
								$realId = $data['profile']['customerPaymentProfileId'];
							}
						}
						if($realId !='' && $profileId !='')
							Mage::getModel('partialpayment/partialpayment')->load($partial_payment_id)->setAutoCaptureProfileId($profileId)->setAutoCapturePaymentProfileId($realId)->save();
					}
					$capturePayment->capture($clonedInvoice);
					$capturePayment->pay($clonedInvoice);
				}else{
					# Compatible pay flow pro payment method
					if (strtolower(Mage::getStoreConfig('payment/' . $paymentInfo['method'] . '/payment_action')) == 'authorize' || strtolower(Mage::getStoreConfig('payment/' . $paymentInfo['method'] . '/payment_action')) == 'authorization' || $action == 'authorize') {
						$capturePayment->_authorize(true, $installment_amount);
					}
					else {
						if($capturePayment->canCapture())
						{
							if($paymentInfo['method'] != 'sagepaydirectpro')
							{
								$capturePayment->capture($clonedInvoice);
								$capturePayment->pay($clonedInvoice);
							}
						}
						else
						{
							$capturePayment->pay($clonedInvoice);
						}
					}
				}
				foreach ($installments_to_pay as $installment) {
					Mage::getModel('partialpayment/calculation')->setInstallmentSuccessData($installment['installment_id'], $order->getEntityId(), $partial_payment_id, $paymentInfo['method']);
				}
				return true;
			}
		} 
		catch(Exception $e)
		{
			foreach ($installments_to_pay as $installment) {
				$installment_data = Mage::getModel('partialpayment/installment')->load($installment['installment_id']);
				$installment_data->setInstallmentStatus('Failed');
				$installment_data->save();
				Mage::helper('partialpayment/emailsender')->sendInstallmentFailureMail($installment['installment_id']);
			}
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__("Gateway Error: Cannot pay installment now. Please try again later."));
			$this->_redirect('*/*/edit', array('id' => $partial_payment_id));
			return false;
		}
	}
}
