<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Adminhtml_PartialpaymentsummaryController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('partialpayment/summary')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Partial Summary'), Mage::helper('adminhtml')->__('Partial Summary'));
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}
	
	// this function added to solve access dined issue of new patch of matento
	protected function _isAllowed()	{
		return true;
	}

	public function massStatusAction() {
		//get detaful table name with prefix, if prefix is there in database
		$sales_flat_order = Mage::getSingleton('core/resource')->getTableName('sales_flat_order');
		$conditionIds = $this->getRequest()->getParam('partialpayment');
		foreach($conditionIds as $conditionId)
		{
			$condition = Mage::getSingleton('partialpayment/installment')->load($conditionId);
			$collection = Mage::getModel('partialpayment/partialpayment')->getCollection()->addFieldToFilter('partial_payment_id', $condition->getPartialPaymentId());
			$collection->getSelect()->joinLeft($sales_flat_order, 'main_table.order_id = ' . $sales_flat_order . '.entity_id', array('customer_firstname', 'customer_email', 'increment_id', 'order_currency_code'));
			$data = $collection->getData();
			$email = Mage::helper('partialpayment/emailsender');
			$email->sendOverDueMail($data[0]['customer_firstname'], $data[0]['customer_email'], $data[0]['increment_id'], $condition->getInstallmentDueDate(), $condition->getInstallmentAmount(), $condition->getPartialPaymentId(), $data[0]['order_currency_code']);
		}
		$this->_getSession()->addSuccess($this->__('Total of %d Installment(s) overdue notice email(s) have been sent successfully', count($conditionIds)));
		$this->_redirect('*/*/index');
	}
	
	public function massPayAction()	{
		$count=0;
		$conditionIds = $this->getRequest()->getParam('partialpayment');
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		foreach($conditionIds as $conditionId)
		{	
			$condition = Mage::getModel('partialpayment/installment')->load($conditionId);
			$ppayment = Mage::getModel('partialpayment/partialpayment')->load($condition->getPartialPaymentId());
			$order = Mage::getModel('sales/order')->load($ppayment->getOrderId());
			$calculation_model = Mage::getModel("partialpayment/calculation");
			
			$flag=0;
			if($order->getStatus()=='canceled')
			{
				$flag++;
			}
			elseif ($condition->getInstallmentStatus()!='Paid')
			{		
				$calculation_model->setInstallmentSuccessData($condition->getId(),$ppayment->getOrderId(),$condition->getPartialPaymentId(),$this->getRequest()->getParam('pay'));
				$count++;
			}		  
		}
		if($count>0)
		{
			$this->_getSession()->addSuccess($this->__('Total of %d Installment(s) were successfully Paid',$count));
		}
		elseif($flag>0)
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__('Installment of Canceled Order cannot be paid'));
		}
		else
		{
			$this->_getSession()->addSuccess($this->__('Selected installment(s) were already paid.'));
		}
		$this->_redirect('*/*/index');
	}
  
	public function massRemindAction() {
		//get detaful table name with prefix, if prefix is there in database
		$sales_flat_order = Mage::getSingleton('core/resource')->getTableName('sales_flat_order');
		
		$conditionIds = $this->getRequest()->getParam('partialpayment');
		foreach($conditionIds as $conditionId)
		{
			$condition = Mage::getSingleton('partialpayment/installment')->load($conditionId);
			$collection = Mage::getModel('partialpayment/partialpayment')->getCollection()->addFieldToFilter( 'partial_payment_id', $condition->getPartialPaymentID());
			$collection->getSelect()->joinLeft($sales_flat_order, 'main_table.order_id = ' . $sales_flat_order . '.entity_id', array('customer_firstname', 'customer_email', 'increment_id', 'order_currency_code'));
			$data = $collection->getData();
			$email = Mage::helper('partialpayment/emailsender');
			$email->sendIntallmentReminderMail('false', $data[0]['customer_firstname'], $data[0]['customer_email'], $data[0]['increment_id'], $condition->getPartialPaymentID(), $condition->getInstallmentDueDate(), $data[0]['order_currency_code']);
		}
		$mailIds = $this->getRequest()->getParam('partialpayment');
		
		$this->_getSession()->addSuccess($this->__('Total of %d Installment(s) reminder email(s) have been sent successfully', count($mailIds)));
		$this->_redirect('*/*/index');
	}

  public function exportCsvAction() {
        $fileName   = 'partialpaymentsummary.csv';
        $content    = $this->getLayout()->createBlock('partialpayment/adminhtml_partialpaymentsummary_grid')
            ->getCsv();
		$this->_sendUploadResponse($fileName, $content);
    }
	
	public function exportXmlAction() {
        $fileName   = 'partialpaymentsumary.xml';
        $content    = $this->getLayout()->createBlock('partialpayment/adminhtml_partialpaymentsummary_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }
	protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream') {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
