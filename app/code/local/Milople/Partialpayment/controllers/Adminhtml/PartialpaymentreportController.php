<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Adminhtml_PartialpaymentreportController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}
	
	// this function added to solve access dined issue of new patch of matento
	protected function _isAllowed()	{
		return true;
	}	
	
	public function editAction() {
		$this->_redirect('*/*/index');
	}
	
	public function massRemindAction() {
	$mailIds = $this->getRequest()->getParam('partialpayment');
	$this->_getSession()->addSuccess($this->__('Email Reminder sent for %d Installment(s) ', count($mailIds)));
	$this->_redirect('*/*/index');
	}
	public function exportCsvAction() {
        $fileName   = 'revenuegeneration.csv';
        $content    = $this->getLayout()->createBlock('partialpayment/adminhtml_partialpaymentreport_grid')
            ->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
    }
	
	public function exportXmlAction() {
        $fileName   = 'revenuegeneration.xml';
        $content    = $this->getLayout()->createBlock('partialpayment/adminhtml_partialpaymentreport_grid')
            ->getXml();
        $this->_sendUploadResponse($fileName, $content);
    }
	protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream') {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}