<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_Adminhtml_PartialpaymentrevenueController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('partialpayment/summary')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Partial Summary'), Mage::helper('adminhtml')->__('Partial Summary'));
		
		return $this;
	}   
 
	public function indexAction() {
		
		$this->_initAction()
			->renderLayout();
	}
	
	// this function added to solve access dined issue of new patch of matento
	protected function _isAllowed()	{
		return true;
	}
	
	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('partialpayment/partialpayment')->load($id);
		$orderId = $model->getOrderId(); 
		$session = Mage::getSingleton('adminhtml/session_quote');
		$session->clear();
		$ordercreatemodel = Mage::getSingleton('adminhtml/sales_order_create');
		$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
		if (!Mage::helper('sales/reorder')->canReorder($order)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
		if ($order->getId()) {
			$order->setReordered(true);
			$session->setUseOldShippingMethod(true);
			$ordercreatemodel->initFromOrder($order);
		}
		else{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__('Partially paid order is not available.'));
		}

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			Mage::register('partialpayment_data', $model);
			$this->loadLayout();
			$this->_setActiveMenu('partialpayment/items');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock('partialpayment/adminhtml_partialpayment_edit'))
				->_addLeft($this->getLayout()->createBlock('partialpayment/adminhtml_partialpayment_edit_tabs'));
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('partialpayment')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
	public function exportCsvAction() {
        $fileName   = 'revenuegeneration.csv';
        $content    = $this->getLayout()->createBlock('partialpayment/adminhtml_partialpaymentsummary_grid')
            ->getCsv();
		$this->_sendUploadResponse($fileName, $content);
    }
	
	public function exportXmlAction() {
        $fileName   = 'revenuegeneration.xml';
        $content    = $this->getLayout()->createBlock('partialpayment/adminhtml_partialpaymentsummary_grid')
            ->getXml();
        $this->_sendUploadResponse($fileName, $content);
    }
	 protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream') {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}