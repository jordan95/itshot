<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Paypal
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Express Checkout Controller
 */

require_once Mage::getModuleDir('controllers', 'Mage_Paypal') . DS . 'ExpressController.php';

class Milople_Partialpayment_ExpressController extends Mage_Paypal_ExpressController
{
	public $token;
	
	#When user cancel payment
    public function installmentCancelAction()
	{
		$partial_payment_id = NULL;
		$incrementId = NULL;
		$installmentIds = explode("~",$this->getRequest()->getParam('installmentId'));
		#set installment payment canceled
		foreach ($installmentIds as $installmentId)
		{ 
			$partial_payment_id = Mage::getModel('partialpayment/installment')->load($installmentId)->getPartialPaymentId();
			$incrementId = Mage::getModel('sales/order')->load(Mage::getModel('partialpayment/partialpayment')->load($partial_payment_id)->getOrderId())->getIncrementId();
			break;
		}
		Mage::getSingleton("core/session")->addError(Mage::helper("partialpayment")->__("Payment has been canceled."));
		$this->_redirect('partialpayment/index/installments',array('order_id'=>$incrementId,'partial_payment_id'=>$partial_payment_id),array('_secure'=>true));	
	}
	
	#When user successfully pay payment
	public function installmentSuccessAction()
	{
		$token = $_REQUEST['token'];
		$payerID = $_REQUEST['PayerID'];
		$installmentIds = explode("~",$this->getRequest()->getParam('installmentId'));
		$partial_payment_id = NULL;
		$incrementId = NULL;
		
		$response = Mage::getModel('paypal/api_nvp')->callGetExpressCheckoutDetailsForInstallment($token,$payerID);//request to fetch user information
		$response = Mage::getModel('paypal/api_nvp')->callDoExpressCheckoutPaymentForInstallment($token,$payerID,$installmentIds);//request to capture amount from paypal
		
		$calculation_model = Mage::getModel("partialpayment/calculation");
		if($response['ACK']=='Success')//if payment successfully captured
		{
			$total_installment = 0;
			foreach ($installmentIds as $installmentId)
			{ 
				$partial_payment_id = Mage::getModel('partialpayment/installment')->load($installmentId)->getPartialPaymentId();
				$order = Mage::getModel('sales/order')->load(Mage::getModel('partialpayment/partialpayment')->load($partial_payment_id)->getOrderId());
				$incrementId = $order->getIncrementId();
				$calculation_model->setInstallmentSuccessData($installmentId,$order->getId(),$partial_payment_id,'paypal_express',$response['TRANSACTIONID']);
				$total_installment++;
			}
			if($total_installment == 1)
			{
			  Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installment of order #%s has been paid successfully.",$total_installment,$incrementId));
			}
			else
			{
			  Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installments of order #%s have been paid successfully.",$total_installment,$incrementId));
			}
			$this->_redirect('partialpayment/index/installments',array('order_id'=>$incrementId,'partial_payment_id'=>$partial_payment_id),array('_secure'=>true));
		}
		else
		{
			foreach ($installmentIds as $installmentId)
			{ 
				$calculation_model->setCanceledOrderData($installmentId);
			}
			$this->_forward('installmentCancel');//if payment not captured successfully
		}
	}
	
	#url to return from paypal
	public function installmentReturnAction()
	{
		if ($this->getRequest()->getParam('retry_authorization') == 'true'
            && is_array($this->_getCheckoutSession()->getPaypalTransactionData())
        ) {
			$this->_forward('installmentSuccess');//call success action
            return;
        }
        try {
			$this->_forward('installmentSuccess');//call success action
            return;
        } catch (Mage_Core_Exception $e) {
            Mage::getSingleton('checkout/session')->addError($e->getMessage());
        }
        catch (Exception $e) {
            Mage::getSingleton('checkout/session')->addError($this->__('Unable to process Express Checkout approval.'));
            Mage::logException($e);
        }
		Mage::getSingleton("core/session")->addError(Mage::helper("partialpayment")->__("Payment has been canceled."));
		$calculation_model->setCanceledOrderData($partial_payment_installment_save->getInstallmentId());
        $this->_forward('installmentCancel');//call cancel action
	}
}
