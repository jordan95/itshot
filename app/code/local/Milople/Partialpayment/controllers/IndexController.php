<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpayment_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction() 
	{
		if(!Mage::getSingleton('customer/session')->isLoggedIn()) 
		{  
			// if not logged in
			header("Status: 301");
			$this->_redirect("customer/account/login/") ;  // redirect to the login page
		}
		else
		{
			$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
			// add partially paid orders grid on when partail payment is enabled
			if($partialpaymentHelper->isEnabled())
			{
				$this->loadLayout();
				$this->getLayout()->getBlock('head')->setTitle($this->__('Partially Paid Orders'));
			}
			else
			{
				$this->_redirect('customer/account/index');
			}
		}
		$this->renderLayout();
	}


	public function reloadoptionsAction() {
		$data = Mage::app()->getRequest()->getPost();
		$price = $data['price'];	
		$allow_partial_payment = NULL;
		$partial_payment_helper = Mage::helper('partialpayment/partialpayment');
		if(isset($data['allow_partial_payment']))
		{
			$allow_partial_payment = $data['allow_partial_payment'];
		}
		$product = Mage::getModel('catalog/product')->load($data['product_id']);
		$optional = $partial_payment_helper->isAllowFullPayment($product);
		$html = '<option value="">' . $this->__('Please Select') . '</option>';
		if($optional)
		{
			if($allow_partial_payment!=NULL)
				$html .= '<option value="0"selected="selected" >' . $this->__('Full Payment of ') . $partial_payment_helper->getCurrencySymbol().number_format((float)$price, 2, '.', '') . '</option>';
			else
				$html .= '<option value="0">' . $this->__('Full Payment of ') .$partial_payment_helper->getCurrencySymbol().number_format((float)$price, 2, '.', '') . '</option>';
		}
		for ($i=2;$i<=$partial_payment_helper->getTotalIinstallments($product);$i++) 
		{
			if($allow_partial_payment==$i)
				$html .= '<option value="' . $i . '" selected="selected">' . $i . $this->__(' Installments of ') . $partial_payment_helper->getCurrencySymbol().number_format((float)($price / $i), 2, '.', '') . '</option>';
			else
				$html .= '<option value="' . $i . '" >' . $i . $this->__(' Installments of ') . $partial_payment_helper->getCurrencySymbol().number_format((float)($price / $i), 2, '.', '') . '</option>';
		}
		echo $html;
	}


	public function displaytableAction() {
	
		$data=Mage::app()->getRequest()->getPost();
		$_product=NUlL;
		if(isset($data['product_id']))
		{
			$_product = Mage::getModel('catalog/product')->load($data['product_id']);
		}
			
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		$price=$data['price'];
		$base_price = $partialpaymentHelper->convertCurrencyAmount($price);
		$partialpaymentAuthentication = Mage::helper('partialpayment/data');
	
		$optional=$partialpaymentHelper->isAllowFullPayment($_product);
		$isFlexyAllow = $partialpaymentHelper->isAllowFlexyPayment($_product);
		$surcharge = 0;
	
		$calculationModel=Mage::getModel("partialpayment/calculation");
		$quote = $calculationModel->getQuote();
	
		$currency=Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
		if(isset($data['product_id']))//if product page
		{
			if(isset($data['installments']))// if flexy with selected or all product
			{
				$noOfInstallments = $data['installments'];
				$dpayment=$calculationModel->getDownPaymentAmount($price, $_product, $noOfInstallments);
				$installmentAmount = (float)$calculationModel->getInstallments($price, $_product, $noOfInstallments);
				$remaining=$calculationModel->getRemainingAmountByProduct($price,$_product,$noOfInstallments);
			}
			else // if no flexy with selected or all product
			{
				$noOfInstallments = $partialpaymentHelper->getTotalIinstallments($_product);
				$dpayment=$partialpaymentHelper->convertToCurrentCurrencyAmount($calculationModel->getDownPaymentAmount($base_price, $_product));
				$installmentAmount = (float)$partialpaymentHelper->convertToCurrentCurrencyAmount($calculationModel->getInstallments($base_price, $_product));
				$remaining=$partialpaymentHelper->convertToCurrentCurrencyAmount($calculationModel->getRemainingAmountByProduct($base_price,$_product));
			}
		}
		else // if cart page
		{
			$price = $calculationModel->getSubtotal($quote);
			$base_price = $partialpaymentHelper->convertCurrencyAmount($price);
			if($isFlexyAllow)// if flexy with whole cart
			{
				$noOfInstallments = Mage::getSingleton('core/session')->getAllowPartialPayment();
				$dpayment=$installmentAmount = (float)($price/$noOfInstallments);
				$installmentAmount = $dpayment;
			}
			else // if no flexy with whole cart
			{
				$noOfInstallments = $partialpaymentHelper->getTotalIinstallments();
				$dpayment=$partialpaymentHelper->convertToCurrentCurrencyAmount($calculationModel->getDownPaymentAmount($base_price));
				$installmentAmount = (float)(($price-$dpayment)/($noOfInstallments-1));
			}
			$remaining=$price-$dpayment;
		}
		
		$html= '<div id="close-table" onclick=hidetable(1);><img src="'.Mage::getDesign()->getSkinUrl('partialpayment/images/close.png').'" alt="close"></div>';
		$html .="<div id='light' class='white_content'>";
		if(($quote && isset($data['cartPage'])) || $_product->getTypeID()!='grouped')
		{
			$html.= "<table class='ppayment'>
				<thead>
					<tr><td>".$this->__('Installment No')." </td><td>".$this->__('Due Date')."</td>";
					if(!$isFlexyAllow )
					{	
						if(isset($data['cartPage']))	
						{
							$html.="<td>".$this->__('Amount')." </td></tr>";}
						else
						{	
							$html.="<td>".$this->__('Amount')."<div id='refresh-only-table' onclick=calltable(2);><img src=".Mage::getDesign()->getSkinUrl('partialpayment/images/refresh.png')." alt='refresh'></div></td></tr>";
						}
					}
					else
					{
						$html.="<td>".$this->__('Amount')." </td></tr>";
					}
				$html.="
				</thead>
				<tbody>
					<tr>
						<td style=text-align:center>1<sup>st</sup></td>
						<td style=text-align:center>";
						$html.= Mage::helper('core')->formatDate();
						$html.="</td><td>".$currency.number_format((float)$dpayment, 2, '.', '')."</td>
					</tr>";
					$i=5;
					for ($x = 2; $x <= $noOfInstallments; $x++) 
					{
						$html.="<tr>
							<td style=text-align:center>";
							$html.= $partialpaymentHelper->addOrdinalNumberSuffix($x)."</td><td style=text-align:center>".$calculationModel->calculateInstallmentDates(Mage::getModel('core/date')->date("m/d/Y"),$x-1)."</td><td >".$currency.number_format($installmentAmount, 2, '.', '')."</td></tr>";
					}
					$html.="<tr class='total'>
						<td colspan=2>".$this->__('Total')."&nbsp;</td>
						<td>".$currency.number_format((float)$price, 2, '.', '')." </td>
					</tr>
					<tr class='totalpp'>
						<td colspan=2>".$this->__('Down Payment')."&nbsp;</td><td style='padding-bottom:2px;'>".$currency.number_format((float)$dpayment, 2, '.', '')." </td>
					</tr>
					<tr class='totalpp'>
						<td colspan=2>".$this->__('Amount to be Paid Later')."&nbsp;</td>
						<td style='padding-bottom:2px;'>".$currency.number_format((float)$remaining, 2, '.', '')."</td>
					</tr>
				</tbody>
			</table>";
			
			if($partialpaymentHelper->isSurcharge())
			{
				if($quote && isset($data['cartPage']))
				{
					$surcharge=$currency.number_format((float)$partialpaymentHelper->convertToCurrentCurrencyAmount($calculationModel->getTotalSurchargeAmount()), 2, '.', '');
				}
				else
				{
					$surcharge=$currency.number_format((float)$partialpaymentHelper->convertToCurrentCurrencyAmount($calculationModel->getSurchargeAmount($base_price,$noOfInstallments)), 2, '.', '');
				}
			}
		}
		else
		{
			if($partialpaymentHelper->getCalculateDownpaymentOn($_product)==1)
			{
				$down_payment=$currency.number_format((float)$partialpaymentHelper->getDownPaymentValue($_product), 2, '.', '');
			}
			else
			{
				$down_payment=$partialpaymentHelper->getDownPaymentValue($_product)."%";
			}
			$remaining_installments = $partialpaymentHelper->getTotalIinstallments($_product)-1;
			if($partialpaymentHelper->getPaymentplan()==1)
			{
				$plan = $this->__('month');
			}
			else if($partialpaymentHelper->getPaymentplan()==2)
			{
				$plan = $this->__('week');
			}
			else
			{
				$plan = $partialpaymentHelper->getNoOfDays().$this->__('days');
			}
			$html .= "<p class='note-data'>".$this->__('Down payment will be')." ".$down_payment." ".$this->__('of product price').".</p>";
			$html .= "<p class='note-data'>".$this->__('Remaining amount will be distributed equally in')." ". $remaining_installments." ".$this->__('installments').", ".$this->__('to be paid every')." $plan.</p>";
			
			if($partialpaymentHelper->isSurcharge())
			{
				if($partialpaymentHelper->getCalculateSurchargeOn()==1)
					$surcharge=$currency.$partialpaymentHelper->getSurchargeValue();
				else
					$surcharge=$partialpaymentHelper->getSurchargeValue()."%";
			}
		}
		if($partialpaymentHelper->autocapture())
		{
			$html.="<p class='note-data'>".$this->__("We don't save your Credit Card details").", ".$this->__("you are secure").".</p>";
			$html.="<p class='note-data'>".$this->__("Your Credit Card will be charged automatically").".</p>";
		}
		if($surcharge)
		{
			$html.="<p class='note-data'>$surcharge ".$this->__('surcharge will be applied on your purchase').".</p>";
		}
		$html.="<p class='note-data'>".$this->__('Final amount may vary based on Selected Custom Options').", ".$this->__('Shipping Method').", ".$this->__('Tax').", ".$this->__('Surcharge').", ".$this->__('and')." ".$this->__('Discount').".</p></div>	";
		echo $html;
	}
	public function installmentsAction () {
		if(!Mage::getSingleton('customer/session')->isLoggedIn()) {  // if not logged in
			header("Status: 301");
			$this->_redirect("customer/account/login/") ;  // send to the login page
		}
		else
		{
			$cartHelper = Mage::helper('checkout/cart');
			if (!sizeof($cartHelper->getCart()->getItems())) 
			{
				$collectionSimple = Mage::getResourceModel('catalog/product_collection')->addAttributeToFilter('type_id', array('eq' => 'simple'));
				foreach ($collectionSimple as $collect)
				{
					$stock = (int)Mage::getModel('cataloginventory/stock_item')->loadByProduct($collect)->getQty();
					$is_in_Stock = $collect->getStockItem()->getData()['is_in_stock'];
					
					if($stock > 1 && $is_in_Stock == 1)
					{	
						$cart = Mage::getSingleton('checkout/cart'); 
						$product = new Mage_Catalog_Model_Product();
						$product->load($collect->getEntityId());				
						$product->setCreatedAt(strtotime('now'));  
						break;  
					}		
					else
					{
						continue ;
					}
				}
						
				$quote = Mage::getSingleton('checkout/session')->getQuote();
				$customerSession = Mage::getSingleton('customer/session')->getCustomer();
				$arr=array( 'country_id' => 'US',
						'region_id' => 2,
						'region' =>0, 
						'city' => 'xyz',
						'postcode' => 90404,
						'cart' => 1
						);
				
				$addToCartInfo = (array) $product->getAddToCartInfo();
				$addressInfo = (array) $arr;
  
				$shippingAddress = $quote->getShippingAddress();
				$shippingAddress->setCountryId('US');
				$shippingAddress->setRegionId(2);
				$shippingAddress->setPostcode(90404);
				$shippingAddress->setRegion(0);
				$shippingAddress->setCity("test");
				$shippingAddress->setCollectShippingRates(true);
	
				$request = new Varien_Object($addToCartInfo);
				$result =  $quote->addProduct($product, $request);
			
				$quote->collectTotals();
				$_cartQty = Mage::getModel('checkout/cart')->getQuote()->getItemsQty();
				if ($_cartQty >0)
				{
					Mage::getModel('checkout/cart')->getQuote()->setItemsQty(0);
				}							
			}
		}
			
		$this->loadLayout();
		$orderId = Mage::app()->getRequest()->getParam('order_id');
		$this->getLayout()->getBlock('head')->setTitle($this->__('Installments of Order # %s',$orderId));
		$this->renderLayout();
	}
	
	public function overviewAction() {	
		$post = $this->getRequest()->getPost();
		$currentUrl = $this->getRequest()->getPost('refer', Mage::getBaseUrl());
                if (!$this->_validateFormKey()) {
                    Mage::getSingleton('core/session')->addError(Mage::helper('checkout')->__('Invalid Form Key'));
                    return $this->_redirectUrl($currentUrl);
                }
		$modules = Mage::getConfig()->getNode('modules')->children();
		$modulesArray = (array)$modules;
		$message = $this->validateSecondInstallmentForm($post);
		if ($message != '') {
			Mage::getSingleton('core/session')->addError($message);
			Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
			Mage::app()->getFrontController()->getResponse()->sendResponse(); exit;
		}
		$partial_payment = Mage::getModel('partialpayment/partialpayment')->load($post['partial_payment_id']);
                if(!$partial_payment->getId()){
                    return;
                }
                Mage::register('current_partialpayment', $partial_payment);
		Mage::getSingleton('core/session')->setPartialPaymentId($post['partial_payment_id']);
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');

		if(isset($post['payment']['cc_type'])) {
			$ccType = $post['payment']['cc_type'];
		}
		
		$partial_payment_amount = 0;
		$incrementId = $partial_payment['order_id'];
		try
		{	
			if(isset($modulesArray['Milople_Partialpaymentpaytm']))
			{
				if(strpos($post['payment']['method'], 'paytm_cc') !== false)
				{
					foreach ($post['installment_id'] as $instllmentid) {
						$installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
						$partial_payment_amount += $installmentData->getInstallmentAmount();
						$installmentids[] = $instllmentid;
					}
					$order = Mage::getModel('sales/order')->load($partial_payment['order_id']);
					$installmentId = implode( "-", $installmentids );
				 
					Mage::getSingleton('core/session')->setInstallmentId($installmentId);
					Mage::getSingleton('core/session')->setInstallmentOrderId($order->getIncrementId());
					
					$post['refer'] = base64_encode($post['refer']);
					Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('paytm/processing/installmentPaymentForm',array('incrementId'=>$incrementId,'amount'=>$partial_payment_amount,'postData'=>serialize($post))));
					Mage::app()->getFrontController()->getResponse()->sendResponse(); 
					exit;
				}
			}
			if(isset($modulesArray['Milople_Partialpaymentsagepay']))
			{
				if(strpos($post['payment']['method'], 'sagepaydirectpro') !== false)
				{
					$directHelper = Mage::helper('partialpaymentsagepay/partialpaymentsagepaydirect');
					$response = $directHelper->payWithSagepayDirectPro($post, $partial_payment->getOrderId());

					try 
					{
						$error_message = '';
						if(empty($response) || !isset($response['Status'])) 
						{
							$error_message = $this->__('Sage Pay is not available at this time. Please try again later.');
						}

						switch($response['Status'])
						{
							case 'FAIL':
								$error_message = $this->__($response['StatusDetail']);
								break;
							case 'FAIL_NOMAIL':
								$error_message = $this->__($response['StatusDetail']);
								break;
							case 'INVALID':
								$error_message = $this->__('INVALID. %s', $response['StatusDetail']);
								break;
							case 'MALFORMED':
								$error_message = $this->__('MALFORMED. %s', $response['StatusDetail']);
								break;
							case 'ERROR':
								$error_message = $this->__('ERROR. %s', $response['StatusDetail']);
								break;
							case 'REJECTED':
								$error_message = $this->__('REJECTED. %s', $response['StatusDetail']);
								break;
							case '3DAUTH':
								$error_message = $this->__($response['StatusDetail']);
								break;
							default:
								break;
						}
						if (!empty($error_message))
						{
							Mage::getSingleton('core/session')->addError($error_message);
							Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
							Mage::app()->getFrontController()->getResponse()->sendResponse(); exit;	
						}
					} 
					catch (Exception $e) 
						{
							Mage::throwException($this->__('%s', $e->getMessage())
						);
					}
				}			
				if(strpos($post['payment']['method'], 'sagepayserver') !== false)
				{
					$partialpaymentsagepayHelper = Mage::helper('partialpaymentsagepay/partialpaymentsagepay');
					$response = $partialpaymentsagepayHelper->payWithSagepayServer($post, $incrementId);
					try {
						$error_message = '';
						if(empty($response) || !isset($response['Status'])) {
							$error_message = $this->__('Sage Pay is not available at this time. Please try again later.');
						}

						switch($response['Status']) {
							case 'FAIL':
								$error_message = $this->__($response['StatusDetail']);
								break;
							case 'FAIL_NOMAIL':
								$error_message = $this->__($response['StatusDetail']);
								break;
							case 'INVALID':
								$error_message = $this->__('INVALID. %s', $response['StatusDetail']);
								break;
							case 'MALFORMED':
								$error_message = $this->__('MALFORMED. %s', $response['StatusDetail']);
								break;
							case 'ERROR':
								$error_message = $this->__('ERROR. %s', $response['StatusDetail']);
								break;
							case 'REJECTED':
								$error_message = $this->__('REJECTED. %s', $response['StatusDetail']);
								break;
							case '3DAUTH':
								$error_message = $this->__($response['StatusDetail']);
								break;
							default:
								break;
						}
						if (!empty($error_message)) {
							
							Mage::getSingleton('core/session')->addError($error_message);
							Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
							Mage::app()->getFrontController()->getResponse()->sendResponse(); exit;	
						}
						$html = '<html><body>';
						$html .= $partialpaymentsagepayHelper->getSagepayServerForm($response);
						$html .= '<script type="text/javascript">document.getElementById("sagepay_server_checkout").submit();</script>';
						$html .= '</body></html>';
						echo $html;
						exit;
					} catch (Exception $e) {
						Mage::throwException(
						   $this->__('%s', $e->getMessage())
						);
					}
				}	
			}
			
			// for netgains stripe gateway
			if(strpos($post['payment']['method'], 'stripe') !== false)
			{				
				foreach ($post['installment_id'] as $instllmentid) 
				{
					$installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
					$partial_payment_amount += $installmentData->getInstallmentAmount();
					$installmentids[] = $instllmentid; 
				}
				$installmentids = implode("-",$installmentids);
				$payment = $post['payment'];				
				try{
					$response = Mage::getModel('partialpaymentstripe/stripe')->callInstallmentApi($installmentids,$post['partial_payment_id'],$payment,$partial_payment_amount,'authorize');
					if($response['transaction_id'] && $response['status'] == 1)
					{
						$total_installment=0;
						$incrementId = 0;
						foreach($post['installment_id'] as $instllment_id) 
						{
							$calculationModel=Mage::getModel("partialpayment/calculation");
							$partialpaymentData = Mage::getModel('partialpayment/partialpayment')->load($post['partial_payment_id']);
		                    $orderId = $partialpaymentData->getOrderId();
							$salesOrderCollection = Mage::getModel('sales/order')->load($orderId);
							$incrementId = $salesOrderCollection->getIncrementId();							
							$calculationModel->setInstallmentSuccessData($instllment_id,$orderId,$post['partial_payment_id'],$post['payment']['method']);
							$total_installment++;
						}
					if($total_installment == 1)
				    {
					   Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installment of order #%s has been paid successfully.",$total_installment,$incrementId));
				    }
				    else
				    {
					   Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installments of order #%s have been paid successfully.",$total_installment,$incrementId));
				    }
								
					Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
					Mage::app()->getFrontController()->getResponse()->sendResponse(); 
					}
				}
				 catch (Exception $e) {
						Mage::throwException(
						   $this->__('%s', $e->getMessage())
						);
					}
				exit;
			}
			
			if(isset($modulesArray['Milople_PartialpaymentPaymentsense']))
			{				
				if(strpos($post['payment']['method'], 'Paymentsense') !== false)
				{
					$amount = 0;
					foreach ($post['installment_id'] as $instllmentid) 
					{
                     $installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
					 $amount += $installmentData->getInstallmentAmount();
					 $installmentids[] = $instllmentid; 
				    }
					
					$request = Mage::getModel('partialpaymentPaymentsense/paymentsense');
					$form = new Varien_Data_Form();
					
					$model = Mage::getModel('Paymentsense/direct');
    				$szActionURL = $model->getConfigData('hostedpaymentactionurl');
					$form->setAction($szActionURL)
						->setId('HostedPaymentForm')
						->setName('HostedPaymentForm')
						->setMethod('POST')
						->setUseContainer(true);
						
					foreach ($request->getInstalmentFormFields($incrementId,$amount,implode("-",$installmentids),$post['partial_payment_id']) as $field=>$value)
					{
						$form->addField($field, 'hidden', array('name'=>$field, 'value'=>$value));
					}
					
					$html = '<html><body>';
					$html.= $this->__('You will be redirected to a secure payment page in a few seconds.');
					$html.= $form->toHtml();
					$html.= '<script type="text/javascript">document.getElementById("HostedPaymentForm").submit();</script>';
					$html.= '</body></html>';
					echo $html;
					exit;
				}
			}
			
			// for paypal plus gateway 
			if(strpos($post['payment']['method'], 'iways_paypalplus_payment') !== false)
			{
			   foreach ($post['installment_id'] as $instllmentid) 
				{
					$installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
					$partial_payment_amount += $installmentData->getInstallmentAmount();
					$installmentids[] = $instllmentid; 
				}
                $installmentids = implode("-",$installmentids);
				$website = Mage::app()->getStore()->getWebsiteId();
				$token = Mage::getModel('partialpaymentpaypalplus/api')->setApiContextInstallment($website);
				$order = Mage::getModel('sales/order')->load($partial_payment['order_id']);
				$quoteId = $order->getQuoteId();
                $quote = Mage::getModel('sales/quote')->load($quoteId);	
				$currencyCode = $order->getOrderCurrencyCode();	
				$installmentAmount = sprintf("%0.2f", $partial_payment_amount);
			    		
				if($token)
				{
			  	  $redirectURL = Mage::getModel('partialpaymentpaypalplus/api')->getInstallmentPaymentExperience($quote,$installmentAmount,$currencyCode,$installmentids,$post['partial_payment_id'],$partial_payment['order_id']);
				
                  if ($redirectURL) { ?>
					   <script src="https://www.paypalobjects.com/webstatic/ppplus/ppplus.min.js" type="text/javascript">
						
						var payment = {};					  
							 window.ppp = PAYPAL.apps.PPP(
                             {							 
							   approvalUrl: "<?php echo $redirectURL; ?>",
							   placeholder: "ppplus",
							   mode: "<?php echo Mage::getStoreConfig('iways_paypalplus/api/mode'); ?>",
							   useraction: "commit",
							   buttonLocation: "outside",
							   country:"DE",
							   language:"<?php echo Mage::getStoreConfig('general/locale/code'); ?>",
							   onContinue: function () {
								   payment.save()
							   },
							 });
						window.startPPP();	 
                         </script>	<?php 
                          try {
							$responsePayPal = Mage::getModel('partialpaymentpaypalplus/api')->patchInstallmentPayment($quote,$currencyCode,$installmentAmount);
							if ($responsePayPal) {
								$result['success'] = true;
							} else {
								$result['success'] = false;
								$result['error'] = false;
								$result['error_messages'] = $this->__('Please select an other payment method.');

							}
						} catch (\Exception $e) {
							$result['success'] = false;
							$result['error'] = false;
							$result['error_messages'] = $e->getMessage();
						}
						$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                        if($result)			
						{
				           ?>
						   <button type="submit" id="continueButton" onclick="window.ppp.doCheckout();"> </button>
						  <script type="text/javascript">							
								$("continueButton").click();						
                           </script>
                            <?php						   
				          $this->getResponse()->setRedirect($redirectURL);			  
						}
				  }			  
				  return;
				}				
                exit;				
			}			
			
			if(isset($modulesArray['Milople_Partialpaymentccavenue']))
			{
				if(strpos($post['payment']['method'], 'ccavenuepay') !== false)
				{
					foreach ($post['installment_id'] as $instllmentid) {
						$installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
						$partial_payment_amount += $installmentData->getInstallmentAmount();
					}
					$html = '<html><body>';
					$html .= Mage::helper('partialpaymentccavenue/partialpaymentccavenue')->ccavenueForm($incrementId,$partial_payment_amount,$post);
					$html .= '<script type="text/javascript">document.getElementById("ccavenuepay_checkout").submit();</script>';
					$html .= '</body></html>';
					echo $html; 
					exit;
				}
			}
			if(isset($modulesArray['Milople_Partialpaymentpayu']))
			{				
				if(strpos($post['payment']['method'], 'payucheckout_shared') !== false)
				{
					$amount = 0;
					foreach ($post['installment_id'] as $instllmentid) 
					{
                     $installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
					 $amount += $installmentData->getInstallmentAmount();
					 $installmentids[] = $instllmentid; 
				    }
					
					$shared = Mage::getModel('partialpaymentpayu/partialpaymentpayu');
					$form = new Varien_Data_Form();
					$form->setAction($shared->getPayuCheckoutSharedUrl())
						->setId('payucheckout_shared_checkout')
						->setName('payucheckout_shared_checkout')
						->setMethod('POST')
						->setUseContainer(true);
						
					foreach ($shared->getInstalmentFormFields($incrementId,$amount,implode("-",$installmentids),$post['partial_payment_id']) as $field=>$value)
					{
						$form->addField($field, 'hidden', array('name'=>$field, 'value'=>$value));
					}

					$html = '<html><body>';
					$html.= $this->__('You will be redirected to PayuCheckout in a few seconds.');
					$html.= $form->toHtml();
					$html.= '<script type="text/javascript">document.getElementById("payucheckout_shared_checkout").submit();</script>';
					$html.= '</body></html>';
					echo $html; 
					exit;
				}
			}
			$partial_payment_amount =0;
			if($post['payment']['method'] == 'paypal_standard')
			{
				foreach ($post['installment_id'] as $instllmentid) {
					$installmentData = Mage::getModel('partialpayment/installment')->load($instllmentid);
					$partial_payment_amount += $installmentData->getInstallmentAmount();
					$installmentids[] = $instllmentid; 
				}
					$partialid = $post['partial_payment_id'];
					$html = '<html><body>';
					$html.= $this->__('You will be redirected to PayPal in a few seconds.');
					$html.= $this->getPaypalForm($incrementId, $partial_payment_amount, Mage::getUrl("partialpayment/standard/success",array('installment_id'=> implode("-",$installmentids),'partial_payment_id'=>$partialid)), Mage::getUrl("partialpayment/standard/cancel",array('installment_id'=> implode("-",$installmentids),'partial_payment_id'=>$partialid)), implode("-",$installmentids));
					$html.= '<script type="text/javascript">document.getElementById("paypal_standard_checkout").submit();</script>';
					$html.= '</body></html>';
					sleep(4);
					echo $html;

			}
			
			#compatible with paypal express
			if($post['payment']['method'] == 'paypal_express')
			{
				$response = Mage::getModel('paypal/api_nvp')->callDoExpressInstallmentPayment($post['installment_id'],$incrementId);
				
				if($response && isset($response['TOKEN']))
				{
					$expressConfig = Mage::getModel('paypal/config');
					$expressConfig->sandboxFlag = Mage::getStoreConfig("paypal/wpp/sandbox_flag", Mage::app()->getStore()->getStoreId());
					$url = $expressConfig->getExpressCheckoutStartUrl($response['TOKEN']);
					$this->getResponse()->setRedirect($url);
					return;
				}
				else
				{
					Mage::getSingleton("core/session")->addError(Mage::helper("partialpayment")->__("Payment Failed"));
					Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
					Mage::app()->getFrontController()->getResponse()->sendResponse();
				}
				exit;
			}

			if (Mage::getStoreConfig('payment/' . $post['payment']['method'] . '/payment_action') != '') {
				$this->_capturePayment($partial_payment, $post['payment']['method']);
			}

			if($post['payment']['method'] != 'paypal_standard')
			{
								
  			    $total_installment=0;
				foreach($post['installment_id'] as $instllment_id) 
				{
					$calculationModel=Mage::getModel("partialpayment/calculation");
					$calculationModel->setInstallmentSuccessData($instllment_id,$incrementId,$post['partial_payment_id'],$post['payment']['method']);
					$total_installment++;
			    }
				if($post['payment']['method'] != 'paypal_standard')
				{	
					$salesOrderCollection = Mage::getModel('sales/order')->load($incrementId,'entity_id');
					$partial_payment = Mage::getModel('partialpayment/partialpayment')->load($post['partial_payment_id']);
					
					if($total_installment == 1)
				     {
					   Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installment of order #%s has been paid successfully.",$total_installment,$salesOrderCollection->getIncrementId()));
				     }
				     else
				     {
					   Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installments of order #%s have been paid successfully.",$total_installment,$salesOrderCollection->getIncrementId()));
				     }
								
					Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
					Mage::app()->getFrontController()->getResponse()->sendResponse(); 
					
				}
						
			}
				
		}
		catch (Mage_Core_Exception $e)
		{	Mage::getSingleton('core/session')->addError($e->getMessage());
			Mage::app()->getFrontController()->getResponse()->setRedirect($currentUrl);
			Mage::app()->getFrontController()->getResponse()->sendResponse(); 
		}
	
	}
	public function validateSecondInstallmentForm ($post) {
		$message = '';

		if (!isset($post['installment_id']))
			$message = 'Please select an installment.';

		if (!isset($post['payment']['method']))
			$message = 'Please select a payment method.';

		if (!isset($post['installment_id']) && !isset($post['payment']['method']))
			$message = 'Please select an installment and payment method.';

		return $message;
	}

	
	public function getPaypalForm($incrementId, $partial_payment_amount, $successUrl, $cancelUrl, $instllmentid) {
		$partialpaymentHelper = Mage::helper('partialpayment/partialpayment');
		$orderid = $incrementId ;
		$setOrderCurrency =  Mage::getModel('sales/order')->getCollection()
							->addFieldToFilter('entity_id',$orderid)
							->load(); 
													
		foreach ($setOrderCurrency as $setCurrency)
		{ 
			$getCurrencyCode = $setCurrency->getOrderCurrencyCode();
		}
		$partial_payment_amount = number_format($partial_payment_amount, 2);
		$order = Mage::getModel('sales/order')->load($incrementId);
		$business_account = Mage::getStoreConfig('paypal/general/business_account');
		$standard = Mage::getModel('paypal/standard');
		$region = $order->getBillingAddress()->getData('region');
		$states = Mage::getModel('directory/country')->load('US')->getRegions(); // get list of states 
   		foreach($states as $state)
		{
			if($state['default_name'] == $region)
			{
				$region = $state['code']; // get state code
				break;
			}
    	}
		$html =	'
		<form id="paypal_standard_checkout" action="' . $standard->getConfig()->getPaypalUrl() . '" method="POST">
			<input type="hidden" id="business" name="business" value="' . $business_account . '" />
			<input type="hidden" id="invoice" name="invoice" value="' . $order->getIncrementId().'-'. $instllmentid .'-'. date('Ymd-His') . '" />
			<input type="hidden" id="currency_code" name="currency_code" value="' . $order->getBaseCurrencyCode() . '" />
			<input type="hidden" name="address_override" value="1">
			<input type="hidden" name="first_name" value="'.$order->getBillingAddress()->getFirstname().'">
			<input type="hidden" name="last_name" value="'.$order->getBillingAddress()->getLastname().'">
			<input type="hidden" name="address1" value="'.$order->getBillingAddress()->getData('street').'">
			<input type="hidden" name="city" value="'.$order->getBillingAddress()->getCity().'">
			<input type="hidden" name="state" value="'.$region.'">
			<input type="hidden" name="zip" value="'.$order->getBillingAddress()->getData('postcode').'">
			<input type="hidden" name="country" value="'.$order->getBillingAddress()->getData('country_id').'">
			<input type="hidden" id="paymentaction" name="paymentaction" value="sale" />
			<input type="hidden" id="cmd" name="cmd" value="_xclick" />
			<input type="hidden" id="upload" name="upload" value="1" />
			<input type="hidden" id="return" name="return" value="' . $successUrl . '" />
			<input type="hidden" id="cancel_return" name="cancel_return" value="' .$cancelUrl .  '" />
			<input type="hidden" id="notify_url" name="notify_url" value="' . Mage::getUrl("paypal/ipn/") . '"/>
			<input type="hidden" id="bn" name="bn" value="Varien_Cart_WPS_US" />
			<input type="hidden" id="charset" name="charset" value="utf-8" />
			<input type="hidden" name="amount" value="' . $partial_payment_amount . '" />
			<input type="hidden" name="no_shipping" value="0" />
			<input type="hidden" name="item_name" value="Remaining Amount" />
			<input type="hidden" name="item_number" value="' . $order->getIncrementId() . '" />
		</form>';
		
		return $html;
	}


	protected function _capturePayment($model,$pmethod)	{	
		$post = $this->getRequest()->getPost();
		$model = Mage::getModel('partialpayment/partialpayment')->load($post['partial_payment_id']);
		$isnew = $model->getTotalInstallment() < $model->getPaidInstallment() ? true : false ;
		if($isnew)
		{
			return false;
		}
		$orderid = $model->getOrderId();
		$order = Mage::getModel("sales/order")->load($orderid);
		$invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
		$invoice->setState(Mage_Sales_Model_Order_Invoice::STATE_OPEN);
		$paymentInfo = $this->getRequest()->getPost('payment', array());
		$amount = 0;
		try
		{
			if(is_array($paymentInfo) && count($paymentInfo) && isset($paymentInfo['method']))
			{	
				foreach($post['installment_id'] as $installment_id)//pay each installment
				{
					$amount += $post[$installment_id];
				}
				if($amount > 0)
				{
					$invoice->setBaseGrandTotal($amount);
					$invoice->setGrandTotal($amount);
					$invoice->getOrder()->setTotalDue($amount);
					$capturePayment = Mage::getModel('partialpayment/sales_order_capture_payment');
					$capturePayment->setOrder($invoice->getOrder());
					$capturePayment->importData($paymentInfo);	
					$capturePayment->setAmountOrdered($amount);
					$capturePayment->setBaseAmountOrdered($amount);
					$capturePayment->setShippingAmount(0);
					$capturePayment->setBaseShippingAmount(0);
					$capturePayment->setAmountAuthorized($amount);
					$capturePayment->setBaseAmountAuthorized($amount);

					$clonedInvoice = clone $invoice;

					$invoice->getOrder()->addRelatedObject($capturePayment);
					if($paymentInfo['method'] == 'authorizenet')
					{
						if(isset($post['save_for_future']) && $post['save_for_future']==1)
						{
							$paymentModel = Mage::getModel('partialpayment/payment_method_authorizenet');
							$service = $paymentModel->getWebService();
							$subscription = new Varien_Object();
							$subscription->setStoreId($order->getStoreId());
							$service->setSubscription($subscription);
							
							$data = json_decode(json_encode($service->createCIMAccountOnInstallmentPayment($order,$paymentInfo)),true);
							$profileId = $data['CreateCustomerPaymentProfileResult']['customerProfileId'];
							$paymentProfiles = array();
														
							if(isset($data['profile']) && sizeof($data['profile']['paymentProfiles']['CustomerPaymentProfileMaskedType']) > 1)
							{
								$total_paymentProfiles = $data['profile']['paymentProfiles']['CustomerPaymentProfileMaskedType'];
								for($i=1;$i<= sizeof($total_paymentProfiles);$i++)
								{  
									if($i == sizeof($total_paymentProfiles) )
									{
										$no = $i - 1 ;
										$paymentProfiles = $data['profile']['paymentProfiles']['CustomerPaymentProfileMaskedType'][$no]['customerPaymentProfileId'];
									}
									else
									{
										continue ;  
									}
								}
								$realId = $paymentProfiles;
							}
							else
							{
								$realId = $data['CreateCustomerPaymentProfileResult']['customerPaymentProfileId'];
							}
							if($realId !='' && $profileId !='')
								Mage::getModel('partialpayment/partialpayment')->load($post['partial_payment_id'])->setAutoCaptureProfileId($profileId)->setAutoCapturePaymentProfileId($realId)->save();
						}
						$capturePayment->capture($clonedInvoice);
						$capturePayment->pay($clonedInvoice);
					}else{
						/* Compatible pay flow pro payment method */
						if (strtolower(Mage::getStoreConfig('payment/' . $paymentInfo['method'] . '/payment_action')) == 'authorize' || strtolower(Mage::getStoreConfig('payment/' . $paymentInfo['method'] . '/payment_action')) == 'authorization') {
							$capturePayment->_authorize(true, $amount);
						}
						else {
							if($capturePayment->canCapture())
							{
								if($pmethod != 'sagepaydirectpro')
								{
									$capturePayment->capture($clonedInvoice);
									$capturePayment->pay($clonedInvoice);
								}
							}
							else
							{
								$capturePayment->pay($clonedInvoice);
							}
						}
					}
					return true;
				}
			}
		}catch(Exception $e){
			 Mage::throwException($this->__("Gateway Error: Cannot pay installment now. Please try again later."));
			  $this->_redirect('*/*/');
		}
	}


	public function autocronAction() {
		/*Mage::getModel('partialpayment/partialpaymentCron')->sentInstallmentReminderMails();*/
		Mage::getModel('partialpayment/partialpaymentCron')->autoCapturePayment();
	}
}
