<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

require_once 'Mage/Checkout/controllers/OnepageController.php';

class Milople_Partialpayment_OnepageController extends Mage_Checkout_OnepageController
{
	public function successAction() {
        $session = $this->getOnepage()->getCheckout();
        if (!$session->getLastSuccessQuoteId()) {
            $this->_redirect('checkout/cart');
            return;
        }
		
        $lastQuoteId = $session->getLastQuoteId();
        $lastOrderId = $session->getLastOrderId();
        $lastRecurringProfiles = $session->getLastRecurringProfileIds();
        if (!$lastQuoteId || (!$lastOrderId && empty($lastRecurringProfiles))) {
            $this->_redirect('checkout/cart');
            return;
        }
		
		$orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
		$partial_payment = Mage::getModel('partialpayment/partialpayment')->getCollection();
		$partial_payment->addFieldToFilter('order_id', $lastOrderId );
		$size = $partial_payment->getSize();
		$id=0;
		foreach ($partial_payment as $item)
		{
			$id = $item->getPartialPaymentId();
		}
		 
		
		Mage::getSingleton('core/session')->unsWirecardCheckoutFailed();
		
		$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
		$payment = Mage::getModel('sales/order_payment_transaction')->getCollection()
							->addFieldToFilter('order_id', $order->getEntityId());
		$transaction_id = "";
		foreach ($payment as $item)
		{
			$transaction_id = $item->getTxnId();
			$installment = Mage::getModel('partialpayment/installment')->getCollection()	
							->addFieldToFilter('installment_status', 'Paid')
							->addFieldToFilter('partial_payment_id',$id);
			foreach ($installment as $item)
			{
				$installmentload = Mage::getModel('partialpayment/installment')->load($item->getInstallmentId());
				$installmentload->setTransactionId($transaction_id);
				$installmentload->save();
			}
			//if payment method is paypal pro(paypal_direct) then store txn id means transacation id for future payment(autocatpure)
			if($item->getPaymentMethod()=='paypal_direct')
			{
				$partial_payment = Mage::getModel('partialpayment/partialpayment')->load($id)->setAutoCaptureProfileId($transaction_id)->save();
			}
		}
	    $session->clear();
        $this->loadLayout();
        $this->_initLayoutMessages('checkout/session');
        Mage::dispatchEvent('checkout_onepage_controller_success_action', array('order_ids' => array($lastOrderId)));
        $this->renderLayout();
    }
}