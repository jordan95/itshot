<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/contact-us.html
* 
* @category		Ecommerce 
* @package		Milople_Partialpayment
* @extension	Partial Payment and Layaway Auto 
* @copyright	Copyright (c) 2016 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

require_once Mage::getModuleDir('controllers', 'Mage_Paypal') . DS . 'StandardController.php';

class Milople_Partialpayment_StandardController extends Mage_Paypal_StandardController
{   
    public function cancelAction() {
		$calculation_model = Mage::getModel("partialpayment/calculation");
		$installment_id = Mage::app()->getRequest()->getParam('installment_id');
		$partial_payment_id =  Mage::app()->getRequest()->getParam('partial_payment_id');
		if ($installment_id == '')
	    {	
			$session = Mage::getSingleton('checkout/session');
			$session->setQuoteId($session->getPaypalStandardQuoteId(true));
			if ($session->getLastOrderId()) {
				$order = Mage::getModel('sales/order')->load($session->getLastOrderId());
				if ($order->getId()) {
					$order->cancel()->save();
				}
				
				$partial_payment = Mage::getModel('partialpayment/partialpayment')->getCollection();
				$partial_payment->addFieldToFilter('order_id',$session->getLastOrderId());
				$size = $partial_payment->getSize();
							
				foreach ($partial_payment as $item)
				{
					$remainingAmount = $item->getTotalAmount();
					$remaningInstallment = $item->getTotalInstallment();
					 									
					$partial_payment_installments_save = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id', $item->getPartialPaymentId());
						
					foreach($partial_payment_installments_save as $partial_payment_installment_save)
					{
						$calculation_model->setCanceledOrderData($partial_payment_installment_save->getInstallmentId());
					}
				}
			}
			$this->_redirect('checkout/cart');
		}
		else
		{
			$installments = explode("-",$installment_id);
			foreach($installments as $installment)
			{
				$calculation_model->setInstallmentCancelData($installment);
				$partial_payment = Mage::getModel('partialpayment/partialpayment')->getCollection()
									->addFieldToFilter('partial_payment_id',$partial_payment_id) ; 
				$arrInstallment=array();
				if(sizeof($partial_payment))
				{
					foreach ($partial_payment as $order) 
					{
						$orderid = $order->getOrderId();
					}
				}
			}
			$incrementId = Mage::getModel('sales/order')->load($orderid)->getIncrementId();
			$this->_redirect('partialpayment/index/installments',array('order_id'=>$incrementId,'partial_payment_id'=>$partial_payment_id),array('_secure'=>true));
		}
    }

	public function successAction() {
		$installment_id = Mage::app()->getRequest()->getParam('installment_id');
		$partial_payment_id =  Mage::app()->getRequest()->getParam('partial_payment_id');
		$calculation_model = Mage::getModel("partialpayment/calculation");
		
		if (!isset($installment_id))
		{
			$session = Mage::getSingleton('checkout/session');
			$session->setQuoteId($session->getPaypalStandardQuoteId(true));	
			$salesOrderCollection = Mage::getModel('sales/order')->load($session->getLastRealOrderId(), 'increment_id');
			$calculation_model->setOrderSuccessData($salesOrderCollection->getId());
			Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
			$this->_redirect('checkout/onepage/success', array('_secure'=>true));
		}
		else    
		{	
			$installment_ids = explode("-",$installment_id);
			$instalmentamt = 0;
			foreach ($installment_ids as $installment_id)
			{
				$installmentData = Mage::getModel('partialpayment/installment')->load($installment_id);
				$instalmentamt += $installmentData->getInstallmentAmount();
			}
		
			if(isset($_REQUEST['invoice']) && $_REQUEST['invoice'] != '')
			{
				$total_installment=0;	
				foreach ($installment_ids as $installment_id)
				{ 
					$order_id_str = $_REQUEST['invoice'];
					$order_id_pos = strpos($order_id_str, '-');
					$order_id = substr($order_id_str, 0, $order_id_pos);
					$order_real_id = Mage::getModel('sales/order')->loadByIncrementId($order_id)->getId(); 
					$partial_payment = Mage::getModel('partialpayment/partialpayment')->getCollection();
					$partial_payment->addFieldToFilter('partial_payment_id', $partial_payment_id);
					$calculation_model->setInstallmentSuccessData($installment_id,$order_real_id,$partial_payment_id,'paypal_standard');
					$total_installment++;
				}
			 
				Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
				$this->_redirect('partialpayment/index/installments',array('order_id'=>$order_id,'partial_payment_id'=>$partial_payment_id),array('_secure'=>true));
				$collection = Mage::getModel('partialpayment/partialpayment')->load($partial_payment_id);
				
				if($total_installment == 1)
				{
				  Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installment of order #%s has been paid successfully.",$total_installment,$order_id));
				}
				else
				{
				  Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installments of order #%s have been paid successfully.",$total_installment,$order_id));
				}
			}
		}
	}
}