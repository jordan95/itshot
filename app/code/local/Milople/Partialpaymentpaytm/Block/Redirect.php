<?php


class Milople_Partialpaymentpaytm_Block_Redirect extends One97_paytm_Block_Redirect
{
    /**
     * Getting gateway ur
     */
    public function getNewFormAction($order1)
    {
		return $order1->getPayment()->getMethodInstance()->getUrl();
    }
}