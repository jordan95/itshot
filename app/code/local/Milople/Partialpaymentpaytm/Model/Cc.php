<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/magento-extensions/contacts/
* 
* @category		Ecommerce 
* @package		Milople_Partialpaymentccavenue 
* @copyright	Copyright (c) 2015 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/
class Milople_Partialpaymentpaytm_Model_Cc extends One97_paytm_Model_Cc
{
   public function getFormFields()
    {
		$orderIncrementId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
		$quote = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
		if($quote->getPaidAmount()>0)
			{
				$price      = number_format($quote->getPaidAmount(),2,'.','');
			}
			else
			{
				$price      = number_format($this->getOrder()->getGrandTotal(),2,'.','');
			}
        $currency   = $this->getOrder()->getOrderCurrencyCode();
 		$locale = explode('_', Mage::app()->getLocale()->getLocaleCode());
		if (is_array($locale) && !empty($locale))
			$locale = $locale[0];
		else
			$locale = $this->getDefaultLocale();
		 
		
		$const = (string)Mage::getConfig()->getNode('global/crypt/key');// Mage::getStoreConfig('payment/paytm_cc/constpaytm');
		$mer = Mage::helper('paytm')->decrypt_e($this->getConfigData('inst_key'),$const);
		$merid = Mage::helper('paytm')->decrypt_e($this->getConfigData('inst_id'),$const);
		$website = $this->getConfigData('website');
		$industry_type = $this->getConfigData('industrytype');
		$is_callback = $this->getConfigData('callbackUrl');
		$callbackUrl = rtrim(Mage::getUrl('paytm/processing/response',array('_nosid'=>true)),'/');
		$lastOrderId = Mage::getSingleton('checkout/session')->getLastOrderId();
		$order = Mage::getSingleton('sales/order');
		$order->load($lastOrderId);
		$_totalData = $order->getData();
		$email = $_totalData['customer_email'];
		$telephone = $order->getBillingAddress()->getTelephone();
		//create array using which checksum is calculated
		
    	$params = 	array(
	    			'MID' =>	$merid,  				
	    			'TXN_AMOUNT' =>	$price,
    				'CHANNEL_ID' => "WEB",
						'INDUSTRY_TYPE_ID' => $industry_type,
						'WEBSITE' => $website,
						'CUST_ID' => Mage::getSingleton('customer/session')->getCustomer()->getId(),
						'ORDER_ID'	=>	$this->getOrder()->getRealOrderId(),   				    
						'EMAIL'=> $email,
						'MOBILE_NO' => preg_replace('#[^0-9]{0,13}#is','',$telephone)
                                                
					);
                                        if($is_callback){
                                            $params['CALLBACK_URL'] = $callbackUrl;
                                        }
					
					//generate customer id in case this is a guest checkout
                                if(empty($params['CUST_ID'])){
                                        $params['CUST_ID'] = $email;
                                }
				
			$checksum = Mage::helper('paytm')->getChecksumFromArray($params, $mer);//generate checksum
			$params['CHECKSUMHASH'] = $checksum;
		  return $params;
    }
	public function getInstallmentFormFields($OrderId,$amount,$post)
    {
		$partial_payment = Mage::getModel('partialpayment/partialpayment')->load($OrderId);
		$order = Mage::getModel('sales/order')->load($OrderId);
		$quote = Mage::getModel('sales/order')->load($OrderId);
		$lastOrderId = $order->getIncrementId();
		$price = $amount;
        $currency   = $order->getOrderCurrencyCode();
 		$locale = explode('_', Mage::app()->getLocale()->getLocaleCode());
		if (is_array($locale) && !empty($locale))
			$locale = $locale[0];
		else
			$locale = $this->getDefaultLocale();
		
		$const = (string)Mage::getConfig()->getNode('global/crypt/key');// Mage::getStoreConfig('payment/paytm_cc/constpaytm');
		$mer = Mage::helper('paytm')->decrypt_e($this->getConfigData('inst_key'),$const);
		$merid = Mage::helper('paytm')->decrypt_e($this->getConfigData('inst_id'),$const);
		$website = $this->getConfigData('website');
		$industry_type = $this->getConfigData('industrytype');
		$is_callback = $this->getConfigData('callbackUrl');
		$callbackUrl = rtrim(Mage::getUrl('paytm/processing/installmentresponse',array('_nosid'=>true)),'/');
		$_totalData = $order->getData();
		$email = $_totalData['customer_email'];
		$telephone = $order->getBillingAddress()->getTelephone();
		//create array using which checksum is calculated
    	$params = 	array(
	    			'MID' =>	$merid,  				
	    			'TXN_AMOUNT' =>	$price,
    				'CHANNEL_ID' => "WEB",
						'INDUSTRY_TYPE_ID' => $industry_type,
						'WEBSITE' => $website,
						'CUST_ID' => Mage::getSingleton('customer/session')->getCustomer()->getId(),
						'ORDER_ID'	=>	$order->getIncrementId().'-'.uniqid(),
						'EMAIL'=> $email,
						'MOBILE_NO' => preg_replace('#[^0-9]{0,13}#is','',$telephone)
                                                
					);
                                        if($is_callback){
                                            $params['CALLBACK_URL'] = $callbackUrl;
                                        }
					
					//generate customer id in case this is a guest checkout
                                if(empty($params['CUST_ID'])){
                                        $params['CUST_ID'] = $email;
                                }
			$checksum = Mage::helper('paytm')->getChecksumFromArray($params, $mer);//generate checksum
			$params['CHECKSUMHASH'] = $checksum;
		  return $params;
    }
}
 