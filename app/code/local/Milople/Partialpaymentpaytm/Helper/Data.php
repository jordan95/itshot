<?php
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/magento-extensions/contacts/
* 
* @category		Ecommerce 
* @package		Milople_Partialpaymentccavenue 
* @copyright	Copyright (c) 2015 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/

class Milople_Partialpaymentpaytm_Helper_Data extends One97_paytm_Helper_Data
{
	public function ccavenueForm($OrderId,$amount,$post)
	{	
		echo'You will be redirected to Paytm Payment Gateway in a few seconds.';
		$new = new Milople_Partialpaymentpaytm_Block_Redirect; 
		$order1 = Mage::getModel('sales/order')->load($OrderId);
		$html ='<form name="paytm_checkout" id="paytm_checkout" action=" '.$new->getNewFormAction($order1) .' " method="POST">';
		$order = Mage::getModel('paytm/cc');
		$check = $order->getInstallmentFormFields($OrderId,$amount,$post);
		foreach ($check  as $field=>$value)
		{
			$html .= '<input type=hidden value="'.$value.'" name="'.$field.'" id="Merchant_Id">';
		}
		return $html;
	}
}