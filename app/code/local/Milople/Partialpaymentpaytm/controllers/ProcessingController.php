<?php      
/** 
* 
* Do not edit or add to this file if you wish to upgrade the module to newer 
* versions in the future. If you wish to customize the module for your 
* needs please contact us to https://www.milople.com/magento-extensions/contacts/
* 
* @category		Ecommerce 
* @package		Milople_Partialpaymentccavenue 
* @copyright	Copyright (c) 2015 Milople Technologies Pvt. Ltd. All Rights Reserved. 
* @url			https://www.milople.com/magento-extensions/partial-payment.html
* 
**/
require_once Mage::getModuleDir('controllers', 'One97_paytm') . DS . 'ProcessingController.php';

 
class Milople_Partialpaymentpaytm_ProcessingController extends One97_paytm_ProcessingController
{	
	public function installmentresponseAction()
    {
			$request = $this->_checkReturnedPost();
        try {
			$parameters = array();
			foreach($request as $key=>$value)
			{
			$parameters[$key] = $request[$key];
			}
			$isValidChecksum = false;
			$txnstatus = false;
			$authStatus = false;
			$mer_encrypted = Mage::getStoreConfig('payment/paytm_cc/inst_key');
			$const = (string)Mage::getConfig()->getNode('global/crypt/key');
			$mer_decrypted= Mage::helper('paytm')->decrypt_e($mer_encrypted,$const);
			
			$merid_encrypted = Mage::getStoreConfig('payment/paytm_cc/inst_id');
			$const = (string)Mage::getConfig()->getNode('global/crypt/key');
			$merid_decrypted= Mage::helper('paytm')->decrypt_e($merid_encrypted,$const);
			
			//setting order status
            $order = Mage::getModel('sales/order');
			
            $order->loadByIncrementId($request['ORDERID']);
			//check returned checksum
			if(isset($request['CHECKSUMHASH']))
			{
				$return = Mage::helper('paytm')->verifychecksum_e($parameters, $mer_decrypted, $request['CHECKSUMHASH']);
				if($return == "TRUE")
				$isValidChecksum = true;
				
			}
			
			if($request['STATUS'] == "TXN_SUCCESS"){
				$txnstatus = true;
			}
			
			$_testurl = NULL;
			if(Mage::getStoreConfig('payment/paytm_cc/mode')==1)
				$_testurl = Mage::helper('paytm/Data2')->STATUS_QUERY_URL_PROD;
			else
				$_testurl = Mage::helper('paytm/Data2')->STATUS_QUERY_URL_TEST;

			if($txnstatus && $isValidChecksum){
				// Create an array having all required parameters for status query.
				//echo "<pre>"; print_r($request);
				$requestParamList = array("MID" => $merid_decrypted , "ORDERID" => $request['ORDERID']);
				
				$StatusCheckSum = Mage::helper('paytm')->getChecksumFromArray($requestParamList, $mer_decrypted);
							
				$requestParamList['CHECKSUMHASH'] = $StatusCheckSum;
				
				// Call the PG's getTxnStatus() function for verifying the transaction status.
				
				$check_status_url = 'https://pguat.paytm.com/oltp/HANDLER_INTERNAL/getTxnStatus';
				if(Mage::getStoreConfig('payment/paytm_cc/mode') == '1')
				{
					$check_status_url = 'https://secure.paytm.in/oltp/HANDLER_INTERNAL/getTxnStatus';
				}
				$responseParamList = Mage::helper('paytm')->callNewAPI($check_status_url, $requestParamList);
				//echo "<pre>"; print_r($responseParamList); die;
				$authStatus = true;
				if($authStatus == false)					
					{
						$this->_processCancel($request);
						
					}
				else
					{
						if($responseParamList['STATUS']=='TXN_SUCCESS' && $responseParamList['TXNAMOUNT']==$_POST['TXNAMOUNT'])
						{
							$response = $this->getRequest()->getPost();
							/****** Installment Success *******/
							$getInstallmentId = Mage::getSingleton('core/session')->getInstallmentId();
							$getOrderInstallmentId = Mage::getSingleton('core/session')->getInstallmentOrderId();
							//Mage::getSingleton('core/session')->unsInstallmentId();
							//Mage::getSingleton('core/session')->unsInstallmentOrderId();
							
							if($getInstallmentId){
							$this->installmentsuccess($response,$getInstallmentId,$getOrderInstallmentId);							
							}
							$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING,true)->save();
							$this->_processSale($request);
							$order_mail = new Mage_Sales_Model_Order();
							$incrementId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
							$order_mail->loadByIncrementId($incrementId);
							if($order->getPaidAmount()>=0 && $order->getRemainingAmount()>=0)
							{			
								$session = Mage::getSingleton('checkout/session');
								$session->setQuoteId($session->getCcavenuepayStandardQuoteId(true));
								$calculation_model = Mage::getModel("partialpayment/calculation");
								$calculation_model->setOrderSuccessData($order->getId());
							}
							try{
								 $order_mail->sendNewOrderEmail();
							   }    
							catch (Exception $ex) {  }
						}
						else
						{
							$this->_processFail($request);
						}
					}
            }
			else
				$this->_processCancel($request);
				
			
        } catch (Mage_Core_Exception $e) {
            $this->getResponse()->setBody(
                $this->getLayout()
                    ->createBlock($this->_failureBlockType)
                    ->setOrder($this->_order)
                    ->toHtml()
            );
			
			$this->_processFail($request);
			
        }
    }
	
	
	public function cancelAction()
    {
        $getInstallmentId = Mage::getSingleton('core/session')->getInstallmentId();
		$getOrderInstallmentId = Mage::getSingleton('core/session')->getInstallmentOrderId();
		Mage::getSingleton('core/session')->unsInstallmentId();
		Mage::getSingleton('core/session')->unsInstallmentOrderId();
		$partial_model = Mage::getModel('partialpayment/installment')->load($getInstallmentId);
		$partialid = $partial_model->getPartialPaymentId();
		
		if($getInstallmentId){
			$calculation_model = Mage::getModel('partialpayment/calculation');
			foreach(explode("-",$getInstallmentId) as $installment_id)
			{
				$calculation_model->setInstallmentCancelData($installment_id);
			}
			Mage::getSingleton('core/session')->addError(Mage::helper("partialpayment")->__('Paytm Payment has been cancelled and the transaction has been declined.'));
			Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('partialpayment/index/installments', array('order_id' => $getOrderInstallmentId, 'partial_payment_id' => $partialid)));	
			Mage::app()->getFrontController()->getResponse()->sendResponse();
			exit;
		}
		// set quote to active
        $session = $this->_getCheckout();
		$order = Mage::getModel('sales/order');
		$order->loadByIncrementId($session->getLastRealOrderId());
		if (!$order->getId()) {
		Mage::throwException('No order for processing found');
		}
		$order->setPaidAmount(0);
		$order->setBasePaidAmount(0);
		$order->setRemainingAmount(0);
		$order->setBaseRemainingAmount(0);
		$order->setState(
		Mage_Sales_Model_Order::STATE_CANCELED,true)->save();
		
        if ($quoteId = $session->getpaytmQuoteId()) {
            $quote = Mage::getModel('sales/quote')->load($quoteId);
            if ($quote->getId()) {
                $quote->setIsActive(true)->save();
                $session->setQuoteId($quoteId);
            }
        }
		$partial_payment = Mage::getModel('partialpayment/partialpayment')->getCollection();
				$partial_payment->addFieldToFilter('order_id',$session->getLastOrderId());
				
				$size = $partial_payment->getSize();
				foreach ($partial_payment as $item)
				{	
					mage::log($item);
					 $remainingAmount = $item->getTotalAmount();
					 $remaningInstallment = $item->getTotalInstallment();
					  
						$partial_payment_save = Mage::getModel('partialpayment/partialpayment')
											->setPaidAmount(0)
											->setPaidInstallment(0)
											->setRemainingAmount($remainingAmount)
											->setRemainingInstallment($remaningInstallment)	
											->setPartialPaymentId($item->getPartialPaymentId())
											->save();
										
						$partial_payment_installments_save = Mage::getModel('partialpayment/installment')->getCollection()->addFieldToFilter('partial_payment_id', $item->getPartialPaymentId());
						
						foreach($partial_payment_installments_save as $partial_payment_installment_save)
						{
							$installment_save = Mage::getModel('partialpayment/installment')->load($partial_payment_installment_save->getInstallmentId());
							$installment_save->setInstallmentStatus('Canceled')->save();
						}
				 }
        $session->addError(Mage::helper('paytm')->__('The order has been canceled.'));
        $this->_redirect('checkout/cart');
    }
	
	public function installmentsuccess($response,$getInstallmentId,$getOrderInstallmentId)
	{
		$modules = Mage::getConfig()->getNode('modules')->children();
		$modulesArray = (array)$modules;
		$partial_model = Mage::getModel('partialpayment/installment')->load($getInstallmentId);
		$partial_payment_id = $partial_model->getPartialPaymentId();
		$partial_payment = Mage::getModel('partialpayment/partialpayment')->load($partial_payment_id);
		$installment_id = $getInstallmentId;
		$installment_ids = explode("-",$installment_id);
		//$payment_method = Mage::app()->getRequest()->getParam('payment_method');
		
		$calculation_model = Mage::getModel('partialpayment/calculation');
		$instalmentamt = 0;
		$total_installment = 0;
				
		if($partial_payment->getTotalInstallments() != $partial_payment->getPaidInstallments() )//&& $response_status == "OK")
		{
			$order=Mage::getModel('sales/order')->load($partial_payment->getOrderId());
			$OrderId = $order->getIncrementId();
			$payment = $order->getPayment();
			$method = $payment->getMethodInstance();
			$payment_method = $method->getTitle();
			foreach ($installment_ids as $installment_id)
			{
				$calculation_model->setInstallmentSuccessData($installment_id,$partial_payment->getOrderId(),$partial_payment_id,$payment_method);
				$total_installment++;
			}
			/*Message to display if the installment is paid */
			
			if($total_installment == 1)
			{
			   Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installment of order #%s has been paid successfully.",$total_installment,$OrderId));
			}
			else
			{
			   Mage::getSingleton("core/session")->addSuccess(Mage::helper("partialpayment")->__("%s installments of order #%s have been paid successfully.",$total_installment,$OrderId));
			}
    	}
		
		Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('partialpayment/index/installments', array('order_id' => $OrderId, 'partial_payment_id' => $partial_payment->getPartialPaymentId())));
	
		Mage::app()->getFrontController()->getResponse()->sendResponse();
		exit;
	}
	
	protected function _checkReturnedPost()
    {
            // check request type
        if (!$this->getRequest()->isPost())
            Mage::throwException('Wrong request type.');


            // get request variables
        $request = $this->getRequest()->getPost();
		
		if( strpos( $request['ORDERID'], '-' ) !== false )
			return $request;
		
		if (empty($request))
            Mage::throwException('Request doesn\'t contain POST elements.');
		
		Mage::log($request);

            // check order id
        if (empty($request['ORDERID']) )
            Mage::throwException('Missing or invalid order ID');

            // load order for further validation
        $this->_order = Mage::getModel('sales/order')->loadByIncrementId($request['ORDERID']);
        if (!$this->_order->getId())
            Mage::throwException('Order not found');


        return $request;
    }	
}