<?php
$this->startSetup();

$featuredTable = $this->getTable('featureds/featured');
$featuredProductTable = $this->getTable('featureds/featuredproduct');

$this->run("
	DROP TABLE IF EXISTS `{$featuredTable}`;
	CREATE TABLE `{$featuredTable}` (
	    `featured_id`   int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Featured ID',
	    `is_active`  tinyint DEFAULT 0,
	    `title`      varchar(255) DEFAULT NULL,
	    `itemqty`      varchar(255) DEFAULT NULL,
	    `columnqty`      varchar(255) DEFAULT NULL,
        `image`      varchar(255) DEFAULT NULL,
        `store_id`   int(10) unsigned NOT NULL COMMENT 'Store ID',
		`created_at` timestamp NULL DEFAULT NULL COMMENT 'Creation Time',
  		`updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Update Time',
		PRIMARY KEY (`featured_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Featured Entity Table';
");

$this->run("
	DROP TABLE IF EXISTS `{$featuredProductTable}`;
	CREATE TABLE `{$featuredProductTable}` (
		`featuredproduct_id`   int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Featured ID',
	    `product_id`   int(10) unsigned NOT NULL COMMENT 'Product ID',
	    `featured_id`   int(10) unsigned NOT NULL COMMENT 'Featured ID',
	    `position`   int(10) unsigned NOT NULL COMMENT 'Position',
		PRIMARY KEY (`featuredproduct_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Featuredproduct Entity Table';
");

$this->endSetup();