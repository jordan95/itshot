<?php
abstract class ML_Featureds_Block_Adminhtml_Associations_Abstract extends Mage_Adminhtml_Block_Widget_Grid
implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

	public function __construct()
	{
		parent::__construct();
	}

    public function canShowTab()
    {
    	return true;
    }

    public function isHidden()
    {
    	return false;
    }

	public function getTabClass()
	{
		return 'ajax';
	}

	public function getSkipGenerateContent()
	{
		return true;
	}

	protected function _getDefaultSort()
	{
		return 'assoc.position';
	}

	protected function _getDefaultDir()
	{
		return 'asc';
	}

	protected function _getUrlParams()
	{
		$params = array(
			'associations_type' => $this->getAssociationType(),
		);

		if ($this->_getObject()) {
			$params['id'] = $this->_getObject()->getId();
		}

		if ($store = Mage::app()->getRequest()->getParam('store', false)) {
			$params['store'] = $store;
		}

		return $params;
	}

	public function getCurrentUrl($params = array())
	{
		return $this->getUrl('adminhtml/featureds_associations/gridRefresh', array('_query' => $this->_getUrlParams()));
	}

	public function getTabUrl()
	{
		return $this->getUrl('adminhtml/featureds_associations/grid', array('_query' => $this->_getUrlParams()));
	}

	protected function _getMbPrimaryKey()
	{
		return 'ID';
	}

	protected function _getObject()
	{
		return Mage::registry('product');
	}

    public function getTabLabel()
    {
	    return $this->__('Featureds As');
	}

    public function getTabTitle()
    {
    	return $this->getTabLabel();
    }

    public function getStoreId()
    {
    	return Mage::app()->getFrontController()->getAction()->getStoreId();
    }
}
