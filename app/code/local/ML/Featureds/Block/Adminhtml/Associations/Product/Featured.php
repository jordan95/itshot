<?php
class ML_Featureds_Block_Adminhtml_Associations_Product_Featured extends ML_Featureds_Block_Adminhtml_Associations_Abstract
{
	
	public function __construct()
	{
		parent::__construct();
		$this->setId('productfeatured');
		$this->setDefaultSort($this->_getDefaultSort());
		$this->setDefaultDir($this->_getDefaultDir());
		$this->setSaveParametersInSession(false);
		$this->setUseAjax(true);
	
		if (is_object($this->_getObject())) {
			$this->setDefaultFilter(array('is_associated' => 1));
		}
	}
	
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('featureds/featured')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();

	}
	
	protected function _prepareColumns()
	{
		$this->addColumn('is_associated', array(
				'header_css_class'  => 'a-center',
				'type' => 'checkbox',
				'name'	=> 'is_associated',
				'align' => 'center',
				'index' => 'featured_id',
				'values' => array_values($this->getSelectedMbItems()),
		));
		
        $this->addColumn('store_id', array(
            'header' => Mage::helper('featureds')->__('Store'),
            'index'  => 'store_id',
            'type'    => 'options',
            'options' => Mage::getModel('featureds/system_config_source_options')->storesOptionArray(),
            'width'   => 120,
            ''
        ));
        
		$this->addColumn('title', array(
				'header'=> 'Title',
				'index' => 'title',
		));
		
		$this->addColumn('is_active', array(
				'header' => Mage::helper('featureds')->__('Is Active'),
				'index'  => 'is_active',
				'type'    => 'options',
				'options' => Mage::getModel('featureds/system_config_source_options')->statusOptionArray(),
				'width'   => 120,
				''
		));

		$this->addColumn('position', array(
				'header'			=> Mage::helper('featureds')->__('Position'),
				'name'				=> 'position',
				'type'				=> 'number',
				'validate_class'	=> 'validate-number',
				'index'				=> 'position',
				'width'				=> 100,
				'editable'			=> true,
				'filterable'			=> false,
				'sortable'			=> false,
		));
	
		return parent::_prepareColumns();
	}

	public function getSelectedMbItems()
	{
		return array_keys($this->getSelectedMbItemPositions());
	}
	
	public function getSelectedMbItemPositions()
	{
		$out = array();
		
		$product_id = $this->_getObject()->getId();
		$store_id = $this->getStoreId();
		
		$collection = Mage::getModel('featureds/featuredproduct')->getCollection();
		$collection->addFieldToFilter('product_id', array('in'=>$product_id));
		
		if($collection){
			foreach($collection as $item){
				$out[$item->getFeaturedId()] = array('position' => $item->getPosition());
			}
		}

		return $out;
		
	}
	
	protected function _addColumnFilterToCollection($column)
	{
		return $this;
	}

	public function getAssociationType()
	{
		return 'product/featured';
	}
}