<?php
class ML_Featureds_Block_Adminhtml_Featureds extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
	    $this->_controller = 'adminhtml_featureds';
	    $this->_blockGroup = 'featureds';
	    $this->_headerText = Mage::helper('featureds')->__('Manage featured');
	    $this->_addButtonLabel = Mage::helper('featureds')->__('Add featured');
	    parent::__construct();
	}
}