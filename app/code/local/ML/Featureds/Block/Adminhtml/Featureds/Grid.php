<?php
class ML_Featureds_Block_Adminhtml_Featureds_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('featuredsGrid');
      $this->setDefaultSort('featured_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('featureds/featured')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('featured_id', array(
          'header'    => Mage::helper('featureds')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'featured_id',
      ));
      
      $this->addColumn('store_id', array(
      		'header' => Mage::helper('featureds')->__('Store'),
      		'index'  => 'store_id',
      		'type'    => 'options',
      		'options' => Mage::getModel('featureds/system_config_source_options')->storesOptionArray(),
      		'width'   => 120,
      		''
      ));
      /*
      $this->addColumn('image', array(
      		'header'    => Mage::helper('featureds')->__('Image'),
      		'align'     => 'left',
      		'width'     => '100px',
      		'index'     => 'image',
      		'type'      => 'image',
      		'escape'    => true,
      		'sortable'  => false,
      		'filter'    => false,
      		'renderer'  => new ML_Featureds_Block_Adminhtml_Template_Grid_Renderer_Image,
      ));
*/
      $this->addColumn('title', array(
          'header'    => Mage::helper('featureds')->__('Title'),
          'align'     =>'left',
          'index'     => 'title',
      ));
      
      $this->addColumn('is_active', array(
      		'header' => Mage::helper('featureds')->__('Is Active'),
      		'index'  => 'is_active',
      		'type'    => 'options',
      		'options' => Mage::getModel('featureds/system_config_source_options')->statusOptionArray(),
      		'width'   => 120,
      		''
      ));

        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('featureds')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('featureds')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('featureds')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('featureds')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('featured_id');
        $this->getMassactionBlock()->setFormFieldName('featureds');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('featureds')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('featureds')->__('Are you sure?')
        ));
        

        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}