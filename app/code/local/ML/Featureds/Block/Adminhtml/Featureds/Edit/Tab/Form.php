<?php
class ML_Featureds_Block_Adminhtml_Featureds_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('featureds_form', array('legend'=>Mage::helper('featureds')->__('Featured information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('featureds')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));
      
      $fieldset->addField('itemqty', 'text', array(
      		'label'     => Mage::helper('featureds')->__('Number of Items'),
      		'class'     => 'required-entry',
      		'required'  => true,
      		'name'      => 'itemqty',
      ));
      
      $fieldset->addField('columnqty', 'text', array(
      		'label'     => Mage::helper('featureds')->__('Items per column'),
      		'class'     => 'required-entry',
      		'required'  => true,
      		'name'      => 'columnqty',
      ));
      
      $fieldset->addField('is_active', 'select', array(
      		'label'     => Mage::helper('featureds')->__('Status'),
      		'name'      => 'is_active',
      		'values'    => Mage::getModel('featureds/system_config_source_options')->statusOptionArray(),
      ));
      
      $fieldset->addField('store_id', 'select', array(
      		'label'     => Mage::helper('featureds')->__('Store'),
      		'name'      => 'store_id',
      		'values'    => Mage::getModel('featureds/system_config_source_options')->storesOptionArray(),
      ));
/*
      $fieldset->addField('image', 'image', array(
          'label'     => Mage::helper('featureds')->__('Image'),
          'required'  => true,
          'class'     => 'required-entry',
          'name'      => 'image',
	  ));
*/
      if ( Mage::getSingleton('adminhtml/session')->getFeaturedsData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getFeaturedsData());
          Mage::getSingleton('adminhtml/session')->setFeaturedsData(null);
      } elseif ( Mage::registry('featureds_data') ) {
          $form->setValues(Mage::registry('featureds_data')->getData());
      }
      return parent::_prepareForm();
  }
}