<?php
class ML_Featureds_Block_Adminhtml_Featureds_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('featureds_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('featureds')->__('Featured Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('featureds')->__('General'),
          'title'     => Mage::helper('featureds')->__('General'),
          'content'   => $this->getLayout()->createBlock('featureds/adminhtml_featureds_edit_tab_form')->toHtml(),
      ));
      
      $product_content = $this->getLayout()->createBlock('featureds/adminhtml_featureds_edit_tab_product', 'featureds_products.grid')->toHtml();
      $serialize_block = $this->getLayout()->createBlock('adminhtml/widget_grid_serializer');
      $serialize_block->initSerializerBlock('featureds_products.grid', 'getSelectedProducts', 'products', 'selected_products');
      $serialize_block->addColumnInputName('position');
      $product_content .= $serialize_block->toHtml();
      $this->addTab('associated_products', array(
      		'label'     => Mage::helper('featureds')->__('Products'),
      		'title'     => Mage::helper('featureds')->__('Products'),
      		'content'   => $product_content
      ));
     
      return parent::_beforeToHtml();
  }
}