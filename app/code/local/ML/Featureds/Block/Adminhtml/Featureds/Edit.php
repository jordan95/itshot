<?php
class ML_Featureds_Block_Adminhtml_Featureds_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'featureds';
        $this->_controller = 'adminhtml_featureds';
        
        $this->_updateButton('save', 'label', Mage::helper('featureds')->__('Save featured'));
        $this->_updateButton('delete', 'label', Mage::helper('featureds')->__('Delete featured'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('featureds')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('featureds_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'featureds_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'featureds_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('featureds_data') && Mage::registry('featureds_data')->getId() ) {
            return Mage::helper('featureds')->__("Edit featured '%s'", $this->htmlEscape(Mage::registry('featureds_data')->getTitle()));
        } else {
            return Mage::helper('featureds')->__('Add featured');
        }
    }
}