<?php
class ML_Featureds_Block_Featured extends Mage_Catalog_Block_Product_Abstract //Mage_Core_Block_Template
{
    protected $_featured = '';
    protected $getBlockId = '';

	public function __construct()
	{
		$enabled = Mage::getStoreConfig('featureds/configuration/enabled');
		if(!$enabled) {
            return "";
        }
        parent::__construct();
	}

	// Config Settings
	 public function enabled()
	{
		$enabled = Mage::getStoreConfig('featureds/configuration/enabled');
		return $enabled;
	}

	public function getTitle($id=null)
	{
		return $this->_featured->getTitle();
	}

	public function getColumnqty($id=null)
	{
		return $this->_featured->getColumnqty();
	}

	public function getProducts($id=null)
	{
        return $this->_featured->getProducts();
	}

	public function getStoreId()
	{
		return Mage::app()->getStore()->getId();
	}

    public function setBlockId($id){
    	$this->getBlockId = $id;
    	$this->_featured = Mage::getModel('featureds/featured')->load($this->getBlockId());
    }

    public function getBlockId(){
    	return $this->getBlockId;
    }

    public function enabledBlock(){
    	return $this->_featured->getIsActive();
    }

    public function getAddToCartUrl($product, $additional = array())
    {
    	switch ($product['type_id']){
    		case 'configurable':{
    			return parent::getAddToCartUrl($product, $additional).'?prodtype='.$product['type_id'];
    		}
    		break;
    		case 'simple':{

    			if (!$product->getTypeInstance(true)->hasRequiredOptions($product)) { //no options
    				return parent::getAddToCartUrl($product, $additional).'prodtype/'.$product['type_id'].'/';

    			}else{ // with options
    				//return $this->helper('checkout/cart')->getAddUrl($product, $additional);
    				return parent::getAddToCartUrl($product, $additional).'?prodtype=configurable';
    			}
    			//return parent::getAddToCartUrl($product, $additional).'prodtype/'.$product['type_id'].'/';
    		}
    		break;
    		default:
    			break;
    	}
    	return parent::getAddToCartUrl($product, $additional).'prodtype/'.$product['type_id'].'/';
    }
}
?>