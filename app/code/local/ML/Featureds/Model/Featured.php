<?php
class ML_Featureds_Model_Featured extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('featureds/featured');
    }

    protected function _afterLoad()
    {
    	parent::_afterLoad();
    	$out = array();
    	$ids = array();
    	$id = $this->getId();
    	if(!empty($id)):
    		$collection = Mage::getModel('featureds/featuredproduct')->getCollection();
    		$collection->addFieldToFilter('featured_id',$id);
    		foreach($collection as $item):
    			$out[$item->getProductId()] = $item->getPosition();
    			$ids[] = $item->getProductId();
    		endforeach;


            if(!empty($ids)):
                $collection = Mage::getModel('catalog/product')->getCollection();

                $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();

                $collection->addAttributeToSelect($attributes)
                ->addAttributeToFilter('entity_id', $ids)
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addStoreFilter()
                ->setOrder('position', 'ASC')
                ->getSelect()->limit($this->getItemqty());

                Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
                Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

	    		$this->setProducts($collection);
    		endif;

    	endif;

    	$this->setProductsPosition($out);
    	return $this;
    }

    protected function _afterSave()
    {
    	parent::_afterSave();
    	try {
    		$id = $this->getId();
    		$featuredsSerialized =  $this->getData();
    		if (is_string($featuredsSerialized['products']) && !empty($featuredsSerialized['products'])) {
    			$featureds = Mage::helper('adminhtml/js')->decodeGridSerializedInput($featuredsSerialized['products']);

    			if(is_array($featureds) && !empty($featureds)){

    				$write = Mage::getSingleton('core/resource')->getConnection('core_write');
    				$tableName = Mage::getSingleton('core/resource')->getTableName('featured_product');

    				$where = $write->quoteInto('featured_id = ?', $id);
    				$write->delete($tableName, $where);

    				foreach($featureds as $productid => $position){
    					$data[] = array('product_id' => $productid, 'featured_id' => $id, 'position' => $position['position']);
    				}
    				$write->insertArray($tableName, array('product_id', 'featured_id', 'position'), $data);
    			}

    		}else{
    			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
    			$tableName = Mage::getSingleton('core/resource')->getTableName('featured_product');
    			$where = $write->quoteInto('featured_id = ?', $id);
    			$write->delete($tableName, $where);
    		}

    	}
    	catch (Exception $e) {
    		Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
    	}


    }

}
