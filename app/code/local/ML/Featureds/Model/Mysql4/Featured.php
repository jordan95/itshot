<?php
class ML_Featureds_Model_Mysql4_Featured extends Mage_Core_Model_Mysql4_Abstract
{

    protected function _construct()
    {
        $this->_init('featureds/featured', 'featured_id');
    }

}
