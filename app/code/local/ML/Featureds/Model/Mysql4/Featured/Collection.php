<?php
class ML_Featureds_Model_Mysql4_Featured_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	protected $_collectAll = false;
	
    protected function _construct()
    {
        $this->_init('featureds/featured');
    }
    
    public function setCollectAll($flag)
    {
    	$this->_collectAll = $flag;
    	return $this;
    }
    
    protected function _getItemId(Varien_Object $item)
    {
    	if ($this->_collectAll) {
    		return null;
    	}
    	return $item->getId();
    }
    
    public function toOptionArray()
    {
    	return $this->_toOptionArray('featured_id', 'title');
    }
    
    public function useActive()
    {
    	$this->addFilter('main_table.is_active', 1);
    	return $this;
    }
    
    public function useStore()
    {
    	$store_id = Mage::app()->getStore()->getId();
    	$this->addFieldToFilter('main_table.store_id', array('in' => array(0, $store_id)));
    	return $this;
    }
    
    public function useProduct($productIds)
    {
    	if (!is_array($productIds)) {
    		$productIds = array((int) $productIds);
    	}
    
    	$cond = 'main_table.featured_id=ps.featured_id AND ps.product_id IN (?)';
    	$cond = $this->getConnection()->quoteInto($cond, $productIds);
    
    	$this->getSelect()->join(array('ps' => $this->getTable('featureds/featuredproduct')), $cond, array('product_id'));
    
    	$this->_additionalFields['product_id'] = 'product_id';
    
    	return $this;
    }
    
    public function toMultiArray($key = 'featured_id', $valueKey = 'featured_id')
    {
    	$options = array();
    	foreach($this as $item) {
    		$options[$item->getData($key)][$item->getData($valueKey)] = $item->getData();
    	}
    	return $options;
    }


}
     