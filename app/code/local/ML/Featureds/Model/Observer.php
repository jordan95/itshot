<?php
class ML_Featureds_Model_Observer
{
    static protected $_singletonFlag = false;
    const XML_PATH_SEARCH_OPTIMIZES_ENABLE_PRODUCT_FEATURED = 'searchoptimizes/general/enable_product_featured';

    public function saveFeaturedProductTabData(Varien_Event_Observer $observer)
    {
        $enabled = Mage::getStoreConfig('featureds/configuration/enabledbackend');
    	if(!$enabled){
    		return $this;
    	}

        if (!self::$_singletonFlag) {
           self::$_singletonFlag = true;
           $product = $observer->getEvent()->getProduct();

            try {

            	$featuredsSerialized =  $this->_getRequest()->getPost('featureds');
            	if (is_string($featuredsSerialized['product']) && !empty($featuredsSerialized['product'])) {
            		$featureds = Mage::helper('adminhtml/js')->decodeGridSerializedInput($featuredsSerialized['product']);
            		if(is_array($featureds) && !empty($featureds)){

            			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
            			$tableName = Mage::getSingleton('core/resource')->getTableName('featured_product');

            			$where = $write->quoteInto('product_id = ?', $product->getId());
            			$write->delete($tableName, $where);

            			foreach($featureds as $id => $position){
            				$data[] = array('product_id' => $product->getId(), 'featured_id' => $id, 'position' => $position['position']);
            			}
            			$write->insertArray($tableName, array('product_id', 'featured_id', 'position'), $data);
            		}
            	}else{
            		$pdata = $this->_getRequest()->getPost('product');
                	//if(isset($pdata['sku'])){
                	if(isset($featuredsSerialized['product']) && isset($pdata['sku'])){
                		$productid = Mage::getModel('catalog/product')->getIdBySku($pdata['sku']);
                		if(!empty($productid)){
	                		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
	                		$tableName = Mage::getSingleton('core/resource')->getTableName('featured_product');
	                		$where = $write->quoteInto('product_id = ?', $productid);
	                		$write->delete($tableName, $where);
                		}
                	}
            	}

            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
    }

    public function getProduct()
    {
        return Mage::registry('product');
    }

    protected function _getRequest()
    {
        return Mage::app()->getRequest();
    }

    public function catalogProductCollectionLoadAfter($observer)
    {
        $enabled = Mage::getStoreConfig('featureds/configuration/enabled');
    	if(!$enabled){
    		return $this;
    	}
       
        $request = Mage::app()->getRequest();
        $module = $request->getRouteName();
        $module_controller = $request->getControllerName();
        $module_controller_action = $request->getActionName();

        $fullActionName = $module . "_" . $module_controller . "_" . $module_controller_action;
        if ($fullActionName == 'catalogsearch_result_index') {
            $enable_product_featured = Mage::getStoreConfig(self::XML_PATH_SEARCH_OPTIMIZES_ENABLE_PRODUCT_FEATURED);
            if (!$enable_product_featured) {
                return $this;
            }
        }

        $productCollection = $observer->getEvent()->getCollection();
    	if ($productCollection && ($productCollection instanceof Varien_Data_Collection) && count($productCollection->getAllIds()))
    	{
    		$featureds = $this->_getFeaturedsByProduct($productCollection->getAllIds());

    		foreach($featureds as $productId => $productFeatureds) {
    			$this->_product = $productCollection->getItemById($productId);
    			if ($this->_product && ($this->_product  instanceof Mage_Catalog_Model_Product) && $this->_product->getId())
    			{
    				$this->_addFeaturedsToCurrentProduct($productFeatureds);
    			}
    		}
    	}
    	return $this;
    }

    public function catalogProductLoadAfter($observer)
    {
    	$enabled = Mage::getStoreConfig('featureds/configuration/enabled');
    	if(!$enabled){
    		return $this;
    	}

    	$data = $observer->getEvent()->getDataObject()->getData();
        if (isset($data['entity_id'])) {
        	$featureds = $this->_getFeaturedsByProduct($data['entity_id']);
        	if(isset($featureds[$data['entity_id']])){
        		$data['featureds'] = $featureds[$data['entity_id']];
        		$observer->getEvent()->getDataObject()->setData($data);
        	}
        }
    	return $this;
    }

    public function _getFeaturedsByProduct($productIds)
    {
        $enabled = Mage::getStoreConfig('featureds/configuration/enabled');
    	if(!$enabled){
    		return $this;
    	}

    	if (!is_array($productIds)) {
    		$productIds = array($productIds);
    	}
    	$featureds = Mage::getModel('featureds/featured')->getCollection()
    		->useActive()
    		->useStore()
    		->useProduct($productIds)
    		->setCollectAll(true)
    		->toMultiArray('product_id', 'featured_id');

    	return $featureds;
    }

    protected function _addFeaturedsToCurrentProduct(array &$featureds)
    {
        $enabled = Mage::getStoreConfig('featureds/configuration/enabled');
    	if(!$enabled){
    		return $this;
    	}

    	$data = $this->_product->getData();
    	$data['featureds'] = $featureds;
    	$this->_product->setData($data);
    	return $this;
    }
}