<?php
class ML_Featureds_Model_System_Config_Source_Options
{
    public function statusOptionArray()
    {
    	$options[0] = 'Disabled';
    	$options[1] = 'Enabled';
    	
    	return $options;
    }
    
    public function storesOptionArray(){
    	$stores = Mage::app()->getStores();
    	$out[0] = 'All';
    	foreach($stores as $store){
    		$out[$store->getId()] = $store->getName();
    	}
    	return $out;
    }

}
