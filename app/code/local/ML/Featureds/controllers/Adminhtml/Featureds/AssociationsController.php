<?php
class ML_Featureds_Adminhtml_Featureds_AssociationsController extends Mage_Adminhtml_Controller_Action
{

	public function gridAction()
	{
		$this->_performAssociationAction();
	}

	public function gridRefreshAction()
	{
		$this->_performAssociationAction('_grid');
	}

	protected function _performAssociationAction($handlePostfix = '')
	{
		try {
		
			if ($this->_initObject() === false) {
				return $this->_forward('noRoute');
			}
	
			$handle = 'adminhtml_featureds_association_' . $this->_getMagentoEntity() . '_' . $this->_getMbEntity() . $handlePostfix;

			$this->loadLayout($handle);
	
			$this->getLayout()->getUpdate()
				->removeHandle('adminhtml_featureds_associations_grid')
				->removeHandle('STORE_admin')
				->removeHandle('THEME_adminhtml_default_default');
			
			$this->renderLayout();
		}
		catch (Exception $e) {
			$this->getResponse()->setBody(
				sprintf('<h1>%s</h1><pre>%s</pre>', $e->getMessage(), $e->getTraceAsString())
			);
		}
	}

	public function getAssociationType()
	{
		return $this->getRequest()->getParam('associations_type', false);
	}

	protected function _getMagentoEntity()
	{
		if (($type = $this->getAssociationType()) !== false) {
			return substr($type, 0, strpos($type, '/'));
		}
		
		return false;
	}

	protected function _getMbEntity()
	{
		if (($type = $this->getAssociationType()) !== false) {
			return substr($type, strpos($type, '/')+1);
		}
		
		return false;
	}

	protected function _isSingleStoreMode()
	{
		return Mage::app()->isSingleStoreMode();
	}

	public function getStoreId()
	{
		if (!$this->_isSingleStoreMode()) {
			return $this->getRequest()->getParam('store', false);
		}

		$defaultStoreId = Mage::app()
		->getWebsite()
		->getDefaultGroup()
		->getDefaultStoreId();
		
		return $defaultStoreId;
	}

	protected function _initObject()
	{
		if ($this->_getMagentoEntity() === 'product') {
			return $this->_initProduct();
		}
		
		return false;
	}	

	protected function _initProduct()
	{
		if (!Mage::registry('product')) {
			if ($productId = $this->getRequest()->getParam('id')) {
				$product = Mage::getModel('catalog/product')->load($productId);
				
				if ($product->getId()) {
					Mage::register('product', $product);
					return $product;
				}
			}
		}
		
		return false;
	}
}
