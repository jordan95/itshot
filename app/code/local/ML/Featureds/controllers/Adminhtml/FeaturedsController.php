<?php
class ML_Featureds_Adminhtml_FeaturedsController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction() {
		$this->loadLayout()
		->_setActiveMenu('featureds/items')
		->_addBreadcrumb(Mage::helper('featureds')->__('Manage featured'), Mage::helper('featureds')->__('Manage featured'));

		return $this;
	}

	public function indexAction() {
		$this->_initAction()->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('featureds/featured')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('featureds_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('featureds/items');

			$this->_addBreadcrumb(Mage::helper('featureds')->__('Manage featureds'), Mage::helper('featureds')->__('Manage featureds'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('featureds/adminhtml_featureds_edit'))
			->_addLeft($this->getLayout()->createBlock('featureds/adminhtml_featureds_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('featureds')->__('Featured does not exist'));
			$this->_redirect('*/*/');
		}
	}

	public function newAction() {
		$this->_forward('edit');
	}

	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {

			if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
				try {
					/* Starting upload */
					$uploader = new Varien_File_Uploader('image');

					// Any extention would work
					$uploader->setAllowedExtensions(array('gif','png'));
					$uploader->setAllowRenameFiles(true);
					$uploader->setFilesDispersion(false);

					$path = Mage::getBaseDir('media').DS.'ml'.DS.'featureds'.DS;
					$uploader->save($path, $_FILES['image']['name'] );

				} catch (Exception $e) {

				}

				$data['image'] = 'ml'.DS.'featureds'.DS.$_FILES['image']['name'];
			}
			else {
				if(isset($data['image']['delete']) && $data['image']['delete'] == 1)
					$data['image'] = '';
				else
					unset($data['image']);
			}

			$model = Mage::getModel('featureds/featured');
			$model->setData($data)
			->setId($this->getRequest()->getParam('id'));

			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
					->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}

				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('featureds')->__('Featureds was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('featureds')->__('Unable to find Featured to save'));
		$this->_redirect('*/*/');
	}

	public function gridAction()
	{
		$this->_registryObject();
		$this->getResponse()->setBody(
				$this->getLayout()->createBlock('featureds/adminhtml_featureds_edit_tab_product')
				->toHtml()
		);
	}

	protected function _registryObject()
	{
		Mage::register('featureds_data', Mage::getModel('featureds/featured'));
	}

	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('featureds/featured');

				$model->setId($this->getRequest()->getParam('id'))
				->delete();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('featureds')->__('Featureds was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	public function massDeleteAction() {
		$featuredsIds = $this->getRequest()->getParam('featured');
		if(!is_array($featuredsIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('featureds')->__('Please select featured(s)'));
		} else {
			try {
				foreach ($featuredsIds as $featuredsId) {
					$featureds = Mage::getModel('featureds/featured')->load($featuredsId);
					$featureds->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(
				Mage::helper('featureds')->__('Total of %d record(s) were successfully deleted', count($featuredsIds))
				);
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}


	public function exportCsvAction()
	{
		$fileName   = 'featureds.csv';
		$content    = $this->getLayout()->createBlock('featureds/adminhtml_featureds_grid')
		->getCsv();

		$this->_sendUploadResponse($fileName, $content);
	}

	public function exportXmlAction()
	{
		$fileName   = 'featureds.xml';
		$content    = $this->getLayout()->createBlock('featureds/adminhtml_featureds_grid')->getXml();

		$this->_sendUploadResponse($fileName, $content);
	}

	protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
	{
		$response = $this->getResponse();
		$response->setHeader('HTTP/1.1 200 OK','');
		$response->setHeader('Pragma', 'public', true);
		$response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
		$response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
		$response->setHeader('Last-Modified', date('r'));
		$response->setHeader('Accept-Ranges', 'bytes');
		$response->setHeader('Content-Length', strlen($content));
		$response->setHeader('Content-type', $contentType);
		$response->setBody($content);
		$response->sendResponse();
		die;
	}
}
