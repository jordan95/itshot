<?php
class ML_Corekit_Model_System_Config_Source_Environment
{
	public function toOptionArray()
	{
		return array(
				array('value' => 'local', 'label'=>Mage::helper('adminhtml')->__('Local or pre-production')),
				array('value' => 'production', 'label'=>Mage::helper('adminhtml')->__('Production'))
		);
	}
   
}
