<?php
class Onsis_Syncorder_Model_Mysql4_Syncorder_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('syncorder/syncorder');
    }
}
