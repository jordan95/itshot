<?php
/**
 * Order Sync API
 *
 * @category   Local
 * @package    Local_Onsis
 * @author     Raviraj(MS)
 */
class Onsis_Syncorder_Model_Order_Api extends Mage_Sales_Model_Order_Api
{
    protected function _initSyncOrder($orderIncrementId)
    {
        $order = Mage::getModel('sales/order');

        /* @var $order Mage_Sales_Model_Order */

        $order->loadByIncrementId($orderIncrementId);
        return $order;
    }
    
    public function update($orderIncrementId)
    {
		$orderIncrementId = trim($orderIncrementId);
		$order = $this->_initSyncOrder($orderIncrementId);
		if (!$order->getId()) {
			return "order does not exists.";
		}
		$dateTime = Mage::getModel('core/date')->date('Y-m-d H:i:s');
		$orderId = $order->getId();
		
		$objModel = Mage::getModel("syncorder/syncorder");
		$id = $objModel->getOrder($orderIncrementId);
		
		if($id<1)
		{
			$objModel->setOrderId($orderId);
			$objModel->setIncrementId($orderIncrementId);
			$objModel->setStatus(1);
			$objModel->setCreatedTime($dateTime);
			$objModel->setUpdateTime($dateTime);
			$objModel->save();
			return "Created";
		}
		else
		{
			$data = $objModel->load($id);
			$getStatus = $data->getStatus();
			$status = 1;
			if($getStatus==1)
			{
				$status = 2;
			}
			$data->setStatus(1);
			$data->setUpdateTime($dateTime);
			$data->save();
			return "Updated";
		}
	}
	
	public function rewardspoint($orderIncrementId)
	{
		$orderIncrementId = trim($orderIncrementId);
		//get order information
		$order = $this->_initSyncOrder($orderIncrementId);
		if (!$order->getId()) {
			return "order does not exists.";
		}
		$orderId = $order->getId();
		
		//get rewards points assoicated with order
		$rewardsInfo = array();
		$order = Mage::getModel('sales/order')->load($orderId);
		$transfer_array = $order->getAssociatedTransfers()->load()->getData();
		if(count($transfer_array)>0)
		{
			$rewardsArr = array();
			foreach($transfer_array AS $rewards)
			{
				$rewardsInfo["rewards_transfer_id"] = $rewards["rewards_transfer_id"];
				$rewardsInfo["quantity"] = $rewards["quantity"];
				$rewardsInfo["reason_id"] = $rewards["reason_id"];
				$rewardsInfo["comments"] = $rewards["comments"];
				$rewardsInfo["status"] = $rewards["status_id"];
				
				$updated_at = $rewards["updated_at"];
				$last_update = date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(strtotime($updated_at)));
				$rewardsInfo["last_update"] = $last_update;
				$rewardsArr[] = $rewardsInfo;
			}
			return $rewardsArr;
		}
		else
		{
			return "reward points does not exists.";
		}
	}
	public function customerRewardsPoint($email)
	{
		$customerId = 0;
		$customer_email = trim($email);
		$customer = Mage::getModel("customer/customer");
		//$WebsiteId = Mage::app()->getWebsite()->getId();
		$WebsiteId = 1;
		$customer->setWebsiteId($WebsiteId);
		$customer->loadByEmail($customer_email);
		//return $customer->getData();
		//return "customerId=>".$customer->getId()." ### Name=>".$customer->getFirstname();
		
		if($customer->getId())
		{
			$customerId = $customer->getId();
		}
		if($customerId>0)
		{
			$conn = Mage::getSingleton('core/resource')->getConnection('core_read');
                        $tablePrefix = (string)Mage::getConfig()->getTablePrefix();
			$sSQL = "SELECT * FROM {$tablePrefix}rewards_transfer WHERE customer_id=".$customerId;
			$resArr = $conn->fetchAll($sSQL);
			if(count($resArr)>0)
			{
				$rewardsArr = array();
				foreach($resArr as $rewards)
				{
					$rewardsInfo = array();
					$rewardsInfo["rewards_transfer_id"] = $rewards["rewards_transfer_id"];
					$rewardsInfo["quantity"] = $rewards["quantity"];
					$rewardsInfo["reason_id"] = $rewards["reason_id"];
					$rewardsInfo["comments"] = $rewards["comments"];
					$rewardsInfo["status"] = $rewards["status_id"];
					
					//get reference order number
					$orderIncrementId = $this->__getRewardsReference($rewards["rewards_transfer_id"]);
					$rewardsInfo["source_reference_id"] = $rewards["reference_id"];
					$rewardsInfo["order_increment_id"] = $orderIncrementId;
					
					$updated_at = $rewards["updated_at"];
					$last_update = date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(strtotime($updated_at)));
					$rewardsInfo["last_update"] = $last_update;
					$rewardsArr[] = $rewardsInfo;
				}
				return $rewardsArr;
			}
			else
			{
				return "reward points does not exists.";
			}
		}
		else
		{
			return "Customer does not exist.";
		}
	}
	
	protected function __getRewardsReference($rewardsTransferId)
	{
		$orderIncrementId = "";
		$conn = Mage::getSingleton('core/resource')->getConnection('core_read');
                $tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		$refSql = "SELECT reference_id FROM {$tablePrefix}rewards_transfer_reference WHERE rewards_transfer_id=".$rewardsTransferId;
		$refArr = $conn->fetchRow($refSql);
		$reference_id = 0;
		if(isset($refArr["reference_id"]) && $refArr["reference_id"]>0)
		{
			$reference_id = $refArr["reference_id"];
			$order = Mage::getModel('sales/order');
			$order->load($reference_id);
			$orderIncrementId = $order->getIncrementId();
		}
		return $orderIncrementId;
	}
	
	protected function __getCustomerInfo($customerId)
	{
		//$customer_email = trim($email);
		$customer = Mage::getModel("customer/customer");
		//$WebsiteId = Mage::app()->getWebsite()->getId();
		$WebsiteId = 1;
		$customer->setWebsiteId($WebsiteId);
		$customer->load($customerId);
		return $customer->getEmail();
	}
	
	public function rewardsPointList($fromDate=null, $toDate=null, $email=null)
	{
		$conn = Mage::getSingleton('core/resource')->getConnection('core_read');
		$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
		$sSQL = "SELECT * FROM {$tablePrefix}rewards_transfer WHERE rewards_transfer_id IS NOT NULL";
		
		//by default get current date updated points
		if($fromDate=="")
		{
			$fromDate = date("Y-m-d", Mage::getModel('core/date')->timestamp(time()));
		}
		
		//now filter the result set using date params
		if($fromDate!="" && $toDate=="")
		{
			$fromDate = Mage::getModel('core/date')->date($fromDate);
			$sSQL .= " AND updated_at LIKE '".$fromDate."%'";
		}
		else if($fromDate!="" && $toDate!="")
		{
			$fromDate = Mage::getModel('core/date')->date($fromDate);
			$fromDate = $fromDate." 00:00:01";
			
			$toDate = Mage::getModel('core/date')->date($toDate);
			$toDate = $toDate." 23:59:59";
			
			$sSQL .= " AND updated_at >= '".$fromDate."%' AND updated_at <= '".$toDate."'";
		}
		
		//if email passed in param then add customer_id filter
		if($email!="")
		{
			$customerId = 0;
			$customer_email = trim($email);
			$customer = Mage::getModel("customer/customer");
			$WebsiteId = 1;
			$customer->setWebsiteId($WebsiteId);
			$customer->loadByEmail($customer_email);
			
			if($customer->getId())
			{
				$customerId = $customer->getId();
			}
			if($customerId>0)
			{
				$sSQL .= " AND customer_id=".$customerId;
			}
		}
		
		$sSQL .= " ORDER BY updated_at DESC";
		
		$resArr = $conn->fetchAll($sSQL);
		if(count($resArr)>0)
		{
			$rewardsArr = array();
			foreach($resArr as $rewards)
			{
				$rewardsInfo = array();
				$rewardsInfo["rewards_transfer_id"] = $rewards["rewards_transfer_id"];
				
				//customer info
				$rewardsInfo["customer_id"] = $rewards["customer_id"];
				$rewardsInfo["customer_email"] = $this->__getCustomerInfo($rewards["customer_id"]);
				
				$rewardsInfo["quantity"] = $rewards["quantity"];
				$rewardsInfo["reason_id"] = $rewards["reason_id"];
				$rewardsInfo["comments"] = $rewards["comments"];
				$rewardsInfo["status"] = $rewards["status_id"];
				
				//get reference order number
				$orderIncrementId = $this->__getRewardsReference($rewards["rewards_transfer_id"]);
				$rewardsInfo["source_reference_id"] = $rewards["reference_id"];
				$rewardsInfo["order_increment_id"] = $orderIncrementId;
				
				$updated_at = $rewards["updated_at"];
				$last_update = date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(strtotime($updated_at)));
				$rewardsInfo["last_update"] = $last_update;
				$rewardsArr[] = $rewardsInfo;
			}
			return $rewardsArr;
		}
		else
		{
			return "reward points does not exists.";
		}
	}
	
	public function removeOrderItem($orderIncrementId, $sku, $productOptions=null)
	{
		$orderIncrementId = trim($orderIncrementId);
		$sku = trim($sku);
		
		$order = $this->_initSyncOrder($orderIncrementId);
		if (!$order->getId()) {
			return "order does not exists.";
		}
		$orderId = $order->getId();
		
		echo "orderId=>".$orderId;
		echo "<br>orderIncrementId=>".$orderIncrementId;
		
		//$order = Mage::getModel('sales/order')->load(31346);//Change order id accordingly

		$base_grand_total = $order->getBaseGrandTotal();
		$base_subtotal = $order->getBaseSubtotal();
		$base_tax_amount = $order->getBaseTaxAmount();
		$base_subtotal_incl_tax = $order->getBaseSubtotalInclTax();

		$grand_total = $order->getGrandTotal();
		$subtotal = $order->getSubtotal();
		$tax_amount = $order->getTaxAmount();
		$subtotal_incl_tax = $order->getSubtotalInclTax();

		$total_item_count = $order->getTotalItemCount();

		$transfer_array = $order->getAssociatedTransfers()->load()->getData();
		$transfer_id = $transfer_array[0]['rewards_transfer_id'];

		//get order items
		$items = $order->getAllItems();
		//$items = $order->getAllVisibleItems();
		$itemFound = false;
		
		echo "<br>Get Order Items:<br>";
		foreach($items as $item)
		{
			$productSku = $item->getSku();
			if($productSku==$sku)
			{
				echo "<br>SKU==>".$productSku;
				echo " ### Qty Ordered=>".$item->getQtyOrdered();
				$itemFound = true;
				$options = $item->getProductOptions();
				$orderItmOptArr = array();
				//check if option exists with item
				if(count($options)>0)
				{
					$customOptions = $options['options'];
					if(!empty($customOptions))
					{
						foreach ($customOptions as $option)
						{	    
							$optionTitle = $option['label'];
							$optionId = $option['option_id'];
							$optionType = $option['option_type'];
							$optionValue = $option['value'];
							echo "<br>optionTitle=>".$optionTitle." ## optionValue=>".$optionValue;
							echo " ## optionId=>".$optionId." ## optionType=>".$optionType;
							$orderItmOptArr[$optionTitle] = $optionValue;
						}
					}
				}//else if
			}
		}//end foreach
		
		//return error if requested item does not exists in order info
		if($itemFound===false)
		{
			echo " Item ".$sku." does not associated with order#".$orderIncrementId;
		}
				
		//return error if product option exists in order info but not present in param
		if(count($orderItmOptArr)>0 && !is_array($productOptions))
		{
			echo " Please specify custom options for Item ".$sku;
		}
		echo "<br>Order item options<br>";
		
		print_r($orderItmOptArr);
		
		echo "<br>Custom options diff<br>";
		if (is_array($productOptions) && count($orderItmOptArr)>0) 
		{
			$optionDiff = array_diff_assoc($orderItmOptArr,$productOptions);
			print_r($optionDiff);
			if(count($optionDiff)>0)
			{
				echo "Product options did not match.";
			}
		}
		
		exit;
		
		foreach($items as $item) {
			if($item->getParentItemId() == '' || $item->getParentItemId() == null) {
				$product_sku = $item->getSku();
				if($product_sku == '890830') {//Change ordered item sku accordingly
					//remove item from order
					$item_base_price = $item->getBasePrice();
					$item_base_tax_amount = $item->getBaseTaxAmount();
					
					$item_price = $item->getPrice();
					$item_tax_amount = $item->getTaxAmount();
					
					$points = ceil($item_base_price * 0.03 + $item_base_price*3);
					
					$item->delete();
					
					//Remove deleted item points
					$point_collection = Mage::getModel('rewards/transfer')->load($transfer_id);
					$total_points = $point_collection->getQuantity();
					$point_collection->setQuantity($total_points-$points);
					$point_collection->save();
					
					//remove item price from total price of order
					$order->setBaseGrandTotal($base_grand_total-($item_base_price+$item_base_tax_amount));
					$order->setBaseSubtotal($base_subtotal-$item_base_price);
					$order->setBaseTaxAmount($base_tax_amount-$item_base_tax_amount);
					$order->setBaseSubtotalInclTax($base_subtotal_incl_tax-($item_base_price+$item_base_tax_amount));
					
					$order->setGrandTotal($grand_total-($item_price+$item_tax_amount));
					$order->setSubtotal($subtotal-$item_price);
					$order->setTaxAmount($tax_amount-$item_tax_amount);
					$order->setSubtotalInclTax($subtotal_incl_tax-($item_price+$item_tax_amount));
					
					$order->setTotalItemCount($total_item_count-1);
					
					$order->save();
				}
			}
		}
	}
    
} // Class Onsis_Syncorder_Model_Order_Api End
