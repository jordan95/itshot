<?php
class Onsis_Syncorder_Model_Syncorder extends Mage_Core_Model_Abstract
{
	public function _construct()
    {
        parent::_construct();
        $this->_init('syncorder/syncorder');
    }
    
	public function prepareData()
    {
        
    }
    
	public function load($id, $field=null)
    {
        $this->_validated = false;
        $this->_isValid = false;

        parent::load($id);

        $this->prepareData();

        return $this;
    }
    
    public function getOrder($incrementId)
    {
		$id = 0;
		$objModel = Mage::getModel("syncorder/syncorder");
		$data = $objModel->getCollection()->addFieldToFilter("increment_id",$incrementId);
		$dataArr = $data->getData();
		if(isset($dataArr[0]["id"]) && $dataArr[0]["id"]>0)
		{
			$id = $dataArr[0]["id"];
		}		
		return $id;
	}
	
	public function checkOrder($incrementId)
    {
		$found = false;
		$objModel = Mage::getModel("syncorder/syncorder");
		$data = $objModel->getCollection()->addFieldToFilter("increment_id",$incrementId)->addFieldToFilter("status",1);
		$dataArr = $data->getData();
		if(isset($dataArr[0]["id"]) && $dataArr[0]["id"]>0)
		{
			$found = true;
		}		
		return $found;
	}		
}
