<?php
class Onsis_Syncorder_Block_Syncorder extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getSyncorder()     
     { 
        if (!$this->hasData('syncorder')) {
            $this->setData('syncorder', Mage::registry('syncorder'));
        }
        return $this->getData('syncorder');        
    }
	
}
