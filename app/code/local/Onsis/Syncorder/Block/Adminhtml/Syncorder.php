<?php
class Onsis_Syncorder_Block_Adminhtml_Syncorder extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_syncorder';
    $this->_blockGroup = 'syncorder';
    $this->_headerText = Mage::helper('syncorder')->__('Adempiere Sync Order List');
    //$this->_addButtonLabel = Mage::helper('syncorder')->__('Add New Order');
    parent::__construct();
  }
}
