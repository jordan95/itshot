<?php
class Onsis_Syncorder_Block_Adminhtml_Syncorder_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('syncorder_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('syncorder')->__('Sync Order Information'));
  }

  protected function _beforeToHtml()
  {
     $this->addTab('general', array(
                'label'   => $this->__('General'),
                'title'   => $this->__('General'),
                'content' => $this->getLayout()->createBlock('syncorder/adminhtml_syncorder_edit_tab_general')->toHtml()
            )
        );
     
      return parent::_beforeToHtml();
 
  }
  
}
