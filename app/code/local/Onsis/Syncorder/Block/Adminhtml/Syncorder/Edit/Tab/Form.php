<?php
class Onsis_Syncorder_Block_Adminhtml_Syncorder_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	public function __construct()
    {
        parent::__construct();
        $this->setTemplate('syncorder/syncorder.phtml');
    }
	
	protected function _getDefaultStoreId()
    {
        return Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID;
    }   
    
	public function getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store');
        return Mage::app()->getStore($storeId);
    }   
    
	public function getOrderId()
	{ 
	    return $this->htmlEscape(Mage::registry('syncorder_data')->getOrderId());
	}
	
	public function getIncrementId()
	{
		    return $this->htmlEscape(Mage::registry('syncorder_data')->getIncrementId());
	}
	
	public function getStatus()
	{
		    return $this->htmlEscape(Mage::registry('syncorder_data')->getStatus());
	}
}
