<?php
class Onsis_Syncorder_Block_Adminhtml_Syncorder_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('syncorderGrid');
      $this->setDefaultSort('id');
      $this->setDefaultDir('DESC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('syncorder/syncorder')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('id', array(
          'header'    => Mage::helper('syncorder')->__('Id'),
          'align'     =>'center',
          'width'     => '80px',
          'index'     => 'id',
      ));

      $this->addColumn('increment_id', array(
          'header'    => Mage::helper('syncorder')->__('Order #'),
          'align'     =>'left',
          'index'     => 'increment_id',
          'width'     => '100px',
      ));
	 

	  $this->addColumn('status', array(
          'header'    => Mage::helper('syncorder')->__('Sync Status'),
          'align'     => 'center',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Sync',
              2 => 'Not Sync',
          ),
      ));
      
      $this->addColumn('created_time', array(
            'header'    => Mage::helper('cms')->__('Date Created'),
            'index'     => 'created_time',
            'width'     => '180px',
            'type'      => 'datetime',
        ));

        $this->addColumn('update_time', array(
            'header'    => Mage::helper('cms')->__('Last Modified'),
            'index'     => 'update_time',
            'width'     => '180px',
            'type'      => 'datetime',
        ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('syncorder')->__('Action'),
                'align'     => 'center',
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('syncorder')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('syncorder')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('syncorder')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('syncorder');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('syncorder')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('syncorder')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('syncorder/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('syncorder')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('syncorder')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}
