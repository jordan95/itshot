<?php
class Onsis_Syncorder_Block_Adminhtml_Syncorder_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'syncorder';
        $this->_controller = 'adminhtml_syncorder';
        
        $this->_updateButton('save', 'label', Mage::helper('syncorder')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('syncorder')->__('Delete'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('syncorder_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'syncorder_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'syncorder_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('syncorder_data') && Mage::registry('syncorder_data')->getId() ) {
		
            return Mage::helper('syncorder')->__("Edit Order Sync Status '%s'", $this->htmlEscape(Mage::registry('syncorder_data')->getIncrementId()));
        } else {
            return Mage::helper('syncorder')->__('Add Sync Order');
        }
    }
	
	
	
}
