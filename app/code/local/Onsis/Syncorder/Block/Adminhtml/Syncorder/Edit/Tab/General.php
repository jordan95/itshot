<?php
class Onsis_Syncorder_Block_Adminhtml_Syncorder_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm() 
    {
        $data = Mage::registry('syncorder_data');

        if (is_object($data)) $data = $data->getData();

        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('general', array('legend' => $this->__('Sync Order')));

        $fieldset->addField('increment_id', 'text', array(
                'label'    => $this->__('Order #'),
                'name'     => 'increment_id',
                'required' => true,
            ));

       $fieldset->addField('order_id', 'text', array(
                'label'    => $this->__('Magento Order Id'),
                'name'     => 'order_id',
                'required' => false,
            ));
	    $fieldset->addField('status', 'select', array(
                'label'  => $this->__('Status'),
                'name'   => 'status',
                'values' => Onsis_Syncorder_Model_Status::getOptionArray(),
            ));
        $form->setValues($data);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
