<?php
class Mgtoxp_Menuadminpro_Adminhtml_MenuadminproController extends Mage_Adminhtml_Controller_Action
{
	protected function _initItem($getRootInstead = false)
	{

		$itemId = (int) $this->getRequest()->getParam('id',false);
		$storeId    = (int) $this->getRequest()->getParam('store');
		$item = Mage::getModel('menuadminpro/menuadminpro');
		$item->setStoreId($storeId);

		if ($itemId) {
			$item->load($itemId);
			if ($storeId) {
				$rootId = 1; //Mage::app()->getStore($storeId)->getRootItemId();
				if (!in_array($rootId, $item->getPathIds())) {
					// load root item instead wrong one
					if ($getRootInstead) {

						$item->load($rootId);
					}
					else {
						$this->_redirect('*/*/', array('_current'=>true, 'id'=>null));
						return false;
					}
				}
			}
		}

		if ($activeTabId = (string) $this->getRequest()->getParam('active_tab_id')) {
			Mage::getSingleton('admin/session')->setActiveTabId($activeTabId);
		}


		Mage::register('item', $item);
		Mage::register('current_item', $item);
		Mage::getSingleton('cms/wysiwyg_config')->setStoreId($this->getRequest()->getParam('store'));
		return $item;
	}

	public function indexAction()
	{
		$this->_forward('edit');


	}

	public function addAction()
	{
		Mage::getSingleton('admin/session')->unsLastEditedItem();
		$this->_forward('edit');
	}

	public function editAction()
	{
		$params['_current'] = true;
		$redirect = false;

		$storeId = (int) $this->getRequest()->getParam('store');

		$parentId = (int) $this->getRequest()->getParam('parent');
		$_prevStoreId = Mage::getSingleton('admin/session')->getLastViewedStore(true);

		if (!empty($_prevStoreId) && !$this->getRequest()->getQuery('isAjax')) {
			$params['store'] = $_prevStoreId;
			$redirect = true;
		}

		$itemId = (int) $this->getRequest()->getParam('id');

		$_prevItemId = Mage::getSingleton('admin/session')->getLastEditedItem(true);


		if ($_prevItemId && !$this->getRequest()->getQuery('isAjax') && !$this->getRequest()->getParam('clear')) {
			$this->getRequest()->setParam('id',$_prevItemId);
		}

		if ($redirect) {
			$this->_redirect('*/*/edit', $params);
			return;
		}
		if ($storeId && !$itemId && !$parentId) {
			$store = Mage::app()->getStore($storeId);
			$_prevItemId = 1; //(int) $store->getRootItemId();
			$this->getRequest()->setParam('id', $_prevItemId);
		}

		if (!($item = $this->_initItem(true))) {
			return;
		}

		$this->_title($itemId ? $item->getName() : $this->__('New Item'));

		/**
		 * Check if we have data in session (if duering category save was exceprion)
		*/
		$data = Mage::getSingleton('adminhtml/session')->getItemData(true);
		if (isset($data)) {
			$item->addData($data);
		}

		/**
		 * Build response for ajax request
		 */
		if ($this->getRequest()->getQuery('isAjax')) {
			// prepare breadcrumbs of selected category, if any
			$breadcrumbsPath = $item->getPath();
			if (empty($breadcrumbsPath)) {
				// but if no item, and it is deleted - prepare breadcrumbs from path, saved in session
				$breadcrumbsPath = Mage::getSingleton('admin/session')->getDeletedPath(true);
				if (!empty($breadcrumbsPath)) {
					$breadcrumbsPath = explode('/', $breadcrumbsPath);
					// no need to get parent breadcrumbs if deleting item level 1
					if (count($breadcrumbsPath) <= 1) {
						$breadcrumbsPath = '';
					}
					else {
						array_pop($breadcrumbsPath);
						$breadcrumbsPath = implode('/', $breadcrumbsPath);
					}
				}
			}


			Mage::getSingleton('admin/session')->setLastViewedStore($this->getRequest()->getParam('store'));
			Mage::getSingleton('admin/session')->setLastEditedItem($item->getId());
			$this->loadLayout();

			$breadcrumbsPath = '';
			$eventResponse = new Varien_Object(array(
					'content' => $this->getLayout()->getBlock('menuadminpro.edit')->getFormHtml()
					. $this->getLayout()->getBlock('menuadminpro.tree')
					->getBreadcrumbsJavascript($breadcrumbsPath, 'editingItemBreadcrumbs'),
					'messages' => $this->getLayout()->getMessagesBlock()->getGroupedHtml(),
			));

			Mage::dispatchEvent('menuadminpro_prepare_ajax_response', array(
			'response' => $eventResponse,
			'controller' => $this
			));

			$this->getResponse()->setBody(
					Mage::helper('core')->jsonEncode($eventResponse->getData())
			);

			return;
		}

		$this->loadLayout();
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true)->setContainerCssClass('item-content');

		$this->_addBreadcrumb(Mage::helper('catalog')->__('Manage Menues'),
				Mage::helper('catalog')->__('Manage Menu Items')
		);

		$block = $this->getLayout()->getBlock('catalog.wysiwyg.js');
		if ($block) {
			$block->setStoreId($storeId);
		}

		$this->renderLayout();
	}

	public function wysiwygAction()
	{
		$elementId = $this->getRequest()->getParam('element_id', md5(microtime()));
		$storeId = $this->getRequest()->getParam('store_id', 0);
		$storeMediaUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);

		$content = $this->getLayout()->createBlock('adminhtml/catalog_helper_form_wysiwyg_content', '', array(
				'editor_element_id' => $elementId,
				'store_id'          => $storeId,
				'store_media_url'   => $storeMediaUrl,
		));

		$this->getResponse()->setBody($content->toHtml());
	}

	public function saveAction()
	{

		if (!$item = $this->_initItem()) {
			return;
		}

		$refreshTree = 'false';
		if ($post = $this->getRequest()->getPost()) {
			$data = $post['general'];
			$data['menuadminpro_products'] = $post['menuadminpro_products'];
			$item->addData($data);

			if (!$item->getId()) {
				$parentId = $this->getRequest()->getParam('parent');
				if (!$parentId) {
					if ($storeId) {
						$parentId = Mage::app()->getStore($storeId)->getRootItemId();
					}
					else {
						$parentId = Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID;
					}
				}
				$parentItem = Mage::getModel('menuadminpro/menuadminpro')->load($parentId);
				$item->setPath($parentItem->getPath());
			}
			$item->setData("use_post_data_config", $this->getRequest()->getPost('use_config'));

			try {
				$item->unsetData('use_post_data_config');
				$item->save();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('catalog')->__('The item has been saved.'));
				$refreshTree = 'true';
			}
			catch (Exception $e){
				$this->_getSession()->addError($e->getMessage())
				->setItemData($data);
				$refreshTree = 'false';
			}
			$this->_redirect('*/*/index', array('id'=>$item->getId()));
		}else{
			$this->_redirect('*/*/index');
		}

	}

	public function deleteAction()
	{
		if ($id = (int) $this->getRequest()->getParam('id')) {
			try {
				$item = Mage::getModel('menuadminpro/menuadminpro')->load($id);
				if($item->getIsDeleteable()){
					Mage::getSingleton('adminhtml/session')->addError(Mage::helper('catalog')->__('The item is not deleteable.'));
					$this->getResponse()->setRedirect($this->getUrl('*/*/edit', array('_current'=>true)));
					return;
				}
				Mage::getSingleton('admin/session')->setDeletedPath($item->getPath());

				$item->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('catalog')->__('The item has been deleted.'));
				$this->_redirect('*/*/index');
			}
			catch (Mage_Core_Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id'=>$id));
				return;
			}
			catch (Exception $e){
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('catalog')->__('An error occurred while trying to delete the item.'));
				$this->_redirect('*/*/edit', array('id'=>$id));
				return;
			}
		}
		$this->_redirect('*/*/index');
	}

	public function moveAction()
	{

		$item = $this->_initItem();
		if (!$item) {
			$this->getResponse()->setBody(Mage::helper('catalog')->__('Item move error'));
			return;
		}
		/**
		 * New parent item identifier
		 */
		$parentNodeId = $this->getRequest()->getPost('pid', false);
		/**
		 * Item id after which we have put our item
		*/
		$prevNodeId = $this->getRequest()->getPost('aid', false);

		try {
			$item->move($parentNodeId, $prevNodeId);
			$this->getResponse()->setBody("SUCCESS");
		}
		catch (Mage_Core_Exception $e) {
			$this->getResponse()->setBody($e->getMessage());
		}
		catch (Exception $e){
			$this->getResponse()->setBody(Mage::helper('catalog')->__('Item move error'.$e));
			Mage::logException($e);
		}
	}

	public function itemsJsonAction()
	{

		if ($this->getRequest()->getParam('expand_all')) {
			Mage::getSingleton('admin/session')->setIsTreeWasExpanded(true);
		} else {
			Mage::getSingleton('admin/session')->setIsTreeWasExpanded(false);
		}
		if ($itemId = (int) $this->getRequest()->getPost('id')) {
			$this->getRequest()->setParam('id', $itemId);
			if (!$item = $this->_initItem()) {
				return;
			}
			$this->getResponse()->setBody(
					$this->getLayout()->createBlock('menuadminpro/adminhtml_menuadminpro_tree')
					->getTreeJson($item)
			);
		}
	}

	public function gridAction()
	{
		if (!$item = $this->_initItem(true)) {
			return;
		}
		$this->getResponse()->setBody(
				$this->getLayout()->createBlock('menuadminpro/adminhtml_menuadminpro_tab_product', 'item.product.grid')
				->toHtml()
		);
	}

	public function treeAction()
	{
		$storeId = (int) $this->getRequest()->getParam('store');
		$itemId = (int) $this->getRequest()->getParam('id');

		if ($storeId) {
			if (!$itemId) {
				$store = Mage::app()->getStore($storeId);
				$rootId = $store->getRootItemId();
				$this->getRequest()->setParam('id', $rootId);
			}
		}

		$item = $this->_initItem(true);

		$block = $this->getLayout()->createBlock('menuadminpro/adminhtml_menuadminpro_tree');
		$root  = $block->getRoot();
		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
				'data' => $block->getTree(),
				'parameters' => array(
						'text'        => $block->buildNodeName($root),
						'draggable'   => false,
						'allowDrop'   => ($root->getIsVisible()) ? true : false,
						'id'          => (int) $root->getId(),
						'expanded'    => (int) $block->getIsWasExpanded(),
						'store_id'    => (int) $block->getStore()->getId(),
						'item_id' => (int) $item->getId(),
						'root_visible'=> (int) $root->getIsVisible()
				))));
	}

	/**
	 * Check if admin has permissions to visit related pages
	 *
	 * @return boolean
	 */
	protected function _isAllowed()
	{
		return Mage::getSingleton('admin/session')->isAllowed('adminhtml/menuadminpro');
	}
}
