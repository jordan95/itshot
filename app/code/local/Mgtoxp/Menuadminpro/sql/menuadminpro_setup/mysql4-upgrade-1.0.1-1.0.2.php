<?php
$installer = $this;

$installer->startSetup();

$table = $installer->getTable('menuadminproitem');

$installer->getConnection()->addColumn($table,'item_description',array('type'=>Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'=>255,
    'nullable'=>false,
    'comment'=>'Menu Item Description'));

$installer->endSetup();