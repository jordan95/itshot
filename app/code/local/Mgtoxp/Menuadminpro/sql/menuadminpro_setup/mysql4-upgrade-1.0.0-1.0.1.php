<?php
$installer = $this;

$installer->startSetup();


$table = $installer->getConnection()
    ->newTable($installer->getTable('menuadminproitem_store'))
    ->addColumn('item_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'primary'   => true,
        ), 'Item ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store ID')
    ->addColumn('region', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
    ), 'Region')
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
    ), 'Is Active')
    ->addColumn('styles', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
    ), 'Styles')
    ->addColumn('stylesresponsive', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
    ), 'Styles responsive')
    ->addColumn('itemsperrow', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
       'nullable'  => false,
    ), 'Items per Row')
    ->addColumn('distributeitems', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
    		'nullable'  => false,
    ), 'Distribute items in columns')
    ->addColumn('customcss', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
    ), 'Custom CSS')
    ->addColumn('is_responsive', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
       'nullable'  => false,
    ), 'Is Responsive')
    ->addColumn('menubutton', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
    		'nullable'  => false,
    ), 'Use Menu Button')
    ->addColumn('menutitle', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
    		'nullable'  => false,
    ), 'Menu Title')
    ->addIndex($installer->getIdxName('menuadminproitem_store', array('store_id')),
        array('store_id'))
    ->addForeignKey($installer->getFkName('menuadminproitem_store', 'item_id', 'menuadminproitem', 'item_id'),
        'item_id', $installer->getTable('menuadminproitem'), 'item_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('menuadminproitem_store', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Menuadminpro To Store Linkage Table');
$installer->getConnection()->createTable($table);

$installer->run("

		insert into {$this->getTable('menuadminproitem_store')}(`item_id`,`store_id`,`region`,`is_active`,`itemsperrow`, `is_responsive`) values (2,0,'top',1,4,0);

");

$installer->endSetup();