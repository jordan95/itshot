<?php
$installer = $this;

$installer->startSetup();

/**
 * Create table 'menuadmin'
 */
$table = $installer->getConnection()
->newTable($installer->getTable('menuadminproitem'))
->addColumn('item_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'identity'  => true,
		'nullable'  => false,
		'primary'   => true,
), 'Item ID')
->addColumn('parent_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
), 'Parent ID')
->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'unsigned'  => true,
		'nullable'  => false,
), 'Store ID')
->addColumn('region', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
), 'Region')
->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
), 'Name')
->addColumn('itemsperrowsub', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
), 'Items per row sub')
->addColumn('distributeitemssub', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
), 'Distribute items in columns')
/*
->addColumn('dropdownwidth', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
), 'Dropdown Width')
*/
->addColumn('type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
), 'Type')
->addColumn('link', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
), 'Link')
->addColumn('linkpage', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
), 'Link to CMS Page')
->addColumn('linkcategory', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
), 'Link to Category')
->addColumn('cmsblock', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
), 'CMS Block')
->addColumn('cmsblockposition', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
), 'CMS Block Position')
->addColumn('cmsblockpercent', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
), 'CMS Block Percent')
->addColumn('productsrow', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
), 'Products per Row')
->addColumn('productstotal', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
), 'Qty Products')
->addColumn('productblockposition', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
), 'Products Block Position')
->addColumn('productblockpercent', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
), 'Products Block Percent')
->addColumn('menuadminpro_products', Varien_Db_Ddl_Table::TYPE_TEXT, 512, array(
		'nullable'  => false,
), 'Products')
->addColumn('header', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
), 'Submenu Header')
->addColumn('footer', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
), 'Submenu Footer')
->addColumn('cssclass', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
'nullable'  => false,
), 'Css class')
->addColumn('target', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
'nullable'  => false,
), 'Target')
->addColumn('position', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
'nullable'  => false,
), 'Position')
->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
'nullable'  => false,
), 'Is Active')
->addColumn('is_readonly', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
), 'Is Read Only')
->addColumn('level', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
), 'Level')
->addColumn('path', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
), 'Path')
->addColumn('children_count', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
), 'Children count')
->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Creation Time')
->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Modification Time')
->setComment('Menu Admin Pro Table');

$installer->getConnection()->createTable($table);


$installer->run("

insert into {$this->getTable('menuadminproitem')}(`item_id`,`parent_id`,`store_id`,`region`,`name`,`type`,`link`,`cssclass`,`target`,`position`,`is_active`,`level`,`path`,`children_count`,`creation_time`,`update_time`) values (1,0,1,'','Root',1,'','','',0,0,0,'1',1,NOW(),NOW());

insert into {$this->getTable('menuadminproitem')}(`item_id`,`parent_id`,`store_id`,`region`,`name`,`type`,`link`,`cssclass`,`target`,`position`,`is_active`,`level`,`path`,`children_count`,`creation_time`,`update_time`) values (2,1,1,'top','Root Top Menu',1,'','','',1,1,1,'1/2',0,NOW(),NOW());

");


$installer->endSetup();