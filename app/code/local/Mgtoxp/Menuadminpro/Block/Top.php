<?php
class Mgtoxp_Menuadminpro_Block_Top extends Mgtoxp_Menuadminpro_Block_Abstract
{
    const CACHE_TAG = 'adminpro_menu';

    protected function _construct()
    {
        $this->addData(array(
            'cache_lifetime'    => false,
            'cache_tags'        => array(self::CACHE_TAG, Mage_Core_Model_Store_Group::CACHE_TAG),
        ));
    }

    protected function _toHtml()
    {
        return parent::_toHtml();
    }

 	public function hasHome(){
    	return true;
    }

}