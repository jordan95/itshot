<?php
class Mgtoxp_Menuadminpro_Block_Products extends Mage_Catalog_Block_Product_Abstract
{
    protected $_productsCount = null;

    const DEFAULT_PRODUCTS_COUNT = 5;

    public function setItemProductCollection($ids)
    {
    	$collection = Mage::getResourceModel('catalog/product_collection');
    	$collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
    	$collection = $this->_addProductAttributesAndPrices($collection)
    	->addStoreFilter()
    	->addAttributeToFilter('entity_id', array('in'=>$ids))
    	->setPageSize($this->getProductsCount());
    	
    	$this->setProductCollection($collection);
    }

    public function setProductsCount($count)
    {
        $this->_productsCount = $count;
        return $this;
    }

    public function getProductsCount()
    {
        if (null === $this->_productsCount) {
            $this->_productsCount = self::DEFAULT_PRODUCTS_COUNT;
        }
        return $this->_productsCount;
    }
}
