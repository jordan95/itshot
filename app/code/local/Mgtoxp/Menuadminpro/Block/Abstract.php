<?php
class Mgtoxp_Menuadminpro_Block_Abstract extends Mage_Core_Block_Template
{
	protected $_categoryInstance = null;
	protected $_currentCategoryKey;
	protected $_itemLevelPositions = array();
	protected $outItemContinuous = array();
	protected $outItemInlineContinuous = array();
	protected $_columnsDefault = '';

	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }

	public function getMenuadmin()
	{
        if (!$this->hasData('menuadminpro')) {
            $this->setData('menuadminpro', Mage::registry('menuadminpro'));
        }
        return $this->getData('menuadminpro');
    }

    public function getCurrentStoreId()
    {
        return Mage::app()->getStore()->getId();
    }

	public function getChildrens($pid=0, $region){
		$out = array();
        $collection = Mage::getModel('menuadminpro/menuadminpro')->getCollection()
        	->addFieldToFilter('parent_id', array('in'=>$pid) )
			->addFieldToFilter('is_active', array('in'=>'1') )
			->setOrder('position', 'asc');

		foreach ($collection as $item){
			$out[] = $item->getData();
		}
		return $out;
	}

	public function hasChildrens($pid=0, $region){
        $collection = Mage::getModel('menuadminpro/menuadminpro')->getCollection()
        	->addFieldToFilter('parent_id', array('in'=>$pid) )
			->addFieldToFilter('is_active', array('in'=>'1') )
			->setOrder('position', 'asc')
			->load();
		if($collection->count() > 0){
			return true;
		}
		return false;
	}


	public function getMenuData($region='top'){
		$store_id = $this->getCurrentStoreId();
		$root_item = Mage::getModel('menuadminpro/menuadminpro')->getRootStoreItem($store_id, $region);
		if(empty($root_item['settings'])){
			$root_item = Mage::getModel('menuadminpro/menuadminpro')->getRootStoreItem(0, $region);
		}

		return $root_item;
	}

	public function renderMenu($region='top')
	{
		$store_id = $this->getCurrentStoreId();
		$root_item = Mage::getModel('menuadminpro/menuadminpro')->getRootStoreItem($store_id, $region);
		$out = array();
		if(isset($root_item['stores']) && !empty($root_item['stores']) ){
			foreach($root_item['stores'] as $item){
				$out = $this->drawItem($region, $item);
			}
		}
		return $out;
	}

	/* render first menu level */
	public function drawMenu($region='top', $pid=0, $cmspercent=0, $productpercent=0, $itemsperrow=5){
        if(empty($this->_columnsDefault)):
			$this->_columnsDefault = $itemsperrow;
		endif;
		$debug_item = 39444444444444444; //9 buy 4 categories
		$html_wide = '';
		$html_small = '';
		$items = $this->getChildrens($pid, $region);
		if (empty($items)) {
			$html['wide'] = $html_wide;
			$html['small'] = $html_small;
			return $html;
		}
		foreach ($items as $k => $item){
			if(isset($item['itemsperrowsub'])  && !empty($item['itemsperrowsub'])):
				if($item['itemsperrowsub'] == 0):
					$item['itemsperrowsub'] = 1;
				endif;
				$itemsperrow = $item['itemsperrowsub'];
			else:
				$itemsperrow = $this->_columnsDefault;
			endif;

			if($item['type'] == 4){
				$cmsblock_percent = $productblock_percent = 0;

				if(($item['cmsblockposition'] == 'right' || $item['cmsblockposition'] == 'left') && !empty($item['cmsblockpercent'])):
					$cmsblock_percent = $item['cmsblockpercent'];
				endif;

				if($item['menuadminpro_products'] != '' && $item['productstotal'] > 0 && !empty($item['productblockpercent'])):
					$productblock_percent = $item['productblockpercent'];
				endif;

				$width_rows_menu = 100 - $cmsblock_percent - $productblock_percent;
				$childrens = $this->renderCategoriesMenuHtml(0 ,$itemsperrow, $item['distributeitemssub']);

				if($item['distributeitemssub']):

					if (!empty($childrens['wideinlinecontinuos']) && is_array($childrens['wideinlinecontinuos'])) {
						$html_wide2 = '';
						foreach($childrens['wideinlinecontinuos'] as $k => $child):
							if(isset($child['top'])):
								$html_wide2.= $child['top'];
							elseif(isset($child['sub'])):
								if(isset($childrens['wideinlinecontinuos'][$k-1]['top'])):
									$html_wide2.= '<div class="sub-nav full-width">';
									$html_wide2.= '<div class="sub-nav-group" style="width:'.$width_rows_menu.'%;">';
									$html_wide2.= '<div class="sub-nav-row">';
								endif;

								$cc = 0;
								$_collectionSize = count($child['sub']);
								$width_column = 100 / $itemsperrow;
								$percolumn = ceil($_collectionSize / $itemsperrow);

								foreach($child['sub'] as $sub):
									if($cc++%$percolumn==0):
										$html_wide2.= '<ul class="sub-nav-column" style="width:'.$width_column.'%;">';
									endif;

									$html_wide2.= $sub;
									if($cc%$percolumn==0 || $cc==$_collectionSize):
										$html_wide2.= '</ul>';
									endif;
								endforeach;

								if(!isset($childrens['wideinlinecontinuos'][$k+1]) || isset($childrens['wideinlinecontinuos'][$k+1]['top'])):
									$html_wide2.= '</div>';
									$html_wide2.= '</div>';
									$html_wide2.= '</div>';
								endif;
							endif;

						endforeach;

					}
					//$this->outItemContinuous = array();

					if(isset($html_wide2) && !empty($html_wide2)):
						$html_wide.= $html_wide2;
					endif;
				else:
					$html_wide.= $childrens['wideinline'];
				endif;

				$html_small.= $childrens['small'];
			}else{
				$html_wide.= '<li';
				$html_small.= '<li';

				$html_wide.= ' class="menu-level-'.($item['level']-1);
				$html_small.= ' class="menu-level-'.($item['level']-1);

				if(!empty($item['cssclass'])){
					$html_wide.= ' '.$item['cssclass'];
					$html_small.= ' '.$item['cssclass'];
				}
				$html_wide.= '">'."\n";
				$html_small.= '">'."\n";

				$link = $this->drawLink($item);

				$html_wide.= $link;
				$html_small.= $link;

				$hasChildrens = $this->hasChildrens($item['item_id'], $region);
				if($hasChildrens || $item['type'] == 6 || $item['cmsblock'] != '' || $item['menuadminpro_products'] != '' || $item['header'] != '' || $item['footer'] != ''){
					/* dropdown menu start here*/
					$class_debug = '';
					if($item['item_id'] == $debug_item):
						$class_debug = ' dropdown-menu-debug';
					endif;

					$html_wide.= '<div class="sub-nav full-width'.$class_debug.'">'."\n";
                    $html_wide.= '<div class="sub-nav-inner">'."\n";
					$html_small.= '<ul class="sub-nav-small">'."\n";

					/* dropdown top */
					if($item['header'] != ''){
						$html_wide.= '<div class="sub-nav-row sub-nav-header">'.$item['header'].'</div>';
					}

					if($item['cmsblock'] != '' && $item['cmsblockposition'] == 'top'){
						$html_wide.= '<div class="sub-nav-row sub-nav-cmsblock-top">'.$this->getCmsBlockHtml($item).'</div>';
					}

					/* dropdown left */
					if($item['cmsblock'] != '' && $item['cmsblockposition'] == 'left'){
						$html_wide.= '<div class="sub-nav-group" style="width:'.$item['cmsblockpercent'].'%;">';
						$html_wide.= '<div class="sub-nav-row sub-nav-cmsblock-left">'.$this->getCmsBlockHtml($item).'</div>';
						$html_wide.= '</div>';
					}

					if($item['menuadminpro_products'] != '' && $item['productblockposition'] == 'left'){
						$html_wide.= '<div class="sub-nav-group" style="width:'.$item['productblockpercent'].'%;">';
						$html_wide.= '<div class="sub-nav-row sub-nav-product-left">'.$this->getProductsBlockHtml($item).'</div>';
						$html_wide.= '</div>';
					}

					if($item['type'] == 6 || $hasChildrens):
						$cmsblock_percent = $productblock_percent = 0;

						if(($item['cmsblockposition'] == 'right' || $item['cmsblockposition'] == 'left') && !empty($item['cmsblockpercent'])):
							$cmsblock_percent = $item['cmsblockpercent'];
						endif;

						if($item['menuadminpro_products'] != '' && $item['productstotal'] > 0 && !empty($item['productblockpercent'])):
						$productblock_percent = $item['productblockpercent'];
						endif;

						$width_rows_menu = 100 - $cmsblock_percent - $productblock_percent;
					endif;

					if($item['type'] == 6){
						$this->outItemContinuous = array();
						$childrens = $this->renderCategoriesMenuHtml(0 ,$itemsperrow, $item['distributeitemssub']);
						$html_wide.= '<div class="sub-nav-group" style="width:'.$width_rows_menu.'%;">';
						if($item['distributeitemssub']){
							if (!empty($childrens['widecontinuos']) && is_array($childrens['widecontinuos'])) {
								$html_wide.= '<div class="sub-nav-row">';
								$cc = 0;
								$_collectionSize = count($childrens['widecontinuos']);
								$width_column = 100 / $itemsperrow;
								$percolumn = ceil($_collectionSize / $itemsperrow);

								foreach($childrens['widecontinuos'] as $child):
									if($cc++%$percolumn==0):
										$html_wide.= '<ul class="sub-nav-column" style="width:'.$width_column.'%;">';
									endif;
									$html_wide.= $child;
									if($cc%$percolumn==0 || $cc==$_collectionSize):
										$html_wide.= '</ul>';
									endif;
								endforeach;
								$html_wide.= '</div>';
							}
						}else{
							$html_wide.= $childrens['wide'];
						}
						$html_wide.= '</div>';
						$html_small.= $childrens['small'];
						$this->outItemContinuous = array();
					}

					if ($hasChildrens){
						if($item['distributeitemssub'] == 1){
							/* continuos items */
							$this->outItemContinuous = array();
							$childrens = $this->drawItemContinuous($region, $item['item_id'], $itemsperrow);
							if (!empty($this->outItemContinuous) && is_array($this->outItemContinuous)) {

								$html_wide.= '<div class="sub-nav-group" style="width:'.$width_rows_menu.'%;"><div class="sub-nav-row">';

								$cc = 0;
								$_collectionSize = count($this->outItemContinuous);
								$width_column = 100 / $itemsperrow;
								$percolumn = ceil($_collectionSize / $itemsperrow);
								$html_wide2 = '';
								foreach($this->outItemContinuous as $child):
									if($cc++%$percolumn==0):
										$html_wide.= '<ul class="sub-nav-column" style="width:'.$width_column.'%;">';
									endif;
									$html_wide.= $child;
									if($cc%$percolumn==0 || $cc==$_collectionSize):
										$html_wide.= '</ul>';
									endif;
								endforeach;
								$html_wide.= '</div></div>';
							}
							$html_small.= $childrens['small'];
							$this->outItemContinuous = array();
						}else{
							$childrens = $this->drawItem($region, $item['item_id'], $itemsperrow);
							if (!empty($childrens['wide'])) {
								$html_wide.= '<div class="sub-nav-group" style="width:'.$width_rows_menu.'%;">'.$childrens['wide'].'</div>';
								$html_small.= $childrens['small'];
							}
						}
					}

					/* dropdown right */
					if($item['menuadminpro_products'] != '' && $item['productblockposition'] == 'right'){
						$html_wide.= '<div class="sub-nav-group" style="width:'.$item['productblockpercent'].'%;">';
						$html_wide.= '<div class="sub-nav-row sub-nav-product-right">'.$this->getProductsBlockHtml($item).'</div>';
						$html_wide.= '</div>';
					}

					if($item['cmsblock'] != '' && $item['cmsblockposition'] == 'right'){
						$html_wide.= '<div class="sub-nav-group" style="width:'.$item['cmsblockpercent'].'%;">';
						$html_wide.= '<div class="sub-nav-row sub-nav-cmsblock-right">'.$this->getCmsBlockHtml($item).'</div>';
						$html_wide.= '</div>';
					}

					/* dropdown bottom */
					if($item['cmsblock'] != '' && $item['cmsblockposition'] == 'bottom'){
						$html_wide.= '<div class="sub-nav-row sub-nav-cmsblock-bottom">'.$this->getCmsBlockHtml($item).'</div>';
					}

					if($item['footer'] != ''){
						$html_wide.= '<div class="sub-nav-row sub-nav-footer">'.$item['footer'].'</div>';
					}

                    $html_wide.= '</div>'."\n"; // close sub-nav
					$html_wide.= '</div>'."\n"; // close sub-nav
					$html_small.= '</ul>'."\n";
					/* dropdown menu end here*/
				}

				$html_wide.= '</li>'."\n";
				$html_small.= '</li>'."\n";

			}
		}
		$html['wide'] = $html_wide;
		//Mage::log($html_small);
		$html['small'] = $html_small;
		return $html;
	}

	public function drawLink($item)
    {
        $htmllink = '';
        $link_class = 'item-menu item-menu-' . ($item['level'] - 1);
        switch ($item['type']) {
            case 1:
                if (strstr($item['link'], 'http://')) {
                    $link = $item['link'];
                } else {
                    $link = Mage::getUrl() . $item['link'];
                }
                break;
            case 2:
                $link = Mage::getUrl() . $item['linkpage'];
                break;
            case 3:
                $link = Mage::getUrl() . $item['linkcategory'];
                break;
            case 6:
                if (strstr($item['link'], 'http://')) {
                    $link = $item['link'];
                } elseif ($item['link'] == '') {
                    $link = 'Javascript:void(0);';
                    $link_class = 'item-menu item-categories-link item-menu-' . ($item['level'] - 1);
                } else {
                    $link = Mage::getUrl() . $item['link'];
                }
                break;
            case 7:
                $link = 'Javascript:void(0);';
                $link_class = 'item-menu item-no-link item-menu-' . ($item['level'] - 1);
                break;
            case 8:
                $link = Mage::getUrl();
                $link_class = 'item-menu item-home item-menu-' . ($item['level'] - 1);
                break;
        }
//        if($link == $this->helper('core/url')->getCurrentUrl()){
//            $link_class .= " active";
//        }

        if ($item['target'] == '_blank') {
            $htmllink .= '<a href="' . $link . '" target="_blank" class="' . $link_class . '">';
        } else {
            $htmllink .= '<a href="' . $link . '" class="' . $link_class . '">';
        }

        // we add item description for category link
        if ($item['level'] > 2) {
            switch ($item['type']) {
                case 3:
                    $htmllink .= '<span class="item-description">' . $this->htmlEscape($item['item_description']) . '</span>' . "\n";
                    if ($image = $this->getCategoryImageFromUrl($item['linkcategory'])) {
                        $htmllink .= '<span class="item-image">' . $image . '</span>' . "\n";
                    }
                    break;
            }
        }

        $htmllink .= '<span class="item-name">' . $this->htmlEscape($item['name']) . '</span></a>' . "\n";

		return $htmllink;
	}

    public function drawItem($region='top', $pid=0, $itemsperrow=5)
    {
        $html_wide = '';
        $html_small = '';
        $items = $this->getChildrens($pid, $region);
        if (!empty($childrens)) {
        	$html['wide'] = $html_wide;
        	$html['small'] = $html_small;
            return $html;
        }
		$i = 0;
		$cc = 0;
		$_columnCount = $itemsperrow;
		$_collectionSize = count($items);

        foreach ($items as $k => $item){

        	if($item['type'] == 4){
        		$itemsperrowcat = $itemsperrow;
        		if(!empty($item['itemsperrowsub']) && $item['itemsperrowsub'] > 0):
        			$itemsperrowcat = $item['itemsperrowsub'];
        		endif;

        	}else{
        		if($item['level'] == 3):
	        		if($cc++%$_columnCount==0):
	        			$html_wide.= '<div class="sub-nav-row">';
	        		endif;
	        		$width = 100 / $_columnCount;
	        		$stylewidth = ' style=" width:'.$width.'%" ';
	        		$html_wide.= '<ul class="sub-nav-column" '.$stylewidth.'>';
	        	else:
	        		$html_wide.= '<ul class="">';
        		endif;

        		$html_wide.= '<li';
		        $html_small.= '<li';
		        $hasChildrens = $this->hasChildrens($item['item_id'], $region);

		        $html_wide.= ' class="menu-level-'.($item['level']-1);
		        $html_small.= ' class="menu-level-'.($item['level']-1);

		        if(!empty($item['cssclass'])){
		        	$html_wide.= ' '.$item['cssclass'];
		        	$html_small.= ' '.$item['cssclass'];
		        }
		        $html_wide.= '" ';
		        $html_small.= '" ';

		        $html_wide.= '>'."\n";
		        $html_small.= '>'."\n";

		        $link = $this->drawLink($item);

		        $html_wide.= $link;
		        $html_small.= $link;

		        if($hasChildrens || $item['type'] == 6){

	        		$html_small.= '<ul class="">'."\n";

			        if ($hasChildrens){
			            $childrens = $this->drawItem($region, $item['item_id']);
			            if (!empty($childrens['wide'])) {
			                $html_wide.= $childrens['wide'];
			                $html_small.= $childrens['small'];
			            }
			        }
		        	$html_small.= '</ul>'."\n";

		        }

		        $html_wide.= '</li>'."\n";
		        $html_small.= '</li>'."\n";
		        $html_wide.= '</ul>';
		        if($item['level'] == 3):
			        if($cc%$_columnCount==0 || $cc==$_collectionSize):
			        	$html_wide.= '</div>';
			        endif;
		        endif;
        	}
        	$i++;
        }

        $html['wide'] = $html_wide;
        $html['small'] = $html_small;
        return $html;
    }

    public function drawItemContinuous($region='top', $pid=0, $itemsperrow=5)
    {
    	$html_wide = '';
    	$html_small = '';
    	$items = $this->getChildrens($pid, $region);
    	if (!empty($childrens)) {
    		$html['wide'] = $html_wide;
    		$html['small'] = $html_small;
    		return $html;
    	}
    	$i = 0;
    	$cc = 0;
    	$_columnCount = $itemsperrow;
    	$_collectionSize = count($items);
    	foreach ($items as $k => $item){

    		if($item['type'] != 4){

    			$html_wide.= '<li';
    			$html_small.= '<li';
    			$hasChildrens = $this->hasChildrens($item['item_id'], $region);

    			$html_wide.= ' class="menu-level-'.($item['level']-1);
    			$html_small.= ' class="menu-level-'.($item['level']-1);

    			if(!empty($item['cssclass'])){
    				$html_wide.= ' '.$item['cssclass'];
    				$html_small.= ' '.$item['cssclass'];
    			}
    			$html_wide.= '" ';
    			$html_small.= '" ';

    			$html_wide.= '>'."\n";
    			$html_small.= '>'."\n";

    			$link = $this->drawLink($item);

    			$html_wide.= $link;
    			$html_small.= $link;
    			$html_wide.= '</li>'."\n";

    			$this->outItemContinuous[] = $html_wide;
    			$html_wide = '';

    			if($hasChildrens){
    				$html_small.= '<ul class="">'."\n";
    				$childrens = $this->drawItemContinuous($region, $item['item_id'],$itemsperrow);
    				if (!empty($childrens['small'])) {
    					$html_small.= $childrens['small'];
    				}
    				$html_small.= '</ul>'."\n";
    			}
    			$html_small.= '</li>'."\n";
    		}
    		$i++;
    	}

    	$html['small'] = $html_small;
    	return $html;
    }


    protected function getCmsBlockHtml($item)
    {
    	$html = '';
    	$block_cms = Mage::getModel('cms/block')->load($item['cmsblock']);
    	$identifier = $block_cms->getIdentifier();
    	$block = $this->getLayout()->createBlock('cms/block')->setBlockId($identifier)->toHtml();

    	$wrapper_menu_cms_block_class = '';
    	if($item['cmsblockposition'] == 'right'){
    		$wrapper_menu_cms_block_class = 'f-right';
    	}elseif($item['cmsblockposition'] == 'left'){
    		$wrapper_menu_cms_block_class = 'f-left';
    	}elseif($item['cmsblockposition'] == 'top'){
    		$wrapper_menu_cms_block_class = 'f-top';
    	}elseif($item['cmsblockposition'] == 'bottom'){
    		$wrapper_menu_cms_block_class = 'f-bottom';
    	}
    	$html.= '<div class="menu-cms-block">'."\n";
    	$html.= $block;
    	$html.= '</div>'."\n";
    	return $html;
    }

    protected function getProductsBlockHtml($item)
    {
    	$wrapper_menu_products_block_class = '';
    	if($item['productblockposition'] == 'right'){
    		$wrapper_menu_products_block_class = 'f-right';
    	}elseif($item['productblockposition'] == 'left'){
    		$wrapper_menu_products_block_class = 'f-left';
    	}
    	$html = '<div class="menu-product-block">'."\n";
    	//$html = '<li class="wrapper-menu-products-block ' . $wrapper_menu_products_block_class . '"><div class="menu-products">'."\n";
    	$html.=  $this->getProductGrid($item);
    	$html.= '</div>'."\n";
    	//$html.= '</div></li>'."\n";
    	return $html;
    }

    protected function getProductGrid($item)
    {
    	$ids = array();
    	foreach(explode('&', $item['menuadminpro_products']) as $value){
    		$parts = explode('=', $value);
    		$ids[] = $parts[0];
    	}

    	$block = $this->getLayout()->createBlock('menuadminpro/products');
    	$block->setProductsCount($item['productstotal']);
    	$block->setColumnCount($item['productsrow']);
    	$block->setItemProductCollection($ids);

    	$html = '';
    	if (($_products = $block->getProductCollection()) && $_products->getSize()):
    	$_columnCount = $block->getColumnCount();
    	$i=0;
    	foreach ($_products->getItems() as $_product):
    	if ($i++%$_columnCount==0):
    	$html.= '<div class="products-grid-menu">'."\n";
    	endif;
    	$html.= '<div class="item-grid';
    	if(($i-1)%$_columnCount==0):
    	$html.= ' first';
    	elseif($i%$_columnCount==0):
    	$html.= ' last';
    	endif;
    	$html.= '">'."\n";
    	$html.= '<a href="'.$_product->getProductUrl().'" title="'.$this->htmlEscape($_product->getName()).'" class="product-image"><img src="'.$this->helper('catalog/image')->init($_product, 'small_image')->resize(300).'" width="100%" height="auto" alt="'.$this->htmlEscape($_product->getName()).'" /></a>'."\n";
    	$html.= '<h3 class="product-name"><a href="'.$_product->getProductUrl().'" title="'.$this->htmlEscape($_product->getName()).'">'.$this->htmlEscape($_product->getName()).'</a></h3>'."\n";
    	$html.= '</div>'."\n";
    	if ($i%$_columnCount==0 || $i==count($_products)):
    	$html.= '</div>'."\n";
    	endif;
    	endforeach;
    	endif;
    	return $html;
    }

    /* category navigation */
    public function renderCategoriesMenuHtml($level = 1, $itemsperrow=5, $continuous=0)
    {
    	$activeCategories = array();
    	foreach ($this->getStoreCategories() as $child) {
    		if ($child->getIsActive()) {
    			$activeCategories[] = $child;
    		}
    	}
    	$activeCategoriesCount = count($activeCategories);
    	$hasActiveCategoriesCount = ($activeCategoriesCount > 0);

    	if (!$hasActiveCategoriesCount) {
    		return '';
    	}

    	$htmlwide = array();
    	$htmlwideinline = array();
    	$html_small = array();
    	$j = 0;
    	$cc = 0;
    	$_collectionSize = count($activeCategories);
    	$_columnCount = $itemsperrow;
    	$this->outItemContinuous = array();
    	$this->outItemInlineContinuous = array();
    	foreach ($activeCategories as $category) {
    		if ($cc++%$_columnCount==0):
    			$htmlwide[] = '<div class="sub-nav-row">';
    		endif;
    		$htmlwide[] = '<ul class="sub-nav-column" style="width:'. (100 / $_columnCount) .'%">';
    		$html = $this->_renderCategoryMenuItemHtml(
    				$category,
    				$level,
    				$itemsperrow,
    				$continuous
    		);

    		$htmlwide[] = $html['wide'];
    		$htmlwideinline[] = $html['wideinline'];
    		$htmlsmall[] = $html['small'];

    		$htmlwide[] = '</ul>';

    		$j++;
    		if ($cc%$_columnCount==0 || $cc==$_collectionSize):
    			$htmlwide[] = '</div>';
    		endif;

    	}
    	$html['wide'] = implode("\n", $htmlwide); /* render category subitem */
    	$html['widecontinuos'] = $this->outItemContinuous; /* render category subitem distributed */
    	$html['wideinline'] = implode("\n", $htmlwideinline); /* render category inline */
    	$html['wideinlinecontinuos'] = $this->outItemInlineContinuous; /* render category subitem distributed */
    	$html['small'] = implode("\n", $htmlsmall); /* render category responsive */
    	return $html;
    }

    protected function _renderCategoryMenuItemHtml($category, $level = 0, $itemsperrow=5, $continuous=0)
    {
    	if (!$category->getIsActive()) {
    		return '';
    	}
    	$html = array();

    	// get all children
    	if (Mage::helper('catalog/category_flat')->isEnabled()) {
    		$children = (array)$category->getChildrenNodes();
    		$childrenCount = count($children);
    	} else {
    		$children = $category->getChildren();
    		$childrenCount = $children->count();
    	}
    	$hasChildren = ($children && $childrenCount);

    	// select active children
    	$activeChildren = array();
    	foreach ($children as $child) {
    		if ($child->getIsActive()) {
    			$activeChildren[] = $child;
    		}
    	}
    	$activeChildrenCount = count($activeChildren);
    	$hasActiveChildren = ($activeChildrenCount > 0);

    	// prepare list item html classes
    	$classes = array();
    	if ($this->isCategoryActive($category)) {
    		$classes[] = 'active';
    	}

    	// prepare list item attributes
    	$attributes = array();
    	if (count($classes) > 0) {
    		$attributes['class'] = implode(' ', $classes);
    	}

    	$_columnCount = $itemsperrow;

    	$htmlwide = '<li class="a menu-level-'.($level+2).'">';
    	$htmlinline = '<li class="menu-level-'.($level+1).'">';
    	$htmlitem = '<li class="menu-level-'.($level+1).'">';
    	$htmlsmall = '<li class="menu-level-'.($level+2).'">';

    	$link = $this->getLinkCategory($category, $level);
    	$htmlwide.= $link['out1'];
    	$htmlinline.= $link['out2'];
    	$htmlitem.= $link['out2'];
    	$htmlsmall.= $link['out1'];

    	$htmlwide.= '</li>';

    	$htmlwideinline[] = $htmlitem;


    	// render children
    	$htmlChildrens = array();
    	$j = 0;
    	if($continuous):
    		//$htmlwide.= '</li>';
	    	$this->outItemContinuous[] = $htmlwide;
	    	if($level == 0):
	    		$this->outItemInlineContinuous[]['top'] = $htmlinline;
	    	else:
	    		$htmlinline.= '</li>';
	    		$qtysub = 0;
    			$pos = count($this->outItemInlineContinuous);
	    		if(isset($this->outItemInlineContinuous[($pos-1)]['sub'])):
	    			$qtysub = count($this->outItemInlineContinuous[($pos-1)]['sub']);
	    			$pos = $pos-1;
	    		endif;
	    		$this->outItemInlineContinuous[$pos]['sub'][$qtysub] = $htmlinline;
	    	endif;
	    	$htmlwide = '';
	    	$htmlinline = '';
    	endif;

    	foreach ($activeChildren as $child) {
    		$htmlChildrens[] = $this->_renderCategoryMenuItemHtml(
    				$child,
    				($level + 1),
    				$itemsperrow,
    				$continuous
    		);
    		$j++;
    	}
    	if (!empty($htmlChildrens)) {
    		$outwide = $outsmall = $outwideinline = '';
    		$cc = 0;
    		$_collectionSize = count($htmlChildrens);

    		foreach($htmlChildrens as $htmlChildren):
	    		$outwide.= $htmlChildren['wide'];
    			if($level == 0):
		    		if ($cc++%$_columnCount==0):
		    			if(!$continuous):
		    				$outwideinline.= '<div class="sub-nav-row">';
		    			endif;
		    		endif;
	    			$outwideinline.= '<ul class="sub-nav-column" style="width:'. (100 / $_columnCount) .'%">';
	    		endif;
	    		$outwideinline.= $htmlChildren['wideinline'];
	    		if($level == 0):
	    			$outwideinline.= '</ul>';
		    		if ($cc%$_columnCount==0 || $cc==$_collectionSize):
		    			if(!$continuous):
		    				$outwideinline.= '</div>';
		    			endif;
		    		endif;
	    		endif;

	    		$outsmall.= $htmlChildren['small'];
    		endforeach;

    		if($level == 0):
    			$widthwrappercategories = 100;
    			if(!$continuous):
    				$htmlwideinline[] = '<div class="sub-nav full-width">';
    				$htmlwideinline[] = '<div class="sub-nav-group" style="width:100%;">';
	   			endif;
    		endif;

    		$htmlsmall.= '<ul class="">';

    		$htmlwide.= $outwide;
    		$htmlinline.= $outwide;
    		$htmlwideinline[] = $outwideinline;

    		$htmlsmall.= $outsmall;

    		if($level == 0):
    			if(!$continuous):
    				$htmlwideinline[] = '</div>';
    				$htmlwideinline[] = '</div>';
    			endif;
    		endif;

    		$htmlsmall.= '</ul>';
    	}

    	//$htmlwide.= '</li>';
    	$htmlinline.= '</li>';
    	//$htmlwideinline[] = '</li>';
    	$htmlsmall.= '</li>';

    	$html['wide'] = $htmlwide;
    	$html['wideinline'] = implode("\n", $htmlwideinline);
    	$html['small'] = $htmlsmall;
    	return $html;
    }

    public function getLinkCategory($category, $level)
    {
    	$linkClass = ' class="item-menu item-menu-'.($level+2).'"';
    	$afirst = '<a href="'.$this->getCategoryUrl($category).'"'.$linkClass.'>';
    	$afirst2 = '<a href="'.$this->getCategoryUrl($category).'" class="item-menu item-menu-'.($level+1).'">';
    	$span = '<span>' . $this->escapeHtml($category->getName()) .'</span>';
    	$aend = '</a>';
    	return array('out1'=>$afirst.$span.$aend, 'out2'=>$afirst2.$span.$aend);
    	//return $afirst.$span.$aend;
    }

    public function getStoreCategories()
    {
    	$helper = Mage::helper('catalog/category');
    	return $helper->getStoreCategories();
    }

    public function isCategoryActive($category)
    {
    	if ($this->getCurrentCategory()) {
    		return in_array($category->getId(), $this->getCurrentCategory()->getPathIds());
    	}
    	return false;
    }

    protected function _getCategoryInstance()
    {
    	if (is_null($this->_categoryInstance)) {
    		$this->_categoryInstance = Mage::getModel('catalog/category');
    	}
    	return $this->_categoryInstance;
    }

    public function getCategoryUrl($category)
    {
    	if ($category instanceof Mage_Catalog_Model_Category) {
    		$url = $category->getUrl();
    	} else {
    		$url = $this->_getCategoryInstance()
    		->setData($category->getData())
    		->getUrl();
    	}

    	return $url;
    }

    private function getCategoryImageFromUrl($url)
    {
        $link = Mage::getUrl().$url;
        $urlKey = explode('.', $url);
        $urlKey = explode('/', $urlKey[0]);
        $urlKey = (count($urlKey) > 1) ? $urlKey[count($urlKey) - 1] : $urlKey[0];

        $collection = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('thumbnail')
            ->addAttributeToFilter('url_key', $urlKey);

        if ($collection->count()>0 and (strlen($collection->getFirstItem()->getData('thumbnail')))) {
            return sprintf('<img src="%s">', $this->helper('timage')->init(Mage::getBaseUrl('media').'catalog/category/'.$collection->getFirstItem()->getData('thumbnail'))->resize(240, 190));
        }
    }
}
?>