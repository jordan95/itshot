<?php
class Mgtoxp_Menuadminpro_Block_Adminhtml_Menuadminpro_Tab_Design extends Mage_Adminhtml_Block_Catalog_Form
{
	public function __construct()
	{
		parent::__construct();
		$this->setShowGlobalIcon(true);
	}
	
	public function getItem()
	{
		if (!$this->_item) {
			$this->_item = Mage::registry('item');
		}
		return $this->_item;
	}
	
	public function _prepareLayout()
	{
		parent::_prepareLayout();
		$form = new Varien_Data_Form();
		$form->setDataObject($this->getItem());
		
		$weight = array('normal'=>'Normal','bold'=>'Bold');
		$fonttransform = array('none' => 'None', 'uppercase' => 'Uppercase', 'lowercase' => 'Lowercase', 'capitalize' => 'Capitalize');
		$bordersize = array();
		for ($i = 0; $i <= 5; $i++) { $bordersize[$i] = $i.'px'; }
		$fontsize = array();
		for ($i = 11; $i <= 36; $i++) { $fontsize[$i.'px'] = $i.'px'; }
		$shift = array();
		for ($i = 0; $i <= 30; $i++) { $shift[$i.'px'] = $i.'px'; }
		
	
		$fieldset = $form->addFieldset('nav_fieldset', array('legend'=>Mage::helper('catalog')->__('Top Nav Bar (first level)')));
		
		$fieldset->addField('note', 'note', array(
				'text'     => Mage::helper('menuadminpro')->__('Use CSS values'),
		));

		$fieldset->addField('navheight', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Nav height'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navheight',
				'value'		=> '40',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Enter a value in pixels. 40').'</small>'
		));
		
		$fieldset->addField('navbackgroundcolor', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Background color'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navbackgroundcolor',
				'value'		=> '#ffffff',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #ffffff').'</small>'
		));

		$fieldset->addField('navborderwidth', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Border width'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navborderwidth',
				'value'  	=> '1px',
				'values'	=> $bordersize,
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Select a value. 0px no border').'</small>'
		));
		
		$fieldset->addField('navbordercolor', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Border color'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navbordercolor',
				'value'		=> '#dddddd',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #dddddd').'</small>'
		));
		
		$fieldset->addField('navborderround', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Border round'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navborderround',
				'value'		=> '0',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('You can specify values ​​for each corner: 5px 5px 0px 0px (top-left top-right bottom-right bottom-left)').'</small>'
		));
		
		$fieldset->addField('navfontcolor', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Font color'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navfontcolor',
				'value'		=> '#000000',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #000000').'</small>'
		));
		
		$fieldset->addField('navfontcolorhover', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Font color mouse over'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navfontcolorhover',
				'value'		=> '#555555',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #555555').'</small>'
		));
		/*
		$fieldset->addField('navpaddingitem', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Item padding'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navpaddingitem',
				'value'		=> '11px 20px',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('You can specify values ​​for each side: 11px 20px').'</small>'
		));
		*/
		$fieldset->addField('navpaddingitemleft', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Item padding left'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navpaddingitemleft',
				'value'		=> '10px',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Enter a value in pixel: 10px').'</small>'
		));
		$fieldset->addField('navpaddingitemright', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Item padding right'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navpaddingitemright',
				'value'		=> '10px',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Enter a value in pixel: 10px').'</small>'
		));
		
		$fieldset->addField('navfontbackgroundhover', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Item background color mouse over'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navfontbackgroundhover',
				'value'		=> '#eeeeee',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #eeeeee').'</small>'
		));

		$fieldset->addField('navfontsize', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font size'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navfontsize',
				'value'		=> '14px',
				'values'    => $fontsize
		));
		
		$fieldset->addField('navfonttransform', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font transform'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navfonttransform',
				'value'		=> 'uppercase',
				'values'    => $fonttransform
		));
		
		$fieldset->addField('navfontweight', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font weight'),
				'class'     => '',
				'required'  => false,
				'name'      => 'navfontweight',
				'value'		=> 'bold',
				'values'    => $weight
		));
		
		$fieldset = $form->addFieldset('dropdown_fieldset', array('legend'=>Mage::helper('catalog')->__('Dropdown area')));
		
		$fieldset->addField('dropbackgroundcolor', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Background color'),
				'class'     => '',
				'required'  => false,
				'name'      => 'dropbackgroundcolor',
				'value'		=> '#ffffff',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #ffffff').'</small>'
		));

		$fieldset->addField('dropborderwidth', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Border width'),
				'class'     => '',
				'required'  => false,
				'name'      => 'dropborderwidth',
				'value'		=> '1px',
				'values'		=> $bordersize,
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Select a value. 0 no border').'</small>'
		));
		
		$fieldset->addField('dropbordercolor', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Border color'),
				'class'     => '',
				'required'  => false,
				'name'      => 'dropbordercolor',
				'value'		=> '#dddddd',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #dddddd').'</small>'
		));
		
		$fieldset->addField('dropborderround', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Border round'),
				'class'     => '',
				'required'  => false,
				'name'      => 'dropborderround',
				'value'		=> '0',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('You can specify values ​​for each corner: 0px 0px 5px 5px (top-left top-right bottom-right bottom-left)').'</small>'
		));

		$fieldset->addField('droppadding', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Dropdown padding'),
				'class'     => '',
				'required'  => false,
				'name'      => 'droppadding',
				'value'		=> '10px 10px',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('You can specify values ​​for each side: 10px 10px').'</small>'
		));
		
		$fieldset = $form->addFieldset('sec_fieldset', array('legend'=>Mage::helper('catalog')->__('Items Second level')));
		
		$fieldset->addField('secpaddingitem', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Item padding'),
				'class'     => '',
				'required'  => false,
				'name'      => 'secpaddingitem',
				'value'		=> '5px 10px',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('You can specify values ​​for each side: 5px 10px').'</small>'
		));
		
		$fieldset->addField('secfontbackground', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Item background color'),
				'class'     => '',
				'required'  => false,
				'name'      => 'secfontbackground',
				'value'		=> '#eeeeee',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #eeeeee').'</small>'
		));
		
		$fieldset->addField('secfontcolor', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Font color'),
				'class'     => '',
				'required'  => false,
				'name'      => 'secfontcolor',
				'value'		=> '#000000',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #000000').'</small>'
		));
		
		$fieldset->addField('secfontcolorhover', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Font color mouse over'),
				'class'     => '',
				'required'  => false,
				'name'      => 'secfontcolorhover',
				'value'		=> '#555555',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #555555').'</small>'
		));
		
		$fieldset->addField('secfontsize', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font size'),
				'class'     => '',
				'required'  => false,
				'name'      => 'secfontsize',
				'value'		=> '14px',
				'values'    => $fontsize
		));
		
		$fieldset->addField('secfonttransform', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font transform'),
				'class'     => '',
				'required'  => false,
				'name'      => 'secfonttransform',
				'value'		=> 'none',
				'values'    => $fonttransform
		));
		
		$fieldset->addField('secfontweight', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font weight'),
				'class'     => '',
				'required'  => false,
				'name'      => 'secfontweight',
				'value'		=> 'bold',
				'values'    => $weight
		));
		
		
		$fieldset = $form->addFieldset('tir_fieldset', array('legend'=>Mage::helper('catalog')->__('Items Other levels')));
		
		$fieldset->addField('tirleftshift', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Left shift'),
				'class'     => '',
				'required'  => false,
				'name'      => 'tirleftshift',
				'value'		=> '8px',
				'values'    => $shift
		));
		
		$fieldset->addField('tirpaddingitem', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Item padding'),
				'class'     => '',
				'required'  => false,
				'name'      => 'tirpaddingitem',
				'value'		=> '5px 10px',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('You can specify values ​​for each side: 5px 10px').'</small>'
		));
		
		$fieldset->addField('tirfontcolor', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Font color'),
				'class'     => '',
				'required'  => false,
				'name'      => 'tirfontcolor',
				'value'		=> '#424242',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #424242').'</small>'
		));
		
		$fieldset->addField('tirfontcolorhover', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Font color mouse over'),
				'class'     => '',
				'required'  => false,
				'name'      => 'tirfontcolorhover',
				'value'		=> '#555555',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #555555').'</small>'
		));
		
		$fieldset->addField('tirfontsize', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font size'),
				'class'     => '',
				'required'  => false,
				'name'      => 'tirfontsize',
				'value'		=> '13px',
				'values'    => $fontsize
		));
		
		$fieldset->addField('tirfonttransform', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font transform'),
				'class'     => '',
				'required'  => false,
				'name'      => 'tirfonttransform',
				'value'		=> 'none',
				'values'    => $fonttransform
		));
		
		$fieldset->addField('tirfontweight', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font weight'),
				'class'     => '',
				'required'  => false,
				'name'      => 'tirfontweight',
				'value'		=> 'normal',
				'values'    => $weight
		));

		$form->addValues($this->getItem()->getData());
		$form->setFieldNameSuffix('design');
		$this->setForm($form);
	}
	

}