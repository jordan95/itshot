<?php
class Mgtoxp_Menuadminpro_Block_Adminhtml_Menuadminpro_Tab_General extends Mage_Adminhtml_Block_Catalog_Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setShowGlobalIcon(true);
    }

    public function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
        	$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('_general');
        $form->setDataObject($this->getItem());

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('catalog')->__('General Information')));

		$storeId = $this->getRequest()->getParam('store');
		if (!$storeId) {
			$storeId = Mgtoxp_Menuadminpro_Model_Menuadminpro::DEFAULT_STORE_ID;
		}
		
		if (!$this->getItem()->getId()) {
			$parentId = $this->getRequest()->getParam('parent');
			if (!$parentId) {
				$parentId = Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID;
			}
			$fieldset->addField('path', 'hidden', array(
					'name'  => 'path',
					'value' => $parentId
			));
			
			$isRoot = $parentId;

		}else{
			$fieldset->addField('path', 'hidden', array(
					'name'      => 'path',
					'value' => $this->getItem()->getId()
			));
			$fieldset->addField('id', 'hidden', array(
					'name'  => 'id',
					'value' => $this->getItem()->getId()
			));
			$isRoot = $this->getItem()->getParentId();

		}
		
		if (!Mage::app()->isSingleStoreMode() && $isRoot == 1) {
			$fieldset->addField('stores', 'multiselect', array(
					'name'      => 'stores',
					'label'     => Mage::helper('menuadminpro')->__('Store View'),
					'title'     => Mage::helper('menuadminpro')->__('Store View'),
					'required'  => true,
					'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
			));
		}
	
		$fieldset->addField('name', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Label'),
				'class'     => 'required-entry',
				'required'  => true,
				'name'      => 'name',
		));
		
		$fieldset->addField('is_active', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Is Active'),
				'name'      => 'is_active',
				'class'     => 'required-entry',
				'required'  => true,
				'values'    => Mage::getSingleton('menuadminpro/status')->getOptionArray(),
		));
		
		if ($isRoot == 1) {
			$fieldset->addField('region', 'select', array(
					'label'     => Mage::helper('menuadminpro')->__('Region'),
					'name'      => 'region',
					'required'  => true,
					'values'    => Mage::getSingleton('menuadminpro/region')->getOptionArray(),
			));
		}
		
		if ($isRoot != 1) {
			$fieldset->addField('cssclass', 'text', array(
					'label'     => Mage::helper('menuadminpro')->__('Css class'),
					'required'  => false,
					'name'      => 'cssclass'
			));
		}

        $fieldset->addField('item_description', 'text', array(
            'label'     => Mage::helper('menuadminpro')->__('Item Description'),
            'required'  => false,
            'name'      => 'item_description'
        ));
		
		if ($isRoot != 1) {
			$fieldset = $form->addFieldset('link_fieldset', array('legend'=>Mage::helper('catalog')->__('Link Settings')));
			
			$linktype = $fieldset->addField('type', 'select', array(
					'label'     => Mage::helper('menuadminpro')->__('Type'),
					'name'      => 'type',
					'values'    => Mage::getSingleton('menuadminpro/type')->getOptionArray(),
			));
		
			$linktext = $fieldset->addField('link', 'text', array(
					'label'     => Mage::helper('menuadminpro')->__('Link'),
					'class'     => '',
					'required'  => false,
					'name'      => 'link',
			));
			
			$linkcms = $fieldset->addField('linkpage', 'select', array(
					'label'     => Mage::helper('menuadminpro')->__('Link to CMS page'),
					'name'      => 'linkpage',
					'class'     => '',
					'required'  => false,
					'values'    => $this->getCmsPages(),
			));
			
			$linkcategory = $fieldset->addField('linkcategory', 'select', array(
					'label'     => Mage::helper('menuadminpro')->__('Link to Category'),
					'name'      => 'linkcategory',
					'class'     => '',
					'required'  => false,
					'values'    => $this->getCategories(),
			));
			
			$this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
					->addFieldMap($linktype->getHtmlId(), $linktype->getName())
					->addFieldMap($linktext->getHtmlId(), $linktext->getName())
					->addFieldMap($linkcms->getHtmlId(), $linkcms->getName())
					->addFieldMap($linkcategory->getHtmlId(), $linkcategory->getName())
					->addFieldDependence(
							$linktext->getName(),
							$linktype->getName(),
							'1'
					)
					->addFieldDependence(
							$linkcms->getName(),
							$linktype->getName(),
							'2'
					)
					->addFieldDependence(
							$linkcategory->getName(),
							$linktype->getName(),
							'3'
					)
			);
		
			$fieldset->addField('target', 'select', array(
					'label'     => Mage::helper('menuadminpro')->__('Target'),
					'name'      => 'target',
					'values'    => Mage::getSingleton('menuadminpro/target')->getOptionArray(),
			));
		
		}else{
			$fieldset = $form->addFieldset('settings_fieldset', array('legend'=>Mage::helper('catalog')->__('Menu Settings')));

			$fieldset->addField('itemsperrow', 'text', array(
					'label'     => Mage::helper('menuadminpro')->__('Items per row by default'),
					'class'     => '',
					'required'  => false,
					'name'      => 'itemsperrow',
					'value'      => '4',
			));
			
			$fieldset->addField('distributeitems', 'select', array(
					'label'     => Mage::helper('menuadminpro')->__('Distribute items in columns by default'),
					'name'      => 'distributeitems',
					'values'    => array(
							1    => Mage::helper('menuadminpro')->__('Yes'),
							0   => Mage::helper('menuadminpro')->__('No')
					),
			));
			
			/* responsive menu options */
			$fieldset = $form->addFieldset('settings_rwd_fieldset', array('legend'=>Mage::helper('catalog')->__('Menu Responsive Settings')));
			
			$isresponsive = $fieldset->addField('is_responsive', 'select', array(
					'label'     => Mage::helper('menuadminpro')->__('Use Responsive Menu'),
					'name'      => 'is_responsive',
					'class'     => 'required-entry',
					'required'  => true,
					'values'    => array(0 => 'No', 1 => 'Yes')
			));
			
			$menubutton = $fieldset->addField('menubutton', 'select', array(
					'label'     => Mage::helper('menuadminpro')->__('Use menu button'),
					'name'      => 'menubutton',
					'values'    => array(
							1    => Mage::helper('menuadminpro')->__('Yes'),
							0   => Mage::helper('menuadminpro')->__('No')
					),
			));
			
			$menutitle = $fieldset->addField('menutitle', 'text', array(
					'label'     => Mage::helper('menuadminpro')->__('Menu title'),
					'class'     => '',
					'required'  => false,
					'name'      => 'menutitle',
					'value'		=> 'Main Menu'
			));
			
			$this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
					->addFieldMap($isresponsive->getHtmlId(), $isresponsive->getName())
					->addFieldMap($menubutton->getHtmlId(), $menubutton->getName())
					->addFieldMap($menutitle->getHtmlId(), $menutitle->getName())
					->addFieldDependence(
							$menubutton->getName(),
							$isresponsive->getName(),
							'1'
					)
					->addFieldDependence(
							$menutitle->getName(),
							$isresponsive->getName(),
							'1'
					)
					->addFieldDependence(
							$menutitle->getName(),
							$menubutton->getName(),
							'1'
					)
			);

		}

        $form->addValues($this->getItem()->getData());

        $form->setFieldNameSuffix('general');
        $this->setForm($form);

    }
    
    public function getCmsPages(){
    	$options = Mage::getResourceModel('cms/page_collection')
    	->load()
    	->toOptionArray();
    	array_unshift($options, array('value'=>'', 'label'=>Mage::helper('menuadminpro')->__('Please select a page link...')));
    	
    	return $options;
    }
    
    public function getCategories(){
    	$out = Mage::getSingleton('menuadminpro/categories')->getArrayCategories();
    	array_unshift($out, array('value'=>'', 'label'=>Mage::helper('menuadminpro')->__('Please select a category link...')));
    	return $out;
    }

    public function getItem()
    {
    	return Mage::registry('item');
    }
    
}
