<?php
class Mgtoxp_Menuadminpro_Block_Adminhtml_Menuadminpro_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId    = 'item';
        $this->_blockGroup	= 'menuadminpro';
        $this->_controller  = 'adminhtml_menuadminpro';
        $this->_mode        = 'edit';

        parent::__construct();
        $this->setTemplate('mgtoxp/menuadminpro/edit.phtml');
    }
}
