<?php
class Mgtoxp_Menuadminpro_Block_Adminhtml_Menuadminpro_Tab_Responsive extends Mage_Adminhtml_Block_Catalog_Form
{
	public function __construct()
	{
		parent::__construct();
		$this->setShowGlobalIcon(true);
	}
	
	public function getItem()
	{
		if (!$this->_item) {
			$this->_item = Mage::registry('item');
		}
		return $this->_item;
	}
	
	public function _prepareLayout()
	{
		parent::_prepareLayout();
		$form = new Varien_Data_Form();
		$form->setDataObject($this->getItem());
		
		$weight = array('normal'=>'Normal','bold'=>'Bold');
		$fonttransform = array('none' => 'None', 'uppercase' => 'Uppercase', 'lowercase' => 'Lowercase', 'capitalize' => 'Capitalize');
		$bordersize = array();
		for ($i = 0; $i <= 5; $i++) { $bordersize[$i] = $i.'px'; }
		$fontsize = array();
		for ($i = 11; $i <= 36; $i++) { $fontsize[$i.'px'] = $i.'px'; }
	
		$fieldset = $form->addFieldset('itemresp_fieldset', array('legend'=>Mage::helper('catalog')->__('Item')));
		
		$fieldset->addField('note', 'note', array(
				'text'     => Mage::helper('menuadminpro')->__('Use CSS values'),
		));

		$fieldset->addField('itemheight', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Item height'),
				'class'     => '',
				'required'  => false,
				'name'      => 'itemheight',
				'value'		=> '40',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Enter a value in pixels. 40').'</small>'
		));
		
		$fieldset->addField('itembgcolorfirst', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Background color first level'),
				'class'     => '',
				'required'  => false,
				'name'      => 'itembgcolorfirst',
				'value'		=> '#EDEDED',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #EDEDED').'</small>'
		));
		
		$fieldset->addField('itembgcolorother', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Background color other levels'),
				'class'     => '',
				'required'  => false,
				'name'      => 'itembgcolorother',
				'value'		=> '#F7F7F7',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #F7F7F7').'</small>'
		));
		
		$fieldset->addField('itembgcolorhover', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Background color on mouse over'),
				'class'     => '',
				'required'  => false,
				'name'      => 'itembgcolorhover',
				'value'		=> '#DADADA',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #DADADA').'</small>'
		));
		
		$fieldset->addField('itemborderwidth', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Border bottom width'),
				'class'     => '',
				'required'  => false,
				'name'      => 'itemborderwidth',
				'value'  	=> '1px',
				'values'	=> $bordersize,
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Select a value. 0px no border').'</small>'
		));

		$fieldset->addField('itembordercolor', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Border bottom color'),
				'class'     => '',
				'required'  => false,
				'name'      => 'itembordercolor',
				'value'		=> '#AAAAAA',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #AAAAAA').'</small>'
		));
		
		$fieldset->addField('itemfontcolor', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Font color'),
				'class'     => '',
				'required'  => false,
				'name'      => 'itemfontcolor',
				'value'		=> '#000000',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #000000').'</small>'
		));

		$fieldset->addField('itemfontsize', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font size'),
				'class'     => '',
				'required'  => false,
				'name'      => 'itemfontsize',
				'value'		=> '14px',
				'values'    => $fontsize
		));
		
		$fieldset->addField('itemfonttransform', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font transform'),
				'class'     => '',
				'required'  => false,
				'name'      => 'itemfonttransform',
				'value'		=> 'uppercase',
				'values'    => $fonttransform
		));
		
		$fieldset->addField('itemfontweight', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font weight'),
				'class'     => '',
				'required'  => false,
				'name'      => 'itemfontweight',
				'value'		=> 'bold',
				'values'    => $weight
		));
		
		$fieldset = $form->addFieldset('itemcollapser_fieldset', array('legend'=>Mage::helper('catalog')->__('Menu button')));
		
		$fieldset->addField('colbgcolor', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Background collapser bar'),
				'class'     => '',
				'required'  => false,
				'name'      => 'colbgcolor',
				'value'		=> '#AAAAAA',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #AAAAAA').'</small>'
		));
		
		$fieldset->addField('colbuttonbgcolor', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Background menu button'),
				'class'     => '',
				'required'  => false,
				'name'      => 'colbuttonbgcolor',
				'value'		=> '#0E0E0E',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #0E0E0E').'</small>'
		));
		
		$fieldset->addField('colbuttonfontcolor', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Menu button color'),
				'class'     => '',
				'required'  => false,
				'name'      => 'colbuttonfontcolor',
				'value'		=> '#FFFFFF',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #FFFFFF').'</small>'
		));
		
		$fieldset->addField('colfontcolor', 'text', array(
				'label'     => Mage::helper('menuadminpro')->__('Font color'),
				'class'     => '',
				'required'  => false,
				'name'      => 'colfontcolor',
				'value'		=> '#FFFFFF',
				'after_element_html' => '<small>'.Mage::helper('menuadminpro')->__('Use hexadecimal format: #FFFFFF').'</small>'
		));
		
		$fieldset->addField('colfontsize', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font size'),
				'class'     => '',
				'required'  => false,
				'name'      => 'colfontsize',
				'value'		=> '14px',
				'values'    => $fontsize
		));
		
		$fieldset->addField('colfonttransform', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font transform'),
				'class'     => '',
				'required'  => false,
				'name'      => 'colfonttransform',
				'value'		=> 'uppercase',
				'values'    => $fonttransform
		));
		
		$fieldset->addField('colfontweight', 'select', array(
				'label'     => Mage::helper('menuadminpro')->__('Font weight'),
				'class'     => '',
				'required'  => false,
				'name'      => 'colfontweight',
				'value'		=> 'bold',
				'values'    => $weight
		));
		
		$form->addValues($this->getItem()->getData());
		$form->setFieldNameSuffix('responsive');
		$this->setForm($form);
	}
	

}