<?php
class Mgtoxp_Menuadminpro_Block_Adminhtml_Menuadminpro_Tab_Css extends Mage_Adminhtml_Block_Catalog_Form
{
	public function __construct()
	{
		parent::__construct();
		$this->setShowGlobalIcon(true);
	}
	
	public function getItem()
	{
		if (!$this->_item) {
			$this->_item = Mage::registry('item');
		}
		return $this->_item;
	}
	
	public function _prepareLayout()
	{
		parent::_prepareLayout();
		$form = new Varien_Data_Form();
		$form->setDataObject($this->getItem());
		
	
		$fieldset = $form->addFieldset('css_fieldset', array('legend'=>Mage::helper('catalog')->__('Custom CSS')));
		
		$fieldset->addField('note', 'note', array(
				'text'     => Mage::helper('menuadminpro')->__('You can use this form to customize the style sheet.'),
		));

		$fieldset->addField('customcss', 'editor', array(
				'label'     => Mage::helper('menuadminpro')->__('Css code'),
				'required'  => false,
				'name'      => 'customcss',
				'style'     => 'width:700px;height:36em;',
				'value'		=> ''
		));

		$form->addValues($this->getItem()->getData());
		$form->setFieldNameSuffix('design');
		$this->setForm($form);
	}
	

}