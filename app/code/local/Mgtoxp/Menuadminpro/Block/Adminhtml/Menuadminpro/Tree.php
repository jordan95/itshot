<?php

class Mgtoxp_Menuadminpro_Block_Adminhtml_Menuadminpro_Tree extends Mgtoxp_Menuadminpro_Block_Adminhtml_Menuadminpro_Abstract
{
    protected $_withChildrenCount;

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('mgtoxp/menuadminpro/tree.phtml');
        $this->setUseAjax(true);
        $this->_withChildrenCount = true;
    }

    protected function _prepareLayout()
    {
        $addUrl = $this->getUrl("*/*/add", array(
            '_current'=>true,
            'id'=>null,
            '_query' => false
        ));

        $this->setChild('add_root_button',
        		$this->getLayout()->createBlock('adminhtml/widget_button')
        		->setData(array(
        				'label'     => Mage::helper('menuadminpro')->__('Add Root Menu'),
        				'onclick'   => "addNew('".$addUrl."', true)",
        				'class'     => 'add',
        				'id'        => 'add_root_item_button'
        		))
        );

        $this->setChild('add_sub_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('menuadminpro')->__('Add Item'),
                    'onclick'   => "addNew('".$addUrl."', false)",
                    'class'     => 'add',
                    'id'        => 'add_subitem_button',
                    'style'     => $this->canAddSubPage() ? '' : 'display: none;'
                ))
        );

        return parent::_prepareLayout();
    }

    protected function _getDefaultStoreId()
    {
        return Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID;
    }

    public function getItemCollection()
    {
        $storeId = $this->getRequest()->getParam('store', $this->_getDefaultStoreId());
        $collection = $this->getData('item_collection');
        if (is_null($collection)) {
            $collection = Mage::getModel('menuadminpro/menuadminpro')->getCollection();

            /* @var $collection Mage_Cms_Model_Mysql4_Page_Collection */
            //$collection->addStoreFilter($storeId);
            if($storeId){
            	//$collection->addFieldToFilter('store_id', $storeId);
            }

            $this->setData('item_collection', $collection);
        }
        return $collection;
    }

    public function getAddRootButtonHtml()
    {
        return $this->getChildHtml('add_root_button');
    }

    public function getAddSubButtonHtml()
    {
        return $this->getChildHtml('add_sub_button');
    }

    public function getExpandButtonHtml()
    {
        return $this->getChildHtml('expand_button');
    }

    public function getCollapseButtonHtml()
    {
        return $this->getChildHtml('collapse_button');
    }

    public function getStoreSwitcherHtml()
    {
        return $this->getChildHtml('store_switcher');
    }

    public function getLoadTreeUrl($expanded=null)
    {
        $params = array('_current'=>true, 'id'=>null,'store'=>null);
        if (
            (is_null($expanded) && Mage::getSingleton('admin/session')->getIsTreeWasExpanded())
            || $expanded == true) {
            $params['expand_all'] = true;
        }
        return $this->getUrl('*/*/itemsJson', $params);
    }

    public function getNodesUrl()
    {
        return $this->getUrl('*/menuadminpro/jsonTree');
    }

    public function getSwitchTreeUrl()
    {
        return $this->getUrl("*/*/tree", array('_current'=>true, 'store'=>null, '_query'=>false, 'id'=>null, 'parent'=>null));
    }

    public function getIsWasExpanded()
    {
        return Mage::getSingleton('admin/session')->getIsTreeWasExpanded();
    }

    public function getMoveUrl()
    {
        return $this->getUrl('*/*/move', array('store'=>$this->getRequest()->getParam('store')));
    }

    public function getTree($parenNodeItem=null)
    {
        $rootArray = $this->_getNodeJson($this->getRoot($parenNodeItem));
        $tree = isset($rootArray['children']) ? $rootArray['children'] : array();
        return $tree;
    }

    public function getTreeJson($parenNodeItem=null)
    {
        $rootArray = $this->_getNodeJson($this->getRoot($parenNodeItem));
        $json = Mage::helper('core')->jsonEncode(isset($rootArray['children']) ? $rootArray['children'] : array());
        return $json;
    }

    public function getBreadcrumbsJavascriptNone($path, $javascriptVarName)
    {
        if (empty($path)) {
            return '';
        }

        $pages = Mage::getResourceSingleton('menuadminpro/menuadminpro_tree')->setStoreId($this->getStore()->getId())->loadBreadcrumbsArray($path);
        if (empty($pages)) {
            return '';
        }
        foreach ($pages as $key => $page) {
            $pages[$key] = $this->_getNodeJson($page);
        }
        return
            '<script type="text/javascript">'
            . $javascriptVarName . ' = ' . Mage::helper('core')->jsonEncode($pages) . ';'
            . ($this->canAddSubPage() ? '$("add_subpage_button").show();' : '$("add_subpage_button").hide();')
            . '</script>';
    }

    public function getBreadcrumbsJavascript($path, $javascriptVarName)
    {
    	if (empty($path)) {
    		return '';
    	}

    	$categories = Mage::getResourceSingleton('menuadminpro/menuadminpro_tree')
    	->setStoreId($this->getStore()->getId())->loadBreadcrumbsArray($path);
    	if (empty($categories)) {
    		return '';
    	}
    	foreach ($categories as $key => $category) {
    		$categories[$key] = $this->_getNodeJson($category);
    	}
    	return
    	'<script type="text/javascript">'
    			. $javascriptVarName . ' = ' . Mage::helper('core')->jsonEncode($categories) . ';'
    					. ($this->canAddSubCategory() ? '$("add_subitem_button").show();' : '$("add_subitem_button").hide();')
    					. '</script>';
    }

//listo nope
    protected function _getNodeJson($node, $level = 0)
    {
    	// create a node from data array
    	if (is_array($node)) {
    		$node = new Varien_Data_Tree_Node($node, 'item_id', new Varien_Data_Tree);
    	}


    	$item = array();
    	$item['text'] = $this->buildNodeName($node);
    	$rootForStores = in_array($node->getItemId(), array(1));
    	$item['id']  = $node->getId();
    	$item['store']  = (int) $this->getStore()->getId();
    	$item['path'] = $node->getData('path');

    	$item['cls'] = 'folder ' . ($node->getIsActive() ? 'active-category' : 'no-active-category');
    	$allowMove = $this->_isPageMoveable($node);
    	$item['allowDrop'] = $allowMove;
    	$item['allowDrag'] = $allowMove && (($node->getLevel()==1 && $rootForStores) ? false : true);

    	if ((int)$node->getChildrenCount()>0) {
    		$item['children'] = array();
    	}

    	$isParent = $this->_isParentSelectedItem($node);

    	if ((int)$node->getChildrenCount()>0) {
    		$item['children'] = array();
    		if (!($this->getUseAjax() && $node->getLevel() > 1 && !$isParent)) {
    			foreach ($node->getChildren() as $child) {
    				$item['children'][] = $this->_getNodeJson($child, $level+1);
    			}
    		}
    	}

    	if ($isParent || $node->getLevel() < 2) {
    		$item['expanded'] = true;
    	}

    	return $item;
    }


//listo
    public function buildNodeName($node)
    {
    	$store_label = '';
    	$parent_id = $node->getParentId();
    	if($parent_id == 1){
    		$store_id = $node->getStoreId();
    		if($store_id == 0){
    			$store_label = ' - All';
    		}else{
    			//get store name
    			$store_label = ' - '.Mage::app()->getStore($store_id)->getName(); ;
    		}
    	}

        $result = $this->htmlEscape($node->getName());
        $result .= $store_label;
        if ($this->_withChildrenCount) {
             $result .= ' (' . $node->getChildrenCount() . ')';
        }
        return $result;
    }

    protected function _isPageMoveable($node)
    {
        $options = new Varien_Object(array(
            'is_moveable' => true,
            'page' => $node
        ));

        Mage::dispatchEvent('adminhtml_page_tree_is_moveable',
            array('options'=>$options)
        );

        return $options->getIsMoveable();
    }
//cambiado
    protected function _isParentSelectedItem($node)
    {

        if ($node && $this->getItem()) {
            $pathIds = $this->getItem()->getPathIds();
            if (in_array($node->getId(), $pathIds)) {
                return true;
            }
        }

        return false;
    }

    public function canAddRootPage()
    {
        return false;
    }

    public function canAddSubPage()
    {
        $options = new Varien_Object(array('is_allow'=>true));
        Mage::dispatchEvent(
            'adminhtml_page_tree_can_add_sub_page',
            array(
                'page'    => $this->getPage(),
                'options' => $options,
                'store'   => $this->getStore()->getId()
            )
        );

        return $options->getIsAllow();
    }
//reformado
    public function getItem()
    {
        return Mage::registry('item');
    }
//reformado
    public function getItemId()
    {

        if ($this->getItem()) {
            return $this->getItem()->getId();
        }

        return Mage::getResourceModel('menuadminpro/menuadminpro')->getStoreRootId($this->getStoreId());
    }
//reformado
    public function getItemName()
    {
        return $this->getItem()->getTitle();
    }
//reformado
    public function getItemPath()
    {
        if ($this->getItem()) {
            return $this->getItem()->getPath();
        }
        return '';
    }

    public function hasStoreRootPage()
    {
        $root = $this->getRoot();
        if ($root && $root->getId()) {
            return true;
        }
        return false;
    }

    public function getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store');
        return Mage::app()->getStore($storeId);
    }

    public function getStoreId()
    {
        return $this->getStore()->getId();
    }

    public function getRoot($parentNodeItem=null, $recursionLevel=3)
    {
    	if (!is_null($parentNodeItem) && $parentNodeItem->getId()) {
    		return $this->getNode($parentNodeItem, $recursionLevel);
    	}
    	$root = Mage::registry('root');
    	if (is_null($root)) {
    		$storeId = (int) $this->getRequest()->getParam('store');

    		if ($storeId) {
    			$store = Mage::app()->getStore($storeId);
    			$rootId = 1;//$store->getRootCategoryId();TODO
    		}
    		else {
    			$rootId = Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID;
    		}

    		$tree = Mage::getResourceSingleton('menuadminpro/menuadminpro_tree')->load(null, $recursionLevel);

    		if ($this->getItem()) {
    			$tree->loadEnsuredNodes($this->getItem(), $tree->getNodeById($rootId));
    		}

    		$tree->addCollectionData($this->getItemCollection());

    		$root = $tree->getNodeById($rootId);
    		if ($root && $rootId != Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID) {
    			$root->setIsVisible(true);
    		}
    		elseif($root && $root->getId() == Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID) {
    			$root->setName(Mage::helper('catalog')->__('Root'));
    		}

    		Mage::register('root', $root);
    	}

    	return $root;
    }

    public function getRootByIds($ids)
    {
        $root = Mage::registry('root');
        if (null === $root) {
            $storeId = (int) $this->getRequest()->getParam('store');
            $pageTreeResource = Mage::getResourceSingleton('menuadminpro/menuadminpro_tree');
            $ids    = $pageTreeResource->getExistingPageIdsBySpecifiedIds($ids);
            $tree   = $pageTreeResource->loadByIds($ids);
            $rootId = Mage::getResourceModel('menuadminpro/menuadminpro')->getStoreRootId($storeId);
            if (! $rootId) {
                //$newRoot = $this->_createStoreRootPage($storeId);
                $rootId = 1;
                $newRoot->getId();
            }
            $root   = $tree->getNodeById($rootId);

            if (!$root) {
                Mage::throwException('Could not retrieve root page of store ' . $storeId);
            }

            $tree->addCollectionData($this->getItemCollection());
            Mage::register('root', $root);
        }
        return $root;
    }

    public function getNode($parentNodePage, $recursionLevel=2)
    {

        $tree = Mage::getResourceModel('menuadminpro/menuadminpro_tree');

        $nodeId     = $parentNodePage->getId();
        $parentId   = $parentNodePage->getParentId();

        $node = $tree->loadNode($nodeId);
        $node->loadChildren($recursionLevel);

        $tree->addCollectionData($this->getItemCollection());

        return $node;
    }

    public function getSaveUrl(array $args = array())
    {
        $params = array('_current'=>true);
        $params = array_merge($params, $args);
        return $this->getUrl('*/*/save', $params);
    }

    public function getEditUrl()
    {
        return $this->getUrl("*/*/edit", array('_current'=>true, 'store'=>$this->getRequest()->getParam('store'), '_query'=>false, 'id'=>null, 'parent'=>null));
    }

    public function getRootIds()
    {
        $ids = $this->getData('root_ids');
        if (is_null($ids)) {
            $ids = array();
            foreach (Mage::app()->getGroups() as $store) {
                $ids[] = $store->getRootPageId();
            }
            $this->setData('root_ids', $ids);
        }
        return $ids;
    }
}
