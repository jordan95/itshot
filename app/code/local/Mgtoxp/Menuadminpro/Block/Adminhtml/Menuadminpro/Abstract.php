<?php
class Mgtoxp_Menuadminpro_Block_Adminhtml_Menuadminpro_Abstract extends Mage_Adminhtml_Block_Template
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getItem()
    {
        return Mage::registry('item');
    }

    public function getItemId()
    {
        if ($this->getItem()) {
            return $this->getItem()->getId();
        }
        return Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID;
    }

    public function getItemName()
    {
        return $this->getItem()->getName();
    }

    public function getItemPath()
    {
        if ($this->getItem()) {
            return $this->getItem()->getPath();
        }
        return Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID;
    }

    public function hasStoreRootItem()
    {
        $root = $this->getRoot();
        if ($root && $root->getId()) {
            return true;
        }
        return false;
    }

    public function getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store');
        return Mage::app()->getStore($storeId);
    }

    public function getRoot($parentNodeItem=null, $recursionLevel=3)
    {
        if (!is_null($parentNodeItem) && $parentNodeItem->getId()) {
            return $this->getNode($parentNodeItem, $recursionLevel);
        }
        $root = Mage::registry('root');
        if (is_null($root)) {
            $storeId = (int) $this->getRequest()->getParam('store');

            if ($storeId) {
                $store = Mage::app()->getStore($storeId);
                $rootId = $store->getRootItemId();
            }
            else {
                $rootId = Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID;
            }

            $tree = Mage::getResourceSingleton('menuadminpro/menuadminpro_tree')
                ->load(null, $recursionLevel);

            if ($this->getItem()) {
                $tree->loadEnsuredNodes($this->getItem(), $tree->getNodeById($rootId));
            }

            $tree->addCollectionData($this->getItemCollection());

            $root = $tree->getNodeById($rootId);

            if ($root && $rootId != Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID) {
                $root->setIsVisible(true);
            }
            elseif($root && $root->getId() == Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID) {
                $root->setName(Mage::helper('catalog')->__('Root'));
            }

            Mage::register('root', $root);
        }

        return $root;
    }

    public function getRootByIds($ids)
    {
        $root = Mage::registry('root');
        if (null === $root) {
            $categoryTreeResource = Mage::getResourceSingleton('menuadminpro/menuadminpro_tree');
            $ids    = $itemTreeResource->getExistingCategoryIdsBySpecifiedIds($ids);
            $tree   = $itemTreeResource->loadByIds($ids);
            $rootId = Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID;
            $root   = $tree->getNodeById($rootId);
            if ($root && $rootId != Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID) {
                $root->setIsVisible(true);
            } else if($root && $root->getId() == Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID) {
                $root->setName(Mage::helper('catalog')->__('Root'));
            }

            $tree->addCollectionData($this->getItemCollection());
            Mage::register('root', $root);
        }
        return $root;
    }

    public function getNode($parentNodeItem, $recursionLevel=2)
    {
        $tree = Mage::getResourceModel('menuadminpro/menuadminpro_tree');

        $nodeId     = $parentNodeItem->getId();
        $parentId   = $parentNodeItem->getParentId();

        $node = $tree->loadNode($nodeId);
        $node->loadChildren($recursionLevel);

        if ($node && $nodeId != Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID) {
            $node->setIsVisible(true);
        } elseif($node && $node->getId() == Mgtoxp_Menuadminpro_Model_Menuadminpro::TREE_ROOT_ID) {
            $node->setName(Mage::helper('catalog')->__('Root'));
        }

        $tree->addCollectionData($this->getItemCollection());

        return $node;
    }

    public function getSaveUrl(array $args = array())
    {
        $params = array('_current'=>true);
        $params = array_merge($params, $args);
        return $this->getUrl('*/*/save', $params);
    }

    public function getEditUrl()
    {
        return $this->getUrl("*/adminhtml_menuadminpro/edit", array('_current'=>true, 'store'=>null, '_query'=>false, 'id'=>null, 'parent'=>null));
    }

    public function getRootIds()
    {
        $ids = $this->getData('root_ids');
        if (is_null($ids)) {
            $ids = array();
            foreach (Mage::app()->getGroups() as $store) {
                $ids[] = $store->getRootItemId();
            }
            $this->setData('root_ids', $ids);
        }
        return $ids;
    }
}
