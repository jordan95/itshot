<?php
class Mgtoxp_Menuadminpro_Block_Adminhtml_Menuadminpro_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    protected $_generalTabBlock = 'menuadminpro/adminhtml_menuadminpro_tab_general';

    public function __construct()
    {
        parent::__construct();
        $this->setId('menuadminpro_info_tabs');
        $this->setDestElementId('item_tab_content');
        $this->setTitle(Mage::helper('catalog')->__('Item Data'));
        $this->setTemplate('widget/tabshoriz.phtml');

    }

    public function getItem()
    {
        return Mage::registry('item');
    }

    public function getCatalogHelper()
    {
        return Mage::helper('adminhtml/catalog');
    }

    public function getGeneralTabBlock()
    {
        return $this->_generalTabBlock;
    }

    protected function _prepareLayout()
    {
		$item = Mage::registry('item');

		$level = $item->getLevel();
		$responsive = $item->getIsResponsive();
    	
    	$this->addTab('general', array(
    			'label'     => Mage::helper('catalog')->__('General'),
    			'content'   => $this->getLayout()->createBlock(
    					'menuadminpro/adminhtml_menuadminpro_tab_general',
    					'general'
    			)->toHtml(),
    	));
    	
    	if($level == 1):
	    	$this->addTab('design', array(
	    			'label'     => Mage::helper('catalog')->__('Design'),
	    			'content'   => $this->getLayout()->createBlock(
	    					'menuadminpro/adminhtml_menuadminpro_tab_design',
	    					'design'
	    			)->toHtml(),
	    	));
	    	if($responsive):
		    	$this->addTab('responsive', array(
		    			'label'     => Mage::helper('catalog')->__('Design responsive'),
		    			'content'   => $this->getLayout()->createBlock(
		    					'menuadminpro/adminhtml_menuadminpro_tab_responsive',
		    					'responsive'
		    			)->toHtml(),
		    	));
	    	endif;
	    	$this->addTab('css', array(
	    			'label'     => Mage::helper('catalog')->__('Custom CSS'),
	    			'content'   => $this->getLayout()->createBlock(
	    					'menuadminpro/adminhtml_menuadminpro_tab_css',
	    					'css'
	    			)->toHtml(),
	    	));
    	endif;
    	
    	if($level == 2):
    	
	    	$this->addTab('aditionals', array(
	    			'label'     => Mage::helper('catalog')->__('Dropdown'),
	    			'content'   => $this->getLayout()->createBlock(
	    					'menuadminpro/adminhtml_menuadminpro_tab_aditionals',
	    					'aditionals'
	    			)->toHtml(),
	    	));
	
	        $this->addTab('products', array(
	            'label'     => Mage::helper('catalog')->__('Item Products'),
	            'content'   => $this->getLayout()->createBlock(
	                'menuadminpro/adminhtml_menuadminpro_tab_product',
	                'item.product.grid'
	            )->toHtml(),
	        ));
        
        endif;

        return parent::_prepareLayout();
    }
}
