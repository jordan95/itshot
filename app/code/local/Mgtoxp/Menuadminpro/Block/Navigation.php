<?php
class Mgtoxp_Menuadminpro_Block_Navigation extends Mage_Core_Block_Template
{
    protected $_categoryInstance = null;
    protected $_currentCategoryKey;
    protected $_itemLevelPositions = array();

    protected function _construct()
    {
        $this->addData(array(
            'cache_lifetime'    => false,
            'cache_tags'        => array(Mage_Catalog_Model_Category::CACHE_TAG, Mage_Core_Model_Store_Group::CACHE_TAG),
        ));
    }

    public function getStoreCategories()
    {
        $helper = Mage::helper('catalog/category');
        return $helper->getStoreCategories();
    }

    public function getCurrentChildCategories()
    {
        $layer = Mage::getSingleton('catalog/layer');
        $category   = $layer->getCurrentCategory();
        $categories = $category->getChildrenCategories();
        $productCollection = Mage::getResourceModel('catalog/product_collection');
        $layer->prepareProductCollection($productCollection);
        $productCollection->addCountToCategories($categories);
        return $categories;
    }

    public function isCategoryActive($category)
    {
        if ($this->getCurrentCategory()) {
            return in_array($category->getId(), $this->getCurrentCategory()->getPathIds());
        }
        return false;
    }

    protected function _getCategoryInstance()
    {
        if (is_null($this->_categoryInstance)) {
            $this->_categoryInstance = Mage::getModel('catalog/category');
        }
        return $this->_categoryInstance;
    }

    public function getCategoryUrl($category)
    {
        if ($category instanceof Mage_Catalog_Model_Category) {
            $url = $category->getUrl();
        } else {
            $url = $this->_getCategoryInstance()
                ->setData($category->getData())
                ->getUrl();
        }

        return $url;
    }

    protected function _getItemPosition($level)
    {
        if ($level == 0) {
            $zeroLevelPosition = isset($this->_itemLevelPositions[$level]) ? $this->_itemLevelPositions[$level] + 1 : 1;
            $this->_itemLevelPositions = array();
            $this->_itemLevelPositions[$level] = $zeroLevelPosition;
        } elseif (isset($this->_itemLevelPositions[$level])) {
            $this->_itemLevelPositions[$level]++;
        } else {
            $this->_itemLevelPositions[$level] = 1;
        }

        $position = array();
        for($i = 0; $i <= $level; $i++) {
            if (isset($this->_itemLevelPositions[$i])) {
                $position[] = $this->_itemLevelPositions[$i];
            }
        }
        return implode('-', $position);
    }

    protected function _renderCategoryMenuItemHtml($category, $level = 0, $isLast = false, $isFirst = false,
        $isOutermost = false, $outermostItemClass = '', $childrenWrapClass = '', $noEventAttributes = false, $itemsperrow=5)
    {
        if (!$category->getIsActive()) {
            return '';
        }
        $html = array();

        // get all children
        if (Mage::helper('catalog/category_flat')->isEnabled()) {
            $children = (array)$category->getChildrenNodes();
            $childrenCount = count($children);
        } else {
            $children = $category->getChildren();
            $childrenCount = $children->count();
        }
        $hasChildren = ($children && $childrenCount);

        // select active children
        $activeChildren = array();
        foreach ($children as $child) {
            if ($child->getIsActive()) {
                $activeChildren[] = $child;
            }
        }
        $activeChildrenCount = count($activeChildren);
        $hasActiveChildren = ($activeChildrenCount > 0);

        // prepare list item html classes
        $classes = array();
        if ($this->isCategoryActive($category)) {
            $classes[] = 'active';
        }
        $linkClass = ' class="item-menu';
        if ($isOutermost && $outermostItemClass) {
            $classes[] = $outermostItemClass;
            $linkClass.= ' '.$outermostItemClass.'';
        }
        $linkClass.= '"';

        // prepare list item attributes
        $attributes = array();
        if (count($classes) > 0) {
            $attributes['class'] = implode(' ', $classes);
        }

        $_columnCount = $itemsperrow;

        $htmlwide[] = '<li class="menu-level-'.($level+2).'"';
        $htmlwideinline[] = '<li class="menu-level-'.($level+1).'"';
        $htmlsmall[] = '<li class="menu-level-'.($level+2).'"';
        if($level == 0):
        	$width = 100 / $_columnCount;
        	$htmlwide[] = 'style="width:'.$width.'%"';
        endif;
        $htmlwide[] = '>';
        $htmlwideinline[] = '>';
        $htmlsmall[] = '>';

        $htmlwide[] = '<a href="'.$this->getCategoryUrl($category).'"'.$linkClass.'>';
        $htmlwideinline[] = '<a href="'.$this->getCategoryUrl($category).'"'.$linkClass.'>';
        $htmlsmall[] = '<a href="'.$this->getCategoryUrl($category).'"'.$linkClass.'>';
        
        $htmlwide[] = '<span>' . $this->escapeHtml($category->getName()) .'</span>';
        $htmlwideinline[] = '<span>' . $this->escapeHtml($category->getName()) . '</span>';
        $htmlsmall[] = '<span>' . $this->escapeHtml($category->getName()) . '</span>';
        
        $htmlwide[] = '</a>';
        $htmlwideinline[] = '</a>';
        $htmlsmall[] = '</a>';

        // render children
        $htmlChildrens = array();
        $j = 0;
        foreach ($activeChildren as $child) {
            $htmlChildrens[] = $this->_renderCategoryMenuItemHtml(
                $child,
                ($level + 1),
                ($j == $activeChildrenCount - 1),
                ($j == 0),
                false,
                $outermostItemClass,
                $childrenWrapClass,
                $noEventAttributes,
            	$itemsperrow
            );
            $j++;
        }
        if (!empty($htmlChildrens)) {
        	$outwide = $outsmall = $outwideinline = '';
        	$cc = 0;
        	$_collectionSize = count($htmlChildrens);
        	foreach($htmlChildrens as $htmlChildren):
        		$outwide.= $htmlChildren['wide'];
        	
	        	if ($cc++%$_columnCount==0):
	        		$outwideinline.= '<li class="wrapper-row-menu "><a class="no-display-link"></a><ul class="alwaysshow">';
	        	endif;

        		$outwideinline.= $htmlChildren['wideinline'];
        		
        		if ($cc%$_columnCount==0 || $cc==$_collectionSize):
        			$outwideinline.= '</ul></li>';
        		endif;
        		
        		$outsmall.= $htmlChildren['small'];
        	endforeach;

        	$htmlwide[] = '<ul class="alwaysshow">';
        	if($level == 0):
        		$widthwrappercategories = 100;
        		$htmlwideinline[] = '<ul class="wrapper-menu-level-2"><li class="wrapper-rows-menu" style="width: '.$widthwrappercategories.'%"><a class="no-display-link"></a><ul class="alwaysshow">';
        	else:
        		$htmlwideinline[] = '<ul class="alwaysshow">';
        	endif;
        	
            $htmlsmall[] = '<ul class="">';
            
            $htmlwide[] = $outwide;
            $htmlwideinline[] = $outwideinline;
            $htmlsmall[] = $outsmall;
            
            $htmlwide[] = '</ul>';
            if($level == 0):
            	$htmlwideinline[] = '</ul></li></ul>';
            else:
            	$htmlwideinline[] = '</ul>';
            endif;

            $htmlsmall[] = '</ul>';
        }

        $htmlwide[] = '</li>';
        $htmlwideinline[] = '</li>';
        $htmlsmall[] = '</li>';

        $html['wide'] = implode("\n", $htmlwide);
        $html['wideinline'] = implode("\n", $htmlwideinline);
        $html['small'] = implode("\n", $htmlsmall);
        return $html;
    }

    public function drawItem($category, $level = 0, $last = false)
    {
        return $this->_renderCategoryMenuItemHtml($category, $level, $last);
    }

    public function getCurrentCategory()
    {
        if (Mage::getSingleton('catalog/layer')) {
            return Mage::getSingleton('catalog/layer')->getCurrentCategory();
        }
        return false;
    }

    public function getCurrentCategoryPath()
    {
        if ($this->getCurrentCategory()) {
            return explode(',', $this->getCurrentCategory()->getPathInStore());
        }
        return array();
    }

    public function drawOpenCategoryItem($category) {
        $html = '';
        if (!$category->getIsActive()) {
            return $html;
        }

        $html.= '<li';

        $html.= '>'."\n";
        $html.= '<a href="'.$this->getCategoryUrl($category).'"><span>'.$this->htmlEscape($category->getName()).'</span></a>'."\n";

        if (in_array($category->getId(), $this->getCurrentCategoryPath())){
            $children = $category->getChildren();
            $hasChildren = $children && $children->count();

            if ($hasChildren) {
                $htmlChildren = '';
                foreach ($children as $child) {
                    $htmlChildren.= $this->drawOpenCategoryItem($child);
                }

                if (!empty($htmlChildren)) {
                    $html.= '<ul>'."\n"
                            .$htmlChildren
                    		.'</ul>'."\n";
                }
            }
        }
        $html.= '</li>'."\n";
        return $html;
    }

    public function renderCategoriesMenuHtml($level = 1, $outermostItemClass = '', $childrenWrapClass = '', $itemsperrow=5)
    {
        $activeCategories = array();
        foreach ($this->getStoreCategories() as $child) {
            if ($child->getIsActive()) {
                $activeCategories[] = $child;
            }
        }
        $activeCategoriesCount = count($activeCategories);
        $hasActiveCategoriesCount = ($activeCategoriesCount > 0);

        if (!$hasActiveCategoriesCount) {
            return '';
        }

        $htmlwide = array();
        $html_small = array();
        $j = 0;
        $cc = 0;
        $_collectionSize = count($activeCategories);
        $_columnCount = $itemsperrow;
        foreach ($activeCategories as $category) {
        	if ($cc++%$_columnCount==0):
        		$htmlwide[] = '<li class="wrapper-row-menu "><a class="no-display-link"></a><ul class="alwaysshow">';
        	endif;

            $html = $this->_renderCategoryMenuItemHtml(
                $category,
                $level,
                ($j == $activeCategoriesCount - 1),
                ($j == 0),
                true,
                $outermostItemClass,
                $childrenWrapClass,
                true,
            	$itemsperrow
            );
            $htmlwide[] = $html['wide'];
            $htmlwideinline[] = $html['wideinline'];
            $htmlsmall[] = $html['small'];
            $j++;
            if ($cc%$_columnCount==0 || $cc==$_collectionSize):
            	$htmlwide[] = '</ul></li>';
            endif;

        }
        $html['wide'] = implode("\n", $htmlwide);
        $html['wideinline'] = implode("\n", $htmlwideinline);
        $html['small'] = implode("\n", $htmlsmall);
        return $html;
    }

}
