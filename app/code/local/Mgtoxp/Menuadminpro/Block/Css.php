<?php
class Mgtoxp_Menuadminpro_Block_Css extends Mgtoxp_Menuadminpro_Block_Abstract
{

    protected function _toHtml()
    {
        return parent::_toHtml();
    }

 	public function hasHome(){
    	return true;
    }

    public function getCss()
    {
    	$menu_data = $this->getMenuData('top');
    	$css = '';
    	if($menu_data){
    		foreach($menu_data['stores'] as $k => $store){

    			$responsive = $menu_data['settings'][$k]['is_responsive'];

    			if($responsive):
    				$css.= '
							#nav-horizontal {
								display: none !important;
							}

							@media all and (min-width: 770px) {
								ul.slimmenu {
									display:none !important;
								}
								#nav-horizontal {
									display: block !important;
								}
							}
    						';
    			endif;

    			$styles = unserialize($menu_data['settings'][$k]['styles']);
    			if(is_array($styles)):
    				$css.= '
    						#header-nav {
    							border: 0;
    						}
    						.nav-menu {
    							'. ((isset($styles['navheight'])) ? 'min-height: '.$styles['navheight'] . 'px;' : '') .'
							    '. ((isset($styles['navbackgroundcolor'])) ? 'background: '.$styles['navbackgroundcolor'] . ';' : '') .'
							    '. ((isset($styles['navborderwidth']) && isset($styles['navbordercolor'])) ? 'border: '.$styles['navborderwidth'].'px solid '.$styles['navbordercolor'] . ';' : '') .'
								'. ((isset($styles['navborderround'])) ? '-moz-border-radius: '.$styles['navborderround'] . ';' : '') .'
								'. ((isset($styles['navborderround'])) ? '-webkit-border-radius: '.$styles['navborderround'] . ';' : '') .'
								'. ((isset($styles['navborderround'])) ? 'border-radius: '.$styles['navborderround'] . ';' : '') .'
    						}

							.nav-menu li.menu-level-1 {
								'. ((isset($styles['navborderwidth']) && isset($styles['navbordercolor'])) ? 'border-right: '.$styles['navborderwidth'].'px solid '.$styles['navbordercolor'] . ';' : '') .'
							}

							.nav-menu li.menu-level-1 {
								'. ((isset($styles['navheight']) && isset($styles['navborderwidth'])) ? 'line-height: '. ($styles['navheight'] - $styles['navborderwidth'] * 2) . 'px;' : '') .'
							}

							.nav-menu li.menu-level-1 a.item-menu-1 {
    							'. ((isset($styles['navfontcolor'])) ? 'color: '.$styles['navfontcolor'] . ';' : '') .'
    							'. ((isset($styles['navfontsize'])) ? 'font-size: '.$styles['navfontsize'] . ';' : '') .'
    							'. ((isset($styles['navfontweight'])) ? 'font-weight: '.$styles['navfontweight'] . ';' : '') .'
    							'. ((isset($styles['navfonttransform'])) ? 'text-transform: '.$styles['navfonttransform'] . ';' : '') .'
    							padding: 0;
    							'. ((isset($styles['navpaddingitemleft'])) ? 'padding-left: '.$styles['navpaddingitemleft'] . ';' : '') .'
    							'. ((isset($styles['navpaddingitemright'])) ? 'padding-right: '.$styles['navpaddingitemright'] . ';' : '') .'
    						}

							.nav-menu li.menu-level-1 a.item-menu-1:hover,
    						.nav-menu li.menu-level-1 a.item-menu-1:focus,
    						.nav-menu li.menu-level-1 a.item-menu-1:active,
    						.menu-level-1 > a.open {
								'. ((isset($styles['navfontcolorhover'])) ? 'color: '.$styles['navfontcolorhover'] . ';' : '') .'
								'. ((isset($styles['navfontbackgroundhover'])) ? 'background: '.$styles['navfontbackgroundhover'] . ';' : '') .'
							}

							.nav-menu div.sub-nav {
								'. ((isset($styles['dropbackgroundcolor'])) ? 'background: '.$styles['dropbackgroundcolor'] . ';' : '') .'
							    '. ((isset($styles['dropborderwidth'])) ? 'border: '.$styles['dropborderwidth'].'px solid '.$styles['dropbordercolor'] . ';' : '') .'
								'. ((isset($styles['dropborderround'])) ? '-moz-border-radius: '.$styles['dropborderround'] . ';' : '') .'
								'. ((isset($styles['dropborderround'])) ? '-webkit-border-radius: '.$styles['dropborderround'] . ';' : '') .'
								'. ((isset($styles['dropborderround'])) ? 'border-radius: '.$styles['dropborderround'] . ';' : '') .'
								'. ((isset($styles['droppadding'])) ? 'padding: '.$styles['droppadding'] . ';' : '') .'

							}

							.nav-menu div.sub-nav.open {
								/*'. ((isset($styles['navheight']) && isset($styles['navborderwidth'])) ? 'top: '.($styles['navheight'] - $styles['navborderwidth'] * 2) . 'px;' : '') .'*/
							}

							.nav-menu a.item-menu-2 {
    							'. ((isset($styles['secfontcolor'])) ? 'color: '.$styles['secfontcolor'] . ';' : '') .'
    							'. ((isset($styles['secfontsize'])) ? 'font-size: '.$styles['secfontsize'] . ';' : '') .'
    							'. ((isset($styles['secfontweight'])) ? 'font-weight: '.$styles['secfontweight'] . ';' : '') .'
    							'. ((isset($styles['secfonttransform'])) ? 'text-transform: '.$styles['secfonttransform'] . ';' : '') .'
    							'. ((isset($styles['secpaddingitem'])) ? 'padding: '.$styles['secpaddingitem'] . ';' : '') .'
    							'. ((isset($styles['secfontbackground'])) ? 'background: '.$styles['secfontbackground'] . ';' : '') .'
							}

							.nav-menu a.item-menu-2:hover {
    							'. ((isset($styles['secfontcolorhover'])) ? 'color: '.$styles['secfontcolorhover'] . ';' : '') .'
    						}

							.nav-menu a.item-menu-3,
							.nav-menu a.item-menu-4,
							.nav-menu a.item-menu-5,
							.nav-menu a.item-menu-6,
							.nav-menu a.item-menu-7 {
    							'. ((isset($styles['tirfontcolor'])) ? 'color: '.$styles['tirfontcolor'] . ';' : '') .'
    							'. ((isset($styles['tirfontsize'])) ? 'font-size: '.$styles['tirfontsize'] . ';' : '') .'
    							'. ((isset($styles['tirfontweight'])) ? 'font-weight: '.$styles['tirfontweight'] . ';' : '') .'
    							'. ((isset($styles['tirfonttransform'])) ? 'text-transform: '.$styles['tirfonttransform'] . ';' : '') .'
    							'. ((isset($styles['tirpaddingitem'])) ? 'padding: '.$styles['tirpaddingitem'] . ';' : '') .'
							}

							.nav-menu a.item-menu-3:hover,
							.nav-menu a.item-menu-4:hover,
							.nav-menu a.item-menu-5:hover,
							.nav-menu a.item-menu-6:hover,
							.nav-menu a.item-menu-7:hover {
    							'. ((isset($styles['tirfontcolorhover'])) ? 'color: '.$styles['tirfontcolorhover'] . ';' : '') .'
							}


    						';
    			endif;

    			$stylesresponsive = unserialize($menu_data['settings'][$k]['stylesresponsive']);
    			if(is_array($stylesresponsive)):
    				$css.= '
							ul.slimmenu li {
								'. ((isset($stylesresponsive['itembgcolorfirst'])) ? 'background: '.$stylesresponsive['itembgcolorfirst'] . ';' : '') .'
							}

							ul.slimmenu li ul li {
								'. ((isset($stylesresponsive['itembgcolorother'])) ? 'background: '.$stylesresponsive['itembgcolorother'] . ';' : '') .'
							}

							ul.slimmenu li a:hover {
								'. ((isset($stylesresponsive['itembgcolorhover'])) ? 'background: '.$stylesresponsive['itembgcolorhover'] . ';' : '') .'
							}

							ul.slimmenu li a {
								'. ((isset($stylesresponsive['itemfontcolor'])) ? 'color: '.$stylesresponsive['itemfontcolor'] . ';' : '') .'
    							'. ((isset($stylesresponsive['itemfontsize'])) ? 'font-size: '.$stylesresponsive['itemfontsize'] . ';' : '') .'
    							'. ((isset($stylesresponsive['itemfontweight'])) ? 'font-weight: '.$stylesresponsive['itemfontweight'] . ';' : '') .'
    							'. ((isset($stylesresponsive['itemfonttransform'])) ? 'text-transform: '.$stylesresponsive['itemfonttransform'] . ';' : '') .'
    							'. ((isset($stylesresponsive['itemheight'])) ? 'height: '. $stylesresponsive['itemheight'] . 'px;' : '') .'
    							'. ((isset($stylesresponsive['itemheight'])) ? 'line-height: '. $stylesresponsive['itemheight'] . 'px;' : '') .'
							}

    						ul.slimmenu.collapsed li a {
    							'. ((isset($stylesresponsive['itemborderwidth']) && isset($stylesresponsive['itembordercolor'])) ? 'border: '.$stylesresponsive['itemborderwidth'].'px solid '.$stylesresponsive['itembordercolor'] . ';' : '') .'
    						}

    						ul.slimmenu.collapsed li .sub-collapser {
    							'. ((isset($stylesresponsive['itemheight']) && isset($stylesresponsive['itemborderwidth'])) ? 'height: '. ($stylesresponsive['itemheight'] - $stylesresponsive['itemborderwidth'] * 2) .'px;' : '') .'
    						}

    						ul.slimmenu li .sub-collapser {
    							'. ((isset($stylesresponsive['itembgcolorhover'])) ? 'background: '.$stylesresponsive['itembgcolorhover'] . ';' : '') .'
    						}
    						ul.slimmenu li .sub-collapser > i {
    							'. ((isset($stylesresponsive['itemfontcolor'])) ? 'color: '.$stylesresponsive['itemfontcolor'] . ';' : '') .'
    						}

    						.menu-collapser {
    							'. ((isset($stylesresponsive['colbgcolor'])) ? 'background-color: '.$stylesresponsive['colbgcolor'] . ';' : '') .'
								'. ((isset($stylesresponsive['colfontcolor'])) ? 'color: '.$stylesresponsive['colfontcolor'] . ';' : '') .'
    							'. ((isset($stylesresponsive['colfontsize'])) ? 'font-size: '.$stylesresponsive['colfontsize'] . ';' : '') .'
    							'. ((isset($stylesresponsive['colfontweight'])) ? 'font-weight: '.$stylesresponsive['colfontweight'] . ';' : '') .'
    							'. ((isset($stylesresponsive['colfonttransform'])) ? 'text-transform: '.$stylesresponsive['colfonttransform'] . ';' : '') .'
    						}

    						.collapse-button {
    							'. ((isset($stylesresponsive['colbuttonbgcolor'])) ? 'background-color: '.$stylesresponsive['colbuttonbgcolor'] . ';' : '') .'
    						}

    						.collapse-button .icon-bar {
    							'. ((isset($stylesresponsive['colbuttonfontcolor'])) ? 'background-color: '.$stylesresponsive['colbuttonfontcolor'] . ';' : '') .'
    						}


    						';
    			endif;

    			if(isset($menu_data['settings'][$k]['customcss']) && !empty($menu_data['settings'][$k]['customcss'])):
    				$css.= $menu_data['settings'][$k]['customcss'];
    			endif;

    		}
    	}
    	$block = $this->getLayout()->createBlock('core/text', 'menuadminpro.css.code')->setText('<style>'.$css.'</style>');
    	$this->getLayout()->getBlock('head')->append($block);
    }
}