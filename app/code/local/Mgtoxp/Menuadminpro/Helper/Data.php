<?php
class Mgtoxp_Menuadminpro_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function recursiveReplace($search, $replace, $subject)
	{
		if(!is_array($subject))
			return $subject;
	
		foreach($subject as $key => $value)
		if(is_string($value))
			$subject[$key] = str_replace($search, $replace, $value);
		elseif(is_array($value))
		$subject[$key] = self::recursiveReplace($search, $replace, $value);
	
		return $subject;
	}
	/*
	public function getSkinCss(){
		$region = 'top';
		$skin = 'default';
		$store_id = Mage::app()->getStore()->getId();
		$root_item = Mage::getModel('menuadminpro/menuadminpro')->getRootStoreItem($store_id, $region);
		if(empty($root_item['settings'])){
			$root_item = Mage::getModel('menuadminpro/menuadminpro')->getRootStoreItem(0, $region);
		}

		if($root_item){
			foreach($root_item['stores'] as $k => $store){
				$skin = $root_item['settings'][$k]['skin'];
			}
		}
		return 'mgtoxp/menuadminpro/css/skins/'.$skin.'.css';
	}
	
	public function getSkinCssVertical(){
		$region = 'top';
		$skin = 'default';
		$store_id = Mage::app()->getStore()->getId();
		$root_item = Mage::getModel('menuadminpro/menuadminpro')->getRootStoreItem($store_id, $region);
		if(empty($root_item['settings'])){
			$root_item = Mage::getModel('menuadminpro/menuadminpro')->getRootStoreItem(0, $region);
		}
		
		if($root_item){
			foreach($root_item['stores'] as $k => $store){
				$skin = $root_item['settings'][$k]['skinvertical'];
			}
		}
		return 'mgtoxp/menuadminpro/css/skinsvertical/'.$skin.'.css';
	}
	*/
}