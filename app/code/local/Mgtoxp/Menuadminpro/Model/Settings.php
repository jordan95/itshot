<?php
class Mgtoxp_Menuadminpro_Model_Settings extends Varien_Object
{

    static public function getWidthArray()
    {
        return array(
        	'true'    => Mage::helper('menuadminpro')->__('Full Width'),
        	'false'    => Mage::helper('menuadminpro')->__('Auto Width')
        );
    }
    
    static public function getSpeedArray()
    {
    	return array(
        	'fast'    => Mage::helper('menuadminpro')->__('Fast'),
        	'slow'    => Mage::helper('menuadminpro')->__('Slow')
    	);
    }
    
    static public function getActivationArray()
    {
    	return array(
    			'hover'    => Mage::helper('menuadminpro')->__('Over'),
    			'click'    => Mage::helper('menuadminpro')->__('Click')
    	);
    }
    
    static public function getShowmethodArray()
    {
    	return array(
    			'slide'    => Mage::helper('menuadminpro')->__('Slide'),
    			'fade'    => Mage::helper('menuadminpro')->__('Fade')
    	);
    }
    
    static public function getSkinArray()
    {
    	return array(
    			'default'    => Mage::helper('menuadminpro')->__('Default Theme'),
    			'defaultwide'    => Mage::helper('menuadminpro')->__('Default Theme Wide'),
    			'rws'    => Mage::helper('menuadminpro')->__('RWS Theme'),
    			'modern'    => Mage::helper('menuadminpro')->__('Modern Theme'),
    			'custom'    => Mage::helper('menuadminpro')->__('Custom Theme'),
    			'grey'    => Mage::helper('menuadminpro')->__('Grey Theme')
    	);
    }
    
    static public function getSkinVerticalArray()
    {
    	return array(
    			'default'    => Mage::helper('menuadminpro')->__('Default Theme'),
    			'graphite'    => Mage::helper('menuadminpro')->__('Graphite Theme'),
    			'blue'    => Mage::helper('menuadminpro')->__('Blue Theme'),
    			'black'    => Mage::helper('menuadminpro')->__('Black Theme'),
    			'custom'    => Mage::helper('menuadminpro')->__('Custom Theme')
    	);
    }
}