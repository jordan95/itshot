<?php
class Mgtoxp_Menuadminpro_Model_Type extends Varien_Object
{

	const TYPE_NORMAL			= 1;
    const TYPE_PAGE				= 2;
    const TYPE_CATEGORY			= 3;
    const TYPE_INLINE			= 4;
    const TYPE_SUBITEM			= 5;
    const TYPE_ALLASSUBITEM		= 6;
    const TYPE_NOLINK			= 7;
    const TYPE_HOME				= 8;
    

    static public function getOptionArray()
    {
        return array(
        self::TYPE_NORMAL    => Mage::helper('menuadminpro')->__('Normal'),
	    self::TYPE_PAGE    => Mage::helper('menuadminpro')->__('To CMS Page'),
	    self::TYPE_CATEGORY    => Mage::helper('menuadminpro')->__('To Category'),
        self::TYPE_INLINE   => Mage::helper('menuadminpro')->__('Insert Categories Inline'),
	    //self::TYPE_SUBITEM   => Mage::helper('menuadminpro')->__('Insert Categories Branch as Subitems'),
        self::TYPE_ALLASSUBITEM   => Mage::helper('menuadminpro')->__('Insert ALL Categories as Subitems'),
	    self::TYPE_NOLINK    => Mage::helper('menuadminpro')->__('NO Link'),
	    self::TYPE_HOME    => Mage::helper('menuadminpro')->__('Home')
        );
    }
}