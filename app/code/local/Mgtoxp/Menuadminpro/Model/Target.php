<?php
class Mgtoxp_Menuadminpro_Model_Target extends Varien_Object
{
    const TARGET_SELF	= 'self';
    const TARGET_BLANK	= '_blank';

    static public function getOptionArray()
    {
        return array(
            self::TARGET_SELF    => Mage::helper('menuadminpro')->__('Self'),
            self::TARGET_BLANK   => Mage::helper('menuadminpro')->__('New window')
        );
    }
}