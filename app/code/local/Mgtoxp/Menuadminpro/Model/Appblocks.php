<?php
class Mgtoxp_Menuadminpro_Model_Appblocks extends Varien_Object
{

    static public function getOptionArray()
    {
        return array(
        	''    => Mage::helper('menuadminpro')->__('Please select a APP'),
            'newsletter'    => Mage::helper('menuadminpro')->__('Newsletter'),
            'contacts'   => Mage::helper('menuadminpro')->__('Contact Form'),
        	'login'    => Mage::helper('menuadminpro')->__('Mini Login'),
        	'custom'    => Mage::helper('menuadminpro')->__('Custom Core Template')
        );
    }
}