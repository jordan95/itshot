<?php
class Mgtoxp_Menuadminpro_Model_Mysql4_Tree extends Varien_Data_Tree_Dbp
{
    const ID_FIELD    = 'item_id';
    const PATH_FIELD  = 'path';
    const ORDER_FIELD = 'position';
    const LEVEL_FIELD = 'level';


    protected $_collection;
    protected $_storeId = null;

    /**
     * Initialize tree
     *
     */
    public function __construct()
    {
        $resource = Mage::getSingleton('core/resource');

        parent::__construct(
            $resource->getConnection('catalog_write'),
            $resource->getTableName('menuadminproitem'),
            array(
                Varien_Data_Tree_Dbp::ID_FIELD       => 'item_id',
                Varien_Data_Tree_Dbp::PATH_FIELD     => 'path',
                Varien_Data_Tree_Dbp::ORDER_FIELD    => 'position',
                Varien_Data_Tree_Dbp::LEVEL_FIELD    => 'level',
            )
        );
    }

    public function setStoreId($storeId)
    {
        $this->_storeId = (int) $storeId;
        return $this;
    }

    public function getStoreId()
    {
        if ($this->_storeId === null) {
            $this->_storeId = Mage::app()->getStore()->getId();
        }
        return $this->_storeId;
    }

    public function addCollectionData($collection=null, $sorted=false, $exclude=array(), $toLoad=true, $onlyActive = false)
    {
        if (is_null($collection)) {
            $collection = $this->getCollection($sorted);
        } else {
            $this->setCollection($collection);
        }

        if (!is_array($exclude)) {
            $exclude = array($exclude);
        }

        $nodeIds = array();
        foreach ($this->getNodes() as $node) {
            if (!in_array($node->getId(), $exclude)) {
                $nodeIds[] = $node->getId();
            }
        }
        $collection->addIdFilter($nodeIds);
        if ($onlyActive) {

            $disabledIds = $this->_getDisabledIds($collection);
            if ($disabledIds) {
                $collection->addFieldToFilter('item_id', array('nin'=>$disabledIds));
            }
        }

        if($toLoad) {
            $collection->load();

            foreach ($collection as $item) {
                if ($this->getNodeById($item->getId())) {
                    $this->getNodeById($item->getId())
                        ->addData($item->getData());
                }
            }

            foreach ($this->getNodes() as $node) {
                if (!$collection->getItemById($node->getId()) && $node->getParent()) {
                    $this->removeNode($node);
                }
            }
        }

        return $this;
    }

    public function getInactiveItemIds()
    {
        if (!is_array($this->_inactiveItemIds)) {
            $this->_initInactiveItemIds();
        }

        return $this->_inactiveItemIds;
    }

    protected function _getDisabledIds($collection)
    {
        $storeId = Mage::app()->getStore()->getId();

        $this->_inactiveItems = $this->getInactiveCategoryIds();


        $this->_inactiveItems = array_merge(
            $this->_getInactiveItemIds($collection, $storeId),
            $this->_inactiveItems
        );


        $allIds = $collection->getAllIds();
        $disabledIds = array();

        foreach ($allIds as $id) {
            $parents = $this->getNodeById($id)->getPath();
            foreach ($parents as $parent) {
                if (!$this->_getItemIsActive($parent->getId(), $storeId)){
                    $disabledIds[] = $id;
                    continue;
                }
            }
        }
        return $disabledIds;
    }

    protected function _getItemIsActive($id)
    {
        if (!in_array($id, $this->_inactiveItems)) {
            return true;
        }
        return false;
    }

    public function getCollection($sorted = false)
    {
        if (is_null($this->_collection)) {
            $this->_collection = $this->_getDefaultCollection($sorted);
        }
        return $this->_collection;
    }

    public function setCollection($collection)
    {
        if (!is_null($this->_collection)) {
            destruct($this->_collection);
        }
        $this->_collection = $collection;
        return $this;
    }

    protected function _getDefaultCollection($sorted = false)
    {
    	$collection = Mage::getModel('menuadminpro/menuadminpro')->getCollection();
    
    	if ($sorted) {
    		if (is_string($sorted)) {
    			// $sorted is supposed to be attribute name
    			$collection->addAttributeToSort($sorted);
    		} else {
    			$collection->addAttributeToSort('name');
    		}
    	}
    
    	return $collection;

    }
    
    public function getChildrenCount($itemId)
    {
    	$select = $this->_getReadAdapter()->select()
    	->from($this->getTable('menuadminpro/menuadminpro'), 'children_count')
    	->where('item_id=?', $itemId);
    
    	$child = $this->_getReadAdapter()->fetchOne($select);
    
    	return $child;
    }

    public function loadByIds($ids, $addCollectionData = true, $updateAnchorProductCount = true)
    {
        $levelField = $this->_conn->quoteIdentifier('level');
        $pathField  = $this->_conn->quoteIdentifier('path');
        if (empty($ids)) {
            $select = $this->_conn->select()
                ->from($this->_table, 'item_id')
                ->where($levelField . ' <= 2');
            $ids = $this->_conn->fetchCol($select);
        }
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        foreach ($ids as $key => $id) {
            $ids[$key] = (int)$id;
        }

        $select = $this->_conn->select()
            ->from($this->_table, array('path', 'level'))
            ->where('item_id IN (?)', $ids);
        $where = array($levelField . '=0' => true);

        foreach ($this->_conn->fetchAll($select) as $item) {
            $pathIds  = explode('/', $item['path']);
            $level = (int)$item['level'];
            while ($level > 0) {
                $pathIds[count($pathIds) - 1] = '%';
                $path = implode('/', $pathIds);
                $where["$levelField=$level AND $pathField LIKE '$path'"] = true;
                array_pop($pathIds);
                $level--;
            }
        }
        $where = array_keys($where);
        $select->where(implode(' OR ', $where));
        $arrNodes = $this->_conn->fetchAll($select);
        if (!$arrNodes) {
            return false;
        }
        if ($updateAnchorProductCount) {
            $this->_updateAnchorProductCount($arrNodes);
        }
        $childrenItems = array();
        foreach ($arrNodes as $key => $nodeInfo) {
            $pathToParent = explode('/', $nodeInfo[$this->_pathField]);
            array_pop($pathToParent);
            $pathToParent = implode('/', $pathToParent);
            $childrenItems[$pathToParent][] = $nodeInfo;
        }
        $this->addChildNodes($childrenItems, '', null);
        return $this;
    }

    public function loadBreadcrumbsArray($path, $addCollectionData = true, $withRootNode = false)
    {
    	$pathIds = explode('/', $path);
    	if (!$withRootNode) {
    		array_shift($pathIds);
    	}
    	$result = array();
    	return $result;
    }
}
