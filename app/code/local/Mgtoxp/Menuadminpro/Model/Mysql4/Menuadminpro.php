<?php
class Mgtoxp_Menuadminpro_Model_Mysql4_Menuadminpro extends Mage_Core_Model_Mysql4_Abstract
{
	
	protected $_storeId                  = null;
	
    public function _construct()
    {
        $this->_init('menuadminpro/menuadminpro', 'item_id');
    }
    
    public function setStoreId($storeId)
    {
    	$this->_storeId = $storeId;
    	return $this;
    }
    
    public function getStoreId()
    {
    	if ($this->_storeId === null) {
    		return Mage::app()->getStore()->getId();
    	}
    	return $this->_storeId;
    }
    
    public function getStoreRootId($storeId)
    {
    	$select = $this->_getReadAdapter()->select()
    	->from($this->getTable('menuadminpro/menuadminpro'), 'item_id')
    	->where('parent_id = 0')
    	->where('store_id = ?', $storeId);
    
    	return $this->_getReadAdapter()->fetchOne($select);
    }
    
    public function getChildrenCount($itemId)
    {
    	$select = $this->_getReadAdapter()->select()
    	->from($this->getTable('menuadminpro/menuadminpro'), 'children_count')
    	->where('item_id = :item_id');
    	$bind = array('item_id' => $itemId);
    
    	return $this->_getReadAdapter()->fetchOne($select, $bind);
    }
    
    protected function _getMaxPosition($path)
    {
    	$adapter = $this->getReadConnection();
    	$positionField = $adapter->quoteIdentifier('position');
    	$level   = count(explode('/', $path));
    	$bind = array(
    			'c_level' => $level,
    			'c_path'  => $path . '/%'
    	);
    	$select  = $adapter->select()
    	->from($this->getTable('menuadminpro/menuadminpro'), 'MAX(' . $positionField . ')')
    	->where($adapter->quoteIdentifier('path') . ' LIKE :c_path')
    	->where($adapter->quoteIdentifier('level') . ' = :c_level');
    
    	$position = $adapter->fetchOne($select, $bind);
    	if (!$position) {
    		$position = 0;
    	}
    	return $position;
    }
    
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
    	if ($object->getId()) {
    		$stores = $this->lookupStoreIds($object->getId());
    		
    		$object->setData('stores', $stores);
   		
    		$settings = $this->menuSettings($object->getId());
    		
    		$styles = unserialize($settings['styles']);
    		if(is_array($styles)):
    			$settings = array_merge($settings, $styles);
    		endif;
    		
    		$stylesresponsive = unserialize($settings['stylesresponsive']);
    		if(is_array($stylesresponsive)):
    			$settings = array_merge($settings, $stylesresponsive);
    		endif;

    		if($settings){
	    		foreach($settings as $option => $value)
	    		{
	    			if($option != 'store_id'){
	    				$object->setData($option, $value);
	    			}	
	    		}
    		}
    		
    	}
    
    	return parent::_afterLoad($object);
    }
    
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
    	parent::_beforeSave($object);
    
    	if (!$object->getChildrenCount()) {
    		$object->setChildrenCount(0);
    	}
    	if ($object->getLevel() === null) {
    		$object->setLevel(1);
    	}
    	
    	$post = Mage::app()->getRequest()->getPost();
    	if(isset($post['design'])):
    		$post['design'] = array_map("trim", $post['design']);
	    	$styles = serialize($post['design']);
	    	$object->setStyles($styles);
    	endif;

    	if(isset($post['responsive'])):
    		$post['responsive'] = array_map("trim", $post['responsive']);
	    	$stylesresponsive = serialize($post['responsive']);
	    	$object->setStylesresponsive($stylesresponsive);
    	endif;
    
    	if (!$object->getId()) {
    		$object->setPosition($this->_getMaxPosition($object->getPath()) + 1);
    		$path  = explode('/', $object->getPath());
    		$level = count($path);
    		$object->setLevel($level);
    		if ($level) {
    			$object->setParentId($path[$level - 1]);
    		}
    		$object->setPath($object->getPath() . '/');
    
    		$toUpdateChild = explode('/',$object->getPath());
    
    		$this->_getWriteAdapter()->update(
    				$this->getTable('menuadminpro/menuadminpro'),
    				array('children_count'  => new Zend_Db_Expr('children_count+1')),
    				array('item_id IN(?)' => $toUpdateChild)
    		);
    
    	}
    	return $this;
    }
    
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {

    	if (substr($object->getPath(), -1) == '/') {
    		$object->setPath($object->getPath() . $object->getId());
    		$this->_savePath($object);
    	}

    	// update related table
    	$newStores = (array)$object->getStores();
    	if(!empty($newStores)){
	    	$oldStores = $this->lookupStoreIds($object->getId());
	    	$table  = $this->getTable('menuadminpro_store');
	    	if ($oldStores) {
	    		$where = array(
	    				'item_id = ?'     => (int) $object->getId(),
	    				'store_id IN (?)' => $oldStores,
	    		);
	    	
	    		$this->_getWriteAdapter()->delete($table, $where);
	    	}

	    	if ($newStores) {
	    		$data = array();
	    		foreach ($newStores as $storeId) {
	    			$data[] = array(
	    					'item_id'  => (int) $object->getId(),
	    					'store_id' => (int) $storeId,
	    					'region' => $object->getRegion(),
	    					'is_active' => (int) $object->getIsActive(),
	    					'styles' => $object->getStyles(),
	    					'stylesresponsive' => $object->getStylesresponsive(),
	    					'itemsperrow' => (int) $object->getItemsperrow(),
	    					'distributeitems' => (int) $object->getDistributeitems(),
	    					'customcss' => $object->getCustomcss(),
	    					'is_responsive' => $object->getIsResponsive(),
	    					'menubutton' => $object->getMenubutton(),
	    					'menutitle' => $object->getMenutitle()
	    			);
	    		}
	    	
	    		$this->_getWriteAdapter()->insertMultiple($table, $data);
	    	}
    	}

    	return parent::_afterSave($object);
    }
    
    protected function _savePath($object)
    {
    	if ($object->getId()) {
    		$this->_getWriteAdapter()->update(
    				$this->getTable('menuadminpro/menuadminpro'),
    				array('path' => $object->getPath()),
    				array('item_id = ?' => $object->getId())
    		);
    	}
    	return $this;
    }
    

    protected function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
    	parent::_beforeDelete($object);
    
    	/**
    	 * Update children count for all parent categories
    	*/
    	$parentIds = $object->getParentIds();
    	
    	if ($parentIds) {
    		$childDecrease = 1;//$object->getChildrenCount() + 1; // +1 is itself
    		
    		$data = array('children_count' => new Zend_Db_Expr('children_count - ' . $childDecrease));
    		$where = array('item_id IN(?)' => $parentIds);
    		$this->_getWriteAdapter()->update( $this->getTable('menuadminpro/menuadminpro'), $data, $where);
    	}

    	$this->deleteChildren($object);
    	return $this;
    }
    
    public function deleteChildren(Mage_Core_Model_Abstract $object)
    {
    	$adapter = $this->_getWriteAdapter();
    	$pathField = $adapter->quoteIdentifier('path');
    
    	$select = $adapter->select()
    	->from($this->getTable('menuadminpro/menuadminpro'), array('item_id'))
    	->where($pathField . ' LIKE :c_path');
    
    	$childrenIds = $adapter->fetchCol($select, array('c_path' => $object->getPath() . '/%'));
    
    	if (!empty($childrenIds)) {
    		$adapter->delete(
    				$this->getTable('menuadminpro/menuadminpro'),
    				array('item_id IN (?)' => $childrenIds)
    		);
    	}
    
    	/**
    	 * Add deleted children ids to object
    	 * This data can be used in after delete event
    	 */
    	$object->setDeletedChildrenIds($childrenIds);
    	return $this;
    }
    
    public function getChildrenAmount($category, $isActiveFlag = true)
    {
    	$storeId = Mage::app()->getStore()->getId();
    	$table   = $this->getTable(array($this->getEntityTablePrefix(), 'int'));
    	$adapter = $this->_getReadAdapter();
    	$bind = array(
    			'store_id'     => $storeId,
    			'active_flag'  => $isActiveFlag,
    			'c_path'       => $category->getPath() . '/%'
    	);
    	$select = $adapter->select()
    	->from(array('m' => $this->getEntityTable()), array('COUNT(m.item_id)'))
    	->where('m.path LIKE :c_path');
    
    	return $this->_getReadAdapter()->fetchOne($select, $bind);
    }
    
    public function changeParent(Mgtoxp_Menuadminpro_Model_Menuadminpro $item, Mgtoxp_Menuadminpro_Model_Menuadminpro $newParent,
    		$afterItemId = null)
    {
    	$childrenCount  = $this->getChildrenCount($item->getId()) + 1;
    	$table          = $this->getTable('menuadminpro/menuadminpro');
    	$adapter        = $this->_getWriteAdapter();
    	$levelFiled     = $adapter->quoteIdentifier('level');
    	$pathField      = $adapter->quoteIdentifier('path');
    
    	/**
    	 * Decrease children count for all old item parent categories
    	*/
    	$adapter->update(
    			$table,
    			array('children_count' => new Zend_Db_Expr('children_count - ' . $childrenCount)),
    			array('item_id IN(?)' => $item->getParentIds())
    	);
    
    	/**
    	 * Increase children count for new item parents
    	*/
    	$adapter->update(
    			$table,
    			array('children_count' => new Zend_Db_Expr('children_count + ' . $childrenCount)),
    			array('item_id IN(?)' => $newParent->getPathIds())
    	);
    
    	$position = $this->_processPositions($item, $newParent, $afterItemId);
    
    	$newPath          = sprintf('%s/%s', $newParent->getPath(), $item->getId());
    	$newLevel         = $newParent->getLevel() + 1;
    	$levelDisposition = $newLevel - $item->getLevel();
    
    	/**
    	 * Update children nodes path
    	*/
    	$adapter->update(
    			$table,
    			array(
    					'path' => new Zend_Db_Expr('REPLACE(' . $pathField . ','.
    							$adapter->quote($item->getPath() . '/'). ', '.$adapter->quote($newPath . '/').')'
    					),
    					'level' => new Zend_Db_Expr( $levelFiled . ' + ' . $levelDisposition)
    			),
    			array($pathField . ' LIKE ?' => $item->getPath() . '/%')
    	);
    	/**
    	 * Update moved item data
    	*/
    	$data = array(
    			'path'      => $newPath,
    			'level'     => $newLevel,
    			'position'  =>$position,
    			'parent_id' =>$newParent->getId()
    	);
    	$adapter->update($table, $data, array('item_id = ?' => $item->getId()));
    	$item->addData($data);
    
    	return $this;
    }
    
    protected function _processPositions($item, $newParent, $afterItemId)
    {
    	$table          = $this->getTable('menuadminpro/menuadminpro');
    	$adapter        = $this->_getWriteAdapter();
    	$positionField  = $adapter->quoteIdentifier('position');
    
    	$bind = array(
    			'position' => new Zend_Db_Expr($positionField . ' - 1')
    	);
    	$where = array(
    			'parent_id = ?'         => $item->getParentId(),
    			$positionField . ' > ?' => $item->getPosition()
    	);
    	$adapter->update($table, $bind, $where);
    
    	/**
    	 * Prepare position value
    	*/
    	if ($afterItemId) {
    		$select = $adapter->select()
    		->from($table,'position')
    		->where('item_id = :item_id');
    		$position = $adapter->fetchOne($select, array('item_id' => $afterItemId));
    
    		$bind = array(
    				'position' => new Zend_Db_Expr($positionField . ' + 1')
    		);
    		$where = array(
    				'parent_id = ?' => $newParent->getId(),
    				$positionField . ' > ?' => $position
    		);
    		$adapter->update($table,$bind,$where);
    	} elseif ($afterItemId !== null) {
    		$position = 0;
    		$bind = array(
    				'position' => new Zend_Db_Expr($positionField . ' + 1')
    		);
    		$where = array(
    				'parent_id = ?' => $newParent->getId(),
    				$positionField . ' > ?' => $position
    		);
    		$adapter->update($table,$bind,$where);
    	} else {
    		$select = $adapter->select()
    		->from($table,array('position' => new Zend_Db_Expr('MIN(' . $positionField. ')')))
    		->where('parent_id = :parent_id');
    		$position = $adapter->fetchOne($select, array('parent_id' => $newParent->getId()));
    	}
    	$position += 1;
    
    	return $position;
    }
    
    public function lookupStoreIds($id)
    {
    	$adapter = $this->_getReadAdapter();
    
    	$select  = $adapter->select()
    	->from($this->getTable('menuadminpro_store'), array('store_id'))
    	->where('item_id = :item_id');
    
    	$binds = array(
    			':item_id' => (int) $id
    	);
    	return $adapter->fetchCol($select, $binds);
    }
    
    public function menuSettings($id)
    {
    	$adapter = $this->_getReadAdapter();
    
    	$select  = $adapter->select()
    	->from($this->getTable('menuadminpro_store'), array('store_id', 'region', 'is_active', 'styles', 'stylesresponsive', 'customcss', 'itemsperrow', 'distributeitems', 'is_responsive', 'menubutton', 'menutitle'))
    	->where('item_id = :item_id');
    
    	$binds = array(
    			':item_id' => (int) $id
    	);

    	return $adapter->fetchRow($select, $binds);
    }
}