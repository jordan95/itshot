<?php
class Mgtoxp_Menuadminpro_Model_Menuadminpro extends Mage_Core_Model_Abstract
{
	const TREE_ROOT_ID = 1;
	const DEFAULT_STORE_ID = 0;
	
    public function _construct()
    {
        parent::_construct();
        $this->_init('menuadminpro/menuadminpro');
    }
    
    public function getRootStoreItem($store_id, $region)
    {
    	$table = Mage::getSingleton('core/resource')->getTableName(array('menuadminproitem', 'store'));
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select  = $adapter->select()
            ->from($table, array('item_id', 'store_id', 'styles', 'stylesresponsive', 'itemsperrow', 'distributeitems', 'customcss', 'is_responsive', 'menubutton', 'menutitle'))
            ->where('store_id = :store_id')
        	->where('region = :region')
        	->where('is_active = :isactive');
        $binds = array(
            ':store_id' => (int) $store_id,
        	':region' => $region,
        	':isactive' => (int) 1,
        );
        $out['settings'] = $adapter->fetchAll($select, $binds);
        $out['stores'] = $adapter->fetchCol($select, $binds);
        return $out;
    }
    
    public function getPathIds()
    {
    	$ids = $this->getData('path_ids');
    	if (is_null($ids)) {
    		$ids = explode('/', $this->getPath());
    		$this->setData('path_ids', $ids);
    	}
    	return $ids;
    }
    
    public function getProductsPosition(){
    	$product = array();
    	$selected = $this->getMenuadminproProducts();
        if(!empty($selected)){
        	foreach(explode('&', $selected) as $value){
        		$parts = explode('=', $value);
        		$product[$parts[0]] = $parts[1];
        	}
        }
    	
    	return $product;
    }
    
    public function isDeleteable()
    {
    	if($this->getIsDeleteable()){
    		return false;
    	}
    	return true;
    }
    
    public function isReadonly()
    {
    	if($this->getIsReadonly()){
    		return true;
    	}
    	return false;
    }
    
    public function getParentIds()
    {
    	return array_diff($this->getPathIds(), array($this->getId()));
    }
    
    public function setStoreId($storeId)
    {
    	if (!is_numeric($storeId)) {
    		$storeId = Mage::app($storeId)->getStore()->getId();
    	}
    	$this->setData('store_id', $storeId);
    	$this->getResource()->setStoreId($storeId);
    	return $this;
    }
    
    public function getTreeModel()
    {
    	return Mage::getResourceModel('menuadminpro/mysql4_tree');
    }
    
    public function getTreeModelInstance()
    {
    	if (is_null($this->_treeModel)) {
    		$this->_treeModel = Mage::getResourceSingleton('menuadminpro/mysql4_menuadminpro_tree');
    	}
    	return $this->_treeModel;
    }
    
    public function move($parentId, $afterItemId)
    {
    	$parent = Mage::getModel('menuadminpro/menuadminpro')
    	->setStoreId($this->getStoreId())
    	->load($parentId);
    
    	if (!$parent->getId()) {
    		Mage::throwException(
    		Mage::helper('catalog')->__('Item move operation is not possible: the new parent item was not found.')
    		);
    	}
    
    	if (!$this->getId()) {
    		Mage::throwException(
    		Mage::helper('catalog')->__('Item move operation is not possible: the current item was not found.')
    		);
    	} elseif ($parent->getId() == $this->getId()) {
    		Mage::throwException(
    		Mage::helper('catalog')->__('Item move operation is not possible: parent item is equal to child item.')
    		);
    	}

    	$this->setMovedItemId($this->getId());
    
    	$eventParams = array(
    			$this->_eventObject => $this,
    			'parent'        => $parent,
    			'category_id'   => $this->getId(),
    			'prev_parent_id'=> $this->getParentId(),
    			'parent_id'     => $parentId
    	);
    	$moveComplete = false;
    
    	$this->_getResource()->beginTransaction();
    	try {
    		$this->getResource()->changeParent($this, $parent, $afterItemId);
    		$this->_getResource()->commit();
    
    		// Set data for indexer
    		$this->setAffectedItemsIds(array($this->getId(), $this->getParentId(), $parentId));
    
    		$moveComplete = true;
    	} catch (Exception $e) {
    		$this->_getResource()->rollBack();
    		throw $e;
    	}
    
    	return $this;
    }
}