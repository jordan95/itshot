<?php
class Mgtoxp_Menuadminpro_Model_Region extends Varien_Object
{
    const REGION_TOP	= 'top';
    const REGION_LEFT	= 'left';
    const REGION_RIGHT	= 'right';
    const REGION_BOTTOM	= 'bottom';

    static public function getOptionArray()
    {
        return array(
            self::REGION_TOP    => Mage::helper('menuadminpro')->__('Top'),
            self::REGION_LEFT   => Mage::helper('menuadminpro')->__('Left'),
            self::REGION_RIGHT   => Mage::helper('menuadminpro')->__('Right'),
            self::REGION_BOTTOM   => Mage::helper('menuadminpro')->__('Bottom')
        );
    }
}