<?php
class Mgtoxp_Menuadminpro_Model_Cmsblocks extends Varien_Object
{

    static public function getOptionArray()
    {
        return array(
        	'right'    	=> Mage::helper('menuadminpro')->__('Right'),
            'left'    	=> Mage::helper('menuadminpro')->__('Left'),
            'top'   	=> Mage::helper('menuadminpro')->__('Top'),
        	'bottom'    => Mage::helper('menuadminpro')->__('Bottom')
        );
    }
    
    public function getCmsBlocks()
    {
    	$options = Mage::getResourceModel('cms/block_collection')
    	->load()
    	->toOptionArray();
    	array_unshift($options, array('value'=>'', 'label'=>Mage::helper('menuadminpro')->__('Please select a static block ...')));
    
    	return $options;
    }
}