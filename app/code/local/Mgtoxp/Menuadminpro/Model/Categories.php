<?php
class Mgtoxp_Menuadminpro_Model_Categories extends Varien_Object
{
	protected $_categorytree = '';

    public function getArrayCategories(){
    	$rootCatIds = array();
    	$groups = Mage::app()->getGroups();
    	foreach ($groups as $group){
    		$rootCatIds[$group->getRootCategoryId()] = $group->getName();
    	}
    	if(!empty($rootCatIds)){
    		foreach($rootCatIds as $rootCatId => $storeName):
    			$this->_categorytree[] = $storeName;
    			$this->getTreeCategories($rootCatId, false);
    		endforeach;
    	}
    	return $this->_categorytree;
    } 
    
    function getTreeCategories($parentId, $isChild){
    	$allCats = Mage::getModel('catalog/category')->getCollection()
    	->addAttributeToSelect('*')
    	->addAttributeToFilter('is_active','1')
    	->addAttributeToFilter('include_in_menu','1')
    	->addAttributeToFilter('parent_id',array('eq' => $parentId))
    	->addAttributeToSort('position', 'asc');

    	foreach($allCats as $category){
    		$level = $category->getLevel() -1;
    		$sep = '';
    		for ($i = 1; $i <= $level; $i++) {
    			$sep.= '...';
    		}
    		$this->_categorytree[$category->getUrlPath()]= $sep . ' ' .$category->getName();
    		$subcats = $category->getChildren();
    		if($subcats != ''){
    			$this->getTreeCategories($category->getId(), true);
    		}
    	}
    }
}