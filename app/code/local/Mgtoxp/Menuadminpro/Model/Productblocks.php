<?php
class Mgtoxp_Menuadminpro_Model_Productblocks extends Varien_Object
{

    static public function getOptionArray()
    {
        return array(
        	'right'    	=> Mage::helper('menuadminpro')->__('Right'),
            'left'    	=> Mage::helper('menuadminpro')->__('Left'),
        );
    }

}