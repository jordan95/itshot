<?php
class Mgtoxp_Slidercontentpro_Model_Widgetslider
{

    public function toOptionArray()
    {
    	$collection = Mage::getModel('slidercontentpro/slidercontentpro')->getCollection();
    	$arrReturn=array();
        foreach ($collection as $item){
        	$arrReturn[]= array('value' => $item->getSlidercontentproId(), 'label' => $item->getTitle());
        }
        return $arrReturn;        
    }
} 