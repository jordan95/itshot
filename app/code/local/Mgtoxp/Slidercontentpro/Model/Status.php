<?php
class Mgtoxp_Slidercontentpro_Model_Status extends Varien_Object
{
    const STATUS_ENABLED	= 1;
    const STATUS_DISABLED	= 2;

    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('slidercontentpro')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('slidercontentpro')->__('Disabled')
        );
    }
    
    public function addEnabledFilterToCollection($collection)
    {
        $collection->addEnableFilter(array('in'=>$this->getEnabledStatusIds()));
        return $this;
    }
    
    public function getEnabledStatusIds()
    {
        return array(self::STATUS_ENABLED);
    }
	
	public function getDisabledStatusIds()
    {
        return array(self::STATUS_DISABLED);
    }
}