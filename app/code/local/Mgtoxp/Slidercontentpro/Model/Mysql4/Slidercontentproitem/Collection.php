<?php
class Mgtoxp_Slidercontentpro_Model_Mysql4_Slidercontentproitem_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('slidercontentpro/slidercontentproitem');
    }
    
    public function addBlockFilter($id)
    {     
		$this->getSelect()->join(
			array('block_table' => $this->getTable('slidercontentproblock')),
			'main_table.slidercontentproitem_id = block_table.slidercontentproitem_id',
			array()
		)
		->where('main_table.slidercontentproitem_id in (?)', array($id));

		return $this;
    }
}