<?php
class Mgtoxp_Slidercontentpro_Model_Mysql4_Slidercontentpro_Service extends Mage_Core_Model_Mysql4_Abstract
{

    protected function _construct()
    {
        $this->_init('slidercontentpro', 'slidercontentpro_id');
    }

    /**
     * Unlinks from $fromStoreId store pages that have same identifiers as pages in $byStoreId
     *
     * Routine is intented to be used before linking pages of some store ($byStoreId) to other store ($fromStoreId)
     * to prevent duplication of url keys
     *
     * Resolved $byLinkTable can be provided when restoring links from some backup table
     *
     * @param int $fromStoreId
     * @param int $byStoreId
     * @param string $byLinkTable

     */
    public function unlinkConflicts($fromStoreId, $byStoreId, $byLinkTable = null)
    {
        $readAdapter = $this->_getReadAdapter();

        $linkTable = $this->getTable('slidercontentpro_store');
        $mainTable = $this->getMainTable();
        $byLinkTable = $byLinkTable ? $byLinkTable : $linkTable;

        // Select all page ids of $fromStoreId that have identifiers as some pages in $byStoreId
        $select = $readAdapter->select()
            ->from(array('from_link' => $linkTable), 'slidercontentpro_id')
            ->join(array('from_entity' => $mainTable), $readAdapter->quoteInto('from_entity.slidercontentpro_id = from_link.slidercontentpro_id AND from_link.store_id = ?', $fromStoreId), array())
            ->join(array('by_entity' => $mainTable), 'from_entity.slidercontentpro_id != by_entity.slidercontentpro_id', array())
            ->join(array('by_link' => $byLinkTable), $readAdapter->quoteInto('by_link.slidercontentpro_id = by_entity.slidercontentpro_id AND by_link.store_id = ?', $byStoreId), array());
        $slidercontentproIds = $readAdapter->fetchCol($select);

        // Unlink found pages
        if ($slidercontentproIds) {
            $writeAdapter = $this->_getWriteAdapter();
            $where = $writeAdapter->quoteInto('slidercontentpro_id IN (?)', $slidercontentproIds);
            $where .= $writeAdapter->quoteInto('AND store_id = ?', $fromStoreId);
            $writeAdapter->delete($linkTable,  $where);
        }
        return $this;
    }
    
     /**
     * Get store ids to which specified item is assigned
     *
     * @param int $id
     * @return array
     */
    public function lookupStoreIds($id)
    {
        return $this->_getReadAdapter()->fetchCol($this->_getReadAdapter()->select()
            ->from($this->getTable('slidercontentpro_store'), 'store_id')
            ->where("{$this->getIdFieldName()} = ?", $id)
        );
    }
    
    /**
     * Set store model
     *
     * @param Mage_Core_Model_Store $store
     * @return Mage_Cms_Model_Mysql4_Page
     */
    public function setStore($store)
    {
        $this->_store = $store;
        return $this;
    }

    /**
     * Retrieve store model
     *
     * @return Mage_Core_Model_Store
     */
    public function getStore()
    {
        return Mage::app()->getStore($this->_store);
    }
}
