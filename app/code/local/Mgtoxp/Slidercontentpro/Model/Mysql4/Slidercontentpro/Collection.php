<?php
class Mgtoxp_Slidercontentpro_Model_Mysql4_Slidercontentpro_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	protected $_previewFlag;
	
    public function _construct()
    {
        $this->_init('slidercontentpro/slidercontentpro');
    }
    
    public function addEnableFilter($status)
    {
        $this->getSelect()
            ->where('status = ?', $status);
        return $this;
    }
	
    public function addStoreFilter($store, $withAdmin = true)
    {
        
        if (!Mage::app()->isSingleStoreMode()) {
			if ($store instanceof Mage_Core_Model_Store) {
				$store = array($store->getId());
			}
	
			$this->getSelect()->join(
				array('store_table' => $this->getTable('store')),
				'main_table.slidercontentpro_id = store_table.slidercontentpro_id',
				array()
			)
			->where('store_table.store_id in (?)', array(0, $store));
	
			return $this;
		}
		return $this;
    }

    /*
      public function addSlidersFilter($store, $withAdmin = true)
    {
        
        if (!Mage::app()->isSingleStoreMode()) {
			if ($store instanceof Mage_Core_Model_Store) {
				$store = array($store->getId());
			}
	
			$this->getSelect()->join(
				array('store_table' => $this->getTable('store')),
				'main_table.slidercontentpro_id = store_table.slidercontentpro_id',
				array()
			)
			->where('store_table.store_id in (?)', array(0, $store));
	
			return $this;
		}
		return $this;
    }
*/
}