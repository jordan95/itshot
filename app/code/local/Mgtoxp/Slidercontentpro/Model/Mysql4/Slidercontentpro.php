<?php
class Mgtoxp_Slidercontentpro_Model_Mysql4_Slidercontentpro extends Mage_Core_Model_Mysql4_Abstract
{
	 /**
     * Store model
     *
     * @var null|Mage_Core_Model_Store
     */
    protected $_store = null;
    
    public function _construct()
    {    
        $this->_init('slidercontentpro/slidercontentpro', 'slidercontentpro_id');
    }
    
    public function load(Mage_Core_Model_Abstract $object, $value, $field=null)
    {
        return parent::load($object, $value, $field);
    }
    
	protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        $condition = $this->_getWriteAdapter()->quoteInto('slidercontentpro_id = ?', $object->getId());
        $this->_getWriteAdapter()->delete($this->getTable('store'), $condition);

        if (!$object->getData('stores'))
		{
			$storeArray = array();
            $storeArray['slidercontentpro_id'] = $object->getId();
            $storeArray['store_id'] = '0';
            $this->_getWriteAdapter()->insert($this->getTable('store'), $storeArray);
		}
		else
		{
			foreach ((array)$object->getData('stores') as $store) {
				$storeArray = array();
				$storeArray['slidercontentpro_id'] = $object->getId();
				$storeArray['store_id'] = $store;
				$this->_getWriteAdapter()->insert($this->getTable('store'), $storeArray);
			}
		}
        return parent::_afterSave($object);
    }
    
    /**
     *
     * @param Mage_Core_Model_Abstract $object
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
    	//store filter
        $select = $this->_getReadAdapter()->select()
            ->from($this->getTable('store'))
            ->where('slidercontentpro_id = ?', $object->getId());

        if ($data = $this->_getReadAdapter()->fetchAll($select)) {
            $storesArray = array();
            foreach ($data as $row) {
                $storesArray[] = $row['store_id'];
            }
            $object->setData('store_id', $storesArray);
        }
        
        //block filter
        
        $select = $this->_getReadAdapter()->select('main.*,block.*,items.*')
            ->from(array('main'=>$this->getTable('slidercontentpro')))  			
            ->join(array('block' => $this->getTable('slidercontentproblock')), '`main`.slidercontentpro_id = `block`.slidercontentpro_id')
            ->join(array('item' => $this->getTable('slidercontentproitem')), '`block`.slidercontentproitem_id = `item`.slidercontentproitem_id')
            ->where('main.slidercontentpro_id = ?', $object->getId())
            ->where('main.status = ?', '1')
			->order('block.position ASC');
			
        if ($data = $this->_getReadAdapter()->fetchAll($select)) {            
            $slidesArray = array();
            foreach ($data as $row) {
            	$slidesArray[$row['slidercontentproitem_id']]['position']=$row['position'];
				$slidesArray[$row['slidercontentproitem_id']]['content']=$row['content'];
				$slidesArray[$row['slidercontentproitem_id']]['id']=$row['slidercontentproitem_id'];
            }
            $object->setData('slides', $slidesArray);
        }
        
        return parent::_afterLoad($object);
    }
    
       /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @return Zend_Db_Select
     */
       
  
    protected function _getLoadSelect($field, $value, $object)
    {    	    	
        $select = parent::_getLoadSelect($field, $value, $object);
        if ($object->getStoreId()) {
            $select->join(array('scs' => $this->getTable('store')), $this->getMainTable().'.slidercontentpro_id = `scs`.slidercontentpro_id')
                    ->where('`scs`.store_id in (0, ?) ', $object->getStoreId())
                    ->order('store_id DESC')
                    ->limit(1);
        }
        return $select;
    }
    
     public function deletestores_handle($observer)
  {
  	
    $event = $observer->getEvent();
    $slider = $event->getSlider();
    $status = $event->getStatus();
    
    if ($status=='success'){
    	$condition = $this->_getWriteAdapter()->quoteInto('slidercontentpro_id = ?', $slider->getSlidercontentproId());
        $this->_getWriteAdapter()->delete($this->getTable('store'), $condition);
    }

  }

}