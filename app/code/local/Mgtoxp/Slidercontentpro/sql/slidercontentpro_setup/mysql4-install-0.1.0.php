<?php
$installer = $this;

$installer->startSetup();

$installer->run("


DROP TABLE IF EXISTS {$this->getTable('slidercontentpro')};
CREATE TABLE {$this->getTable('slidercontentpro')} (
  `slidercontentpro_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `config` text NOT NULL default '', 
  `status` tinyint(1) NOT NULL default '1',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`slidercontentpro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS {$this->getTable('slidercontentproitem')};
CREATE TABLE {$this->getTable('slidercontentproitem')} (
  `slidercontentproitem_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `content` text NOT NULL default '',
  `created_time` datetime default NULL,
  `update_time` datetime default NULL,
  PRIMARY KEY  (`slidercontentproitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS {$this->getTable('slidercontentproblock')};
CREATE TABLE {$this->getTable('slidercontentproblock')} (
  `slidercontentproblock_id` int(11) NOT NULL auto_increment,
  `slidercontentpro_id` int(11) NOT NULL,
  `slidercontentproitem_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY  (`slidercontentproblock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS {$this->getTable('slidercontentpro/store')};
CREATE TABLE {$this->getTable('slidercontentpro/store')} (
`slidercontentpro_id` int(11) unsigned,
`store_id` smallint(6) unsigned
) ENGINE = InnoDB DEFAULT CHARSET = utf8;


    ");

$installer->endSetup(); 