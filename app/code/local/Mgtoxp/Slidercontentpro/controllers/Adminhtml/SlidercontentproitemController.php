<?php
class Mgtoxp_Slidercontentpro_Adminhtml_SlidercontentproitemController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('slidercontentpro/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Slidercontentpro Items Manager'), Mage::helper('adminhtml')->__('Slideshow Items Manager'));

		return $this;
	}

	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}
	
	
	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('slidercontentpro/slidercontentproitem')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('slidercontentproitem_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('slidercontentpro/item');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Slider Content Items Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->_addContent($this->getLayout()->createBlock('slidercontentpro/adminhtml_slidercontentproitem_edit'));
	
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			
			if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
				$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
			} 
		
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slidercontentpro')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
			$this->renderLayout();
	}

	public function newAction() {
		$this->_forward('edit');
	}

	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {

			$model = Mage::getModel('slidercontentpro/slidercontentproitem');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));

			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}

				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('slidercontentpro')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slidercontentpro')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}

	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$id = $this->getRequest()->getParam('id');	
				$model = Mage::getModel('slidercontentpro/slidercontentproitem');
						
				if (!$this->hasAsociateBlocks($model,$id)){									
					$model->setId($id)
						->delete();
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
					$this->_redirect('*/*/');
				}
				else{
					Mage::throwException(Mage::helper('adminhtml')->__('Item could not be deleted. It has a slider block asociated.'));
				}
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	public function hasAsociateBlocks($modelitem,$id){
	        $collectionblocks = $modelitem->getCollection()
	        					->addBlockFilter($id);			
	        $retval=false;					
	        if(count($collectionblocks->getData())){
				$retval = true;
			}
			return $retval;			
	}
	
    public function massDeleteAction() {
        $slidercontentproitemsIds = $this->getRequest()->getParam('slidercontentproitem');
        if(!is_array($slidercontentproitemsIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
            	$notalldeleted=0;
                foreach ($slidercontentproitemsIds as $slidercontentproitemId) {
                    $slidercontentproitem = Mage::getModel('slidercontentpro/slidercontentproitem')->load($slidercontentproitemId);
                    if (!$this->hasAsociateBlocks($slidercontentproitem,$slidercontentproitemId)){		
                    	$slidercontentproitem->delete();
                    }
                    else{
                    	 $notalldeleted=1;
                    }
                }
                if ($notalldeleted==0){
	                Mage::getSingleton('adminhtml/session')->addSuccess(
	                    Mage::helper('adminhtml')->__(
	                        'Total of %d record(s) were successfully deleted', count($slidercontentproitemsIds)
	                    )
	                );
                }
                else{
                	Mage::throwException(Mage::helper('adminhtml')->__('Not all items were deleted. Some has asociated a slider block.'));
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

   
    public function exportCsvAction()
    {
        $fileName   = 'slidercontentpro.csv';
        $content    = $this->getLayout()->createBlock('slidercontentpro/adminhtml_slidercontentpro_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'slidercontentpro.xml';
        $content    = $this->getLayout()->createBlock('slidercontentpro/adminhtml_slidercontentpro_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}