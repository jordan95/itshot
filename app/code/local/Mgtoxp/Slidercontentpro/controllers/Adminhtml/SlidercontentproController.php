<?php
class Mgtoxp_Slidercontentpro_Adminhtml_SlidercontentproController extends Mage_Adminhtml_Controller_action
{

	public function preDispatch()
    {
        parent::preDispatch();
    }
	
    
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('slidercontentpro/block')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Slider Content Blocks Manager'), Mage::helper('adminhtml')->__('Slider Content Blocks Manager'));

		return $this;
	}


	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('slidercontentpro/slidercontentpro')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('slidercontentpro_data', $model);
			
			$this->_regSlidercontentproBlock($model->getId());
		
			$this->loadLayout();
			$this->_setActiveMenu('slidercontentpro/block');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Slider Block'), Mage::helper('adminhtml')->__('Item Slider Block'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('slidercontentpro/adminhtml_slidercontentpro_edit'));
			
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slidercontentpro')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
		
	}

	protected function _regSlidercontentproBlock($id){
		$selecteds = array();
		$blocks= array();
		
	    $modelblock = Mage::getModel('slidercontentpro/slidercontentproblock');
	  
        $collectionblocks = $modelblock->getCollection()
	        		->addFieldToFilter('slidercontentpro_id', array('in'=>$id) );
	        			
	        		
	        foreach ($collectionblocks as $block) {
	        	  
	        	$slidercontentproitem_id = $block->getSlidercontentproitemId();
	        	$slidercontentproitem_position = $block->getPosition();
	        	$blocks[$slidercontentproitem_id] = $slidercontentproitem_position;
	        	$selecteds[] = $slidercontentproitem_id;
	        }
	 
	        //Mage::register('slider_items',$selecteds);
	        Mage::register('slider_blocks',$blocks);
	        
	}
	
	public function newAction() {
		$this->_forward('edit');
	}

	public function saveAction() {

		$data = $this->getRequest()->getPost();
		if ($data) {
			$model = Mage::getModel('slidercontentpro/slidercontentpro');
			$data = $this->_setConfig($data);
			$model->setData($data);
			if (isset($data['slidercontentpro_id'])){
				$model->setId($data['slidercontentpro_id']);
			}

			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}
				
				$model->save();

				$lastid = $model->getSlidercontentproId();
				$this->getRequest()->setParam('id', $lastid);

				try {
					$this->_saveRelatedAction($data);
				} catch (Exception $e) {
	                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
	                Mage::getSingleton('adminhtml/session')->setFormData($data);
	                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
	                return;
		        }

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('slidercontentpro')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slidercontentpro')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}

	protected function _setConfig($data){
		
		//$config['width']=$data['width'];
		//$config['height']=$data['height'];
		
		$config['animation'] = $data['animation'];
		//$config['timer_speed'] = $data['timer_speed'];
		$config['pause_on_hover'] = $data['pause_on_hover'];
		//$config['resume_on_mouseout'] = $data['resume_on_mouseout'];
		$config['animation_speed'] = $data['animation_speed'];
		//$config['stack_on_small'] = $data['stack_on_small'];
		$config['navigation_arrows'] = $data['navigation_arrows'];
		//$config['slide_number'] = $data['slide_number'];
		$config['bullets'] = $data['bullets'];
		//$config['timer'] = $data['timer'];
		//$config['variable_height'] = $data['variable_height'];

		$data['config']=serialize($config);
		return $data;
	}
	
	
	protected function _saveRelatedAction($data){

        $out = array();
		$links = $this->getRequest()->getPost('links');
	
        if (isset($links['related'])) {
            $out = Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['related']);
            $this->_deleteRelatedAction();            
            if(!empty($out)){
				$slideshow_id = $this->getRequest()->getParam('id');
				$model = Mage::getModel('slidercontentpro/slidercontentproblock');
				try {
					if(isset($data)){ unset($data); }
					foreach ($out as $k => $v){				
						if(is_numeric($k) and ($k>0)){	
							$data['slidercontentpro_id'] = $slideshow_id;
							$data['slidercontentproitem_id'] = $k;						
							if (isset($v['position'])){
								$data['position'] = $v['position'];								
							}else{
								$data['position'] = $v[0];								
							}		
							$model->setData($data);
							$model->save();
							unset($data);
						}
					}
	
				} catch (Exception $e) {
					$postreq = $this->getRequest()->getPost();
	                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
	                Mage::getSingleton('adminhtml/session')->setFormData($postreq);
	                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
		        }
            }

        }
	}

	protected function _deleteRelatedAction($id=null){

		try {
			$model = Mage::getModel('slidercontentpro/slidercontentproblock');
			if($id==null){
				$slidercontentpro_id = $this->getRequest()->getParam('id');
			}else{$slidercontentpro_id = $id;}
	        $collection = $model->getCollection()
	        		->addFieldToFilter('slidercontentpro_id', array('in'=>$slidercontentpro_id) );

			foreach ($collection as $item){
				$data = $item->getData();
				$slidercontentproblock_id = $data['slidercontentproblock_id'];
				$model->setId($slidercontentproblock_id)->delete();
			}
			
		} catch (Exception $e) {
			$data = $this->getRequest()->getPost();
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setFormData($data);
            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
        }
	}

  
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
						
			try {
				$this->_deleteRelatedAction();
				$model = Mage::getModel('slidercontentpro/slidercontentpro');
				$title = $model->getName();
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				Mage::dispatchEvent('adminhtml_slidercontentpro_on_delete', array('title' => $title, 'status' => 'success', 'slider' =>$model));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::dispatchEvent('adminhtml_slidercontentpro_on_delete', array('title' => $title, 'status' => 'fail', 'model' =>$model));
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function itemsAction()
    { 
        $this->_initAction();
        $this->loadLayout();
        $this->getLayout()->getBlock('slidercontentpro.edit.tab.items');        	
         $this->renderLayout();    	 	 
     }

      public function itemsGridAction()
    { 
		$slidercontentpro_id = $this->getRequest()->getParam('id');
		$collection = Mage::getModel('slidercontentpro/slidercontentproblock')->getCollection()
	        		->addFieldToFilter('slidercontentpro_id', array('in'=>$slidercontentpro_id) );
	    
	    $this->loadLayout();
        $this->getLayout()->getBlock('slidercontentpro.edit.tab.items')->setCollection($collection);;        	
        $this->renderLayout();    	 	 
     }
   
    public function massDeleteAction() {
        $slidercontentproIds = $this->getRequest()->getParam('slidercontentpro');
        if(!is_array($slidercontentproIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($slidercontentproIds as $slidercontentproId) {
                	$this->_deleteRelatedAction($slidercontentproId);
                    $slidercontentpro = Mage::getModel('slidercontentpro/slidercontentpro')->load($slidercontentproId);
                    $slidercontentpro->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($slidercontentproIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }


    public function exportCsvAction()
    {
        $fileName   = 'slidercontentpro.csv';
        $content    = $this->getLayout()->createBlock('slidercontentpro/adminhtml_slidercontentpro_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'slidercontentpro.xml';
        $content    = $this->getLayout()->createBlock('slidercontentpro/adminhtml_slidercontentpro_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
    
  
}