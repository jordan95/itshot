<?php
class Mgtoxp_Slidercontentpro_Block_Slidercontentpro extends Mage_Core_Block_Template
{
    protected $_sliderdata = '';
    protected $getBlockId = '';
    
	public function __construct()
	{
		$enabled = Mage::getStoreConfig('slidercontentpro/general/enabled');
		if(!$enabled) {
            return "";
        }
        parent::__construct();
	}
		
	// Config Settings
	 public function enabled()
	{
		$enabled = Mage::getStoreConfig('slidercontentpro/general/enabled');
		return $enabled;
	}

    public function getConfig($id)
	{		
		$model  = $this->_sliderdata;
		$config = unserialize($model->getConfig());		
		return $config;
	}
	
					
	// End Config Settings
	
	public function getSlidercontents($id)
	{	
		$sliders = $this->_sliderdata;
		$slidersbypos=array();	
		if ($sliders){
			$slidersitems =  $sliders->getSlides();
		}
		if (isset($slidersitems)){
			foreach ($slidersitems as $slideritem){	
				$slidersbypos[$slideritem['position']]['content']=$slideritem['content'];
				$slidersbypos[$slideritem['position']]['id']=$slideritem['id'];
			}
		}		
        return $slidersbypos;
	}
	
	public function getStoreId()
	{
		return Mage::app()->getStore()->getId();
	}
	
	
	public function getContent($content)
    {
    	$helper = Mage::helper('cms');
    	
    	$processor = $helper->getBlockTemplateProcessor();
     	$html = $processor->filter($content);
     	return $html;
    }
    
    public function setBlockId($id){
                $cache_key  =  'slidercontentpro_' . $id;
                $this->addData(array(
                    'cache_lifetime' => 0,
                    'cache_tags' => array($cache_key),
                    'cache_key' => $cache_key,
                ));
    	$this->getBlockId = $id;
    	$this->_sliderdata=Mage::getModel('slidercontentpro/slidercontentpro')->load($this->getBlockId());	            
    }
    
    public function getBlockId(){
    	return $this->getBlockId;
    }
    
    public function enabledBlock(){
    	return $this->_sliderdata->getStatus();
    }


}
?>