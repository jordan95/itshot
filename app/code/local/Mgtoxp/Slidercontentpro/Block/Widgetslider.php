<?php
class Mgtoxp_Slidercontentpro_Block_Widgetslider
    extends Mage_Core_Block_Abstract
    implements Mage_Widget_Block_Interface
{


    protected $_serializer = null;

    /**
     * Initialization
     */
    protected function _construct()
    {
        $this->_serializer = new Varien_Object();
        parent::_construct();
    }

    /**
     * Produce links list rendered as html
     *
     * @return string
     */
    protected function _toHtml()
    {
        $html = '';
        $enabled = Mage::getStoreConfig('slidercontentpro/general/enabled');
		if($enabled) {
	        $config = $this->getData('slider_blocks');
	        if (empty($config)) {
	            return $html;
	        }
	        $sliderblockId = $config;

	        $block = $this->getLayout()->createBlock('slidercontentpro/slidercontentpro');
                        $block->setBlockId($sliderblockId);
		    $block->setTemplate('mgtoxp/slidercontentpro/slidercontentpro.phtml');

		    return $block->_toHtml();
		}

    }


}
