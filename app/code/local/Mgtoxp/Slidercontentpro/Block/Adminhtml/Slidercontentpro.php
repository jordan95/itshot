<?php
class Mgtoxp_Slidercontentpro_Block_Adminhtml_Slidercontentpro extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_slidercontentpro';
    $this->_blockGroup = 'slidercontentpro';
    $this->_headerText = Mage::helper('slidercontentpro')->__('Slider Block Manager');
    $this->_addButtonLabel = Mage::helper('slidercontentpro')->__('Add Slider Block');
    parent::__construct();
  }

}
