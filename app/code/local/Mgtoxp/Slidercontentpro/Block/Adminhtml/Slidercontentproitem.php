<?php
class Mgtoxp_Slidercontentpro_Block_Adminhtml_Slidercontentproitem extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_slidercontentproitem';
    $this->_blockGroup = 'slidercontentpro';
    $this->_headerText = Mage::helper('slidercontentpro')->__('Slider Items Manager');
    $this->_addButtonLabel = Mage::helper('slidercontentpro')->__('Add Slider Item');
    parent::__construct();
  }
}