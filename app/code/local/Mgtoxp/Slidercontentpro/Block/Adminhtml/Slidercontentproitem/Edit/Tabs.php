<?php
class Mgtoxp_Slidercontentpro_Block_Adminhtml_Slidercontentproitem_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('slidercontentproitem_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('slidercontentpro')->__('Slider Item Information'));
  }

}