<?php
class Mgtoxp_Slidercontentpro_Block_Adminhtml_Slidercontentproitem_Edit_Tab_Form 
extends Mage_Adminhtml_Block_Widget_Form
implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
  protected function _prepareForm()
  {
  
      $model = Mage::registry('slidercontentproitem_data');

      $form = new Varien_Data_Form();
      $form->setHtmlIdPrefix('slidercontentproitem_');
      $fieldset = $form->addFieldset('content_fieldset', array('legend'=>Mage::helper('cms')->__('Content'),'class'=>'fieldset-wide'));

      $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
            array('tab_id' => $this->getTabId())
        );
        
      $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('slidercontentpro')->__('Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));

	  try{
            $config = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
                array(
                        'add_widgets' 					=> true,
                        'add_variables' 				=> false,
                        'tab_id' 						=> $this->getTabId() ,//'form_section',
                        'files_browser_window_url'      => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index'),//'/cms_wysiwyg_images/index'
                        'directives_url'                => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive'),
                        'directives_url_quoted' 		=> preg_quote(Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive'))
                      ));
       
         
         $config->setData(Mage::helper('slidercontentpro')->recursiveReplace(
                        '/slidercontentproitem_admin/',
                        '/'.(string)Mage::app()->getConfig()->getNode('admin/routers/adminhtml/args/frontName').'/',
                        $config->getData()
                    )
                    
                );

        }
        catch (Exception $ex){
            $config = null;
        }
     
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('slidercontentpro')->__('Content'),
          'title'     => Mage::helper('slidercontentpro')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => true,
		  'config'    => $config,
          'required'  => true,
      ));
      
        $form->setValues($model->getData());
        $this->setForm($form);
        
      return parent::_prepareForm();
  }
  
   /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slidercontentpro')->__('General');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slidercontentpro')->__('General');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
    	return true;
        //return Mage::getSingleton('admin/session')->isAllowed('cms/page/' . $action);
    }
}