<?php
class Mgtoxp_Slidercontentpro_Block_Adminhtml_Slidercontentproitem_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'slidercontentproitem_id';
        $this->_blockGroup = 'slidercontentpro';
        $this->_controller = 'adminhtml_slidercontentproitem';

        $this->_updateButton('save', 'label', Mage::helper('slidercontentpro')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('slidercontentpro')->__('Delete Item'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('slidercontentpro_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'slidercontentpro_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'slidercontentpro_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('adminhtml_slidercontentproitem') && Mage::registry('slidercontentproitem_data')->getId() ) {
            return Mage::helper('slidercontentpro')->__("Edit Slide Item '%s'", $this->htmlEscape(Mage::registry('slidercontentproitem_data')->getName()));
        } else {
            return Mage::helper('slidercontentpro')->__('Add Slider Item');
        }
    }
      
    protected function _prepareLayout()
    {
        $tabsBlock = $this->getLayout()->getBlock('adminhtml_slidercontentproitem_edit_tabs');
        if ($tabsBlock) {
            $tabsBlockJsObject = $tabsBlock->getJsObjectName();
            $tabsBlockPrefix = $tabsBlock->getId() . '_';
        } else {
            $tabsBlockJsObject = 'slidercontentproitem_tabsJsTabs';
            $tabsBlockPrefix = 'slidercontentproitem_tabs_';
        }

        
        return parent::_prepareLayout();
    }
}