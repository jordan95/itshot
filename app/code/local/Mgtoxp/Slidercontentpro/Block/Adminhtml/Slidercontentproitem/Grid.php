<?php
class Mgtoxp_Slidercontentpro_Block_Adminhtml_Slidercontentproitem_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('slidercontentproitemGrid');
      $this->setDefaultSort('slidercontentproitem_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('slidercontentpro/slidercontentproitem')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('slidercontentproitem_id', array(
          'header'    => Mage::helper('slidercontentpro')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'slidercontentproitem_id',
      ));

      $this->addColumn('name', array(
          'header'    => Mage::helper('slidercontentpro')->__('Name'),
          'align'     =>'left',
          'index'     => 'name',
      ));


     
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('slidercontentpro')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('slidercontentpro')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));

		$this->addExportType('*/*/exportCsv', Mage::helper('slidercontentpro')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('slidercontentpro')->__('XML'));

      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('slidercontentproitem_id');
        $this->getMassactionBlock()->setFormFieldName('slidercontentproitem');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('slidercontentpro')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('slidercontentpro')->__('Are you sure?')
        ));

        
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}