<?php
class Mgtoxp_Slidercontentpro_Block_Adminhtml_Slidercontentpro_Edit_Tab_Items 
extends Mage_Adminhtml_Block_Widget_Grid
//implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
	private $selecteds = array();
	private $blocks = array();

    public function __construct()
    {
        parent::__construct();
        $this->setId('slidercontentpro_items_grid');
        $this->setDefaultSort('slidercontentproitem_id');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);      
        $this->_initial();
        //if ($this->_getSlidercontentpro()->getSlidercontentproId()) {$this->setDefaultFilter(array('in_products'=>1)); }
        if ($this->_getId()) {$this->setDefaultFilter(array('in_products'=>1)); }        
  		//if ($this->isReadonly()) { $this->setFilterVisibility(false); }
  		
    }
  
    protected function _prepareLayout()
 	{
 	 parent::_prepareLayout();
 	 $this->unsetChild('search_button');
 	}

    public function isReadonly() { 
    	if ($this->getRequest()->getParam('readonly')){
    		return $this->getRequest()->getParam('readonly');
    	}
    	else {return false;} 

     }
    
     /*
    protected function _getSlidercontentpro()
	 {	  
	  return unserialize($this->getRequest()->getParam('slidercontentpro_data'));
	 }
	*/
    protected function _getId(){
      	return $this->getRequest()->getParam('id');
    }
      
    protected function _getSlidercontentproblock()
    {
        $slidercontentpro_id = $this->getRequest()->getParam('id');
		if(!empty($slidercontentpro_id)){
			$modelblocks = Mage::getModel('slidercontentpro/slidercontentproblock');
	        $collectionblocks = $modelblocks->getCollection()
	        		->addFieldToFilter('slidercontentpro_id', array('in'=>$slidercontentpro_id) );
	        return $collectionblocks;
		}
    }
       
     protected function _prepareCollection()
    {
		$collection = Mage::getModel('slidercontentpro/slidercontentproitem')->getCollection();
		$collectionblocks=$this->_getSlidercontentproblock();

		if(!empty($collectionblocks)){
	        foreach ($collectionblocks as $block) {
	        	$slidercontentproitem_id = $block->getSlidercontentproitemId();
	        	$slidercontentproitem_position = $block->getPosition();
	        	$blocks[$slidercontentproitem_id] = $slidercontentproitem_position;	        	
	        	$selecteds[] = $slidercontentproitem_id;
	        }

	        if(!empty($selecteds)){
	        	$this->selecteds = $selecteds;
	        	$this->blocks = $blocks;
	        }
		}
		
		if(!empty($this->blocks)){
			foreach ($collection->getItems() as $item) {
	        	$itemid = $item->getSlidercontentproitemId();
	        	if( isset( $this->blocks[$itemid] )){
	        		$position = $this->blocks[$itemid];	        		
	        		$item->setData('position', $position);
	        	}else{
	        		if ((!$this->isReadonly())) {
	        			$collection->removeItemByKey($itemid);
	        		}	
	        	}
	        }
		}

        $this->setCollection($collection);
        parent::_prepareCollection();
    }

    protected function _initial(){
     	$slidercontentpro_id = $this->getRequest()->getParam('id');
		 if(!empty($slidercontentpro_id)){
		 	//$this->selecteds =unserialize($this->getRequest()->getParam('slider_items')); 			 	
		 	$this->blocks =unserialize($this->getRequest()->getParam('slider_blocks'));
		 	$this->selecteds =array_keys($this->blocks); 	
		} 
		
    }
  
    protected function _prepareColumns()
    {
    	$this->addColumn('in_products', array(
            'header_css_class' => 'a-center',
            'type'      => 'checkbox',
            'name'      => 'in_products',
            'values'    => $this->_getSelectedSlides(), 
            'align'     => 'center',
            'index'     => 'slidercontentproitem_id',
            'filter' => false,
        ));

       $this->addColumn('slidercontentproitem_id', array(        
            'header'    => Mage::helper('slidercontentpro')->__('ID'),
            'sortable'  => true,
            'width'     => '60px',
            'index'     => 'slidercontentproitem_id',
            'filter' => false,
        ));
        
        $this->addColumn('name', array(
            'header'    => Mage::helper('slidercontentpro')->__('Name'),
            'index'     => 'name',
            'filter' => false,
        ));

       
        $this->addColumn('position', array(
            'header'    => Mage::helper('slidercontentpro')->__('Position'),
            'name'      => 'position',
            'type'      => 'number',
            'validate_class' => 'validate-number',
            'index'     => 'position',
            'width'     => '90px',
            //'editable' => !$this->isReadonly(),
            'editable'  => true,
            //'edit_only' => !$this->_getSlidercontentpro()->getSlidercontentproId(),
            'edit_only' => !$this->_getId(),
            'filter' => false,
        ));

        return parent::_prepareColumns();
    }

    
   public function getGridUrl()
   {
       return $this->getData('grid_url') ? $this->getData('grid_url') : $this->getUrl('*/*/itemsGrid', array('_current'=>true,'_query'=>array('readonly'=>true)));
   }
	
    protected function _getSelectedSlides()
    {
        return array_values($this->selecteds);
    }
    
    public function getSelectedSliderItems(){
    	$items=array();
    	foreach ($this->blocks as $k => $v){
    	 $items[$k]=array('position'=>$v);
    	}
    	return $items;    	
    }
 	
   
}