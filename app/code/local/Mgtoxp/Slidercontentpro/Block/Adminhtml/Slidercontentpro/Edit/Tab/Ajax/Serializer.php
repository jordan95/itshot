<?php
class Mgtoxp_Slidercontentpro_Block_Adminhtml_Slidercontentpro_Edit_Tab_Ajax_Serializer extends Mage_Core_Block_Template
{
    
    public function _construct()
    {    	
        parent::_construct();
        $this->setTemplate('slidercontentpro/edit/serializer.phtml');      
        return $this;
    }

        
    public function getProductsJSON()
    {    	
    	$result = array();
        if ($this->getProducts()) {
            $isEntityId = $this->getIsEntityId();
            foreach ($this->getProducts() as $product) {
                $id = $isEntityId ? $product->getEntityId() : $product->getSlidercontentproitemId();      	
                $result[$id] = $product->toArray(array('position'));
            }
        }
        return $result ? Zend_Json_Encoder::encode($result) : '{}';

    }
    
}
