<?php
class Mgtoxp_Slidercontentpro_Block_Adminhtml_Slidercontentpro_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('slidercontentpro_tabs');      
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('slidercontentpro')->__('Slider Block Information'));
  }
  
    protected function _prepareLayout()
    {      
       $this->addTab('slidercontentpro.edit.tab.items', array(
          'label'     => Mage::helper('slidercontentpro')->__('Slides'),
          'title'     => Mage::helper('slidercontentpro')->__('Slides'),
          //'url'       => $this->getUrl('*/*/items', array('_current' => true,'_query'=>array('slider_items'=>serialize(Mage::registry('slider_items')),'slider_blocks'=>serialize(Mage::registry('slider_blocks')),'slidercontentpro_data'=>serialize(Mage::registry('slidercontentpro_data'))))),          
          'url'       => $this->getUrl('*/*/items', array('_current' => true,'_query'=>array('slider_blocks'=>serialize(Mage::registry('slider_blocks'))))),          
          'class'     => 'ajax',
          'after'    => 'config_section',
         
      ));
      
     $this->setActive('form_section');
      parent:: _prepareLayout();
      
    }
 /**
     * Getting attribute block name for tabs
     *
     * @return string
     */
    public function getAttributeTabBlock()
    {
        if (is_null(Mage::helper('adminhtml/catalog')->getAttributeTabBlock())) {
            return $this->_attributeTabBlock;
        }
        return Mage::helper('adminhtml/catalog')->getAttributeTabBlock();
    }

    public function setAttributeTabBlock($attributeTabBlock)
    {
        $this->_attributeTabBlock = $attributeTabBlock;
        return $this;
    }

}