<?php
class Mgtoxp_Slidercontentpro_Block_Adminhtml_Slidercontentpro_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();       
      $this->setId('slidercontentproGrid');
      $this->setDefaultSort('slidercontentpro_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('slidercontentpro/slidercontentpro')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('slidercontentpro_id', array(
          'header'    => Mage::helper('slidercontentpro')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'slidercontentpro_id',
      ));
      
      if (!Mage::app()->isSingleStoreMode()) {
      $this->addColumn('store_id', array(
                'header'        => Mage::helper('slidercontentpro')->__('Store View'),
                'index'         => 'store_id',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
                'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
            ));
     }
       
           
      $this->addColumn('title', array(
          'header'    => Mage::helper('slidercontentpro')->__('Title'),
          'align'     =>'left',
          'index'     => 'title',
      ));

           
        $this->addColumn('status', array(
          'header'    => Mage::helper('slidercontentpro')->__('Status'),
          'align'     =>'left',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
                2 => Mage::helper('slidercontentpro')->__('Disabled'),
                1 => Mage::helper('slidercontentpro')->__('Enabled')
            ),
      ));
      
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('slidercontentpro')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('slidercontentpro')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('slidercontentpro')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('slidercontentpro')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('slidercontentpro_id');
        $this->getMassactionBlock()->setFormFieldName('slidercontentpro');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('slidercontentpro')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('slidercontentpro')->__('Are you sure?')
        ));
 
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

  protected function _afterLoadCollection() {
    $this->getCollection()->walk('afterLoad');
    parent::_afterLoadCollection();
  }
	 
  protected function _filterStoreCondition($collection, $column) {
	    if (!$value = $column->getFilter()->getValue()) {
	        return;
	    }
	    $this->getCollection()->addStoreFilter($value);
	}
}