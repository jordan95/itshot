<?php
class Mgtoxp_Slidercontentpro_Block_Adminhtml_Slidercontentpro_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'slidercontentpro_id';
        $this->_blockGroup = 'slidercontentpro';
        $this->_controller = 'adminhtml_slidercontentpro';

        $this->_updateButton('save', 'label', Mage::helper('slidercontentpro')->__('Save Block'));
        $this->_updateButton('delete', 'label', Mage::helper('slidercontentpro')->__('Delete Block'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('slidercontentpro')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);


        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('block_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'block_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'block_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('slidercontentpro_data') && Mage::registry('slidercontentpro_data')->getId() ) {
            return Mage::helper('slidercontentpro')->__("Edit Slider Block '%s'", $this->htmlEscape(Mage::registry('slidercontentpro_data')->getTitle()));
        } else {
            return Mage::helper('slidercontentpro')->__('Add Slider Block');
        }
    }

    protected function _prepareLayout()
    {
        $tabsBlock = $this->getLayout()->getBlock('adminhtml_slidercontentpro_edit_tabs');
        if ($tabsBlock) {
            $tabsBlockJsObject = $tabsBlock->getJsObjectName();
            $tabsBlockPrefix = $tabsBlock->getId() . '_';
        } else {
            $tabsBlockJsObject = 'slidercontentpro_tabsJsTabs';
            $tabsBlockPrefix = 'slidercontentpro_tabs_';
        }


        return parent::_prepareLayout();
    }
}