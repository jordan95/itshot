<?php
class Mgtoxp_Slidercontentpro_Block_Adminhtml_Slidercontentpro_Edit_Tab_Form 
extends Mage_Adminhtml_Block_Widget_Form
implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
	 
	 protected function _prepareLayout()
    {
    	$this->setActive(true);
    }
    protected function _prepareForm()
    {

        /** @var $model Mage_Cms_Model_Page */
        $model = Mage::registry('slidercontentpro_data');

        $form = new Varien_Data_Form();  
        $this->setForm($form);      
        $form->setHtmlIdPrefix('slidercontentpro_');	
    	$fieldset = $form->addFieldset('slidercontentpro_block', array('legend'=>Mage::helper('slidercontentpro')->__('Slider Block information')));
         
        $sliderpro_id=$model->getSlidercontentpro_id();
	      if (!empty($sliderpro_id)){
		      $fieldset->addField('slidercontentpro_id', 'hidden', array(
		          'label'     => Mage::helper('slidercontentpro')->__('Id'),
		          'name'      => 'slidercontentpro_id',
		      ));
	      }
      
   
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('slidercontentpro')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

       if (!Mage::app()->isSingleStoreMode()) {
        	$fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => Mage::helper('slidercontentpro')->__('Store View'),
                'title'     => Mage::helper('slidercontentpro')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }

        $fieldset->addField('status', 'select', array(
            'label'     => Mage::helper('slidercontentpro')->__('Status'),
            'title'     => Mage::helper('slidercontentpro')->__('Status'),
            'style'     => 'width:100px',
            'name'      => 'status',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('slidercontentpro')->__('Enabled'),
                '2' => Mage::helper('slidercontentpro')->__('Disabled'),
            ),

        ));
        
        
        $form->setValues($model->getData());
		$this->setForm($form);
		
        return parent::_prepareForm();
    }
    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slidercontentpro')->__('General');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slidercontentpro')->__('General');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
    	return true;
        //return Mage::getSingleton('admin/session')->isAllowed('cms/page/' . $action);
    }
}