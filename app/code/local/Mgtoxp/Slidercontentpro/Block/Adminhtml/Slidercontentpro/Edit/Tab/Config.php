<?php
class Mgtoxp_Slidercontentpro_Block_Adminhtml_Slidercontentpro_Edit_Tab_Config 
extends Mage_Adminhtml_Block_Widget_Form
implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
  protected function _prepareForm()
  {
   	$model = Mage::registry('slidercontentpro_data');
	$config= unserialize($model->getConfig());
	 
	if (empty($config)){ 
		$config=Mage::getStoreConfig('slidercontentpro/defaultsettings');
	}
	    	
    $form = new Varien_Data_Form();        
    $form->setHtmlIdPrefix('slidercontentpro_');	
    //$this->setForm($form);
    $fieldset = $form->addFieldset('slidercontentpro_conf', array('legend'=>Mage::helper('slidercontentpro')->__('Slide Settings information')));

     /*
      $fieldset->addField('width', 'text', array(
          'label'     => Mage::helper('slidercontentpro')->__('Width'),
          'class'     => '',
          'required'  => false,
          'name'      => 'width',         
      ));

      $fieldset->addField('height', 'text', array(
          'label'     => Mage::helper('slidercontentpro')->__('Height'),
          'class'     => '',
          'required'  => false,
          'name'      => 'height',
      ));
      */
      
      $fieldset->addField('animation', 'select', array(
      		'label'     => Mage::helper('slidercontentpro')->__('Animation'),
      		'class'     => 'required-entry',
      		'required'  => true,
      		'name'      => 'animation',
      		'values'    => array(
      				array(
      						'value'     => 'scrollHorz',
      						'label'     => Mage::helper('slidercontentpro')->__('Slide Horizontal'),
      				),
      				array(
      						'value'     => 'fade',
      						'label'     => Mage::helper('slidercontentpro')->__('Fade'),
      				),
      				array(
      						'value'     => 'fadeout',
      						'label'     => Mage::helper('slidercontentpro')->__('Fade Out'),
      				)
      		),
      ));
      /*
      $fieldset->addField('timer_speed', 'text', array(
      		'label'     => Mage::helper('slidercontentpro')->__('Timer Speed'),
      		'class'     => 'required-entry',
      		'required'  => true,
      		'name'      => 'timer_speed',
      ));
*/
      $fieldset->addField('pause_on_hover', 'select', array(
          'label'     => Mage::helper('slidercontentpro')->__('Pause on hover'),
          'name'      => 'pause_on_hover',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('slidercontentpro')->__('Yes'),
              ),
              array(
                  'value'     => 0,
                  'label'     => Mage::helper('slidercontentpro')->__('No'),
              ),
          ),
      ));
      /*
      $fieldset->addField('resume_on_mouseout', 'select', array(
          'label'     => Mage::helper('slidercontentpro')->__('Resume on mouseout'),
          'name'      => 'resume_on_mouseout',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('slidercontentpro')->__('Yes'),
              ),
              array(
                  'value'     => 0,
                  'label'     => Mage::helper('slidercontentpro')->__('No'),
              ),
          ),
      ));
*/
      
      $fieldset->addField('animation_speed', 'text', array(
          'label'     => Mage::helper('slidercontentpro')->__('Animation Speed'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'animation_speed',
      ));
      /*
      $fieldset->addField('stack_on_small', 'select', array(
          'label'     => Mage::helper('slidercontentpro')->__('Stack on small'),
          'name'      => 'stack_on_small',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('slidercontentpro')->__('Yes'),
              ),
              array(
                  'value'     => 0,
                  'label'     => Mage::helper('slidercontentpro')->__('No'),
              ),
          ),
      ));
      */
      
      
      $fieldset->addField('navigation_arrows', 'select', array(
          'label'     => Mage::helper('slidercontentpro')->__('Navigation Arrows'),
          'name'      => 'navigation_arrows',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('slidercontentpro')->__('Yes'),
              ),
              array(
                  'value'     => 0,
                  'label'     => Mage::helper('slidercontentpro')->__('No'),
              ),
          ),
      ));
      
      /*
      $fieldset->addField('slide_number', 'select', array(
          'label'     => Mage::helper('slidercontentpro')->__('Slide Number'),
          'name'      => 'slide_number',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('slidercontentpro')->__('Yes'),
              ),
              array(
                  'value'     => 0,
                  'label'     => Mage::helper('slidercontentpro')->__('No'),
              ),
          ),
      ));
      */
      
      $fieldset->addField('bullets', 'select', array(
          'label'     => Mage::helper('slidercontentpro')->__('Bullets'),
          'name'      => 'bullets',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('slidercontentpro')->__('Yes'),
              ),
              array(
                  'value'     => 0,
                  'label'     => Mage::helper('slidercontentpro')->__('No'),
              ),
          ),
      ));
      /*
      $fieldset->addField('timer', 'select', array(
          'label'     => Mage::helper('slidercontentpro')->__('Timer'),
          'name'      => 'timer',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('slidercontentpro')->__('Yes'),
              ),

              array(
                  'value'     => 0,
                  'label'     => Mage::helper('slidercontentpro')->__('No'),
              ),
          ),
      ));

      $fieldset->addField('variable_height', 'select', array(
          'label'     => Mage::helper('slidercontentpro')->__('Variable Height'),
          'name'      => 'variable_height',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('slidercontentpro')->__('Yes'),
              ),

              array(
                  'value'     => 0,
                  'label'     => Mage::helper('slidercontentpro')->__('No'),
              ),
          ),
      ));
      */

	  //$form->setValues($model->getData());
	  $form->setValues($config);
	  $this->setForm($form);
      return parent::_prepareForm();
  }
  
  /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('slidercontentpro')->__('Settings');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('slidercontentpro')->__('Settings');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
    	return true;
        //return Mage::getSingleton('admin/session')->isAllowed('cms/page/' . $action);
    }
}