<?php

class OnlineBiz_COD_Helper_Data extends Mage_Core_Helper_Abstract {

    public function _getTotalPaidPayment($orderId) {        
        $order_row = Mage::getModel('sales/order')->load($orderId);
        $total_paid = 0;
        $invoice_collection = Mage::getModel('cod/invoicegrid')->getCollection()
                        ->addfieldtofilter('order_id', array('eq' => $orderId));

        foreach ($invoice_collection as $invoice) {
            $payment_collection = Mage::getModel('cod/paymentgrid')->getCollection()
                            ->addfieldtofilter('invoice_id', array('eq' => $invoice->getId()));
            if ($payment_collection->count() > 0) {
                foreach ($payment_collection as $payment) {
                    $total_paid += $payment->getTotal();
                }
            } else {
                $total_paid = 0;
            }
        }
        return $total_paid;
    }

    public function _getBaseTotalInvoiced($orderId){
        $order_row = Mage::getModel('sales/order')->load($orderId);
        return $order_row->getBase_total_invoiced();
    }
}