<?php

class OnlineBiz_COD_Model_Paymentgrid extends Mage_Core_Model_Abstract
{
    
    public function _construct()
    {
        parent::_construct();
        $this->_init('cod/paymentgrid');
    }

    public function getPayType(){
        $paytype_collection = Mage::getModel('cod/payment')->getCollection();
        $arr = array();
        if($paytype_collection->count()){            
            foreach($paytype_collection as $row){
                $arr[$row->getPayment_id()] = $row->getName();
            }
        }
        return $arr;
    }
 
}