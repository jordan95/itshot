<?php

class OnlineBiz_COD_Model_Mysql4_Paymentgrid_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('cod/paymentgrid');
    }
}