<?php

class OnlineBiz_COD_Model_Mysql4_Invoice extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the web_id refers to the key field in your database table.
        $this->_init('cod/invoice', 'entity_id');
    }
}