<?php

class OnlineBiz_COD_Model_Mysql4_Paymentgrid extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the web_id refers to the key field in your database table.
        $this->_init('cod/paymentgrid', 'payment_grid_id');
    }
}