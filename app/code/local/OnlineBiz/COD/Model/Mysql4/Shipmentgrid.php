<?php

class OnlineBiz_COD_Model_Mysql4_Shipmentgrid extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the web_id refers to the key field in your database table.
        $this->_init('cod/shipmentgrid', 'entity_id');
    }
}