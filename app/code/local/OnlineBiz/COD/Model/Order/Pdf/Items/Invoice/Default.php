<?php

/**
 * Inchoo PDF rewrite to add products images
 * Original: Sales Order Invoice PDF model
 *
 * @category   Inchoo
 * @package    Inhoo_Invoice
 * @author     Mladen Lotar - Inchoo <mladen.lotar@inchoo.net>
 */
class OnlineBiz_COD_Model_Order_Pdf_Items_Invoice_Default extends Mage_Sales_Model_Order_Pdf_Items_Abstract {

    /**
     * Draw item line
     * */
    public function draw() {
        $order = $this->getOrder();
        $item = $this->getItem();
        $pdf = $this->getPdf();
        $page = $this->getPage();
        $lines = array();

        if ($item->getOrder_id()) {
            $order_payment = Mage::getModel('sales/order_payment')->load($item->getOrder_id(), 'parent_id');

            // draw Product name
            $lines[0] = array(array(
                    'text' => Mage::helper('core/string')->str_split($item->getIncrement_id(), 35, true, true),
                    'feed' => 35,
                    ));

            $lines[0][] = array(
                'text' => Mage::helper('core')->formatDate($item->getCreated_at(), 'medium'),
                'feed' => 90,
            );

            $lines[0][] = array(
                'text' => $order_payment->getPo_number(),
                'feed' => 145,
            );

            $lines[0][] = array(
                'text' => $order->formatPriceTxt($item->getBase_grand_total()),
                'feed' => 435,
            );

            $lines[0][] = array(
                'text' => $order->formatPriceTxt($item->getPaid_fee_amount()),
                'feed' => 485,
            );

            $lines[0][] = array(
                'text' => $order->formatPriceTxt($item->getFee_amount()),
                'feed' => 535,
            );

            $lineBlock = array(
                'lines' => $lines,
                'height' => 10
            );

            $page = $pdf->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
            $this->setPage($page);
        } else {
            // draw Product name
            $lines[0] = array(array(
                    'text' => Mage::helper('core/string')->str_split($item->getName(), 60, true, true),
                    'feed' => 35,
                    ));

            // draw SKU
            $lines[0][] = array(
                'text' => Mage::helper('core/string')->str_split($this->getSku($item), 25),
                'feed' => 255
            );
			

            // draw QTY
            $lines[0][] = array(
                'text' => $item->getOrderItem()->getShelfLocation(),
                'feed' => 380
            );
			
            // draw QTY
            $lines[0][] = array(
                'text' => $item->getQty() * 1,
                'feed' => 475
            );

            // draw Price
            $lines[0][] = array(
                'text' => $order->formatPriceTxt($item->getPrice()),
                'feed' => 445,
                'font' => 'bold',
                'align' => 'right'
            );

            // draw Tax
            $lines[0][] = array(
                'text' => $order->formatPriceTxt($item->getTaxAmount()),
                'feed' => 515,
                'font' => 'bold',
                'align' => 'right'
            );

            // draw Subtotal
            $lines[0][] = array(
                'text' => $order->formatPriceTxt($item->getRowTotal()),
                'feed' => 565,
                'font' => 'bold',
                'align' => 'right'
            );

            // custom options
            $options = $this->getItemOptions();
            if ($options) {
                foreach ($options as $option) {
                    // draw options label
                    $lines[][] = array(
                        'text' => Mage::helper('core/string')->str_split(strip_tags($option['label']), 70, true, true),
                        'font' => 'italic',
                        'feed' => 35
                    );

                    if ($option['value']) {
                        $_printValue = isset($option['print_value']) ? $option['print_value'] : strip_tags($option['value']);
                        $values = explode(', ', $_printValue);
                        foreach ($values as $value) {
                            $lines[][] = array(
                                'text' => Mage::helper('core/string')->str_split($value, 50, true, true),
                                'feed' => 40
                            );
                        }
                    }
                }
            }

            $lineBlock = array(
                'lines' => $lines,
                'height' => 10
            );

            $page = $pdf->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
            $this->setPage($page);
        }
    }

}
