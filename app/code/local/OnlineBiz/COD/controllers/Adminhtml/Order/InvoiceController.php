<?php

class OnlineBiz_COD_Adminhtml_Order_InvoiceController extends Mage_Adminhtml_Controller_action
{

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/order')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('COD Unpaid Invoices Manager'), Mage::helper('adminhtml')->__('COD Unpaid Invoice Manager'));
        return $this;
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('cod/adminhtml_order_invoice')->toHtml()
        );
    }

    public function ajaxAction()
    {
        $cusid = $this->getRequest()->getParam('customerid');
        $order_collection = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('customer_id', array('eq' => $cusid))
            ->addFieldToSelect('entity_id');

        $order_ids = array();
        foreach ($order_collection as $i => $item) {
            $order_ids[$i] = $item->getEntity_id();
        }

        $data = 0;
        $invoice_collection = Mage::getModel('sales/order_invoice')->getCollection()
            ->addFieldToFilter('order_id', array('in' => $order_ids));


        foreach ($invoice_collection as $invoice) {
            $data = $data + $invoice->getFee_amount();
            //$data = $data + $invoice->getBase_grand_total();
        }
        echo $data;
    }

    public function ajaxupdatefeeAction()
    {
        $invid = $this->getRequest()->getParam('invoiceid');
        $invoice_row = Mage::getModel('sales/order_invoice')->load($invid);
        $state = $invoice_row->getState();
        $grand_total = $invoice_row->getGrand_total();
        if ($state == 1) {
            $data['fee_amount'] = $grand_total;
            $data['base_fee_amount'] = $grand_total;
        } else {
            $data['paid_fee_amount'] = $grand_total;
            $data['paid_base_fee_amount'] = $grand_total;
        }


        $invoice_row->addData($data);
        $invoice_row->save();

        echo Mage::helper('core')->currency(round($grand_total, 2)) . '?' . $state;
    }

    public function pdfinvoiceAction()
    {
        if ($invoiceId = $this->getRequest()->getParam('invoice_id')) {
            if ($invoice = Mage::getModel('sales/order_invoice')->load($invoiceId)) {
                $pdf = Mage::getModel('sales/order_pdf_invoice')->getPdf(array($invoice));
                $this->_prepareDownloadResponse('invoice' . Mage::getSingleton('core/date')->date('Y-m-d_H-i-s') .
                    '.pdf', $pdf->render(), 'application/pdf');
            }
        } else {
            $this->_forward('noRoute');
        }
    }

    public function pdfSlipAction(){
        if ($invoiceId = $this->getRequest()->getParam('invoice_id')) {
            $shipment = Mage::getModel('sales/order_shipment')->load($invoiceId);
            if($shipment && $shipment->getId()){
                $order = $shipment->getOrder();
                $slip_number = $shipment->getIncrementId();
            }else {
                $invoice = Mage::getModel('sales/order_invoice')->load($invoiceId);
                $order = $invoice->getOrder();
                $slip_number = $invoice->getId();
            }
            $topAddress = Mage::getStoreConfig('sales/identity/address');
            $topAddress = explode(PHP_EOL, $topAddress);
            $itshot = strtoupper($topAddress[0]);
            $topAddressFirstLine = $topAddress[1];
            $topAddressSecondLine = $topAddress[2];

            $data = [];
            $data['brand_name']  = $itshot;
            $data['top_address_1'] = $topAddressFirstLine;
            $data['top_address_2'] = $topAddressSecondLine;
            $data['slip_number'] = $slip_number;
            $data['order_id']    = $order->getIncrementId();
            $data['order_date']  = date_format(date_create($order->getCreatedAt()),"F d, Y");
            $data['order_items'] = $order->getAllItems();

            try{
                require_once(Mage::getBaseDir('lib') . DS . 'mpdf/vendor/autoload.php');
                $block = $this->getLayout()->createBlock('core/template');

                $block->setItemData($data);
                $block->setTemplate('opentechiz/sales_extend/pdf/packingslip.phtml');

                $mpdf = new \Mpdf\Mpdf(array('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0));
                $mpdf->showImageErrors = true;
                $mpdf->SetDisplayMode('fullpage');
                $mpdf->AddPage('', // L - landscape, P - portrait
                    '', '', '', '',
                    5, // margin_left
                    5, // margin right
                    10, // margin top
                    20, // margin bottom
                    0, // margin header
                    0); // margin footer
                // $mpdf->list_indent_first_level = 0;
                $mpdf->WriteHTML($block->toHtml());
                $mpdf->Output('packingslip.pdf','D');
                exit;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/view/', array('invoice_id'));
            }
        }else {
            $this->_forward('noRoute');
        }
    }

    public function pdfinvoicesAction()
    {
        $back = $this->getRequest()->getParam('back');
        $customer_id = $this->getRequest()->getParam('customer_id');
        $warning = '<script type="text/javascript">alert("This customer does not have the Account Receivable Invoices"); </script>';
        if ($back == null) {
            $_url = Mage::helper("adminhtml")->getUrl("adminhtml/customer/index/");
        } else {
            $_url = Mage::helper("adminhtml")->getUrl("adminhtml/customer/edit/id/" . $customer_id);
        }
        $backUrl = '<script type="text/javascript">window.location.href = "' . $_url . '";</script>';
        $orderIds = array();
        $invoicesIds = array();
        //$invoicesIds = $this->getRequest()->getPost('customer_id');

        $order_collection = Mage::getModel('sales/order')->getCollection()
            ->addfieldtofilter('customer_id', array('eq' => $customer_id));
        if ($order_collection->count() > 0) {
            foreach ($order_collection as $o => $order) {
                $orderIds[$o] = $order->getEntity_id();
            }
        } else {
            echo $warning;
            echo $backUrl;
        }

        $payment_collection = Mage::getModel('sales/order_payment')->getCollection()
            ->addfieldtofilter('parent_id', array('in' => $orderIds))
            ->addfieldtofilter('method', array('eq' => 'cashondelivery'));
        $orderIds = array();
        if ($payment_collection->count() > 0) {
            foreach ($payment_collection as $p => $payment) {
                $orderIds[$p] = $payment->getParent_id();
            }
        } else {
            echo $warning;
            echo $backUrl;
        }

        $invoice_collection = Mage::getModel('sales/order_invoice')->getCollection()
            ->addfieldtofilter('order_id', array('in' => $orderIds))
            ->addfieldtofilter('fee_amount', array('gt' => 0));
        if ($invoice_collection->count() > 0) {
            foreach ($invoice_collection as $i => $invoice) {
                $invoicesIds[$i] = $invoice->getId();
            }
        } else {
            echo $warning;
            echo $backUrl;
        }


        if (!empty($invoicesIds)) {
            $invoices = Mage::getResourceModel('sales/order_invoice_collection')
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('entity_id', array('in' => $invoicesIds))
                ->load();
            if (!isset($pdf)) {
                $pdf = Mage::getModel('cod/order_pdf_invoice')->getPdf($invoices);
            } else {
                $pages = Mage::getModel('cod/order_pdf_invoice')->getPdf($invoices);
                $pdf->pages = array_merge($pdf->pages, $pages->pages);
            }

            return $this->_prepareDownloadResponse('invoice' . Mage::getSingleton('core/date')->date('Y-m-d_H-i-s') .
                '.pdf', $pdf->render(), 'application/pdf');
        } else {
            echo $warning;
            echo $backUrl;
        }
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/invoice');
    }

}
