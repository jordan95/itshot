<?php

class OnlineBiz_COD_Adminhtml_Order_PaymentController extends Mage_Adminhtml_Controller_action {

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/invoice/cod');
    }

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('sales/order')
                ->_addBreadcrumb(Mage::helper('adminhtml')->__('COD Invoice Payment Method Manager'), Mage::helper('adminhtml')->__('COD Invoice Payment Method Manager'));
        return $this;
    }

    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }

    public function editAction() {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('cod/payment')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('cod_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('cod/items');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('cod/adminhtml_order_payment_edit'))
                    ->_addLeft($this->getLayout()->createBlock('cod/adminhtml_order_payment_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cod')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {

            $model = Mage::getModel('cod/payment');
            $model->setData($data)
                    ->setId($this->getRequest()->getParam('id'));

            try {
                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                    $model->setCreatedTime(now())
                            ->setUpdateTime(now());
                } else {
                    $model->setUpdateTime(now());
                }

                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('cod')->__('Payment Method was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cod')->__('Unable to find payment method to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('cod/payment');

                $model->setId($this->getRequest()->getParam('id'))
                        ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Payment Method was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction() {
        $itemIds = $this->getRequest()->getParam('cod');
        if (!is_array($itemIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($itemIds as $itemId) {
                    $web = Mage::getModel('cod/payment')->load($itemId);
                    $web->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__(
                                'Total of %d record(s) were successfully deleted', count($itemIds)
                        )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function gridAction() {
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('cod/adminhtml_order_payment_search_grid')->toHtml()
        );
    }

    public function changeFormat($date) {
        $date = explode('/', $date);
        return $date[2] . '-' . $date[1] . '-' . $date[0];
    }

    public function savepaymentAction() {
        if(Mage::registry("recollect_installment") === null) {
            Mage::register('recollect_installment', true);
        }
        if ($data = $this->getRequest()->getPost()) {
            for ($i = 1; $i <= $data['hd_num_row']; $i++) {
                if ($data['hd_total_' . $i] > 0 || $data['hd_total_' . $i] > 0.00) {
                    $received_date = $data['hd_date_recei_' . $i];
                    if ($received_date == '') {
                        $received_date = Mage::getModel('core/date')->date('Y-m-d');
                    } else {
                        $received_date = $this->changeFormat($received_date);
                    }
                    $invoice_id = $data['hd_invoice_id'];
                    $invoice_row = Mage::getModel('cod/invoicegrid')->load($invoice_id);
                    $order = Mage::getModel('sales/order')->load($invoice_row->getOrder_id());
                    $customer_id = $order->getCustomer_id();
                    $customer_row = Mage::getModel('customer/customer')->load($customer_id);
                    $data_arr = array(
                        'increment_id' => $invoice_row->getIncrement_id(),
                        'invoice_id' => $data['hd_invoice_id'],
                        'payment_id' => $data['hd_payment_' . $i],
                        'number' => $data['hd_number_' . $i],
                        'total' => $data['hd_total_' . $i],
                        'billing_name' => $invoice_row->getBilling_name(),
                        'email' => $customer_row->getEmail(),
                        'order_id' => $order->getId(),
                        'received_date' => $received_date,
                        'user_id' => Mage::helper('opentechiz_payment')->getUserId()
                    );
                    $model = Mage::getModel('cod/paymentgrid')->setData($data_arr);

                    if ($model->getCreatedTime() == NULL || $model->getUpdateTime() == NULL) {
                        $model->setCreatedTime(now())
                                ->setUpdateTime(now());
                    } else {
                        $model->setUpdateTime(now());
                    }
					
                    $model->save();
                }
            }            
            if(isset($data['total'])) {
                $this->UpdateInvoiceStatusNew($data['hd_invoice_id'], $data['total']);
            }
            $this->_redirect('adminhtml/sales_invoice/view', array('invoice_id' => $this->getRequest()->getParam('invoice_id')));
            return;
        }
    }

    public function updatepaymentAction() {
        if ($data = $this->getRequest()->getPost()) {
            for ($i = 1; $i <= $data['hd_num_row_update']; $i++) {
                if ($data['hd_status_' . $i] == 'true' || $data['hd_total_' . $i] == 0) {
                    $item = Mage::getModel('cod/paymentgrid')->load($data['hd_paymentid_' . $i], 'payment_grid_id');
                    $item->delete();
                } else {
                    $received_date = $data['hd_date_recei_' . $i];
                    if ($received_date == '') {
                        $received_date = Mage::getModel('core/date')->date('Y-m-d');
                    } else {
                        $received_date =  date("Y-m-d", strtotime($received_date));
                    }
                    $data_update['number'] = $data['hd_number_' . $i];
                    $data_update['total'] = $data['hd_total_' . $i];
                    $data_update['received_date'] = $received_date;
                    $data_update['user_id'] = Mage::helper('opentechiz_payment')->getUserId();
                    $model = Mage::getModel('cod/paymentgrid')->load($data['hd_paymentid_' . $i], 'payment_grid_id');
                    if ($model->getId()) {
                        $model->addData($data_update);
                        $model->setUpdateTime(now());
						$model->setTotal($data_update['total']);						
                        $model->save();
                    }
                }
            }
            if(isset($data_update)){
                $this->UpdateInvoiceStatusNew($data['hd_invoice_id'], $data_update['total']);
            }
            $this->_redirect('adminhtml/sales_invoice/view', array('invoice_id' => $this->getRequest()->getParam('invoice_id')));
            return;
        }
    }

    public function UpdateInvoiceTotals($invoiceId, $totalpaid){
        if(!$totalpaid){
            $totalpaid = 0;
        }
        $invoice_roww = Mage::getModel('cod/invoice')->load($invoiceId);
        $data['paid_fee_amount'] = $totalpaid;
        $data['paid_base_fee_amount'] = $totalpaid;

        $grandtotal = $invoice_roww->getGrand_total();
        $totaldue = $grandtotal - $totalpaid;

        $data['fee_amount'] = $totaldue;
        $data['base_fee_amount'] = $totaldue;

        $invoice_roww->addData($data);
        $invoice_roww->save();

        die();
    }

    public function UpdateOrderTotals($orderId) {
        $order_row = Mage::getModel('sales/order')->load($orderId);
        $base_total_invoiced = $order_row->getBase_total_invoiced();
        $total_paid = 0;

        $invoice_collection = Mage::getModel('cod/invoicegrid')->getCollection()
                        ->addfieldtofilter('order_id', array('eq' => $orderId));

        foreach ($invoice_collection as $invoice) {
            $payment_collection = Mage::getModel('cod/paymentgrid')->getCollection()
                            ->addfieldtofilter('invoice_id', array('eq' => $invoice->getId()));
            if ($payment_collection->count() > 0) {
                foreach ($payment_collection as $payment) {
                    $total_paid += $payment->getTotal();
                }
            } else {
                $total_paid = 0;
            }
        }
        $totaldue = $order_row->getGrandTotal() - $total_paid;
        $data['total_paid'] = $total_paid;
        $data['base_total_paid'] = $total_paid;
        $data['paid_amount'] = $total_paid;
        $data['base_paid_amount'] = $total_paid;
        $data['remaining_amount'] = $totaldue;
        $data['base_remaining_amount'] = $totaldue;
        $order_row->addData($data);
        $order_row->save();    
//        if ($base_total_invoiced > $total_paid) {
//            $order_row->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
//        }
    }

    public function CheckShipment($orderId) {
        $shipment_row = Mage::getModel('cod/shipment')->load($orderId, 'order_id' );
        $shipment_grid = Mage::getModel('cod/shipmentgrid')->load($orderId, 'order_id');
        if ($shipment_row->getId() && $shipment_grid->getId()) {
            return true;
        } else {
            return false;
        }
    }

    public function UpdateInvoiceStatus($invoiceId) {

        $invoice_paymen_total = 0;
        $invoice_row = Mage::getModel('sales/order_invoice')->load($invoiceId);
        $invoice_grid_row = Mage::getModel('cod/invoicegrid')->load($invoiceId);
        $payment_invoice_rows = Mage::getModel('cod/paymentgrid')->getCollection()
                        ->addfieldtofilter('invoice_id', array('eq' => $invoiceId));
        if ($payment_invoice_rows->count() > 0) {
            foreach ($payment_invoice_rows as $item) {
                $invoice_paymen_total += $item->getTotal();
            }
        }

        $this->UpdateOrderTotals($invoice_row->getOrder_id());        
       // $this->UpdateInvoiceTotals($invoiceId, $invoice_paymen_total);

        $data_status['paid_base_fee_amount'] = $invoice_paymen_total;
        $data_status['paid_fee_amount'] = $invoice_paymen_total;
        $grand_total = $invoice_row->getGrand_total();
        $totaldue = $grand_total - $invoice_paymen_total;
        $data_status['fee_amount'] = $totaldue;
        $data_status['base_fee_amount'] = $totaldue;
        $invoice_row->addData($data_status);
        
        $total_pending = $grand_total - $invoice_paymen_total;
        $data['cod_total_received'] = $invoice_paymen_total;
        $data['cod_total_pending'] = $total_pending;
        //if ($invoice_paymen_total == $grand_total) {
        if (abs($grand_total - $invoice_paymen_total) < 0.00001) {
            $data['state'] = 2;
            $data_status['state'] = 2;
        } else {
            $data['state'] = 1;
            $data_status['state'] = 1;
        }
        $invoice_grid_row->addData($data);
        $invoice_grid_row->save();
        $invoice_row->addData($data_status);
        $invoice_row->save();
    }

	public function UpdateInvoiceStatusNew($invoiceId, $invoice_paymen_total) {
        $invoice_row = Mage::getModel('sales/order_invoice')->load($invoiceId);
        $invoice_grid_row = Mage::getModel('cod/invoicegrid')->load($invoiceId);


        $this->UpdateOrderTotals($invoice_row->getOrder_id());        
       // $this->UpdateInvoiceTotals($invoiceId, $invoice_paymen_total);

        $data_status['paid_base_fee_amount'] = $invoice_paymen_total;
        $data_status['paid_fee_amount'] = $invoice_paymen_total;
        $grand_total = $invoice_row->getGrand_total();
        $totaldue = $grand_total - $invoice_paymen_total;
        $data_status['fee_amount'] = $totaldue;
        $data_status['base_fee_amount'] = $totaldue;
        
        $invoice_row->addData($data_status);
        
        $total_pending = $grand_total - $invoice_paymen_total;
        $data['cod_total_received'] = $invoice_paymen_total;
        $data['cod_total_pending'] = $total_pending;

        $payment_invoice_rows = Mage::getModel('cod/paymentgrid')->getCollection()
            ->addfieldtofilter('invoice_id', array('eq' => $invoiceId));
        $total = 0;
        if ($payment_invoice_rows->count() > 0) {
            foreach ($payment_invoice_rows as $item) {
                $total += $item->getTotal();
            }
        }
        if (abs($grand_total - $total) < 0.00001) {
            $data['state'] = 2;
            $data_status['state'] = 2;
        } else {
            $data['state'] = 1;
            $data_status['state'] = 1;
        }
        $data_status['paid_amount'] = $total;
        $data_status['base_paid_amount'] = $total;
        $data_status['remaining_amount'] = $grand_total - $total;
        $data_status['base_remaining_amount'] = $grand_total - $total;

        $invoice_grid_row->addData($data);
        $invoice_grid_row->save();
        $invoice_row->addData($data_status);
        $invoice_row->save();
    }
}