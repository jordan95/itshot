<?php

class OnlineBiz_COD_Adminhtml_Order_PaymentgridController extends Mage_Adminhtml_Controller_action {

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/invoice/cod');
    }
    
    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('sales/order')
                ->_addBreadcrumb(Mage::helper('adminhtml')->__('COD Payments Received Manager'), Mage::helper('adminhtml')->__('COD Payment Received Manager'));
        return $this;
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('cod/adminhtml_order_paymentgrid_grid')->toHtml()
        );
    }

    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }
    

}