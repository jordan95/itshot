<?php

class OnlineBiz_COD_Block_Adminhtml_Sales_Order_Invoice_View extends Mage_Adminhtml_Block_Sales_Order_Invoice_View {

    public function __construct() {
        parent::__construct();
        $this->_removeButton('capture');
        $this->_removeButton('print');

        $this->_addButton('print', array(
                'label'     => Mage::helper('sales')->__('Print Invoice'),
                'class'     => 'save',
                'onclick'   => 'setLocation(\''.$this->getPrintUrl().'\')'
            )
        );
        $this->_addButton('print_slip', array(
                'label'     => Mage::helper('sales')->__('Print Packaging Slip'),
                'class'     => 'save',
                'onclick'   => 'setLocation(\''.$this->getPrintSlipUrl().'\')'
            )
        );
        $this->_addButton('print_invoice_and_slip', array(
                'label'     => Mage::helper('sales')->__('Invoice and Packaging Slip'),
                'class'     => 'save',
                'onclick'   => 'setLocation(\''.$this->getInvoiceAndPrintSlipUrl().'\')'
            )
        );
    }

    public function getPrintUrl()
    {
        return $this->getUrl('cod/adminhtml_order_invoice/pdfinvoice', array(
            'invoice_id' => $this->getInvoice()->getId()
        ));
    }

    public function getPrintSlipUrl()
    {
        
        return $this->getUrl('adminhtml/sales_shipment/print', array(
            'invoice_id' => $this->getInvoice()->getId()
        ));
    }
    public function getInvoiceAndPrintSlipUrl()
    {
        return $this->getUrl('adminhtml/invoiceandshipment/printpdf', array(
            'order_ids' => $this->getInvoice()->getOrderId()
        ));
    }

}

?>
