<?php

class OnlineBiz_COD_Block_Adminhtml_Customer_Invoice_Total extends Mage_Core_Block_Template {
    const STATE_INVOICE_PENDING = 1; 
    protected function _construct() {
        parent::_construct();
        $this->setTemplate('cod/customer/invoice/total.phtml');
    }
    public function _getCollectionClass() {
        return 'sales/order_invoice_grid_collection';
    }
    public function _getInvoices(){
        $orderId = array();
        $order_payment_collection = Mage::getModel('sales/order_payment')->getCollection()
                        ->addfieldtofilter('method', array('eq' => 'cashondelivery'));

        foreach ($order_payment_collection as $i => $row) {
            $orderId[$i] = $row->getParent_id();
        }

        $order_collection = Mage::getModel('sales/order')->getCollection()
                    ->addfieldtofilter('customer_id', array('eq' => Mage::registry('current_customer')->getId()));
        $orderId = array();
        foreach($order_collection as $o => $order){
            $orderId[$o] = $order->getEntity_id();
        }
        
        $collection = Mage::getResourceModel($this->_getCollectionClass())->addFieldToFilter('main_table.state',array('eq'=>self::STATE_INVOICE_PENDING));
        $collection->addFieldToFilter('main_table.order_id', array('in' => $orderId));

        $collection->getSelect()
                ->join(array('m1' => 'tsht_sales_flat_order_grid'), 'main_table.order_id = m1.entity_id', array('main_table.entity_id', 'main_table.billing_name', 'main_table.created_at', 'main_table.increment_id'))
                ->join(array('m3' => 'tsht_sales_flat_invoice'), 'm3.entity_id = main_table.entity_id');
                
        return $collection;
    }


}

?>
