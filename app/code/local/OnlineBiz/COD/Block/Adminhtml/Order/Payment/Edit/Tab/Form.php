<?php

class OnlineBiz_COD_Block_Adminhtml_Order_Payment_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {
    

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('cod_form', array('legend' => Mage::helper('cod')->__('General Information')));

        $fieldset->addField('name', 'text', array(
            'label' => Mage::helper('cod')->__('Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'name',
        ));



        if (Mage::getSingleton('adminhtml/session')->getCODData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getCODData());
            Mage::getSingleton('adminhtml/session')->setWebData(null);
        } elseif (Mage::registry('cod_data')) {
            $form->setValues(Mage::registry('cod_data')->getData());
        }
        return parent::_prepareForm();
    }

}