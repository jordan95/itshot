<?php

class OnlineBiz_COD_Block_Adminhtml_Order_Payment_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct(); 
        $this->_objectId = 'payment_id';
        $this->_blockGroup = 'cod';
        $this->_controller = 'adminhtml_order_payment';
        $this->_updateButton('save', 'label', Mage::helper('cod')->__('Save Payment Method'));
        $this->_updateButton('delete', 'label', Mage::helper('cod')->__('Delete Payment Method'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('cod_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'cod_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'cod_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";        
    }

    public function getHeaderText()
    {
        if( Mage::registry('cod_data') && Mage::registry('cod_data')->getId() ) {
            return Mage::helper('cod')->__("Edit Payment Method '%s'", $this->htmlEscape(Mage::registry('cod_data')->getName()));
        } else {
            return Mage::helper('cod')->__('Add Payment Method');
        }
    }
}

?>