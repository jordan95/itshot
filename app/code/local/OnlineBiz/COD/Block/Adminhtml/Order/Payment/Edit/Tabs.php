<?php
class OnlineBiz_COD_Block_Adminhtml_Order_Payment_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('cod_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('cod')->__('Payment Information'));
    }

    protected function _beforeToHtml() {
        $this->addTab('form_section', array(
            'label' => Mage::helper('cod')->__('General Information'),
            'title' => Mage::helper('cod')->__('General Information'),
            'content' => $this->getLayout()->createBlock('cod/adminhtml_order_payment_edit_tab_form')->toHtml(),
        ));
       

        return parent::_beforeToHtml();
    }

}