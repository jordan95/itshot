<?php
class OnlineBiz_COD_Block_Adminhtml_Order_Payment extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_order_payment';
    $this->_blockGroup = 'cod';
    $this->_headerText = Mage::helper('cod')->__('Payment Received Methods');
    $this->_addButtonLabel = Mage::helper('cod')->__('Add Payment Method');
    parent::__construct();
  }
}