<?php
class OnlineBiz_COD_Block_Adminhtml_Order_Invoice extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_order_invoice';
    $this->_blockGroup = 'cod';
    $this->_headerText = Mage::helper('cod')->__('Account Receivable Invoices');
    parent::__construct();
    $this->_removeButton('add');
  }
}