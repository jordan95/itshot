<?php
class OnlineBiz_COD_Block_Adminhtml_Order_Invoice_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

  public function __construct()
  {
    parent::__construct();
    $this->setId('order_invoice_grid');
    $this->setDefaultSort('state');
    $this->setDefaultDir('ASC');
    $this->setSaveParametersInSession(true);
    $this->UpdateInvoice();
  }

  public function UpdateInvoice()
  {
    $orderId = array();
    $order_payment_collection = Mage::getModel('sales/order_payment')->getCollection()
      ->addfieldtofilter('method', array('eq' => 'cashondelivery'));

    foreach ($order_payment_collection as $i => $row) {
      $orderId[$i] = $row->getParent_id();
    }
    $unpaid_invoice_collection = Mage::getModel('cod/invoicegrid')->getCollection()
      ->addfieldtofilter('cod_total_received', array('eq' => 0.0000))
      ->addFieldToFilter('order_id', array('in' => $orderId));
    foreach ($unpaid_invoice_collection as $item) {
      $grand_total = $item->getGrand_total();
      $data['cod_total_received'] = 0;
      $data['cod_total_pending'] = $grand_total;

      $invoice_grid_row = Mage::getModel('cod/invoicegrid')->load($item->getId());
      $invoice_grid_row->addData($data);
      $invoice_grid_row->save();

      $invoice_row = Mage::getModel('sales/order_invoice')->load($item->getId());
      $data2['fee_amount'] = $grand_total;
      $data2['base_fee_amount'] = $grand_total;
      $data2['paid_fee_amount'] = 0;
      $data2['paid_base_fee_amount'] = 0;
      $invoice_row->addData($data2);
      $invoice_row->save();
    }
  }

  /**
  * Retrieve collection class
  *
  * @return string
  */
  protected function _getCollectionClass()
  {
    $this->getAjax();
    return 'sales/order_invoice_grid_collection';
  }

  protected function _prepareCollection()
  {
//    $orderId = array();
//    $order_payment_collection = Mage::getModel('sales/order_payment')->getCollection();
////      ->addfieldtofilter('method', array('eq' => 'cashondelivery'));
//
//    foreach ($order_payment_collection as $i => $row) {
//      $orderId[$i] = $row->getParent_id();
//    }
//    $attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('customer_address','company');
    $collection = Mage::getResourceModel($this->_getCollectionClass());
//    $collection->addFieldToFilter('order_id', array('in' => $orderId));
    $collection->getSelect()->join('tsht_sales_flat_order_payment', 'main_table.order_id = tsht_sales_flat_order_payment.parent_id', array('method'));
    $collection->getSelect()->join(array('m1' => 'tsht_sales_flat_order_grid')  , 'main_table.order_id = m1.entity_id', array( 'tsht_sales_flat_order_payment.po_number'));
    $collection->getSelect()->join(array('m2' => 'tsht_sales_flat_order'), 'main_table.order_id = m2.entity_id', array('customer_id','main_table.created_at', 'main_table.increment_id'));
    $collection->getSelect()->joinLeft(array('s2'=>'tsht_sales_flat_order_address'),'m2.billing_address_id = s2.entity_id',array('company'));
    $this->setCollection($collection);
    return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
    $helper = Mage::helper('custom');
    $this->addColumn('increment_id', array(
      'header' => Mage::helper('sales')->__('Invoice #'),
      'index' => 'increment_id',
      'type' => 'text',
      'filter_index' => 'main_table.increment_id',
    ));

    $this->addColumn('created_at', array(
      'header' => Mage::helper('sales')->__('Invoice Date'),
      'index' => 'created_at',
      'type' => 'datetime',
      'filter_index' => 'main_table.created_at',
    ));

    $this->addColumn('order_increment_id', array(
      'header' => Mage::helper('cod')->__('Order #'),
      'index' => 'order_increment_id',
      'type' => 'text',
    ));

//    $this->addColumn('po_number', array(
//      'header'=> Mage::helper('sales')->__('Purchase Order #'),
//      'width' => '80px',
//      'type'  => 'text',
//      'index' => 'po_number',
//      'filter_index' => 'm1.po_number',
//    ));

    $this->addColumn('order_created_at', array(
      'header' => Mage::helper('sales')->__('Order Date'),
      'index' => 'order_created_at',
      'type' => 'datetime',
    ));

//    $this->addColumn('company', array(
//      'header' => Mage::helper('sales')->__('Company'),
//      'index' => 'company',
//      'type' => 'text',
//    ));

    $this->addColumn('billing_name', array(
      'header' => Mage::helper('sales')->__('Customer'),
      'index' => 'billing_name',
      'renderer'  => 'OnlineBiz_COD_Block_Adminhtml_Order_Invoice_Renderer_Customer',
      'filter' => false,
    ));

    $this->addColumn('state', array(
      'header' => Mage::helper('sales')->__('Status'),
      'index' => 'state',
      'type' => 'options',
      'options' => Mage::getModel('sales/order_invoice')->getStates(),
      'filter_index' => 'main_table.state',
    ));

    $this->addColumn('payment_method', array(
      'type'  => 'options',
      'options' => $helper->getAllPaymentMethods(),
      'header' => $helper->__('Payment Method'),
      'index' => 'method',
      'align' => 'center'
    ));

    $this->addColumn('grand_total', array(
      'header' => Mage::helper('sales')->__('Amount'),
      'index' => 'grand_total',
      'type' => 'currency',
      'align' => 'right',
      'currency' => 'order_currency_code',
      'filter_index'=>'main_table.grand_total'
    ));

    $this->addColumn('cod_total_received', array(
      'header' => Mage::helper('sales')->__('Total Received'),
      'index' => 'cod_total_received',
      'type' => 'currency',
      'align' => 'right',
      'currency' => 'order_currency_code',
    ));

    $this->addColumn('cod_total_pending', array(
      'header' => Mage::helper('sales')->__('Total Pending'),
      'index' => 'cod_total_pending',
      'type' => 'currency',
      'align' => 'right',
      'currency' => 'order_currency_code',
    ));

    $this->addColumn('action',
      array(
        'header' => Mage::helper('sales')->__('Action'),
        'width' => '50px',
        'type' => 'action',
        'getter' => 'getId',
        'actions' => array(
          array(
            'caption' => Mage::helper('sales')->__('View'),
            'url' => array('base' => 'adminhtml/sales_invoice/view'),
            'field' => 'invoice_id'
          )
        ),
        'filter' => false,
        'sortable' => false,
        'is_system' => true
      ));

    return parent::_prepareColumns();
  }

  public function getRowUrl($row)
  {
    if (!Mage::getSingleton('admin/session')->isAllowed('sales/order/invoice')) {
      return false;
    }
    return $this->getUrl('adminhtml/sales_invoice/view', array('invoice_id' => $row->getId()));
  }

  public function getAjax()
  {

    echo '<script type="text/javascript" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS).'onlinebiz/cod/jquery-1.8.2.min.js"></script><script type="text/javascript" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS).'onlinebiz/cod/numberformater.js"></script>';
    $tfoot = "<tfoot><tr><td colspan='11' class='label' align='right'><strong><big>Total Pending Invoices</big></strong></td><td colspan='1' class='emph' align='right'><strong><big class='total-pending'></big></strong></td><td>&nbsp;</td></tr></tfoot>";
    echo '<script>
    $j(window).load(function(){
      var _total_pending = 0;
      var _total_afterdecimal = 0;
      var _total_beforedecimal = 0;
      $j("#order_invoice_grid_table").append("'.$tfoot.'");
      $j("#order_invoice_grid_table tbody tr").each(function(){
        var _single = $j(this).find("td").eq(11).html();
        _single = _single.replace("$", "");
        _single = _single.replace(",", "");
        _single = parseFloat(_single);
        _total_pending = _total_pending + _single;
      });

      _total_pending = parseFloat(_total_pending).toFixed(2);
      _total_pending = _total_pending.toString();
      var split = _total_pending.split(".");
      _total_beforedecimal = split[0];
      _total_afterdecimal = split[1];
      _total_pending = number_format(_total_beforedecimal, _total_beforedecimal.length, ".");

      $j(".total-pending").html("$"+_total_pending+"."+_total_afterdecimal);
    });

    </script>';
  }


}
