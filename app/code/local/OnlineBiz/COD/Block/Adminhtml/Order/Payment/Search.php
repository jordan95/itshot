<?php

class OnlineBiz_COD_Block_Adminhtml_Order_Payment_Search extends Mage_Adminhtml_Block_Widget {

    public function __construct() {
        parent::__construct();
        $this->setId('order_payment_search');
    }

    public function getHeaderText() {
        return Mage::helper('cod')->__('Please Select Payment Method to Add');
    }

    public function getButtonsHtml() {
        $addButtonData = array(
            'label' => Mage::helper('cod')->__('Save Payment'),
            'onclick' => 'payment.paymentSubmitAddPayment(payment)',
            'class' => 'add',
        );
        return $this->getLayout()->createBlock('adminhtml/widget_button')->setData($addButtonData)->toHtml();
    }

    public function getHeaderCssClass() {
        return 'head-catalog-product';
    }

    public function _getInvoice(){
        return Mage::registry('current_invoice');
    }

}