<?php

class OnlineBiz_COD_Block_Adminhtml_Order_Invoice_Renderer_Customer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
    
    public function render(Varien_Object $row) {
        $cusid = $row->getData('customer_id');
        $value = $row->getData('billing_name');
        if ($cusid)
            return sprintf('
				<a href="%s" title="%s">%s</a>',
                    $this->getUrl('adminhtml/customer/edit/', array('_current' => true, 'id' => $cusid)),
                    Mage::helper('cod')->__('View Customer Detail'),
                    $value
            );
        else
            return sprintf('%s', $value);
    }

}

?>