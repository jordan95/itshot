<?php

class OnlineBiz_COD_Block_Adminhtml_Order_Paymentgrid_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('order_paymentgrid_grid');
        $this->setDefaultSort('payment_grid_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('cod/paymentgrid')->getResourceCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('payment_grid_id', array(
            'header' => Mage::helper('cod')->__('Payment #'),
            'index' => 'payment_grid_id',
            'type' => 'text',
        ));

        $this->addColumn('number', array(
            'header' => Mage::helper('cod')->__('Note'),
            'index' => 'number',
            'type' => 'text',
        ));

        $this->addColumn('increment_id', array(
            'header' => Mage::helper('cod')->__('Invoice #'),
            'index' => 'increment_id',
            'type' => 'text',
        ));


        $this->addColumn('payment_id', array(
            'header' => Mage::helper('cod')->__('Payment Method'),
            'index' => 'payment_id',
            'type' => 'options',
            'options' => Mage::getModel('cod/paymentgrid')->getPayType(),
        ));

        $this->addColumn('total', array(
            'header' => Mage::helper('cod')->__('Total'),
            'index' => 'total',
            'type' => 'currency',
            'align' => 'right',
            'currency_code' => Mage::app()->getStore()->getBaseCurrencyCode()
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('cod')->__('Customer'),
            'index' => 'billing_name',
            'type' => 'text',
        ));
       
       
        $this->addColumn('email', array(
            'header' => Mage::helper('cod')->__('Email'),
            'index' => 'email',
            'type' => 'text',
        ));

        $this->addColumn('received_date', array(
            'header' => Mage::helper('cod')->__('Received Date'),
            'index' => 'received_date',
            'type' => 'date',
        ));

        $this->addColumn('created_time', array(
            'header' => Mage::helper('cod')->__('Created Date'),
            'index' => 'created_time',
            'type' => 'datetime',
        ));

        $this->addColumn('update_time', array(
            'header' => Mage::helper('cod')->__('Updated Date'),
            'index' => 'update_time',
            'type' => 'datetime',
        ));



//        $this->addExportType('*/*/exportCsv', Mage::helper('web')->__('CSV'));
//        $this->addExportType('*/*/exportXml', Mage::helper('web')->__('XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        //return $this->getUrl('*/*/view', array('id' => $row->getId()));
        return '#';
    }

}

?>
