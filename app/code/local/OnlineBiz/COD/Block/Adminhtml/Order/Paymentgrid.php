<?php

class OnlineBiz_COD_Block_Adminhtml_Order_Paymentgrid extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_order_paymentgrid';
        $this->_blockGroup = 'cod';
        $this->_headerText = Mage::helper('cod')->__('Payment Received Records');
        //$this->_addButtonLabel = Mage::helper('web')->__('Add Payment Type');
        parent::__construct();
        $this->_removeButton('add');

        
    }
}

?>
