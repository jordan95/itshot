<?php

class OnlineBiz_COD_Block_Adminhtml_Order_Payment_Search_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('order_payment_search_grid');
        $this->setRowClickCallback('payment.paymentGridRowClick.bind(payment)');
        $this->setRowInitCallback('payment.paymentGridRowInit.bind(payment)');
        $this->setDefaultSort('name');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('collapse')) {
            $this->setIsCollapsed(true);
        }
    }

    protected function _prepareCollection() {
        $paymentId = array();
        $cur_invoice_id = $this->getRequest()->getParam('invoice_id');
        $paygrid_collection = Mage::getModel('cod/paymentgrid')->getCollection()
                        ->addfieldtofilter('invoice_id', array('eq' => $cur_invoice_id));
        foreach ($paygrid_collection as $i => $row) {
            $paymentId[$i] = $row->getPayment_id();
        }

        $collection = Mage::getModel('cod/payment')->getCollection();
        if($paygrid_collection->count() > 0){
            $collection->addFieldToFilter('payment_id', array('nin' => $paymentId));
        }
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {


        $this->addColumn('name', array(
            'header' => Mage::helper('cod')->__('Name'),
            'index' => 'name',
            'type' => 'text',
        ));

        $this->addColumn('number', array(
            'filter' => false,
            'sortable' => false,
            'header' => Mage::helper('cod')->__('Note'),
            'name' => 'number',
            'align' => 'center',
            'type' => 'input',
            'validate_class' => 'validate-number',
            'index' => 'number',
            'width' => '400px',
        ));

        $this->addColumn('total', array(
            'filter' => false,
            'sortable' => false,
            'header' => Mage::helper('cod')->__('Total'),
            'name' => 'total[]',
            'align' => 'center',
            'type' => 'input',
            'validate_class' => 'validate-number',
            'index' => 'total',
            'width' => '200px',
        ));

        $this->addColumn('date_recei', array(
            'filter' => false,
            'sortable' => false,
            'header' => Mage::helper('cod')->__('Date Received'),
            'name' => 'date_recei[]',
            'align' => 'center',
            'type' => 'input',
            'id' => 'abcd',
            'index' => 'date_recei',
            'width' => '200px',
        ));

        $this->addColumn('in_payments', array(
            'header' => Mage::helper('sales')->__('Select'),
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'in_payments',
            'values' => $this->_getSelectedProducts(),
            'align' => 'center',
            'index' => 'payment_id',
            'sortable' => false,
        ));

        return parent::_prepareColumns();
    }

    protected function _getSelectedProducts() {
        $payments = $this->getRequest()->getPost('payments', array());

        return $payments;
    }

    public function getGridUrl() {
        return $this->getUrl('cod/adminhtml_order_payment/grid', array('_current' => true));
    }

}

?>
