<?php

class OnlineBiz_COD_Block_Adminhtml_Order_Payment_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('order_payment_grid');
        $this->setDefaultSort('payment_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('cod/payment')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('payment_id', array(
            'header' => Mage::helper('cod')->__('Method ID #'),
            'width' => '50px',
            'index' => 'payment_id',
            'type' => 'text',
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('cod')->__('Name'),
            'index' => 'name',
            'type' => 'text',
        ));

        $this->addColumn('created_time', array(
            'header' => Mage::helper('cod')->__('Created Date'),
            'index' => 'created_time',
            'type' => 'datetime',
        ));

        $this->addColumn('update_time', array(
            'header' => Mage::helper('cod')->__('Update Date'),
            'index' => 'update_time',
            'type' => 'datetime',
        ));

        $this->addColumn('action',
                array(
                    'header' => Mage::helper('cod')->__('Action'),
                    'width' => '100',
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('cod')->__('Edit'),
                            'url' => array('base' => '*/*/edit'),
                            'field' => 'id'
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'is_system' => true,
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('payment_id');
        $this->getMassactionBlock()->setFormFieldName('cod');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('cod')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('cod')->__('Are you sure?')
        ));

        return $this;
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}

?>
