<?php

class OnlineBiz_COD_Block_Paymentform extends Mage_Core_Block_Template {

    protected function _construct() {
        parent::_construct();
        $this->setTemplate('cod/payment/form.phtml');
    }

    public function _getInvoice() {
        return Mage::registry('current_invoice');
    }

    public function isCOD() {
        $invoice_id = $this->_getInvoice()->getId();
        $invoice_row = Mage::getModel('cod/invoicegrid')->load($invoice_id);
        $order_id = $invoice_row->getOrder_id();

        $order_row = Mage::getModel('sales/order_payment')->load($order_id, 'parent_id');
        $order_method = $order_row->getMethod();
        if ($order_method == 'cashondelivery') {
            return true;
        } else {
            return false;
        }
    }

    public function _getPaymentGrid() {
        $invoice_id = $this->_getInvoice()->getId();
        $paygrid_collection = Mage::getModel('cod/paymentgrid')->getCollection()
                        ->addfieldtofilter('invoice_id', array('eq' => $invoice_id));

        return $paygrid_collection;
    }

    public function changeDateFormat($date) {
        return Mage::helper('core')->formatDate($date, 'short', false);
    }

    public function getDateFormatStr(){
        return Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
    }

}

?>
