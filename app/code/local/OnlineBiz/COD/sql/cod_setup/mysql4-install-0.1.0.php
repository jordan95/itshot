<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('onlinebiz_payment')};
CREATE TABLE {$this->getTable('onlinebiz_payment')} (
  `payment_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- DROP TABLE IF EXISTS {$this->getTable('onlinebiz_payment_grid')};
CREATE TABLE {$this->getTable('onlinebiz_payment_grid')} (
  `payment_grid_id` int(11) unsigned NOT NULL auto_increment,
  `invoice_id` int(11) NOT NULL default 0,
  `increment_id` int(11) NOT NULL default 0,
  `payment_id` int(11) NOT NULL default 0,
  `number` varchar(225) NULL,
  `total` float NOT NULL default 0,
  `billing_name` varchar(225) NOT NULL default '',
  `email` varchar(225) NOT NULL default '',
  `received_date` date NULL,
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`payment_grid_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 