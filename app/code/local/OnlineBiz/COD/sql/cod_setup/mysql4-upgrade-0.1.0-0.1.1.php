<?php

$installer = $this;

$installer->startSetup();

$this->_conn->addColumn($this->getTable('sales_flat_invoice_grid'), 'cod_total_received', 'decimal(12,4) NOT NULL default 0');
$this->_conn->addColumn($this->getTable('sales_flat_invoice_grid'), 'cod_total_pending', 'decimal(12,4) NOT NULL default 0');

$installer->endSetup(); 