<?php
/**
 * Extensions Manager Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://store.onlinebizsoft.com/license.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@onlinebizsoft.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    ExtensionManager
 * @author     OnlineBiz <sales@onlinebizsoft.com>
 * @copyright  2007-2011 OnlineBiz
 * @license    http://store.onlinebizsoft.com/license.txt
 * @version    1.0.1
 * @link       http://store.onlinebizsoft.com
 */


class OnlineBiz_ObBase_Block_System_Config_Form_Fieldset_Support
	extends Mage_Adminhtml_Block_System_Config_Form_Fieldset
{
	protected $_dummyElement;
	protected $_fieldRenderer;
	protected $_values;

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
		$html = $this->_getHeaderHtml($element);
		
		$html .= 'You can submit a ticket at <a href="http://ticket.onlinebizsoft.com">ticket.onlinebizsoft.com</a> (preferred) or contact our support team at <a href="mailto:support@onlinebizsoft.com?subject=Support request for order XXXXXXXXX&amp;body=Problem description%20%3A%0D%0AMy store URL%20%3A%0D%0AMy license key%20%3A%0D%0AMy backend URL%20%3A%0D%0AAdmin login account%20%3A%0D%0AFTP login%20%3A%0D%0AphpMyAdmin access%20%3A%0D%0A">support@onlinebizsoft.com</a><br>
		We will require you to provide below information for support<br>
		<ul>
			<li>- Your order ID at our store</li>
			<li>- URL of your Magento installation</li>
			<li>- The license key which is valid for your above Magento URL</li>
			<li>- Magento backend URL and admin login (user and password)</li>
			<li>- FTP Login to your Magento installation</li>
			<li>- In some cases, we may require phpMyAdmin access to your Magento database</li>
		</ul>';

		
        $html .= $this->_getFooterHtml($element);		
        return $html;
    }
}
