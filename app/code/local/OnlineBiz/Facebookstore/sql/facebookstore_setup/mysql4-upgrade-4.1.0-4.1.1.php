<?php
$installer = $this;
$installer->startSetup();

$setup = new Mage_Sales_Model_Mysql4_Setup('core_setup');
$setup->addAttribute('quote', 'facebook_order', array('type' => 'int', 'visible' => false));
$setup->addAttribute('order', 'facebook_order', array('type' => 'int', 'visible' => false));

$installer->endSetup();


