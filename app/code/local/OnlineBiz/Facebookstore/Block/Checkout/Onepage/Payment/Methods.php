<?php

class OnlineBiz_Facebookstore_Block_Checkout_Onepage_Payment_Methods extends Mage_Checkout_Block_Onepage_Payment_Methods
{
    protected function _construct()
    {
		if(Mage::getVersion() >= 1.8) {
			$this->setTemplate('checkout/onepage/payment/info.phtml');
		} else {
			$this->setTemplate('checkout/onepage/payment/methods.phtml');
		}
        parent::_construct();
    }
}
