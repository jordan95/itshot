<?php

class CueBlocks_GtmExtension_Block_Gtm extends Shopgo_GTM_Block_Gtm {

    private $_storeId = 0;
    private $_pid = false;
    private $_pid_prefix = "";
    private $_pid_prefix_ofcp = 0;
    private $_pid_ending = "";
    private $_pid_ending_ofcp = 0;

    private function getEcommProdid($product) {
        $ecomm_prodid = (string) ($product->getId() ? $product->getId() : $product->getSku());
        $ofcp = false;
        if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE ||
            $product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_GROUPED) {
            $ofcp = true;
        }

        if (!empty($this->_pid_prefix) && (($this->_pid_prefix_ofcp === 1 && $ofcp) ||
                $this->_pid_prefix_ofcp === 0)) {
            $ecomm_prodid = $this->_pid_prefix . $ecomm_prodid;
        }

        if (!empty($this->_pid_ending) && (($this->_pid_ending_ofcp === 1 && $ofcp) ||
                $this->_pid_ending_ofcp === 0)) {
            $ecomm_prodid .= $this->_pid_ending;
        }

        return $ecomm_prodid;
    }

    /**
     * Generate JavaScript for the data layer.
     *
     * @return string|null
     */
    protected function _getDataLayer() {
        // Initialise our data source.
        $data = array();
        $dataScript = '';

        // Get transaction and visitor data.
        if(Mage::helper('gtm')->isDataLayerEnabled()) {
            $data = $data + $this->_getTransactionData();
            $data = $data + $this->_getVisitorData();
        }
        if(Mage::helper('gtm')->isRemarketingDataLayerEnabled()) {
            $data = $data + $this->_getRemarketingData();
        }
        if(Mage::helper('gtm')->isCriteoDataLayerEnabled()) {
            $data = $data + $this->_getCriteoData();
        }
        if(Mage::helper('gtm')->isShareASaleDataLayerEnabled()) {
            $data = $data + $this->_getShareASaleData();
        }

        // Get transaction and visitor data, if desired.
        if (!empty($data)) {
            // Generate the data layer JavaScript.
            $dataScript .= "<script type='text/javascript'>dataLayer = [" . json_encode($data) . "];</script>\n\n";

            // Generate shopgoStoresDataLayer JavaScript.
            $dataScript .= "<script>shopgoStoresDataLayer = [" . json_encode($data) . "];</script>\n\n";
        }

        return $dataScript;
    }
    /**
     * Get transaction data for use in the data layer.
     *
     * @link https://developers.google.com/tag-manager/reference
     * @return array
     */
    protected function _getTransactionData()
	{
		$data = array();

		$orderIds = $this->getOrderIds();
		if (empty($orderIds) || !is_array($orderIds)) return array();

		$collection = Mage::getResourceModel('sales/order_collection')->addFieldToFilter('entity_id', array('in' => $orderIds));

		$i = 0;
		$products = array();

		foreach ($collection as $order) {
			if ($i == 0) {
				// Build all fields for first order.
				$data = array(
					'transactionId' => $order->getIncrementId(),
					'transactionDate' => date("Y-m-d"),
					'transactionType' => Mage::helper('gtm')->getTransactionType(),
					'transactionAffiliation' => Mage::helper('gtm')->getTransactionAffiliation(),
					'transactionTotal' => round($order->getBaseGrandTotal(),2),
					'transactionShipping' => round($order->getBaseShippingAmount(),2),
					'transactionTax' => round($order->getBaseTaxAmount(),2),
					'transactionPaymentType' => $order->getPayment()->getMethodInstance()->getTitle(),
					'transactionCurrency' => Mage::app()->getStore()->getBaseCurrencyCode(),
					'transactionShippingMethod' => $order->getShippingCarrier() ? $order->getShippingCarrier()->getCarrierCode() : 'No Shipping Method',
					'transactionPromoCode' => $order->getCouponCode(),
					'transactionProducts' => array(),
					'transactionBillingCountry' => $order->getBillingAddress()->getCountry(),
					'transactionBillingState' => $order->getBillingAddress()->getRegion()
				);
			} else {
				// For subsequent orders, append to order ID, totals and shipping method.
				$data['transactionId'] .= '|' . $order->getIncrementId();
				$data['transactionTotal'] += $order->getBaseGrandTotal();
				$data['transactionShipping'] += $order->getBaseShippingAmount();
				$data['transactionTax'] += $order->getBaseTaxAmount();
				$data['transactionShippingMethod'] .= '|' . $order->getShippingCarrier() ? $order->getShippingCarrier()->getCarrierCode() : 'No Shipping Method';
			}

			// Build products array.
			foreach ($order->getAllVisibleItems() as $item) {
				$product = $item->getProduct();
				$product_categories = array();
				try {
					$product_categories = $product->getCategoryIds();
				} catch (Mage_Exception $e) {
					// todo
				}
				$categories = array();
				foreach ($product_categories as $category) {
					$categories[] = Mage::getModel('catalog/category')->load($category)->getName();
				}
				if (empty($products[$item->getSku()])) {
					// Build all fields the first time we encounter this item.
					$products[$item->getSku()] = array(
						'name' => $this->jsQuoteEscape(Mage::helper('core')->escapeHtml($item->getName())),
						'sku' => $this->jsQuoteEscape(Mage::helper('core')->escapeHtml($item->getSku())),
						'category' => implode('|',$categories),
						'price' => (double)number_format($item->getBasePrice(),2,'.',''),
						'quantity' => (int)$item->getQtyOrdered()
					);
				} else {
					// If we already have the item, update quantity.
					$products[$item->getSku()]['quantity'] += (int)$item->getQtyOrdered();
				}
			}

			$i++;
		}

		// Push products into main data array.
		foreach ($products as $product) {
			$data['transactionProducts'][] = $product;
		}

		// Trim empty fields from the final output.
		foreach ($data as $key => $value) {
			if (!is_numeric($value) && empty($value)) unset($data[$key]);
		}

		return $data;
	}

    /**
     * Get remarketing data for use in the data layer.
     *
     * @return array
     */
    protected function _getRemarketingData() {
        $params = array();
        $inclTax = false;
        if ((int) Mage::getStoreConfig('google/affiliate/gdrt_tax', $this->_storeId) === 1) {
            $inclTax = true;
        }

        $page_type = Mage::app()->getRequest()->getModuleName() . '-' . Mage::app()->getFrontController()->getRequest()->getControllerName();

        $params['ecomm_pagetype'] = 'other';
        switch ($page_type) {
            case 'cms-index':
                $params['ecomm_pagetype'] = 'home';
                break;

            case 'nsearch-index':
                $params['ecomm_pagetype'] = 'searchresults';
                break;

            case 'catalog-category':
                $category = Mage::registry('current_category');
                $params['ecomm_pagetype'] = 'category';
                $params['ecomm_category'] = (string) $category->getName();
                unset($category);
                break;

            case 'catalog-product':
                $product = Mage::registry('current_product');
                $totalvalue = Mage::helper('tax')->getPrice($product, $product->getFinalPrice(), $inclTax);

                $params['ecomm_prodid'] = $this->getEcommProdid($product);
                $params['ecomm_pagetype'] = 'product';
                $params['ecomm_totalvalue'] = (float) number_format($totalvalue, '2', '.', '');
                unset($product);
                break;

            case 'checkout-cart':
                $cart = Mage::getSingleton('checkout/session')->getQuote();
                $items = $cart->getAllVisibleItems();

                if (count($items) > 0) {
                    $data = array();
                    $totalvalue = 0;
                    foreach ($items as $item) {
                        $data[0][] = $this->getEcommProdid($item->getProduct());
                        $data[1][] = (int) $item->getQty();
                        $totalvalue += $inclTax ? $item->getRowTotalInclTax() : $item->getRowTotal();
                    }
                    $params['ecomm_prodid'] = $data[0];
                    $params['ecomm_pagetype'] = 'cart';
                    $params['ecomm_quantity'] = $data[1];
                    $params['ecomm_totalvalue'] = (float) number_format($totalvalue, '2', '.', '');
                } else
                    $params['ecomm_pagetype'] = 'siteview';

                unset($cart, $items, $item, $data);
                break;

            case 'onestepcheckout-index':
                if ('index' == $this->getRequest()->getActionName()) {
                    $cart = Mage::getSingleton('checkout/session')->getQuote();
                    $params['ecomm_pagetype'] = 'cart';
                } elseif ('success' == $this->getRequest()->getActionName()) {
                    $cart = Mage::getModel('sales/order')->loadByIncrementId(Mage::getSingleton('checkout/session')->getLastRealOrderId());
                    $params['ecomm_pagetype'] = 'purchase';
                }


                $items = $cart->getAllVisibleItems();

                if (count($items) > 0) {
                    $data = array();
                    $totalvalue = 0;

                    if ('index' == $this->getRequest()->getActionName()) {
                        foreach ($items as $item) {
                            $data[0][] = $this->getEcommProdid($item->getProduct());
                            $data[1][] = (int) $item->getQty();
                            $totalvalue += $inclTax ? $item->getRowTotalInclTax() : $item->getRowTotal();
                        }
                    } elseif ('success' == $this->getRequest()->getActionName()) {
                        foreach ($items as $item) {
                            $data[0][] = $this->getEcommProdid($item->getProduct());
                            $data[1][] = (int) $item->getData('qty_ordered');
                            $totalvalue += $inclTax ? $item->getRowTotalInclTax() : $item->getRowTotal();
                        }
                    }

                    $params['ecomm_prodid'] = $data[0];

                    $params['ecomm_quantity'] = $data[1];
                    $params['ecomm_totalvalue'] = (float) number_format($totalvalue, '2', '.', '');
                }
                unset($order, $items, $item);
                break;

            case 'checkout-onepage':
                if ('index' == $this->getRequest()->getActionName()) {
                    $cart = Mage::getSingleton('checkout/session')->getQuote();
                    $params['ecomm_pagetype'] = 'cart';
                } elseif ('success' == $this->getRequest()->getActionName()) {
                    $cart = Mage::getModel('sales/order')->loadByIncrementId(Mage::getSingleton('checkout/session')->getLastRealOrderId());
                    $params['ecomm_pagetype'] = 'purchase';
                }


                $items = $cart->getAllVisibleItems();

                if (count($items) > 0) {
                    $data = array();
                    $totalvalue = 0;

                    if ('index' == $this->getRequest()->getActionName()) {
                        foreach ($items as $item) {
                            $data[0][] = $this->getEcommProdid($item->getProduct());
                            $data[1][] = (int) $item->getQty();
                            $totalvalue += $inclTax ? $item->getRowTotalInclTax() : $item->getRowTotal();
                        }
                    } elseif ('success' == $this->getRequest()->getActionName()) {
                        foreach ($items as $item) {
                            $data[0][] = $this->getEcommProdid($item->getProduct());
                            $data[1][] = (int) $item->getData('qty_ordered');
                            $totalvalue += $inclTax ? $item->getRowTotalInclTax() : $item->getRowTotal();
                        }
                    }

                    $params['ecomm_prodid'] = $data[0];

                    $params['ecomm_quantity'] = $data[1];
                    $params['ecomm_totalvalue'] = (float) number_format($totalvalue, '2', '.', '');
                }
                unset($order, $items, $item);
                break;

            default:
                break;
        }

        return $params;
    }


    /**
     * Get criteo data for use in the data layer.
     *
     * @return array
     */
    protected function _getCriteoData() {
        $params = array();

        $page_type = Mage::app()->getRequest()->getModuleName() . '-' . Mage::app()->getFrontController()->getRequest()->getControllerName();
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customerEmail = ($customer->getEmail()) ? $customer->getEmail() : '';

        switch ($page_type) {
            case 'cms-index':
                $params['criteo_pagetype'] = 'HomePage';
                $params['criteo_customer_email'] = $customerEmail;
                break;

            case 'catalog-category':
                $category = Mage::registry('current_category');
                $productIds = $category->getProductCollection()->getAllIds();
                $params['criteo_pagetype'] = 'ListingPage';
                $params['criteo_customer_email'] = $customerEmail;
                $params['criteo_product_id_list'] = $productIds;
                unset($category);
                break;

            case 'catalog-product':
                $product = Mage::registry('current_product');
                $params['criteo_pagetype'] = 'ProductPage';
                $params['criteo_customer_email'] = $customerEmail;
                $params['criteo_product_id'] = $this->getEcommProdid($product);
                unset($product);
                break;

            case 'checkout-cart':
                $cart = Mage::getSingleton('checkout/session')->getQuote();
                $items = $cart->getAllVisibleItems();
                $params['criteo_pagetype'] = 'BasketPage';
                $params['criteo_customer_email'] = $customerEmail;
                if (count($items) > 0) {
                    $data = array();
                    $i = 0;
                    foreach ($items as $item) {
                        $data[$i]['id'] = $this->getEcommProdid($item->getProduct());
                        $data[$i]['price'] = $item->getRowTotal();
                        $data[$i]['quantity'] = (int) $item->getQty();
                        $i++;
                    }
                    $params['criteo_product_basket_products'] = $data;
                }

                unset($cart, $items, $item, $data, $i);
                break;

            case 'onestepcheckout-index':
                if ('success' == $this->getRequest()->getActionName()) {
                    $lastOrderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
                    $cart = Mage::getModel('sales/order')->loadByIncrementId($lastOrderId);
                    $params['criteo_pagetype'] = 'TransactionPage';
                    $params['criteo_customer_email'] = $customerEmail;

                    $items = $cart->getAllVisibleItems();

                    if (count($items) > 0) {
                        $data = array();
                        $i = 0;
                        foreach ($items as $item) {
                            $data[$i]['id'] = $this->getEcommProdid($item->getProduct());
                            $data[$i]['price'] = $item->getRowTotal();
                            $data[$i]['quantity'] = (int) $item->getQty();
                            $i++;
                        }
                        $params['criteo_product_transaction_products'] = $data;
                        $params['criteo_transaction_id'] = $lastOrderId;
                    }
                    unset($cart, $items, $item, $data);
                }
                break;

            case 'checkout-onepage':
                if ('success' == $this->getRequest()->getActionName()) {
                    $lastOrderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
                    $cart = Mage::getModel('sales/order')->loadByIncrementId($lastOrderId);
                    $params['criteo_pagetype'] = 'TransactionPage';
                    $params['criteo_customer_email'] = $customerEmail;

                    $items = $cart->getAllVisibleItems();

                    if (count($items) > 0) {
                        $data = array();
                        $i = 0;
                        foreach ($items as $item) {
                            $data[$i]['id'] = $this->getEcommProdid($item->getProduct());
                            $data[$i]['price'] = $item->getRowTotal();
                            $data[$i]['quantity'] = (int) $item->getData('qty_ordered');
                            $i++;
                        }
                        $params['criteo_product_transaction_products'] = $data;
                        $params['criteo_transaction_id'] = $lastOrderId;
                    }
                    unset($cart, $items, $item, $data);
                }
                break;

            default:
                break;
        }

        return $params;
    }


    /**
     * Get ShareASale data for use in the data layer.
     *
     * @return array
     */
    protected function _getShareASaleData() {
        $params = array();

        $page_type = Mage::app()->getRequest()->getModuleName() . '-' . Mage::app()->getFrontController()->getRequest()->getControllerName();
        $actionName = $this->getRequest()->getActionName();
        if(($page_type == 'onestepcheckout-index' && 'success' == $actionName) ||
            ($page_type == 'checkout-onepage' && 'success' == $actionName)) {
            $params['shareasale_sscid'] = ! empty( $_COOKIE['shareasaleMagentoSSCID'] ) ? $_COOKIE['shareasaleMagentoSSCID'] : '';
            $params['shareasale_order_id'] = Mage::getSingleton('checkout/session')->getLastRealOrderId();
            $order = Mage::getModel('sales/order')->loadByIncrementId($params['shareasale_order_id']);
            $total = $order->getGrandTotal(); //not used
            $subtotal = $order->getSubtotal();
            $discount = $order->getDiscountAmount();
            $params['shareasale_amount'] = ($subtotal + $discount);
            $ordered_items = $order->getAllVisibleItems();
            $params['shareasale_skulist'] = ''; //setup empty skulist param
            $params['shareasale_pricelist'] = ''; //setup empty pricelist param
            $params['shareasale_quantitylist'] = ''; //setup empty quantitylist param
            $last_index = array_search(end($ordered_items), $ordered_items, true);
            foreach($ordered_items as $index => $item){
                $delimiter = $index === $last_index ? '' : ',';
                $params['shareasale_skulist'] .= $item->getSku() . $delimiter;
                $params['shareasale_quantitylist'] .= ceil($item->getQtyOrdered()) . $delimiter;
                //append correct item base price, before any kind of cart or product discount
                $params['shareasale_pricelist'] .= ($item->getProduct()->getFinalPrice() - ($item->getDiscountAmount() / $item->getQtyOrdered())) . $delimiter;
            }
            //Magento has only one customer coupon code allowed, so no comma-separated list to make...
            $params['shareasale_couponcode'] = $order->getCouponCode();
            $params['shareasale_newcustomer'] = ''; //setup newcustomer param
            $customer = $order->getCustomerId();
            if($customer){
                $_orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('customer_id', $customer);
                $_orderCnt = $_orders->count(); //orders count
                //if customer has more than an order, they're not new. Some developers will use another method, like matching an email address or credit card number.
                $params['shareasale_newcustomer'] = ($_orderCnt > 1 ? 0 : 1);
            }
            //setup currency code
            $params['shareasale_currency'] = $order->getOrderCurrencyCode();
        }

        return $params;
    }

}
