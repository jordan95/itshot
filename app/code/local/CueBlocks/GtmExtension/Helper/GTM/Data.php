<?php
/**
 * Created by PhpStorm.
 * User: suneet
 * Date: 20/02/19
 * Time: 12:17 PM
 */ 
class CueBlocks_GtmExtension_Helper_GTM_Data extends Shopgo_GTM_Helper_Data {
    const XML_PATH_REMARKETING_DATALAYER = 'google/gtm/remarketing_datalayer';
    const XML_PATH_CRITEO_DATALAYER = 'google/gtm/criteo_datalayer';
    const XML_PATH_SHAREASALE_DATALAYER = 'google/gtm/shareasale_datalayer';

    /**
     * Add remarketing data to the data layer?
     *
     * @return bool
     */
    public function isRemarketingDataLayerEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_REMARKETING_DATALAYER);
    }

    /**
     * Add criteo data to the data layer?
     *
     * @return bool
     */
    public function isCriteoDataLayerEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_CRITEO_DATALAYER);
    }

    /**
     * Add ShareASale data to the data layer?
     *
     * @return bool
     */
    public function isShareASaleDataLayerEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_SHAREASALE_DATALAYER);
    }
}